﻿Option Strict On
Imports System.ComponentModel
Imports System.Data.SqlClient
Public Class clsLetterFields

    Public Function GetList(ByVal intModuleRefId As Integer) As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetLetterFields"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@email_ref_id", SqlDbType.Int)).Value = CInt(intModuleRefId)

                        da.SelectCommand = cmd
                        da.Fill(ds, "LetterType")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return ds
    End Function
End Class
