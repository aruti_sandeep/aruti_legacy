﻿Public Class Career
    Inherits Base_General


#Region "Provate Variable"
    'Private objSearchJob As New clsSearchJob
    'Private dsVacancyList As DataSet = Nothing
    Private blnIsArutiHRM As Boolean = True
#End Region

#Region " Method Functions "

    Private Sub FillVacancyList(strCompCode As String _
                                , intCompanyunkid As Integer _
                                , strExtInt As String
                                )
        Try
            'dsVacancyList = objSearchJob.GetApplicantVacancy(strCompCode:=strCompCode,
            '                                          intComUnkID:=intCompanyunkid,
            '                                          intMasterTypeId:=clsCommon_Master.enCommonMaster.VACANCY_MASTER,
            '                                          intEType:=clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE,
            '                                          blnVacancyType:=True,
            '                                          blnAllVacancy:=False,
            '                                          intDateZoneDifference:=0,
            '                                          strVacancyUnkIdLIs:="")

            'Dim dsApplyedJob As DataSet = (New clsApplicantApplyJob).GetApplicantAppliedJob(strCompCode:=Session("CompCode").ToString,
            '                                                                                intComUnkID:=CInt(Session("companyunkid")),
            '                                                                                intApplicantUnkId:=CInt(Session("applicantunkid")))
            'If dsApplyedJob IsNot Nothing AndAlso dsApplyedJob.Tables(0).Rows.Count > 0 Then
            '    Dim str = String.Join(",", dsApplyedJob.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("vacancyid").ToString))
            '    For Each dRow As DataRow In dsVacancyList.Tables(0).Select("vacancyid IN (" & str & ")")
            '        dRow.Item("IsApplied") = True
            '    Next

            'End If
            'dsVacancyList.AcceptChanges()
            'dlVaanciesList.DataSource = New DataView(dsVacancyList.Tables(0), "IsApplied = 0", "IsApplied ASC, vacancytitle ASC", DataViewRowState.CurrentRows).ToTable
            'dlVaanciesList.DataBind()
            odsVacancy.SelectParameters.Item("strCompCode").DefaultValue = strCompCode
            odsVacancy.SelectParameters.Item("intComUnkID").DefaultValue = intCompanyunkid
            odsVacancy.SelectParameters.Item("intMasterTypeId").DefaultValue = clsCommon_Master.enCommonMaster.VACANCY_MASTER
            odsVacancy.SelectParameters.Item("intEType").DefaultValue = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE
            'Sohail (31 May 2018) -- Start
            'TANAPA Enhancement - Ref #  : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
            'odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = True
            If strExtInt = "9" Then
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = False
            Else
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = True
            End If
            'Sohail (31 May 2018) -- End
            odsVacancy.SelectParameters.Item("blnAllVacancy").DefaultValue = False
            odsVacancy.SelectParameters.Item("intDateZoneDifference").DefaultValue = 0
            odsVacancy.SelectParameters.Item("strVacancyUnkIdLIs").DefaultValue = ""
            odsVacancy.SelectParameters.Item("intApplicantUnkId").DefaultValue = 0
            If Session("email") Is Nothing OrElse (Roles.IsUserInRole(Session("email").ToString, "admin") = False AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = False) Then
                odsVacancy.SelectParameters.Item("blnOnlyCurrent").DefaultValue = True
            Else
                odsVacancy.SelectParameters.Item("blnOnlyCurrent").DefaultValue = False
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            Dim blnIsAdmin As Boolean = False
            LanguageOpner.Visible = False
            LanguageControl.Visible = False 'Sohail (05 May 2021)
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        blnIsAdmin = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            blnIsAdmin = True
                        End If
                    End If
                End If
            End If
            'Sohail (16 Aug 2019) -- End
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Session("LangId") = 1

                Dim strComp As String = ""
                Dim intCompID As Integer = 0
                Dim ExtInt As String = "7"

                If Request.QueryString("cc") Is Nothing OrElse Request.QueryString("cc") = "" OrElse Request.QueryString("ext") Is Nothing OrElse Request.QueryString("ext") = "" Then
                    'Response.Redirect("UnauthorizedAccess.aspx", False)
                    'Exit Try
                    'Sohail (27 Oct 2020) -- Start
                    'NMB Enhancement # : Allow to access login page with simple URL and without encrypted text in URL.
                    Dim strVD As String = Request.ApplicationPath.ToLower.Replace("/", "")
                    blnIsArutiHRM = True
                    If strVD <> "arutihrm" Then
                        Dim objCompany As New clsCompany
                        Dim ds As DataSet = objCompany.GetCompanyByVirtualDirectory(strVD)
                        If ds.Tables(0).Rows.Count > 0 Then
                            Session("companyunkid") = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))
                            Session("CompCode") = ds.Tables(0).Rows(0).Item("company_code").ToString
                            Session("Vacancy") = "Ext"
                            strComp = ds.Tables(0).Rows(0).Item("company_code").ToString
                            intCompID = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

                            Dim blnActive As Boolean = False
                            Dim objApplicant As New clsApplicant
                            If objApplicant.GetCompanyInfo(CInt(Session("companyunkid")), Session("CompCode").ToString, blnActive) = False Then
                                'Response.Redirect("UnauthorizedAccess.aspx", False)
                                Exit Sub
                            Else
                                If blnActive = False Then
                                    ShowMessage("Sorry, This company is not active.", MessageType.Info)
                                    Exit Try
                                End If
                            End If
                            objApplicant = Nothing

                            blnIsArutiHRM = False
                        End If
                        objCompany = Nothing
                    End If
                    'Sohail (27 Oct 2020) -- End
                Else
                    strComp = clsCrypto.Dicrypt(Request.QueryString("cc").ToString)
                    Dim a As String() = Split(strComp, "?")

                    If a.Length <> 2 Then
                        Response.Redirect("UnauthorizedAccess.aspx", False)
                        Exit Sub
                    Else
                        intCompID = CInt(a(0))
                        strComp = a(1)
                    End If

                    If Request.QueryString("ext") IsNot Nothing Then
                        ExtInt = Request.QueryString("ext").ToString
                    End If

                    blnIsArutiHRM = True
                End If

                Session("CompCode") = strComp
                Session("companyunkid") = intCompID
                'Dim objApplicant As New clsApplicant
                'Dim strMsg As String = ""
                'If objApplicant.IsValidForApplyVacancy(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), True, strMsg) = False Then
                '    If strMsg.Trim <> "" Then
                '        ShowMessage("Sorry, Your profile is incomplete. \n Please fill required details." & strMsg)
                '        HttpContext.Current.Response.Redirect("~/User/UserHome.aspx", False)
                '    End If
                '    Exit Sub
                'End If

                Call FillVacancyList(strComp, intCompID, ExtInt)
            Else
                'dsVacancyList = CType(Me.ViewState("dsVacancyList"), DataSet)
                blnIsArutiHRM = CBool(ViewState("blnIsArutiHRM"))
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SearchJob_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ViewState("blnIsArutiHRM") = Session("blnIsArutiHRM")
            'Me.ViewState("dsVacancyList") = dsVacancyList
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " DataList Events "

    Private Sub dlVaanciesList_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlVaanciesList.ItemCommand
        Try
            If e.CommandName.ToUpper = "APPLY" Then
                If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Dim Idx As Integer = e.Item.ItemIndex
                'Dim txtVFF As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtVacancyFoundOutFrom"), TextBox)
                'Dim txtCom As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtComments"), TextBox)
                'Dim dtp As DateCtrl = CType(dlVaanciesList.Items(Idx).FindControl("dtpEarliestPossibleStartDate"), DateCtrl)
                'Dim chk As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkAccept"), CheckBox)

                'If CBool(Session("VacancyFoundOutFromMandatory")) = True AndAlso txtVFF.Text.Trim = "" Then
                '    ShowMessage("Please enter Vacancy Found Out From.", MessageType.Errorr)
                '    txtVFF.Focus()
                '    Exit Try
                'ElseIf CBool(Session("EarliestPossibleStartDateMandatory")) = True AndAlso dtp.GetDate = CDate("01/Jan/1900") Then
                '    ShowMessage("Please enter Earliest Possible Start Date.", MessageType.Errorr)
                '    dtp.Focus()
                '    Exit Try
                'ElseIf CBool(Session("EarliestPossibleStartDateMandatory")) = True AndAlso dtp.GetDate < Today.Date Then
                '    ShowMessage("Earliest Possible Start Date should be greater than current date.", MessageType.Errorr)
                '    dtp.Focus()
                '    Exit Try
                'ElseIf CBool(Session("CommentsMandatory")) = True AndAlso txtCom.Text.Trim = "" Then
                '    ShowMessage("Please enter Comments.", MessageType.Errorr)
                '    txtCom.Focus()
                '    Exit Try
                'End If

                'If chk.Visible = True AndAlso chk.Checked = False Then
                '    ShowMessage("Please accept the Declaration to proceed.", MessageType.Errorr)
                '    Exit Try
                'End If
                'Sohail (31 May 2018) -- Start
                'TANAPA Enhancement - Ref #  : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
                'Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/Login.aspx?ext=7&cc=" & CType(dlVaanciesList.Items(Idx).FindControl("objlblAuthCode"), Label).Text & "", False)
                Dim ExtInt As String = "7"
                If Request.QueryString("ext") IsNot Nothing Then
                    ExtInt = Request.QueryString("ext").ToString
                End If
                'Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/Login.aspx?ext=" & ExtInt & "&cc=" & CType(dlVaanciesList.Items(Idx).FindControl("objlblAuthCode"), Label).Text & "", False)
                Response.Redirect("Login.aspx?ext=" & ExtInt & "&cc=" & CType(dlVaanciesList.Items(Idx).FindControl("objlblAuthCode"), Label).Text & "", False)
                'Response.Redirect(Request.Form(hflocationorigin.UniqueID) & Request.ApplicationPath & "/Login.aspx?ext=" & ExtInt & "&cc=" & CType(dlVaanciesList.Items(Idx).FindControl("objlblAuthCode"), Label).Text & "", False)
                'Sohail (31 May 2018) -- End
                Exit Sub

                'If objSearchJob.AppliedVacancy(strCompCode:=Session("CompCode").ToString,
                '                             intComUnkID:=CInt(Session("companyunkid")),
                '                             intApplicantUnkid:=CInt(Session("applicantunkid")),
                '                             intVacancyUnkId:=CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString),
                '                             dtEarliest_possible_startdate:=CDate("01/Jan/1900"),
                '                             strComments:="",
                '                             strVacancy_found_out_from:=""
                '                             ) = True Then
                '    ShowMessage("Job Applied Successfully !", MessageType.Info)
                'End If

                'Call FillVacancyList()
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dlVaanciesList_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlVaanciesList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl As Label = CType(e.Item.FindControl("objlblIsApplyed"), Label)
                CType(e.Item.FindControl("objlblAuthCode"), Label).Visible = False

                If CBool(lbl.Text) = True Then
                    CType(e.Item.FindControl("lblApplyText"), Label).Text = "Applied"
                    CType(e.Item.FindControl("btnApply"), LinkButton).Enabled = False
                    CType(e.Item.FindControl("divApplied"), Control).Visible = True
                    'If Session("applicant_declaration").ToString.Trim.Length > 0 Then
                    'CType(e.Item.FindControl("pnlDis"), Panel).Visible = False
                    'End If
                Else
                    'If Session("applicant_declaration").ToString.Trim.Length > 0 Then
                    '    CType(e.Item.FindControl("btnApply"), LinkButton).Attributes.Add("disabled", "disabled")
                    'End If
                    'If Session("applicant_declaration").ToString.Trim.Length <= 0 Then
                    'CType(e.Item.FindControl("pnlDis"), Panel).Visible = False
                    'End If
                End If
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("experience").ToString.Trim.Length <= 0 Then
                CType(e.Item.FindControl("objlblVacancyTitle"), Label).Text = drv.Item("vacancytitle").ToString & " (" & drv.Item("noposition").ToString & " " & lblPositionMsg.Text & ")"
                'If drv.Item("experience").ToString.Trim.Length <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                'If CInt(drv.Item("experience")) <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                If CInt(drv.Item("experience")) <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divExp"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isexpbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isexpitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    'If drv.Item("other_experience").ToString.Trim = "" Then
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    Else
                        '    Dim htmlOutput = "Document.html"
                        '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                        '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_experience").ToString, contentUriPrefix)
                        '    htmlResult.WriteToFile(htmlOutput)
                        '    CType(e.Item.FindControl("objlblExp"), Label).Text = htmlResult._HTML
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length > 0 Then
                        If drv.Item("other_experience").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_experience").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_experience").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If
                            Dim strRTF As String = drv.Item("other_experience").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_experience").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblExp"), Label).Text = strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If drv.Item("noposition").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divNoPosition"), Control).Visible = False
                End If

                'Gajanan (04 July 2020) -- Start
                'Enhancment: #0004765
                If drv.Item("experience_comment").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblExpCmt"), Control).Visible = False
                End If
                'Gajanan (04 July 2020) -- End

                'Sohail (11 Nov 2021) -- Start
                'NMB Enhancement: Display Job Location on published vacancy in online recruitment when company code is NMB. e.g Job Location - Northern Zone, Arusha.
                Dim arrJobLocation As New ArrayList
                If Session("CompCode").ToString.ToUpper = "NMB" Then
                    If drv.Item("classgroupname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classgroupname").ToString)
                    End If
                    If drv.Item("classname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classname").ToString)
                    End If
                End If
                CType(e.Item.FindControl("objlblJobLocation"), Label).Text = String.Join(", ", TryCast(arrJobLocation.ToArray(GetType(String)), String()))
                If arrJobLocation.Count <= 0 Then
                    CType(e.Item.FindControl("divJobLocation"), Control).Visible = False
                End If
                'Sohail (11 Nov 2021) -- End

                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divSkill"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isskillbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isskillitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Hemant (08 Jul 2021) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'If drv.Item("other_skill").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_skill").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                        'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                        Dim str As String = ""
                        If drv.Item("skill").ToString.Trim.Length > 0 Then
                            str = "<ul class='p-l-17'>"
                            Dim arr() As String = drv.Item("skill").ToString.Split(";")
                            For i As Integer = 0 To arr.Length - 1
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Next
                            str &= "</ul>"
                        End If
                        CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length > 0 Then
                        If drv.Item("other_skill").ToString.Trim.Length > 0 Then
                            Dim str As String = ""
                            If drv.Item("skill").ToString.Trim.Length > 0 Then
                                str = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("skill").ToString.Split(";")
                                For i As Integer = 0 To arr.Length - 1
                                    str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                Next
                                str &= "</ul>"
                            End If
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_skill").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_skill").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                            
                            Dim strRTF As String = drv.Item("other_skill").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_skill").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                    'Hemant (08 Jul 2021) -- End
                End If

                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
                'If drv.Item("Lang").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divLang"), Control).Visible = False
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                Else
                    'If drv.Item("other_language").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_language").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_language").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length > 0 Then
                        If drv.Item("other_language").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_language").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_language").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                          
                            Dim strRTF As String = drv.Item("other_language").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_language").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If CDec(drv.Item("pay_from").ToString) <= 0 AndAlso CDec(drv.Item("pay_to").ToString) <= 0 Then
                    CType(e.Item.FindControl("divScale"), Control).Visible = False
                Else
                    'Dim str As String = Math.Round(CDec(drv.Item("pay_from").ToString), 2) & " - " & Math.Round(CDec(drv.Item("pay_to").ToString), 2)
                    Dim str As String = Format(CDec(drv.Item("pay_from").ToString), "##,##,##,##,##0.00") & " - " & Format(CDec(drv.Item("pay_to").ToString), "##,##,##,##,##0.00")
                    CType(e.Item.FindControl("objlblScale"), Label).Text = str
                End If

                If drv.Item("openingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divOpenningDate"), Control).Visible = False
                End If
                If drv.Item("closingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divClosuingDate"), Control).Visible = False
                End If
                If drv.Item("remark").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divRemark"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("remark").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblJobDiscription"), Label).Text = strResult
                End If
                If drv.Item("duties").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divResponsblity"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("duties").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblResponsblity"), Label).Text = strResult
                End If
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("Qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString).Length <= 0 Then
                If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divQualification"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isqualibold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isqualiitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'If drv.Item("other_qualification").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_qualification").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_qualification").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length > 0 Then
                        If drv.Item("other_qualification").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_qualification").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_qualification").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                           
                            Dim strRTF As String = drv.Item("other_qualification").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_qualification").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If blnIsArutiHRM = False OrElse Request.QueryString.Count > 0 Then
                    CType(e.Item.FindControl("pnlCompanyName"), Panel).Visible = False
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Protected Sub chkAccept_CheckedChanged(sender As Object, e As EventArgs)
    '    Try

    '        Dim chk As CheckBox = CType(sender, CheckBox)
    '        Dim dlItem As DataListItem = CType(chk.NamingContainer, DataListItem)
    '        If chk.Checked Then
    '            CType(dlItem.FindControl("btnApply"), LinkButton).Attributes.Remove("disabled")
    '        Else
    '            CType(dlItem.FindControl("btnApply"), LinkButton).Attributes.Add("disabled", "disabled")
    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub odsVacancy_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles odsVacancy.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is DataTable Then
                intC = CType(e.ReturnValue, DataTable).Rows.Count
            End If

            objlblCount.Text = "(" & intC.ToString & ")"

            If intC <= 0 Then
                pnlNoVacancy.Visible = True
            Else
                pnlNoVacancy.Visible = False
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region "Button Event(S)"

    Private Sub btnSearchVacancies_Click(sender As Object, e As EventArgs) Handles btnSearchVacancies.Click
        'Dim dtTable As DataTable = Nothing
        Try
            'If txtVacanciesSearch.Text.Trim.Length > 0 Then
            '    Dim strSearch As String = "vacancytitle LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR remark LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR company_name LIKE '%" & txtVacanciesSearch.Text.Trim & "%' "
            '    Dim drow() As DataRow = dsVacancyList.Tables(0).Select(strSearch)
            '    If drow.Length > 0 Then
            '        dtTable = drow.CopyToDataTable
            '    Else
            '        dtTable = dsVacancyList.Tables(0).Clone
            '    End If
            'Else
            '    dtTable = dsVacancyList.Tables(0)
            'End If
            'dlVaanciesList.DataSource = New DataView(dtTable, "IsApplied = 0", "IsApplied ASC, vacancytitle ASC", DataViewRowState.CurrentRows).ToTable
            'dlVaanciesList.DataBind()
            odsVacancy.FilterExpression = "vacancytitle LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR remark LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR company_name LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR skill LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR Qualification LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR duties LIKE '%" & txtVacanciesSearch.Text.Trim & "%' "
            dlVaanciesList.DataBind()
            objlblCount.Text = "(" & dlVaanciesList.Items.Count & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then LanguageControl.show()
    End Sub

    Private Sub btnLoginBioData_Click(sender As Object, e As EventArgs) Handles btnLoginBioData.Click
        Dim str As String = Request.Url.AbsolutePath.ToString.ToLower.Replace("career.aspx", "login.aspx")
        Response.Redirect(str & Request.Url.Query.ToString, False)
    End Sub
    'Sohail (16 Aug 2019) -- End

#End Region

End Class