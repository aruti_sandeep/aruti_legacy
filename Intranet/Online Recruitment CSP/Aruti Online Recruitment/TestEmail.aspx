﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TestEmail.aspx.vb" Inherits="Aruti_Online_Recruitment.TestEmail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Test Email</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="stylesheet" href="Content/bootstrap-switch/bootstrap3/bootstrap-switch.css" />
    <link rel="stylesheet" href="Content/bootstrap-switch/bootstrap3/bootstrap-switch.min.css" />
    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
    <link rel="stylesheet" href="customtheme.css" />

    <script type="text/javascript" src="WebResource.js"></script>
    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-switch.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-switch.min.js"></script>

    <%--<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>--%>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $(".switch input").bootstrapSwitch();

        }

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }

       <%-- var onloadCallback = function () {
            grecaptcha.render('dvCaptcha', {
                'sitekey': '<%= ReCaptcha_Key %>',
                'callback': function (response) {
                    PageMethods.VerifyCaptcha(response, onSuccess, onFailure);
                    //$.ajax({
                    //    type: "POST",
                    //    url: "TestEmail.aspx/VerifyCaptcha",
                    //    data: '{response: ' + JSON.stringify(response) + '}',
                    //    contentType: "application/json; charset=utf-8",
                    //    dataType: "json",
                    //    success: function (r) {
                    //        debugger;
                    //        var captchaResponse = jQuery.parseJSON(r.d);
                    //        alert(r.d);
                    //        //if (captchaResponse.success) {
                    //        if (captchaResponse == true) {
                    //            //$("[id*=txtCaptcha]").val(captchaResponse.success);
                    //            $("[id*=txtCaptcha]").val(captchaResponse);
                    //            $("[id*=rfvCaptcha]").hide();
                    //            //$("[id*=txtstatus]").val(captchaResponse.success);
                    //            $("[id*=txtstatus]").val(captchaResponse);
                    //        } else {
                    //            $("[id*=txtCaptcha]").val("");
                    //            $("[id*=rfvCaptcha]").show();
                    //            var error = captchaResponse["error-codes"][0];
                    //            alert(error);
                    //            $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
                    //        }
                    //    },
                    //    error: function(r){
                    //        debugger;
                    //        cosole.log(r);
                    //    }

                    //});

                    //if (typeof (grecaptcha) != 'undefined') {
                    //    var response = grecaptcha.getResponse();
                    //    alert(jQuery.parseJSON(response.d));
                    //    (response.length === 0) ? (message = 'Captcha verification failed') : (message = 'Success!');
                    //} 
                }
            });
        };--%>

        //function onSuccess(captchaResponse, usercontext, methodname) {
        //    $("[id*=txtCaptcha]").val(captchaResponse);
        //    $("[id*=rfvCaptcha]").hide();
        //    $("[id*=txtstatus]").val(captchaResponse);
        //}

        //function onFailure(error, usercontext, methodname) {
        //    $("[id*=txtCaptcha]").val("");
        //    $("[id*=rfvCaptcha]").show();
        //    var error = captchaResponse["error-codes"][0];
        //    alert(error);
        //    $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
        //}

        function Ticked(id, i) {
            alert(id);
            //PageMethods.SMTPEWS_Ticked
        }



        $(document).ready(function () {

            $$('btnSubmit').on('click', function () {
                return IsValidate();
            });

            $$('btnTestEmail').on('click', function () {
                return IsTestEmailValidate();
            });

            $$('radSMTP').on('change', function () {
                __doPostBack('<%= radSMTP.UniqueID %>', '');
            });

            $$('radEWS').on('change', function () {
                __doPostBack('<%= radEWS.UniqueID %>', '');
            });



        });

        function IsValidate() {
            if ($$('txtVerificationCode').val().trim() == '') {
                ShowMessage('Please Enter Verification Code.', 'MessageType.Errorr');
                $$('txtVerificationCode').focus();
                return false;
            }
        }

        function IsTestEmailValidate() {
            if ($$('txtSenderName').val().trim() == '') {
                ShowMessage('Please Enter Sender Name', 'MessageType.Errorr');
                $$('txtSenderName').focus();
                return false;
            }

            else if (IsValidEmail($$('txtSenderName').val()) == false) {
                ShowMessage('Receiver Email address is not valid', 'MessageType.Errorr')
                $$('txtSenderName').focus();
                return false;
            }

            else if ($$('txtSenderAddress').val().trim() == '') {
                ShowMessage('Please Enter Sender Email', 'MessageType.Errorr')
                $$('txtSenderAddress').focus();
                return false;
            }

            else if (document.getElementById("radSMTP").checked && IsValidEmail($$('txtSenderAddress').val()) == false) {
                ShowMessage('Sender Email address is not valid', 'MessageType.Errorr')
                $$('txtSenderAddress').focus();
                return false;
            }

            else if ($$('txtPassword').val().trim() == '') {
                ShowMessage('Please Enter Password', 'MessageType.Errorr')
                $$('txtPassword').focus();
                return false;
            }

            else if (document.getElementById("radSMTP").checked && $$('txtMailServer').val().trim() == '') {
                ShowMessage('Please Enter SMTP Mail Server.', 'MessageType.Errorr')
                $$('txtMailServer').focus();
                return false;
            }

            else if (document.getElementById("radSMTP").checked && $$('txtMailServerPort').val().trim() == '') {
                ShowMessage('Please Enter Mail Server Port.', 'MessageType.Errorr')
                $$('txtMailServerPort').focus();
                return false;
            }

            else if (document.getElementById("radEWS").checked && $$('txtEWS_URL').val().trim() == '') {
                ShowMessage('Please Enter EWS URL.', 'MessageType.Errorr')
                $$('txtEWS_URL').focus();
                return false;
            }

            else if (document.getElementById("radEWS").checked && $$('txtEWS_Domain').val().trim() == '') {
                ShowMessage('Please Enter EWS Domain.', 'MessageType.Errorr')
                $$('txtEWS_Domain').focus();
                return false;
            }

        }

        function IsValidEmail(strEmail) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(strEmail)) {
                return false;
            } else {
                return true;
            }
        }



    </script>

    <%--   <style>
        .ModalPopupBG {
            /*background-color: #000000;
            filter: alpha(opacity=50);
            opacity: 0.5;
            z-index: 100100 !important;*/
            background-color: #000000;
            filter: alpha(opacity=60);
            opacity: 0.6;
            z-index: 100001;
            -moz-opacity: 0.6;
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0px;
            left: 0px;
            padding-top: 20%;
            text-align: center;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManger1" runat="server" EnablePageMethods="true"></asp:ScriptManager>



        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div id="MainDivCtrl" class="container-lg w-100 mt-3 mr-auto mb-0 ml-auto">
            <div class="card">


                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:Panel ID="pnlCaptcha" runat="server">
                            <div class="card-header col-md-4">
                                <h4>Security Check</h4>
                            </div>

                            <div class="card-body">
                                <%--<div class="form-group row">

                                    <div class="col-md-4">--%>

                                <%--<cc2:Recaptcha ID="Recaptcha1" runat="server" />--%>
                                <%-- <div id="dvCaptcha" class="g-recaptcha">
                                        </div>
                                        <asp:TextBox ID="txtCaptcha" runat="server" CssClass="d-none" />--%>
                                <%--Style="display: none"--%>
                                <%--<asp:RequiredFieldValidator ID="rfvCaptcha" ErrorMessage="Captcha validation is required." ControlToValidate="txtCaptcha" ValidationGroup="Submit"
                                            runat="server" ForeColor="Red" Display="Dynamic" />--%>

                                <%--<asp:TextBox ID="txtstatus" runat="server" />--%>

                                <%--</div>
                                </div>--%>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <asp:Label ID="lblVerificationCode" runat="server" >Verification Code:</asp:Label>
                                        <img src="Captcha.aspx">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <asp:Label ID="lblEnterVerificationCode" runat="server">Enter Verifaction Code:</asp:Label>
                                        <asp:TextBox runat="server" ID="txtVerificationCode" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer col-md-4">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary w-100" Text="Continue" ValidationGroup="Submit" />
                                        <%--Style="width: 100%;"--%>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <asp:Panel ID="pnlEmail" runat="server" Visible="false">
                            <div class="card-header">
                                <h4>Email Settings</h4>
                            </div>

                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <asp:RadioButton ID="radSMTP" runat="server" Text=" SMTP" Checked="true" GroupName="EmailType" AutoPostBack="false" />
                                    </div>

                                    <div class="col-md-6">
                                        <asp:RadioButton ID="radEWS" runat="server" Text=" Microsoft Exchange Web Service (EWS)" GroupName="EmailType" AutoPostBack="false" />
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-4">
                                        <%--<asp:Label ID="lblSenderName" runat="server" Text="Sender Name:"></asp:Label>--%>
                                        <asp:Label ID="lblSenderName" runat="server" Text="Receiver Email:"></asp:Label>
                                        <asp:TextBox ID="txtSenderName" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="rtfSenderName" runat="server" Display="None"
                                            ControlToValidate="txtSenderName" ErrorMessage="Receiver Email cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSenderName"
                                            ErrorMessage="Receiver Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="TestEmail" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                    </div>

                                    <div class="col-md-4">
                                        <asp:Label ID="lblSenderAddress" runat="server" Text="Sender Email:"></asp:Label>
                                        <asp:TextBox ID="txtSenderAddress" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                            ControlToValidate="txtSenderAddress" ErrorMessage="Sender Email cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                        <%--<asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="txtSenderAddress"
                                        ErrorMessage="Sender Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="TestEmail" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                    </div>

                                    <div class="col-md-4">
                                        <%--<asp:Label ID="lblReference" runat="server" Text="Reference:"></asp:Label>
                <asp:TextBox ID="txtReference" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="PersonalInfo"></asp:TextBox>--%>
                                        <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label>
                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="50" TextMode="Password" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
                                            ControlToValidate="txtPassword" ErrorMessage="Password cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                    </div>

                                </div>


                                <asp:MultiView ID="mvEmail_Type" runat="server" ActiveViewIndex="0">

                                    <asp:View ID="vwSMTP" runat="server">

                                        <div class="form-group row">

                                            <div class="col-md-4">
                                                <asp:Label ID="lblMailServer" runat="server" Text="Mail Server (SMTP):"></asp:Label>
                                                <asp:TextBox ID="txtMailServer" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="rfvMailServer" runat="server" Display="None"
                                                    ControlToValidate="txtMailServer" ErrorMessage="SMTP Mail Server cannot be blank "
                                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label ID="lblMailServerPort" runat="server" Text="Mail Server Port:"></asp:Label>
                                                <asp:TextBox ID="txtMailServerPort" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="rfvMailServerPort" runat="server" Display="None"
                                                    ControlToValidate="txtMailServerPort" ErrorMessage="Mail Server Post cannot be blank "
                                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                <cc1:FilteredTextBoxExtender ID="ftbMailServerPort" runat="server" TargetControlID="txtMailServerPort" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="col-md-3">
                                                <asp:Label ID="lblReference" runat="server" Text="Sender Name:"></asp:Label>
                                                <asp:TextBox ID="txtReference" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:CheckBox ID="chkIsSSL" runat="server" Text="SSL" CssClass="d-block" />
                                            </div>

                                        </div>

                                    </asp:View>

                                    <asp:View ID="vwEWS" runat="server">
                                        <div class="form-group row">

                                            <div class="col-md-4">
                                                <asp:Label ID="lblEWS_URL" runat="server" Text="EWS URL:"></asp:Label>
                                                <asp:TextBox ID="txtEWS_URL" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="rfvEWS_URL" runat="server" Display="None"
                                                    ControlToValidate="txtEWS_URL" ErrorMessage="EWS URL cannot be blank "
                                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="col-md-4">
                                                <asp:Label ID="lblEWS_Domain" runat="server" Text="EWS Domain:"></asp:Label>
                                                <asp:TextBox ID="txtEWS_Domain" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="rfvEWS_Domain" runat="server" Display="None"
                                                    ControlToValidate="txtEWS_Domain" ErrorMessage="EWS Domain cannot be blank "
                                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <%--<div class="col-md-4">
                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="SSL" Style="display: block;" />
                                            </div>--%>
                                        </div>
                                    </asp:View>

                                </asp:MultiView>

                                <div class="form-group row">
                                    <div class="col-md-12 text-center">
                                        <%--<asp:ValidationSummary
                                            HeaderText="You must enter a value in the following fields:"
                                            DisplayMode="BulletList"
                                            EnableClientScript="true"
                                            runat="server" ValidationGroup="TestEmail" Style="color: red" />--%>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="form-group row">

                                    <div class="col-md-4">
                                    </div>

                                    <div class="col-md-4">
                                        <asp:Button ID="btnTestEmail" runat="server" CssClass="btn btn-primary w-100" Text="Test Email" ValidationGroup="TestEmail" />
                                    </div>

                                    <div class="col-md-4">
                                    </div>

                                </div>
                            </div>

                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnTestEmail" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>


            </div>
        </div>
    </form>
    <script type="text/javascript">

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {

                $$('btnSubmit').on('click', function () {
                    return IsValidate();
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('btnTestEmail').on('click', function () {
                    return IsTestEmailValidate();
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('radSMTP').on('change', function () {
                    __doPostBack('<%= radSMTP.UniqueID %>', '');
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('radEWS').on('change', function () {
                    __doPostBack('<%= radEWS.UniqueID %>', '');
                });

            });



        };
    </script>
    <script type="text/javascript" src="showmessage.js"></script>
</body>
</html>
