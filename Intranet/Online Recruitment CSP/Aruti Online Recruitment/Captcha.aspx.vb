﻿'Imports System
'Imports System.Collections.Generic
'Imports System.Drawing
'Imports System.Drawing.Drawing2D
'Imports System.Drawing.Imaging
'Imports System.Linq
'Imports System.Web
'Imports System.Web.UI
'Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Public Class Captcha
    Inherits Base_General

    ' For generating random numbers.
    Private random As Random = New Random()
    Private newPropertyValue As Object
    Public Property NewProperty() As Object
        Get
            Return newPropertyValue
        End Get
        Set(ByVal value As Object)
            newPropertyValue = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim objBitmap As Bitmap = New Bitmap(130, 80)
            'Dim objGraphics As Graphics = Graphics.FromImage(objBitmap)
            'objGraphics.Clear(Color.White)
            'Dim objRandom As Random = New Random()
            'objGraphics.DrawLine(Pens.Black, objRandom.Next(0, 50), objRandom.Next(10, 30), objRandom.Next(0, 200), objRandom.Next(0, 50))
            'objGraphics.DrawRectangle(Pens.Blue, objRandom.Next(0, 20), objRandom.Next(0, 20), objRandom.Next(50, 80), objRandom.Next(0, 20))
            'objGraphics.DrawLine(Pens.Blue, objRandom.Next(0, 20), objRandom.Next(10, 50), objRandom.Next(100, 200), objRandom.Next(0, 80))
            'Dim objBrush As Brush = Nothing

            ''create background style
            'Dim aHatchStyles() As HatchStyle = New HatchStyle() {HatchStyle.BackwardDiagonal, HatchStyle.Cross, HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal, HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical, HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross, HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid, HatchStyle.ForwardDiagonal, HatchStyle.Horizontal, HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard, HatchStyle.LargeConfetti, HatchStyle.LargeGrid, HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal}


            ''create rectangular area
            'Dim oRectangleF As RectangleF = New RectangleF(0, 0, 300, 600)
            'objBrush = New HatchBrush(aHatchStyles(objRandom.Next(aHatchStyles.Length - 3)), Color.FromArgb((objRandom.Next(100, 255)), (objRandom.Next(100, 255)), (objRandom.Next(100, 255))), Color.White)
            'objGraphics.FillRectangle(objBrush, oRectangleF)

            ''Generate the image for captcha
            ''Dim captchaText As String = String.Format("{0:X}", objRandom.Next(1000000, 9999999))
            'Dim combination As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            'Dim captchaText As StringBuilder = New StringBuilder()
            'For i As Integer = 0 To 6
            '    captchaText.Append(combination(objRandom.Next(combination.Length)))
            'Next
            ''add the captcha value in session
            'Session("CaptchaVerify") = captchaText.ToString()
            'Dim objFont As Font = New Font("Courier New", 15, FontStyle.Bold)

            ''Draw the image for captcha
            'objGraphics.DrawString(captchaText.ToString(), objFont, Brushes.Black, 20, 20)
            'objBitmap.Save(Response.OutputStream, ImageFormat.Gif)

            ' Create a CAPTCHA image using the text stored in the Session object.
            HttpContext.Current.Session("CaptchaImageText") = GenerateRandomCode()
            'Dim ci As CaptchaImage = New CaptchaImage(Me.Session("CaptchaImageText").ToString(), 200, 50, "Arial Black")
            Dim ci As CaptchaImage = New CaptchaImage(Me.Session("CaptchaImageText").ToString(), 200, 50, "Tahoma")

            ' Change the response headers to output a JPEG image.
            Me.Response.Clear()
            Me.Response.ContentType = "image/jpeg"

            ' Write the image to the response stream in JPEG format.
            ci._Image.Save(Me.Response.OutputStream, ImageFormat.Jpeg)

            ' Dispose of the CAPTCHA image object.
            ci.Dispose()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Function GenerateRandomCode() As String
        Dim s As String = ""
        Dim objRandom As Random = New Random()
        Try
            'Dim i As Integer
            'For i = 0 To 6 - 1 Step i + 1
            '    s = String.Concat(s, Me.random.Next(10).ToString())
            'Next
            Dim combination As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            Dim captchaText As StringBuilder = New StringBuilder()
            For i As Integer = 0 To 6
                captchaText.Append(combination(objRandom.Next(combination.Length)))
            Next
            s = captchaText.ToString()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try

        Return s
    End Function

End Class