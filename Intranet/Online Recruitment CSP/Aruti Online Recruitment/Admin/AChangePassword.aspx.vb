﻿Option Strict On

Imports System.Data.SqlClient

Public Class AChangePassword
    Inherits Base_Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim ConString As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim ConString As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End
                Dim Email As String, UserID As String
                Dim i As Integer = 0

                If (Request.QueryString("UserID") IsNot Nothing) AndAlso (Request.QueryString("Email") IsNot Nothing) Then
                    UserID = Request.QueryString("UserID")
                    Email = Request.QueryString("Email")

                    ChangePassword1.UserName = Email

                End If

            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ChangePassword1_ChangedPassword(sender As Object, e As EventArgs) Handles ChangePassword1.ChangedPassword
        Try
            ChangePassword1.SuccessPageUrl = Session("mstrUrlArutiLink").ToString
            Session.Abandon()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ChangePassword1_ChangingPassword(sender As Object, e As LoginCancelEventArgs) Handles ChangePassword1.ChangingPassword
        Try
            If ChangePassword1.UserName.Trim.ToLower <> Session("email").ToString.Trim.ToLower Then
                ShowMessage("Sorry, You cannot change password of different email.")
                e.Cancel = True
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
End Class