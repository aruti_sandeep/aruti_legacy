﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
'Imports eZeeCommonLib

Public Class clsAdmin

#Region " General Methods "

    Public Function CreateAdminUser(strAdminEmail As String) As Boolean
        Dim createStatus As MembershipCreateStatus
        Try
            Dim arrAdmin() As String = {"", "suhail_patel@yahoo.co.in"} 'strAdminEmail

            For Each strName As String In arrAdmin

                If strName.Trim.Length <= 0 Then Continue For

                If Membership.GetUser(strName) Is Nothing Then
                    Membership.CreateUser(strName, clsSecurity.Decrypt("CNjh9qOZUGhuqVpVp/LjZv1WE1HQbDTNvm5tKCg4KhA=", "ezee"), strName, "sp", "64", True, createStatus)

                    Select Case createStatus

                        Case MembershipCreateStatus.Success
                            'lblResult.ForeColor = Color.Green
                            'lblResult.Text = "The user account was successfully created"
                            Exit Select

                        Case MembershipCreateStatus.DuplicateUserName
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The user with the same UserName already exists!"
                            Exit Select

                        Case MembershipCreateStatus.DuplicateEmail
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The user with the same email address already exists!"
                            Exit Select

                        Case MembershipCreateStatus.InvalidEmail
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The email address you provided is invalid."
                            Exit Select
                        Case MembershipCreateStatus.InvalidAnswer
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The security answer was invalid."
                            Exit Select

                        Case MembershipCreateStatus.InvalidPassword
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The password you provided is invalid. It must be 7 characters long and have at least 1 special character."
                            Exit Select
                        Case Else
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "There was an unknown error; the user account was NOT created."
                            Exit Select

                    End Select
                End If

                If Roles.IsUserInRole(strName, "admin") = False Then
                    Roles.AddUserToRole(strName, "admin")
                End If
            Next

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

#End Region
End Class
