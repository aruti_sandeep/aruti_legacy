﻿Imports System.IO

Public Class ASendNotification
    Inherits Base_Page


#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Call FillCombo()
                dsApplicants.SelectParameters.Item("intTitleId").DefaultValue = CInt(clsCommon_Master.enCommonMaster.TITLE)
                dsApplicants.SelectParameters.Item("intVacancyMasterTypeId").DefaultValue = CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER)
                dsApplicants.SelectParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString()
                dsApplicants.SelectParameters.Item("intMaxalert").DefaultValue = CInt(Session("maxvacancyalert"))
				dsApplicants.SelectParameters.Item("isAdmin").DefaultValue = Roles.IsUserInRole(Session("email").ToString(), "admin")
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SendNotification_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Method Functions "

    Private Sub FillCombo()
        Dim objVacancy As New clsSearchJob
        Dim objNotification As New clsNotificationSettings
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Try
            dsCombo = objNotification.GetApplicantNotificationStatus()
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "1"
            End With

            dsCombo = objApplicant.GetPageSize()
            With cboPageSize
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "15"
            End With

            dsCombo = objVacancy.GetApplicantVacancy(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.VACANCY_MASTER, clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, CBool(IIf(Session("Vacancy") = "Int", False, True)), True, CInt(Session("timezone_minute_diff")), "")
            Dim dr As DataRow = dsCombo.Tables(0).NewRow
            dr.Item("vacancyid") = 0
            dr.Item("vacancy_title") = "All"
            dsCombo.Tables(0).Rows.InsertAt(dr, 0)
            dsCombo.Tables(0).AcceptChanges()

            With cboVacancy
                .DataValueField = "vacancyid"
                .DataTextField = "vacancy_title"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objNotification = Nothing
            objVacancy = Nothing
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Try
            If CInt(Session("companyunkid")) <= 0 OrElse cboVacancy.Items.Count <= 0 Then
                Exit Try
            End If



            dsList = clsNotificationSettings.GetApplicantsNotificationList(strCompCode:=Session("CompCode").ToString _
                                                   , intComUnkID:=CInt(Session("companyunkid")) _
                                                   , intApplicantUnkId:=0 _
                                                   , intVacancyUnkId:=CInt(cboVacancy.SelectedValue) _
                                                   , strFirstName:=txtFirstName.Text _
                                                   , strSurName:=txtSurname.Text _
                                                   , strEmail:=txtEmail.Text _
                                                   , strPresentMobileNo:=txtMobile.Text _
                                                   , intStatusId:=CInt(cboStatus.SelectedValue) _
                                                   , intTitleId:=clsCommon_Master.enCommonMaster.TITLE _
                                                   , intVacancyMasterTypeId:=clsCommon_Master.enCommonMaster.VACANCY_MASTER _
                                                   , intPageSize:=10 _
                                                   , startRowIndex:=1 _
                                                   , strSortExpression:="" _
												   , intMaxalert:=CInt(Session("maxvacancyalert")) _
												   , isAdmin:=Roles.IsUserInRole(Session("email").ToString(), "admin")
                                                   )

            grdApplicants.Columns(0).Visible = True
            grdApplicants.Columns(1).Visible = True
            grdApplicants.Columns(2).Visible = True
            grdApplicants.DataSource = dsList.Tables(0)
            grdApplicants.DataBind()
            lblCount.Text = "(" & grdApplicants.Rows.Count & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In grdApplicants.Rows
                cb = CType(grdApplicants.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                cb.Checked = blnCheckAll
            Next
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboPageSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPageSize.SelectedIndexChanged
        Try
            grdApplicants.PageSize = CInt(cboPageSize.SelectedValue)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnSendNotification_Click(sender As Object, e As EventArgs) Handles btnSendNotification.Click
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try

            Dim blnChecked As Boolean = False
            Dim blnSuccess As Boolean = True

            If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                                                   , strCompCode:=Session("CompCode").ToString() _
                                                   , ByRefblnIsActive:=False
                                                   ) = False Then

                Exit Try
            End If

            Dim objCompany1 As New clsCompany
            Session("e_emailsetupunkid") = 0
            objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))

            Dim objNotification As New clsNotificationSettings

            For Each dgRow As GridViewRow In grdApplicants.Rows
                Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)
                If cb IsNot Nothing AndAlso cb.Checked Then
                    Dim intApplicantUnkId As Integer = CInt(grdApplicants.DataKeys(dgRow.RowIndex).Item("applicantunkid"))
                    Dim intVacancyunkId As Integer = CInt(grdApplicants.DataKeys(dgRow.RowIndex).Item("vacancyunkid"))
                    Dim blnIsEmailSent As Boolean = CBool(grdApplicants.DataKeys(dgRow.RowIndex).Item("isemailsent"))

                    Dim dsList As DataSet = objNotification.GetApplicantForJobAlerts(xCompanyId:=CInt(Session("companyunkid")), mstrCompanyCode:=Session("CompCode").ToString() _
                                                                                                                            , xMaxVacancyAlert:=CInt(Session("maxvacancyalert")), xApplicantID:=intApplicantUnkId _
                                                                                                                            , xVacancyID:=intVacancyunkId, mblnIsEmailSent:=blnIsEmailSent)

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                        If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdApplicants.DataKeys(dgRow.RowIndex).Item("email").ToString) _
                                  , strSubject:=Server.HtmlDecode(dsList.Tables(0).Rows(0)("subject").ToString()) _
                                  , strBody:=Server.HtmlDecode(dsList.Tables(0).Rows(0)("message").ToString)
                                  ) = False Then
                            blnSuccess = False
                        Else
                            objNotification.InsertUpdateApplicantVacancyNotfications(xCompanyId:=CInt(Session("companyunkid")), mstrCompanyCode:=Session("CompCode").ToString(), xApplicantID:=intApplicantUnkId _
                                                                                                                       , xVacancyID:=intVacancyunkId, xNotificationpriorityID:=CInt(dsList.Tables(0).Rows(0)("appnotificationpriorityunkid")) _
                                                                                                                       , mstrLink:=dsList.Tables(0).Rows(0)("link").ToString(), mstrSubject:=dsList.Tables(0).Rows(0)("subject").ToString() _
                                                                                                                       , mstrMessage:=dsList.Tables(0).Rows(0)("message").ToString(), mblnIsEmailSent:=True)
                        End If

                    End If
                    blnChecked = True
                End If

            Next

            If blnChecked = False Then
                ShowMessage("Please tick atleast one email to send job alert.", MessageType.Info)
            ElseIf blnSuccess = False Then
                ShowMessage("Mail sending failed!", MessageType.Errorr)
            Else
                grdApplicants.DataBind()
                ShowMessage("Job Alert Sent successfully!", MessageType.Success)
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Try
            txtFirstName.Text = ""
            txtSurname.Text = ""
            txtEmail.Text = ""
            txtMobile.Text = ""
            cboVacancy.SelectedValue = 0
            cboPageSize.SelectedValue = 15
            cboStatus.SelectedValue = 1
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    Private Sub grdApplicants_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdApplicants.RowCommand
        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub dsApplicants_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsApplicants.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(e.ReturnValue, intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdApplicants.PageIndex = 0
            End If
            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region


End Class