﻿Option Strict On
Imports System.Net
Imports System.Net.Mail
Imports System.Web.Services
Imports Microsoft.Exchange.WebServices.Data

Public Class TestEmail1
    Inherits Base_Page

#Region " Private Function "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetCompanyForCombo("", 0, True)
            With cboCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                If CInt(Session("companyunkid")) > 0 Then
                    .SelectedValue = Session("companyunkid").ToString
                    Call cboCompany_SelectedIndexChanged(cboCompany, New System.EventArgs)
                End If
            End With

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objCompany = Nothing
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try

            If txtReceiverName.Text.Trim = "" Then
                ShowMessage("Please Enter Receiver Name", MessageType.Errorr)
                txtReceiverName.Focus()
                Return False
            ElseIf txtSenderAddress.Text.Trim = "" Then
                ShowMessage("Please Enter Sender Email", MessageType.Errorr)
                txtSenderAddress.Focus()
                Return False
            ElseIf txtPassword.Text.Trim = "" Then
                ShowMessage("Please Enter Password", MessageType.Errorr)
                txtPassword.Focus()
                Return False
            ElseIf radSMTP.Checked = True AndAlso txtMailServer.Text.Trim = "" Then
                ShowMessage("Please Enter SMTP Mail Server.", MessageType.Errorr)
                txtMailServer.Focus()
                Return False
            ElseIf radSMTP.Checked = True AndAlso txtMailServerPort.Text.Trim = "" Then
                ShowMessage("Please Enter Mail Server Port.", MessageType.Errorr)
                txtMailServerPort.Focus()
                Return False
            ElseIf radEWS.Checked = True AndAlso txtEWS_URL.Text.Trim = "" Then
                ShowMessage("Please Enter EWS URL.", MessageType.Errorr)
                txtEWS_URL.Focus()
                Return False
            ElseIf radEWS.Checked = True AndAlso txtEWS_Domain.Text.Trim = "" Then
                ShowMessage("Please Enter EWS Domain.", MessageType.Errorr)
                txtEWS_Domain.Focus()
                Return False
            End If

            'If clsApplicant.IsValidEmail(txtSenderAddress.Text) = False Then
            If radSMTP.Checked = True AndAlso clsApplicant.IsValidEmail(txtSenderAddress.Text) = False Then
                ShowMessage("Sender Email address is not valid", MessageType.Errorr)
                txtSenderAddress.Focus()
                Return False
            ElseIf clsApplicant.IsValidEmail(txtReceiverName.Text) = False Then
                ShowMessage("Receiver Email address is not valid", MessageType.Errorr)
                txtReceiverName.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub EmailType()
        Try
            If radSMTP.Checked = True Then
                mvEmail_Type.ActiveViewIndex = 0
                'lblSenderName.Text = "Sender Name"
                lblSenderAddress.Text = "Sender Email"
                'EmailValidator.ControlToValidate = "txtSenderAddress"
                'txtSenderAddress.ValidationGroup = "TestEmail"
                'EmailValidator.ValidationGroup = "TestEmail"

                txtMailServer.ValidationGroup = "TestEmail"
                txtMailServerPort.ValidationGroup = "TestEmail"

                txtEWS_URL.ValidationGroup = "nothing"
                txtEWS_Domain.ValidationGroup = "nothing"
            ElseIf radEWS.Checked = True Then
                mvEmail_Type.ActiveViewIndex = 1
                'lblSenderName.Text = "User Name"
                lblSenderAddress.Text = "User Name"
                'EmailValidator.ControlToValidate = ""
                'txtSenderAddress.ValidationGroup = "nothing"
                'EmailValidator.ValidationGroup = "nothing"

                txtMailServer.ValidationGroup = "nothing"
                txtMailServerPort.ValidationGroup = "nothing"

                txtEWS_URL.ValidationGroup = "TestEmail"
                txtEWS_Domain.ValidationGroup = "TestEmail"
            End If

            If Session("e_emailsetupunkid") IsNot Nothing AndAlso CInt(HttpContext.Current.Session("e_emailsetupunkid")) > 0 Then
                'txtSenderName.Text = ""
                txtSenderAddress.Text = Session("e_username").ToString
                txtPassword.Text = Session("e_password").ToString
                txtMailServer.Text = Session("e_mailserverip").ToString
                txtMailServerPort.Text = Session("e_mailserverport").ToString
                txtSenderName.Text = Session("e_sendername").ToString
                chkIsSSL.Checked = CBool(Session("e_isloginssl"))

                txtEWS_URL.Text = Session("e_ews_url").ToString
                txtEWS_Domain.Text = Session("e_ews_domain").ToString
                If Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                    txtKey.Text = Session("e_originuserpassword").ToString
                End If
            Else
                'txtSenderName.Text = ""
                txtSenderAddress.Text = Session("username").ToString
                txtPassword.Text = Session("password").ToString
                txtMailServer.Text = Session("mailserverip").ToString
                txtMailServerPort.Text = Session("mailserverport").ToString
                txtSenderName.Text = Session("sendername").ToString
                chkIsSSL.Checked = CBool(Session("isloginssl"))

                txtEWS_URL.Text = Session("ews_url").ToString
                txtEWS_Domain.Text = Session("ews_domain").ToString
                If Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                    txtKey.Text = Session("originuserpassword").ToString
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Page Events "
    Private Sub TestEmail_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                    pnlCompany.Visible = True
                Else
                    cboCompany.SelectedValue = Session("companyunkid").ToString
                    'Call cboCompany_SelectedIndexChanged(cboCompany, New System.EventArgs)

                    If CInt(Session("email_type")) = 0 Then 'SMTP
                        radSMTP.Checked = True
                        radEWS.Checked = False
                    ElseIf CInt(Session("email_type")) = 1 Then 'EWS
                        radSMTP.Checked = False
                        radEWS.Checked = True
                    End If
                    Call radSMTP_CheckedChanged(radSMTP, New System.EventArgs)

                    pnlCompany.Visible = False
                End If
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '    LanguageControl.Visible = True
            'Else
            '    LanguageControl.Visible = False
            'End If
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub TestEmail1_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        Try
            If Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                Me.MasterPageFile = "~/SAdmin/Site2.Master"
            Else
                Me.MasterPageFile = "~/Admin/Site3.Master"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Private Sub cboCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCompany.SelectedIndexChanged
        Dim objApplicant As New clsApplicant
        Dim objCompany As New clsCompany
        Try
            Dim strCompCode As String = ""
            Dim intCompanyunkid As Integer = CInt(cboCompany.SelectedValue)

            If intCompanyunkid > 0 Then

                Dim strCode As String = objCompany.GetCompanyCode(intComUnkID:=intCompanyunkid)
                If objApplicant.GetCompanyInfo(intCompanyunkid, strCode, True) = True Then

                    Dim objCompany1 As New clsCompany
                    Session("e_emailsetupunkid") = 0
                    objCompany1.GetEmailSetup(strCode, intCompanyunkid)

                    If CInt(Session("email_type")) = 0 Then 'SMTP
                        radSMTP.Checked = True
                        radEWS.Checked = False
                    ElseIf CInt(Session("email_type")) = 1 Then 'EWS
                        radSMTP.Checked = False
                        radEWS.Checked = True
                    End If
                    Call radSMTP_CheckedChanged(radSMTP, New System.EventArgs)
                    'Sohail (16 Aug 2019) -- Start
                    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
                    'If Me.IsPostBack = True Then LanguageControl.SetControlLanguage()
                    'Sohail (16 Aug 2019) -- End
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
        End Try
    End Sub
#End Region

#Region " Button Events "

    Private Sub btnTestEmail_Click(sender As Object, e As EventArgs) Handles btnTestEmail.Click
        Dim message As MailMessage
        Dim smtp As SmtpClient
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            'If Recaptcha1.VerifyIfSolvedAsync IsNot Nothing Then
            '    'Response.Redirect("Welcome.aspx")
            'Else
            '    ShowMessage("Incorrect CAPTCHA response.", MessageType.Errorr)

            '    Exit Try
            'End If

            Dim intPort As Integer
            Integer.TryParse(txtMailServerPort.Text, intPort)

            Dim strUserName As String = txtSenderAddress.Text.Trim
            Dim strReceiverName As String = txtReceiverName.Text.Trim

            If radSMTP.Checked = True Then '0 = SMTP, 1 = EWS

                'message = New MailMessage(strUserName, strUserName)
                'message = New MailMessage(strUserName, strReceiverName)
                Dim strSenderName As String = txtSenderName.Text
                If strSenderName.Trim.Length <= 0 Then
                    strSenderName = strUserName
                End If
                message = New MailMessage(New MailAddress(strUserName, strSenderName), New MailAddress(strReceiverName))

                'message.To.Add(strUserName)
                message.To.Add(strReceiverName)
                message.Subject = "Test Email"
                message.IsBodyHtml = True

                message.Body = "<P>Hi,</P><P>This is testing email from Aruti Online Recruitment. Please do not reply.</P>"

                smtp = New SmtpClient
                smtp.Host = txtMailServer.Text.Trim
                smtp.Port = intPort
                smtp.Credentials = New System.Net.NetworkCredential(strUserName, txtPassword.Text.Trim)
                smtp.EnableSsl = chkIsSSL.Checked

                'Sohail (22 Jan 2020) -- Start
                'ZRA Issue - # 0004046 : System Failure to send activation Link.
                System.Net.ServicePointManager.Expect100Continue = False
                System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
                'Sohail (22 Jan 2020) -- End
                Try
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
                Catch ex As Exception

                End Try

                smtp.Send(message)

            ElseIf radEWS.Checked = True Then '0 = SMTP, 1 = EWS

                'Creating the ExchangeService Object 
                Dim objService As New ExchangeService()

                'Seting Credentials to be used
                'objService.Credentials = New WebCredentials(txtSenderName.Text, txtPassword.Text.Trim, txtEWS_Domain.Text)
                objService.Credentials = New WebCredentials(strUserName, txtPassword.Text.Trim, txtEWS_Domain.Text)

                'Setting the EWS Uri
                objService.Url = New Uri(txtEWS_URL.Text)

                'Creating an Email Service and passing in the service
                Dim objMessage As New EmailMessage(objService)

                objMessage.Subject = "Test Email"
                'objMessage.ToRecipients.Add(strUserName)
                objMessage.ToRecipients.Add(strReceiverName)

                objMessage.Body = "<P>Hi,</P><P>This is testing email from Aruti Online Recruitment. Please do not reply.</P>"

                'objMessage.Send()
                objMessage.SendAndSaveCopy()

            End If


            ShowMessage("Email sent successfully!", MessageType.Info)

            'Response.Redirect("TestEmail.aspx", False)
        Catch ex As Exception
            'ShowMessage(ex.Message.Replace("'", "`") & "\n" & ex.InnerException.Message.Replace("'", "`") & "\n" & ex.InnerException.InnerException.Message.Replace("'", "`"), MessageType.Errorr)
            Dim strMsg As String = ex.Message.Replace("'", "`")
            If ex.InnerException IsNot Nothing Then
                strMsg &= "\n" & ex.InnerException.Message.Replace("'", "`")

                If ex.InnerException.InnerException IsNot Nothing Then
                    strMsg &= "\n" & ex.InnerException.InnerException.Message.Replace("'", "`")
                End If
            End If
            ShowMessage(strMsg, MessageType.Errorr)
        End Try
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Try
            Call cboCompany_SelectedIndexChanged(cboCompany, New System.EventArgs)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim objCompany As New clsCompany
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            Dim intPort As Integer
            Integer.TryParse(txtMailServerPort.Text, intPort)

            Dim intEMailType As Integer = 0
            If radSMTP.Checked = True Then '0 = SMTP, 1 = EWS
                intEMailType = 0
            ElseIf radEWS.Checked = True Then '0 = SMTP, 1 = EWS
                intEMailType = 1
            End If

            If Session("e_emailsetupunkid") IsNot Nothing AndAlso CInt(HttpContext.Current.Session("e_emailsetupunkid")) > 0 Then
                If objCompany.EditEmailSeup(strCompCode:=Session("CompCode").ToString _
                                            , intComUnkID:=CInt(Session("companyunkid")) _
                                            , intEmailsetupunkid:=CInt(Session("e_emailsetupunkid")) _
                                            , strUserName:=txtSenderAddress.Text.Trim _
                                            , strPassword:=txtPassword.Text _
                                            , strMailServerIP:=txtMailServer.Text.Trim _
                                            , intMailServerPort:=intPort _
                                            , blnIsLoginSSL:=chkIsSSL.Checked _
                                            , intEmailType:=intEMailType _
                                            , strEWS_URL:=txtEWS_URL.Text.Trim _
                                            , strEWS_Domain:=txtEWS_Domain.Text.Trim _
                                            , strSenderName:=txtSenderName.Text.Trim
                                            ) = True Then

                    ShowMessage("Email Settings Saved Successfully!", MessageType.Info)
                End If
            Else
                If objCompany.EditCompanyEmailSettings(strCompCode:=Session("CompCode").ToString _
                                                 , intComUnkID:=CInt(Session("companyunkid")) _
                                                 , strUserName:=txtSenderAddress.Text.Trim _
                                                 , strPassword:=txtPassword.Text _
                                                 , strMailServerIP:=txtMailServer.Text.Trim _
                                                 , intMailServerPort:=intPort _
                                                 , blnIsLoginSSL:=chkIsSSL.Checked _
                                                 , intEmailType:=intEMailType _
                                                 , strEWS_URL:=txtEWS_URL.Text.Trim _
                                                 , strEWS_Domain:=txtEWS_Domain.Text.Trim _
                                                 , strSenderName:=txtSenderName.Text.Trim
                                                 ) = True Then

                    ShowMessage("Email Settings Saved Successfully!", MessageType.Info)
                End If
            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objCompany = Nothing
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    'Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then LanguageControl.show()
    'End Sub
    'Sohail (16 Aug 2019) -- End

#End Region

#Region " Radio Buttons Events "

    Private Sub radSMTP_CheckedChanged(sender As Object, e As EventArgs) Handles radSMTP.CheckedChanged
        Try
            Call EmailType()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub radEWS_CheckedChanged(sender As Object, e As EventArgs) Handles radEWS.CheckedChanged
        Try
            Call EmailType()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class