﻿Option Strict On

Imports System.Data.SqlClient

Public Class ResetPassword
    Inherits Base_General


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objApplicant As New clsApplicant
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim ConString As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim ConString As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim i As Integer = 0
                Dim Email As String, Code As String, OTP As String, answer As String, lnk As String
                Dim ccode As String
                Dim cid As Integer = 0

                If (Request.QueryString("Email") IsNot Nothing) AndAlso (Request.QueryString("Code") IsNot Nothing) AndAlso Request.QueryString("OTP") IsNot Nothing AndAlso Request.QueryString("a") IsNot Nothing AndAlso Request.QueryString("lnk") IsNot Nothing Then
                    Email = clsCrypto.Dicrypt(Request.QueryString("Email"))
                    Code = clsCrypto.Dicrypt(Request.QueryString("Code"))
                    OTP = clsCrypto.Dicrypt(Request.QueryString("OTP"))
                    answer = clsCrypto.Dicrypt(Request.QueryString("a"))
                    lnk = clsCrypto.Dicrypt(Request.QueryString("lnk"))
                    ccode = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("c")))
                    cid = CInt(Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("i"))))

                    Dim strResult As String = ""
                    If objApplicant.IsExistResetPassword(strCompCode:=ccode _
                                                , intCompUnkID:=CInt(cid) _
                                                , strEmail:=Email _
                                                , strCode:=Code _
                                                , strOTP:=OTP _
                                                , strRefResult:=strResult
                                                ) = True Then
                        lblMsg.Text = ""
                        pnlReset.Visible = True
                        pnlSuccess.Visible = False
                    Else
                        Select Case strResult.Trim
                            Case "1"
                                lblSuccess.Text = ""

                            Case "-1"
                                lblSuccess.Text = "Sorry, This link got expired."

                            Case "-2"
                                lblSuccess.Text = "Sorry, Verification Code is wrong."

                            Case "-3"
                                lblSuccess.Text = "Sorry, This link is not valid."

                            Case Else
                                lblSuccess.Text = "Sorry, This link is invalid."
                        End Select
                        'lblSuccess.Text = strResult
                        pnlReset.Visible = False
                        pnlSuccess.Visible = True
                    End If

                End If

            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'lblSuccess.Text = ex.Message
            pnlReset.Visible = False
            pnlSuccess.Visible = True
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub btnChangePwd_Click(sender As Object, e As EventArgs) Handles btnChangePwd.Click
        Dim objApplicant As New clsApplicant
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If txtOTP.Text.Trim = "" Then
                ShowMessage("Please Enter Mobile No.", MessageType.Info)
                txtOTP.Focus()
                Exit Try
            ElseIf txtNewPwd.Text.Trim = "" Then
                ShowMessage("Please Enter Password.", MessageType.Info)
                txtNewPwd.Focus()
                Exit Try
            ElseIf txtConfirmPwd.Text.Trim = "" Then
                ShowMessage("Please Enter Confirm Password.", MessageType.Info)
                txtConfirmPwd.Focus()
                Exit Try
            ElseIf String.Compare(txtNewPwd.Text, txtConfirmPwd.Text, False) > 0 Then
                ShowMessage("The Password and Confirmation Password must match.", MessageType.Info)
                txtConfirmPwd.Focus()
                Exit Try
            End If

            Dim r As New Text.RegularExpressions.Regex("(?=.{7,})(?=(.*\d){1,})(?=(.*\W){1,})")

            If r.IsMatch(txtNewPwd.Text) = False Then
                ShowMessage("Password should have a minimum length of 7 characters. It should also include at least 1 special character such as ! # $ % ( ) * + , - . / \ : ; = ? @ [ ] ^ _ ` | ~ .")
                Exit Try
            End If

            Dim Email As String, Code As String, OTP As String, answer As String, lnk As String
            Dim ccode As String
            Dim cid As Integer = 0
            If (Request.QueryString("Email") IsNot Nothing) AndAlso (Request.QueryString("Code") IsNot Nothing) AndAlso Request.QueryString("OTP") IsNot Nothing AndAlso Request.QueryString("a") IsNot Nothing AndAlso Request.QueryString("lnk") IsNot Nothing Then
                Email = clsCrypto.Dicrypt(Request.QueryString("Email"))
                Code = clsCrypto.Dicrypt(Request.QueryString("Code"))
                OTP = clsCrypto.Dicrypt(Request.QueryString("OTP"))
                answer = clsCrypto.Dicrypt(Request.QueryString("a"))
                lnk = clsCrypto.Dicrypt(Request.QueryString("lnk"))
                ccode = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("c")))
                cid = CInt(Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("i"))))

                Dim strResult As String = ""
                If objApplicant.IsExistResetPassword(strCompCode:=ccode _
                                                    , intCompUnkID:=CInt(cid) _
                                                    , strEmail:=Email _
                                                    , strCode:=Code _
                                                    , strOTP:=txtOTP.Text.Trim _
                                                    , strRefResult:=strResult
                                                    ) = True Then


                    Dim u As MembershipUser = Membership.GetUser(Email, False)
                    Dim strPwd As String = ""

                    If u IsNot Nothing Then
                        '  UserID = u.ResetPassword(PasswordRecovery1.Answer)
                        strPwd = u.ResetPassword(answer)

                        If u.ChangePassword(strPwd, txtNewPwd.Text) = True Then
                            'lblMsg.Text = "Your Password has been changed succesfully! <BR>Click <a href='" & lnk & "'>here</a> to Login"
                            'Response.Write("Your Password has been changed succesfully! <BR>Click <a href='" & lnk & "'>here</a> to Login")

                            If objApplicant.ResetPasswordActivated(strCompCode:=Session("CompCode").ToString _
                                                                    , intCompUnkID:=CInt(Session("companyunkid")) _
                                                                    , strEmail:=Email _
                                                                    , strCode:=Code _
                                                                    , strOTP:=txtOTP.Text.Trim
                                                                    ) = True Then
                                pnlSuccess.Visible = True
                                pnlReset.Visible = False
                                lblSuccess.Text = "Your Password has been changed succesfully! <BR>Click <a href='" & lnk & "'>here</a> to Login"
                            End If
                        End If

                    End If
                Else
                    Select Case strResult.Trim
                        Case "1"
                            lblMsg.Text = ""

                        Case "-1"
                            lblMsg.Text = "Sorry, This link got expired."

                        Case "-2"
                            lblMsg.Text = "Sorry, Verification Code is wrong."

                        Case "-3"
                            lblMsg.Text = "Sorry, This link is not valid."

                        Case Else
                            lblMsg.Text = "Sorry, This link is invalid."
                    End Select

                End If
            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'lblMsg.Text = ex.Message
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        Finally
            objApplicant = Nothing
        End Try

    End Sub

    Private Sub ResetPassword_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
End Class