﻿Imports System.Security.Cryptography

Public Class clsCrypto
    Private Shared strKey As String = "@%^"
    Public Shared Function Encrypt(ByVal strText As String) As String
        Dim Crypt As New MD5CryptoServiceProvider
        Dim b() As Byte = Encoding.UTF8.GetBytes(strText)
        Dim key() As Byte

        key = Crypt.ComputeHash(Encoding.UTF8.GetBytes(strKey))
        Crypt.Clear()

        Dim t As New TripleDESCryptoServiceProvider
        t.Key = key
        t.Mode = CipherMode.ECB
        t.Padding = PaddingMode.PKCS7

        Dim it As ICryptoTransform = t.CreateEncryptor()
        Dim rslt() As Byte = it.TransformFinalBlock(b, 0, b.Length)
        t.Clear()

        Return Convert.ToBase64String(rslt, Base64FormattingOptions.None, rslt.Length)

    End Function

    Public Shared Function Dicrypt(ByVal strEncodedText As String) As String
        Dim Crypt As New MD5CryptoServiceProvider
        'Dim b() As Byte = Convert.FromBase64String(strEncodedText) 'Error : Invalid length for a Base-64 char array or string.
        Dim b() As Byte = Convert.FromBase64String(strEncodedText.Replace(" ", "+"))
        Dim key() As Byte

        key = Crypt.ComputeHash(Encoding.UTF8.GetBytes(strKey))
        Crypt.Clear()

        Dim t As New TripleDESCryptoServiceProvider
        t.Key = key
        t.Mode = CipherMode.ECB
        t.Padding = PaddingMode.PKCS7

        Dim it As ICryptoTransform = t.CreateDecryptor()
        Dim rslt() As Byte = it.TransformFinalBlock(b, 0, b.Length)
        t.Clear()

        Return Encoding.UTF8.GetString(rslt)
    End Function
End Class
