﻿Imports System.Data
Imports System.Data.SqlClient
Imports Aruti_Online_Recruitment

<Serializable>
Public Class clsWebMessages

    Public Sub New()
    End Sub

    Public Property _Comp_Code As String
    Public Property _companyunkid As String
    Public Property _messageunkid As Integer
    Public Property _messagecode As String
    Public Property _module_name As String
    Public Property _message As String
    Public Property _message1 As String
    Public Property _message2 As String

    Public Function GetAllMessages(intCompanyUnkId As Integer, strCompCode As String, ByVal strCsvModuleNames As String, ByVal strModuleName As String, ByVal intLanguageID As Integer, ByVal strMsgCode As String) As List(Of clsWebMessages)
        Dim list As List(Of clsWebMessages) = Nothing
        Dim StrQ As String = ""

        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim arrCls() As String = Nothing

            StrQ = "procGetMessages"

            If strCsvModuleNames.Trim().Length > 0 Then
                arrCls = strCsvModuleNames.Split(","c)
            End If

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(StrQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyUnkId
                        cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@LangId", SqlDbType.Int)).Value = intLanguageID
                        If strMsgCode <> "" Then
                            cmd.Parameters.Add(New SqlParameter("@messagecode", SqlDbType.NVarChar)).Value = strMsgCode
                        Else
                            cmd.Parameters.Add(New SqlParameter("@messagecode", SqlDbType.NVarChar)).Value = ""
                        End If
                        cmd.Parameters.Add(New SqlParameter("@module_name", SqlDbType.NVarChar)).Value = strModuleName

                        Using dataReader As SqlDataReader = cmd.ExecuteReader

                            If dataReader.HasRows Then
                                list = New List(Of clsWebMessages)()

                                While dataReader.Read()

                                    If arrCls IsNot Nothing AndAlso arrCls.Contains(dataReader("module_name").ToString) = False Then Continue While

                                    list.Add(New clsWebMessages() With {
                                            ._companyunkid = CInt(dataReader("companyunkid")),
                                            ._Comp_Code = dataReader("Comp_Code").ToString(),
                                            ._messageunkid = CInt(dataReader("messageunkid")),
                                            ._messagecode = dataReader("messagecode").ToString(),
                                            ._module_name = dataReader("module_name").ToString(),
                                            ._message = dataReader("message").ToString(),
                                            ._message1 = dataReader("message1").ToString(),
                                            ._message2 = dataReader("message2").ToString()
                                        })
                                End While
                            End If
                        End Using

                    End Using
                End Using
            End Using



        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try

        Return list
    End Function

    Public Function InsertUpdateDelete(strCompCode As String, intCompanyUnkId As Integer) As Boolean
        Dim flag As Boolean = False
        Dim strQ As String = ""

        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            strQ = "procInsertUpdateMessages"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyUnkId
                        cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@messageunkid", SqlDbType.Int)).Value = _messageunkid
                        cmd.Parameters.Add(New SqlParameter("@messagecode", SqlDbType.NVarChar)).Value = _messagecode
                        cmd.Parameters.Add(New SqlParameter("@module_name", SqlDbType.NVarChar)).Value = _module_name
                        cmd.Parameters.Add(New SqlParameter("@message", SqlDbType.NVarChar)).Value = _message
                        cmd.Parameters.Add(New SqlParameter("@message1", SqlDbType.NVarChar)).Value = _message1
                        cmd.Parameters.Add(New SqlParameter("@message2", SqlDbType.NVarChar)).Value = _message2

                        cmd.ExecuteNonQuery()

                    End Using
                End Using
            End Using

            flag = True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try

        Return flag
    End Function

    'Public Function getMessage(ByVal strModuleName As String, ByVal MsgCode As Integer, ByVal strDefaultMsg As String, ByVal strDBName As String, ByVal Optional intLanguageID As Integer = -1) As String
    '    Dim strMessage As String = ""
    '    Dim StrQ As String = ""

    '    Try
    '        Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

    '        StrQ = "DECLARE @LangId INT " & "SET @LangId = @lid  " & "SELECT " & " CASE WHEN @LangId = 1 THEN message1 " & "      WHEN @LangId = 2 THEN message2 " & " ELSE message END AS msg " & "FROM cfmessages " & "WHERE module_name = @module_name AND messagecode = @messagecode "

    '        Using con As New SqlConnection(strConn)

    '            con.Open()

    '            Using da As New SqlDataAdapter()
    '                Using cmd As New SqlCommand(StrQ, con)

    '                    cmd.CommandType = CommandType.Text
    '                    cmd.Parameters.Add(New SqlParameter("@lid", SqlDbType.Int)).Value = intLanguageID
    '                    cmd.Parameters.Add(New SqlParameter("@messagecode", SqlDbType.NVarChar)).Value = MsgCode
    '                    cmd.Parameters.Add(New SqlParameter("@module_name", SqlDbType.NVarChar)).Value = strModuleName

    '                    Using dataReader As SqlDataReader = cmd.ExecuteReader

    '                        If dataReader.HasRows Then

    '                            While dataReader.Read()
    '                                strMessage = dataReader("msg").ToString()
    '                            End While
    '                        Else
    '                            strMessage = strDefaultMsg
    '                        End If
    '                    End Using
    '                End Using
    '            End Using
    '        End Using

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, System.Web.HttpContext.Current)
    '    End Try

    '    Return strMessage
    'End Function

    Public Sub setmessage(strCompCode As String, intCompanyUnkId As Integer, ByVal strModulename As String, ByVal strMsgCode As Integer, ByVal strDefaultMsg As String, ByVal page As Page)
        Try
            Dim wMsgs As clsWebMessages = New clsWebMessages()
            Dim msg1 As String
            Dim msg2 As String
            Dim lst As List(Of clsWebMessages) = wMsgs.GetAllMessages(intCompanyUnkId:=intCompanyUnkId _
                                                 , strCompCode:=strCompCode _
                                                 , strCsvModuleNames:="" _
                                                 , strModuleName:=strModulename _
                                                 , intLanguageID:=0 _
                                                 , strMsgCode:=strMsgCode
                                                 )

            If lst.Count > 0 Then
                msg1 = lst(0)._message1
                msg2 = lst(0)._message2
            Else
                msg1 = strDefaultMsg
                msg2 = strDefaultMsg
            End If
            wMsgs._message = strDefaultMsg
            wMsgs._message1 = msg1
            wMsgs._message2 = msg2
            wMsgs._messagecode = strMsgCode
            wMsgs._module_name = strModulename

            Dim flag As Boolean = wMsgs.InsertUpdateDelete(strCompCode:=strCompCode, intCompanyUnkId:=intCompanyUnkId)

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
    End Sub
End Class

