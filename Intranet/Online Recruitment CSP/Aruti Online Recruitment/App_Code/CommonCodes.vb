﻿Imports Microsoft.VisualBasic
Imports System.Web.UI
Imports System.Net
Imports System.Web.HttpContext

Public Class CommonCodes
    'Dim objDataOperation As New eZeeCommonLib.clsDataOperation(True)
    Dim mSQL As String

    'Public Sub PkgDtaStrCommon()
    '    Try
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='hrskill_master') alter table hrskill_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcapplicant_master') alter table rcapplicant_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcapplicantqualification_tran') alter table rcapplicantqualification_tran add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcapplicantskill_tran') alter table rcapplicantskill_tran add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcjobhistory') alter table rcjobhistory add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfcommon_master') alter table cfcommon_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcvacancy_master') alter table rcvacancy_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfcity_master') alter table cfcity_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfstate_master') alter table cfstate_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)
    '        mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfcountry_master') alter table cfcountry_master add comp_code varchar(10)"
    '        objDataOperation.ExecNonQuery(mSQL)

    '        mSQL = ""

    '        mSQL = " if exists(select 1 from sys.procedures where name='Applicate_Insert') drop proc Applicate_Insert" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)

    '        mSQL = "Create procedure Applicate_Insert (" & vbCrLf
    '        mSQL += "@applicant_code cName," & vbCrLf
    '        mSQL += "@titleunkid int," & vbCrLf
    '        mSQL += "@firstname cName," & vbCrLf
    '        mSQL += "@surname cName," & vbCrLf
    '        mSQL += "@othername cName," & vbCrLf
    '        mSQL += "@gender cGender," & vbCrLf
    '        mSQL += "@email cEmail," & vbCrLf
    '        mSQL += "@present_address1 cAddress," & vbCrLf
    '        mSQL += "@present_address2 cAddress," & vbCrLf
    '        mSQL += "@present_countryunkid int," & vbCrLf
    '        mSQL += "@present_stateunkid int," & vbCrLf
    '        mSQL += "@present_province cState," & vbCrLf
    '        mSQL += "@present_post_townunkid int," & vbCrLf
    '        mSQL += "@present_zipcode int," & vbCrLf
    '        mSQL += "@present_road cName," & vbCrLf
    '        mSQL += "@present_estate cName," & vbCrLf
    '        mSQL += "@present_plotno cCardNo," & vbCrLf
    '        mSQL += "@present_mobileno  cCardNo," & vbCrLf
    '        mSQL += "@present_alternateno  cCardNo," & vbCrLf
    '        mSQL += "@present_tel_no cPhone," & vbCrLf
    '        mSQL += "@present_fax cPhone," & vbCrLf
    '        mSQL += "@perm_address1 cAddress," & vbCrLf
    '        mSQL += "@perm_address2 cAddress," & vbCrLf
    '        mSQL += "@perm_countryunkid int," & vbCrLf
    '        mSQL += "@perm_stateunkid int," & vbCrLf
    '        mSQL += "@perm_province cState," & vbCrLf
    '        mSQL += "@perm_post_townunkid int," & vbCrLf
    '        mSQL += "@perm_zipcode int," & vbCrLf
    '        mSQL += "@perm_road cState," & vbCrLf
    '        mSQL += "@perm_estate cState," & vbCrLf
    '        mSQL += "@perm_plotno cCardNo," & vbCrLf
    '        mSQL += "@perm_mobileno cCardNo," & vbCrLf
    '        mSQL += "@perm_alternateno cCardNo," & vbCrLf
    '        mSQL += "@perm_tel_no cPhone," & vbCrLf
    '        mSQL += "@perm_fax cPhone," & vbCrLf
    '        mSQL += "@birth_date datetime," & vbCrLf
    '        mSQL += "@marital_statusunkid int," & vbCrLf
    '        mSQL += "@anniversary_date datetime," & vbCrLf
    '        mSQL += "@language1unkid int," & vbCrLf
    '        mSQL += "@language2unkid int," & vbCrLf
    '        mSQL += "@language3unkid int," & vbCrLf
    '        mSQL += "@language4unkid int," & vbCrLf
    '        mSQL += "@nationality int," & vbCrLf
    '        mSQL += "@userunkid int," & vbCrLf
    '        mSQL += "@vacancyunkid int," & vbCrLf
    '        mSQL += "@isimport  bit," & vbCrLf
    '        mSQL += "@other_skill  cDescription," & vbCrLf
    '        mSQL += "@other_qualification  cDescription," & vbCrLf
    '        mSQL += "@Comp_Code nvarchar(255)," & vbCrLf
    '        mSQL += "@companyunkid int," & vbCrLf
    '        mSQL += "@Syncdatetime datetime," & vbCrLf
    '        mSQL += "@ImageName nvarchar(100)," & vbCrLf
    '        mSQL += "@Skill xml," & vbCrLf
    '        mSQL += "@Qualification Xml," & vbCrLf
    '        mSQL += "@Job xml  )" & vbCrLf
    '        mSQL += "as   " & vbCrLf
    '        mSQL += "DECLARE @TranName varchar , @ErrNo int , @mApplicantID int   " & vbCrLf
    '        'Sohail (15 Jul 2011) -- Start
    '        'mSQL += "set @TranName = 'SaveApplicate'  " & vbCrLf
    '        'mSQL += "print  @applicant_code" & vbCrLf
    '        'mSQL += " if exists(select applicant_code from rcApplicant_master where  applicant_code = @applicant_code)   " & vbCrLf
    '        'mSQL += "begin" & vbCrLf
    '        'mSQL += "select @applicant_code = max(applicant_code)  from rcapplicant_master " & vbCrLf
    '        'mSQL += "select @applicant_code = @applicant_code + 1   " & vbCrLf
    '        'mSQL += "End" & vbCrLf
    '        mSQL += "set @TranName = 'SaveApplicant'  " & vbCrLf
    '        'Sohail (15 Jul 2011) -- End
    '        mSQL += "begin tran @TranName   " & vbCrLf
    '        mSQL += "SET @ErrNo = @@ERROR     " & vbCrLf
    '        mSQL += "IF @ErrNo <> 0 GOTO Err   " & vbCrLf
    '        mSQL += "begin" & vbCrLf
    '        mSQL += "INSERT  INTO rcapplicant_master" & vbCrLf
    '        mSQL += "(applicant_code ," & vbCrLf
    '        mSQL += "titleunkid ," & vbCrLf
    '        mSQL += "firstname ," & vbCrLf
    '        mSQL += "surname ," & vbCrLf
    '        mSQL += "othername ," & vbCrLf
    '        mSQL += "gender ," & vbCrLf
    '        mSQL += "email ," & vbCrLf
    '        mSQL += "present_address1 ," & vbCrLf
    '        mSQL += "present_address2 ," & vbCrLf
    '        mSQL += "present_countryunkid ," & vbCrLf
    '        mSQL += "present_stateunkid ," & vbCrLf
    '        mSQL += "present_province ," & vbCrLf
    '        mSQL += "present_post_townunkid ," & vbCrLf
    '        mSQL += "present_zipcode ," & vbCrLf
    '        mSQL += "present_road ," & vbCrLf
    '        mSQL += "present_estate ," & vbCrLf
    '        mSQL += "present_plotno ," & vbCrLf
    '        mSQL += "present_mobileno  ," & vbCrLf
    '        mSQL += "present_alternateno  ," & vbCrLf
    '        mSQL += "present_tel_no ," & vbCrLf
    '        mSQL += "present_fax ," & vbCrLf
    '        mSQL += "perm_address1 ," & vbCrLf
    '        mSQL += "perm_address2 ," & vbCrLf
    '        mSQL += "perm_countryunkid ," & vbCrLf
    '        mSQL += "perm_stateunkid ," & vbCrLf
    '        mSQL += "perm_province ," & vbCrLf
    '        mSQL += "perm_post_townunkid ," & vbCrLf
    '        mSQL += "perm_zipcode ," & vbCrLf
    '        mSQL += "perm_road ," & vbCrLf
    '        mSQL += "perm_estate ," & vbCrLf
    '        mSQL += "perm_plotno ," & vbCrLf
    '        mSQL += "perm_mobileno ," & vbCrLf
    '        mSQL += "perm_alternateno ," & vbCrLf
    '        mSQL += "perm_tel_no ," & vbCrLf
    '        mSQL += "perm_fax ," & vbCrLf
    '        mSQL += "birth_date ," & vbCrLf
    '        mSQL += "marital_statusunkid ," & vbCrLf
    '        mSQL += "anniversary_date ," & vbCrLf
    '        mSQL += "language1unkid ," & vbCrLf
    '        mSQL += "language2unkid ," & vbCrLf
    '        mSQL += "language3unkid ," & vbCrLf
    '        mSQL += "language4unkid ," & vbCrLf
    '        mSQL += "nationality ," & vbCrLf
    '        mSQL += "userunkid ," & vbCrLf
    '        mSQL += "vacancyunkid ," & vbCrLf
    '        mSQL += "isimport  ," & vbCrLf
    '        mSQL += "other_skill ," & vbCrLf
    '        mSQL += "other_qualification ," & vbCrLf
    '        mSQL += "Comp_Code," & vbCrLf
    '        mSQL += "companyunkid," & vbCrLf
    '        mSQL += "Syncdatetime )" & vbCrLf
    '        mSQL += "values (@applicant_code ," & vbCrLf
    '        mSQL += "@titleunkid ," & vbCrLf
    '        mSQL += "@firstname ," & vbCrLf
    '        mSQL += "@surname ," & vbCrLf
    '        mSQL += "@othername ," & vbCrLf
    '        mSQL += "@gender ," & vbCrLf
    '        mSQL += "@email ," & vbCrLf
    '        mSQL += "@present_address1 ," & vbCrLf
    '        mSQL += "@present_address2 ," & vbCrLf
    '        mSQL += "@present_countryunkid ," & vbCrLf
    '        mSQL += "@present_stateunkid ," & vbCrLf
    '        mSQL += "@present_province ," & vbCrLf
    '        mSQL += "@present_post_townunkid ," & vbCrLf
    '        mSQL += "@present_zipcode ," & vbCrLf
    '        mSQL += "@present_road ," & vbCrLf
    '        mSQL += "@present_estate ," & vbCrLf
    '        mSQL += "@present_plotno ," & vbCrLf
    '        mSQL += "@present_mobileno  ," & vbCrLf
    '        mSQL += "@present_alternateno  ," & vbCrLf
    '        mSQL += "@present_tel_no ," & vbCrLf
    '        mSQL += "@present_fax ," & vbCrLf
    '        mSQL += "@perm_address1 ," & vbCrLf
    '        mSQL += "@perm_address2 ," & vbCrLf
    '        mSQL += "	@perm_countryunkid ," & vbCrLf
    '        mSQL += "	@perm_stateunkid ," & vbCrLf
    '        mSQL += "	@perm_province ," & vbCrLf
    '        mSQL += "	@perm_post_townunkid ," & vbCrLf
    '        mSQL += "	@perm_zipcode ," & vbCrLf
    '        mSQL += "	@perm_road ," & vbCrLf
    '        mSQL += "	@perm_estate ," & vbCrLf
    '        mSQL += "	@perm_plotno ," & vbCrLf
    '        mSQL += "	@perm_mobileno ," & vbCrLf
    '        mSQL += "	@perm_alternateno ," & vbCrLf
    '        mSQL += "		@perm_tel_no ," & vbCrLf
    '        mSQL += "		@perm_fax ," & vbCrLf
    '        mSQL += "		@birth_date ," & vbCrLf
    '        mSQL += "		@marital_statusunkid ," & vbCrLf
    '        mSQL += "		@anniversary_date ," & vbCrLf
    '        mSQL += "		@language1unkid ," & vbCrLf
    '        mSQL += "	@language2unkid ," & vbCrLf
    '        mSQL += "	@language3unkid ," & vbCrLf
    '        mSQL += "	@language4unkid ," & vbCrLf
    '        mSQL += "	@nationality ," & vbCrLf
    '        mSQL += "	@userunkid ," & vbCrLf
    '        mSQL += "	@vacancyunkid ," & vbCrLf
    '        mSQL += "	@isimport  ," & vbCrLf
    '        mSQL += "   @other_skill ," & vbCrLf
    '        mSQL += "   @other_qualification ," & vbCrLf
    '        mSQL += "   @Comp_Code," & vbCrLf
    '        mSQL += "   @companyunkid," & vbCrLf
    '        mSQL += "	@Syncdatetime )" & vbCrLf
    '        mSQL += "	 set @mApplicantID= (select max(applicantunkid) as applicantunkid  from rcApplicant_master )  " & vbCrLf
    '        mSQL += "	if exists(select 1 from @Skill.nodes('NewDataSet/Skill') pd(x))   " & vbCrLf
    '        mSQL += "begin" & vbCrLf
    '        mSQL += "	insert into rcapplicantskill_tran" & vbCrLf
    '        mSQL += "		(applicantunkid,skillcategoryunkid,skillunkid,remark,Comp_Code, companyunkid)     " & vbCrLf
    '        mSQL += "	select @mApplicantID   " & vbCrLf
    '        mSQL += "   ,x.value('(skillcategoryunkid)[1]' , 'int') as skillcategoryunkid      " & vbCrLf
    '        mSQL += "	,x.value('(skillunkid)[1]','int') as skillunkid" & vbCrLf
    '        mSQL += "	,x.value('(remark)[1]','cDescription') as remark" & vbCrLf
    '        'Sohail (05 Jul 2011) -- Start
    '        'Issue : NULL value entered in comp_code field
    '        'mSQL += "       ,x.value('(Comp_code)[1]','nvarchar(50)') as Comp_Code" & vbCrLf
    '        msql += "       ,x.value('(Comp_Code)[1]','nvarchar(50)') as Comp_Code" & vbCrLf
    '        'Sohail (05 Jul 2011) -- End
    '        mSQL += "	,x.value('(companyunkid)[1]','int') as companyunkid" & vbCrLf
    '        mSQL += "		 from @Skill.nodes('NewDataSet/Skill') Ld(x)   " & vbCrLf
    '        mSQL += "          IF  @@ERROR  <> 0 GOTO Err   " & vbCrLf
    '        mSQL += "End" & vbCrLf
    '        mSQL += "	if exists(select 1 from @Qualification.nodes('NewDataSet/Qualification') pd(x))   " & vbCrLf
    '        mSQL += "begin" & vbCrLf
    '        mSQL += "      select @mApplicantID" & vbCrLf
    '        mSQL += "		insert into rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,Comp_Code,companyunkid)" & vbCrLf
    '        mSQL += "		select @mApplicantID" & vbCrLf
    '        mSQL += "	,x.value('(qualificationgroupunkid)[1]','int') as qualificationgroupunkid" & vbCrLf
    '        mSQL += "	,x.value('(qualificationunkid)[1]','int') as qualificationunkid" & vbCrLf
    '        mSQL += "		, convert(datetime,left(x.value('(transaction_date)[1]','varchar(15)'),10),101) as transaction_date" & vbCrLf
    '        'Sohail (05 Jul 2011) -- Start
    '        'Issue : NULL value entered in reference_no field
    '        'mSQL += "		,x.value('(ReferenceNo)[1]','cCardNo') as reference_no" & vbCrLf
    '        msql += "		,x.value('(Refe_No)[1]','cCardNo') as reference_no" & vbCrLf
    '        'Sohail (05 Jul 2011) -- End
    '        mSQL += "		,convert(datetime,left(x.value('(startdate)[1]','varchar(15)'),10),101) as award_start_date" & vbCrLf
    '        mSQL += "		,convert(datetime,left(x.value('(enddate)[1]','varchar(15)'),10),101) as award_end_date" & vbCrLf
    '        mSQL += "		,x.value('(instituteunkid)[1]','int') as instituteunkid" & vbCrLf
    '        mSQL += "		,x.value('(remark)[1]','cDescription') as remark" & vbCrLf
    '        mSQL += "        ,x.value('(Comp_Code)[1]','cDescription') as Comp_Code" & vbCrLf
    '        mSQL += "	    ,x.value('(companyunkid)[1]','int') as companyunkid" & vbCrLf
    '        mSQL += "		from @Qualification.nodes('NewDataSet/Qualification') Ld(x)" & vbCrLf
    '        mSQL += "     IF  @@ERROR  <> 0 GOTO Err " & vbCrLf
    '        mSQL += "End" & vbCrLf
    '        mSQL += "		if exists(select 1 from @Job.nodes('NewDataSet/Job') Pd(x))" & vbCrLf
    '        mSQL += "begin" & vbCrLf
    '        mSQL += "		insert into rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason,Comp_Code,companyunkid)" & vbCrLf
    '        mSQL += "			select @mApplicantID" & vbCrLf
    '        mSQL += "			,x.value('(employer)[1]','cName') as employername" & vbCrLf
    '        mSQL += "		,x.value('(company)[1]','cName') as companyname" & vbCrLf
    '        mSQL += "		,x.value('(Designation)[1]','CName') As designation" & vbCrLf
    '        mSQL += "		,x.value('(Responsibility)[1]','cDescription') as responsibility" & vbCrLf
    '        mSQL += "		,convert(datetime,left( x.value('(JoinDate)[1]','varchar(15)'),10),101) As joiningdate" & vbCrLf
    '        mSQL += "		,convert(datetime,left(x.value('(terminationdate)[1]','varchar(15)'),10),101)as terminationdate" & vbCrLf
    '        mSQL += "		,x.value('(Phone)[1]','cPhone') as officephone" & vbCrLf
    '        mSQL += "		,x.value('(leavingReason)[1]','cDescription') as leavingreason" & vbCrLf
    '        mSQL += "          ,x.value('(Comp_Code)[1]','cDescription') as Comp_Code" & vbCrLf
    '        mSQL += "	       ,x.value('(companyunkid)[1]','int') as companyunkid" & vbCrLf
    '        mSQL += "			from @Job.nodes('NewDataSet/Job') Ld(x)" & vbCrLf
    '        mSQL += "           IF  @@ERROR  <> 0 GOTO Err " & vbCrLf
    '        mSQL += "End" & vbCrLf

    '        mSQL += "			IF  @@ERROR  <> 0 GOTO Err        " & vbCrLf
    '        mSQL += "End" & vbCrLf
    '        'Sohail (05 Jul 2011) -- Start
    '        'mSQL += "   insert  into hr_images_tran(employeeunkid,imagename,isapplicant,Comp_Code)" & vbCrLf
    '        'mSQL += "        values (@mApplicantID,@ImageName,'True',@Comp_Code)" & vbCrLf
    '        mSQL += "   insert  into hr_images_tran(employeeunkid,imagename,transactionid,referenceid,isapplicant,Comp_Code,companyunkid)" & vbCrLf
    '        mSQL += "        values (@mApplicantID,@ImageName,@mApplicantID," & Aruti.Data.enImg_Email_RefId.Applicant_Module & ",'True',@Comp_Code,@companyunkid)" & vbCrLf
    '        'Sohail (05 Jul 2011) -- End
    '        mSQL += "   GoTo Sucess" & vbCrLf
    '        mSQL += "Err:" & vbCrLf
    '        mSQL += "    rollback tran  @TranName       " & vbCrLf
    '        mSQL += "  RAISERROR ('Error in SP ApplicateMaster. Cannot insert the entry !!!', 16, 1)     " & vbCrLf
    '        mSQL += "Sucess:" & vbCrLf
    '        mSQL += "commit tran @TranName  " & vbCrLf
    '        mSQL += "Return 0" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)

    '    Catch ex As Exception
    '        Throw New Exception("CommonCode-->Pkgdtastr !!!" & ex.Message.ToString & vbCrLf & mSQL)
    '    End Try

    'End Sub

    'Public Sub PkgDtaStrCompany()
    '    Try

    '        'StoreProcedure CreateCalendar
    '        mSQL = " if exists(select 1 from sys.procedures where name='CreateCalendar') drop proc CreateCalendar" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)

    '        mSQL = "create proc CreateCalendar (@tablename as nvarchar(20),@fdt as smalldatetime," & vbCrLf
    '        mSQL &= " @tdt as smalldatetime,@dept as varchar(5) ,@sect as varchar(5)," & vbCrLf
    '        mSQL &= "@job as varchar(5) ," & vbCrLf
    '        mSQL &= "@UserId as nvarchar(5),@Employeeid as nvarchar(5))  " & vbCrLf
    '        mSQL &= "AS   " & vbCrLf
    '        mSQL &= "   BEGIN" & vbCrLf
    '        mSQL &= "declare @dt as smalldatetime ,@temp varchar(100) , @sql varchar(4000), @new  varchar(4000) " & vbCrLf
    '        mSQL &= "if exists(select name from sys.tables where name= @tablename) " & vbCrLf
    '        mSQL &= " select @temp = 'drop table '+ @tablename +''" & vbCrLf
    '        mSQL &= "  print @temp" & vbCrLf
    '        mSQL &= "exec (@temp)" & vbCrLf

    '        mSQL &= "select @dt = @fdt  " & vbCrLf
    '        mSQL &= "print @dt" & vbCrLf

    '        mSQL &= "select @sql = ' create table ' + @tablename + '( CalendarTableID int identity(1,1) ,leaveplannerunkid int, EmployeeUnkid int, DisplayName varchar(100) , '" & vbCrLf

    '        mSQL &= "while @dt<=@tdt  " & vbCrLf
    '        mSQL &= "begin" & vbCrLf
    '        mSQL &= "select @sql = @sql + '[' + convert(varchar(10),@dt,103) + '] varchar(10) default('+char(39)+' '+char(39)+') , '" & vbCrLf
    '        mSQL &= "select @dt = dateadd(d,1,@dt)  " & vbCrLf
    '        mSQL &= "End" & vbCrLf
    '        mSQL &= "select @sql = left(@sql,len(@sql)-2) + ')'" & vbCrLf
    '        mSQL &= "print @sql" & vbCrLf
    '        mSQL &= "exec (@sql)" & vbCrLf

    '        mSQL &= "select @new ='  select l.leaveplannerunkid as leaveplannerunkid, l.EmployeeUnkID as EmployeeUnkID, e.Firstname as  Displayname into ' + @tablename + '1 from lvleaveplanner l'" & vbCrLf
    '        mSQL &= " select @new =  @new +   '    left outer join hremployee_master e on e.EmployeeUnkID = l.EmployeeUnkID ' " & vbCrLf
    '        mSQL &= "  select @new =   @new + ' WHERE 1=1'" & vbCrLf
    '        mSQL &= "if @Employeeid <> '-1' select @new =   @new + ' AND  l.employeeunkid ='+   @Employeeid  + ''" & vbCrLf
    '        mSQL &= " if @UserId <> '-1' select @new =   @new + 'AND l.userunkid =' + @UserId +''" & vbCrLf
    '        mSQL &= " if   @dept<>'-1'    select @new =   @new + ' and departmentunkid = '+ @dept     " & vbCrLf
    '        mSQL &= " if   @sect<>'-1'    select @new =   @new + ' and sectionunkid ='+ @sect        " & vbCrLf
    '        mSQL &= " if   @dept<>'-1'     select @new =   @new + ' and jobunkid ='+  @dept  " & vbCrLf
    '        mSQL &= " select @new =   @new +   ' group by l.employeeunkid,l.leaveplannerunkid,e.Firstname  '" & vbCrLf
    '        mSQL &= "  exec (@new)" & vbCrLf
    '        mSQL &= " select @new ='insert into '+ @tablename  +'(leaveplannerunkid,EmployeeUnkID, DisplayName)  select leaveplannerunkid,EmployeeUnkID, DisplayName  from ' + @tablename +'1  '" & vbCrLf
    '        mSQL &= "  select @new = @new + ' '    " & vbCrLf
    '        mSQL &= " print @new" & vbCrLf
    '        mSQL &= " exec (@new)" & vbCrLf
    '        mSQL &= "select @new = 'drop table '+ @tablename +'1 '" & vbCrLf
    '        mSQL &= "exec (@new)" & vbCrLf
    '        mSQL &= " End" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)


    '        'Sp CreateMonthCalendar

    '        mSQL = " if exists(select 1 from sys.procedures where name='CreateMonthCalendar') drop proc CreateMonthCalendar" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)

    '        mSQL = " create proc CreateMonthCalendar (@tablename as nvarchar(20),@fdt as datetime, @tdt as datetime )      " & vbCrLf
    '        mSQL &= "AS             " & vbCrLf
    '        mSQL &= " BEGIN" & vbCrLf
    '        mSQL &= "declare @dt as smalldatetime ,@temp varchar(100) , @sql varchar(8000), @new  varchar(8000),@msql varchar(8000),@td  as smalldatetime  ,@day int,@totday int,@i as int,@j int,@stwkno as int,@sql1 varchar(8000),@stwend int,@countday int,@startday varchar(10)" & vbCrLf
    '        mSQL &= "if exists(select name from sys.tables where name= @tablename)     " & vbCrLf
    '        mSQL &= " select @temp = 'drop table '+ @tablename +''    " & vbCrLf
    '        mSQL &= "   print @temp    " & vbCrLf
    '        mSQL &= " exec (@temp)    " & vbCrLf
    '        mSQL &= "select @dt = @fdt    " & vbCrLf
    '        mSQL &= "select @countday=datediff(d,@fdt,@tdt)  " & vbCrLf
    '        mSQL &= "select @stwkno = datename(wk,@fdt)  " & vbCrLf
    '        mSQL &= "select @stwend=datename(wk,@tdt)  " & vbCrLf
    '        mSQL &= "print @dt    " & vbCrLf
    '        mSQL &= "select @sql = ' create table ' + @tablename + '( CalendarTableID int identity(1,1),MonthYear varchar(30),startday varchar(10),MonthDays int,'    " & vbCrLf
    '        mSQL &= "select @i=1  " & vbCrLf
    '        mSQL &= "select @j=-1  " & vbCrLf
    '        mSQL &= "print @totday  " & vbCrLf
    '        mSQL &= "while @i <=6  " & vbCrLf
    '        mSQL &= "begin" & vbCrLf
    '        mSQL &= "select @j=-1  " & vbCrLf
    '        mSQL &= "while @j<6  " & vbCrLf
    '        mSQL &= "begin" & vbCrLf
    '        mSQL &= "select @sql = @sql + '[' + left(datename(dw,@j),3) + '_' + convert(varchar(3), + @i,103)+  '] nvarchar(50) default('+char(39)+' '+char(39)+') , '    " & vbCrLf
    '        mSQL &= "select @j=@j +1    " & vbCrLf
    '        mSQL &= "End" & vbCrLf
    '        mSQL &= " select @i = @i + 1  " & vbCrLf
    '        mSQL &= "End" & vbCrLf
    '        mSQL &= "select @sql = left(@sql,len(@sql)-1) + ')'    " & vbCrLf
    '        mSQL &= "print @sql    " & vbCrLf
    '        mSQL &= "exec (@sql)    " & vbCrLf
    '        mSQL &= "select @totday=datediff(mm,@fdt,@tdt)  " & vbCrLf
    '        mSQL &= "select @day=datepart(m,@fdt)   " & vbCrLf
    '        mSQL &= "select @totday=datepart(m,@tdt)  " & vbCrLf
    '        mSQL &= "print @day  " & vbCrLf
    '        mSQL &= "print @totday  " & vbCrLf
    '        mSQL &= "while @day <= @totday   " & vbCrLf
    '        mSQL &= "begin" & vbCrLf
    '        mSQL &= "select @startday = left(datename(dw,ltrim(str(datepart(yyyy,@dt))) + right('0'+ltrim(str(datepart(mm,@dt))) ,2) +  '01'),3)+'_1'  " & vbCrLf
    '        mSQL &= "select @countday= day(dateadd(day,-1,dateadd(month,1,dateadd(day,1 - day(@dt),@dt))))  " & vbCrLf
    '        mSQL &= "select @new ='insert into '+ @tablename  +'(MonthYear,startday,MonthDays) values ('+char(39) + (datename(m,@dt))+' '+str(year(@dt),4)+char(39)+','+char(39)+@startday+char(39)+','+ str(@countday)+')'  " & vbCrLf
    '        mSQL &= "select @dt= DATEADD(m, 1, @dt)  " & vbCrLf
    '        mSQL &= "select @day=@day + 1   " & vbCrLf
    '        mSQL &= "print @day  " & vbCrLf
    '        mSQL &= "exec (@new)   " & vbCrLf
    '        mSQL &= "End" & vbCrLf
    '        mSQL &= " End" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)


    '        'weekdayoccurance function

    '        mSQL = " if exists(select 1 from sys.objects where name='weekdayoccurance') drop function weekdayoccurance" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)

    '        mSQL = " create  function weekdayoccurance(  @date as smalldatetime  )" & vbCrLf
    '        mSQL &= "returns Int" & vbCrLf
    '        mSQL &= "as" & vbCrLf
    '        mSQL &= " begin" & vbCrLf
    '        mSQL &= "declare @startdate as smalldatetime, @dayoccurance as int, @i int" & vbCrLf
    '        mSQL &= "select @startdate  = (ltrim(str(datepart(yyyy,@date )))+ right('0'+ltrim(str(datepart(mm,@date ))) ,2) + '01') " & vbCrLf
    '        mSQL &= "select @i = 0" & vbCrLf
    '        mSQL &= "select @dayoccurance = 0"
    '        mSQL &= "while @i <= datediff(d,@startdate,@date)" & vbCrLf
    '        mSQL &= "begin" & vbCrLf
    '        mSQL &= "if datename(dw,dateadd(d,@i,@startdate)) = datename(dw,@date)" & vbCrLf
    '        mSQL &= "	select @dayoccurance = @dayoccurance + 1" & vbCrLf
    '        mSQL &= "select @i = @i + 1" & vbCrLf
    '        mSQL &= "End" & vbCrLf
    '        mSQL &= "if datepart(dw,@date) < datepart(dw,@startdate)" & vbCrLf
    '        mSQL &= "	select @dayoccurance = @dayoccurance + 1" & vbCrLf
    '        mSQL &= "else" & vbCrLf
    '        mSQL &= "	select @dayoccurance = @dayoccurance + 0" & vbCrLf
    '        mSQL &= "return @dayoccurance" & vbCrLf
    '        mSQL &= "    End" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)


    '        'lvleaveformdetail function
    '        mSQL = " if exists(select 1 from sys.objects where name='lvleaveformdetail') drop function lvleaveformdetail" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)


    '        mSQL = " create  Function lvleaveformdetail" & vbCrLf
    '        mSQL &= "(       " & vbCrLf
    '        mSQL &= " @employeeunkid int" & vbCrLf
    '        mSQL &= ")              " & vbCrLf
    '        mSQL &= "RETURNS @tbl TABLE       " & vbCrLf
    '        mSQL &= "(                 " & vbCrLf
    '        mSQL &= "   employeeunkid(Int)" & vbCrLf
    '        mSQL &= ", leavedate		smalldatetime" & vbCrLf
    '        mSQL &= ", weekday		varchar(10)" & vbCrLf
    '        mSQL &= ", monthyear		varchar(15)" & vbCrLf
    '        mSQL &= ", lvid			int" & vbCrLf
    '        mSQL &= ", lvcolorid		int" & vbCrLf
    '        mSQL &= ",statusunkid int" & vbCrLf
    '        mSQL &= ")" & vbCrLf
    '        mSQL &= "AS           " & vbCrLf
    '        mSQL &= " BEGIN" & vbCrLf
    '        mSQL &= "declare @startdate as smalldatetime, @returndate as smalldatetime, @totdays  as int, @lvtypeid int, @lvcolor int,@statusunkid int" & vbCrLf
    '        mSQL &= "declare @mday as int" & vbCrLf
    '        mSQL &= "declare cur1 cursor" & vbCrLf
    '        mSQL &= "FOR " & vbCrLf
    '        mSQL &= "	select a.startdate, a.returndate, datediff(d,a.startdate,a.returndate) as totdays, a.leavetypeunkid , b.color as lvcolor,a.statusunkid" & vbCrLf
    '        mSQL &= "from lvleaveform  a " & vbCrLf
    '        mSQL &= "left outer join lvleavetype_master b on a.leavetypeunkid = b.leavetypeunkid " & vbCrLf
    '        mSQL &= "where a.employeeunkid = @employeeunkid  and statusunkid=@statusunkid" & vbCrLf
    '        mSQL &= "order by a.employeeunkid, a.applydate" & vbCrLf
    '        mSQL &= " OPEN cur1" & vbCrLf
    '        mSQL &= "FETCH NEXT FROM cur1 INTO @startdate, @returndate, @totdays, @lvtypeid , @lvcolor,@statusunkid" & vbCrLf
    '        mSQL &= "WHILE @@FETCH_STATUS = 0" & vbCrLf
    '        mSQL &= "BEGIN" & vbCrLf
    '        mSQL &= "select @mday = 0" & vbCrLf
    '        mSQL &= "while @mday <= @totdays" & vbCrLf
    '        mSQL &= "BEGIN" & vbCrLf
    '        mSQL &= "insert into @tbl (employeeunkid,leavedate, weekday, monthyear, lvid, lvcolorid,statusunkid) " & vbCrLf
    '        mSQL &= "values (  @employeeunkid , dateadd(d,@mday,@startdate) , left(datename(dw,dateadd(d,@mday,@startdate)),3)+'_'+str(dbo.weekdayoccurance(dateadd(d,@mday,@startdate)),1) , datename(mm,dateadd(d,@mday,@startdate))+' '+str(year(dateadd(d,@mday,@startdate)),4) , @lvtypeid , @lvcolor , @statusunkid)" & vbCrLf
    '        mSQL &= "select @mday = @mday + 1" & vbCrLf
    '        mSQL &= "End" & vbCrLf
    '        mSQL &= "FETCH NEXT FROM cur1 INTO @startdate, @returndate, @totdays, @lvtypeid , @lvcolor,@statusunkid" & vbCrLf
    '        mSQL &= "   End" & vbCrLf
    '        mSQL &= "   Return" & vbCrLf
    '        mSQL &= "  End" & vbCrLf
    '        objDataOperation.ExecNonQuery(mSQL)

    '        'select object_name(object_id), * from sys.columns 

    '        'select * from sys.procedures 
    '    Catch ex As Exception
    '        Throw New Exception("CommonCode-->Pkgdtastr !!!" & ex.Message.ToString & vbCrLf & mSQL)
    '    End Try
    'End Sub

    Public Sub DisplayMessage(ByVal S_dispmsg As String, ByVal f_curform As Page)

        S_dispmsg = S_dispmsg.Replace("'", "")
        S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
        ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "alert('" & S_dispmsg & "');", True)

    End Sub
    Public Sub DisplayError(ByVal S_dispmsg As String, ByVal f_curform As Page)

        'Sohail (21 May 2012) -- Start
        'TRA - ENHANCEMENT

        '*** Error Log ****
        Dim dt As Date = DateAndTime.Now.Date
        'Sohail (20 Oct 2015) -- Start
        'Issue - Cannot access the file because it is being used by another process.
        'Dim wrtr As New IO.StreamWriter(Current.Server.MapPath("ErrorLog\" & DateAndTime.Now.Date.ToString("yyMMdd") & ".log"), True)
        'wrtr.WriteLine("")
        'wrtr.WriteLine(Current.Request.Url.ToString & vbTab & " Company : " & Current.Session("CompName") & ";" & Current.Session("Vacancy") & vbTab & "Ref. No: " & Current.Session("referenceno") & vbTab & " [SessionID : " & HttpContext.Current.Session.SessionID & "]" & vbTab & " Time : " & DateAndTime.Now.ToString)
        'wrtr.WriteLine(S_dispmsg)
        'wrtr.Close()
        Dim strMsg As String = vbCrLf &
                                Current.Request.Url.ToString & vbTab & " Company : " & Current.Session("CompName") & ";" & Current.Session("Vacancy") & vbTab & "Ref. No: " & Current.Session("referenceno") & vbTab & " [SessionID : " & HttpContext.Current.Session.SessionID & "]" & vbTab & " Time : " & DateAndTime.Now.ToString &
                                vbCrLf &
                                S_dispmsg

        IO.File.AppendAllText(Current.Server.MapPath("~\ErrorLog\" & DateAndTime.Now.Date.ToString("yyMMdd") & ".log"), strMsg)
        'Sohail (20 Oct 2015) -- End
        'Sohail (21 May 2012) -- End

        S_dispmsg = S_dispmsg.Replace("'", "")
        S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
        ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "alert('" & S_dispmsg & "');", True)

    End Sub

    Public Sub DisplayMessage(ByVal S_dispmsg As String, ByVal f_curform As Page, ByVal strLoaction As String)

        S_dispmsg = S_dispmsg.Replace("'", "")
        S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
        ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "alert('" & S_dispmsg & "');window.location.href='" & strLoaction & "';", True)

    End Sub
End Class
