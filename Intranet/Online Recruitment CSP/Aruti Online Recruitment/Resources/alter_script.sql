/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'admin_email')
BEGIN
	ALTER TABLE cfcompany_info ADD admin_email NVARCHAR(510) NULL
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'Date_Format')
BEGIN
	ALTER TABLE cfcompany_info ADD Date_Format [nvarchar](100) NULL DEFAULT 'dd-MMM-yyyy'
	EXECUTE('UPDATE cfcompany_info SET Date_Format = ''dd-MMM-yyyy'' WHERE Date_Format IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'DateSeparator')
BEGIN
	ALTER TABLE cfcompany_info ADD DateSeparator [nvarchar](100) NULL DEFAULT '-'
	EXECUTE('UPDATE cfcompany_info SET DateSeparator = ''-'' WHERE DateSeparator IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'MiddleNameMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD MiddleNameMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET MiddleNameMandatory = 0 WHERE MiddleNameMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'GenderMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD GenderMandatory BIT NULL DEFAULT 1
	EXECUTE('UPDATE cfcompany_info SET GenderMandatory = 1 WHERE GenderMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'BirthDateMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD BirthDateMandatory BIT NULL DEFAULT 1
	EXECUTE('UPDATE cfcompany_info SET BirthDateMandatory = 1 WHERE BirthDateMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'Address1Mandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD Address1Mandatory BIT NULL DEFAULT 1
	EXECUTE('UPDATE cfcompany_info SET Address1Mandatory = 1 WHERE Address1Mandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'Address2Mandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD Address2Mandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET Address2Mandatory = 0 WHERE Address2Mandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'CountryMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD CountryMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET CountryMandatory = 0 WHERE CountryMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'StateMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD StateMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET StateMandatory = 0 WHERE StateMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'CityMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD CityMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET CityMandatory = 0 WHERE CityMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'PostCodeMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD PostCodeMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET PostCodeMandatory = 0 WHERE PostCodeMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'MaritalStatusMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD MaritalStatusMandatory BIT NULL DEFAULT 1
	EXECUTE('UPDATE cfcompany_info SET MaritalStatusMandatory = 1 WHERE MaritalStatusMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'Language1Mandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD Language1Mandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET Language1Mandatory = 0 WHERE Language1Mandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'NationalityMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD NationalityMandatory BIT NULL DEFAULT 1
	EXECUTE('UPDATE cfcompany_info SET NationalityMandatory = 1 WHERE NationalityMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'OneSkillMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD OneSkillMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET OneSkillMandatory = 0 WHERE OneSkillMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'OneQualificationMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD OneQualificationMandatory BIT NULL DEFAULT 1
	EXECUTE('UPDATE cfcompany_info SET OneQualificationMandatory = 1 WHERE OneQualificationMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'OneJobExperienceMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD OneJobExperienceMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET OneJobExperienceMandatory = 0 WHERE OneJobExperienceMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'OneReferenceMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD OneReferenceMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET OneReferenceMandatory = 0 WHERE OneReferenceMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 06/07/2019 09:00 AM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'OneCurriculamVitaeMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD OneCurriculamVitaeMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET OneCurriculamVitaeMandatory = 0 WHERE OneCurriculamVitaeMandatory IS NULL')
	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to check whether Curriculam Vitae is Mandatory or not.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'OneCurriculamVitaeMandatory';
END
GO

/****** SP: 64.1    Script Date: 06/07/2019 09:00 AM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'OneCoverLetterMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD OneCoverLetterMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET OneCoverLetterMandatory = 0 WHERE OneCoverLetterMandatory IS NULL')
	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to check whether Cover Letter is Mandatory or not.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'OneCoverLetterMandatory';
END
GO

/****** SP: 70.1    Script Date: 04/12/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' AND COLUMN_NAME = 'email_type')
BEGIN 
	ALTER TABLE cfcompany_info ADD email_type INT NULL DEFAULT 0

	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to store Email Type to send an email.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'email_type';

	EXECUTE('UPDATE cfcompany_info SET email_type = 0 WHERE email_type IS NULL')
END
GO

/****** SP: 70.1    Script Date: 04/12/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' AND COLUMN_NAME = 'ews_url')
BEGIN 
	ALTER TABLE cfcompany_info ADD ews_url nvarchar(255) NULL DEFAULT ''

	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to store Web Service URL for Exchange Server Web Service.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'ews_url';

	EXECUTE('UPDATE cfcompany_info SET ews_url = '''' WHERE ews_url IS NULL')
END
GO

/****** SP: 70.1    Script Date: 04/12/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' AND COLUMN_NAME = 'ews_domain')
BEGIN 
	ALTER TABLE cfcompany_info ADD ews_domain nvarchar(255) NULL DEFAULT ''

	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to store Web Service Domain for Exchange Server Web Service.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'ews_domain';

	EXECUTE('UPDATE cfcompany_info SET ews_domain = '''' WHERE ews_domain IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'UserId')
BEGIN
	ALTER TABLE rcapplicant_master ADD UserId UNIQUEIDENTIFIER NULL
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantskill_tran' AND COLUMNS.COLUMN_NAME = 'isvoid')
BEGIN
	ALTER TABLE rcapplicantskill_tran ADD isvoid BIT NULL
	EXECUTE('UPDATE rcapplicantskill_tran SET isvoid = 0 WHERE isvoid IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantskill_tran' AND COLUMNS.COLUMN_NAME = 'voiddatetime')
BEGIN
	ALTER TABLE rcapplicantskill_tran ADD voiddatetime DATETIME NULL	
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantskill_tran' AND COLUMNS.COLUMN_NAME = 'voidreason')
BEGIN
	ALTER TABLE rcapplicantskill_tran ADD voidreason NVARCHAR(510) NULL
	EXECUTE('UPDATE rcapplicantskill_tran SET voidreason = '''' WHERE voidreason IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantqualification_tran' AND COLUMNS.COLUMN_NAME = 'isvoid')
BEGIN
	ALTER TABLE rcapplicantqualification_tran ADD isvoid BIT NULL
	EXECUTE('UPDATE rcapplicantqualification_tran SET isvoid = 0 WHERE isvoid IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantqualification_tran' AND COLUMNS.COLUMN_NAME = 'voiddatetime')
BEGIN
	ALTER TABLE rcapplicantqualification_tran ADD voiddatetime DATETIME NULL	
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantqualification_tran' AND COLUMNS.COLUMN_NAME = 'voidreason')
BEGIN
	ALTER TABLE rcapplicantqualification_tran ADD voidreason NVARCHAR(510) NULL
	EXECUTE('UPDATE rcapplicantqualification_tran SET voidreason = '''' WHERE voidreason IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcjobhistory' AND COLUMNS.COLUMN_NAME = 'isvoid')
BEGIN
	ALTER TABLE rcjobhistory ADD isvoid BIT NULL
	EXECUTE('UPDATE rcjobhistory SET isvoid = 0 WHERE isvoid IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcjobhistory' AND COLUMNS.COLUMN_NAME = 'voiddatetime')
BEGIN
	ALTER TABLE rcjobhistory ADD voiddatetime DATETIME NULL	
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcjobhistory' AND COLUMNS.COLUMN_NAME = 'voidreason')
BEGIN
	ALTER TABLE rcjobhistory ADD voidreason NVARCHAR(510) NULL
	EXECUTE('UPDATE rcjobhistory SET voidreason = '''' WHERE voidreason IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_reference_tran' AND COLUMNS.COLUMN_NAME = 'isvoid')
BEGIN
	ALTER TABLE rcapp_reference_tran ADD isvoid BIT NULL
	EXECUTE('UPDATE rcapp_reference_tran SET isvoid = 0 WHERE isvoid IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_reference_tran' AND COLUMNS.COLUMN_NAME = 'voiddatetime')
BEGIN
	ALTER TABLE rcapp_reference_tran ADD voiddatetime DATETIME NULL	
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_reference_tran' AND COLUMNS.COLUMN_NAME = 'voidreason')
BEGIN
	ALTER TABLE rcapp_reference_tran ADD voidreason NVARCHAR(510) NULL
	EXECUTE('UPDATE rcapp_reference_tran SET voidreason = '''' WHERE voidreason IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'isvoid')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD isvoid BIT NULL
	EXECUTE('UPDATE rcapp_vacancy_mapping SET isvoid = 0 WHERE isvoid IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'voiddatetime')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD voiddatetime DATETIME NULL	
END
GO

/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'voidreason')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD voidreason NVARCHAR(510) NULL
	EXECUTE('UPDATE rcapp_vacancy_mapping SET voidreason = '''' WHERE voidreason IS NULL')
END
GO
/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'loginemployeeunkid')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD loginemployeeunkid INT NULL DEFAULT 0
	EXECUTE('UPDATE rcapp_vacancy_mapping SET loginemployeeunkid = 0 WHERE loginemployeeunkid IS NULL')
END
GO
/****** Object:  Table [dbo].[superadmin_emailsetup]    Script Date: 07/02/2017 04:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[superadmin_emailsetup]') AND type in (N'U'))
BEGIN
CREATE TABLE [superadmin_emailsetup](
	[emailsetupunkid] [int] IDENTITY(1,1) NOT NULL,
	[sendername] [nvarchar](510) NULL,
	[senderaddress] [nvarchar](510) NULL,
	[reference] [nvarchar](510) NULL,
	[mailserverip] [nvarchar](510) NULL,
	[mailserverport] [int] NULL,
	[username] [nvarchar](510) NULL,
	[password] [nvarchar](510) NULL,
	[isloginssl] [bit] NULL,
	[mail_body] [nvarchar](2000) NULL,
	[isactive] [bit] NULL,
 CONSTRAINT [PK_superadmin_emailsetup] PRIMARY KEY CLUSTERED 
(
	[emailsetupunkid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT 1 FROM superadmin_emailsetup WHERE superadmin_emailsetup.isactive = 1)
BEGIN
	INSERT INTO superadmin_emailsetup (sendername, senderaddress, reference, mailserverip, mailserverport, username, [password], isloginssl, mail_body, isactive)
	VALUES ('', '', '', '', 0, '', '', 0, '', 1)
END
GO
/****** SP: 70.1    Script Date: 04/12/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'superadmin_emailsetup' AND COLUMN_NAME = 'email_type')
BEGIN 
	ALTER TABLE superadmin_emailsetup ADD email_type INT NULL DEFAULT 0

	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to store Email Type to send an email.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'superadmin_emailsetup' ,
	@level2type = N'Column',
	@level2name = 'email_type';

	EXECUTE('UPDATE superadmin_emailsetup SET email_type = 0 WHERE email_type IS NULL')
END
GO

/****** SP: 70.1    Script Date: 04/12/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'superadmin_emailsetup' AND COLUMN_NAME = 'ews_url')
BEGIN 
	ALTER TABLE superadmin_emailsetup ADD ews_url nvarchar(255) NULL DEFAULT ''

	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to store Web Service URL for Exchange Server Web Service.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'superadmin_emailsetup' ,
	@level2type = N'Column',
	@level2name = 'ews_url';

	EXECUTE('UPDATE superadmin_emailsetup SET ews_url = '''' WHERE ews_url IS NULL')
END
GO

/****** SP: 70.1    Script Date: 04/12/2016 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'superadmin_emailsetup' AND COLUMN_NAME = 'ews_domain')
BEGIN 
	ALTER TABLE superadmin_emailsetup ADD ews_domain nvarchar(255) NULL DEFAULT ''

	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to store Web Service Domain for Exchange Server Web Service.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'superadmin_emailsetup' ,
	@level2type = N'Column',
	@level2name = 'ews_domain';

	EXECUTE('UPDATE superadmin_emailsetup SET ews_domain = '''' WHERE ews_domain IS NULL')
END
GO

/****** Object:  StoredProcedure [dbo].[procGetSuperadminEmailsetting]    Script Date: 07/02/2017 05:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procGetSuperadminEmailsetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procGetSuperadminEmailsetting] AS' 
END
GO
ALTER PROCEDURE [dbo].[procGetSuperadminEmailsetting] 
AS	
SELECT  superadmin_emailsetup.emailsetupunkid 
      , superadmin_emailsetup.sendername
      , superadmin_emailsetup.senderaddress
      , superadmin_emailsetup.reference
      , superadmin_emailsetup.mailserverip
      , superadmin_emailsetup.mailserverport
      , superadmin_emailsetup.username
	  , superadmin_emailsetup.[password]
	  , superadmin_emailsetup.isloginssl
	  , superadmin_emailsetup.mail_body
	  , superadmin_emailsetup.isactive
	  , ISNULL(superadmin_emailsetup.email_type, 0) AS email_type
	  , ISNULL(superadmin_emailsetup.ews_url, '') AS ews_url
	  , ISNULL(superadmin_emailsetup.ews_domain, '') AS ews_domain
FROM    superadmin_emailsetup
WHERE   superadmin_emailsetup.isactive = 1	
GO
/****** Object:  StoredProcedure [dbo].[procUpdateSuperadminEmailsetting]    Script Date: 07/02/2017 05:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procUpdateSuperadminEmailsetting]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procUpdateSuperadminEmailsetting] AS' 
END
GO
ALTER PROCEDURE [dbo].[procUpdateSuperadminEmailsetting]
	  @emailsetupunkid INT 
	, @sendername NVARCHAR(510)  
	, @senderaddress NVARCHAR(510) 
	, @reference NVARCHAR(510) 
	, @mailserverip NVARCHAR(510) 
	, @mailserverport INT 
	, @username NVARCHAR(510) 
	, @password NVARCHAR(510) 
	, @isloginssl BIT 
	, @mail_body NVARCHAR(MAX)
	, @email_type INT
	, @ews_url NVARCHAR(MAX)
	, @ews_domain NVARCHAR(MAX) 	
AS 
	UPDATE superadmin_emailsetup SET
		  sendername = @sendername
		, senderaddress = @senderaddress
		, reference = @reference
		, mailserverip = @mailserverip
		, mailserverport = @mailserverport
		, username = @username		
		, [password] = @password 
		, isloginssl = @isloginssl
		, mail_body = @mail_body
		, email_type = @email_type
		, ews_url = @ews_url
		, ews_domain = @ews_domain		
	WHERE emailsetupunkid = @emailsetupunkid			
GO
/****** SP: 64.1    Script Date: 08/02/2017 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'RegionMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD RegionMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET RegionMandatory = 0 WHERE RegionMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/02/2017 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'HighestQualificationMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD HighestQualificationMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET HighestQualificationMandatory = 0 WHERE HighestQualificationMandatory IS NULL')
END
GO

/****** SP: 64.1    Script Date: 08/02/2017 05:12 PM ******/
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'EmployerMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD EmployerMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET EmployerMandatory = 0 WHERE EmployerMandatory IS NULL')
END
GO
/****** SP: 64.1    Script Date: 07/02/2017 05:19 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantskill_tran' AND COLUMNS.COLUMN_NAME = 'skillexpertiseunkid')
BEGIN
	ALTER TABLE rcapplicantskill_tran ADD skillexpertiseunkid INT NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicantskill_tran SET skillexpertiseunkid = 0 WHERE skillexpertiseunkid IS NULL')
END
GO
/****** SP: 64.1    Script Date: 07/02/2017 05:19 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'appqualisortbyid')
BEGIN
	ALTER TABLE cfcompany_info ADD appqualisortbyid INT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET appqualisortbyid = 0 WHERE appqualisortbyid IS NULL')
END
GO
/****** SP: 64.1    Script Date: 07/02/2017 05:12 PM ******/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantqualification_tran' AND COLUMNS.COLUMN_NAME = 'ishighestqualification')
BEGIN
	ALTER TABLE rcapplicantqualification_tran ADD ishighestqualification BIT NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicantqualification_tran SET ishighestqualification = 0 WHERE ishighestqualification IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'mother_tongue')
BEGIN
	ALTER TABLE rcapplicant_master ADD mother_tongue NVARCHAR(512) NULL DEFAULT ''
	EXECUTE('UPDATE rcapplicant_master SET mother_tongue = '''' WHERE mother_tongue IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'isimpaired')
BEGIN
	ALTER TABLE rcapplicant_master ADD isimpaired BIT NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master SET isimpaired = 0 WHERE isimpaired IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'impairment')
BEGIN
	ALTER TABLE rcapplicant_master ADD impairment NVARCHAR(MAX) NULL DEFAULT ''
	EXECUTE('UPDATE rcapplicant_master SET impairment = '''' WHERE impairment IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'current_salary')
BEGIN
	ALTER TABLE rcapplicant_master ADD current_salary DECIMAL(36, 6) NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master SET current_salary = 0 WHERE current_salary IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'expected_salary')
BEGIN
	ALTER TABLE rcapplicant_master ADD expected_salary DECIMAL(36, 6) NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master SET expected_salary = 0 WHERE expected_salary IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'expected_benefits')
BEGIN
	ALTER TABLE rcapplicant_master ADD expected_benefits NVARCHAR(MAX) NULL DEFAULT ''
	EXECUTE('UPDATE rcapplicant_master SET expected_benefits = '''' WHERE expected_benefits IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'willing_to_relocate')
BEGIN
	ALTER TABLE rcapplicant_master ADD willing_to_relocate BIT NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master SET willing_to_relocate = 0 WHERE willing_to_relocate IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'willing_to_travel')
BEGIN
	ALTER TABLE rcapplicant_master ADD willing_to_travel BIT NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master SET willing_to_travel = 0 WHERE willing_to_travel IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'notice_period_days')
BEGIN
	ALTER TABLE rcapplicant_master ADD notice_period_days INT NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master SET notice_period_days = 0 WHERE notice_period_days IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'earliest_possible_startdate')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD earliest_possible_startdate DATETIME NULL DEFAULT '19000101'
	EXECUTE('UPDATE rcapp_vacancy_mapping SET earliest_possible_startdate = 0 WHERE earliest_possible_startdate IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'comments')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD comments NVARCHAR(MAX) NULL DEFAULT ''
	EXECUTE('UPDATE rcapp_vacancy_mapping SET comments = '''' WHERE comments IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_vacancy_mapping' AND COLUMNS.COLUMN_NAME = 'vacancy_found_out_from')
BEGIN
	ALTER TABLE rcapp_vacancy_mapping ADD vacancy_found_out_from NVARCHAR(MAX) NULL DEFAULT ''
	EXECUTE('UPDATE rcapp_vacancy_mapping SET vacancy_found_out_from = '''' WHERE vacancy_found_out_from IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'MotherTongueMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD MotherTongueMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET MotherTongueMandatory = 0 WHERE MotherTongueMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'CurrentSalaryMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD CurrentSalaryMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET CurrentSalaryMandatory = 0 WHERE CurrentSalaryMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'ExpectedSalaryMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD ExpectedSalaryMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET ExpectedSalaryMandatory = 0 WHERE ExpectedSalaryMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'ExpectedBenefitsMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD ExpectedBenefitsMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET ExpectedBenefitsMandatory = 0 WHERE ExpectedBenefitsMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'NoticePeriodMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD NoticePeriodMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET NoticePeriodMandatory = 0 WHERE NoticePeriodMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'EarliestPossibleStartDateMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD EarliestPossibleStartDateMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET EarliestPossibleStartDateMandatory = 0 WHERE EarliestPossibleStartDateMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'CommentsMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD CommentsMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET CommentsMandatory = 0 WHERE CommentsMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'VacancyFoundOutFromMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD VacancyFoundOutFromMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET VacancyFoundOutFromMandatory = 0 WHERE VacancyFoundOutFromMandatory IS NULL')
END
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'isemailpwd_encrypted')
BEGIN
	ALTER TABLE cfcompany_info ADD isemailpwd_encrypted BIT NULL DEFAULT 0
	EXEC('UPDATE cfcompany_info SET isemailpwd_encrypted = 0 WHERE isemailpwd_encrypted IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'CompanyNameJobHistoryMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD CompanyNameJobHistoryMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET CompanyNameJobHistoryMandatory = 1 WHERE CompanyNameJobHistoryMandatory IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'PositionHeldMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD PositionHeldMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET PositionHeldMandatory = 0 WHERE PositionHeldMandatory IS NULL')
END
GO
/****** SP: 70.1    Script Date: 30/12/2017 11:40 AM ******/
IF NOT EXISTS ( SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' and COLUMN_NAME = 'isvacancyAlert' ) 
BEGIN
	alter table cfcompany_info add isvacancyAlert bit DEFAULT 0
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Determines company wise vacancy alert or not.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfcompany_info', @level2type=N'COLUMN',@level2name=N'isvacancyAlert'
	EXECUTE('Update cfcompany_info set isvacancyAlert = 0 WHERE isvacancyAlert IS NULL')
END
GO
IF NOT EXISTS ( SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' and COLUMN_NAME = 'maxvacancyalert' ) 
BEGIN
	alter table cfcompany_info add maxvacancyalert int DEFAULT 0
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Determines company wise maximum number of vacancy alert.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfcompany_info', @level2type=N'COLUMN',@level2name=N'maxvacancyalert'
	EXECUTE('Update cfcompany_info set maxvacancyalert = 0 WHERE maxvacancyalert IS NULL')
END
GO
IF NOT EXISTS ( SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' and COLUMN_NAME = 'issendjobconfirmationemail' ) 
BEGIN
	alter table cfcompany_info add issendjobconfirmationemail bit DEFAULT 0
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Determines whether company wise send job confirmation email or not .' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfcompany_info', @level2type=N'COLUMN',@level2name=N'issendjobconfirmationemail'
	EXECUTE('Update cfcompany_info set issendjobconfirmationemail = 0 WHERE issendjobconfirmationemail IS NULL')
END
GO
/****** Object:  StoredProcedure [dbo].[procGetDashboard]    Script Date: 05/01/2018 03:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procGetDashboard]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procGetDashboard] AS' 
END
GO
ALTER PROCEDURE [dbo].[procGetDashboard]
	  @company_code NVARCHAR(510) 
    , @companyunkid INT
AS 
	SELECT 0 AS Id,
		   'Total Users Registered' AS Caption,
		   count(DISTINCT ActivationLink.email) AS Cnt
	FROM aspnet_Membership
		JOIN ActivationLink
			ON ActivationLink.UserId = aspnet_Membership.UserId
	WHERE Comp_Code = @company_code AND companyunkid = @companyunkid
	UNION ALL
	SELECT 1 AS Id,
		   'Users Registered (Today)' AS Caption,
		   count(DISTINCT ActivationLink.email) AS Cnt
	FROM aspnet_Membership
		JOIN ActivationLink
			ON ActivationLink.UserId = aspnet_Membership.UserId
	WHERE Comp_Code = @company_code AND companyunkid = @companyunkid
		  AND CONVERT(CHAR(8), DATEADD(MINUTE,DATEDIFF(MINUTE,GETUTCDATE(),GETDATE()),aspnet_Membership.CreateDate), 112) = CONVERT(CHAR(8), GETDATE(), 112)
	UNION ALL
	SELECT 2 AS Id,
		   'Total Users Activated' AS Caption,
		   count(DISTINCT ActivationLink.email) AS Cnt
	FROM aspnet_Membership
		JOIN ActivationLink
			ON ActivationLink.UserId = aspnet_Membership.UserId
	WHERE Comp_Code = @company_code AND companyunkid = @companyunkid
		  AND IsApproved = 1
	UNION ALL
	SELECT 3 AS Id,
		   'Users Activated (Today)' AS Caption,
		   count(DISTINCT ActivationLink.email) AS Cnt
	FROM aspnet_Membership
		JOIN ActivationLink
			ON ActivationLink.UserId = aspnet_Membership.UserId
	WHERE Comp_Code = @company_code AND companyunkid = @companyunkid
		  AND IsApproved = 1
		  AND CONVERT(CHAR(8), DATEADD(MINUTE,DATEDIFF(MINUTE,GETUTCDATE(),GETDATE()),aspnet_Membership.CreateDate), 112) = CONVERT(CHAR(8), GETDATE(), 112)
	UNION ALL
	SELECT 4 AS Id,
		   'Total Users Registered but not Activated' AS Caption,
		   count(DISTINCT ActivationLink.email) AS Cnt
	FROM aspnet_Membership
		JOIN ActivationLink
			ON ActivationLink.UserId = aspnet_Membership.UserId
	WHERE Comp_Code = @company_code AND companyunkid = @companyunkid
		  AND IsApproved = 0
		   AND isemailsent = 1
	UNION ALL
	SELECT 5 AS Id,
		   'Total Pending Send Activation Link' AS Caption,
		   count(DISTINCT ActivationLink.email) AS Cnt
	FROM aspnet_Membership
		JOIN ActivationLink
			ON ActivationLink.UserId = aspnet_Membership.UserId
	WHERE Comp_Code = @company_code AND companyunkid = @companyunkid
		  AND IsApproved = 0
		  AND isemailsent = 0
GO
/****** Object:  StoredProcedure [dbo].[procGetListNULLAdminEmail]    Script Date: 08/11/2016 03:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procGetListNULLAdminEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procGetListNULLAdminEmail] AS' 
END
GO
ALTER PROCEDURE [dbo].[procGetListNULLAdminEmail]
AS	
	SELECT    cfcompany_info.companyunkid
			, cfcompany_info.[password]
			FROM cfcompany_info
	WHERE	admin_email IS NULL			
GO
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'rcvacancy_master')
BEGIN
	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'rcvacancy_master' AND COLUMN_NAME = 'responsibilities_duties' AND CHARACTER_MAXIMUM_LENGTH = 1000)
	BEGIN
		ALTER TABLE rcvacancy_master ALTER COLUMN responsibilities_duties NVARCHAR(MAX)
	END

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'rcvacancy_master' AND COLUMN_NAME = 'remark' AND CHARACTER_MAXIMUM_LENGTH = 1000)
	BEGIN
		ALTER TABLE rcvacancy_master ALTER COLUMN remark NVARCHAR(MAX)
	END
END
GO
IF NOT EXISTS ( SELECT  COLUMN_NAME From INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'cfcompany_info' and COLUMN_NAME = 'isattachapplicantcv' ) 
BEGIN
	alter table cfcompany_info add isattachapplicantcv bit DEFAULT 0
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Determines whether applicant cv attach with job confirmation email or not.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfcompany_info', @level2type=N'COLUMN',@level2name=N'isattachapplicantcv'
	EXECUTE('Update cfcompany_info set isattachapplicantcv = 0 WHERE isattachapplicantcv IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'current_salary_currency_id')
BEGIN
	ALTER TABLE rcapplicant_master  ADD current_salary_currency_id int NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master  SET current_salary_currency_id = 0 WHERE current_salary_currency_id IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicant_master' AND COLUMNS.COLUMN_NAME = 'expected_salary_currency_id')
BEGIN
	ALTER TABLE rcapplicant_master  ADD expected_salary_currency_id int NULL DEFAULT 0
	EXECUTE('UPDATE rcapplicant_master  SET expected_salary_currency_id = 0 WHERE expected_salary_currency_id IS NULL')
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'QualificationStartDateMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD QualificationStartDateMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET QualificationStartDateMandatory = 0 WHERE QualificationStartDateMandatory IS NULL')
	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to check whether Qualification Start Date is Mandatory or not.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'QualificationStartDateMandatory';
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'QualificationAwardDateMandatory')
BEGIN
	ALTER TABLE cfcompany_info ADD QualificationAwardDateMandatory BIT NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET QualificationAwardDateMandatory = 0 WHERE QualificationAwardDateMandatory IS NULL')
	EXEC sp_addextendedproperty
	@name = N'MS_Description' ,
	@value = N'Used to check whether Qualification Award Date is Mandatory or not.' ,
	@level0type = N'Schema', 
	@level0name = dbo ,
	@level1type = N'Table',
	@level1name = 'cfcompany_info' ,
	@level2type = N'Column',
	@level2name = 'QualificationAwardDateMandatory';
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcompany_info' AND COLUMNS.COLUMN_NAME = 'virtual_directory')
BEGIN
	ALTER TABLE cfcompany_info ADD virtual_directory NVARCHAR(255) NULL DEFAULT 0
	EXECUTE('UPDATE cfcompany_info SET virtual_directory = ''ArutiHRM'' WHERE virtual_directory IS NULL')
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to get virtual directory name.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfcompany_info', @level2type=N'COLUMN',@level2name=N'virtual_directory';
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procGetCompanyByVirtualDirectory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procGetCompanyByVirtualDirectory] AS' 
END
GO
ALTER PROCEDURE [dbo].[procGetCompanyByVirtualDirectory]
		@virtual_directory NVARCHAR(255) 	  
AS	
	SELECT  cfcompany_info.companyunkid ,
			cfcompany_info.company_code 			
	FROM    cfcompany_info
	WHERE   1 = 1
			AND LOWER(virtual_directory) = LOWER(@virtual_directory)
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procGetCompanyInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procGetCompanyInfo] AS' 
END
GO
ALTER PROCEDURE [dbo].[procGetCompanyInfo]
  @company_code NVARCHAR(510) 
, @companyunkid INT 
AS 
SELECT  cfcompany_info.companyunkid ,
        cfcompany_info.company_code ,
        cfcompany_info.company_name ,
        cfcompany_info.email ,
        cfcompany_info.database_name ,
        cfcompany_info.userunkid ,
        company_date_added ,
        company_last_updated ,
        ISNULL(cfcompany_info.authentication_code, '') AS authentication_code ,
        ISNULL(cfcompany_info.websiteurl, '') AS websiteurl ,
        cfcompany_info.isactive ,
        ISNULL(cfcompany_info.websiteurlinternal, '') AS websiteurlinternal ,
        ISNULL(cfcompany_info.sendername, '') AS sendername ,
        ISNULL(cfcompany_info.senderaddress, '') AS senderaddress ,
        ISNULL(cfcompany_info.reference, '') AS reference ,
        ISNULL(cfcompany_info.mailserverip, '') AS mailserverip ,
        ISNULL(cfcompany_info.mailserverport, 0) AS mailserverport ,
        ISNULL(cfcompany_info.username, '') AS username ,
        ISNULL(cfcompany_info.password, '') AS password ,
        ISNULL(cfcompany_info.isloginssl, 0) AS isloginssl ,
        ISNULL(cfcompany_info.mail_body, '') AS mail_body ,
        ISNULL(cfcompany_info.applicant_declaration, '') AS applicant_declaration ,
        ISNULL(cfcompany_info.timezone_minute_diff, 0) AS timezone_minute_diff ,
        ISNULL(cfcompany_info.isQualiCertAttachMandatory, 0) AS isQualiCertAttachMandatory ,
        cfcompany_info.image AS image ,
		cfcompany_info.admin_email,
		cfcompany_info.Date_Format,
		cfcompany_info.DateSeparator,
		cfcompany_info.MiddleNameMandatory,
		cfcompany_info.GenderMandatory,
		cfcompany_info.BirthDateMandatory,
		cfcompany_info.Address1Mandatory,
		cfcompany_info.Address2Mandatory,
		cfcompany_info.CountryMandatory,
		cfcompany_info.StateMandatory,
		cfcompany_info.CityMandatory,
		cfcompany_info.PostCodeMandatory,
		cfcompany_info.MaritalStatusMandatory,
		cfcompany_info.Language1Mandatory,
		cfcompany_info.NationalityMandatory,
		cfcompany_info.OneSkillMandatory,
		cfcompany_info.OneQualificationMandatory,
		cfcompany_info.OneJobExperienceMandatory,
		cfcompany_info.OneReferenceMandatory,
		cfcompany_info.RegionMandatory,
		cfcompany_info.HighestQualificationMandatory,
		cfcompany_info.EmployerMandatory,
		cfcompany_info.MotherTongueMandatory,
		cfcompany_info.CurrentSalaryMandatory,
		cfcompany_info.ExpectedSalaryMandatory,
		cfcompany_info.ExpectedBenefitsMandatory,
		cfcompany_info.NoticePeriodMandatory,
		cfcompany_info.EarliestPossibleStartDateMandatory,
		cfcompany_info.CommentsMandatory,
		cfcompany_info.VacancyFoundOutFromMandatory,
		cfcompany_info.CompanyNameJobHistoryMandatory,
		cfcompany_info.PositionHeldMandatory,
		cfcompany_info.appqualisortbyid,
		cfcompany_info.isemailpwd_encrypted,
		 ISNULL(cfcompany_info.email_type, 0) AS email_type,
		 ISNULL(cfcompany_info.ews_url, '') AS ews_url,
		 ISNULL(cfcompany_info.ews_domain, '') AS ews_domain,
		 ISNULL(cfcompany_info.isvacancyAlert,0) AS isvacancyAlert,
		 ISNULL(cfcompany_info.maxvacancyalert,0) AS maxvacancyalert,
		 ISNULL(cfcompany_info.issendjobconfirmationemail,0) AS issendjobconfirmationemail,
		 ISNULL(cfcompany_info.isattachapplicantcv,0) AS isattachapplicantcv,
		 ISNULL(cfcompany_info.OneCurriculamVitaeMandatory,0) AS OneCurriculamVitaeMandatory,
		 ISNULL(cfcompany_info.OneCoverLetterMandatory,0) AS OneCoverLetterMandatory,
		 ISNULL(cfcompany_info.QualificationStartDateMandatory,0) AS QualificationStartDateMandatory,
		 ISNULL(cfcompany_info.QualificationAwardDateMandatory,0) AS QualificationAwardDateMandatory,
		 ISNULL(cfcompany_info.virtual_directory,'') AS virtual_directory
FROM    cfcompany_info
WHERE   cfcompany_info.companyunkid = @companyunkid
        AND cfcompany_info.company_code = @company_code
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'cfemail_setup')
BEGIN
	CREATE TABLE [dbo].cfemail_setup (
		[emailsetupunkid] [int] NULL,
		[sendername] [nvarchar](510) NULL,
		[senderaddress] [nvarchar](510) NULL,
		[reference] [nvarchar](510) NULL,
		[mailserverip] [nvarchar](510) NULL,
		[mailserverport] [int] NULL,
		[username] [nvarchar](510) NULL,
		[password] [nvarchar](510) NULL,
		[isloginssl] [bit] NULL,
		[iscrt_authenticated] [bit] NULL,
		[isbypassproxy] [bit] NULL,
		[mail_body] [nvarchar](2000) NULL,		
		[email_type] [int] NULL DEFAULT ((0)),
		[ews_url] [nvarchar](255) NULL DEFAULT (''),
		[ews_domain] [nvarchar](255) NULL DEFAULT (''),
		[isvoid] [bit] NULL,
		[companyunkid] int NULL,
		[Comp_Code] [nvarchar](510) NULL,	
	)

	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'emailsetupunkid', @name=N'MS_Description', @value=N'Transaction Unique ID.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'sendername', @name=N'MS_Description', @value=N'sender name.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'senderaddress', @name=N'MS_Description', @value=N'sender email address.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'reference', @name=N'MS_Description', @value=N'reference.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'mailserverip', @name=N'MS_Description', @value=N'smtp mail server ip.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'mailserverport', @name=N'MS_Description', @value=N'smtp mail server port.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'username', @name=N'MS_Description', @value=N'email address.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'password', @name=N'MS_Description', @value=N'password for email.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'isloginssl', @name=N'MS_Description', @value=N'to check it is ssl.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'iscrt_authenticated', @name=N'MS_Description', @value=N'to check certificate is authenticated.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'isbypassproxy', @name=N'MS_Description', @value=N'to check by pass proxy.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'mail_body', @name=N'MS_Description', @value=N'body part of email.'	
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'email_type', @name=N'MS_Description', @value=N'0 = SMTP, 1 = EWS.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'ews_url', @name=N'MS_Description', @value=N'Used to store Web Service URL for Exchange Server Web Service.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'ews_domain', @name=N'MS_Description', @value=N'Used to store Web Service Domain for Exchange Server Web Service.'	
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'Syncdatetime', @name=N'MS_Description', @value=N'Determines date time when it was synced with recruitment portal.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'isvoid', @name=N'MS_Description', @value=N'Determines whether transaction deleted or not.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'companyunkid', @name=N'MS_Description', @value=N'Used to store companyunkid from cfcompany_info.'
	EXEC sys.sp_addextendedproperty @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cfemail_setup', @level2type=N'COLUMN',@level2name=N'Comp_Code', @name=N'MS_Description', @value=N'Used to store Comp_Code.'

END
GO
/****** Object:  StoredProcedure [dbo].[procGetSuperadminEmailsetting]    Script Date: 07/02/2017 05:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procGetEmailSetup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procGetEmailSetup] AS' 
END
GO
ALTER PROCEDURE [dbo].[procGetEmailSetup] 
	  @Comp_Code NVARCHAR(510) 
	, @companyunkid INT 
AS	
SELECT  cfemail_setup.emailsetupunkid 
      , cfemail_setup.sendername
      , cfemail_setup.senderaddress
      , cfemail_setup.reference
      , cfemail_setup.mailserverip
      , cfemail_setup.mailserverport
      , cfemail_setup.username
	  , cfemail_setup.[password]
	  , cfemail_setup.isloginssl
	  , cfemail_setup.mail_body
	  , cfemail_setup.isvoid
	  , ISNULL(cfemail_setup.email_type, 0) AS email_type
	  , ISNULL(cfemail_setup.ews_url, '') AS ews_url
	  , ISNULL(cfemail_setup.ews_domain, '') AS ews_domain
FROM    cfemail_setup
WHERE   cfemail_setup.isvoid = 0	
AND cfemail_setup.Comp_Code = @Comp_Code
AND cfemail_setup.companyunkid = @companyunkid
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcvacancy_master' AND COLUMNS.COLUMN_NAME = 'classgroupunkid')
BEGIN
	ALTER TABLE rcvacancy_master ADD classgroupunkid INT NULL
	EXECUTE('UPDATE rcvacancy_master SET classgroupunkid = 0  WHERE classgroupunkid IS NULL')
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rcvacancy_master', @level2type=N'COLUMN',@level2name=N'classgroupunkid',  @value=N'Unique Id from hrclassgroup_master table.'
END

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcvacancy_master' AND COLUMNS.COLUMN_NAME = 'classunkid')
BEGIN
	ALTER TABLE rcvacancy_master ADD classunkid INT NULL
	EXECUTE('UPDATE rcvacancy_master SET classunkid = 0  WHERE classunkid IS NULL')
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rcvacancy_master', @level2type=N'COLUMN',@level2name=N'classunkid',  @value=N'Unique Id from hrclasses_master table.'
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hrclassgroup_master]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[hrclassgroup_master](
	[classgroupunkid] [int] NOT NULL,
	[code] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[description] [nvarchar](1000) NULL,
	[isactive] [bit] NULL,
	[name1] [nvarchar](255) NULL,
	[name2] [nvarchar](255) NULL,
	[Comp_Code] [nvarchar](510) NULL,
	[companyunkid] [int] NULL,	
) ON [PRIMARY]
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'classgroupunkid',  @value=N'Transaction Unique Id.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'code',  @value=N'class group code.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'name',  @value=N'class group name.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'description',  @value=N'class group description.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'isactive',  @value=N'to check transaction is active.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'name1',  @value=N'class group name in custom language 1.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'name2',  @value=N'class group name custom language 2.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'Comp_Code',  @value=N'company code from cfcompany_master.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclassgroup_master', @level2type=N'COLUMN',@level2name=N'companyunkid',  @value=N'unique id from cfcompany_master.'
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hrclasses_master]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[hrclasses_master](
	[classesunkid] [int] NOT NULL,
	[classgroupunkid] [int] NULL,
	[code] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[description] [nvarchar](1000) NULL,
	[isactive] [bit] NULL,
	[name1] [nvarchar](255) NULL,
	[name2] [nvarchar](255) NULL,
	[Comp_Code] [nvarchar](510) NULL,
	[companyunkid] [int] NULL,	
) ON [PRIMARY]
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'classesunkid',  @value=N'Transaction Unique Id.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'classgroupunkid',  @value=N'Unique Id from hrclassgroup_master table.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'code',  @value=N'class code.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'name',  @value=N'class name.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'description',  @value=N'class description.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'isactive',  @value=N'to check transaction is active.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'name1',  @value=N'class name in custom language 1.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'name2',  @value=N'class name custom language 2.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'Comp_Code',  @value=N'company code from cfcompany_master table.'
	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hrclasses_master', @level2type=N'COLUMN',@level2name=N'companyunkid',  @value=N'unique id from cfcompany_master table.'
END
GO
/****** Object:  StoredProcedure [dbo].[procEditEmailSetup]    Script Date: 08/11/2016 03:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procEditEmailSetup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[procEditEmailSetup] AS' 
END
GO
ALTER PROCEDURE [dbo].[procEditEmailSetup]
	  @comp_code NVARCHAR(510) 
    , @companyunkid INT
	, @emailsetupunkid INT
	, @username NVARCHAR(510) 
	, @password NVARCHAR(510)
	, @mailserverip  NVARCHAR(510) 		
	, @mailserverport INT 
	, @isloginssl BIT 
	, @email_type INT 
	, @ews_url NVARCHAR(510) 	
	, @ews_domain NVARCHAR(510) 
	, @sendername NVARCHAR(510) 
AS 
IF @emailsetupunkid > 0
BEGIN	
	IF (@email_type = 0 )
	BEGIN
		UPDATE cfemail_setup SET
			username = @username 
			, [password] = @password 
			, mailserverip = @mailserverip 			
			, mailserverport = @mailserverport 
			, isloginssl = @isloginssl 
			, sendername = @sendername
		WHERE comp_code = @comp_code 
			AND companyunkid = @companyunkid
			AND emailsetupunkid = @emailsetupunkid
	END
	ELSE IF (@email_type = 1 )
	BEGIN
		UPDATE cfemail_setup SET
			username = @username 
			, [password] = @password 
			, ews_url = @ews_url 
			, ews_domain = @ews_domain 
		WHERE comp_code = @comp_code 
			AND companyunkid = @companyunkid
			AND emailsetupunkid = @emailsetupunkid
	END
END
GO
/* 12-Apr-2022 Start */
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantskill_tran' AND COLUMNS.COLUMN_NAME = 'created_date')
BEGIN
	ALTER TABLE rcapplicantskill_tran ADD created_date DATETIME NULL
	EXECUTE('UPDATE rcapplicantskill_tran SET created_date = ''19000101'' WHERE created_date IS NULL')
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapplicantqualification_tran' AND COLUMNS.COLUMN_NAME = 'created_date')
BEGIN
	ALTER TABLE rcapplicantqualification_tran ADD created_date DATETIME NULL
	EXECUTE('UPDATE rcapplicantqualification_tran SET created_date = ''19000101'' WHERE created_date IS NULL')
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcjobhistory' AND COLUMNS.COLUMN_NAME = 'created_date')
BEGIN
	ALTER TABLE rcjobhistory ADD created_date DATETIME NULL
	EXECUTE('UPDATE rcjobhistory SET created_date = ''19000101'' WHERE created_date IS NULL')
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcapp_reference_tran' AND COLUMNS.COLUMN_NAME = 'created_date')
BEGIN
	ALTER TABLE rcapp_reference_tran ADD created_date DATETIME NULL
	EXECUTE('UPDATE rcapp_reference_tran SET created_date = ''19000101'' WHERE created_date IS NULL')
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'cfcommon_master' AND COLUMNS.COLUMN_NAME = 'issyncwithrecruitment')
BEGIN
	ALTER TABLE cfcommon_master ADD issyncwithrecruitment BIT NULL
	EXECUTE('UPDATE cfcommon_master SET issyncwithrecruitment = 0 WHERE cfcommon_master.mastertype NOT IN (33, 71) AND issyncwithrecruitment IS NULL')
	EXECUTE('UPDATE cfcommon_master SET issyncwithrecruitment = 1 WHERE cfcommon_master.mastertype IN (33, 71) AND issyncwithrecruitment IS NULL')
END
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = 'rcattachfiletran' AND COLUMNS.COLUMN_NAME = 'appvacancytranunkid')
BEGIN
	ALTER TABLE rcattachfiletran ADD appvacancytranunkid INT NULL
	EXECUTE('UPDATE rcattachfiletran SET appvacancytranunkid = 0 WHERE appvacancytranunkid IS NULL')
END
GO
/* 12-Apr-2022 End */