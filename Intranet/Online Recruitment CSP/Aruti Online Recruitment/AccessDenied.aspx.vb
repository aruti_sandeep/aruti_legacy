﻿Public Class AccessDenied
    Inherits Base_General

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString.Count > 0 Then
            If Request.QueryString.Item("Reason") <> "" Then
                lblReason.Text = Request.QueryString.Item("Reason")
            End If
        End If
    End Sub

End Class