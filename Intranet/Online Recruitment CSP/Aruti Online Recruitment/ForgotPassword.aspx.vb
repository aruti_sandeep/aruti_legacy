﻿Option Strict On

Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail
Imports Microsoft.AspNet.Identity
'Imports Microsoft.AspNet.Identity.Owin

Public Class ForgotPassword
    Inherits Base_General

    Dim Email, ConString, ActivationUrl As String
    'Dim message As MailMessage
    'Dim smtp As SmtpClient

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                    Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes)
                    img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                Else
                    img_shortcuticon.ImageUrl = "~/Images/blank.png"
                End If

                '*** "Object reference not set to an instance of an object." error was coming if register.aspx is visited directly.
                If Session("mstrUrlArutiLink") Is Nothing Then
                    Response.Redirect("UnauthorizedAccess.aspx", False)
                    Exit Try
                End If

                If Session("mstrUrlArutiLink").ToString.ToLower.Contains("/alogin.aspx") = True Then

                End If

                If Request.UrlReferrer.ToString.ToLower.Contains("slogin.aspx") Then

                    If Session("sadminusername").ToString <> "" AndAlso Session("sadminpassword").ToString <> "" Then
                        Session("blnEmailSetupDone") = True
                    Else
                        Session("blnEmailSetupDone") = False
                        Response.Redirect("ucons.aspx", False)
                        Exit Sub
                    End If
                Else
                    If Session("username").ToString <> "" AndAlso Session("password").ToString <> "" Then
                        Session("blnEmailSetupDone") = True
                    Else
                        Session("blnEmailSetupDone") = False
                        Response.Redirect("ucons.aspx", False)
                        Exit Sub
                    End If
                End If

                If Request.Url.AbsoluteUri.ToString.ToLower.Contains("arutihr.com") = False Then
                    pnlPoweredBy.Visible = False
                Else
                    pnlPoweredBy.Visible = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    'Private Sub PasswordRecovery1_SendingMail(sender As Object, e As MailMessageEventArgs) Handles PasswordRecovery1.SendingMail
    '    Dim objApplicant As New clsApplicant
    '    Try
    '        e.Cancel = True

    '        Email = PasswordRecovery1.UserName
    '        Dim strBody As String = "<!DOCTYPE html>" &
    '            "<html>" &
    '            "<head>" &
    '                "<title>Your Request Has been received...</title>" &
    '                "<meta charset='utf-8' />" &
    '            "</head>" &
    '            "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
    '                "<br />" &
    '                "#imgcmp#" &
    '                "<p>Dear <b>#firstname# #lastname#</b>,</p>" &
    '                "<p>Your password has been reset!</p>" &
    '                "<p>As per our records, you have requested for password reset.</p>" &
    '                "<p>Your Verification Code is <B>#otp#</B></p>" &
    '                "<p>Please click on the below link or copy and paste the link to address bar to reset your password.</p>" &
    '                "<p> #resetpwdlink# </p>" &
    '                "<p>This link is valid for 24 hours.</p>" &
    '                "<br />" &
    '                "<p><B>Thank you,</B></p>" &
    '                "<p><B>#companyname# Support Team</B></p>" &
    '                "<br />" &
    '                "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
    '                "#imgaruti#" &
    '            "</body>" &
    '            "</html>"

    '        Dim strPwd As String = ""
    '        Dim strSubject As String = ""
    '        Dim reader As New StreamReader(Server.MapPath("~/EmailTemplate/PasswordReset.html"))
    '        If reader IsNot Nothing Then
    '            strBody = reader.ReadToEnd
    '            reader.Close()
    '        End If
    '        strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", Session("CompName").ToString)
    '        If strSubject.Trim = "" Then
    '            'strSubject = "Your Password Has been Reset..."
    '            strSubject = "Your Request Has been received..."
    '        End If

    '        Dim strCode As String = Guid.NewGuid.ToString
    '        Dim strOTP As String = objApplicant.GenerateOTP(6)

    '        ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("ResetPassword.aspx") & "?Email=" & Server.UrlEncode(clsCrypto.Encrypt(Email)) + "&Code=" + Server.UrlEncode(clsCrypto.Encrypt(strCode)) + "&OTP=" + Server.UrlEncode(clsCrypto.Encrypt(strOTP)) + "&a=" + Server.UrlEncode(clsCrypto.Encrypt(PasswordRecovery1.Answer)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(Session("mstrUrlArutiLink").ToString)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(Session("CompCode").ToString)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(Session("companyunkid").ToString))
    '        ActivationUrl = ActivationUrl.Replace("&", "&amp;")

    '        Dim profile As UserProfile = UserProfile.GetUserProfile(Email)
    '        Dim sFName As String = profile.FirstName
    '        Dim sLName As String = profile.LastName
    '        If sFName.Trim = "" Then sFName = Email.Trim

    '        If objApplicant.InsertResetPassword(strCompCode:=Session("CompCode").ToString _
    '                                            , intCompUnkID:=CInt(Session("companyunkid")) _
    '                                            , strEmail:=Email _
    '                                            , strCode:=strCode _
    '                                            , strOTP:=strOTP _
    '                                            , strSubject:=strSubject _
    '                                            , strMessage:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
    '                                            , strLink:=ActivationUrl
    '                                            ) = True Then
    '            'message = New MailMessage(Session("username").ToString, Email.Trim())
    '            'Message.To.Add(Email.Trim())
    '            'Message.Subject = strSubject
    '            ''message.Body = "Hi " + PasswordRecovery1.UserName + "!<BR>" &
    '            ''       "Please return to the site and log in using the following information.<BR>" &
    '            ''       "User Name: " & PasswordRecovery1.UserName & "<BR>" &
    '            ''       "Password: " & GetNewPassword(Email) & "<BR>Thanks!"
    '            'strPwd = GetNewPassword(Email)
    '            'message.Body = strBody.Replace("#username#", Email.Trim()).Replace("#password#", strPwd).Replace("#chagepwdlink#", Request.Url.GetLeftPart(UriPartial.Authority) & "/ChangePassword.aspx?UserID=" & Server.UrlEncode(GetUserID(Email)) & "&Email=" & Server.UrlEncode(Email))
    '            'message.Body = strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP)
    '            'Message.IsBodyHtml = True

    '            'smtp = New SmtpClient
    '            'smtp.Host = Session("mailserverip").ToString
    '            'smtp.Port = CInt(Session("mailserverport"))
    '            'smtp.Credentials = New System.Net.NetworkCredential(Session("username").ToString, Session("password").ToString)
    '            'smtp.EnableSsl = CBool(Session("isloginssl"))
    '            'smtp.Send(message)

    '            'Dim objMail As New clsMail
    '            Dim bAddLogo As Boolean = True
    '            Dim bSadmin As Boolean = False
    '            'If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = True Then
    '            If Roles.IsUserInRole(Email, "superadmin") = True Then
    '                bAddLogo = False
    '                bSadmin = True
    '            End If

    '            ''Sohail - (04 Oct 2018) - Start
    '            ''Issue : Emails were unable to send for Super Admin forgot password
    '            ''If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
    '            ''                                   , strCompCode:=Session("CompCode").ToString() _
    '            ''                                   , ByRefblnIsActive:=False
    '            ''                                   ) = False Then

    '            ''    Exit Try
    '            ''End If
    '            'If bSadmin = False AndAlso objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
    '            '                                   , strCompCode:=Session("CompCode").ToString() _
    '            '                                   , ByRefblnIsActive:=False
    '            '                                   ) = False Then

    '            '    Exit Try
    '            'End If
    '            ''Sohail - (04 Oct 2018) - End

    '            'Dim objMail As New clsMail
    '            'If objMail.SendEmail(strToEmail:=Email _
    '            '                     , strSubject:=strSubject _
    '            '                     , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
    '            '                     , blnAddLogo:=bAddLogo _
    '            '                     , blnSuperAdmin:=bSadmin
    '            '                     ) = False Then

    '            '    PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
    '            '    PasswordRecovery1.SuccessText = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
    '            '    e.Cancel = True
    '            'Else
    '            '    PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
    '            '    PasswordRecovery1.SuccessText = "Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours."
    '            'End If

    '            If bSadmin = False Then

    '                If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
    '                                               , strCompCode:=Session("CompCode").ToString() _
    '                                               , ByRefblnIsActive:=False
    '                                               ) = False Then
    '                    Exit Try
    '                End If

    '                Dim objMail As New clsMail
    '                If objMail.SendEmail(strToEmail:=Email _
    '                                     , strSubject:=strSubject _
    '                                     , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
    '                                     , blnAddLogo:=True
    '                                     ) = False Then

    '                    PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
    '                    PasswordRecovery1.SuccessText = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
    '                    e.Cancel = True
    '                Else
    '                    PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
    '                    PasswordRecovery1.SuccessText = "Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours."
    '                End If
    '            Else
    '                Dim objSA As New clsSACommon
    '                If objSA.GetSuperAdminEmailSettings() Then

    '                    Dim objMail As New clsMail
    '                    If objMail.SendEmail(strToEmail:=Email _
    '                                         , strSubject:=strSubject _
    '                                         , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
    '                                         , blnAddLogo:=False _
    '                                         , blnSuperAdmin:=True
    '                                         ) = False Then

    '                        PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
    '                        PasswordRecovery1.SuccessText = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
    '                        e.Cancel = True
    '                    Else
    '                        PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
    '                        PasswordRecovery1.SuccessText = "Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours."
    '                    End If
    '                End If

    '            End If

    '        Else

    '        End If


    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START            
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    Finally
    '        objApplicant = Nothing
    '    End Try
    'End Sub

    'Private Sub PasswordRecovery1_VerifyingUser(sender As Object, e As LoginCancelEventArgs) Handles PasswordRecovery1.VerifyingUser
    '    Try
    '        If Membership.GetUser(PasswordRecovery1.UserName) IsNot Nothing Then

    '            'If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = True Then
    '            '    ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
    '            '    e.Cancel = True
    '            '    Exit Try
    '            'End If

    '            If Membership.GetUser(PasswordRecovery1.UserName).IsApproved = False Then
    '                ShowMessage("Sorry, Please check your email and activate your account first.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If

    '            If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = False Then
    '                Dim objSA As New clsSACommon

    '                'Sohail (14 Mar 2018) -- Start
    '                'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    '                'Dim ds As DataSet = objSA.GetCompanyCodeFromEmail(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                'Sohail (23 Mar 2018) -- Start
    '                'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    '                'Dim ds As DataSet = objSA.GetCompanyCodeFromApplicant(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                Dim ds As DataSet
    '                If Roles.IsUserInRole(PasswordRecovery1.UserName, "admin") = True Then
    '                    ds = objSA.GetCompanyCodeFromEmail(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                Else
    '                    ds = objSA.GetCompanyCodeFromApplicant(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                End If

    '                'Sohail (23 Mar 2018) -- End
    '                'Sohail (14 Mar 2018) -- End
    '                If ds.Tables(0).Rows.Count <= 0 Then
    '                    ShowMessage("Sorry, You are not registered.", MessageType.Info)
    '                    e.Cancel = True
    '                    Exit Try
    '                Else
    '                    'Sohail (14 Mar 2018) -- Start
    '                    'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    '                    'If ds.Tables(0).Rows(0).Item("Comp_Code").ToString = Session("CompCode").ToString AndAlso CInt(ds.Tables(0).Rows(0).Item("companyunkid")) = CInt(Session("companyunkid")) Then
    '                    If ds.Tables(0).Select(" Comp_Code = '" & Session("CompCode").ToString & "'  AND companyunkid = " & CInt(Session("companyunkid")) & " ").Length > 0 Then
    '                        'Sohail (14 Mar 2018) -- End

    '                    Else
    '                        ShowMessage("Sorry, This company is restricted for your account.", MessageType.Info)
    '                        e.Cancel = True
    '                        Exit Try
    '                    End If
    '                End If
    '            End If

    '            If Membership.GetUser(PasswordRecovery1.UserName).IsLockedOut = True Then
    '                Dim dtLastLockOut As Date = Membership.GetUser(PasswordRecovery1.UserName).LastLockoutDate
    '                Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
    '                'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

    '                If DateTime.Now > dtUnlockDate Then
    '                    Membership.GetUser(PasswordRecovery1.UserName).UnlockUser()
    '                Else
    '                    If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
    '                    Else
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
    '                    End If
    '                    e.Cancel = True
    '                    Exit Try
    '                End If
    '            End If

    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub SubmitButton_Click(sender As Object, e As EventArgs) Handles SubmitButton.Click
        Dim objApplicant As New clsApplicant
        Try
            If txtUserName.Text.Trim = "" Then
                ShowMessage("Please enter Email.", MessageType.Errorr)
                txtUserName.Focus()
                Exit Try

            ElseIf clsApplicant.IsValidEmail(txtUserName.Text) = False Then
                ShowMessage("Please enter Valid Email.", MessageType.Errorr)
                txtUserName.Focus()
                Exit Try
            End If

            If Membership.GetUser(txtUserName.Text) IsNot Nothing Then

                'If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = True Then
                '    ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
                '    e.Cancel = True
                '    Exit Try
                'End If

                If Membership.GetUser(txtUserName.Text).IsApproved = False Then
                    ShowMessage("Sorry, Please check your email and activate your account first.", MessageType.Info)
                    'e.Cancel = True
                    Exit Try
                End If

                If Roles.IsUserInRole(txtUserName.Text, "superadmin") = False Then
                    Dim objSA As New clsSACommon

                    'Sohail (14 Mar 2018) -- Start
                    'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
                    'Dim ds As DataSet = objSA.GetCompanyCodeFromEmail(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
                    'Sohail (23 Mar 2018) -- Start
                    'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
                    'Dim ds As DataSet = objSA.GetCompanyCodeFromApplicant(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
                    Dim ds As DataSet
                    If Roles.IsUserInRole(txtUserName.Text, "admin") = True Then
                        ds = objSA.GetCompanyCodeFromEmail(txtUserName.Text, Membership.GetUser(txtUserName.Text).ProviderUserKey.ToString)
                    Else
                        ds = objSA.GetCompanyCodeFromApplicant(txtUserName.Text, Membership.GetUser(txtUserName.Text).ProviderUserKey.ToString)
                    End If

                    'Sohail (23 Mar 2018) -- End
                    'Sohail (14 Mar 2018) -- End
                    If ds.Tables(0).Rows.Count <= 0 Then
                        ShowMessage("Sorry, You are not registered.", MessageType.Info)
                        'e.Cancel = True
                        Exit Try
                    Else
                        'Sohail (14 Mar 2018) -- Start
                        'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
                        'If ds.Tables(0).Rows(0).Item("Comp_Code").ToString = Session("CompCode").ToString AndAlso CInt(ds.Tables(0).Rows(0).Item("companyunkid")) = CInt(Session("companyunkid")) Then
                        If ds.Tables(0).Select(" Comp_Code = '" & Session("CompCode").ToString & "'  AND companyunkid = " & CInt(Session("companyunkid")) & " ").Length > 0 Then
                            'Sohail (14 Mar 2018) -- End

                        Else
                            ShowMessage("Sorry, This company is restricted for your account.", MessageType.Info)
                            'e.Cancel = True
                            Exit Try
                        End If
                    End If
                End If

                If Membership.GetUser(txtUserName.Text).IsLockedOut = True Then
                    Dim dtLastLockOut As Date = Membership.GetUser(txtUserName.Text).LastLockoutDate
                    Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
                    'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

                    If DateTime.Now > dtUnlockDate Then
                        Membership.GetUser(txtUserName.Text).UnlockUser()
                    Else
                        If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
                        Else
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
                        End If
                        'e.Cancel = True
                        Exit Try
                    End If
                End If

                'e.Cancel = True

                Email = txtUserName.Text
                Dim strBody As String = "<!DOCTYPE html>" &
                "<html>" &
                "<head>" &
                    "<title>Your Request Has been received...</title>" &
                    "<meta charset='utf-8' />" &
                "</head>" &
                "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
                    "<br />" &
                    "#imgcmp#" &
                    "<p>Dear <b>#firstname# #lastname#</b>,</p>" &
                    "<p>Your password has been reset!</p>" &
                    "<p>As per our records, you have requested for password reset.</p>" &
                    "<p>Your Verification Code is <B>#otp#</B></p>" &
                    "<p>Please click on the below link or copy and paste the link to address bar to reset your password.</p>" &
                    "<p> #resetpwdlink# </p>" &
                    "<p>This link is valid for 24 hours.</p>" &
                    "<br />" &
                    "<p><B>Thank you,</B></p>" &
                    "<p><B>#companyname# Support Team</B></p>" &
                    "<br />" &
                    "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
                    "#imgaruti#" &
                "</body>" &
                "</html>"

                Dim strPwd As String = ""
                Dim strSubject As String = ""
                Dim reader As New StreamReader(Server.MapPath("~/EmailTemplate/PasswordReset.html"))
                If reader IsNot Nothing Then
                    strBody = reader.ReadToEnd
                    reader.Close()
                End If
                strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", Session("CompName").ToString)
                If strSubject.Trim = "" Then
                    'strSubject = "Your Password Has been Reset..."
                    strSubject = "Your Request Has been received..."
                End If

                Dim strCode As String = Guid.NewGuid.ToString
                Dim strOTP As String = objApplicant.GenerateOTP(6)

                'ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("ResetPassword.aspx") & "?Email=" & Server.UrlEncode(clsCrypto.Encrypt(Email)) + "&Code=" + Server.UrlEncode(clsCrypto.Encrypt(strCode)) + "&OTP=" + Server.UrlEncode(clsCrypto.Encrypt(strOTP)) + "&a=" + Server.UrlEncode(clsCrypto.Encrypt(txtUserName.Text)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(Session("mstrUrlArutiLink").ToString)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(Session("CompCode").ToString)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(Session("companyunkid").ToString))
                ActivationUrl = Request.Form(hflocationorigin.UniqueID) & Replace(Request.ApplicationPath & "/ResetPassword.aspx", "//", "/") & "?Email=" & Server.UrlEncode(clsCrypto.Encrypt(Email)) + "&Code=" + Server.UrlEncode(clsCrypto.Encrypt(strCode)) + "&OTP=" + Server.UrlEncode(clsCrypto.Encrypt(strOTP)) + "&a=" + Server.UrlEncode(clsCrypto.Encrypt(txtUserName.Text)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(Session("mstrUrlArutiLink").ToString)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(Session("CompCode").ToString)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(Session("companyunkid").ToString))
                'ActivationUrl = ActivationUrl.Replace("&", "&amp;")

                Dim profile As UserProfile = UserProfile.GetUserProfile(Email)
                Dim sFName As String = profile.FirstName
                Dim sLName As String = profile.LastName
                If sFName.Trim = "" Then sFName = Email.Trim

                If objApplicant.InsertResetPassword(strCompCode:=Session("CompCode").ToString _
                                                , intCompUnkID:=CInt(Session("companyunkid")) _
                                                , strEmail:=Email _
                                                , strCode:=strCode _
                                                , strOTP:=strOTP _
                                                , strSubject:=strSubject _
                                                , strMessage:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
                                                , strLink:=ActivationUrl
                                                ) = True Then
                    'message = New MailMessage(Session("username").ToString, Email.Trim())
                    'Message.To.Add(Email.Trim())
                    'Message.Subject = strSubject
                    ''message.Body = "Hi " + PasswordRecovery1.UserName + "!<BR>" &
                    ''       "Please return to the site and log in using the following information.<BR>" &
                    ''       "User Name: " & PasswordRecovery1.UserName & "<BR>" &
                    ''       "Password: " & GetNewPassword(Email) & "<BR>Thanks!"
                    'strPwd = GetNewPassword(Email)
                    'message.Body = strBody.Replace("#username#", Email.Trim()).Replace("#password#", strPwd).Replace("#chagepwdlink#", Request.Url.GetLeftPart(UriPartial.Authority) & "/ChangePassword.aspx?UserID=" & Server.UrlEncode(GetUserID(Email)) & "&Email=" & Server.UrlEncode(Email))
                    'message.Body = strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP)
                    'Message.IsBodyHtml = True

                    'smtp = New SmtpClient
                    'smtp.Host = Session("mailserverip").ToString
                    'smtp.Port = CInt(Session("mailserverport"))
                    'smtp.Credentials = New System.Net.NetworkCredential(Session("username").ToString, Session("password").ToString)
                    'smtp.EnableSsl = CBool(Session("isloginssl"))
                    'smtp.Send(message)

                    'Dim objMail As New clsMail
                    Dim bAddLogo As Boolean = True
                    Dim bSadmin As Boolean = False
                    'If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = True Then
                    If Roles.IsUserInRole(Email, "superadmin") = True Then
                        bAddLogo = False
                        bSadmin = True
                    End If

                    ''Sohail - (04 Oct 2018) - Start
                    ''Issue : Emails were unable to send for Super Admin forgot password
                    ''If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                    ''                                   , strCompCode:=Session("CompCode").ToString() _
                    ''                                   , ByRefblnIsActive:=False
                    ''                                   ) = False Then

                    ''    Exit Try
                    ''End If
                    'If bSadmin = False AndAlso objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                    '                                   , strCompCode:=Session("CompCode").ToString() _
                    '                                   , ByRefblnIsActive:=False
                    '                                   ) = False Then

                    '    Exit Try
                    'End If
                    ''Sohail - (04 Oct 2018) - End

                    'Dim objMail As New clsMail
                    'If objMail.SendEmail(strToEmail:=Email _
                    '                     , strSubject:=strSubject _
                    '                     , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
                    '                     , blnAddLogo:=bAddLogo _
                    '                     , blnSuperAdmin:=bSadmin
                    '                     ) = False Then

                    '    PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
                    '    PasswordRecovery1.SuccessText = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
                    '    e.Cancel = True
                    'Else
                    '    PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
                    '    PasswordRecovery1.SuccessText = "Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours."
                    'End If

                    If bSadmin = False Then

                        If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                                                   , strCompCode:=Session("CompCode").ToString() _
                                                   , ByRefblnIsActive:=False
                                                   ) = False Then
                            Exit Try
                        End If

                        Dim objCompany1 As New clsCompany
                        Session("e_emailsetupunkid") = 0
                        objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))
                        If Session("m_strLogFile") Is Nothing OrElse CStr(Session("m_strLogFile")).Trim = "" Then
                            Session("m_strLogFile") = IO.Path.Combine(Server.MapPath("~") & "/UploadImage", "EmailSetting.txt")
                        End If
                        'System.IO.File.AppendAllText(CStr(Session("m_strLogFile")), "Forgot Password Submit : emailsetupunkid : " & CStr(Session("e_emailsetupunkid")) & vbCrLf)
                        'If CInt(Session("e_emailsetupunkid")) > 0 Then
                        '    System.IO.File.AppendAllText(CStr(Session("m_strLogFile")), "e email : " & CStr(Session("e_username")) & vbCrLf)
                        '    System.IO.File.AppendAllText(CStr(Session("m_strLogFile")), "e sender name : " & CStr(Session("e_sendername")) & vbCrLf)
                        'Else
                        '    System.IO.File.AppendAllText(CStr(Session("m_strLogFile")), "email : " & CStr(Session("username")) & vbCrLf)
                        '    System.IO.File.AppendAllText(CStr(Session("m_strLogFile")), "sender name : " & CStr(Session("sendername")) & vbCrLf)
                        'End If

                        Dim objMail As New clsMail
                        If objMail.SendEmail(strToEmail:=Email _
                                         , strSubject:=strSubject _
                                         , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
                                         , blnAddLogo:=True
                                         ) = False Then

                            'PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
                            FailureText.Text = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
                            'e.Cancel = True
                        Else
                            'PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
                            lblSuccess.Text = "Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours."
                            pnlSuccess.Visible = True
                            pnlForgot.Visible = False
                        End If
                    Else
                        Dim objSA As New clsSACommon
                        If objSA.GetSuperAdminEmailSettings() Then

                            Dim objMail As New clsMail
                            If objMail.SendEmail(strToEmail:=Email _
                                             , strSubject:=strSubject _
                                             , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#resetpwdlink#", ActivationUrl).Replace("#otp#", strOTP).Replace("#firstname#", sFName).Replace("#lastname#", sLName).Replace("#companyname#", Session("CompName").ToString) _
                                             , blnAddLogo:=False _
                                             , blnSuperAdmin:=True
                                             ) = False Then

                                'PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
                                FailureText.Text = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
                                'e.Cancel = True
                            Else
                                'PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
                                lblSuccess.Text = "Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours."
                                pnlSuccess.Visible = True
                                pnlForgot.Visible = False
                            End If
                        End If

                    End If

                Else

                End If
            Else
                FailureText.Text = "Sorry, This Email does not exist!"
                'e.Cancel = True
                Exit Try
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
        End Try
    End Sub

    'Private Sub PasswordRecovery1_VerifyingUser(sender As Object, e As LoginCancelEventArgs) Handles PasswordRecovery1.VerifyingUser
    '    Try
    '        If Membership.GetUser(PasswordRecovery1.UserName) IsNot Nothing Then

    '            'If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = True Then
    '            '    ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
    '            '    e.Cancel = True
    '            '    Exit Try
    '            'End If

    '            If Membership.GetUser(PasswordRecovery1.UserName).IsApproved = False Then
    '                ShowMessage("Sorry, Please check your email and activate your account first.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If

    '            If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = False Then
    '                Dim objSA As New clsSACommon

    '                'Sohail (14 Mar 2018) -- Start
    '                'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    '                'Dim ds As DataSet = objSA.GetCompanyCodeFromEmail(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                'Sohail (23 Mar 2018) -- Start
    '                'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    '                'Dim ds As DataSet = objSA.GetCompanyCodeFromApplicant(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                Dim ds As DataSet
    '                If Roles.IsUserInRole(PasswordRecovery1.UserName, "admin") = True Then
    '                    ds = objSA.GetCompanyCodeFromEmail(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                Else
    '                    ds = objSA.GetCompanyCodeFromApplicant(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                End If

    '                'Sohail (23 Mar 2018) -- End
    '                'Sohail (14 Mar 2018) -- End
    '                If ds.Tables(0).Rows.Count <= 0 Then
    '                    ShowMessage("Sorry, You are not registered.", MessageType.Info)
    '                    e.Cancel = True
    '                    Exit Try
    '                Else
    '                    'Sohail (14 Mar 2018) -- Start
    '                    'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    '                    'If ds.Tables(0).Rows(0).Item("Comp_Code").ToString = Session("CompCode").ToString AndAlso CInt(ds.Tables(0).Rows(0).Item("companyunkid")) = CInt(Session("companyunkid")) Then
    '                    If ds.Tables(0).Select(" Comp_Code = '" & Session("CompCode").ToString & "'  AND companyunkid = " & CInt(Session("companyunkid")) & " ").Length > 0 Then
    '                        'Sohail (14 Mar 2018) -- End

    '                    Else
    '                        ShowMessage("Sorry, This company is restricted for your account.", MessageType.Info)
    '                        e.Cancel = True
    '                        Exit Try
    '                    End If
    '                End If
    '            End If

    '            If Membership.GetUser(PasswordRecovery1.UserName).IsLockedOut = True Then
    '                Dim dtLastLockOut As Date = Membership.GetUser(PasswordRecovery1.UserName).LastLockoutDate
    '                Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
    '                'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

    '                If DateTime.Now > dtUnlockDate Then
    '                    Membership.GetUser(PasswordRecovery1.UserName).UnlockUser()
    '                Else
    '                    If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
    '                    Else
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
    '                    End If
    '                    e.Cancel = True
    '                    Exit Try
    '                End If
    '            End If

    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Function GetNewPassword(strEmail As String) As String
        Dim UserID As String = ""
        Try
            Dim u As MembershipUser = Membership.GetUser(Email, False)

            If u IsNot Nothing Then
                'UserID = u.ResetPassword(PasswordRecovery1.Answer)
                UserID = u.ResetPassword()
            End If

            'Dim ConString As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
            'Dim con As SqlConnection = New SqlConnection(ConString)

            'Dim cmd As SqlCommand = New SqlCommand("SELECT Password FROM aspnet_Membership WHERE email=@Email", con)
            'cmd.Parameters.AddWithValue("@Email", Email)
            'con.Open()
            'UserID = cmd.ExecuteScalar().ToString()
            'con.Close()

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return UserID
    End Function

    'Private Function GetUserID(strEmail As String) As String
    '    Dim UserID As String = ""
    '    Try
    '        Dim ConString As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '        Dim con As SqlConnection = New SqlConnection(ConString)

    '        Dim cmd As SqlCommand = New SqlCommand("SELECT UserId FROM aspnet_Membership WHERE email=@Email", con)
    '        cmd.Parameters.AddWithValue("@Email", Email)
    '        con.Open()
    '        UserID = cmd.ExecuteScalar().ToString()
    '        con.Close()

    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START            
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    '    Return UserID
    'End Function
End Class