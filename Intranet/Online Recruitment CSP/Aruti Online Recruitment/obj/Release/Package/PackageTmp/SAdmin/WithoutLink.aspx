﻿<%@ Page Title="Applicants Without Activation Link" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="WithoutLink.aspx.vb" Inherits="Aruti_Online_Recruitment.WithoutLink" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        $(document).ready(function () {
            setEvent();
        });

        function setEvent() {
            $("[id*=chkSelect]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id*=chkSelectAll]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id*=objlnkDelete]").on('click', function () {
                if (!ShowConfirm("Are you sure you want to Delete this Applicant?", this))
                    return false;
                else
                    return true;
            });

        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                setEvent()
            })
        }
    </script>


    <div class="card">

        <div class="card-header">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>Applicants Without Activation Link
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                    </h4>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdLink" EventName="DataBound" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                             <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblCompany" runat="server" Text="Company:"></asp:Label>
                            <asp:DropDownList ID="cboCompany" runat="server" CssClass="selectpicker form-control" />  <%--AutoPostBack="true" --%>                         
                        </ContentTemplate>
                    </asp:UpdatePanel>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btn-block"  Text="Search" ValidationGroup="Search" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>


            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_link" runat="server" CssClass="table-responsive minheight300">
                        <asp:GridView ID="grdLink" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                            AllowPaging="true" DataSourceID="dsLink" PageSize="200" DataKeyNames="activationlinkunkid, Comp_Code, companyunkid, LoweredEmail, subject, message">

                            <Columns>
                                <asp:BoundField DataField="Comp_Code" HeaderText="Comp. Code" />
                                <asp:BoundField DataField="companyunkid" HeaderText="Client ID" />
                                <asp:BoundField DataField="subject" HeaderText="subject" Visible="false" />
                                <asp:BoundField DataField="message" HeaderText="message" Visible="false" />
                                <asp:BoundField DataField="LoweredEmail" HeaderText="Email" />
                                <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" />
                                <asp:TemplateField HeaderText="Is Approved" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%--<asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApproved"))) %>' Enabled="false" />--%>
                                        <asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApproved"), True)) %>' Enabled="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="objlnkDelete" Text="Delete" runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="DeleteApplicant" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:ObjectDataSource ID="dsLink" runat="server" SelectMethod="GetWithoutActivationLink" TypeName="Aruti_Online_Recruitment.clsApplicant" EnablePaging="true"
                            MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetWithoutActivationLinkTotal">
                            <SelectParameters>
                                <asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                                <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />
                                <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                                <asp:Parameter Name="startRowIndex" Type="Int32" />
                                <asp:Parameter Name="intPageSize" Type="Int32" />
                                <asp:Parameter Name="strSortExpression" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:Panel>
                </ContentTemplate>
              <Triggers>
                  <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                  <asp:AsyncPostBackTrigger ControlID="btnRegister" EventName="Click" />
                  <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
              </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-footer">
            <asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-primary btn-block" Text="Delete Applicants" />
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnRegister" runat="server" CssClass="btn btn-primary btn-block" Text="Register Applicants" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="hflocationorigin" runat="server" Value="" />
        </div>
    </div>
</asp:Content>
