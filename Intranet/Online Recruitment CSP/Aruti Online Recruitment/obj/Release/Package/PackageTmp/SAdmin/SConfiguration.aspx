﻿<%@ Page Title="Configuration" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="SConfiguration.aspx.vb" Inherits="Aruti_Online_Recruitment.SConfiguration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<%--  <style>
        th a {
            color: white;
        }
    </style>--%>
	<script type="text/javascript">

		$(document).ready(function () {

			$(".switch input").bootstrapSwitch();
			$('.switch input').bootstrapSwitch('onText', 'Yes');
			$('.switch input').bootstrapSwitch('offText', 'No');

			$('#<%= chkVacancyalert.ClientID %>').on('switchChange.bootstrapSwitch', function (event, state) {
				javascript: __doPostBack('<%= chkVacancyalert.UniqueID %>', 'chkVacancyalert_CheckedChanged');
			});

			$('.switch input').bootstrapSwitch('labelWidth', 'auto');

			setevents();
		});

		var prm = Sys.WebForms.PageRequestManager.getInstance();
		if (prm != null) {
			prm.add_endRequest(function (sender, e) {

				setevents();
			});
		};

		function pageLoad(sender, args) {

			$('.selectpicker').selectpicker({
				liveSearch: true,
				maxOptions: 1
			});
		}
		function integersOnly(obj) {
			obj.value = obj.value.replace(/[^0-9]/g, '');
		}

		function setevents() {
			$("[id*=ddlCompany]").on('change', function () {
				__doPostBack($(this).get(0).id, 0)
			});
		}
	</script>

	<div class="card mx-auto MaxWidth80">
		<%--style="max-width: 80%; margin-left: auto; margin-right: auto;"--%>

		<div class="card-header">
			<h4>Configuration</h4>
		</div>

		<div class="card-body">
			<div class="form-group row">
				<div class="col-md-4">
					<asp:Label ID="LblCompany" runat="server" Text="Company"></asp:Label>
					<asp:DropDownList ID="ddlCompany" runat="server" CssClass="selectpicker form-control" />
					<%--AutoPostBack="true"--%>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-4">
					<asp:Label ID="LblVacancyAlert" runat="server" Text="Vacancy Alert / Max Alerts"></asp:Label>
					<asp:CheckBox ID="chkVacancyalert" runat="server" CssClass="switch float-right" AutoPostBack="true" />
					<%--Style="float: right;"--%>
				</div>
				<div class="col-md-4">
					<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:TextBox ID="txtMaxAlert" runat="server" TextMode="Number" required="yes" CssClass="form-control text-center width87px" onkeyup="integersOnly(this)"></asp:TextBox>
							<%--Style="text-align: center" Width="80px"--%>
						</ContentTemplate>
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="chkVacancyalert" EventName="CheckedChanged" />
						</Triggers>
					</asp:UpdatePanel>
				</div>
			</div>
		</div>

		<div class="card-footer">
			<div class="form-group row">
				<div class="col-md-4">
				</div>

				<div class="col-md-4">
					<asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary w-100" Text="Save" />
					<%--Style="width: 100%;" --%>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
