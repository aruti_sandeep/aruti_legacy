﻿<%@ Page Title="Change Password" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="SChangePassword.aspx.vb" Inherits="Aruti_Online_Recruitment.SChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

		<script type="text/javascript">

					
				$(document).ready(function () {
					$$('ChangePasswordPushButton').on('click', function () {
						return IsValidate();
					});
				});

				var prm = Sys.WebForms.PageRequestManager.getInstance();
				if (prm != null) {
					prm.add_endRequest(function (sender, e) {
						$$('ChangePasswordPushButton').on('click', function () {
							return IsValidate();
						});
					});
				};

			function IsValidate() {

					var strEmailValidExpression = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/g
					var strEmail = $('#UserName').val();

					if ($$('UserName').val().trim() == '') {
						ShowMessage('Email is required.', 'MessageType.Errorr');
						$$('UserName').focus();
						return false;
					}
					else if ($$('CurrentPassword').val().trim() == '') {
						ShowMessage('Password is required.', 'MessageType.Errorr');
						$$('CurrentPassword').focus();
						return false;
					}
					else if ($$('NewPassword').val().trim() == '') {
						ShowMessage('New Password is required.', 'MessageType.Errorr');
						$$('NewPassword').focus();
						return false;
					}
					else if ($$('ConfirmNewPassword').val().trim() == '') {
						ShowMessage('Confirm New Password is required.', 'MessageType.Errorr');
						$$('ConfirmNewPassword').focus();
						return false;
					}
					else if ($$('NewPassword').val().trim() != $$('ConfirmNewPassword').val().trim()) {
						ShowMessage('The Confirm New Password must match the New Password entry.', 'MessageType.Errorr');
						$$('NewPassword').focus();
						return false;
					}
					else if (!strEmailValidExpression.test(strEmail)) {
						ShowMessage('Email address is not valid.', 'MessageType.Errorr');
						return false;
					}

				}


	    </script>


    <div class="card mx-auto MaxWidth422px" >  <%--style="max-width: 422px; margin-left: auto; margin-right: auto;"--%>

        <div class="card-header">
            <h4>Change Password</h4>
        </div>

        <asp:ChangePassword ID="ChangePassword1" runat="server" PasswordLabelText="Old Password:" SuccessPageUrl="~/Login.aspx" DisplayUserName="True"  CancelDestinationPageUrl="~/User/UserHome.aspx" CssClass="w-100">  <%--Style="width: 100%;"--%>
            <ChangePasswordTemplate>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                            <asp:TextBox ID="UserName" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="UserName"
                                ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" ValidationGroup="ChangePassword1" Display="None"></asp:RegularExpressionValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label>
                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <%--<asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="None" ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ChangePassword1"></asp:CompareValidator>--%>
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12" style="text-align: center;">
                            <asp:Literal ID="Literal1" runat="server" EnableViewState="False"></asp:Literal>
                         <%--   <asp:ValidationSummary
                                HeaderText="You must enter a value in the following fields:"
                                DisplayMode="BulletList"
                                EnableClientScript="true"
                                runat="server" ValidationGroup="ChangePassword1" Style="color: red" />--%>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangePassword1" CssClass="btn btn-primary w-100"  />  <%--Style="width: 100%;"--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="btn btn-primary w-100"  /> <%--Style="width: 100%;"--%>
                        </div>
                    </div>
                </div>

            </ChangePasswordTemplate>
        </asp:ChangePassword>

    </div>
</asp:Content>
