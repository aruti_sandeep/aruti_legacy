﻿<%@ Page Title="Send Notification" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="ASendNotification.aspx.vb" Inherits="Aruti_Online_Recruitment.ASendNotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="AChangePassword.css" />
    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        $(document).ready(function () {
            setEvent();
        });

        function setEvent() {

            $("[id*=cboPageSize]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id*=chkSelectAll]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                setEvent()
            })
        }
    </script>

    <div class="card">
        <div class="card-header">
            <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>Notification List
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label></h4>
                </ContentTemplate>
                <Triggers>
<%--                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />--%>
                    <asp:AsyncPostBackTrigger ControlID="grdApplicants" EventName="DataBound" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-md-4">
                    <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
                    <asp:TextBox ID="txtFirstName" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="col-md-4">
                    <asp:Label ID="lblSurname" runat="server" Text="Surname"></asp:Label>
                    <asp:TextBox ID="txtSurname" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="col-md-4">
                    <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                </div>
            </div>


            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">


                        <div class="col-md-4">
                            <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                            <asp:TextBox ID="txtMobile" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblVacancy" runat="server" Text="Vacancy"></asp:Label>
                            <asp:DropDownList ID="cboVacancy" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                            <asp:DropDownList ID="cboStatus" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblPageSize" runat="server" Text="Page Size:"></asp:Label>
                            <asp:DropDownList ID="cboPageSize" runat="server" CssClass="selectpicker form-control" AutoPostBack="false"></asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
                <%--<Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                </Triggers>--%>
            </asp:UpdatePanel>
        </div>

        <div class="card-footer">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btn-block" Text="Search" ValidationGroup="Search" />
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary btn-block"  Text="Reset" ValidationGroup="Search" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_Applicants" runat="server" CssClass="table-responsive overflow-auto" >

                        <asp:GridView ID="grdApplicants" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="True"
                            AllowPaging="True" DataSourceID="dsApplicants" PageSize="15" DataKeyNames="applicantunkid, companyunkid, Comp_Code,vacancyunkid,email,isemailsent">
                            <Columns>
                                <asp:BoundField DataField="applicantunkid" HeaderText="ID" NullDisplayText=" " ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="Comp_Code" HeaderText="Comp. Code"></asp:BoundField>
                                <asp:BoundField DataField="companyunkid" HeaderText="Client ID"></asp:BoundField>
                                <asp:BoundField DataField="Title" HeaderText="Title"></asp:BoundField>
                                <asp:BoundField DataField="ApplicantName" HeaderText="Applicant Name"></asp:BoundField>
                                <asp:BoundField DataField="GenderName" HeaderText="Gender"></asp:BoundField>
                                <asp:BoundField DataField="email" HeaderText="Email"></asp:BoundField>
                                <asp:BoundField DataField="present_mobileno" HeaderText="Mobile No."></asp:BoundField>
                                <asp:BoundField DataField="VacancyName" HeaderText="Applied For"></asp:BoundField>
                                <asp:BoundField DataField="ProfileLastUpdated" HeaderText="Profile Last Updated"></asp:BoundField>
                                <asp:TemplateField HeaderText="Notification Sent" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEmailSent" runat="server" Enabled="false" Checked='<%#Convert.ToBoolean(Eval("isemailsent"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" Checked="false" OnCheckedChanged="chkSelectAll_OnCheckedChanged" AutoPostBack="false" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked="false" AutoPostBack="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:ObjectDataSource ID="dsApplicants" runat="server" SelectMethod="GetApplicantsNotificationList" TypeName="Aruti_Online_Recruitment.clsNotificationSettings" EnablePaging="true"
                            MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetApplicantNotificationCount">
                            <SelectParameters>
                                <asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                                <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:ControlParameter ControlID="cboVacancy" DefaultValue="0" Name="intVacancyUnkId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="txtFirstName" DefaultValue="" Name="strFirstName" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="txtSurname" DefaultValue="" Name="strSurName" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="txtMobile" DefaultValue="" Name="strPresentMobileNo" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="cboStatus" DefaultValue="0" Name="intStatusId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:Parameter Name="intTitleId" Type="Int32" />
                                <asp:Parameter Name="intVacancyMasterTypeId" Type="Int32" />
                                <asp:Parameter Name="startRowIndex" Type="Int32" />
                                <asp:Parameter Name="intPageSize" Type="Int32" />
                                <asp:Parameter Name="strSortExpression" Type="String" />
                                <asp:Parameter Name="intMaxalert" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                    </asp:Panel>
                </ContentTemplate>
               <%-- <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                </Triggers>--%>
            </asp:UpdatePanel>
        </div>

        <div class="card-footer">
            <div class="form-group row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <asp:Button ID="btnSendNotification" runat="server" CssClass="btn btn-primary btn-block" Text="Send Notification" />
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>
    </div>

</asp:Content>


