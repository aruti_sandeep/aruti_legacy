﻿<%@ Page Title="Configuration" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="AConfiguration.aspx.vb" Inherits="Aruti_Online_Recruitment.AConfiguration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        th a {
            color: white;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            $(".switch input").bootstrapSwitch();
            $('.switch input').bootstrapSwitch('onText', 'Yes');
            $('.switch input').bootstrapSwitch('offText', 'No');

            $('#<%= chkVacancyalert.ClientID %>').on('switchChange.bootstrapSwitch', function (event, state) {
                javascript: __doPostBack('<%= chkVacancyalert.UniqueID %>', 'chkVacancyalert_CheckedChanged');
            });

            $('.switch input').bootstrapSwitch('labelWidth', 'auto');
        });

        function integersOnly(obj) {
            obj.value = obj.value.replace(/[^0-9]/g, '');
        }

    </script>

    <div class="card" style="max-width: 80%; margin-left: auto; margin-right: auto;">
        <div class="card-header">
            <h4>Configuration</h4>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-md-4">
                    <asp:Label ID="LblVacancyAlert" runat="server" Text="Vacancy Alert / Max Alerts"></asp:Label>
                    <asp:CheckBox ID="chkVacancyalert" runat="server" CssClass="switch" Style="float: right;" AutoPostBack="true" />
                </div>

                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtMaxAlert" runat="server" TextMode="Number" required="yes" Style="text-align: center" Width="80px" CssClass="form-control" onkeyup="integersOnly(this)"></asp:TextBox>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="chkVacancyalert" EventName="CheckedChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="card-footer">
            <div class="form-group row">
                <div class="col-md-4">
                </div>

                <div class="col-md-4">
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Style="width: 100%;" Text="Save" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
