﻿$(document).ready(function () {

    $$('btnChangePwd').on('click', function () {
        return IsValidate();
    });


});

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {

        $$('btnChangePwd').on('click', function () {
            return IsValidate();
        });


    });
};

function IsValidate() {
    if ($$('txtOTP').val().trim() == '') {
        ShowMessage('Please enter a Verification Code sent to your email.', 'MessageType.Info');
        $$('txtOTP').focus();
        return false;
    }
    else if ($$('txtNewPwd').val().trim() == '') {
        ShowMessage('Please enter New Password.', 'MessageType.Info');
        $$('txtNewPwd').focus();
        return false;
    }
    else if ($$('txtConfirmPwd').val() == '') {
        ShowMessage('Please enter Confirm Password.', 'MessageType.Info');
        $$('txtConfirmPwd').focus();
        return false;
    }
    else if ($$('txtNewPwd').val() != $$('txtConfirmPwd').val()) {
        ShowMessage('The New Password and Confirmation Password must match.', 'MessageType.Info');
        $$('txtConfirmPwd').focus();
        return false;
    }

    return true;
}

