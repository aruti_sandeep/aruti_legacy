﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DateCtrl.ascx.vb" Inherits="Aruti_Online_Recruitment.DateCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<style>
    .ajax__calendar_dayname {
        width:100%;
    }
</style>--%>
<div class="row m-0 p-0">
    <div class="col-md-11 col-sm-11 pl-0" >
        <asp:TextBox runat="server" ID="TxtDate" AutoPostBack="True" class="form-control" />
    <%--<br /> --%>  
    </div>

    <div class="col-md-1 col-sm-1 pl-0" >
        <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/images/PopUpCalendar.gif"
            AlternateText="Click to show calendar" CssClass="width20px height20px " ImageAlign="Top" />
        <asp:ImageButton runat="Server" ID="objbtnDateReminder" ImageUrl="~/images/Reminder_32.png"
        CssClass="width25px height25px"  ImageAlign="Middle" Visible="false" />
</div>
    </div>

<ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" TargetControlID="TxtDate"
    PopupButtonID="Image1" PopupPosition="BottomLeft" Enabled="True" CssClass="cal_Theme1" />
