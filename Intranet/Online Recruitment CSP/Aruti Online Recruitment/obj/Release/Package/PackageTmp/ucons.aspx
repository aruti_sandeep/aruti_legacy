﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ucons.aspx.vb" Inherits="Aruti_Online_Recruitment.ucons" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Under Construction</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    
    <link rel="stylesheet" href="Content/style.css?version=1" />
    <script type="text/javascript" src="WebResource.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnableScriptGlobalization="True">
        </asp:ScriptManager>

        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Panel ID="pnlBlank" runat="server" Visible="true">
                    </asp:Panel>

                    <asp:Panel ID="pnlComingSoon" runat="server" Visible="false" CssClass="text-center" > <%--HorizontalAlign="Center"--%>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="lblComingSoon" runat="server" CssClass="text-success display-3 font-weight-normal" >Coming Soon...</asp:Label> <%--ForeColor="Green" Style="font-size: 80px"--%>
                        <br />
                        <asp:Label ID="lblUcons" runat="server" CssClass="purple h4 font-weight-normal" >This site is in under construction!</asp:Label> <%--ForeColor="Purple" Style="font-size: 25px"--%>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="lblPoweredBy" runat="server" Text="Powered By"  CssClass="h6" ></asp:Label> <%--Style="font-size: 11px;"--%>
                        <br />
                        <br />
                        <img src="Images/logo_aruti.png" class="img-rounded" alt="Aruti" width="100" height="60" />

                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
