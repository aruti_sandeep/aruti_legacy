﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Career.aspx.vb" Inherits="Aruti_Online_Recruitment.Career" %>

<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<meta http-equiv="Content-Security-Policy" content="script-src 'self' 'unsafe-eval' https://www.google.com https://www.gstatic.com; script-src-elem 'self' 'unsafe-inline' https://www.google.com https://www.gstatic.com; 
        style-src 'self' 'unsafe-hashes' 
        'sha256-qFxhV3M2fLROpszOq8NePWwYh7whaaBxJhWiEQG+bhY=' 
        'sha256-qFxhV3M2fLROpszOq8NePWwYh7whaaBxJhWiEQG+bhY=' 
        'sha256-BkxeWXF4Kox6BCSC7HILjrD6WL+j4u/ypLmqh8pvNEc=' 
        'sha256-dG0r4ToMv/tEAZmK/dSAEHH1YbnigbB73lCTVpLn/9U='; 
        frame-src 'self' https://www.google.com https://www.gstatic.com; frame-ancestors 'self'; img-src 'self' data:; font-src 'self'; form-action 'self'; " />--%>
    <title>Career | Jobs</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/fontawesome-all.css" />
    <link rel="stylesheet" href="Content/style.css?version=2" />
    <link rel="stylesheet" href="customtheme.css" />

    <script type="text/javascript" src="WebResource.js"></script>
    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>

    <%--<style>
        .UpdateProgressBG {
            /*background-color: #000000;
            filter: alpha(opacity=50);
            opacity: 0.5;
            z-index: 100100 !important;*/
            background-color: #000000;
            filter: alpha(opacity=60);
            opacity: 0.6;
            z-index: 100001;
            -moz-opacity: 0.6;
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0px;
            left: 0px;
            padding-top: 20%;
            text-align: center;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off" >
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="400" EnablePageMethods="true">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false" >
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div id="MainDivCtrl" class="MaxWidth1200">


            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="">

                        <asp:Panel ID="pnlForm" runat="server">
                            <div class="border mb-1">
                                <div class="card-header">
                                    <div class="float-right">
                                        <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" Visible="false" />
                                    </div>
                                    <h4>
                                        <asp:Label ID="lblHeader" runat="server">Vacancies</asp:Label>&nbsp;<asp:Label ID="objlblCount" runat="server" Text=""></asp:Label>
                                    </h4>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-11">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                                <asp:TextBox ID="txtVacanciesSearch" runat="server" CssClass="form-control" placeholder="Search Vacancy / Company / Skill / Qualification / Responsibility / Job Description" />
                                            </div>

                                        </div>
                                        <div class="col-md-1 p-0">
                                            <asp:Button ID="btnSearchVacancies" runat="server" Text="Search" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>


                                <div class="card-body pb-0">
                                    <asp:DataList ID="dlVaanciesList" runat="server" CssClass="w-100 vacancy" DataKeyField="vacancyid" DataSourceID="odsVacancy">
                                        <ItemTemplate>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="card mb-3">
                                                        <div class="card-header">
                                                            <div class="form-group row mb-0">
                                                                <div class="col-md-12">
                                                                    <div class="">
                                                                        <div class="d-none">
                                                                            <div class="far fa-hand-point-right"></div>
                                                                        </div>
                                                                        <h4>
                                                                            <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("vacancytitle")) %>--%>
                                                                            <asp:Label ID="objlblVacancyTitle" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("vacancytitle"), True) %>'></asp:Label>
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <asp:Panel ID="pnlCompanyName" runat="server" CssClass="f-s-14">
                                                                <div class="d-none"></div>
                                                                <h5 class="mb-0">
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_name")) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_name"), True) %>--%>
                                                                    <asp:Label ID="objlblCompName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_name"), True) %>'></asp:Label>
                                                                    <%--<asp:Label ID="lblAuthCode" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("authentication_code")) %>'></asp:Label>--%>
                                                                    <asp:Label ID="objlblAuthCode" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("authentication_code"), True) %>'></asp:Label>
                                                                </h5>
                                                            </asp:Panel>
                                                        </div>

                                                        <div class="card-body">
                                                            <div class="form-group row" id="divJobLocation" runat="server">
                                                                <div class="col-md-2">
                                                                    <asp:Label ID="lblJobLocation" runat="server" Text="Job Location :" CssClass="font-weight-bold"></asp:Label>                                                                    
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <asp:Label ID="objlblJobLocation" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" id="divRemark" runat="server">
                                                                <div class="col-md-2">
                                                                    <asp:Label ID="lblJobDiscription" runat="server" Text="Job Description" CssClass="font-weight-bold"></asp:Label>
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("remark").Replace(vbCrLf, "<br />").Replace(vbLf, "<br />").Replace(" ", "*")) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("remark"), True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;") %>--%>
                                                                    <asp:Label ID="objlblJobDiscription" runat="server" Text='<%# Eval("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %>'></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row" id="divResponsblity" runat="server">
                                                                <div class="col-md-2">
                                                                    <asp:Label ID="lblResponsblity" runat="server" Text="Responsibility: " CssClass="font-weight-bold"></asp:Label>
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("duties").Replace(vbCrLf, "<br />").Replace(vbLf, "<br />").Replace(" ", "*")) %>--%>
                                                                    <%--<%#  AntiXss.AntiXssEncoder.HtmlEncode(Eval("duties"), True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;") %>--%>
                                                                    <asp:Label ID="objlblResponsblity" runat="server" Text='<%# Eval("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %>'></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row" id="divSkill" runat="server">
                                                                <div class="col-md-2">
                                                                    <asp:Label ID="lblSkill" runat="server" Text="Skill :" CssClass="font-weight-bold"></asp:Label>
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("skill")) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("skill"), True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&amp;", "&") %>--%>
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <asp:Label ID="objlblSkill" runat="server" Text='<%# Eval("skill") %>'></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row" id="divQualification" runat="server">
                                                                <div class="col-md-2">
                                                                    <asp:Label ID="lblQualification" runat="server" Text="Qualification Required: " CssClass="font-weight-bold"></asp:Label>
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Qualification")) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Qualification"), True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&amp;", "&") %>--%>
                                                                    <asp:Label ID="objlblQualification" runat="server" Text='<%# Eval("Qualification") %>'></asp:Label>
                                                                </div>
                                                            </div>


                                                            <div class="form-group row" id="divExp" runat="server">
                                                                <div class="col-md-2" >
                                                                    <asp:Label ID="lblExp" runat="server" Text="Experience :" CssClass="font-weight-bold"></asp:Label>
                                                                    <%--<%# IIf(Eval("experience") <> 0, Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Format(CDec(Eval("experience")) / 12, "###0.0#")) + " Year(s)", Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment("Fresher Can Apply")) %>--%>
                                                                    <%--<%# IIf(Eval("experience") <> 0, AntiXss.AntiXssEncoder.HtmlEncode(Format(CDec(Eval("experience")) / 12, "###0.0#"), True) + " Year(s)", AntiXss.AntiXssEncoder.HtmlEncode("Fresher Can Apply", True)) %>--%>                                                                    
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <asp:Label ID="objlblExp" runat="server" Text='<%# IIf(Eval("experience") <> 0, AntiXss.AntiXssEncoder.HtmlEncode(Format(CDec(Eval("experience")) / 12, "###0.0#"), True) + " Year(s)", AntiXss.AntiXssEncoder.HtmlEncode("Fresher Can Apply", True)) %>'></asp:Label>
                                                                    <%--<br />--%>
                                                                    <asp:Label ID="objlblExpCmt" runat="server" Text='<%# Eval("experience_comment")%>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-6 d-none" id="divNoPosition" runat="server">
                                                                    <asp:Label ID="lblNoPosition" runat="server" Text="No. of Position :" CssClass="font-weight-bold"></asp:Label>
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("noposition")) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("noposition"), True) %>--%>
                                                                    <asp:Label ID="objlblNoPosition" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("noposition"), True) %>'></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row" id="divLang" runat="server">
                                                                <div class="col-md-2">
                                                                    <asp:Label ID="lblLang" runat="server" Text="Preferred Language Skill :" CssClass="font-weight-bold"></asp:Label>                                                                    
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <asp:Label ID="objlblLang" runat="server" Text='<%# Eval("Lang") %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" id="divScale" runat="server">
                                                                <div class="col-md-12">
                                                                    <asp:Label ID="lblScale" runat="server" Text="Scale :" CssClass="font-weight-bold"></asp:Label>
                                                                    <asp:Label ID="objlblScale" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-md-6" id="divOpenningDate" runat="server">
                                                                    <asp:Label ID="lblOpeningDate" runat="server" Text="Job Opening date :" CssClass="font-weight-bold"></asp:Label>
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(CDate(Eval("openingdate")).ToShortDateString()) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("openingdate")).ToShortDateString(), True) %>--%>
                                                                    <asp:Label ID="objlblOpeningDate" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("openingdate")).ToShortDateString(), True) %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-6" id="divClosuingDate" runat="server">
                                                                    <asp:Label ID="lblClosingDate" runat="server" Text="Job closing date :" CssClass="font-weight-bold"></asp:Label>
                                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(CDate(Eval("closingdate")).ToShortDateString()) %>--%>
                                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("closingdate")).ToShortDateString(), True) %>--%>
                                                                    <asp:Label ID="objlblClosingDate" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("closingdate")).ToShortDateString(), True) %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                        <div class="card-footer bg-light pl-1" >
                                            <div class="form-group row mb-1" >
                                                                <div class="col-md-12 text-right">
                                                                    <div>
                                                                        <%--<asp:Label ID="lblIsApplyed" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApplied"))  %>' Visible="false"></asp:Label>--%>
                                                                        <asp:Label ID="objlblIsApplyed" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApplied"), True)  %>' Visible="false"></asp:Label>
                                                        <asp:LinkButton ID="btnApply" runat="server" CommandName="Apply" CssClass="btn btn-primary text-decoration-none" Text="" >
                                                                            <div id="divApplied" runat="server" class="fa fa-check mr-1" visible="false"></div>
                                                                            <asp:Label ID="objlblApplyText" runat="server" Text="Login to Apply"></asp:Label>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>

                                    <asp:Panel ID="pnlNoVacancy" runat="server" Visible="false">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <h5>
                                                    <asp:Label ID="lblNVMsg" runat="server" Text="There are no open vacancies, Click on the login button below if you wish to update your profile."></asp:Label></h5>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <asp:Button ID="btnLoginBioData" runat="server" Text="Login" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <asp:ObjectDataSource ID="odsVacancy" runat="server" SelectMethod="GetApplicantVacancies" TypeName="Aruti_Online_Recruitment.clsSearchJob" EnablePaging="false">

                                <SelectParameters>
                                    <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                    <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="intMasterTypeId" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="intEType" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="blnVacancyType" Type="Boolean" DefaultValue="True" />
                                    <asp:Parameter Name="blnAllVacancy" Type="Boolean" DefaultValue="False" />
                                    <asp:Parameter Name="intDateZoneDifference" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="strVacancyUnkIdLIs" Type="String" DefaultValue="" />
                                    <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="blnOnlyCurrent" Type="Boolean" DefaultValue="True" />
                                </SelectParameters>
                            </asp:ObjectDataSource>

                            <asp:Label ID="lblPositionMsg" runat="server" Text="Position(s)" CssClass="d-none"></asp:Label>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearchVacancies" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>            
        </div>
        <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsSearchJob" _ModuleName="SearchJob" _TargetControl="pnlForm" Visible="false" />
    </form>

    <script type="text/javascript" src="showmessage.js"></script>
    <script src="Browser.js"></script>

</body>
</html>
