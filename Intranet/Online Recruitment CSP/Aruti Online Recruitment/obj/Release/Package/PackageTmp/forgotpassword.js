﻿$(document).ready(function () {

    $$('SubmitButton').on('click', function () {
        return IsValidate();
    });


});

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {

        $$('SubmitButton').on('click', function () {
            return IsValidate();
        });


    });
};

function IsValidate() {
    if ($$('txtUserName').val().trim() == '' ) {
        ShowMessage('Email is compulsory information, it can not be blank. Please enter Email to continue.', 'MessageType.Errorr');
        $$('txtUserName').focus();
        return false;
    }
    else if (IsValidEmail($$('txtUserName').val()) == false) {
        ShowMessage('Sorry, Email is not valid. Please enter valid email.', 'MessageType.Errorr')
        $$('txtUserName').focus();
        return false;
    }
}
function IsValidEmail(strEmail) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(strEmail)) {
        return false;
    } else {
        return true;
    }
}