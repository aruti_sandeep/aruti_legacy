﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ResendLink.aspx.vb" Inherits="Aruti_Online_Recruitment.ResendLink" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Resend Link</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/style.css" />
    <link rel="stylesheet" href="customtheme.css" />

    <script type="text/javascript" src="WebResource.js"></script>
    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>

   <%-- <style>
        .ModalPopupBG {
            /*background-color: #000000;
            filter: alpha(opacity=50);
            opacity: 0.5;
            z-index: 100100 !important;*/
            background-color: #000000;
            filter: alpha(opacity=60);
            opacity: 0.6;
            z-index: 100001;
            -moz-opacity: 0.6;
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0px;
            left: 0px;
            padding-top: 20%;
            text-align: center;
        }
    </style>--%>

</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="400" EnablePageMethods="true" ScriptMode="Release">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="card MaxWidth1422" >

            <div class="card-header">
                <div class="form-group row">
                    <div class="col-md-12 text-center" >
                        <div class="col-md-12">
                            <asp:Image ID="img_shortcuticon" CssClass="img-rounded img_shortcuticon" runat="server" />
                        </div>
                    </div>
                </div>

                <h5 class="text-center">Recruitment Portal</h5>
            </div>

            <%--<asp:PasswordRecovery ID="PasswordRecovery1" runat="server" Style="margin-left: auto; margin-right: auto;" SuccessText="Activation Link has been sent to you on your email. Please check your email to activate your account." TextLayout="TextOnTop" UserNameInstructionText="Enter your email to get activation link." UserNameLabelText="Email:" UserNameTitleText="Didn't receive Activation Link?" QuestionInstructionText="Answer the following question to receive Activation Link." GeneralFailureText="Your attempt to Resend Activation Link was not successful. Please try again.">
                <SubmitButtonStyle CssClass="btn btn-primary" Width="100%" />
                <MailDefinition From="suhail4ezee@gmail.com">
                </MailDefinition>
                <TextBoxStyle CssClass="form-control" />
            </asp:PasswordRecovery>--%>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="card-body">
                        <asp:Panel ID="pnlResend" runat="server">
                            <h6>Didn't receive Activation Link?</h6>
                            <h6>Enter your email to get activation link.</h6>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblEmail" runat="server" Text="Email:" TextMode="Email"></asp:Label>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="ResendLink" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ResendLink" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    <%--<asp:ValidationSummary
                                        HeaderText="You must enter a value in the following fields:"
                                        DisplayMode="BulletList"
                                        EnableClientScript="true"
                                        runat="server" ValidationGroup="ResendLink" />--%>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary w-100" Text="Submit" ValidationGroup="ResendLink" />
                                </div>
                            </div>


                        </asp:Panel>

                        <asp:Panel ID="pnlResult" runat="server" Visible="false">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblResult" runat="server" Text="Activation Link has been sent to you on your email. Please check your email to activate your account." TextMode="Email"></asp:Label>
                                </div>
                                <%-- <div class="col-md-12">
                        <asp:Button ID="btnContinue" runat="server" CssClass="btn btn-primary" Style="width: 100%;" Text="Submit" ValidationGroup="Continue" />
                    </div>--%>
                            </div>
                        </asp:Panel>

                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

            <asp:Panel ID="pnlPoweredBy" runat="server">
                <div class="form-group row">
                    <div class="col-md-12 text-center mt-3" >
                        <asp:Label ID="lblPoweredBy" runat="server" Text="Powered By" CssClass="PoweredBy"></asp:Label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 text-center" >
                        <img src="Images/logo_aruti.png" class="img-rounded" alt="Aruti" width="100" height="60" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </form>

     <script type="text/javascript" src="resendlink.js"></script>
    <script type="text/javascript" src="showmessage.js"></script>
    <script type="text/javascript" src="Browser.js"></script>
</body>
</html>
