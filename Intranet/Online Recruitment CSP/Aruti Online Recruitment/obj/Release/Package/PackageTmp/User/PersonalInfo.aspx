﻿<%@ Page Title="Personal Information" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="PersonalInfo.aspx.vb"
    Inherits="Aruti_Online_Recruitment.PersonalInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>
<%@ Register Src="../Controls/FileUpload.ascx" TagName="flupload" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        function IsValidAttach(objFile) {
            if ($$('ddlBirthCertificateDocType').val() <= 0) {
                ShowMessage($$('lblDocTypeMsg').text(), 'MessageType.Errorr');
                $$('ddlBirthCertificateDocType').focus();
                return false;
            }
            else if ($$('ddlBirthCertificateAttachType').val() <= 0) {
                ShowMessage($$('lblAttachTypeMsg').text(), 'MessageType.Errorr');
                $$('ddlBirthCertificateAttachType').focus();
                return false;
            }
            return true;
        }

        function setEvent() {

            var fupld = document.getElementById('<%= flBirthCertificate.ClientID %>' + '_image_file');
            if (fupld != null)
                fileUpLoadChange(fupld);

            $("[id*=flBirthCertificate]").on('click', function () {
                return IsValidAttach();
            });

            $('.close').on('click', function () {
                $(this).parents('[id*=pnlFileBirthCertificate]').find('span[id*=lblFileNameBirthCertificate]').text('');
                $(this).parents('[id*=pnlFileBirthCertificate]').find('input[id*=hfFileNameBirthCertificate]').val('');
                $(this).parents('[id*=pnlFileBirthCertificate]').hide();
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {

                $$('btnSave').on('click', function () {
                    return IsValidate();
                });

                setEvent();
            });
        };
        
        $(document).ready(function () {

            $$('btnSave').on('click', function () {
                return IsValidate();
            });

            setEvent();
        });

        function IsValidate() {

            if ($$('txtFirstName').val().trim() == '') {
                ShowMessage($$('lblFirstNameMsg').text(), 'MessageType.Errorr');
                $$('txtFirstName').focus();
                return false;
            }
            //'S.SANDEEP |04-MAY-2023| -- START
            //'ISSUE/ENHANCEMENT : A1X-833
            <%--else if ($$('txtSurName').val().trim() == '') {
                ShowMessage($$('lblSurNameMsg').val().trim(), 'MessageType.Errorr');
                $$('txtSurName').focus();
                return false;
            }
            else if ($$('txtOtherName').val().trim() == '' && '<%= Session("MiddleNameMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblOthernameMsg').val().trim(), 'MessageType.Errorr');
                $$('txtOtherName').focus();
                return false;
            }
            //'S.SANDEEP |04-MAY-2023| -- END
            else if ($$('drpGender').val() <= 0 && '<%= Session("GenderMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblGenderMsg').val().trim(), 'MessageType.Errorr');
                $$('drpGender').focus();
                return false;
            }--%>

            else if ($$('txtSurName').val().trim() == '') {
                ShowMessage($$('lblSurNameMsg').text(), 'MessageType.Errorr');
                $$('txtSurName').focus();
                return false;
            }
            else if ($$('txtOtherName').val().trim() == '' && '<%= Session("MiddleNameMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblOthernameMsg').text(), 'MessageType.Errorr');
                $$('txtOtherName').focus();
                return false;
            }
            //'S.SANDEEP |04-MAY-2023| -- END
            else if ($$('drpGender').val() <= 0 && '<%= Session("GenderMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblGenderMsg').text(), 'MessageType.Errorr');
                $$('drpGender').focus();
                return false;
            }
           <%-- else if ('<%= dtBirthdate.GetDate = CDate("01/Jan/1900") %>'.toLowerCase() == 'true' && '<%= Session("BirthDateMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage('Please Enter Valid Birth Date.', 'MessageType.Errorr');
                $$('dtBirthdate').focus();
                return false;
            }
            else if ('<%= dtBirthdate.GetDate >= System.DateTime.Today.Date %>'.toLowerCase() == 'true') {
                ShowMessage('Birth Date should be less than current Date.', 'MessageType.Errorr');
                $$('dtBirthdate').focus();
                return false;
            }--%>
            else if ($$('txtMobileNo').val().trim() == '') {
                ShowMessage($$('lblMobileNoMsg').text(), 'MessageType.Errorr');
                $$('txtMobileNo').focus();
                return false;
            }
            else if ($$('drpNationality').val() <= 0 && '<%= Session("NationalityMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblNationalityMsg').text(), 'MessageType.Errorr');
                $$('drpNationality').focus();
                return false;
            }
            else if ($$('txtEmpCode')[0] != null && $$('txtEmpCode').val().trim() == '') {
                ShowMessage($$('lblEmpCodeMsg').text(), 'MessageType.Errorr');
                $$('txtEmpCode').focus();
                return false;
            }
            //'S.SANDEEP |04-MAY-2023| -- START
            //'ISSUE/ENHANCEMENT : A1X-833
            else if ($$('drpResidency').val() <= 0 &&  <%= Session("CompCode")%>.toLowerCase() == "tra") {
                ShowMessage($$('lblResidencyMsg').text(), 'MessageType.Errorr');
                $$('drpResidency').focus();
                return false;
            }
            else if ($$('txtIndexNo').val().trim() == '' &&  <%=Session("CompCode")%>.toLowerCase() == "tra") {
                ShowMessage($$('lblIndexNoMsg').text(), 'MessageType.Errorr');
                $$('txtIndexNo').focus();
                return false;
            }
            //'S.SANDEEP |04-MAY-2023| -- END
            return true;
        }

    </script>

    <%--<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Aruti</h4>
                </div>
                <div class="modal-body" style="padding:0px;padding-left:15px;">       
                    <div class="col-md-1" style="padding-top: 5px;">
                        <span id="msgicon" class="glyphicon glyphicon-info-sign" style="font-size: 25px; color: #337ab7;"></span>
                    </div>             
                    <div class="col-md-11" style="padding-top: 5px;">
                        <span id="msgbody"></span>
                    </div>
                </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>--%>


    <div class="card">
        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>
                <h4>
                    <asp:Label ID="lblHeaderPersonalInfo" runat="server">Personal Information</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
            </div>

            <div class="card-body">
                <div class="d-none" >
                    <div>
                        <asp:Label ID="lblappcode" runat="server" Text="Application Code:" Visible="False"></asp:Label>
                    </div>
                    <div>
                        <asp:UpdatePanel ID="UpdatePanel56" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAppcode" runat="server" MaxLength="10" Visible="False" CssClass="form-control"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <%--<asp:AsyncPostBackTrigger ControlID="btnCancle" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSaveYes" EventName="Click" />--%>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>




                <div class="form-group row">

                    <div class="col-md-4">
                        <asp:Label ID="lblTitle" runat="server" Text="Title:"></asp:Label>
                        <asp:Button ID="bttnHidden" runat="Server" CssClass="d-none"  />
                        <asp:Button ID="btnHidden1" runat="Server" CssClass="d-none" />
                        <asp:DropDownList ID="DrpTitle" CssClass="selectpicker form-control" runat="server" />

                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblFullName" runat="server" Text="Full Name:" Visible="false"></asp:Label>
                        <asp:Label ID="lblFirstName" runat="server" Text="First Name:" CssClass="required"></asp:Label>

                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" MaxLength="50" ></asp:TextBox>
                        <asp:Label ID="lblFirstNameMsg" runat="server" Text="Please Enter First Name." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFirstName" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" "></cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="None"
                            ControlToValidate="txtFirstName" ErrorMessage="FirstName can not be blank "
                            ValidationGroup="PersonalInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblSurName" runat="server" Text="SurName:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtSurName" runat="server" CssClass="form-control" MaxLength="50" ></asp:TextBox>
                        <asp:Label ID="lblSurNameMsg" runat="server" Text="Please Enter Surname." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtSurName" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" "></cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
                            ControlToValidate="txtSurName" ErrorMessage="Surname can not be blank "
                            ForeColor="White" ValidationGroup="PersonalInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>

                <div class="form-group  row">

                    <div class="col-md-4">
                        <asp:Label ID="lblOthername" runat="server" Text="Other Name:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtOtherName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="lblOthernameMsg" runat="server" Text="Please Enter Other Name." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtOtherName" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" "></cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="rfvOtherName" runat="server" Display="None"
                            ControlToValidate="txtOtherName" ErrorMessage="Other Name can not be blank "
                            ForeColor="White" ValidationGroup="PersonalInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="col-md-4">
                        <asp:Label ID="lblGender" runat="server" Text="Gender:" CssClass="required"></asp:Label>
                        <asp:DropDownList ID="drpGender" CssClass="selectpicker form-control" runat="server" />
                        <asp:Label ID="lblGenderMsg" runat="server" Text="Please Select Gender." CssClass="d-none"></asp:Label>                       
                        <%--<asp:RequiredFieldValidator ID="rfvGender" runat="server" Display="None" ControlToValidate="drpGender" InitialValue="0"
                            ErrorMessage="Please Select Gender" ValidationGroup="PersonalInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>

                    <div class="col-md-4 col-sm-12 col-md-offset-0">
                        <asp:Label ID="lblBirthDate" runat="server" Text="Birth Date:"></asp:Label>
                        <uc2:DateCtrl ID="dtBirthdate" runat="server" AutoPostBack="false" CssClass="form-control" />
                         <asp:Label ID="lblBirthDateMsg" runat="server" Text="Please Enter Valid Birth Date." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblBirthDateMsg2" runat="server" Text="Birth Date should be less than current Date." CssClass="d-none"></asp:Label>
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                        <asp:Label ID="lblMobileNo" runat="server" Text="Mobile:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblMobileNoMsg" runat="server" Text="Please Enter Mobile No." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtMobileNo" FilterType="Numbers, Custom" ValidChars="\+"></cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobileNo"
                            Display="None" ErrorMessage="Mobile No. Can Not be Blank" SetFocusOnError="True"
                            ValidationGroup="PersonalInfo"></asp:RequiredFieldValidator>--%>
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblAlterNativeno" runat="server" Text="Alternative No:"></asp:Label>
                        <asp:TextBox ID="txtAlterNativeno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div id="divEmpCode" class="col-md-4">

                        <asp:Label ID="lblEmpCode" runat="server" Text="Emp. Code:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtEmpCode" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="lblEmpCodeMsg" runat="server" Text="Please Enter Employee Code." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtEmpCode" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" "></cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="ReqValidEmpCode" runat="server" Display="None"
                            ControlToValidate="txtEmpCode" ErrorMessage="Employee Code Can Not be Blank "
                            ValidationGroup="PersonalInfo" ForeColor="White" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>


                </div>


                <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                <%--'ISSUE/ENHANCEMENT : A1X-833--%>
                <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                <div class="form-group row">


                    <div class="col-md-4">
                        <asp:Label ID="lblnin" runat="server" Text="Nida Number:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtNin" runat="server" CssClass="form-control" MaxLength="200" Enabled="false"></asp:TextBox>
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lbltin" runat="server" Text="Tin Number:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtTin" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblNidaMobile" runat="server" Text="Nida Mobile:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtNidaMobile" runat="server" CssClass="form-control" MaxLength="200" Enabled="false"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                        <asp:Label ID="lblResidency" runat="server" Text="Residency:" CssClass="required"></asp:Label>
                        <asp:DropDownList ID="drpResidency" runat="server" CssClass="selectpicker form-control" />
                        <asp:Label ID="lblResidencyMsg" runat="server" Text="Please Select Residency." CssClass="d-none"></asp:Label>
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblIndexNo" runat="server" Text="Index Number:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtIndexNo" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                        <asp:Label ID="lblIndexNoMsg" runat="server" Text="Please Enter Index Number." CssClass="d-none"></asp:Label>
                    </div>


                    <div class="col-md-4">
                        <asp:Label ID="lblphoneNumber" runat="server" Text="Phone Number:"></asp:Label>
                        <asp:TextBox ID="txtphoneNumber" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <% End If%>
                <%--'S.SANDEEP |04-MAY-2023| -- END--%>

                <div class="form-group row">

                    <div class="col-md-4">
                        <asp:Label ID="lblNationality" runat="server" Text="Nationality:" CssClass="required"></asp:Label>
                        <asp:DropDownList ID="drpNationality" runat="server" CssClass="selectpicker form-control" />
                        <asp:Label ID="lblNationalityMsg" runat="server" Text="Please Select Nationality." CssClass="d-none"></asp:Label>
                        <%--<asp:RequiredFieldValidator ID="rfvNationality" runat="server" Display="None" ControlToValidate="drpNationality" InitialValue="0"
                            ErrorMessage="Please Select Nationality" ValidationGroup="PersonalInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>

                    <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : A1X-833--%>
                    <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                    <div class="col-md-4">
                        <asp:Label ID="lblvillage" runat="server" Text="Village:"></asp:Label>
                        <asp:TextBox ID="txtvillage" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                    </div>
                    <%Else %>
                    <div class="col-md-4">
                    </div>
                    <% End If%>
                    <%--'S.SANDEEP |04-MAY-2023| -- END--%>
                    <div class="col-md-4">
                    </div>
                </div>

                <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                <%--'ISSUE/ENHANCEMENT : A1X-833--%>
                <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                <div class="form-group row">
                    <div class="col-md-12">
                        <asp:Image ID="imgApplPhoto" runat="server" Width="160px" Height="200px" />
                    </div>
                </div>
                <% End If%>
                <%--'S.SANDEEP |04-MAY-2023| -- END--%>

                <%--'Pinkal (30-Sep-2023) -- Start--%>
                <%--'(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".%>--%>

               <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                <div class="form-group row">
                    <div class="card-body">
                        <asp:Panel ID="pnlAttachMsg" runat="server">
                            <asp:Label ID="lblUpload" runat="server" Font-Bold="true" Text="Select file (Image / PDF) (All file size should not exceed more than 5 MB.)" CssClass="d-block"> 
                            </asp:Label>
                        </asp:Panel>

                        <div class="col-md-12">
                            <asp:Panel ID="pnlBirthCertificate" runat="server" CssClass="form-group row attachdoc">
                                <div class="col-md-3">
                                    <asp:Label ID="LblBirthCertificateDocType" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                    <asp:DropDownList ID="ddlBirthCertificateDocType" runat="server" AutoPostBack="true" CssClass="selectpicker form-control"></asp:DropDownList>
                                </div>
                                <div class="col-md-3">
                                    <asp:Label ID="LblBirthCertificateAttachType" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                    <asp:DropDownList ID="ddlBirthCertificateAttachType" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                </div>
                                <div class="col-md-2 mt-3">
                                    <uc1:flupload id="flBirthCertificate" runat="server" maxsizekb="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click"/> 
                                </div>
                                <div class="col-md-4 mt-2">
                                    <asp:Panel ID="pnlFileBirthCertificate" runat="server" CssClass="card rounded">
                                        <div class="row no-gutters m-0">
                                            <div class="col-md-3 text-center pt-3 fileext">
                                                <asp:Label ID="lblFileExtBirthCertificate" runat="server" Text="PDF"></asp:Label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="card-body py-1 pr-2">
                                                    <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                    <h6 class="card-title m-0">
                                                        <asp:Label ID="lblFileNameBirthCertificate" runat="server" Text=""></asp:Label></h6>
                                                    <p class="card-text">
                                                        <small class="text-muted">
                                                            <asp:Label ID="lblDocSizeBirthCertificate" runat="server" Text="0.0 MB"></asp:Label></small>
                                                    </p>
                                                    <asp:HiddenField ID="hfDocSizeBirthCertificate" runat="server" Value="" />
                                                    <asp:HiddenField ID="hfFileNameBirthCertificate" runat="server" Value="" />
                                                    <asp:HiddenField ID="hfBirthCertificateAttachId" runat="server" Value="" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
               <% End If  %>
                <%--'Pinkal (30-Sep-2023) -- End--%>



                <div class="d-none" >
                    <div class="d-none w-100" >
                        <asp:UpdatePanel ID="UpdatePanel73" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Image ID="Image1" runat="server" CssClass="selectImage"
                                    AlternateText="Select Image" />
                            </ContentTemplate>
                            <Triggers>
                                <%--<asp:PostBackTrigger ControlID="btnSaveImg" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancle" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSaveYes" EventName="Click" />--%>
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel74" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:FileUpload ID="imageUpload" runat="server" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSaveImg" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnSaveImg" runat="server" CssClass="btndefault" Text="Save Images" />
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtApplicatunkid" runat="server" CssClass="width15px" ></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSaveImg" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 text-center" >
                       <%-- <asp:ValidationSummary ID="vs"
                            HeaderText="You must enter a value in the following fields:"
                            DisplayMode="BulletList"
                            EnableClientScript="true"
                            runat="server" ValidationGroup="PersonalInfo" Style="color: red" />--%>
                    </div>
                </div>

                <%--<div class="form-group row">
            <div class="col-md-12" style="text-align: center;">
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
            </div>
        </div>--%>
            </div>

            <div class="card-footer">
                <div class="form-group row d-flex justify-content-center">
                   <%-- <div class="col-md-4">
                    </div>--%>
                    <div class="col-md-4">
                        <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="btn btn-primary w-100"   />
                        <asp:Label ID="lblSaveMsg" runat="server" Text="Personal Information Saved Successfully! \n\n Click Ok to continue with other section(s)." CssClass="d-none"></asp:Label>
                    </div>
                    <%--<div class="col-md-4">
                    </div>--%>
                </div>
            </div>

            <asp:Label ID="lblDocTypeMsg" runat="server" CssClass="d-none" Text="Please select Document Type."></asp:Label>
            <asp:Label ID="lblAttachTypeMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
            <asp:Label ID="lblAttachImageMsg" runat="server" CssClass="d-none" Text="Please select proper Image file or PDF file."></asp:Label>
            <asp:Label ID="lblAttachSizeMsg" runat="server" CssClass="d-none" Text="Sorry,You cannot upload file greater than #maxsize# MB."></asp:Label>
            <asp:Label ID="lblAttachMsg" runat="server" CssClass="d-none" Text="Sorry, #attachmenttype# attachment is mandatory."></asp:Label>

        </asp:Panel>
        <asp:HiddenField ID="hfCancelLang" runat="server" />
        <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicant" _ModuleName="PersonalInfo" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
    </div>

    <script type="text/javascript">
        var dv = document.getElementById('divEmpCode');
        <%--var s = '<%=  Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Session("Vacancy")) %>';--%>
        var s = '<%=  AntiXss.AntiXssEncoder.HtmlEncode((Session("Vacancy")), True) %>';

        if (s == 'Int') {
            //dv.style.display = "block";
            $(dv).show();
        }
        else {
            //dv.style.display = "none";
            $(dv).hide();
        }
    </script>
</asp:Content>
