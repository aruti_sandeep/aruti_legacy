﻿<%@ Page Title="Home : Aruti" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="UserHome.aspx.vb" Inherits="Aruti_Online_Recruitment.UserHome" %>

<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link rel="stylesheet" href="../scripts/bootstrap.js" />
        <link rel="stylesheet" href="../scripts/bootstrap.min.js" />--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">
        function pageLoad(sender, args) {
            //$('.selectpicker').selectpicker({
            //    liveSearch: true,
            //    maxOptions: 1
            //});
        }
    </script>

    <div class="card" id="divMessage" runat="server">
        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <h4>&nbsp;
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
            </div>

            <div class="card-body">
                <h5><b>
                    <asp:Label ID="lblWelcome" runat="server">WELCOME TO OUR RECRUITMENT PORTAL.</asp:Label></b></h5>
                <h5><b>
                    <asp:Label ID="lblFillProfile" runat="server">As a registered member of our portal you can now fill your profile for job application</asp:Label></b></h5>
                <h6><b>
                    <asp:Label ID="lblStep1" runat="server">Step 1: Complete your Profile.</asp:Label></b><asp:Label ID="lblStep1_1" runat="server">Please complete ALL of the required fields of the forms found in the left-hand menu of this page.</asp:Label></h6>
                <h6><b>
                    <asp:Label ID="lblStep2" runat="server">Step 2: Apply job</asp:Label></b>&nbsp;<asp:Label ID="lblStep2_1" runat="server">by Click on the</asp:Label>&nbsp;<b><asp:Label ID="lblStep2_2" runat="server">search job</asp:Label></b>&nbsp;<asp:Label ID="lblStep2_3" runat="server">at the top of the page or on left side menu.</asp:Label></h6>
                <ul typeof="square" class="m-l-6-PER">
                    <li>
                        <h6>
                            <asp:Label ID="lblSelectVacancy" runat="server">Select a vacancy of interest.</asp:Label></h6>
                    </li>
                    <li>
                        <h6>
                            <asp:Label ID="lblReadJob" runat="server">Read the job requirements thoroughly.</asp:Label></h6>
                    </li>
                    <li>
                        <h6>
                            <asp:Label ID="lblClick" runat="server">Click</asp:Label>&nbsp;<b><asp:Label ID="lblClickApply" runat="server">apply</asp:Label></b></h6>
                    </li>
                </ul>

                <%  If CBool(Session("issendjobconfirmationemail")) = True Then %>
                <h6>
                    <asp:Label ID="lblReceipt" runat="server">Receipt Of online applications will be confirmed via the email address you used To register.</asp:Label></h6>
                <%End If %>
                <h6>
                    <asp:Label ID="lblClickOn" runat="server">Click on the</asp:Label>&nbsp;<b><asp:Label ID="lblClickPreview" runat="server">Preview</asp:Label></b>&nbsp;<asp:Label ID="lblOptiononTop" runat="server">option on top of the page or</asp:Label>&nbsp;<b><asp:Label ID="lblPreviewResume" runat="server">Preview Resume</asp:Label></b>&nbsp;<asp:Label ID="lblOnLeft" runat="server">on left hand side menu bar to review your CV before submission and verify that your email address is correct.</asp:Label></h6>
                <br />


                <h5>
                    <asp:Label ID="lblIncomplete" runat="server">Your Profile is incomplete.</asp:Label></h5>
                <h6>
                    <asp:Label ID="lblFillFollowing" runat="server">Please fill following details to complete your profile.</asp:Label></h6>
                <%--<h5><i><b style="color:red;font-size:20px;vertical-align:middle;">*</b> Mandatory informations are indicated on left menu with asterisk (<b style="color:#00FF3E;font-size:20px;vertical-align:middle;">*</b>).</i></h5><br />--%>
                <h6><i><b>
                    <%--<asp:Label ID="objlblNote" runat="server">Note: Mandatory informations are indicated on left menu with asterisk (<span class=" h5 font-weight-bold text-danger align-middle" >*</span>).</asp:Label>--%>
                    <asp:Label ID="lblNote" runat="server">Note: Mandatory informations are indicated on left menu with asterisk (*).</asp:Label>
                </b></i></h6>
                <br />

                <asp:Label ID="objlblRequired" runat="server" Text="" Visible="false"></asp:Label>
            </div>

        </asp:Panel>

    </div>
    <asp:HiddenField ID="hfCancelLang" runat="server" />
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicant" _ModuleName="UserHome" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
</asp:Content>
