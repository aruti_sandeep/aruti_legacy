﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Preview.aspx.vb" Inherits="Aruti_Online_Recruitment.Preview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Preview Resume</title>

    <link rel="stylesheet" href="../Content/bootstrap.css" />
    <link rel="stylesheet" href="Preview.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="shortcut icon" href="../Images/logo_32.ico" type="image/x-icon" />
    <script type="text/javascript" src="../scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="../scripts/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../Content/bootstrap-dialog.css" />
    <script type="text/javascript" src="../scripts/bootstrap-dialog.js"></script>
    <script type="text/javascript" src="../showmessage.js"></script>

 <%--   <style>
        
    </style>--%>

    <script type="text/javascript">

        //function ShowMessage(message, messagetype) {
        //    var cssclass;
        //    var msgtype = "";
        //    switch (messagetype) {
        //        case 0:
        //            cssclass = 'alert-success'
        //            msgtype = "Succcess"
        //            break;
        //        case 1:
        //            cssclass = 'alert-danger'
        //            msgtype = "Error"
        //            break;
        //        case 3:
        //            cssclass = 'alert-warning'
        //            msgtype = "Warning"
        //            break;
        //        default:
        //            cssclass = 'alert-info'
        //            msgtype = "Info"
        //    }
        //    $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
        //}

        //function ShowConfirm(strMessage, id, strType) {
        //    if (strMessage == null)
        //        strMessage = 'Are you sure you want to Delete?';

        //    if (strType == null)
        //        strType = BootstrapDialog.TYPE_PRIMARY;

        //    //var types = [BootstrapDialog.TYPE_DEFAULT,
        //    //       BootstrapDialog.TYPE_INFO,
        //    //        BootstrapDialog.TYPE_PRIMARY,
        //    //        BootstrapDialog.TYPE_SUCCESS,
        //    //        BootstrapDialog.TYPE_WARNING,
        //    //        BootstrapDialog.TYPE_DANGER];

        //    var strTitle = 'Aruti';
        //    //var pstbk = $(id)[0].pathname.replace("__doPostBack('", "").replace("','')", ""); //pathname retunring blank in firefox
        //    var pstbk = $(id)[0].href.replace("javascript:__doPostBack('", "").replace("','')", "");

        //    BootstrapDialog.confirm({
        //        title: strTitle,
        //        message: strMessage,
        //        type: strType, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        //        closable: false, // <-- Default value is false
        //        draggable: true, // <-- Default value is false
        //        btnCancelLabel: 'No', // <-- Default value is 'Cancel',
        //        btnOKLabel: 'Yes', // <-- Default value is 'OK',
        //        btnOKClass: 'btn-primary', // <-- If you didn't specify it, dialog type will be used,
        //        callback: function (result) {
        //            // result will be true if button was click, while it will be false if users close the dialog directly.
        //            //return result;
        //            if (result) {
        //                //alert('Yup.');
        //                //return true;
        //                //javascript: __doPostBack('ctl00$ContentPlaceHolder1$grdviewSkill$ctl04$btndelete', '');
        //                //javascript: pstbk;
        //                //javascript: __doPostBack(pstbk, ''); // firefox error : TypeError: access to strict mode caller function is censored
        //                setTimeout(function () { __doPostBack(pstbk, ''); }, 1);
        //            } else {
        //                //alert('Nope.');
        //                //return false;
        //                BootstrapDialog.closeAll();
        //            }
        //        }
        //    });
        //}
    </script>

</head>
<body>
    <form id="form1" runat="server" autocomplete="off" class="fontfamily" >
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:Panel ID="pnlPreview" runat="server">

            <div id="page" size="A4">
                <asp:Panel ID="pnlContactInfo" runat="server" >
                <table class="w-100">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblName" runat="server" CssClass="font-weight-bold h5" >Name</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblEmailMobile" runat="server">Email: , Mobile:</asp:Label>
                            <asp:Label ID="lblMobileNo" runat="server" Text="Mobile:" Visible="false"></asp:Label>
                            <asp:Label ID="lblFax" runat="server" Text="Fax:" Visible="false"></asp:Label>
                            <asp:Label ID="lblTelno" runat="server" Text="Tel. No:" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblAddress" runat="server">Address</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblAddress2" runat="server">Address2</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <hr class="mt-0 mb-0 border-top border-dark"  />
                        </td>
                    </tr>
                </table>
                </asp:Panel>

                <asp:Panel ID="divPersonalInfo" runat="server" CssClass="sectionpanel">
                    <table class="w-100">
                        <tr>
                            <td colspan="4" class="sectionheader">
                                <asp:Label ID="lblHeaderPersonalInfo" runat="server">Personal Information</asp:Label></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">
                                <asp:Label ID="lblFullName" runat="server" Text="Name:"></asp:Label></td>
                            <td colspan="3">
                                <asp:Label ID="txtFullName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="w-30 font-weight-bold">
                                <asp:Label ID="lblGender" runat="server" Text="Gender:"></asp:Label></td>
                            <td colspan="3">
                                <asp:Label ID="txtGender" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">
                                <asp:Label ID="lblBirthDate" runat="server" Text="Birth Date:"></asp:Label></td>
                            <td colspan="3">
                                <asp:Label ID="txtBirthDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <asp:Panel ID="pnlMarital" runat="server">
                        <tr>
                            <td class="w-25 font-weight-bold">
                                <asp:Label ID="lblMaritalStatus" runat="server" Text="Marital Status:"></asp:Label></td>
                            <td class="w-25">
                                <asp:Label ID="txtMaritalStatus" runat="server"></asp:Label>
                            </td>
                            <td class="w-25 font-weight-bold">
                                <asp:Label ID="lblMarriedDate" runat="server" Text="Married Date:" ></asp:Label>
                            </td>
                            <td class="w-25">
                                <asp:Label ID="txtMarriedDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        </asp:Panel>
                        <tr>
                            <td class="font-weight-bold">
                                <asp:Label ID="lblNationality" runat="server" Text="Nationality:"></asp:Label></td>
                            <td colspan="3">
                                <asp:Label ID="txtNationality" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <asp:Panel ID="pnlLanguagesKnown" runat="server">
                        <tr>
                            <td class="font-weight-bold">
                                <asp:Label ID="lblLanguagesKnown" runat="server" Text="Languages Known:"></asp:Label></td>
                            <td colspan="3">
                                <asp:Label ID="txtLanguagesKnown" runat="server"></asp:Label>
                            </td>
                        </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlMotherTongue" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblMotherTongue" runat="server" Text="Mother Tongue:"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtMotherTongue" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlImpaired" runat="server" Visible="false">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblImpaired" runat="server" Text="Impaired:"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtImpaired" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlCurrentSalary" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblCurrent_salary" runat="server" Text="Current Salary :"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtCurrent_salary" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlExpectedSalary" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblExpected_salary" runat="server" Text="Expected Salary :"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtExpected_salary" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlExpectedBenefits" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblExpected_benefits" runat="server" Text="Expected Benefits:"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtExpected_benefits" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlWillingToRelocate" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblWilling_to_relocate" runat="server" Text="Willing to Relocate"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtWilling_to_relocate" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlWillingToTravel" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblWilling_to_travel" runat="server" Text="Willing to Travel"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtWilling_to_travel" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlNoticePeriodDays" runat="server">
                            <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblNotice_period_days" runat="server" Text="Notice Period (in Days):"></asp:Label></td>
                                <td colspan="3">
                                    <asp:Label ID="txtNotice_period_days" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlEarliestPossibleStartDate" runat="server">
                            <tr>
                                <td class="font-weight-bold">Earliest Possible Start Date:</td>
                                <td colspan="3">
                                    <asp:Label ID="lblEarliestPossibleStartDate" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlComments" runat="server">
                            <tr>
                                <td class="font-weight-bold">Comments:</td>
                                <td colspan="3">
                                    <asp:Label ID="lblComments" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlPermanentAddress" runat="server" >
                        <tr>
                                <td class="font-weight-bold">
                                    <asp:Label ID="lblPermanentAddress" runat="server">Permanent Address</asp:Label></td>
                            <td colspan="3">
                                <asp:Label ID="txtPermanentAddress" runat="server"></asp:Label>
                                <asp:Label ID="lblPrmTelNo" runat="server" Text="Tel. No:" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3">
                                <asp:Label ID="txtPermanentAddress2" runat="server"></asp:Label>
                                <asp:Label ID="lblPrmfax" runat="server" Text="Fax:" Visible="false"></asp:Label>
                            </td>
                        </tr>
                            </asp:Panel>
                    </table>
                </asp:Panel>

                <asp:Panel ID="divSkill" runat="server" CssClass="sectionpanel">
                    <table class="w-100">
                        <tr>
                            <td class="w-15 v-align-t font-weight-bold">
                                <asp:Label ID="lblKeySkill" runat="server">Key Skills</asp:Label></td>
                            <td class="w-85">
                                <asp:Label ID="txtSkill" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="divQualification" runat="server" CssClass="sectionpanel">
                    <div class="sectionheader m-b-10">
                        <asp:Label ID="lblQualification" runat="server">Qualification</asp:Label>
                    </div>
                    <asp:DataGrid ID="dlQualification" runat="server" AutoGenerateColumns="false" CssClass="w-100"
                        HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                        <Columns>
                            <asp:BoundColumn DataField="Qualification" HeaderText="Qualification" ItemStyle-CssClass="w-30" FooterText="colhQualification" />
                            <asp:BoundColumn DataField="institute" HeaderText="Institution" ItemStyle-CssClass="w-20" FooterText="colhInstitute" />
                            <asp:BoundColumn DataField="resultcode" HeaderText="Result" ItemStyle-CssClass="w-15" FooterText="colhResultcode" />
                            <asp:BoundColumn DataField="" HeaderText="Duration" ItemStyle-CssClass="w-20" FooterText="colhDuration" />
                            <asp:BoundColumn DataField="gpacode" HeaderText="GPA Point" ItemStyle-CssClass="w-15 text-right" FooterText="colhGpacode" />
                        </Columns>
                    </asp:DataGrid>
                </asp:Panel>
                <asp:Panel ID="divExperience" runat="server" CssClass="sectionpanel">
                    <div class="sectionheader">
                        <asp:Label ID="lblWorkExperience" runat="server">Work Experience</asp:Label>
                    </div>
                    <asp:Label ID="lblOfficePhone" runat="server" Text="Office Phone:" Visible="false"></asp:Label>
                    <asp:DataList ID="dlExperience" runat="server" CssClass="w-100" CellPadding="0" CellSpacing="0">
                        <ItemTemplate>
                            <div class="d-flex">
                                <div class="float-left w-25">
                                    <asp:Label ID="lblCompNam" runat="server" Text="Company:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <%--<asp:Label ID="lblCompanyName" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company"))  %>'></asp:Label>--%>
                                    <asp:Label ID="txtCompanyName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company"), True)  %>'></asp:Label>
                                </div>
                            </div>
                            <asp:Panel ID="drEmployer" CssClass="d-flex" runat="server" Visible='<%# IIf(GetEmployerNameTelNo(CType(Container.DataItem, Data.DataRowView)).ToString.Trim = "", False, True) %>'>
                                <div class="float-left w-25">
                                    <asp:Label ID="lblEmployer" runat="server" Text="Employer:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <asp:Label ID="txtEmployer" runat="server" Text='<%#  GetEmployerNameTelNo(CType(Container.DataItem, Data.DataRowView)) %>'></asp:Label>
                                </div>
                            </asp:Panel>
                            <div class="d-flex">
                                <div class="float-left w-25">
                                    <asp:Label ID="lblDesignation" runat="server" Text="Designation:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <%--<asp:Label ID="lblDesignation" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Designation"))  %>'></asp:Label>--%>
                                    <asp:Label ID="txtDesignation" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Designation"), True)  %>'></asp:Label>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="float-left w-25">
                                    <asp:Label ID="lblDuration" runat="server" Text="Duration:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <asp:Label ID="txtDuration" runat="server" Text='<%# GetDuration(CType(Container.DataItem, Data.DataRowView))  %>'></asp:Label>
                                </div>
                            </div>
                            <%--<asp:Panel ID="drResponsibility" runat="server" class="d-flex" Visible='<%# IIf(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Responsibility")).ToString.Trim = "", False, True) %>'>--%>
                            <asp:Panel ID="drResponsibility" runat="server" class="d-flex" Visible='<%# IIf(AntiXss.AntiXssEncoder.HtmlEncode(Eval("Responsibility"), True).ToString.Trim = "", False, True) %>'>
                                <div class="float-left w-25">
                                    <asp:Label ID="lblRes" runat="server" Text="Responsibility:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <%--<asp:Label ID="lblResponsibility" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Responsibility"))  %>'></asp:Label>--%>
                                    <asp:Label ID="txtRes" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Responsibility"), True)  %>'></asp:Label>
                                </div>
                            </asp:Panel>
                            <%--<asp:Panel ID="drAchievement" runat="server" class="d-flex" Visible='<%# IIf(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("achievements")).ToString.Trim = "", False, True) %>'>--%>
                            <asp:Panel ID="drAchievement" runat="server" class="d-flex" Visible='<%# IIf(AntiXss.AntiXssEncoder.HtmlEncode(Eval("achievements"), True).ToString.Trim = "", False, True) %>'>
                                <div class="float-left w-25">
                                    <asp:Label ID="lblAchievement" runat="server" Text="Achievement:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <%--<asp:Label ID="lblAchievement" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("achievements"))  %>'></asp:Label>--%>
                                    <asp:Label ID="txtAchievement" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("achievements"), True)  %>'></asp:Label>
                                </div>
                            </asp:Panel>
                            <%--<asp:Panel ID="drLeavingReason" runat="server" class="d-flex" Visible='<%# IIf(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("leavingReason")).ToString.Trim = "", False, True) %>'>--%>
                            <asp:Panel ID="drLeavingReason" runat="server" class="d-flex" Visible='<%# IIf(AntiXss.AntiXssEncoder.HtmlEncode(Eval("leavingReason"), True).ToString.Trim = "", False, True) %>'>
                                <div class="float-left w-25">
                                    <asp:Label ID="lblLeavingReson" runat="server" Text="Leaving Reason:" CssClass="font-weight-bold"></asp:Label>
                                </div>
                                <div class="w-75">
                                    <%--<asp:Label ID="lblleavingReason" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("leavingReason"))  %>'></asp:Label>--%>
                                    <asp:Label ID="txtleavingReason" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("leavingReason"), True)  %>'></asp:Label>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <hr class="m-t-10 m-b-10 hr-border" />
                        </SeparatorTemplate>
                    </asp:DataList>
                </asp:Panel>
                <asp:Panel ID="pnlOtherInfo" runat="server" CssClass="sectionpanel">
                    <table class="w-100">
                        <tr>
                            <td class="sectionheader">
                                <asp:Label ID="lblOtherInformation" runat="server">Other Information</asp:Label></td>
                        </tr>
                    </table>

                    <asp:Panel ID="divMembership" runat="server" CssClass="w-100 p-0">
                        <table class="w-100">
                            <tr>
                                <td class="w-25 v-align-t font-weight-bold">
                                    <asp:Label ID="lblMembership" runat="server" Text="Membership:"></asp:Label></td>
                                <td class="w-75">
                                    <asp:Label ID="txtMembership" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                    <asp:Panel ID="divAchievements" runat="server" CssClass="w-100 p-0">
                        <table class="w-100">
                            <tr>
                                <td class="v-align-t w-25 font-weight-bold">
                                    <asp:Label ID="lblAchievement" runat="server" Text="Achievements:"></asp:Label></td>
                                <td class="w-75">
                                    <asp:Label ID="txtAchievements" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </asp:Panel>

                    <asp:Panel ID="divJournalResearchPaper" runat="server" CssClass="w-100 p-0">
                        <table class="w-100">
                            <tr>
                                <td class="v-align-t w-25 font-weight-bold">
                                    <asp:Label ID="lblJournalsResearchPaper" runat="server" Text="Links / Journals / Research Papers:"></asp:Label></td>
                                <td class="w-75">
                                    <asp:Label ID="txtJournalResearchPaper" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel ID="pnlRefrence" runat="server" CssClass="sectionpanel">
                    <div class="sectionheader m-b-10">
                        <asp:Label ID="lblReference" runat="server">Reference</asp:Label>
                    </div>
                    <asp:Label ID="lblRefMobile" runat="server" Text="Mobile:" Visible="false"></asp:Label>
                    <asp:Label ID="lblRefTelNo" runat="server" Text="Tel. No.:" Visible="false"></asp:Label>
                    <asp:DataGrid ID="dlRefrences" runat="server" AutoGenerateColumns="false" CssClass="w-100"
                        HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridItem">
                        <Columns>
                            <asp:BoundColumn DataField="name" HeaderText="Name" ItemStyle-CssClass="w-40 v-align-t" FooterText="colhRefrenceName" />
                            <asp:BoundColumn DataField="" HeaderText="Contact Details" ItemStyle-CssClass="w-60" FooterText="colhContactDetail" />
                        </Columns>
                    </asp:DataGrid>
                </asp:Panel>
            </div>

        </asp:Panel>

        <asp:Panel ID="pnlBlank" runat="server" Visible="false" CssClass="MaxWidth422px mx-auto m-t-5-PER" >
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        </asp:Panel>
    </form>
</body>
</html>
