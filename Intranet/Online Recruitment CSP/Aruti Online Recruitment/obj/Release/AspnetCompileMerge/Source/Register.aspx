﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Register.aspx.vb" Inherits="Aruti_Online_Recruitment.Register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Register</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-select.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="stylesheet" href="Content/style.css" />
    <link rel="stylesheet" href="Content/fontawesome-all.css" />
    <link rel="stylesheet" href="customtheme.css" />

    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <script type="text/javascript" src="WebResource.js"></script>
    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/umd/popper.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>


    <%--<style>
        .row {
            margin-bottom: -15px;
        }
    </style>--%>

    <script type="text/javascript">
        /*'S.SANDEEP |24-JUN-2023| -- START*/
        /*'ISSUE/ENHANCEMENT : Sprint 2023-13*/
        $(document).ready(function () {
            $('#form1').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
        });
        /*'S.SANDEEP |24-JUN-2023| -- END*/
    </script>

</head>
<body>


    <form id="form1" runat="server" autocomplete="off">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="400" EnablePageMethods="true" ScriptMode="Release">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="card MaxWidth1422">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
                <ContentTemplate>

                    <asp:Panel ID="pnlMain" runat="server">
                        <div class="card-header">
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <div class="col-md-12">
                                        <asp:Image ID="img_shortcuticon" CssClass="img-rounded img_shortcuticon" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <h5 class="text-center">Recruitment Portal</h5>

                            <h4 class="text-center">Sign Up for Your New Account</h4>
                        </div>

                        <div class="card-body">
                            <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                            <%--<div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" CssClass="required">First Name: </asp:Label>
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblMiddleName" runat="server" AssociatedControlID="txtMiddleName">Middle Name:</asp:Label>
                                    <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" CssClass="required">Last Name: </asp:Label>
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="required">E-mail: </asp:Label>
                                    <asp:TextBox ID="UserName" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblGender" runat="server" AssociatedControlID="drpGender" CssClass="required">Gender: </asp:Label>
                                    <asp:DropDownList ID="drpGender" CssClass="selectpicker form-control" runat="server" />                                   
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblBirthDate" runat="server" AssociatedControlID="dtBirthdate" CssClass="required">Birth Date: </asp:Label>
                                    <uc2:DateCtrl ID="dtBirthdate" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblMobileNo" runat="server" AssociatedControlID="txtMobileNo" CssClass="required">Mobile: </asp:Label>
                                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="required">Password: </asp:Label>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword" CssClass="required">Confirm Password: </asp:Label>
                                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:TextBox ID="Email" runat="server" Visible="False"></asp:TextBox>
                                    <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question" CssClass="required">Security Question: </asp:Label>
                                    <asp:TextBox ID="Question" runat="server" CssClass="form-control"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" CssClass="required">Security Answer: </asp:Label>
                                    <asp:TextBox ID="Answer" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>                                    
                                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </div>
                            </div>--%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="NINLabel" runat="server" AssociatedControlID="txtNinNumber" CssClass="required">National ID Number:</asp:Label>
                                    <asp:TextBox ID="txtNinNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="NINMobileLabel" runat="server" AssociatedControlID="txtNINMobile" CssClass="required">Mobile Registered With Nida:</asp:Label>
                                    <asp:TextBox ID="txtNINMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <% End If%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" CssClass="required">First Name: </asp:Label>
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <% End If%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblMiddleName" runat="server" AssociatedControlID="txtMiddleName">Middle Name:</asp:Label>
                                    <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <% End If%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" CssClass="required">Last Name: </asp:Label>
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <% End If%>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="required">E-mail: </asp:Label>
                                    <asp:TextBox ID="UserName" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                </div>
                            </div>
                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblGender" runat="server" AssociatedControlID="drpGender" CssClass="required">Gender: </asp:Label>
                                    <asp:DropDownList ID="drpGender" CssClass="selectpicker form-control" runat="server" />
                                </div>
                            </div>
                            <% End If%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblBirthDate" runat="server" AssociatedControlID="dtBirthdate" CssClass="required">Birth Date: </asp:Label>
                                    <uc2:DateCtrl ID="dtBirthdate" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <% End If%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblMobileNo" runat="server" AssociatedControlID="txtMobileNo" CssClass="required">Mobile: </asp:Label>
                                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <% End If%>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="required">Password: </asp:Label>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword" CssClass="required">Confirm Password: </asp:Label>
                                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:TextBox ID="Email" runat="server" Visible="False"></asp:TextBox>
                                    <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question" CssClass="required">Security Question: </asp:Label>
                                    <asp:TextBox ID="Question" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <% End If%>

                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" CssClass="required">Security Answer: </asp:Label>
                                    <asp:TextBox ID="Answer" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </div>
                            </div>
                            <% End If%>

                            <%--'S.SANDEEP |04-MAY-2023| -- END--%>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Button ID="btnRegister" runat="server" Text="Register your Account" CssClass="btn btn-primary width100" />
                                </div>
                            </div>
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </div>

                    </asp:Panel>

                    <asp:Panel ID="pnlComplete" runat="server" Visible="false">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblMsg1" runat="server" Text="Your account has been successfully created."></asp:Label>
                                    <br />
                                    <asp:Label ID="lblMsg2" runat="server" Text="Activation link has been sent to you on your Email"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblMsg3" runat="server" Text="Please Activate your account before login."></asp:Label>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <div class="form-group row">
                                <div class="col-md-12 text-right">
                                    <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="btn btn-primary"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--<asp:CreateUserWizard ID="CreateUserWizard1" runat="server" DisableCreatedUser="True" MailDefinition-From="suhail4ezee@gmail.com" Style="margin-left: auto; margin-right: auto; width: 100%;" UserNameLabelText="Your E-mail:" UserNameRequiredErrorMessage="Email is required." CompleteSuccessText="Your account has been successfully created and Activation link has been sent to you on your Email. <BR>Please Activate your account before login."
                        PasswordRegularExpression="@\'(?:.{7,})(?=(.*\d){1,})(?=(.*\W){1,})" PasswordRegularExpressionErrorMessage="Your password must be 7 characters long, and contain at least one number and one special character."
                        InvalidPasswordErrorMessage="Password should have a minimum length of {0} characters. It should also include at least {1} special character such as ! # $ % ( ) * + , - . / \ : ; = ? @ [ ] ^ _ ` | ~ " RequireEmail="false" DuplicateUserNameErrorMessage="This Email is already registered. Please enter a different Email." ContinueDestinationPageUrl="~/Login.aspx" CreateUserButtonText="Register your Account">

                        <CreateUserButtonStyle CssClass="btn btn-primary" Width="100%" />
                        <MailDefinition From="suhail4ezee@gmail.com"></MailDefinition>
                        <WizardSteps>
                            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                <ContentTemplate>


                                    <div class="card-body">
                                        <table style="width: 100%; margin-bottom: 10px;">

                                           
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td>

                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            
                                            <tr>
                                                <td align="center">
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="color: Red;">
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="color: Red;">
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:CreateUserWizardStep>
                            <asp:CompleteWizardStep runat="server" />
                        </WizardSteps>
                    </asp:CreateUserWizard>--%>


                    <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                    <cc1:ModalPopupExtender ID="popupNidaOtp" runat="server" CancelControlID="btnOtpClose"
                        PopupControlID="pnlNidaOtp" TargetControlID="hfNidaOtp" BackgroundCssClass="ModalPopupBG">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlNidaOtp" runat="server" CssClass="modal1" Style="display: none;" TabIndex="-1" role="dialog">
                        <div class="modal-dialog modal-lg width260px">
                            <div class="modal-content">
                                <div id="header" class="modal-header ">
                                    <h6>
                                        <asp:HiddenField ID="hfNidaOtp" runat="server" />
                                        <asp:Label ID="lblOtpVerification" runat="server" Text="Passcode Verification"></asp:Label>
                                    </h6>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div id="Div1" runat="server"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Label ID="NidaOtpLabel" runat="server" AssociatedControlID="txtNidaOtpNumber" CssClass="required" Text="Enter One time passcode"></asp:Label>
                                            <asp:TextBox ID="txtNidaOtpNumber" runat="server" CssClass="form-control" onkeyup="EnableDisableButton(this)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer text-right">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnValidateOtp" runat="server" Text="Verify" CssClass="btn btn-primary" Enabled="false" />
                                            <asp:Button ID="btnOtpClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupNidaQuestions" runat="server" CancelControlID="btnpopNidaClose"
                        PopupControlID="pnlNidaQuestions" TargetControlID="hfNidaQuestions" BackgroundCssClass="ModalPopupBG">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlNidaQuestions" runat="server" CssClass="modal1" Style="display: none;" TabIndex="-1" role="dialog">
                        <div class="modal-dialog modal-lg width80vw">
                            <div class="modal-content">
                                <div id="abc" class="modal-header ">
                                    <h6>
                                        <asp:HiddenField ID="hfNidaQuestions" runat="server" />
                                        <asp:Label ID="lblNidaHeader" runat="server" Text="Answer the Question"></asp:Label>
                                    </h6>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Label ID="objlblSt1" runat="server"></asp:Label>
                                            <asp:Label ID="objlblSt2" runat="server"></asp:Label>
                                            <asp:Label ID="objlblSt3" runat="server"></asp:Label>
                                            <asp:Label ID="objlblSt4" runat="server"></asp:Label>
                                            <asp:Label ID="objlblSt5" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Label ID="lblNidaQuestion" runat="server" CssClass="required"></asp:Label>
                                            <asp:HiddenField ID="hdfNidaQestionTag" runat="server" />
                                            <asp:TextBox ID="txtNidaQueryAnswer" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer text-right">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnVerify" runat="server" Text="Submit" CssClass="btn btn-primary" Enabled="false" />
                                            <asp:Button ID="btnpopNidaClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--'S.SANDEEP |04-MAY-2023| -- END--%>
                </ContentTemplate>

            </asp:UpdatePanel>
            <asp:Panel ID="pnlPoweredBy" runat="server">
                <div class="form-group row">
                    <div class="col-md-12 text-center mt-3">
                        <asp:Label ID="lblPoweredBy" runat="server" Text="Powered By" CssClass="PoweredBy"></asp:Label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 text-center">
                        <img src="Images/logo_aruti.png" class="img-rounded" alt="Aruti" width="100" height="60" />
                    </div>
                </div>
            </asp:Panel>
        </div>
        <asp:HiddenField ID="hflocationorigin" runat="server" Value="" />
        <%--'S.SANDEEP |04-MAY-2023| -- START--%>
        <asp:HiddenField ID="hfPassword" runat="server" Value="" />
        <input type="hidden" id="hdfCompCode" value="<%=Session("CompCode")%>" />
        <%--'S.SANDEEP |04-MAY-2023| -- END--%>
    </form>

    <script type="text/javascript" src="register.js"></script>
    <script type="text/javascript" src="showmessage.js"></script>
    <script type="text/javascript" src="Browser.js"></script>
</body>
</html>
