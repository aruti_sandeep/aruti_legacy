﻿<%@ Page Title="Change Password" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="AChangePassword.aspx.vb" Inherits="Aruti_Online_Recruitment.AChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="AChangePassword.css" />
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnableScriptGlobalization="True">
    </asp:ScriptManager>--%>
    <script type="text/javascript">

        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }


        $(document).ready(function () {
            $$('ChangePasswordPushButton').on('click', function () {
                return IsValidate();
            });
            
        });

        function IsValidate() {
            
            if ($$('UserName').val().trim() == '') {
                ShowMessage('Email is required.', 'MessageType.Errorr');
                $$('UserName').focus();
                return false;
            }
           
            else if (emailTest($$('UserName').val())) {
                ShowMessage('Email address is not valid.', 'MessageType.Errorr');
                $$('UserName').focus();
                return false;
            }

            else if ($$('CurrentPassword').val().trim() == '') {
                ShowMessage('Password is required.', 'MessageType.Errorr');
                $$('CurrentPassword').focus();
                return false;
            }

            else if ($$('NewPassword').val().trim() == '') {
                ShowMessage('New Password is required.', 'MessageType.Errorr');
                $$('NewPassword').focus();
                return false;
            }

            else if ($$('ConfirmNewPassword').val().trim() == '') {
                ShowMessage('Confirm New Password is required.', 'MessageType.Errorr');
                $$('ConfirmNewPassword').focus();
                return false;
            }

            else if ($$('ConfirmNewPassword').val().trim() !== $$('NewPassword').val().trim()) {
                ShowMessage('The Confirm New Password must match the New Password entry.', 'MessageType.Errorr');
                $$('ConfirmNewPassword').focus();
                return false;
            }
            return true;
        }

        function emailTest(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return !regex.test(email);
        }


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
            })
        }
    </script>
    <div class="card main-card">

        <div class="card-header">
            <h4>Change Password</h4>
        </div>

        <asp:ChangePassword ID="ChangePassword1" runat="server" CssClass="w-100" PasswordLabelText="Old Password:" SuccessPageUrl="~/Login.aspx" DisplayUserName="True"  CancelDestinationPageUrl="~/User/UserHome.aspx">
            <ChangePasswordTemplate>                
                <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-12">
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                        <asp:TextBox ID="UserName" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                           <%-- <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="UserName"
                                ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" ValidationGroup="ChangePassword1" Display="None"></asp:RegularExpressionValidator>--%>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label>
                        <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                        <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                    </div>
                </div>

                    <%--<div class="form-group row">
                    <div class="col-md-12">
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" CssClass="d-none" ErrorMessage="" ValidationGroup="ChangePassword1"></asp:CompareValidator>
                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                    </div>--%>

                   <%-- <div class="form-group row">
                    <div class="col-md-12" style="text-align: center;">
                        <asp:Literal ID="Literal1" runat="server" EnableViewState="False"></asp:Literal>
                        <asp:ValidationSummary
                            HeaderText="You must enter a value in the following fields:"
                            DisplayMode="BulletList"
                            EnableClientScript="true"
                            runat="server" ValidationGroup="ChangePassword1" Style="color: red" />
                    </div>
                    </div>--%>
                </div>

                <div class="card-footer">
                <div class="form-group row">
                    <div class="col-md-12">
                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangePassword1" CssClass="btn btn-primary btn-block"  />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="btn btn-primary btn-block" />
                    </div>
                </div>
                </div>

            </ChangePasswordTemplate>
        </asp:ChangePassword>

    </div>
</asp:Content>
