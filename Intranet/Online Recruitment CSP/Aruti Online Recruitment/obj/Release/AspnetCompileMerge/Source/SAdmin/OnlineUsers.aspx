﻿<%@ Page Title="Online Users" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="OnlineUsers.aspx.vb" Inherits="Aruti_Online_Recruitment.OnlineUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="card">
        <div class="card-header">
            <h4>Online Users</h4>

            Number of Users Online:
        <asp:Label ID="UsersOnlineLabel" runat="Server" />
        </div>

        <div class="card-body">
            <%--<asp:panel id="NavigationPanel" visible="false" runat="server">
    <table border="0"  cellpadding="3" cellspacing="3">
      <tr>
        <td style="width:100">Page <asp:Label id="CurrentPageLabel" runat="server" />
            of <asp:Label id="TotalPagesLabel" runat="server" /></td>
        <td style="width:60"><asp:LinkButton id="PreviousButton" Text="< Prev"
                            OnClick="PreviousButton_OnClick" runat="server" /></td>
        <td style="width:60"><asp:LinkButton id="NextButton" Text="Next >"
                            OnClick="NextButton_OnClick" runat="server" /></td>
      </tr>
    </table>
  </asp:panel>
            <asp:datagrid id="UserGrid" runat="server" CssClass="table"
            cellpadding="2" cellspacing="1"
            gridlines="Both">

            <HeaderStyle BackColor="darkblue" ForeColor="white" />
  </asp:datagrid>
            --%>

            <asp:Panel ID="pnl_link" runat="server" CssClass="table-responsive minheight300" > <%--Style="min-height: 300px;"--%>
                <asp:GridView ID="grdLink" runat="server" AutoGenerateColumns="false" CssClass="table" ShowHeaderWhenEmpty="true"
                    AllowPaging="true" PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="UserName" HeaderText="User Name" />
                        <%--<asp:BoundField DataField="LastLogin" HeaderText="Last Login Date" />--%>
                        <asp:BoundField DataField="CreationDate" HeaderText="Creation Date" />
                        <asp:BoundField DataField="LastActive" HeaderText="Last Activity Date" />
                        <asp:BoundField DataField="IsApproved" HeaderText="Is Approved" />
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </div>


    </div>
</asp:Content>
