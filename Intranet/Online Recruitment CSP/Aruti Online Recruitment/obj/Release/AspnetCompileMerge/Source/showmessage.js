﻿function ShowMessage(strMessage, mtype) {
    var strType = '';
    if (strMessage == null)
        strMessage = 'Transaction saved successfully!';

    if (mtype == null)
        mtype = 'MessageType.Info';

    var strTitle = 'Aruti';

    switch (mtype) {
        case 'MessageType.Errorr':
            strType = BootstrapDialog.TYPE_DANGER;
            break;
        case 'MessageType.Success':
            strType = BootstrapDialog.TYPE_INFO;
            break;
        case 'MessageType.Warning':
            strType = BootstrapDialog.TYPE_WARNING;
            break;
        default:
            strType = BootstrapDialog.TYPE_INFO;
            break;
    }


    BootstrapDialog.show({
        type: strType,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        title: strTitle,
        message: strMessage,
        buttons: [{
            label: 'Ok',
            cssClass: 'btn-primary',
            action: function (dialogItself) {
                dialogItself.close();
            }
        }]
    });

}

function $$(id, context) {
    var el = $("#" + id, context);
    if (el.length < 1)
        el = $("[id$=_" + id + "]", context);
    return el;
}

$(document).ready(function () {
    if ($$('hflocationorigin')[0] != undefined)
        $$('hflocationorigin').val(window.location.origin);

    if ($$('hflocationhref')[0] != undefined)
        $$('hflocationhref').val(window.location.href);
});