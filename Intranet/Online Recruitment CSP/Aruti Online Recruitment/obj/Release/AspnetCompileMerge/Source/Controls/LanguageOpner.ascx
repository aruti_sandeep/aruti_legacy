﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LanguageOpner.ascx.vb" Inherits="Aruti_Online_Recruitment.LanguageOpner" %>

<asp:LinkButton ID="objlnkLanguageOpner" runat="server" OnClick="Click" CssClass="float-right" ToolTip="Change Language" ValidationGroup="LanguageOpner">
    <i class="fa fa-language"></i>
</asp:LinkButton>
