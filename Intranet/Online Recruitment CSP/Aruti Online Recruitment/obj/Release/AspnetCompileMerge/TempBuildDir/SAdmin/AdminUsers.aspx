﻿<%@ Page Title="Admin Users" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="AdminUsers.aspx.vb" Inherits="Aruti_Online_Recruitment.AdminUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }


		$(document).ready(function () {
			setevents();
		});


		function setevents() {
			$("[id*=btndelete]").on('click', function () {
				if (!ShowConfirm("Are you sure you want to Delete this User?", this))
					return false;
				else
					return true;
			});

			$("[id*=cboCompany]").on('change', function () {
				__doPostBack($(this).get(0).id, 0)
			});
		}

		var prm = Sys.WebForms.PageRequestManager.getInstance();
		if (prm != null) {
			prm.add_endRequest(function (sender, e) {
				setevents();
			});
		};


    </script>

    <div class="card">

        <div class="card-header">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>Admin Users
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                    </h4>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="grdLink" EventName="DataBound" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblCompany" runat="server" Text="Company:"></asp:Label>
                            <asp:DropDownList ID="cboCompany" runat="server" CssClass="selectpicker form-control"/> <%-- AutoPostBack="true"--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary w-100" Text="Search" ValidationGroup="Search" /> <%--Style="width: 100%;"--%>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnl_link" runat="server" CssClass="table-responsive" >  <%--Style="min-height: 300px;"--%>
                    <asp:GridView ID="grdLink" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                        AllowPaging="true" DataSourceID="dsLink" PageSize="10" DataKeyNames="activationlinkunkid, Comp_Code, companyunkid, email, UserId">

                        <Columns>
                            <asp:TemplateField HeaderText="Admin User">
                                <ItemTemplate>
                                    <div class="form-group row ml-0 mr-0">  <%--style="margin-left: 0px; margin-right: 0px;"--%>
                                        <div class="col-md-1">
                                            <asp:Label ID="objlblClientID" runat="server" Text="ID" CssClass ="font-weight-bold" ></asp:Label>  
                                            <%--<asp:Label ID="objtxtClientID" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("companyunkid")) %>'></asp:Label>--%>
                                            <asp:Label ID="objtxtClientID" runat="server" CssClass="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("companyunkid"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblCode" runat="server" Text="Code" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objtxtCode" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Comp_Code")) %>'></asp:Label>--%>
                                            <asp:Label ID="objtxtCode" runat="server" CssClass ="d-block"  Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Comp_Code"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="objlblEmail" runat="server" Text="Email" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objtxtEmail" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("email")) %>'></asp:Label>--%>
                                            <asp:Label ID="objtxtEmail" runat="server" CssClass="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("email"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="objlblCreated" runat="server" Text="Created" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objtxtCreated" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CreateDate")) %>'></asp:Label>--%>
                                            <asp:Label ID="objtxtCreated" runat="server" CssClass ="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("CreateDate"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="objlblLastLogin" runat="server" Text="Last Login" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objtxtLastLogin" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("LastLoginDate")) %>'></asp:Label>--%>
                                            <asp:Label ID="objtxtLastLogin" runat="server" CssClass="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("LastLoginDate"), True) %>'></asp:Label> <%-- Style="display: block;"--%>
                                        </div>
                                    </div>
                                    <div class="form-group row ml-0 mr-0" >  <%--style="margin-left: 0px; margin-right: 0px;"--%>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblIsApproved" runat="server" Text="Approved" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objtxtIsApproved" runat="server" style="display:block;" Enabled="false" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApproved"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objtxtIsApproved" runat="server" CssClass="d-block" Enabled="false" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApproved"), True)) %>'></asp:CheckBox>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblCanViewErrorLog" runat="server" Text="Can View Error Log" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objtxtCanViewErrorLog" runat="server" style="display:block;" Enabled="false" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CanViewErrorLog"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objtxtCanViewErrorLog" runat="server" CssClass="d-block" Enabled="false" Checked='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanViewErrorLog"), True) %>'></asp:CheckBox>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblCanTruncateErrorLog" runat="server" Text="Can Truncate Error Log" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objtxtCanTruncateErrorLog" runat="server" style="display:block;" Enabled="false" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CanTruncateErrorLog"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objtxtCanTruncateErrorLog" runat="server" CssClass="d-block" Enabled="false" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanTruncateErrorLog"), True)) %>'></asp:CheckBox> <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblCanShrinkDataBase" runat="server" Text="Can Shrink DataBase" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objtxtCanShrinkDataBase" runat="server" style="display:block;" Enabled="false" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CanShrinkDataBase"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objtxtCanShrinkDataBase" runat="server" CssClass="d-block"  Enabled="false" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanShrinkDataBase"), True)) %>'></asp:CheckBox>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblCanRunFullScript" runat="server" Text="Can Run Full Script" CssClass ="font-weight-bold"></asp:Label>
                                            <asp:CheckBox ID="objtxtCanRunFullScript" runat="server" CssClass="d-block" Enabled="false" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanRunFullScript"), True)) %>'></asp:CheckBox> <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objlblCanRunAlterScript" runat="server" Text="Can Run Alter Script" CssClass ="font-weight-bold"></asp:Label>
                                            <asp:CheckBox ID="objtxtCanRunAlterScript" runat="server" CssClass="d-block" Enabled="false" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanRunAlterScript"), True)) %>'></asp:CheckBox> <%--Style="display: block;"--%>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="form-group row ml-0 mr-0" >  <%--style="margin-left: 0px; margin-right: 0px;"--%>
                                        <div class="col-md-1">
                                            <asp:Label ID="objelblClientID" runat="server" Text="ID" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objetxtClientID" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("companyunkid")) %>'></asp:Label>--%>
                                            <asp:Label ID="objetxtClientID" runat="server" CssClass="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("companyunkid"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblCode" runat="server" Text="Code" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objetxtCode" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Comp_Code")) %>'></asp:Label>--%>
                                            <asp:Label ID="objetxtCode" runat="server" CssClass="d-block"  Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Comp_Code"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="objelblEmail" runat="server" Text="Email" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objetxtEmail" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("email")) %>'></asp:Label>--%>
                                            <asp:Label ID="objetxtEmail" runat="server" CssClass="d-block"   Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("email"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="objelblCreated" runat="server" Text="Created" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objetxtCreated" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CreateDate")) %>'></asp:Label>--%>
                                            <asp:Label ID="objetxtCreated" runat="server" CssClass="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("CreateDate"), True) %>'></asp:Label>  S<%--tyle="display: block;"--%>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="objelblLastLogin" runat="server" Text="Last Login" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:Label ID="objetxtLastLogin" runat="server" style="display:block;" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("LastLoginDate")) %>'></asp:Label>--%>
                                            <asp:Label ID="objetxtLastLogin" runat="server" CssClass="d-block" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("LastLoginDate"), True) %>'></asp:Label>  <%--Style="display: block;"--%>
                                        </div>
                                    </div>
                                    <div class="form-group row ml-0 mr-0" >  <%--style="margin-left: 0px; margin-right: 0px;"--%>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblIsApproved" runat="server" Text="Approved" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objetxtIsApproved" runat="server" style="display:block;" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApproved"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objetxtIsApproved" runat="server" CssClass="d-block" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApproved"), True)) %>'></asp:CheckBox>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblCanViewErrorLog" runat="server" Text="Can View Error Log" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objetxtCanViewErrorLog" runat="server" style="display:block;" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CanViewErrorLog"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objetxtCanViewErrorLog" runat="server" CssClass="d-block" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanViewErrorLog"), True)) %>'></asp:CheckBox> <%-- Style="display: block;" --%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblCanTruncateErrorLog" runat="server" Text="Can Truncate Error Log" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objetxtCanTruncateErrorLog" runat="server" style="display:block;" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CanTruncateErrorLog"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objetxtCanTruncateErrorLog" runat="server" CssClass="d-block" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanTruncateErrorLog"), True)) %>'></asp:CheckBox>  <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblCanShrinkDataBase" runat="server" Text="Can Shrink DataBase" CssClass ="font-weight-bold"></asp:Label>
                                            <%--<asp:CheckBox ID="objetxtCanShrinkDataBase" runat="server" style="display:block;" Checked='<%#CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("CanShrinkDataBase"))) %>'></asp:CheckBox>--%>
                                            <asp:CheckBox ID="objetxtCanShrinkDataBase" runat="server" CssClass="d-block" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanShrinkDataBase"), True)) %>'></asp:CheckBox><%-- Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblCanRunFullScript" runat="server" Text="Can Run Full Script" CssClass ="font-weight-bold"></asp:Label>
                                            <asp:CheckBox ID="objetxtCanRunFullScript" runat="server" CssClass="d-block" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanRunFullScript"), True)) %>'></asp:CheckBox> <%--Style="display: block;"--%>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="objelblCanRunAlterScript" runat="server" Text="Can Run Alter Script" CssClass ="font-weight-bold"></asp:Label>
                                            <asp:CheckBox ID="objetxtCanRunAlterScript" runat="server" CssClass="d-block" Checked='<%#CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("CanRunAlterScript"), True)) %>'></asp:CheckBox> <%-- Style="display: block;"--%>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:CommandField ButtonType="Link" ShowEditButton="true" />--%>
                            <asp:TemplateField HeaderText="Edit">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="<i aria-hidden='true' class='fa fa-edit'></i>"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                    <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Delete">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="btndelete" runat="server" CommandName="Delete" Text="<i aria-hidden='true' class='fa fa-trash'></i>"></asp:LinkButton>  <%--OnClientClick='if(!ShowConfirm("Are you sure you want to Delete this User?", this)) return false; else return true;'--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:ObjectDataSource ID="dsLink" runat="server" SelectMethod="GetAdminUsers" TypeName="Aruti_Online_Recruitment.clsSACommon" EnablePaging="true"
                        MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetAdminUsersCount">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                            <asp:Parameter Name="strUserId" Type="String" DefaultValue="" />
                            <asp:Parameter Name="strComp_Code" Type="String" DefaultValue="" />
                            <asp:ControlParameter ControlID="cboCompany" DefaultValue="0" Name="intCompanyunkid" PropertyName="SelectedValue" Type="Int32" />
                            <asp:Parameter Name="startRowIndex" Type="Int32" />
                            <asp:Parameter Name="intPageSize" Type="Int32" />
                            <asp:Parameter Name="strSortExpression" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <%--<asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="form-group row">
                    <div class="col-md-4">
                    </div>

                    <div class="col-md-4">
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-primary" Style="width: 100%" Text="Delete Applicants"
                            ValidationGroup="DeleteApplicants" />
                    </div>

                    <div class="col-md-4">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>

</asp:Content>
