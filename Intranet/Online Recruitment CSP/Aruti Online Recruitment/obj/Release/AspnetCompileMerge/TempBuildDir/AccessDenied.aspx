﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AccessDenied.aspx.vb" Inherits="Aruti_Online_Recruitment.AccessDenied" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
    <script type="text/javascript" src="WebResource.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

    <div>
            <p></p>
            <asp:Label ID="Label1" runat="server"
                Text="Access Denied!" CssClass="font-weight-bold h3 text-danger"></asp:Label>
        <br />
        <br />
            <asp:Label ID="Label2" runat="server" CssClass="font-weight-bold"
            Text="Sorry! You are not authorized to accecss this page."></asp:Label>
        <br />
        <br />
            <asp:Label ID="lblReason" runat="server" CssClass="font-italic text-danger"></asp:Label>

    </div>
    </form>
</body>
</html>
