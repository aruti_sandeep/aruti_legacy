﻿<%@ Page Title="Test Email" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="TestEmail.aspx.vb" Inherits="Aruti_Online_Recruitment.TestEmail1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        $(document).ready(function () {
            setEvent();
        });

        $(document).ready(function () {
            debugger;
            $$('btnTestEmail').on('click', function () {
                return IsValidate();
            });
            $$('btnUpdate').on('click', function () {
                return IsValidate();
            });
        });

        function IsValidate() {
            
            if ($$('txtReceiverName').val().trim() == '') {
                ShowMessage('Receiver Email cannot be blank.', 'MessageType.Errorr');
                $$('txtReceiverName').focus();
                return false;
            }
           
            else if (emailTest($$('txtReceiverName').val())) {
                ShowMessage('Enter Valid Email ID.', 'MessageType.Errorr');
                $$('txtReceiverName').focus();
                return false;
            }

            else if ($$('txtSenderAddress').val().trim() == '') {
                ShowMessage('Sender Email address is not valid.', 'MessageType.Errorr');
                $$('txtSenderAddress').focus();
                return false;
            }

            else if ($$('txtPassword').val().trim() == '') {
                ShowMessage('Password cannot be blank.', 'MessageType.Errorr');
                $$('txtPassword').focus();
                return false;
            }

            else if ($$('txtMailServer').val().trim() == '') {
                ShowMessage('SMTP Mail Server cannot be blank.', 'MessageType.Errorr');
                $$('txtMailServer').focus();
                return false;
            }
            else if ($$('txtMailServerPort').val().trim() == '') {
                ShowMessage('Mail Server Post cannot be blank.', 'MessageType.Errorr');
                $$('txtMailServerPort').focus();
                return false;
            }
            else if ($$('txtEWS_URL').val().trim() == '') {
                ShowMessage('EWS URL cannot be blank.', 'MessageType.Errorr');
                $$('txtEWS_URL').focus();
                return false;
            }
            else if ($$('txtEWS_Domain').val().trim() == '') {
                ShowMessage('EWS Domain cannot be blank.', 'MessageType.Errorr');
                $$('txtEWS_Domain').focus();
                return false;
            }
            
            return true;
        }

        function emailTest(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return !regex.test(email);
        }

        function setEvent() {

            $("[id*=cboCompany]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                setEvent()
            })
        }
    </script>

    <div class="card">

        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlForm" runat="server">
                    <div class="card-header">
                       <%-- <div class="float-right">
                            <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                        </div>--%>
                        <asp:Panel ID="pnlCompany" runat="server" Visible="false">
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <asp:Label ID="lblCompany" runat="server" Text="Company:"></asp:Label>
                                            <asp:DropDownList ID="cboCompany" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />

                                        </ContentTemplate>
                                        <%--<Triggers>
                                <asp:AsyncPostBackTrigger ControlID="chkOnlyActive" EventName="CheckedChanged" />
                            </Triggers>--%>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="col-md-8">
                                    <%--<asp:CheckBox ID="chkOnlyActive" runat="server" Text="Only Active" Checked="true" AutoPostBack="true" />--%>
                                    <asp:Label ID="lblKey" runat="server" Text="Auth. Key:"></asp:Label>
                                    <asp:TextBox ID="txtKey" runat="server" CssClass="form-control" ReadOnly="true" Text=""></asp:TextBox>
                                </div>

                                <%--<div class="col-md-4">
                        </div>--%>
                            </div>
                        </asp:Panel>

                        <h4>Email Settings</h4>
                    </div>

                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-6">
                                <asp:RadioButton ID="radSMTP" runat="server" Text=" SMTP" Checked="true" GroupName="EmailType" AutoPostBack="true" Visible="false" />
                            </div>

                            <div class="col-md-6">
                                <asp:RadioButton ID="radEWS" runat="server" Text=" Microsoft Exchange Web Service (EWS)" GroupName="EmailType" AutoPostBack="true" Visible="false" />
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-4">
                                <%--<asp:Label ID="lblSenderName" runat="server" Text="Sender Name:"></asp:Label>--%>
                                <asp:Label ID="lblReceiverName" runat="server" Text="Receiver Email:"></asp:Label>
                                <asp:TextBox ID="txtReceiverName" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rtfSenderName" runat="server" Display="None"
                                    ControlToValidate="txtReceiverName" ErrorMessage="Receiver Email cannot be blank "
                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtReceiverName"
                                    ErrorMessage="Receiver Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="TestEmail" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lblSenderAddress" runat="server" Text="Sender Email:"></asp:Label>
                                <asp:TextBox ID="txtSenderAddress" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                    ControlToValidate="txtSenderAddress" ErrorMessage="Sender Email cannot be blank "
                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                <%--<asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="txtSenderAddress"
                                        ErrorMessage="Sender Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="TestEmail" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                            </div>

                            <div class="col-md-4">
                                <%--<asp:Label ID="lblReference" runat="server" Text="Reference:"></asp:Label>
                <asp:TextBox ID="txtReference" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="PersonalInfo"></asp:TextBox>--%>
                                <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label>
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="50" TextMode="Password" ValidationGroup="TestEmail"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
                                    ControlToValidate="txtPassword" ErrorMessage="Password cannot be blank "
                                    ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            </div>

                        </div>

                        <asp:MultiView ID="mvEmail_Type" runat="server" ActiveViewIndex="0">

                            <asp:View ID="vwSMTP" runat="server">

                                <div class="form-group row">

                                    <div class="col-md-4">
                                        <asp:Label ID="lblMailServer" runat="server" Text="Mail Server (SMTP):"></asp:Label>
                                        <asp:TextBox ID="txtMailServer" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="rfvMailServer" runat="server" Display="None"
                                            ControlToValidate="txtMailServer" ErrorMessage="SMTP Mail Server cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                    </div>

                                    <div class="col-md-4">
                                        <asp:Label ID="lblMailServerPort" runat="server" Text="Mail Server Port:"></asp:Label>
                                        <asp:TextBox ID="txtMailServerPort" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="rfvMailServerPort" runat="server" Display="None"
                                            ControlToValidate="txtMailServerPort" ErrorMessage="Mail Server Post cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                        <cc1:FilteredTextBoxExtender ID="ftbMailServerPort" runat="server" TargetControlID="txtMailServerPort" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                    </div>

                                    <div class="col-md-3">
                                        <asp:Label ID="lblSenderName" runat="server" Text="Sender Name:"></asp:Label>
                                        <asp:TextBox ID="txtSenderName" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-end ">
                                        <asp:CheckBox ID="chkIsSSL" runat="server" Text="SSL" CssClass="d-block" />
                                    </div>

                                </div>

                            </asp:View>

                            <asp:View ID="vwEWS" runat="server">
                                <div class="form-group row">

                                    <div class="col-md-4">
                                        <asp:Label ID="lblEWS_URL" runat="server" Text="EWS URL:"></asp:Label>
                                        <asp:TextBox ID="txtEWS_URL" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="rfvEWS_URL" runat="server" Display="None"
                                            ControlToValidate="txtEWS_URL" ErrorMessage="EWS URL cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                    </div>

                                    <div class="col-md-4">
                                        <asp:Label ID="lblEWS_Domain" runat="server" Text="EWS Domain:"></asp:Label>
                                        <asp:TextBox ID="txtEWS_Domain" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="TestEmail"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="rfvEWS_Domain" runat="server" Display="None"
                                            ControlToValidate="txtEWS_Domain" ErrorMessage="EWS Domain cannot be blank "
                                            ValidationGroup="TestEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                    </div>

                                    <%--<div class="col-md-4">
                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="SSL" Style="display: block;" />
                                            </div>--%>
                                </div>
                            </asp:View>

                        </asp:MultiView>

                        <%--<div class="form-group row">
                            <div class="col-md-12" style="text-align: center;">
                                <asp:ValidationSummary ID="vs"
                                    HeaderText="You must enter a value in the following fields:"
                                    DisplayMode="BulletList"
                                    EnableClientScript="true"
                                    runat="server" ValidationGroup="TestEmail" Style="color: red" />
                            </div>
                        </div>--%>
                    </div>

                    <div class="card-footer">
                        <div class="form-group row">

                            <div class="col-md-4">
                                <asp:Button ID="btnTestEmail" runat="server" CssClass="btn btn-primary btn-block" Text="Test Email"  />
                            </div>

                            <div class="col-md-4">
                                <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary btn-block" Text="Update Email Settings"  />
                            </div>

                            <div class="col-md-4">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary btn-block" Text="Reset Default" ValidationGroup="clear" />
                            </div>

                        </div>
                    </div>

                </asp:Panel>
                <%--<cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsTestEmail" _ModuleName="TestEmail" _TargetControl="pnlForm" />--%>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnTestEmail" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="cboCompany" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
