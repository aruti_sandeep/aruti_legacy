﻿<%@ Page Title="Activation Link" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="ASendLink.aspx.vb" Inherits="Aruti_Online_Recruitment.ASendLink" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

			$(document).ready(function () {
			setEvents();
		});


		var prm = Sys.WebForms.PageRequestManager.getInstance();
		if (prm != null) {
			prm.add_endRequest(function (sender, e) {
				setEvents();
			});
		};

		function setEvents() {

				$("[id*=cboLink]").on('change', function () {
				__doPostBack($(this).get(0).id, 0)
				});

				$("[id*=chkSelectAll]").on('click', function () {
				__doPostBack($(this).get(0).id, "onCheckedChanged");
			});


		}


    </script>



    <div class="card">

        <div class="card-header">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>Send Activation Link
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                    </h4>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSendLink" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="grdLink" EventName="DataBound" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">

                        <div class="col-md-4">
                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblPageSize" runat="server" Text="Page Size:"></asp:Label>
                            <asp:DropDownList ID="cboPageSize" runat="server" CssClass="selectpicker form-control" AutoPostBack="true"></asp:DropDownList>

                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="form-group row">
                <div class="col-md-4">
                    <asp:Label ID="lblLink" runat="server" Text="Link:"></asp:Label>
                    <asp:DropDownList ID="cboLink" runat="server" CssClass="selectpicker form-control" ></asp:DropDownList> <%-- AutoPostBack="true"--%>
                </div>

                <div class="col-md-4">
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary w-100" Text="Search" ValidationGroup="Search" /><%-- Style="width: 100%;"--%>
                </div>

                <div class="col-md-4">
                    <asp:LinkButton ID="lnkExcel" runat="server" Text="Export to Excel..."></asp:LinkButton>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_link" runat="server" CssClass="table-responsive minheight300" >  <%--Style="min-height: 300px;"--%>
                        <asp:GridView ID="grdLink" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                            AllowPaging="true" DataSourceID="dsLink" PageSize="5" DataKeyNames="activationlinkunkid, Comp_Code, companyunkid, LoweredEmail, subject, message, UserId">

                            <Columns>
                                <asp:BoundField DataField="Comp_Code" HeaderText="Comp. Code" />
                                <asp:BoundField DataField="companyunkid" HeaderText="Client ID" Visible="false" />
                                <asp:BoundField DataField="subject" HeaderText="subject" Visible="false" />
                                <asp:BoundField DataField="message" HeaderText="message" Visible="false" />
                                <asp:BoundField DataField="LoweredEmail" HeaderText="Email" />
                                <asp:BoundField DataField="MobileNo" HeaderText="Mobile No." />
                                <asp:BoundField DataField="CreateDate" HeaderText="Creation Date" />
                                <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" />
                                <asp:TemplateField HeaderText="Is Approved" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%--<asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApproved"))) %>' Enabled="false" />--%>
                                        <asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApproved"), True)) %>' Enabled="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Email Sent" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%--<asp:CheckBox ID="objchkIsEmailSent" runat="server" Text="" Checked='<%# CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("isemailsent"))) %>' Enabled="false" />--%>
                                        <asp:CheckBox ID="objchkIsEmailSent" runat="server" Text="" Checked='<%# CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("isemailsent"), True)) %>' Enabled="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Send Link">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="objlnkSend" Text="Send" runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Send" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />  <%--AutoPostBack="true" --%>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server"  Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>' />  <%--AutoPostBack="true"--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:ObjectDataSource ID="dsLink" runat="server" SelectMethod="GetActivationLink" TypeName="Aruti_Online_Recruitment.clsApplicant" EnablePaging="true"
                            MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetActivationLinkTotal">
                            <SelectParameters>
                                <asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                                <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />
                                <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                                <asp:Parameter Name="intIsApproved" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="startRowIndex" Type="Int32" />
                                <asp:Parameter Name="intPageSize" Type="Int32" />
                                <asp:Parameter Name="strSortExpression" Type="String" />
                                <asp:Parameter Name="isemailsent" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSendLink" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnActivateApplicants" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnActivateApplicantsAll" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="cboLink" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="cboPageSize" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="lnkExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-footer">
            <asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Button ID="btnSendLink" runat="server" CssClass="btn btn-primary w-100"  Text="Send Links"
                                ValidationGroup="SendLink" />  <%--Style="width: 100%"--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnActivateApplicants" runat="server" CssClass="btn btn-primary w-100" Enabled="false" Text="Activate Applicants"
                                ValidationGroup="SendLink" /> <%--Style="width: 100%" --%>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnActivateApplicantsAll" runat="server" CssClass="btn btn-primary w-100" Enabled="false"  Text="Activate All Applicants"
                                ValidationGroup="ActivateApplicantsAll" /> <%--Style="width: 100%"--%>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
