﻿<%@ Page Title="Qualification" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="ApplicantQualification.aspx.vb" Inherits="Aruti_Online_Recruitment.ApplicantQualification" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="../Controls/FileUpload.ascx" TagName="FUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <%-- <style>
        th a {
            color: white;
        }
    </style>--%>
    <script type="text/javascript">

        $(document).ready(function () {


            //$(".switch input").bootstrapSwitch();
            //$('.switch input').bootstrapSwitch('onText', 'Yes');
            //$('.switch input').bootstrapSwitch('offText', 'No'); 

            setevents();
        });

        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        function setevents() {
            checkBoxCSS();

            //'Pinkal (30-Sep-2023) -- Start
            //' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
         <%--   var fAttachment = document.getElementById('<%= flQualificationAttachment.ClientID %>' + '_image_file');
            if (fAttachment != null)
                fileUpLoadChange();
         
            var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
            if (fupld != null)
                fileUpLoadChange();
        --%>

            $("[id*=_image_file]").each(function () {
                fileUpLoadChange('#' + this.id)
            });


            $('.close').on('click', function () {
                $(this).parents('[id*=pnlFileQualificationAttachment]').find('span[id*=lblFileNameQualificationAttachment]').text('');
                $(this).parents('[id*=pnlFileQualificationAttachment]').find('input[id*=hfFileNameQualificationAttachment]').val('');
                $(this).parents('[id*=pnlFileQualificationAttachment]').hide();
            });


            //'Pinkal (30-Sep-2023) -- End


            //$("[id*=chkqualificationgroup]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=chkqualificationAward]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=chkResultCode]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=chkInstitute]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchkQualiGrp]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchkQualification]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchkQualificationResult]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchkQualificationInst]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchklvQualiGrp]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchklvQualification]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchklvQualificationResult]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});

            //$("[id*=objchklvQualificationInst]").on('click', function () {
            //    __doPostBack($(this).get(0).id, "onCheckedChanged");
            //});



            $$('btnAddQuali').on('click', function () {
                return IsValidate();
            });



            //'Pinkal (30-Sep-2023) -- Start
            //' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            $("[id*=flQualificationAttachment]").on('click', function () {
                return IsValidQualificationAttach();
            });
            //'Pinkal (30-Sep-2023) -- End


            $("[id*=flUpload]").on('click', function () {
                return IsValidAttach();
            });

            $("[id$=ddlAwardgp]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id$=ddlAward]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id$=ddlResultCode]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id$=drpInstitute]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            //$("[id*=objddlQualiGrp]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objddlQuali]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchkQualiGrp]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchkQualification]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchkQualificationResult]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchkQualificationInst]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            $("[name$=objddllvQualiGrp]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[name$=objddllvQuali]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[name$=objddllvQualiResult]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[name$=objddllvQualiInst]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            //$("[id*=objchklvQualiGrp]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchklvQualification]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchklvQualificationResult]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            //$("[id*=objchklvQualificationInst]").on('change', function () {
            //    __doPostBack($(this).get(0).id, 0)
            //});

            $("[id*=btndelete]").on('click', function () {
                if (!ShowConfirm($$('lblDeleteConfMsg').text(), this))
                    return false;
                else
                    return true;
            });

            $("[id*=btnCertidelete]").on('click', function () {
                if (!ShowConfirm($$('lblAttachDeleteConfMsg').text(), this))
                    return false;
                else
                    return true;
            });
        }


        //'Pinkal (30-Sep-2023) -- Start
        //' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.

        function IsValidQualificationAttach() {
            if ($$('ddlQualificationAttachType').val() <= 0) {
                ShowMessage($$('LblQualificationAttachTypeMsg').text(), 'MessageType.Errorr');
                $$('ddlQualificationAttachType').focus();
                return false;
            }

            <%--var attach = document.getElementById('<%= ddlQualificationAttachType.ClientID %>');

            var cntQuali = $('.lvQuali').length;

            if (cntQuali <= 1) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: 'Aruti',
                    message: 'Please add atleast one Qualification.',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            dialogItself.close();
                        }
                    }]
                });
                return false;
            }

            if (parseInt(attach.value) <= 0) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: 'Aruti',
                    message: 'Please Select Attachment Type.',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            dialogItself.close();
                        }
                    }]
                });

                attach.focus();
                return false;
            }--%>
        }
        /*'Pinkal (30-Sep-2023) -- End*/

        function IsValidAttach() {
            if ($$('ddlDocTypeQuali').val() <= 0) {
                ShowMessage($$('lblDocTypeQualiMsg').text(), 'MessageType.Errorr');
                $$('ddlDocTypeQuali').focus();
                return false;
            }

            var attach = document.getElementById('<%= ddlDocTypeQuali.ClientID %>');

            <%--var cntQuali = $('#<%= grdQualification.ClientID %> tr').filter(function () {
                return $(this).css('display') !== 'none';
            }).length;--%>
            var cntQuali = $('.lvQuali').length;

            if (cntQuali <= 1) {
                //alert('Please add atleast one Qualification from Qualification Tab.');
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: 'Aruti',
                    message: 'Please add atleast one Qualification.',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            dialogItself.close();
                        }
                    }]
                });
                return false;
            }

            if (parseInt(attach.value) <= 0) {
                //alert('Please Select Document Type.');
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: 'Aruti',
                    message: 'Please Select Attachment Type.',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            dialogItself.close();
                        }
                    }]
                });

                attach.focus();
                return false;
            }
        }

        function checkBoxCSS() {
            $(".switch input").bootstrapSwitch();
            $('.switch input').bootstrapSwitch('onText', 'Yes');
            $('.switch input').bootstrapSwitch('offText', 'No');

            <%--$('#<%= chkOtherQuali.ClientID %>').bootstrapSwitch('labelText', 'Tick here if Other Qualification / Short Courses not on the list.');--%>
          <%-- $('#<%= chkOtherQuali.ClientID %>').on('switchChange.bootstrapSwitch', function (event, state) {
                javascript: __doPostBack('<%= chkOtherQuali.UniqueID %>', 'chkOtherQuali_CheckedChanged');
            });

            $('#<%= chkNaGPA.ClientID %>').on('switchChange.bootstrapSwitch', function (event, state) {
                javascript: __doPostBack('<%= chkOtherQuali.UniqueID %>', 'chkOtherQuali_CheckedChanged');
           });--%>


            $('.switch input').bootstrapSwitch('labelWidth', 'auto');

        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    checkBoxCSS();
                }

                setevents();
            });

        };

        function IsValidate() {
            if ($$('ddlAwardgp').val() <= 0) {
                ShowMessage($$('lblAwardgpMsg').text(), 'MessageType.Errorr');
                $$('ddlAwardgp').focus();
                return false;
            }
            else if ($$('txtOtherQualiGroup')[0] != null && $$('txtOtherQualiGroup').val().trim() == '') {
                ShowMessage($$('lblOtherQualiGroupMsg').text(), 'MessageType.Errorr');
                $$('txtOtherQualiGroup').focus();
                return false;
            }
            else if ($$('ddlAward').val() <= 0) {
                ShowMessage($$('lblAwardMsg').text(), 'MessageType.Errorr');
                $$('ddlAward').focus();
                return false;
            }
            else if ($$('txtOtherQualification')[0] != null && $$('txtOtherQualification').val().trim() == '') {
                ShowMessage($$('lblOtherQualificationMsg').text(), 'MessageType.Errorr');
                $$('txtOtherQualification').focus();
                return false;
            }
            else if ($$('ddlResultCode').val() <= 0) {
                ShowMessage($$('lblResultCodeMsg').text(), 'MessageType.Errorr');
                $$('ddlResultCode').focus();
                return false;
            }
            else if ($$('txtOtherResultCode')[0] != null && $$('txtOtherResultCode').val().trim() == '') {
                ShowMessage($$('lblOtherResultCodeMsg').text(), 'MessageType.Errorr');
                $$('txtOtherResultCode').focus();
                return false;
            }
            else if ($$('txtCertiNo').length > 0 && $$('txtCertiNo').val().trim() == '' && '<%= Session("CertificateNoMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblCertNoMsg').text(), 'MessageType.Errorr');
                $$('txtCertiNo').focus();
                return false;
            }

            return true;
        }
    </script>

    <div class="card">

        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>

                <h4>
                    <asp:Label ID="lblHeader" runat="server">Qualification Information</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
                <asp:Label ID="lblQualification" runat="server" Visible="false">Qualification</asp:Label>
            </div>

            <div class="form-group row d-none">
                <div class="col-md-4">
                    <h5>
                        <asp:Label ID="lblSubHeader1" runat="server" Visible="false">Qualification Details</asp:Label></h5>
                </div>
                <%--<div class="col-md-8">
                    <%--<asp:Label ID="lblOtherQualiNotInList" runat="server" Text="Tick here if Other Qualification / Short Courses not on the list." Font-Bold="true"></asp:Label>
                    <asp:CheckBox ID="chkOtherQuali" runat="server" CssClass="switch" AutoPostBack="true" Style="float: right;" />
                </div>--%>
            </div>

            <div class="card-body">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group row">

                            <div class="col-md-4">
                                <div class="row m-0 p-0">
                                    <div class="col-md-8 m-0 p-0">
                                        <asp:Label ID="lblqalifiawagp" runat="server" Text="Quali./Award Grp:" CssClass="required"></asp:Label>
                                    </div>
                                    <div class="col-md-4 m-0 p-0 text-right">
                                        <%--style="text-align: right--%>
                                        <asp:CheckBox ID="chkqualificationgroup" runat="server" Text="Others" AutoPostBack="false" Visible="false" />
                                    </div>
                                </div>

                                <div class="row m-0 p-0">
                                    <div class="col-md-12 m-0 p-0">
                                        <asp:Panel ID="pnlQualficationGrp" runat="server">
                                            <asp:DropDownList ID="ddlAwardgp" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                          <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None" InitialValue="0"
                                                ControlToValidate="ddlAwardgp" ErrorMessage="Please select Qualification Group. "
                                                ValidationGroup="Qualification" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblAwardgpMsg" runat="server" Text="Please select qualification group." CssClass="d-none"></asp:Label>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlOtherQualficationGrp" runat="server" CssClass="input-group" Visible="false">                                                                                     
                                            <asp:TextBox ID="txtOtherQualiGroup" runat="server" ValidationGroup="Qualification" CssClass="form-control"></asp:TextBox>
                                            <button id="btnHideOtherQualiGrp" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list"><i class="fa fa-times"></i></button>
                                          <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="None"
                                                ControlToValidate="txtOtherQualiGroup" ErrorMessage="Qualification Group can not be Blank. "
                                                ValidationGroup="Qualification" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblOtherQualiGroupMsg" runat="server" Text="Please enter qualification group." CssClass="d-none"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </div>

                                <%--<cc1:CascadingDropDown ID="cddAwardgp" runat="server" TargetControlID="ddlAwardgp"
                        Category="qualigrp" PromptText="-- Select Qualification Group --" LoadingText="[Loading Qualification Group...]"
                        ServiceMethod="GetQualiGroup" BehaviorID="cddAwardgp" />--%>
                            </div>

                            <div class="col-md-4">
                                <div class="row m-0 p-0">
                                    <div class="col-md-8 m-0 p-0">
                                        <asp:Label ID="lblQuliAwrd" runat="server" Text="Quali./Award:" CssClass="required"></asp:Label>
                                    </div>
                                    <div class="col-md-4 m-0 p-0 text-right">
                                        <%--  style="text-align: right"--%>
                                        <asp:CheckBox ID="chkqualificationAward" runat="server" Text="Others" AutoPostBack="false" Visible="false" />
                                    </div>
                                </div>

                                <div class="row m-0 p-0">
                                    <div class="col-md-12 m-0 p-0">
                                        <asp:Panel ID="pnlQualficationAward" runat="server">
                                            <asp:DropDownList ID="ddlAward" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None" InitialValue="0"
                                                ControlToValidate="ddlAward" ErrorMessage="Please select Qualification. "
                                                ValidationGroup="Qualification" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblAwardMsg" runat="server" Text="Please select qualification." CssClass="d-none"></asp:Label>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlOtherQualficationAward" runat="server" CssClass="input-group" Visible="false">
                                            <asp:TextBox ID="txtOtherQualification" runat="server" ValidationGroup="Qualification" CssClass="form-control"></asp:TextBox>
                                            <button id="btnHideOtherQuali" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list"><i class="fa fa-times"></i></button>
                                          <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="None"
                                                ControlToValidate="txtOtherQualification" ErrorMessage="Qualification can not be Blank. "
                                                ValidationGroup="Qualification" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblOtherQualificationMsg" runat="server" Text="Please enter qualification." CssClass="d-none"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </div>

                                <%--<cc1:CascadingDropDown ID="cddAward" runat="server" TargetControlID="ddlAward" Category="quali"
                        ParentControlID="ddlAwardgp" PromptText="-- Select Qualification --" LoadingText="[Loading Qualification ...]"
                        ServiceMethod="GetQualification" BehaviorID="cddAward" />--%>
                            </div>

                            <div class="col-md-4">
                                <div class="row m-0 p-0">
                                    <div class="col-md-8 m-0 p-0">
                                        <asp:Label ID="lblResult" runat="server" Text="Result:" CssClass="required"></asp:Label>
                                    </div>
                                    <div class="col-md-4 m-0 p-0 text-right">
                                        <%-- style="text-align: right"--%>
                                        <asp:CheckBox ID="chkResultCode" runat="server" Text="Others" AutoPostBack="false" Visible="false" />
                                    </div>
                                </div>

                                <div class="row m-0 p-0">
                                    <div class="col-md-12 m-0 p-0">
                                        <asp:Panel ID="pnlResultCode" runat="server">
                                            <asp:DropDownList ID="ddlResultCode" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                         <%--   <asp:RequiredFieldValidator ID="rfvResultCode" runat="server" Display="None" InitialValue="0"
                                                ControlToValidate="ddlResultCode" ErrorMessage="Please select Result. "
                                                ValidationGroup="Qualification" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblResultCodeMsg" runat="server" Text="Please select result." CssClass="d-none"></asp:Label>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlOtherResultCode" runat="server" CssClass="input-group" Visible="false">
                                            <asp:TextBox ID="txtOtherResultCode" runat="server" ValidationGroup="Qualification" CssClass="form-control"></asp:TextBox>
                                            <button id="btnHideOtherResultCode" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list"><i class="fa fa-times"></i></button>
                                         <%--   <asp:RequiredFieldValidator ID="rfvOtherResultCode" runat="server" Display="None" ControlToValidate="txtOtherResultCode" ErrorMessage="Result Code can not be Blank. "
                                                ValidationGroup="Qualification" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblOtherResultCodeMsg" runat="server" Text="Please enter result." CssClass="d-none"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </div>

                                <%--<cc1:CascadingDropDown ID="cddResultCode" runat="server" TargetControlID="ddlResultCode"
                        Category="resultcode" ParentControlID="ddlAward" PromptText="-- Select Result Code --"
                        LoadingText="[Loading Result Code ...]" ServiceMethod="GetResultCode" BehaviorID="cddResultCode" />--%>
                            </div>

                        </div>


                        <div class="form-group row">
                            <div class="col-md-4">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row m-0 p-0">
                                            <div class="col-md-12 m-0 p-0">
                                                <div class="row m-0 p-0">
                                                    <div class="col-md-8 m-0 p-0">
                                                        <asp:Label ID="lblInstitute" runat="server" Text="Institution:"></asp:Label>
                                                    </div>
                                                    <div class="col-md-4 m-0 p-0 text-right">
                                                        <%--style="text-align: right"--%>
                                                        <asp:CheckBox ID="chkInstitute" runat="server" Text="Others" AutoPostBack="false" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row m-0 p-0">
                                                    <div class="col-md-12 m-0 p-0">
                                                        <asp:Panel ID="pnlInstitute" runat="server">
                                                            <asp:DropDownList ID="drpInstitute" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlOtherInstitute" runat="server" CssClass="input-group" Visible="false">
                                                            <asp:TextBox ID="txtOtherInstitution" runat="server" ValidationGroup="Qualification" CssClass="form-control"></asp:TextBox>
                                                            <button id="btnHideOtherInstitute" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list"><i class="fa fa-times"></i></button>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:AsyncPostBackTrigger ControlID="chkOtherQuali" EventName="CheckedChanged" />--%>
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>


                            <div class="col-md-4 col-sm-12 col-md-offset-0">
                                <asp:Label ID="lblAwardDate" runat="server" Text="Start Date:" CssClass=" required"></asp:Label>
                                <uc2:DateCtrl ID="dtAwardDate" runat="server" AutoPostBack="false" CssClass="form-control" />
                                <asp:Label ID="lblAwardDateMsg" runat="server" Text="Please Enter Valid Qualification Start Date." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4 col-sm-12  col-md-offset-0">
                                <asp:Label ID="lblDateto" runat="server" Text="Award Date:" CssClass=" required"></asp:Label>
                                <uc2:DateCtrl ID="dtdateto" runat="server" AutoPostBack="false" CssClass="form-control" />
                                <asp:Label ID="lblDatetoMsg" runat="server" Text="Please Enter Valid Qualification Award Date." CssClass="d-none"></asp:Label>
                                <asp:Label ID="lblDatetoMsg2" runat="server" Text="Award date cannot be less or equal to start date." CssClass="d-none"></asp:Label>
                            </div>

                        </div>

                        <div class="form-group row">
                            <asp:Panel ID="pnlGPA" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblGPA" runat="server" Text="GPA/Points:" CssClass="required"></asp:Label>
                                <asp:TextBox ID="txtGPAcode" runat="server" ValidationGroup="Qualification" CssClass="form-control"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtGPAcode" FilterMode="ValidChars" FilterType="Numbers, Custom" ValidChars="."></cc1:FilteredTextBoxExtender>
                               <%-- <asp:RangeValidator ID="NumericValidator" runat="server" ControlToValidate="txtGPAcode"
                                    CssClass="ErrorControl" ErrorMessage="This Number is Invalid!" Display="None"
                                    ForeColor="White" MaximumValue="99999" MinimumValue="0" Type="Double" ValidationGroup="Qualification"
                                    SetFocusOnError="True">
                                </asp:RangeValidator>--%>
                                <asp:Label ID="lblGPAMsg" runat="server" Text="GPA is compulsory information. Please either enter GPA or Tick on GPA Not Applicable option." CssClass="d-none"></asp:Label>
                                <asp:Label ID="lblGPAMsg2" runat="server" Text="Please enter GPA between 1 to 999." CssClass="d-none"></asp:Label>
                            </asp:Panel>

                            <asp:panel ID="pnlCertNo" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblCertiNo" runat="server" Text="Certificate No."></asp:Label>
                                <asp:TextBox ID="txtCertiNo" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:Label ID="lblCertNoMsg" runat="server" Text="Please enter certificate no.." CssClass="d-none"></asp:Label>
                            </asp:panel>

                            <asp:Panel ID="pnlRemark" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblRemark" runat="server" Text="Remarks:"></asp:Label>
                                <asp:TextBox ID="txtQualiremark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </asp:Panel>



                            <asp:Panel ID="pnlGPANotApplicable" runat="server" class="col-md-4 mb-1 pt-3">
                                <uc2:DateCtrl ID="dtDate" runat="server" Visible="false" AutoPostBack="false" CssClass="form-control" />
                                <asp:Label ID="lblGPNotApplicable" runat="server" Text="GPA Not Applicable" CssClass="font-weight-bold"></asp:Label>
                                <asp:CheckBox ID="chkNaGPA" runat="server" CssClass="switch float-right" Checked="true" />
                                <%--Style="float: right;"--%>
                                <asp:Label ID="lblRefNo" runat="server" Text="Reference No:" Visible="false" CssClass="form-control"></asp:Label>
                                <asp:TextBox ID="txtRefno" runat="server" MaxLength="50" Visible="false" CssClass="form-control"></asp:TextBox>
                            </asp:Panel>

                            <asp:Panel ID="pnlHighestQuali" runat="server" class="col-md-4 mb-1 pt-3">
                                <asp:Label ID="lvlHigh" runat="server" Text="Highest Qualification" CssClass="font-weight-bold"></asp:Label>
                                <asp:CheckBox ID="chkHighestqualification" runat="server" CssClass="switch float-right" />
                                <%-- Style="float: right;"--%>
                            </asp:Panel>

                            <div class="col-md-4 mb-1">
                            </div>
                        </div>


                        <%--'Pinkal (30-Sep-2023) -- Start--%>
                        <%--'(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.%>--%>

                         <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                        <div class="form-group row">
                            <div class="card-body">
                                <asp:Panel ID="pnlQualificationAttachmentMsg" runat="server">
                                    <asp:Label ID="lblQualificationAttachmentUpload" runat="server" Font-Bold="true" Text="Select file (Image / PDF) (All file size should not exceed more than 5 MB.)" CssClass="d-block"> 
                                    </asp:Label>
                                </asp:Panel>

                                <div class="col-md-12">
                                    <asp:Panel ID="pnlQualificationAttachment" runat="server" CssClass="form-group row attachdoc">
                                        <div class="col-md-3">
                                            <asp:Label ID="LblQualificationAttachType" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                            <asp:DropDownList ID="ddlQualificationAttachType" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                            <asp:Label ID="LblQualificationAttachTypeMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
                                        </div>
                                        <div class="col-md-2 mt-3">
                                            <uc9:FUpload id="flQualificationAttachment" runat="server" maxsizekb="5242880" onbtnupload_click="flQualificationAttachment_btnUpload_Click" />
                                            <asp:Label ID="lblAttachMsg" runat="server" CssClass="d-none" Text="Sorry, Qualification attachment is mandatory."></asp:Label>
                                        </div>
                                        <div class="col-md-4 mt-2">
                                            <asp:Panel ID="pnlFileQualificationAttachment" runat="server" CssClass="card rounded">
                                                <div class="row no-gutters m-0">
                                                    <div class="col-md-3 text-center pt-3 fileext">
                                                        <asp:Label ID="lblFileExtQualificationAttachment" runat="server" Text="PDF"></asp:Label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="card-body py-1 pr-2">
                                                            <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                            <h6 class="card-title m-0">
                                                                <asp:Label ID="lblFileNameQualificationAttachment" runat="server" Text=""></asp:Label></h6>
                                                            <p class="card-text">
                                                                <small class="text-muted">
                                                                    <asp:Label ID="lblDocSizeQualificationAttachment" runat="server" Text="0.0 MB"></asp:Label></small>
                                                            </p>
                                                            <asp:HiddenField ID="hfDocSizeQualificationAttachment" runat="server" Value="" />
                                                            <asp:HiddenField ID="hfFileNameQualificationAttachment" runat="server" Value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <% End If  %>
                        <%--'Pinkal (30-Sep-2023) -- End--%>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="chkOtherQuali" EventName="CheckedChanged" />--%>
                        <asp:AsyncPostBackTrigger ControlID="btnAddQuali" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <%--<div class="form-group row">
                    

                </div>--%>

                <div class="form-group row">
                    <div class="col-md-12 text-center">
                        <%--style="text-align: center;"--%>
                       <%-- <asp:ValidationSummary ID="vs"
                            HeaderText="You must enter a value in the following fields:"
                            DisplayMode="BulletList"
                            EnableClientScript="true"
                            runat="server" ValidationGroup="Qualification" CssClass ="text-danger"  /> <%-- Style="color: red" --%>
                    </div>
                </div>

                <%--<div class="form-group row">
            <div class="col-md-12" style="text-align: center;">
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
            </div>
        </div>--%>
            </div>

            <div class="card-footer">
                <div class="form-group row d-flex justify-content-center">
                    <%--<div class="col-md-4">
                    </div>--%>

                    <div class="col-md-4">
                        <asp:Button ID="btnAddQuali" runat="server" CssClass="btn btn-primary w-100" Text="Add Qualification" /><%--   Style="width: 100%;"--%>
                        <asp:Label ID="lblExistMsg" runat="server" Text="Sorry, Selected Qualification is already added to the list." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblSaveMsg" runat="server" Text="Qualification Saved Successfully! \n\n Click Ok to add another Qualification or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblUpdateMsg" runat="server" Text="Qualification Updated Successfully! \n\n Click Ok to add another Qualification or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblDeleteMsg" runat="server" Text="Qualification Deleted Successfully! \n\n Click Ok to add another Qualification or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblDeleteConfMsg" runat="server" Text="Are you sure you want to Delete this Qualification?" CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblOtherNotInListmsg" runat="server" Text="Other (If not in the list)" CssClass="d-none"></asp:Label>
                    </div>

                    <%--<div class="col-md-4">
                        <%--<asp:Button ID="btnEditquli" runat="server" CssClass="btn btn-primary" Style="width: 100%;" Enabled="False"
                    Text="Update Qualification" ValidationGroup="Qualification" OnClientClick="return IsValidQualification();" />--%>
                    <%--</div>--%>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_grdQualification" runat="server" CssClass="table-responsive min-vh-100 overflow-auto">
                        <%-- Style="min-height: 300px;"--%>
                        <asp:Label ID="colhQualification" runat="server" Text="Qualification" Visible="false"></asp:Label>
                        <asp:Label ID="colhInstitute" runat="server" Text="Institution" Visible="false"></asp:Label>
                        <asp:Label ID="colhResultcode" runat="server" Text="Result" Visible="false"></asp:Label>
                        <asp:Label ID="colhDuration" runat="server" Text="Duration" Visible="false"></asp:Label>
                        <asp:Label ID="colhGpacode" runat="server" Text="GPA Point" Visible="false"></asp:Label>

                        <asp:ListView ID="lvQualification" runat="server" ItemPlaceholderID="QualificationItemsPlaceHolder" DataSourceID="objodsQuali" DataKeyNames="qualificationtranunkid" >
                            <LayoutTemplate>
                                <div class="form-group row">
                                    <%--<div class="col-md-10 mb-1">
                                        <div class="col-md-2" >
                                                                                       
                                        </div>
                                        <div class="col-md-2">
                                                                                   
                                        </div>
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        ad asd 
                                    </div>--%>
                                    <div class="col-md-12 text-center font-weight-bold">
                                        <asp:Label ID="lblQualificationList" runat="server" Text="Qualification list" ></asp:Label>
                                        <hr class="mt-0" />
                                    </div>
                                </div>
                                <asp:PlaceHolder ID="QualificationItemsPlaceHolder" runat="server" ></asp:PlaceHolder>
                            </LayoutTemplate>       
                            <ItemTemplate>
                                <div class="form-group row lvQuali">
                                    <div class="col-md-10 mb-1">
                                        <div class="form-group row mb-0">
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvQualiGrp" runat="server" Text='Qualification Group' CssClass="font-weight-bold" />
                                                <br />
                                                <asp:Label ID="objlbllvQualiGrp" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(If(CInt(Eval("qualificationgroupunkid")) > 0, Eval("qualification_group"), Eval("other_qualificationgrp")), True) %>' />
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvQuali" runat="server" Text='Qualification' CssClass="font-weight-bold" />
                                                <br />
                                                <asp:Label ID="objlbllvQuali" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(If(CInt(Eval("qualificationunkid")) > 0, Eval("Qualification"), Eval("other_qualification")), True) %>' />
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvQualiResult" runat="server" Text='Result' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvQualiResult" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(If(CInt(Eval("resultunkid")) > 0, Eval("resultcode"), Eval("other_resultcode")), True) %>' />                                                
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvQualiInst" runat="server" Text='Institution' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvQualiInst" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(If(CInt(Eval("instituteunkid")) > 0, Eval("institute"), Eval("other_institute")), True) %>' />
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvQualiStartDate" runat="server" Text='Start Date' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvQualiStartDate" runat="server" Text='<%# IIf(CDate(Eval("startdate")) = CDate("01/Jan/1900"), "", CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("startdate"), True)).ToShortDateString) %>' />                                                
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvQualiAwardtDate" runat="server" Text='Award Date' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvQualiAwardtDate" runat="server" Text='<%# IIf(CDate(Eval("enddate")) = CDate("01/Jan/1900"), "", CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("enddate"), True)).ToShortDateString)  %>' />
                                            </div>
                                            <asp:Panel ID="pnllvGPANotApplicable" runat="server" class="col-md-2 px-2 mb-1">
                                                <asp:Label ID="lbllvNaGPA" runat="server" Text='GPA Not Applicable' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvNaGPA" runat="server" Text='<%# If(AntiXss.AntiXssEncoder.HtmlEncode(Eval("gpacode"), True) = 0, "Yes", "No")  %>' />                                                
                                            </asp:Panel>
                                            <asp:Panel ID="pnllvGPA" runat="server" class="col-md-2 px-2 mb-1">
                                                <asp:Label ID="lbllvGPACode" runat="server" Text='GPA/Points' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvGPACode" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("gpacode"), True)  %>' /><br />
                                            </asp:Panel>
                                            <asp:Panel ID="pnllvCertNo" runat="server" class="col-md-2 px-2 mb-1">
                                                <asp:Label ID="lbllvCertiNo" runat="server" Text='Certificate No' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvCertiNo" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("certificateno"), True)  %>' />                                                
                                            </asp:Panel>
                                            <asp:Panel ID="pnllvRemark" runat="server" class="col-md-2 px-2 mb-1">
                                                <asp:Label ID="lbllvRemark" runat="server" Text='Remark' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvRemark" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("remark"), True)  %>' />
                                            </asp:Panel>
                                            <asp:Panel ID="pnllvHighestQuali" runat="server" class="col-md-2 px-2 mb-1">
                                                <asp:Label ID="lbllvHighestQuali" runat="server" Text='Highest Qualification' CssClass="font-weight-bold" /><br />
                                                <asp:Label ID="objlbllvHighestQuali" runat="server" Text='<%# If(AntiXss.AntiXssEncoder.HtmlEncode(Eval("ishighestqualification"), True) = True, "Yes", "")  %>' />
                                            </asp:Panel>

                                        </div>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="objlvbtnedit" runat="server" CommandName="Edit" Text="<i aria-hidden='true' class='fa fa-edit'></i>"></asp:LinkButton>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="objbtndelete" runat="server" CommandName="Delete" Text="<i aria-hidden='true' class='fa fa-trash'></i>" ></asp:LinkButton>
                                    </div>
                                </div>
                                <hr class="mt-0" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="form-group row">
                                <div class="col-md-10 mb-1">
                                    <div class="form-group row mb-0">
                                        <div class="col-md-4 px-2">
                                            <div class="row m-0 p-0">
                                                <div class="col-md-8 m-0 p-0">
                                                    <asp:Label ID="lbllvQualiGrp" runat="server" Text='Qualification Group' CssClass="font-weight-bold" />
                                                </div>
                                                <div class="col-md-4 m-0 p-0 text-right">
                                                    <asp:CheckBox ID="objchklvQualiGrp" runat="server" AutoPostBack="false" OnCheckedChanged="chkListViewOther_CheckedChanged" Text="Others" Visible="false" />
                                                </div>
                                            </div>
                                            <div class="row m-0 p-0">
                                                <div class="col-md-12 m-0 p-0">
                                                    <asp:UpdatePanel ID="UpnllvQualificationgrp" runat="server">
                                                        <ContentTemplate>
                                                            <div class="input-group">
                                                            <asp:DropDownList ID="objddllvQualiGrp" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddllvAwardgp_SelectedIndexChanged" />
                                                            <asp:TextBox ID="objtxtlvQualiGrp" runat="server" Text='<%# Eval("other_qualificationgrp") %>' TextMode="MultiLine" CssClass="form-control" Visible="false" />                                                    
                                                                <button id="btnlvHideOtherQualiGrp" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list" visible="false" onserverclick="btnlvHideOtherQualiGrp_ServerClick"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>                                           
                                            
                                        </div>
                                        <div class="col-md-4 px-2">
                                            <div class="row m-0 p-0">
                                                <div class="col-md-8 m-0 p-0">
                                                    <asp:Label ID="lbllvQuali" runat="server" Text='Qualification' CssClass="font-weight-bold" />
                                                </div>
                                                <div class="col-md-4 m-0 p-0 text-right" >
                                                    <asp:CheckBox ID="objchklvQualification" runat="server" AutoPostBack="false" OnCheckedChanged="chkListViewOther_CheckedChanged" Text="Other" Visible="false" />
                                                </div>
                                            </div>
                                            <div class="row m-0 p-0">
                                                <div class="col-md-12 m-0 p-0">
                                                    <asp:UpdatePanel ID="UpnllvQualification" runat="server">
                                                    <ContentTemplate>
                                                        <div class="input-group">
                                                        <asp:DropDownList ID="objddllvQuali" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddllvAward_SelectedIndexChanged" />
                                                        <asp:TextBox ID="objtxtlvQuali" runat="server" Text='<%# Eval("other_qualification") %>' TextMode="MultiLine" CssClass="form-control" Visible="false" />
                                                            <button id="btnlvHideOtherQuali" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list" visible="false" onserverclick="btnlvHideOtherQualiGrp_ServerClick"><i class="fa fa-times"></i></button>
                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-4 px-2">
                                            <div class="row m-0 p-0">
                                                <div class="col-md-8 m-0 p-0">
                                                    <asp:Label ID="lbllvQualiResult" runat="server" Text='Result' CssClass="font-weight-bold" />
                                                </div>
                                                <div class="col-md-4 m-0 p-0 text-right">
                                                    <asp:CheckBox ID="objchklvQualificationResult" runat="server" AutoPostBack="false" OnCheckedChanged="chkListViewOther_CheckedChanged" Text="Other" Visible="false" />
                                                </div>
                                            </div>
                                            <div class="row m-0 p-0">
                                                <div class="col-md-12 m-0 p-0">
                                                    <asp:UpdatePanel ID="UpnllvQualificationResult" runat="server">
                                                        <ContentTemplate>
                                                            <div class="input-group">
                                                                <asp:DropDownList ID="objddllvQualiResult" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddllvQualiResult_SelectedIndexChanged" />
                                                            <asp:TextBox ID="objtxtlvQualiResult" runat="server" Text='<%# Eval("other_resultcode") %>' TextMode="MultiLine" CssClass="form-control" Visible="false" />
                                                                <button id="btnlvHideOtherResult" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list" visible="false" onserverclick="btnlvHideOtherQualiGrp_ServerClick"><i class="fa fa-times"></i></button>
                                                            </div>
                                                    
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-md-4 px-2">
                                            <div class="row m-0 p-0">
                                                <div class="col-md-8 m-0 p-0">
                                                    <asp:Label ID="lbllvQualiInst" runat="server" Text='Institution' CssClass="font-weight-bold" />
                                                </div>
                                                <div class="col-md-4 m-0 p-0 text-right">
                                                    <asp:CheckBox ID="objchklvQualificationInst" runat="server" AutoPostBack="false" OnCheckedChanged="chkListViewOther_CheckedChanged" Text="Other" Visible="false" />
                                                </div>
                                            </div>
                                            <div class="row m-0 p-0">
                                                <div class="col-md-12 m-0 p-0">
                                                    <asp:UpdatePanel ID="UpnllvQualificationInst" runat="server">
                                                        <ContentTemplate>
                                                            <div class="input-group">
                                                                <asp:DropDownList ID="objddllvQualiInst" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddllvQualiInst_SelectedIndexChanged" />
                                                            <asp:TextBox ID="objtxtlvQualiInst" runat="server" Text='<%# Eval("other_institute")%>' CssClass="form-control" TextMode="MultiLine" Visible="false" />
                                                                <button id="btnlvHideOtherInst" runat="server" class="btn bg-transparent btnclose" title="Close and show dropdown list" visible="false" onserverclick="btnlvHideOtherQualiGrp_ServerClick"><i class="fa fa-times"></i></button>
                                                            </div>
                                                    
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <div class="col-md-4 px-2">
                                            <asp:Label ID="lbllvQualiStartDate" runat="server" Text='Start Date' CssClass="font-weight-bold" /><br />
                                            <uc2:DateCtrl ID="objdtlvStartDate" runat="server" AutoPostBack="false" SetDate='<%# IIf(CDate(Eval("startdate")) = CDate("01/Jan/1900"), Nothing, CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("startdate"), True)).ToShortDateString) %>' />
                                                                                        
                                        </div>
                                        <div class="col-md-4 px-2">
                                            <asp:Label ID="lbllvQualiAwardtDate" runat="server" Text='Award Date' CssClass="font-weight-bold" /><br />
                                            <uc2:DateCtrl ID="objdtlvAwardDate" runat="server" AutoPostBack="false" SetDate='<%# IIf(CDate(Eval("enddate")) = CDate("01/Jan/1900"), Nothing, CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("enddate"), True)).ToShortDateString) %>' />
                                        </div>
                                        <asp:Panel ID="pnllvGPANotApplicable" runat="server"  class="col-md-4 px-2 mb-1">
                                            <asp:Label ID="lbllvNaGPA" runat="server" Text='GPA Not Applicable' CssClass="font-weight-bold" /><br />
                                            <%--<asp:CheckBox ID="objchkNaGPA" runat="server" Text="" Checked='<%# If(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("gpacode")) = 0, "true", "false")  %>' /><br />--%>
                                            <asp:CheckBox ID="objchklvNaGPA" runat="server" Text="" Checked='<%# If(AntiXss.AntiXssEncoder.HtmlEncode(Eval("gpacode"), True) = 0, "true", "false")  %>' />                                                                                                                                   
                                        </asp:Panel>
                                        <asp:Panel ID="pnllvGPA" runat="server" class="col-md-4 px-2 mb-1">
                                            <asp:Label ID="lbllvGPACode" runat="server" Text='GPA/Points' CssClass="font-weight-bold" /><br />
                                            <asp:TextBox ID="objtxtlvGPACode" runat="server" CssClass="form-control" Text='<%# Eval("gpacode")  %>' />
                                        </asp:Panel>
                                        <asp:Panel ID="pnllvCertNo" runat="server" class="col-md-4 px-2 mb-1">
                                            <asp:Label ID="lbllvCertiNo" runat="server" Text='Certificate No' CssClass="font-weight-bold" /><br />
                                            <asp:TextBox ID="objtxtlvCertiNo" runat="server" CssClass="form-control" Text='<%# Eval("certificateno")  %>' TextMode="MultiLine" />                                            
                                        </asp:Panel>
                                        <asp:Panel ID="pnllvRemark" runat="server" class="col-md-4 px-2 mb-1">
                                            <asp:Label ID="lbllvRemark" runat="server" Text='Remark' CssClass="font-weight-bold" /><br />
                                            <asp:TextBox ID="objtxtlvRemark" runat="server" CssClass="form-control" Text='<%# Eval("remark")  %>' TextMode="MultiLine" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnllvHighestQuali" runat="server" class="col-md-4 px-2 mb-1">
                                            <asp:CheckBox ID="objchklvHighestQualification" runat="server" CssClass="font-weight-bold" Text="Highest Qualification" Checked='<%# If(AntiXss.AntiXssEncoder.HtmlEncode(Eval("ishighestqualification"), True) = True, "true", "false")  %>' />
                                        </asp:Panel>                                       



                                    </div>
                                </div>
                                <div class="col-md-1 mb-1">
                                    <asp:LinkButton ID="btnlvupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>                                    
                                </div>
                                <div class="col-md-1 mb-1">
                                    <asp:LinkButton ID="btnlvcancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </div>
                                    </div>
                            </EditItemTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="objodsQuali" runat="server" SelectMethod="GetAppQualifications" UpdateMethod="EditAppQualification" TypeName="Aruti_Online_Recruitment.clsApplicantQualification" DeleteMethod="DeleteApplicantQualification" EnablePaging="false" EnableCaching="True" CacheDuration="120" >
                            <SelectParameters>
                                <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intQualificationGroup_ID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intSortOrder" Type="Int32" DefaultValue="0" />                                
                                <asp:Parameter Name="dtCurrentDate" Type="String" DefaultValue="" />
                            </SelectParameters>

                            <UpdateParameters>
                              <%--  <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intQualificationTranUnkid" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intQualificationGroupUnkid" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intQualificationUnkid" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="dtTransaction_Date" Type="DateTime" DefaultValue="0" />
                                <asp:Parameter Name="strReference_no" Type="String" DefaultValue="" />
                                <asp:Parameter Name="dtAward_start_Date" Type="DateTime" DefaultValue="0" />
                                <asp:Parameter Name="dtAward_end_Date" Type="DateTime" DefaultValue="0" />
                                <asp:Parameter Name="intInstituteunkid" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intResultunkid" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="decGPACode" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="strCertificateno" Type="String" DefaultValue="" />
                                <asp:Parameter Name="strOther_Qualificationgrp" Type="String" DefaultValue="" />
                                <asp:Parameter Name="strOther_Qualification" Type="String" DefaultValue="" />
                                <asp:Parameter Name="strOther_Institute" Type="String" DefaultValue="" />
                                <asp:Parameter Name="strOther_Resultcode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="strRemark" Type="String" DefaultValue="" />
                                <asp:Parameter Name="blnHighestQualification" Type="Boolean" DefaultValue="False" />--%>
                                <asp:Parameter Name="objAppQualiUpdate" Type="Object" />
                            </UpdateParameters>
                            
                            <DeleteParameters>
                                <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intQualificationTranUnkid" Type="Int32" DefaultValue="0" />
                            </DeleteParameters>

                        </asp:ObjectDataSource>
                                                
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddQuali" EventName="Click" />
                    <%--<asp:AsyncPostBackTrigger ControlID="grdQualification" EventName="Sorting" />--%>
                    <asp:AsyncPostBackTrigger ControlID="lvQualification" EventName="ItemUpdating" />
                </Triggers>
            </asp:UpdatePanel>


            <asp:Panel ID="pnlAttachQuali" runat="server">

                <div class="card-body">
                    <h5>
                        <asp:Label ID="lblSubHeader2" runat="server">Qualification Certificate Attachments</asp:Label></h5>

                    <div class="form-group row">

                        <div class="col-md-4">
                            <asp:Label ID="lblDocTypeQuali" runat="server" Text="Attachment Type"></asp:Label>
                            <asp:DropDownList ID="ddlDocTypeQuali" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None" ControlToValidate="ddlDocTypeQuali" ErrorMessage="Please select Attachment Type."
                                InitialValue="0" ValidationGroup="Attach" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            <asp:Label ID="lblDocTypeQualiMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblUpload" runat="server" Text="Select Certificate (Image / PDF) (All file size should not exceed more than 3 MB.)" CssClass="d-block">   <%--Style="display: block;"--%>
                            </asp:Label>
                        </div>

                        <div class="col-md-4">
                            <%--<uc9:FileUpload ID="flUpload" runat="server" MaxSizeKB="1048576" OnClick="return IsValidAttach();" />--%>
                            <uc9:FUpload ID="flUpload" runat="server" MaxSizeKB="3145728" />
                            <asp:Label ID="objlblTotSize" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblAttachExistMsg" runat="server" CssClass="d-none" Text="Sorry, Selected file with same name is already added to the list."></asp:Label>
                             <asp:Label ID="lblAttachImageMsg" runat="server" CssClass="d-none" Text="Please select proper Image file or PDF file."></asp:Label>
                            <asp:Label ID="lblAttachSizeMsg" runat="server" CssClass="d-none" Text="Sorry,You cannot upload file greater than #maxsize# MB."></asp:Label>
                            <asp:Label ID="lblAttachDeleteMsg" runat="server" CssClass="d-none" Text="Attachment Deleted Successfully! \n\n Click Ok to add another Qualification or to continue with other section(s)."></asp:Label>
                            <asp:Label ID="lblAttachSaveMsg" runat="server" CssClass="d-none" Text="Attachment Saved Successfully! \n\n Click Ok to add another Qualification or to continue with other section(s)."></asp:Label>
                             <asp:Label ID="lblAttachDeleteConfMsg" runat="server" CssClass="d-none" Text="Are you sure you want to Delete this attachment?"></asp:Label>
                        </div>

                    </div>
                </div>

                <asp:UpdatePanel ID="updgvQualiCerti" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnl_gvQualiCerti" runat="server" CssClass="table-responsive overflow-auto">

                            <asp:GridView ID="gvQualiCerti" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                                AllowPaging="false" DataKeyNames="attachfiletranunkid, filepath">
                                <Columns>

                                    <asp:TemplateField HeaderText="File Name" FooterText="colhFileName">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="objlblFilename" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("filename"))  %>'></asp:Label>--%>
                                            <asp:Label ID="objlblFilename" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("filename"), True)  %>'></asp:Label>
                                        </ItemTemplate>
                                        <%-- <EditItemTemplate>
                                    <asp:TextBox ID="objtxtFilename" runat="server" CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>--%>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="File Path" Visible="false" FooterText="objcolhFilePath">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="objlblFilepath" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("filepath"))  %>'></asp:Label>--%>
                                            <asp:Label ID="objlblFilepath" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("filepath"), True)  %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<EditItemTemplate>
                                    <asp:TextBox ID="objtxtFilepath" runat="server" CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>--%>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="File Size" FooterText="colhFileSize">
                                        <HeaderStyle CssClass="text-right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <%--<asp:Label ID="objlblFilesize" runat="server" Text='<%# If(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("file_size")) = 0, "Less than 1024 KB", Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("file_size")) & " KB")  %>'></asp:Label>--%>
                                            <asp:Label ID="objlblFilesize" runat="server" Text='<%# If(AntiXss.AntiXssEncoder.HtmlEncode(Eval("file_size"), True) = 0, "Less than 1024 KB", AntiXss.AntiXssEncoder.HtmlEncode(Eval("file_size"), True) & " KB")  %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<EditItemTemplate>
                                    <asp:TextBox ID="objtxtFilesize" runat="server" CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>--%>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Download" HeaderStyle-CssClass="width90px" ItemStyle-CssClass="width90px" FooterText="colhDownload">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDownload" runat="server" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>" CausesValidation="false">Download</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Delete" FooterText="colhDelete">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="objbtnCertidelete" runat="server" CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>" Text="<i aria-hidden='true' class='fa fa-trash'></i>" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </asp:Panel>
                        &#160;<asp:Button ID="btnRemoveImage" runat="server" Text="Remove Image" Visible="false" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvQualiCerti" />
                        <asp:PostBackTrigger ControlID="flUpload" />
                        <%--<asp:AsyncPostBackTrigger ControlID="gvQualiCerti" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="gvQualiCerti" EventName="DataBound" />--%>
                    </Triggers>
                </asp:UpdatePanel>
                <%--<div id="divAttachQuali" class="panel-default">

            <div id="Div19" class="panel-heading-default panel-height">
                <div style="float: left;">
                    <asp:Label ID="lblAttachQuali" runat="server" Text="Qualification Certificate Attachments"></asp:Label>
                </div>
            </div>
            <div id="Div20" class="panel-body-default">
                <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="responsive">
                            <div class="newrow">
                                <div class="lbl">
                                    <asp:Label ID="lblDocTypeQuali" runat="server" Text="Document Type"></asp:Label>
                                </div>
                                <div class="ctrl">
                                    <uc1:dropdownlist id="ddlDocTypeQuali" runat="server" autopostback="false"></uc1:dropdownlist>
                                </div>
                                <div class="col-1-2-3-4">
                                    <div class="divlblUpload" style="float: left;">
                                        <asp:Label ID="lblUpload" runat="server" Text="Select Certificate (Image / PDF)"
                                            Style="display: block;"></asp:Label>
                                        <asp:Label ID="lblUpload2" runat="server" Text="(Max. size 1 MB, Max. attachments 3.)"></asp:Label>
                                    </div>
                                    <div class="divflUpload" style="float: left;">
                                        <uc9:fileupload id="flUpload" runat="server" maxsizekb="1048576" onclick="return IsValidAttach();" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            </div>
        </div>--%>
            </asp:Panel>


        </asp:Panel>
    </div>
    <asp:HiddenField ID="hfCancelLang" runat="server" />
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicantQualification" _ModuleName="ApplicantQualification" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
</asp:Content>
