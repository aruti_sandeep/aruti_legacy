﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ContactDetails
    
    '''<summary>
    '''pnlForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlForm As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''LanguageOpner control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LanguageOpner As Global.Aruti_Online_Recruitment.LanguageOpner
    
    '''<summary>
    '''lblCurrentAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCurrentAddress As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlAdd1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAdd1 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPresentAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentAdd As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtpresentAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpresentAdd As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlAdd2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAdd2 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPresentAdd1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentAdd1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtpresentadd2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpresentadd2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPlotno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPlotno As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPlotno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPlotno As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPlotno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPlotno As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblPlotnoMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPlotnoMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlEstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlEstate As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblEstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEstate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtEstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEstate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblEstateMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEstateMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlStreet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStreet As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblRoad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRoad As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtroad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtroad As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblRoadMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRoadMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlProvince As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProvince As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProvince As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCountry As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCountry As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblState As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlState As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlPostTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPostTown As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''UpdatePanel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel3 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblPostTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPostTown As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPostTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPostTown As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPostCode As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''UpdatePanel4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel4 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPostCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPostcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPostcode As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblTelno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelno As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txttelno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txttelno As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlFax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFax As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblFax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFax As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtFax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFax As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblFaxMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaxMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlPermAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermAddress As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPermanentAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPermanentAddress As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lnkCopyAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkCopyAdd As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''pnlPermAdd1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermAdd1 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmAdd As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtprmAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtprmAdd As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPermAdd2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermAdd2 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmAdd2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmAdd2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtprmAdd2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtprmAdd2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPermPlotno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermPlotno As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmPlotno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmPlotno As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtprmPlotno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtprmPlotno As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPermEstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermEstate As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmEstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmEstate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPrmEstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPrmEstate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPermStreet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermStreet As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmRoad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmRoad As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPrmRoad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPrmRoad As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPermProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermProvince As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmProvince As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPrmProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPrmProvince As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblPrmCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmCountry As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPrmCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPrmCountry As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''UpdatePanel5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel5 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblPrmstate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmstate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPrmState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPrmState As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlPermPostTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermPostTown As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''UpdatePanel6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel6 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblPrmposttown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmposttown As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPrmPostTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPrmPostTown As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlPermPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermPostCode As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''UpdatePanel7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel7 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''lblPrmPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmPostCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPrmPostcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPrmPostcode As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblPrmTelNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmTelNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPrmTelNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPrmTelNo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnlPermFax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPermFax As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPrmfax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrmfax As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPrmfax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPrmfax As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblPresentAdd1Msg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentAdd1Msg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPresentAdd2Msg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentAdd2Msg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPresentRegionMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentRegionMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPresentCountryMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentCountryMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPresentStateMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentStateMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPresentCityMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentCityMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPresentPostcodeMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPresentPostcodeMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblSaveMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSaveMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''hfCancelLang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCancelLang As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''LanguageControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LanguageControl As Global.Aruti_Online_Recruitment.LanguageControl
End Class
