﻿Imports System.ComponentModel.DataAnnotations
Public Class dtoAppQualiUpdate

    <Required()>
    Public Property CompCode As String
    <Required()>
    Public Property ComUnkID As Integer
    <Required()>
    Public Property ApplicantUnkid As Integer
    <Required()>
    Public Property QualificationTranUnkid As Integer
    <Required()>
    Public Property QualificationGroupUnkid As Integer
    <Required()>
    Public Property QualificationUnkid As Integer
    <Required()>
    Public Property Transaction_Date As Date
    <Required()>
    Public Property Reference_no As String
    <Required()>
    Public Property Award_start_Date As Date
    <Required()>
    Public Property Award_end_Date As Date
    <Required()>
    Public Property Instituteunkid As Integer
    <Required()>
    Public Property Resultunkid As Integer
    <Required()>
    Public Property GPACode As Decimal
    <Required()>
    Public Property Certificateno As String
    <Required()>
    Public Property Other_Qualificationgrp As String
    <Required()>
    Public Property Other_Qualification As String
    <Required()>
    Public Property Other_Institute As String
    <Required()>
    Public Property Other_Resultcode As String
    <Required()>
    Public Property Remark As String
    <Required()>
    Public Property HighestQualification As Boolean


End Class
