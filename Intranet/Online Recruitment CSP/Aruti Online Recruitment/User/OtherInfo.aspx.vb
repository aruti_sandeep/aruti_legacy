﻿Option Strict On
Imports System.Runtime.Caching

Public Class OtherInfo
    Inherits Base_Page

    Private objOtherInfo As New clsOtherInfo
    Private objContact As New clsContactDetails

#Region " Private Function "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant
        'Dim objContactDetails As New clsContactDetails
        Dim dsCombo As DataSet
        Try
            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.MARRIED_STATUS)
            With drpMaritalstatus
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.LANGUAGES)
            With drplanguage1
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            With drplanguage2
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            With drplanguage3
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            With drplanguage4
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            'dsCombo = objContactDetails.GetCountry()
            'With drpNationality
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "Country_name"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            dsCombo = objContact.GetCountry()
            With ddlCurrentCurrencyCountry
                .DataValueField = "countryunkid"
                .DataTextField = "currency"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"

            End With


            With ddlExpectCurrencyCountry
                .DataValueField = "countryunkid"
                .DataTextField = "currency"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'dsCombo = objApplicant.GetYesNo()
            'With ddlWilling_to_relocate
            '    .DataValueField = "id"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombo.Tables("YesNo")
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With

            'With ddlWilling_to_travel
            '    .DataValueField = "id"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombo.Tables("YesNo")
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        Finally
            objApplicant = Nothing
            'objContactDetails = Nothing
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        'lblError.Visible = False
        Try
            Dim decCurrentSalary As Decimal = 0
            Dim decExpectedSalary As Decimal = 0
            Dim intNoticePeriodDays As Integer = 0
            Decimal.TryParse(txtCurrent_salary.Text, decCurrentSalary)
            Decimal.TryParse(txtExpected_salary.Text, decExpectedSalary)
            Integer.TryParse(txtNotice_period_days.Text, intNoticePeriodDays)

            If CInt(drplanguage1.SelectedValue) <= 0 AndAlso CBool(Session("Language1Mandatory")) = True Then
                ShowMessage(lblLang1Msg.Text, MessageType.Errorr)
                drplanguage1.Focus()
                Return False
                'ElseIf CInt(drpNationality.SelectedValue) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                '    ShowMessage("Please Select Nationality.", MessageType.Errorr)
                '    drpNationality.Focus()
                '    Return False
            ElseIf CInt(drpMaritalstatus.SelectedValue) <= 0 AndAlso CBool(Session("MaritalStatusMandatory")) = True Then
                ShowMessage(lblMaritalStatusMsg.Text, MessageType.Errorr)
                drpMaritalstatus.Focus()
                Return False
            ElseIf dtMarrieddate.GetDate > System.DateTime.Today.Date Then
                ShowMessage(lblMarriedDateMsg.Text, MessageType.Info)
                dtMarrieddate.Focus()
                Return False
            ElseIf dtMarrieddate.GetDate <> CDate("01/Jan/1900") Then
                Dim objPersonalInfo As New clsPersonalInfo
                Dim ds As DataSet = objPersonalInfo.GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
                If ds.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(ds.Tables(0).Rows(0).Item("birth_date")) = False AndAlso dtMarrieddate.GetDate < CDate(ds.Tables(0).Rows(0).Item("birth_date")) Then
                        ShowMessage(lblMarriedDateMsg2.Text, MessageType.Info)
                        dtMarrieddate.Focus()
                        Return False
                    End If
                End If
            ElseIf chkImpaired.Checked = True AndAlso txtImpairment.Text.Trim = "" Then
                ShowMessage(lblImpairedMsg.Text, MessageType.Info)
                txtImpairment.Focus()
                Return False
            ElseIf txtMotherTongue.Text.Trim = "" AndAlso CBool(Session("MotherTongueMandatory")) = True Then
                ShowMessage(lblMotherTongueMsg.Text, MessageType.Errorr)
                txtMotherTongue.Focus()
                Return False
            ElseIf decCurrentSalary <= 0 AndAlso CBool(Session("CurrentSalaryMandatory")) = True Then
                ShowMessage(lblCurrent_salaryMsg.Text, MessageType.Errorr)
                txtCurrent_salary.Focus()
                Return False
            ElseIf decExpectedSalary <= 0 AndAlso CBool(Session("ExpectedSalaryMandatory")) = True Then
                ShowMessage(lblExpected_salaryMsg.Text, MessageType.Errorr)
                txtExpected_salary.Focus()
                Return False
            ElseIf txtExpected_benefits.Text.Trim = "" AndAlso CBool(Session("ExpectedBenefitsMandatory")) = True Then
                ShowMessage(lblExpected_benefitsMsg.Text, MessageType.Errorr)
                txtExpected_benefits.Focus()
                Return False
            ElseIf intNoticePeriodDays <= 0 AndAlso CBool(Session("NoticePeriodMandatory")) = True Then
                ShowMessage(lblNotice_period_daysMsg.Text, MessageType.Errorr)
                txtNotice_period_days.Focus()
                Return False
                'ElseIf dtpEarliest_possible_startdate.GetDate = CDate("01-Jan-1900") AndAlso CBool(Session("EarliestPossibleStartDateMandatory")) = True Then
                '    ShowMessage("Please enter Earliest Possible Start Date.", MessageType.Errorr)
                '    dtpEarliest_possible_startdate.Focus()
                '    Return False
                'ElseIf txtComments.Text.Trim = "" AndAlso CBool(Session("CommentsMandatory")) = True Then
                '    ShowMessage("Please enter Comments.", MessageType.Errorr)
                '    txtComments.Focus()
                '    Return False

                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)

            ElseIf decCurrentSalary > 0 AndAlso CInt(ddlCurrentCurrencyCountry.SelectedValue) <= 0 Then
                ShowMessage(lblCurrent_salaryMsg.Text, MessageType.Info)
                ddlCurrentCurrencyCountry.Focus()
                Return False

            ElseIf decExpectedSalary > 0 AndAlso CInt(ddlExpectCurrencyCountry.SelectedValue) <= 0 Then
                ShowMessage(lblExpected_salaryMsg.Text, MessageType.Info)
                ddlExpectCurrencyCountry.Focus()
                Return False
                'Hemant (05 july 2018) -- -- End
            ElseIf txtAchievements.Text.Trim = "" AndAlso CBool(Session("OtherInfoAchievementMandatory")) = True Then
                ShowMessage(lblAchievementsMsg.Text, MessageType.Errorr)
                txtAchievements.Focus()
                Return False
            ElseIf txtJournalsResearchPapers.Text.Trim = "" AndAlso CBool(Session("JournalsResearchPapersMandatory")) = True Then
                ShowMessage(lblJournalsResearchPapersMsg.Text, MessageType.Errorr)
                txtJournalsResearchPapers.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
    End Function

    Private Sub GetValue()
        Dim dsList As DataSet
        Try
            dsList = objOtherInfo.GetOtherInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)

            If dsList.Tables(0).Rows.Count > 0 Then

                drplanguage1.SelectedValue = dsList.Tables(0).Rows(0).Item("language1unkid").ToString
                drplanguage2.SelectedValue = dsList.Tables(0).Rows(0).Item("language2unkid").ToString
                drplanguage3.SelectedValue = dsList.Tables(0).Rows(0).Item("language3unkid").ToString
                drplanguage4.SelectedValue = dsList.Tables(0).Rows(0).Item("language4unkid").ToString
                'drpNationality.SelectedValue = dsList.Tables(0).Rows(0).Item("nationality").ToString
                txtMotherTongue.Text = dsList.Tables(0).Rows(0).Item("mother_tongue").ToString
                chkImpaired.Checked = CBool(dsList.Tables(0).Rows(0).Item("isimpaired"))
                txtImpairment.Text = dsList.Tables(0).Rows(0).Item("impairment").ToString
                Call chkImpaired_CheckedChanged(chkImpaired, New System.EventArgs)
                txtCurrent_salary.Text = Format(CDec(dsList.Tables(0).Rows(0).Item("current_salary").ToString), "##,##,##,##0.00")
                txtExpected_salary.Text = Format(CDec(dsList.Tables(0).Rows(0).Item("expected_salary").ToString), "##,##,##,##0.00")
                ddlCurrentCurrencyCountry.SelectedValue = dsList.Tables(0).Rows(0).Item("current_salary_currency_id").ToString
                ddlExpectCurrencyCountry.SelectedValue = dsList.Tables(0).Rows(0).Item("Expected_salary_currency_id").ToString
                txtExpected_benefits.Text = dsList.Tables(0).Rows(0).Item("expected_benefits").ToString
                chkWilling_to_relocate.Checked = CBool(dsList.Tables(0).Rows(0).Item("willing_to_relocate"))
                chkWilling_to_travel.Checked = CBool(dsList.Tables(0).Rows(0).Item("willing_to_travel"))
                'ddlWilling_to_relocate.SelectedValue = dsList.Tables(0).Rows(0).Item("willing_to_relocate").ToString()
                'ddlWilling_to_travel.SelectedValue = dsList.Tables(0).Rows(0).Item("willing_to_travel").ToString()
                txtNotice_period_days.Text = Format(CInt(dsList.Tables(0).Rows(0).Item("notice_period_days").ToString), "0")
                'If IsDBNull(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate")) = False AndAlso CDate(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate")).Date <> CDate("01/Jan/1900") Then
                '    dtpEarliest_possible_startdate.SetDate = CDate(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate"))
                'End If
                'txtComments.Text = dsList.Tables(0).Rows(0).Item("comments").ToString
                drpMaritalstatus.SelectedValue = dsList.Tables(0).Rows(0).Item("marital_statusunkid").ToString
                If IsDBNull(dsList.Tables(0).Rows(0).Item("anniversary_date")) = False AndAlso CDate(dsList.Tables(0).Rows(0).Item("anniversary_date")).Date <> CDate("01/Jan/1900") Then
                    dtMarrieddate.SetDate = CDate(dsList.Tables(0).Rows(0).Item("anniversary_date"))
                End If
                'txtAlterNativeno.Text = dsList.Tables(0).Rows(0).Item("present_alternateno").ToString
                txtMemberships.Text = dsList.Tables(0).Rows(0).Item("memberships").ToString
                txtAchievements.Text = dsList.Tables(0).Rows(0).Item("achievements").ToString
                txtJournalsResearchPapers.Text = dsList.Tables(0).Rows(0).Item("journalsresearchpapers").ToString

            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub setVisibility()

        Try
            If CBool(Session("MotherTongueMandatory")) = False AndAlso Session("CompCode").ToString().ToUpper() = "NMB" Then
                pnlMotherTongue.Visible = CBool(Session("MotherTongueMandatory"))
            End If

            If CBool(Session("MaritalStatusMandatory")) = False AndAlso Session("CompCode").ToString().ToUpper() = "NMB" Then
                pnlMaritalStatus.Visible = CBool(Session("MotherTongueMandatory"))
            End If

            If Session("CompCode").ToString().ToUpper() = "NMB" Then
                UpdatePanel2.Visible = False
            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If CBool(Session("Language1Mandatory")) = True Then
                    'rfvLang1.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblLang1.Text = lblLang1.Text & "*"
                    lblLang1.CssClass = "required"
                Else
                    'rfvLang1.ValidationGroup = ""
                    lblLang1.CssClass = ""
                End If

                If CBool(Session("MaritalStatusMandatory")) = True Then
                    'rfvMaritalStatus.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblMaritalStatus.Text = lblMaritalStatus.Text & "*"
                    lblMaritalStatus.CssClass = "required"
                Else
                    'rfvMaritalStatus.ValidationGroup = ""
                    lblMaritalStatus.CssClass = ""
                End If

                'If CBool(Session("NationalityMandatory")) = True Then
                '    rfvNationality.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                '    lblNationality.Text = lblNationality.Text & "*"
                'Else
                '    rfvNationality.ValidationGroup = ""
                'End If

                If CBool(Session("MotherTongueMandatory")) = True Then
                    'rfvMotherTongue.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblMotherTongue.Text = lblMotherTongue.Text & "*"
                    lblMotherTongue.CssClass = "required"
                Else
                    'rfvMotherTongue.ValidationGroup = ""
                    lblMotherTongue.CssClass = ""
                End If

                If CBool(Session("CurrentSalaryMandatory")) = True Then
                    'rfvCurrent_salary.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblCurrent_salary.Text = lblCurrent_salary.Text & "*"
                    lblCurrent_salary.CssClass = "required"
                Else
                    'rfvCurrent_salary.ValidationGroup = ""
                    lblCurrent_salary.CssClass = ""
                End If

                If CBool(Session("ExpectedSalaryMandatory")) = True Then
                    'rfvExpected_salary.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblExpected_salary.Text = lblExpected_salary.Text & "*"
                    lblExpected_salary.CssClass = "required"
                Else
                    'rfvExpected_salary.ValidationGroup = ""
                    lblExpected_salary.CssClass = ""
                    lblExpected_salary.Visible = False
                    txtExpected_salary.Visible = False
                    lblExpected_salary_Currency.Visible = False
                    ddlExpectCurrencyCountry.Visible = False
                End If

                If CBool(Session("ExpectedBenefitsMandatory")) = True Then
                    'rfvExpected_benefits.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblExpected_benefits.Text = lblExpected_benefits.Text & "*"
                    lblExpected_benefits.CssClass = "required"
                Else
                    'rfvExpected_benefits.ValidationGroup = ""
                    lblExpected_benefits.CssClass = ""
                End If

                If CBool(Session("NoticePeriodMandatory")) = True Then
                    'rfvNotice_period_days.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                    'lblNotice_period_days.Text = lblNotice_period_days.Text & "*"
                    lblNotice_period_days.CssClass = "required"
                Else
                    'rfvNotice_period_days.ValidationGroup = ""
                    lblNotice_period_days.CssClass = ""
                End If

                'If CBool(Session("EarliestPossibleStartDateMandatory")) = True Then
                '    rfvEarliest_possible_startdate.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                '    lblEarliest_possible_startdate.Text = lblEarliest_possible_startdate.Text & "*"
                'Else
                '    rfvEarliest_possible_startdate.ValidationGroup = ""
                'End If

                'If CBool(Session("CommentsMandatory")) = True Then
                '    rfvComments.ValidationGroup = btnSaveOtherDetails.ValidationGroup
                '    lblComments.Text = lblComments.Text & "*"
                'Else
                '    rfvComments.ValidationGroup = ""
                'End If

                If CBool(Session("OtherInfoAchievementMandatory")) = True Then
                    lblAchievements.CssClass = "required"
                Else
                    lblAchievements.CssClass = ""
                    pnlAchievement.Visible = False
                End If

                If CBool(Session("JournalsResearchPapersMandatory")) = True Then
                    lblJournalsResearchPapers.CssClass = "required"
                Else
                    lblJournalsResearchPapers.CssClass = ""
                    pnlJournalsResearchPaper.Visible = False
                End If

                Call GetValue()

                Call setVisibility()
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnSaveOtherDetails.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub OtherInfo_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Combobox Events "
    Private Sub drplanguage1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drplanguage1.SelectedIndexChanged _
                                                                                        , drplanguage2.SelectedIndexChanged _
                                                                                        , drplanguage3.SelectedIndexChanged _
                                                                                        , drplanguage4.SelectedIndexChanged
        Try
            Dim ddl As DropDownList = CType(sender, DropDownList)

            If CInt(ddl.SelectedValue) > 0 Then
                If ddl.ID = drplanguage1.ID Then
                    If ddl.SelectedValue = drplanguage2.SelectedValue OrElse ddl.SelectedValue = drplanguage3.SelectedValue OrElse ddl.SelectedValue = drplanguage4.SelectedValue Then
                        ddl.SelectedValue = "0"
                        ShowMessage(lblLang1Msg2.Text, MessageType.Info)
                        Exit Try
                    End If
                ElseIf ddl.ID = drplanguage2.ID Then
                    If ddl.SelectedValue = drplanguage1.SelectedValue OrElse ddl.SelectedValue = drplanguage3.SelectedValue OrElse ddl.SelectedValue = drplanguage4.SelectedValue Then
                        ddl.SelectedValue = "0"
                        ShowMessage(lblLang2Msg2.Text, MessageType.Info)
                        Exit Try
                    End If
                ElseIf ddl.ID = drplanguage3.ID Then
                    If ddl.SelectedValue = drplanguage1.SelectedValue OrElse ddl.SelectedValue = drplanguage2.SelectedValue OrElse ddl.SelectedValue = drplanguage4.SelectedValue Then
                        ddl.SelectedValue = "0"
                        ShowMessage(lblLang3Msg2.Text, MessageType.Info)
                        Exit Try
                    End If
                ElseIf ddl.ID = drplanguage4.ID Then
                    If ddl.SelectedValue = drplanguage1.SelectedValue OrElse ddl.SelectedValue = drplanguage2.SelectedValue OrElse ddl.SelectedValue = drplanguage3.SelectedValue Then
                        ddl.SelectedValue = "0"
                        ShowMessage(lblLang4Msg2.Text, MessageType.Info)
                        Exit Try
                    End If
                End If
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Button Events "

    Private Sub btnSaveOtherDetails_Click(sender As Object, e As EventArgs) Handles btnSaveOtherDetails.Click
        'lblError.Visible = False
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            Dim decCurrentSalary As Decimal = 0
            Dim decExpectedSalary As Decimal = 0
            Dim intNoticePeriodDays As Integer = 0
            Decimal.TryParse(txtCurrent_salary.Text, decCurrentSalary)
            Decimal.TryParse(txtExpected_salary.Text, decExpectedSalary)
            Integer.TryParse(txtNotice_period_days.Text, intNoticePeriodDays)

            If objOtherInfo.SaveOtherInfo(strCompCode:=Session("CompCode").ToString _
                                          , intComUnkID:=CInt(Session("companyunkid")) _
                                          , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                          , intLanguage1unkid:=CInt(drplanguage1.SelectedValue) _
                                          , intLanguage2unkid:=CInt(drplanguage2.SelectedValue) _
                                          , intLanguage3unkid:=CInt(drplanguage3.SelectedValue) _
                                          , intLanguage4unkid:=CInt(drplanguage4.SelectedValue) _
                                          , intMaritalStatusUnkid:=CInt(drpMaritalstatus.SelectedValue) _
                                          , dtAnniversaryDate:=dtMarrieddate.GetDate() _
                                          , strMemberships:=txtMemberships.Text _
                                          , strAchievements:=txtAchievements.Text _
                                          , strJournalsresearchpapers:=txtJournalsResearchPapers.Text _
                                          , strMother_tongue:=txtMotherTongue.Text _
                                          , blnIsimpaired:=chkImpaired.Checked _
                                          , strImpairment:=txtImpairment.Text _
                                          , decCurrent_Salary:=decCurrentSalary _
                                          , decExpected_Salary:=decExpectedSalary _
                                          , strExpected_benefits:=txtExpected_benefits.Text _
                                          , blnWilling_to_relocate:=CBool(chkWilling_to_relocate.Checked) _
                                          , blnWilling_to_travel:=CBool(chkWilling_to_travel.Checked) _
                                          , intNotice_period_days:=intNoticePeriodDays _
                                          , intCurrent_salary_currency_id:=CInt(ddlCurrentCurrencyCountry.SelectedValue) _
                                          , intExpected_salary_currency_id:=CInt(ddlExpectCurrencyCountry.SelectedValue)
                                          ) = True Then

                ShowMessage(lblSaveOtherDetailsMsg.Text, MessageType.Info)

                Dim strCacheKey As String = "AppOtherInfo_" & CStr(Session("CompCode")) & "_" & CInt(Session("companyunkid")).ToString & "_" & CInt(Session("applicantunkid")).ToString & "_" & CInt(clsCommon_Master.enCommonMaster.LANGUAGES).ToString & "_" & CInt(clsCommon_Master.enCommonMaster.MARRIED_STATUS).ToString
                Dim cache As ObjectCache = MemoryCache.Default
                If cache(strCacheKey) IsNot Nothing Then
                    cache.Remove(strCacheKey)
                End If

                Dim s4 As Site4 = CType(Me.Master, Site4)
                s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End

#End Region

#Region " Checkbox Events "
    Private Sub chkImpaired_CheckedChanged(sender As Object, e As EventArgs) Handles chkImpaired.CheckedChanged
        Try
            If chkImpaired.Checked = True Then
                txtImpairment.ReadOnly = False
                'rfvImpairment.ValidationGroup = "OtherInfo"
            Else
                txtImpairment.Text = ""
                txtImpairment.ReadOnly = True
                'rfvImpairment.ValidationGroup = ""
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

End Class