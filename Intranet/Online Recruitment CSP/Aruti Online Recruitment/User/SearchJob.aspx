﻿<%@ Page Title="Search Vacancies" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="SearchJob.aspx.vb" Inherits="Aruti_Online_Recruitment.SearchJob" %>

<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>
<%@ Register Src="../Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                $('.adetails').on('click', function () {
                    toggle(this, false);
                });

                $('.sthf').on('click', function () {
                    sethf(this);
                });

                $("[id*=chkVacancyFoundOutFrom]").on('click', function () {
                    sethf(this);
                    __doPostBack($(this).get(0).id, "onCheckedChanged");
                });

                $("[id*=ddlDocTypeCV]").on('change', function () {
                    sethf(this);
                    __doPostBack($(this).get(0).id, 0);
                });

                $("[id*=ddlDocTypeCoverLetter]").on('change', function () {
                    sethf(this);
                    __doPostBack($(this).get(0).id, 0);
                });

                $("[id*=ddlDocTypeQuali]").on('change', function () {
                    sethf(this);
                    __doPostBack($(this).get(0).id, 0);
                });

                gethf();

                setEvents();
            });
        };


        $(document).ready(function () {

            $('.adetails').on('click', function () {
                toggle(this, false);
            });

            $('.sthf').on('click', function () {
                sethf(this);
            });

            $("[id*=chkVacancyFoundOutFrom]").on('click', function () {
                sethf(this);
                __doPostBack($(this).get(0).id, "onCheckedChanged");
            });

            $("[id*=ddlDocTypeCV]").on('change', function () {
                sethf(this);
                __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=ddlDocTypeCoverLetter]").on('change', function () {
                sethf(this);
                __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=ddlDocTypeQuali]").on('change', function () {
                sethf(this);
                __doPostBack($(this).get(0).id, 0);
            });

            gethf();

            setEvents();
        });

        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        function toggle(obj, ispostback = false) {

            $(obj).closest('.panel-default').children('#moretext').toggle('slow', function () {
                if (ispostback == false) {
                    setScrollPosition($(window).scrollTop());
                }
                $(window).scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
            });

            if ($(obj).children('span').hasClass('fa-plus-circle') == true) {
                $(obj).children('span').removeClass('fa-plus-circle');
                $(obj).children('span').addClass('fa-minus-circle');
                $(obj).children('div').text('Hide Details');
                $($(obj).closest('.panel-default').children('#moretext')).removeClass('d-none');
            }
            else {
                $(obj).children('span').removeClass('fa-minus-circle');
                $(obj).children('span').addClass('fa-plus-circle');
                $(obj).children('div').text('Show Details');
                $($(obj).closest('.panel-default').children('#moretext')).addClass('d-none');
            }
        }

        function gethf() {
            if ($('#<% = hf.ClientID %>').val() != '') {
                toggle($('#' + $('#<% = hf.ClientID %>').val()).closest('#moretext').siblings().find('#adetails'), true);
            }
        }

        function sethf(obj) {
            $('#<% = hf.ClientID %>').val($(obj).attr('id'));
            setScrollPosition($(window).scrollTop());
        }

        function setScrollPosition(scrollValue) {
            $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }

    </script>

    <div class="card">
        <asp:Panel ID="pnlForm" runat="server">
            <%-- <div style="float: right">
                
            </div>--%>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <%--<div class="form-group row">
                    <div class="col-md-8">
                    <h3>Vacancies <asp:Label ID="lblCount" runat="server" Text=""></asp:Label> </h3>
                        </div>
                    <div class="col-md-4" style="text-align:right;padding-right:15px;color:white;">                        
                         <a href="Preview.aspx" target="_blank" class="btn btn-primary">Preview Resume</a>                       
                    </div>
                </div> --%>
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                <asp:Label ID="lblHeader" runat="server">Vacancies</asp:Label>&nbsp;<asp:Label ID="objlblCount" runat="server" Text=""></asp:Label>
                                <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                            </h4>
                            <div class="form-group row mb-0">
                                <div class="col-md-11">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                        <asp:TextBox ID="txtVacanciesSearch" runat="server" CssClass="form-control" placeholder="Search Vacancy / Company / Skill / Qualification / Responsibility / Job Description" />
                                    </div>
                                </div>
                                <div class="col-md-1 text-right p-0">
                                    <asp:Button ID="btnSearchVacancies" runat="server" Text="Search" CssClass="btn btn-primary" />
                                </div>
                            </div>
                        </div>

                        <div class="card-body pb-0">
                            <input id="hf" runat="server" type="hidden" />
                            <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
                            <asp:HiddenField ID="hfCancelLang" runat="server" />
                            <asp:DataList ID="dlVaanciesList" runat="server" CssClass="w-100 vacancy" DataKeyField="vacancyid" DataSourceID="odsVacancy">
                                <ItemTemplate>
                                    <div class="form-group row">
                                                <div class="col-md-12">
                                    <div class="card mb-3 panel-default">
                                        <div class="card-header ">
                                            <div class="form-group row mb-0">
                                                <div class="col-md-12">
                                                    <div class="form-group row mb-0">
                                                        <div class="d-none">
                                                            <div class="far fa-hand-point-right"></div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <h4>
                                                            <%--<asp:Label ID="lblVacancyTitle" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("vacancytitle")) %>' ></asp:Label>--%>
                                                            <asp:Label ID="objlblVacancyTitle" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("vacancytitle"), True) %>'></asp:Label>
                                                            <%--  'Pinkal (01-Jan-2018) -- Start
                                                        'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.--%>
                                                            <%--<asp:HiddenField ID = "hdnfieldVacancyTitleId" runat="server" Value ='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("vacancytitleid"))%>' />--%>
                                                            <asp:HiddenField ID="hdnfieldVacancyTitleId" runat="server" Value='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("vacancytitleid"), True)%>' />
                                                            <%--Pinkal (01-Jan-2018) -- Start--%>
                                                                <asp:HiddenField ID="hfVTitle" runat="server" Value='<%# Eval("vacancytitle") %>'></asp:HiddenField>
                                                                </h4>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 text-right">
                                                            <div id="adetails" class="adetails cursor-pointer">
                                                                <span class="fa fa-plus-circle pr-1" aria-hidden="true"></span>
                                                                <div id="lblShowDetail" class="float-right">Show Details</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 d-none">
                                                    <div class="row d-none">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-9">
                                                            <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Session("CompName").ToString) %>--%>
                                                            <asp:Label ID="objlblCompName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Session("CompName").ToString, True) %>'></asp:Label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="moretext" class="d-none">
                                            <div class="card-body">
                                                <div class="form-group row" id="divJobLocation" runat="server">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="lblJobLocation" runat="server" Text="Job Location :" CssClass="font-weight-bold"></asp:Label>                                                                    
                                                    </div>
                                                    <div class="col-md-10">
                                                        <asp:Label ID="objlblJobLocation" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group row" id="divRemark" runat="server">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="lblJobDiscription" runat="server" Text="Job Description :" CssClass="font-weight-bold"></asp:Label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <asp:Label ID="objlblJobDiscription" runat="server" Text='<%# Eval("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %>'></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group row" id="divResponsblity" runat="server">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="lblResponsblity" runat="server" Text="Responsiblity: " CssClass="font-weight-bold"></asp:Label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <asp:Label ID="objlblResponsblity" runat="server" Text='<%#Eval("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %> '></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group row" id="divSkill" runat="server">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="lblSkill" runat="server" Text="Skill Required :" CssClass="font-weight-bold"></asp:Label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <asp:Label ID="objlblSkill" runat="server" Text='<%# Eval("skill") %>'></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group row" id="divQualification" runat="server">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="lblQualification" runat="server" Text="Qualification Required: " CssClass="font-weight-bold"></asp:Label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Qualification")) %>--%>
                                                        <asp:Label ID="objlblQualification" runat="server" Text='<%# Eval("Qualification") %>'></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group row" id="divExp"  runat="server">
                                                    <div class="col-md-2" >
                                                        <asp:Label ID="lblExp" runat="server" Text="Experience :" CssClass="font-weight-bold"></asp:Label>
                                                        <%--<%# IIf(Eval("experience") <> 0, Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Format(CDec(Eval("experience")) / 12, "###0.0#")) + " Year(s)", Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment("Fresher Can Apply")) %>--%>                                                        
                                                        <asp:HiddenField ID="hfExp" runat="server" Value='<%# Eval("experience") %>'></asp:HiddenField>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <asp:Label ID="objlblExp" runat="server" Text='<%# IIf(Eval("experience") <> 0, AntiXss.AntiXssEncoder.HtmlEncode(Format(CDec(Eval("experience")) / 12, "###0.0#"), True) + " Year(s)", AntiXss.AntiXssEncoder.HtmlEncode("Fresher Can Apply", True)) %>'></asp:Label>
                                                        <%--<br />--%>
                                                        <asp:Label ID="objlblExpCmt" runat="server" Text='<%# Eval("experience_comment")%>'></asp:Label>
                                                    </div>
                                                    <div class="col-md-6 d-none" id="divNoPosition" runat="server">
                                                        <asp:Label ID="lblNoPosition" runat="server" Text="No. of Position :" CssClass="font-weight-bold"></asp:Label>
                                                        <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("noposition")) %>--%>
                                                        <asp:Label ID="objlblNoPosition" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("noposition"), True) %>'></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group row" id="divLang" runat="server">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="lblLang" runat="server" Text="Preferred Language Skill :" CssClass="font-weight-bold"></asp:Label>                                                        
                                                    </div>
                                                    <div class="col-md-10">
                                                        <asp:Label ID="objlblLang" runat="server" Text='<%# Eval("Lang") %>'></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group row" id="divScale" runat="server">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="lblScale" runat="server" Text="Scale :" CssClass="font-weight-bold"></asp:Label>
                                                        <asp:Label ID="objlblScale" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-6" id="divOpenningDate" runat="server">
                                                        <asp:Label ID="lblOpeningDate" runat="server" Text="Job Opening date :" CssClass="font-weight-bold"></asp:Label>
                                                        <asp:Label ID="objlblOpeningDate" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("openingdate")).ToShortDateString(), True) %>'></asp:Label>
                                                    </div>
                                                    <div class="col-md-6" id="divClosuingDate" runat="server">
                                                        <asp:Label ID="lblClosingDate" runat="server" Text="Job closing date :" CssClass="font-weight-bold"></asp:Label>
                                                        <asp:Label ID="objlblClosingDate" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("closingdate")).ToShortDateString(), True) %>'></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group row" runat="server">

                                                    <%--<div class="col-md-4">--%>
                                                    <asp:Panel ID="pnlVacancyFoundCol" runat="server" CssClass="col-md-4">
                                                        <div class="row m-0 p-0">
                                                            <div class="col-md-8 m-0 p-0">
                                                                <asp:Label ID="lblVacancyFoundOutFrom" runat="server" Text="Vacancy Found Out From: "></asp:Label>
                                                            </div>
                                                            <div class="col-md-4 m-0 p-0 text-right">
                                                                <asp:CheckBox ID="chkVacancyFoundOutFrom" runat="server" Text="Others" OnCheckedChanged="chkGridOther_CheckedChanged" AutoPostBack="false" />
                                                            </div>
                                                        </div>

                                                        <div class="row m-0 p-0">
                                                            <div class="col-md-12 m-0 p-0">
                                                                <asp:Panel ID="pnlVacancyFoundOutFrom" runat="server">
                                                                    <asp:DropDownList ID="ddlVacancyFoundOutFrom" runat="server" CssClass="selectpicker form-control" />
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlOtherVacancyFoundOutFrom" runat="server" Visible="false">
                                                                    <asp:TextBox ID="txtVacancyFoundOutFrom" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <%--</div>--%>

                                                    <%--<div class="col-md-4 col-xs-12 col-md-offset-0">--%>
                                                    <asp:Panel ID="pnlEarliestPossibleStartDateCol" runat="server" CssClass="col-md-4">
                                                        <asp:Label ID="lblEarliestPossibleStartDate" runat="server" Text="Earliest Possible Start Date: "></asp:Label>
                                                        <uc2:DateCtrl ID="dtpEarliestPossibleStartDate" runat="server" AutoPostBack="false" CssClass="form-control" />
                                                    </asp:Panel>
                                                    <%--</div>--%>

                                                    <%--<div class="col-md-4">--%>
                                                    <asp:Panel ID="pnlCommentsCol" runat="server" CssClass="col-md-4">
                                                        <asp:Label ID="lblComments" runat="server" Text="Comments: "></asp:Label>
                                                        <asp:TextBox ID="txtComments" runat="server" Text="" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                    </asp:Panel>
                                                    <%--</div>--%>
                                                </div>

                                                <asp:Panel ID="pnlAttachMsg" runat="server" >
                                                    <asp:Label ID="lblUpload" runat="server" Font-Bold="true" Text="Select file (Image / PDF) (All file size should not exceed more than 5 MB.)" CssClass="d-block">   <%--Style="display: block;"--%>
                                                    </asp:Label>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlCV" runat="server" CssClass="form-group row attachdoc">
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblDocTypeCV" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                                                <asp:DropDownList ID="ddlDocTypeCV" runat="server" AutoPostBack="false" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDocTypeCV_OnSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblAttachTypeCV" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                                        <asp:DropDownList ID="ddlAttachTypeCV" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                                        <asp:Label ID="lblAttachTypeCVMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
                                                    </div>
                                                    <div class="col-md-2 mt-3" >
                                                        <%--<asp:UpdatePanel ID="UpdatePanel21" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>--%>
                                                                <uc9:FileUpload ID="flUploadCV" runat="server" MaxSizeKB="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click"   />
                                                            <%--</ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="btnSearchVacancies" EventName="Click" />
                                                                <asp:PostBackTrigger ControlID="flUploadCV" />
                                                                <asp:PostBackTrigger ControlID="btnApply" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                    </div>
                                                    <div class="col-md-4 mt-2">
                                                        <asp:Panel ID="pnlFileCV" runat="server" CssClass="card rounded" >
                                                            <div class="row no-gutters m-0" >
                                                                <div class="col-md-3 text-center pt-3 fileext">
                                                                    <asp:Label ID="lblFileExtCV" runat="server" Text="PDF" ></asp:Label>
                                                                </div>
                                                                <div class="col-md-9">                                                                    
                                                                    <div class="card-body py-1 pr-2">
                                                                        <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                                                <h6 class="card-title m-0">
                                                                                    <asp:Label ID="lblFileNameCV" runat="server" Text=""></asp:Label></h6>
                                                                                <p class="card-text">
                                                                                    <small class="text-muted">
                                                                                        <asp:Label ID="lblDocSizeCV" runat="server" Text="0.0 MB"></asp:Label></small>
                                                                                </p>
                                                                        <asp:HiddenField ID="hfDocSizeCV" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hfFileNameCV" runat="server" Value="" />
                                                                    </div>                                                                    
                                                                </div>
                                                            </div>                                                            
                                                        </asp:Panel>
                                                        <%--<asp:Label ID="lblPathCV" runat="server" Text="Path" CssClass="required"></asp:Label><br />
                                                        <asp:TextBox ID="txtFilePathCV" runat="server" ReadOnly="true" style="width:80%" ></asp:TextBox>--%>
                                                    </div>                                                    
                                                    <%--<div class="col-md-8">
                                                        <asp:Label ID="objlblRequired" runat="server" Text="" CssClass="text-danger"></asp:Label>
                                                    </div>--%>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlCoverLetter" runat="server" CssClass="form-group row attachdoc">
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblDocTypeCoverLetter" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                                                <asp:DropDownList ID="ddlDocTypeCoverLetter" runat="server" AutoPostBack="true" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDocTypeCoverLetter_OnSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblAttachTypeCoverLetter" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                                        <asp:DropDownList ID="ddlAttachTypeCoverLetter" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>                                                        
                                                    </div>
                                                    <div class="col-md-2 mt-3" >
                                                        <uc9:FileUpload ID="flUploadCoverLetter" runat="server" MaxSizeKB="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click"   />
                                                    </div>
                                                    <div class="col-md-4 mt-2">
                                                        <asp:Panel ID="pnlFileCoverLetter" runat="server" CssClass="card rounded" >
                                                            <div class="row no-gutters m-0" >
                                                                <div class="col-md-3 text-center pt-3 fileext">
                                                                    <asp:Label ID="lblFileExtCoverLetter" runat="server" Text="PDF" ></asp:Label>
                                                                </div>
                                                                <div class="col-md-9">                                                                    
                                                                    <div class="card-body py-1 pr-2">
                                                                        <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                                                <h6 class="card-title m-0">
                                                                                    <asp:Label ID="lblFileNameCoverLetter" runat="server" Text=""></asp:Label></h6>
                                                                                <p class="card-text">
                                                                                    <small class="text-muted">
                                                                                        <asp:Label ID="lblDocSizeCoverLetter" runat="server" Text="0.0 MB"></asp:Label></small>
                                                                                </p>
                                                                        <asp:HiddenField ID="hfDocSizeCoverLetter" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hfFileNameCoverLetter" runat="server" Value="" />
                                                                    </div>                                                                    
                                                                </div>
                                                            </div>                                                            
                                                        </asp:Panel>
                                                        <%--<asp:Label ID="lblPathCoverLetter" runat="server" Text="Path" CssClass="required"></asp:Label><br />
                                                        <asp:TextBox ID="txtFilePathCoverLetter" runat="server" ReadOnly="true" style="width:80%" ></asp:TextBox>--%>
                                                    </div>                                                                                                        
                                                </asp:Panel>
                                                        <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                                                <asp:Panel ID="pnlQuali" runat="server" CssClass="form-group row attachdoc">
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblDocTypeQuali" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                                                <asp:DropDownList ID="ddlDocTypeQuali" runat="server" AutoPostBack="true" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDocTypeQuali_OnSelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblAttachTypeQuali" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                                        <asp:DropDownList ID="ddlAttachTypeQuali" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-2 mt-3" >
                                                        <uc9:FileUpload ID="flUploadQuali" runat="server" MaxSizeKB="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click"   />
                                                    </div>
                                                    <div class="col-md-4 mt-2">
                                                        <asp:Panel ID="pnlFileQuali" runat="server" CssClass="card rounded" >
                                                            <div class="row no-gutters m-0" >
                                                                <div class="col-md-3 text-center pt-3 fileext">
                                                                    <asp:Label ID="lblFileExtQuali" runat="server" Text="PDF" ></asp:Label>
                                                                </div>
                                                                <div class="col-md-9">                                                                    
                                                                    <div class="card-body py-1 pr-2">
                                                                        <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                                                <h6 class="card-title m-0">
                                                                                    <asp:Label ID="lblFileNameQuali" runat="server" Text=""></asp:Label></h6>
                                                                                <p class="card-text">
                                                                                    <small class="text-muted">
                                                                                        <asp:Label ID="lblDocSizeQuali" runat="server" Text="0.0 MB"></asp:Label></small>
                                                                                </p>
                                                                        <asp:HiddenField ID="hfDocSizeQuali" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hfFileNameQuali" runat="server" Value="" />
                                                                    </div>                                                                    
                                                                </div>
                                                            </div>                                                            
                                                        </asp:Panel>
                                                        <%--<asp:Label ID="lblFilePathQuali" runat="server" Text="Path" CssClass="required"></asp:Label><br />
                                                        <asp:TextBox ID="txtFilePathQuali" runat="server" ReadOnly="true" style="width:80%" ></asp:TextBox>--%>
                                                    </div>                                                                                                        
                                                </asp:Panel>
                                                        <% End If%>
                                            </div>
                                            

                                            <% If AntiXss.AntiXssEncoder.HtmlEncode(Session("applicant_declaration"), True).ToString.Trim.Length > 0 THEN %>
                                            <asp:Panel ID="pnlDis" runat="server">
                                                <div class="card-body  bg-light p-1">
                                                    <div class="form-group row text-center mb-1">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="lblValidate" runat="server" Text="Declaration" CssClass="font-weight-bold h6"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12 px-4 text-justify">
                                                        <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Session("applicant_declaration").ToString).ToString %>--%>
                                                        <asp:Label ID="objlblDeclare" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Session("applicant_declaration").ToString, True).ToString %>'></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group row text-center">
                                                    <div class="col-md-12">
                                                        <asp:CheckBox ID="chkAccept" runat="server" Text="   I Accept this Declaration." AutoPostBack="false" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <% End If %>

                                            <div class="card-footer  bg-light">
                                                <div class="form-group row mb-1">
                                                    <div class="col-md-12 text-right">
                                                        <div>
                                                            <%--<asp:Label ID="lblIsApplyed" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApplied"))  %>' Visible="false"></asp:Label>--%>
                                                            <asp:Label ID="objlblIsApplyed" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApplied"), True)  %>' Visible="false"></asp:Label>
                                                            <asp:LinkButton ID="btnApply" runat="server" CommandName="Apply" CssClass="btn btn-primary sthf text-decoration-none" Text="">
                                                                <div id="divApplied" runat="server" class="fa fa-check mr-1" visible="false"></div>
                                                                <asp:Label ID="lblApplyText" runat="server" Text="Apply Now"></asp:Label>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                            </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>

                            <asp:Panel ID="pnlNoVacancy" runat="server" Visible="false">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <h5>
                                            <asp:Label ID="lblNVMsg" runat="server" Text="There are no open vacancies, Click on the login button below if you wish to update your profile."></asp:Label></h5>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <asp:Button ID="btnLoginBioData" runat="server" Text="Login" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>


                    <asp:ObjectDataSource ID="odsVacancy" runat="server" SelectMethod="GetApplicantVacancies" TypeName="Aruti_Online_Recruitment.clsSearchJob" EnablePaging="false">

                        <SelectParameters>
                            <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                            <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="intMasterTypeId" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="intEType" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="blnVacancyType" Type="Boolean" DefaultValue="True" />
                            <asp:Parameter Name="blnAllVacancy" Type="Boolean" DefaultValue="False" />
                            <asp:Parameter Name="intDateZoneDifference" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="strVacancyUnkIdLIs" Type="String" DefaultValue="" />
                            <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="blnOnlyCurrent" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearchVacancies" EventName="Click" />
                    <asp:PostBackTrigger ControlID="dlVaanciesList" />                    
                </Triggers>
            </asp:UpdatePanel>
            <asp:Label ID="lblProfileMsg" runat="server" Text="Sorry, Your profile is incomplete. \n Please fill required details." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblVacFoundMsg" runat="server" Text="Please enter Vacancy Found Out From." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblVacFoundMsg2" runat="server" Text="Please select Vacancy Found Out From." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblPosStartMsg" runat="server" Text="Please enter Earliest Possible Start Date." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblPosStartMsg2" runat="server" Text="Earliest Possible Start Date should be greater than current date." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblCommentsMsg" runat="server" Text="Please enter Comments." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblAcceptMsg" runat="server" Text="Please accept the Declaration to proceed." CssClass="d-none"></asp:Label>
            <asp:Label ID="lblJobSuccessMsg" runat="server" Text="Job Applied Successfully !" CssClass="d-none"></asp:Label>
            <asp:Label ID="lblPositionMsg" runat="server" Text="Position(s)" CssClass="d-none"></asp:Label>
            <asp:Label ID="lblDocTypeMsg" runat="server" CssClass="d-none" Text="Please select Document Type."></asp:Label>
            <asp:Label ID="lblAttachTypeMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
            <asp:Label ID="lblAttachImageMsg" runat="server" CssClass="d-none" Text="Please select proper Image file or PDF file."></asp:Label>
            <asp:Label ID="lblAttachSizeMsg" runat="server" CssClass="d-none" Text="Sorry,You cannot upload file greater than #maxsize# MB."></asp:Label>
            <asp:Label ID="lblAttachMsg" runat="server" CssClass="d-none" Text="Sorry, #attachmenttype# attachment is mandatory."></asp:Label>
            <asp:Label ID="lblExpMsg" runat="server" Text="Sorry, you cannot apply for this job; you do not have the required experience." CssClass="d-none"></asp:Label>

            <asp:Label ID="lblTRABirthCertificateAttachement" runat="server" Text="Sorry, you cannot apply for this job; you do not attached birth certificate.Please Attach birth ceftificate in Personal Info Tab." CssClass="d-none"></asp:Label>

        </asp:Panel>        
    </div>
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsSearchJob" _ModuleName="SearchJob" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />

    <script type="text/javascript">

        function setEvents() {

            $("[id*=_image_file]").each(function () {
                fileUpLoadChange('#' + this.id)
            });

            $("[id*=_image_file]").each(function () {
                $(this).on('click', function () {
                    sethf(this);
                    return IsValidAttach('#' + this.id);
                });                
            });


            $('.close').on('click', function () {
                $(this).parents('[id*=pnlFile]').find('span[id*=lblFileName]').text('');
                $(this).parents('[id*=pnlFile]').find('input[id*=hfFileName]').val('');
                $(this).parents('[id*=pnlFile]').hide();
            });
            
        }

        function IsValidAttach(objFile) {

            if ($(objFile).parents('.attachdoc:first').find("select[id*='ddlDocType']").val() <= 0) {
                ShowMessage($$('lblDocTypeMsg').text(), 'MessageType.Errorr');
                $(objFile).parents('.attachdoc:first').find("select[id*='ddlDocType']").focus();
                return false;
            }
            else if ($(objFile).parents('.attachdoc:first').find("select[id*='ddlAttachType']").val() <= 0) {
                ShowMessage($$('lblAttachTypeMsg').text(), 'MessageType.Errorr');
                $(objFile).parents('.attachdoc:first').find("select[id*='ddlAttachType']").focus();
                return false;
            }            
            return true;
        }
    </script>

</asp:Content>

