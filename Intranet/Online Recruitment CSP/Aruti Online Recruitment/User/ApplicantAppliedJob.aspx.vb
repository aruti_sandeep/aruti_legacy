﻿Imports System.Net.WebRequestMethods
Imports System.Runtime.InteropServices
Imports Microsoft.Azure.KeyVault.Core
Imports Org.BouncyCastle.Crypto

Public Class ApplicantAppliedJob
    Inherits Base_Page

#Region "Provate Variable"
    'Private objApplicantAppliedJob As New clsApplicantApplyJob

    'S.SANDEEP |01-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-960
    Private mDicDeletedAttachment As New Dictionary(Of Integer, String)
    'S.SANDEEP |01-JUN-2023| -- END

#End Region
#Region " Method Functions "

    Private Sub FillVacancyList()
        Dim dsList As DataSet = Nothing
        Try
            'dsList = objApplicantAppliedJob.GetApplicantAppliedJob(strCompCode:=Session("CompCode").ToString,
            '                                                       intComUnkID:=CInt(Session("companyunkid")),
            '                                                       intApplicantUnkId:=CInt(Session("applicantunkid")))
            'dlVaanciesList.DataSource = dsList.Tables(0)
            'dlVaanciesList.DataBind()
            odsVacancy.SelectParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            odsVacancy.SelectParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid"))
            odsVacancy.SelectParameters.Item("intApplicantUnkId").DefaultValue = CInt(Session("applicantunkid"))
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub




#End Region
#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            Dim blnIsAdmin As Boolean = False
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                        blnIsAdmin = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                            blnIsAdmin = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                    blnIsAdmin = True
                End If
            End If
            'If lo = False Then LanguageOpner.Visible = False
            'If lc = False Then LanguageControl.Visible = False
            LanguageOpner.Visible = False
            LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Call FillVacancyList()
            End If
            'S.SANDEEP |01-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-960
            If ViewState("mDicDeletedAttachment") IsNot Nothing Then mDicDeletedAttachment = ViewState("mDicDeletedAttachment")
            'S.SANDEEP |01-JUN-2023| -- END
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ApplicantAppliedJob_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            'S.SANDEEP |01-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-960
            ViewState("mDicDeletedAttachment") = mDicDeletedAttachment
            'S.SANDEEP |01-JUN-2023| -- END
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

    'S.SANDEEP |01-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-960    
#Region " Button Event(s) "
    Protected Sub flUpload_btnUpload_Click(sender As Object, e As EventArgs)
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim flUpload As FileUpload = CType(CType(sender, Button).Parent, FileUpload)

            If flUpload.HasFile = False Then Exit Try

            Dim ddlDocType As DropDownList = CType(CType(sender, Button).Parent.Parent.FindControl("ddlDocType" & CType(sender, Button).Parent.Parent.ID.Substring(3)), DropDownList)
            Dim ddlAttachType As DropDownList = CType(CType(sender, Button).Parent.Parent.FindControl("ddlAttachType" & CType(sender, Button).Parent.Parent.ID.Substring(3)), DropDownList)
            'Dim txtFilePath As TextBox = CType(CType(sender, Button).Parent.Parent.Parent.Parent.FindControl("txtFilePath" & CType(sender, Button).Parent.Parent.ID.Substring(3)), TextBox)
            Dim txtFilePath As Label = CType(CType(sender, Button).Parent.Parent.FindControl("lblFileName" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Label)
            Dim hfFileName As HiddenField = CType(CType(sender, Button).Parent.Parent.FindControl("hfFileName" & CType(sender, Button).Parent.Parent.ID.Substring(3)), HiddenField)
            Dim lblDocSize As Label = CType(CType(sender, Button).Parent.Parent.FindControl("lblDocSize" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Label)
            Dim hfDocSize As HiddenField = CType(CType(sender, Button).Parent.Parent.FindControl("hfDocSize" & CType(sender, Button).Parent.Parent.ID.Substring(3)), HiddenField)
            Dim lblFileExt As Label = CType(CType(sender, Button).Parent.Parent.FindControl("lblFileExt" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Label)
            Dim pnlFile As Panel = CType(CType(sender, Button).Parent.Parent.FindControl("pnlFile" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Panel)

            'S.SANDEEP |01-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-960
            Dim hftranid As HiddenField = CType(CType(sender, Button).Parent.Parent.FindControl("hftranid" & CType(sender, Button).Parent.Parent.ID.Substring(3)), HiddenField)
            'S.SANDEEP |01-JUN-2023| -- END

            txtFilePath.Text = ""
            hfFileName.Value = txtFilePath.Text
            pnlFile.Visible = False

            If CInt(ddlDocType.SelectedValue) <= 0 Then
                ShowMessage(lblDocTypeMsg.Text, MessageType.Info)
                ddlDocType.Focus()
                Exit Sub
            ElseIf CInt(ddlAttachType.SelectedValue) <= 0 Then
                ShowMessage(lblAttachTypeMsg.Text, MessageType.Info)
                ddlAttachType.Focus()
                Exit Sub
            End If

            Dim blnInvalidFile As Boolean = False
            Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG", "JPEG", "BMP", "GIF", "PNG"
                    If flUpload.AllowImageFile = False Then
                        blnInvalidFile = True
                    End If
                Case "DOC", "DOCX"
                    If flUpload.AllowDocumentFile = False Then
                        blnInvalidFile = True
                    End If
                Case "PDF"
                Case Else
                    blnInvalidFile = True
                    'ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                    'Exit Try
            End Select

            If blnInvalidFile = True Then
                ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                Exit Try
            End If

            'Dim mintByes As Integer = CInt(flUpload.MaxSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            Dim mintByes As Integer = CInt(flUpload.RemainingSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            If flUpload.FileBytes.LongLength > mintByes Then   ' 6 MB = 6291456 Bytes
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            txtFilePath.Text = flUpload.PostedFile.FileName
            lblFileExt.Text = flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
            lblDocSize.Text = Format(((flUpload.FileBytes.LongLength / 1024) / 1024), "0.00") & " MB."
            hfDocSize.Value = flUpload.FileBytes.LongLength
            hfFileName.Value = txtFilePath.Text
            pnlFile.Visible = True
            'S.SANDEEP |01-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-960
            hftranid.Value = "0"
            'S.SANDEEP |01-JUN-2023| -- END
            Session(flUpload.ClientID) = flUpload.PostedFile

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

    'S.SANDEEP |01-JUN-2023| -- END

#Region " DataList Events "

    Private Sub dlVaanciesList_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlVaanciesList.ItemCommand
        Dim objApplicantAppliedJob As New clsApplicantApplyJob
        Try
            If e.CommandName.ToUpper = "DELETE" Then
                If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())



                If objApplicantAppliedJob.DeleteApplicantAppliedJob(strCompCode:=Session("CompCode").ToString,
                                                                    intComUnkID:=CInt(Session("companyunkid")),
                                                                    intApplicantUnkid:=CInt(Session("applicantunkid")),
                                                                    intVacancyTranUnkId:=CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) = True Then



                    'Pinkal (01-Jan-2018) -- Start
                    'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.

                    'If CBool(Session("isvacancyalert")) Then
                    '    Dim mintVacancyTitleID As Integer = CInt(CType(dlVaanciesList.Items(e.Item.ItemIndex).FindControl("hdnfieldVacancyTitleId"), HiddenField).Value)

                    '    Dim objAppNotificationSettings As New clsNotificationSettings
                    '    Dim dsList As DataSet = objAppNotificationSettings.GetApplicantNotificationSettings(xCompanyID:=CInt(Session("companyunkid")), xCompanyCode:=Session("CompCode").ToString() _
                    '                                                                                                                                    , xApplicantID:=CInt(Session("applicantunkid")), intMasterType:=clsCommon_Master.enCommonMaster.VACANCY_MASTER)

                    '    Dim drRow() As DataRow = dsList.Tables(0).Select("isactive = 1 AND priority > 0 AND vacancytitleunkid  =  " & mintVacancyTitleID)
                    '    If drRow.Length > 0 Then
                    '        objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(xCompanyID:=CInt(Session("companyunkid")), xCompanyCode:=Session("CompCode").ToString(), xApplicantID:=CInt(Session("applicantunkid")) _
                    '                                                                                                              , xVacancytitleId:=mintVacancyTitleID, xPriority:=0, xIsActive:=False)
                    '        objAppNotificationSettings = Nothing
                    '    End If
                    'End If

                    'Pinkal (01-Jan-2018) -- End

                    dlVaanciesList.DataBind()

                    ShowMessage("Applied Job Deleted Successfully !", MessageType.Info)
                End If

                Call FillVacancyList()

                'S.SANDEEP |01-JUN-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-960
            ElseIf e.CommandName.ToUpper = "UPDATE" Then

                Dim iFlag As Boolean = False
                Dim Idx As Integer = e.Item.ItemIndex
                Dim lstAttach As New List(Of dtoAddAppAttach)
                Dim dtoAttach As dtoAddAppAttach = Nothing
                Dim flUploadCV As FileUpload = Nothing
                Dim flUploadCoverLetter As FileUpload = Nothing
                Dim flUploadQuali As FileUpload = Nothing

                If CBool(Session("OneCurriculamVitaeMandatory")) = True Then
                    Dim txtFilePath As Label = CType(dlVaanciesList.Items(Idx).FindControl("lblFileNameCV"), Label)
                    If txtFilePath.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Curriculam Vitae"), MessageType.Errorr)
                        Exit Try
                    End If

                    Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadCV"), FileUpload)
                    flUploadCV = flUpload
                    Dim ddlDocType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlDocTypeCV"), DropDownList)
                    Dim ddlAttachType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlAttachTypeCV"), DropDownList)
                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("CVSavePath")
                    Dim hfDocSize As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfDocSizeCV"), HiddenField)
                    'S.SANDEEP |01-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-960
                    'Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                    'Dim decSize As Decimal = 0
                    'Decimal.TryParse(hfDocSize.Value, decSize)

                    'dtoAttach = New dtoAddAppAttach
                    'With dtoAttach
                    '    .CompCode = Session("CompCode").ToString
                    '    .ComUnkID = CInt(Session("companyunkid"))
                    '    .ApplicantUnkid = CInt(Session("applicantunkid"))
                    '    .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                    '    .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                    '    .AttachrefId = CInt(ddlDocType.SelectedValue)
                    '    .FolderName = strFolder
                    '    .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                    '    .Filename = txtFilePath.Text
                    '    .Fileuniquename = strUnkImgName
                    '    .File_size = CInt(decSize / 1024)
                    '    .Attached_Date = DateAndTime.Now
                    '    .flUpload = Session(flUpload.ClientID)
                    'End With
                    'lstAttach.Add(dtoAttach)
                    Dim hfTranId As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hftranidCV"), HiddenField)
                    If CInt(hfTranId.Value) <= 0 Then
                        Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                        Dim decSize As Decimal = 0
                        Decimal.TryParse(hfDocSize.Value, decSize)

                        dtoAttach = New dtoAddAppAttach
                        With dtoAttach
                            .CompCode = Session("CompCode").ToString
                            .ComUnkID = CInt(Session("companyunkid"))
                            .ApplicantUnkid = CInt(Session("applicantunkid"))
                            .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                            .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                            .AttachrefId = CInt(ddlDocType.SelectedValue)
                            .FolderName = strFolder
                            .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                            .Filename = txtFilePath.Text
                            .Fileuniquename = strUnkImgName
                            .File_size = CInt(decSize / 1024)
                            .Attached_Date = DateAndTime.Now
                            .flUpload = Session(flUpload.ClientID)
                        End With
                        lstAttach.Add(dtoAttach)
                    End If
                    'S.SANDEEP |01-JUN-2023| -- END
                End If

                If CBool(Session("OneCoverLetterMandatory")) = True Then
                    'Dim txtFilePath As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtFilePathCoverLetter"), TextBox)
                    Dim txtFilePath As Label = CType(dlVaanciesList.Items(Idx).FindControl("lblFileNameCoverLetter"), Label)
                    'Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadCoverLetter"), FileUpload)
                    If txtFilePath.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Cover Letter"), MessageType.Errorr)
                        Exit Try
                    End If

                    Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadCoverLetter"), FileUpload)
                    flUploadCoverLetter = flUpload
                    Dim ddlDocType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlDocTypeCoverLetter"), DropDownList)
                    Dim ddlAttachType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlAttachTypeCoverLetter"), DropDownList)
                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("CoverLetterSavePath")
                    Dim hfDocSize As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfDocSizeCoverLetter"), HiddenField)
                    'S.SANDEEP |01-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-960
                    'Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                    'Dim decSize As Decimal = 0
                    'Decimal.TryParse(hfDocSize.Value, decSize)

                    'dtoAttach = New dtoAddAppAttach
                    'With dtoAttach
                    '    .CompCode = Session("CompCode").ToString
                    '    .ComUnkID = CInt(Session("companyunkid"))
                    '    .ApplicantUnkid = CInt(Session("applicantunkid"))
                    '    .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                    '    .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                    '    .AttachrefId = CInt(ddlDocType.SelectedValue)
                    '    .FolderName = strFolder
                    '    .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                    '    .Filename = txtFilePath.Text
                    '    .Fileuniquename = strUnkImgName
                    '    .File_size = CInt(decSize / 1024)
                    '    .Attached_Date = DateAndTime.Now
                    '    .flUpload = Session(flUpload.ClientID)
                    'End With
                    'lstAttach.Add(dtoAttach)

                    Dim hfTranId As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hftranidCoverLetter"), HiddenField)
                    If CInt(hfTranId.Value) <= 0 Then
                        Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                        Dim decSize As Decimal = 0
                        Decimal.TryParse(hfDocSize.Value, decSize)

                        dtoAttach = New dtoAddAppAttach
                        With dtoAttach
                            .CompCode = Session("CompCode").ToString
                            .ComUnkID = CInt(Session("companyunkid"))
                            .ApplicantUnkid = CInt(Session("applicantunkid"))
                            .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                            .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                            .AttachrefId = CInt(ddlDocType.SelectedValue)
                            .FolderName = strFolder
                            .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                            .Filename = txtFilePath.Text
                            .Fileuniquename = strUnkImgName
                            .File_size = CInt(decSize / 1024)
                            .Attached_Date = DateAndTime.Now
                            .flUpload = Session(flUpload.ClientID)
                        End With
                        lstAttach.Add(dtoAttach)
                    End If
                    'S.SANDEEP |01-JUN-2023| -- END
                End If

                If CBool(Session("AttachmentQualificationMandatory")) = True AndAlso Session("CompCode").ToString <> "TRA" Then
                    'Hemant (17 Oct 2023) -- [AndAlso Session("CompCode").ToString <> "TRA"]
                    Dim txtFilePath As Label = CType(dlVaanciesList.Items(Idx).FindControl("lblFileNameQuali"), Label)
                    If txtFilePath.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Qualification"), MessageType.Errorr)
                        Exit Try
                    End If

                    Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadQuali"), FileUpload)
                    flUploadQuali = flUpload
                    Dim ddlDocType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlDocTypeQuali"), DropDownList)
                    Dim ddlAttachType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlAttachTypeQuali"), DropDownList)
                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
                    Dim hfDocSize As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfDocSizeQuali"), HiddenField)
                    'S.SANDEEP |01-JUN-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-960
                    'Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                    'Dim decSize As Decimal = 0
                    'Decimal.TryParse(hfDocSize.Value, decSize)

                    'dtoAttach = New dtoAddAppAttach
                    'With dtoAttach
                    '    .CompCode = Session("CompCode").ToString
                    '    .ComUnkID = CInt(Session("companyunkid"))
                    '    .ApplicantUnkid = CInt(Session("applicantunkid"))
                    '    .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                    '    .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                    '    .AttachrefId = CInt(ddlDocType.SelectedValue)
                    '    .FolderName = strFolder
                    '    .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                    '    .Filename = txtFilePath.Text
                    '    .Fileuniquename = strUnkImgName
                    '    .File_size = CInt(decSize / 1024)
                    '    .Attached_Date = DateAndTime.Now
                    '    .flUpload = Session(flUpload.ClientID)
                    'End With
                    'lstAttach.Add(dtoAttach)
                    Dim hfTranId As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hftranidQuali"), HiddenField)
                    If CInt(hfTranId.Value) <= 0 Then
                        Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                        Dim decSize As Decimal = 0
                        Decimal.TryParse(hfDocSize.Value, decSize)

                        dtoAttach = New dtoAddAppAttach
                        With dtoAttach
                            .CompCode = Session("CompCode").ToString
                            .ComUnkID = CInt(Session("companyunkid"))
                            .ApplicantUnkid = CInt(Session("applicantunkid"))
                            .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                            .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                            .AttachrefId = CInt(ddlDocType.SelectedValue)
                            .FolderName = strFolder
                            .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                            .Filename = txtFilePath.Text
                            .Fileuniquename = strUnkImgName
                            .File_size = CInt(decSize / 1024)
                            .Attached_Date = DateAndTime.Now
                            .flUpload = Session(flUpload.ClientID)
                        End With
                        lstAttach.Add(dtoAttach)
                    End If
                    'S.SANDEEP |01-JUN-2023| -- END
                End If

                Dim hpf As HttpPostedFile
                Dim objSearch As New clsSearchJob
                If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex))) Then
                    Dim strValues = mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex)))
                    For Each _val As String In strValues.Split(",")
                        If objSearch.DeleteApplicantAttachment(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(_val)) = False Then
                            Exit Sub
                        End If
                    Next
                End If

                For Each dto As dtoAddAppAttach In lstAttach
                    If System.IO.Directory.Exists(Server.MapPath("~") & "\" & dto.FolderName) = False Then
                        System.IO.Directory.CreateDirectory(Server.MapPath("~") & "\" & dto.FolderName)
                    End If

                    hpf = dto.flUpload
                    hpf.SaveAs(Server.MapPath("~") & "\" & dto.FolderName & "\" & dto.Fileuniquename)

                    If objSearch.AddApplicantAttachment(dto.CompCode, dto.ComUnkID, dto.ApplicantUnkid, dto.DocumentUnkid, dto.ModulerefId, dto.AttachrefId, dto.Filepath, dto.Filename, dto.Fileuniquename, dto.Attached_Date, dto.File_size, CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex))) = False Then
                        Exit Sub
                    Else
                        iFlag = True
                    End If
                Next
                If iFlag Then
                    ShowMessage(lblUpdateMsg.Text, MessageType.Info)
                    dlVaanciesList.DataBind()
                    FillVacancyList()
                End If
            ElseIf e.CommandName.ToUpper() = "DELDOCCV" Then
                Dim Idx As Integer = e.Item.ItemIndex
                Dim tranid As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hftranidCV"), HiddenField)
                If CInt(tranid.Value) > 0 Then
                    If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(Idx).ToString)) Then
                        'Dim objSearchVacany As New clsSearchJob
                        'If objSearchVacany.DeleteApplicantAttachment(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(tranid.Value)) Then
                        '    dlVaanciesList.DataBind()
                        '    Call FillVacancyList()
                        'End If
                        'objSearchVacany = Nothing
                        Dim _value As String = mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(Idx).ToString))
                        mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(Idx).ToString)) = _value & "," & tranid.Value
                    Else
                        mDicDeletedAttachment.Add(CInt(dlVaanciesList.DataKeys(Idx).ToString), tranid.Value)
                    End If
                    tranid.Value = 0
                    dlVaanciesList.DataBind()
                    Call FillVacancyList()
                Else

                    Dim pnlFile As Panel = CType(dlVaanciesList.Items(Idx).FindControl("pnlFileCV"), Panel)


                    Dim iCtrl = pnlFile.FindControl("lblFileNameCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    iCtrl = pnlFile.FindControl("lblDocSizeCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    iCtrl = pnlFile.FindControl("hftranidCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = 0
                    End If
                    iCtrl = pnlFile.FindControl("hfOldFileNameCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = ""
                    End If
                    iCtrl = pnlFile.FindControl("lblFileExtCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If

                    pnlFile.Visible = False
                End If
            ElseIf e.CommandName.ToUpper() = "DELDOCCOVLETTR" Then
                Dim Idx As Integer = e.Item.ItemIndex
                Dim tranid As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hftranidCoverLetter"), HiddenField)
                If CInt(tranid.Value) > 0 Then
                    If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(Idx).ToString)) Then
                        'Dim objSearchVacany As New clsSearchJob
                        'If objSearchVacany.DeleteApplicantAttachment(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(tranid.Value)) Then
                        '    dlVaanciesList.DataBind()
                        '    Call FillVacancyList()
                        'End If
                        'objSearchVacany = Nothing
                        Dim _value As String = mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(Idx).ToString))
                        mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(Idx).ToString)) = _value & "," & tranid.Value
                    Else
                        mDicDeletedAttachment.Add(CInt(dlVaanciesList.DataKeys(Idx).ToString), tranid.Value)
                    End If
                    tranid.Value = 0
                    dlVaanciesList.DataBind()
                    Call FillVacancyList()
                Else
                    Dim pnlFile As Panel = CType(dlVaanciesList.Items(Idx).FindControl("pnlFileCoverLetter"), Panel)

                    Dim iCtrl = pnlFile.FindControl("lblFileNameCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    iCtrl = pnlFile.FindControl("lblDocSizeCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    iCtrl = pnlFile.FindControl("hftranidCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = 0
                    End If
                    iCtrl = pnlFile.FindControl("hfOldFileNameCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = ""
                    End If
                    iCtrl = pnlFile.FindControl("lblFileExtCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    pnlFile.Visible = False
                End If

            ElseIf e.CommandName.ToUpper() = "DELDOCQUALI" Then
                Dim Idx As Integer = e.Item.ItemIndex
                Dim tranid As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hftranidQuali"), HiddenField)
                If CInt(tranid.Value) > 0 Then
                    If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(Idx).ToString)) Then
                        'Dim objSearchVacany As New clsSearchJob
                        'If objSearchVacany.DeleteApplicantAttachment(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(tranid.Value)) Then
                        '    dlVaanciesList.DataBind()
                        '    Call FillVacancyList()
                        'End If
                        'objSearchVacany = Nothing
                        Dim _value As String = mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(Idx).ToString))
                        mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(Idx).ToString)) = _value & "," & tranid.Value
                    Else
                        mDicDeletedAttachment.Add(CInt(dlVaanciesList.DataKeys(Idx).ToString), tranid.Value)
                    End If
                    tranid.Value = 0
                    dlVaanciesList.DataBind()
                    Call FillVacancyList()
                Else
                    Dim pnlFile As Panel = CType(dlVaanciesList.Items(Idx).FindControl("pnlFileQuali"), Panel)

                    Dim iCtrl = pnlFile.FindControl("lblFileNameQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    iCtrl = pnlFile.FindControl("lblDocSizeQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    iCtrl = pnlFile.FindControl("hftranidQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = 0
                    End If
                    iCtrl = pnlFile.FindControl("hfOldFileNameQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = ""
                    End If
                    iCtrl = pnlFile.FindControl("lblFileExtQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = ""
                    End If
                    pnlFile.Visible = False
                End If
                'S.SANDEEP |01-JUN-2023| -- END

                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1425) TRA - Provide applicant feedback on the online recruitment portal.
            ElseIf e.CommandName.ToUpper() = "FEEDBACK" Then

                Dim hdnVacancyId As HiddenField = CType(dlVaanciesList.Items(e.Item.ItemIndex).FindControl("hdnFieldVacancyId"), HiddenField)
                Dim xVacancyId As Integer = CInt(hdnVacancyId.Value)
                Dim mstrEmail As String = Session("email").ToString()

                Session.Add("FeedbackVacancyID", xVacancyId)

                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Feedback", "openwin();", True)

                'Pinkal (16-Oct-2023) -- End

            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicantAppliedJob = Nothing
        End Try
    End Sub

    Private Sub dlVaanciesList_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlVaanciesList.ItemDataBound
        'S.SANDEEP |01-JUN-2023| -- START
        'ISSUE/ENHANCEMENT : A1X-960
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Dim dsAttachType As DataSet
        'S.SANDEEP |01-JUN-2023| -- END
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("experience").ToString.Trim.Length <= 0 Then
                CType(e.Item.FindControl("objlblVacancyTitle"), Label).Text = drv.Item("vacancytitle").ToString & " (" & drv.Item("noposition").ToString & " " & lblPositionMsg.Text & ")"
                'If drv.Item("experience").ToString.Trim.Length <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                'If CInt(drv.Item("experience")) <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                If CInt(drv.Item("experience")) <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divExp"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isexpbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isexpitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    'If drv.Item("other_experience").ToString.Trim = "" Then
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    Else
                        '    Dim htmlOutput = "Document.html"
                        '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                        '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_experience").ToString, contentUriPrefix)
                        '    htmlResult.WriteToFile(htmlOutput)
                        '    CType(e.Item.FindControl("objlblExp"), Label).Text = htmlResult._HTML
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length > 0 Then
                        If drv.Item("other_experience").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_experience").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_experience").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If
                            Dim strRTF As String = drv.Item("other_experience").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_experience").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblExp"), Label).Text = strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If drv.Item("noposition").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divNoPosition"), Control).Visible = False
                End If

                'Gajanan (04 July 2020) -- Start
                'Enhancment: #0004765
                If drv.Item("experience_comment").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblExpCmt"), Control).Visible = False
                End If
                'Gajanan (04 July 2020) -- End

                'Sohail (11 Nov 2021) -- Start
                'NMB Enhancement: Display Job Location on published vacancy in online recruitment when company code is NMB. e.g Job Location - Northern Zone, Arusha.
                Dim arrJobLocation As New ArrayList
                If Session("CompCode").ToString.ToUpper = "NMB" Then
                    If drv.Item("classgroupname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classgroupname").ToString)
                    End If
                    If drv.Item("classname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classname").ToString)
                    End If
                End If
                CType(e.Item.FindControl("objlblJobLocation"), Label).Text = String.Join(", ", TryCast(arrJobLocation.ToArray(GetType(String)), String()))
                If arrJobLocation.Count <= 0 Then
                    CType(e.Item.FindControl("divJobLocation"), Control).Visible = False
                End If
                'Sohail (11 Nov 2021) -- End

                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divSkill"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isskillbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isskillitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Hemant (08 Jul 2021) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'If drv.Item("other_skill").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_skill").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                        'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                        Dim str As String = ""
                        If drv.Item("skill").ToString.Trim.Length > 0 Then
                            str = "<ul class='p-l-17'>"
                            Dim arr() As String = drv.Item("skill").ToString.Split(";")
                            For i As Integer = 0 To arr.Length - 1
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Next
                            str &= "</ul>"
                        End If
                        CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length > 0 Then
                        If drv.Item("other_skill").ToString.Trim.Length > 0 Then
                            Dim str As String = ""
                            If drv.Item("skill").ToString.Trim.Length > 0 Then
                                str = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("skill").ToString.Split(";")
                                For i As Integer = 0 To arr.Length - 1
                                    str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                Next
                                str &= "</ul>"
                            End If
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_skill").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_skill").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                            
                            Dim strRTF As String = drv.Item("other_skill").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_skill").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                    'Hemant (08 Jul 2021) -- End
                End If

                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
                'If drv.Item("Lang").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divLang"), Control).Visible = False
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                Else
                    'If drv.Item("other_language").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_language").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_language").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length > 0 Then
                        If drv.Item("other_language").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_language").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_language").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                          
                            Dim strRTF As String = drv.Item("other_language").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_language").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If CDec(drv.Item("pay_from").ToString) <= 0 AndAlso CDec(drv.Item("pay_to").ToString) <= 0 Then
                    CType(e.Item.FindControl("divScale"), Control).Visible = False
                Else
                    'Dim str As String = Math.Round(CDec(drv.Item("pay_from").ToString), 2) & " - " & Math.Round(CDec(drv.Item("pay_to").ToString), 2)
                    Dim str As String = Format(CDec(drv.Item("pay_from").ToString), "##,##,##,##,##0.00") & " - " & Format(CDec(drv.Item("pay_to").ToString), "##,##,##,##,##0.00")
                    CType(e.Item.FindControl("objlblScale"), Label).Text = str
                End If

                If drv.Item("openingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divOpenningDate"), Control).Visible = False
                End If
                If drv.Item("closingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divClosuingDate"), Control).Visible = False
                End If
                If drv.Item("remark").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divRemark"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("remark").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblJobDiscription"), Label).Text = strResult
                End If
                If drv.Item("duties").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divResponsblity"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("duties").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblResponsblity"), Label).Text = strResult
                End If
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("Qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString).Length <= 0 Then
                If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divQualification"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isqualibold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isqualiitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'If drv.Item("other_qualification").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_qualification").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_qualification").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length > 0 Then
                        If drv.Item("other_qualification").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_qualification").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_qualification").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                           
                            Dim strRTF As String = drv.Item("other_qualification").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_qualification").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If
            End If

            'S.SANDEEP |01-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-960
            Dim _JobClosingDate As Date = CDate(CType(e.Item.FindControl("objlblClosingDate"), Label).Text)
            Dim _btnUpdate As LinkButton = CType(e.Item.FindControl("btnUpdate"), LinkButton)

            Dim pnlCV As Panel = CType(e.Item.FindControl("pnlCV"), Panel)
            Dim pnlCoverLetter As Panel = CType(e.Item.FindControl("pnlCoverLetter"), Panel)
            Dim pnlQuali As Panel = CType(e.Item.FindControl("pnlQuali"), Panel)

            pnlCV.Visible = CBool(Session("OneCurriculamVitaeMandatory"))
            pnlCoverLetter.Visible = CBool(Session("OneCoverLetterMandatory"))
            pnlQuali.Visible = CBool(Session("AttachmentQualificationMandatory"))

            dsAttachType = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
            dsCombo = objApplicant.GetDocType()
            If pnlCV.Visible = True Then
                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.CURRICULAM_VITAE).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
                Dim ddlDocTypeCV As DropDownList = CType(e.Item.FindControl("ddlDocTypeCV"), DropDownList)
                With ddlDocTypeCV
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim ddlAttachTypeCV As DropDownList = CType(e.Item.FindControl("ddlAttachTypeCV"), DropDownList)
                With ddlAttachTypeCV
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim flUpload As FileUpload = CType(e.Item.FindControl("flUploadCV"), FileUpload)
                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                Dim ids As DataSet = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.CURRICULAM_VITAE), True, CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString))

                Dim pnlFile As Panel = CType(e.Item.FindControl("pnlFileCV"), Panel)

                Dim _tbl As DataTable
                If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) Then
                    _tbl = New DataView(ids.Tables(0), "attachfiletranunkid NOT IN (" & mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) & ")", "", DataViewRowState.CurrentRows).ToTable()
                Else
                    _tbl = New DataView(ids.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
                End If


                If _tbl IsNot Nothing AndAlso _tbl.Rows.Count > 0 Then
                    Dim iCtrl = pnlFile.FindControl("lblFileNameCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = _tbl.Rows(0)("filename")
                    End If
                    iCtrl = pnlFile.FindControl("lblDocSizeCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = Format((CDec(_tbl.Rows(0)("file_size")) / 1024), "0.00") & " MB."
                    End If
                    iCtrl = pnlFile.FindControl("hftranidCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = _tbl.Rows(0)("attachfiletranunkid")
                    End If
                    iCtrl = pnlFile.FindControl("hfOldFileNameCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = _tbl.Rows(0)("filename")
                    End If
                    iCtrl = pnlFile.FindControl("lblFileExtCV")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = _tbl.Rows(0)("filename").ToString().Split(".")(1).ToUpper()
                    End If
                Else
                    pnlFile.Visible = False
                End If
                _tbl = Nothing : ids = Nothing

                If _JobClosingDate.Date < DateTime.Now.Date Then
                    Dim _del As LinkButton = CType(pnlFile.FindControl("btncvDocDel"), LinkButton)
                    _del.Visible = False
                    flUpload.Visible = False
                End If
            End If

            If _JobClosingDate.Date < DateTime.Now.Date Then
                pnlCV.Enabled = False
                _btnUpdate.Visible = False
            End If

            If pnlCoverLetter.Visible = True Then
                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.COVER_LETTER).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
                Dim ddlDocTypeCoverLetter As DropDownList = CType(e.Item.FindControl("ddlDocTypeCoverLetter"), DropDownList)
                With ddlDocTypeCoverLetter
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim ddlAttachTypeCoverLetter As DropDownList = CType(e.Item.FindControl("ddlAttachTypeCoverLetter"), DropDownList)
                With ddlAttachTypeCoverLetter
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim flUpload As FileUpload = CType(e.Item.FindControl("flUploadCoverLetter"), FileUpload)
                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                Dim ids As DataSet = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.COVER_LETTER), True, CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString))

                Dim pnlFile As Panel = CType(e.Item.FindControl("pnlFileCoverLetter"), Panel)

                Dim _tbl As DataTable
                If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) Then
                    _tbl = New DataView(ids.Tables(0), "attachfiletranunkid NOT IN (" & mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) & ")", "", DataViewRowState.CurrentRows).ToTable()
                Else
                    _tbl = New DataView(ids.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
                End If

                If _tbl IsNot Nothing AndAlso _tbl.Rows.Count > 0 Then
                    Dim iCtrl = pnlFile.FindControl("lblFileNameCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = _tbl.Rows(0)("filename")
                    End If
                    iCtrl = pnlFile.FindControl("lblDocSizeCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = Format((CDec(_tbl.Rows(0)("file_size")) / 1024), "0.00") & " MB."
                    End If
                    iCtrl = pnlFile.FindControl("hftranidCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = _tbl.Rows(0)("attachfiletranunkid")
                    End If
                    iCtrl = pnlFile.FindControl("hfOldFileNameCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = _tbl.Rows(0)("filename")
                    End If
                    iCtrl = pnlFile.FindControl("lblFileExtCoverLetter")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = _tbl.Rows(0)("filename").ToString().Split(".")(1).ToUpper()
                    End If
                Else
                    pnlFile.Visible = False
                End If
                _tbl = Nothing : ids = Nothing

                If _JobClosingDate.Date < DateTime.Now.Date Then
                    Dim _del As LinkButton = CType(pnlFile.FindControl("btnclDocDel"), LinkButton)
                    _del.Visible = False
                    flUpload.Visible = False
                End If
            End If

            If _JobClosingDate.Date < DateTime.Now.Date Then
                pnlCoverLetter.Enabled = False
                _btnUpdate.Visible = False
            End If

            If pnlQuali.Visible = True Then
                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.QUALIFICATIONS).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
                Dim ddlDocTypeQuali As DropDownList = CType(e.Item.FindControl("ddlDocTypeQuali"), DropDownList)
                With ddlDocTypeQuali
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim ddlAttachTypeQuali As DropDownList = CType(e.Item.FindControl("ddlAttachTypeQuali"), DropDownList)
                With ddlAttachTypeQuali
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim flUpload As FileUpload = CType(e.Item.FindControl("flUploadQuali"), FileUpload)
                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                Dim ids As DataSet = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS), True, CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString))

                Dim pnlFile As Panel = CType(e.Item.FindControl("pnlFileQuali"), Panel)

                Dim _tbl As DataTable
                If mDicDeletedAttachment.ContainsKey(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) Then
                    _tbl = New DataView(ids.Tables(0), "attachfiletranunkid NOT IN (" & mDicDeletedAttachment(CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)) & ")", "", DataViewRowState.CurrentRows).ToTable()
                Else
                    _tbl = New DataView(ids.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
                End If

                If _tbl IsNot Nothing AndAlso _tbl.Rows.Count > 0 Then
                    Dim iCtrl = pnlFile.FindControl("lblFileNameQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = _tbl.Rows(0)("filename")
                    End If
                    iCtrl = pnlFile.FindControl("lblDocSizeQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = Format((CDec(_tbl.Rows(0)("file_size")) / 1024), "0.00") & " MB."
                    End If
                    iCtrl = pnlFile.FindControl("hftranidQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = _tbl.Rows(0)("attachfiletranunkid")
                    End If
                    iCtrl = pnlFile.FindControl("hfOldFileNameQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "HIDDENFIELD" Then
                        CType(iCtrl, HiddenField).Value = _tbl.Rows(0)("filename")
                    End If
                    iCtrl = pnlFile.FindControl("lblFileExtQuali")
                    If iCtrl IsNot Nothing AndAlso iCtrl.GetType().Name.ToUpper() = "LABEL" Then
                        CType(iCtrl, Label).Text = _tbl.Rows(0)("filename").ToString().Split(".")(1).ToUpper()
                    End If
                Else
                    pnlFile.Visible = False
                End If
                _tbl = Nothing : ids = Nothing

                If _JobClosingDate.Date < DateTime.Now.Date Then
                    Dim _del As LinkButton = CType(pnlFile.FindControl("btnQualiDocDel"), LinkButton)
                    _del.Visible = False
                    flUpload.Visible = False
                End If

            End If
            If _JobClosingDate.Date < DateTime.Now.Date Then
                pnlQuali.Enabled = False
                _btnUpdate.Visible = False
            End If
            'S.SANDEEP |01-JUN-2023| -- END

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then LanguageControl.show()
    End Sub

    'Sohail (16 Aug 2019) -- End
#End Region

#Region " Combobox Events "

    'Hemant (17 Oct 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA) : Every qualification category should be bound with specific document type in attachment.
    Protected Sub ddlDocTypeCV_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlDocTypeCV As DropDownList = CType(sender, DropDownList)
            Dim itm As DataListItem = DirectCast(ddlDocTypeCV.NamingContainer, DataListItem)
            Dim ddlAttachTypeCV As DropDownList = CType(itm.FindControl("ddlAttachTypeCV"), DropDownList)

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlDocTypeCV.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlDocTypeCV.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlAttachTypeCV.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlAttachTypeCV.Enabled = False
                    Else
                        ddlAttachTypeCV.Enabled = True
                    End If

                Else
                    ddlAttachTypeCV.SelectedIndex = 0
                    ddlAttachTypeCV.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub ddlDocTypeCoverLetter_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlDocTypeCoverLetter As DropDownList = CType(sender, DropDownList)
            Dim itm As DataListItem = DirectCast(ddlDocTypeCoverLetter.NamingContainer, DataListItem)
            Dim ddlAttachTypeCoverLetter As DropDownList = CType(itm.FindControl("ddlAttachTypeCoverLetter"), DropDownList)

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlDocTypeCoverLetter.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlDocTypeCoverLetter.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlAttachTypeCoverLetter.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlAttachTypeCoverLetter.Enabled = False
                    Else
                        ddlAttachTypeCoverLetter.Enabled = True
                    End If

                Else
                    ddlAttachTypeCoverLetter.SelectedIndex = 0
                    ddlAttachTypeCoverLetter.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub ddlDocTypeQuali_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlDocTypeQuali As DropDownList = CType(sender, DropDownList)
            Dim itm As DataListItem = DirectCast(ddlDocTypeQuali.NamingContainer, DataListItem)
            Dim ddlAttachTypeQuali As DropDownList = CType(itm.FindControl("ddlAttachTypeQuali"), DropDownList)

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlDocTypeQuali.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlDocTypeQuali.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlAttachTypeQuali.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlAttachTypeQuali.Enabled = False
                    Else
                        ddlAttachTypeQuali.Enabled = True
                    End If

                Else
                    ddlAttachTypeQuali.SelectedIndex = 0
                    ddlAttachTypeQuali.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Hemant (17 Oct 2023) -- End

#End Region

End Class