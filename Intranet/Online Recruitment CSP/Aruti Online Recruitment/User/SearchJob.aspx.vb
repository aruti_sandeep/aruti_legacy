﻿Imports System.IO
'Imports System.IO.Directory
Imports iTextSharp.text
'Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.tool.xml

Public Class SearchJob
    Inherits Base_Page

#Region "Provate Variable"
    'Private objSearchJob As New clsSearchJob
    'Private dsVacancyList As DataSet = Nothing
    Private blnHideApply As Boolean = False
    Private mblnAllowImageFile As Boolean
    Private mblnAllowDocumentFile As Boolean
#End Region

#Region " Method Functions "

    Private Sub FillVacancyList()
        Try
            'dsVacancyList = objSearchJob.GetApplicantVacancy(strCompCode:=Session("CompCode").ToString,
            '                                          intComUnkID:=CInt(Session("companyunkid")),
            '                                          intMasterTypeId:=clsCommon_Master.enCommonMaster.VACANCY_MASTER,
            '                                          intEType:=clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE,
            '                                          blnVacancyType:=CBool(IIf(Session("Vacancy") = "Int", False, True)),
            '                                          blnAllVacancy:=False,
            '                                          intDateZoneDifference:=CInt(Session("timezone_minute_diff")),
            '                                          strVacancyUnkIdLIs:="")

            'Dim dsApplyedJob As DataSet = (New clsApplicantApplyJob).GetApplicantAppliedJob(strCompCode:=Session("CompCode").ToString,
            '                                                                                intComUnkID:=CInt(Session("companyunkid")),
            '                                                                                intApplicantUnkId:=CInt(Session("applicantunkid")))
            'If dsApplyedJob IsNot Nothing AndAlso dsApplyedJob.Tables(0).Rows.Count > 0 Then
            '    Dim str = String.Join(",", dsApplyedJob.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("vacancyid").ToString))
            '    For Each dRow As DataRow In dsVacancyList.Tables(0).Select("vacancyid IN (" & str & ")")
            '        dRow.Item("IsApplied") = True
            '    Next

            'End If
            'dsVacancyList.AcceptChanges()
            'dlVaanciesList.DataSource = New DataView(dsVacancyList.Tables(0), "IsApplied = 0", "IsApplied ASC, vacancytitle ASC", DataViewRowState.CurrentRows).ToTable
            'dlVaanciesList.DataBind()
            odsVacancy.SelectParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            odsVacancy.SelectParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid"))
            odsVacancy.SelectParameters.Item("intMasterTypeId").DefaultValue = clsCommon_Master.enCommonMaster.VACANCY_MASTER
            odsVacancy.SelectParameters.Item("intEType").DefaultValue = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE
            odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = CBool(IIf(Session("Vacancy") = "Int", False, True))
            odsVacancy.SelectParameters.Item("blnAllVacancy").DefaultValue = False
            odsVacancy.SelectParameters.Item("intDateZoneDifference").DefaultValue = CInt(Session("timezone_minute_diff"))
            odsVacancy.SelectParameters.Item("strVacancyUnkIdLIs").DefaultValue = ""
            odsVacancy.SelectParameters.Item("intApplicantUnkId").DefaultValue = CInt(Session("applicantunkid"))

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim dsList As DataSet
        Dim strMsg As String = ""
        Try
            dsList = (New clsPersonalInfo).GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count > 0 Then
                'If dsList.Tables(0).Rows(0).Item("firstname").ToString.Length <= 0 _
                '    OrElse dsList.Tables(0).Rows(0).Item("surname").ToString.Length <= 0 _
                '    OrElse CInt(dsList.Tables(0).Rows(0).Item("gender").ToString) <= 0 _
                '    OrElse IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) = True _
                '    OrElse dsList.Tables(0).Rows(0).Item("present_mobileno").ToString.Length <= 0 Then
                '    strMsg &= "\n Personal Information"
                '    'GoTo Redirect
                'End If
                If dsList.Tables(0).Rows(0).Item("firstname").ToString.Length <= 0 Then
                    strMsg &= "\n First Name"
                End If
                If dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 AndAlso CBool(Session("MiddleNameMandatory")) = True Then
                    strMsg &= "\n Other Name"
                End If
                If dsList.Tables(0).Rows(0).Item("surname").ToString.Length <= 0 Then
                    strMsg &= "\n Surname"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 AndAlso CBool(Session("GenderMandatory")) = True Then
                    strMsg &= "\n Gender"
                End If
                If ((IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900")) AndAlso CBool(Session("BirthDateMandatory")) = True) Then
                    strMsg &= "\n Birth Date"
                End If
                If dsList.Tables(0).Rows(0).Item("present_mobileno").ToString.Length <= 0 Then
                    strMsg &= "\n Mobile No."
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                    strMsg &= "\n Nationality"
                End If
            Else
                strMsg &= "\n Personal Information"
                'GoTo Redirect
            End If

            dsList = (New clsContactDetails).GetContactDetail(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 AndAlso CBool(Session("Address1Mandatory")) = True Then
                    strMsg &= "\n Current Address 1"
                    'GoTo Redirect
                End If
                If dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 AndAlso CBool(Session("Address2Mandatory")) = True Then
                    strMsg &= "\n Current Address 2"
                End If
                If dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 AndAlso CBool(Session("RegionMandatory")) = True Then
                    strMsg &= "\n Current Region"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 AndAlso CBool(Session("CountryMandatory")) = True Then
                    strMsg &= "\n Current Country"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 AndAlso CBool(Session("StateMandatory")) = True Then
                    strMsg &= "\n Current State"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 AndAlso CBool(Session("CityMandatory")) = True Then
                    strMsg &= "\n Current Post Town/City"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 AndAlso CBool(Session("PostCodeMandatory")) = True Then
                    strMsg &= "\n Current Post Code"
                End If
            Else
                strMsg &= "\n Current Address 1"
                'GoTo Redirect
            End If


            dsList = (New clsOtherInfo).GetOtherInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)
            If dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 AndAlso CBool(Session("Language1Mandatory")) = True Then
                    strMsg &= "\n Language 1"
                    'GoTo Redirect
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 AndAlso CBool(Session("MaritalStatusMandatory")) = True Then
                    strMsg &= "\n Marital Status"
                    'GoTo Redirect
                End If
                'If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                '    strMsg &= "\n Nationality"
                '    'GoTo Redirect
                'End If
            Else
                strMsg &= "\n Marital Status"
                'GoTo Redirect
            End If

            dsList = (New clsApplicantSkill).GetApplicantSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.SKILL_CATEGORY, CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneSkillMandatory")) = True Then
                strMsg &= "\n Skills. <br/>"
            End If

            dsList = (New clsApplicantQualification).GetApplicantQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, CInt(Session("appqualisortbyid")))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneQualificationMandatory")) = True Then
                strMsg &= "\n Atleast one Qualification"
                'GoTo Redirect
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                If CBool(Session("HighestQualificationMandatory")) = True AndAlso dsList.Tables(0).Select("ishighestqualification =  1 ").Length <= 0 Then
                    strMsg &= "\n Highest Qualification"
                    'GoTo Redirect
                End If
            End If

            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
            'dsList = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
            '    strMsg &= "\n Atleast one Qualification Attachement"
            '    'GoTo Redirect
            'End If
            'Sohail (11 Nov 2021) -- End

            dsList = (New clsApplicantExperience).GetApplicantExperience(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneJobExperienceMandatory")) = True Then
                strMsg &= "\n Job Experiences. <br/>"
            End If

            dsList = (New clsApplicantReference).GetApplicantReference(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneReferenceMandatory")) = True Then
                strMsg &= "\n References. <br/>"
            End If

            'Redirect:
            If strMsg.Trim <> "" Then
                ShowMessage(lblProfileMsg.Text & strMsg)
                HttpContext.Current.Response.Redirect("~/User/UserHome.aspx", False)
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    'Pinkal (18-May-2018) -- Start
    'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.

    Private Function GetApplicantCV() As String
        Dim strBuilder As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objApplicant As New clsApplicant
        Dim objPersonalInfo As New clsPersonalInfo
        Dim dsPersonalInfo As DataSet
        Dim objExperience As New clsApplicantExperience
        Dim dsExperience As DataSet
        Dim objContactDetails As New clsContactDetails
        Dim dsContactDetails As DataSet
        Dim objOtherInfo As New clsOtherInfo
        Dim dsOtherInfo As DataSet
        Dim strTitle As String = ""
        Dim strName As String = ""
        Dim strEmail As String = ""
        Dim strGender As String = ""
        Dim strBirthDate As String = ""
        Dim strMaritalStatus As String = ""
        Dim strMarriedDate As String = ""
        Dim strNationality As String = ""
        Dim strLanguagesKnown As String = ""
        Dim strMotherTongue As String = ""
        Dim strImpaired As String = ""
        Dim strCurrentSalary As String = ""
        Dim strExpectedSalary As String = ""
        Dim strExpectedBenefits As String = ""
        Dim strWillingToRelocate As String = ""
        Dim strWillingToTravel As String = ""
        Dim strNoticePeriodDays As String = ""
        Dim strSkills As String = ""
        Dim strPresentAddress As String = ""
        Dim strPresentAddress2 As String = ""
        Dim strPermenantAddress As String = ""
        Dim strPermenantAddress2 As String = ""
        Dim strMemberships As String = ""
        Dim strAchievements As String = ""
        Dim strJournalResearchPaper As String = ""
        Dim mstrFilePath As String = ""
        Try

            Dim dsTitle As DataSet = objApplicant.GetCommonMaster(Session("PreviewCompCode").ToString, CInt(Session("Previewcompanyunkid")), clsCommon_Master.enCommonMaster.TITLE)
            Dim dsGender As DataSet = objPersonalInfo.GetGender()

            dsPersonalInfo = objPersonalInfo.GetPersonalInfo(strCompCode:=Session("PreviewCompCode").ToString,
                                                                 intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                 intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid"))
                                                                 )

            If dsPersonalInfo.Tables(0).Rows.Count > 0 Then
                strName = dsPersonalInfo.Tables(0).Rows(0).Item("firstname").ToString & " " & dsPersonalInfo.Tables(0).Rows(0).Item("othername").ToString & " " & dsPersonalInfo.Tables(0).Rows(0).Item("surname").ToString
                strEmail = "Email: " & dsPersonalInfo.Tables(0).Rows(0).Item("email").ToString & ", " & "Mobile: " & dsPersonalInfo.Tables(0).Rows(0).Item("present_mobileno").ToString
                strBirthDate = CDate(dsPersonalInfo.Tables(0).Rows(0).Item("birth_date")).ToString("dd-MMM-yyyy")

                If CInt(dsPersonalInfo.Tables(0).Rows(0).Item("titleunkid")) > 0 Then
                    If dsTitle.Tables(0).Select("masterunkid = " & CInt(dsPersonalInfo.Tables(0).Rows(0).Item("titleunkid")) & " ").Length > 0 Then
                        strTitle = dsTitle.Tables(0).Select("masterunkid = " & CInt(dsPersonalInfo.Tables(0).Rows(0).Item("titleunkid")) & " ")(0).Item("Name").ToString
                    End If
                End If

                If CInt(dsPersonalInfo.Tables(0).Rows(0).Item("gender")) > 0 Then
                    strGender = dsGender.Tables(0).Select("Id = " & CInt(dsPersonalInfo.Tables(0).Rows(0).Item("gender")) & " ")(0).Item("Name").ToString
                End If

            End If

            dsContactDetails = objContactDetails.GetContactDetail(strCompCode:=Session("PreviewCompCode").ToString,
                                                                  intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                  intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid"))
                                                                  )

            If dsContactDetails.Tables(0).Rows.Count > 0 Then
                '*** Tel. No
                If dsContactDetails.Tables(0).Rows(0).Item("present_tel_no").ToString.Trim.Length > 0 Then
                    strEmail &= ", Tel. No.: " & dsContactDetails.Tables(0).Rows(0).Item("present_tel_no").ToString.Trim
                End If
                '*** Fax
                If dsContactDetails.Tables(0).Rows(0).Item("present_fax").ToString.Trim.Length > 0 Then
                    strEmail &= ", Fax: " & dsContactDetails.Tables(0).Rows(0).Item("present_fax").ToString.Trim
                End If

                '*** Current Address
                strPresentAddress = dsContactDetails.Tables(0).Rows(0).Item("present_address1").ToString.Trim


                If dsContactDetails.Tables(0).Rows(0).Item("present_address2").ToString.Trim.Length > 0 Then
                    strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_address2").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_plotno").ToString.Trim.Length > 0 Then
                    strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_plotno").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_estate").ToString.Trim.Length > 0 Then
                    strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_estate").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_road").ToString.Trim.Length > 0 Then
                    strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_road").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_province").ToString.Trim.Length > 0 Then
                    strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_province").ToString.Trim
                End If


                If dsContactDetails.Tables(0).Rows(0).Item("present_country_name").ToString.Trim.Length > 0 Then
                    strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_country_name").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_State_Name").ToString.Trim.Length > 0 Then
                    strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_State_Name").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_City_Name").ToString.Trim.Length > 0 Then
                    strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_City_Name").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("present_zipcode_no").ToString.Trim.Length > 0 Then
                    strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_zipcode_no").ToString.Trim
                End If

                If strPresentAddress2.Trim.Length > 0 Then
                    strPresentAddress2 = strPresentAddress2.Substring(2)
                End If

                '*** Permenant Address
                If dsContactDetails.Tables(0).Rows(0).Item("perm_address1").ToString.Trim.Length > 0 Then
                    strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_address1").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_address2").ToString.Trim.Length > 0 Then
                    strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_address2").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_plotno").ToString.Trim.Length > 0 Then
                    strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_plotno").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_estate").ToString.Trim.Length > 0 Then
                    strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_estate").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_road").ToString.Trim.Length > 0 Then
                    strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_road").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_province").ToString.Trim.Length > 0 Then
                    strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_province").ToString.Trim
                End If


                If dsContactDetails.Tables(0).Rows(0).Item("perm_country_name").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_country_name").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_State_Name").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_State_Name").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_City_Name").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_City_Name").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_zipcode_no").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_zipcode_no").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_tel_no").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", Tel. No.: " & dsContactDetails.Tables(0).Rows(0).Item("perm_tel_no").ToString.Trim
                End If

                If dsContactDetails.Tables(0).Rows(0).Item("perm_fax").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", Fax: " & dsContactDetails.Tables(0).Rows(0).Item("perm_fax").ToString.Trim
                End If

            End If

            '*** Other Info
            dsOtherInfo = objOtherInfo.GetOtherInfo(strCompCode:=Session("PreviewCompCode").ToString,
                                                        intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                        intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")),
                                                        intLanguage_id:=clsCommon_Master.enCommonMaster.LANGUAGES,
                                                        intMaritalStatus_id:=clsCommon_Master.enCommonMaster.MARRIED_STATUS
                                                        )

            If dsOtherInfo.Tables(0).Rows.Count > 0 Then

                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language1unkid")) > 0 Then
                    strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language1name").ToString
                End If
                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language2unkid")) > 0 Then
                    strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language2name").ToString
                End If
                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language3unkid")) > 0 Then
                    strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language3name").ToString
                End If
                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language4unkid")) > 0 Then
                    strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language4name").ToString
                End If

                If strLanguagesKnown.Trim.Length > 0 Then
                    strLanguagesKnown = strLanguagesKnown.Substring(2)
                End If

                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("nationality")) > 0 Then
                    strNationality = dsOtherInfo.Tables(0).Rows(0).Item("country_name").ToString & "n"
                End If

                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("marital_statusunkid")) > 0 Then
                    strMaritalStatus = dsOtherInfo.Tables(0).Rows(0).Item("marital_status").ToString
                End If

                If IsDBNull(dsOtherInfo.Tables(0).Rows(0).Item("anniversary_date")) = False AndAlso CDate(dsOtherInfo.Tables(0).Rows(0).Item("anniversary_date")).Date <> CDate("01/Jan/1900") Then
                    strMarriedDate = CDate(dsOtherInfo.Tables(0).Rows(0).Item("anniversary_date")).ToString("dd-MMM-yyyy")
                End If

                If dsOtherInfo.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim.Length > 0 Then
                    strMotherTongue = dsOtherInfo.Tables(0).Rows(0).Item("mother_tongue").ToString
                End If

                If CBool(dsOtherInfo.Tables(0).Rows(0).Item("isimpaired")) = True Then
                    strImpaired = "Yes <BR />" & dsOtherInfo.Tables(0).Rows(0).Item("impairment").ToString
                Else
                    strImpaired = "No"
                End If

                If CDec(dsOtherInfo.Tables(0).Rows(0).Item("current_salary")) <> 0 Then
                    strCurrentSalary = Format(CDec(dsOtherInfo.Tables(0).Rows(0).Item("current_salary").ToString), "0.00")
                End If

                If CDec(dsOtherInfo.Tables(0).Rows(0).Item("expected_salary")) <> 0 Then
                    strExpectedSalary = Format(CDec(dsOtherInfo.Tables(0).Rows(0).Item("expected_salary").ToString), "0.00")
                End If

                If dsOtherInfo.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim.Length > 0 Then
                    strExpectedBenefits = dsOtherInfo.Tables(0).Rows(0).Item("expected_benefits").ToString
                End If

                If CBool(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = True Then
                    strWillingToRelocate = "Yes"
                Else
                    strWillingToRelocate = "No"
                End If

                'If CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = enYesno.None Then
                '    strWillingToRelocate = ""
                'ElseIf CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = enYesno.Yes Then
                '    strWillingToRelocate = "Yes"
                'Else
                '    strWillingToRelocate = "No"
                'End If

                If CBool(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = True Then
                    strWillingToTravel = "Yes"
                Else
                    strWillingToTravel = "No"
                End If

                'If CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = enYesno.None Then
                '    strWillingToTravel = ""
                'ElseIf CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = enYesno.Yes Then
                '    strWillingToTravel = "Yes"
                'Else
                '    strWillingToTravel = "No"
                'End If

                If CInt(dsOtherInfo.Tables(0).Rows(0).Item("notice_period_days")) <> 0 Then
                    strNoticePeriodDays = dsOtherInfo.Tables(0).Rows(0).Item("notice_period_days").ToString & " Days"
                End If

                'pnlEarliestPossibleStartDate.Visible = False

                'pnlComments.Visible = False

                If dsOtherInfo.Tables(0).Rows(0).Item("perm_alternateno").ToString.Trim.Length > 0 Then
                    strPermenantAddress2 &= ", Alt. Tel. No.: " & dsOtherInfo.Tables(0).Rows(0).Item("perm_alternateno").ToString
                End If

                If dsOtherInfo.Tables(0).Rows(0).Item("memberships").ToString.Trim.Length > 0 Then
                    strMemberships = dsOtherInfo.Tables(0).Rows(0).Item("memberships").ToString
                End If

                If dsOtherInfo.Tables(0).Rows(0).Item("achievements").ToString.Trim.Length > 0 Then
                    strAchievements = dsOtherInfo.Tables(0).Rows(0).Item("achievements").ToString
                End If

                If dsOtherInfo.Tables(0).Rows(0).Item("journalsresearchpapers").ToString.Trim.Length > 0 Then
                    strJournalResearchPaper = dsOtherInfo.Tables(0).Rows(0).Item("journalsresearchpapers").ToString
                End If

            End If

            If strPermenantAddress.Trim.Length > 0 Then
                strPermenantAddress = strPermenantAddress.Substring(2)
            End If

            If strPermenantAddress2.Trim.Length > 0 Then
                strPermenantAddress2 = strPermenantAddress2.Substring(2)
            End If


            '=============Skill Information Start==============
            Dim dsSkill As DataSet = (New clsApplicantSkill).GetApplicantSkills(strCompCode:=Session("PreviewCompCode").ToString,
                                                                                intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                                intSkillCategory_ID:=clsCommon_Master.enCommonMaster.SKILL_CATEGORY,
                                                                                intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")))
            Dim strSkillCSV As String = ""
            If dsSkill IsNot Nothing AndAlso dsSkill.Tables(0).Rows.Count > 0 Then
                strSkillCSV = String.Join(", ", dsSkill.Tables(0).AsEnumerable().Select(Function(x) " " & IIf(x.Field(Of Integer)("skillunkid") > 0, x.Field(Of String)("SKILL"), x.Field(Of String)("other_skill")).ToString))
            End If

            '=============Skill Information End==============



            '=============Qualification Information Start ==========
            Dim dsQualification As DataSet = (New clsApplicantQualification).GetApplicantQualifications(strCompCode:=Session("PreviewCompCode").ToString,
                                                                                                        intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                                                        intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")),
                                                                                                        intQualificationGroup_ID:=clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, intSortOrder:=CInt(Session("appqualisortbyid")))

            '=============Qualification Information END==========


            '=============Experience Information Start ==========
            dsExperience = (New clsApplicantExperience).GetApplicantExperience(strCompCode:=Session("PreviewCompCode").ToString,
                                                                     intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                     intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid"))
                                                                     )
            '=============Experience Information Start ==========


            '=============Refrences Information Start===========
            Dim dsRefrence As DataSet = (New clsApplicantReference).GetApplicantReference(Session("PreviewCompCode").ToString, CInt(Session("Previewcompanyunkid")), CInt(Session("PreviewApplicantUnkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            '=============Refrences Information End===========



            If strTitle.Trim.Length > 0 Then
                strTitle = strTitle & " "
            Else
                strTitle = ""
            End If


            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append("<HEAD>")
            strBuilder.Append(" <TITLE>" & strName & " CV </TITLE> " & vbCrLf)
            strBuilder.Append(" <style> body { background: rgb(255,255,255); } .sectionheader { font-size: 14px; font-weight: 600; background-color: #e0e0e0;
                                            text-align: center;} .sectionpanel { padding-left: 20px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; } #page { background: white; display: block;
                                            margin: 0 auto;margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgba(0,0,0,0.5); } #page[size='A4'] {width: 21cm; min-height: 29.7cm; } #page[size='A4'][layout='portrait'] { width: 29.7cm;
                                            height: 21cm;}#page[size='A3'] {width: 29.7cm;height: 42cm;}#page[size='A3'][layout='portrait'] {width: 42cm;height: 29.7cm; }#page[size='A5'] { width: 14.8cm; height: 21cm; }
                                            #page[size='A5'][layout='portrait'] {width: 21cm; height: 14.8cm; }@media print { body, #page { margin: 0;box-shadow: 0; }} .gridHeader > td, .gridItem > td {padding: 5px; }.gridHeader {
                                            background-color: #AFAFAF;font-weight: bold; }</style>")
            strBuilder.Append("</HEAD>")
            strBuilder.Append("<BODY>")
            strBuilder.Append("<FORM ID = 'form1' autocomplete='off' style='font-family: Verdana, Geneva, Tahoma, sans-serif'>")
            strBuilder.Append("<DIV ID='PAGE' SIZE='A4'>")

            strBuilder.Append("<TABLE STYLE='WIDTH:100%'>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:Large;font-weight:bold;'>" & strName & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:14px'>" & strEmail & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:14px'>" & strPresentAddress & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:14px'>" & strPresentAddress2 & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<HR STYLE='margin-top: 0px; margin-bottom: 0px; border-top: 1px solid #000;' />")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("</TABLE>")

            strBuilder.Append("<DIV ID='divPersonalInfo' Class='sectionpanel'>")
            strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>Personal Information</DIV>")
            strBuilder.Append("<BR></BR>")
            strBuilder.Append("<TABLE STYLE = 'width: 100%'>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='font-weight:bold;font-size:14px'>Name:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strTitle & strName & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Gender:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strGender & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Birth Date:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strBirthDate & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 25%;font-weight:bold;font-size:14px'>Marital Status:</TD>")
            strBuilder.Append("<TD STYLE='width: 25%;font-size:14px'> <SPAN STYLE='font-size:14px'> " & strMaritalStatus & "</SPAN></TD>")

            If strMarriedDate.ToString().Trim.Length > 0 Then
                strBuilder.Append("<TD STYLE='width: 25%;font-weight:bold;font-size:14px'>Married Date:</TD>")
                strBuilder.Append("<TD STYLE='width: 25%;font-size:14px'> <SPAN STYLE='font-size:14px'> " & strMarriedDate & "</SPAN></TD>")
            Else
                strBuilder.Append("<TD STYLE='width: 25%;font-weight:bold;font-size:14px'></TD>")
                strBuilder.Append("<TD STYLE='width: 25%;font-size:14px'> <SPAN STYLE='font-size:14px'></SPAN></TD>")
            End If

            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Nationality:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strNationality & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Languages Known:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strLanguagesKnown & "</SPAN></TD>")
            strBuilder.Append("</TR>")

            If strMotherTongue.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Mother Tongue:</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strMotherTongue & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strImpaired.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Impaired:</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strImpaired & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strCurrentSalary.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Current Salary:</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strCurrentSalary & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strCurrentSalary.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Expected Salary:</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strExpectedSalary & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strExpectedBenefits.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Expected Benefits:</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strExpectedBenefits & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            'If strWillingToRelocate.Length > 0 Then
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Willing to relocate:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strWillingToRelocate & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            'End If

            'If strWillingToTravel.Length > 0 Then
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Willing to travel:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strWillingToTravel & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            'End If

            If strNoticePeriodDays.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Notice Period (Days):</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strNoticePeriodDays & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>Permanent Address:</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strPermenantAddress & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'></TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strPermenantAddress2 & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("</TABLE>")
            strBuilder.Append("</DIV>")

            If strSkillCSV.Trim.Length > 0 Then
                strBuilder.Append("<DIV ID='divSkill' Class='sectionpanel'>")
                strBuilder.Append("<TABLE STYLE = 'width: 100%'>")
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 15%;vertical-align: top;font-weight:bold;font-size:14px'>Key Skills:</TD>")
                strBuilder.Append("<TD STYLE='width: 85%;font-size:14px'> " & strSkillCSV & "</TD>")
                strBuilder.Append("</TR>")
                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If


            If dsQualification IsNot Nothing AndAlso dsQualification.Tables(0).Rows.Count > 0 Then
                strBuilder.Append("<DIV ID='divQualification' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>Qualification</DIV>")
                strBuilder.Append("<BR></BR>")
                strBuilder.Append("<TABLE border='1' STYLE = 'width: 100%;border-collapse: collapse;'>")
                strBuilder.Append("<TR class='gridHeader'>")
                strBuilder.Append("<TD STYLE='width: 17%;font-weight:bold;font-size:14px;'>Qualification</TD>")
                strBuilder.Append("<TD STYLE='width: 17%;font-weight:bold;font-size:14px;'>Institution</TD>")
                strBuilder.Append("<TD STYLE='width: 16%;font-weight:bold;font-size:14px;'>Result</TD>")
                strBuilder.Append("<TD STYLE='width: 17%;font-weight:bold;font-size:14px;'>Duration</TD>")
                strBuilder.Append("<TD STYLE='width: 16%;font-weight:bold;font-size:14px;text-align:right;'>GPA Point</TD>")
                strBuilder.Append("</TR>")



                If dsQualification.Tables(0).Columns.Contains("startdate") Then
                    dsQualification.Tables(0).Columns("startdate").SetOrdinal(dsQualification.Tables(0).Columns.Count - 1)
                End If
                If dsQualification.Tables(0).Columns.Contains("enddate") Then
                    dsQualification.Tables(0).Columns("enddate").SetOrdinal(dsQualification.Tables(0).Columns.Count - 1)
                End If
                If dsQualification.Tables(0).Columns.Contains("gpacode") Then
                    dsQualification.Tables(0).Columns("gpacode").SetOrdinal(dsQualification.Tables(0).Columns.Count - 1)
                End If
                If dsQualification.Tables(0).Columns.Contains("other_resultcode") Then
                    dsQualification.Tables(0).Columns("other_resultcode").SetOrdinal(dsQualification.Tables(0).Columns("other_institute").Ordinal)
                End If


                For Each dr As DataRow In dsQualification.Tables(0).Rows
                    strBuilder.Append("<TR class='gridItem'>")
                    For Each dc As DataColumn In dsQualification.Tables(0).Columns
                        'If CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("qualificationgroupunkid")) > 0 AndAlso CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("qualificationunkid")) > 0 Then
                        '    If dc.ColumnName = "Qualification" OrElse dc.ColumnName = "institute" OrElse dc.ColumnName = "resultcode" Then
                        '        strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        '    End If
                        'ElseIf CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("qualificationgroupunkid")) <= 0 AndAlso CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("qualificationunkid")) <= 0 Then
                        '    If dc.ColumnName = "other_qualification" OrElse dc.ColumnName = "other_institute" OrElse dc.ColumnName = "other_resultcode" Then
                        '        strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        '    End If
                        'End If
                        If CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("qualificationunkid")) > 0 Then
                            If dc.ColumnName = "Qualification" Then
                                strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                            End If
                        Else
                            If dc.ColumnName = "other_qualification" Then
                                strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                            End If
                        End If
                        If CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("instituteunkid")) > 0 Then
                            If dc.ColumnName = "institute" Then
                                strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                            End If
                        Else
                            If dc.ColumnName = "other_institute" Then
                                strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                            End If
                        End If
                        If CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("resultunkid")) > 0 Then
                            If dc.ColumnName = "resultcode" Then
                                strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                            End If
                        Else
                            If dc.ColumnName = "other_resultcode" Then
                                strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                            End If
                        End If

                        If dc.ColumnName = "gpacode" Then
                            strBuilder.Append("<TD STYLE='font-size:14px;text-align:right;'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        ElseIf dc.ColumnName = "startdate" Then
                            Dim strDuration As String = CDate(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString()).ToString("dd-MMM-yyyy") & " - "
                            If CDate(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("enddate")).Date <> CDate("01/Jan/1900").Date Then
                                strDuration &= CDate(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("enddate")).ToString("dd-MMM-yyyy")
                            Else
                                strDuration &= "Present"
                            End If
                            strBuilder.Append("<TD STYLE='font-size:14px'> " & strDuration & "</TD>")
                        End If

                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If

            If dsExperience IsNot Nothing AndAlso dsExperience.Tables(0).Rows.Count > 0 Then
                strBuilder.Append("<DIV ID='divExperience' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>Work Experience</DIV>")
                strBuilder.Append("<BR></BR>")
                strBuilder.Append("<TABLE ID = 'dlExperience'  STYLE='border-collapse: collapse;width: 100%'>")

                For Each dr As DataRow In dsExperience.Tables(0).Rows
                    Dim strEmployerName As String = ""
                    Dim strEmployer As String = ""
                    Dim strOPhone As String = ""

                    If dr.Item("Employer").ToString.Trim.Length > 0 Then
                        strEmployer = dr.Item("Employer").ToString
                    End If
                    If dr.Item("Phone").ToString.Trim.Length > 0 Then
                        strOPhone = "<b>Tel. No.:</b> " & dr.Item("Phone").ToString
                    End If
                    If strEmployer.Trim.Length > 0 Then
                        If strOPhone.Trim.Length > 0 Then
                            strEmployer &= ", " & strOPhone
                        End If
                    Else
                        strEmployer = strOPhone
                    End If
                    strEmployerName = strEmployer

                    strBuilder.Append("<TR STYLE='width:100%;'>")
                    strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                    strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Company:</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("<TD WIDTH= '75%'>")
                    strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("company").ToString() & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("</TR>")

                    If strEmployer.Trim.Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Employer:</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH ='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & strEmployerName & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    strBuilder.Append("<TR STYLE='width:100%;'>")
                    strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                    strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Designation:</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("<TD WIDTH='75%'>")
                    strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("Designation").ToString() & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("</TR>")

                    strBuilder.Append("<TR STYLE='width:100%;'>")
                    strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                    strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Duration:</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("<TD WIDTH='75%'>")
                    strBuilder.Append("<SPAN style='font-size:14px;'>" & GetDuration(dr).ToString() & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("</TR>")

                    If dr("Responsibility").ToString().Trim().Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Responsibility:</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("Responsibility").ToString().Trim() & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    If dr("achievements").ToString().Trim().Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Achievement:</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("achievements").ToString().Trim() & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    If dr("leavingReason").ToString().Trim().Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:70%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>Leaving Reason:</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("leavingReason").ToString().Trim() & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    If dsExperience.Tables(0).Rows.Count - 1 <> dsExperience.Tables(0).Rows.IndexOf(dr) Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%;'>")
                        strBuilder.Append("<HR STYLE='margin-top: 0px; margin-bottom: 0px; border-top: 1px solid #000;' />")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%' STYLE = ''>")
                        strBuilder.Append("<HR STYLE='margin-top: 0px; margin-bottom: 0px; border-top: 1px solid #000;' />")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                Next


                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If

            If strMemberships.Trim.Length > 0 OrElse strAchievements.Trim.Length > 0 OrElse strJournalResearchPaper.Trim.Length > 0 Then
                strBuilder.Append("<DIV ID = 'pnlOtherInfo' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>Other Information</DIV>")
                strBuilder.Append("<BR></BR>")

                If strMemberships.Trim.Length > 0 Then
                    strBuilder.Append("<DIV ID='divMembership' style='padding: 0px; width: 100%'>")
                    strBuilder.Append("<TABLE STYLE = 'WIDTH:100%'>")
                    strBuilder.Append("<TR>")
                    strBuilder.Append("<TD STYLE='width: 25%; vertical-align: top; font-weight: bold;font-size:14px;'>Membership:</TD>")
                    strBuilder.Append("<TD STYLE='width: 75%;font-size:14px;'>" & strMemberships.Trim() & "</TD>")
                    strBuilder.Append("</TR>")
                    strBuilder.Append("</TABLE>")
                    strBuilder.Append("</DIV>")
                End If

                If strAchievements.Trim.Length > 0 Then
                    strBuilder.Append("<DIV ID='divAchievements' style='padding: 0px; width: 100%'>")
                    strBuilder.Append("<TABLE STYLE = 'WIDTH:100%'>")
                    strBuilder.Append("<TR>")
                    strBuilder.Append("<TD STYLE='width: 25%; vertical-align: top; font-weight: bold;font-size:14px;'>Achievements:</TD>")
                    strBuilder.Append("<TD STYLE='width: 75%;font-size:14px;'>" & strAchievements.Trim() & "</TD>")
                    strBuilder.Append("</TR>")
                    strBuilder.Append("</TABLE>")
                    strBuilder.Append("</DIV>")
                End If

                If strJournalResearchPaper.Trim.Length > 0 Then
                    strBuilder.Append("<DIV ID='divJournalResearchPaper' style='padding: 0px; width: 100%'>")
                    strBuilder.Append("<TABLE STYLE = 'WIDTH:100%'>")
                    strBuilder.Append("<TR>")
                    strBuilder.Append("<TD STYLE='width: 25%; vertical-align: top; font-weight: bold;font-size:14px;'>Links / Journals / Research Papers:</TD>")
                    strBuilder.Append("<TD STYLE='width: 75%;font-size:14px;'>" & strJournalResearchPaper.Trim() & "</TD>")
                    strBuilder.Append("</TR>")
                    strBuilder.Append("</TABLE>")
                    strBuilder.Append("</DIV>")
                End If

                strBuilder.Append("</DIV>")
            End If


            If dsRefrence IsNot Nothing AndAlso dsRefrence.Tables(0).Rows.Count > 0 Then
                strBuilder.Append("<DIV ID='pnlRefrence' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>References</DIV>")
                strBuilder.Append("<BR></BR>")
                strBuilder.Append("<TABLE border='1' STYLE = 'width: 100%;border-collapse:collapse;'>")
                strBuilder.Append("<TR class='gridHeader'>")
                strBuilder.Append("<TD STYLE='width: 40%;font-weight:bold;font-size:14px;'>Name</TD>")
                strBuilder.Append("<TD STYLE='width: 48%;font-weight:bold;font-size:14px;'>Contact Details</TD>")
                strBuilder.Append("</TR>")

                For Each dr As DataRow In dsRefrence.Tables(0).Rows
                    strBuilder.Append("<TR class='gridItem'>")
                    Dim strContactDetails As String = ""
                    For Each dc As DataColumn In dsRefrence.Tables(0).Columns
                        If dc.ColumnName = "Address" Then
                            If dr.Item("Address").ToString.Length > 0 Then
                                strContactDetails &= dr.Item("Address").ToString
                            End If
                            If dr.Item("mobile_no").ToString.Trim.Length > 0 Then
                                If strContactDetails.Trim.Length > 0 Then strContactDetails &= "<br/>"
                                strContactDetails &= "<B> Mobile No. : </b>" & dr.Item("mobile_no").ToString
                            End If
                            If dr.Item("telephone_no").ToString.Trim.Length > 0 Then
                                If dr.Item("mobile_no").ToString.Trim.Length > 0 Then
                                    strContactDetails &= ", "
                                Else
                                    strContactDetails &= "<br/>"
                                End If
                                strContactDetails &= "<b>Tel. No. : </b> " & dr.Item("telephone_no").ToString
                            End If
                            strBuilder.Append("<TD STYLE='font-size:14px'>" & strContactDetails & "</TD>")

                        ElseIf dc.ColumnName = "name" Then
                            strBuilder.Append("<TD STYLE='font-size:14px'>" & dsRefrence.Tables(0).Rows(dsRefrence.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        End If
                    Next
                    strBuilder.Append("</TR>")
                Next
                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If
            strBuilder.Append("</DIV>")
            strBuilder.Append("</FORM>")
            strBuilder.Append("</BODY>")
            strBuilder.Append("</HTML>")

            mstrFilePath = Server.MapPath("~") & "/UploadImage"
            'Dim mstrCVName As String = Membership.GetUser(Session("email").ToString()).ProviderUserKey.ToString() & "_CV"
            Dim mstrCVName As String = Session("email").ToString.Replace(".", "_") & "_CV"
            If Directory.Exists(mstrFilePath & "\CV") = False Then
                Directory.CreateDirectory(mstrFilePath & "\CV")
            End If

            mstrFilePath &= "\CV"

            If File.Exists(mstrFilePath & "\" & mstrCVName & ".pdf") Then
                Try
                    File.Delete(mstrFilePath & "\" & mstrCVName & ".pdf")
                Catch ex As Exception

                End Try
            End If

            mstrFilePath &= "\" & mstrCVName & ".pdf"

            Using sw As New StringWriter(strBuilder)
                Using hw As New HtmlTextWriter(sw)
                    Dim sr As New StringReader(sw.ToString())
                    Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 10.0F)
                    Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(mstrFilePath, FileMode.Create))
                    pdfDoc.Open()
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr)
                    pdfDoc.Close()
                    writer.Close()
                    'Response.ContentType = "application/pdf"
                    'Response.AddHeader("content-disposition", "attachment;filename=" & mstrCVName & ".pdf")
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
        Return mstrFilePath
    End Function

    Protected Function GetDuration(dr As DataRow) As String
        Dim strDuration As String = ""
        Try
            strDuration = CDate(dr.Item("JoinDate")).ToString("dd-MMM-yyyy") & " - "
            If CDate(dr.Item("terminationdate")).Date <> CDate("01/Jan/1900").Date Then
                strDuration &= CDate(dr.Item("terminationdate")).ToString("dd-MMM-yyyy")
            Else
                strDuration &= "Present"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
        'Return Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(strDuration)
        Return AntiXss.AntiXssEncoder.HtmlEncode(strDuration, True)
    End Function

    Private Function Savefile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New System.IO.FileStream(fpath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim strWriter As New System.IO.StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, System.IO.SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    'Pinkal (18-May-2018) -- End

#End Region

#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            If IsPostBack = True Then
                blnHideApply = Me.ViewState("blnHideApply")
            End If
            Dim blnIsAdmin As Boolean = False
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                        blnIsAdmin = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                            blnIsAdmin = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                    blnIsAdmin = True
                End If
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Dim objApplicant As New clsApplicant
                Dim strMsg As String = ""
                If objApplicant.IsValidForApplyVacancy(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), True, strMsg) = False Then
                    If strMsg.Trim <> "" Then
                        If blnIsAdmin = False Then
                            ShowMessage(lblProfileMsg.Text & strMsg)
                            HttpContext.Current.Response.Redirect("~/User/UserHome.aspx", False)
                            Exit Sub
                        Else
                            blnHideApply = True
                        End If
                    End If
                    If blnIsAdmin = False Then
                        Exit Sub
                    End If
                End If
                'If Validation() = False Then Exit Sub
                Call FillVacancyList()
            Else
                'dsVacancyList = CType(Me.ViewState("dsVacancyList"), DataSet)

                For Each gvRow As DataListItem In dlVaanciesList.Items
                    If CBool(Session("OneCurriculamVitaeMandatory")) = True Then
                        Dim txtFilePath As Label = CType(gvRow.FindControl("lblFileNameCV"), Label)
                        Dim hfFilePath As HiddenField = CType(gvRow.FindControl("hfFileNameCV"), HiddenField)
                        txtFilePath.Text = Request.Form(hfFilePath.UniqueID)
                        If txtFilePath.Text.Trim = "" Then CType(gvRow.FindControl("pnlFileCV"), Panel).Visible = False
                        'Sohail (13 May 2022) -- Start
                        'Enhancement : AC2-311 : ZRA - After attaching one PDF system allows any image file attachment even if image attachment is not allowed in setting.
                        Dim flUploadCV As FileUpload = CType(gvRow.FindControl("flUploadCV"), FileUpload)
                        flUploadCV.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                        flUploadCV.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                        'Sohail (13 May 2022) -- End
                    End If

                        If CBool(Session("OneCoverLetterMandatory")) = True Then
                        Dim txtFilePath As Label = CType(gvRow.FindControl("lblFileNameCoverLetter"), Label)
                        Dim hfFilePath As HiddenField = CType(gvRow.FindControl("hfFileNameCoverLetter"), HiddenField)
                        txtFilePath.Text = Request.Form(hfFilePath.UniqueID)
                        If txtFilePath.Text.Trim = "" Then CType(gvRow.FindControl("pnlFileCoverLetter"), Panel).Visible = False
                        'Sohail (13 May 2022) -- Start
                        'Enhancement : AC2-311 : ZRA - After attaching one PDF system allows any image file attachment even if image attachment is not allowed in setting.
                        Dim flUploadCoverLetter As FileUpload = CType(gvRow.FindControl("flUploadCoverLetter"), FileUpload)
                        flUploadCoverLetter.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                        flUploadCoverLetter.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                        'Sohail (13 May 2022) -- End
                    End If

                    If CBool(Session("AttachmentQualificationMandatory")) = True Then
                        Dim txtFilePath As Label = CType(gvRow.FindControl("lblFileNameQuali"), Label)
                        Dim hfFilePath As HiddenField = CType(gvRow.FindControl("hfFileNameQuali"), HiddenField)
                        txtFilePath.Text = Request.Form(hfFilePath.UniqueID)
                        If txtFilePath.Text.Trim = "" Then CType(gvRow.FindControl("pnlFileQuali"), Panel).Visible = False
                        'Sohail (13 May 2022) -- Start
                        'Enhancement : AC2-311 : ZRA - After attaching one PDF system allows any image file attachment even if image attachment is not allowed in setting.
                        Dim flUploadQuali As FileUpload = CType(gvRow.FindControl("flUploadQuali"), FileUpload)
                        flUploadQuali.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                        flUploadQuali.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                        'Sohail (13 May 2022) -- End
                    End If
                Next

            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SearchJob_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ViewState("blnHideApply") = Session("blnHideApply")
            'Me.ViewState("dsVacancyList") = dsVacancyList
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " DataList Events "

    Protected Sub dlVaanciesList_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlVaanciesList.ItemCommand
        Dim objSearchJob As New clsSearchJob
        Try
            If e.CommandName.ToUpper = "APPLY" Then
                If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Dim Idx As Integer = e.Item.ItemIndex
                Dim txtVFF As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtVacancyFoundOutFrom"), TextBox)
                Dim txtCom As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtComments"), TextBox)
                Dim dtp As DateCtrl = CType(dlVaanciesList.Items(Idx).FindControl("dtpEarliestPossibleStartDate"), DateCtrl)
                Dim chk As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkAccept"), CheckBox)
                Dim lblVTitle As Label = CType(dlVaanciesList.Items(Idx).FindControl("objlblVacancyTitle"), Label)
                Dim chkVFF As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkVacancyFoundOutFrom"), CheckBox)
                Dim cboVFF As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlVacancyFoundOutFrom"), DropDownList)
                Dim hfVTitle As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfVTitle"), HiddenField)
                Dim hfExp As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfExp"), HiddenField)

                'Pinkal (01-Jan-2018) -- Start
                'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.
                Dim hdnVacanyTitleID As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hdnfieldVacancyTitleId"), HiddenField)
                'Pinkal (01-Jan-2018) -- End

                'If CBool(Session("VacancyFoundOutFromMandatory")) = True AndAlso txtVFF.Text.Trim = "" Then
                '    ShowMessage("Please enter Vacancy Found Out From.", MessageType.Errorr)
                '    txtVFF.Focus()
                '    Exit Try
                If CBool(Session("VacancyFoundOutFromMandatory")) = True Then
                    If chkVFF.Checked = True Then
                        If txtVFF.Text.Trim = "" Then
                            ShowMessage(lblVacFoundMsg.Text, MessageType.Errorr)
                            txtVFF.Focus()
                            Exit Try
                        End If
                    Else
                        If CInt(cboVFF.SelectedValue) <= 0 Then
                            ShowMessage(lblVacFoundMsg2.Text, MessageType.Errorr)
                            cboVFF.Focus()
                            Exit Try
                        End If
                    End If
                ElseIf CBool(Session("EarliestPossibleStartDateMandatory")) = True AndAlso dtp.GetDate = CDate("01/Jan/1900") Then
                    ShowMessage(lblPosStartMsg.Text, MessageType.Errorr)
                    dtp.Focus()
                    Exit Try
                ElseIf CBool(Session("EarliestPossibleStartDateMandatory")) = True AndAlso dtp.GetDate < Today.Date Then
                    ShowMessage(lblPosStartMsg2.Text, MessageType.Errorr)
                    dtp.Focus()
                    Exit Try
                ElseIf CBool(Session("CommentsMandatory")) = True AndAlso txtCom.Text.Trim = "" Then
                    ShowMessage(lblCommentsMsg.Text, MessageType.Errorr)
                    txtCom.Focus()
                    Exit Try
                End If

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-324 : ZRA - As an applicant with zero experience, I should be able to apply for jobs that require zero experience regardless of experience years set in config.
                If CInt(Session("RecruitMandatoryJobExperiences")) > 0 AndAlso CInt(hfExp.Value) > 0 Then
                    Dim dsExp As DataSet = (New clsApplicantExperience).GetApplicantExperience(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
                    If dsExp.Tables(0).Rows.Count <= 0 Then
                        ShowMessage(lblExpMsg.Text, MessageType.Errorr)
                        txtCom.Focus()
                        Exit Try
                    End If

                    Dim lngExpMonth As Long = (From p In dsExp.Tables(0) Select (CInt(DateDiff(DateInterval.Month, CDate(p.Item("joindate")), CDate(If(Format(CDate(p.Item("terminationdate")), "yyyyMMdd") = "19000101", DateAndTime.Now, CDate(p.Item("terminationdate")))))))).Sum()
                    'Dim lng As Double = 0
                    'For Each dsRow As DataRow In dsExp.Tables(0).Rows
                    '    lng += CInt(DateDiff(DateInterval.Month, CDate(dsRow.Item("joindate")), CDate(If(Format(CDate(dsRow.Item("terminationdate")), "yyyyMMdd") = "19000101", DateAndTime.Now, CDate(dsRow.Item("terminationdate"))))))
                    'Next
                    If lngExpMonth < CInt(hfExp.Value) Then
                        ShowMessage(lblExpMsg.Text, MessageType.Errorr)
                        txtCom.Focus()
                        Exit Try
                    End If
                End If
                'Sohail (13 May 2022) -- End


                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" AndAlso CBool(Session("BirthCertificateAttachment")) Then
                    Dim ds As DataSet = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.APPLICANT_BIRTHINFO), True, 0)
                    If ds Is Nothing OrElse ds.Tables(0).Rows.Count <= 0 Then
                        ShowMessage(lblTRABirthCertificateAttachement.Text, MessageType.Errorr)
                        txtCom.Focus()
                        Exit Try
                    End If  'If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                End If  'If If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA"  AndAlso CBool(Session("BirthCertificateAttachment")) Then 
                'Pinkal (30-Sep-2023) -- End


                Dim lstAttach As New List(Of dtoAddAppAttach)
                Dim dtoAttach As dtoAddAppAttach = Nothing
                Dim flUploadCV As FileUpload = Nothing
                Dim flUploadCoverLetter As FileUpload = Nothing
                Dim flUploadQuali As FileUpload = Nothing

                If CBool(Session("OneCurriculamVitaeMandatory")) = True Then
                    'Dim txtFilePath As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtFilePathCV"), TextBox)
                    Dim txtFilePath As Label = CType(dlVaanciesList.Items(Idx).FindControl("lblFileNameCV"), Label)
                    If txtFilePath.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Curriculam Vitae"), MessageType.Errorr)
                        Exit Try
                    End If

                    Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadCV"), FileUpload)
                    flUploadCV = flUpload
                    Dim ddlDocType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlDocTypeCV"), DropDownList)
                    Dim ddlAttachType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlAttachTypeCV"), DropDownList)
                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("CVSavePath")
                    Dim hfDocSize As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfDocSizeCV"), HiddenField)
                    Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                    Dim decSize As Decimal = 0
                    Decimal.TryParse(hfDocSize.Value, decSize)

                    dtoAttach = New dtoAddAppAttach
                    With dtoAttach
                        .CompCode = Session("CompCode").ToString
                        .ComUnkID = CInt(Session("companyunkid"))
                        .ApplicantUnkid = CInt(Session("applicantunkid"))
                        .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                        .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                        .AttachrefId = CInt(ddlDocType.SelectedValue)
                        .FolderName = strFolder
                        .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                        .Filename = txtFilePath.Text
                        .Fileuniquename = strUnkImgName
                        .File_size = CInt(decSize / 1024)
                        .Attached_Date = DateAndTime.Now
                        .flUpload = Session(flUpload.ClientID)
                    End With
                    lstAttach.Add(dtoAttach)
                End If

                If CBool(Session("OneCoverLetterMandatory")) = True Then
                    'Dim txtFilePath As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtFilePathCoverLetter"), TextBox)
                    Dim txtFilePath As Label = CType(dlVaanciesList.Items(Idx).FindControl("lblFileNameCoverLetter"), Label)
                    'Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadCoverLetter"), FileUpload)
                    If txtFilePath.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Cover Letter"), MessageType.Errorr)
                        Exit Try
                    End If

                    Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadCoverLetter"), FileUpload)
                    flUploadCoverLetter = flUpload
                    Dim ddlDocType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlDocTypeCoverLetter"), DropDownList)
                    Dim ddlAttachType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlAttachTypeCoverLetter"), DropDownList)
                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("CoverLetterSavePath")
                    Dim hfDocSize As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfDocSizeCoverLetter"), HiddenField)
                    Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                    Dim decSize As Decimal = 0
                    Decimal.TryParse(hfDocSize.Value, decSize)

                    dtoAttach = New dtoAddAppAttach
                    With dtoAttach
                        .CompCode = Session("CompCode").ToString
                        .ComUnkID = CInt(Session("companyunkid"))
                        .ApplicantUnkid = CInt(Session("applicantunkid"))
                        .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                        .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                        .AttachrefId = CInt(ddlDocType.SelectedValue)
                        .FolderName = strFolder
                        .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                        .Filename = txtFilePath.Text
                        .Fileuniquename = strUnkImgName
                        .File_size = CInt(decSize / 1024)
                        .Attached_Date = DateAndTime.Now
                        .flUpload = Session(flUpload.ClientID)
                    End With
                    lstAttach.Add(dtoAttach)
                End If

                If CBool(Session("AttachmentQualificationMandatory")) = True AndAlso Session("CompCode").ToString <> "TRA" Then
                    'Hemant (17 Oct 2023) -- [AndAlso Session("CompCode").ToString <> "TRA"]
                    'Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadQuali"), FileUpload)
                    'Dim txtFilePath As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtFilePathQuali"), TextBox)
                    Dim txtFilePath As Label = CType(dlVaanciesList.Items(Idx).FindControl("lblFileNameQuali"), Label)
                    If txtFilePath.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Qualification"), MessageType.Errorr)
                        Exit Try
                    End If

                    Dim flUpload As FileUpload = CType(dlVaanciesList.Items(Idx).FindControl("flUploadQuali"), FileUpload)
                    flUploadQuali = flUpload
                    Dim ddlDocType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlDocTypeQuali"), DropDownList)
                    Dim ddlAttachType As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlAttachTypeQuali"), DropDownList)
                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
                    Dim hfDocSize As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfDocSizeQuali"), HiddenField)
                    Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & txtFilePath.Text.Substring(txtFilePath.Text.LastIndexOf(".") + 1)
                    Dim decSize As Decimal = 0
                    Decimal.TryParse(hfDocSize.Value, decSize)

                    dtoAttach = New dtoAddAppAttach
                    With dtoAttach
                        .CompCode = Session("CompCode").ToString
                        .ComUnkID = CInt(Session("companyunkid"))
                        .ApplicantUnkid = CInt(Session("applicantunkid"))
                        .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                        .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                        .AttachrefId = CInt(ddlDocType.SelectedValue)
                        .FolderName = strFolder
                        .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                        .Filename = txtFilePath.Text
                        .Fileuniquename = strUnkImgName
                        .File_size = CInt(decSize / 1024)
                        .Attached_Date = DateAndTime.Now
                        .flUpload = Session(flUpload.ClientID)
                    End With
                    lstAttach.Add(dtoAttach)
                End If

                If chk.Visible = True AndAlso chk.Checked = False Then
                    ShowMessage(lblAcceptMsg.Text, MessageType.Errorr)
                    Exit Try
                End If

                Dim hpf As HttpPostedFile
                For Each dto As dtoAddAppAttach In lstAttach
                    If System.IO.Directory.Exists(Server.MapPath("~") & "\" & dto.FolderName) = False Then
                        System.IO.Directory.CreateDirectory(Server.MapPath("~") & "\" & dto.FolderName)
                    End If

                    hpf = dto.flUpload
                    hpf.SaveAs(Server.MapPath("~") & "\" & dto.FolderName & "\" & dto.Fileuniquename)
                Next

                'Sohail (10 Oct 2018) -- Start
                'Issue : EarliestPossibleStartDate, Comments and VacancyFoundOutFrom not getting saved.
                'If objSearchJob.AppliedVacancy(strCompCode:=Session("CompCode").ToString,
                '                             intComUnkID:=CInt(Session("companyunkid")),
                '                             intApplicantUnkid:=CInt(Session("applicantunkid")),
                '                             intVacancyUnkId:=CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString),
                '                             dtEarliest_possible_startdate:=CDate("01/Jan/1900"),
                '                             strComments:="",
                '                             strVacancy_found_out_from:=""
                '                             ) = True Then
                Dim dtoVac As New dtoAddAppVacancy
                With dtoVac
                    .CompCode = Session("CompCode").ToString
                    .ComUnkID = CInt(Session("companyunkid"))
                    .ApplicantUnkid = CInt(Session("applicantunkid"))
                    .VacancyUnkId = CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString)
                    .Earliest_possible_startdate = dtp.GetDate
                    .Comments = txtCom.Text.Trim
                    .Vacancy_found_out_from = If(chkVFF.Checked = True, txtVFF.Text.Trim, "").ToString
                    .Vacancy_found_out_from_UnkId = CInt(If(chkVFF.Checked = True, 0, CInt(cboVFF.SelectedValue)))
                End With

                Dim intUnkId As Integer = 0
                If objSearchJob.AddAppVacancy_Attachment(objAddAppVacancy:=dtoVac _
                                                      , objAddAppAttach:=lstAttach _
                                             , intRet_UnkID:=intUnkId
                                             ) = True Then
                    'If objSearchJob.AppliedVacancy(strCompCode:=Session("CompCode").ToString,
                    '                         intComUnkID:=CInt(Session("companyunkid")),
                    '                         intApplicantUnkid:=CInt(Session("applicantunkid")),
                    '                         intVacancyUnkId:=CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString),
                    '                         dtEarliest_possible_startdate:=dtp.GetDate,
                    '                         strComments:=txtCom.Text.Trim,
                    '                         strVacancy_found_out_from:=If(chkVFF.Checked = True, txtVFF.Text.Trim, "").ToString,
                    '                         intrVacancy_found_out_from_UnkId:=CInt(If(chkVFF.Checked = True, 0, CInt(cboVFF.SelectedValue))) _
                    '                         , intRet_UnkID:=intUnkId
                    '                         ) = True Then
                    'Sohail (10 Oct 2018) -- End

                    dlVaanciesList.DataBind()
                    'ShowMessage("Job Applied Successfully !", MessageType.Info)

                    'Pinkal (01-Jan-2018) -- Start
                    'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.
                    'Dim blnSendJobConfirmationEmail As Boolean = False
                    'If blnSendJobConfirmationEmail = True Then


                    If CBool(Session("isvacancyalert")) Then

                        Dim mintVacancyTitleID As Integer = CInt(hdnVacanyTitleID.Value)

                        Dim objAppNotificationSettings As New clsNotificationSettings
                        Dim mintPriority As Integer = 0
                        Dim dsList As DataSet = objAppNotificationSettings.GetApplicantNotificationSettings(xCompanyID:=CInt(Session("companyunkid")), xCompanyCode:=Session("CompCode").ToString() _
                                                                                                                                                          , xApplicantID:=CInt(Session("applicantunkid")), intMasterType:=clsCommon_Master.enCommonMaster.VACANCY_MASTER)


                        Dim drRow() As DataRow = dsList.Tables(0).Select("isactive = 1 AND priority > 0")

                        If drRow.Length < CInt(Session("maxvacancyalert")) Then

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                If Not IsDBNull(dsList.Tables(0).Compute("Max(priority)", "isactive = 1")) Then
                                    mintPriority = dsList.Tables(0).Compute("Max(priority)", "isactive = 1")
                                End If
                            End If

                            mintPriority += 1

                            objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(xCompanyID:=CInt(Session("companyunkid")), xCompanyCode:=Session("CompCode").ToString(), xApplicantID:=CInt(Session("applicantunkid")) _
                                                                                                                                  , xVacancytitleId:=mintVacancyTitleID, xPriority:=mintPriority, xIsActive:=True)


                            dsList.Clear()
                            dsList = Nothing
                            objAppNotificationSettings = Nothing

                        End If

                    End If

                    If Convert.ToBoolean(Session("issendjobconfirmationemail")) Then
                        'Pinkal (01-Jan-2018) -- End
                        Dim objApplicant As New clsApplicant
                        If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                                                   , strCompCode:=Session("CompCode").ToString() _
                                                   , ByRefblnIsActive:=False
                                                   ) = False Then

                            objApplicant = Nothing
                            Exit Try
                        Else
                            objApplicant = Nothing
                        End If

                        Dim objCompany1 As New clsCompany
                        Session("e_emailsetupunkid") = 0
                        objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))

                        Dim profile As UserProfile = UserProfile.GetUserProfile(Session("email"))
                        Dim sFName As String = profile.FirstName
                        Dim sLName As String = profile.LastName
                        If sFName.Trim = "" Then sFName = Session("email")
                        Dim strSubject As String = "Your application is successfully sent to " + Session("CompName").ToString

                        'Pinkal (18-May-2018) -- Start
                        'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
                        'Sohail (06 Nov 2019) -- Start
                        'NMB Recruitment UAT # 20: Email sent back to employee after application of a vacancy should read as below. And should not have Powered by Aruti Signature.
                        Dim strValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=Session("CompCode").ToString() _
                                                                  , intCompanyUnkID:=CInt(Session("companyunkid")) _
                                                                  , strKeyName:="JOBCONFIRMATIONTEMPLATEID"
                                                                  )

                        Dim strBody As String = ""
                        Dim mstrFilePath As String = ""
                        Dim intID As Integer
                        Integer.TryParse(strValue, intID)
                        Dim intTemplateFound As Boolean = False
                        If intID > 0 AndAlso intUnkId > 0 Then
                            Dim objLetterType As New clsLetterType
                            Dim lstLetter As List(Of clsLetterType) = objLetterType.GetList(intCompanyId:=CInt(Session("companyunkid")) _
                                                                                          , strCompanyCode:=Session("CompCode").ToString _
                                                                                          , intUnkId:=intID
                                                                                          )

                            If lstLetter IsNot Nothing AndAlso lstLetter.Count > 0 Then
                                Dim strLetterContent As String = lstLetter(0)._lettercontent
                                Dim intFIdx As Integer = strLetterContent.IndexOf(CChar("{")) 'to remove xml start and end tags
                                Dim intLIdx As Integer = strLetterContent.LastIndexOf(CChar("}")) 'to remove xml start and end tags
                                strLetterContent = strLetterContent.Substring(intFIdx, intLIdx - intFIdx + 1) 'to remove xml start and end tags

                                If strLetterContent.Trim.Length > 0 Then
                                    Dim objApplied As New clsApplicantApplyJob
                                    Dim dsAppVac As DataSet = objApplied.GetApplicantAppliedJob(strCompCode:=Session("CompCode").ToString _
                                                                     , intComUnkID:=CInt(Session("companyunkid")) _
                                                                     , intApplicantUnkId:=CInt(Session("applicantunkid")) _
                                                                     , intAppvacancytranunkid:=intUnkId
                                                                     )

                                    If dsAppVac.Tables(0).Rows.Count > 0 Then
                                        Dim objLF As New clsLetterFields
                                        Dim dsField As DataSet = objLF.GetList(lstLetter(0)._fieldtypeid)
                                        intTemplateFound = True
                                        Dim strDataName As String = ""
                                        Dim StrCol As String = ""

                                        strLetterContent = strLetterContent.Replace("#CompanyCode#", Session("CompCode").ToString)
                                        strLetterContent = strLetterContent.Replace("#CompanyName#", Session("CompName").ToString)

                                        For Each dsRow As DataRow In dsField.Tables(0).Rows
                                            StrCol = dsRow.Item("Name").ToString

                                            If strLetterContent.Contains("#" & StrCol & "#") Then

                                                If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                                    strDataName = strLetterContent.Replace("#" & StrCol & "#", "#imgcmp#")
                                                Else
                                                    If dsAppVac.Tables(0).Columns.Contains(StrCol) = True Then
                                                        If StrCol = "Pay_Range_From" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                                                        ElseIf StrCol = "Pay_Range_To" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                                                        ElseIf StrCol = "VacancyOpeningDate" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                                                        ElseIf StrCol = "VacancyClosingDate" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                                                        Else
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", dsAppVac.Tables(0).Rows(0)(StrCol).ToString)
                                                        End If
                                                    Else
                                                        strDataName = strLetterContent.Replace("#" & StrCol & "#", "")
                                                    End If
                                                End If

                                                strLetterContent = strDataName
                                            End If
                                        Next

                                        Dim htmlOutput = "Template.html"
                                        Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(strLetterContent, contentUriPrefix)

                                        strBody = htmlResult._HTML

                                        If Session("isattachapplicantcv") Then
                                            mstrFilePath = GetApplicantCV()
                                        End If

                                    End If
                                End If
                            End If
                        End If
                        'Sohail (06 Nov 2019) -- End

                        If intTemplateFound = False Then

                            'strBody = "<p style='font-size:14px'>Thank you for expressing interest in <b>" + lblVTitle.Text + "</b> role </p>"
                            strBody = "<p style='font-size:14px'>Thank you for expressing interest in <b>" + hfVTitle.Value + "</b> role </p>"
                            'Gajanan (04 July 2020) -- Start
                            'Enhancment: #0004765
                            'strBody += "<p style='font-size:14px'>This email is to confirm that we have received your application. You will be contacted once it has been reviewed.</p>"
                            strBody += "<p style='font-size:14px'>This email is to confirm that we have received your application. You will only be contacted if shortlisted.</p>"
                            'Gajanan (04 July 2020) -- End

                            If Session("isattachapplicantcv") Then
                                mstrFilePath = GetApplicantCV()
                                strBody += "<p style='font-size:14px'>Attached is the CV generated as per the information you provided in the application form.</p>"
                            End If

                            strBody += "<p style='font-size:14px'>Please do not reply to this email as it is auto generated.</p>"
                            Dim sValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=Session("CompCode").ToString _
                                                                    , intCompanyUnkID:=CInt(Session("companyunkid")) _
                                                                    , strKeyName:="SHOWARUTISIGNATURE"
                                                                    )
                            Dim blnValue As Integer
                            Boolean.TryParse(sValue, blnValue)
                            If blnValue = True Then
                                strBody += "<p  style='font-size:12px'><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p> "
                            End If
                            'strBody += "<p style='font-size:12px'><b>#CompanyName# Recruitment</b></p> "
                            strBody += "<p style='font-size:12px'><b>#CompanyName# Talent Acquisition</b></p> "
                            strBody += "#imgcmp#"
                            strBody += " #imgaruti#"

                            'Dim objMail As New clsMail
                            'If objMail.SendEmail(strToEmail:=Session("email") _
                            '                 , strSubject:=strSubject _
                            '                 , strBody:=strBody _
                            '                 , blnAddLogo:=True _
                            '                 , blnSuperAdmin:=False, mstrAttachedFiles:=mstrFilePath
                            '                 ) = False Then

                            '    ' e.Cancel = True
                            'End If

                            'Pinkal (18-May-2018) -- End
                        End If

                        'Sohail (06 Nov 2019) -- Start
                        'NMB Recruitment UAT # 20: Email sent back to employee after application of a vacancy should read as below. And should not have Powered by Aruti Signature.
                        Dim objMail As New clsMail
                        If objMail.SendEmail(strToEmail:=Session("email") _
                                         , strSubject:=strSubject _
                                         , strBody:=strBody _
                                         , blnAddLogo:=True _
                                         , blnSuperAdmin:=False, mstrAttachedFiles:=mstrFilePath
                                         ) = False Then

                            ' e.Cancel = True
                        End If
                        'Sohail (06 Nov 2019) -- End
                    End If

                End If

                Call FillVacancyList()
                ShowMessage(lblJobSuccessMsg.Text, MessageType.Info)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objSearchJob = Nothing
        End Try
    End Sub

    Private Sub dlVaanciesList_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlVaanciesList.ItemDataBound
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Dim dsAttachType As DataSet
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                If blnHideApply = True Then CType(e.Item.FindControl("btnApply"), LinkButton).Visible = False

                Dim lbl As Label = CType(e.Item.FindControl("objlblIsApplyed"), Label)
                If CBool(lbl.Text) = True Then
                    CType(e.Item.FindControl("lblApplyText"), Label).Text = "Applied"
                    CType(e.Item.FindControl("btnApply"), LinkButton).Enabled = False
                    CType(e.Item.FindControl("divApplied"), Control).Visible = True
                    If Session("applicant_declaration").ToString.Trim.Length > 0 Then
                        CType(e.Item.FindControl("pnlDis"), Panel).Visible = False
                    End If
                Else
                    'If Session("applicant_declaration").ToString.Trim.Length > 0 Then
                    '    CType(e.Item.FindControl("btnApply"), LinkButton).Attributes.Add("disabled", "disabled")
                    'End If
                    If Session("applicant_declaration").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("pnlDis"), Panel).Visible = False
                    End If
                End If
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("experience").ToString.Trim.Length <= 0 Then
                CType(e.Item.FindControl("objlblVacancyTitle"), Label).Text = drv.Item("vacancytitle").ToString & " (" & drv.Item("noposition").ToString & " " & lblPositionMsg.Text & ")"
                'If drv.Item("experience").ToString.Trim.Length <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                'If CInt(drv.Item("experience")) <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                If CInt(drv.Item("experience")) <= 0 AndAlso drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divExp"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isexpbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isexpitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    'If drv.Item("other_experience").ToString.Trim = "" Then
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_experience").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                    Else
                        '    Dim htmlOutput = "Document.html"
                        '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                        '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_experience").ToString, contentUriPrefix)
                        '    htmlResult.WriteToFile(htmlOutput)
                        '    CType(e.Item.FindControl("objlblExp"), Label).Text = htmlResult._HTML
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_experience").ToString.Trim).Length > 0 Then
                        If drv.Item("other_experience").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_experience").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_experience").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If
                            Dim strRTF As String = drv.Item("other_experience").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_experience").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_experience").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_experience").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblExp"), Label).Text = strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If drv.Item("noposition").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divNoPosition"), Control).Visible = False
                End If

                'Gajanan (04 July 2020) -- Start
                'Enhancment: #0004765
                If drv.Item("experience_comment").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblExpCmt"), Control).Visible = False
                End If
                'Gajanan (04 July 2020) -- End

                'Sohail (11 Nov 2021) -- Start
                'NMB Enhancement: Display Job Location on published vacancy in online recruitment when company code is NMB. e.g Job Location - Northern Zone, Arusha.
                Dim arrJobLocation As New ArrayList
                If Session("CompCode").ToString.ToUpper = "NMB" Then
                    If drv.Item("classgroupname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classgroupname").ToString)
                    End If
                    If drv.Item("classname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classname").ToString)
                    End If
                End If
                CType(e.Item.FindControl("objlblJobLocation"), Label).Text = String.Join(", ", TryCast(arrJobLocation.ToArray(GetType(String)), String()))
                If arrJobLocation.Count <= 0 Then
                    CType(e.Item.FindControl("divJobLocation"), Control).Visible = False
                End If
                'Sohail (11 Nov 2021) -- End

                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                'If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                If drv.Item("skill").ToString.Trim.Length <= 0 AndAlso drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divSkill"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isskillbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isskillitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Hemant (08 Jul 2021) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "•" & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'If drv.Item("other_skill").ToString.Trim= "" Then
                    '    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "•" & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'Else
                    '        Dim htmlOutput = "Document.html"
                    '        Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_skill").ToString, contentUriPrefix)
                    '        htmlResult.WriteToFile(htmlOutput)
                    '        CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_skill").ToString.Trim.Length <= 0 Then
                        'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "•" & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                        Dim str As String = ""
                        If drv.Item("skill").ToString.Trim.Length > 0 Then
                            str = "<ul class='p-l-17'>"
                            Dim arr() As String = drv.Item("skill").ToString.Split(";")
                            For i As Integer = 0 To arr.Length - 1
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Next
                            str &= "</ul>"
                        End If
                        CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_skill").ToString.Trim).Length > 0 Then
                        If drv.Item("other_skill").ToString.Trim.Length > 0 Then
                            Dim str As String = ""
                            If drv.Item("skill").ToString.Trim.Length > 0 Then
                                str = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("skill").ToString.Split(";")
                                For i As Integer = 0 To arr.Length - 1
                                    str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                Next
                                str &= "</ul>"
                            End If
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_skill").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_skill").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                            
                            Dim strRTF As String = drv.Item("other_skill").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_skill").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_skill").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_skill").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & str & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                    'Hemant (08 Jul 2021) -- End
                End If

                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
                'If drv.Item("Lang").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                'If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                If drv.Item("Lang").ToString.Trim.Length <= 0 AndAlso drv.Item("other_language").ToString.Trim.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divLang"), Control).Visible = False
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                Else
                    'If drv.Item("other_language").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_language").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_language").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_language").ToString.Trim).Length > 0 Then
                        If drv.Item("other_language").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_language").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_language").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                          
                            Dim strRTF As String = drv.Item("other_language").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_language").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_language").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_language").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblLang"), Label).Text = drv.Item("Lang").ToString & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                If CDec(drv.Item("pay_from").ToString) <= 0 AndAlso CDec(drv.Item("pay_to").ToString) <= 0 Then
                    CType(e.Item.FindControl("divScale"), Control).Visible = False
                Else
                    'Dim str As String = Math.Round(CDec(drv.Item("pay_from").ToString), 2) & " - " & Math.Round(CDec(drv.Item("pay_to").ToString), 2)
                    Dim str As String = Format(CDec(drv.Item("pay_from").ToString), "##,##,##,##,##0.00") & " - " & Format(CDec(drv.Item("pay_to").ToString), "##,##,##,##,##0.00")
                    CType(e.Item.FindControl("objlblScale"), Label).Text = str
                End If

                If drv.Item("openingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divOpenningDate"), Control).Visible = False
                End If
                If drv.Item("closingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divClosuingDate"), Control).Visible = False
                End If
                If drv.Item("remark").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divRemark"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("remark").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("remark").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("remark").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblJobDiscription"), Label).Text = strResult
                End If
                If drv.Item("duties").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divResponsblity"), Control).Visible = False
                Else
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    If drv.Item("duties").ToString.Contains("") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("•") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("•")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                'str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") & "</li>"
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    ElseIf drv.Item("duties").ToString.Contains("·") = True Then
                        str = "<ul class='p-l-17'>"
                        Dim arr() As String = drv.Item("duties").ToString.Split("·")
                        For i As Integer = 0 To arr.Length - 1
                            If i > 0 Then
                                str &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            Else
                                str &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            End If
                        Next
                        str &= "</ul>"
                    End If
                    'Sohail (13 Sep 2021) -- End
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblResponsblity"), Label).Text = strResult
                End If
                'Sohail (13 Sep 2021) -- Start
                'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                'If drv.Item("Qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                'If drv.Item("Qualification").ToString.Length <= 0 AndAlso Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString).Length <= 0 Then
                If drv.Item("Qualification").ToString.Length <= 0 AndAlso drv.Item("other_qualification").ToString.Length <= 0 Then
                    'Sohail (13 Sep 2021) -- End
                    CType(e.Item.FindControl("divQualification"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isqualibold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isqualiitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Sohail (13 Sep 2021) -- Start
                    'NMB Enhancement :  : Show free text for other qualification, other skill, other language and other experience in self service and recruitment portal.
                    'CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'If drv.Item("other_qualification").ToString.Trim = "" Then
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    'Else
                    '    Dim htmlOutput = "Document.html"
                    '    Dim contentUriPrefix = System.IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(drv.Item("other_qualification").ToString, contentUriPrefix)
                    '    htmlResult.WriteToFile(htmlOutput)
                    '    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & htmlResult._HTML
                    'End If
                    'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length <= 0 Then
                    If drv.Item("other_qualification").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                    Else
                        'If Basepage.RemoveRTFFormatting(drv.Item("other_qualification").ToString.Trim).Length > 0 Then
                        If drv.Item("other_qualification").ToString.Trim.Length > 0 Then
                            'Dim rtf As New System.Windows.Forms.RichTextBox
                            'Try
                            '    rtf.Rtf = drv.Item("other_qualification").ToString
                            'Catch ex As Exception
                            '    rtf.Text = drv.Item("other_qualification").ToString
                            'End Try
                            'Dim strRTF As String = rtf.Text.ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            'If rtf.Text.Contains("") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("•") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("•")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'ElseIf rtf.Text.Contains("·") = True Then
                            '    strRTF = "<ul class='p-l-17'>"
                            '    Dim arr() As String = rtf.Text.ToString.Split("·")
                            '    For i As Integer = 0 To arr.Length - 1
                            '        If i > 0 Then
                            '            strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                            '        Else
                            '            strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            '        End If
                            '    Next
                            '    strRTF &= "</ul>"
                            'End If                           
                            Dim strRTF As String = drv.Item("other_qualification").ToString.Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                            If drv.Item("other_qualification").ToString.Contains("") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("•") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("•")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            ElseIf drv.Item("other_qualification").ToString.Contains("·") = True Then
                                strRTF = "<ul class='p-l-17'>"
                                Dim arr() As String = drv.Item("other_qualification").ToString.Split("·")
                                For i As Integer = 0 To arr.Length - 1
                                    If i > 0 Then
                                        strRTF &= "<li>" & arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "") & "</li>"
                                    Else
                                        strRTF &= arr(i).Replace(vbCrLf, "<br />").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                                    End If
                                Next
                                strRTF &= "</ul>"
                            End If
                            Dim strResult As String = String.Concat(Regex.Split(strRTF, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                            strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                            CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI & " " & strResult
                        End If
                    End If
                    'Sohail (13 Sep 2021) -- End
                End If

                'Sohail (10 Oct 2018) -- Start
                Dim lblVacancyFoundOutFrom As Label = CType(e.Item.FindControl("lblVacancyFoundOutFrom"), Label)
                If CBool(Session("VacancyFoundOutFromMandatory")) = True Then
                    lblVacancyFoundOutFrom.CssClass = "required"
                Else
                    lblVacancyFoundOutFrom.CssClass = ""
                    'Sohail (12 Nov 2020) -- Start
                    'NMB Enhancement # : - Hide those items which are not set as mandatory on search job page in recruitment portal and hide all items in ESS on same page.
                    CType(e.Item.FindControl("pnlVacancyFoundCol"), Panel).Visible = False
                    'Sohail (12 Nov 2020) -- End
                End If

                Dim lblEarliestPossibleStartDateMandatory As Label = CType(e.Item.FindControl("lblEarliestPossibleStartDate"), Label)
                If CBool(Session("EarliestPossibleStartDateMandatory")) = True Then
                    lblEarliestPossibleStartDateMandatory.CssClass = "required"
                Else
                    lblEarliestPossibleStartDateMandatory.CssClass = ""
                    'Sohail (12 Nov 2020) -- Start
                    'NMB Enhancement # : - Hide those items which are not set as mandatory on search job page in recruitment portal and hide all items in ESS on same page.
                    CType(e.Item.FindControl("pnlEarliestPossibleStartDateCol"), Panel).Visible = False
                    'Sohail (12 Nov 2020) -- End
                End If

                Dim lblComments As Label = CType(e.Item.FindControl("lblComments"), Label)
                If CBool(Session("CommentsMandatory")) = True Then
                    lblComments.CssClass = "required"
                Else
                    lblComments.CssClass = ""
                    'Sohail (12 Nov 2020) -- Start
                    'NMB Enhancement # : - Hide those items which are not set as mandatory on search job page in recruitment portal and hide all items in ESS on same page.
                    CType(e.Item.FindControl("pnlCommentsCol"), Panel).Visible = False
                    'Sohail (12 Nov 2020) -- End
                End If
                'Sohail (10 Oct 2018) -- End
            End If

            Dim cboVacancyFoundOutFrom As DropDownList = CType(e.Item.FindControl("ddlVacancyFoundOutFrom"), DropDownList)
            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.VACANCY_SOURCE)
            With cboVacancyFoundOutFrom
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim pnlCV As Panel = CType(e.Item.FindControl("pnlCV"), Panel)
            Dim pnlCoverLetter As Panel = CType(e.Item.FindControl("pnlCoverLetter"), Panel)
            Dim pnlQuali As Panel = CType(e.Item.FindControl("pnlQuali"), Panel)

            pnlCV.Visible = CBool(Session("OneCurriculamVitaeMandatory"))
            pnlCoverLetter.Visible = CBool(Session("OneCoverLetterMandatory"))
            pnlQuali.Visible = CBool(Session("AttachmentQualificationMandatory"))

            dsAttachType = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
            dsCombo = objApplicant.GetDocType()
            If pnlCV.Visible = True Then
                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.CURRICULAM_VITAE).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
                Dim ddlDocTypeCV As DropDownList = CType(e.Item.FindControl("ddlDocTypeCV"), DropDownList)
                With ddlDocTypeCV
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim ddlAttachTypeCV As DropDownList = CType(e.Item.FindControl("ddlAttachTypeCV"), DropDownList)
                With ddlAttachTypeCV
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim flUpload As FileUpload = CType(e.Item.FindControl("flUploadCV"), FileUpload)
                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))

                Dim pnlFile As Panel = CType(e.Item.FindControl("pnlFileCV"), Panel)
                pnlFile.Visible = False
            End If

            If pnlCoverLetter.Visible = True Then
                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.COVER_LETTER).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
                Dim ddlDocTypeCoverLetter As DropDownList = CType(e.Item.FindControl("ddlDocTypeCoverLetter"), DropDownList)
                With ddlDocTypeCoverLetter
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim ddlAttachTypeCoverLetter As DropDownList = CType(e.Item.FindControl("ddlAttachTypeCoverLetter"), DropDownList)
                With ddlAttachTypeCoverLetter
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim flUpload As FileUpload = CType(e.Item.FindControl("flUploadCoverLetter"), FileUpload)
                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))

                Dim pnlFile As Panel = CType(e.Item.FindControl("pnlFileCoverLetter"), Panel)
                pnlFile.Visible = False
            End If

            If pnlQuali.Visible = True Then
                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.QUALIFICATIONS).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
                Dim ddlDocTypeQuali As DropDownList = CType(e.Item.FindControl("ddlDocTypeQuali"), DropDownList)
                With ddlDocTypeQuali
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim ddlAttachTypeQuali As DropDownList = CType(e.Item.FindControl("ddlAttachTypeQuali"), DropDownList)
                With ddlAttachTypeQuali
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                Dim flUpload As FileUpload = CType(e.Item.FindControl("flUploadQuali"), FileUpload)
                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))

                Dim pnlFile As Panel = CType(e.Item.FindControl("pnlFileQuali"), Panel)
                pnlFile.Visible = False
            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Protected Sub chkAccept_CheckedChanged(sender As Object, e As EventArgs)
    '    Try

    '        Dim chk As CheckBox = CType(sender, CheckBox)
    '        Dim dlItem As DataListItem = CType(chk.NamingContainer, DataListItem)
    '        If chk.Checked Then
    '            CType(dlItem.FindControl("btnApply"), LinkButton).Attributes.Remove("disabled")
    '        Else
    '            CType(dlItem.FindControl("btnApply"), LinkButton).Attributes.Add("disabled", "disabled")
    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub odsVacancy_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles odsVacancy.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is DataTable Then
                intC = CType(e.ReturnValue, DataTable).Rows.Count
            End If

            objlblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub flUpload_btnUpload_Click(sender As Object, e As EventArgs)
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim flUpload As FileUpload = CType(CType(sender, Button).Parent, FileUpload)

            If flUpload.HasFile = False Then Exit Try

            Dim ddlDocType As DropDownList = CType(CType(sender, Button).Parent.Parent.FindControl("ddlDocType" & CType(sender, Button).Parent.Parent.ID.Substring(3)), DropDownList)
            Dim ddlAttachType As DropDownList = CType(CType(sender, Button).Parent.Parent.FindControl("ddlAttachType" & CType(sender, Button).Parent.Parent.ID.Substring(3)), DropDownList)
            'Dim txtFilePath As TextBox = CType(CType(sender, Button).Parent.Parent.Parent.Parent.FindControl("txtFilePath" & CType(sender, Button).Parent.Parent.ID.Substring(3)), TextBox)
            Dim txtFilePath As Label = CType(CType(sender, Button).Parent.Parent.FindControl("lblFileName" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Label)
            Dim hfFileName As HiddenField = CType(CType(sender, Button).Parent.Parent.FindControl("hfFileName" & CType(sender, Button).Parent.Parent.ID.Substring(3)), HiddenField)
            Dim lblDocSize As Label = CType(CType(sender, Button).Parent.Parent.FindControl("lblDocSize" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Label)
            Dim hfDocSize As HiddenField = CType(CType(sender, Button).Parent.Parent.FindControl("hfDocSize" & CType(sender, Button).Parent.Parent.ID.Substring(3)), HiddenField)
            Dim lblFileExt As Label = CType(CType(sender, Button).Parent.Parent.FindControl("lblFileExt" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Label)
            Dim pnlFile As Panel = CType(CType(sender, Button).Parent.Parent.FindControl("pnlFile" & CType(sender, Button).Parent.Parent.ID.Substring(3)), Panel)

            txtFilePath.Text = ""
            hfFileName.Value = txtFilePath.Text
            pnlFile.Visible = False

            If CInt(ddlDocType.SelectedValue) <= 0 Then
                ShowMessage(lblDocTypeMsg.Text, MessageType.Info)
                ddlDocType.Focus()
                Exit Sub
            ElseIf CInt(ddlAttachType.SelectedValue) <= 0 Then
                ShowMessage(lblAttachTypeMsg.Text, MessageType.Info)
                ddlAttachType.Focus()
                Exit Sub
            End If

            Dim blnInvalidFile As Boolean = False
            Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG", "JPEG", "BMP", "GIF", "PNG"
                    If flUpload.AllowImageFile = False Then
                        blnInvalidFile = True
                    End If
                Case "DOC", "DOCX"
                    If flUpload.AllowDocumentFile = False Then
                        blnInvalidFile = True
                    End If
                Case "PDF"
                Case Else
                    blnInvalidFile = True
                    'ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                    'Exit Try
            End Select

            If blnInvalidFile = True Then
                ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                Exit Try
            End If

            'Dim mintByes As Integer = CInt(flUpload.MaxSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            Dim mintByes As Integer = CInt(flUpload.RemainingSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            If flUpload.FileBytes.LongLength > mintByes Then   ' 6 MB = 6291456 Bytes
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            txtFilePath.Text = flUpload.PostedFile.FileName
            lblFileExt.Text = flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
            lblDocSize.Text = Format(((flUpload.FileBytes.LongLength / 1024) / 1024), "0.00") & " MB."
            hfDocSize.Value = flUpload.FileBytes.LongLength
            hfFileName.Value = txtFilePath.Text
            pnlFile.Visible = True
            Session(flUpload.ClientID) = flUpload.PostedFile
            'Dim strUnkImgName As String
            'Dim strFilename As String = flUpload.FileName

            'If flUpload.PostedFile.FileName <> "" Then
            '    strUnkImgName = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & Split(flUpload.FileName, ".")(1)
            '    Dim Str As String = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
            '    If CInt(ddlDocType.SelectedValue) = CInt(enScanAttactRefId.CURRICULAM_VITAE) Then
            '        Str = System.Configuration.ConfigurationManager.AppSettings("CVSavePath")
            '    ElseIf CInt(ddlDocType.SelectedValue) = CInt(enScanAttactRefId.COVER_LETTER) Then
            '        Str = System.Configuration.ConfigurationManager.AppSettings("CoverLetterSavePath")
            '    ElseIf CInt(ddlDocType.SelectedValue) = CInt(enScanAttactRefId.QUALIFICATIONS) Then
            '        Str = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
            '    End If
            '    If io.Directory.Exists(Server.MapPath("~") & "\" & Str) = False Then
            '        io.Directory.CreateDirectory(Server.MapPath("~") & "\" & Str)
            '    End If
            '    Str &= "\" & strUnkImgName

            '    flUpload.SaveAs(Server.MapPath("~") & "\" & Str)

            '    If objAppQuali.AddApplicantAttachment(strCompCode:=Session("CompCode").ToString _
            '                                         , intComUnkID:=CInt(Session("companyunkid")) _
            '                                         , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            '                                         , intDocumentUnkid:=CInt(ddlDocTypeQuali.SelectedValue) _
            '                                         , intModulerefId:=CInt(enImg_Email_RefId.Applicant_Module) _
            '                                         , intAttachrefId:=CInt(ddlDocType.SelectedValue) _
            '                                         , strFilepath:=Server.MapPath("~") & "\" & Str _
            '                                         , strFilename:=flUpload.FileName _
            '                                         , strFileuniquename:=strUnkImgName _
            '                                         , intFile_size:=CInt(flUpload.FileBytes.LongLength / 1024) _
            '                                         , dtAttached_Date:=DateAndTime.Now
            '                                         ) = True Then

            '        ShowMessage(lblAttachSaveMsg.Text, MessageType.Info)

            '    End If


            'Call FillApplicantAttachments(True)

            'Else

            'End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Checkbox Events "
    Protected Sub chkGridOther_CheckedChanged(sender As Object, e As EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim itm As DataListItem = DirectCast(chk.NamingContainer, DataListItem)

            If CType(itm.FindControl("chkVacancyFoundOutFrom"), CheckBox).Checked = True Then
                CType(itm.FindControl("pnlVacancyFoundOutFrom"), Panel).Visible = False
                CType(itm.FindControl("pnlOtherVacancyFoundOutFrom"), Panel).Visible = True
            Else
                CType(itm.FindControl("pnlVacancyFoundOutFrom"), Panel).Visible = True
                CType(itm.FindControl("pnlOtherVacancyFoundOutFrom"), Panel).Visible = False
            End If

            CType(itm.FindControl("ddlVacancyFoundOutFrom"), DropDownList).SelectedValue = "0"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Button Event(S)"

    Private Sub btnSearchVacancies_Click(sender As Object, e As EventArgs) Handles btnSearchVacancies.Click
        'Dim dtTable As DataTable = Nothing
        Try
            'If txtVacanciesSearch.Text.Trim.Length > 0 Then
            '    Dim strSearch As String = "vacancytitle Like '%" & txtVacanciesSearch.Text.Trim & "%' OR remark LIKE '%" & txtVacanciesSearch.Text.Trim & "%'"
            '    Dim drow() As DataRow = dsVacancyList.Tables(0).Select(strSearch)
            '    If drow.Length > 0 Then
            '        dtTable = drow.CopyToDataTable
            '    Else
            '        dtTable = dsVacancyList.Tables(0).Clone
            '    End If
            'Else
            '    dtTable = dsVacancyList.Tables(0)
            'End If
            'dlVaanciesList.DataSource = New DataView(dtTable, "IsApplied = 0", "IsApplied ASC, vacancytitle ASC", DataViewRowState.CurrentRows).ToTable
            'dlVaanciesList.DataBind()
            odsVacancy.FilterExpression = "vacancytitle LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR remark LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR company_name LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR skill LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR Qualification LIKE '%" & txtVacanciesSearch.Text.Trim & "%' OR duties LIKE '%" & txtVacanciesSearch.Text.Trim & "%' "
            dlVaanciesList.DataBind()
            objlblCount.Text = "(" & dlVaanciesList.Items.Count & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.Show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End
#End Region

#Region " Combobox Events "

    'Hemant (17 Oct 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA) : Every qualification category should be bound with specific document type in attachment.
    Protected Sub ddlDocTypeCV_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlDocTypeCV As DropDownList = CType(sender, DropDownList)
            Dim itm As DataListItem = DirectCast(ddlDocTypeCV.NamingContainer, DataListItem)
            Dim ddlAttachTypeCV As DropDownList = CType(itm.FindControl("ddlAttachTypeCV"), DropDownList)

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlDocTypeCV.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlDocTypeCV.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlAttachTypeCV.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlAttachTypeCV.Enabled = False
                    Else
                        ddlAttachTypeCV.Enabled = True
                    End If

                Else
                    ddlAttachTypeCV.SelectedIndex = 0
                    ddlAttachTypeCV.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


    Protected Sub ddlDocTypeCoverLetter_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlDocTypeCoverLetter As DropDownList = CType(sender, DropDownList)
            Dim itm As DataListItem = DirectCast(ddlDocTypeCoverLetter.NamingContainer, DataListItem)
            Dim ddlAttachTypeCoverLetter As DropDownList = CType(itm.FindControl("ddlAttachTypeCoverLetter"), DropDownList)

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlDocTypeCoverLetter.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlDocTypeCoverLetter.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlAttachTypeCoverLetter.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlAttachTypeCoverLetter.Enabled = False
                    Else
                        ddlAttachTypeCoverLetter.Enabled = True
                    End If

                Else
                    ddlAttachTypeCoverLetter.SelectedIndex = 0
                    ddlAttachTypeCoverLetter.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub ddlDocTypeQuali_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlDocTypeQuali As DropDownList = CType(sender, DropDownList)
            Dim itm As DataListItem = DirectCast(ddlDocTypeQuali.NamingContainer, DataListItem)
            Dim ddlAttachTypeQuali As DropDownList = CType(itm.FindControl("ddlAttachTypeQuali"), DropDownList)

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlDocTypeQuali.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlDocTypeQuali.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlAttachTypeQuali.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlAttachTypeQuali.Enabled = False
                    Else
                        ddlAttachTypeQuali.Enabled = True
                    End If

                Else
                    ddlAttachTypeQuali.SelectedIndex = 0
                    ddlAttachTypeQuali.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Hemant (17 Oct 2023) -- End

#End Region

End Class