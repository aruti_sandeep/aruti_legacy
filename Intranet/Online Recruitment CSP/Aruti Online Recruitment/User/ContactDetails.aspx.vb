﻿Option Strict On
Imports System.Runtime.Caching
Imports System.Web.Services
Imports AjaxControlToolkit

Public Class ContactDetails
    Inherits Base_Page

    Private objContact As New clsContactDetails

#Region " Methods Functions "

    Private Sub FillCombo()
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetCountry()
            With ddlCountry
                .DataValueField = "countryunkid"
                .DataTextField = "Country_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlCountry_SelectedIndexChanged(ddlCountry, New System.EventArgs)
            End With

            With ddlPrmCountry
                .DataValueField = "countryunkid"
                .DataTextField = "Country_name"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
                Call ddlPrmCountry_SelectedIndexChanged(ddlPrmCountry, New System.EventArgs)
            End With


        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If txtpresentAdd.Text.Trim = "" AndAlso CBool(Session("Address1Mandatory")) = True Then
                ShowMessage(lblPresentAdd1Msg.Text, MessageType.Errorr)
                txtpresentAdd.Focus()
                Return False
            ElseIf txtpresentadd2.Text.Trim = "" AndAlso CBool(Session("Address2Mandatory")) = True Then
                ShowMessage(lblPresentAdd2Msg.Text, MessageType.Errorr)
                txtpresentadd2.Focus()
                Return False
            ElseIf txtPlotno.Text.Trim = "" AndAlso CBool(Session("ContactEstateMandatory")) = True Then
                ShowMessage(lblEstate.Text, MessageType.Errorr)
                txtPlotno.Focus()
                Return False
            ElseIf txtEstate.Text.Trim = "" AndAlso CBool(Session("ContactPlotNoMandatory")) = True Then
                ShowMessage(lblPlotnoMsg.Text, MessageType.Errorr)
                txtEstate.Focus()
                Return False
            ElseIf txtroad.Text.Trim = "" AndAlso CBool(Session("ContactStreetMandatory")) = True Then
                ShowMessage(lblRoadMsg.Text, MessageType.Errorr)
                txtroad.Focus()
                Return False
            ElseIf txtProvince.Text.Trim = "" AndAlso CBool(Session("RegionMandatory")) = True Then
                ShowMessage(lblPresentRegionMsg.Text, MessageType.Errorr)
                txtpresentadd2.Focus()
                Return False
            ElseIf CInt(ddlCountry.SelectedValue) <= 0 AndAlso CBool(Session("GenderMandatory")) = True Then
                ShowMessage(lblPresentCountryMsg.Text, MessageType.Errorr)
                ddlCountry.Focus()
                Return False
            ElseIf CInt(ddlState.SelectedValue) <= 0 AndAlso CBool(Session("StateMandatory")) = True Then
                ShowMessage(lblPresentStateMsg.Text, MessageType.Errorr)
                ddlState.Focus()
                Return False
            ElseIf CInt(ddlPostTown.SelectedValue) <= 0 AndAlso CBool(Session("CityMandatory")) = True Then
                ShowMessage(lblPresentCityMsg.Text, MessageType.Errorr)
                ddlPostTown.Focus()
                Return False
            ElseIf CInt(ddlPostcode.SelectedValue) <= 0 AndAlso CBool(Session("PostCodeMandatory")) = True Then
                ShowMessage(lblPresentPostcodeMsg.Text, MessageType.Errorr)
                ddlPostcode.Focus()
                Return False
            ElseIf txtFax.Text.Trim = "" AndAlso CBool(Session("ContactFaxMandatory")) = True Then
                ShowMessage(lblFaxMsg.Text, MessageType.Errorr)
                txtFax.Focus()
                Return False
            End If


            Return True

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
    End Function

    Private Sub GetValue()
        Dim dsList As DataSet
        Try
            dsList = objContact.GetContactDetail(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            If dsList.Tables(0).Rows.Count > 0 Then

                txtpresentAdd.Text = dsList.Tables(0).Rows(0).Item("present_address1").ToString
                txtpresentadd2.Text = dsList.Tables(0).Rows(0).Item("present_address2").ToString
                txtPlotno.Text = dsList.Tables(0).Rows(0).Item("present_plotno").ToString
                txtEstate.Text = dsList.Tables(0).Rows(0).Item("present_estate").ToString
                txtroad.Text = dsList.Tables(0).Rows(0).Item("present_road").ToString
                txtProvince.Text = dsList.Tables(0).Rows(0).Item("present_province").ToString
                ddlCountry.SelectedValue = dsList.Tables(0).Rows(0).Item("present_countryunkid").ToString
                Call ddlCountry_SelectedIndexChanged(ddlCountry, New System.EventArgs)
                ddlState.SelectedValue = dsList.Tables(0).Rows(0).Item("present_stateunkid").ToString
                Call ddlState_SelectedIndexChanged(ddlState, New System.EventArgs)
                ddlPostTown.SelectedValue = dsList.Tables(0).Rows(0).Item("present_post_townunkid").ToString
                Call ddlPostTown_SelectedIndexChanged(ddlPostTown, New System.EventArgs)
                ddlPostcode.SelectedValue = dsList.Tables(0).Rows(0).Item("present_zipcode").ToString
                txttelno.Text = dsList.Tables(0).Rows(0).Item("present_tel_no").ToString
                txtFax.Text = dsList.Tables(0).Rows(0).Item("present_fax").ToString

                txtprmAdd.Text = dsList.Tables(0).Rows(0).Item("perm_address1").ToString
                txtprmAdd2.Text = dsList.Tables(0).Rows(0).Item("perm_address2").ToString
                txtprmPlotno.Text = dsList.Tables(0).Rows(0).Item("perm_plotno").ToString
                txtPrmEstate.Text = dsList.Tables(0).Rows(0).Item("perm_estate").ToString
                txtPrmRoad.Text = dsList.Tables(0).Rows(0).Item("perm_road").ToString
                txtPrmProvince.Text = dsList.Tables(0).Rows(0).Item("perm_province").ToString
                ddlPrmCountry.SelectedValue = dsList.Tables(0).Rows(0).Item("perm_countryunkid").ToString
                Call ddlPrmCountry_SelectedIndexChanged(ddlPrmCountry, New System.EventArgs)
                ddlPrmState.SelectedValue = dsList.Tables(0).Rows(0).Item("perm_stateunkid").ToString
                Call ddlPrmState_SelectedIndexChanged(ddlPrmState, New System.EventArgs)
                ddlPrmPostTown.SelectedValue = dsList.Tables(0).Rows(0).Item("perm_post_townunkid").ToString
                Call ddlPrmPostTown_SelectedIndexChanged(ddlPrmPostTown, New System.EventArgs)
                ddlPrmPostcode.SelectedValue = dsList.Tables(0).Rows(0).Item("perm_zipcode").ToString
                txtPrmTelNo.Text = dsList.Tables(0).Rows(0).Item("perm_tel_no").ToString
                txtPrmfax.Text = dsList.Tables(0).Rows(0).Item("perm_fax").ToString

            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Web Methods "
    '<WebMethod()> Public Shared Function GetCountry(ByVal knownCategoryValues As String, ByVal category As String) As CascadingDropDownNameValue()
    '    Dim ds As New DataSet
    '    Dim CountryList As New List(Of CascadingDropDownNameValue)

    '    Try

    '        ds = clsContactDetails.GetCountry()

    '        For Each dsRow As DataRow In ds.Tables(0).Rows
    '            CountryList.Add(New CascadingDropDownNameValue(dsRow.Item("Country_name").ToString, dsRow.Item("countryunkid").ToString))
    '            Dim c As New CascadingDropDownNameValue
    '        Next

    '    Catch ex As Exception
    '        Throw New Exception("Error In GetCountry " & ex.Message)
    '    End Try
    '    Return CountryList.ToArray
    'End Function

    '<WebMethod()> Public Shared Function GetState(ByVal knownCategoryValues As String, ByVal category As String) As CascadingDropDownNameValue()
    '    'Dim msql As String
    '    Dim ds As New DataSet
    '    Dim StateList As New List(Of CascadingDropDownNameValue)
    '    Dim objCD As New clsContactDetails
    '    Try
    '        Dim country As StringDictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)
    '        Dim countryid As Integer ' = Convert.ToInt32(country("country"))
    '        Integer.TryParse(country("country"), countryid)

    '        ds = objCD.GetState(HttpContext.Current.Session("CompCode").ToString, CInt(HttpContext.Current.Session("companyunkid")), countryid)

    '        For Each dsRow As DataRow In ds.Tables(0).Rows
    '            StateList.Add(New CascadingDropDownNameValue(dsRow.Item("state_name").ToString, dsRow.Item("stateunkid").ToString))
    '        Next

    '    Catch ex As Exception
    '        Throw New Exception("Error In GetState " & ex.Message)
    '    End Try
    '    Return StateList.ToArray
    'End Function

    '<WebMethod()> Public Shared Function GetCity(ByVal knownCategoryValues As String, ByVal category As String) As CascadingDropDownNameValue()
    '    'Dim msql As String
    '    Dim ds As New DataSet
    '    Dim CityList As New List(Of CascadingDropDownNameValue)
    '    Dim objCD As New clsContactDetails
    '    Try
    '        Dim state As StringDictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)
    '        Dim stateid As Integer '= Convert.ToInt32(state("state"))
    '        Integer.TryParse(state("state"), stateid)

    '        ds = objCD.GetCity(HttpContext.Current.Session("CompCode").ToString, CInt(HttpContext.Current.Session("companyunkid")), stateid)

    '        For Each dsRow As DataRow In ds.Tables(0).Rows
    '            CityList.Add(New CascadingDropDownNameValue(dsRow.Item("city_name").ToString, dsRow.Item("cityunkid").ToString))
    '        Next

    '    Catch ex As Exception
    '        Throw New Exception("Error In GetCity " & ex.Message)
    '    End Try
    '    Return CityList.ToArray
    'End Function

    '<WebMethod()> Public Shared Function GetZipCode(ByVal knownCategoryValues As String, ByVal category As String) As CascadingDropDownNameValue()
    '    'Dim msql As String
    '    Dim ds As New DataSet
    '    Dim ZipList As New List(Of CascadingDropDownNameValue)
    '    Dim objCD As New clsContactDetails
    '    Try
    '        Dim city As StringDictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)
    '        Dim cityid As Integer '= Convert.ToInt32(city("city"))
    '        Integer.TryParse(city("city"), cityid)

    '        ds = objCD.GetZipCode(HttpContext.Current.Session("CompCode").ToString, CInt(HttpContext.Current.Session("companyunkid")), cityid)

    '        For Each dsRow As DataRow In ds.Tables(0).Rows
    '            ZipList.Add(New CascadingDropDownNameValue(dsRow.Item("zipcode_no").ToString, dsRow.Item("zipcodeunkid").ToString))
    '        Next

    '    Catch ex As Exception
    '        Throw New Exception("Error In GetZipCode " & ex.Message)
    '    End Try
    '    Return ZipList.ToArray
    'End Function
#End Region

#Region " Pages Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If CBool(Session("Address1Mandatory")) = True Then
                    'rfvPresentAdd1.ValidationGroup = btnSave.ValidationGroup
                    'lblPresentAdd.Text = lblPresentAdd.Text & "*"
                    lblPresentAdd.CssClass = "required"
                Else
                    'rfvPresentAdd1.ValidationGroup = ""
                    lblPresentAdd.CssClass = ""
                    'pnlAdd1.Visible = False 'Sohail (31 Mar 2022)
                    'pnlPermAdd1.Visible = False 'Sohail (31 Mar 2022)
                End If

                If CBool(Session("Address2Mandatory")) = True Then
                    'rfvPresentAdd2.ValidationGroup = btnSave.ValidationGroup
                    'lblPresentAdd1.Text = lblPresentAdd1.Text & "*"
                    lblPresentAdd1.CssClass = "required"
                Else
                    'rfvPresentAdd2.ValidationGroup = ""
                    lblPresentAdd1.CssClass = ""
                    pnlAdd2.Visible = False 'Sohail (31 Mar 2022)
                    pnlPermAdd2.Visible = False 'Sohail (31 Mar 2022)
                End If

                If CBool(Session("RegionMandatory")) = True Then
                    'rfvPresentRegion.ValidationGroup = btnSave.ValidationGroup
                    'lblProvince.Text = lblProvince.Text & "*"
                    lblProvince.CssClass = "required"
                Else
                    'rfvPresentRegion.ValidationGroup = ""
                    lblProvince.CssClass = ""
                    pnlProvince.Visible = False 'Sohail (31 Mar 2022)
                    pnlPermProvince.Visible = False 'Sohail (31 Mar 2022)
                End If

                If CBool(Session("CountryMandatory")) = True Then
                    'rfvCountry.ValidationGroup = btnSave.ValidationGroup
                    'lblCountry.Text = lblCountry.Text & "*"
                    lblCountry.CssClass = "required"
                Else
                    'rfvCountry.ValidationGroup = ""
                    lblCountry.CssClass = ""
                End If

                If CBool(Session("StateMandatory")) = True Then
                    'rfvState.ValidationGroup = btnSave.ValidationGroup
                    'lblState.Text = lblState.Text & "*"
                    lblState.CssClass = "required"
                Else
                    'rfvState.ValidationGroup = ""
                    lblState.CssClass = ""
                End If

                If CBool(Session("CityMandatory")) = True Then
                    'rfvCity.ValidationGroup = btnSave.ValidationGroup
                    'lblPostTown.Text = lblPostTown.Text & "*"
                    lblPostTown.CssClass = "required"
                Else
                    'rfvCity.ValidationGroup = ""
                    lblPostTown.CssClass = ""
                    pnlPostTown.Visible = False
                    pnlPermPostTown.Visible = False
                End If

                If CBool(Session("PostCodeMandatory")) = True Then
                    'rfvPostCode.ValidationGroup = btnSave.ValidationGroup
                    'lblPostCode.Text = lblPostCode.Text & "*"
                    lblPostCode.CssClass = "required"
                Else
                    'rfvPostCode.ValidationGroup = ""
                    lblPostCode.CssClass = ""
                    pnlPostCode.Visible = False
                    pnlPermPostCode.Visible = False
                End If

                'Sohail (31 Mar 2022) - Start
                If CBool(Session("ContactPlotNoMandatory")) = True Then
                    lblPlotno.CssClass = "required"
                Else
                    lblPlotno.CssClass = ""
                    pnlPlotno.Visible = False
                    pnlPermPlotno.Visible = False
                End If

                If CBool(Session("ContactEstateMandatory")) = True Then
                    lblEstate.CssClass = "required"
                Else
                    lblEstate.CssClass = ""
                    pnlEstate.Visible = False
                    pnlPermEstate.Visible = False
                End If

                If CBool(Session("ContactStreetMandatory")) = True Then
                    lblRoad.CssClass = "required"
                Else
                    lblRoad.CssClass = ""
                    pnlStreet.Visible = False
                    pnlPermStreet.Visible = False
                End If

                If CBool(Session("ContactFaxMandatory")) = True Then
                    lblFax.CssClass = "required"
                Else
                    lblFax.CssClass = ""
                    pnlFax.Visible = False
                    pnlPermFax.Visible = False
                End If

                If CBool(Session("HideRecruitementPermanentAddress")) = True Then
                    pnlPermAddress.Visible = False
                End If
                'Sohail (31 Mar 2022) - End

                Call GetValue()
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnSave.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ContactDetails_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " LinkButton's Events "
    Protected Sub lnkCopyAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyAdd.Click
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            'lblError.Visible = False

            txtprmAdd.Text = txtpresentAdd.Text
            txtprmAdd2.Text = txtpresentadd2.Text
            txtprmPlotno.Text = txtPlotno.Text
            txtPrmEstate.Text = txtEstate.Text
            txtPrmRoad.Text = txtroad.Text
            ddlPrmCountry.SelectedValue = ddlCountry.SelectedValue
            Call ddlPrmCountry_SelectedIndexChanged(ddlPrmCountry, New System.EventArgs)
            ddlPrmState.SelectedValue = ddlState.SelectedValue
            Call ddlPrmState_SelectedIndexChanged(ddlPrmState, New System.EventArgs)
            ddlPrmPostTown.SelectedValue = ddlPostTown.SelectedValue
            Call ddlPrmPostTown_SelectedIndexChanged(ddlPrmPostTown, New System.EventArgs)
            ddlPrmPostcode.SelectedValue = ddlPostcode.SelectedValue
            'cddPrmCountry.SelectedValue = ddlCountry.SelectedItem.Value
            'cddPrmState.SelectedValue = ddlState.SelectedItem.Value
            'cddPrmPostTown.SelectedValue = ddlPostTown.SelectedItem.Value
            'cddPrmPostCode.SelectedValue = ddlPostcode.SelectedItem.Value
            txtPrmProvince.Text = txtProvince.Text
            'txtPrmMobileno.Text = txtMobileNo.Text
            'txtprmAterno.Text = txtAlterNativeno.Text
            txtPrmTelNo.Text = txttelno.Text
            txtPrmfax.Text = txtFax.Text
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Private Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlCountry.SelectedValue))
            With ddlState
                .DataValueField = "stateunkid"
                .DataTextField = "state_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlState_SelectedIndexChanged(ddlState, New System.EventArgs)
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ddlPrmCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrmCountry.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlPrmCountry.SelectedValue))
            With ddlPrmState
                .DataValueField = "stateunkid"
                .DataTextField = "state_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlPrmState_SelectedIndexChanged(ddlPrmState, New System.EventArgs)
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlState.SelectedValue))
            With ddlPostTown
                .DataValueField = "cityunkid"
                .DataTextField = "city_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlPostTown_SelectedIndexChanged(ddlPostTown, New System.EventArgs)
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ddlPrmState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrmState.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlPrmState.SelectedValue))
            With ddlPrmPostTown
                .DataValueField = "cityunkid"
                .DataTextField = "city_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlPrmPostTown_SelectedIndexChanged(ddlPrmPostTown, New System.EventArgs)
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ddlPostTown_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPostTown.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetZipCode(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlPostTown.SelectedValue))
            With ddlPostcode
                .DataValueField = "zipcodeunkid"
                .DataTextField = "zipcode_no"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ddlPrmPostTown_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrmPostTown.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objContact.GetZipCode(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlPrmPostTown.SelectedValue))
            With ddlPrmPostcode
                .DataValueField = "zipcodeunkid"
                .DataTextField = "zipcode_no"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Buttons Events "

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'lblError.Visible = False
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            If objContact.SaveContactDetail(strCompCode:=Session("CompCode").ToString _
                                            , intComUnkID:=CInt(Session("companyunkid")) _
                                            , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                            , strPresent_Address1:=txtpresentAdd.Text _
                                            , strPresent_Address2:=txtpresentadd2.Text _
                                            , strPresent_PlotNo:=txtPlotno.Text _
                                            , strPresent_Estate:=txtEstate.Text _
                                            , strPresent_Road:=txtroad.Text _
                                            , strPresent_Province:=txtProvince.Text _
                                            , intPresent_Country:=CInt(ddlCountry.SelectedValue) _
                                            , intPresent_State:=CInt(ddlState.SelectedValue) _
                                            , intPresent_PostTown:=CInt(ddlPostTown.SelectedValue) _
                                            , intPresent_Zipcode:=CInt(ddlPostcode.SelectedValue) _
                                            , strPresent_Tel_No:=txttelno.Text _
                                            , strPresent_Fax:=txtFax.Text _
                                            , strPerm_Address1:=txtprmAdd.Text _
                                            , strPerm_Address2:=txtprmAdd2.Text _
                                            , strPerm_PlotNo:=txtprmPlotno.Text _
                                            , strPerm_Estate:=txtPrmEstate.Text _
                                            , strPerm_Road:=txtPrmRoad.Text _
                                            , strPerm_Province:=txtPrmProvince.Text _
                                            , intPerm_Country:=CInt(ddlPrmCountry.SelectedValue) _
                                            , intPerm_State:=CInt(ddlPrmState.SelectedValue) _
                                            , intPerm_PostTown:=CInt(ddlPrmPostTown.SelectedValue) _
                                            , intPerm_Zipcode:=CInt(ddlPrmPostcode.SelectedValue) _
                                            , strPerm_Tel_No:=txtPrmTelNo.Text _
                                            , strPerm_Fax:=txtPrmfax.Text
                                            ) = True Then

                ShowMessage(lblSaveMsg.Text, MessageType.Info)

                Dim strCacheKey As String = "AppContact_" & CStr(Session("CompCode")) & "_" & CInt(Session("companyunkid")).ToString & "_" & CInt(Session("applicantunkid")).ToString
                Dim cache As ObjectCache = MemoryCache.Default
                If cache(strCacheKey) IsNot Nothing Then
                    cache.Remove(strCacheKey)
                End If

                Dim s4 As Site4 = CType(Me.Master, Site4)
                s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End
#End Region



End Class