﻿Option Strict On
Imports System.Runtime.Caching

Public Class ApplicantQualification
    Inherits Base_Page

    Private objAppQuali As New clsApplicantQualification
    'Private mdtQualification As DataTable

#Region " Method Functions "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Try
            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.

            'Pinkal (30-Sep-2023) -- Start
            ''(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            Dim dr As DataRow = Nothing
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            dr = Nothing
            dr = dsCombo.Tables(0).NewRow()
            dr.Item("masterunkid") = -2
            dr.Item("name") = lblOtherNotInListmsg.Text
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)
            End If
            'Pinkal (30-Sep-2023) -- End


            'Sohail (13 May 2022) -- End
            With ddlAwardgp
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlAwardgp_SelectedIndexChanged(ddlAwardgp, New System.EventArgs)
            End With

            dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.


            'Pinkal (30-Sep-2023) -- Start
            ''(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            dr = Nothing
            dr = dsCombo.Tables(0).NewRow()
            dr.Item("instituteunkid") = -2
            dr.Item("institute_name") = lblOtherNotInListmsg.Text
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)
            End If
            'Pinkal (30-Sep-2023) -- End


            'Sohail (13 May 2022) -- End
            With drpInstitute
                .DataValueField = "instituteunkid"
                .DataTextField = "institute_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
            With ddlDocTypeQuali
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
                'Hemant (17 Oct 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : Every qualification category should be bound with specific document type in attachment.
                Dim drQuali() As DataRow = dsCombo.Tables(0).Select("name = 'qualifications'")
                'Hemant (17 Oct 2023) -- End
                With ddlQualificationAttachType
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsCombo.Tables(0).Copy()
                    .DataBind()
                    'Hemant (17 Oct 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA) : Every qualification category should be bound with specific document type in attachment.
                    If drQuali.Length > 0 Then
                        .SelectedValue = CStr(drQuali(0).Item("masterunkid"))
                        .Enabled = False
                    Else
                        'Hemant (17 Oct 2023) -- End
                    .SelectedValue = "0"
                        .Enabled = True
                    End If 'Hemant (17 Oct 2023) 
                End With


                pnlQualificationAttachment.Visible = True
                flQualificationAttachment.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flQualificationAttachment.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
            Else
                pnlQualificationAttachment.Visible = True
            End If
            pnlFileQualificationAttachment.Visible = False
            'Pinkal (30-Sep-2023) -- End

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub FillApplicantQualifications(Optional blnRefreshCache As Boolean = False)
        'Dim dsList As DataSet
        'Sohail (13 May 2022) -- Start
        'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
        'Dim intSortOrder As Integer = -1
        Dim intSortOrder As Integer = CInt(Session("appqualisortbyid"))
        'Sohail (13 May 2022) -- End
        Try

            If IsPostBack = False Then
                intSortOrder = CInt(Session("appqualisortbyid"))
            End If
            ''dsList = objAppQuali.GetApplicantQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, intSortOrder)
            'dsList = clsApplicantQualification.GetApplicantQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, intSortOrder)
            'mdtQualification = dsList.Tables(0)

            ''grdQualification.DataSource = dsList.Tables(0)
            'grdQualification.DataSource = mdtQualification.DefaultView
            'grdQualification.DataBind()
            ''ViewState("dtTable") = dsList.Tables(0)

            objodsQuali.SelectParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            objodsQuali.SelectParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            objodsQuali.SelectParameters.Item("intApplicantUnkId").DefaultValue = CInt(Session("applicantunkid")).ToString
            objodsQuali.SelectParameters.Item("intQualificationGroup_ID").DefaultValue = CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP).ToString
            objodsQuali.SelectParameters.Item("intSortOrder").DefaultValue = intSortOrder.ToString
            If Session("QualiAddDate") Is Nothing OrElse blnRefreshCache = True Then
                Session("QualiAddDate") = DateAndTime.Now.ToString
            End If
            objodsQuali.SelectParameters.Item("dtCurrentDate").DefaultValue = CStr(Session("QualiAddDate"))
            lvQualification.DataBind()

            Dim s4 As Site4 = CType(Me.Master, Site4)
            s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub FillApplicantAttachments(Optional blnRefreshCache As Boolean = False)
        Dim dsList As DataSet
        Try
            dsList = objAppQuali.GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS), True)

            gvQualiCerti.DataSource = dsList.Tables(0)
            gvQualiCerti.DataBind()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            Dim decGPA As Decimal = 0
            Decimal.TryParse(txtGPAcode.Text, decGPA)

            If chkqualificationgroup.Checked = False AndAlso CInt(ddlAwardgp.SelectedItem.Value) <= 0 Then
                ShowMessage(lblAwardgpMsg.Text, MessageType.Info)
                ddlAwardgp.Focus()
                Return False
            ElseIf chkqualificationAward.Checked = False AndAlso CInt(ddlAward.SelectedItem.Value) <= 0 Then
                ShowMessage(lblAwardMsg.Text, MessageType.Info)
                ddlAward.Focus()
                Return False
            ElseIf chkResultCode.Checked = False AndAlso CInt(ddlResultCode.SelectedItem.Value) <= 0 AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
                ShowMessage(lblResultCodeMsg.Text, MessageType.Info)
                ddlResultCode.Focus()
                Return False
            ElseIf chkqualificationgroup.Checked = True AndAlso txtOtherQualiGroup.Text.Trim = "" Then
                ShowMessage(lblOtherQualiGroupMsg.Text, MessageType.Info)
                txtOtherQualiGroup.Focus()
                Return False
            ElseIf chkqualificationAward.Checked = True AndAlso txtOtherQualification.Text.Trim = "" Then
                ShowMessage(lblOtherQualiGroupMsg.Text, MessageType.Info)
                txtOtherQualification.Focus()
                Return False
            ElseIf chkResultCode.Checked = True AndAlso txtOtherResultCode.Text.Trim = "" AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
                ShowMessage(lblOtherResultCodeMsg.Text, MessageType.Info)
                txtOtherResultCode.Focus()
                Return False
            ElseIf dtAwardDate.GetDate = CDate("01/Jan/1900") AndAlso CBool(Session("QualificationStartDateMandatory")) = True Then
                ShowMessage(lblAwardDateMsg.Text, MessageType.Info)
                dtAwardDate.Focus()
                Return False
            ElseIf dtdateto.GetDate = CDate("01/Jan/1900") AndAlso CBool(Session("QualificationAwardDateMandatory")) = True Then
                ShowMessage(lblDatetoMsg.Text, MessageType.Info)
                dtdateto.Focus()
                Return False
            ElseIf chkNaGPA.Checked = False AndAlso decGPA <= 0 AndAlso CBool(Session("QualificationGPAMandatory")) = True Then  '
                ShowMessage(lblGPAMsg.Text, MessageType.Info)
                txtGPAcode.Focus()
                Return False
            ElseIf decGPA > 999 Then  '
                ShowMessage(lblGPAMsg2.Text, MessageType.Info)
                txtGPAcode.Focus()
                Return False
            ElseIf dtdateto.GetDate <> CDate("01/Jan/1900") AndAlso dtAwardDate.GetDate <> CDate("01/Jan/1900") Then
                If dtdateto.GetDate <= dtAwardDate.GetDate Then
                    ShowMessage(lblDatetoMsg2.Text, MessageType.Info)
                    dtdateto.Focus()
                    Return False
                End If
            ElseIf txtCertiNo.Text.Trim = "" AndAlso CBool(Session("CertificateNoMandatory")) = True Then
                ShowMessage(lblCertiNo.Text, MessageType.Info)
                txtCertiNo.Focus()
                Return False
            End If

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
                If CInt(ddlQualificationAttachType.SelectedValue) <= 0 Then
                    ShowMessage(LblQualificationAttachTypeMsg.Text, MessageType.Info)
                    ddlQualificationAttachType.Focus()
                    Return False
                End If
                If lblFileNameQualificationAttachment.Text.Trim = "" Then
                    ShowMessage(lblAttachMsg.Text, MessageType.Errorr)
                    Return False
                End If
            End If
            'Pinkal (30-Sep-2023) -- End 


            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub GetValue()
        Try
            Call FillApplicantQualifications()
            Call FillApplicantAttachments()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub Reset_Qualification()
        Try
            ddlAwardgp.SelectedValue = "0"
            ddlAward.SelectedValue = "0"
            dtAwardDate.SetDate = Nothing
            dtdateto.SetDate = Nothing
            dtDate.SetDate = System.DateTime.Today.Date
            txtRefno.Text = ""
            txtQualiremark.Text = ""
            drpInstitute.SelectedValue = "0"

            ddlResultCode.SelectedValue = "0"
            txtGPAcode.Text = "0"
            txtCertiNo.Text = ""

            btnAddQuali.Enabled = True
            chkNaGPA.Checked = False

            txtOtherQualiGroup.Text = ""
            txtOtherQualification.Text = ""
            txtOtherResultCode.Text = ""
            txtOtherInstitution.Text = ""
            'chkOtherQuali.Enabled = True

            'Pinkal (30-Sep-2023) -- Start
            ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
                ddlQualificationAttachType.SelectedValue = "0"
                lblFileNameQualificationAttachment.Text = ""
                lblFileExtQualificationAttachment.Text = ""
                lblDocSizeQualificationAttachment.Text = ""
                hfDocSizeQualificationAttachment.Value = ""
                hfFileNameQualificationAttachment.Value = ""
                pnlFileQualificationAttachment.Visible = False
                Session(flQualificationAttachment.ClientID) = Nothing
            End If
            'Pinkal (30-Sep-2023) -- End

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If CBool(Session("QualificationStartDateMandatory")) = True Then
                    lblAwardDate.CssClass = "required"
                Else
                    lblAwardDate.CssClass = ""
                End If

                If CBool(Session("QualificationAwardDateMandatory")) = True Then
                    lblDateto.CssClass = "required"

                Else
                    lblDateto.CssClass = ""
                End If

                If CBool(Session("QualificationResultCodeMandatory")) = True Then
                    'rfvResultCode.ValidationGroup = "Qualification"
                    'rfvOtherResultCode.ValidationGroup = "Qualification"
                    lblResult.CssClass = "required"
                Else
                    'rfvResultCode.ValidationGroup = "Nothing"
                    'rfvOtherResultCode.ValidationGroup = "Nothing"
                    lblResult.CssClass = ""
                End If
                If CBool(Session("QualificationGPAMandatory")) = True Then
                    lblGPA.CssClass = "required"

                Else
                    lblGPA.CssClass = ""
                    pnlGPA.Visible = False 'Sohail (31 Mar 2022)
                    pnlGPANotApplicable.Visible = False 'Sohail (31 Mar 2022)
                End If

                'Sohail (31 Mar 2022) - Start
                If CBool(Session("CertificateNoMandatory")) = True Then
                    lblCertiNo.CssClass = "required"
                Else
                    lblCertiNo.CssClass = ""
                    pnlCertNo.Visible = False
                End If

                If CBool(Session("HideRecruitementQualificationRemark")) = True Then
                    pnlRemark.Visible = False
                End If
                'Sohail (31 Mar 2022) - End

                'Call chkOtherQuali_CheckedChanged(chkOtherQuali, New System.EventArgs)
                Call GetValue()

                If CBool(Session("isQualiCertAttachMandatory")) = True Then
                    'Sohail (11 Nov 2021) -- Start
                    'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
                    'pnlAttachQuali.Visible = True
                    'lblUpload.CssClass = "required" 'Sohail (10 Oct 2018)
                    pnlAttachQuali.Visible = False
                    lblUpload.CssClass = ""

                    'Sohail (11 Nov 2021) -- End
                Else
                    pnlAttachQuali.Visible = False
                    lblUpload.CssClass = "" 'Sohail (10 Oct 2018)
                End If

                'Sohail (10 Oct 2018) -- Start
                If CBool(Session("HighestQualificationMandatory")) = True Then
                    lvlHigh.CssClass = "required"
                Else
                    lvlHigh.CssClass = ""
                    pnlHighestQuali.Visible = False
                End If
                'Sohail (10 Oct 2018) -- End

                ViewState("sort") = "Asc"
            Else
                'mdtQualification = CType(ViewState("mdtQualification"), DataTable)

                'Pinkal (30-Sep-2023) -- Start
                ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                If (hfFileNameQualificationAttachment.Value Is Nothing OrElse hfFileNameQualificationAttachment.Value.Length <= 0) AndAlso Session(flQualificationAttachment.ClientID) IsNot Nothing Then
                    lblFileNameQualificationAttachment.Text = ""
                    lblFileExtQualificationAttachment.Text = ""
                    lblDocSizeQualificationAttachment.Text = ""
                    hfDocSizeQualificationAttachment.Value = ""
                    hfFileNameQualificationAttachment.Value = ""
                    pnlFileQualificationAttachment.Visible = False
                    Session(flQualificationAttachment.ClientID) = Nothing
                End If
                'Pinkal (30-Sep-2023) -- End

            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnAddQuali.Enabled = False
                flUpload.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ApplicantQualification_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            'ViewState("mdtQualification") = mdtQualification
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Combobox Events "
    Private Sub ddlAwardgp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAwardgp.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
            chkqualificationgroup.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkOther_CheckedChanged(chkqualificationgroup, New EventArgs)
            'Sohail (13 May 2022) -- End
            dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlAwardgp.SelectedValue))
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            Dim dr As DataRow = Nothing
            dr = Nothing
            dr = dsCombo.Tables(0).NewRow()
            dr.Item("qualificationunkid") = -2
            dr.Item("Q_name") = lblOtherNotInListmsg.Text
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)
            End If
            'Pinkal (30-Sep-2023) -- End

            'Sohail (13 May 2022) -- End
            With ddlAward
                .DataValueField = "qualificationunkid"
                .DataTextField = "Q_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlAward_SelectedIndexChanged(ddlAward, New System.EventArgs)
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ddlAward_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAward.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
            chkqualificationAward.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkOther_CheckedChanged(chkqualificationAward, New EventArgs)
            'Sohail (13 May 2022) -- End
            dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlAward.SelectedValue))
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.

            'Pinkal (30-Sep-2023) -- Start
            ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            Dim dr As DataRow = Nothing
            dr = Nothing
            dr = dsCombo.Tables(0).NewRow()
            dr.Item("resultunkid") = -2
            dr.Item("resultname") = lblOtherNotInListmsg.Text
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)
            End If
            'Pinkal (30-Sep-2023) -- End


            'Sohail (13 May 2022) -- End
            With ddlResultCode
                .DataValueField = "resultunkid"
                .DataTextField = "resultname"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (13 May 2022) -- Start
    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
    Private Sub ddlResultCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlResultCode.SelectedIndexChanged
        Try
            chkResultCode.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkOther_CheckedChanged(chkResultCode, New EventArgs)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub drpInstitute_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpInstitute.SelectedIndexChanged
        Try
            chkInstitute.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkOther_CheckedChanged(chkInstitute, New EventArgs)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Sohail (13 May 2022) -- End
    'Protected Sub objddlAwardgp_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim dsCombo As DataSet
    '    Try
    '        dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
    '        Dim dp As DropDownList = DirectCast(grdQualification.Rows(grdQualification.EditIndex).FindControl("objddlQuali"), DropDownList)
    '        With dp
    '            .DataValueField = "qualificationunkid"
    '            .DataTextField = "Q_name"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '            .SelectedValue = "0"
    '            Call objddlAward_SelectedIndexChanged(dp, New System.EventArgs)
    '        End With
    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    'Protected Sub objddlAward_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim dsCombo As DataSet
    '    Try
    '        dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
    '        Dim dp As DropDownList = DirectCast(grdQualification.Rows(grdQualification.EditIndex).FindControl("objddlQualiResult"), DropDownList)
    '        With dp
    '            .DataValueField = "resultunkid"
    '            .DataTextField = "resultname"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '            .SelectedValue = "0"
    '        End With

    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    Protected Sub objddllvAwardgp_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dsCombo As DataSet
        Try
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
            Dim chklvQualificationGrp As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualiGrp"), CheckBox)
            chklvQualificationGrp.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkListViewOther_CheckedChanged(chklvQualificationGrp, New EventArgs)
            'Sohail (13 May 2022) -- End
            dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.

            'Pinkal (30-Sep-2023) -- Start
            ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            Dim dr As DataRow = Nothing
            dr = Nothing
            dr = dsCombo.Tables(0).NewRow()
            dr.Item("qualificationunkid") = -2
            dr.Item("Q_name") = lblOtherNotInListmsg.Text
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)
            End If
            'Pinkal (30-Sep-2023) -- End

            'Sohail (13 May 2022) -- End
            Dim dp As DropDownList = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQuali"), DropDownList)
            With dp
                .DataValueField = "qualificationunkid"
                .DataTextField = "Q_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call objddllvAward_SelectedIndexChanged(dp, New System.EventArgs)
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub objddllvAward_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dsCombo As DataSet
        Try
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
            Dim chklvQualification As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualification"), CheckBox)
            chklvQualification.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkListViewOther_CheckedChanged(chklvQualification, New EventArgs)
            'Sohail (13 May 2022) -- End
            dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.


            'Pinkal (30-Sep-2023) -- Start
            ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            Dim dr As DataRow = Nothing
            dr = Nothing
            dr = dsCombo.Tables(0).NewRow()
            dr.Item("resultunkid") = -2
            dr.Item("resultname") = lblOtherNotInListmsg.Text
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)
            End If
            'Pinkal (30-Sep-2023) -- End


            'Sohail (13 May 2022) -- End
            Dim dp As DropDownList = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQualiResult"), DropDownList)
            With dp
                .DataValueField = "resultunkid"
                .DataTextField = "resultname"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (13 May 2022) -- Start
    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
    Protected Sub objddllvQualiResult_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim chklvQualiResult As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualificationResult"), CheckBox)
            chklvQualiResult.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkListViewOther_CheckedChanged(chklvQualiResult, New EventArgs)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub objddllvQualiInst_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim chklvQualiInst As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualificationInst"), CheckBox)
            chklvQualiInst.Checked = CInt(DirectCast(sender, DropDownList).SelectedValue) = -2
            Call chkListViewOther_CheckedChanged(chklvQualiInst, New EventArgs)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Sohail (13 May 2022) -- End
#End Region

#Region " Datagridview Events "

    ' Private Sub grdQualification_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdQualification.RowDataBound
    '     Dim objApplicant As New clsApplicant
    '     Dim dsCombo As DataSet
    '     Try
    '         Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)

    '         If e.Row.RowType = DataControlRowType.DataRow Then

    '             If e.Row.RowState = DataControlRowState.Edit OrElse e.Row.RowState = 5 Then '5 = Alternate Or Edit


    '                 'If CInt(drv.Item("qualificationgroupunkid")) <= 0 AndAlso CInt(drv.Item("qualificationunkid")) <= 0 Then
    '                 '    DirectCast(e.Row.FindControl("objtxtQualiGrp"), TextBox).Visible = True
    '                 '    DirectCast(e.Row.FindControl("objddlQualiGrp"), DropDownList).Visible = False

    '                 '    DirectCast(e.Row.FindControl("objtxtQuali"), TextBox).Visible = True
    '                 '    DirectCast(e.Row.FindControl("objddlQuali"), DropDownList).Visible = False

    '                 '    DirectCast(e.Row.FindControl("objtxtQualiResult"), TextBox).Visible = True
    '                 '    DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList).Visible = False

    '                 '    DirectCast(e.Row.FindControl("objtxtQualiInst"), TextBox).Visible = True
    '                 '    DirectCast(e.Row.FindControl("objddlQualiInst"), DropDownList).Visible = False
    '                 'Else
    '                 '    DirectCast(e.Row.FindControl("objtxtQualiGrp"), TextBox).Visible = False
    '                 '    DirectCast(e.Row.FindControl("objddlQualiGrp"), DropDownList).Visible = True

    '                 '    DirectCast(e.Row.FindControl("objtxtQuali"), TextBox).Visible = False
    '                 '    DirectCast(e.Row.FindControl("objddlQuali"), DropDownList).Visible = True

    '                 '    DirectCast(e.Row.FindControl("objtxtQualiResult"), TextBox).Visible = False
    '                 '    DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList).Visible = True

    '                 '    DirectCast(e.Row.FindControl("objtxtQualiInst"), TextBox).Visible = False
    '                 '    DirectCast(e.Row.FindControl("objddlQualiInst"), DropDownList).Visible = True

    '                 '    Dim dp As DropDownList = DirectCast(e.Row.FindControl("objddlQualiGrp"), DropDownList)
    '                 '    dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
    '                 '    With dp
    '                 '        .DataValueField = "masterunkid"
    '                 '        .DataTextField = "Name"
    '                 '        .DataSource = dsCombo.Tables(0)
    '                 '        .DataBind()
    '                 '        .SelectedValue = drv.Item("qualificationgroupunkid").ToString
    '                 '    End With
    '                 '    Dim drp As DropDownList = DirectCast(e.Row.FindControl("objddlQuali"), DropDownList)
    '                 '    dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(dp.SelectedValue))
    '                 '    With drp
    '                 '        .DataValueField = "Qualificationunkid"
    '                 '        .DataTextField = "Q_name"
    '                 '        .DataSource = dsCombo.Tables(0)
    '                 '        .DataBind()
    '                 '    End With
    '                 '    DirectCast(e.Row.FindControl("objddlQuali"), DropDownList).SelectedValue = drv.Item("qualificationunkid").ToString

    '                 '    Dim drpRslt As DropDownList = DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList)
    '                 '    dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drp.SelectedValue))
    '                 '    With drpRslt
    '                 '        .DataValueField = "resultunkid"
    '                 '        .DataTextField = "resultname"
    '                 '        .DataSource = dsCombo.Tables(0)
    '                 '        .DataBind()
    '                 '    End With
    '                 '    DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList).SelectedValue = drv.Item("resultunkid").ToString

    '                 '    Dim drpinst As DropDownList = DirectCast(e.Row.FindControl("objddlQualiInst"), DropDownList)
    '                 '    dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))
    '                 '    With drpinst
    '                 '        .DataValueField = "instituteunkid"
    '                 '        .DataTextField = "institute_name"
    '                 '        .DataSource = dsCombo.Tables(0)
    '                 '        .DataBind()
    '                 '        .SelectedValue = drv.Item("instituteunkid").ToString
    '                 '    End With
    '                 'End If
    '                 Dim drpQualiGrp As DropDownList = DirectCast(e.Row.FindControl("objddlQualiGrp"), DropDownList)
    '                 Dim drpAward As DropDownList = DirectCast(e.Row.FindControl("objddlQuali"), DropDownList)
    '                 Dim drpRslt As DropDownList = DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList)
    '                 Dim drpinst As DropDownList = DirectCast(e.Row.FindControl("objddlQualiInst"), DropDownList)
    '                 Dim chkQualification As CheckBox = DirectCast(e.Row.FindControl("objchkQualification"), CheckBox)
    '                 Dim chkQualificationGrp As CheckBox = DirectCast(e.Row.FindControl("objchkQualiGrp"), CheckBox)
    '                 Dim chkQualificationResult As CheckBox = DirectCast(e.Row.FindControl("objchkQualificationResult"), CheckBox)
    '                 Dim chkQualificationInst As CheckBox = DirectCast(e.Row.FindControl("objchkQualificationInst"), CheckBox)


    '                 If CInt(drv.Item("qualificationgroupunkid")) <= 0 Then
    '                     DirectCast(e.Row.FindControl("objtxtQualiGrp"), TextBox).Visible = True
    '                     DirectCast(e.Row.FindControl("objddlQualiGrp"), DropDownList).Visible = False
    '                     chkQualificationGrp.Checked = True
    '                 Else
    '                     DirectCast(e.Row.FindControl("objtxtQualiGrp"), TextBox).Visible = False
    '                     DirectCast(e.Row.FindControl("objddlQualiGrp"), DropDownList).Visible = True
    '                     dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
    '                     With drpQualiGrp
    '                         .DataValueField = "masterunkid"
    '                         .DataTextField = "Name"
    '                         .DataSource = dsCombo.Tables(0)
    '                         .DataBind()
    '                         .SelectedValue = drv.Item("qualificationgroupunkid").ToString
    '                     End With
    '                     chkQualificationGrp.Checked = False
    '                 End If



    '                 If CInt(drv.Item("qualificationunkid")) <= 0 Then
    '                     DirectCast(e.Row.FindControl("objtxtQuali"), TextBox).Visible = True
    '                     DirectCast(e.Row.FindControl("objddlQuali"), DropDownList).Visible = False
    '                     chkQualification.Checked = True

    '                 Else
    '                     DirectCast(e.Row.FindControl("objtxtQuali"), TextBox).Visible = False
    '                     DirectCast(e.Row.FindControl("objddlQuali"), DropDownList).Visible = True

    '                     dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drv.Item("qualificationgroupunkid")))
    '                     With drpAward
    '                         .DataValueField = "Qualificationunkid"
    '                         .DataTextField = "Q_name"
    '                         .DataSource = dsCombo.Tables(0)
    '                         .DataBind()
    '                     End With
    '                     DirectCast(e.Row.FindControl("objddlQuali"), DropDownList).SelectedValue = drv.Item("qualificationunkid").ToString
    '                     chkQualification.Checked = False
    '                 End If

    '                 If CInt(drv.Item("resultunkid")) <= 0 Then
    '                     DirectCast(e.Row.FindControl("objtxtQualiResult"), TextBox).Visible = True
    '                     DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList).Visible = False
    '                     chkQualificationResult.Checked = True
    '                 Else
    '                     DirectCast(e.Row.FindControl("objtxtQualiResult"), TextBox).Visible = False
    '                     DirectCast(e.Row.FindControl("objddlQualiResult"), DropDownList).Visible = True

    '                     dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drv.Item("qualificationunkid")))
    '                     With drpRslt
    '                         .DataValueField = "resultunkid"
    '                         .DataTextField = "resultname"
    '                         .DataSource = dsCombo.Tables(0)
    '                         .DataBind()
    '                     End With
    '                     chkQualificationResult.Checked = False
    '                 End If

    '                 If CInt(drv.Item("instituteunkid")) <= 0 Then
    '                     DirectCast(e.Row.FindControl("objtxtQualiInst"), TextBox).Visible = True
    '                     DirectCast(e.Row.FindControl("objddlQualiInst"), DropDownList).Visible = False
    '                     chkQualificationInst.Checked = True

    '                 Else
    '                     DirectCast(e.Row.FindControl("objtxtQualiInst"), TextBox).Visible = False
    '                     DirectCast(e.Row.FindControl("objddlQualiInst"), DropDownList).Visible = True

    '                     dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))
    '                     With drpinst
    '                         .DataValueField = "instituteunkid"
    '                         .DataTextField = "institute_name"
    '                         .DataSource = dsCombo.Tables(0)
    '                         .DataBind()
    '                         .SelectedValue = drv.Item("instituteunkid").ToString
    '                     End With
    '                     chkQualificationInst.Checked = False
    '                 End If

    '                 'DirectCast(e.Row.FindControl("objdtStartDate"), DateCtrl).SetDate = CDate(drv.Item("startdate"))
    '                 'DirectCast(e.Row.FindControl("objdtStartDate"), DateCtrl).Width = 100
    '                 'DirectCast(e.Row.FindControl("objdtAwardDate"), DateCtrl).SetDate = CDate(drv.Item("enddate"))
    '                 'DirectCast(e.Row.FindControl("objdtAwardDate"), DateCtrl).Width = 100

    '                 'DirectCast(e.Row.FindControl("objchkNaGPA"), CheckBox).Checked = CBool(If(CDec(drv.Item("gpacode")) = 0, True, False))

    '                 'DirectCast(e.Row.FindControl("objtxtCertiNo"), TextBox).Width = 100
    '                 'DirectCast(e.Row.FindControl("objtxtRemark"), TextBox).Width = 100

    '             ElseIf e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
    '                 Dim colQualiGrp As IEnumerable(Of DataControlField) = (From c In grdQualification.Columns.Cast(Of DataControlField) Where (c.HeaderText = "Qualification Group/Qualification") Select (c))
    '                 'Dim colQuali As IEnumerable(Of DataControlField) = (From c In grdQualification.Columns.Cast(Of DataControlField) Where (c.HeaderText = "Qualification") Select (c))
    '                 Dim colResult As IEnumerable(Of DataControlField) = (From c In grdQualification.Columns.Cast(Of DataControlField) Where (c.HeaderText = "Result / Institution") Select (c))
    '                 'Dim colInst As IEnumerable(Of DataControlField) = (From c In grdQualification.Columns.Cast(Of DataControlField) Where (c.HeaderText = "Institution") Select (c))

    '                 'If CInt(drv.Item("qualificationgroupunkid")) <= 0 Then
    '                 '    CType(e.Row.FindControl("objlblQualiGrp"), Label).Text = drv.Item("other_qualificationgrp").ToString
    '                 'End If

    '                 'If CInt(drv.Item("qualificationunkid")) <= 0 Then
    '                 '    CType(e.Row.FindControl("objlblQuali"), Label).Text = drv.Item("other_qualification").ToString
    '                 'End If

    '                 'If CInt(drv.Item("resultunkid")) <= 0 Then
    '                 '    CType(e.Row.FindControl("objlblQualiResult"), Label).Text = drv.Item("other_resultcode").ToString

    '                 'End If

    '                 'If CInt(drv.Item("instituteunkid")) <= 0 Then
    '                 '    CType(e.Row.FindControl("objlblQualiInst"), Label).Text = drv.Item("other_institute").ToString

    '                 'End If

    '             End If
    '         End If
    '     Catch ex As Exception
    '         'S.SANDEEP [03-NOV-2016] -- START
    '         Global_asax.CatchException(ex, Context)
    '         'S.SANDEEP [03-NOV-2016] -- END
    '     End Try
    ' End Sub

    ' Private Sub grdQualification_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdQualification.RowEditing
    '     Try
    '         grdQualification.EditIndex = e.NewEditIndex
    '         'Call FillApplicantQualifications()
    '         grdQualification.DataSource = mdtQualification
    '         grdQualification.DataBind()
    '     Catch ex As Exception
    '         'S.SANDEEP [03-NOV-2016] -- START
    '         Global_asax.CatchException(ex, Context)
    '         'S.SANDEEP [03-NOV-2016] -- END
    '     End Try
    ' End Sub

    ' Private Sub grdQualification_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdQualification.RowCancelingEdit
    '     Try
    '         grdQualification.EditIndex = -1
    '         Call FillApplicantQualifications()
    '     Catch ex As Exception
    '         'S.SANDEEP [03-NOV-2016] -- START
    '         Global_asax.CatchException(ex, Context)
    '         'S.SANDEEP [03-NOV-2016] -- END
    '     End Try
    ' End Sub

    ' Private Sub grdQualification_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdQualification.RowUpdating
    '     Try
    '         '** To Prevent Buttun Click when Page is Refreshed by User. **
    '         If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
    '         Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

    '         Dim intQualiGrp As Integer = 0
    '         Dim intQuali As Integer = 0
    '         Dim intResultCode As Integer = 0
    '         Dim intInstitute As Integer = 0
    '         Dim decGPA As Decimal
    '         Integer.TryParse(DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objddlQualiGrp"), DropDownList).SelectedValue, intQualiGrp)
    '         Integer.TryParse(DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objddlQuali"), DropDownList).SelectedValue, intQuali)
    '         Integer.TryParse(DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objddlQualiResult"), DropDownList).SelectedValue, intResultCode)
    '         Integer.TryParse(DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objddlQualiInst"), DropDownList).SelectedValue, intInstitute)

    '         Dim txtQualiGrp As String = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiGrp"), TextBox).Text
    '         Dim txtQuali As String = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtQuali"), TextBox).Text
    '         Dim txtQualiResult As String = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiResult"), TextBox).Text
    '         Dim txtQualiInstitute As String = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiInst"), TextBox).Text

    '         Dim dtAwardStartDate As Date = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objdtStartDate"), DateCtrl).GetDate
    '         Dim dtAwardDate As Date = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objdtAwardDate"), DateCtrl).GetDate
    '         Dim blnNaGPA As Boolean = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objchkNaGPA"), CheckBox).Checked
    '         Decimal.TryParse(DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtGPACode"), TextBox).Text, decGPA)
    '         Dim txtCertiNo As String = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtCertiNo"), TextBox).Text
    '         Dim txtRemark As String = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objtxtRemark"), TextBox).Text
    '         Dim blnHighestQualification As Boolean = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objchkHighestQualification"), CheckBox).Checked

    '         Dim chkQualiGrp As CheckBox = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objchkQualiGrp"), CheckBox)
    '         Dim chkQualification As CheckBox = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objchkQualification"), CheckBox)
    '         Dim chkQualificationResult As CheckBox = DirectCast(grdQualification.Rows(e.RowIndex).FindControl("objchkQualificationResult"), CheckBox)


    '         'If grdQualification.Rows(e.RowIndex).FindControl("objddlQualiGrp").Visible = True AndAlso grdQualification.Rows(e.RowIndex).FindControl("objddlQuali").Visible = True Then
    '         '    If intQualiGrp <= 0 Then
    '         '        ShowMessage("Please select Qualification Group", MessageType.Info)
    '         '        grdQualification.Rows(e.RowIndex).FindControl("objddlQualiGrp").Focus()
    '         '        e.Cancel = True
    '         '        Exit Try
    '         '    ElseIf intQuali <= 0 Then
    '         '        ShowMessage("Please select Qualification", MessageType.Info)
    '         '        grdQualification.Rows(e.RowIndex).FindControl("objddlQuali").Focus()
    '         '        e.Cancel = True
    '         '        Exit Try
    '         '    ElseIf intResultCode <= 0 Then
    '         '        ShowMessage("Please select Result Code", MessageType.Info)
    '         '        grdQualification.Rows(e.RowIndex).FindControl("objddlQualiResult").Focus()
    '         '        e.Cancel = True
    '         '        Exit Try
    '         '    End If
    '         'Else
    '         '    If txtQualiGrp.Trim = "" Then
    '         '        ShowMessage("Please enter Qualification Group", MessageType.Info)
    '         '        grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiGrp").Focus()
    '         '        e.Cancel = True
    '         '        Exit Try
    '         '    ElseIf txtQuali.Trim = "" Then
    '         '        ShowMessage("Please enter Qualification", MessageType.Info)
    '         '        grdQualification.Rows(e.RowIndex).FindControl("objtxtQuali").Focus()
    '         '        e.Cancel = True
    '         '        Exit Try
    '         '    ElseIf txtQualiResult.Trim = "" Then
    '         '        ShowMessage("Please enter Qualification Result", MessageType.Info)
    '         '        grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiResult").Focus()
    '         '        e.Cancel = True
    '         '        Exit Try
    '         '    End If
    '         'End If



    '         If chkQualiGrp.Checked = False Then
    '             If intQualiGrp <= 0 Then
    '                 ShowMessage(lblAwardgpMsg.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objddlQualiGrp").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    '         Else
    '             If txtQualiGrp.Trim = "" Then
    '                 ShowMessage(lblOtherQualiGroupMsg.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiGrp").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    '         End If


    '         If chkQualification.Checked = False Then
    '             If intQuali <= 0 Then
    '                 ShowMessage(lblAwardMsg.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objddlQuali").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    '         Else
    '             If txtQuali.Trim = "" Then
    '                 ShowMessage(lblOtherQualificationMsg.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objtxtQuali").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    '         End If

    '         If chkQualificationResult.Checked = False Then
    '             If intResultCode <= 0 AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
    '                 ShowMessage(lblResultCodeMsg.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objddlQualiResult").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    '         Else
    '             If txtQualiResult.Trim = "" AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
    '                 ShowMessage(lblOtherResultCodeMsg.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objtxtQualiResult").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    '         End If

    '         'If intInstitute <= 0 Then
    '         '    ShowMessage("Please select Institute", MessageType.Info)
    '         '    grdQualification.Rows(e.RowIndex).FindControl("objddlQualiInst").Focus()
    '         '    e.Cancel = True
    '         '    Exit Try
    '         If dtAwardStartDate = CDate("01-Jan-1900") AndAlso CBool(Session("QualificationStartDateMandatory")) = True Then
    '             ShowMessage(lblAwardDateMsg.Text, MessageType.Info)
    '             grdQualification.Rows(e.RowIndex).FindControl("objdtStartDate").Focus()
    '             e.Cancel = True
    '             Exit Try
    '         ElseIf dtAwardDate = CDate("01-Jan-1900") AndAlso CBool(Session("QualificationAwardDateMandatory")) = True Then
    '             ShowMessage(lblDatetoMsg.Text, MessageType.Info)
    '             grdQualification.Rows(e.RowIndex).FindControl("objdtAwardDate").Focus()
    '             e.Cancel = True
    '             Exit Try
    '         ElseIf dtAwardDate <> CDate("01/Jan/1900") AndAlso dtAwardStartDate <> CDate("01/Jan/1900") Then
    '             If dtAwardDate <= dtAwardStartDate Then
    '                 ShowMessage(lblDatetoMsg2.Text, MessageType.Info)
    '                 grdQualification.Rows(e.RowIndex).FindControl("objdtAwardDate").Focus()
    '                 e.Cancel = True
    '                 Exit Try
    '             End If
    'ElseIf blnNaGPA = False AndAlso decGPA <= 0 Then
    '             ShowMessage(lblGPAMsg.Text, MessageType.Info)
    '	grdQualification.Rows(e.RowIndex).FindControl("objtxtGPACode").Focus()
    '	e.Cancel = True
    '	Exit Try
    'ElseIf decGPA > 999 Then  '
    '             ShowMessage(lblGPAMsg2.Text, MessageType.Info)
    '	grdQualification.Rows(e.RowIndex).FindControl("objtxtGPACode").Focus()
    '             e.Cancel = True
    '             Exit Try
    '         End If

    '         If objAppQuali.IsExistApplicantQualification(strCompCode:=Session("CompCode").ToString _
    '                                                      , intComUnkID:=CInt(Session("companyunkid")) _
    '                                                      , intApplicantUnkid:=CInt(Session("applicantunkid")) _
    '                                                      , intQualificationTranUnkid:=CInt(e.Keys("qualificationtranunkid")) _
    '                                                      , intQualificationGroupUnkid:=intQualiGrp _
    '                                                      , intQualificationUnkid:=intQuali _
    '                                                      , strOther_Qualificationgrp:=txtQualiGrp _
    '                                                      , strOther_Qualification:=txtQuali
    '                                                      ) = True Then

    '             ShowMessage(lblExistMsg.Text, MessageType.Info)
    '             Exit Try
    '         End If

    '         If clsApplicantQualification.EditApplicantQualification(strCompCode:=Session("CompCode").ToString _
    '                                                  , intComUnkID:=CInt(Session("companyunkid")) _
    '                                                  , intApplicantUnkid:=CInt(Session("applicantunkid")) _
    '                                                  , intQualificationTranUnkid:=CInt(e.Keys("qualificationtranunkid")) _
    '                                                  , intQualificationGroupUnkid:=intQualiGrp _
    '                                                  , intQualificationUnkid:=intQuali _
    '                                                  , intInstituteunkid:=intInstitute _
    '                                                  , intResultunkid:=intResultCode _
    '                                                  , strOther_Qualificationgrp:=txtQualiGrp.Trim _
    '                                                  , strOther_Qualification:=txtQuali.Trim _
    '                                                  , strOther_Resultcode:=txtQualiResult _
    '                                                  , strOther_Institute:=txtQualiInstitute _
    '                                                  , dtTransaction_Date:=Now _
    '                                                  , dtAward_start_Date:=dtAwardStartDate _
    '                                                  , dtAward_end_Date:=dtAwardDate _
    '                                                  , decGPACode:=decGPA _
    '                                                  , strCertificateno:=txtCertiNo _
    '                                                  , strRemark:=txtRemark.Trim _
    '                                                  , strReference_no:="" _
    '                                                  , blnHighestQualification:=blnHighestQualification _
    '                                                  , qualificationtranunkid:=CInt(e.Keys("qualificationtranunkid"))
    '                                                  ) = True Then

    '             ShowMessage(lblUpdateMsg.Text, MessageType.Info)

    '             grdQualification.EditIndex = -1
    '             Call FillApplicantQualifications()

    '         End If

    '     Catch ex As Exception
    '         'S.SANDEEP [03-NOV-2016] -- START
    '         Global_asax.CatchException(ex, Context)
    '         'S.SANDEEP [03-NOV-2016] -- END
    '     End Try
    ' End Sub

    ' Private Sub grdQualification_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdQualification.RowDeleting
    '     Try
    '         '** To Prevent Buttun Click when Page is Refreshed by User. **
    '         If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
    '         Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

    '         If clsApplicantQualification.DeleteApplicantQualification(strCompCode:=Session("CompCode").ToString _
    '                                                      , intComUnkID:=CInt(Session("companyunkid")) _
    '                                                      , intApplicantUnkid:=CInt(Session("applicantunkid")) _
    '                                                      , intQualificationTranUnkid:=CInt(e.Keys("qualificationtranunkid")) _
    '                                                      , qualificationtranunkid:=CInt(e.Keys("qualificationtranunkid"))
    '                                                      ) = True Then

    '             ShowMessage(lblDeleteMsg.Text, MessageType.Info)

    '             Call FillApplicantQualifications()
    '         End If
    '     Catch ex As Exception
    '         'S.SANDEEP [03-NOV-2016] -- START
    '         Global_asax.CatchException(ex, Context)
    '         'S.SANDEEP [03-NOV-2016] -- END
    '     End Try
    ' End Sub

    'Private Sub grdQualification_Sorting(sender As Object, e As GridViewSortEventArgs) Handles grdQualification.Sorting
    '    Try
    '        'Dim dt As DataTable = CType(ViewState("dtTable"), DataTable)

    '        If mdtQualification.Rows.Count > 0 Then
    '            Dim columnIndex As Integer = 0

    '            If ViewState("sort").ToString = "Asc" Then
    '                ViewState("sort") = "Desc"
    '            Else
    '                ViewState("sort") = "Asc"
    '            End If

    '            mdtQualification.DefaultView.Sort = e.SortExpression & " " & ViewState("sort").ToString
    '            grdQualification.DataSource = mdtQualification.DefaultView
    '            grdQualification.DataBind()

    '            For Each field As DataControlFieldHeaderCell In grdQualification.HeaderRow.Cells
    '                If field.ContainingField.SortExpression = e.SortExpression Then
    '                    columnIndex = grdQualification.HeaderRow.Cells.GetCellIndex(field)
    '                    Exit For
    '                End If
    '            Next
    '            Dim a As LinkButton = CType(grdQualification.HeaderRow.Cells(columnIndex).Controls(0), LinkButton)
    '            If ViewState("sort").ToString = "Asc" Then
    '                a.Text &= " <i aria-hidden='true' class='fa fa-sort-up'></i>"
    '            Else
    '                a.Text &= " <i aria-hidden='true' class='fa fa-sort-down'></i>"
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub lvQualification_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvQualification.ItemDataBound
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Try
            If e.Item.ItemType = ListViewItemType.DataItem AndAlso lvQualification.EditIndex = -1 Then
                Dim pnllvGPA As Panel = DirectCast(e.Item.FindControl("pnllvGPA"), Panel)
                Dim pnllvGPANotApplicable As Panel = DirectCast(e.Item.FindControl("pnllvGPANotApplicable"), Panel)
                Dim pnllvCertiNo As Panel = DirectCast(e.Item.FindControl("pnllvCertNo"), Panel)
                Dim pnllvRemark As Panel = DirectCast(e.Item.FindControl("pnllvRemark"), Panel)

                If CBool(Session("QualificationGPAMandatory")) = False Then
                    pnllvGPA.Visible = False
                    pnllvGPANotApplicable.Visible = False
                End If
                If CBool(Session("CertificateNoMandatory")) = False Then
                    pnllvCertiNo.Visible = False
                End If
                If CBool(Session("HideRecruitementQualificationRemark")) = True Then
                    pnllvRemark.Visible = False
                End If
            End If
            If e.Item.ItemType = ListViewItemType.DataItem AndAlso lvQualification.EditIndex >= 0 AndAlso e.Item.DataItemIndex = lvQualification.EditIndex Then

                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)

                Dim drpQualiGrp As DropDownList = DirectCast(e.Item.FindControl("objddllvQualiGrp"), DropDownList)
                Dim drpAward As DropDownList = DirectCast(e.Item.FindControl("objddllvQuali"), DropDownList)
                Dim drpRslt As DropDownList = DirectCast(e.Item.FindControl("objddllvQualiResult"), DropDownList)
                Dim drpinst As DropDownList = DirectCast(e.Item.FindControl("objddllvQualiInst"), DropDownList)
                Dim chkQualification As CheckBox = DirectCast(e.Item.FindControl("objchklvQualification"), CheckBox)
                Dim chkQualificationGrp As CheckBox = DirectCast(e.Item.FindControl("objchklvQualiGrp"), CheckBox)
                Dim chkQualificationResult As CheckBox = DirectCast(e.Item.FindControl("objchklvQualificationResult"), CheckBox)
                Dim chkQualificationInst As CheckBox = DirectCast(e.Item.FindControl("objchklvQualificationInst"), CheckBox)
                Dim pnllvGPA As Panel = DirectCast(e.Item.FindControl("pnllvGPA"), Panel)
                Dim pnllvGPANotApplicable As Panel = DirectCast(e.Item.FindControl("pnllvGPANotApplicable"), Panel)
                Dim pnllvCertiNo As Panel = DirectCast(e.Item.FindControl("pnllvCertNo"), Panel)
                Dim pnllvRemark As Panel = DirectCast(e.Item.FindControl("pnllvRemark"), Panel)

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.

                'Pinkal (30-Sep-2023) -- Start
                ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                Dim dr As DataRow = Nothing
                dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)

                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
                dr = Nothing
                dr = dsCombo.Tables(0).NewRow()
                dr.Item("masterunkid") = -2
                dr.Item("name") = lblOtherNotInListmsg.Text
                dsCombo.Tables(0).Rows.InsertAt(dr, 1)
                End If
                'Pinkal (30-Sep-2023) -- End

                With drpQualiGrp
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    '.SelectedValue = drv.Item("qualificationgroupunkid").ToString
                End With

                dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drv.Item("qualificationgroupunkid")))

                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
                dr = Nothing
                dr = dsCombo.Tables(0).NewRow()
                dr.Item("qualificationunkid") = -2
                dr.Item("Q_name") = lblOtherNotInListmsg.Text
                dsCombo.Tables(0).Rows.InsertAt(dr, 1)
                End If
                'Pinkal (30-Sep-2023) -- End

                With drpAward
                    .DataValueField = "Qualificationunkid"
                    .DataTextField = "Q_name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With

                dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drv.Item("qualificationunkid")))

                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
                dr = Nothing
                dr = dsCombo.Tables(0).NewRow()
                dr.Item("resultunkid") = -2
                dr.Item("resultname") = lblOtherNotInListmsg.Text
                dsCombo.Tables(0).Rows.InsertAt(dr, 1)
                End If
                'Pinkal (30-Sep-2023) -- End

                With drpRslt
                    .DataValueField = "resultunkid"
                    .DataTextField = "resultname"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With

                dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))

                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
                dr = Nothing
                dr = dsCombo.Tables(0).NewRow()
                dr.Item("instituteunkid") = -2
                dr.Item("institute_name") = lblOtherNotInListmsg.Text
                dsCombo.Tables(0).Rows.InsertAt(dr, 1)
                End If
                'Pinkal (30-Sep-2023) -- End

                With drpinst
                    .DataValueField = "instituteunkid"
                    .DataTextField = "institute_name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
                'Sohail (13 May 2022) -- End

                If CInt(drv.Item("qualificationgroupunkid")) <= 0 Then
                    DirectCast(e.Item.FindControl("objtxtlvQualiGrp"), TextBox).Visible = True
                    DirectCast(e.Item.FindControl("objddllvQualiGrp"), DropDownList).Visible = False
                    DirectCast(e.Item.FindControl("btnlvHideOtherQualiGrp"), HtmlButton).Visible = True 'Sohail (13 May 2022)
                    chkQualificationGrp.Checked = True
                Else
                    DirectCast(e.Item.FindControl("objtxtlvQualiGrp"), TextBox).Visible = False
                    DirectCast(e.Item.FindControl("objddllvQualiGrp"), DropDownList).Visible = True
                    DirectCast(e.Item.FindControl("btnlvHideOtherQualiGrp"), HtmlButton).Visible = False 'Sohail (13 May 2022)
                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
                    'With drpQualiGrp
                    '    .DataValueField = "masterunkid"
                    '    .DataTextField = "Name"
                    '    .DataSource = dsCombo.Tables(0)
                    '    .DataBind()
                    '    .SelectedValue = drv.Item("qualificationgroupunkid").ToString
                    'End With
                    drpQualiGrp.SelectedValue = drv.Item("qualificationgroupunkid").ToString
                    'Sohail (13 May 2022) -- End
                    chkQualificationGrp.Checked = False
                End If

                If CInt(drv.Item("qualificationunkid")) <= 0 Then
                    DirectCast(e.Item.FindControl("objtxtlvQuali"), TextBox).Visible = True
                    DirectCast(e.Item.FindControl("objddllvQuali"), DropDownList).Visible = False
                    DirectCast(e.Item.FindControl("btnlvHideOtherQuali"), HtmlButton).Visible = True 'Sohail (13 May 2022)
                    chkQualification.Checked = True

                Else
                    DirectCast(e.Item.FindControl("objtxtlvQuali"), TextBox).Visible = False
                    DirectCast(e.Item.FindControl("objddllvQuali"), DropDownList).Visible = True
                    DirectCast(e.Item.FindControl("btnlvHideOtherQuali"), HtmlButton).Visible = False 'Sohail (13 May 2022)

                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drv.Item("qualificationgroupunkid")))
                    'With drpAward
                    '    .DataValueField = "Qualificationunkid"
                    '    .DataTextField = "Q_name"
                    '    .DataSource = dsCombo.Tables(0)
                    '    .DataBind()
                    'End With
                    'Sohail (13 May 2022) -- End
                    DirectCast(e.Item.FindControl("objddllvQuali"), DropDownList).SelectedValue = drv.Item("qualificationunkid").ToString
                    chkQualification.Checked = False
                End If

                If CInt(drv.Item("resultunkid")) <= 0 Then
                    DirectCast(e.Item.FindControl("objtxtlvQualiResult"), TextBox).Visible = True
                    DirectCast(e.Item.FindControl("objddllvQualiResult"), DropDownList).Visible = False
                    DirectCast(e.Item.FindControl("btnlvHideOtherResult"), HtmlButton).Visible = True 'Sohail (13 May 2022)
                    chkQualificationResult.Checked = True
                Else
                    DirectCast(e.Item.FindControl("objtxtlvQualiResult"), TextBox).Visible = False
                    DirectCast(e.Item.FindControl("objddllvQualiResult"), DropDownList).Visible = True
                    DirectCast(e.Item.FindControl("btnlvHideOtherResult"), HtmlButton).Visible = False 'Sohail (13 May 2022)

                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(drv.Item("qualificationunkid")))
                    'With drpRslt
                    '    .DataValueField = "resultunkid"
                    '    .DataTextField = "resultname"
                    '    .DataSource = dsCombo.Tables(0)
                    '    .DataBind()
                    'End With
                    drpRslt.SelectedValue = drv.Item("resultunkid").ToString
                    'Sohail (13 May 2022) -- End
                    chkQualificationResult.Checked = False
                End If

                If CInt(drv.Item("instituteunkid")) <= 0 Then
                    DirectCast(e.Item.FindControl("objtxtlvQualiInst"), TextBox).Visible = True
                    DirectCast(e.Item.FindControl("objddllvQualiInst"), DropDownList).Visible = False
                    DirectCast(e.Item.FindControl("btnlvHideOtherInst"), HtmlButton).Visible = True 'Sohail (13 May 2022)
                    chkQualificationInst.Checked = True

                Else
                    DirectCast(e.Item.FindControl("objtxtlvQualiInst"), TextBox).Visible = False
                    DirectCast(e.Item.FindControl("objddllvQualiInst"), DropDownList).Visible = True
                    DirectCast(e.Item.FindControl("btnlvHideOtherInst"), HtmlButton).Visible = False 'Sohail (13 May 2022)

                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))
                    'With drpinst
                    '    .DataValueField = "instituteunkid"
                    '    .DataTextField = "institute_name"
                    '    .DataSource = dsCombo.Tables(0)
                    '    .DataBind()
                    '    .SelectedValue = drv.Item("instituteunkid").ToString
                    'End With
                    drpinst.SelectedValue = drv.Item("instituteunkid").ToString
                    'Sohail (13 May 2022) -- End
                    chkQualificationInst.Checked = False
                End If

                If CBool(Session("QualificationGPAMandatory")) = False Then
                    pnllvGPA.Visible = False
                    pnllvGPANotApplicable.Visible = False
                End If
                If CBool(Session("CertificateNoMandatory")) = False Then
                    pnllvCertiNo.Visible = False
                End If
                If CBool(Session("HideRecruitementQualificationRemark")) = True Then
                    pnllvRemark.Visible = False
                End If


            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvQualification_ItemEditing(sender As Object, e As ListViewEditEventArgs) Handles lvQualification.ItemEditing
        Try
            lvQualification.EditIndex = e.NewEditIndex
            'lvQualification.DataSource = mdtQualification
            lvQualification.DataBind()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvQualification_ItemCanceling(sender As Object, e As ListViewCancelEventArgs) Handles lvQualification.ItemCanceling
        Try
            lvQualification.EditIndex = -1
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvQualification_ItemUpdating(sender As Object, e As ListViewUpdateEventArgs) Handles lvQualification.ItemUpdating
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim intQualiGrp As Integer = 0
            Dim intQuali As Integer = 0
            Dim intResultCode As Integer = 0
            Dim intInstitute As Integer = 0
            Dim decGPA As Decimal
            Integer.TryParse(DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objddllvQualiGrp"), DropDownList).SelectedValue, intQualiGrp)
            Integer.TryParse(DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objddllvQuali"), DropDownList).SelectedValue, intQuali)
            Integer.TryParse(DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objddllvQualiResult"), DropDownList).SelectedValue, intResultCode)
            Integer.TryParse(DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objddllvQualiInst"), DropDownList).SelectedValue, intInstitute)

            Dim txtQualiGrp As String = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQualiGrp"), TextBox).Text
            Dim txtQuali As String = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQuali"), TextBox).Text
            Dim txtQualiResult As String = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQualiResult"), TextBox).Text
            Dim txtQualiInstitute As String = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQualiInst"), TextBox).Text

            Dim dtAwardStartDate As Date = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objdtlvStartDate"), DateCtrl).GetDate
            Dim dtAwardDate As Date = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objdtlvAwardDate"), DateCtrl).GetDate
            Dim blnNaGPA As Boolean = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objchklvNaGPA"), CheckBox).Checked
            Decimal.TryParse(DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvGPACode"), TextBox).Text, decGPA)
            Dim txtCertiNo As String = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvCertiNo"), TextBox).Text
            Dim txtRemark As String = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objtxtlvRemark"), TextBox).Text
            Dim blnHighestQualification As Boolean = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objchklvHighestQualification"), CheckBox).Checked

            Dim chkQualiGrp As CheckBox = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objchklvQualiGrp"), CheckBox)
            Dim chkQualification As CheckBox = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objchklvQualification"), CheckBox)
            Dim chkQualificationResult As CheckBox = DirectCast(lvQualification.Items(e.ItemIndex).FindControl("objchklvQualificationResult"), CheckBox)

            If chkQualiGrp.Checked = False Then
                If intQualiGrp <= 0 Then
                    ShowMessage(lblAwardgpMsg.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objddllvQualiGrp").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            Else
                If txtQualiGrp.Trim = "" Then
                    ShowMessage(lblOtherQualiGroupMsg.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQualiGrp").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            End If


            If chkQualification.Checked = False Then
                If intQuali <= 0 Then
                    ShowMessage(lblAwardMsg.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objddllvQuali").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            Else
                If txtQuali.Trim = "" Then
                    ShowMessage(lblOtherQualificationMsg.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQuali").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            End If

            If chkQualificationResult.Checked = False Then
                If intResultCode <= 0 AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
                    ShowMessage(lblResultCodeMsg.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objddllvQualiResult").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            Else
                If txtQualiResult.Trim = "" AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
                    ShowMessage(lblOtherResultCodeMsg.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objtxtlvQualiResult").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            End If

            'If intInstitute <= 0 Then
            '    ShowMessage("Please select Institute", MessageType.Info)
            '    grdQualification.Rows(e.RowIndex).FindControl("objddlQualiInst").Focus()
            '    e.Cancel = True
            '    Exit Try
            If dtAwardStartDate = CDate("01-Jan-1900") AndAlso CBool(Session("QualificationStartDateMandatory")) = True Then
                ShowMessage(lblAwardDateMsg.Text, MessageType.Info)
                lvQualification.Items(e.ItemIndex).FindControl("objdtlvStartDate").Focus()
                e.Cancel = True
                Exit Try
            ElseIf dtAwardDate = CDate("01-Jan-1900") AndAlso CBool(Session("QualificationAwardDateMandatory")) = True Then
                ShowMessage(lblDatetoMsg.Text, MessageType.Info)
                lvQualification.Items(e.ItemIndex).FindControl("objdtlvAwardDate").Focus()
                e.Cancel = True
                Exit Try
            ElseIf dtAwardDate <> CDate("01/Jan/1900") AndAlso dtAwardStartDate <> CDate("01/Jan/1900") Then
                If dtAwardDate <= dtAwardStartDate Then
                    ShowMessage(lblDatetoMsg2.Text, MessageType.Info)
                    lvQualification.Items(e.ItemIndex).FindControl("objdtlvAwardDate").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            ElseIf blnNaGPA = False AndAlso decGPA <= 0 Then
                ShowMessage(lblGPAMsg.Text, MessageType.Info)
                lvQualification.Items(e.ItemIndex).FindControl("objtxtlvGPACode").Focus()
                e.Cancel = True
                Exit Try
            ElseIf decGPA > 999 Then  '
                ShowMessage(lblGPAMsg2.Text, MessageType.Info)
                lvQualification.Items(e.ItemIndex).FindControl("objtxtlvGPACode").Focus()
                e.Cancel = True
                Exit Try
            End If

            If objAppQuali.IsExistApplicantQualification(strCompCode:=Session("CompCode").ToString _
                                                         , intComUnkID:=CInt(Session("companyunkid")) _
                                                         , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                         , intQualificationTranUnkid:=CInt(e.Keys("qualificationtranunkid")) _
                                                         , intQualificationGroupUnkid:=intQualiGrp _
                                                         , intQualificationUnkid:=intQuali _
                                                         , strOther_Qualificationgrp:=txtQualiGrp _
                                                         , strOther_Qualification:=txtQuali
                                                         ) = True Then

                ShowMessage(lblExistMsg.Text, MessageType.Info)
                e.Cancel = True
                Exit Try
            End If

            'objodsQuali.UpdateParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            'objodsQuali.UpdateParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            'objodsQuali.UpdateParameters.Item("intApplicantUnkid").DefaultValue = CInt(Session("applicantunkid")).ToString
            'objodsQuali.UpdateParameters.Item("intQualificationTranUnkid").DefaultValue = CInt(e.Keys("qualificationtranunkid")).ToString
            'objodsQuali.UpdateParameters.Item("intQualificationGroupUnkid").DefaultValue = intQualiGrp.ToString
            'objodsQuali.UpdateParameters.Item("intQualificationUnkid").DefaultValue = intQuali.ToString
            'objodsQuali.UpdateParameters.Item("intInstituteunkid").DefaultValue = intInstitute.ToString
            'objodsQuali.UpdateParameters.Item("intResultunkid").DefaultValue = intResultCode.ToString
            'objodsQuali.UpdateParameters.Item("strOther_Qualificationgrp").DefaultValue = txtQualiGrp.Trim
            'objodsQuali.UpdateParameters.Item("strOther_Qualification").DefaultValue = txtQuali.Trim
            'objodsQuali.UpdateParameters.Item("strOther_Resultcode").DefaultValue = txtQualiResult
            'objodsQuali.UpdateParameters.Item("strOther_Institute").DefaultValue = txtQualiInstitute
            'objodsQuali.UpdateParameters.Item("dtTransaction_Date").DefaultValue = Now.ToString
            'objodsQuali.UpdateParameters.Item("dtAward_start_Date").DefaultValue = dtAwardStartDate.ToString
            'objodsQuali.UpdateParameters.Item("dtAward_end_Date").DefaultValue = dtAwardDate.ToString
            'objodsQuali.UpdateParameters.Item("decGPACode").DefaultValue = decGPA.ToString
            'objodsQuali.UpdateParameters.Item("strCertificateno").DefaultValue = txtCertiNo
            'objodsQuali.UpdateParameters.Item("strRemark").DefaultValue = txtRemark.Trim
            'objodsQuali.UpdateParameters.Item("strReference_no").DefaultValue = ""
            'objodsQuali.UpdateParameters.Item("blnHighestQualification").DefaultValue = blnHighestQualification.ToString
            ''If objAppQuali.EditApplicantQualification(strCompCode:=Session("CompCode").ToString _
            ''                                         , intComUnkID:=CInt(Session("companyunkid")) _
            ''                                         , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            ''                                         , intQualificationTranUnkid:=CInt(e.Keys("qualificationtranunkid")) _
            ''                                         , intQualificationGroupUnkid:=intQualiGrp _
            ''                                         , intQualificationUnkid:=intQuali _
            ''                                         , intInstituteunkid:=intInstitute _
            ''                                         , intResultunkid:=intResultCode _
            ''                                         , strOther_Qualificationgrp:=txtQualiGrp.Trim _
            ''                                         , strOther_Qualification:=txtQuali.Trim _
            ''                                         , strOther_Resultcode:=txtQualiResult _
            ''                                         , strOther_Institute:=txtQualiInstitute _
            ''                                         , dtTransaction_Date:=Now _
            ''                                         , dtAward_start_Date:=dtAwardStartDate _
            ''                                         , dtAward_end_Date:=dtAwardDate _
            ''                                         , decGPACode:=decGPA _
            ''                                         , strCertificateno:=txtCertiNo _
            ''                                         , strRemark:=txtRemark.Trim _
            ''                                         , strReference_no:="" _
            ''                                         , blnHighestQualification:=blnHighestQualification
            ''                                         ) = True Then

            ''ShowMessage(lblUpdateMsg.Text, MessageType.Info)


            ''    'objodsQuali.DataBind()
            ''    'lvQualification.DataSource = objodsQuali.Select
            ''    'lvQualification.DataBind()
            ''    'lvQualification.EditIndex = -1
            ''    'UpdatePanel1.Update()
            ''    'Call FillApplicantQualifications()

            ''End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsQuali_Updating(sender As Object, e As ObjectDataSourceMethodEventArgs) Handles objodsQuali.Updating
        Try

            Dim intQualiGrp As Integer = 0
            Dim intQuali As Integer = 0
            Dim intResultCode As Integer = 0
            Dim intInstitute As Integer = 0
            Dim decGPA As Decimal
            Integer.TryParse(DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQualiGrp"), DropDownList).SelectedValue, intQualiGrp)
            Integer.TryParse(DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQuali"), DropDownList).SelectedValue, intQuali)
            Integer.TryParse(DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQualiResult"), DropDownList).SelectedValue, intResultCode)
            Integer.TryParse(DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQualiInst"), DropDownList).SelectedValue, intInstitute)

            Dim txtQualiGrp As String = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQualiGrp"), TextBox).Text
            Dim txtQuali As String = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQuali"), TextBox).Text
            Dim txtQualiResult As String = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQualiResult"), TextBox).Text
            Dim txtQualiInstitute As String = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQualiInst"), TextBox).Text

            Dim dtAwardStartDate As Date = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objdtlvStartDate"), DateCtrl).GetDate
            Dim dtAwardDate As Date = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objdtlvAwardDate"), DateCtrl).GetDate
            Dim blnNaGPA As Boolean = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvNaGPA"), CheckBox).Checked
            Decimal.TryParse(DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvGPACode"), TextBox).Text, decGPA)
            Dim txtCertiNo As String = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvCertiNo"), TextBox).Text
            Dim txtRemark As String = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvRemark"), TextBox).Text
            Dim blnHighestQualification As Boolean = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvHighestQualification"), CheckBox).Checked

            Dim chkQualiGrp As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualiGrp"), CheckBox)
            Dim chkQualification As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualification"), CheckBox)
            Dim chkQualificationResult As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualificationResult"), CheckBox)

            'If chkQualiGrp.Checked = False Then
            '    If intQualiGrp <= 0 Then
            '        ShowMessage(lblAwardgpMsg.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQualiGrp").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'Else
            '    If txtQualiGrp.Trim = "" Then
            '        ShowMessage(lblOtherQualiGroupMsg.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQualiGrp").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'End If


            'If chkQualification.Checked = False Then
            '    If intQuali <= 0 Then
            '        ShowMessage(lblAwardMsg.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQuali").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'Else
            '    If txtQuali.Trim = "" Then
            '        ShowMessage(lblOtherQualificationMsg.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQuali").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'End If

            'If chkQualificationResult.Checked = False Then
            '    If intResultCode <= 0 AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
            '        ShowMessage(lblResultCodeMsg.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQualiResult").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'Else
            '    If txtQualiResult.Trim = "" AndAlso CBool(Session("QualificationResultCodeMandatory")) = True Then
            '        ShowMessage(lblOtherResultCodeMsg.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvQualiResult").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'End If

            ''If intInstitute <= 0 Then
            ''    ShowMessage("Please select Institute", MessageType.Info)
            ''    grdQualification.Rows(e.RowIndex).FindControl("objddlQualiInst").Focus()
            ''    e.Cancel = True
            ''    Exit Try
            'If dtAwardStartDate = CDate("01-Jan-1900") AndAlso CBool(Session("QualificationStartDateMandatory")) = True Then
            '    ShowMessage(lblAwardDateMsg.Text, MessageType.Info)
            '    lvQualification.Items(lvQualification.EditIndex).FindControl("objdtlvStartDate").Focus()
            '    e.Cancel = True
            '    Exit Try
            'ElseIf dtAwardDate = CDate("01-Jan-1900") AndAlso CBool(Session("QualificationAwardDateMandatory")) = True Then
            '    ShowMessage(lblDatetoMsg.Text, MessageType.Info)
            '    lvQualification.Items(lvQualification.EditIndex).FindControl("objdtlvAwardDate").Focus()
            '    e.Cancel = True
            '    Exit Try
            'ElseIf dtAwardDate <> CDate("01/Jan/1900") AndAlso dtAwardStartDate <> CDate("01/Jan/1900") Then
            '    If dtAwardDate <= dtAwardStartDate Then
            '        ShowMessage(lblDatetoMsg2.Text, MessageType.Info)
            '        lvQualification.Items(lvQualification.EditIndex).FindControl("objdtlvAwardDate").Focus()
            '        e.Cancel = True
            '        Exit Try
            '    End If
            'ElseIf blnNaGPA = False AndAlso decGPA <= 0 Then
            '    ShowMessage(lblGPAMsg.Text, MessageType.Info)
            '    lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvGPACode").Focus()
            '    e.Cancel = True
            '    Exit Try
            'ElseIf decGPA > 999 Then  '
            '    ShowMessage(lblGPAMsg2.Text, MessageType.Info)
            '    lvQualification.Items(lvQualification.EditIndex).FindControl("objtxtlvGPACode").Focus()
            '    e.Cancel = True
            '    Exit Try
            'End If

            'If objAppQuali.IsExistApplicantQualification(strCompCode:=Session("CompCode").ToString _
            '                                             , intComUnkID:=CInt(Session("companyunkid")) _
            '                                             , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            '                                             , intQualificationTranUnkid:=CInt(lvQualification.DataKeys(lvQualification.EditItem.DataItemIndex).Item("qualificationtranunkid")) _
            '                                             , intQualificationGroupUnkid:=intQualiGrp _
            '                                             , intQualificationUnkid:=intQuali _
            '                                             , strOther_Qualificationgrp:=txtQualiGrp _
            '                                             , strOther_Qualification:=txtQuali
            '                                             ) = True Then

            '    ShowMessage(lblExistMsg.Text, MessageType.Info)
            '    e.Cancel = True
            '    Exit Try
            'End If


            Dim objAppQualiUpdate As New dtoAppQualiUpdate
            With objAppQualiUpdate
                .CompCode = Session("CompCode").ToString
                .ComUnkID = CInt(Session("companyunkid"))
                .ApplicantUnkid = CInt(Session("applicantunkid"))
                .QualificationTranUnkid = CInt(lvQualification.DataKeys(lvQualification.EditItem.DataItemIndex).Item("qualificationtranunkid"))
                .QualificationGroupUnkid = intQualiGrp
                .QualificationUnkid = intQuali
                .Instituteunkid = intInstitute
                .Resultunkid = intResultCode
                .Other_Qualificationgrp = txtQualiGrp.Trim
                .Other_Qualification = txtQuali.Trim
                .Other_Resultcode = txtQualiResult
                .Other_Institute = txtQualiInstitute
                .Transaction_Date = Now
                .Award_start_Date = dtAwardStartDate
                .Award_end_Date = dtAwardDate
                .GPACode = decGPA
                .Certificateno = txtCertiNo
                .Remark = txtRemark.Trim
                .Reference_no = ""
                .HighestQualification = blnHighestQualification
            End With

            e.InputParameters("objAppQualiUpdate") = objAppQualiUpdate

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsQuali_Updated(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles objodsQuali.Updated
        Try
            Call FillApplicantQualifications(True)
            ShowMessage(lblUpdateMsg.Text, MessageType.Info)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvQualification_ItemDeleting(sender As Object, e As ListViewDeleteEventArgs) Handles lvQualification.ItemDeleting
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            objodsQuali.DeleteParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            objodsQuali.DeleteParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            objodsQuali.DeleteParameters.Item("intApplicantUnkid").DefaultValue = CInt(Session("applicantunkid")).ToString
            objodsQuali.DeleteParameters.Item("intQualificationTranUnkid").DefaultValue = CInt(e.Keys("qualificationtranunkid")).ToString
            'If objAppQuali.DeleteApplicantQualification(strCompCode:=Session("CompCode").ToString _
            '                                             , intComUnkID:=CInt(Session("companyunkid")) _
            '                                             , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            '                                             , intQualificationTranUnkid:=CInt(e.Keys("qualificationtranunkid"))
            '                                             ) = True Then

            '    ShowMessage(lblDeleteMsg.Text, MessageType.Info)
            '    lvQualification.DataBind()
            '    'Call FillApplicantQualifications()
            'End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsQuali_Deleted(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles objodsQuali.Deleted
        Try
            Call FillApplicantQualifications(True)
            ShowMessage(lblDeleteMsg.Text, MessageType.Info)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub gvQualiCerti_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvQualiCerti.RowCommand
        Try
            If e.CommandName = "Remove" Then

                Dim strFile As String = gvQualiCerti.DataKeys(CInt(e.CommandArgument)).Item("filepath").ToString ' dsAttach.Tables(0).Rows(CInt(e.CommandArgument)).Item("filepath").ToString
                'dsAttach.Tables(0).Rows(CInt(e.CommandArgument)).Item("AUD") = "D"
                'dsAttach.Tables(0).AcceptChanges()
                If objAppQuali.DeleteApplicantAttachment(strCompCode:=Session("CompCode").ToString _
                                                         , intComUnkID:=CInt(Session("companyunkid")) _
                                                         , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                         , intAttachfiletranUnkid:=CInt(gvQualiCerti.DataKeys(CInt(e.CommandArgument)).Item("attachfiletranunkid"))
                                                         ) = True Then

                    ShowMessage(lblAttachDeleteMsg.Text, MessageType.Info)

                    Call FillApplicantAttachments(True)

                End If
                'gvQualiCerti.DataSource = dsAttach.Tables(0)
                'gvQualiCerti.DataBind()

                'File.Delete(strFile)

                'If dsAttach.Tables(0).Select(" AUD <> 'D' ").Length >= 3 Then
                '    flUpload.Enabled = False
                'Else
                '    flUpload.Enabled = True
                'End If

            ElseIf e.CommandName = "Download" Then

                Dim strFile As String = gvQualiCerti.DataKeys(CInt(e.CommandArgument)).Item("filepath").ToString 'dsAttach.Tables(0).Rows(CInt(e.CommandArgument)).Item("filepath").ToString
                Dim r As GridViewRow = gvQualiCerti.Rows(CInt(e.CommandArgument))
                Response.ContentType = "image/jpg/pdf/doc"
                Response.AddHeader("Content-Disposition", "attachment;filename=""" & DirectCast(r.FindControl("objlblFilename"), Label).Text & """")
                'Response.AddHeader("Content-Length", 1024)
                Response.Clear()
                'Response.ContentType = "application/octet-stream"
                'Response.TransmitFile(Server.MapPath("~/" & filePath))
                'Response.TransmitFile(Server.MapPath(strFile))
                Response.TransmitFile(strFile)
                'Response.WriteFile(Server.MapPath("~/" & System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath") & "/DEMO_eb8c6c40-52ce-42b2-92d2-5c8966f69594.jpg"))
                'Response.TransmitFile(Server.MapPath("~/" & System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath") & "/DEMO_eb8c6c40-52ce-42b2-92d2-5c8966f69594.jpg").Replace("\", "/"))
                'Response.[End]()
                'Dim req As WebClient = New WebClient()
                'Dim data As Byte() = req.DownloadData(strFile)
                'Response.BinaryWrite(data)
                'Response.End()
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                Call FillApplicantAttachments()
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub gvQualiCerti_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvQualiCerti.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim s As New ScriptManager
                s.RegisterPostBackControl(e.Row.FindControl("lnkDownload"))
                'ScriptManager.RegisterPostBackControl(e.Row.FindControl("lnkDownload"))
            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " Checkbox Events "
    'Private Sub chkOtherQuali_CheckedChanged(sender As Object, e As EventArgs) Handles chkOtherQuali.CheckedChanged
    '    Try
    '        If chkOtherQuali.Checked = True Then
    '            ddlAwardgp.SelectedValue = "0"
    '            Call ddlAwardgp_SelectedIndexChanged(ddlAwardgp, New System.EventArgs)
    '            ddlAward.SelectedValue = "0"
    '            Call ddlAward_SelectedIndexChanged(ddlAward, New System.EventArgs)
    '            ddlResultCode.SelectedValue = "0"
    '        Else
    '            txtOtherQualiGroup.Text = ""
    '            txtOtherQualification.Text = ""
    '            txtOtherResultCode.Text = ""
    '        End If
    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    Protected Sub chkOther_CheckedChanged(sender As Object, e As EventArgs) Handles chkqualificationgroup.CheckedChanged, chkqualificationAward.CheckedChanged, chkResultCode.CheckedChanged, chkInstitute.CheckedChanged
        Try
            If CType(sender, CheckBox).ID = chkqualificationgroup.ID Then
                If ddlAwardgp.Visible = False Then
                    ddlAwardgp.SelectedValue = "0"
                End If
                pnlQualficationGrp.Visible = Not chkqualificationgroup.Checked
                pnlOtherQualficationGrp.Visible = chkqualificationgroup.Checked
                If pnlOtherQualficationGrp.Visible = False Then
                    txtOtherQualiGroup.Text = ""
                ElseIf pnlQualficationGrp.Visible = False Then
                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'If ddlAwardgp.Items.Count > 0 Then ddlAwardgp.SelectedIndex = 0
                    'Call ddlAwardgp_SelectedIndexChanged(sender, New EventArgs)
                    'Sohail (13 May 2022) -- End
                End If

            ElseIf CType(sender, CheckBox).ID = chkqualificationAward.ID Then
                If ddlAward.Visible = False Then
                    ddlAward.SelectedValue = "0"
                End If
                pnlQualficationAward.Visible = Not chkqualificationAward.Checked
                pnlOtherQualficationAward.Visible = chkqualificationAward.Checked
                If pnlOtherQualficationAward.Visible = False Then
                    txtOtherQualification.Text = ""
                ElseIf pnlQualficationAward.Visible = False Then
                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'ddlAward.SelectedIndex = 0
                    'Call ddlAward_SelectedIndexChanged(ddlAward, New System.EventArgs)
                    'Sohail (13 May 2022) -- End
                End If
            ElseIf CType(sender, CheckBox).ID = chkResultCode.ID Then
                If ddlResultCode.Visible = False Then
                    ddlResultCode.SelectedValue = "0"
                End If
                pnlResultCode.Visible = Not chkResultCode.Checked
                pnlOtherResultCode.Visible = chkResultCode.Checked

                If pnlOtherQualficationAward.Visible = False Then
                    txtOtherResultCode.Text = ""
                ElseIf pnlResultCode.Visible = False Then
                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'ddlResultCode.SelectedIndex = 0
                    'Sohail (13 May 2022) -- End
                End If
            ElseIf CType(sender, CheckBox).ID = chkInstitute.ID Then
                If drpInstitute.Visible = False Then
                    drpInstitute.SelectedValue = "0"
                End If
                pnlInstitute.Visible = Not chkInstitute.Checked
                pnlOtherInstitute.Visible = chkInstitute.Checked
                If pnlOtherQualficationAward.Visible = False Then
                    txtOtherInstitution.Text = ""
                ElseIf pnlInstitute.Visible = False Then
                    'Sohail (13 May 2022) -- Start
                    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                    'drpInstitute.SelectedIndex = 0
                    'Sohail (13 May 2022) -- End
                End If
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Protected Sub chkGridOther_CheckedChanged(sender As Object, e As EventArgs)
    '    Try
    '        Dim chk As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = DirectCast(chk.NamingContainer, GridViewRow)
    '        Dim objApplicant As New clsApplicant
    '        Dim dsCombo As DataSet

    '        Dim drpQualificationGrp As DropDownList = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objddlQualiGrp"), DropDownList)
    '        Dim txtQualificationGrp As TextBox = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objtxtQualiGrp"), TextBox)

    '        Dim drpQualification As DropDownList = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objddlQuali"), DropDownList)
    '        Dim txtQualification As TextBox = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objtxtQuali"), TextBox)

    '        Dim ddlQualiResult As DropDownList = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objddlQualiResult"), DropDownList)
    '        Dim txtQualiResult As TextBox = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objtxtQualiResult"), TextBox)

    '        Dim ddlQualiInst As DropDownList = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objddlQualiInst"), DropDownList)
    '        Dim txtQualiInst As TextBox = CType(grdQualification.Rows(gvRow.RowIndex).FindControl("objtxtQualiInst"), TextBox)

    '        If chk.ID.ToUpper() = "OBJCHKQUALIGRP" Then
    '            drpQualificationGrp.Visible = Not chk.Checked
    '            txtQualificationGrp.Visible = chk.Checked

    '            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
    '            With drpQualificationGrp
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "Name"
    '                .DataSource = dsCombo.Tables(0)
    '                .DataBind()
    '                drpQualificationGrp.SelectedValue = "0"
    '            End With

    '            Call objddlAwardgp_SelectedIndexChanged(drpQualificationGrp, New EventArgs)

    '        ElseIf chk.ID.ToUpper() = "OBJCHKQUALIFICATION" Then
    '            drpQualification.Visible = Not chk.Checked
    '            txtQualification.Visible = chk.Checked

    '            Dim QualificationGrp As Integer = 0
    '            If drpQualificationGrp.SelectedValue <> "" Then
    '                QualificationGrp = CInt(drpQualificationGrp.SelectedValue)
    '            End If

    '            dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), If(drpQualificationGrp.SelectedValue <> "", CInt(drpQualificationGrp.SelectedValue), 0))
    '            Dim dp As DropDownList = DirectCast(grdQualification.Rows(grdQualification.EditIndex).FindControl("objddlQuali"), DropDownList)
    '            With dp
    '                .DataValueField = "qualificationunkid"
    '                .DataTextField = "Q_name"
    '                .DataSource = dsCombo.Tables(0)
    '                .DataBind()
    '                .SelectedValue = "0"
    '                Call objddlAward_SelectedIndexChanged(dp, New System.EventArgs)
    '            End With

    '        ElseIf chk.ID.ToUpper() = "OBJCHKQUALIFICATIONRESULT" Then
    '            ddlQualiResult.Visible = Not chk.Checked
    '            txtQualiResult.Visible = chk.Checked



    '            dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), If(drpQualification.SelectedValue <> "", CInt(drpQualification.SelectedValue), 0))
    '            With ddlQualiResult
    '                .DataValueField = "resultunkid"
    '                .DataTextField = "resultname"
    '                .DataSource = dsCombo.Tables(0)
    '                .DataBind()
    '            End With

    '        ElseIf chk.ID.ToUpper() = "OBJCHKQUALIFICATIONINST" Then
    '            ddlQualiInst.Visible = Not chk.Checked
    '            txtQualiInst.Visible = chk.Checked

    '            dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))
    '            With ddlQualiInst
    '                .DataValueField = "instituteunkid"
    '                .DataTextField = "institute_name"
    '                .DataSource = dsCombo.Tables(0)
    '                .DataBind()
    '            End With

    '        End If

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Protected Sub chkListViewOther_CheckedChanged(sender As Object, e As EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim gvRow As ListViewItem = DirectCast(chk.NamingContainer, ListViewItem)
            Dim objApplicant As New clsApplicant
            Dim dsCombo As DataSet

            Dim drpQualificationGrp As DropDownList = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objddllvQualiGrp"), DropDownList)
            Dim txtQualificationGrp As TextBox = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objtxtlvQualiGrp"), TextBox)
            Dim btnlvHideOtherQualiGrp As HtmlButton = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("btnlvHideOtherQualiGrp"), HtmlButton) 'Sohail (13 May 2022)

            Dim drpQualification As DropDownList = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objddllvQuali"), DropDownList)
            Dim txtQualification As TextBox = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objtxtlvQuali"), TextBox)
            Dim btnlvHideOtherQuali As HtmlButton = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("btnlvHideOtherQuali"), HtmlButton) 'Sohail (13 May 2022)

            Dim ddlQualiResult As DropDownList = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objddllvQualiResult"), DropDownList)
            Dim txtQualiResult As TextBox = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objtxtlvQualiResult"), TextBox)
            Dim btnlvHideOtherResult As HtmlButton = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("btnlvHideOtherResult"), HtmlButton) 'Sohail (13 May 2022)

            Dim ddlQualiInst As DropDownList = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objddllvQualiInst"), DropDownList)
            Dim txtQualiInst As TextBox = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("objtxtlvQualiInst"), TextBox)
            Dim btnlvHideOtherInst As HtmlButton = CType(lvQualification.Items(gvRow.DataItemIndex).FindControl("btnlvHideOtherInst"), HtmlButton) 'Sohail (13 May 2022)

            If chk.ID.ToUpper() = "OBJCHKLVQUALIGRP" Then
                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                btnlvHideOtherQualiGrp.Visible = chk.Checked
                If drpQualificationGrp.Visible = False Then
                    drpQualificationGrp.SelectedValue = "0"
                End If
                'Sohail (13 May 2022) -- End
                drpQualificationGrp.Visible = Not chk.Checked
                txtQualificationGrp.Visible = chk.Checked

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                'dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
                'With drpQualificationGrp
                '    .DataValueField = "masterunkid"
                '    .DataTextField = "Name"
                '    .DataSource = dsCombo.Tables(0)
                '    .DataBind()
                '    drpQualificationGrp.SelectedValue = "0"
                'End With
                'Sohail (13 May 2022) -- End

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                'Call objddllvAwardgp_SelectedIndexChanged(drpQualificationGrp, New EventArgs)
                'Sohail (13 May 2022) -- End

            ElseIf chk.ID.ToUpper() = "OBJCHKLVQUALIFICATION" Then
                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                btnlvHideOtherQuali.Visible = chk.Checked
                If drpQualification.Visible = False Then
                    drpQualification.SelectedValue = "0"
                End If
                'Sohail (13 May 2022) -- End
                drpQualification.Visible = Not chk.Checked
                txtQualification.Visible = chk.Checked

                Dim QualificationGrp As Integer = 0
                If drpQualificationGrp.SelectedValue <> "" Then
                    QualificationGrp = CInt(drpQualificationGrp.SelectedValue)
                End If

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                'dsCombo = objAppQuali.GetQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), If(drpQualificationGrp.SelectedValue <> "", CInt(drpQualificationGrp.SelectedValue), 0))
                'Dim dp As DropDownList = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objddllvQuali"), DropDownList)
                'With dp
                '    .DataValueField = "qualificationunkid"
                '    .DataTextField = "Q_name"
                '    .DataSource = dsCombo.Tables(0)
                '    .DataBind()
                '    .SelectedValue = "0"
                '    Call objddllvAward_SelectedIndexChanged(dp, New System.EventArgs)
                'End With
                'Sohail (13 May 2022) -- End

            ElseIf chk.ID.ToUpper() = "OBJCHKLVQUALIFICATIONRESULT" Then
                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                btnlvHideOtherResult.Visible = chk.Checked
                If ddlQualiResult.Visible = False Then
                    ddlQualiResult.SelectedValue = "0"
                End If
                'Sohail (13 May 2022) -- End
                ddlQualiResult.Visible = Not chk.Checked
                txtQualiResult.Visible = chk.Checked

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                'dsCombo = objAppQuali.GetResultCodes(Session("CompCode").ToString, CInt(Session("companyunkid")), If(drpQualification.SelectedValue <> "", CInt(drpQualification.SelectedValue), 0))
                'With ddlQualiResult
                '    .DataValueField = "resultunkid"
                '    .DataTextField = "resultname"
                '    .DataSource = dsCombo.Tables(0)
                '    .DataBind()
                'End With
                'Sohail (13 May 2022) -- End

            ElseIf chk.ID.ToUpper() = "OBJCHKLVQUALIFICATIONINST" Then
                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                btnlvHideOtherInst.Visible = chk.Checked
                If ddlQualiInst.Visible = False Then
                    ddlQualiInst.SelectedValue = "0"
                End If
                'Sohail (13 May 2022) -- End
                ddlQualiInst.Visible = Not chk.Checked
                txtQualiInst.Visible = chk.Checked

                'Sohail (13 May 2022) -- Start
                'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
                'dsCombo = objAppQuali.GetInstitute(Session("CompCode").ToString, CInt(Session("companyunkid")))
                'With ddlQualiInst
                '    .DataValueField = "instituteunkid"
                '    .DataTextField = "institute_name"
                '    .DataSource = dsCombo.Tables(0)
                '    .DataBind()
                'End With
                'Sohail (13 May 2022) -- End

            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Button Events "
    Private Sub btnAddQuali_Click(sender As Object, e As EventArgs) Handles btnAddQuali.Click
        'lblError.Visible = False
        Dim decGPA As Decimal
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            If objAppQuali.IsExistApplicantQualification(strCompCode:=Session("CompCode").ToString _
                                                         , intComUnkID:=CInt(Session("companyunkid")) _
                                                         , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                         , intQualificationTranUnkid:=-1 _
                                                         , intQualificationGroupUnkid:=CInt(ddlAwardgp.SelectedValue) _
                                                         , intQualificationUnkid:=CInt(ddlAward.SelectedValue) _
                                                         , strOther_Qualificationgrp:=txtOtherQualiGroup.Text.Trim _
                                                         , strOther_Qualification:=txtOtherQualification.Text.Trim
                                                         ) = True Then

                ShowMessage(lblExistMsg.Text, MessageType.Info)
                Call FillApplicantQualifications()
                ddlAward.Focus()
                Exit Try
            End If

            Decimal.TryParse(txtGPAcode.Text, decGPA)


            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".

            Dim dtoAttach As dtoAddAppAttach = Nothing
            Dim lstAttach As List(Of dtoAddAppAttach) = Nothing

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then

                If CBool(Session("isQualiCertAttachMandatory")) Then

                    '* START IF ANY APPLICANT ATTACH BIRTH CERTIFICATE AGAIN THEN PREVIOUS ONE WILL BE DELETED AND NEW WILL INSERT


                    Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
                    Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & lblFileNameQualificationAttachment.Text.Substring(lblFileNameQualificationAttachment.Text.LastIndexOf(".") + 1)
                    Dim decSize As Decimal = 0
                    Decimal.TryParse(hfDocSizeQualificationAttachment.Value, decSize)

                    dtoAttach = New dtoAddAppAttach
                    lstAttach = New List(Of dtoAddAppAttach)

                    With dtoAttach
                        .CompCode = Session("CompCode").ToString
                        .ComUnkID = CInt(Session("companyunkid"))
                        .ApplicantUnkid = CInt(Session("applicantunkid"))
                        .DocumentUnkid = CInt(ddlQualificationAttachType.SelectedValue)
                        .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                        .AttachrefId = CInt(enScanAttactRefId.QUALIFICATIONS)
                        .FolderName = strFolder
                        .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                        .Filename = lblFileNameQualificationAttachment.Text
                        .Fileuniquename = strUnkImgName
                        .File_size = CInt(decSize / 1024)
                        .Attached_Date = Now
                        .flUpload = CType(Session(flQualificationAttachment.ClientID), HttpPostedFile)
                    End With

                    lstAttach.Add(dtoAttach)

                    'Pinkal (30-Sep-2023) -- Start
                    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                    Dim hpf As HttpPostedFile
                    If System.IO.Directory.Exists(Server.MapPath("~") & "\" & dtoAttach.FolderName) = False Then
                        System.IO.Directory.CreateDirectory(Server.MapPath("~") & "\" & dtoAttach.FolderName)
                    End If
                    hpf = dtoAttach.flUpload
                    hpf.SaveAs(Server.MapPath("~") & "\" & dtoAttach.FolderName & "\" & dtoAttach.Fileuniquename)
                    'Pinkal (30-Sep-2023) -- End

                End If   '  If CBool(Session("isQualiCertAttachMandatory")) Then

            End If  'If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then


            If objAppQuali.AddApplicantQualification(strCompCode:=Session("CompCode").ToString _
                                                     , intComUnkID:=CInt(Session("companyunkid")) _
                                                     , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                     , intQualificationGroupUnkid:=CInt(ddlAwardgp.SelectedValue) _
                                                     , intQualificationUnkid:=CInt(ddlAward.SelectedValue) _
                                                     , intResultunkid:=CInt(ddlResultCode.SelectedValue) _
                                                     , intInstituteunkid:=CInt(drpInstitute.SelectedValue) _
                                                     , dtAward_start_Date:=dtAwardDate.GetDate _
                                                     , dtAward_end_Date:=dtdateto.GetDate _
                                                     , dtTransaction_Date:=Now _
                                                     , decGPACode:=decGPA _
                                                     , strOther_Qualificationgrp:=txtOtherQualiGroup.Text.Trim _
                                                     , strOther_Qualification:=txtOtherQualification.Text.Trim _
                                                     , strOther_Resultcode:=txtOtherResultCode.Text.Trim _
                                                     , strOther_Institute:=txtOtherInstitution.Text.Trim _
                                                     , strCertificateno:=txtCertiNo.Text.Trim _
                                                     , strReference_no:="" _
                                                     , strRemark:=txtQualiremark.Text.Trim _
                                                     , blnHighestQualification:=chkHighestqualification.Checked _
                                                     , dtCreated_date:=Now _
                                                     , objAddAppAttach:=lstAttach
                                                     ) = True Then

                'Pinkal (30-Sep-2023) --  (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal..[ , lstAttach]

                ShowMessage(lblSaveMsg.Text, MessageType.Info)



            End If

            Call FillApplicantQualifications(True)
            Call Reset_Qualification()
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub flUpload_btnUpload_Click(sender As Object, e As EventArgs) Handles flUpload.btnUpload_Click
        Try
            If flUpload.HasFile = False Then Exit Try

            'mblnFromPreview = True 'To ignore job history validation and submit application
            'If Entryvalid(1) = False Then Exit Try
            'mblnFromPreview = False

            If CInt(ddlDocTypeQuali.SelectedValue) <= 0 Then
                ShowMessage(lblDocTypeQualiMsg.Text, MessageType.Info)
                Exit Sub
            End If

            If objAppQuali.IsExistApplicantAttachment(strCompCode:=Session("CompCode").ToString _
                                                     , intComUnkID:=CInt(Session("companyunkid")) _
                                                     , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                     , intAttachfiletranUnkid:=-1 _
                                                     , intModuleRefId:=CInt(enImg_Email_RefId.Applicant_Module) _
                                                     , intAttachRefId:=CInt(enScanAttactRefId.QUALIFICATIONS) _
                                                     , strFilename:=flUpload.FileName
                                                     ) = True Then

                ShowMessage(lblAttachExistMsg.Text, MessageType.Info)
                Exit Sub

            End If
            'Dim dtRow As DataRow()
            'dtRow = dsAttach.Tables(0).Select("filename = '" & flUpload.FileName & "'" & " AND AUD <> 'D' ")

            'If dtRow.Length > 0 Then
            '    DisplayMessage.DisplayMessage("Selected file with same name is already added to the list.", Me)
            '    Exit Sub
            'End If

            'dtRow = dsAttach.Tables(0).Select(" AUD <> 'D' ")

            'Dim dsList As DataSet = objAppQuali.GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            'Sohail (21 Jul 2017) - Start
            'Enhancement - Now dont allow to attach total size of files more than 3 MB.
            'If dsList.Tables(0).Rows.Count >= 3 Then
            '    ShowMessage("Sorry, You can attach maximum 3 attachments only.", MessageType.Info)
            '    Exit Sub
            'End If
            'Sohail (21 Jul 2017) - End
            'If dtRow.Length >= 3 Then
            '    DisplayMessage.DisplayMessage("Sorry you can attach maximum 3 attachments only.", Me)
            '    Exit Sub
            'End If

            Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG"
                Case "JPEG"
                Case "BMP"
                Case "GIF"
                Case "PNG"
                Case "PDF"
                Case Else
                    ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                    Exit Try
            End Select

            'Sohail (21 Jul 2017) - Start
            'Enhancement - Now dont allow to attach total size of files more than 3 MB.
            'Dim mintByes As Integer = 1048576 '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            Dim mintByes As Integer = CInt(flUpload.MaxSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            'Sohail (21 Jul 2017) - End
            If flUpload.FileBytes.LongLength > mintByes Then   ' 6 MB = 6291456 Bytes
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            Dim strUnkImgName As String
            Dim strFilename As String = flUpload.FileName

            'If txtApplicatunkid.Text = "" Then
            If flUpload.PostedFile.FileName <> "" Then
                strUnkImgName = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & Split(flUpload.FileName, ".")(1)
                Dim Str As String = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
                If IO.Directory.Exists(Server.MapPath("~") & "\" & Str) = False Then
                    IO.Directory.CreateDirectory(Server.MapPath("~") & "\" & Str)
                End If
                Str &= "\" & strUnkImgName

                flUpload.SaveAs(Server.MapPath("~") & "\" & Str)

                If objAppQuali.AddApplicantAttachment(strCompCode:=Session("CompCode").ToString _
                                                     , intComUnkID:=CInt(Session("companyunkid")) _
                                                     , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                     , intDocumentUnkid:=CInt(ddlDocTypeQuali.SelectedValue) _
                                                     , intModulerefId:=CInt(enImg_Email_RefId.Applicant_Module) _
                                                     , intAttachrefId:=CInt(enScanAttactRefId.QUALIFICATIONS) _
                                                     , strFilepath:=Server.MapPath("~") & "\" & Str _
                                                     , strFilename:=flUpload.FileName _
                                                     , strFileuniquename:=strUnkImgName _
                                                     , intFile_size:=CInt(flUpload.FileBytes.LongLength / 1024) _
                                                     , dtAttached_Date:=DateAndTime.Now
                                                     ) = True Then
                    'intModulerefId:=2 = enImg_Email_RefId.Applicant_Module
                    'tAttachrefId:=3 = enScanAttactRefId.QUALIFICATIONS

                    ShowMessage(lblAttachSaveMsg.Text, MessageType.Info)

                End If
                'Dim dtIdRow As DataRow = dsAttach.Tables(0).NewRow

                'dtIdRow.Item("attachfiletranunkid") = -1
                'dtIdRow.Item("applicantunkid") = mintApplicantunkid
                'dtIdRow.Item("documentunkid") = CInt(ddlDocTypeQuali.SelectedValue)
                'dtIdRow.Item("modulerefid") = 2 '*** enImg_Email_RefId.Applicant_Module
                'dtIdRow.Item("attachrefid") = 3 '*** enScanAttactRefId.QUALIFICATIONS
                'dtIdRow.Item("filepath") = Server.MapPath(Str)
                'dtIdRow.Item("filename") = flUpload.FileName
                'dtIdRow.Item("fileuniquename") = strUnkImgName
                'dtIdRow.Item("file_size") = (flUpload.FileBytes.LongLength / 1024)
                'dtIdRow.Item("attached_date") = DateAndTime.Now
                'dtIdRow.Item("Comp_Code") = Session("CompCode")
                'dtIdRow.Item("companyunkid") = Session("companyunkid")
                'dtIdRow.Item("Syncdatetime") = DBNull.Value
                'dtIdRow.Item("localfilepath") = flUpload.PostedFile.FileName
                'dtIdRow.Item("GUID") = Guid.NewGuid
                'dtIdRow.Item("AUD") = "A"

                'dsAttach.Tables(0).Rows.Add(dtIdRow)

                Call FillApplicantAttachments(True)

            Else

            End If

            'Else
            '    If flUpload.PostedFile.FileName <> "" Then
            '        Str = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath") & "/" & flUpload.FileName
            '        flUpload.SaveAs(Server.MapPath(Str))
            '    Else

            '    End If
            'End If

            'Sohail (21 Jul 2017) - Start
            'Enhancement - Now dont allow to attach total size of files more than 3 MB.
            'If dsList.Tables(0).Rows.Count >= 3 Then
            '    flUpload.Enabled = False
            'Else
            '    flUpload.Enabled = True
            'End If
            'Sohail (21 Jul 2017) - End
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub


    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
    Protected Sub flQualificationAttachment_btnUpload_Click(sender As Object, e As EventArgs)
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())


            If flQualificationAttachment.HasFile = False Then Exit Try

            If CInt(ddlQualificationAttachType.SelectedValue) <= 0 Then
                ShowMessage(LblQualificationAttachType.Text, MessageType.Info)
                ddlQualificationAttachType.Focus()
                Exit Sub
            End If

            Dim blnInvalidFile As Boolean = False
            Select Case flQualificationAttachment.FileName.Substring(flQualificationAttachment.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG", "JPEG", "BMP", "GIF", "PNG"
                    If flQualificationAttachment.AllowImageFile = False Then
                        blnInvalidFile = True
                    End If
                Case "DOC", "DOCX"
                    If flQualificationAttachment.AllowDocumentFile = False Then
                        blnInvalidFile = True
                    End If
                Case "PDF"
                Case Else
                    blnInvalidFile = True
            End Select

            If blnInvalidFile = True Then
                ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                Exit Try
            End If

            Dim mintByes As Integer = CInt(flQualificationAttachment.RemainingSizeKB)
            If flQualificationAttachment.FileBytes.LongLength > mintByes Then
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            lblFileNameQualificationAttachment.Text = flQualificationAttachment.PostedFile.FileName
            lblFileExtQualificationAttachment.Text = flQualificationAttachment.FileName.Substring(flQualificationAttachment.FileName.LastIndexOf(".") + 1).ToUpper
            lblDocSizeQualificationAttachment.Text = Format(((flQualificationAttachment.FileBytes.LongLength / 1024) / 1024), "0.00") & " MB."
            hfDocSizeQualificationAttachment.Value = flQualificationAttachment.FileBytes.LongLength.ToString()
            hfFileNameQualificationAttachment.Value = lblFileNameQualificationAttachment.Text
            pnlFileQualificationAttachment.Visible = True
            Session(flQualificationAttachment.ClientID) = flQualificationAttachment.PostedFile

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Pinkal (30-Sep-2023) -- End


    Private Sub gvQualiCerti_DataBound(sender As Object, e As EventArgs) Handles gvQualiCerti.DataBound
        Try
            If gvQualiCerti.DataSource IsNot Nothing Then
                Dim dt As DataTable = CType(gvQualiCerti.DataSource, DataTable)
                Dim intTotKB As Integer = (From p In dt.AsEnumerable() Select (p.Field(Of Integer)("file_size"))).Sum()
                flUpload.MaxSizeKB = (((1024 * 3) - intTotKB) * 1024).ToString()
                objlblTotSize.Text = "(" & Format((intTotKB / 1024), "0.00") & " MB / 3 MB) Used."
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Private ReadOnly Property SuppliersSelectedIndex() As Integer
    '    Get
    '        If String.IsNullOrEmpty(Request.Form("SuppliersGroup")) Then
    '            Return -1
    '        Else
    '            Return Convert.ToInt32(Request.Form("SuppliersGroup"))
    '        End If
    '    End Get
    'End Property

    'Private Sub grdQualification_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles grdQualification.RowCreated
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim output As Literal = CType(e.Row.FindControl("RadioButtonMarkup"), Literal)
    '            output.Text = String.Format("<input type='radio' name='SuppliersGroup' id='RowSelector{0}' value='{0}' ", e.Row.RowIndex)

    '            'output.Text = String.Format("<input type='radio' name='SuppliersGroup' id='RowSelector{0}' value='{0}'", e.Row.RowIndex)
    '            '// See if we need to add the "checked" attribute
    '            If SuppliersSelectedIndex = e.Row.RowIndex OrElse (IsPostBack = False AndAlso e.Row.RowIndex = 0) Then
    '                output.Text += " checked='checked'"
    '            End If

    '            '// Add the closing tag
    '            output.Text += " />"

    '            End If


    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub

    'Sohail (16 Aug 2019) -- End

    'Sohail (13 May 2022) -- Start
    'Enhancement : AC2-311 : ZRA - As a user, on the qualifications page, I want the ‘Others’ check boxes to be defined as part of the drop-down menus so that when I choose it as an option, system should provide me with the free text boxes to type in content.
    Private Sub btnHideOtherQualiGrp_ServerClick(sender As Object, e As EventArgs) Handles btnHideOtherQualiGrp.ServerClick, btnHideOtherQuali.ServerClick, btnHideOtherResultCode.ServerClick, btnHideOtherInstitute.ServerClick
        Try

            If CType(sender, HtmlButton).ID = btnHideOtherQualiGrp.ID Then
                chkqualificationgroup.Checked = False
                Call chkOther_CheckedChanged(chkqualificationgroup, New EventArgs)

            ElseIf CType(sender, HtmlButton).ID = btnHideOtherQuali.ID Then
                chkqualificationAward.Checked = False
                Call chkOther_CheckedChanged(chkqualificationAward, New EventArgs)

            ElseIf CType(sender, HtmlButton).ID = btnHideOtherResultCode.ID Then
                chkResultCode.Checked = False
                Call chkOther_CheckedChanged(chkResultCode, New EventArgs)

            ElseIf CType(sender, HtmlButton).ID = btnHideOtherInstitute.ID Then
                chkInstitute.Checked = False
                Call chkOther_CheckedChanged(chkInstitute, New EventArgs)

            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnlvHideOtherQualiGrp_ServerClick(sender As Object, e As EventArgs)
        Try
            Dim objchklvQualiGrp As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualiGrp"), CheckBox)
            Dim objchklvQualification As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualification"), CheckBox)
            Dim objchklvQualificationResult As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualificationResult"), CheckBox)
            Dim objchklvQualificationInst As CheckBox = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("objchklvQualificationInst"), CheckBox)

            Dim btnlvHideOtherQualiGrp As HtmlButton = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("btnlvHideOtherQualiGrp"), HtmlButton)
            Dim btnlvHideOtherQuali As HtmlButton = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("btnlvHideOtherQuali"), HtmlButton)
            Dim btnlvHideOtherResult As HtmlButton = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("btnlvHideOtherResult"), HtmlButton)
            Dim btnlvHideOtherInst As HtmlButton = DirectCast(lvQualification.Items(lvQualification.EditIndex).FindControl("btnlvHideOtherInst"), HtmlButton)

            If CType(sender, HtmlButton).ID = btnlvHideOtherQualiGrp.ID Then
                objchklvQualiGrp.Checked = False
                Call chkListViewOther_CheckedChanged(objchklvQualiGrp, New EventArgs)

            ElseIf CType(sender, HtmlButton).ID = btnlvHideOtherQuali.ID Then
                objchklvQualification.Checked = False
                Call chkListViewOther_CheckedChanged(objchklvQualification, New EventArgs)

            ElseIf CType(sender, HtmlButton).ID = btnlvHideOtherResult.ID Then
                objchklvQualificationResult.Checked = False
                Call chkListViewOther_CheckedChanged(objchklvQualificationResult, New EventArgs)

            ElseIf CType(sender, HtmlButton).ID = btnlvHideOtherInst.ID Then
                objchklvQualificationInst.Checked = False
                Call chkListViewOther_CheckedChanged(objchklvQualificationInst, New EventArgs)

            End If

        Catch ex As Exception

        End Try
    End Sub
    'Sohail (13 May 2022) -- End



#End Region

End Class