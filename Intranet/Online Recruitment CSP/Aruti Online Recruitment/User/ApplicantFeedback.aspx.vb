﻿Option Strict On

Imports System.Web.Security.AntiXss

Public Class ApplicantFeedback
    Inherits Base_Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objExperience As New clsApplicantExperience
        Dim dsFeedback As DataSet
        Dim objAppliedJob As New clsApplicantApplyJob

        Try

            If Session("email") Is Nothing OrElse Session("CompCode") Is Nothing OrElse Session("companyunkid") Is Nothing _
                OrElse Session("email").ToString() = "" OrElse Session("CompCode").ToString = "" OrElse CInt(Session("companyunkid")) <= 0 Then
                pnlBlank.Visible = True
                pnlFeedback.Visible = False
                Exit Try

            Else
                pnlBlank.Visible = False
                pnlFeedback.Visible = True
            End If

            If IsPostBack = False Then
                '***  Appliying Language ***
                Call SetLanguage()
                '***************************

                Dim wLang As New clsWebLanguage
                Dim langid As Integer = 0
                Try
                    langid = Convert.ToInt32(HttpContext.Current.Session("LangId"))
                Catch
                    langid = 0
                End Try


                dsFeedback = objAppliedJob.GetApplicantFeedbackVacancyWise(strCompCode:=Session("CompCode").ToString,
                                                                     intComUnkID:=CInt(Session("companyunkid")),
                                                                     intVacancyId:=CInt(Session("FeedbackVacancyID")),
                                                                     mstrApplicantEmail:=Session("email").ToString())

                dlFeedback.DataSource = dsFeedback.Tables(0)
                dlFeedback.DataBind()
                If dsFeedback.Tables(0).Rows.Count <= 0 Then
                    divFeedback.Visible = False
                Else
                    Dim lstExp As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantFeedback", intLanguageID:=0)
                    If lstExp IsNot Nothing Then
                        Call SetCaptions(divFeedback, lstExp, langid)
                    End If
                End If
                    '*********************************************************
                End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objExperience = Nothing
            objAppliedJob = Nothing
        End Try
    End Sub

    Private Sub SetLanguage()
        Dim wLang As New clsWebLanguage
        Dim langid As Integer = 0
        Try
            Try
                langid = Convert.ToInt32(HttpContext.Current.Session("LangId"))
            Catch
                langid = 0
            End Try

            Dim lstExp As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantFeedback", intLanguageID:=0)
            If lstExp IsNot Nothing Then
                Call SetCaptions(divFeedback, lstExp, langid)
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SetCaptions(pnl As Panel, list As List(Of clsWebLanguage), langid As Integer)

        Try

            For Each ctrl As Control In pnl.Controls
                If ctrl.GetType() = GetType(Panel) Then Call SetCaptions(CType(ctrl, Panel), list, langid)
                'Dim w As clsWebLanguage = list.Find(Function(x) x._controlunqid = ctrl.ID)
                Dim w As clsWebLanguage = Nothing
                'If w IsNot Nothing Then
                If ctrl.GetType() = GetType(Label) Then
                    w = list.Find(Function(x) x._controlunqid = ctrl.ID)

                    If w IsNot Nothing Then

                        If CType(ctrl, Label).Text = "" Then Continue For 'Don't apply language is label is set as blank from code side

                        Select Case langid
                            Case 1
                                CType(ctrl, Label).Text = w._language1
                            Case 2
                                CType(ctrl, Label).Text = w._language2
                            Case Else
                                CType(ctrl, Label).Text = w._language
                        End Select

                    End If

                ElseIf ctrl.GetType() = GetType(DataGrid) Then
                    For Each col As DataGridColumn In CType(ctrl, DataGrid).Columns
                        If col.FooterText = "" Then Continue For

                        w = list.Find(Function(x) x._controlunqid = col.FooterText)

                        If w IsNot Nothing Then

                            Select Case langid
                                Case 1
                                    CType(col, DataGridColumn).HeaderText = w._language1
                                Case 2
                                    CType(col, DataGridColumn).HeaderText = w._language2
                                Case Else
                                    CType(col, DataGridColumn).HeaderText = w._language
                            End Select

                        End If

                    Next

                ElseIf ctrl.GetType() = GetType(DataList) Then

                    If CType(ctrl, DataList).Items.Count > 0 Then
                        For Each itm As DataListItem In CType(ctrl, DataList).Items

                            For Each c As Control In itm.Controls
                                If c.GetType() = GetType(Panel) Then Call SetCaptions(CType(c, Panel), list, langid)

                                If c.GetType() = GetType(Label) Then

                                    w = list.Find(Function(x) x._controlunqid = c.ID)

                                    If w IsNot Nothing Then

                                        If CType(c, Label).Text = "" Then Continue For 'Don't apply language is label is set as blank from code side

                                        Select Case langid
                                            Case 1
                                                CType(c, Label).Text = w._language1
                                            Case 2
                                                CType(c, Label).Text = w._language2
                                            Case Else
                                                CType(c, Label).Text = w._language
                                        End Select

                                    End If

                                End If
                            Next
                        Next
                    End If

                End If

                'End If
            Next
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

End Class