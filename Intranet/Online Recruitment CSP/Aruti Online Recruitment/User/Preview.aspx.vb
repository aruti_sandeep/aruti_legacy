﻿Option Strict On

'Imports Microsoft.Security.Application
Imports System.Web.Security.AntiXss

Public Class Preview
    Inherits Base_Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objApplicant As New clsApplicant
        Dim objPersonalInfo As New clsPersonalInfo
        Dim dsPersonalInfo As DataSet
        Dim objExperience As New clsApplicantExperience
        Dim dsExperience As DataSet
        Dim objContactDetails As New clsContactDetails
        Dim dsContactDetails As DataSet
        Dim objOtherInfo As New clsOtherInfo
        Dim dsOtherInfo As DataSet
        Try

            'If Validation() = False Then Exit Sub
            Dim strMessage As String = ""
            If objApplicant.IsValidForApplyVacancy(Session("PreviewCompCode").ToString, CInt(Session("Previewcompanyunkid")), CInt(Session("PreviewApplicantUnkid")), False, strMessage) = False Then
                If strMessage.Trim <> "" Then
                    ShowMessage("Sorry, Your profile is incomplete. <BR> Please fill required details.")
                    'HttpContext.Current.Response.Redirect("~/User/UserHome.aspx", False)
                    pnlBlank.Visible = True
                    pnlPreview.Visible = False
                    lblMessage.Text = "<h4>Your Profile is incomplete.</h4><h5>Please fill following details to complete your profile.</h5><BR>" & strMessage
                End If
                Exit Sub
            End If

            If Session("PreviewApplicantUnkid") Is Nothing OrElse Session("PreviewCompCode") Is Nothing OrElse Session("Previewcompanyunkid") Is Nothing _
                OrElse CInt(Session("PreviewApplicantUnkid")) <= 0 OrElse Session("PreviewCompCode").ToString = "" OrElse CInt(Session("Previewcompanyunkid")) <= 0 Then

                pnlBlank.Visible = True
                pnlPreview.Visible = False
                Exit Try

            Else
                pnlBlank.Visible = False
                pnlPreview.Visible = True
            End If

            If IsPostBack = False Then
                Dim strTitle As String = ""
                Dim strName As String = ""
                Dim strEmail As String = ""
                Dim strGender As String = ""
                Dim strBirthDate As String = ""
                Dim strMaritalStatus As String = ""
                Dim strMarriedDate As String = ""
                Dim strNationality As String = ""
                Dim strLanguagesKnown As String = ""
                Dim strPresentAddress As String = ""
                Dim strPresentAddress2 As String = ""
                Dim strPermenantAddress As String = ""
                Dim strPermenantAddress2 As String = ""
                Dim strMemberships As String = ""
                Dim strAchievements As String = ""
                Dim strJournalResearchPaper As String = ""

                'Dim strExpCompanyName As String = ""
                'Dim strEmployer As String = ""
                'Dim strOPhone As String = ""
                'Dim strDesignation As String = ""
                'Dim strDuration As String = ""
                'Dim strResponsibility As String = ""
                'Dim strExpAchievement As String = ""
                'Dim strLeavingReason As String = ""

                '***  Appliying Language ***
                Call SetLanguage()
                '***************************

                Dim wLang As New clsWebLanguage
                Dim langid As Integer = 0
                Try
                    langid = Convert.ToInt32(HttpContext.Current.Session("LangId"))
                Catch
                    langid = 0
                End Try


                Dim dsTitle As DataSet = objApplicant.GetCommonMaster(Session("PreviewCompCode").ToString, CInt(Session("Previewcompanyunkid")), clsCommon_Master.enCommonMaster.TITLE)
                Dim dsGender As DataSet = objPersonalInfo.GetGender()

                dsPersonalInfo = objPersonalInfo.GetPersonalInfo(strCompCode:=Session("PreviewCompCode").ToString,
                                                                 intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                 intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid"))
                                                                 )
                If dsPersonalInfo.Tables(0).Rows.Count > 0 Then

                    strName = dsPersonalInfo.Tables(0).Rows(0).Item("firstname").ToString & " " & dsPersonalInfo.Tables(0).Rows(0).Item("othername").ToString & " " & dsPersonalInfo.Tables(0).Rows(0).Item("surname").ToString
                    strEmail = "Email: " & dsPersonalInfo.Tables(0).Rows(0).Item("email").ToString & ", " & lblMobileNo.Text & " " & dsPersonalInfo.Tables(0).Rows(0).Item("present_mobileno").ToString
                    strBirthDate = CDate(dsPersonalInfo.Tables(0).Rows(0).Item("birth_date")).ToString("dd-MMM-yyyy")

                    If CInt(dsPersonalInfo.Tables(0).Rows(0).Item("titleunkid")) > 0 Then
                        If dsTitle.Tables(0).Select("masterunkid = " & CInt(dsPersonalInfo.Tables(0).Rows(0).Item("titleunkid")) & " ").Length > 0 Then
                            strTitle = dsTitle.Tables(0).Select("masterunkid = " & CInt(dsPersonalInfo.Tables(0).Rows(0).Item("titleunkid")) & " ")(0).Item("Name").ToString
                        End If
                    End If

                    If CInt(dsPersonalInfo.Tables(0).Rows(0).Item("gender")) > 0 Then
                        strGender = dsGender.Tables(0).Select("Id = " & CInt(dsPersonalInfo.Tables(0).Rows(0).Item("gender")) & " ")(0).Item("Name").ToString
                    End If

                    'If CInt(dsPersonalInfo.Tables(0).Rows(0).Item("nationality")) > 0 Then
                    '    strNationality = dsPersonalInfo.Tables(0).Rows(0).Item("country_name").ToString & "n"
                    'End If
                End If

                dsContactDetails = objContactDetails.GetContactDetail(strCompCode:=Session("PreviewCompCode").ToString,
                                                                     intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                     intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid"))
                                                                     )

                If dsContactDetails.Tables(0).Rows.Count > 0 Then
                    '*** Tel. No
                    If dsContactDetails.Tables(0).Rows(0).Item("present_tel_no").ToString.Trim.Length > 0 Then
                        strEmail &= ", " & lblTelno.Text & " " & dsContactDetails.Tables(0).Rows(0).Item("present_tel_no").ToString.Trim
                    End If
                    '*** Fax
                    If dsContactDetails.Tables(0).Rows(0).Item("present_fax").ToString.Trim.Length > 0 Then
                        strEmail &= ", " & lblFax.Text & " " & dsContactDetails.Tables(0).Rows(0).Item("present_fax").ToString.Trim
                    End If

                    '*** Current Address
                    strPresentAddress = dsContactDetails.Tables(0).Rows(0).Item("present_address1").ToString.Trim


                    If dsContactDetails.Tables(0).Rows(0).Item("present_address2").ToString.Trim.Length > 0 Then
                        strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_address2").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_plotno").ToString.Trim.Length > 0 Then
                        strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_plotno").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_estate").ToString.Trim.Length > 0 Then
                        strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_estate").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_road").ToString.Trim.Length > 0 Then
                        strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_road").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_province").ToString.Trim.Length > 0 Then
                        strPresentAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_province").ToString.Trim
                    End If


                    If dsContactDetails.Tables(0).Rows(0).Item("present_country_name").ToString.Trim.Length > 0 Then
                        strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_country_name").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_State_Name").ToString.Trim.Length > 0 Then
                        strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_State_Name").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_City_Name").ToString.Trim.Length > 0 Then
                        strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_City_Name").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("present_zipcode_no").ToString.Trim.Length > 0 Then
                        strPresentAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("present_zipcode_no").ToString.Trim
                    End If

                    If strPresentAddress2.Trim.Length > 0 Then
                        strPresentAddress2 = strPresentAddress2.Substring(2)
                    End If


                    '*** Permenant Address
                    If dsContactDetails.Tables(0).Rows(0).Item("perm_address1").ToString.Trim.Length > 0 Then
                        strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_address1").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_address2").ToString.Trim.Length > 0 Then
                        strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_address2").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_plotno").ToString.Trim.Length > 0 Then
                        strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_plotno").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_estate").ToString.Trim.Length > 0 Then
                        strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_estate").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_road").ToString.Trim.Length > 0 Then
                        strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_road").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_province").ToString.Trim.Length > 0 Then
                        strPermenantAddress &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_province").ToString.Trim
                    End If


                    If dsContactDetails.Tables(0).Rows(0).Item("perm_country_name").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_country_name").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_State_Name").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_State_Name").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_City_Name").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_City_Name").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_zipcode_no").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", " & dsContactDetails.Tables(0).Rows(0).Item("perm_zipcode_no").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_tel_no").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", " & lblPrmTelNo.Text & " " & dsContactDetails.Tables(0).Rows(0).Item("perm_tel_no").ToString.Trim
                    End If

                    If dsContactDetails.Tables(0).Rows(0).Item("perm_fax").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", " & lblPrmfax.Text & " " & dsContactDetails.Tables(0).Rows(0).Item("perm_fax").ToString.Trim
                    End If


                End If

                '*** Other Info
                dsOtherInfo = objOtherInfo.GetOtherInfo(strCompCode:=Session("PreviewCompCode").ToString,
                                                        intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                        intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")),
                                                        intLanguage_id:=clsCommon_Master.enCommonMaster.LANGUAGES,
                                                        intMaritalStatus_id:=clsCommon_Master.enCommonMaster.MARRIED_STATUS
                                                        )

                If dsOtherInfo.Tables(0).Rows.Count > 0 Then

                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language1unkid")) > 0 Then
                        strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language1name").ToString
                    End If
                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language2unkid")) > 0 Then
                        strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language2name").ToString
                    End If
                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language3unkid")) > 0 Then
                        strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language3name").ToString
                    End If
                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("language4unkid")) > 0 Then
                        strLanguagesKnown &= ", " & dsOtherInfo.Tables(0).Rows(0).Item("language4name").ToString
                    End If

                    If strLanguagesKnown.Trim.Length > 0 Then
                        strLanguagesKnown = strLanguagesKnown.Substring(2)
                    End If

                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("nationality")) > 0 Then
                        strNationality = dsOtherInfo.Tables(0).Rows(0).Item("country_name").ToString & "n"
                    End If

                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("marital_statusunkid")) > 0 Then
                        strMaritalStatus = dsOtherInfo.Tables(0).Rows(0).Item("marital_status").ToString
                    End If

                    If IsDBNull(dsOtherInfo.Tables(0).Rows(0).Item("anniversary_date")) = False AndAlso CDate(dsOtherInfo.Tables(0).Rows(0).Item("anniversary_date")).Date <> CDate("01/Jan/1900") Then
                        strMarriedDate = CDate(dsOtherInfo.Tables(0).Rows(0).Item("anniversary_date")).ToString("dd-MMM-yyyy")
                    End If

                    If dsOtherInfo.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim.Length > 0 AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False]
                        txtMotherTongue.Text = dsOtherInfo.Tables(0).Rows(0).Item("mother_tongue").ToString
                    Else
                        pnlMotherTongue.Visible = False
                    End If

                    If CBool(dsOtherInfo.Tables(0).Rows(0).Item("isimpaired")) = True Then
                        txtImpaired.Text = "Yes <BR />" & dsOtherInfo.Tables(0).Rows(0).Item("impairment").ToString
                    Else
                        txtImpaired.Text = "No"
                    End If

                    If CDec(dsOtherInfo.Tables(0).Rows(0).Item("current_salary")) <> 0 AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False]
                        txtCurrent_salary.Text = Format(CDec(dsOtherInfo.Tables(0).Rows(0).Item("current_salary").ToString), "##,##,##,##,##0.00")
                    Else
                        pnlCurrentSalary.Visible = False
                    End If

                    If CDec(dsOtherInfo.Tables(0).Rows(0).Item("expected_salary")) <> 0 AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False] Then
                        txtExpected_salary.Text = Format(CDec(dsOtherInfo.Tables(0).Rows(0).Item("expected_salary").ToString), "##,##,##,##,##0.00")
                    Else
                        pnlExpectedSalary.Visible = False
                    End If

                    If dsOtherInfo.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim.Length > 0 AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False]
                        txtExpected_benefits.Text = dsOtherInfo.Tables(0).Rows(0).Item("expected_benefits").ToString
                    Else
                        pnlExpectedBenefits.Visible = False
                    End If

                    If CBool(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = True Then
                        txtWilling_to_relocate.Text = "Yes"
                    Else
                        txtWilling_to_relocate.Text = "No"
                    End If

                    'If CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = enYesno.None Then
                    '    pnlWillingToRelocate.Visible = False
                    'ElseIf CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = enYesno.Yes Then
                    '    lblWillingToRelocate.Text = "Yes"
                    'Else
                    '    lblWillingToRelocate.Text = "No"
                    'End If
                    'Hemant (01 Sep 2023) -- Start
                    'ISSUE/Enhancement(ZRA) : A1X-1157 - The system generated CV on applicant should not show fields that have been hidden on the applicant portal pages
                    If CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        pnlWillingToRelocate.Visible = True
                    Else
                        pnlWillingToRelocate.Visible = False
                    End If
                    'Hemant (01 Sep 2023) -- End

                    If CBool(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = True Then
                        txtWilling_to_travel.Text = "Yes"
                    Else
                        txtWilling_to_travel.Text = "No"
                    End If

                    'If CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = enYesno.None Then
                    '    pnlWillingToTravel.Visible = False
                    'ElseIf CInt(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = enYesno.Yes Then
                    '    lblWillingToTravel.Text = "Yes"
                    'Else
                    '    lblWillingToTravel.Text = "No"
                    'End If
                    'Hemant (01 Sep 2023) -- Start
                    'ISSUE/Enhancement(ZRA) : A1X-1157 - The system generated CV on applicant should not show fields that have been hidden on the applicant portal pages
                    If CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        pnlWillingToTravel.Visible = True
                    Else
                        pnlWillingToTravel.Visible = False
                    End If
                    'Hemant (01 Sep 2023) -- End

                    If CInt(dsOtherInfo.Tables(0).Rows(0).Item("notice_period_days")) <> 0 AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                        'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False]
                        txtNotice_period_days.Text = dsOtherInfo.Tables(0).Rows(0).Item("notice_period_days").ToString & " Days"
                    Else
                        pnlNoticePeriodDays.Visible = False
                    End If

                    'If CDate(dsOtherInfo.Tables(0).Rows(0).Item("earliest_possible_startdate")) <> CDate("01-Jan-1900") Then
                    '    lblEarliestPossibleStartDate.Text = CDate(dsOtherInfo.Tables(0).Rows(0).Item("earliest_possible_startdate")).ToShortDateString
                    'Else
                    pnlEarliestPossibleStartDate.Visible = False
                    'End If

                    'If dsOtherInfo.Tables(0).Rows(0).Item("comments").ToString.Trim.Length > 0 Then
                    '    lblComments.Text = dsOtherInfo.Tables(0).Rows(0).Item("comments").ToString
                    'Else
                    pnlComments.Visible = False
                    'End If

                    If dsOtherInfo.Tables(0).Rows(0).Item("perm_alternateno").ToString.Trim.Length > 0 Then
                        strPermenantAddress2 &= ", Alt. Tel. No.: " & dsOtherInfo.Tables(0).Rows(0).Item("perm_alternateno").ToString
                    End If

                    If dsOtherInfo.Tables(0).Rows(0).Item("memberships").ToString.Trim.Length > 0 Then
                        strMemberships = dsOtherInfo.Tables(0).Rows(0).Item("memberships").ToString
                    End If

                    If dsOtherInfo.Tables(0).Rows(0).Item("achievements").ToString.Trim.Length > 0 Then
                        strAchievements = dsOtherInfo.Tables(0).Rows(0).Item("achievements").ToString
                    End If

                    If dsOtherInfo.Tables(0).Rows(0).Item("journalsresearchpapers").ToString.Trim.Length > 0 Then
                        strJournalResearchPaper = dsOtherInfo.Tables(0).Rows(0).Item("journalsresearchpapers").ToString
                    End If
                End If

                If strPermenantAddress.Trim.Length > 0 Then
                    strPermenantAddress = strPermenantAddress.Substring(2)
                End If

                If strPermenantAddress2.Trim.Length > 0 Then
                    strPermenantAddress2 = strPermenantAddress2.Substring(2)
                End If

                dsExperience = (New clsApplicantExperience).GetApplicantExperience(strCompCode:=Session("PreviewCompCode").ToString,
                                                                     intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                     intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid"))
                                                                     )

                dlExperience.DataSource = dsExperience.Tables(0)
                dlExperience.DataBind()
                If dsExperience.Tables(0).Rows.Count <= 0 Then
                    divExperience.Visible = False
                Else
                    Dim lstExp As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantExperience", intLanguageID:=0)
                    If lstExp IsNot Nothing Then
                        Call SetCaptions(divExperience, lstExp, langid)
                    End If
                End If
                '*********************************************************

                Me.Title = strName & " CV"
                'lblName.Text = Sanitizer.GetSafeHtmlFragment(strName)
                lblName.Text = AntiXss.AntiXssEncoder.HtmlEncode(strName, True)

                If strTitle.Trim.Length > 0 Then
                    strTitle = strTitle & " "
                Else
                    strTitle = ""
                End If
                'txtFullName.Text = Sanitizer.GetSafeHtmlFragment(strTitle & strName)
                txtFullName.Text = AntiXss.AntiXssEncoder.HtmlEncode(strTitle & strName, True)

                'lblEmailMobile.Text = Sanitizer.GetSafeHtmlFragment(strEmail)
                'txtGender.Text = Sanitizer.GetSafeHtmlFragment(strGender)
                lblEmailMobile.Text = AntiXss.AntiXssEncoder.HtmlEncode(strEmail, True)
                txtGender.Text = AntiXss.AntiXssEncoder.HtmlEncode(strGender, True)

                If strPresentAddress.Trim.Length > 0 Then
                    'lblAddress.Text = Sanitizer.GetSafeHtmlFragment(strPresentAddress)
                    lblAddress.Text = AntiXss.AntiXssEncoder.HtmlEncode(strPresentAddress, True)
                Else
                    lblAddress.Text = ""
                End If

                If strPresentAddress2.Trim.Length > 0 Then
                    'lblAddress2.Text = Sanitizer.GetSafeHtmlFragment(strPresentAddress2)
                    lblAddress2.Text = AntiXss.AntiXssEncoder.HtmlEncode(strPresentAddress2, True)
                Else
                    lblAddress2.Text = ""
                End If

                'txtBirthDate.Text = Sanitizer.GetSafeHtmlFragment(strBirthDate)
                txtBirthDate.Text = AntiXss.AntiXssEncoder.HtmlEncode(strBirthDate, True)

                If strMaritalStatus.Trim.Length > 0 Then
                    'txtMaritalStatus.Text = Sanitizer.GetSafeHtmlFragment(strMaritalStatus)
                    txtMaritalStatus.Text = AntiXss.AntiXssEncoder.HtmlEncode(strMaritalStatus, True)
                Else
                    txtMaritalStatus.Text = ""
                End If
                If strMarriedDate.Trim.Length > 0 Then
                    'objlblMarriedDate.Text = Sanitizer.GetSafeHtmlFragment("Married Date: ")
                    'txtMarriedDate.Text = Sanitizer.GetSafeHtmlFragment(strMarriedDate)
                    lblMarriedDate.Text = AntiXss.AntiXssEncoder.HtmlEncode("Married Date: ", True)
                    txtMarriedDate.Text = AntiXss.AntiXssEncoder.HtmlEncode(strMarriedDate, True)
                Else
                    'lblMarriedDate.Text = ""
                    lblMarriedDate.Visible = False
                    txtMarriedDate.Text = ""
                End If
                'Hemant (01 Sep 2023) -- Start
                'ISSUE/Enhancement(ZRA) : A1X-1157 - The system generated CV on applicant should not show fields that have been hidden on the applicant portal pages
                If CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                    pnlMarital.Visible = True
                Else
                    pnlMarital.Visible = False
                End If
                'Hemant (01 Sep 2023) -- End
                'txtNationality.Text = Sanitizer.GetSafeHtmlFragment(strNationality)
                'lblLanguagesKnown.Text = Sanitizer.GetSafeHtmlFragment(strLanguagesKnown)
                txtNationality.Text = AntiXss.AntiXssEncoder.HtmlEncode(strNationality, True)
                'Hemant (01 Sep 2023) -- Start
                'ISSUE/Enhancement(ZRA) : A1X-1157 - The system generated CV on applicant should not show fields that have been hidden on the applicant portal pages
                If CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                    pnlLanguagesKnown.Visible = True
                    'Hemant (01 Sep 2023) -- End
                txtLanguagesKnown.Text = AntiXss.AntiXssEncoder.HtmlEncode(strLanguagesKnown, True)
                    'Hemant (01 Sep 2023) -- Start
                    'ISSUE/Enhancement(ZRA) : A1X-1157 - The system generated CV on applicant should not show fields that have been hidden on the applicant portal pages
                Else
                    pnlLanguagesKnown.Visible = False
                    txtLanguagesKnown.Text = ""
                End If
                'Hemant (01 Sep 2023) -- End


                If strPermenantAddress.Trim.Length > 0 AndAlso CBool(Session("HideRecruitementPermanentAddress")) = False Then
                    'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementPermanentAddress")) = False]
                    'lblPermanentAddress.Text = Sanitizer.GetSafeHtmlFragment(strPermenantAddress)
                    'lblPermanentAddress2.Text = Sanitizer.GetSafeHtmlFragment(strPermenantAddress2)
                    txtPermanentAddress.Text = AntiXss.AntiXssEncoder.HtmlEncode(strPermenantAddress, True)
                    txtPermanentAddress2.Text = AntiXss.AntiXssEncoder.HtmlEncode(strPermenantAddress2, True)
                Else
                    pnlPermanentAddress.Visible = False
                    txtPermanentAddress.Text = ""
                    txtPermanentAddress2.Text = ""
                End If

                If (strMemberships.Trim.Length > 0 OrElse strAchievements.Trim.Length > 0 OrElse strJournalResearchPaper.Trim.Length > 0) AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False Then
                    'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementOtherInfoScreen")) = False]
                    pnlOtherInfo.Visible = True

                    If strMemberships.Trim.Length > 0 Then
                        'lblMembership.Text = Sanitizer.GetSafeHtmlFragment(strMemberships)
                        txtMembership.Text = AntiXss.AntiXssEncoder.HtmlEncode(strMemberships, True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;")
                        divMembership.Visible = True
                    Else
                        divMembership.Visible = False
                    End If

                    If strAchievements.Trim.Length > 0 Then
                        'lblAchievements.Text = Sanitizer.GetSafeHtmlFragment(strAchievements)
                        txtAchievements.Text = AntiXss.AntiXssEncoder.HtmlEncode(strAchievements, True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;")
                        divAchievements.Visible = True
                    Else
                        divAchievements.Visible = False
                    End If

                    If strJournalResearchPaper.Trim.Length > 0 Then
                        'lblJournalResearchPaper.Text = Sanitizer.GetSafeHtmlFragment(strJournalResearchPaper)
                        txtJournalResearchPaper.Text = AntiXss.AntiXssEncoder.HtmlEncode(strJournalResearchPaper, True).Replace(";&#13;&#10;", "<br />").Replace("&#10;", "<br />").Replace(" ", "*").Replace("&#9;", "&nbsp;&nbsp;&nbsp;&nbsp;")
                        divJournalResearchPaper.Visible = True
                    Else
                        divJournalResearchPaper.Visible = False
                    End If
                Else
                    pnlOtherInfo.Visible = False
                End If

                'lblCompanyName.Text = Sanitizer.GetSafeHtmlFragment(strExpCompanyName)
                'lblEmployer.Text = Sanitizer.GetSafeHtmlFragment(strEmployer)
                'If strEmployer.Trim = "" Then
                '    trExpEmployer.Visible = False
                'End If
                'lblDesignation.Text = Sanitizer.GetSafeHtmlFragment(strDesignation)
                'lblDuration.Text = Sanitizer.GetSafeHtmlFragment(strDuration)
                'lblResponsibility.Text = Sanitizer.GetSafeHtmlFragment(strResponsibility)
                'lblAchievement.Text = Sanitizer.GetSafeHtmlFragment(strExpAchievement)
                'If strExpAchievement.Trim = "" Then
                '    trExpAchive.Visible = False
                'End If
                'lblLeavingReason.Text = Sanitizer.GetSafeHtmlFragment(strLeavingReason)
                'If strLeavingReason.Trim = "" Then
                '    trExpLeavingReason.Visible = False
                'End If
            End If
            '=============Skill Information Start==============
            Dim dsSkill As DataSet = (New clsApplicantSkill).GetApplicantSkills(strCompCode:=Session("PreviewCompCode").ToString,
                                                                                intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                                intSkillCategory_ID:=clsCommon_Master.enCommonMaster.SKILL_CATEGORY,
                                                                                intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")))
            Dim strSkillCSV As String = ""
            If dsSkill IsNot Nothing AndAlso dsSkill.Tables(0).Rows.Count > 0 Then
                strSkillCSV = String.Join(", ", dsSkill.Tables(0).AsEnumerable().Select(Function(x) " " & IIf(x.Field(Of Integer)("skillunkid") > 0, x.Field(Of String)("SKILL"), x.Field(Of String)("other_skill")).ToString))
                'strSkillCSV = strSkillCSV.Substring(1)
            End If
            txtSkill.Text = strSkillCSV
            If strSkillCSV.Trim.Length <= 0 OrElse CBool(Session("HideRecruitementSkillScreen")) = True Then
                'Hemant (01 Sep 2023) -- [AndAlso CBool(Session("HideRecruitementSkillScreen")) = False]
                divSkill.Visible = False
            End If
            '=============Skill Information End==============

            '=============Qualification Information Start ==========
            Dim dsQualification As DataSet = (New clsApplicantQualification).GetApplicantQualifications(strCompCode:=Session("PreviewCompCode").ToString,
                                                                                                        intComUnkID:=CInt(Session("Previewcompanyunkid")),
                                                                                                        intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")),
                                                                                                        intQualificationGroup_ID:=clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, intSortOrder:=CInt(Session("appqualisortbyid")))

            dlQualification.DataSource = dsQualification.Tables(0)
            dlQualification.DataBind()
            If dsQualification.Tables(0).Rows.Count <= 0 Then divQualification.Visible = False
            '=============Qualification Information END==========

            '=============Refrences Information Start===========
            Dim dsRefrence As DataSet = (New clsApplicantReference).GetApplicantReference(Session("PreviewCompCode").ToString, CInt(Session("Previewcompanyunkid")), CInt(Session("PreviewApplicantUnkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            dlRefrences.DataSource = dsRefrence.Tables(0)
            dlRefrences.DataBind()
            If dsRefrence.Tables(0).Rows.Count <= 0 Then pnlRefrence.Visible = False
            '=============Refrences Information END===========


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            objPersonalInfo = Nothing
            objContactDetails = Nothing
            objOtherInfo = Nothing
        End Try
    End Sub

    Protected Function GetEmployerNameTelNo(dr As DataRowView) As String
        Dim strEmployerName As String = ""
        Try
            Dim strEmployer As String = ""
            Dim strOPhone As String = ""

            If dr.Item("Employer").ToString.Trim.Length > 0 Then
                'strEmployer = dr.Item("Employer").ToString
                strEmployer = AntiXss.AntiXssEncoder.HtmlEncode(dr.Item("Employer").ToString, True)
            End If
            If dr.Item("Phone").ToString.Trim.Length > 0 Then
                'strOPhone = "<b>Tel. No.:</b> " & dr.Item("Phone").ToString
                strOPhone = "<b>" & lblOfficePhone.Text & "</b> " & AntiXss.AntiXssEncoder.HtmlEncode(dr.Item("Phone").ToString, True)
            End If
            If strEmployer.Trim.Length > 0 Then
                If strOPhone.Trim.Length > 0 Then
                    strEmployer &= ", " & strOPhone
                End If
            Else
                strEmployer = strOPhone
            End If

            'strEmployerName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(strEmployer)
            'strEmployerName = AntiXss.AntiXssEncoder.HtmlEncode(strEmployer, True)
            strEmployerName = strEmployer

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
        'Return Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(strEmployerName)
        'Return AntiXss.AntiXssEncoder.HtmlEncode(strEmployerName, True)
        Return strEmployerName
    End Function

    Protected Function GetDuration(dr As DataRowView) As String
        Dim strDuration As String = ""
        Try
            strDuration = CDate(dr.Item("JoinDate")).ToString("dd-MMM-yyyy") & " - "
            If CDate(dr.Item("terminationdate")).Date <> CDate("01/Jan/1900").Date Then
                strDuration &= CDate(dr.Item("terminationdate")).ToString("dd-MMM-yyyy")
            Else
                strDuration &= "Present"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
        'Return Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(strDuration)
        Return AntiXss.AntiXssEncoder.HtmlEncode(strDuration, True)
    End Function

    Private Sub dlQualification_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dlQualification.ItemDataBound
        Dim strDuration As String = ""
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                'If CInt(drv.Item("qualificationgroupunkid")) <= 0 AndAlso CInt(drv.Item("qualificationunkid")) <= 0 Then
                '    e.Item.Cells(0).Text = drv.Item("other_qualification").ToString
                '    e.Item.Cells(1).Text = drv.Item("other_institute").ToString
                '    e.Item.Cells(2).Text = drv.Item("other_resultcode").ToString
                'End If
                If CInt(drv.Item("qualificationgroupunkid")) <= 0 Then
                    e.Item.Cells(0).Text = drv.Item("other_qualificationgrp").ToString
                End If
                If CInt(drv.Item("qualificationunkid")) <= 0 Then
                    e.Item.Cells(0).Text = drv.Item("other_qualification").ToString
                End If
                If CInt(drv.Item("instituteunkid")) <= 0 Then
                    e.Item.Cells(1).Text = drv.Item("other_institute").ToString
                End If
                If CInt(drv.Item("resultunkid")) <= 0 Then
                    e.Item.Cells(2).Text = drv.Item("other_resultcode").ToString
                End If

                If CDate(drv.Item("startdate")).Date <> CDate("01/Jan/1900").Date Then
                    strDuration = CDate(drv.Item("startdate")).ToString("dd-MMM-yyyy") & " - "

                End If

                If CDate(drv.Item("enddate")).Date <> CDate("01/Jan/1900").Date Then
                    If strDuration.Length <= 0 Then
                        strDuration = " - "
                    End If
                    strDuration &= CDate(drv.Item("enddate")).ToString("dd-MMM-yyyy")
                ElseIf CDate(drv.Item("startdate")).Date <> CDate("01/Jan/1900").Date AndAlso CBool(Session("QualificationAwardDateMandatory")) = True Then
                    If strDuration.Length <= 0 Then
                        strDuration = " - "
                    End If
                    strDuration &= "Present"
                End If
                e.Item.Cells(3).Text = strDuration
                e.Item.Cells(4).Text = Math.Round(CDec(e.Item.Cells(4).Text), 2).ToString
                'e.Item.Cells(4).Style.Add("text-align", "right")
            ElseIf e.Item.ItemType = ListItemType.Header Then
                'e.Item.Cells(4).Style.Add("text-align", "right")
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dlRefrences_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dlRefrences.ItemDataBound
        Dim strDuration As String = ""
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                If drv.Item("Address").ToString.Length > 0 Then
                    strDuration &= drv.Item("Address").ToString
                End If
                If drv.Item("mobile_no").ToString.Trim.Length > 0 Then
                    If strDuration.Trim.Length > 0 Then strDuration &= "<br/>"
                    strDuration &= "<B> " & lblRefMobile.Text & " </b>" & drv.Item("mobile_no").ToString
                End If
                If drv.Item("telephone_no").ToString.Trim.Length > 0 Then
                    If drv.Item("mobile_no").ToString.Trim.Length > 0 Then
                        strDuration &= ", "
                    Else
                        strDuration &= "<br/>"
                    End If
                    strDuration &= "<b>" & lblRefTelNo.Text & " </b> " & drv.Item("telephone_no").ToString
                End If
                e.Item.Cells(1).Text = strDuration
                'e.Item.Cells(0).Style.Add("vertical-align", "top")
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
    Private Sub dlExperience_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlExperience.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)

                CType(e.Item.FindControl("txtRes"), Label).Text = drv.Item("Responsibility").ToString.Replace(vbCrLf, "<br />").Replace(vbLf, "<br />").Replace(" ", "*").Replace("", "&bull;").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                CType(e.Item.FindControl("txtAchievement"), Label).Text = drv.Item("achievements").ToString.Replace(vbCrLf, "<br />").Replace(vbLf, "<br />").Replace(" ", "*").Replace("", "&bull;").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")


            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Sohail (18 Feb 2020) -- End

    Private Function Validation() As Boolean
        Dim dsList As DataSet
        Dim strMessage As String = ""
        Dim strMsg As String = ""
        Dim i As Integer = 0
        Try
            strMsg = ""
            dsList = (New clsPersonalInfo).GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("firstname").ToString.Length <= 0 Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". First Name <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; First Name <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 AndAlso CBool(Session("MiddleNameMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Other Name <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("surname").ToString.Length <= 0 Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". SurName <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; SurName <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 AndAlso CBool(Session("GenderMandatory")) = True Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Gender <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender <br/>"
                End If
                If ((IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900")) AndAlso CBool(Session("BirthDateMandatory")) = True) Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Birth Date <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Birth Date <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("present_mobileno").ToString.Length <= 0 Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Mobile No. <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mobile No. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
                End If
            Else
                'strMsg &= "&nbsp;&nbsp;&nbsp;1. First Name <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;2. SurName <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;3. Gender <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;4. Birth Date <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;5. Mobile Number <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; First Name <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; SurName <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Birth Date <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mobile Number <br/>"
                i = 5
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
            End If
            If strMsg.Trim <> "" Then
                strMessage &= "<h5>My Profile > Personal Info</h5>" & strMsg
            End If

            strMsg = ""
            dsList = (New clsContactDetails).GetContactDetail(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 AndAlso CBool(Session("Address1Mandatory")) = True Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Address 1. <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 AndAlso CBool(Session("Address2Mandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 2. <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 AndAlso CBool(Session("RegionMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Region. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 AndAlso CBool(Session("CountryMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Country. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 AndAlso CBool(Session("StateMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current State. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 AndAlso CBool(Session("CityMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Town/City. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 AndAlso CBool(Session("PostCodeMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Code. <br/>"
                End If
            Else
                i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Address 1. <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. <br/>"
            End If

            If strMsg.Trim <> "" Then
                'strMessage &= "<B><h5>My Profile > Contact Details</h5></B>" & strMsg
                strMessage &= "<h5>My Profile > Contact Details</h5>" & strMsg
            End If

            strMsg = ""
            dsList = (New clsOtherInfo).GetOtherInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)
            If dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 AndAlso CBool(Session("Language1Mandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Language 1. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 AndAlso CBool(Session("MaritalStatusMandatory")) = True Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Marital Status. <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Marital Status. <br/>"
                End If
                'If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                '    i = i + 1
                '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Nationality. <br/>"
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
                'End If
            Else
                'i = i + 1
                ''strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Nationality. <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
                i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Marital Status. <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Marital Status. <br/>"
            End If
            If strMsg.Trim <> "" Then
                strMessage &= "<h5>My Profile > Language and Other Information</h5>" & strMsg
            End If

            strMsg = ""
            dsList = (New clsApplicantSkill).GetApplicantSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.SKILL_CATEGORY, CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneSkillMandatory")) = True Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Skills. <br/>"
            End If
            If strMsg.Trim <> "" Then
                strMessage &= "<h5><a href='ApplicantSkill.aspx'>Skills</a> > </h5>" & strMsg
            End If

            strMsg = ""
            dsList = (New clsApplicantQualification).GetApplicantQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, intSortOrder:=CInt(Session("appqualisortbyid")))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneQualificationMandatory")) = True Then
                i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification. <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Atleast one Qualification. <br/>"
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                If CBool(Session("HighestQualificationMandatory")) = True AndAlso dsList.Tables(0).Select("ishighestqualification =  1 ").Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Highest Qualification. <br/>"
                End If
            End If

            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
            'dsList = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
            '    i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification Attchment. <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Atleast one Qualification Attachment. <br/>"
            'End If
            'Sohail (11 Nov 2021) -- End
            If strMsg.Trim <> "" Then
                strMessage &= "<h5>Qualifications</h5>" & strMsg
            End If

            dsList = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.CURRICULAM_VITAE))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneCurriculamVitaeMandatory")) = True Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Atleast one Curriculum Vitae Attachment. <br/>"
            End If
            If strMsg.Trim <> "" Then
                strMessage &= "<h5>Attachments</h5>" & strMsg
            End If

            dsList = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.COVER_LETTER))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneCoverLetterMandatory")) = True Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Atleast one Cover Letter Attachment. <br/>"
            End If
            If strMsg.Trim <> "" Then
                strMessage &= "<h5>Other Attachments</h5>" & strMsg
            End If

            strMsg = ""
            dsList = (New clsApplicantExperience).GetApplicantExperience(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneJobExperienceMandatory")) = True Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Experiences. <br/>"
            End If
            If strMsg.Trim <> "" Then
                strMessage &= "<h5><a href='ApplicantExperience.aspx'>Experiences</a> > </h5>" & strMsg
            End If

            strMsg = ""
            dsList = (New clsApplicantReference).GetApplicantReference(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneReferenceMandatory")) = True Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; References. <br/>"
            End If

            'Redirect:
            If strMessage.Trim <> "" Then
                ShowMessage("Sorry, Your profile is incomplete. <BR> Please fill required details.")
                'HttpContext.Current.Response.Redirect("~/User/UserHome.aspx", False)
                pnlBlank.Visible = True
                pnlPreview.Visible = False
                lblMessage.Text = "<h4>Your Profile is incomplete.</h4><h5>Please fill following details to complete your profile.</h5><BR>" & strMessage
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub SetLanguage()
        Dim wLang As New clsWebLanguage
        Dim langid As Integer = 0
        Try
            Try
                langid = Convert.ToInt32(HttpContext.Current.Session("LangId"))
            Catch
                langid = 0
            End Try

            Dim lstPersonalInfo As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="PersonalInfo", intLanguageID:=0)
            If lstPersonalInfo IsNot Nothing Then
                Call SetCaptions(divPersonalInfo, lstPersonalInfo, langid)
                Call SetCaptions(pnlContactInfo, lstPersonalInfo, langid)
            End If

            Dim lstOtherInfo As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="OtherInfo", intLanguageID:=0)
            If lstOtherInfo IsNot Nothing Then
                Call SetCaptions(divPersonalInfo, lstOtherInfo, langid)
            End If

            Dim lstSkill As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantSkill", intLanguageID:=0)
            If lstSkill IsNot Nothing Then
                Call SetCaptions(divSkill, lstSkill, langid)
            End If

            Dim lstQuali As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantQualification", intLanguageID:=0)
            If lstQuali IsNot Nothing Then
                Call SetCaptions(divQualification, lstQuali, langid)
            End If

            Dim lstExp As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantExperience", intLanguageID:=0)
            If lstExp IsNot Nothing Then
                Call SetCaptions(divExperience, lstExp, langid)
            End If

            If lstOtherInfo IsNot Nothing Then
                Call SetCaptions(pnlOtherInfo, lstOtherInfo, langid)
            End If

            Dim lstRef As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ApplicantReference", intLanguageID:=0)
            If lstRef IsNot Nothing Then
                Call SetCaptions(pnlRefrence, lstRef, langid)
            End If

            Dim lstContactDetail As List(Of clsWebLanguage) = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:="ContactDetails", intLanguageID:=0)
            If lstContactDetail IsNot Nothing Then
                Call SetCaptions(pnlContactInfo, lstContactDetail, langid)
                Call SetCaptions(divPersonalInfo, lstContactDetail, langid)
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SetCaptions(pnl As Panel, list As List(Of clsWebLanguage), langid As Integer)

        Try

            For Each ctrl As Control In pnl.Controls
                If ctrl.GetType() = GetType(Panel) Then Call SetCaptions(CType(ctrl, Panel), list, langid)
                'Dim w As clsWebLanguage = list.Find(Function(x) x._controlunqid = ctrl.ID)
                Dim w As clsWebLanguage = Nothing
                'If w IsNot Nothing Then
                If ctrl.GetType() = GetType(Label) Then
                    w = list.Find(Function(x) x._controlunqid = ctrl.ID)

                    If w IsNot Nothing Then

                        If CType(ctrl, Label).Text = "" Then Continue For 'Don't apply language is label is set as blank from code side

                        Select Case langid
                            Case 1
                                CType(ctrl, Label).Text = w._language1
                            Case 2
                                CType(ctrl, Label).Text = w._language2
                            Case Else
                                CType(ctrl, Label).Text = w._language
                        End Select

                    End If

                ElseIf ctrl.GetType() = GetType(DataGrid) Then
                    For Each col As DataGridColumn In CType(ctrl, DataGrid).Columns
                        If col.FooterText = "" Then Continue For

                        w = list.Find(Function(x) x._controlunqid = col.FooterText)

                        If w IsNot Nothing Then

                            Select Case langid
                                Case 1
                                    CType(col, DataGridColumn).HeaderText = w._language1
                                Case 2
                                    CType(col, DataGridColumn).HeaderText = w._language2
                                Case Else
                                    CType(col, DataGridColumn).HeaderText = w._language
                            End Select

                        End If

                    Next

                ElseIf ctrl.GetType() = GetType(DataList) Then

                    If CType(ctrl, DataList).Items.Count > 0 Then
                        For Each itm As DataListItem In CType(ctrl, DataList).Items

                            For Each c As Control In itm.Controls
                                If c.GetType() = GetType(Panel) Then Call SetCaptions(CType(c, Panel), list, langid)

                                If c.GetType() = GetType(Label) Then

                                    w = list.Find(Function(x) x._controlunqid = c.ID)

                                    If w IsNot Nothing Then

                                        If CType(c, Label).Text = "" Then Continue For 'Don't apply language is label is set as blank from code side

                                        Select Case langid
                                            Case 1
                                                CType(c, Label).Text = w._language1
                                            Case 2
                                                CType(c, Label).Text = w._language2
                                            Case Else
                                                CType(c, Label).Text = w._language
                                        End Select

                                    End If

                                End If
                            Next
                        Next
                    End If

                End If

                'End If
            Next
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

End Class