﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
Imports System.Runtime.Caching

Public Class clsApplicantSkill

    Dim cache As ObjectCache = MemoryCache.Default

#Region " Method Functions "

    Public Function GetSkills(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intSkillCategoryUnkid As Integer
                                    ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetSkills"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@Skillcategoryunkid", SqlDbType.Int)).Value = intSkillCategoryUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "Skills")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetApplicantSkills(ByVal strCompCode As String _
                                       , intComUnkID As Integer _
                                       , intSkillCategory_ID As Integer _
                                       , intApplicantUnkid As Integer _
                                       , Optional blnRefreshCache As Boolean = False
                                       ) As DataSet
        ', blnAddHeaderRowIfNoRecordExist As Boolean

        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppSkill_" & strCompCode & "_" & intComUnkID.ToString & "_" & intSkillCategory_ID.ToString & "_" & intApplicantUnkid.ToString
            Dim dsCache As DataSet = TryCast(cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetApplicantSkills"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@skillcategory_id", SqlDbType.Int)).Value = intSkillCategory_ID
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid

                            da.SelectCommand = cmd
                            da.Fill(ds, "ApplicantSkills")

                            '*** To display Row Header when Empty Data Row
                            'If ds.Tables(0).Rows.Count = 0 AndAlso blnAddHeaderRowIfNoRecordExist = True Then
                            '    Dim r As DataRow = ds.Tables(0).NewRow

                            '    r.Item("skillcategory") = "None" ' To Hide the row and display only Row Header
                            '    r.Item("SKILL") = ""
                            '    ds.Tables(0).Rows.Add(r)
                            'End If

                        End Using
                    End Using
                End Using

                cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function AddApplicantSkill(ByVal strCompCode As String _
                                        , intComUnkID As Integer _
                                        , intApplicantUnkid As Integer _
                                        , intSkillCategoryUnkid As Integer _
                                        , intSkillUnkid As Integer _
                                        , strRemark As String _
                                        , strOtherSkillCategory As String _
                                        , strOtherSkill As String _
                                        , intSkillExpertiseUnkid As Integer _
                                        , dtCreated_date As Date
                                        ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procAddApplicantSkill"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@skillcategoryunkid", SqlDbType.Int)).Value = intSkillCategoryUnkid
                    cmd.Parameters.Add(New SqlParameter("@skillunkid", SqlDbType.Int)).Value = intSkillUnkid
                    cmd.Parameters.Add(New SqlParameter("@remark", SqlDbType.NVarChar)).Value = strRemark.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_skillcategory", SqlDbType.NVarChar)).Value = strOtherSkillCategory.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_skill", SqlDbType.NVarChar)).Value = strOtherSkill.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@skillexpertiseunkid", SqlDbType.Int)).Value = intSkillExpertiseUnkid
                    cmd.Parameters.Add(New SqlParameter("@created_date", SqlDbType.DateTime)).Value = dtCreated_date

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function EditApplicantSkill(ByVal strCompCode As String _
                                       , intComUnkID As Integer _
                                       , intApplicantUnkid As Integer _
                                       , intSkillTranUnkid As Integer _
                                       , intSkillCategoryUnkid As Integer _
                                       , intSkillUnkid As Integer _
                                       , strRemark As String _
                                       , strOtherSkillCategory As String _
                                       , strOtherSkill As String _
                                       , intSkillExpertiseUnkid As Integer
                                       ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procEditApplicantSkill"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@skilltranunkid", SqlDbType.Int)).Value = intSkillTranUnkid
                    cmd.Parameters.Add(New SqlParameter("@skillcategoryunkid", SqlDbType.Int)).Value = intSkillCategoryUnkid
                    cmd.Parameters.Add(New SqlParameter("@skillunkid", SqlDbType.Int)).Value = intSkillUnkid
                    cmd.Parameters.Add(New SqlParameter("@remark", SqlDbType.NVarChar)).Value = strRemark.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_skillcategory", SqlDbType.NVarChar)).Value = strOtherSkillCategory.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_skill", SqlDbType.NVarChar)).Value = strOtherSkill.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@skillexpertiseunkid", SqlDbType.Int)).Value = intSkillExpertiseUnkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function DeleteApplicantSkill(ByVal strCompCode As String _
                                        , intComUnkID As Integer _
                                        , intApplicantUnkid As Integer _
                                        , intSkillTranUnkid As Integer
                                        ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteApplicantSkill"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@skilltranunkid", SqlDbType.Int)).Value = intSkillTranUnkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function IsExistApplicantSkill(ByVal strCompCode As String _
                                          , intComUnkID As Integer _
                                          , intApplicantUnkid As Integer _
                                          , intSkillTranUnkid As Integer _
                                          , intSkillCategoryUnkid As Integer _
                                          , intSkillUnkid As Integer _
                                          , strOtherSkillCategory As String _
                                          , strOtherSkill As String
                                          ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistApplicantSkill"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@skilltranunkid", SqlDbType.Int)).Value = intSkillTranUnkid
                    cmd.Parameters.Add(New SqlParameter("@skillcategoryunkid", SqlDbType.Int)).Value = intSkillCategoryUnkid
                    cmd.Parameters.Add(New SqlParameter("@skillunkid", SqlDbType.Int)).Value = intSkillUnkid
                    cmd.Parameters.Add(New SqlParameter("@other_skillcategory", SqlDbType.NVarChar)).Value = strOtherSkillCategory.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_skill", SqlDbType.NVarChar)).Value = strOtherSkill.Trim.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return blnIsExist
    End Function

    Public Function GetSkillExpertise(ByVal strCompCode As String _
                                     , intComUnkID As Integer
                                     ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetSkillexpertise"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetSkillExpertise")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function
#End Region

End Class
