﻿Option Strict On

Public Class MiscAttachment
    Inherits Base_Page

    Private objAppQuali As New clsApplicantQualification
    Private mdblRemainingSizeKB As Double
    Private mblnAllowImageFile As Boolean
    Private mblnAllowDocumentFile As Boolean

#Region " Method Functions "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try

            dsCombo = objApplicant.GetDocType()
            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
            'dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.QUALIFICATIONS) & ", " & CInt(enScanAttactRefId.CURRICULAM_VITAE) & ", " & CInt(enScanAttactRefId.COVER_LETTER) & ") ", "", DataViewRowState.CurrentRows).ToTable
            'dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.CURRICULAM_VITAE) & ", " & CInt(enScanAttactRefId.COVER_LETTER) & ") ", "", DataViewRowState.CurrentRows).ToTable
            Dim str As String = ""
            If CBool(Session("OneCurriculamVitaeMandatory")) = True Then
                str &= ", " & CInt(enScanAttactRefId.CURRICULAM_VITAE) & " "
            End If
            If CBool(Session("OneCoverLetterMandatory")) = True Then
                str &= ", " & CInt(enScanAttactRefId.COVER_LETTER) & " "
            End If
            If CBool(Session("AttachmentQualificationMandatory")) = True Then
                str &= ", " & CInt(enScanAttactRefId.QUALIFICATIONS) & " "
            End If
            If str.Trim = "" Then
                str = " -998"
            Else
                str = str.Substring(1)
            End If
            dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & str & ") ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (11 Nov 2021) -- End
            With ddlDocType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
            With ddlDocTypeQuali
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub FillApplicantAttachments(Optional blnRefreshCache As Boolean = False)
        Dim dsList As DataSet
        Dim ds As DataSet
        Dim strMsg As String = ""
        Try
            dsList = objAppQuali.GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), 0, blnRefreshCache)

            gvQualiCerti.DataSource = dsList.Tables(0)
            gvQualiCerti.DataBind()

            Dim s4 As Site4 = CType(Me.Master, Site4)
            s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
            'ds = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
            'If ds.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("isQualiCertAttachMandatory")) = True Then
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>" & lblQualiAttachMsg.Text & "*</B> <br/>"
            'End If
            'Sohail (11 Nov 2021) -- End

            ds = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.CURRICULAM_VITAE))
            If ds.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneCurriculamVitaeMandatory")) = True Then
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>" & lblCVAttachMsg.Text & "*</B> <br/>"
            End If

            ds = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.COVER_LETTER))
            If ds.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneCoverLetterMandatory")) = True Then
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>" & lblCLAttachMsg.Text & "*</B> <br/>"
            End If

            ds = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
            If ds.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("AttachmentQualificationMandatory")) = True Then
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>" & lblQualiAttachMsg.Text & "*</B> <br/>"
            End If

            objlblRequired.Text = strMsg

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Call FillApplicantAttachments()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Page Events "


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                flUpload.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flUpload.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))

                mdblRemainingSizeKB = CDbl(flUpload.RemainingSizeKB)
                mblnAllowImageFile = flUpload.AllowImageFile
                mblnAllowDocumentFile = flUpload.AllowDocumentFile
                'Call GetValue()
            Else
                mdblRemainingSizeKB = CType(ViewState("mdblRemainingSizeKB"), Double)
                mblnAllowImageFile = CType(ViewState("mblnAllowImageFile"), Boolean)
                mblnAllowDocumentFile = CType(ViewState("mblnAllowDocumentFile"), Boolean)
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                flUpload.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub MiscAttachment_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Try
            Call GetValue()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ApplicantQualification_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ViewState("mdblRemainingSizeKB") = mdblRemainingSizeKB
            ViewState("mblnAllowImageFile") = mblnAllowImageFile
            ViewState("mblnAllowDocumentFile") = mblnAllowDocumentFile
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

#End Region

#Region " Datagridview Events "

    Private Sub gvQualiCerti_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvQualiCerti.RowCommand
        Try
            If e.CommandName = "Remove" Then

                Dim strFile As String = gvQualiCerti.DataKeys(CInt(e.CommandArgument)).Item("filepath").ToString ' dsAttach.Tables(0).Rows(CInt(e.CommandArgument)).Item("filepath").ToString
                'dsAttach.Tables(0).Rows(CInt(e.CommandArgument)).Item("AUD") = "D"
                'dsAttach.Tables(0).AcceptChanges()
                If objAppQuali.DeleteApplicantAttachment(strCompCode:=Session("CompCode").ToString _
                                                         , intComUnkID:=CInt(Session("companyunkid")) _
                                                         , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                         , intAttachfiletranUnkid:=CInt(gvQualiCerti.DataKeys(CInt(e.CommandArgument)).Item("attachfiletranunkid"))
                                                         ) = True Then

                    ShowMessage(lblAttachDeleteMsg.Text, MessageType.Info)

                    Call FillApplicantAttachments(True)

                End If
                'gvQualiCerti.DataSource = dsAttach.Tables(0)
                'gvQualiCerti.DataBind()

                'File.Delete(strFile)

                'If dsAttach.Tables(0).Select(" AUD <> 'D' ").Length >= 3 Then
                '    flUpload.Enabled = False
                'Else
                '    flUpload.Enabled = True
                'End If

            ElseIf e.CommandName = "Download" Then

                Dim strFile As String = gvQualiCerti.DataKeys(CInt(e.CommandArgument)).Item("filepath").ToString 'dsAttach.Tables(0).Rows(CInt(e.CommandArgument)).Item("filepath").ToString
                Dim r As GridViewRow = gvQualiCerti.Rows(CInt(e.CommandArgument))
                Response.ContentType = "image/jpg/pdf/doc"
                Response.AddHeader("Content-Disposition", "attachment;filename=""" & DirectCast(r.FindControl("objlblFilename"), Label).Text & """")
                'Response.AddHeader("Content-Length", 1024)
                Response.Clear()
                'Response.ContentType = "application/octet-stream"
                'Response.TransmitFile(Server.MapPath("~/" & filePath))
                'Response.TransmitFile(Server.MapPath(strFile))
                Response.TransmitFile(strFile)
                'Response.WriteFile(Server.MapPath("~/" & System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath") & "/DEMO_eb8c6c40-52ce-42b2-92d2-5c8966f69594.jpg"))
                'Response.TransmitFile(Server.MapPath("~/" & System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath") & "/DEMO_eb8c6c40-52ce-42b2-92d2-5c8966f69594.jpg").Replace("\", "/"))
                'Response.[End]()
                'Dim req As WebClient = New WebClient()
                'Dim data As Byte() = req.DownloadData(strFile)
                'Response.BinaryWrite(data)
                'Response.End()
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                Call FillApplicantAttachments()
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub gvQualiCerti_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvQualiCerti.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim s As New ScriptManager
                s.RegisterPostBackControl(e.Row.FindControl("lnkDownload"))
                'ScriptManager.RegisterPostBackControl(e.Row.FindControl("lnkDownload"))
            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub gvQualiCerti_DataBound(sender As Object, e As EventArgs) Handles gvQualiCerti.DataBound
        Try
            If gvQualiCerti.DataSource IsNot Nothing Then
                Dim dt As DataTable = CType(gvQualiCerti.DataSource, DataTable)
                Dim intTotKB As Integer = (From p In dt.AsEnumerable() Select (p.Field(Of Integer)("file_size"))).Sum()
                mdblRemainingSizeKB = (((CDbl(flUpload.MaxSizeKB) / 1024) - intTotKB) * 1024)
                flUpload.RemainingSizeKB = mdblRemainingSizeKB.ToString
                objlblTotSize.Text = "(" & Format((intTotKB / 1024), "0.00") & " MB / " & CDbl(flUpload.MaxSizeKB) / 1024 / 1024 & " MB) Used."
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Button Events "

    Private Sub flUpload_btnUpload_Click(sender As Object, e As EventArgs) Handles flUpload.btnUpload_Click
        Try
            If flUpload.HasFile = False Then Exit Try

            'mblnFromPreview = True 'To ignore job history validation and submit application
            'If Entryvalid(1) = False Then Exit Try
            'mblnFromPreview = False

            If CInt(ddlDocType.SelectedValue) <= 0 Then
                ShowMessage(lblDocTypeMsg.Text, MessageType.Info)
                ddlDocType.Focus()
                Exit Sub
            ElseIf CInt(ddlDocTypeQuali.SelectedValue) <= 0 Then
                ShowMessage(lblDocTypeQualiMsg.Text, MessageType.Info)
                ddlDocTypeQuali.Focus()
                Exit Sub
            End If

            If objAppQuali.IsExistApplicantAttachment(strCompCode:=Session("CompCode").ToString _
                                                     , intComUnkID:=CInt(Session("companyunkid")) _
                                                     , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                     , intAttachfiletranUnkid:=-1 _
                                                     , intModuleRefId:=CInt(enImg_Email_RefId.Applicant_Module) _
                                                     , intAttachRefId:=CInt(ddlDocType.SelectedValue) _
                                                     , strFilename:=flUpload.FileName
                                                     ) = True Then

                ShowMessage(lblAttachExistMsg.Text, MessageType.Info)
                Exit Sub

            End If
            'Dim dtRow As DataRow()
            'dtRow = dsAttach.Tables(0).Select("filename = '" & flUpload.FileName & "'" & " AND AUD <> 'D' ")

            'If dtRow.Length > 0 Then
            '    DisplayMessage.DisplayMessage("Selected file with same name is already added to the list.", Me)
            '    Exit Sub
            'End If

            'dtRow = dsAttach.Tables(0).Select(" AUD <> 'D' ")

            'Dim dsList As DataSet = objAppQuali.GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            'Sohail (21 Jul 2017) - Start
            'Enhancement - Now dont allow to attach total size of files more than 3 MB.
            'If dsList.Tables(0).Rows.Count >= 3 Then
            '    ShowMessage("Sorry, You can attach maximum 3 attachments only.", MessageType.Info)
            '    Exit Sub
            'End If
            'Sohail (21 Jul 2017) - End
            'If dtRow.Length >= 3 Then
            '    DisplayMessage.DisplayMessage("Sorry you can attach maximum 3 attachments only.", Me)
            '    Exit Sub
            'End If
            Dim blnInvalidFile As Boolean = False
            'Dim blnImage As Boolean = flUpload.AllowImageFile
            'Dim blnDoc As Boolean = flUpload.AllowDocumentFile

            Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG", "JPEG", "BMP", "GIF", "PNG"
                    If mblnAllowImageFile = False Then
                        blnInvalidFile = True
                    End If
                Case "DOC", "DOCX"
                    If mblnAllowDocumentFile = False Then
                        blnInvalidFile = True
                    End If
                Case "PDF"
                Case Else
                    blnInvalidFile = True
                    'ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                    'Exit Try
            End Select

            If blnInvalidFile = True Then
                ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                Exit Try
            End If

            'Dim mintByes As Integer = CInt(flUpload.MaxSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            Dim mintByes As Integer = CInt(mdblRemainingSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            If flUpload.FileBytes.LongLength > mintByes Then   ' 6 MB = 6291456 Bytes
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            Dim strUnkImgName As String
            Dim strFilename As String = flUpload.FileName

            If flUpload.PostedFile.FileName <> "" Then
                strUnkImgName = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & Split(flUpload.FileName, ".")(1)
                Dim Str As String = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath")
                If CInt(ddlDocType.SelectedValue) = CInt(enScanAttactRefId.CURRICULAM_VITAE) Then
                    Str = System.Configuration.ConfigurationManager.AppSettings("CVSavePath")
                ElseIf CInt(ddlDocType.SelectedValue) = CInt(enScanAttactRefId.COVER_LETTER) Then
                    Str = System.Configuration.ConfigurationManager.AppSettings("CoverLetterSavePath")
                End If
                If IO.Directory.Exists(Server.MapPath("~") & "\" & Str) = False Then
                    IO.Directory.CreateDirectory(Server.MapPath("~") & "\" & Str)
                End If
                Str &= "\" & strUnkImgName

                flUpload.SaveAs(Server.MapPath("~") & "\" & Str)

                If objAppQuali.AddApplicantAttachment(strCompCode:=Session("CompCode").ToString _
                                                     , intComUnkID:=CInt(Session("companyunkid")) _
                                                     , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                     , intDocumentUnkid:=CInt(ddlDocTypeQuali.SelectedValue) _
                                                     , intModulerefId:=CInt(enImg_Email_RefId.Applicant_Module) _
                                                     , intAttachrefId:=CInt(ddlDocType.SelectedValue) _
                                                     , strFilepath:=Server.MapPath("~") & "\" & Str _
                                                     , strFilename:=flUpload.FileName _
                                                     , strFileuniquename:=strUnkImgName _
                                                     , intFile_size:=CInt(flUpload.FileBytes.LongLength / 1024) _
                                                     , dtAttached_Date:=DateAndTime.Now
                                                     ) = True Then

                    ShowMessage(lblAttachSaveMsg.Text, MessageType.Info)

                End If
                'Dim dtIdRow As DataRow = dsAttach.Tables(0).NewRow

                'dtIdRow.Item("attachfiletranunkid") = -1
                'dtIdRow.Item("applicantunkid") = mintApplicantunkid
                'dtIdRow.Item("documentunkid") = CInt(ddlDocTypeQuali.SelectedValue)
                'dtIdRow.Item("modulerefid") = 2 '*** enImg_Email_RefId.Applicant_Module
                'dtIdRow.Item("attachrefid") = 3 '*** enScanAttactRefId.QUALIFICATIONS
                'dtIdRow.Item("filepath") = Server.MapPath(Str)
                'dtIdRow.Item("filename") = flUpload.FileName
                'dtIdRow.Item("fileuniquename") = strUnkImgName
                'dtIdRow.Item("file_size") = (flUpload.FileBytes.LongLength / 1024)
                'dtIdRow.Item("attached_date") = DateAndTime.Now
                'dtIdRow.Item("Comp_Code") = Session("CompCode")
                'dtIdRow.Item("companyunkid") = Session("companyunkid")
                'dtIdRow.Item("Syncdatetime") = DBNull.Value
                'dtIdRow.Item("localfilepath") = flUpload.PostedFile.FileName
                'dtIdRow.Item("GUID") = Guid.NewGuid
                'dtIdRow.Item("AUD") = "A"

                'dsAttach.Tables(0).Rows.Add(dtIdRow)

                Call FillApplicantAttachments(True)

            Else

            End If

            'Else
            '    If flUpload.PostedFile.FileName <> "" Then
            '        Str = System.Configuration.ConfigurationManager.AppSettings("QualiCertiSavePath") & "/" & flUpload.FileName
            '        flUpload.SaveAs(Server.MapPath(Str))
            '    Else

            '    End If
            'End If

            'Sohail (21 Jul 2017) - Start
            'Enhancement - Now dont allow to attach total size of files more than 3 MB.
            'If dsList.Tables(0).Rows.Count >= 3 Then
            '    flUpload.Enabled = False
            'Else
            '    flUpload.Enabled = True
            'End If
            'Sohail (21 Jul 2017) - End
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End
#End Region

End Class