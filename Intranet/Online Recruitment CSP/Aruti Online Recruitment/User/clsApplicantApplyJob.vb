﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient

Public Class clsApplicantApplyJob

#Region " Method Functions "

    Public Function GetApplicantAppliedJob(ByVal strCompCode As String,
                                           intComUnkID As Integer,
                                           intApplicantUnkId As Integer _
                                           , Optional intAppvacancytranunkid As Integer = 0
                                          ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantVacancy"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkId
                        cmd.Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intAppvacancytranunkid
                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantVacancyList")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Function GetApplicantJobApplied(ByVal strCompCode As String,
                                           intComUnkID As Integer,
                                           intApplicantUnkId As Integer
                                          ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantVacancy"

            If strCompCode Is Nothing Then strCompCode = ""

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkId
                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantVacancyList")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    Public Function DeleteApplicantAppliedJob(ByVal strCompCode As String,
                                             intComUnkID As Integer,
                                             intApplicantUnkid As Integer,
                                             intVacancyTranUnkId As Integer
                                             ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteApplicantAppliedJob"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intVacancyTranUnkId
                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1425) TRA - Provide applicant feedback on the online recruitment portal.

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Function GetApplicantFeedbackVacancyWise(ByVal strCompCode As String,
                                           intComUnkID As Integer,
                                           intVacancyId As Integer,
                                           mstrApplicantEmail As String
                                          ) As DataSet
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetApplicantFeedbackVacancyWise"

            If strCompCode Is Nothing Then strCompCode = ""

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@vacancyid", SqlDbType.Int)).Value = intVacancyId
                        cmd.Parameters.Add(New SqlParameter("@applicantemail", SqlDbType.NVarChar)).Value = mstrApplicantEmail
                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantFeedBackList")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    'Pinkal (16-Oct-2023) -- End

#End Region

End Class
