﻿Imports System.ComponentModel.DataAnnotations
Public Class dtoAppReferenceUpdate

    <Required()>
    Public Property CompCode As String
    <Required()>
    Public Property ComUnkID As Integer
    <Required()>
    Public Property ApplicantUnkid As Integer
    <Required()>
    Public Property ReferenceTranUnkId As Integer
    <Required()>
    Public Property RefName As String
    <Required()>
    Public Property Address As String
    <Required()>
    Public Property CountryUnkId As Integer
    <Required()>
    Public Property StateUnkId As Integer
    <Required()>
    Public Property CityUnkid As Integer
    <Required()>
    Public Property Email As String
    <Required()>
    Public Property Geneder As Integer
    <Required()>
    Public Property Position As String
    <Required()>
    Public Property TelNo As String
    <Required()>
    Public Property Mobile As String
    <Required()>
    Public Property RelationUnkId As Integer
End Class
