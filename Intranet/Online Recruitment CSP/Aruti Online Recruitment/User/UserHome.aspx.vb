﻿Option Strict On

Public Class UserHome
    Inherits Base_Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Dim str As String = Validation()
                Dim objApplicant As New clsApplicant
                objApplicant.IsValidForApplyVacancy(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), True, str)
                objlblRequired.Text = str
                If str.Trim.Length <= 0 Then
                    divMessage.Visible = False
                End If
            End If
            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Function Validation() As String
        'Dim dsList As DataSet
        Dim strMessage As String = ""
        'Dim strMsg As String = ""
        'Dim i As Integer = 0
        'Dim objApplicant As New clsApplicant
        Try
            'strMsg = ""
            'dsList = (New clsPersonalInfo).GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    If dsList.Tables(0).Rows(0).Item("firstname").ToString.Length <= 0 Then
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". First Name <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; First Name <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 AndAlso CBool(Session("MiddleNameMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Other Name <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Other Name [Optional] <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("surname").ToString.Length <= 0 Then
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". SurName <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; SurName <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 AndAlso CBool(Session("GenderMandatory")) = True Then
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Gender <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 Then
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender [Optional] <br/>"
            '    End If
            '    If ((IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900")) AndAlso CBool(Session("BirthDateMandatory")) = True) Then
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Birth Date <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Birth Date [Optional] <br/>"
            '    ElseIf IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900") Then
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Birth Date <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("present_mobileno").ToString.Length <= 0 Then
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Mobile No. <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mobile No. <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. [Optional] <br/>"
            '    End If
            'Else
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;1. First Name <br/>"
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;2. SurName <br/>"
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;3. Gender <br/>"
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;4. Birth Date <br/>"
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;5. Mobile Number <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; First Name <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; SurName <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Birth Date <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mobile Number <br/>"
            '    i = 5
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
            '    i = i + 1
            'End If
            'If strMsg.Trim <> "" Then
            '    strMessage &= "<h5>My Profile > <a href='PersonalInfo.aspx'>Personal Info</a> > </h5>" & strMsg
            'End If

            'strMsg = ""
            'dsList = (New clsContactDetails).GetContactDetail(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    If dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 AndAlso CBool(Session("Address1Mandatory")) = True Then
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Address 1. <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. [Optional] <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 AndAlso CBool(Session("Address2Mandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 2. <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 2. [Optional] <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 AndAlso CBool(Session("RegionMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Region. <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Region. [Optional] <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 AndAlso CBool(Session("CountryMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Country. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Country. [Optional] <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 AndAlso CBool(Session("StateMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current State. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current State. [Optional] <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 AndAlso CBool(Session("CityMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Town/City. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Town/City. [Optional] <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 AndAlso CBool(Session("PostCodeMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Code. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Code. [Optional] <br/>"
            '    End If
            'Else
            '    i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Address 1. <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    'strMessage &= "<B><h5>My Profile > Contact Details</h5></B>" & strMsg
            '    strMessage &= "<h5>My Profile > <a href='ContactDetails.aspx'>Contact Details</a> > </h5>" & strMsg
            'End If

            'strMsg = ""
            'dsList = (New clsOtherInfo).GetOtherInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    If CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 AndAlso CBool(Session("Language1Mandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Language Known. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Language Known [Optional]. <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 AndAlso CBool(Session("MaritalStatusMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Marital Status. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Marital Status. [Optional] <br/>"
            '    End If
            '    'If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
            '    '    i = i + 1
            '    '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
            '    'ElseIf CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 Then
            '    '    i = i + 1
            '    '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. [Optional] <br/>"
            '    'End If
            '    If dsList.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim = "" AndAlso CBool(Session("MotherTongueMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mother Tongue. <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim = "" Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mother Tongue. [Optional] <br/>"
            '    End If
            '    If CDec(dsList.Tables(0).Rows(0).Item("current_salary")) <= 0 AndAlso CBool(Session("CurrentSalaryMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Salary. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("current_salary")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Salary. [Optional] <br/>"
            '    End If
            '    If CDec(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 AndAlso CBool(Session("CurrentSalaryMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Salary. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Salary. [Optional] <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim = "" AndAlso CBool(Session("ExpectedBenefitsMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Benefits. <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim = "" Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Benefits. [Optional] <br/>"
            '    End If
            '    If CInt(dsList.Tables(0).Rows(0).Item("notice_period_days")) <= 0 AndAlso CBool(Session("NoticePeriodMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Notice Period. <br/>"
            '    ElseIf CInt(dsList.Tables(0).Rows(0).Item("notice_period_days")) <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Notice Period. [Optional] <br/>"
            '    End If
            '    If CDate(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate")) = CDate("01/Jan/1900") AndAlso CBool(Session("EarliestPossibleStartDateMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Earliest Possible Start Date. <br/>"
            '    ElseIf CDate(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate")) = CDate("01/Jan/1900") Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Earliest Possible Start Date. [Optional] <br/>"
            '    End If
            '    If dsList.Tables(0).Rows(0).Item("comments").ToString.Trim = "" AndAlso CBool(Session("CommentsMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Comments. <br/>"
            '    ElseIf dsList.Tables(0).Rows(0).Item("comments").ToString.Trim = "" Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Comments. [Optional] <br/>"
            '    End If
            'Else
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Language Known [Optional]. <br/>"
            '    i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Nationality. <br/>"
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
            '    'i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Marital Status. <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Marital Status. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mother Tongue. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Salary. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Salary. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Benefits. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Notice Period. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Earliest Possible Start Date. <br/>"
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Comments. <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    strMessage &= "<h5>My Profile > <a href='OtherInfo.aspx'>Language and Other Information</a> > </h5>" & strMsg
            'End If


            'strMsg = ""
            'dsList = (New clsApplicantSkill).GetApplicantSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.SKILL_CATEGORY, CInt(Session("applicantunkid")))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneSkillMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Skills. <br/>"
            'ElseIf dsList.Tables(0).Rows.Count <= 0 Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Skills. [Optional] <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    strMessage &= "<h5><a href='ApplicantSkill.aspx'>Skills</a> > </h5>" & strMsg
            'End If


            'strMsg = ""
            'dsList = (New clsApplicantQualification).GetApplicantQualifications(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, CInt(Session("appqualisortbyid")))
            ''If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneQualificationMandatory")) = True Then
            ''    i = i + 1
            ''    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. <br/>"
            ''ElseIf dsList.Tables(0).Rows.Count <= 0 Then
            ''    i = i + 1
            ''    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification. <br/>"
            ''    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. [Optional] <br/>"
            ''End If
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    If CBool(Session("HighestQualificationMandatory")) = True AndAlso dsList.Tables(0).Select("ishighestqualification =  1 ").Length <= 0 Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Highest Qualification. <br/>"
            '    End If
            'Else
            '    If CBool(Session("OneQualificationMandatory")) = True Then
            '        i = i + 1
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. <br/>"
            '    Else
            '        i = i + 1
            '        'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification. <br/>"
            '        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. [Optional] <br/>"
            '    End If
            'End If

            'dsList = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
            '    i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification Attchment. <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualification Attachments. <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    strMessage &= "<h5><a href='ApplicantQualification.aspx'>Qualifications</a> > </h5>" & strMsg
            'End If


            'strMsg = ""
            'dsList = (New clsApplicantExperience).GetApplicantExperience(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneJobExperienceMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Experiences. <br/>"
            'ElseIf dsList.Tables(0).Rows.Count <= 0 Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Experiences. [Optional] <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    strMessage &= "<h5><a href='ApplicantExperience.aspx'>Experiences</a> > </h5>" & strMsg
            'End If


            'strMsg = ""
            'dsList = (New clsApplicantReference).GetApplicantReference(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneReferenceMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; References. <br/>"
            'ElseIf dsList.Tables(0).Rows.Count <= 0 Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; References. [Optional] <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    strMessage &= "<h5><a href='ApplicantReference.aspx'>References</a> > </h5>" & strMsg
            'End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            strMessage = ""
        Finally
            'objApplicant = Nothing
        End Try
        Return strMessage
    End Function

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End
End Class