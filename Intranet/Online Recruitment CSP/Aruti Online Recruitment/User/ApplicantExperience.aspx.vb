﻿Option Strict On
Imports System.Runtime.Caching
Imports System.Windows.Media.Animation

Public Class ApplicantExperience
    Inherits Base_Page

    Private objAppExp As New clsApplicantExperience

#Region " Method Functions "
    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objApplicant As New clsApplicant
        Try
            dsCombo = objApplicant.GetDocType()
            Dim dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.JOBHISTORYS).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable
            With ddlDocType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
            With ddlAttachType
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        Finally
            dsCombo.Dispose() : objApplicant = Nothing
        End Try
    End Sub

    Private Sub ClearControl()
        Try
            ddlAttachType.SelectedIndex = 0
            ddlDocType.SelectedIndex = 0
            lblFileName.Text = ""
            pnluploadbutton.Enabled = False
            pnlFile.Visible = False
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'S.SANDEEP |04-MAY-2023| -- END

    Private Sub FillApplicantExperience(Optional blnRefreshCache As Boolean = False)
        'Dim dsList As DataSet
        Try
            'If blnRefreshCache = True Then
            '    Dim strCacheKey As String = "AppExp_" & CStr(Session("CompCode")) & "_" & CInt(Session("companyunkid")).ToString & "_" & CInt(Session("applicantunkid")).ToString
            '    Dim cache As ObjectCache = MemoryCache.Default
            '    If cache(strCacheKey) IsNot Nothing Then
            '        cache.Remove(strCacheKey)
            '    End If
            'End If
            'dsList = clsApplicantExperience.GetApplicantExperience(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            'grdJob.DataSource = dsList.Tables(0)
            'grdJob.DataBind()

            objodsJob.SelectParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            objodsJob.SelectParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            objodsJob.SelectParameters.Item("intApplicantUnkId").DefaultValue = CInt(Session("applicantunkid")).ToString
            If Session("JobAddDate") Is Nothing OrElse blnRefreshCache = True Then
                Session("JobAddDate") = DateAndTime.Now.ToString
            End If
            objodsJob.SelectParameters.Item("dtCurrentDate").DefaultValue = CStr(Session("JobAddDate"))
            lvJob.DataBind()

            Dim s4 As Site4 = CType(Me.Master, Site4)
            s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If txtEmployer.Text.Trim = "" AndAlso CBool(Session("EmployerMandatory")) = True Then
                ShowMessage(lblEmployerMsg.Text, MessageType.Info)
                txtEmployer.Focus()
                Return False
            ElseIf txtCompName.Text.Trim = "" AndAlso CBool(Session("CompanyNameJobHistoryMandatory")) = True Then
                ShowMessage(lblCompNamMsg.Text, MessageType.Info)
                txtCompName.Focus()
                Return False
            ElseIf txtCompName.Text.Trim = "" AndAlso txtEmployer.Text.Trim = "" Then
                ShowMessage(lblCompNamMsg.Text, MessageType.Info)
                txtCompName.Focus()
                Return False
            ElseIf txtDesignation.Text.Trim = "" AndAlso CBool(Session("PositionHeldMandatory")) = True Then
                ShowMessage(lblDesignationMsg.Text, MessageType.Info)
                txtDesignation.Focus()
                Return False
            ElseIf txtAchievement.Text.Trim = "" AndAlso CBool(Session("JobAchievementMandatory")) = True Then
                ShowMessage(lblAchievementMsg.Text, MessageType.Info)
                txtAchievement.Focus()
                Return False
            ElseIf dtjoindate.GetDate = CDate("01/Jan/1900") Then
                ShowMessage(lblDurationMsg1.Text, MessageType.Info)
                dtjoindate.Focus()
                Return False
                'ElseIf dtTerminationDate.GetDate = CDate("01/Jan/1900") Then
                '    ShowMessage("Termination Date is mandatory information. Please Enter Valid Termination Date.", MessageType.Info)
                '    dtTerminationDate.Focus()
                '    Return False
            ElseIf eZeeDate.convertDate(dtjoindate.GetDate) > eZeeDate.convertDate(System.DateTime.Today.Date) Then
                ShowMessage(lblDurationMsg2.Text, MessageType.Info)
                dtjoindate.Focus()
                Return False
            ElseIf dtTerminationDate.GetDate <> CDate("01/Jan/1900") Then
                If eZeeDate.convertDate(dtTerminationDate.GetDate) > eZeeDate.convertDate(System.DateTime.Today.Date) Then
                    ShowMessage(lblTermDateMsg1.Text, MessageType.Info)
                    dtTerminationDate.Focus()
                    Return False
                End If
            ElseIf dtjoindate.GetDate <> CDate("01/Jan/1900") AndAlso dtTerminationDate.GetDate <> CDate("01/Jan/1900") Then
                If eZeeDate.convertDate(dtTerminationDate.GetDate) < eZeeDate.convertDate(dtjoindate.GetDate) Then
                    ShowMessage(lblTermDateMsg2.Text, MessageType.Info)
                    dtTerminationDate.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If chkYesNo.Checked Then
                    If CInt(ddlDocType.SelectedValue) <= 0 Then
                        ShowMessage(lblDocTypeMsg.Text, MessageType.Info)
                        ddlDocType.Focus()
                        Return False
                    ElseIf CInt(ddlAttachType.SelectedValue) <= 0 Then
                        ShowMessage(lblAttachTypeMsg.Text, MessageType.Info)
                        ddlAttachType.Focus()
                        Return False
                    End If
                    If lblFileName.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text, MessageType.Errorr)
                        Return False
                    End If
                End If
            End If
            'S.SANDEEP |04-MAY-2023| -- END

            Return True

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
    End Function

    Private Sub GetValue()
        Try
            Call FillApplicantExperience()
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub Reset_JobHistory()
        Try
            dtjoindate.SetDate = DateAndTime.Now.Date
            dtTerminationDate.SetDate = Nothing
            txtEmployer.Text = ""
            txtDesignation.Text = ""
            txtResponsebilty.Text = ""
            txtOfficePhone.Text = ""
            txtCompName.Text = ""
            txtLeavingreson.Text = ""
            txtAchievement.Text = ""
            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            chkYesNo.Checked = False
            Call chkYesNo_CheckedChanged(Nothing, Nothing)
            'S.SANDEEP |04-MAY-2023| -- END
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If CBool(Session("EmployerMandatory")) = True Then
                    'txtEmployer.ValidationGroup = "JobHistory"
					'rfvEmployer.ValidationGroup = "JobHistory"
                    lblEmployer.CssClass = "required"
                Else
                    'txtEmployer.ValidationGroup = "Nothing"
					'rfvEmployer.ValidationGroup = "Nothing"
                    lblEmployer.CssClass = ""
                End If
                If CBool(Session("CompanyNameJobHistoryMandatory")) = True Then
					'rfvCompanyName.ValidationGroup = "JobHistory"
                    lblCompNam.CssClass = "required"
                Else
					'rfvCompanyName.ValidationGroup = "Nothing"
                    lblCompNam.CssClass = ""
                End If
                If CBool(Session("PositionHeldMandatory")) = True Then
					'rfvPositionHeld.ValidationGroup = "JobHistory"
                    lblDesignation.CssClass = "required"
                Else
					'rfvPositionHeld.ValidationGroup = "Nothing"
                    lblDesignation.CssClass = ""
                End If
                If CBool(Session("JobAchievementMandatory")) = True Then
                    lblAchievement.CssClass = "required"
                Else
                    lblAchievement.CssClass = ""
                    pnlAchievement.Visible = False
                End If
                Call GetValue()
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnAddJoB.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub ApplicantExperience_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    'Private Sub grdJob_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdJob.RowDataBound
    '    Try
    '        Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)

    '        If e.Row.RowType = DataControlRowType.DataRow Then

    '            If e.Row.RowState = DataControlRowState.Edit OrElse e.Row.RowState = 5 Then '5 = Alternate Or Edit

    '                'DirectCast(e.Row.FindControl("objdtJoinDate"), DateCtrl).SetDate = CDate(drv.Item("JoinDate"))
    '                'DirectCast(e.Row.FindControl("objdtJoinDate"), DateCtrl).Width = 100
    '                'DirectCast(e.Row.FindControl("objdtterminationdate"), DateCtrl).SetDate = CDate(drv.Item("terminationdate"))
    '                'DirectCast(e.Row.FindControl("objdtterminationdate"), DateCtrl).Width = 100

    '            ElseIf e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then

    '            End If
    '        End If
    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    'Private Sub grdJob_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdJob.RowEditing
    '    Try
    '        grdJob.EditIndex = e.NewEditIndex
    '        Call FillApplicantExperience()
    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    'Private Sub grdJob_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdJob.RowCancelingEdit
    '    Try
    '        grdJob.EditIndex = -1
    '        Call FillApplicantExperience()
    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    'Private Sub grdJob_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdJob.RowUpdating
    '    Try
    '        '** To Prevent Buttun Click when Page is Refreshed by User. **
    '        If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
    '        Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

    '        Dim txtEmployer As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtEmployer"), TextBox).Text
    '        Dim txtCompany As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtCompany"), TextBox).Text
    '        Dim txtPhone As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtPhone"), TextBox).Text
    '        Dim txtDesignation As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtDesignation"), TextBox).Text
    '        Dim dtJoinDate As Date = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objdtJoinDate"), DateCtrl).GetDate
    '        Dim dtTerminationDate As Date = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objdtterminationdate"), DateCtrl).GetDate
    '        Dim txtResponsibility As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtResponsibility"), TextBox).Text
    '        Dim txtAchievement As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtAchievement"), TextBox).Text
    '        Dim txtleavingReason As String = DirectCast(grdJob.Rows(e.RowIndex).FindControl("objtxtleavingReason"), TextBox).Text


    '        If txtEmployer.Trim = "" AndAlso CBool(Session("EmployerMandatory")) = True Then
    '            ShowMessage(lblEmployerMsg.Text, MessageType.Info)
    '            grdJob.Rows(e.RowIndex).FindControl("objtxtEmployer").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf txtCompany.Trim = "" AndAlso CBool(Session("CompanyNameJobHistoryMandatory")) = True Then
    '            ShowMessage(lblCompNamMsg.Text, MessageType.Info)
    '            grdJob.Rows(e.RowIndex).FindControl("objtxtCompany").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf txtCompany.Trim = "" AndAlso txtEmployer.Trim = "" Then
    '            ShowMessage(lblCompNamMsg.Text, MessageType.Info)
    '            grdJob.Rows(e.RowIndex).FindControl("objtxtCompany").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf txtDesignation.Trim = "" AndAlso CBool(Session("PositionHeldMandatory")) = True Then
    '            ShowMessage(lblDesignationMsg.Text, MessageType.Info)
    '            grdJob.Rows(e.RowIndex).FindControl("objtxtDesignation").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf dtJoinDate = CDate("01-Jan-1900") Then
    '            ShowMessage(lblDurationMsg1.Text, MessageType.Info)
    '            grdJob.Rows(e.RowIndex).FindControl("objdtJoinDate").Focus()
    '            e.Cancel = True
    '            Exit Try
    '            'ElseIf dtTerminationDate = CDate("01-Jan-1900") Then
    '            '    ShowMessage("Termination Date is mandatory information. Please Enter Valid Termination Date.", MessageType.Info)
    '            '    grdJob.Rows(e.RowIndex).FindControl("objdtterminationdate").Focus()
    '            '    e.Cancel = True
    '            '    Exit Try
    '        ElseIf eZeeDate.convertDate(dtJoinDate) > eZeeDate.convertDate(System.DateTime.Today.Date) Then
    '            ShowMessage(lblDurationMsg2.Text, MessageType.Info)
    '            grdJob.Rows(e.RowIndex).FindControl("objdtJoinDate").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf dtJoinDate <> CDate("01/Jan/1900") Then
    '            If eZeeDate.convertDate(dtTerminationDate) > eZeeDate.convertDate(System.DateTime.Today.Date) Then
    '                ShowMessage(lblTermDateMsg1.Text, MessageType.Info)
    '                grdJob.Rows(e.RowIndex).FindControl("objdtterminationdate").Focus()
    '                e.Cancel = True
    '                Exit Try
    '            End If
    '        ElseIf dtJoinDate <> CDate("01/Jan/1900") AndAlso dtTerminationDate <> CDate("01/Jan/1900") Then
    '            If eZeeDate.convertDate(dtTerminationDate) < eZeeDate.convertDate(dtJoinDate) Then
    '                ShowMessage(lblTermDateMsg2.Text, MessageType.Info)
    '                grdJob.Rows(e.RowIndex).FindControl("objdtterminationdate").Focus()
    '                e.Cancel = True
    '                Exit Try
    '            End If
    '        End If

    '        If objAppExp.IsExistApplicantExperience(strCompCode:=Session("CompCode").ToString _
    '                                                , intComUnkID:=CInt(Session("companyunkid")) _
    '                                                , intApplicantUnkid:=CInt(Session("applicantunkid")) _
    '                                                , intJobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid")) _
    '                                                , strCompanyName:=txtCompany.Trim _
    '                                                , dtJoiningdate:=dtJoinDate _
    '                                                , dtTerminationdate:=dtTerminationDate
    '                                                ) = True Then

    '            ShowMessage(lblgrdJobRowUpdatingMsg1.Text, MessageType.Info)
    '            Exit Try
    '        End If


    '        If clsApplicantExperience.EditApplicantExperience(strCompCode:=Session("CompCode").ToString _
    '                                            , intComUnkID:=CInt(Session("companyunkid")) _
    '                                            , intApplicantUnkid:=CInt(Session("applicantunkid")) _
    '                                            , intJobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid")) _
    '                                            , strEmployerName:=txtEmployer _
    '                                            , strCompanyName:=txtCompany _
    '                                            , strOfficephone:=txtPhone _
    '                                            , strDesignation:=txtDesignation _
    '                                            , dtJoiningdate:=dtJoinDate _
    '                                            , dtTerminationdate:=dtTerminationDate _
    '                                            , strResponsibility:=txtResponsibility _
    '                                            , strAchievements:=txtAchievement _
    '                                            , strLeavingreason:=txtleavingReason _
    '                                            , jobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid"))
    '                                            ) = True Then

    '            ShowMessage(lblgrdJobRowUpdatingMsg2.Text, MessageType.Info)

    '            grdJob.EditIndex = -1
    '            Call FillApplicantExperience()

    '        End If

    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    'Private Sub grdJob_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdJob.RowDeleting
    '    Try
    '        '** To Prevent Buttun Click when Page is Refreshed by User. **
    '        If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
    '        Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

    '        If clsApplicantExperience.DeleteApplicantExperience(strCompCode:=Session("CompCode").ToString _
    '                                                , intComUnkID:=CInt(Session("companyunkid")) _
    '                                                , intApplicantUnkid:=CInt(Session("applicantunkid")) _
    '                                                , intJobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid")) _
    '                                                , jobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid"))
    '                                                ) = True Then

    '            ShowMessage(lblgrdJobRowDeletingMsg1.Text, MessageType.Info)

    '            Call FillApplicantExperience()
    '        End If
    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    Private Sub lvJob_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvJob.ItemDataBound
        Try
            If e.Item.ItemType = ListViewItemType.DataItem AndAlso lvJob.EditIndex = -1 Then
                Dim pnllvEditItemAchievement As Panel = DirectCast(e.Item.FindControl("pnllvEditItemAchievement"), Panel)

                If CBool(Session("JobAchievementMandatory")) = False Then
                    pnllvEditItemAchievement.Visible = False
                End If

                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
                Dim objlblIsGovtJob As HiddenField = DirectCast(e.Item.FindControl("objlblIsGovtJob"), HiddenField)
                If CBool(objlblIsGovtJob.Value) Then
                    Dim objlvbtnedit As LinkButton = DirectCast(e.Item.FindControl("objlvbtnedit"), LinkButton)
                    If objlvbtnedit IsNot Nothing Then objlvbtnedit.Visible = False
                End If
                'S.SANDEEP |04-MAY-2023| -- END

            End If
            If e.Item.ItemType = ListViewItemType.DataItem AndAlso lvJob.EditIndex >= 0 AndAlso e.Item.DataItemIndex = lvJob.EditIndex Then
                Dim pnllvEditItemAchievement As Panel = DirectCast(e.Item.FindControl("pnllvEditItemAchievement"), Panel)

                If CBool(Session("JobAchievementMandatory")) = False Then
                    pnllvEditItemAchievement.Visible = False
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvJob_ItemEditing(sender As Object, e As ListViewEditEventArgs) Handles lvJob.ItemEditing
        Try
            lvJob.EditIndex = e.NewEditIndex
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvJob_ItemUpdating(sender As Object, e As ListViewUpdateEventArgs) Handles lvJob.ItemUpdating
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim txtEmployer As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvEmployer"), TextBox).Text
            Dim txtCompany As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvCompany"), TextBox).Text
            Dim txtPhone As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvPhone"), TextBox).Text
            Dim txtDesignation As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvDesignation"), TextBox).Text
            Dim dtJoinDate As Date = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objdtlvJoinDate"), DateCtrl).GetDate
            Dim dtTerminationDate As Date = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objdtlvterminationdate"), DateCtrl).GetDate
            Dim txtResponsibility As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvResponsibility"), TextBox).Text
            Dim txtAchievement As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvAchievement"), TextBox).Text
            Dim txtleavingReason As String = DirectCast(lvJob.Items(e.ItemIndex).FindControl("objtxtlvleavingReason"), TextBox).Text


            If txtEmployer.Trim = "" AndAlso CBool(Session("EmployerMandatory")) = True Then
                ShowMessage(lblEmployerMsg.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objtxtlvEmployer").Focus()
                e.Cancel = True
                Exit Try
            ElseIf txtCompany.Trim = "" AndAlso CBool(Session("CompanyNameJobHistoryMandatory")) = True Then
                ShowMessage(lblCompNamMsg.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objtxtlvCompany").Focus()
                e.Cancel = True
                Exit Try
            ElseIf txtCompany.Trim = "" AndAlso txtEmployer.Trim = "" Then
                ShowMessage(lblCompNamMsg.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objtxtlvCompany").Focus()
                e.Cancel = True
                Exit Try
            ElseIf txtDesignation.Trim = "" AndAlso CBool(Session("PositionHeldMandatory")) = True Then
                ShowMessage(lblDesignationMsg.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objtxtlvDesignation").Focus()
                e.Cancel = True
                Exit Try
            ElseIf txtAchievement.Trim = "" AndAlso CBool(Session("JobAchievementMandatory")) = True Then
                ShowMessage(lblAchievementMsg.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objtxtlvAchievement").Focus()
                e.Cancel = True
                Exit Try
            ElseIf dtJoinDate = CDate("01-Jan-1900") Then
                ShowMessage(lblDurationMsg1.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objdtlvJoinDate").Focus()
                e.Cancel = True
                Exit Try
                'ElseIf dtTerminationDate = CDate("01-Jan-1900") Then
                '    ShowMessage("Termination Date is mandatory information. Please Enter Valid Termination Date.", MessageType.Info)
                '    lvJob.Items(e.RowIndex).FindControl("objdtterminationdate").Focus()
                '    e.Cancel = True
                '    Exit Try
            ElseIf eZeeDate.convertDate(dtJoinDate) > eZeeDate.convertDate(System.DateTime.Today.Date) Then
                ShowMessage(lblDurationMsg2.Text, MessageType.Info)
                lvJob.Items(e.ItemIndex).FindControl("objdtlvJoinDate").Focus()
                e.Cancel = True
                Exit Try
            ElseIf dtJoinDate <> CDate("01/Jan/1900") Then
                If eZeeDate.convertDate(dtTerminationDate) > eZeeDate.convertDate(System.DateTime.Today.Date) Then
                    ShowMessage(lblTermDateMsg1.Text, MessageType.Info)
                    lvJob.Items(e.ItemIndex).FindControl("objdtlvterminationdate").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            ElseIf dtJoinDate <> CDate("01/Jan/1900") AndAlso dtTerminationDate <> CDate("01/Jan/1900") Then
                If eZeeDate.convertDate(dtTerminationDate) < eZeeDate.convertDate(dtJoinDate) Then
                    ShowMessage(lblTermDateMsg2.Text, MessageType.Info)
                    lvJob.Items(e.ItemIndex).FindControl("objdtlvterminationdate").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            End If

            If objAppExp.IsExistApplicantExperience(strCompCode:=Session("CompCode").ToString _
                                                    , intComUnkID:=CInt(Session("companyunkid")) _
                                                    , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                    , intJobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid")) _
                                                    , strCompanyName:=txtCompany.Trim _
                                                    , dtJoiningdate:=dtJoinDate _
                                                    , dtTerminationdate:=dtTerminationDate
                                                    ) = True Then

                ShowMessage(lblgrdJobRowUpdatingMsg1.Text, MessageType.Info)
                e.Cancel = True
                Exit Try
            End If


            'If clsApplicantExperience.EditApplicantExperience(strCompCode:=Session("CompCode").ToString _
            '                                    , intComUnkID:=CInt(Session("companyunkid")) _
            '                                    , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            '                                    , intJobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid")) _
            '                                    , strEmployerName:=txtEmployer _
            '                                    , strCompanyName:=txtCompany _
            '                                    , strOfficephone:=txtPhone _
            '                                    , strDesignation:=txtDesignation _
            '                                    , dtJoiningdate:=dtJoinDate _
            '                                    , dtTerminationdate:=dtTerminationDate _
            '                                    , strResponsibility:=txtResponsibility _
            '                                    , strAchievements:=txtAchievement _
            '                                    , strLeavingreason:=txtleavingReason _
            '                                    , jobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid"))
            '                                    ) = True Then

            '    ShowMessage(lblgrdJobRowUpdatingMsg2.Text, MessageType.Info)

            '    grdJob.EditIndex = -1
            '    Call FillApplicantExperience()

            'End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsJob_Updating(sender As Object, e As ObjectDataSourceMethodEventArgs) Handles objodsJob.Updating
        Try
            Dim txtEmployer As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvEmployer"), TextBox).Text
            Dim txtCompany As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvCompany"), TextBox).Text
            Dim txtPhone As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvPhone"), TextBox).Text
            Dim txtDesignation As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvDesignation"), TextBox).Text
            Dim dtJoinDate As Date = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objdtlvJoinDate"), DateCtrl).GetDate
            Dim dtTerminationDate As Date = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objdtlvterminationdate"), DateCtrl).GetDate
            Dim txtResponsibility As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvResponsibility"), TextBox).Text
            Dim txtAchievement As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvAchievement"), TextBox).Text
            Dim txtleavingReason As String = DirectCast(lvJob.Items(lvJob.EditIndex).FindControl("objtxtlvleavingReason"), TextBox).Text

            Dim objAppJobHistoryUpdate As New dtoAppJobHistoryUpdate
            With objAppJobHistoryUpdate
                .CompCode = Session("CompCode").ToString
                .ComUnkID = CInt(Session("companyunkid"))
                .ApplicantUnkid = CInt(Session("applicantunkid"))
                .Jobhistorytranunkid = CInt(lvJob.DataKeys(lvJob.EditItem.DataItemIndex).Item("jobhistorytranunkid"))
                .EmployerName = txtEmployer
                .CompanyName = txtCompany
                .Designation = txtDesignation
                .Responsibility = txtResponsibility
                .Joiningdate = dtJoinDate
                .Terminationdate = dtTerminationDate
                .Officephone = txtPhone
                .Leavingreason = txtleavingReason
                .Achievements = txtAchievement
            End With
            e.InputParameters("objAppJobHistoryUpdate") = objAppJobHistoryUpdate

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try

    End Sub

    Private Sub objodsJob_Updated(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles objodsJob.Updated
        Try
            Call FillApplicantExperience(True)
            ShowMessage(lblgrdJobRowUpdatingMsg2.Text, MessageType.Info)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvJob_ItemDeleting(sender As Object, e As ListViewDeleteEventArgs) Handles lvJob.ItemDeleting
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            objodsJob.DeleteParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            objodsJob.DeleteParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            objodsJob.DeleteParameters.Item("intApplicantUnkid").DefaultValue = CInt(Session("applicantunkid")).ToString
            objodsJob.DeleteParameters.Item("intJobhistorytranunkid").DefaultValue = CInt(e.Keys("jobhistorytranunkid")).ToString

            'If clsApplicantExperience.DeleteApplicantExperience(strCompCode:=Session("CompCode").ToString _
            '                                        , intComUnkID:=CInt(Session("companyunkid")) _
            '                                        , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            '                                        , intJobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid")) _
            '                                        , jobhistorytranunkid:=CInt(e.Keys("jobhistorytranunkid"))
            '                                        ) = True Then

            '    ShowMessage(lblgrdJobRowDeletingMsg1.Text, MessageType.Info)

            '    Call FillApplicantExperience()
            'End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsJob_Deleted(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles objodsJob.Deleted
        Try
            Call FillApplicantExperience(True)
                ShowMessage(lblgrdJobRowDeletingMsg1.Text, MessageType.Info)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvJob_ItemCanceling(sender As Object, e As ListViewCancelEventArgs) Handles lvJob.ItemCanceling
        Try
            lvJob.EditIndex = -1
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Button Events "
    Private Sub btnAddJoB_Click(sender As Object, e As EventArgs) Handles btnAddJoB.Click
        'lblError.Visible = False
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            If objAppExp.IsExistApplicantExperience(strCompCode:=Session("CompCode").ToString _
                                                    , intComUnkID:=CInt(Session("companyunkid")) _
                                                    , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                    , intJobhistorytranunkid:=-1 _
                                                    , strCompanyName:=txtCompName.Text _
                                                    , dtJoiningdate:=dtjoindate.GetDate _
                                                    , dtTerminationdate:=dtTerminationDate.GetDate
                                                    ) = True Then

                ShowMessage(lblbtnAddJoBMsg1.Text, MessageType.Info)
                Call FillApplicantExperience()
                txtEmployer.Focus()
                Exit Try
            End If

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            'If objAppExp.AddApplicantExperience(strCompCode:=Session("CompCode").ToString _
            '                                    , intComUnkID:=CInt(Session("companyunkid")) _
            '                                    , intApplicantUnkid:=CInt(Session("applicantunkid")) _
            '                                    , strEmployerName:=txtEmployer.Text.Trim _
            '                                    , strCompanyName:=txtCompName.Text.Trim _
            '                                    , strDesignation:=txtDesignation.Text.Trim _
            '                                    , strResponsibility:=txtResponsebilty.Text.Trim _
            '                                    , dtJoiningdate:=dtjoindate.GetDate _
            '                                    , dtTerminationdate:=dtTerminationDate.GetDate _
            '                                    , strOfficephone:=txtOfficePhone.Text.Trim _
            '                                    , strLeavingreason:=txtLeavingreson.Text.Trim _
            '                                    , strAchievements:=txtAchievement.Text.Trim _
            '                                    , dtCreated_date:=Now
            '                                    ) = True Then

            '    ShowMessage(lblbtnAddJoBMsg2.Text, MessageType.Info)
            'End If

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
            If objAppExp.AddApplicantExperience(strCompCode:=Session("CompCode").ToString _
                                                , intComUnkID:=CInt(Session("companyunkid")) _
                                                , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                , strEmployerName:=txtEmployer.Text.Trim _
                                                , strCompanyName:=txtCompName.Text.Trim _
                                                , strDesignation:=txtDesignation.Text.Trim _
                                                , strResponsibility:=txtResponsebilty.Text.Trim _
                                                , dtJoiningdate:=dtjoindate.GetDate _
                                                , dtTerminationdate:=dtTerminationDate.GetDate _
                                                , strOfficephone:=txtOfficePhone.Text.Trim _
                                                , strLeavingreason:=txtLeavingreson.Text.Trim _
                                                , strAchievements:=txtAchievement.Text.Trim _
                                                                , dtCreated_date:=Now _
                                                                , bnlIsGovJob:=False
                                                ) = True Then
                    'S.SANDEEP |04-MAY-2023| -- START {A1X-833, False} -- END

                ShowMessage(lblbtnAddJoBMsg2.Text, MessageType.Info)
            End If
            Else
                Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("JobHistory")
                Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & lblFileName.Text.Substring(lblFileName.Text.LastIndexOf(".") + 1)
                Dim decSize As Decimal = 0
                Decimal.TryParse(hfDocSize.Value, decSize)
                Dim lstAttach As New List(Of dtoAddAppAttach)
                Dim dtoAttach As dtoAddAppAttach = Nothing
                dtoAttach = New dtoAddAppAttach
                With dtoAttach
                    .CompCode = Session("CompCode").ToString
                    .ComUnkID = CInt(Session("companyunkid"))
                    .ApplicantUnkid = CInt(Session("applicantunkid"))
                    .DocumentUnkid = CInt(ddlAttachType.SelectedValue)
                    .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                    .AttachrefId = CInt(ddlDocType.SelectedValue)
                    .FolderName = strFolder
                    .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                    .Filename = lblFileName.Text
                    .Fileuniquename = strUnkImgName
                    .File_size = CInt(decSize / 1024)
                    .Attached_Date = Now
                    .flUpload = CType(Session(flUpload.ClientID), HttpPostedFile)
                End With
                lstAttach.Add(dtoAttach)

                If objAppExp.AddApplicantExperience_Attachment(strCompCode:=Session("CompCode").ToString _
                                                                , intComUnkID:=CInt(Session("companyunkid")) _
                                                                , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                                , strEmployerName:=txtEmployer.Text.Trim _
                                                                , strCompanyName:=txtCompName.Text.Trim _
                                                                , strDesignation:=txtDesignation.Text.Trim _
                                                                , strResponsibility:=txtResponsebilty.Text.Trim _
                                                                , dtJoiningdate:=dtjoindate.GetDate _
                                                                , dtTerminationdate:=dtTerminationDate.GetDate _
                                                                , strOfficephone:=txtOfficePhone.Text.Trim _
                                                                , strLeavingreason:=txtLeavingreson.Text.Trim _
                                                                , strAchievements:=txtAchievement.Text.Trim _
                                                                , dtCreated_date:=Now _
                                                                , objAddAppAttach:=lstAttach _
                                                                , bnlIsGovJob:=True
                                                ) = True Then

                ShowMessage(lblbtnAddJoBMsg2.Text, MessageType.Info)
            End If

            End If
            'S.SANDEEP |04-MAY-2023| -- END

            Call FillApplicantExperience(True)
            Call Reset_JobHistory()
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Protected Sub flUpload_btnUpload_Click(sender As Object, e As EventArgs) Handles flUpload.btnUpload_Click
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If CInt(ddlDocType.SelectedValue) <= 0 Then
                ShowMessage(lblDocTypeMsg.Text, MessageType.Info)
                ddlDocType.Focus()
                Exit Sub
            ElseIf CInt(ddlAttachType.SelectedValue) <= 0 Then
                ShowMessage(lblAttachTypeMsg.Text, MessageType.Info)
                ddlAttachType.Focus()
                Exit Sub
            End If

            Dim blnInvalidFile As Boolean = False
            Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG", "JPEG", "BMP", "GIF", "PNG"
                    If flUpload.AllowImageFile = False Then
                        blnInvalidFile = True
                    End If
                Case "DOC", "DOCX"
                    If flUpload.AllowDocumentFile = False Then
                        blnInvalidFile = True
                    End If
                Case "PDF"
                Case Else
                    blnInvalidFile = True
                    'ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                    'Exit Try
            End Select

            If blnInvalidFile = True Then
                ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                Exit Try
            End If

            'Dim mintByes As Integer = CInt(flUpload.MaxSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            Dim mintByes As Integer = CInt(flUpload.RemainingSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            If flUpload.FileBytes.LongLength > mintByes Then   ' 6 MB = 6291456 Bytes
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            lblFileName.Text = flUpload.PostedFile.FileName
            lblFileExt.Text = flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
            lblDocSize.Text = Format(((flUpload.FileBytes.LongLength / 1024) / 1024), "0.00") & " MB."
            hfDocSize.Value = CStr(flUpload.FileBytes.LongLength)
            hfFileName.Value = lblFileName.Text
            pnlFile.Visible = True
            hftranid.Value = "0"
            Session(flUpload.ClientID) = flUpload.PostedFile

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'S.SANDEEP |04-MAY-2023| -- END

#End Region

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
#Region " Checkbox Events "
    Protected Sub chkYesNo_CheckedChanged(sender As Object, e As EventArgs) Handles chkYesNo.CheckedChanged
        Try
            If chkYesNo.Checked = True Then
                pnlGovtInfo.Enabled = True
                pnluploadbutton.Enabled = True
            Else
                pnlGovtInfo.Enabled = False
                pnluploadbutton.Enabled = False
                ClearControl()
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region
    'S.SANDEEP |04-MAY-2023| -- END


End Class