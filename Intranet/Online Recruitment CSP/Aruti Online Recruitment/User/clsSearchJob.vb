﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports Org.BouncyCastle.Crypto

Public Class clsSearchJob

#Region " Method Functions "

    Public Function GetApplicantVacancy(ByVal strCompCode As String,
                                        intComUnkID As Integer,
                                        intMasterTypeId As Integer,
                                        intEType As Integer,
                                        blnVacancyType As Boolean,
                                        blnAllVacancy As Boolean,
                                        intDateZoneDifference As Integer,
                                        strVacancyUnkIdLIs As String) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetVacancy"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()

                    'For performance issue on view applicants screen
                    Using c As New SqlCommand("SET ARITHABORT ON", con)
                        c.ExecuteNonQuery()
                    End Using

                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@mastertypeid", SqlDbType.Int)).Value = intMasterTypeId
                        cmd.Parameters.Add(New SqlParameter("@EType", SqlDbType.Int)).Value = intEType
                        cmd.Parameters.Add(New SqlParameter("@VacancyType", SqlDbType.Bit)).Value = blnVacancyType
                        cmd.Parameters.Add(New SqlParameter("@AllVacancy", SqlDbType.Bit)).Value = blnAllVacancy
                        cmd.Parameters.Add(New SqlParameter("@DateZoneDifference", SqlDbType.Int)).Value = intDateZoneDifference
                        cmd.Parameters.Add(New SqlParameter("@VacancyUnkIdList", SqlDbType.NVarChar)).Value = strVacancyUnkIdLIs
                        da.SelectCommand = cmd
                        da.Fill(ds, "VacancyList")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetApplicantVacancies(ByVal strCompCode As String,
                                                intComUnkID As Integer,
                                                intMasterTypeId As Integer,
                                                intEType As Integer,
                                                blnVacancyType As Boolean,
                                                blnAllVacancy As Boolean,
                                                intDateZoneDifference As Integer,
                                                strVacancyUnkIdLIs As String,
                                                intApplicantUnkId As Integer,
                                                Optional blnOnlyCurrent As Boolean = True
                                                ) As DataTable
        Dim ds As New DataSet
        Dim dsApplied As DataSet
        Dim dt As DataTable = Nothing
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetVacancy"

            If strCompCode Is Nothing Then strCompCode = ""
            If strVacancyUnkIdLIs Is Nothing Then strVacancyUnkIdLIs = ""

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@mastertypeid", SqlDbType.Int)).Value = intMasterTypeId
                        cmd.Parameters.Add(New SqlParameter("@EType", SqlDbType.Int)).Value = intEType
                        cmd.Parameters.Add(New SqlParameter("@VacancyType", SqlDbType.Bit)).Value = blnVacancyType
                        cmd.Parameters.Add(New SqlParameter("@AllVacancy", SqlDbType.Bit)).Value = blnAllVacancy
                        cmd.Parameters.Add(New SqlParameter("@DateZoneDifference", SqlDbType.Int)).Value = intDateZoneDifference
                        cmd.Parameters.Add(New SqlParameter("@VacancyUnkIdList", SqlDbType.NVarChar)).Value = strVacancyUnkIdLIs
                        cmd.Parameters.Add(New SqlParameter("@onlycurrent", SqlDbType.Bit)).Value = blnOnlyCurrent

                        da.SelectCommand = cmd
                        da.Fill(ds, "VacancyList")

                        If intApplicantUnkId > 0 Then
                            Dim objApplied As New clsApplicantApplyJob
                            dsApplied = objApplied.GetApplicantAppliedJob(strCompCode:=strCompCode, intComUnkID:=intComUnkID, intApplicantUnkId:=intApplicantUnkId)

                            If dsApplied IsNot Nothing AndAlso dsApplied.Tables(0).Rows.Count > 0 Then
                                Dim str = String.Join(",", dsApplied.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("vacancyid").ToString))
                                For Each dRow As DataRow In ds.Tables(0).Select("vacancyid IN (" & str & ")")
                                    dRow.Item("IsApplied") = True
                                Next
                                ds.AcceptChanges()
                            End If

                        End If
                    End Using
                End Using
            End Using

            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dt = New DataView(ds.Tables(0), "IsApplied = 0", "IsApplied ASC, vacancytitle ASC", DataViewRowState.CurrentRows).ToTable
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return dt
    End Function

    Public Function AppliedVacancy(ByVal strCompCode As String _
                                  , intComUnkID As Integer _
                                  , intApplicantUnkid As Integer _
                                  , intVacancyUnkId As Integer _
                                  , dtEarliest_possible_startdate As Date _
                                  , strComments As String _
                                  , strVacancy_found_out_from As String _
                                  , intrVacancy_found_out_from_UnkId As Integer _
                                  , Optional ByRef intRet_UnkID As Integer = 0
                                  ) As Boolean
        Dim ds As New DataSet
        intRet_UnkID = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            Dim strQ As String = "procAddVacancyApply"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = intVacancyUnkId
                    cmd.Parameters.Add(New SqlParameter("@earliest_possible_startdate", SqlDbType.DateTime)).Value = dtEarliest_possible_startdate
                    cmd.Parameters.Add(New SqlParameter("@comments", SqlDbType.NVarChar)).Value = strComments.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@vacancy_found_out_from", SqlDbType.NVarChar)).Value = strVacancy_found_out_from.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@vacancy_found_out_from_unkid", SqlDbType.Int)).Value = intrVacancy_found_out_from_UnkId
                    'cmd.ExecuteNonQuery()
                    intRet_UnkID = CInt(cmd.ExecuteScalar())
                End Using
            End Using

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    Public Function AddApplicantAttachment(ByVal strCompCode As String _
                                              , intComUnkID As Integer _
                                              , intApplicantUnkid As Integer _
                                              , intDocumentUnkid As Integer _
                                              , intModulerefId As Integer _
                                              , intAttachrefId As Integer _
                                              , strFilepath As String _
                                              , strFilename As String _
                                              , strFileuniquename As String _
                                              , dtAttached_Date As Date _
                                              , intFile_size As Integer _
                                              , intAppvacancytranunkid As Integer
                                              ) As Boolean
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procAddApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = intDocumentUnkid
                    cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModulerefId
                    cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachrefId
                    cmd.Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = strFilepath.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = strFilename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = strFileuniquename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dtAttached_Date
                    cmd.Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = intFile_size
                    cmd.Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intAppvacancytranunkid

                    'S.SANDEEP |21-JUL-2023| -- START
                    'ISSUE/ENHANCEMENT : NOT ADDED THIS
                    cmd.Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = 0
                    'S.SANDEEP |21-JUL-2023| -- END


                    'Pinkal (30-Sep-2023) -- Start
                    ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                    cmd.Parameters.Add(New SqlParameter("@applicantqualiftranunkid", SqlDbType.Int)).Value = 0
                    'Pinkal (30-Sep-2023) -- End

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            'Dim aa = (From p In Cache.AsEnumerable() Where (p.Key.StartsWith("AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            'For Each a In aa
            '    If Cache(a.Key) IsNot Nothing Then
            '        Cache.Remove(a.Key)
            '    End If
            'Next

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    Public Function AddAppVacancy_Attachment(objAddAppVacancy As dtoAddAppVacancy _
                                             , objAddAppAttach As List(Of dtoAddAppAttach) _
                                             , Optional ByRef intRet_UnkID As Integer = 0
                                             ) As Boolean
        Dim ds As New DataSet
        Dim trn As SqlTransaction = Nothing
        intRet_UnkID = 0
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ1 As String = "procAddVacancyApply"
            Dim strQ2 As String = "procAddApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                trn = con.BeginTransaction

                Using cmd As New SqlCommand(strQ1, con)

                    With cmd
                        .Transaction = trn
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = objAddAppVacancy.CompCode
                        .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objAddAppVacancy.ComUnkID
                        .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = objAddAppVacancy.ApplicantUnkid
                        .Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = objAddAppVacancy.VacancyUnkId
                        .Parameters.Add(New SqlParameter("@earliest_possible_startdate", SqlDbType.DateTime)).Value = objAddAppVacancy.Earliest_possible_startdate
                        .Parameters.Add(New SqlParameter("@comments", SqlDbType.NVarChar)).Value = objAddAppVacancy.Comments.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@vacancy_found_out_from", SqlDbType.NVarChar)).Value = objAddAppVacancy.Vacancy_found_out_from.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@vacancy_found_out_from_unkid", SqlDbType.Int)).Value = objAddAppVacancy.Vacancy_found_out_from_UnkId

                        intRet_UnkID = CInt(.ExecuteScalar())

                    End With

                End Using

                If objAddAppAttach.Count > 0 Then

                    For Each dto As dtoAddAppAttach In objAddAppAttach

                        Using cmd As New SqlCommand(strQ2, con)

                            With cmd
                                .Transaction = trn
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = dto.ComUnkID
                                .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = dto.CompCode
                                .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = dto.ApplicantUnkid
                                .Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = dto.DocumentUnkid
                                .Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = dto.ModulerefId
                                .Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = dto.AttachrefId
                                .Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = dto.Filepath.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = dto.Filename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = dto.Fileuniquename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dto.Attached_Date
                                .Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = dto.File_size
                                .Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intRet_UnkID
                                'S.SANDEEP |21-JUL-2023| -- START
                                'ISSUE/ENHANCEMENT : NOT ADDED THIS
                                .Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = 0
                                'S.SANDEEP |21-JUL-2023| -- END

                                'Pinkal (30-Sep-2023) -- Start
                                ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                                .Parameters.Add(New SqlParameter("@applicantqualiftranunkid", SqlDbType.Int)).Value = 0
                                'Pinkal (30-Sep-2023) -- End

                                .ExecuteNonQuery()

                            End With

                        End Using

                    Next

                End If


                'Dim cmd1 As New SqlCommand(strQ1, con)
                'With cmd1
                '    .Transaction = trn
                '    .CommandType = CommandType.StoredProcedure
                '    .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = objAddAppVacancy.CompCode
                '    .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objAddAppVacancy.ComUnkID
                '    .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = objAddAppVacancy.ApplicantUnkid
                '    .Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = objAddAppVacancy.VacancyUnkId
                '    .Parameters.Add(New SqlParameter("@earliest_possible_startdate", SqlDbType.DateTime)).Value = objAddAppVacancy.Earliest_possible_startdate
                '    .Parameters.Add(New SqlParameter("@comments", SqlDbType.NVarChar)).Value = objAddAppVacancy.Comments.Trim.Replace("'", "''")
                '    .Parameters.Add(New SqlParameter("@vacancy_found_out_from", SqlDbType.NVarChar)).Value = objAddAppVacancy.Vacancy_found_out_from.Trim.Replace("'", "''")
                '    .Parameters.Add(New SqlParameter("@vacancy_found_out_from_unkid", SqlDbType.Int)).Value = objAddAppVacancy.Vacancy_found_out_from_UnkId

                '    intRet_UnkID = CInt(.ExecuteScalar())

                'End With

                'If objAddAppAttach.Count > 0 Then

                '    For Each dto As dtoAddAppAttach In objAddAppAttach

                '        Dim cmd2 As New SqlCommand(strQ2, con)
                '        With cmd2
                '            .Transaction = trn
                '            .CommandType = CommandType.StoredProcedure
                '            .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = dto.ComUnkID
                '            .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = dto.CompCode
                '            .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = dto.ApplicantUnkid
                '            .Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = dto.DocumentUnkid
                '            .Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = dto.ModulerefId
                '            .Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = dto.AttachrefId
                '            .Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = dto.Filepath.Trim.Replace("'", "''") & "\" & dto.Fileuniquename.Trim.Replace("'", "''")
                '            .Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = dto.Filename.Trim.Replace("'", "''")
                '            .Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = dto.Fileuniquename.Trim.Replace("'", "''")
                '            .Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dto.Attached_Date
                '            .Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = dto.File_size
                '            .Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intRet_UnkID

                '            .ExecuteNonQuery()

                '        End With

                '    Next

                'End If


                trn.Commit()

            End Using

            Return True
        Catch ex As Exception
            trn.Rollback()
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    Public Function EditApplicantAttachment(ByVal strCompCode As String _
                                              , intComUnkID As Integer _
                                              , intApplicantUnkid As Integer _
                                              , intAttachfiletranUnkid As Integer _
                                              , intDocumentUnkid As Integer _
                                              , intModulerefId As Integer _
                                              , intAttachrefId As Integer _
                                              , strFilepath As String _
                                              , strFilename As String _
                                              , strFileuniquename As String _
                                              , dtAttached_Date As Date _
                                              , intFile_size As Integer _
                                              , intAppvacancytranunkid As Integer
                                              ) As Boolean
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procEditApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@attachfiletranunkid", SqlDbType.Int)).Value = intAttachfiletranUnkid
                    cmd.Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = intDocumentUnkid
                    cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModulerefId
                    cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachrefId
                    cmd.Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = strFilepath.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = strFilename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = strFileuniquename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dtAttached_Date
                    cmd.Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = intFile_size
                    cmd.Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intAppvacancytranunkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            'Dim aa = (From p In Cache.AsEnumerable() Where (p.Key.StartsWith("AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            'For Each a In aa
            '    If Cache(a.Key) IsNot Nothing Then
            '        Cache.Remove(a.Key)
            '    End If
            'Next

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    Public Function DeleteApplicantAttachment(ByVal strCompCode As String _
                                                , intComUnkID As Integer _
                                                , intApplicantUnkid As Integer _
                                                , intAttachfiletranUnkid As Integer
                                                ) As Boolean
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procDeleteApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@attachfiletranunkid", SqlDbType.Int)).Value = intAttachfiletranUnkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            'Dim aa = (From p In Cache.AsEnumerable() Where (p.Key.StartsWith("AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            'For Each a In aa
            '    If Cache(a.Key) IsNot Nothing Then
            '        Cache.Remove(a.Key)
            '    End If
            'Next

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    Public Function IsExistApplicantAttachment(ByVal strCompCode As String _
                                                  , intComUnkID As Integer _
                                                  , intApplicantUnkid As Integer _
                                                  , intAttachfiletranUnkid As Integer _
                                                  , intModuleRefId As Integer _
                                                  , intAttachRefId As Integer _
                                                  , strFilename As String
                                                  ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@attachfiletranunkid", SqlDbType.Int)).Value = intAttachfiletranUnkid
                    cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModuleRefId
                    cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachRefId
                    cmd.Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = strFilename.Trim.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return blnIsExist
    End Function

#End Region

End Class

#Region " Dtos "
Public Class dtoAddAppVacancy
    Public Property CompCode() As String
    Public Property ComUnkID() As Integer
    Public Property ApplicantUnkid() As Integer
    Public Property VacancyUnkId() As Integer
    Public Property Earliest_possible_startdate() As Date
    Public Property Comments() As String
    Public Property Vacancy_found_out_from() As String
    Public Property Vacancy_found_out_from_UnkId() As Integer
    Public ReadOnly Property Ret_UnkID() As Integer
End Class

Public Class dtoAddAppAttach
    Public Property CompCode() As String
    Public Property ComUnkID() As Integer
    Public Property ApplicantUnkid() As Integer
    Public Property DocumentUnkid() As Integer
    Public Property ModulerefId() As Integer
    Public Property AttachrefId() As Integer
    Public Property Filepath() As String
    Public Property FolderName() As String
    Public Property Filename() As String
    Public Property Fileuniquename() As String
    Public Property Attached_Date() As Date
    Public Property File_size() As Integer
    Public Property RetAppvacancytranunkid_UnkID() As Integer
    Public Property flUpload() As HttpPostedFile

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Public Property JobHistorytranunkid_UnkID() As Integer
    'S.SANDEEP |04-MAY-2023| -- END

    'Pinkal (30-Sep-2023) -- Start
    ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
    Public Property ApplicantQualificationtranunkid_UnkID() As Integer
    'Pinkal (30-Sep-2023) -- End

End Class

#End Region