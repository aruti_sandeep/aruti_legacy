﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Runtime.Caching

Public Class clsApplicantReference

    Dim cache As ObjectCache = MemoryCache.Default

#Region " Method Functions "

    Public Function GetApplicantReference(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , intRelation_id As Integer _
                                               , Optional blnRefreshCache As Boolean = False
                                               ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppRef_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_" & intRelation_id.ToString
            Dim dsCache As DataSet = TryCast(Cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetApplicantReference"

                Using con As New SqlConnection(strConn)
                    con.Open()
                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                            cmd.Parameters.Add(New SqlParameter("@relation_id", SqlDbType.Int)).Value = intRelation_id

                            da.SelectCommand = cmd
                            da.Fill(ds, "ApplicantReference")
                        End Using
                    End Using
                End Using

                Cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetAppReference(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , intRelation_id As Integer _
                                               , Optional dtCurrentDate As Date = Nothing
                                               ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantReference"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                        cmd.Parameters.Add(New SqlParameter("@relation_id", SqlDbType.Int)).Value = intRelation_id

                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantReference")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return ds
    End Function

    Public Function IsExistApplicantReference(ByVal strCompCode As String _
                                                , intComUnkID As Integer _
                                                , intApplicantUnkid As Integer _
                                                , intReferenceTranUnkId As Integer _
                                                , strName As String
                                                  ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistApplicantReference"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@referencetranunkid", SqlDbType.Int)).Value = intReferenceTranUnkId
                    cmd.Parameters.Add(New SqlParameter("@name", SqlDbType.NVarChar)).Value = strName.Trim.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return blnIsExist
    End Function

    Public Function AddApplicantReference(ByVal strCompCode As String,
                                          intComUnkID As Integer,
                                          intApplicantUnkid As Integer,
                                          strRefName As String,
                                          strAddress As String,
                                          intCountryUnkId As Integer,
                                          intStateUnkId As Integer,
                                          intCityUnkid As Integer,
                                          strEmail As String,
                                          strGeneder As String,
                                          strPosition As String,
                                          strTelNo As String,
                                          strMobile As String,
                                          intRelationUnkId As Integer,
                                          dtCreated_date As Date
                                           ) As Boolean
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            Dim strQ As String = "procAddApplicantReference"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@name", SqlDbType.NVarChar)).Value = strRefName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@address", SqlDbType.NVarChar)).Value = strAddress.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@countryunkid", SqlDbType.Int)).Value = intCountryUnkId
                    cmd.Parameters.Add(New SqlParameter("@stateunkid", SqlDbType.Int)).Value = intStateUnkId
                    cmd.Parameters.Add(New SqlParameter("@cityunkid", SqlDbType.Int)).Value = intCityUnkid
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.NVarChar)).Value = strGeneder.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@position", SqlDbType.NVarChar)).Value = strPosition.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@telephone_no", SqlDbType.NVarChar)).Value = strTelNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mobile_no", SqlDbType.NVarChar)).Value = strMobile.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@relationunkid", SqlDbType.Int)).Value = intRelationUnkId
                    cmd.Parameters.Add(New SqlParameter("@Syncdatetime", SqlDbType.DateTime)).Value = DBNull.Value
                    cmd.Parameters.Add(New SqlParameter("@created_date", SqlDbType.DateTime)).Value = dtCreated_date

                    cmd.ExecuteNonQuery()
                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppRef_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Update, True)>
    Public Shared Function EditApplicantReference(ByVal strCompCode As String,
                                           intComUnkID As Integer,
                                           intApplicantUnkid As Integer,
                                           intReferenceTranUnkId As Integer,
                                           strRefName As String,
                                           strAddress As String,
                                           intCountryUnkId As Integer,
                                           intStateUnkId As Integer,
                                           intCityUnkid As Integer,
                                           strEmail As String,
                                           intGeneder As Integer,
                                           strPosition As String,
                                           strTelNo As String,
                                           strMobile As String,
                                           intRelationUnkId As Integer
                                           ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            Dim strQ As String = "procEditApplicantReference"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@referencetranunkid", SqlDbType.Int)).Value = intReferenceTranUnkId
                    cmd.Parameters.Add(New SqlParameter("@name", SqlDbType.NVarChar)).Value = strRefName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@address", SqlDbType.NVarChar)).Value = strAddress.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@countryunkid", SqlDbType.Int)).Value = intCountryUnkId
                    cmd.Parameters.Add(New SqlParameter("@stateunkid", SqlDbType.Int)).Value = intStateUnkId
                    cmd.Parameters.Add(New SqlParameter("@cityunkid", SqlDbType.Int)).Value = intCityUnkid
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.Int)).Value = intGeneder
                    cmd.Parameters.Add(New SqlParameter("@position", SqlDbType.NVarChar)).Value = strPosition.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@telephone_no", SqlDbType.NVarChar)).Value = strTelNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mobile_no", SqlDbType.NVarChar)).Value = strMobile.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@relationunkid", SqlDbType.Int)).Value = intRelationUnkId
                    cmd.Parameters.Add(New SqlParameter("@Syncdatetime", SqlDbType.DateTime)).Value = DBNull.Value
                    cmd.ExecuteNonQuery()
                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppRef_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Update, True)>
    Public Shared Function EditAppReference(objAppReferenceUpdate As dtoAppReferenceUpdate _
                                            , referencetranUnkId As Integer
                                           ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procEditApplicantReference"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.CompCode
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.ComUnkID
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.ApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@referencetranunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.ReferenceTranUnkId
                    cmd.Parameters.Add(New SqlParameter("@name", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.RefName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@address", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.Address.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@countryunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.CountryUnkId
                    cmd.Parameters.Add(New SqlParameter("@stateunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.StateUnkId
                    cmd.Parameters.Add(New SqlParameter("@cityunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.CityUnkid
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.Email.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.Int)).Value = objAppReferenceUpdate.Geneder
                    cmd.Parameters.Add(New SqlParameter("@position", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.Position.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@telephone_no", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.TelNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mobile_no", SqlDbType.NVarChar)).Value = objAppReferenceUpdate.Mobile.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@relationunkid", SqlDbType.Int)).Value = objAppReferenceUpdate.RelationUnkId
                    cmd.Parameters.Add(New SqlParameter("@Syncdatetime", SqlDbType.DateTime)).Value = DBNull.Value
                    cmd.ExecuteNonQuery()
                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppRef_" & objAppReferenceUpdate.CompCode & "_" & objAppReferenceUpdate.ComUnkID.ToString & "_" & objAppReferenceUpdate.ApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Delete, True)>
    Public Shared Function DeleteApplicantReference(ByVal strCompCode As String,
                                             intComUnkID As Integer,
                                             intApplicantUnkid As Integer,
                                             intReferenceTranUnkId As Integer,
                                             referencetranunkid As Integer
                                             ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteApplicantReference"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@referencetranunkid", SqlDbType.Int)).Value = intReferenceTranUnkId
                    cmd.ExecuteNonQuery()
                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppRef_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If

            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

#End Region

End Class
