﻿Option Strict On
Imports System.IO

Public Class SendLink
    Inherits Base_Page

    'Private mdtTable As DataTable

#Region " Methods Functions "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetCompanyForCombo("", 0, True)
            With cboCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objApplicant.GetPendingSent()
            With cboLink
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objApplicant.GetPageSize()
            With cboPageSize
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "5"
            End With

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objCompany = Nothing
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub FillList()
        'Dim objApplicant As New clsApplicant
        'Dim dsList As DataSet
        Try
            'Dim sCompCode As String = ""
            'Dim iCompanyunkid As Integer = 0
            'If CInt(cboCompany.SelectedValue) > 0 Then
            '    iCompanyunkid = CInt(cboCompany.SelectedValue)
            'End If
            'dsList = clsApplicant.GetActivationLink(strCompCode:=sCompCode _
            '                                        , intComUnkID:=iCompanyunkid
            '                                        )
            'mdtTable = dsList.Tables(0)

            'grdLink.Columns(0).Visible = True
            'grdLink.Columns(1).Visible = True
            'grdLink.Columns(2).Visible = True
            'grdLink.Columns(3).Visible = True
            'grdLink.DataSource = mdtTable
            'grdLink.DataBind()
            ''grdLink.Columns(0).Visible = False
            'grdLink.Columns(1).Visible = False
            'grdLink.Columns(2).Visible = False
            'grdLink.Columns(3).Visible = False

            'lblCount.Text = "(" & mdtTable.Rows.Count.ToString & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            'objApplicant = Nothing
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In grdLink.Rows
                cb = CType(grdLink.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                cb.Checked = blnCheckAll

                'mdtTable.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
            Next
            'mdtTable.AcceptChanges()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
    '        If chkSelect Is Nothing Then Exit Try

    '        Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            mdtTable.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
    '        End If
    '        mdtTable.AcceptChanges()

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub
#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                'Call FillList()
            Else
                'mdtTable = CType(ViewState("mdtTable"), DataTable)
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SendLink_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ' Me.ViewState.Add("mdtTable", mdtTable)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Combobox Events "

    Private Sub cboCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim objComp As New clsCompany
                dsLink.SelectParameters.Item("strCompCode").DefaultValue = objComp.GetCompanyCode(CInt(cboCompany.SelectedValue))
                objComp = Nothing
            Else
                dsLink.SelectParameters.Item("strCompCode").DefaultValue = ""
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub cboLink_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboLink.SelectedIndexChanged
        Try
            If CInt(cboLink.SelectedValue) >= 0 Then
                dsLink.SelectParameters.Item("isemailsent").DefaultValue = cboLink.SelectedValue.ToString
            Else
                dsLink.SelectParameters.Item("isemailsent").DefaultValue = "-1"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub cboPageSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPageSize.SelectedIndexChanged
        Try
            grdLink.PageSize = CInt(cboPageSize.SelectedValue)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "
    Private Sub grdLink_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLink.RowCommand
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try
            If e.CommandName = "Send" Then

                Dim strCompCode As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("Comp_Code").ToString
                Dim intCompanyunkid As Integer = CInt(grdLink.DataKeys(CInt(e.CommandArgument)).Item("companyunkid").ToString)
                Dim intUnkId As Integer = CInt(grdLink.DataKeys(CInt(e.CommandArgument)).Item("activationlinkunkid"))

                If objApplicant.GetCompanyInfo(intComUnkID:=intCompanyunkid _
                                               , strCompCode:=strCompCode _
                                               , ByRefblnIsActive:=False
                                               ) Then

                    Dim objCompany1 As New clsCompany
                    Session("e_emailsetupunkid") = 0
                    objCompany1.GetEmailSetup(strCompCode, intCompanyunkid)

                    If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString()) _
                                     , strSubject:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("subject").ToString) _
                                     , strBody:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("message").ToString).Replace("&", "&amp;")
                                     ) = False Then

                        'objApplicant.UpdateActivationEmailSent(intUnkId, False, grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString, DateAndTime.Now)
                        ShowMessage("Mail sending failed!", MessageType.Errorr)
                    Else
                        objApplicant.UpdateActivationEmailSent(intUnkId, True, grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString, DateAndTime.Now)
                        grdLink.DataBind()

                        ShowMessage("Mail Sent successfully!")
                    End If


                    clsApplicant.ClearSessionForSAdmin()
                Else

                End If

            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            objMail = Nothing
            'FillList()
        End Try
    End Sub

    Private Sub grdLink_DataBound(sender As Object, e As EventArgs) Handles grdLink.DataBound
        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dsLink_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsLink.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(CType(e.ReturnValue, String), intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdLink.PageIndex = 0
            End If
            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnSendLink_Click(sender As Object, e As EventArgs) Handles btnSendLink.Click
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                ShowMessage("Please select Company.")
                Exit Try
            End If
            'If mdtTable.Select("IsChecked = 1").Length <= 0 Then
            '    ShowMessage("Please tick atleast one email to send link.")
            '    Exit Sub
            'End If

            'Dim dRow() As DataRow = mdtTable.Select("IsChecked")

            'For Each drRow As DataRow In dRow

            '    Dim strCompCode As String = drRow.Item("Comp_Code").ToString
            '    Dim intCompanyunkid As Integer = CInt(drRow.Item("companyunkid"))

            '    If objApplicant.GetCompanyInfo(intComUnkID:=intCompanyunkid _
            '                                   , strCompCode:=strCompCode _
            '                                   , ByRefblnIsActive:=False
            '                                   ) Then

            '        If objMail.SendEmail(strToEmail:=Server.HtmlDecode(drRow.Item("LoweredEmail").ToString) _
            '                         , strSubject:=Server.HtmlDecode(drRow.Item("subject").ToString) _
            '                         , strBody:=Server.HtmlDecode(drRow.Item("message").ToString)
            '                         ) Then

            '        End If

            '        clsApplicant.ClearSessionForSAdmin()
            '    End If
            'Next

            Dim blnChecked As Boolean = False
            Dim blnSuccess As Boolean = True
            Dim strCode As String = (New clsCompany).GetCompanyCode(CInt(cboCompany.SelectedValue))

            If objApplicant.GetCompanyInfo(intComUnkID:=CInt(cboCompany.SelectedValue) _
                                                   , strCompCode:=strCode _
                                                   , ByRefblnIsActive:=False
                                                   ) = False Then

                Exit Try
            End If

            Dim objCompany1 As New clsCompany
            Session("e_emailsetupunkid") = 0
            objCompany1.GetEmailSetup(strCode, CInt(cboCompany.SelectedValue))

            For Each dgRow As GridViewRow In grdLink.Rows

                Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)

                If cb IsNot Nothing AndAlso cb.Checked Then

                    Dim intUnkId As Integer = CInt(grdLink.DataKeys(dgRow.RowIndex).Item("activationlinkunkid"))
                    Dim strCompCode As String = grdLink.DataKeys(dgRow.RowIndex).Item("Comp_Code").ToString
                    Dim intCompanyunkid As Integer = CInt(grdLink.DataKeys(dgRow.RowIndex).Item("companyunkid"))

                    'If objApplicant.GetCompanyInfo(intComUnkID:=intCompanyunkid _
                    '                               , strCompCode:=strCompCode _
                    '                               , ByRefblnIsActive:=False
                    '                               ) Then

                    If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString) _
                                         , strSubject:=Server.HtmlDecode(grdLink.DataKeys(dgRow.RowIndex).Item("subject").ToString) _
                                         , strBody:=Server.HtmlDecode(grdLink.DataKeys(dgRow.RowIndex).Item("message").ToString).Replace("&", "&amp;")
                                         ) = False Then

                        blnSuccess = False
                        'objApplicant.UpdateActivationEmailSent(intUnkId, False, grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString, DateAndTime.Now)
                    Else
                        objApplicant.UpdateActivationEmailSent(intUnkId, True, grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString, DateAndTime.Now)
                    End If


                    'clsApplicant.ClearSessionForSAdmin()
                    'End If

                    blnChecked = True
                End If

            Next
            clsApplicant.ClearSessionForSAdmin()

            If blnChecked = False Then
                ShowMessage("Please tick atleast one email to send link.")
            ElseIf blnSuccess = False Then
                ShowMessage("Mail sending failed!", MessageType.Errorr)
            Else
                grdLink.DataBind()
                ShowMessage("Mail Sent successfully!")
            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            objMail = Nothing
        End Try
    End Sub

    Private Sub btnActivateApplicants_Click(sender As Object, e As EventArgs) Handles btnActivateApplicants.Click
        Dim objApplicant As New clsApplicant
        Try
            Dim blnChecked As Boolean = False

            For Each dgRow As GridViewRow In grdLink.Rows

                Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)

                If cb IsNot Nothing AndAlso cb.Checked Then

                    Dim intUnkId As Integer = CInt(grdLink.DataKeys(dgRow.RowIndex).Item("activationlinkunkid"))
                    Dim strCompCode As String = grdLink.DataKeys(dgRow.RowIndex).Item("Comp_Code").ToString
                    Dim intCompanyunkid As Integer = CInt(grdLink.DataKeys(dgRow.RowIndex).Item("companyunkid"))
                    Dim strLoweredEmail As String = grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString
                    Dim strUserUnkId As String = grdLink.DataKeys(dgRow.RowIndex).Item("UserId").ToString

                    'If objApplicant.ActivateApplicant(intCompanyId:=intCompanyunkid _
                    '                               , strCompanyCode:=strCompCode _
                    '                               , strEmail:=strLoweredEmail _
                    '                               , strUserID:=strUserUnkId
                    '                               ) = False Then

                    '    objApplicant.UpdateActivationEmailSent(intUnkId, False, grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString, DateAndTime.Now)
                    'Else
                    '    objApplicant.UpdateActivationEmailSent(intUnkId, True, grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString, DateAndTime.Now)

                    'End If
                    If objApplicant.ActivateUser(strCompCode:=strCompCode, intComUnkID:=intCompanyunkid, strEmail:=strLoweredEmail, strUserID:=strUserUnkId) <= 0 Then

                    End If

                    blnChecked = True
                End If

            Next

            If blnChecked = False Then
                ShowMessage("Please tick atleast one applicant to activate.")
            Else
                grdLink.DataBind()
                ShowMessage("Applicants activated successfully!")
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub btnActivateApplicantsAll_Click(sender As Object, e As EventArgs) Handles btnActivateApplicantsAll.Click
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                ShowMessage("Please select Company.")
                Exit Try
            End If
            Dim sCompCode As String = ""
            Dim iCompanyunkid As Integer = 0
            If CInt(cboCompany.SelectedValue) > 0 Then
                iCompanyunkid = CInt(cboCompany.SelectedValue)
            End If

            Dim ds As DataSet = clsApplicant.GetActivationLink(strCompCode:=sCompCode _
                                                    , intComUnkID:=iCompanyunkid, strEmail:="", intIsApproved:=0, startRowIndex:=0, intPageSize:=5000, strSortExpression:="", isemailsent:=CInt(cboLink.SelectedValue))

            For Each dsRow As DataRow In ds.Tables(0).Rows

                Dim intUnkId As Integer = CInt(dsRow.Item("activationlinkunkid"))
                Dim strCompCode As String = dsRow.Item("Comp_Code").ToString
                Dim intCompanyunkid As Integer = CInt(dsRow.Item("companyunkid"))
                Dim strLoweredEmail As String = dsRow.Item("LoweredEmail").ToString
                Dim strUserUnkId As String = dsRow.Item("UserId").ToString

                'If objApplicant.ActivateApplicant(intCompanyId:=intCompanyunkid _
                '                                   , strCompanyCode:=strCompCode _
                '                                   , strEmail:=strLoweredEmail _
                '                                   , strUserID:=strUserUnkId
                '                                   ) = False Then

                '    objApplicant.UpdateActivationEmailSent(intUnkId, False, strLoweredEmail, DateAndTime.Now)
                'Else
                '    objApplicant.UpdateActivationEmailSent(intUnkId, True, strLoweredEmail, DateAndTime.Now)

                'End If
                If objApplicant.ActivateUser(strCompCode:=strCompCode, intComUnkID:=intCompanyunkid, strEmail:=strLoweredEmail, strUserID:=strUserUnkId) <= 0 Then

                End If

            Next

            grdLink.DataBind()
            ShowMessage("Applicants Activated successfully!")

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            objMail = Nothing
        End Try
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            'Call FillList()
            grdLink.DataBind()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub lnkExcel_Click(sender As Object, e As EventArgs) Handles lnkExcel.Click
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                ShowMessage("Please select company.")
                Exit Try
            End If

            'Dim strSorting As String = " CASE WHEN rcapplicant_master.Syncdatetime IS NULL THEN 1  ELSE 2 END, rcapplicant_master.firstname + ISNULL(rcapplicant_master.othername, '''''''') + rcapplicant_master.surname "
            'Select Case grdLink.SortExpression.ToString.ToUpper
            '    Case "CASE WHEN RCAPPLICANT_MASTER.TITLEUNKID > 0 THEN TITLE.NAME ELSE '' END"
            '        strSorting = " CASE WHEN rcapplicant_master.titleunkid > 0 THEN title.name ELSE '''''''' END "

            '    Case "RCAPPLICANT_MASTER.FIRSTNAME + ISNULL(RCAPPLICANT_MASTER.OTHERNAME, '') + RCAPPLICANT_MASTER.SURNAME"
            '        strSorting = " rcapplicant_master.firstname + ISNULL(rcapplicant_master.othername, '''''''') + rcapplicant_master.surname "

            '    Case "CASE RCAPPLICANT_MASTER.GENDER WHEN 1 THEN 1 WHEN 2 THEN 0 ELSE 0 END"
            '        strSorting = " " & grdLink.SortExpression & " "

            '    Case "EMAIL"
            '        strSorting = " " & grdLink.SortExpression & " "

            '    Case "PRESENT_MOBILENO"
            '        strSorting = " " & grdLink.SortExpression & " "

            '    Case "ISNULL(VACANCY.NAME, '')"
            '        strSorting = " ISNULL(Vacancy.name, '''''''') "

            '    Case "RCAPPLICANT_MASTER.TRANSACTIONDATE"
            '        strSorting = " rcapplicant_master.transactiondate "

            '    Case "RCAPPLICANT_MASTER.SYNCDATETIME"
            '        strSorting = " rcapplicant_master.Syncdatetime "

            'End Select

            'If grdLink.SortDirection = SortDirection.Descending Then
            '    strSorting += " DESC "
            'End If
            Dim objComp As New clsCompany
            Dim ds As DataSet = clsApplicant.GetActivationLink(strCompCode:=objComp.GetCompanyCode(CInt(cboCompany.SelectedValue)), intComUnkID:=CInt(cboCompany.SelectedValue), strEmail:=txtEmail.Text, intIsApproved:=0, startRowIndex:=0, intPageSize:=99999999, strSortExpression:="", isemailsent:=CInt(cboLink.SelectedValue))

            If ds.Tables(0).Rows.Count <= 0 Then
                ShowMessage("Sorry, There is no recort to be exported.")
                Exit Try
            End If

            If ds.Tables(0).Columns.Contains("activationlinkunkid") = True Then
                ds.Tables(0).Columns.Remove("activationlinkunkid")
            End If
            If ds.Tables(0).Columns.Contains("LoweredEmail") = True Then
                ds.Tables(0).Columns("LoweredEmail").ColumnName = "Email"
            End If
            If ds.Tables(0).Columns.Contains("subject") = True Then
                ds.Tables(0).Columns.Remove("subject")
            End If
            If ds.Tables(0).Columns.Contains("message") = True Then
                ds.Tables(0).Columns.Remove("message")
            End If
            If ds.Tables(0).Columns.Contains("link") = True Then
                ds.Tables(0).Columns.Remove("link")
            End If
            If ds.Tables(0).Columns.Contains("IsChecked") = True Then
                ds.Tables(0).Columns.Remove("IsChecked")
            End If
            If ds.Tables(0).Columns.Contains("RowNumber") = True Then
                ds.Tables(0).Columns.Remove("RowNumber")
            End If


            'Create a dummy GridView
            Dim GridView1 As New GridView()

            GridView1.AllowPaging = False

            GridView1.DataSource = ds.Tables(0)

            GridView1.DataBind()



            Response.Clear()

            Response.Buffer = True

            'Response.ContentType = "application/x-msexcel"
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("content-disposition", "attachment;filename=Applicants.xls")

            Response.Charset = ""

            'Response.ContentType = "application/vnd.ms-excel"


            Dim sw As New StringWriter()

            Dim hw As New HtmlTextWriter(sw)



            For i As Integer = 0 To GridView1.Rows.Count - 1

                'Apply text style to each Row

                GridView1.Rows(i).Attributes.Add("class", "textmode")

            Next

            GridView1.RenderControl(hw)



            'style to format numbers to string

            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"

            Response.Write(style)
            Response.Output.Write(sw.ToString)
            Response.Flush()
            'Response.End()

            ''Response.Flush()
            ''System.IO.File.Delete(sw.ToString())

            'Response.End()

            'For Each dsRow As DataRow In ds.Tables(0).Rows
            '    sw.WriteLine(String.Join(",", dsRow.ItemArray.Select(Function(r) r.ToString().Replace(",", ""))))
            'Next
            'Response.Write(sw.ToString())
            'sw.Flush()
            Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()
            'Response.End()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class