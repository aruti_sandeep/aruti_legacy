﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
'Imports eZeeCommonLib

Public Class clsSACommon

#Region " General Methods "

    Public Function RunFullScript(strConn As String) As Boolean
        Dim StrQ As String = ""
        Try
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim sb As New SqlConnectionStringBuilder(strConn)

            Dim arrSep() As String
            Dim arrScript() As String

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand()

                    StrQ = "USE " & sb.InitialCatalog '"USE arutihrms "
                    cmd.Connection = con
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = StrQ
                    cmd.ExecuteNonQuery()

                    StrQ = My.Resources.full_script
                    arrSep = New String() {vbNewLine & "GO" & vbCrLf}
                    arrScript = StrQ.Split(arrSep, StringSplitOptions.None)

                    For Each str As String In arrScript

                        If str.Trim.Length <= 0 Then Continue For
                        If con.State = ConnectionState.Closed Then
                            con.ConnectionString = strConn
                            con.Open()
                        End If

                        cmd.CommandType = CommandType.Text
                        cmd.Connection = con
                        cmd.CommandText = str.Trim
                        cmd.ExecuteNonQuery()

                    Next

                End Using
            End Using


            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function RunAlterScript(strConn As String) As Boolean
        Dim StrQ As String = ""
        Try
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim sb As New SqlConnectionStringBuilder(strConn)

            Dim arrSep() As String
            Dim arrScript() As String

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand()

                    StrQ = "USE " & sb.InitialCatalog '"USE arutihrms "
                    cmd.Connection = con
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = StrQ
                    cmd.ExecuteNonQuery()

                    StrQ = My.Resources.alter_script
                    arrSep = New String() {vbNewLine & "GO" & vbCrLf}
                    arrScript = StrQ.Split(arrSep, StringSplitOptions.None)

                    For Each str As String In arrScript

                        If str.Trim.Length <= 0 Then Continue For
                        If con.State = ConnectionState.Closed Then
                            con.ConnectionString = strConn
                            con.Open()
                        End If

                        cmd.CommandType = CommandType.Text
                        cmd.Connection = con
                        cmd.CommandText = str
                        cmd.ExecuteNonQuery()

                    Next

                End Using
            End Using

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function CreateAruti_SA_User() As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = ""
            Dim strUser As String = "aruti_sa"

            strQ = "IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = '" & strUser & "') " &
                       "BEGIN " &
                       "    USE [master] " &
                       "    CREATE LOGIN [" & strUser & "] WITH PASSWORD=N'" & clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee") & "', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'bulkadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'dbcreator' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'diskadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'processadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'securityadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'serveradmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'setupadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'sysadmin' " &
                       "END "

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.ExecuteNonQuery()

                    End Using
                End Using
            End Using

            Return True

        Catch ex As Exception
            'Sohail (30 Oct 2018) - Start
            'Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current,,, False)
            'Sohail (30 Oct 2018) - End
            Return False
        End Try
    End Function

    Public Function CreateRoles() As Boolean
        Try
            If Roles.RoleExists("superadmin") = False Then
                Roles.CreateRole("superadmin")
            End If

            If Roles.RoleExists("admin") = False Then
                Roles.CreateRole("admin")
            End If

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function CreateSAdminUser() As Boolean
        Dim createStatus As MembershipCreateStatus
        Try
            Dim arrSAdmin() As String = {"aruti@aruti.com", "anjan@ezeetechnosys.com", "sohail@ezeetechnosys.com", "andrew.muga@npktechnologies.com", "anatory@npktechnologies.com", "sandeep@ezeetechnosys.com", "pinkal@ezeetechnosys.com"}

            For Each strName As String In arrSAdmin

                If strName.Trim.Length <= 0 Then Continue For

                If Membership.GetUser(strName) Is Nothing Then
                    Membership.CreateUser(strName, clsSecurity.Decrypt("MqDIqE8UEGuxHVwHY7TUZDkN3iAYPLjCJCuH9lGEA08=", "ezee"), strName, "sp", "64", True, createStatus)

                    Select Case createStatus

                        Case MembershipCreateStatus.Success
                            'lblResult.ForeColor = Color.Green
                            'lblResult.Text = "The user account was successfully created"
                            Exit Select

                        Case MembershipCreateStatus.DuplicateUserName
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The user with the same UserName already exists!"
                            Exit Select

                        Case MembershipCreateStatus.DuplicateEmail
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The user with the same email address already exists!"
                            Exit Select

                        Case MembershipCreateStatus.InvalidEmail
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The email address you provided is invalid."
                            Exit Select
                        Case MembershipCreateStatus.InvalidAnswer
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The security answer was invalid."
                            Exit Select

                        Case MembershipCreateStatus.InvalidPassword
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "The password you provided is invalid. It must be 7 characters long and have at least 1 special character."
                            Exit Select
                        Case Else
                            'lblResult.ForeColor = Color.Red
                            'lblResult.Text = "There was an unknown error; the user account was NOT created."
                            Exit Select

                    End Select
                End If

                If Roles.IsUserInRole(strName, "superadmin") = False Then
                    Roles.AddUserToRole(strName, "superadmin")
                End If
            Next

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function EncryptMailPassword() As Boolean
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Using con As New SqlConnection(strConn)

                con.Open()

                Dim strQ As String = "procGetNULLAdminEmail"

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure

                    intCount = CInt(cmd.ExecuteScalar().ToString())

                    If intCount > 0 Then

                        strQ = "procGetListNULLAdminEmail"
                        cmd.Connection = con
                        cmd.Parameters.Clear()
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.CommandText = strQ

                        Using da As New SqlDataAdapter
                            da.SelectCommand = cmd
                            da.Fill(ds, "CompanyInfo")
                        End Using

                        For Each dsRow As DataRow In ds.Tables(0).Rows

                            strQ = "UPDATE cfcompany_info SET password = @password, admin_email = email WHERE companyunkid = @companyunkid "

                            cmd.Connection = con
                            cmd.Parameters.Clear()
                            cmd.CommandType = CommandType.Text
                            cmd.CommandText = strQ
                            cmd.Parameters.Add(New SqlParameter("@password", SqlDbType.NVarChar)).Value = dsRow.Item("password").ToString
                            'cmd.Parameters.Add(New SqlParameter("@password", SqlDbType.NVarChar)).Value = clsSecurity.Encrypt(dsRow.Item("password").ToString, "ezee")
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = CInt(dsRow.Item("companyunkid"))

                            cmd.ExecuteNonQuery()

                        Next

                    Else

                        Return True
                    End If

                End Using
            End Using


            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        Finally
            ds = Nothing
        End Try
    End Function

    Public Function GetSuperAdminEmailSettings() As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetSuperadminEmailsetting"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        da.SelectCommand = cmd
                        da.Fill(ds, "EmailSettings")

                    End Using
                End Using
            End Using



            For Each dsRow As DataRow In ds.Tables("EmailSettings").Rows

                HttpContext.Current.Session("emailsetupunkid") = dsRow.Item("emailsetupunkid")
                HttpContext.Current.Session("sadminsendername") = dsRow.Item("sendername")
                HttpContext.Current.Session("sadminsenderaddress") = dsRow.Item("senderaddress")
                HttpContext.Current.Session("sadminreference") = dsRow.Item("reference")
                HttpContext.Current.Session("sadminmailserverip") = dsRow.Item("mailserverip")
                HttpContext.Current.Session("sadminmailserverport") = dsRow.Item("mailserverport")
                HttpContext.Current.Session("sadminusername") = dsRow.Item("username").ToString
                If dsRow.Item("password").ToString.Trim = "" Then
                    HttpContext.Current.Session("sadminpassword") = dsRow.Item("password").ToString
                Else
                    HttpContext.Current.Session("sadminpassword") = clsSecurity.Decrypt(dsRow.Item("password").ToString, "ezee")
                End If
                HttpContext.Current.Session("sadminisloginssl") = dsRow.Item("isloginssl")
                HttpContext.Current.Session("sadminmail_body") = dsRow.Item("mail_body")
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                HttpContext.Current.Session("sadminemail_type") = dsRow.Item("email_type")
                HttpContext.Current.Session("sadminews_url") = dsRow.Item("ews_url")
                HttpContext.Current.Session("sadminews_domain") = dsRow.Item("ews_domain")
                'Sohail (30 Nov 2017) -- End

            Next

            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function SaveSuperAdminEmailSettings(intEmailSetupUnkId As Integer _
                                               , strSenderName As String _
                                               , strSenderAddress As String _
                                               , strReference As String _
                                               , strMailServerIP As String _
                                               , intMailServerPort As Integer _
                                               , strUserName As String _
                                               , strPassword As String _
                                               , blnIsLoginSSL As Boolean _
                                               , strMailBody As String _
                                               , intEmail_Type As Integer _
                                               , strEWS_URL As String _
                                               , strEWS_Domain As String
                                               ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procUpdateSuperadminEmailsetting"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@emailsetupunkid", SqlDbType.Int)).Value = intEmailSetupUnkId
                    cmd.Parameters.Add(New SqlParameter("@sendername", SqlDbType.NVarChar)).Value = strSenderName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@senderaddress", SqlDbType.NVarChar)).Value = strSenderAddress.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@reference", SqlDbType.NVarChar)).Value = strReference.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mailserverip", SqlDbType.NVarChar)).Value = strMailServerIP.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mailserverport", SqlDbType.Int)).Value = intMailServerPort
                    cmd.Parameters.Add(New SqlParameter("@username", SqlDbType.NVarChar)).Value = strUserName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@password", SqlDbType.NVarChar)).Value = clsSecurity.Encrypt(strPassword.Trim.Replace("'", "''"), "ezee")
                    cmd.Parameters.Add(New SqlParameter("@isloginssl", SqlDbType.Bit)).Value = blnIsLoginSSL
                    cmd.Parameters.Add(New SqlParameter("@mail_body", SqlDbType.NVarChar)).Value = strMailBody.Trim.Replace("'", "''")
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    cmd.Parameters.Add(New SqlParameter("@email_type", SqlDbType.Int)).Value = intEmail_Type
                    cmd.Parameters.Add(New SqlParameter("@ews_url", SqlDbType.NVarChar)).Value = strEWS_URL.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ews_domain", SqlDbType.NVarChar)).Value = strEWS_Domain.Trim.Replace("'", "''")
                    'Sohail (30 Nov 2017) -- End

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    Public Shared Function TruncateErrorLog() As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procTruncateErrorLog"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Shared Function ShrinkDatabase(strDBName As String) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procShrinkDatabase"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@dbname", SqlDbType.NVarChar)).Value = strDBName.Trim.Replace("'", "''")

                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function GetCompanyCodeFromEmail(ByVal strEmail As String _
                                        , strUserId As String
                                        ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCompanyCodeFromEmail"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                        cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.NVarChar)).Value = strUserId

                        da.SelectCommand = cmd
                        da.Fill(ds, "Companycode")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    'Sohail (14 Mar 2018) -- Start
    'Enhancement – Allow to Reset password from the other companies already logged in before in 70.2.
    Public Function GetCompanyCodeFromApplicant(ByVal strEmail As String _
                                        , strUserId As String
                                        ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetCompanyCodeFromApplicant"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                        cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.NVarChar)).Value = strUserId

                        da.SelectCommand = cmd
                        da.Fill(ds, "Companycode")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function
    'Sohail (14 Mar 2018) -- End

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID. (Allow admin to access user panel)
    Public Function AdminToApplicant() As Boolean
        Dim objApplicant As New clsApplicant
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procAdminToApplicant"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "Admin")

                        For Each dsRow As DataRow In ds.Tables(0).Rows
                            If objApplicant.ActivateUser(strCompCode:=dsRow.Item("Comp_Code").ToString _
                                                      , intComUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                      , strEmail:=dsRow.Item("email").ToString _
                                                      , strUserID:=dsRow.Item("UserId").ToString _
                                                      , blnSkipActivation:=True
                                                      ) > 0 Then


                            End If
                        Next
                    End Using
                End Using
            End Using

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        Finally
        End Try
    End Function
    'Sohail (16 Aug 2019) -- End

    Public Shared Function DeleteExpiredSessions() As Boolean
        Try
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            Dim strQ As String = "ArutiState.dbo.DeleteExpiredSessions"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.ExecuteNonQuery()
                End Using
            End Using

            Return True

        Catch ex As Exception
            'Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function
#End Region

#Region " Data Object Methods "

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetAdminUsers(ByVal strEmail As String _
                                        , strUserId As String _
                                        , strComp_Code As String _
                                        , intCompanyunkid As Integer _
                                        , startRowIndex As Integer _
                                        , intPageSize As Integer _
                                        , strSortExpression As String
                                        ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetAdminUsers"

            startRowIndex = Convert.ToInt32(startRowIndex / intPageSize) + 1

            If strEmail Is Nothing Then strEmail = ""
            If strUserId Is Nothing Then strUserId = ""
            If strComp_Code Is Nothing Then strComp_Code = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                        cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.NVarChar)).Value = strUserId
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strComp_Code
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyunkid
                        cmd.Parameters.Add(New SqlParameter("@CurrentPage", SqlDbType.Int)).Value = startRowIndex
                        cmd.Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int)).Value = intPageSize
                        cmd.Parameters.Add(New SqlParameter("@SortExpression", SqlDbType.NVarChar)).Value = strSortExpression.Trim.Replace("'", "''")

                        da.SelectCommand = cmd
                        da.Fill(ds, "AdminUsers")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetAdminUsersCount(ByVal strEmail As String _
                                             , strUserId As String _
                                             , strComp_Code As String _
                                             , intCompanyunkid As Integer _
                                             , startRowIndex As Integer _
                                             , intPageSize As Integer _
                                             , strSortExpression As String
                                             ) As Integer
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetAdminUsersTotal"

            If strEmail Is Nothing Then strEmail = ""
            If strUserId Is Nothing Then strUserId = ""
            If strComp_Code Is Nothing Then strComp_Code = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                        cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.NVarChar)).Value = strUserId
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strComp_Code
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyunkid

                        Dim intCnt As Object = cmd.ExecuteScalar()

                        intCount = DirectCast(intCnt, Integer)

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return intCount
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetDashboard(ByVal strComp_Code As String _
                                             , intCompanyunkid As Integer
                                             ) As DataSet
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetDashboard"

            If strComp_Code Is Nothing Then strComp_Code = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        'For performance issue on view applicants screen
                        Using c As New SqlCommand("SET ARITHABORT ON", con)
                            c.ExecuteNonQuery()
                        End Using
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strComp_Code
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyunkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "Dashboard")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    '<DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Function EditAdminUsers(intActivationLinkUnkId As Integer,
                                   strCompCode As String,
                                   intComUnkID As Integer,
                                   strEmail As String,
                                   strUserId As String,
                                   blnCanViewErrorLog As Boolean,
                                   blnCanTruncateErrorLog As Boolean,
                                   blnCanShrinkDataBase As Boolean,
                                   blnCanRunFullScript As Boolean,
                                   blnCanRunAlterScript As Boolean,
                                   blnIsDeleted As Boolean,
                                   strAuditEmail As String,
                                   dtAuditDateTime As Date
                                   ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procEditAdminUsers"

            If strEmail Is Nothing Then strEmail = ""
            If strUserId Is Nothing Then strUserId = ""
            If strCompCode Is Nothing Then strCompCode = ""
            If strAuditEmail Is Nothing Then strAuditEmail = ""

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@activationlinkunkid", SqlDbType.Int)).Value = intActivationLinkUnkId
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@UserId", SqlDbType.NVarChar)).Value = strUserId.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@cvel", SqlDbType.Bit)).Value = blnCanViewErrorLog
                    cmd.Parameters.Add(New SqlParameter("@ctel", SqlDbType.Bit)).Value = blnCanTruncateErrorLog
                    cmd.Parameters.Add(New SqlParameter("@csdb", SqlDbType.Bit)).Value = blnCanShrinkDataBase
                    cmd.Parameters.Add(New SqlParameter("@crfs", SqlDbType.Bit)).Value = blnCanRunFullScript
                    cmd.Parameters.Add(New SqlParameter("@cras", SqlDbType.Bit)).Value = blnCanRunAlterScript
                    cmd.Parameters.Add(New SqlParameter("@isdeleted", SqlDbType.Bit)).Value = blnIsDeleted
                    cmd.Parameters.Add(New SqlParameter("@auditemail", SqlDbType.NVarChar)).Value = strAuditEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@auditdatetime", SqlDbType.Date)).Value = dtAuditDateTime

                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function DeleteMembershiUser(intActivationLinkUnkId As Integer,
                                        strUserName As String,
                                        blnIsDeleted As Boolean,
                                        strAuditEmail As String,
                                        dtAuditDateTime As Date
                                        ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteMembershipUser"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@activationlinkunkid", SqlDbType.Int)).Value = intActivationLinkUnkId
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strUserName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@isdeleted", SqlDbType.Bit)).Value = blnIsDeleted
                    cmd.Parameters.Add(New SqlParameter("@auditemail", SqlDbType.NVarChar)).Value = strAuditEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@auditdatetime", SqlDbType.Date)).Value = dtAuditDateTime
                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function
#End Region

End Class
