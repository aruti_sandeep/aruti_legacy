﻿Option Strict On

Public Class SendResetPwd
    Inherits Base_Page

#Region " Methods Functions "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetCompanyForCombo("", 0, True)
            With cboCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objCompany = Nothing
        End Try
    End Sub

    Private Sub FillList()
        'Dim objApplicant As New clsApplicant
        'Dim dsList As DataSet
        Try
            'dsList = objApplicant.GetResetPasswordLink(strCompCode:=Session("CompCode").ToString _
            '                                            , intComUnkID:=CInt(Session("companyunkid"))
            '                                            )

            ''grdLink.Columns(0).Visible = True
            ''grdLink.Columns(1).Visible = True
            'grdLink.Columns(2).Visible = True
            'grdLink.Columns(3).Visible = True
            'grdLink.DataSource = dsList.Tables(0)
            'grdLink.DataBind()
            ''grdLink.Columns(0).Visible = False
            ''grdLink.Columns(1).Visible = False
            'grdLink.Columns(2).Visible = False
            'grdLink.Columns(3).Visible = False

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            'objApplicant = Nothing
        End Try
    End Sub

#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()
                'Call FillList()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SendResetPwd_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Combobox Events "

    Private Sub cboCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim objComp As New clsCompany
                dsLink.SelectParameters.Item("strCompCode").DefaultValue = objComp.GetCompanyCode(CInt(cboCompany.SelectedValue))
                objComp = Nothing
            Else
                dsLink.SelectParameters.Item("strCompCode").DefaultValue = ""
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "
    Private Sub grdLink_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLink.RowCommand
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try
            If e.CommandName = "Send" Then

                'Dim strCompCode As String = grdLink.Rows(CInt(e.CommandArgument)).Cells(0).Text
                'Dim intCompanyunkid As Integer = CInt(grdLink.Rows(CInt(e.CommandArgument)).Cells(1).Text)
                Dim strCompCode As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("Comp_Code").ToString
                Dim intCompanyunkid As Integer = CInt(grdLink.DataKeys(CInt(e.CommandArgument)).Item("companyunkid").ToString)

                If intCompanyunkid > 0 Then

                    If objApplicant.GetCompanyInfo(intComUnkID:=intCompanyunkid _
                                               , strCompCode:=strCompCode _
                                               , ByRefblnIsActive:=False
                                               ) Then

                        Dim objCompany1 As New clsCompany
                        Session("e_emailsetupunkid") = 0
                        objCompany1.GetEmailSetup(strCompCode, intCompanyunkid)

                        If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString()) _
                                         , strSubject:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("subject").ToString()) _
                                         , strBody:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("message").ToString()).Replace("&", "&amp;")
                                         ) Then


                            grdLink.DataBind()
                            ShowMessage("Mail Sent successfully!")
                        Else
                            ShowMessage("Mail sending failed!", MessageType.Errorr)
                        End If

                    Else

                    End If

                Else
                    Dim objSA As New clsSACommon
                    If objSA.GetSuperAdminEmailSettings() Then

                        If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString()) _
                                         , strSubject:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("subject").ToString()) _
                                         , strBody:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("message").ToString()) _
                                         , blnAddLogo:=False _
                                         , blnSuperAdmin:=True
                                         ) Then

                            grdLink.DataBind()
                            ShowMessage("Mail Sent successfully!")
                        Else
                            ShowMessage("Mail sending failed!", MessageType.Errorr)
                        End If

                    Else

                    End If

                End If


            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            FillList()
        End Try
    End Sub

    Private Sub dsLink_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsLink.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(CType(e.ReturnValue, String), intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdLink.PageIndex = 0
            End If
            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class