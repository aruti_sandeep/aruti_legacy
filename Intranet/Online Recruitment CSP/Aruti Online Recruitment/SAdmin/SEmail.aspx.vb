﻿Option Strict On

'Imports eZeeCommonLib

Public Class SEmail
    Inherits Base_Page

#Region " Private Function "

    Private Function IsValidated() As Boolean
        Try

            If txtSenderName.Text.Trim = "" Then
                ShowMessage("Please Enter Sender Name", MessageType.Errorr)
                txtSenderName.Focus()
                Return False
            ElseIf txtSenderAddress.Text.Trim = "" Then
                ShowMessage("Please Enter Sender Email", MessageType.Errorr)
                txtSenderAddress.Focus()
                Return False
            ElseIf txtPassword.Text.Trim = "" Then
                ShowMessage("Please Enter Password", MessageType.Errorr)
                txtPassword.Focus()
                Return False
            ElseIf txtMailServer.Text.Trim = "" Then
                ShowMessage("Please Enter SMTP Mail Server.", MessageType.Errorr)
                txtMailServer.Focus()
                Return False
            ElseIf txtMailServerPort.Text.Trim = "" Then
                ShowMessage("Please Enter Mail Server Port.", MessageType.Errorr)
                txtMailServerPort.Focus()
                Return False
            End If

            If clsApplicant.IsValidEmail(txtSenderAddress.Text) = False Then
                ShowMessage("Sender Email address is not valid", MessageType.Errorr)
                txtSenderAddress.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub GetValue()
        'Dim objSAdmin As New clsSACommon
        'Dim dsList As DataSet
        Try
            'dsList = objSAdmin.GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            'If dsList.Tables(0).Rows.Count > 0 Then
            txtSenderName.Text = Session("sadminsendername").ToString
            txtSenderAddress.Text = Session("sadminusername").ToString
            txtPassword.Text = Session("sadminpassword").ToString
            txtMailServer.Text = Session("sadminmailserverip").ToString
            txtMailServerPort.Text = Session("sadminmailserverport").ToString
            chkIsSSL.Checked = CBool(Session("sadminisloginssl"))
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            If CInt(Session("sadminemail_type")) = 0 Then
                radSMTP.Checked = True
                radEWS.Checked = False
            Else
                radSMTP.Checked = False
                radEWS.Checked = True
            End If
            Call radSMTP_CheckedChanged(radSMTP, New System.EventArgs())
            txtEWS_URL.Text = Session("sadminews_url").ToString()
            txtEWS_Domain.Text = Session("sadminews_domain").ToString()
            'Sohail (30 Nov 2017) -- End

            'End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Page Events "
    Private Sub SEmail_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call GetValue()
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Button Events "

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim objSAdmin As New clsSACommon
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            Dim intPort As Integer
            Integer.TryParse(txtMailServerPort.Text, intPort)

            If objSAdmin.SaveSuperAdminEmailSettings(intEmailSetupUnkId:=CInt(Session("emailsetupunkid")) _
                                                     , strSenderName:=txtSenderName.Text _
                                                     , strSenderAddress:=txtSenderAddress.Text _
                                                     , strReference:="" _
                                                     , strMailServerIP:=txtMailServer.Text _
                                                     , intMailServerPort:=intPort _
                                                     , strUserName:=txtSenderAddress.Text _
                                                     , strPassword:=txtPassword.Text _
                                                     , blnIsLoginSSL:=chkIsSSL.Checked _
                                                     , strMailBody:="" _
                                                     , intEmail_Type:=If(radSMTP.Checked = True, 0, 1) _
                                                     , strEWS_URL:=txtEWS_URL.Text _
                                                     , strEWS_Domain:=txtEWS_Domain.Text
                                                     ) = True Then

                Call objSAdmin.GetSuperAdminEmailSettings()

                ShowMessage("Email Settings Saved Successfully!", MessageType.Info)
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Radio Buttons Events "

    Private Sub radSMTP_CheckedChanged(sender As Object, e As EventArgs) Handles radSMTP.CheckedChanged
        Try
            If radSMTP.Checked = True Then
                mvEmail_Type.ActiveViewIndex = 0
            Else
                mvEmail_Type.ActiveViewIndex = 1
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub radEWS_CheckedChanged(sender As Object, e As EventArgs) Handles radEWS.CheckedChanged
        Try
            If radEWS.Checked = True Then
                mvEmail_Type.ActiveViewIndex = 1
            Else
                mvEmail_Type.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class