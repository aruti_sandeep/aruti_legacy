﻿Option Strict On
Imports System.Data.SqlClient
'Imports Microsoft.Security.Application

Public Class Site2
    Inherits System.Web.UI.MasterPage

    'Sohail (21 Nov 2017) -- Start
    'Enhancement - 70.1 - OWASP changes.
    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String
    'Sohail (21 Nov 2017) -- End

#Region " Public Enum "
    Public Enum MessageType
        Success
        Errorr
        Info
        Warning
    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("email") Is Nothing Then
                'If Session.IsNewSession = True OrElse Session("email") Is Nothing Then
                ShowMessage("Sorry, Session is expired!", Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/SLogin.aspx", MessageType.Info)
                Exit Try
            End If

            If IsPostBack = False Then
                'lblUserName.Text = Sanitizer.GetSafeHtmlFragment(Session("email").ToString)
                lblUserName.Text = AntiXss.AntiXssEncoder.HtmlEncode(Session("email").ToString, True)
                LoginStatus1.LogoutPageUrl = "~/SLogin.aspx"
            End If

            If Session("email") Is Nothing OrElse Session("email").ToString.Trim = "" Then
                Response.Redirect("~/SLogin.aspx", False)
                Exit Sub
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub Site2_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try
            'Sohail (21 Nov 2017) -- Start
            'Enhancement - 70.1 - OWASP changes.                
            'The code below helps to protect against XSRF attacks
            Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
            Dim requestCookieGuidValue As Guid
            If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then

                'Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value
                Page.ViewStateUserKey = _antiXsrfTokenValue

            Else

                'Generate a New Anti-XSRF token And save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N")
                Page.ViewStateUserKey = _antiXsrfTokenValue

                Dim responseCookie = New HttpCookie(AntiXsrfTokenKey) With {
                    .HttpOnly = True,
                    .Value = _antiXsrfTokenValue,
                    .Path = "/;sameSite=Strict"
                    }

                If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then

                    responseCookie.Secure = True
                End If
                Response.Cookies.Set(responseCookie)
            End If

            Dim AspxAuthCookie = Request.Cookies(".ASPXAUTH")
            If AspxAuthCookie IsNot Nothing AndAlso Request.IsSecureConnection Then
                AspxAuthCookie.HttpOnly = True
                AspxAuthCookie.Secure = True
                AspxAuthCookie.Path = "/;sameSite=Strict"
                Response.Cookies.Set(AspxAuthCookie)
            End If

            AddHandler Page.PreLoad, AddressOf Site2_PreLoad
            'Sohail (21 Nov 2017) -- End

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (21 Nov 2017) -- Start
    'Enhancement - 70.1 - OWASP changes.  
    Private Sub Site2_PreLoad(sender As Object, e As EventArgs)
        Try
            If Not IsPostBack Then

                'Set Anti-XSRF token
                ViewState(AntiXsrfTokenKey) = Page.ViewStateUserKey
                ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)

            Else

                'Validate the Anti-XSRF token
                If ViewState(AntiXsrfTokenKey).ToString() <> _antiXsrfTokenValue Then
                    Throw New InvalidOperationException("Validation of Anti-XSRF token failed.")
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Sohail (21 Nov 2017) -- End

    Private Sub lnkRunAlterFullScript_Click(sender As Object, e As EventArgs) Handles lnkRunAlterFullScript.Click
        Dim objSA As New clsSACommon
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            If objSA.RunAlterScript(strConn) = True Then
                ShowMessage("Script Run Successfully!", MessageType.Info)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lnkRunFullScript_Click(sender As Object, e As EventArgs) Handles lnkRunFullScript.Click
        Dim objSA As New clsSACommon
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            If objSA.RunFullScript(strConn) = True Then
                Call objSA.AdminToApplicant()
                Call objSA.CreateAruti_SA_User()
                ShowMessage("Script Run Successfully!", MessageType.Info)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lnkTruncateErrorLog_Click(sender As Object, e As EventArgs) Handles lnkTruncateErrorLog.Click
        Try
            If clsSACommon.TruncateErrorLog() = True Then
                ShowMessage("Error Log truncated Successfully!", MessageType.Info)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lnkShrinkDatabase_Click(sender As Object, e As EventArgs) Handles lnkShrinkDatabase.Click
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim ssb As New SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings("paydb").ToString)
            Dim ssb As New SqlConnectionStringBuilder(HttpContext.Current.Session("ConnectionString").ToString())
            'Sohail (30 Nov 2017) -- End
            If clsSACommon.ShrinkDatabase(ssb.InitialCatalog) = True Then
                ShowMessage("Database Shrunk Successfully!", MessageType.Info)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


    Public Sub ShowMessage(strMessage As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(dialogItself){
                        dialogItself.close();
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Public Sub ShowMessage(strMessage As String, strRedirectLink As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(){
                        window.location.href='" & strRedirectLink & "';
                        return false;
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Try
            FormsAuthentication.SignOut()
            Session.Abandon()

            Response.Redirect("~/SLogin.aspx", False)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try

    End Sub

End Class