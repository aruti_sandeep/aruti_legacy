﻿Public Class Details
    Inherits Base_Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                txtDetailCopy.Text = Session("CopyDetails")
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/SAdmin/CompanyInfo.aspx", False)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
End Class