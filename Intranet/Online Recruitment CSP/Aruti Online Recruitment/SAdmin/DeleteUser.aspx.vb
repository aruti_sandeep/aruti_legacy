﻿Option Strict On

Public Class DeleteUser
    Inherits Base_Page

#Region " Methods Functions "

    Private Sub FillCombo()

        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub FillList()

        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    'Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
    '    Try
    '        Dim cb As CheckBox
    '        For Each gvr As GridViewRow In grdLink.Rows
    '            cb = CType(grdLink.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
    '            cb.Checked = blnCheckAll

    '        Next

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
    '        If chkSelectAll Is Nothing Then Exit Try

    '        Call CheckAllEmployee(chkSelectAll.Checked)

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub


#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()
                dsLink.SelectParameters.Item("isemailsent").DefaultValue = "-1"
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub DeleteUser_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Datagridview Events "
    Private Sub grdLink_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLink.RowCommand
        Dim objSA As New clsSACommon
        Try
            If e.CommandName = "DeleteApplicant" Then

                Dim intActivationUnkId As Integer = 0
                Dim intCompanyunkid As Integer = 0
                Dim strCompCode As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("Comp_Code").ToString
                Integer.TryParse(grdLink.DataKeys(CInt(e.CommandArgument)).Item("activationlinkunkid").ToString, intActivationUnkId)
                Integer.TryParse(grdLink.DataKeys(CInt(e.CommandArgument)).Item("companyunkid").ToString, intCompanyunkid)
                Dim strEmail As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString

                'If Membership.GetUser(strEmail) IsNot Nothing Then
                '    Membership.DeleteUser(strEmail)

                '    grdLink.DataBind()
                '    ShowMessage("Applicant deleted successfully!")
                'End If
                If objSA.DeleteMembershiUser(intActivationLinkUnkId:=intActivationUnkId,
                                         strUserName:=strEmail,
                                         blnIsDeleted:=True,
                                         strAuditEmail:=Session("email").ToString,
                                         dtAuditDateTime:=Now
                                         ) = True Then

                    grdLink.DataBind()
                    ShowMessage("User Deleted Successfully!", MessageType.Info)
                    txtEmail.Text = ""

                    Call FillList()
                End If

            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objSA = Nothing
        End Try
    End Sub

    Private Sub grdLink_DataBound(sender As Object, e As EventArgs) Handles grdLink.DataBound
        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dsLink_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsLink.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(CType(e.ReturnValue, String), intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdLink.PageIndex = 0
            End If

            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    'Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
    '    Try


    '        Dim blnChecked As Boolean = False

    '        For Each dgRow As GridViewRow In grdLink.Rows

    '            Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)

    '            If cb IsNot Nothing AndAlso cb.Checked Then

    '                Dim intUnkId As Integer = 0
    '                Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("activationlinkunkid").ToString, intUnkId)
    '                Dim strCompCode As String = grdLink.DataKeys(dgRow.RowIndex).Item("Comp_Code").ToString
    '                Dim intCompanyunkid As Integer = 0
    '                Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("companyunkid").ToString, intCompanyunkid)
    '                Dim strEmail As String = grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString

    '                If Membership.GetUser(strEmail) IsNot Nothing Then
    '                    Membership.DeleteUser(strEmail)
    '                End If

    '                blnChecked = True
    '            End If

    '        Next

    '        If blnChecked = False Then
    '            ShowMessage("Please tick atleast one email to delete.")
    '        Else
    '            grdLink.DataBind()
    '            ShowMessage("Applicants deleted successfully!")
    '        End If


    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    Finally
    '    End Try
    'End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            'Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub


#End Region

End Class