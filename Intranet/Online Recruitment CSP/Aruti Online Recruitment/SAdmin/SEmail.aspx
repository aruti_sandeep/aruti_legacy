﻿<%@ Page Title="Email Setup" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="SEmail.aspx.vb" Inherits="Aruti_Online_Recruitment.SEmail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        $(document).ready(function () {
            setEvent();
        });

        $(document).ready(function () {
            $$('btnAddReference').on('click', function () {
                return IsValidate();
            });
        });

        function IsValidate() {
            if ($$('txtSenderName').val().trim() == '') {
                ShowMessage('Sender Name cannot be blank.', 'MessageType.Errorr');
                $$('txtRefAddress').focus();
                return false;
            }
            else if ($$('txtSenderAddress').val().trim() == '') {
                ShowMessage('Sender Email cannot be blank.', 'MessageType.Errorr');
                $$('txtSenderAddress').focus();
                return false;
            }
            else if (emailTest($$('txtSenderAddress').val())) {
                ShowMessage('Sender Email address is not valid.', 'MessageType.Errorr');
                $$('txtSenderAddress').focus();
                return false;
            }



            else if ($$('txtPassword').val().trim() == '') {
                ShowMessage('Password can not be Blank.', 'MessageType.Errorr');
                $$('txtPassword').focus();
                return false;
            }

            else if ($$('txtMailServer').val().trim() == '') {
                ShowMessage('SMTP Mail Server cannot be blank.', 'MessageType.Errorr');
                $$('txtMailServer').focus();
                return false;
            }

            else if ($$('txtMailServerPort').val().trim() == '') {
                ShowMessage('Mail Server Post cannot be blank.', 'MessageType.Errorr');
                $$('txtMailServerPort').focus();
                return false;
            }

            else if ($$('txtEWS_URL').val().trim() == '') {
                ShowMessage('EWS URL cannot be blank.', 'MessageType.Errorr');
                $$('txtEWS_URL').focus();
                return false;
            }

            else if ($$('txtEWS_Domain').val().trim() == '') {
                ShowMessage('EWS Domain cannot be blank.', 'MessageType.Errorr');
                $$('txtEWS_Domain').focus();
                return false;
            }

            return true;
        }

        function emailTest(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return !regex.test(email);
        }

        function setEvent() {
            $("[id*=radSMTP]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });
            $("[id*=radEWS]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                setEvent()
            })
        }
</script>

    <div class="card">
        <div class="card-header">
            <h4>Email Settings</h4>
        </div>

        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>


                    <div class="form-group row">

                        <div class="col-md-4">
                            <asp:Label ID="lblSenderName" runat="server" Text="Sender Name:"></asp:Label>
                            <asp:TextBox ID="txtSenderName" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                            <%-- <asp:RequiredFieldValidator ID="rtfSenderName" runat="server" Display="None"
                                ControlToValidate="txtSenderName" ErrorMessage="Sender Name cannot be blank "
                                ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblSenderAddress" runat="server" Text="Sender Email:"></asp:Label>
                            <asp:TextBox ID="txtSenderAddress" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                ControlToValidate="txtSenderAddress" ErrorMessage="Sender Email cannot be blank "
                                ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            <%--<asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="txtSenderAddress"
                                ErrorMessage="Sender Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SaveEmail" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                        </div>

                        <div class="col-md-4">
                            <%--<asp:Label ID="lblReference" runat="server" Text="Reference:"></asp:Label>
                <asp:TextBox ID="txtReference" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="PersonalInfo"></asp:TextBox>--%>
                            <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="50" TextMode="Password" ValidationGroup="SaveEmail"></asp:TextBox>
                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
                                ControlToValidate="txtPassword" ErrorMessage="Password cannot be blank "
                                ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <asp:RadioButton ID="radSMTP" runat="server" Text="SMTP" Checked="true" AutoPostBack="false" GroupName="EmailType" />
                        </div>

                        <div class="col-md-6">
                            <asp:RadioButton ID="radEWS" runat="server" Text="EWS" AutoPostBack="false" GroupName="EmailType" />
                        </div>
                    </div>

                    <asp:MultiView ID="mvEmail_Type" runat="server">
                        <asp:View ID="vwSMTP" runat="server">
                            <div class="form-group row">

                                <div class="col-md-4">
                                    <asp:Label ID="lblMailServer" runat="server" Text="Mail Server (SMTP):"></asp:Label>
                                    <asp:TextBox ID="txtMailServer" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="None"
                                        ControlToValidate="txtMailServer" ErrorMessage="SMTP Mail Server cannot be blank "
                                        ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>

                                <div class="col-md-4">
                                    <asp:Label ID="lblMailServerPort" runat="server" Text="Mail Server Port:"></asp:Label>
                                    <asp:TextBox ID="txtMailServerPort" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None"
                                        ControlToValidate="txtMailServerPort" ErrorMessage="Mail Server Post cannot be blank "
                                        ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtMailServerPort" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                </div>

                                <%--<div class="col-md-3">
                                <asp:Label ID="lblReference" runat="server" Text="Sender Name:"></asp:Label>
                                <asp:TextBox ID="txtReference" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                            </div>--%>
                                <div class="col-md-4">
                                    <asp:CheckBox ID="chkIsSSL" runat="server" Text="SSL" CssClass="d-block" />
                                </div>

                            </div>
                        </asp:View>

                        <asp:View ID="vwEWS" runat="server">

                            <div class="form-group row">

                                <div class="col-md-4">
                                    <asp:Label ID="lblEWS_URL" runat="server" Text="EWS URL:"></asp:Label>
                                    <asp:TextBox ID="txtEWS_URL" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="None"
                                        ControlToValidate="txtEWS_URL" ErrorMessage="EWS URL cannot be blank "
                                        ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>

                                <div class="col-md-4">
                                    <asp:Label ID="lblEWS_Domain" runat="server" Text="EWS Domain:"></asp:Label>
                                    <asp:TextBox ID="txtEWS_Domain" runat="server" CssClass="form-control" MaxLength="50" ValidationGroup="SaveEmail"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="None"
                                        ControlToValidate="txtEWS_Domain" ErrorMessage="EWS Domain cannot be blank "
                                        ValidationGroup="SaveEmail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </div>

                                <div class="col-md-4">
                                    <asp:CheckBox ID="CheckBox1" runat="server" Text="SSL" Style="display: block;" />
                                </div>

                            </div>

                        </asp:View>
                    </asp:MultiView>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

            <%-- <div class="form-group row">
                <div class="col-md-12" style="text-align: center;">
                    <asp:ValidationSummary
                        HeaderText="You must enter a value in the following fields:"
                        DisplayMode="BulletList"
                        EnableClientScript="true"
                        runat="server" ValidationGroup="SaveEmail" Style="color: red" />
                </div>
            </div>--%>
        </div>

        <div class="card-footer">
            <div class="form-group row">

                <div class="col-md-4">
                </div>

                <div class="col-md-4">
                    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary btn-block" Text="Update" ValidationGroup="SaveEmail" />
                </div>

                <div class="col-md-4">
                </div>

            </div>
        </div>
    </div>
</asp:Content>
