﻿Option Strict On

Public Class CompanyInfo
    Inherits Base_Page

#Region " Methods Functions "

    Private Sub FillList()
        'Dim objCompany As New clsCompany
        'Dim dsList As DataSet
        Try
            'dsList = objCompany.GetCompany(strCompCode:="" _
            '                               , intComUnkID:=0 _
            '                               , blnOnlyActive:=True
            '                               )

            'Dim strFilter As String = " 1 = 1 "

            'If txtCompanyID.Text.Trim <> "" Then
            '    strFilter &= " AND companyunkid = " & txtCompanyID.Text.Trim & " "
            'End If

            'If txtCompCode.Text.Trim <> "" Then
            '    strFilter &= " AND company_code like '%" & txtCompCode.Text.Trim & "%' "
            'End If

            'If txtCompName.Text.Trim <> "" Then
            '    strFilter &= " AND company_name like '%" & txtCompName.Text.Trim & "%' "
            'End If

            'If txtEmail.Text.Trim <> "" Then
            '    strFilter &= " AND email like '%" & txtEmail.Text.Trim & "%' "
            'End If

            'If txtAuthenticationCode.Text.Trim <> "" Then
            '    strFilter &= " AND authentication_code like '%" & txtAuthenticationCode.Text.Trim & "%' "
            'End If

            'If txtDatabaseName.Text.Trim <> "" Then
            '    strFilter &= " AND database_name like '%" & txtDatabaseName.Text.Trim & "%' "
            'End If

            'If txtWebsiteURL.Text.Trim <> "" Then
            '    strFilter &= " AND websiteurl like '%" & txtWebsiteURL.Text.Trim & "%' "
            'End If

            'If txtWebsiteURLInternal.Text.Trim <> "" Then
            '    strFilter &= " AND websiteurlinternal like '%" & txtWebsiteURLInternal.Text.Trim & "%' "
            'End If

            'Dim dtView As DataView = dsList.Tables(0).DefaultView
            'dtView.RowFilter = strFilter

            ''grdCompanyInfo.Columns(4).Visible = True
            ''grdCompanyInfo.Columns(6).Visible = True
            ''grdCompanyInfo.Columns(9).Visible = True
            ''grdCompanyInfo.Columns(3).Visible = True
            'grdCompanyInfo.DataSource = dtView
            'grdCompanyInfo.DataBind()
            ''grdCompanyInfo.Columns(4).Visible = False
            ''grdCompanyInfo.Columns(6).Visible = False
            ''grdCompanyInfo.Columns(9).Visible = False
            ''grdCompanyInfo.Columns(3).Visible = False
            'lblCount.Text = "(" & grdCompanyInfo.Rows.Count.ToString & ")"
            odsCompany.SelectParameters.Item("strCompCode").DefaultValue = ""
            odsCompany.SelectParameters.Item("intComUnkID").DefaultValue = "0"
            odsCompany.SelectParameters.Item("blnOnlyActive").DefaultValue = True.ToString
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            'objCompany = Nothing
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If txtCompCode.Text.Trim = "" Then
                ShowMessage("Company code is compulsory information, it can not be blank.", MessageType.Info)
                txtCompCode.Focus()
                Return False
            ElseIf txtCompName.Text.Trim = "" Then
                ShowMessage("Company name is compulsory information, it can not be blank.", MessageType.Info)
                txtCompName.Focus()
                Return False
            ElseIf txtEmail.Text.Trim = "" Then
                ShowMessage("Email is compulsory information, it can not be blank.", MessageType.Info)
                txtEmail.Focus()
                Return False
            ElseIf txtDatabaseName.Text.Trim = "" Then
                ShowMessage("Database name is compulsory information, it can not be blank.", MessageType.Info)
                txtDatabaseName.Focus()
                Return False
            End If

            If clsApplicant.IsValidEmail(txtEmail.Text) = False Then
                ShowMessage("Please enter proper email.", MessageType.Info)
                txtEmail.Focus()
                Return False
            End If


            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub Reset_CompanyInfo()
        Try
            txtCompanyID.Text = ""
            txtCompCode.Text = ""
            txtCompName.Text = ""
            txtEmail.Text = ""
            'txtDatabaseName.Text = ""
            txtWebsiteURL.Text = ""
            txtWebsiteURLInternal.Text = ""
            txtAuthenticationCode.Text = ""
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Session("CopyDetails") = ""

                Call FillList()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub CompanyInfo_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Button Event(S) "
    Private Sub btnAddCompany_Click(sender As Object, e As EventArgs) Handles btnAddCompany.Click
        Dim objCompany As New clsCompany
        Try

            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            If objCompany.IsExistCompanyInfo(strCompCode:=Session("CompCode").ToString,
                                                      intComUnkID:=0
                                                    ) = True Then

                grdCompanyInfo.DataBind()
                ShowMessage("Sorry, Selected company is already added to the list.", MessageType.Info)
                Call FillList()
                txtCompCode.Focus()
                Exit Try
            End If


            If objCompany.AddCompanyInfo(strCompCode:=txtCompCode.Text,
                                         strCompanyName:=txtCompName.Text,
                                         strEmail:=txtEmail.Text.Trim,
                                         strDBName:=txtDatabaseName.Text,
                                         intUserUnkId:=1,
                                         dtCompanyAdded:=Now,
                                         dtCompanyupdated:=Now,
                                         strAuthenticationCode:="",
                                         strWesiteurlExternal:="Login.aspx?ext=7",
                                         strWesiteurlInternal:="Login.aspx?ext=9",
                                         blnIsactive:=True,
                                         strVirtualDirectory:=txtVirtualDirectory.Text) Then
                ShowMessage("Company Saved Successfully!", MessageType.Info)
            End If

            Call Reset_CompanyInfo()
            odsCompany.FilterExpression = ""
            grdCompanyInfo.DataBind()
            Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            'Call FillList()
            Dim strFilter As String = " 1 = 1 "

            If txtCompanyID.Text.Trim <> "" Then
                strFilter &= " AND companyunkid = " & txtCompanyID.Text.Trim & " "
            End If

            If txtCompCode.Text.Trim <> "" Then
                strFilter &= " AND company_code like '%" & txtCompCode.Text.Trim & "%' "
            End If

            If txtCompName.Text.Trim <> "" Then
                strFilter &= " AND company_name like '%" & txtCompName.Text.Trim & "%' "
            End If

            If txtEmail.Text.Trim <> "" Then
                strFilter &= " AND email like '%" & txtEmail.Text.Trim & "%' "
            End If

            If txtAuthenticationCode.Text.Trim <> "" Then
                strFilter &= " AND authentication_code like '%" & txtAuthenticationCode.Text.Trim & "%' "
            End If

            If txtDatabaseName.Text.Trim <> "" Then
                strFilter &= " AND database_name like '%" & txtDatabaseName.Text.Trim & "%' "
            End If

            If txtWebsiteURL.Text.Trim <> "" Then
                strFilter &= " AND websiteurl like '%" & txtWebsiteURL.Text.Trim & "%' "
            End If

            If txtWebsiteURLInternal.Text.Trim <> "" Then
                strFilter &= " AND websiteurlinternal like '%" & txtWebsiteURLInternal.Text.Trim & "%' "
            End If

            If txtVirtualDirectory.Text.Trim <> "" Then
                strFilter &= " AND virtual_directory like '%" & txtVirtualDirectory.Text.Trim & "%' "
            End If

            odsCompany.FilterExpression = strFilter
            grdCompanyInfo.DataBind()
            lblCount.Text = "(" & grdCompanyInfo.Rows.Count & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Try
            Call Reset_CompanyInfo()
            'Call FillList()
            odsCompany.FilterExpression = ""
            grdCompanyInfo.DataBind()
            lblCount.Text = "(" & grdCompanyInfo.Rows.Count & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    Private Sub grdCompanyInfo_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdCompanyInfo.RowCancelingEdit
        Try
            grdCompanyInfo.EditIndex = -1
            'Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdCompanyInfo_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdCompanyInfo.RowEditing
        Try
            grdCompanyInfo.EditIndex = e.NewEditIndex
            'Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdCompanyInfo_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdCompanyInfo.RowUpdating
        Dim objCompany As New clsCompany
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim intComoanyunkid As Integer = 0

            Integer.TryParse(DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objlblCompanyunkid"), Label).Text, intComoanyunkid)

            Dim strCompcode, strCompname, strEmail, strExtUrl, strIntUrl, strAuthCode, strDBName, strVirtualDirectory As String
            strCompcode = "" : strCompname = "" : strEmail = "" : strExtUrl = "" : strIntUrl = "" : strAuthCode = "" : strDBName = "" : strVirtualDirectory = ""

            strCompcode = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtCompanycode"), Label).Text
            strCompname = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtCompanyname"), TextBox).Text
            strEmail = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtEmail"), TextBox).Text
            strExtUrl = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtExternalURL"), TextBox).Text
            strIntUrl = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtInternalURL"), TextBox).Text
            strAuthCode = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtAuthCode"), TextBox).Text
            strDBName = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtDBName"), TextBox).Text
            strVirtualDirectory = DirectCast(grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtVirtual_Directory"), TextBox).Text

            If strCompcode.Trim = "" Then
                ShowMessage("Company code Name is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtCompanycode").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strCompname.Trim = "" Then
                ShowMessage("Company name Name is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtCompanyname").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strEmail.Trim = "" Then
                ShowMessage("Email Name is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtEmail").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strExtUrl.Trim = "" Then
                ShowMessage("External URL is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtExternalURL").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strIntUrl.Trim = "" Then
                ShowMessage("Internal URL is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtInternalURL").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strAuthCode.Trim = "" Then
                ShowMessage("Authentication code is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtAuthCode").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strDBName.Trim = "" Then
                ShowMessage("Database name is compulsory information, it can not be blank.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtDBName").Focus()
                e.Cancel = True
                Exit Try
            End If

            If clsApplicant.IsValidEmail(strEmail) = False Then
                ShowMessage("Please enter proper email.", MessageType.Info)
                grdCompanyInfo.Rows(e.RowIndex).FindControl("objtxtEmail").Focus()
                e.Cancel = True
                Exit Try
            End If
            'If objCompany.IsExistCompanyInfo(strCompCode:=strCompcode,
            '                                          intComUnkID:=intComoanyunkid
            '                                          ) = True Then

            '    ShowMessage("Sorry, Selected company code and name is already added to the list.", MessageType.Info)
            '    Exit Try
            'End If

            If objCompany.EditCompanyInfo(strCompCode:=strCompcode,
                                          strCompanyName:=strCompname,
                                          intComUnkID:=intComoanyunkid,
                                          strEmail:=strEmail,
                                          strDBName:=strDBName,
                                          intUserUnkId:=1,
                                          dtCompanyAdded:=Now,
                                          dtCompanyupdated:=Now,
                                          strAuthenticationCode:=strAuthCode,
                                          strWesiteurlExternal:=strExtUrl,
                                          strWesiteurlInternal:=strIntUrl,
                                          blnIsactive:=True,
                                          strVirtualDirectory:=strVirtualDirectory
                                          ) = True Then

                ShowMessage("Company Updated Successfully!", MessageType.Info)
                grdCompanyInfo.EditIndex = -1
                e.Cancel = True 'To Prevent Object DataSource Update Method calling
                'Call FillList()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdCompanyInfo_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdCompanyInfo.RowDeleting
        Dim objCompany As New clsCompany
        Try

            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If objCompany.DeleteCompanyInfo(strCompCode:=Session("CompCode").ToString,
                                                     intComUnkID:=CInt(Session("companyunkid"))
                                                     ) = True Then

                ShowMessage("Company Deleted Successfully!", MessageType.Info)
                e.Cancel = True 'To Prevent Object DataSource Delete Method calling
                'Call FillList()
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdCompanyInfo_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCompanyInfo.RowCommand
        Try
            If grdCompanyInfo.EditIndex >= 0 Then Exit Sub
            If e.CommandName = "Copy" Then
                Dim intComoanyunkid As Integer = 0
                Dim intIdx As Integer = CInt(e.CommandArgument)

                Integer.TryParse(DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblCompanyunkid"), Label).Text, intComoanyunkid)

                Dim strCompcode, strCompname, strEmail, strExtUrl, strIntUrl, strAuthCode, strDBName As String
                strCompcode = "" : strCompname = "" : strEmail = "" : strExtUrl = "" : strIntUrl = "" : strAuthCode = "" : strDBName = ""

                strCompcode = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblCompanycode"), Label).Text
                strCompname = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblCompanyname"), Label).Text
                strEmail = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblEmail"), Label).Text
                strExtUrl = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblExternalURL"), Label).Text
                strIntUrl = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblInternalURL"), Label).Text
                strAuthCode = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblAuthCode"), Label).Text
                strDBName = DirectCast(grdCompanyInfo.Rows(intIdx).FindControl("objlblDBName"), Label).Text

                Dim strDetails As String = "Web Client ID " & vbTab & vbTab & vbTab & vbTab & ": " & intComoanyunkid.ToString
                strDetails += Environment.NewLine & "Company Code " & vbTab & vbTab & vbTab & ": " & strCompcode
                strDetails += Environment.NewLine & "Company Name " & vbTab & vbTab & vbTab & ": " & strCompname
                strDetails += Environment.NewLine & "Email " & vbTab & vbTab & vbTab & vbTab & vbTab & ": " & strEmail
                strDetails += Environment.NewLine & "Authentication Code " & vbTab & vbTab & vbTab & ": " & strAuthCode
                'strDetails += Environment.NewLine & "Website URL (External Vacancy)" & vbTab & ": " & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/" & strExtUrl
                'strDetails += Environment.NewLine & "Website URL (Internal Vacancy) " & vbTab & ": " & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/" & strIntUrl
                'strDetails += Environment.NewLine & "Admin Login URL" & vbTab & vbTab & vbTab & ": " & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/" & strExtUrl.Replace("Login.aspx", "ALogin.aspx")
                strDetails += Environment.NewLine
                strDetails += Environment.NewLine & "Web Service Link" & vbTab & ": "
                strDetails += Environment.NewLine & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/ArutiRecruitmentService.asmx"
                strDetails += Environment.NewLine
                strDetails += Environment.NewLine & "Website URL (External Vacancy)" & vbTab & ": "
                strDetails += Environment.NewLine & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/" & strExtUrl
                strDetails += Environment.NewLine
                strDetails += Environment.NewLine & "Website URL (Internal Vacancy) " & vbTab & ": "
                strDetails += Environment.NewLine & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/" & strIntUrl
                strDetails += Environment.NewLine
                strDetails += Environment.NewLine & "Vacancy URL (External Vacancy) : "
                strDetails += Environment.NewLine & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/Career.aspx?ext=7&cc=" & strAuthCode & ""
                strDetails += Environment.NewLine
                strDetails += Environment.NewLine & "Admin Login URL" & vbTab & vbTab & vbTab & ": "
                strDetails += Environment.NewLine & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/" & strExtUrl.Replace("Login.aspx", "ALogin.aspx")
                strDetails += Environment.NewLine
                strDetails += Environment.NewLine & "Test Email URL" & vbTab & vbTab & vbTab & vbTab & ": "
                strDetails += Environment.NewLine & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/TestEmail.aspx"

                Session("CopyDetails") = strDetails

                Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/SAdmin/Details.aspx", False)
                Exit Sub
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub odsCompany_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles odsCompany.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is DataSet Then
                intC = CType(e.ReturnValue, DataSet).Tables(0).Rows.Count
            End If

            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub



#End Region

End Class