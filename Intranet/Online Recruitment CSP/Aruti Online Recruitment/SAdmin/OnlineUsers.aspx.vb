﻿Public Class OnlineUsers
    Inherits Base_Page

    Dim pageSize As Integer = 5
    Dim totalUsers As Integer
    Dim totalPages As Integer
    Dim currentPage As Integer = 1

#Region " Private Function "

    Private Sub GetUsers()
        UsersOnlineLabel.Text = Membership.GetNumberOfUsersOnline().ToString()

        'UserGrid.DataSource = Membership.GetAllUsers(currentPage - 1, pageSize, totalUsers)
        'totalPages = ((totalUsers - 1) \ pageSize) + 1

        '' Ensure that we do not navigate past the last page of users.

        'If currentPage > totalPages Then
        '    currentPage = totalPages
        '    GetUsers()
        '    Return
        'End If

        'UserGrid.DataBind()
        'CurrentPageLabel.Text = currentPage.ToString()
        'TotalPagesLabel.Text = totalPages.ToString()

        'If currentPage = totalPages Then
        '    NextButton.Visible = False
        'Else
        '    NextButton.Visible = True
        'End If

        'If currentPage = 1 Then
        '    PreviousButton.Visible = False
        'Else
        '    PreviousButton.Visible = True
        'End If

        'If totalUsers <= 0 Then
        '    NavigationPanel.Visible = False
        'Else
        '    NavigationPanel.Visible = True
        'End If

        Dim lst = (From p As MembershipUser In Membership.GetAllUsers Where (p.IsOnline = True) Order By p.LastActivityDate Descending Select (New With {.UserName = p.UserName.ToString, .CreationDate = p.CreationDate, .LastLogin = p.LastLoginDate.ToString, .LastActive = p.LastActivityDate.ToString, .IsApproved = p.IsApproved})).ToList
        grdLink.DataSource = lst
        grdLink.DataBind()
    End Sub

#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GetUsers()
            End If
        Catch ex As Exception
            Call Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdLink_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdLink.PageIndexChanging
        Try
            grdLink.PageIndex = e.NewPageIndex
            Call GetUsers()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region " Datagridview Events "

#End Region

#Region " Button Events "
    'Public Sub NextButton_OnClick(sender As Object, args As EventArgs)
    '    currentPage = Convert.ToInt32(CurrentPageLabel.Text)
    '    currentPage += 1
    '    GetUsers()
    'End Sub

    'Public Sub PreviousButton_OnClick(sender As Object, args As EventArgs)
    '    currentPage = Convert.ToInt32(CurrentPageLabel.Text)
    '    currentPage -= 1
    '    GetUsers()
    'End Sub
#End Region
End Class