﻿<%@ Page Title="Company Information" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="CompanyInfo.aspx.vb" Inherits="Aruti_Online_Recruitment.CompanyInfo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {

                $$('btnAddCompany').on('click', function () {
                    return IsValidate();
                });

                $("[id*=btndelete]").on('click', function () {
                    if (!ShowConfirm("Are you sure you want to Delete this Company?", this))
                        return false;
                    else
                        return true;
                });


            });
        };

        $(document).ready(function () {
            $$('btnAddCompany').on('click', function () {
                return IsValidate();
            });

            $("[id*=btndelete]").on('click', function () {
                if (!ShowConfirm("Are you sure you want to Delete this Company?", this))
                    return false;
                else
                    return true;
            });
        });

        function IsValidate() {

            if ($$('txtCompCode').val().trim() == '') {
                ShowMessage('Company code is compulsory information, it can not be blank.', 'MessageType.Errorr');
                $$('txtCompCode').focus();
                return false;
            }
            else if ($$('txtCompName').val().trim() == '') {
                ShowMessage('Company name is compulsory information, it can not be blank.', 'MessageType.Errorr');
                $$('txtCompName').focus();
                return false;
            }
            else if ($$('txtEmail').val().trim() == '') {
                ShowMessage('Email is compulsory information, it can not be blank.', 'MessageType.Errorr');
                $$('txtEmail').focus();
                return false;
            }
            else if (IsValidEmail($$('txtEmail').val()) == false) {
                ShowMessage('Please enter proper email.', 'MessageType.Errorr')
                $$('txtEmail').focus();
                return false;
            } 
            else if ($$('txtDatabaseName').val().trim() == '') {
                ShowMessage('Database name is compulsory information, it can not be blank.', 'MessageType.Errorr');
                $$('txtDatabaseName').focus();
                return false;
            }

            
        }

        function IsValidEmail(strEmail) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(strEmail)) {
                return false;
            } else {
                return true;
            }
        }
    </script>
    <div class="card">


        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="card-header">
                    <h4>Company Information
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                    </h4>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblCompanyID" runat="server" Text="Company ID:"></asp:Label>
                            <asp:TextBox ID="txtCompanyID" runat="server" CssClass="form-control" ReadOnly="false"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblCompCode" runat="server" Text="*Company Code :"></asp:Label>
                            <asp:TextBox ID="txtCompCode" runat="server" MaxLength="5" CssClass="form-control" ValidationGroup="CompanyInfo"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="None"
                                ControlToValidate="txtCompCode" ErrorMessage="Company Code cannot be blank. "
                                ValidationGroup="CompanyInfo" SetFocusOnError="True">
                            </asp:RequiredFieldValidator>--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblCompName" runat="server" Text="*Company Name :"></asp:Label>
                            <asp:TextBox ID="txtCompName" runat="server" MaxLength="50" CssClass="form-control" ValidationGroup="CompanyInfo"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                ControlToValidate="txtCompName" ErrorMessage="Company Name cannot be blank. "
                                ValidationGroup="CompanyInfo" SetFocusOnError="True">
                            </asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-md-4">
                            <asp:Label ID="lblEmail" runat="server" Text="*Company Email :"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="form-control" ValidationGroup="CompanyInfo"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
                                ControlToValidate="txtEmail" ErrorMessage="Company Email cannot be blank. "
                                ValidationGroup="CompanyInfo" SetFocusOnError="True">
                            </asp:RequiredFieldValidator>--%>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                                ControlToValidate="txtEmail" ErrorMessage="Enter Valid Email ID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="CompanyInfo" SetFocusOnError="true"></asp:RegularExpressionValidator>--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblDatabaseName" runat="server" Text="Database Name :"></asp:Label>
                            <asp:TextBox ID="txtDatabaseName" runat="server" MaxLength="200" ReadOnly="false" Text="arutihrms" CssClass="form-control"
                                ValidationGroup="CompanyInfo"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="None"
                                ValidationGroup="CompanyInfo" ControlToValidate="txtDatabaseName" ErrorMessage="Database Name can not be Blank "
                                SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblAuthenticationCode" runat="server" Text="Authenti. Code :"></asp:Label>
                            <asp:TextBox ID="txtAuthenticationCode" runat="server" MaxLength="200" ReadOnly="false" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-md-4">
                            <asp:Label ID="lblWebsiteURL" runat="server" Text="Website URL :"></asp:Label>
                            <asp:TextBox ID="txtWebsiteURL" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblWebsiteURLInternal" runat="server" Text="Website URL (Int.) :"></asp:Label>
                            <asp:TextBox ID="txtWebsiteURLInternal" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblVirtualDirectory" runat="server" Text="Virtual Directory :"></asp:Label>
                            <asp:TextBox ID="txtVirtualDirectory" runat="server" MaxLength="50" CssClass="form-control" Text="ArutiHRM" ValidationGroup="CompanyInfo"></asp:TextBox>
                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None"
                            ControlToValidate="txtVirtualDirectory" ErrorMessage="Virtual Directory cannot be blank. "
                            ValidationGroup="CompanyInfo" SetFocusOnError="True">
                        </asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12 text-center" > <%--style="text-align: center;"--%>
                            <%--<asp:ValidationSummary
                                HeaderText="You must enter a value in the following fields:"
                                DisplayMode="BulletList"
                                EnableClientScript="true"
                                runat="server" ValidationGroup="CompanyInfo" Style="color: red" />--%>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group row">

                        <div class="col-md-4">
                            <asp:Button ID="btnAddCompany" runat="server" CssClass="btn btn-primary w-100"  Text="Add Company"
                                ValidationGroup="CompanyInfo" /> <%--Style="width: 100%;"--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary w-100"  Text="Search"
                                ValidationGroup="CompanyInfoSearch" /> <%--Style="width: 100%;"--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary w-100"  Text="Reset"
                                ValidationGroup="CompanyInfoSearch" /> <%--Style="width: 100%;"--%>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAddCompany" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <asp:Panel ID="pnl_CompanyInfo" runat="server" CssClass="table-responsive minheight300" > <%--Style="min-height: 300px;"--%>
                    <asp:GridView ID="grdCompanyInfo" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                        AllowPaging="false" DataKeyNames="companyunkid, company_code, admin_email" DataSourceID="odsCompany">

                        <Columns>
                            <%--<asp:BoundField DataField="companyunkid" HeaderText="ID" />--%>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanyunkid" runat="server" Text='ID' CssClass="font-weight-bold"  /><br /> <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objlblCompanyunkid" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("companyunkid")) %>' />--%>
                                    <asp:Label ID="objlblCompanyunkid" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("companyunkid"), True) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="company_code" HeaderText="Code" />
                                <asp:BoundField DataField="company_name" HeaderText="Company Name" />--%>
                            <asp:TemplateField HeaderText="Code / Company Name">
                                <ItemTemplate>
                                    <div class="col-md-4 no-padding">
                                        <asp:Label ID="lblCompanycode" runat="server" Text='Code' CssClass ="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                        <%--<asp:Label ID="objlblCompanycode" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_code")) %>' /><br />--%>
                                        <asp:Label ID="objlblCompanycode" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_code"), True) %>' />
                                    </div>
                                    <div class="col-md-8 no-padding">
                                        <asp:Label ID="lblVirtual_Directory" runat="server" Text='Virt. Directory' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                        <asp:Label ID="objlblVirtual_Directory" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("virtual_directory"), True) %>' /><br />
                                    </div>
                                    <asp:Label ID="lblCompanyname" runat="server" Text='Company Name' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objlblCompanyname" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_name")) %>' />--%>
                                    <asp:Label ID="objlblCompanyname" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_name"), True) %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="col-md-4 no-padding">
                                        <asp:Label ID="lblCompanycode" runat="server" Text='Code' CssClass="font-weight-bold" /><br /> <%--Font-Bold="true"--%>
                                        <%--<asp:Label ID="objtxtCompanycode" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_code")) %>' /><br />--%>
                                        <asp:Label ID="objtxtCompanycode" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_code"), True) %>' />
                                    </div>
                                    <div class="col-md-8 no-padding">
                                        <asp:Label ID="lblVirtual_Directory" runat="server" Text='Virt. Directory' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                        <asp:TextBox ID="objtxtVirtual_Directory" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("virtual_directory"), True) %>' CssClass="form-control" /><br />
                                    </div>
                                    <asp:Label ID="lblCompanyname" runat="server" Text='Company Name' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                    <%--<asp:TextBox ID="objtxtCompanyname" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_name")) %>' TextMode="MultiLine" CssClass="form-control" />--%>
                                    <asp:TextBox ID="objtxtCompanyname" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_name"), True) %>' TextMode="MultiLine" CssClass="form-control" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="email" HeaderText="Email" />
                            <asp:BoundField DataField="company_last_updated" HeaderText="Last Updated" />--%>
                            <asp:TemplateField HeaderText="Email / Last Updated">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='Email:' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objlblEmail" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("email")) %>' /><br />--%>
                                    <asp:Label ID="objlblEmail" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("email"), True) %>' /><br />
                                    <asp:Label ID="lblLastUpdated" runat="server" Text='Last Updated:' CssClass="font-weight-bold"  /><br />   <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objlblLastUpdated" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_last_updated")) %>' />--%>
                                    <asp:Label ID="objlblLastUpdated" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_last_updated"), True) %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='Email' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                    <%--<asp:TextBox ID="objtxtEmail" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("email")) %>' TextMode="MultiLine" CssClass="form-control" />--%>
                                    <asp:TextBox ID="objtxtEmail" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("email"), True) %>' TextMode="MultiLine" CssClass="form-control" />
                                    <asp:Label ID="lbleLastUpdated" runat="server" Text='Last Updated:' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objelblLastUpdated" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("company_last_updated")) %>' />--%>
                                    <asp:Label ID="objelblLastUpdated" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company_last_updated"), True) %>' />
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:BoundField DataField="websiteurl" HeaderText="URL" />   --%>
                            <asp:TemplateField HeaderText="URL External / Internal">
                                <ItemTemplate>
                                    <asp:Label ID="lblExternalURL" runat="server" Text='External URL:' CssClass="font-weight-bold"  /><br /> <%--Font-Bold="true"--%>
                                    <asp:Label ID="objlblExternalURL" runat="server" Text='<%# Eval("websiteurl") %>' /><br />
                                    <asp:Label ID="lblInternalURL" runat="server" Text='Internal URL:' CssClass="font-weight-bold"  /><br /> <%--Font-Bold="true"--%>
                                    <asp:Label ID="objlblInternalURL" runat="server" Text='<%# Eval("websiteurlinternal") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblExternalURL" runat="server" Text='External URL:' CssClass="font-weight-bold"  /><br /> <%--Font-Bold="true"--%>
                                    <asp:TextBox ID="objtxtExternalURL" runat="server" Text='<%# Eval("websiteurl") %>' TextMode="MultiLine" CssClass="form-control" /><br />
                                    <asp:Label ID="lblInternalURL" runat="server" Text='Internal URL:' CssClass="font-weight-bold"  /><br /> <%--Font-Bold="true"--%>
                                    <asp:TextBox ID="objtxtInternalURL" runat="server" Text='<%# Eval("websiteurlinternal") %>' TextMode="MultiLine" CssClass="form-control" />
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:BoundField DataField="authentication_code" HeaderText="Auth. Code" />
                            <asp:BoundField DataField="database_name" HeaderText="database" />--%>
                            <asp:TemplateField HeaderText="Authent. Code / Database Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAuthCode" runat="server" Text='Authent. Code:' CssClass="font-weight-bold"  /><br /> <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objlblAuthCode" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("authentication_code")) %>' /><br />--%>
                                    <asp:Label ID="objlblAuthCode" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("authentication_code"), True) %>' /><br />
                                    <asp:Label ID="lblDBName" runat="server" Text='Database Name:' CssClass="font-weight-bold"  /><br />    <%--Font-Bold="true"--%>
                                    <%--<asp:Label ID="objlblDBName" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("database_name")) %>' />--%>
                                    <asp:Label ID="objlblDBName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("database_name"), True) %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblAuthCode" runat="server" Text='Authent. Code:' CssClass="font-weight-bold"  /><br />  <%--Font-Bold="true"--%>
                                    <%--<asp:TextBox ID="objtxtAuthCode" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("authentication_code")) %>' TextMode="MultiLine" CssClass="form-control" /><br />--%>
                                    <asp:TextBox ID="objtxtAuthCode" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("authentication_code"), True) %>' TextMode="MultiLine" CssClass="form-control" /><br />
                                    <asp:Label ID="lblDBName" runat="server" Text='Database Name:' CssClass="font-weight-bold"  /><br />    <%--Font-Bold="true"--%>
                                    <%--<asp:TextBox ID="objtxtDBName" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("database_name")) %>' TextMode="MultiLine" CssClass="form-control" />--%>
                                    <asp:TextBox ID="objtxtDBName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("database_name"), True) %>' TextMode="MultiLine" CssClass="form-control" />
                                </EditItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Ext. / Int." ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<a href='<%# "../" + Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("websiteurl")) %>' target="_blank">View</a><br />
                                    <a href='<%# "../" + Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("websiteurlinternal")) %>' target="_blank">ViewInt</a><br />--%>
                                    <a href='<%# "../" + AntiXss.AntiXssEncoder.HtmlEncode(Eval("websiteurl"), True) %>' target="_blank">View</a><br />
                                    <a href='<%# "../" + AntiXss.AntiXssEncoder.HtmlEncode(Eval("websiteurlinternal"), True) %>' target="_blank">ViewInt</a><br />
                                    <asp:LinkButton ID="btnCopy" runat="server" CommandName="Copy" CommandArgument="<%# Container.DataItemIndex %>" Text="<i aria-hidden='true' class='fa fa-copy'></i>" ToolTip="Copy Details"></asp:LinkButton>
                                    <%--<asp:HyperLink runat="server"  NavigateUrl='<%# "../" + Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("websiteurl")) %>' Target="_blank" Text="View"  />--%>
                                    <%--<asp:HyperLink runat="server"  NavigateUrl='<%# "../" + Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("websiteurlinternal")) %>' Target="_blank" Text="ViewInt" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Edit">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="<i aria-hidden='true' class='fa fa-edit'></i>"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                    <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Delete">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="btndelete" runat="server" CommandName="Delete" Text="<i aria-hidden='true' class='fa fa-trash'></i>" ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:ObjectDataSource ID="odsCompany" runat="server" SelectMethod="GetCompanies" TypeName="Aruti_Online_Recruitment.clsCompany" EnablePaging="false">

                        <SelectParameters>
                            <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                            <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="blnOnlyActive" Type="Boolean" DefaultValue="True" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>



            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAddCompany" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="grdCompanyInfo" EventName="RowUpdating" />
                <asp:AsyncPostBackTrigger ControlID="grdCompanyInfo" EventName="RowDeleting" />
                <asp:AsyncPostBackTrigger ControlID="grdCompanyInfo" EventName="RowCancelingEdit" />
                <asp:AsyncPostBackTrigger ControlID="grdCompanyInfo" EventName="RowEditing" />
                <asp:AsyncPostBackTrigger ControlID="grdCompanyInfo" EventName="RowCommand" />
            </Triggers>
        </asp:UpdatePanel>



    </div>


</asp:Content>
