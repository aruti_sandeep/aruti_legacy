﻿Imports System.IO
Imports System.Security.Cryptography

Public Class clsSecurity

    Private Shared bytIV As Byte() = {21, 54, 9, 13, 78, 19, 62, 45, 121, 195, 245, 35, 5, 1, 121, 212}

    Public Shared Function Encrypt(ByVal vstrTextToBeEncrypted As String, ByVal vstrEncryptionKey As String) As String
        Dim bytValue As Byte() = Nothing
        Dim bytKey As Byte() = Nothing
        Dim bytEncoded As Byte() = Nothing
        Dim objMemoryStream As MemoryStream = New MemoryStream()
        Dim objCryptoStream As CryptoStream = Nothing
        Dim objRijndaelManaged As RijndaelManaged = Nothing

        Try
            vstrTextToBeEncrypted = StripNullCharacters(vstrTextToBeEncrypted)
            bytValue = Encoding.Unicode.GetBytes(vstrTextToBeEncrypted.ToCharArray())
            vstrEncryptionKey = vstrEncryptionKey.PadRight(32, "X"c)
            bytKey = Encoding.ASCII.GetBytes(vstrEncryptionKey.ToCharArray())
            objRijndaelManaged = New RijndaelManaged()
            objCryptoStream = New CryptoStream(objMemoryStream, objRijndaelManaged.CreateEncryptor(bytKey, bytIV), CryptoStreamMode.Write)
            objCryptoStream.Write(bytValue, 0, bytValue.Length)
            objCryptoStream.FlushFinalBlock()
            bytEncoded = objMemoryStream.ToArray()
            Return Convert.ToBase64String(bytEncoded)
        Catch
            Return ""
        Finally
            bytValue = Nothing
            bytKey = Nothing
            bytEncoded = Nothing

            If objMemoryStream IsNot Nothing Then
                objMemoryStream.Close()
            End If

            objMemoryStream = Nothing

            If objCryptoStream IsNot Nothing Then
                objCryptoStream.Close()
            End If

            objCryptoStream = Nothing

            If objRijndaelManaged IsNot Nothing Then
                objRijndaelManaged.Clear()
            End If

            objRijndaelManaged = Nothing
        End Try
    End Function

    Public Shared Function Decrypt(ByVal vstrStringToBeDecrypted As String, ByVal vstrDecryptionKey As String) As String
        Dim bytDataToBeDecrypted As Byte() = Nothing
        Dim bytTemp As Byte() = Nothing
        Dim bytDecryptionKey As Byte() = Nothing
        Dim objRijndaelManaged As RijndaelManaged = New RijndaelManaged()
        Dim objMemoryStream As MemoryStream = Nothing
        Dim objCryptoStream As CryptoStream = Nothing
        Dim strReturnString As String = String.Empty

        Try
            bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted)
            vstrDecryptionKey = vstrDecryptionKey.PadRight(32, "X"c)
            bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray())
            bytTemp = New Byte(bytDataToBeDecrypted.Length + 1 - 1) {}
            objMemoryStream = New MemoryStream(bytDataToBeDecrypted)
            objCryptoStream = New CryptoStream(objMemoryStream, objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV), CryptoStreamMode.Read)
            objCryptoStream.Read(bytTemp, 0, bytTemp.Length)
            Return StripNullCharacters(Encoding.Unicode.GetString(bytTemp, 0, bytTemp.Length))
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return ""
        Finally
            bytDataToBeDecrypted = Nothing
            bytTemp = Nothing
            bytDecryptionKey = Nothing

            If objMemoryStream IsNot Nothing Then
                objMemoryStream.Close()
            End If

            objMemoryStream = Nothing

            If objCryptoStream IsNot Nothing Then
                objCryptoStream.Close()
            End If

            objCryptoStream = Nothing

            If objRijndaelManaged IsNot Nothing Then
                objRijndaelManaged.Clear()
            End If

            objRijndaelManaged = Nothing
        End Try
    End Function

    Private Shared Function StripNullCharacters(ByVal vstrStringWithNulls As String) As String
        Dim intPosition As Integer = 0
        Dim strStringWithOutNulls As String = Nothing
        intPosition = 1
        strStringWithOutNulls = vstrStringWithNulls

        While intPosition > 0
            intPosition = (vstrStringWithNulls.IndexOf(vbNullChar, intPosition - 1) + 1)

            If intPosition > 0 Then
                strStringWithOutNulls = strStringWithOutNulls.Substring(0, intPosition - 1) & strStringWithOutNulls.Substring(strStringWithOutNulls.Length - (strStringWithOutNulls.Length - intPosition))
            End If

            If intPosition > strStringWithOutNulls.Length Then
                Exit While
            End If
        End While

        Return strStringWithOutNulls
    End Function
End Class
