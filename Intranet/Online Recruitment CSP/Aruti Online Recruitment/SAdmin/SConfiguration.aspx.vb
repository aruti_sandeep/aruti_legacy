﻿Public Class SConfiguration
    Inherits Base_Page


#Region "Variable Declarations"

    Private objCompanySettings As New clsCompany
    Private mstrCompanyCode As String = String.Empty
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                FillCombo()
            Else
                mstrCompanyCode = Me.ViewState("Companycode").ToString()
            End If
			'If CBool(Session("isvacancyalert")) = False Then  'OrElse 1 = 1
			'	Response.Redirect("~/SAdmin/UserHome.aspx", False)
			'	Exit Sub
			'End If
		Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SConfiguration_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            Me.ViewState("Companycode") = mstrCompanyCode
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objCompanySettings.GetCompanyForCombo("", 0, True)
            With ddlCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            ddlCompany_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub GetValue()
        Dim dsList As DataSet = Nothing
        Try
            mstrCompanyCode = objCompanySettings.GetCompanyCode(Convert.ToInt32(ddlCompany.SelectedValue))
            dsList = objCompanySettings.GetCompany(strCompCode:=mstrCompanyCode, intComUnkID:=Convert.ToInt32(ddlCompany.SelectedValue), blnOnlyActive:=True)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                chkVacancyalert.Checked = Convert.ToBoolean(dsList.Tables(0).Rows(0)("isvacancyAlert"))
                txtMaxAlert.Text = Convert.ToInt32(dsList.Tables(0).Rows(0)("maxvacancyalert")).ToString()
            End If
            chkVacancyalert_CheckedChanged(New Object(), New EventArgs())
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            dsList.Clear()
            dsList = Nothing
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub chkVacancyalert_CheckedChanged(sender As Object, e As EventArgs) Handles chkVacancyalert.CheckedChanged
        Try
            If chkVacancyalert.Checked = True Then
                txtMaxAlert.Attributes.Add("max", "10")
                txtMaxAlert.Attributes.Add("min", "1")
                If (txtMaxAlert.Text.Length <= 0 OrElse Convert.ToInt32(txtMaxAlert.Text) <= 0) Then txtMaxAlert.Text = "1"
            Else
                txtMaxAlert.Attributes.Add("max", "0")
                txtMaxAlert.Attributes.Add("min", "0")
                txtMaxAlert.Text = "0"
            End If
            txtMaxAlert.Enabled = chkVacancyalert.Checked
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region "Textbox Event"

    Private Sub txtMaxAlert_TextChanged(sender As Object, e As EventArgs) Handles txtMaxAlert.TextChanged
        Try
            If txtMaxAlert.Enabled AndAlso txtMaxAlert.Text.Trim = "" Then
                txtMaxAlert.Text = "1"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If objCompanySettings.EditCompanySettings(strCompCode:=mstrCompanyCode _
                                             , intComUnkID:=Convert.ToInt32(ddlCompany.SelectedValue) _
                                             , mblnVacancyAlert:=chkVacancyalert.Checked _
                                             , xMaxVacancyAlert:=IIf(txtMaxAlert.Text.Length <= 0, "0", Convert.ToInt32(txtMaxAlert.Text))) = True Then

                ShowMessage("Company Settings Saved Successfully.", MessageType.Info)
                ddlCompany.SelectedIndex = 0
                ddlCompany_SelectedIndexChanged(New Object(), New EventArgs())
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Dropdown Events"

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            If Convert.ToInt32(ddlCompany.SelectedValue) > 0 Then
                chkVacancyalert.Enabled = True
                GetValue()
                btnSave.Enabled = True
            Else
                chkVacancyalert.Checked = False
                chkVacancyalert.Enabled = False
                chkVacancyalert_CheckedChanged(New Object(), New EventArgs())
                btnSave.Enabled = False
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

End Class