﻿Option Strict On

Public Class AdminUsers
    Inherits Base_Page

#Region " Methods Functions "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetCompanyForCombo("", 0, True)
            With cboCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objCompany = Nothing
        End Try
    End Sub

    Private Sub FillList()

        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In grdLink.Rows
                cb = CType(grdLink.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                cb.Checked = blnCheckAll

            Next

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub AdminUsers_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim objComp As New clsCompany
                dsLink.SelectParameters.Item("strComp_Code").DefaultValue = objComp.GetCompanyCode(CInt(cboCompany.SelectedValue))
                objComp = Nothing
            Else
                dsLink.SelectParameters.Item("strComp_Code").DefaultValue = ""
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "
    Private Sub grdLink_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLink.RowCommand
        Try
            If grdLink.EditIndex >= 0 Then Exit Sub
            'If e.CommandName = "DeleteApplicant" Then

            '    Dim strCompCode As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("Comp_Code").ToString
            '    Dim intCompanyunkid As Integer = 0
            '    Integer.TryParse(grdLink.DataKeys(CInt(e.CommandArgument)).Item("companyunkid").ToString, intCompanyunkid)
            '    Dim strEmail As String = grdLink.Rows(CInt(e.CommandArgument)).Cells(4).Text

            '    If Membership.GetUser(strEmail) IsNot Nothing Then
            '        Membership.DeleteUser(strEmail)

            '        grdLink.DataBind()
            '        ShowMessage("Applicant deleted successfully!")
            '    End If


            'End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            'FillList()
        End Try
    End Sub

    Private Sub grdLink_DataBound(sender As Object, e As EventArgs) Handles grdLink.DataBound
        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdLink_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdLink.RowCancelingEdit
        Try
            grdLink.EditIndex = -1
            Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdLink_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdLink.RowEditing
        Try
            grdLink.EditIndex = e.NewEditIndex
            Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdLink_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdLink.RowUpdating
        Dim objSA As New clsSACommon
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim intActivationUnkId As Integer = 0
            Dim intCompanyunkid As Integer = 0

            Integer.TryParse(grdLink.DataKeys(e.RowIndex).Item("activationlinkunkid").ToString, intActivationUnkId)
            Integer.TryParse(DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtClientID"), Label).Text, intCompanyunkid)

            Dim strCompcode, strUserId, strEmail As String
            Dim blnCanViewErrorLog = False
            Dim blnCanTruncateErrorLog = False
            Dim blnCanShrinkDataBase = False
            Dim blnCanRunFullScript = False
            Dim blnCanRunAlterScript = False

            strCompcode = "" : strUserId = "" : strEmail = ""

            strCompcode = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtCode"), Label).Text
            strEmail = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtEmail"), Label).Text
            strUserId = grdLink.DataKeys(e.RowIndex).Item("UserId").ToString 'DirectCast(grdLink.Rows(e.RowIndex).FindControl("objtxtCompanyname"), TextBox).Text
            blnCanViewErrorLog = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtCanViewErrorLog"), CheckBox).Checked
            blnCanTruncateErrorLog = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtCanTruncateErrorLog"), CheckBox).Checked
            blnCanShrinkDataBase = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtCanShrinkDataBase"), CheckBox).Checked
            blnCanRunFullScript = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtCanRunFullScript"), CheckBox).Checked
            blnCanRunAlterScript = DirectCast(grdLink.Rows(e.RowIndex).FindControl("objetxtCanRunAlterScript"), CheckBox).Checked

            If strCompcode.Trim = "" Then
                ShowMessage("Company code Name is compulsory information, it can not be blank.", MessageType.Info)
                grdLink.Rows(e.RowIndex).FindControl("objtxtCompanycode").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strUserId.Trim = "" Then
                ShowMessage("Company name Name is compulsory information, it can not be blank.", MessageType.Info)
                grdLink.Rows(e.RowIndex).FindControl("objtxtCompanyname").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strEmail.Trim = "" Then
                ShowMessage("Email Name is compulsory information, it can not be blank.", MessageType.Info)
                grdLink.Rows(e.RowIndex).FindControl("objtxtEmail").Focus()
                e.Cancel = True
                Exit Try

            End If

            If clsApplicant.IsValidEmail(strEmail) = False Then
                ShowMessage("Please enter proper email.", MessageType.Info)
                grdLink.Rows(e.RowIndex).FindControl("objtxtEmail").Focus()
                e.Cancel = True
                Exit Try
            End If

            If objSA.EditAdminUsers(intActivationLinkUnkId:=intActivationUnkId,
                                    strCompCode:=strCompcode,
                                    intComUnkID:=intCompanyunkid,
                                    strEmail:=strEmail,
                                    strUserId:=strUserId,
                                    blnCanViewErrorLog:=blnCanViewErrorLog,
                                    blnCanTruncateErrorLog:=blnCanTruncateErrorLog,
                                    blnCanShrinkDataBase:=blnCanShrinkDataBase,
                                    blnCanRunFullScript:=blnCanRunFullScript,
                                    blnCanRunAlterScript:=blnCanRunAlterScript,
                                    blnIsDeleted:=False,
                                    strAuditEmail:=Session("email").ToString,
                                    dtAuditDateTime:=Now
                                    ) = True Then

                ShowMessage("Transaction Updated Successfully!", MessageType.Info)
                e.Cancel = True 'To Prevent Object DataSource Update Method calling
                grdLink.EditIndex = -1
                Call FillList()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub grdLink_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdLink.RowDeleting
        Dim objSA As New clsSACommon
        Try

            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim intActivationUnkId As Integer = 0
            Dim strEmail As String = ""

            Integer.TryParse(grdLink.DataKeys(e.RowIndex).Item("activationlinkunkid").ToString, intActivationUnkId)
            strEmail = grdLink.DataKeys(e.RowIndex).Item("email").ToString

            If objSA.DeleteMembershiUser(intActivationLinkUnkId:=intActivationUnkId,
                                         strUserName:=strEmail,
                                         blnIsDeleted:=True,
                                         strAuditEmail:=Session("email").ToString,
                                         dtAuditDateTime:=Now
                                         ) = True Then

                grdLink.DataBind()
                ShowMessage("User Deleted Successfully!", MessageType.Info)
                e.Cancel = True 'To Prevent Object DataSource Delete Method calling
                Call FillList()
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objSA = Nothing
        End Try
    End Sub

    Private Sub dsLink_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsLink.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(CType(e.ReturnValue, String), intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdLink.PageIndex = 0
            End If

            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    'Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
    '    Try


    '        Dim blnChecked As Boolean = False

    '        For Each dgRow As GridViewRow In grdLink.Rows

    '            Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)

    '            If cb IsNot Nothing AndAlso cb.Checked Then

    '                Dim intUnkId As Integer = 0
    '                Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("activationlinkunkid").ToString, intUnkId)
    '                Dim strCompCode As String = grdLink.DataKeys(dgRow.RowIndex).Item("Comp_Code").ToString
    '                Dim intCompanyunkid As Integer = 0
    '                Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("companyunkid").ToString, intCompanyunkid)
    '                Dim strEmail As String = grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString

    '                If Membership.GetUser(strEmail) IsNot Nothing Then
    '                    Membership.DeleteUser(strEmail)
    '                End If

    '                blnChecked = True
    '            End If

    '        Next

    '        If blnChecked = False Then
    '            ShowMessage("Please tick atleast one email to delete.")
    '        Else
    '            grdLink.DataBind()
    '            ShowMessage("Applicants deleted successfully!")
    '        End If


    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    Finally
    '    End Try
    'End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            'Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    'Private Sub dsLink_Updating(sender As Object, e As ObjectDataSourceMethodEventArgs) Handles dsLink.Updating
    '    Try
    '        Dim intActivationUnkId As Integer = 0
    '        Dim intCompanyunkid As Integer = 0

    '        Integer.TryParse(grdLink.DataKeys(grdLink.EditIndex).Item("activationlinkunkid").ToString, intActivationUnkId)
    '        Integer.TryParse(DirectCast(grdLink.Rows(grdLink.EditIndex).FindControl("objetxtClientID"), Label).Text, intCompanyunkid)

    '        Dim strCompcode, strUserId, strEmail As String
    '        Dim blnCanViewErrorLog = False
    '        Dim blnCanTruncateErrorLog = False
    '        Dim blnCanShrinkDataBase = False

    '        strCompcode = "" : strUserId = "" : strEmail = ""

    '        strCompcode = DirectCast(grdLink.Rows(grdLink.EditIndex).FindControl("objetxtCode"), Label).Text
    '        strEmail = DirectCast(grdLink.Rows(grdLink.EditIndex).FindControl("objetxtEmail"), Label).Text
    '        strUserId = grdLink.DataKeys(grdLink.EditIndex).Item("UserId").ToString 'DirectCast(grdLink.Rows(e.RowIndex).FindControl("objtxtCompanyname"), TextBox).Text
    '        blnCanViewErrorLog = DirectCast(grdLink.Rows(grdLink.EditIndex).FindControl("objetxtCanViewErrorLog"), CheckBox).Checked
    '        blnCanTruncateErrorLog = DirectCast(grdLink.Rows(grdLink.EditIndex).FindControl("objetxtCanTruncateErrorLog"), CheckBox).Checked
    '        blnCanShrinkDataBase = DirectCast(grdLink.Rows(grdLink.EditIndex).FindControl("objetxtCanShrinkDataBase"), CheckBox).Checked

    '        If strCompcode.Trim = "" Then
    '            ShowMessage("Company code Name is compulsory information, it can not be blank.", MessageType.Info)
    '            grdLink.Rows(grdLink.EditIndex).FindControl("objtxtCompanycode").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strUserId.Trim = "" Then
    '            ShowMessage("Company name Name is compulsory information, it can not be blank.", MessageType.Info)
    '            grdLink.Rows(grdLink.EditIndex).FindControl("objtxtCompanyname").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strEmail.Trim = "" Then
    '            ShowMessage("Email Name is compulsory information, it can not be blank.", MessageType.Info)
    '            grdLink.Rows(grdLink.EditIndex).FindControl("objtxtEmail").Focus()
    '            e.Cancel = True
    '            Exit Try

    '        End If

    '        If clsApplicant.IsValidEmail(strEmail) = False Then
    '            ShowMessage("Please enter proper email.", MessageType.Info)
    '            grdLink.Rows(grdLink.EditIndex).FindControl("objtxtEmail").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        End If

    '        dsLink.UpdateParameters("intActivationLinkUnkId").DefaultValue = intActivationUnkId.ToString
    '        dsLink.UpdateParameters("strCompCode").DefaultValue = strCompcode
    '        dsLink.UpdateParameters("intComUnkID").DefaultValue = intCompanyunkid.ToString
    '        dsLink.UpdateParameters("strEmail").DefaultValue = strEmail
    '        dsLink.UpdateParameters("strUserId").DefaultValue = strUserId
    '        dsLink.UpdateParameters("blnCanViewErrorLog").DefaultValue = blnCanViewErrorLog.ToString
    '        dsLink.UpdateParameters("blnCanTruncateErrorLog").DefaultValue = blnCanTruncateErrorLog.ToString
    '        dsLink.UpdateParameters("blnCanShrinkDataBase").DefaultValue = blnCanShrinkDataBase.ToString
    '        dsLink.UpdateParameters("blnIsDeleted").DefaultValue = (False).ToString
    '        dsLink.UpdateParameters("strAuditEmail").DefaultValue = Session("email").ToString
    '        dsLink.UpdateParameters("dtAuditDateTime").DefaultValue = Now.ToString


    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub



#End Region

End Class