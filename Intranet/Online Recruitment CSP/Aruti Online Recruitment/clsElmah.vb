﻿Public Class clsElmah
    Inherits Elmah.SqlErrorLog

    Protected connectionStringName As String

    Public Sub New(config As IDictionary)
        MyBase.New(config)

        connectionStringName = config("connectionStringName")
    End Sub

    Public Overrides ReadOnly Property ConnectionString As String
        Get
            Return HttpContext.Current.Session("ConnectionString").ToString()
        End Get
    End Property

End Class
