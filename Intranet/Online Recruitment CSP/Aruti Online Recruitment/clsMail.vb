﻿Option Strict On

Imports System.Net.Mail
'Imports eZeeCommonLib
Imports Microsoft.Exchange.WebServices.Data

Public Class clsMail

#Region " Methods Functions "

    Public Function SendEmail(strToEmail As String _
                              , strSubject As String _
                              , strBody As String _
                              , Optional blnAddLogo As Boolean = False _
                              , Optional blnSuperAdmin As Boolean = False _
                              , Optional mstrAttachedFiles As String = "") As Boolean

        'Pinkal (18-May-2018) --  'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[  , Optional mstrAttachedFiles As String = ""]

        Dim message As MailMessage
        Dim smtp As SmtpClient
        Try

            Dim strSenderAddress As String = ""
            Dim strMailServerIP As String = ""
            Dim intMailServerPort As Integer = 0
            Dim strUserName As String = ""
            Dim strPassword As String = ""
            Dim blnIsloginssl As Boolean = False
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            Dim strSenderName As String = ""
            Dim intEmail_Type As Integer = 0
            Dim strEWs_URL As String = ""
            Dim strEWs_Domain As String = ""
            'Sohail (30 Nov 2017) -- End

            'If HttpContext.Current.Session("m_strLogFile") Is Nothing OrElse CStr(HttpContext.Current.Session("m_strLogFile")).Trim = "" Then
            '    HttpContext.Current.Session("m_strLogFile") = IO.Path.Combine(HttpContext.Current.Server.MapPath("~") & "/UploadImage", "EmailSetting.txt")
            'End If
            'System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "Send Email : emailsetupunkid : " & CStr(HttpContext.Current.Session("e_emailsetupunkid")) & vbCrLf)
            'If CInt(HttpContext.Current.Session("e_emailsetupunkid")) > 0 Then
            '    System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "e email : " & CStr(HttpContext.Current.Session("e_username")) & vbCrLf)
            '    System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "e sender name : " & CStr(HttpContext.Current.Session("e_sendername")) & vbCrLf)
            'Else
            '    System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "email : " & CStr(HttpContext.Current.Session("username")) & vbCrLf)
            '    System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "sender name : " & CStr(HttpContext.Current.Session("sendername")) & vbCrLf)
            'End If

            'System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "super admin : " & CStr(blnSuperAdmin.ToString) & vbCrLf)
            If blnSuperAdmin = False Then
                If HttpContext.Current.Session("e_emailsetupunkid") IsNot Nothing AndAlso CInt(HttpContext.Current.Session("e_emailsetupunkid")) > 0 Then
                    strSenderAddress = HttpContext.Current.Session("e_senderaddress").ToString.Trim
                    strMailServerIP = HttpContext.Current.Session("e_mailserverip").ToString
                    intMailServerPort = CInt(HttpContext.Current.Session("e_mailserverport"))
                    strUserName = HttpContext.Current.Session("e_username").ToString
                    strPassword = HttpContext.Current.Session("e_password").ToString
                    blnIsloginssl = CBool(HttpContext.Current.Session("e_isloginssl"))
                    strSenderName = HttpContext.Current.Session("e_sendername").ToString
                    intEmail_Type = CInt(HttpContext.Current.Session("e_email_type"))
                    strEWs_URL = HttpContext.Current.Session("e_ews_url").ToString()
                    strEWs_Domain = HttpContext.Current.Session("e_ews_domain").ToString()
                Else
                    strSenderAddress = HttpContext.Current.Session("senderaddress").ToString.Trim
                    strMailServerIP = HttpContext.Current.Session("mailserverip").ToString
                    intMailServerPort = CInt(HttpContext.Current.Session("mailserverport"))
                    strUserName = HttpContext.Current.Session("username").ToString
                    strPassword = HttpContext.Current.Session("password").ToString
                    blnIsloginssl = CBool(HttpContext.Current.Session("isloginssl"))
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    strSenderName = HttpContext.Current.Session("sendername").ToString
                    intEmail_Type = CInt(HttpContext.Current.Session("email_type"))
                    strEWs_URL = HttpContext.Current.Session("ews_url").ToString()
                    strEWs_Domain = HttpContext.Current.Session("ews_domain").ToString()
                    'Sohail (30 Nov 2017) -- End
                End If
            Else
                blnAddLogo = False

                If HttpContext.Current.Session("sadminsenderaddress") Is Nothing OrElse HttpContext.Current.Session("sadminsenderaddress").ToString = "" Then
                    Dim objSA As New clsSACommon
                    Call objSA.GetSuperAdminEmailSettings()
                End If

                strSenderAddress = HttpContext.Current.Session("sadminsenderaddress").ToString.Trim
                strMailServerIP = HttpContext.Current.Session("sadminmailserverip").ToString
                intMailServerPort = CInt(HttpContext.Current.Session("sadminmailserverport"))
                strUserName = HttpContext.Current.Session("sadminusername").ToString
                strPassword = HttpContext.Current.Session("sadminpassword").ToString
                blnIsloginssl = CBool(HttpContext.Current.Session("sadminisloginssl"))
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                strSenderName = HttpContext.Current.Session("sadminsendername").ToString
                intEmail_Type = CInt(HttpContext.Current.Session("sadminemail_type"))
                strEWs_URL = HttpContext.Current.Session("sadminews_url").ToString()
                strEWs_Domain = HttpContext.Current.Session("sadminews_domain").ToString()
                'Sohail (30 Nov 2017) -- End
            End If

            'Dim strSenderEmail As String = HttpContext.Current.Session("username").ToString.Trim
            Dim strSenderEmail As String = strUserName
            'Dim strSenderEmail As String = strSenderAddress
            strToEmail = strToEmail.Trim
            strSubject = strSubject.Trim

            If intEmail_Type = 0 Then '0 = SMTP, 1 = EWS

                'message = New MailMessage(strSenderEmail, strToEmail)
                Dim strSenderDispName As String = strSenderName
                If strSenderDispName.Trim.Length <= 0 Then
                    strSenderDispName = strSenderEmail
                End If
                message = New MailMessage(New MailAddress(strSenderEmail, strSenderDispName), New MailAddress(strToEmail))
                message.To.Add(strToEmail)
                message.Subject = strSubject
                message.IsBodyHtml = True
                'message.Body = strBody

                'System.IO.File.AppendAllText(CStr(HttpContext.Current.Session("m_strLogFile")), "SenderEmail : " & strSenderEmail & "; Sender Display Name : " & strSenderDispName & vbCrLf)

                strBody = strBody.Replace("#CompanyName#", HttpContext.Current.Session("CompName").ToString())

                If blnAddLogo = False Then
                    strBody = strBody.Replace("#imgcmp#", "").Replace("#imgaruti#", "")
                    message.Body = strBody
                Else
                    strBody = strBody.Replace("#imgcmp#", "<img style='height:  128px; width: 225px;' height='128' width='225' src='cid:InlineImageCompID' /> <br /> ").Replace("#imgaruti#", "<img src='cid:InlineImageID' style='height:75px; width: 90px;' height='75' width='90' />")

                    Dim HTMLEmail As AlternateView = AlternateView.CreateAlternateViewFromString(strBody, Nothing, "text/html")
                    Dim PlainTextEmail As AlternateView = AlternateView.CreateAlternateViewFromString(strBody, Nothing, "text/plain")


                    Dim CompImage As LinkedResource
                    If HttpContext.Current.Session("image") IsNot Nothing AndAlso IsDBNull(HttpContext.Current.Session("image")) = False Then
                        Dim bytes As Byte() = DirectCast(HttpContext.Current.Session("image"), Byte())
                        'Dim base64String As String = Convert.ToBase64String(bytes)
                        'HTMLEmail.LinkedResources.Add(HttpContext.Current.Session("image"))
                        CompImage = New LinkedResource(New IO.MemoryStream(bytes))
                    Else
                        'CompImage = New LinkedResource(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & "/" & HttpContext.Current.Request.ApplicationPath & "/Images/blank.png")
                        CompImage = New LinkedResource(HttpContext.Current.Server.MapPath("~") & "/Images/blank.png")
                    End If
                    CompImage.ContentId = "InlineImageCompID"
                    HTMLEmail.LinkedResources.Add(CompImage)

                    If HttpContext.Current.Request.Url.AbsoluteUri.ToString.ToLower.Contains("arutihr.com") = True Then
                        'Dim ArutiImage As LinkedResource = New LinkedResource(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & HttpContext.Current.Request.ApplicationPath & "/Images/logo_aruti.png")
                        Dim ArutiImage As LinkedResource = New LinkedResource(HttpContext.Current.Server.MapPath("~") & "/Images/logo_aruti.png")
                        ArutiImage.ContentId = "InlineImageID"
                        HTMLEmail.LinkedResources.Add(ArutiImage)
                    End If

                    message.AlternateViews.Add(HTMLEmail)
                        'message.AlternateViews.Add(PlainTextEmail)

                    End If


                    'Pinkal (18-May-2018) -- Start
                    'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
                    If mstrAttachedFiles <> "" Then
                    Dim strAtt() As String
                    strAtt = mstrAttachedFiles.Split(CChar(","))
                    If strAtt.Length > 0 Then
                        For i As Integer = 0 To strAtt.Length - 1
                            If IO.File.Exists(strAtt(i)) Then message.Attachments.Add(New System.Net.Mail.Attachment(strAtt(i)))
                        Next
                    End If
                End If
                'Pinkal (18-May-2018) -- End

                smtp = New SmtpClient
                smtp.Host = strMailServerIP
                smtp.Port = intMailServerPort
                smtp.Credentials = New System.Net.NetworkCredential(strUserName, strPassword)
                'smtp.Credentials = New System.Net.NetworkCredential(HttpContext.Current.Session("username").ToString, clsSecurity.Decrypt(HttpContext.Current.Session("password").ToString, "ezee"))
                smtp.EnableSsl = blnIsloginssl

                'Sohail (22 Jan 2020) -- Start
                'ZRA Issue - # 0004046 : System Failure to send activation Link.
                System.Net.ServicePointManager.Expect100Continue = False
                System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
                'Sohail (22 Jan 2020) -- End
                Try
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
                Catch ex As Exception

                End Try

                smtp.Send(message)

            ElseIf intEmail_Type = 1 Then '0 = SMTP, 1 = EWS

                'Creating the ExchangeService Object 
                Dim objService As New ExchangeService()

                'Seting Credentials to be used
                objService.Credentials = New WebCredentials(strSenderName, strPassword, strEWs_Domain)

                'Setting the EWS Uri
                objService.Url = New Uri(strEWs_URL)

                'Creating an Email Service and passing in the service
                Dim objMessage As New EmailMessage(objService)

                'Setting Email message properties
                objMessage.Subject = strSubject
                'objMessage.Body = mstrMessage
                objMessage.ToRecipients.Add(strToEmail)
                If blnAddLogo = False Then
                    strBody = strBody.Replace("#imgcmp#", "").Replace("#imgaruti#", "")
                Else
                    strBody = strBody.Replace("#imgcmp#", "<img style='height:  128px; width: 225px;' src='cid:InlineImageCompID' /> <br /> ").Replace("#imgaruti#", "<img src='cid:InlineImageID' style='height:75px; width: 90px;' height='75' width='90'/>")

                    If HttpContext.Current.Request.Url.AbsoluteUri.ToString.ToLower.Contains("arutihr.com") = True Then
                        If HttpContext.Current.Session("image") IsNot Nothing AndAlso IsDBNull(HttpContext.Current.Session("image")) = False Then
                            Dim bytes As Byte() = DirectCast(HttpContext.Current.Session("image"), Byte())
                            objMessage.Attachments.AddFileAttachment("logo_aruti.png", bytes)

                        Else
                            objMessage.Attachments.AddFileAttachment("logo_aruti.png", HttpContext.Current.Server.MapPath("") & "/Images/blank.png")
                        End If

                        objMessage.Attachments(0).IsInline = True
                        objMessage.Attachments(0).ContentId = "logo_aruti.png"
                    End If

                End If

                    'Pinkal (18-May-2018) -- Start
                    'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
                    If mstrAttachedFiles <> "" Then
                    Dim strAtt() As String
                    strAtt = mstrAttachedFiles.Split(CChar(","))
                    If strAtt.Length > 0 Then
                        For i As Integer = 0 To strAtt.Length - 1
                            If IO.File.Exists(strAtt(i)) Then objMessage.Attachments.AddFileAttachment(strAtt(i))
                        Next
                    End If
                End If
                'Pinkal (18-May-2018) -- End


                objMessage.Body = strBody

                'objMessage.Send()
                objMessage.SendAndSaveCopy()

            End If


            Return True

        Catch ex As Exception
            'Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current,,, False)
            Return False
        End Try
    End Function
#End Region
End Class
