﻿Imports System.Web.Profile

Public Class UserProfile
    Inherits ProfileBase

    Public Shared Function GetUserProfile(username As String) As UserProfile
        Return TryCast(Create(username), UserProfile)
    End Function

    Public Shared Function GetUserProfile() As UserProfile
        Return TryCast(Create(Membership.GetUser().UserName), UserProfile)
    End Function

    '<SettingsAllowAnonymous(False)>
    'Public Property Description() As String
    '    Get
    '        Return TryCast(MyBase.Item("Description"), String)
    '    End Get
    '    Set
    '        MyBase.Item("Description") = Value
    '    End Set
    'End Property

    '<SettingsAllowAnonymous(False)>
    'Public Property Location() As String
    '    Get
    '        Return TryCast(MyBase.Item("Location"), String)
    '    End Get
    '    Set
    '        MyBase.Item("Location") = Value
    '    End Set
    'End Property

    <SettingsAllowAnonymous(False)>
    Public Property FirstName() As String
        Get
            Return TryCast(MyBase.Item("FirstName"), String)
        End Get
        Set
            MyBase.Item("FirstName") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property MiddleName() As String
        Get
            Return TryCast(MyBase.Item("MiddleName"), String)
        End Get
        Set
            MyBase.Item("MiddleName") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property LastName() As String
        Get
            Return TryCast(MyBase.Item("LastName"), String)
        End Get
        Set
            MyBase.Item("LastName") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property Gender() As Integer
        Get
            Dim i As Integer
            Integer.TryParse(MyBase.Item("Gender"), i)
            Return i
        End Get
        Set
            MyBase.Item("Gender") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property BirthDate() As Date
        Get
            Dim d As Date
            Date.TryParse(MyBase.Item("BirthDate"), d)
            Return d
        End Get
        Set
            MyBase.Item("BirthDate") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property MobileNo() As String
        Get
            Return TryCast(MyBase.Item("MobileNo"), String)
        End Get
        Set
            MyBase.Item("MobileNo") = Value
        End Set
    End Property

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    <SettingsAllowAnonymous(False)>
    Public Property NIN() As String
        Get
            Return TryCast(MyBase.Item("nin"), String)
        End Get
        Set
            MyBase.Item("nin") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property TIN() As String
        Get
            Return TryCast(MyBase.Item("tin"), String)
        End Get
        Set
            MyBase.Item("tin") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property NinMobile() As String
        Get
            Return TryCast(MyBase.Item("ninmobile"), String)
        End Get
        Set
            MyBase.Item("ninmobile") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property Village() As String
        Get
            Return TryCast(MyBase.Item("village"), String)
        End Get
        Set
            MyBase.Item("village") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property Phonenumber() As String
        Get
            Return TryCast(MyBase.Item("phonenumber"), String)
        End Get
        Set
            MyBase.Item("phonenumber") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property Nationality() As Integer
        Get
            Dim i As Integer
            Integer.TryParse(MyBase.Item("nationality"), i)
            Return i
        End Get
        Set
            MyBase.Item("nationality") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property ApplicantPhoto() As String
        Get
            Return TryCast(MyBase.Item("ApplicantPhoto"), String)
        End Get
        Set
            MyBase.Item("ApplicantPhoto") = Value
        End Set
    End Property

    <SettingsAllowAnonymous(False)>
    Public Property ApplicantSignature() As String
        Get
            Return TryCast(MyBase.Item("ApplicantSignature"), String)
        End Get
        Set
            MyBase.Item("ApplicantSignature") = Value
        End Set
    End Property
    'S.SANDEEP |04-MAY-2023| -- END

End Class
