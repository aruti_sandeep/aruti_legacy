﻿Imports System.Globalization

Public Class eZeeDate
    ''' <summary>
    ''' Convert system date in Database Date format(YYYYMMDD).
    ''' </summary>
    ''' <param name="Date">Date in System Format.</param> 
    ''' <returns>Date as string in database date Format(YYYYMMDD).</returns>
    Public Shared Function convertDate(ByVal [Date] As System.DateTime) As String
        Return String.Format("{0:0000}{1:00}{2:00}", [Date].Year, [Date].Month, [Date].Day)
    End Function

    ''' <summary>
    ''' Convert Database Formated Date string in system date format.
    ''' </summary>
    ''' <param name="DateSting">Date as string in database date Format(YYYYMMDD).</param> 
    ''' <returns>Date in System Format.</returns>
    Public Shared Function convertDate(ByVal DateSting As String) As System.DateTime
        Try


            Return New DateTime(Integer.Parse(DateSting.Substring(0, 4)), Integer.Parse(DateSting.Substring(4, 2)), Integer.Parse(DateSting.Substring(6, 2)))
        Catch ex As Exception
            Throw New Exception(ex.Message & "[convertDate]")
        End Try
    End Function

    ''' <summary>
    ''' Convert system date in Database Date Time format(YYYYMMDD HH:mm:ss).
    ''' </summary>
    ''' <returns>Date as string in database date Format(YYYYMMDD HH:mm:ss).</returns>
    Public Shared Function convertDateTime(ByVal [Date] As System.DateTime) As String
        Return String.Format("{0:0000}{1:00}{2:00} {3:00}:{4:00}:{5:00}", [Date].Year, [Date].Month, [Date].Day, [Date].Hour, [Date].Minute,
         [Date].Second)
    End Function

    ''' <summary>
    ''' Convert given different date and time in Database Date Time format(YYYYMMDD HH:mm:ss).
    ''' </summary>
    ''' <param name="Date">Date in System Format.</param> 
    ''' <param name="Time">Time in System Format.</param> 
    ''' <returns>Date as string in database date Format(YYYYMMDD HH:mm:ss).</returns>
    Public Shared Function convertDateTime(ByVal [Date] As System.DateTime, ByVal Time As System.DateTime) As String
        Return String.Format("{0:0000}{1:00}{2:00} {3:00}:{4:00}:{5:00}", [Date].Year, [Date].Month, [Date].Day, Time.Hour, Time.Minute,
         Time.Second)
    End Function

    ''' <summary>
    ''' Convert Database Formated Date/Time string in system date format.
    ''' </summary>
    ''' <param name="DateTimeSting">Date as string in database date Format(YYYYMMDD HH:mm:ss [112] + ' ' + [108]).</param> 
    ''' <returns>Date/Time in System Format.</returns>
    Public Shared Function convertDateTime(ByVal DateTimeSting As String) As System.DateTime
        Try
            Return New DateTime(Integer.Parse(DateTimeSting.Substring(0, 4)), Integer.Parse(DateTimeSting.Substring(4, 2)), Integer.Parse(DateTimeSting.Substring(6, 2)), Integer.Parse(DateTimeSting.Substring(9, 2)), Integer.Parse(DateTimeSting.Substring(12, 2)), Integer.Parse(DateTimeSting.Substring(15, 2)))
        Catch ex As Exception
            Throw New Exception(ex.Message & "[convertDateTime]")
        End Try
    End Function

    ''' <summary>
    ''' Convert separately Database Format Date Time string in system date format.
    ''' </summary>
    ''' <param name="DateSting">Date as string in database Format(YYYYMMDD [112]).</param> 
    ''' <param name="TimeSting">Time as string in database Format(HH:mm:ss [108]).</param> 
    ''' <returns>Date/Time in System Format.</returns>
    Public Shared Function convertDateTime(ByVal DateSting As String, ByVal TimeSting As String) As System.DateTime
        Try
            Return New DateTime(Integer.Parse(DateSting.Substring(0, 4)), Integer.Parse(DateSting.Substring(4, 2)), Integer.Parse(DateSting.Substring(6, 2)), Integer.Parse(TimeSting.Substring(0, 2)), Integer.Parse(TimeSting.Substring(3, 2)), Integer.Parse(TimeSting.Substring(6, 2)))
        Catch ex As Exception
            Throw New Exception(ex.Message & "[convertDateTime]")
        End Try
    End Function

    ''' <summary>
    ''' Replace given Date's time value with given Time.
    ''' </summary>
    ''' <param name="Date">Date in System Format.</param>
    ''' <param name="Time">Time in System Format.</param>
    ''' <returns>Date Time in System Format.</returns>
    Public Shared Function ReplaceTime(ByVal [Date] As System.DateTime, ByVal Time As System.DateTime) As System.DateTime
        Try
            Return New DateTime([Date].Year, [Date].Month, [Date].Day, Time.Hour, Time.Minute, Time.Second,
             Time.Millisecond)
        Catch ex As Exception
            Throw New Exception(ex.Message & "[ReplaceTime]")
        End Try
    End Function

    ''' <summary>
    ''' Replace given Date's time value with given Time.
    ''' </summary>
    ''' <param name="Date">Date in System Format.</param>
    ''' <param name="TimeSting">Time in Database Format.</param>
    ''' <returns>Date Time in System Format.</returns>
    Public Shared Function ReplaceTime(ByVal [Date] As System.DateTime, ByVal TimeSting As String) As System.DateTime
        Try
            Return New DateTime([Date].Year, [Date].Month, [Date].Day, Integer.Parse(TimeSting.Substring(0, 2)), Integer.Parse(TimeSting.Substring(3, 2)), Integer.Parse(TimeSting.Substring(6, 2)))
        Catch ex As Exception
            Throw New Exception(ex.Message & "[ReplaceTime]")
        End Try
    End Function

    ''' <summary>
    ''' Convert system time in Database Time format(HH:mm:ss).
    ''' </summary>
    ''' <returns>Time as string in database date Format(HH:mm:ss).</returns>
    Public Shared Function convertTime(ByVal Time As System.DateTime) As String
        Try
            Return String.Format("{0:00}:{1:00}:{2:00}", Time.Hour, Time.Minute, Time.Second)
        Catch ex As Exception
            Throw New Exception(ex.Message & "[convertTime]")
        End Try
    End Function


#Region " Old Code "

    ''// <summary>
    ''// Convert Database Date in system date format.
    ''// </summary>
    ''// <param name="strDate">Date as string in database date Format(YYYYMMDD).</param> 
    ''// <returns>Date in System Format.</returns>
    'public static System.DateTime convertDate(string strDate)
    '{
    '    System.DateTime dtTemp = DateTime.MinValue;
    '    CultureInfo culture = null;

    '    try
    '    {
    '        culture = CultureInfo.CurrentCulture;

    '        string fmtDate = culture.DateTimeFormat.ShortDatePattern;
    '        string strYear = strDate.Substring(0, 4);
    '        string strMonth = strDate.Substring(4, 2);
    '        string strDay = strDate.Substring(strDate.Length - 2);
    '        string[] part = null;
    '        string strDateSeparator = culture.DateTimeFormat.DateSeparator;

    '        part = fmtDate.Split(strDateSeparator.ToCharArray());

    '        string strDateFormat = null;
    '        strDateFormat = part[0].Substring(0, 1).ToUpper() + part[1].Substring(0, 1).ToUpper() + part[2].Substring(0, 1).ToUpper();

    '        switch (strDateFormat)
    '        {
    '            case "DMY":
    '                dtTemp = System.Convert.ToDateTime(strDay + strDateSeparator + strMonth + strDateSeparator + strYear);
    '                break;
    '            case "DYM":
    '                dtTemp = System.Convert.ToDateTime(strDay + strDateSeparator + strYear + strDateSeparator + strMonth);
    '                break;
    '            case "MDY":
    '                dtTemp = System.Convert.ToDateTime(strMonth + strDateSeparator + strDay + strDateSeparator + strYear);
    '                break;
    '            case "MYD":
    '                dtTemp = System.Convert.ToDateTime(strMonth + strDateSeparator + strYear + strDateSeparator + strDay);
    '                break;
    '            case "YDM":
    '                dtTemp = System.Convert.ToDateTime(strYear + strDateSeparator + strDay + strDateSeparator + strMonth);
    '                break;
    '            case "YMD":
    '                dtTemp = System.Convert.ToDateTime(strYear + strDateSeparator + strMonth + strDateSeparator + strDay);
    '                break;
    '        }

    '        return dtTemp;

    '    }
    '    catch (System.Exception ex)
    '    {
    '        throw new Exception( ex.Message + "[convertDate]");
    '    }
    '    finally
    '    {
    '        culture = null;
    '    }
    '}

    ''// <summary>
    ''// Convert system date in Database Date format(YYYYMMDD).
    ''// </summary>
    ''// <param name="dtDate">Date in System Format.</param> 
    ''// <returns>Date as string in database date Format(YYYYMMDD).</returns>
    'public static string convertDate(System.DateTime dtDate)
    '{
    '    string strDate = "";
    '    try
    '    {
    '        strDate = dtDate.Year.ToString("####") + dtDate.Month.ToString("0#") + dtDate.Day.ToString("0#");

    '        return strDate;

    '    }
    '    catch (Exception ex)
    '    {
    '        throw new Exception(ex.Message + "[convertDate]");
    '        //return "";
    '    }
    '}

#End Region
End Class
