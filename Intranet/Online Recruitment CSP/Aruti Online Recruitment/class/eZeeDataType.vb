﻿Imports System.Drawing

Public Enum enDataType As Integer
    ALIAS_SIZE = 5
    NAME_SIZE = 255
    DESC_SIZE = 1000
    ADD_SIZE = 255
    CITY_SIZE = 100
    STATE_SIZE = 100
    ZIPCODE_SIZE = 15
    COUNTRY_SIZE = 100
    EMAIL_SIZE = 100
    FAX_SIZE = 20
    PHONE_SIZE = 15
    WEBSITE_SIZE = 255
    GENDER_SIZE = 10
    CARDNO_SIZE = 50
    EXPDATE_SIZE = 10
    REGISTRATION_SIZE = 20
    VOUCHERNO_SIZE = 25
    GUID_SIZE = 16
    KEYNAME_SIZE = 255
    KEYVALUE_SIZE = 50
    IP_SIZE = 15

    INT_SIZE = 4
    FLOAT_SIZE = 8
    MONEY_SIZE = 8
    DATETIME_SIZE = 17
    IMAGE_SIZE = 16
    BIT_SIZE = 1
    DECIMAL_SIZE = 36

End Enum

Public Class eZeeDataType


#Region "Data Field Size Constant"

    ''' <summary>
    ''' 5
    ''' </summary>
    Public Shared ReadOnly Property ALIAS_SIZE() As Integer
        Get
            Return 5
        End Get
    End Property

    ''' <summary>
    ''' 255
    ''' </summary>
    Public Shared ReadOnly Property NAME_SIZE() As Integer
        Get
            Return 255
        End Get
    End Property

    ''' <summary>
    ''' 1000
    ''' </summary>
    Public Shared ReadOnly Property DESC_SIZE() As Integer
        Get
            Return 1000
        End Get
    End Property

    ''' <summary>
    ''' 255
    ''' </summary>
    Public Shared ReadOnly Property ADD_SIZE() As Integer
        Get
            Return 255
        End Get
    End Property

    ''' <summary>
    ''' 100
    ''' </summary>
    Public Shared ReadOnly Property CITY_SIZE() As Integer
        Get
            Return 100
        End Get
    End Property

    ''' <summary>
    ''' 100
    ''' </summary>
    Public Shared ReadOnly Property STATE_SIZE() As Integer
        Get
            Return 100
        End Get
    End Property

    ''' <summary>
    ''' 15
    ''' </summary>
    Public Shared ReadOnly Property ZIPCODE_SIZE() As Integer
        Get
            Return 15
        End Get
    End Property

    ''' <summary>
    ''' 100
    ''' </summary>
    Public Shared ReadOnly Property COUNTRY_SIZE() As Integer
        Get
            Return 100
        End Get
    End Property

    ''' <summary>
    ''' 100
    ''' </summary>
    Public Shared ReadOnly Property EMAIL_SIZE() As Integer
        Get
            Return 100
        End Get
    End Property

    ''' <summary>
    ''' 20
    ''' </summary>
    Public Shared ReadOnly Property FAX_SIZE() As Integer
        Get
            Return 20
        End Get
    End Property

    ''' <summary>
    ''' 15
    ''' </summary>
    Public Shared ReadOnly Property PHONE_SIZE() As Integer
        Get
            Return 15
        End Get
    End Property

    ''' <summary>
    ''' 255
    ''' </summary>
    Public Shared ReadOnly Property WEBSITE_SIZE() As Integer
        Get
            Return 255
        End Get
    End Property

    ''' <summary>
    ''' 10
    ''' </summary>
    Public Shared ReadOnly Property GENDER_SIZE() As Integer
        Get
            Return 10
        End Get
    End Property

    ''' <summary>
    ''' 50
    ''' </summary>
    Public Shared ReadOnly Property CARDNO_SIZE() As Integer
        Get
            Return 50
        End Get
    End Property

    ''' <summary>
    ''' 10
    ''' </summary>
    Public Shared ReadOnly Property EXPDATE_SIZE() As Integer
        Get
            Return 10
        End Get
    End Property

    ''' <summary>
    ''' 20
    ''' </summary>
    Public Shared ReadOnly Property REGISTRATION_SIZE() As Integer
        Get
            Return 20
        End Get
    End Property

    ''' <summary>
    ''' 25
    ''' </summary>
    Public Shared ReadOnly Property VOUCHERNO_SIZE() As Integer
        Get
            Return 25
        End Get
    End Property

    ''' <summary>
    ''' 16
    ''' </summary>
    Public Shared ReadOnly Property GUID_SIZE() As Integer
        Get
            Return 16
        End Get
    End Property

    ''' <summary>
    ''' 255
    ''' </summary>
    Public Shared ReadOnly Property KEYNAME_SIZE() As Integer
        Get
            Return 255
        End Get
    End Property

    ''' <summary>
    ''' 50
    ''' </summary>
    Public Shared ReadOnly Property KEYVALUE_SIZE() As Integer
        Get
            Return 50
        End Get
    End Property

    ''' <summary>
    ''' 15
    ''' </summary>
    Public Shared ReadOnly Property IP_SIZE() As Integer
        Get
            Return 15
        End Get
    End Property


#End Region

#Region "Data Type Size Constant"
    ''' <summary>
    ''' 4
    ''' </summary>
    Public Shared ReadOnly Property INT_SIZE() As Integer
        Get
            Return 4
        End Get
    End Property

    ''' <summary>
    ''' 8
    ''' </summary>
    Public Shared ReadOnly Property FLOAT_SIZE() As Integer
        Get
            Return 8
        End Get
    End Property

    ''' <summary>
    ''' 8
    ''' </summary>
    Public Shared ReadOnly Property MONEY_SIZE() As Integer
        Get
            Return 8
        End Get
    End Property

    ''' <summary>
    ''' 17
    ''' </summary>
    Public Shared ReadOnly Property DATETIME_SIZE() As Integer
        Get
            Return 17
        End Get
    End Property

    ''' <summary>
    ''' 16
    ''' </summary>
    Public Shared ReadOnly Property IMAGE_SIZE() As Integer
        Get
            Return 16
        End Get
    End Property

    ''' <summary>
    ''' 1
    ''' </summary>
    Public Shared ReadOnly Property BIT_SIZE() As Integer
        Get
            Return 1
        End Get
    End Property

    Public Shared ReadOnly Property DECIMAL_SIZE() As Integer
        Get
            Return 36
        End Get
    End Property


#End Region

    ''' <summary>
    ''' Convert database image data field value into image.
    ''' </summary>
    ''' <param name="Expression">A image data field value.</param> 
    ''' <returns>A image which is converted from image data field value.</returns>
    Public Shared Function data2Image(ByVal Expression As Object) As Image
        Try
            If System.Convert.IsDBNull(Expression) Then
                Return Nothing
            Else
                Dim imgContent As Byte() = DirectCast(Expression, Byte())
                Dim stream As New System.IO.MemoryStream(imgContent)
                Dim image__1 As Image = Image.FromStream(stream)
                Return image__1
            End If
        Catch ex As System.Exception
            '	System.Windows.Forms.MessageBox.Show(ex.Message & "[data2Image-eZeeDataType]")
            Return Nothing

        Finally
        End Try
    End Function

    ''' <summary>
    ''' Convert image into database image data field value.
    ''' </summary>
    ''' <param name="Expression">A image.</param> 
    ''' <returns>A image data field value which is converted from image.</returns>
    Public Shared Function image2Data(ByVal Expression As Image) As Byte()
        Try
            If Expression Is Nothing Then
                Return Nothing
            Else

                Dim stream As New System.IO.MemoryStream()
                'pression.RawFormat 
                Expression.Save(stream, System.Drawing.Imaging.ImageFormat.Png)
                Dim imgContent As Byte() = stream.ToArray()

                Return imgContent

            End If
        Catch ex As System.Exception
            '	System.Windows.Forms.MessageBox.Show(ex.Message & "[image2Data-eZeeDataType]")
            Return Nothing

        Finally
        End Try
    End Function

    Public Shared Function ResizeImage(ByVal picture As Image, ByVal img_size As Size) As Image
        If picture Is Nothing Then
            Return Nothing
        End If
        Dim inp As New IntPtr()

        Return picture.GetThumbnailImage(img_size.Width, img_size.Height, Nothing, inp)
    End Function

End Class
