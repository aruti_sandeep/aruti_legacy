﻿Option Strict On
Imports System.ComponentModel
Imports System.Data.SqlClient

Public Class clsConfiguration

    Public Shared Function GetConfigurationValue(ByVal strCompCode As String _
                                         , intCompanyUnkID As Integer _
                                         , strKeyName As String
                                         ) As String

        Dim ds As New DataSet
        Dim strValue As String = ""
        Try
            'Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            Dim strQ As String = "procGetConfigurationValue"

            If strCompCode Is Nothing Then strCompCode = ""
            If strKeyName Is Nothing Then strKeyName = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyUnkID
                        cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@key_name", SqlDbType.NVarChar)).Value = strKeyName.Trim.Replace("'", "''")

                        Dim s As Object = cmd.ExecuteScalar()

                        If s IsNot Nothing Then
                            strValue = s.ToString
                        End If

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return strValue
    End Function

End Class
