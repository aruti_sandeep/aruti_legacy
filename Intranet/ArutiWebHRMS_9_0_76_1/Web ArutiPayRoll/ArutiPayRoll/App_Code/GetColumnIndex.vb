﻿Imports Microsoft.VisualBasic


'Pinkal (13-Aug-2020) -- Start
'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

Public Module GetColumnIndex
    Public Function getColumnID_Griview(ByVal gv As GridView, ByVal input As String, Optional ByVal throwExceptionIfFailed As Boolean = False, Optional ByVal ByFooter As Boolean = True) As Integer
        Dim result As Integer = -1
        Try
        If input.Trim() <> "" Then
            Dim datacol = gv.Columns
            Dim icol = CObj(Nothing)

            If ByFooter Then
                icol = datacol.Cast(Of DataControlField)().Where(Function(x) x.FooterText = input).[Select](Function(x) x).FirstOrDefault()
            Else
                icol = datacol.Cast(Of DataControlField)().Where(Function(x) x.HeaderText = input).[Select](Function(x) x).FirstOrDefault()
            End If

            result = gv.Columns.IndexOf(CType(icol, DataControlField))

            If result < 0 Then
                If throwExceptionIfFailed Then
                    Throw New FormatException(String.Format("'{0}' not found as column in gridview.", input))
                End If
            End If
        End If
        Catch ex As Exception
            Throw New Exception("getColumnID_Griview : " & ex.Message)
        End Try
        Return result
    End Function

    'S.SANDEEP |25-MAR-2019| -- START
    Public Function getColumnId_Datagrid(ByVal gd As DataGrid, ByVal input As String, Optional ByVal throwExceptionIfFailed As Boolean = False, Optional ByVal ByFooter As Boolean = True) As Integer
        Dim result As Integer = -1
        Try
        If input.Trim() <> "" Then
            Dim datacol = gd.Columns
            Dim icol = CObj(Nothing)
            If ByFooter Then
                icol = datacol.Cast(Of DataGridColumn)().Where(Function(x) x.FooterText = input).[Select](Function(x) x).FirstOrDefault()
            Else
                icol = datacol.Cast(Of DataGridColumn)().Where(Function(x) x.HeaderText = input).[Select](Function(x) x).FirstOrDefault()
            End If
            result = gd.Columns.IndexOf(CType(icol, DataGridColumn))
            If result < 0 Then
                If throwExceptionIfFailed Then
                    Throw New FormatException(String.Format("'{0}' not found as column in DataGrid.", input))
                End If
            End If
        End If
        Catch ex As Exception
            Throw New Exception("getColumnId_Datagrid : " & ex.Message)
        End Try
        Return result
    End Function
    'S.SANDEEP |25-MAR-2019| -- END

End Module

'Pinkal (13-Aug-2020) -- End


