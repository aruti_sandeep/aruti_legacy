﻿Imports System.Data
Imports System.Globalization


Partial Class Controls_TimeCtrl
    Inherits System.Web.UI.UserControl

    Dim clsdataopr As New eZeeCommonLib.clsDataOperation(True)
    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Public WriteOnly Property SetDate() As DateTime
        Set(ByVal value As DateTime)
            Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))

            If Format.Trim.Length <= 0 Then
                Format = culture.DateTimeFormat.ShortTimePattern.ToString()
            End If

            If (value = Nothing) Then
                TxtTime.Text = ""
            Else
                TxtTime.Text = IIf(IsDate(value), value.ToString(Format), CDate("01/01/1900"))
            End If
        End Set
    End Property

    Public ReadOnly Property GetDate() As DateTime
        Get
            Try
                Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))

                If Format.Trim.Length <= 0 Then
                    Format = culture.DateTimeFormat.ShortTimePattern.ToString()
                End If

                If IsNull() = False Then
                    Return DateTime.ParseExact(TxtTime.Text, Format, CultureInfo.InvariantCulture)
                Else
                    Return DateTime.ParseExact(Now.ToString(Format), Format, CultureInfo.InvariantCulture)
                End If

            Catch ex As Exception
                TxtTime.Text = ""
            End Try
        End Get
    End Property

    Public ReadOnly Property IsNull() As Boolean
        Get
            Try
                Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
                Dim dtDate As Date = DateTime.ParseExact(TxtTime.Text, Format, CultureInfo.InvariantCulture)
                If IsDate(dtDate) = False Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                TxtTime.Text = ""
                Return True
            End Try
        End Get
    End Property

    Public Property Enabled() As Boolean
        Get
            Return TxtTime.Enabled
        End Get
        Set(ByVal value As Boolean)
            TxtTime.Enabled = value
        End Set
    End Property

    Public Property ReadonlyDate() As Boolean
        Get
            Return TxtTime.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            TxtTime.ReadOnly = value
        End Set
    End Property

    Public Property Width() As Integer
        Get
            Return TxtTime.Width.Value
        End Get
        Set(ByVal value As Integer)
            TxtTime.Width = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return TxtTime.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            TxtTime.AutoPostBack = value
        End Set
    End Property

    Protected Sub TxtTime_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtTime.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

    Public Sub BindServerDateTime()
        Try
            Dim msql As String
            Dim ds As DataSet
            msql = "select Getdate() as ServerDt "
            ds = clsdataopr.WExecQuery(msql, "in")
            TxtTime.Text = ds.Tables(0).Rows(0)("ServerDt").ToString
            Me.SysDatetime = ds.Tables(0).Rows(0)("ServerDt").ToString
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("DateCtrl-->BindServerDateTime Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Private _pSysdate As Date
    Public Property SysDatetime() As Date
        Get
            Return _pSysdate
        End Get
        Set(ByVal value As Date)
            _pSysdate = value
        End Set
    End Property

    Private _Format As String = String.Empty
    Public Property Format() As String
        Get
            Return _Format
        End Get
        Set(ByVal value As String)
            _Format = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))

        If _Format.ToString.Trim.Length <= 0 Then
            _Format = culture.DateTimeFormat.ShortTimePattern.ToString()
        End If

    End Sub

End Class
