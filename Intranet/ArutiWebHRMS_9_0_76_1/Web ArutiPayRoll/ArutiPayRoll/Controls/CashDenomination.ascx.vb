﻿#Region "Imports"
Imports System.Data
Imports eZeeCommonLib.clsDataOperation
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.Web.Services
#End Region

Partial Class Controls_CashDenomination
    Inherits System.Web.UI.UserControl

    Public Event buttonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    'Private CInt(Me.ViewState("ctrmintCountryunkid")) As Integer = -1
    Private mstrDenomeFormat As String = "#####################.##"
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrId As Decimal = 0
    Private mintExchangeTranunkid As Integer = -1
    Private mdtCashDataSource As DataTable = Nothing
    Private mdtTran As DataTable
#End Region

#Region "Properties"

    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return lblMessage.Text
        End Get
        Set(ByVal value As String)
            lblMessage.Text = value
        End Set
    End Property

    Public Property Countryunkid() As Integer
        Get
            Return CInt(Me.ViewState("ctrmintCountryunkid"))
        End Get
        Set(ByVal value As Integer)
            Me.ViewState("ctrmintCountryunkid") = value
        End Set
    End Property

    Public Property ExchangeTranunkid() As Integer
        Get
            Return mintExchangeTranunkid
        End Get
        Set(ByVal value As Integer)
            mintExchangeTranunkid = value
        End Set
    End Property

    Public ReadOnly Property CashDataSource() As DataTable
        Get
            Return mdtCashDataSource
        End Get
    End Property

    'Public Property DataSource() As Object
    '    Get
    '        Return gvCashDenom.DataSource
    '    End Get
    '    Set(ByVal value As Object)

    '        gvCashDenom.DataSource = value

    '        Dim dtTable As Data.DataTable = TryCast(gvCashDenom.DataSource, Data.DataTable)
    '        If dtTable IsNot Nothing AndAlso dtTable.Columns.Count <> gvCashDenom.Columns.Count - 1 Then

    '            For i = 1 To gvCashDenom.Columns.Count - 1
    '                gvCashDenom.Columns.RemoveAt(1)
    '            Next

    '            For Each col As Data.DataColumn In dtTable.Columns

    '                Dim bf As New BoundField
    '                bf.DataField = col.ColumnName
    '                bf.HeaderText = col.Caption

    '                If bf.DataField = "employeeunkid" OrElse bf.DataField = "exchagerateunkid" OrElse bf.DataField = "currency_sign" OrElse bf.DataField = "countryunkid" Then
    '                    bf.Visible = False
    '                Else
    '                    bf.Visible = True
    '                End If

    '                If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Int32") Then
    '                    'Nilay (28-Aug-2015) -- Start
    '                    bf.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
    '                    'Nilay (28-Aug-2015) -- End
    '                    bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right
    '                Else
    '                    'Nilay (28-Aug-2015) -- Start
    '                    bf.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
    '                    'Nilay (28-Aug-2015) -- End
    '                    bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left
    '                End If

    '                If bf.DataField = "total" Then

    '                End If
    '                gvCashDenom.Columns.Add(bf)

    '            Next

    '        End If

    '    End Set
    'End Property

#End Region

#Region "Private Methods"

    Private Sub CreateDenomTable()

        Dim objDenome As New clsDenomination
        Dim dsList As New DataSet
        mdtTran = New DataTable("DenomTbl")
        Try
            Dim dtCol As DataColumn

            dtCol = New DataColumn("employeename")
            dtCol.DataType = System.Type.GetType("System.String")
            dtCol.Caption = "Employee"
            mdtTran.Columns.Add(dtCol)

            dtCol = New DataColumn("amount")
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.Caption = "Amount"
            dtCol.DefaultValue = 0
            mdtTran.Columns.Add(dtCol)

            dtCol = New DataColumn("total")
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.Caption = "Total"
            dtCol.DefaultValue = 0
            mdtTran.Columns.Add(dtCol)

            dsList = objDenome.GetList("List", True, True, CInt(Me.ViewState("ctrmintCountryunkid")))
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    dtCol = New DataColumn("Column" & dRow.Item("denomunkid").ToString)
                    dtCol.Caption = Format(dRow.Item("denomination"), mstrDenomeFormat) & "'s"
                    dtCol.DataType = System.Type.GetType("System.Decimal")
                    dtCol.DefaultValue = 0
                    dtCol.ExtendedProperties.Add("dvalue", CInt(dRow.Item("denomination")))
                    mdtTran.Columns.Add(dtCol)
                Next
            Else
                Throw New Exception("Denomination's are not defined for this currency. Please define denomination to perform this operation.")
                Exit Try
            End If

            dtCol = New DataColumn("employeeunkid")
            dtCol.DataType = System.Type.GetType("System.Int32")
            dtCol.Caption = "employeeunkid"
            mdtTran.Columns.Add(dtCol)

            dtCol = New DataColumn("exchagerateunkid")
            dtCol.DataType = System.Type.GetType("System.Int32")
            dtCol.Caption = "exchagerateunkid"
            mdtTran.Columns.Add(dtCol)

            dtCol = New DataColumn("currency_sign")
            dtCol.DataType = System.Type.GetType("System.String")
            dtCol.Caption = "currency_sign"
            mdtTran.Columns.Add(dtCol)

            dtCol = New DataColumn("countryunkid")
            dtCol.DataType = System.Type.GetType("System.Int32")
            dtCol.Caption = "countryunkid"
            mdtTran.Columns.Add(dtCol)

            objDenome = Nothing

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateDenomTable:- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub FillDenomDataTable(ByVal dtTable As DataTable)

        Dim objExRate As New clsExchangeRate
        Dim decBalAmt As Decimal
        Dim dtRow As DataRow
        Try
            objExRate._ExchangeRateunkid = mintExchangeTranunkid

            gvCashDenom.Columns.Clear()

            Call CreateDenomTable()
            Call GenerateCustomGrid()

            mdecBaseExRate = objExRate._Exchange_Rate1
            mdecPaidExRate = objExRate._Exchange_Rate2

            For Each dRow As DataRow In dtTable.Rows

                dtRow = mdtTran.NewRow

                dtRow.Item("employeename") = dRow.Item("employeename")

                decBalAmt = CDec(dRow.Item("balanceamount")) * CDec(dRow.Item("Percentage")) / 100

                If mdecBaseExRate <> 0 Then
                    dtRow.Item("amount") = Format(decBalAmt * mdecPaidExRate / mdecBaseExRate, Session("fmtCurrency"))
                Else
                    dtRow.Item("amount") = Format(decBalAmt * mdecPaidExRate, Session("fmtCurrency"))
                End If

                dtRow.Item("total") = 0
                dtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                dtRow.Item("exchagerateunkid") = objExRate._ExchangeRateunkid
                dtRow.Item("countryunkid") = CInt(Me.ViewState("ctrmintCountryunkid"))
                dtRow.Item("currency_sign") = objExRate._Currency_Sign

                mdtTran.Rows.Add(dtRow)
            Next

            gvCashDenom.AutoGenerateColumns = False
            gvCashDenom.DataSource = mdtTran
            gvCashDenom.DataBind()
            HttpContext.Current.Session("PayCashDenomination") = mdtTran
        Catch ex As Exception
            'Throw New Exception("FillDenomDataTable:- " & ex.Message)
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub GenerateCustomGrid()

        Dim objDenome As New clsDenomination
        Dim dsList As New DataSet
        Try
            Dim boundField As BoundField
            Dim templateField As TemplateField

            boundField = New BoundField()
            boundField.DataField = "employeename"
            boundField.HeaderText = "Employee"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Left
            boundField.HeaderStyle.Width = Unit.Pixel(300)
            boundField.ItemStyle.Width = Unit.Pixel(300)
            boundField.FooterText = "gvColhEmployeeName"
            gvCashDenom.Columns.Add(boundField)

            boundField = New BoundField()
            boundField.DataField = "amount"
            boundField.HeaderText = "Amount"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.HeaderStyle.Width = Unit.Pixel(150)
            boundField.ItemStyle.Width = Unit.Pixel(150)
            boundField.FooterText = "gvColhAmount"
            gvCashDenom.Columns.Add(boundField)

            boundField = New BoundField()
            boundField.DataField = "total"
            boundField.HeaderText = "Total"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.HeaderStyle.Width = Unit.Pixel(150)
            boundField.ItemStyle.Width = Unit.Pixel(150)
            boundField.FooterText = "gvColhTotal"
            gvCashDenom.Columns.Add(boundField)

            dsList = objDenome.GetList("List", True, True, CInt(Me.ViewState("ctrmintCountryunkid")))
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    templateField = New TemplateField()
                    templateField.HeaderText = Format(dRow.Item("denomination"), mstrDenomeFormat) & "'s"
                    templateField.FooterText = "objgvColh" & Format(dRow.Item("denomination"), mstrDenomeFormat) & "s"
                    templateField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
                    templateField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If gvCashDenom.Columns.Contains(templateField) = True Then Continue For
                    gvCashDenom.Columns.Add(templateField)
                Next
            End If

            boundField = New BoundField()
            boundField.DataField = "employeeunkid"
            boundField.HeaderText = "employeeunkid"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.FooterText = "objgvColhemployeeunkid"
            boundField.Visible = False
            gvCashDenom.Columns.Add(boundField)

            boundField = New BoundField()
            boundField.DataField = "exchagerateunkid"
            boundField.HeaderText = "exchagerateunkid"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.FooterText = "objgvColhexchagerateunkid"
            boundField.Visible = False
            gvCashDenom.Columns.Add(boundField)

            boundField = New BoundField()
            boundField.DataField = "currency_sign"
            boundField.HeaderText = "currency_sign"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.FooterText = "objgvColhcurrency_sign"
            boundField.Visible = False
            gvCashDenom.Columns.Add(boundField)

            boundField = New BoundField()
            boundField.DataField = "countryunkid"
            boundField.HeaderText = "countryunkid"
            boundField.ReadOnly = True
            boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Right
            boundField.FooterText = "objgvColhcountryunkid"
            boundField.Visible = False
            gvCashDenom.Columns.Add(boundField)

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("GenerateCustomGrid:- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Show(ByVal dtTable As DataTable)
        Try
            Call FillDenomDataTable(dtTable)
            popupCashDenomination.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CashDenomination : Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
            Try
            popupCashDenomination.Hide()
            Catch ex As Exception
                Throw New Exception("Hide :- " & ex.Message)
            End Try
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CashDenomination : Hide :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

#End Region

#Region "GridView Events"

    Protected Sub gvCashDenom_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCashDenom.RowDataBound

        Dim objDenome As New clsDenomination
        Dim dsList As New DataSet
        Dim strDenom As String = String.Empty
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                dsList = objDenome.GetList("List", True, True, CInt(Me.ViewState("ctrmintCountryunkid")))
                If dsList.Tables("List").Rows.Count > 0 Then
                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        Dim txtBox As New TextBox
                        strDenom = Format(dsList.Tables(0).Rows(i)("denomination"), mstrDenomeFormat)
                        If strDenom.Contains(".") Then
                            strDenom &= strDenom.Replace(".", "point")
                        End If
                        txtBox.ID = "txtDenom" & strDenom
                        txtBox.CssClass = "CashGridCell"
                        If mdtCashDataSource IsNot Nothing Then
                            If mdtCashDataSource.Rows.Count >= e.Row.RowIndex Then
                                txtBox.Text = mdtCashDataSource.Rows(e.Row.RowIndex)("Column" & dsList.Tables(0).Rows(i)("denomunkid").ToString)
                            End If
                        Else
                            txtBox.Text = 0
                        End If

                        txtBox.Style.Add("text-align", "right")
                        txtBox.Style.Add("border", "none")
                        txtBox.Style.Add("width", "60px")
                        txtBox.Attributes.Add("denomevalue", Format(dsList.Tables(0).Rows(i)("denomination"), mstrDenomeFormat))
                        txtBox.Attributes.Add("ColumnName", "Column" & dsList.Tables(0).Rows(i)("denomunkid").ToString)
                        e.Row.Cells(i + 3).Controls.Add(txtBox)
                    Next
                End If

                e.Row.Cells(1).Attributes.Add("NetAmount", e.Row.Cells(1).Text.Replace(",", ""))
                e.Row.Cells(2).Attributes.Add("NetAmount", e.Row.Cells(2).Text.Replace(",", ""))
                e.Row.Cells(1).Text = Format(CDec(e.Row.Cells(1).Text), Session("fmtCurrency"))
                e.Row.Cells(2).Text = Format(CDec(e.Row.Cells(2).Text), Session("fmtCurrency"))

            End If

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvCashDenom_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim blnFlag As Boolean = False
        Try
            mdtCashDataSource = CType(HttpContext.Current.Session("PayCashDenomination"), DataTable)
            For Each dtRow As DataRow In mdtCashDataSource.Rows
                If Format(CDec(dtRow.Item("amount")), Session("fmtCurrency")) <> Format(CDec(dtRow.Item("total")), Session("fmtCurrency")) Then
                    lblMessage.Text = "Sorry, Amount and Total Cash Denomination Amount must be same for All employees."
                    gvCashDenom.DataSource = mdtCashDataSource
                    gvCashDenom.DataBind()
                    popupCashDenomination.Show()
                    Exit Sub
                End If
            Next
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CashDenomination: btnSave_Click:-" & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
        RaiseEvent buttonSave_Click(sender, e)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        RaiseEvent buttonCancel_Click(sender, e)
    End Sub
#End Region

End Class
