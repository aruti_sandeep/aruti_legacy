﻿Imports System.Data
Imports Aruti.Data

Partial Class Controls_AnalysisBy
    Inherits System.Web.UI.UserControl

    Public Event buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmViewAnalysis" 'Sohail (23 May 2017)

    Private mstrReportBy_Ids As String = String.Empty
    Private mstrReportBy_Name As String = String.Empty

    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrHr_EmployeeTable_Alias As String = "hremployee_master"
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    'Sohail (23 May 2017) -- Start
    'Enhancement - 67.1 - Link budget with Payroll.
    Private mdtEffectiveDate As Date = Nothing
    Private mstrAnalysis_CodeField As String = String.Empty
    'Sohail (23 May 2017) -- End

    Private dtTable As DataTable
#End Region

#Region " Properties "
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return lblMessage.Text
        End Get
        Set(ByVal value As String)
            lblMessage.Text = value
        End Set
    End Property

    Public Property _ReportBy_Ids() As String
        Get
            Return mstrReportBy_Ids
        End Get
        Set(ByVal value As String)
            mstrReportBy_Ids = value
        End Set
    End Property

    Public Property _ReportBy_Name() As String
        Get
            Return mstrReportBy_Name
        End Get
        Set(ByVal value As String)
            mstrReportBy_Name = value
        End Set
    End Property

    Public ReadOnly Property _ViewIndex() As Integer
        Get
            Return mintViewIndex
        End Get
    End Property

    Public ReadOnly Property _Analysis_Fields() As String
        Get
            Return mstrAnalysis_Fields
        End Get
    End Property

    Public ReadOnly Property _Analysis_Join() As String
        Get
            Return mstrAnalysis_Join
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy() As String
        Get
            Return mstrAnalysis_OrderBy
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy_GName() As String
        Get
            Return mstrAnalysis_OrderBy_GroupName
        End Get
    End Property

    Public ReadOnly Property _Report_GroupName() As String
        Get
            Return mstrReport_GroupName
        End Get
    End Property

    Public ReadOnly Property _Analysis_TableName() As String
        Get
            Return mstrAnalysis_TableName
        End Get
    End Property

    Public WriteOnly Property _Hr_EmployeeTable_Alias() As String
        Set(ByVal value As String)
            mstrHr_EmployeeTable_Alias = value
        End Set
    End Property

    'Sohail (23 May 2017) -- Start
    'Enhancement - 67.1 - Link budget with Payroll.
    Public WriteOnly Property _EffectiveDate() As Date
        Set(ByVal value As Date)
            mdtEffectiveDate = value
        End Set
    End Property

    Public ReadOnly Property _Analysis_CodeField() As String
        Get
            Return mstrAnalysis_CodeField
        End Get
    End Property
    'Sohail (23 May 2017) -- End

#End Region

#Region " Methods & Functions "
    Private Sub CreateTable()
        Try
            dtTable = New DataTable("List")
            dtTable.Columns.Add("IsCheck", Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""

            Call TabContainer1_ActiveTabChanged(TabContainer1, New System.EventArgs)
            popupAdvanceFilter.Hide()

            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                dtTable.Rows.Add(r)
            End If

            gvDetails.DataSource = dtTable
            gvDetails.DataBind()

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            'Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateTable :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Show()
        Try
        Call Reset()
        'Sohail (23 May 2017) -- Start
        'Enhancement - 67.1 - Link budget with Payroll.
        If mdtEffectiveDate = Nothing Then mdtEffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
        'Sohail (23 May 2017) -- End
        popupAdvanceFilter.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(ex)
            'Throw New Exception("Show :- " & ex.Message)
            'Gajanan (26-Aug-2020) -- End       
        End Try
    End Sub

    Public Sub Hide()
        Try
        popupAdvanceFilter.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide:- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub Reset()
        Try
            Call CreateTable()
            mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            'mstrHr_EmployeeTable_Alias = "hremployee_master"
            mstrReport_GroupName = ""
            mstrAnalysis_TableName = " "
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Reset :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    'Sohail (23 May 2017) -- Start
    'Enhancement - 67.1 - Link budget with Payroll.
    Public Shared Function GetAnalysisByDetails(ByVal strViewAnalysisModuleName As String _
                                                , ByVal intViewIndex As Integer _
                                                , ByVal strReportBy_Ids As String _
                                                , ByVal dtEffectiveDate As Date _
                                                , Optional ByVal strHr_EmployeeTable_Alias As String = "" _
                                                , Optional ByRef stroutAnalysis_Fields As String = "" _
                                                , Optional ByRef stroutAnalysis_Join As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy_GroupName As String = "" _
                                                , Optional ByRef stroutReport_GroupName As String = "" _
                                                , Optional ByRef stroutAnalysis_TableName As String = "" _
                                                , Optional ByRef stroutAnalysis_CodeField As String = "" _
                                                ) As Boolean
        'Sohail (01 Mar 2017) - [stroutAnalysis_CodeField]

        Try



            Dim strAllocationQry, strJobQry, strCCQry, strSalQry As String
            strAllocationQry = "" : strJobQry = "" : strCCQry = "" : strSalQry = ""

            strAllocationQry = "SELECT " & _
                               "     stationunkid " & _
                               "    ,deptgroupunkid " & _
                               "    ,departmentunkid " & _
                               "    ,sectiongroupunkid " & _
                               "    ,sectionunkid " & _
                               "    ,unitgroupunkid " & _
                               "    ,unitunkid " & _
                               "    ,teamunkid " & _
                               "    ,classgroupunkid " & _
                               "    ,classunkid " & _
                               "    ,employeeunkid " & _
                               "FROM " & _
                               "( " & _
                               "        SELECT " & _
                               "             stationunkid " & _
                               "            ,deptgroupunkid " & _
                               "            ,departmentunkid " & _
                               "            ,sectiongroupunkid " & _
                               "            ,sectionunkid " & _
                               "            ,unitgroupunkid " & _
                               "            ,unitunkid " & _
                               "            ,teamunkid " & _
                               "            ,classgroupunkid " & _
                               "            ,classunkid " & _
                               "            ,employeeunkid " & _
                               "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                               "        FROM hremployee_transfer_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                               ") AS A WHERE A.rno = 1 "

            strJobQry = "SELECT " & _
                        "    jobgroupunkid " & _
                        "   ,jobunkid " & _
                        "   ,employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "   FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                        ") AS A WHERE A.rno = 1 "

            strCCQry = "SELECT " & _
                       "     costcenterunkid " & _
                       "    ,employeeunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         cctranheadvalueid AS costcenterunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_cctranhead_tran WHERE isvoid = 0 AND istransactionhead = 0 " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                       ") AS A WHERE A.rno = 1 "

            strSalQry = "SELECT " & _
                        "    gradegroupunkid " & _
                        "   ,gradeunkid " & _
                        "   ,gradelevelunkid " & _
                        "   ,employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        gradegroupunkid " & _
                        "       ,gradeunkid " & _
                        "       ,gradelevelunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "   FROM prsalaryincrement_tran WHERE isvoid = 0 AND isapproved = 1 " & _
                        "       AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtEffectiveDate) & "' " & _
                        ") AS A WHERE A.rno = 1 "


            If strHr_EmployeeTable_Alias.Trim = "" Then strHr_EmployeeTable_Alias = "hremployee_master"

            Select Case intViewIndex

                Case enAnalysisReport.Branch

                    stroutAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrstation_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                   "(" & vbCrLf & _
                                       strAllocationQry & vbCrLf & _
                                   ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                   " JOIN hrstation_master ON hrstation_master.stationunkid = VB_TRF.stationunkid AND hrstation_master.stationunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrstation_master.stationunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrstation_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 2, "Branch :")
                    stroutReport_GroupName = Language._Object.getCaption("radBranch", "Branch") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrstation_master "

                Case enAnalysisReport.Department
                    stroutAnalysis_Fields = ", hrdepartment_master.departmentunkid AS Id, hrdepartment_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrdepartment_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = VB_TRF.departmentunkid AND hrdepartment_master.departmentunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrdepartment_master.departmentunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrdepartment_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 3, "Department :")
                    stroutReport_GroupName = Language._Object.getCaption("radDepartment", "Department") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrdepartment_master "

                Case enAnalysisReport.Job
                    stroutAnalysis_Fields = ",  hrjob_master.jobunkid AS Id, hrjob_master.job_name AS GName "

                    stroutAnalysis_CodeField = ", hrjob_master.job_code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strJobQry & vbCrLf & _
                                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrjob_master ON hrjob_master.jobunkid = VB_RECAT.jobunkid AND hrjob_master.jobunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrjob_master.jobunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrjob_master.job_name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 4, "Job :")
                    stroutReport_GroupName = Language._Object.getCaption("radJob", "Job") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrjob_master "

                Case enAnalysisReport.Section
                    stroutAnalysis_Fields = ",  hrsection_master.sectionunkid AS Id, hrsection_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrsection_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrsection_master ON hrsection_master.sectionunkid = VB_TRF.sectionunkid AND hrsection_master.sectionunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrsection_master.sectionunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrsection_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 5, "Section :")
                    stroutReport_GroupName = Language._Object.getCaption("radSection", "Section") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrsection_master "

                Case enAnalysisReport.Unit
                    stroutAnalysis_Fields = ",  hrunit_master.unitunkid AS Id, hrunit_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrunit_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrunit_master ON hrunit_master.unitunkid = VB_TRF.unitunkid AND hrunit_master.unitunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrunit_master.unitunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrunit_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 6, "Unit :")
                    stroutReport_GroupName = Language._Object.getCaption("radUnit", "Unit") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrunit_master "

                Case enAnalysisReport.CostCenter
                    stroutAnalysis_Fields = ", prcostcenter_master.costcenterunkid AS Id, prcostcenter_master.costcentername AS GName "

                    stroutAnalysis_CodeField = ", prcostcenter_master.costcentercode AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strCCQry & vbCrLf & _
                                        ") AS VB_CCR ON VB_CCR.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = VB_CCR.costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " prcostcenter_master.costcenterunkid "
                    stroutAnalysis_OrderBy_GroupName = " prcostcenter_master.costcentername "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 7, "Cost Center :")
                    stroutReport_GroupName = Language._Object.getCaption("radCostCenter", "Cost Center") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " prcostcenter_master "

                Case enAnalysisReport.SectionGroup
                    stroutAnalysis_Fields = ", hrsectiongroup_master.sectiongroupunkid AS Id, hrsectiongroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrsectiongroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = VB_TRF.sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrsectiongroup_master.sectiongroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrsectiongroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 8, "Section Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radSectionGrp", "Section Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrsectiongroup_master "

                Case enAnalysisReport.UnitGroup
                    stroutAnalysis_Fields = ", hrunitgroup_master.unitgroupunkid AS Id, hrunitgroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrunitgroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = VB_TRF.unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrunitgroup_master.unitgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrunitgroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 9, "Unit Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radUnitGrp", "Unit Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrunitgroup_master "

                Case enAnalysisReport.Team
                    stroutAnalysis_Fields = ", hrteam_master.teamunkid AS Id, hrteam_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrteam_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrteam_master ON hrteam_master.teamunkid = VB_TRF.teamunkid AND hrteam_master.teamunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrteam_master.teamunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrteam_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 10, "Team :")
                    stroutReport_GroupName = Language._Object.getCaption("radTeam", "Team") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrteam_master "

                Case enAnalysisReport.DepartmentGroup
                    stroutAnalysis_Fields = ", hrdepartment_group_master.deptgroupunkid AS Id, hrdepartment_group_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrdepartment_group_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = VB_TRF.deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrdepartment_group_master.deptgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrdepartment_group_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 11, "Department Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radDepartmentGrp", "Department Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrdepartment_group_master "

                Case enAnalysisReport.JobGroup
                    stroutAnalysis_Fields = ", hrjobgroup_master.jobgroupunkid AS Id, hrjobgroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrjobgroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strJobQry & vbCrLf & _
                                        ") AS VB_RECAT ON VB_RECAT.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = VB_RECAT.jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrjobgroup_master.jobgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrjobgroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 12, "Job Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radJobGrp", "Job Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrjobgroup_master "

                Case enAnalysisReport.ClassGroup
                    stroutAnalysis_Fields = ", hrclassgroup_master.classgroupunkid AS Id, hrclassgroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrclassgroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = VB_TRF.classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrclassgroup_master.classgroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrclassgroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 13, "Class Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radClassGrp", "Class Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrclassgroup_master "

                Case enAnalysisReport.Classs
                    stroutAnalysis_Fields = ", hrclasses_master.classesunkid AS Id, hrclasses_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrclasses_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strAllocationQry & vbCrLf & _
                                        ") AS VB_TRF ON VB_TRF.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrclasses_master ON hrclasses_master.classesunkid = VB_TRF.classunkid AND hrclasses_master.classesunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrclasses_master.classesunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrclasses_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 14, "Class :")
                    stroutReport_GroupName = Language._Object.getCaption("radClass", "Class") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrclasses_master "

                Case enAnalysisReport.GradeGroup
                    stroutAnalysis_Fields = ", hrgradegroup_master.gradegroupunkid AS Id, hrgradegroup_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrgradegroup_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                    "(" & vbCrLf & _
                                        strSalQry & vbCrLf & _
                                    ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                    " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = VB_SALI.gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgradegroup_master.gradegroupunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgradegroup_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 15, "Grade Group :")
                    stroutReport_GroupName = Language._Object.getCaption("radGradeGrp", "Grade Group") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrgradegroup_master "

                Case enAnalysisReport.Grade
                    stroutAnalysis_Fields = ", hrgrade_master.gradeunkid AS Id, hrgrade_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrgrade_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strSalQry & vbCrLf & _
                                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = VB_SALI.gradeunkid AND hrgrade_master.gradeunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgrade_master.gradeunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgrade_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 16, "Grade :")
                    stroutReport_GroupName = Language._Object.getCaption("radGrade", "Grade") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrgrade_master "

                Case enAnalysisReport.GradeLevel
                    stroutAnalysis_Fields = ", hrgradelevel_master.gradelevelunkid AS Id, hrgradelevel_master.name AS GName "

                    stroutAnalysis_CodeField = ", hrgradelevel_master.code AS GCode "

                    stroutAnalysis_Join = "JOIN " & _
                                        "(" & vbCrLf & _
                                            strSalQry & vbCrLf & _
                                        ") AS VB_SALI ON VB_SALI.employeeunkid = " & strHr_EmployeeTable_Alias & ".employeeunkid " & _
                                        " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = VB_SALI.gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & strReportBy_Ids & " ) "

                    stroutAnalysis_OrderBy = " hrgradelevel_master.gradelevelunkid "
                    stroutAnalysis_OrderBy_GroupName = " hrgradelevel_master.name "
                    'Hemant (19 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
                    'stroutReport_GroupName = Language.getMessage(strViewAnalysisModuleName, 17, "Grade Level :")
                    stroutReport_GroupName = Language._Object.getCaption("radGradeLevel", "Grade Level") & " :"
                    'Hemant (19 Jul 2019) -- End
                    stroutAnalysis_TableName = " hrgradelevel_master "

                Case Else
                    stroutAnalysis_Fields = ", 0 AS Id, '' AS GName "
                    stroutAnalysis_CodeField = ""
                    stroutAnalysis_Join = ""
                    stroutAnalysis_OrderBy = ""
                    stroutAnalysis_OrderBy_GroupName = ""
                    strHr_EmployeeTable_Alias = ""
                    stroutReport_GroupName = ""
                    stroutAnalysis_TableName = " "

            End Select

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Procedure name : GetAnalysisByDetails ; ModuleName : " & strViewAnalysisModuleName & ";" & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Function
    'Sohail (23 May 2017) -- End

#End Region

#Region " Page's Events "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("dtTable", dtTable)
        Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
        Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
        Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
        Me.ViewState.Add("mstrAnalysis_OrderBy_GroupName", mstrAnalysis_OrderBy_GroupName)
        Me.ViewState.Add("mstrHr_EmployeeTable_Alias", mstrHr_EmployeeTable_Alias)
        Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
        Me.ViewState.Add("mstrAnalysis_TableName", mstrAnalysis_TableName)
        'Sohail (23 May 2017) -- Start
        'Enhancement - 67.1 - Link budget with Payroll.
        Me.ViewState.Add("mstrAnalysis_CodeField", mstrAnalysis_CodeField)
        Me.ViewState.Add("mdtEffectiveDate", mdtEffectiveDate)
        'Sohail (23 May 2017) -- End
        
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                'If mstrHr_EmployeeTable_Alias.Trim.Length > 0 Then mstrHr_EmployeeTable_Alias = mstrHr_EmployeeTable_Alias & "."
                Call CreateTable()
                Call SetLanguage()

                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                Me.ViewState.Add("AllocationTagId", -1)
                'Shani [ 31 OCT 2014 ] -- END

            Else
                dtTable = ViewState("dtTable")
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy")
                mstrAnalysis_OrderBy_GroupName = ViewState("mstrAnalysis_OrderBy_GroupName")
                mstrHr_EmployeeTable_Alias = ViewState("mstrHr_EmployeeTable_Alias")
                mstrReport_GroupName = ViewState("mstrReport_GroupName")
                mstrAnalysis_TableName = ViewState("mstrAnalysis_TableName")
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                mstrAnalysis_CodeField = ViewState("mstrAnalysis_CodeField")
                mdtEffectiveDate = ViewState("mdtEffectiveDate")
                'Sohail (23 May 2017) -- End
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try
            Dim dtTab As DataTable = CType(Me.ViewState("dtTable"), DataTable)

            For i As Integer = 0 To dtTab.Rows.Count - 1
                If dtTab.Rows(i)("Name").ToString.Trim <> "None" AndAlso CBool(dtTab.Rows(i)("IsCheck")) <> cb.Checked Then
                    Dim gvRow As GridViewRow = gvDetails.Rows(i)
                    CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                    dtTab.Rows(i)("IsCheck") = cb.Checked

                End If
            Next
            dtTab.AcceptChanges()
            Me.ViewState("dtTable") = dtTab


        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("chkSelectAll_CheckedChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
            popupAdvanceFilter.Show()
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataTable = CType(Me.ViewState("dtTable"), DataTable)
            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
            dtTab.Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
            dtTab.AcceptChanges()
            Me.ViewState("dtTable") = dtTab

            Dim drcheckedRow() As DataRow = dtTab.Select("Ischeck = True")

            If drcheckedRow.Length = dtTab.Rows.Count Then
                CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("chkSelect_CheckedChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
            popupAdvanceFilter.Show()
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Call SetLanguage() 'Hemant (19 Jul 2019)
            If dtTable.Rows.Count > 0 Then
                Dim dtTemp() As DataRow = dtTable.Select("IsCheck=true")
                If dtTemp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please check atleast one report by"), Me.Page)
                    Exit Sub
                End If
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Please select any one Analysis by"), Me.Page)
                Exit Sub
            End If

            Dim lstIDs As List(Of String) = (From p In dtTable Where CBool(p.Item("IsCheck")) = True Select (p.Item("Id").ToString)).ToList
            mstrReportBy_Ids = String.Join(",", lstIDs.ToArray)

            Dim lstNAMEs As List(Of String) = (From p In dtTable Where CBool(p.Item("IsCheck")) = True Select (p.Item("Name").ToString)).ToList
            mstrReportBy_Name = String.Join(",", lstNAMEs.ToArray)


            Select Case TabContainer1.ActiveTab.HeaderText
                Case tbpnlBranch.HeaderText

                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Branch
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Branch, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END
                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrstation_master.stationunkid AS Id, hrstation_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrstation_master ON hrstation_master.stationunkid = " & mstrHr_EmployeeTable_Alias & ".stationunkid AND hrstation_master.stationunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrstation_master.stationunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrstation_master.name "
                    'mstrReport_GroupName = "Branch :"
                    'mstrAnalysis_TableName = " hrstation_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlDept.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Department
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Department, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrdepartment_master.departmentunkid AS Id, hrdepartment_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = " & mstrHr_EmployeeTable_Alias & ".departmentunkid AND hrdepartment_master.departmentunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrdepartment_master.departmentunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrdepartment_master.name "
                    'mstrReport_GroupName = "Department :"
                    'mstrAnalysis_TableName = " hrdepartment_master"
                    'Sohail (23 May 2017) -- End

                Case tbpnlDeptGroup.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.DepartmentGroup
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.DepartmentGroup, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrdepartment_group_master.deptgroupunkid AS Id, hrdepartment_group_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = " & mstrHr_EmployeeTable_Alias & ".deptgroupunkid AND hrdepartment_group_master.deptgroupunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrdepartment_group_master.deptgroupunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrdepartment_group_master.name "
                    'mstrReport_GroupName = "Department Group :"
                    'mstrAnalysis_TableName = " hrdepartment_group_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlSection.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Section
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Section, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ",  hrsection_master.sectionunkid AS Id, hrsection_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrsection_master ON hrsection_master.sectionunkid = " & mstrHr_EmployeeTable_Alias & ".sectionunkid AND hrsection_master.sectionunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrsection_master.sectionunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrsection_master.name "
                    'mstrReport_GroupName = "Section :"
                    'mstrAnalysis_TableName = " hrsection_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlUnit.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Unit
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Unit, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ",  hrunit_master.unitunkid AS Id, hrunit_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrunit_master ON hrunit_master.unitunkid = " & mstrHr_EmployeeTable_Alias & ".unitunkid AND hrunit_master.unitunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrunit_master.unitunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrunit_master.name "
                    'mstrReport_GroupName = "Unit :"
                    'mstrAnalysis_TableName = " hrunit_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlJob.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Job
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Job, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ",  hrjob_master.jobunkid AS Id, hrjob_master.job_name AS GName "
                    'mstrAnalysis_Join = " JOIN hrjob_master ON hrjob_master.jobunkid = " & mstrHr_EmployeeTable_Alias & ".jobunkid AND hrjob_master.jobunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrjob_master.jobunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrjob_master.job_name "
                    'mstrReport_GroupName = "Job :"
                    'mstrAnalysis_TableName = " hrjob_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlSectionGroup.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.SectionGroup
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.SectionGroup, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrsectiongroup_master.sectiongroupunkid AS Id, hrsectiongroup_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = " & mstrHr_EmployeeTable_Alias & ".sectiongroupunkid AND hrsectiongroup_master.sectiongroupunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrsectiongroup_master.sectiongroupunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrsectiongroup_master.name "
                    'mstrReport_GroupName = "Section Group :"
                    'mstrAnalysis_TableName = " hrsectiongroup_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlUnitGroup.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.UnitGroup
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.UnitGroup, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrunitgroup_master.unitgroupunkid AS Id, hrunitgroup_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = " & mstrHr_EmployeeTable_Alias & ".unitgroupunkid AND hrunitgroup_master.unitgroupunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrunitgroup_master.unitgroupunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrunitgroup_master.name "
                    'mstrReport_GroupName = "Unit Group :"
                    'mstrAnalysis_TableName = " hrunitgroup_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlTeam.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Team
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Team, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrteam_master.teamunkid AS Id, hrteam_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrteam_master ON hrteam_master.teamunkid = " & mstrHr_EmployeeTable_Alias & ".teamunkid AND hrteam_master.teamunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrteam_master.teamunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrteam_master.name "
                    'mstrReport_GroupName = "Team :"
                    'mstrAnalysis_TableName = " hrteam_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlJobGroup.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.JobGroup
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.JobGroup, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrjobgroup_master.jobgroupunkid AS Id, hrjobgroup_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = " & mstrHr_EmployeeTable_Alias & ".jobgroupunkid AND hrjobgroup_master.jobgroupunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrjobgroup_master.jobgroupunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrjobgroup_master.name "
                    'mstrReport_GroupName = "Job Group :"
                    'mstrAnalysis_TableName = " hrjobgroup_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlClassGroup.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.ClassGroup
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.ClassGroup, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrclassgroup_master.classgroupunkid AS Id, hrclassgroup_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = " & mstrHr_EmployeeTable_Alias & ".classgroupunkid AND hrclassgroup_master.classgroupunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrclassgroup_master.classgroupunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrclassgroup_master.name "
                    'mstrReport_GroupName = "Class Group :"
                    'mstrAnalysis_TableName = " hrclassgroup_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlClass.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Classs
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Classs, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrclasses_master.classesunkid AS Id, hrclasses_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrclasses_master ON hrclasses_master.classesunkid = " & mstrHr_EmployeeTable_Alias & ".classunkid AND hrclasses_master.classesunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrclasses_master.classesunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrclasses_master.name "
                    'mstrReport_GroupName = "Class :"
                    'mstrAnalysis_TableName = " hrclasses_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlGradeGroup.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.GradeGroup
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.GradeGroup, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrgradegroup_master.gradegroupunkid AS Id, hrgradegroup_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = " & mstrHr_EmployeeTable_Alias & ".gradegroupunkid AND hrgradegroup_master.gradegroupunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrgradegroup_master.gradegroupunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrgradegroup_master.name "
                    'mstrReport_GroupName = "Grade Group :"
                    'mstrAnalysis_TableName = " hrgradegroup_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlGrade.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.Grade
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.Grade, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrgrade_master.gradeunkid AS Id, hrgrade_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrgrade_master ON hrgrade_master.gradeunkid = " & mstrHr_EmployeeTable_Alias & ".gradeunkid AND hrgrade_master.gradeunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrgrade_master.gradeunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrgrade_master.name "
                    'mstrReport_GroupName = "Grade :"
                    'mstrAnalysis_TableName = " hrgrade_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlGradeLevel.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.GradeLevel
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.GradeLevel, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", hrgradelevel_master.gradelevelunkid AS Id, hrgradelevel_master.name AS GName "
                    'mstrAnalysis_Join = " JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = " & mstrHr_EmployeeTable_Alias & ".gradelevelunkid AND hrgradelevel_master.gradelevelunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " hrgradelevel_master.gradelevelunkid "
                    'mstrAnalysis_OrderBy_GroupName = " hrgradelevel_master.name "
                    'mstrReport_GroupName = "Grade Level :"
                    'mstrAnalysis_TableName = " hrgradelevel_master "
                    'Sohail (23 May 2017) -- End

                Case tbpnlCostCenter.HeaderText
                    'Shani [ 31 OCT 2014 ] -- START
                    'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                    'mintViewIndex = enAnalysisReport.CostCenter
                    mintViewIndex = CInt(IIf(CInt(Me.ViewState("AllocationTagId")) = -1, enAnalysisReport.CostCenter, Me.ViewState("AllocationTagId")))
                    'Shani [ 31 OCT 2014 ] -- END

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    'mstrAnalysis_Fields = ", prcostcenter_master.costcenterunkid AS Id, prcostcenter_master.costcentername AS GName "
                    'mstrAnalysis_Join = " JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = " & mstrHr_EmployeeTable_Alias & ".costcenterunkid AND prcostcenter_master.costcenterunkid IN ( " & mstrReportBy_Ids & " ) "
                    'mstrAnalysis_OrderBy = " prcostcenter_master.costcenterunkid "
                    'mstrAnalysis_OrderBy_GroupName = " prcostcenter_master.costcentername "
                    'mstrReport_GroupName = "Cost Center :"
                    'mstrAnalysis_TableName = " prcostcenter_master "
                    'Sohail (23 May 2017) -- End


                Case Else
                    mintViewIndex = 0 'Sohail (23 May 2017)
                    mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
                    mstrAnalysis_Join = ""
                    mstrAnalysis_OrderBy = ""
                    mstrAnalysis_OrderBy_GroupName = ""
                    mstrHr_EmployeeTable_Alias = ""
                    mstrReport_GroupName = ""
                    mstrAnalysis_TableName = " "
            End Select

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Call GetAnalysisByDetails(mstrModuleName _
                                          , mintViewIndex _
                                          , mstrReportBy_Ids _
                                          , mdtEffectiveDate _
                                          , mstrHr_EmployeeTable_Alias _
                                          , mstrAnalysis_Fields _
                                          , mstrAnalysis_Join _
                                          , mstrAnalysis_OrderBy _
                                          , mstrAnalysis_OrderBy_GroupName _
                                          , mstrReport_GroupName _
                                          , mstrAnalysis_TableName _
                                          , mstrAnalysis_CodeField _
                                          )
            'Sohail (23 May 2017) -- End

            RaiseEvent buttonApply_Click(sender, e)
            popupAdvanceFilter.Dispose()

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnApply_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mstrAnalysis_Fields = ", 0 AS Id, '' AS GName "
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrHr_EmployeeTable_Alias = ""
            mstrReport_GroupName = ""
            mstrAnalysis_TableName = " "
            mstrAnalysis_CodeField = "" 'Sohail (23 May 2017)

            RaiseEvent buttonClose_Click(sender, e)
            popupAdvanceFilter.Dispose()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnClose_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " GridView's Events "

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(DataBinder.Eval(e.Row.DataItem, "Id")) = -111 AndAlso DataBinder.Eval(e.Row.DataItem, "Name").ToString = "None" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvDetails_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

#End Region

#Region " Tab Control's Events "

    Protected Sub TabContainer1_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabContainer1.ActiveTabChanged
        Dim intColType As Integer = 0
        Dim dsList As DataSet
        Try
            If TabContainer1.ActiveTab Is tbpnlBranch Then
                Dim objBranch As New clsStation
                dsList = objBranch.GetList("List", True)

                If Session("AccessLevelBranchFilterString").ToString.Trim.Length > 0 Then

                    'Anjan [09 February 2015] -- Start
                    'Issue : It was giving exception on payroll report for TabContainer1_ActiveTabChanged :- Syntax error: Missing operand before 'And' operator.
                    'dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    If CInt(Session("UserId")) > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelBranchFilterString").ToString.Substring(4).Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Anjan [20 January 2014] -- End


                Else
                    dtTable = dsList.Tables("List")
                End If

                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.BRANCH
                Me.ViewState("AllocationTagId") = enAnalysisReport.Branch
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END

            ElseIf TabContainer1.ActiveTab Is tbpnlDeptGroup Then
                Dim objDeptGrp As New clsDepartmentGroup
                dsList = objDeptGrp.GetList("List", True)

                If Session("AccessLevelDepartmentGroupFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.DEPARTMENT_GROUP
                Me.ViewState("AllocationTagId") = enAnalysisReport.DepartmentGroup
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlDept Then
                Dim objDept As New clsDepartment
                dsList = objDept.GetList("List", True)

                If Session("AccessLevelDepartmentFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelDepartmentFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.DEPARTMENT
                Me.ViewState("AllocationTagId") = enAnalysisReport.Department
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlSectionGroup Then
                Dim objSG As New clsSectionGroup
                dsList = objSG.GetList("List", True)

                If Session("AccessLevelSectionGroupFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.SECTION_GROUP
                Me.ViewState("AllocationTagId") = enAnalysisReport.SectionGroup
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlSection Then
                Dim objSection As New clsSections
                dsList = objSection.GetList("List", True)

                If Session("AccessLevelSectionFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelSectionFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.SECTION
                Me.ViewState("AllocationTagId") = enAnalysisReport.Section
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlUnitGroup Then
                Dim objUG As New clsUnitGroup
                dsList = objUG.GetList("List", True)

                If Session("AccessLevelUnitGroupFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.UNIT_GROUP
                Me.ViewState("AllocationTagId") = enAnalysisReport.UnitGroup
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlUnit Then
                Dim objUnit As New clsUnits
                dsList = objUnit.GetList("List", True)

                If Session("AccessLevelUnitFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelUnitFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.UNIT
                Me.ViewState("AllocationTagId") = enAnalysisReport.Unit
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlTeam Then
                Dim objTeam As New clsTeams
                dsList = objTeam.GetList("List", True)

                If Session("AccessLevelTeamFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelTeamFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.TEAM
                Me.ViewState("AllocationTagId") = enAnalysisReport.Team
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlJobGroup Then
                Dim objjobGRP As New clsJobGroup
                dsList = objjobGRP.GetList("List", True)

                If Session("AccessLevelJobGroupFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.JOB_GROUP
                Me.ViewState("AllocationTagId") = enAnalysisReport.JobGroup
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlJob Then
                Dim objJobs As New clsJobs
                dsList = objJobs.GetList("List", True)
                intColType = 1

                If Session("AccessLevelJobFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelJobFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.JOBS
                Me.ViewState("AllocationTagId") = enAnalysisReport.Job
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlClassGroup Then
                Dim objClassGrp As New clsClassGroup
                dsList = objClassGrp.GetList("List", True)

                If Session("AccessLevelClassGroupFilterString").ToString.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("List"), Session("AccessLevelClassGroupFilterString").ToString.Replace("hremployee_master.", ""), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.CLASS_GROUP
                Me.ViewState("AllocationTagId") = enAnalysisReport.ClassGroup
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlClass Then
                Dim objClass As New clsClass
                dsList = objClass.GetList("List", True)

                If Session("AccessLevelClassFilterString").ToString.Trim.Length > 0 Then
                    Dim StrFilter As String = Session("AccessLevelClassFilterString").ToString.Replace("hremployee_master.", "")
                    StrFilter = StrFilter.Replace("classunkid", "classesunkid")
                    dtTable = New DataView(dsList.Tables("List"), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("List")
                End If
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.CLASSES
                Me.ViewState("AllocationTagId") = enAnalysisReport.Classs
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlGradeGroup Then
                Dim objGradeGrp As New clsGradeGroup
                dsList = objGradeGrp.GetList("List", True)

                dtTable = dsList.Tables("List")

                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = -1
                Me.ViewState("AllocationTagId") = enAnalysisReport.GradeGroup
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlGrade Then
                Dim objGrade As New clsGrade
                dsList = objGrade.GetList("List", True)

                dtTable = dsList.Tables("List")

                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = -1
                Me.ViewState("AllocationTagId") = enAnalysisReport.Grade
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlGradeLevel Then
                Dim objGradeLvl As New clsGradeLevel
                dsList = objGradeLvl.GetList("List", True)

                dtTable = dsList.Tables("List")
                'ElseIf TabContainer1.ActiveTab Is tbpnlCostCenterGroup Then
                '    Dim objCCG As New clspayrollgroup_master
                '    mdsList = objCCG.getListForCombo(enPayrollGroupType.CostCenter, "List")

                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = -1
                Me.ViewState("AllocationTagId") = enAnalysisReport.GradeLevel
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            ElseIf TabContainer1.ActiveTab Is tbpnlCostCenter Then
                Dim objConstCenter As New clscostcenter_master
                dsList = objConstCenter.GetList("List", True)
                intColType = 2

                dtTable = dsList.Tables("List")
                objConstCenter = Nothing
                'Shani [ 31 OCT 2014 ] -- START
                'Enhansment - Assign allcation tags for same allcation Enums to all reports (assign by Sandeepsir)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'Me.ViewState("AllocationTagId") = enAllocation.COST_CENTER
                Me.ViewState("AllocationTagId") = enAnalysisReport.CostCenter
                'Sohail (23 May 2017) -- End
                'Shani [ 31 OCT 2014 ] -- END
            End If

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobgroupunkid").ColumnName = "Id"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_name").ColumnName = "Name"
            End If

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsCheck"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)


        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("TabContainer1_ActiveTabChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally

            If dtTable.Rows.Count <= 0 Then
                Dim r As DataRow = dtTable.NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                dtTable.Rows.Add(r)
            End If

            gvDetails.DataSource = dtTable
            gvDetails.DataBind()

            popupAdvanceFilter.Show()
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)


            Me.btnClose.Text = Language._Object.getCaption("btnCancel", Me.btnClose.Text).Replace("&", "")
            Me.btnApply.Text = Language._Object.getCaption("btnSet", Me.btnApply.Text).Replace("&", "")
            Me.lblTitle.Text = Language._Object.getCaption("lblAnalysisBy", Me.lblTitle.Text)
            Me.tbpnlCostCenter.HeaderText = Language._Object.getCaption("radCostCenter", Me.tbpnlCostCenter.HeaderText)
            Me.tbpnlJob.HeaderText = Language._Object.getCaption("radJob", Me.tbpnlJob.HeaderText)
            Me.tbpnlUnit.HeaderText = Language._Object.getCaption("radUnit", Me.tbpnlUnit.HeaderText)
            Me.tbpnlDept.HeaderText = Language._Object.getCaption("radDepartment", Me.tbpnlDept.HeaderText)
            Me.tbpnlSection.HeaderText = Language._Object.getCaption("radSection", Me.tbpnlSection.HeaderText)
            Me.tbpnlBranch.HeaderText = Language._Object.getCaption("radBranch", Me.tbpnlBranch.HeaderText)

            Me.tbpnlTeam.HeaderText = Language._Object.getCaption("radTeam", Me.tbpnlTeam.HeaderText)
            Me.tbpnlUnitGroup.HeaderText = Language._Object.getCaption("radUnitGrp", Me.tbpnlUnitGroup.HeaderText)
            Me.tbpnlSectionGroup.HeaderText = Language._Object.getCaption("radSectionGrp", Me.tbpnlSectionGroup.HeaderText)
            Me.tbpnlJobGroup.HeaderText = Language._Object.getCaption("radJobGrp", Me.tbpnlJobGroup.HeaderText)
            Me.tbpnlDeptGroup.HeaderText = Language._Object.getCaption("radDepartmentGrp", Me.tbpnlDeptGroup.HeaderText)
            Me.tbpnlGradeLevel.HeaderText = Language._Object.getCaption("radGradeLevel", Me.tbpnlGradeLevel.HeaderText)
            Me.tbpnlGrade.HeaderText = Language._Object.getCaption("radGtbpnle", Me.tbpnlGrade.HeaderText)
            Me.tbpnlGradeGroup.HeaderText = Language._Object.getCaption("radGradeGrp", Me.tbpnlGradeGroup.HeaderText)
            Me.tbpnlClass.HeaderText = Language._Object.getCaption("radClass", Me.tbpnlClass.HeaderText)
            Me.tbpnlClassGroup.HeaderText = Language._Object.getCaption("radClassGrp", Me.tbpnlClassGroup.HeaderText)

            gvDetails.Columns(2).HeaderText = Language._Object.getCaption(gvDetails.Columns(2).FooterText, gvDetails.Columns(2).HeaderText)

            Language.getMessage(mstrModuleName, 1, "Please check atleast one report by")
            Language.getMessage(mstrModuleName, 18, "Please select any Analysis by")


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)

            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(Ex, Me.Page)
            CommonCodes.LogErrorOnly(Ex)
            'Gajanan (26-Aug-2020) -- End

            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
End Class
