﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ExportReport.ascx.vb" Inherits="Controls_ExportReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popupExportReport" runat="server" BackgroundCssClass="ModalPopupBG2"
    CancelControlID="btnCloseReportExport" PopupControlID="Panel1" TargetControlID="hdnfieldDelReason">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none; width: 450px;
    z-index: 100002!important;" DefaultButton="btnSave">
    <div class="panel-primary" style="margin-bottom: 0px;">
        <div class="panel-heading">
            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="Div1" class="panel-default">
                <div id="Div2" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <h1>File Exported Successfully.</h1>
                                <br />
                                <h4>Click 'Save' button to Save File</h4>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                      <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCloseReportExport" runat="server" Text="Close" CssClass="btnDefault"/>
                       <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
