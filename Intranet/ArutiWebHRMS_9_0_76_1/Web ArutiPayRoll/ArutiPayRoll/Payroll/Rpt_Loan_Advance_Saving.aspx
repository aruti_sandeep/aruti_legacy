﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Loan_Advance_Saving.aspx.vb" Inherits="Reports_Rpt_Loan_Advance_Saving"
    Title="Loan Advance Saving Report" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 60%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan Advance Saving"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                            Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position:relative">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%; text-align: left">
                                                <asp:Label ID="lblCurrency" Style="margin-left: 10px" runat="server" Text="Currency"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboCurrency" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboBranch" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%; text-align: left">
                                                <asp:Label ID="lblStatus" runat="server" Style="margin-left: 10px" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBankName" runat="server" Text="Emp. Bank Name"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboBankName" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%; text-align: left">
                                                <asp:Label ID="LblBankBranch" runat="server" Style="margin-left: 10px" Text="Bank Branch"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboBankBranch" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr></tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%; text-align:left" colspan="3">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <asp:Panel ID="pnlInterestInfo" runat="server">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%; text-align: left" colspan="3">
                                        <asp:CheckBox ID="chkShowInterestInfo" runat="server" Text="Show Interest Information">
                                        </asp:CheckBox>
                                            </td>
                                        </tr>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlLoanReceipt" runat="server">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%; text-align: left" colspan="3">
                                        <asp:CheckBox ID="chkShowLoanReceipt" runat="server" Text="Show Receipt/Written Off">
                                        </asp:CheckBox>
                                            </td>
                                        </tr>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlSaving" runat="server">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%; text-align: left" colspan="3">
                                        <asp:CheckBox ID="chkIgnoreZeroSavingDeduction" runat="server" Text="Ignore Zero Saving Deduction">
                                        </asp:CheckBox>
                                            </td>
                                        </tr>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlShowOnHold" runat="server">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%; text-align: left" colspan="3">
                                                    <asp:CheckBox ID="chkShowOnHold" runat="server" Text="Show On Hold"></asp:CheckBox>
                                                </td>
                                            </tr>
                                                </asp:Panel>
                                        <asp:Panel ID="pnlShowPrincipleBFCF" runat="server">
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                </td>
                                                <td style="width: 80%; text-align: left" colspan="3">
                                                    <asp:CheckBox ID="chkShowPrincipleBFCF" runat="server" Text="Show Principle BF/CF"></asp:CheckBox>
                                                </td>
                                            </tr>
                                        </asp:Panel>
                                    </table>
                                    <asp:Panel ID="pnlScheme" runat="server">
                                        <table style="width: 100%; margin-top: 10px">
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblSearchScheme" runat="server" Text="Search Scheme"></asp:Label>
                                                </td>
                                                <td style="width: 80%">
                                                    <asp:TextBox ID="txtSearchScheme" runat="server" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                                <td style="width: 100%; vertical-align: top" colspan="2">
                                                    <Div ID="divScheme" runat="server" ScrollBars="Auto">
                                                        <asp:GridView ID="gvScheme" runat="server" Style="margin: auto;" AutoGenerateColumns="False"
                                                            AllowPaging="False" Width="100%" CssClass="gridview" HeaderStyle-Font-Bold="false"
                                                            HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem">
                                                        <Columns>
                                                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="headerstyle" ItemStyle-CssClass="itemstyle">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                        OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true" Visible="false" />
                                                            <asp:BoundField DataField="code" HeaderText="Scheme Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                    FooterText="colhCode" HeaderStyle-Width="30%" ItemStyle-Width="30%" />
                                                            <asp:BoundField DataField="name" HeaderText="Scheme Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                    FooterText="colhName" HeaderStyle-Width="60%" ItemStyle-Width="60%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    </Div>
                                            </td>
                                        </tr>
                                    </table>
                                    </asp:Panel>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
