﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Payroll_Total_Variance.aspx.vb" Inherits="Reports_Rpt_Payroll_Total_Variance"
    Title="Payroll Total Variance" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Total Variance"></asp:Label>
                        </div>
                        <div class="panel-body" style="text-align:left;">
                            <div id="FilterCriteria" class="panel-default" style="width:48%;float:left;">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblPeriodFrom" runat="server" Text="Period From"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboPeriodFrom" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 5%; text-align: left">
                                                <asp:Label ID="lblPeriodTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboPeriodTo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblTrnHeadType" runat="server" Text="Tran.Head Type"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 35%">
                                                <asp:CheckBox ID="chkIgnoreZeroVariance" runat="server" Text="Ignore Zero Variance" Checked="True" Visible="false"  />
                                            </td>
                                            <td style="width: 5%; text-align: left"></td>
                                            <td style="width: 35%">
                                        </tr>
                                        <%--Hemant (22 Jan 2019) -- Start--%>
                                        <%--Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.--%>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkShowVariancePercentageColumns" runat="server" Text="Show Variance% Columns" />
                                            </td>
                                            <%--<td style="width: 5%; text-align: left"></td>
                                            <td style="width: 35%">--%>
                                        </tr>
                                        <%--Hemant (22 Jan 2019) -- End--%>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                            
                            <asp:Panel ID="pnlInfoheads" runat="server" class="panel-default" style="width:48%;float:left;">
                                <div id="infoheadsTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblInfoHeadHeader" runat="server" Text="Informational Heads"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkSave" runat="server" Text="Save Settings" CssClass="lnkhover" Visible="false"></asp:LinkButton>
                                    </div>
                                </div>
                                
                                <div id="infoheadsBody" class="panel-body-default">
                                    <div style="margin: 3px 3px;">
                                        <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true" placeholder="Search Heads"></asp:TextBox>
                                    </div>
                                    <div id="scrollable-container2" style="max-height: 250px; overflow: auto" onscroll="$(scroll2.Y).val(this.scrollTop);">
                                        <asp:GridView ID="dgHeads" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            DataKeyNames="tranheadunkid">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                            OnCheckedChanged="chkHeadSelect_OnCheckedChanged" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhHeadCode" />
                                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhHeadName" />
                                                <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                <%--<asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                            Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                        <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                            Text="&#x25BC;" OnClick="ChangeTranHeadLocation" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

