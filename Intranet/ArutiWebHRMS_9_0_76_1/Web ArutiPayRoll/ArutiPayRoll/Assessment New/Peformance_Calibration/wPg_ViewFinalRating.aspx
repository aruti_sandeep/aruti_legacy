﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ViewFinalRating.aspx.vb" Inherits="wPg_ViewFinalRating" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="View Final Rating"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right; width: 20%">
                                        <asp:CheckBox ID="chkShowPreviousRating" runat="server" Text="Show previous ratings"
                                            Font-Bold="true" />
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <div style="width: 10%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee Name"></asp:Label>
                                        </div>
                                        <div style="width: 26%" class="ib">
                                            <asp:TextBox ID="txtEmployeeName" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <asp:Label ID="lblYear" runat="server" Text="Financial Year"></asp:Label>
                                        </div>
                                        <div style="width: 13%" class="ib">
                                            <asp:DropDownList ID="cboYear" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 20%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 9%" class="ib">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container" style="width: 100%; height: 240px; overflow: auto">
                                <asp:GridView ID="gvCalibrateList" DataKeyNames="yid,pid,empid,fscr,dbname" runat="server"
                                    AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:BoundField DataField="fy" HeaderText="FY" FooterText="dgFY" HeaderStyle-Width="10%" />
                                        <asp:BoundField DataField="pname" HeaderText="Period" FooterText="dgPeriod" HeaderStyle-Width="25%" />
                                        <asp:BoundField DataField="rating" HeaderText="Rating" FooterText="dgRating" HeaderStyle-Width="62%" />
                                        <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDescription" runat="server" ToolTip="View Rating Info" OnClick="lnkInfo_Click"><i class="fa fa-info-circle" style="font-size:19px; color:Blue;"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div style="width: 100%" class="ib">
                                <div id="btnfixedbottom" class="btn-default">
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupinfo" BackgroundCssClass="modalBackground" TargetControlID="lblRatingInfo"
                        runat="server" PopupControlID="pnlRatingInfo" CancelControlID="btnCloseRating" />
                    <asp:Panel ID="pnlRatingInfo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 500px; top: 30px;" Height="313px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblRatingInfo" runat="server" Text="Ratings Information"></asp:Label>
                            </div>
                            <div class="panel-body" style="height: 274px; overflow: auto">
                                <div id="Div5" class="panel-default">
                                    <div id="Div7" style="width: 99%; height: 200px; overflow: auto;">
                                        <asp:GridView ID="gvRating" runat="server" AutoGenerateColumns="False" Width="99%"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="scrf" HeaderText="Score From" FooterText="dgcolhScrFrm"
                                                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="scrt" HeaderText="Score To" FooterText="dgcolhScrTo" HeaderStyle-Width="100px"
                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <asp:Button ID="btnCloseRating" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
