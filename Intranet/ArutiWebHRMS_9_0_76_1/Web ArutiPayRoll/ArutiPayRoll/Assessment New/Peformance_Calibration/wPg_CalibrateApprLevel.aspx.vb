﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Partial Class wPg_CalibrateApprLevel
    Inherits Basepage

#Region " Private Variable "

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmCalibrationApproverLevelList"
    Private ReadOnly mstrModuleName1 As String = "frmCalibrationApproverLevelAddEdit"
    Private mintLevelmstid As Integer
    Private blnpopupApproverLevel As Boolean = False
    Private objApproverLevel As New clscalibrate_approverlevel_master

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            If IsPostBack = False Then
                If IsPostBack = False Then
                    Call SetControlCaptions()
                    'S.SANDEEP |27-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                    Call SetMessages()
                    'S.SANDEEP |27-JUL-2019| -- END
                    Call Language._Object.SaveValue()
                    Call SetLanguage()
                End If
                If CBool(Session("AllowToViewCalibrationApproverLevel")) Then
                    FillList(False)
                Else
                    FillList(True)
                End If
                SetVisibility()
            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If
                If ViewState("blnpopupGreApprover") IsNot Nothing Then
                    blnpopupApproverLevel = Convert.ToBoolean(ViewState("blnpopupApproverLevel").ToString())
                End If
                If blnpopupApproverLevel Then
                    popupApproverLevel.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupApproverLevel") = blnpopupApproverLevel
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim strfilter As String = ""
        Try
            If isblank Then strfilter = " 1 = 2 "

            dsApproverList = objApproverLevel.GetList("List", True, False, strfilter)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objApproverLevel.GetList("List", True, True, "1 = 2")
                isblank = True
            End If

            GvApprLevelList.DataSource = dsApproverList.Tables("List")
            GvApprLevelList.DataBind()
            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Visible = CBool(Session("AllowToAddCalibrationApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetValue()
        Try
            If mintLevelmstid > 0 Then
                objApproverLevel._Levelunkid = mintLevelmstid
            End If
            objApproverLevel._Levelcode = txtlevelcode.Text.Trim
            objApproverLevel._Levelname = txtlevelname.Text.Trim
            objApproverLevel._Priority = Convert.ToInt32(txtlevelpriority.Text)
            Call SetAtValue(mstrModuleName1)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue(ByVal strFormName As String)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objApproverLevel._AuditUserId = CInt(Session("UserId"))
            End If
            objApproverLevel._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproverLevel._ClientIP = CStr(Session("IP_ADD"))
            objApproverLevel._HostName = CStr(Session("HOST_NAME"))
            objApproverLevel._FormName = strFormName
            objApproverLevel._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub GetValue()
        Try
            If mintLevelmstid > 0 Then
                objApproverLevel._Levelunkid = mintLevelmstid
            End If
            txtlevelcode.Text = objApproverLevel._Levelcode
            txtlevelname.Text = objApproverLevel._Levelname
            txtlevelpriority.Text = objApproverLevel._Priority.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearData()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveApprover.Click
        Dim blnFlag As Boolean = False
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            End If
            Call SetValue()
            If mintLevelmstid > 0 Then
                blnFlag = objApproverLevel.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objApproverLevel.Insert()
            End If

            If blnFlag = False And objApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objApproverLevel._Message, Me)
                blnpopupApproverLevel = True
                popupApproverLevel.Show()
            Else
                ClearData()
                FillList(False)
                mintLevelmstid = 0
                blnpopupApproverLevel = False
                popupApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValue()
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())

            If objApproverLevel.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()
            popup_YesNo.Title = "Confirmation"
            popup_YesNo.Message = "Are You Sure You Want To Delete This Approver Level ?"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue(mstrModuleName)
            blnFlag = objApproverLevel.Delete(mintLevelmstid)
            If blnFlag = False And objApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objApproverLevel._Message, Me)
            Else
                FillList(False)
                mintLevelmstid = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseApprover.Click
        Try
            ClearData()
            blnpopupApproverLevel = False
            popupApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ClearData()
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("levelunkid").ToString) > 0 Then
                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                        lnkedit.Visible = CBool(Session("AllowToEditCalibrationApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteCalibrationApproverLevel"))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            Language._Object.setCaption(Me.btnnew.ID, Me.btnnew.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            Language._Object.setCaption(GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            Language._Object.setCaption(GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            Language._Object.setCaption(GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)

            Language.setLanguage(mstrModuleName1)
            Language._Object.setCaption(Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Language._Object.setCaption(Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Language._Object.setCaption(Me.lbllevelname.ID, Me.lbllevelname.Text)
            Language._Object.setCaption(Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)

            Language._Object.setCaption(Me.btnSaveApprover.ID, Me.btnSaveApprover.Text)
            Language._Object.setCaption(Me.btnCloseApprover.ID, Me.btnCloseApprover.Text)


        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader2.Text = Language._Object.getCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            Me.btnnew.Text = Language._Object.getCaption(Me.btnnew.ID, Me.btnnew.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            GvApprLevelList.Columns(2).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            GvApprLevelList.Columns(3).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            GvApprLevelList.Columns(4).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)

            Language.setLanguage(mstrModuleName1)
            Me.lblCancelText1.Text = Language._Object.getCaption(Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Me.lbllevelcode.Text = Language._Object.getCaption(Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Me.lbllevelname.Text = Language._Object.getCaption(Me.lbllevelname.ID, Me.lbllevelname.Text)
            Me.lbllevelpriority.Text = Language._Object.getCaption(Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)

            Me.btnSaveApprover.Text = Language._Object.getCaption(Me.btnSaveApprover.ID, Me.btnSaveApprover.Text).Replace("&", "")
            Me.btnCloseApprover.Text = Language._Object.getCaption(Me.btnCloseApprover.ID, Me.btnCloseApprover.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Language.setMessage(mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Language.setMessage(mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Language.setMessage(mstrModuleName1, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
