﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanApproval.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Approval_Process_wPg_LoanApproval" Title="Loan Approval" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 65%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan Approval"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Loan Approval Info"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblApplicationNo" runat="server" Text="Application No."></asp:Label>
                                            </td>
                                            <td style="width: 37%" colspan="2">
                                                <asp:TextBox ID="txtApplicationNo" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 43%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            <asp:Label ID="lblApplicationDate" Style="margin-left: 15px" runat="server" Text="Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 55%">
                                                            <uc1:DateCtrl ID="dtpApplicationDate" runat="server" AutoPostBack="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 37%" colspan="2">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 43%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            <asp:Label ID="lblBasicSal" Style="margin-left: 15px" runat="server" Text="Basic Salary"></asp:Label>
                                                        </td>
                                                        <td style="width: 55%; padding-right: 4px">
                                                            <asp:TextBox ID="txtBasicSal" runat="server" ReadOnly="true" Style="text-align: right;
                                                                padding-right: 3px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtApprover" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLoanAdvance" runat="server" Text="Select Mode"></asp:Label>
                                            </td>
                                            <td style="width: 40%" colspan="2">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%" align="left">
                                                            <asp:RadioButton ID="radLoan" runat="server" Text="Loan" Enabled="false" GroupName="LoanAdvance"
                                                                AutoPostBack="true" Checked="true" />
                                                        </td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:RadioButton ID="radAdvance" runat="server" Text="Advance" Enabled="false" GroupName="LoanAdvance"
                                                                AutoPostBack="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 43%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                        </td>
                                                        <td style="width: 60%">
                                                            <asp:Label ID="lblDuration" runat="server" Text="Duration In Months" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%">
                                                            <asp:TextBox ID="txtDurationInMths" CssClass="numberonly" Visible="false" onkeypress="return onlyNumbers(this, event);"
                                                                runat="server" Style="text-align: right; background-color: White" Text="0"></asp:TextBox>
                                                        </td>
                                                        <%--<td style="width: 5%" align="right">
                                                            <cc1:NumericUpDownExtender ID="nudDurationInMths" TargetControlID="txtDurationInMths"
                                                                runat="server" Width="85" Minimum="0" Maximum="85">
                                                            </cc1:NumericUpDownExtender>
                                                        </td>--%>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblExternalEntity" runat="server" Text="External Entity"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtExternalEntity" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="true" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAmt" runat="server" Text="Approved Amount"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtLoanAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    AutoPostBack="true" Style="text-align: right" Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:TextBox ID="txtCurrency" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 43%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            <asp:Label ID="lblAppliedAmnt" Style="margin-left: 15px" runat="server" Text="Applied Amount"></asp:Label>
                                                        </td>
                                                        <td style="width: 55%; padding-right: 4px">
                                                            <asp:TextBox ID="txtAppliedAmnt" runat="server" ReadOnly="true" Style="text-align: right;
                                                                padding-right: 4px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduction Period"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amt."></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" AutoPostBack="true" Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                            </td>
                                            <td style="width: 43%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            <asp:Label ID="lblEMIInstallments" runat="server" style="margin-left: 15px;" Text="No. of Installments"></asp:Label>
                                                        </td>
                                                        <td style="width: 55%; padding-right: 4px">
                                                            <asp:TextBox ID="txtEMIInstallments" runat="server" CssClass="numberonly" onkeypress="return onlyNumbers(this, event);"
                                                                AutoPostBack="true" Style="text-align: right; padding-right: 3px;
                                                                background-color: White" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%" valign="top">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 55%" colspan="2">
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%" valign="top">
                                                <asp:Label ID="lblApproverRemarks" runat="server" Text="Remarks"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtRemark" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
