﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Application_wPg_LoanApplicationList
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmLoanApplicationList"
    Dim DisplayMessage As New CommonCodes
    Private objLoanAdvanceunkid As New clsLoan_Advance
    Private objProcesspendingloan As New clsProcess_pending_loan
    Private dtLoanAppList As New DataTable
    Dim blnIsEdit As Boolean = False
    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Private mstrAdvanceFilter As String = String.Empty
    'Nilay (20-Sept-2016) -- End

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                Call SetVisibility()
                'Nilay (20-Sept-2016) -- End

                If dgvLoanApplicationList.Items.Count <= 0 Then
                    dgvLoanApplicationList.DataSource = New List(Of String)
                    dgvLoanApplicationList.DataBind()
                End If
                'Sohail (09 Apr 2016) -- Start
                'Enhancement : 58.1 changes in SS
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'btnGlobalApprove.Visible = False
                    'btnGlobalAssign.Visible = False
                    lnkGlobalApprove.Visible = False
                    lnkGlobalAssign.Visible = False
                    'Nilay (20-Sept-2016) -- End
                    dgvLoanApplicationList.Columns(2).Visible = False
                End If
                'Sohail (09 Apr 2016) -- End
                
            Else
                dtLoanAppList = CType(Me.ViewState("LoanApplicationList"), DataTable)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
                'Nilay (20-Sept-2016) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("LoanApplicationList") = dtLoanAppList
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Nilay (20-Sept-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMaster As New clsMasterData
            Dim dsList As DataSet = Nothing

            'Sohail (09 Apr 2016) -- Start
            'Enhancement : 58.1 changes in SS
            'dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    CStr(Session("UserAccessModeSetting")), _
            '                                    True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                    "EmpList")

            'Dim dRow As DataRow = dsList.Tables(0).NewRow
            'dRow.Item("employeeunkid") = 0
            'dRow.Item("employeename") = "Select"
            'dsList.Tables(0).Rows.InsertAt(dRow, 0)

            'With cboEmployee
            '    .DataValueField = "employeeunkid"
            '    .DataTextField = "employeename"
            '    .DataSource = dsList.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "EmpList")

                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("employeeunkid") = 0
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                'dRow.Item("employeename") = "Select"
                dRow.Item("EmpCodeName") = "Select"
                'Nilay (09-Aug-2016) -- End
                dsList.Tables(0).Rows.InsertAt(dRow, 0)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If
            'Sohail (09 Apr 2016) -- End

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)
            'Pinkal (07-Dec-2017) -- End
            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'dsList = objProcesspendingloan.GetLoan_Status("Status", , True)
            dsList = objProcesspendingloan.GetLoan_Status("Status", , True, True)
            'Nilay (20-Sept-2016) -- End
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboLoanAdvance.Items.Clear()
            Language.setLanguage(mstrModuleName)
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(False, True)
            dsList = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dtCondition
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboAppAmountCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dtCondition.Copy
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim dsProcessPendingLoan As New DataSet
            Dim objExchangeRate As New clsExchangeRate
            Dim StrSearching As String = String.Empty

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewProcessLoanAdvanceList")) = False Then Exit Sub
            End If

            'If CBool(Session("AllowToViewProcessLoanAdvanceList")) = True Then

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                StrSearching &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = " & CInt(cboLoanAdvance.SelectedIndex) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If dtpAppDate.IsNull = False Then
                StrSearching &= "AND lnloan_process_pending_loan.application_date = '" & eZeeDate.convertDate(dtpAppDate.GetDate.Date) & "' "
            End If

            If txtApplicationNo.Text.Trim <> "" Then
                StrSearching &= "AND lnloan_process_pending_loan.Application_No like '" & txtApplicationNo.Text.Trim & "' "
            End If

            If txtAmount.Text.Trim <> "" AndAlso CDec(txtAmount.Text) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.loan_amount " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtAmount.Text) & " "
            End If

            If txtApprovedAmount.Text.Trim <> "" AndAlso CDec(txtApprovedAmount.Text) > 0 Then
                StrSearching = "AND lnloan_process_pending_loan.approved_amount " & cboAppAmountCondition.SelectedItem.Text & " " & CDec(txtApprovedAmount.Text) & " "
            End If

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If
            'Nilay (20-Sept-2016) -- End

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            'Sohail (09 Apr 2016) -- Start
            'Enhancement : 58.1 changes in SS
            'dsProcessPendingLoan = objProcesspendingloan.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                                     CStr(Session("UserAccessModeSetting")), _
            '                                                     True, _
            '                                                     CBool(Session("IsIncludeInactiveEmp")), _
            '                                                     "Loan", , StrSearching)

            dsProcessPendingLoan = objProcesspendingloan.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                             "Loan", , StrSearching, , _
                                                             CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
            'Sohail (09 Apr 2016) -- End


            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'dtLoanAppList = New DataView(dsProcessPendingLoan.Tables("Loan"), "", "Application_No desc", DataViewRowState.CurrentRows).ToTable
            dtLoanAppList = New DataView(dsProcessPendingLoan.Tables("Loan"), "", "processpendingloanunkid desc", DataViewRowState.CurrentRows).ToTable
            'Nilay (20-Sept-2016) -- End

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If CInt(cboStatus.SelectedValue) = 1 Then '1=Pending
            '    dgvLoanApplicationList.Columns(0).Visible = True
            '    dgvLoanApplicationList.Columns(1).Visible = True
            '    dgvLoanApplicationList.Columns(2).Visible = False
            'ElseIf CInt(cboStatus.SelectedValue) = 2 Then '2=Approved
            '    dgvLoanApplicationList.Columns(0).Visible = False
            '    dgvLoanApplicationList.Columns(1).Visible = False
            '    dgvLoanApplicationList.Columns(2).Visible = True
            'ElseIf CInt(cboStatus.SelectedValue) = 3 Then '3=Rejected
            '    dgvLoanApplicationList.Columns(0).Visible = False
            '    dgvLoanApplicationList.Columns(1).Visible = False
            '    dgvLoanApplicationList.Columns(2).Visible = False
            'ElseIf CInt(cboStatus.SelectedValue) = 4 Then '4=Assigned
            '    dgvLoanApplicationList.Columns(0).Visible = False
            '    dgvLoanApplicationList.Columns(1).Visible = False
            '    dgvLoanApplicationList.Columns(2).Visible = False
            'Else
            '    dgvLoanApplicationList.Columns(0).Visible = True
            '    dgvLoanApplicationList.Columns(1).Visible = True
            '    dgvLoanApplicationList.Columns(2).Visible = True
            'End If

            If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.PENDING Then
                dgvLoanApplicationList.Columns(0).Visible = CBool(Session("EditPendingLoan"))
                dgvLoanApplicationList.Columns(1).Visible = CBool(Session("DeletePendingLoan"))
                dgvLoanApplicationList.Columns(2).Visible = False
            ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                dgvLoanApplicationList.Columns(0).Visible = False
                dgvLoanApplicationList.Columns(1).Visible = False
                dgvLoanApplicationList.Columns(2).Visible = CBool(Session("AllowAssignPendingLoan"))
            ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                dgvLoanApplicationList.Columns(0).Visible = False
                dgvLoanApplicationList.Columns(1).Visible = False
                dgvLoanApplicationList.Columns(2).Visible = False
            ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.ASSIGNED OrElse _
                   CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.CANCELLED Then
                dgvLoanApplicationList.Columns(0).Visible = False
                dgvLoanApplicationList.Columns(1).Visible = False
                dgvLoanApplicationList.Columns(2).Visible = False
            Else
                dgvLoanApplicationList.Columns(0).Visible = CBool(Session("EditPendingLoan"))
                dgvLoanApplicationList.Columns(1).Visible = CBool(Session("DeletePendingLoan"))
                dgvLoanApplicationList.Columns(2).Visible = CBool(Session("AllowAssignPendingLoan"))
            End If
            'Nilay (20-Sept-2016) -- End
            'Nilay (06-Aug-2016) -- END

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.CANCELLED Then
                dgvLoanApplicationList.Columns(18).Visible = True
            Else
                dgvLoanApplicationList.Columns(18).Visible = False
            End If
            'Nilay (20-Sept-2016) -- End

            dgvLoanApplicationList.AutoGenerateColumns = False
            dgvLoanApplicationList.DataSource = dtLoanAppList
            dgvLoanApplicationList.DataBind()

            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Private Sub SetVisibility()

        Try
            'Nilay (11-Oct-2016) -- Start
            'btnNew.Enabled = CBool(Session("AddPendingLoan"))
            'lnkGlobalAssign.Enabled = CBool(Session("AllowAssignPendingLoan"))
            'lnkGlobalCancelApproved.Enabled = CBool(Session("AllowToCancelApprovedLoanApp"))
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                btnNew.Enabled = CBool(Session("AddPendingLoan"))
                lnkGlobalAssign.Enabled = CBool(Session("AllowAssignPendingLoan"))
                lnkGlobalCancelApproved.Visible = CBool(Session("AllowToCancelApprovedLoanApp"))

                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                lnkGlobalApprove.Enabled = CBool(Session("AllowToProcessGlobalLoanApprove"))
                'Varsha Rana (17-Oct-2017) -- End

            ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                btnOperation.Visible = False
            End If
            'Nilay (11-Oct-2016) -- End
            'btnEdit.Enabled = User._Object.Privilege._EditPendingLoan
            'btnDelete.Enabled = User._Object.Privilege._DeletePendingLoan

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub
    'Nilay (20-Sept-2016) -- End

#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("LoanApplicationList") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("ProcessPendingLoanunkid") = Nothing
            Me.ViewState("LoanApplicationList") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_AddEditLoanApplication.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'Hemant (25 Oct 2021) -- Start
            'cboEmployee.SelectedValue = "0"
            cboEmployee.SelectedIndex = 0
            'Hemant (25 Oct 2021) -- End
            cboLoanScheme.SelectedIndex = 0
            cboLoanAdvance.SelectedIndex = 0
            cboStatus.SelectedValue = "1"
            txtApplicationNo.Text = ""
            txtAmount.Text = ""
            txtApprovedAmount.Text = ""
            dtpAppDate.SetDate = Nothing
            cboAmountCondition.SelectedIndex = 0
            cboAppAmountCondition.SelectedIndex = 0
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            mstrAdvanceFilter = ""
            'Nilay (20-Sept-2016) -- End
            dgvLoanApplicationList.DataSource = New List(Of String)
            dgvLoanApplicationList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    'Protected Sub btnGlobalApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGlobalApprove.Click
    '    Try
    '        Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_GlobalApproveLoan.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("btnGlobalApprove_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnGlobalAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGlobalAssign.Click
    '    Try
    '        Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_GlobalAssignLoanAdvance.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("" & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (20-Sept-2016) -- End

    Protected Sub popDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonNo_Click
        Try
            Session("ProcessPendingLoanunkid") = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popDeleteReason_buttonDelReasonNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonYes_Click
        Try
            objProcesspendingloan._Voidreason = popDeleteReason.Reason
            objProcesspendingloan._Isvoid = True
            objProcesspendingloan._Voiddatetime = DateAndTime.Now.Date
            objProcesspendingloan._Voiduserunkid = CInt(Session("UserId"))

            'Nilay (05-May-2016) -- Start
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objProcesspendingloan._WebClientIP = Session("IP_ADD").ToString
            objProcesspendingloan._WebFormName = "frmLoanApplicationList"
            objProcesspendingloan._WebHostName = Session("HOST_NAME").ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objProcesspendingloan._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            'Nilay (05-May-2016) -- End

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            'If objProcesspendingloan.Delete(CInt(Session("ProcessPendingLoanunkid"))) = False Then
            If objProcesspendingloan.Delete(CInt(Session("CompanyUnkId")), CInt(Session("ProcessPendingLoanunkid"))) = False Then
                'Pinkal (20-Sep-2022)-- End
                DisplayMessage.DisplayMessage(objProcesspendingloan._Message, Me)
            Else

                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                Dim enloginmode As New enLogin_Mode
                Dim intEmpId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enloginmode = enLogin_Mode.EMP_SELF_SERVICE
                    intEmpId = CInt(Session("Employeeunkid"))
                Else
                    enloginmode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                End If

                If objProcesspendingloan._Isloan = True Then
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                    '                                                 objProcesspendingloan._Processpendingloanunkid, _
                    '                                                 objProcesspendingloan._Employeeunkid, -1, _
                    '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Approver, True, _
                    '                                                 objProcesspendingloan._Processpendingloanunkid.ToString, _
                    '                                                 ConfigParameter._Object._ArutiSelfServiceURL, True, enloginmode, intEmpId, intUserId)
                    objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                     objProcesspendingloan._Processpendingloanunkid, _
                                                                     objProcesspendingloan._Employeeunkid, -1, _
                                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Approver, True, _
                                                                     objProcesspendingloan._Processpendingloanunkid.ToString, _
                                                                     ConfigParameter._Object._ArutiSelfServiceURL, CInt(Session("CompanyUnkId")), True, enloginmode, intEmpId, intUserId)
                    'Sohail (30 Nov 2017) -- End
                Else
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                    '                                                 objProcesspendingloan._Processpendingloanunkid, _
                    '                                                 objProcesspendingloan._Employeeunkid, -1, _
                    '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, True, _
                    '                                                 objProcesspendingloan._Processpendingloanunkid.ToString, _
                    '                                                 ConfigParameter._Object._ArutiSelfServiceURL, True, enloginmode, intEmpId, intUserId)
                    objProcesspendingloan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                     objProcesspendingloan._Processpendingloanunkid, _
                                                                     objProcesspendingloan._Employeeunkid, -1, _
                                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Advance, True, _
                                                                     objProcesspendingloan._Processpendingloanunkid.ToString, _
                                                                     ConfigParameter._Object._ArutiSelfServiceURL, CInt(Session("CompanyUnkId")), True, enloginmode, intEmpId, intUserId)
                    'Sohail (30 Nov 2017) -- End
                End If
                'Shani (21-Jul-2016) -- End
                FillList()
            End If
            Session("ProcessPendingLoanunkid") = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (20-Sept-2016) -- End

#End Region

#Region "DataGrid Events"

    Protected Sub dgvLoanApplicationList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvLoanApplicationList.ItemCommand
        Try
            If e.CommandName.ToUpper = "EDIT" Then

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(cboStatus.SelectedValue) > 1 Then
                If CInt(cboStatus.SelectedValue) <> enLoanApplicationStatus.PENDING Then
                    'Nilay (20-Sept-2016) -- End
                    DisplayMessage.DisplayMessage("You cannot Edit this loan application. Reason: This loan application is in " & e.Item.Cells(10).Text & " status.", Me)
                    Exit Sub
                End If
                'Nilay (05-May-2016) -- Start
                If CInt(e.Item.Cells(12).Text) = 1 Then
                    Dim objApprovalProcessTran As New clsloanapproval_process_Tran
                    If CBool(objApprovalProcessTran.IsPendingLoanApplication(CInt(e.Item.Cells(17).Text))) = False Then
                        DisplayMessage.DisplayMessage("You cannot Edit this loan application. Reason: It is already in approval process.", Me)
                        Exit Sub
                    End If
                End If
                'Nilay (05-May-2016) -- End

                Session("ProcessPendingLoanunkid") = e.Item.Cells(17).Text
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_AddEditLoanApplication.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(cboStatus.SelectedValue) > 1 Then
                If CInt(cboStatus.SelectedValue) <> enLoanApplicationStatus.PENDING Then
                    'Nilay (20-Sept-2016) -- End
                    DisplayMessage.DisplayMessage("You cannot Delete this loan application. Reason: This loan application is in " & e.Item.Cells(10).Text & " status.", Me)
                    Exit Sub
                End If
                'Nilay (05-May-2016) -- Start

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(e.Item.Cells(12).Text) = 1 Then
                If CInt(e.Item.Cells(12).Text) = enLoanApplicationStatus.PENDING Then
                    'Nilay (20-Sept-2016) -- End
                    Dim objApprovalProcessTran As New clsloanapproval_process_Tran
                    If CBool(objApprovalProcessTran.IsPendingLoanApplication(CInt(e.Item.Cells(17).Text))) = False Then
                        DisplayMessage.DisplayMessage("You cannot Delete this loan application. Reason: It is already in approval process.", Me)
                        Exit Sub
                    End If
                End If
                'Nilay (05-May-2016) -- End
                Session("ProcessPendingLoanunkid") = e.Item.Cells(17).Text
                Language.setLanguage(mstrModuleName)
                popDeleteReason.Title = Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Loan/Advance Application?")
                popDeleteReason.Show()

            ElseIf e.CommandName.ToUpper = "ASSIGN" Then

                'Nilay (05-May-2016) -- Start
                'If CInt(e.Item.Cells(12).Text) = 1 Then
                '    Dim objApprovalProcessTran As New clsloanapproval_process_Tran
                '    If CBool(objApprovalProcessTran.IsPendingLoanApplication(CInt(e.Item.Cells(17).Text))) = True Then
                '        DisplayMessage.DisplayMessage("You cannot Assign this loan application. Reason: This Loan application is in " & e.Item.Cells(10).Text & " status.", Me)
                '        Exit Sub
                '    End If
                'End If
                'Nilay (05-May-2016) -- End

                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(cboStatus.SelectedValue) = 2 Then 'Approved
                If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                    'Nilay (20-Sept-2016) -- End
                    Dim mintPendingApprovalTranID As Integer = -1
                    Dim mintEmployeeID As Integer = -1
                    Dim objApprovalTran As New clsloanapproval_process_Tran
                    Dim dsList As DataSet = Nothing
                    Dim dtList As DataTable = Nothing

                    dsList = objApprovalTran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                                 "List", _
                                                                 CInt(e.Item.Cells(13).Text), _
                                                                 CInt(e.Item.Cells(17).Text), _
                                                                 "lnloanapproval_process_tran.statusunkid = 2")

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        dtList = New DataView(dsList.Tables(0), "", "priority desc", DataViewRowState.CurrentRows).ToTable
                        If dtList.Rows.Count > 0 Then
                            mintPendingApprovalTranID = CInt(dtList.Rows(0)("pendingloantranunkid"))
                            mintEmployeeID = CInt(dtList.Rows(0)("employeeunkid"))
                        End If
                    End If
                    dtList = Nothing
                    dsList = Nothing
                    objApprovalTran = Nothing

                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    Session("PendingApprovalTranID") = mintPendingApprovalTranID
                    Session("EmployeeID") = mintEmployeeID
                    'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx?" _
                    '                  & HttpUtility.UrlEncode(clsCrypto.Encrypt(mintPendingApprovalTranID.ToString & "|" & mintEmployeeID.ToString)), False)
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvance_AddEdit.aspx", False)
                    'Nilay (06-Aug-2016) -- END
                Else
                    DisplayMessage.DisplayMessage("You cannot Assign this Loan application. Reason: This Loan application is in " & e.Item.Cells(10).Text & " status.", Me)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanApplicationList_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvLoanApplicationList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLoanApplicationList.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                If e.Item.Cells(4).Text.ToString().Trim <> "" AndAlso e.Item.Cells(4).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).Date.ToShortDateString
                End If

                'Pinkal (16-Apr-2016) -- End
                e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString)
                e.Item.Cells(9).Text = Format(CDec(e.Item.Cells(9).Text), Session("fmtCurrency").ToString)

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanApplicationList_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
#Region " Link Button's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "emp."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkGlobalApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalApprove.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_GlobalApproveLoan.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkGlobalApprove_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkGlobalAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalAssign.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_GlobalAssignLoanAdvance.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkGlobalAssign_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkGlobalCancelApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalCancelApproved.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_GlobalCancelApprovedApplication.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkGlobalCancelApproved_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region
    'Nilay (20-Sept-2016) -- End
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblApprovedAmount.Text = Language._Object.getCaption(Me.lblApprovedAmount.ID, Me.lblApprovedAmount.Text)
            Me.lblAppNo.Text = Language._Object.getCaption(Me.lblAppNo.ID, Me.lblAppNo.Text)
            Me.lblAppDate.Text = Language._Object.getCaption(Me.lblAppDate.ID, Me.lblAppDate.Text)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvLoanApplicationList.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(3).FooterText, Me.dgvLoanApplicationList.Columns(3).HeaderText)
            Me.dgvLoanApplicationList.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(4).FooterText, Me.dgvLoanApplicationList.Columns(4).HeaderText)
            Me.dgvLoanApplicationList.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(5).FooterText, Me.dgvLoanApplicationList.Columns(5).HeaderText)
            Me.dgvLoanApplicationList.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(6).FooterText, Me.dgvLoanApplicationList.Columns(6).HeaderText)
            Me.dgvLoanApplicationList.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(7).FooterText, Me.dgvLoanApplicationList.Columns(7).HeaderText)
            Me.dgvLoanApplicationList.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(8).FooterText, Me.dgvLoanApplicationList.Columns(8).HeaderText)
            Me.dgvLoanApplicationList.Columns(9).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(9).FooterText, Me.dgvLoanApplicationList.Columns(9).HeaderText)
            Me.dgvLoanApplicationList.Columns(10).HeaderText = Language._Object.getCaption(Me.dgvLoanApplicationList.Columns(10).FooterText, Me.dgvLoanApplicationList.Columns(10).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
