﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Status_wPg_Global_LoanStatus_Change
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmGlobalLoanStatus_Change"
    Dim DisplayMessage As New CommonCodes
    Private objLoan_Advance As New clsLoan_Advance
    Private objStatustran As New clsLoan_Status_tran
    Private mdtTable As New DataTable
    Private mdtStatusPeriodDate As Date = Nothing

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                Call FillCombo()
                If dgvGridData.Items.Count <= 0 Then
                    dgvGridData.DataSource = New List(Of String)
                    dgvGridData.DataBind()
                End If
                objPnlData.Visible = False
            Else
                mdtTable = CType(Me.Session("mdtLoanAdvance"), DataTable)
                mdtStatusPeriodDate = CDate(Me.ViewState("StatusPeriodDate"))
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.Session.Add("mdtLoanAdvance", mdtTable)
            Me.ViewState.Add("StatusPeriodDate", mdtStatusPeriodDate)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objMaster.GetLoan_Saving_Status("LoanStatus")
            Dim dtTab1 = New DataView(dsList.Tables("LoanStatus"), "Id IN (0,1,2)", "", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dtTab1
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dtTab2 = New DataView(dsList.Tables("LoanStatus"), "Id IN(0,1,2,3)", "", DataViewRowState.CurrentRows).ToTable
            With cboOperation
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dtTab2
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboStatusPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period").Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboLoanAdvance
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Loan"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Advance"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim dsLoanAdvance As DataSet = Nothing
            Dim strSearch As String = ""

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                strSearch &= "AND LN.Loan_AdvanceUnkid = " & CInt(cboLoanAdvance.SelectedIndex) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND LN.loanschemeunkid =  " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND LN.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                strSearch &= "AND LN.periodunkid =  " & CInt(cboPayPeriod.SelectedValue) & " "
            End If

            If dtpDateFrom.IsNull = False AndAlso dtpToDate.IsNull = False Then
                strSearch &= "AND LN.effective_date >= '" & eZeeDate.convertDate(dtpDateFrom.GetDate.Date) & "' AND effective_date <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsLoanAdvance = objLoan_Advance.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    CStr(Session("AccessLevelFilterString")), strSearch)

            mdtTable = Nothing
            mdtTable = dsLoanAdvance.Tables(0).Copy

            mdtTable.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mdtTable.Columns.Add("ddate", System.Type.GetType("System.String"))

            For Each dRow As DataRow In mdtTable.Rows
                dRow.Item("ddate") = eZeeDate.convertDate(dRow.Item("effective_date").ToString).ToShortDateString
                dRow.Item("ischeck") = False
            Next

            dgvGridData.AutoGenerateColumns = False
            dgvGridData.DataSource = mdtTable
            dgvGridData.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStatustran._Isvoid = False
            objStatustran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
            objStatustran._Remark = txtRemarks.Text
            objStatustran._Settle_Amount = 0
            objStatustran._Staus_Date = DateAndTime.Now.Date
            objStatustran._Voiddatetime = Nothing
            objStatustran._Voiduserunkid = -1
            objStatustran._Statusunkid = CInt(cboOperation.SelectedValue)
            objStatustran._Periodunkid = CInt(cboStatusPeriod.SelectedValue)
            'Nilay (05-May-2016) -- Start
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objStatustran._WebClientIP = Session("IP_ADD").ToString
            objStatustran._WebFormName = "frmGlobalLoanStatus_Change"
            objStatustran._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing
            Dim objMaster As New clsMasterData
            Dim dtTable As New DataTable

            dsList = objMaster.GetLoan_Saving_Status("LoanStatus")

            If CInt(cboStatus.SelectedValue) > 0 Then

                dtTable = New DataView(dsList.Tables("LoanStatus"), "Id IN(0,1,2,3) AND Id <> '" & CInt(cboStatus.SelectedValue) & "'", "", DataViewRowState.CurrentRows).ToTable
                With cboOperation
                    .DataValueField = "Id"
                    .DataTextField = "NAME"
                    .DataSource = dtTable
                    .DataBind()
                    .SelectedValue = "0"
                End With
                objMaster = Nothing
            Else
                dtTable = New DataView(dsList.Tables("LoanStatus"), "Id IN(0,1,2,3)", "", DataViewRowState.CurrentRows).ToTable
                cboOperation.DataSource = dtTable
                cboOperation.DataBind()
                cboOperation.SelectedValue = "0"
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStatus_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboOperation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOperation.SelectedIndexChanged
        Try
            If CInt(cboOperation.SelectedValue) = 3 Then
                txtRemarks.Text = ""
                txtRemarks.Enabled = True
            Else
                txtRemarks.Text = ""
                txtRemarks.Enabled = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboOperation_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboStatusPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatusPeriod.SelectedIndexChanged
        Try
            If CInt(cboStatusPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboStatusPeriod.SelectedValue)
                mdtStatusPeriodDate = objPeriod._End_Date
                objPeriod = Nothing
            Else
                mdtStatusPeriodDate = Nothing
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStatusPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Loan status is mandatory information. Please select loan status in order to continue."), Me)
                cboStatus.Focus()
                Exit Sub
            End If

            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboLoanAdvance.SelectedIndex = 0
            cboLoanScheme.SelectedValue = "0"
            cboStatus.SelectedValue = "0"
            cboPayPeriod.SelectedValue = "0"

            Call cboStatus_SelectedIndexChanged(Nothing, Nothing)
            Call cboOperation_SelectedIndexChanged(Nothing, Nothing)

            dgvGridData.DataSource = New List(Of String)
            dgvGridData.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim blnFlag As Boolean = False

            If CInt(cboOperation.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Change Status is mandatory information. Please select Change Status to continue."), Me)
                cboOperation.Focus()
                Exit Sub
            End If

            If CInt(cboOperation.SelectedValue) = 3 Then
                If txtRemarks.Text.Trim.Length <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Remark is mandatory information. Please enter remark to continue."), Me)
                    txtRemarks.Focus()
                    Exit Sub
                End If
            End If

            If CInt(cboStatusPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Status Period is mandatory information. Please select status period to continue."), Me)
                cboStatusPeriod.Focus()
                Exit Sub
            End If

            Dim blnFutureLoan As Boolean = False
            Language.setLanguage(mstrModuleName)
            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set the status for future loan with period less than the assigned period.")
            objPnlData.Visible = False

            mdtTable.AcceptChanges()
            If mdtTable IsNot Nothing Then
                Dim dtmp() As DataRow = mdtTable.Select("ischeck = true")
                If dtmp.Length > 0 Then
                    For index As Integer = 0 To dtmp.Length - 1
                        Dim idx As Integer = -1
                        idx = mdtTable.Rows.IndexOf(dtmp(index))

                        objLoan_Advance._Loanadvancetranunkid = CInt(dtmp(index).Item("loanadvancetranunkid"))

                        If mdtStatusPeriodDate <> Nothing Then
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(Session("Database_Name").ToString) = objLoan_Advance._Periodunkid
                            If objPeriod._End_Date.Date > mdtStatusPeriodDate.Date Then
                                objPeriod = Nothing
                                If blnFutureLoan = False Then
                                    objPnlData.Visible = True
                                    blnFutureLoan = True
                                End If

                                If idx > -1 Then
                                    dgvGridData.Items(idx).ForeColor = Drawing.Color.Maroon
                                    dgvGridData.Items(idx).BackColor = Drawing.Color.AntiqueWhite
                                End If
                                Continue For
                            End If
                            objPeriod = Nothing
                        End If
                        If objStatustran IsNot Nothing Then
                            objStatustran = Nothing
                            objStatustran = New clsLoan_Status_tran
                        End If

                        If objStatustran.IsStatusExists(CInt(cboStatusPeriod.SelectedValue), CInt(cboOperation.SelectedValue), _
                                                        CInt(dtmp(index).Item("loanadvancetranunkid"))) = True Then
                            If idx > -1 Then
                                dgvGridData.Items(idx).ForeColor = Drawing.Color.Red
                            End If
                            Continue For
                        End If
                        Call SetValue()
                        'Nilay (25-Mar-2016) -- Start
                        'blnFlag = objStatustran.Insert(CStr(Session("Database_Name")), CInt(Session("Fin_year")))
                        blnFlag = objStatustran.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                       CStr(Session("UserAccessModeSetting")), True, , True)
                        'Nilay (25-Mar-2016) -- End

                        If blnFlag = True Then
                            objLoan_Advance._LoanStatus = CInt(cboOperation.SelectedValue)
                            objLoan_Advance.Update()
                        Else
                            If idx > -1 Then
                                dgvGridData.Items(idx).ForeColor = Drawing.Color.Red
                            End If
                        End If
                    Next

                    If blnFlag = True Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Change Status process done successfully."), Me)
                        Call FillList()
                    Else
                        If blnFutureLoan = False Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Change Status for some of the loan/advance did not changed and highlighted in red color."), Me)
                        End If
                        Exit Sub
                    End If

                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)

            If mdtTable.Rows.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgvGridData.Items
                CType(item.Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkBox.Checked
                Dim dRow() As DataRow = mdtTable.Select("employeeunkid = " & item.Cells(9).Text)
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = chkBox.Checked
                End If
            Next
            mdtTable.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim dRow() As DataRow = mdtTable.Select("employeeunkid = " & item.Cells(9).Text)
            If dRow.Length > 0 Then
                dRow(0).Item("ischeck") = chkBox.Checked
            End If
            mdtTable.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, lblPageHeader.Text)
            Me.gbFilter.Text = Language._Object.getCaption("gbFilterCriteria", Me.gbFilter.Text)
            Me.gbOperation.Text = Language._Object.getCaption("gbOperation", Me.gbOperation.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblFrmDate.Text = Language._Object.getCaption(Me.lblFrmDate.ID, Me.lblFrmDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.ID, Me.lblOperation.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvGridData.Columns(1).HeaderText = Language._Object.getCaption(dgvGridData.Columns(1).FooterText, dgvGridData.Columns(1).HeaderText)
            Me.dgvGridData.Columns(2).HeaderText = Language._Object.getCaption(dgvGridData.Columns(2).FooterText, dgvGridData.Columns(2).HeaderText)
            Me.dgvGridData.Columns(3).HeaderText = Language._Object.getCaption(dgvGridData.Columns(3).FooterText, dgvGridData.Columns(3).HeaderText)
            Me.dgvGridData.Columns(4).HeaderText = Language._Object.getCaption(dgvGridData.Columns(4).FooterText, dgvGridData.Columns(4).HeaderText)
            Me.dgvGridData.Columns(5).HeaderText = Language._Object.getCaption(dgvGridData.Columns(5).FooterText, dgvGridData.Columns(5).HeaderText)
            Me.dgvGridData.Columns(6).HeaderText = Language._Object.getCaption(dgvGridData.Columns(6).FooterText, dgvGridData.Columns(6).HeaderText)
            Me.dgvGridData.Columns(7).HeaderText = Language._Object.getCaption(dgvGridData.Columns(7).FooterText, dgvGridData.Columns(7).HeaderText)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
