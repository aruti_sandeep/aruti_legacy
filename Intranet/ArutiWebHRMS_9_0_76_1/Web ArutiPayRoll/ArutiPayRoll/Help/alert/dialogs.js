
function showSuccessMessage(msg) {
swal({
        title: "",
        text: msg,
        type: "",
        showCancelButton: false,
        closeOnConfirm: true,
		focusConfirm:true,
		closeOnEsc: true
    });
    $('.sweet-alert').drags();
}

function showLocationMessage(msg,location) {
    //window.stop();
	document.execCommand('Stop');
	swal({
        title: "",
        text: msg,
        type: "",
        showCancelButton: false,
        confirmButtonText: "Ok",
        closeOnConfirm: true,
	    focusConfirm: true,
        //S.SANDEEP |18-JAN-2020| -- START
        //ISSUE/ENHANCEMENT : ALTER CLOSES MESSAGE BOX AND DOES NOT REDIRECT TO LOCATION WHEN 'ESC' KEY WAS PRESSED
        //closeOnEsc: true
        allowEscapeKey: false,
        closeOnEsc: false
        //S.SANDEEP |18-JAN-2020| -- END                
	}, function(rslt) {
	    window.location.href = location;
    });
    $('.sweet-alert').drags();
}
    
function showErroMessage(msg, errormsg) {
swal({
        title: "",
        text: msg,
        type: "info",
        showCancelButton: true,
        closeOnConfirm: true,
        focusConfirm: true,
        closeOnEsc: true,
        confirmButtonText: "Report Error",
        cancelButtonText: "Don't Report Now"
    }, function() {

        $.ajax({
            type: "POST",
            url: window.location.pathname + "/SendErrorReport",
            data: '{strError:' + JSON.stringify(errormsg) + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                if (response.d.trim() != "") {
                    swal({
                        title: "",
                        text: response.d,
                        type: "",
        showCancelButton: false,
                        confirmButtonText: "Ok",
        closeOnConfirm: true,
                        focusConfirm: true,
		closeOnEsc: true
    });
}

            }
        });
    }
    );
    $('.sweet-alert').drags();
}
