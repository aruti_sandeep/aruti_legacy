﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region


Partial Class HR_wPg_EmployeeExperienceList
    Inherits Basepage


#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim strSearching As String = ""


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmJobHistory_ExperienceList"
    'Pinkal (06-May-2014) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objAExperienceTran As New clsJobExperience_approval_tran

    Dim Arr() As String
    Dim ExperienceApprovalFlowVal As String

    Dim blnPendingEmployee As Boolean = False
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mblnisEmployeeApprove As Boolean = False
    'Gajanan [17-April-2019] -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Try

            'S.SANDEEP [ 16 JAN 2014 ] -- START
            'If (Session("loginBy") = Global.User.en_loginby.Employee) Then
            '    drpEmployee.BindEmployee(CInt(Session("Employeeunkid")))
            'Else
            '    drpEmployee.BindEmployee()
            'End If
            Dim dsCombos As New DataSet
            Dim objEmployee As New clsEmployee_Master
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .




                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmJobHistory_ExperienceList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If



                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                            CInt(Session("UserId")), _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            CStr(Session("UserAccessModeSetting")), True, _
                '                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)


                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                       mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Employee", True, _
                                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [20-JUN-2018] -- End




                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If
            'S.SANDEEP [ 16 JAN 2014 ] -- END

            Dim objJob As New clsJobs
            drpJob.DataSource = objJob.getComboList("Job", True)
            drpJob.DataValueField = "jobunkid"
            drpJob.DataTextField = "name"
            drpJob.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Public Sub GetData()
        Dim strSearching As String = ""
        Try

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewEmpExperienceList")) = False Then Exit Sub
            End If

            Dim objEmpExp As New clsJobExperience_tran
            Dim dtEmpExperience As DataTable = Nothing
            Dim dsEmpExperience As DataSet = Nothing


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            If CInt(drpEmployee.SelectedValue) > 0 Then
                strSearching += "AND hremployee_master.employeeunkid =" & CInt(drpEmployee.SelectedValue) & " "
            End If

            If CInt(drpJob.SelectedValue) > 0 Then
                strSearching += "AND hremp_experience_tran.JobUnkId =" & CInt(drpJob.SelectedValue) & " "
            End If

            If TxtSupervisor.Text.Trim.Length > 0 Then
                strSearching += "AND ISNULL(hremp_experience_tran.supervisor,'') LIKE '%" & TxtSupervisor.Text.Trim & "%' "
            End If

            If TxtCompany.Text.Trim.Length > 0 Then
                strSearching += "AND ISNULL(hremp_experience_tran.company,'') LIKE '%" & TxtCompany.Text.Trim & "%' "
            End If

            If dtpStartdate.IsNull = False Then
                strSearching += "AND CONVERT(CHAR(8),hremp_experience_tran.start_date,112) >= '" & eZeeDate.convertDate(dtpStartdate.GetDate) & "' "
            End If

            If dtpEnddate.IsNull = False Then
                strSearching += "AND CONVERT(CHAR(8),hremp_experience_tran.end_date,112) <= '" & eZeeDate.convertDate(dtpEnddate.GetDate) & "' "
            End If


            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            'Pinkal (28-Dec-2015) -- End



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmpExperience = objEmpExp.GetList("Experience", Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            'dsEmpExperience = objEmpExp.GetList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Experience", , strSearching)



            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If Session("PendingEmployeeScreenIDs").ToString().Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString().Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmJobHistory_ExperienceList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsEmpExperience = objEmpExp.GetList(CStr(Session("Database_Name")), _
            '                                  CInt(Session("UserId")), _
            '                                  CInt(Session("Fin_year")), _
            '                                  CInt(Session("CompanyUnkId")), _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                  CStr(Session("UserAccessModeSetting")), True, _
            '                                  CBool(Session("IsIncludeInactiveEmp")), "Experience", , strSearching, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            Dim mblnisusedmss As Boolean = False
            IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, mblnisusedmss = True, mblnisusedmss = False)

            dsEmpExperience = objEmpExp.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                              CBool(Session("IsIncludeInactiveEmp")), "Experience", , strSearching, mblnisusedmss, mblnAddApprovalCondition) 'Gajanan [17-DEC-2018] -- Add isusedmss

            'Gajanan [17-DEC-2018] -- End
            'Pinkal (28-Dec-2015) -- End
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [20-JUN-2018] -- End



            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsEmpExperience.Tables(0).Columns.Add(dcol)

            If ExperienceApprovalFlowVal Is Nothing Then
                Dim dsPending As New DataSet
                dsPending = objAExperienceTran.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", , strSearching, mblnisusedmss, mblnAddApprovalCondition)




                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsEmpExperience.Tables(0).ImportRow(row)
                    Next
                End If
            End If

            'Gajanan [17-DEC-2018] -- End




            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            dtEmpExperience = New DataView(dsEmpExperience.Tables("Experience"), "", "", DataViewRowState.CurrentRows).ToTable
            'Pinkal (28-Dec-2015) -- End



            If (Not dtEmpExperience Is Nothing) Then
                GvExpList.DataSource = dtEmpExperience
                GvExpList.DataKeyField = "ExpId"
                GvExpList.DataBind()
            Else
                DisplayMessage.DisplayMessage("GetData :-  ", Me)
                Exit Sub
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                GvExpList.CurrentPageIndex = 0
                GvExpList.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End

                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmJobHistory_ExperienceList"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes
            If Not IsPostBack Then
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                Else
                    clsCommonATLog._LoginEmployeeUnkid = -1
                End If

                'Pinkal (24-Aug-2012) -- End

                'S.SANDEEP [ 04 JULY 2012 ] -- END


                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnNew.Visible = ConfigParameter._Object._AllowEditExperience
                    BtnNew.Visible = CBool(Session("AllowEditExperience"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = False
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP [20-JUN-2018] -- End

                Else


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = False
                    BtnNew.Visible = CBool(Session("AddEmployeeExperience"))
                    'Anjan (30 May 2012)-End 


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    'If Session("ShowPending") IsNot Nothing Then
                    '    chkShowPending.Checked = True
                    'End If
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP [20-JUN-2018] -- End

                End If


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'SHANI [01 FEB 2015]--END
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    GvExpList.Columns(2).Visible = False
                Else
                    GvExpList.Columns(2).Visible = True
                End If
                'Pinkal (22-Mar-2012) -- End




                FillCombo()


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Experience_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(Session("Experience_EmpUnkID"))
                    Session.Remove("Experience_EmpUnkID")
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Call BtnSearch_Click(BtnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END 

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Else
                blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))
                'Gajanan [17-DEC-2018] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                    mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                End If
                'Gajanan [17-April-2019] -- End
            End If
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'lblPendingData.Visible = False
            'btnApprovalinfo.Visible = False
            objtblPanel.Visible = False

            If blnPendingEmployee Then
                objtblPanel.Visible = blnPendingEmployee
            End If

            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            ExperienceApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmJobHistory_ExperienceList)))

            'Gajanan [17-DEC-2018] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END


        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Try
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState.Add("mblnisEmployeeApprove", mblnisEmployeeApprove)
            'Gajanan [17-April-2019] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Gajanan [17-DEC-2018] -- End

    End Sub
    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "
    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End


    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(drpEmployee.SelectedValue) > 0 Then
                Session("Experience_EmpUnkID") = drpEmployee.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END 
            Response.Redirect(CStr(Session("servername")) & "~/HR/wPg_EmployeeExperience.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then  'SHANI [01 FEB 2015]-START If (txtreasondel.Text = "")
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If

            Dim clsExperience As New clsJobExperience_tran

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Aruti.Data.User._Object._Userunkid = -1
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            Else
                clsExperience._Userunkid = CInt(Session("UserId"))
            End If

            With clsExperience
                'Hemant (01 Sep 2023) -- Start
                'ISSUE(ZRA) : A1X-1250 - Resolve bug when adding/editing/deleting experiences (Old Ui) 
                clsExperience._Experiencetranunkid = CInt(Me.ViewState("ExpId"))
                'Hemant (01 Sep 2023) -- End
                ._Isvoid = True
                ._Voidatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'SHANI [01 FEB 2015]-START  txtreasondel.Text

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    ._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    ._Voiduserunkid = CInt(Session("UserId"))
                End If

                'Hemant (01 Sep 2023) -- Start
                'ISSUE(ZRA) : A1X-1250 - Resolve bug when adding/editing/deleting experiences (Old Ui) 
                'clsExperience._Experiencetranunkid = CInt(Me.ViewState("ExpId"))
                'Hemant (01 Sep 2023) -- End

                'If ExperienceApprovalFlowVal Is Nothing Then
                If ExperienceApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Dim eLMode As Integer
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        eLMode = enLogin_Mode.MGR_SELF_SERVICE
                    Else
                        eLMode = enLogin_Mode.EMP_SELF_SERVICE
                    End If
                    'Gajanan [17-April-2019] -- End




                    objAExperienceTran._Isvoid = True
                    objAExperienceTran._Audituserunkid = CInt(Session("UserId"))
                    objAExperienceTran._Isweb = False
                    objAExperienceTran._Ip = getIP()
                    objAExperienceTran._Host = getHostName()
                    objAExperienceTran._Form_Name = mstrModuleName
                    If objAExperienceTran.Delete(CInt(Me.ViewState("ExpId")), popup_DeleteReason.Reason.Trim, CInt(Session("CompanyUnkId")), Nothing) = False Then
                        If objAExperienceTran._Message <> "" Then
                            DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & objAExperienceTran._Message, Me)
                        Else
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Dim objJobExperience_tran As New clsJobExperience_tran
                            objJobExperience_tran._Experiencetranunkid = CInt(Me.ViewState("Unkid"))

                            objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                             CStr(Session("UserAccessModeSetting")), _
                                                             CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                             enScreenName.frmJobHistory_ExperienceList, CStr(Session("EmployeeAsOnDate")), _
                                                             CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                             Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.DELETED, , objJobExperience_tran._Employeeunkid.ToString(), , , _
                                                             " experiencetranunkid = " & CInt(Me.ViewState("ExpId")), Nothing, , , _
                                                             " experiencetranunkid = " & CInt(Me.ViewState("ExpId")), Nothing)
                            'Gajanan [17-April-2019] -- End




                            DisplayMessage.DisplayMessage("Entry Successfully deleted." & objAExperienceTran._Message, Me)
                            Me.ViewState("ExpId") = Nothing
                            GetData()
                        End If
                        Exit Sub
                    End If
                Else
                    .Delete(CInt(Me.ViewState("ExpId")))

                    If (._Message.Length > 0) Then
                        DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                        Me.ViewState("ExpId") = Nothing
                        GetData()
                    End If
                End If

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'popup1.Dispose()
                'txtreasondel.Text = ""
                popup_DeleteReason.Dispose()
                ''SHANI [01 FEB 2015]--END
                GetData()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            GetData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpEmployee.SelectedIndex = 0
            If drpJob.Items.Count > 0 Then drpJob.SelectedIndex = 0
            TxtCompany.Text = ""
            TxtSupervisor.Text = ""
            dtpStartdate.SetDate = Nothing
            dtpEnddate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END
    'Gajanan [17-DEC-2018] -- Start
    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences
            Popup_Viewreport._FillType = enScreenName.frmJobHistory_ExperienceList
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(drpEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE
            Popup_Viewreport.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprovalinfo_Click Event : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Gajanan [17-DEC-2018] -- End
#End Region

#Region " Control's Event(s) "

#Region "GridView Event"

    Protected Sub GvExpList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GvExpList.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If (e.Item.ItemIndex >= 0) Then
            '    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        'GvExpList.Columns(1).Visible = ConfigParameter._Object._AllowDeleteExperience

            '        'Pinkal (22-Nov-2012) -- Start
            '        'Enhancement : TRA Changes

            '        GvExpList.Columns(0).Visible = CBool(Session("AllowEditExperience"))
            '        GvExpList.Columns(1).Visible = CBool(Session("AllowDeleteExperience"))

            '        'Pinkal (22-Nov-2012) -- End

            '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



            '    Else

            '        'Anjan (30 May 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        'GvExpList.Columns(1).Visible = False

            '        'Pinkal (22-Nov-2012) -- Start
            '        'Enhancement : TRA Changes

            '        GvExpList.Columns(0).Visible = CBool(Session("EditEmployeeExperience"))
            '        GvExpList.Columns(1).Visible = CBool(Session("DeleteEmployeeExperience"))

            '        'Pinkal (22-Nov-2012) -- End


            '        'Anjan (30 May 2012)-End 


            '        'S.SANDEEP [20-JUN-2018] -- Start
            '        'Enhancement - Implementing Employee Approver Flow For NMB .

            '        ''S.SANDEEP [ 16 JAN 2014 ] -- START
            '        'If Session("ShowPending") IsNot Nothing Then
            '        '    GvExpList.Columns(1).Visible = False
            '        'End If
            '        ''S.SANDEEP [ 16 JAN 2014 ] -- END

            '        'S.SANDEEP [20-JUN-2018] -- End
            'End If

            '    'S.SANDEEP [ 31 DEC 2013 ] -- START
            '    'If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.Length > 0 Then
            '    '    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date
            '    'End If
            '    'If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Length > 0 Then
            '    '    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date
            '    'End If

            '    'Pinkal (16-Apr-2016) -- Start
            '    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            '    'If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.Length > 0 Then
            '    '    If Session("DateFormat").ToString <> Nothing Then
            '    '        e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToString(Session("DateFormat").ToString)
            '    '    Else
            '    '        e.Item.Cells(5).Text = CStr(eZeeDate.convertDate(e.Item.Cells(5).Text).Date)
            '    '    End If
            '    'End If
            '    'If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Length > 0 Then
            '    '    If Session("DateFormat").ToString <> Nothing Then
            '    '        e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToString(Session("DateFormat").ToString)
            '    '    Else
            '    '        e.Item.Cells(6).Text = CStr(eZeeDate.convertDate(e.Item.Cells(6).Text).Date)
            '    '    End If
            '    'End If
            'If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.Length > 0 Then
            '        e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
            'End If
            'If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Length > 0 Then
            '        e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToShortDateString
            'End If
            '    'Pinkal (16-Apr-2016) -- End

            ''S.SANDEEP [ 31 DEC 2013 ] -- END

            If (e.Item.ItemIndex >= 0) Then

                If ExperienceApprovalFlowVal Is Nothing Then
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = CBool(Session("EditEmployeeExperience"))
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = CBool(Session("DeleteEmployeeExperience"))
                    Else
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = CBool(Session("AllowEditExperience"))
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = CBool(Session("AllowDeleteExperience"))
                    End If

                    If e.Item.Cells(9).Text <> "&nbsp;" AndAlso e.Item.Cells(9).Text.Length > 0 Then
                        e.Item.BackColor = Color.PowderBlue
                        e.Item.ForeColor = Color.Black

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'lblPendingData.Visible = True
                        'btnApprovalinfo.Visible = True
                        objtblPanel.Visible = True
                        blnPendingEmployee = True
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(0).FindControl("ImgSelect"), LinkButton).Visible = False

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If

                Else
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        GvExpList.Columns(0).Visible = CBool(Session("AllowEditExperience"))
                        GvExpList.Columns(1).Visible = CBool(Session("AllowDeleteExperience"))
                    Else
                        GvExpList.Columns(0).Visible = CBool(Session("EditEmployeeExperience"))
                        GvExpList.Columns(1).Visible = CBool(Session("DeleteEmployeeExperience"))
                    End If
                End If

                If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.Length > 0 Then
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
                End If
                If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Length > 0 Then
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToShortDateString
                End If
                'Gajanan [17-DEC-2018] -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvExpList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvExpList.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If e.CommandName = "Select" Then
            '    Session.Add("ExpId", GvExpList.DataKeys(e.Item.ItemIndex))
            '    'SHANI [09 Mar 2015]-START
            '    'Enhancement - REDESIGN SELF SERVICE.
            '    If CInt(drpEmployee.SelectedValue) > 0 Then
            '        Session("Experience_EmpUnkID") = drpEmployee.SelectedValue
            '    End If
            '    'SHANI [09 Mar 2015]--END 
            '    Response.Redirect(CStr(Session("servername")) & "~/HR/wPg_EmployeeExperience.aspx", False)
            'End If

            If GvExpList.Items.Count > 0 Then
                If GvExpList.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).Count() > 0 Then
                    Dim item = GvExpList.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Color.White
                End If
            End If

            If e.CommandName = "Select" Then
                If ExperienceApprovalFlowVal Is Nothing Then
                    Dim item = GvExpList.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(9).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), Me)
                        Exit Sub
                    Else
                        Session.Add("ExpId", GvExpList.DataKeys(e.Item.ItemIndex))
                        If CInt(drpEmployee.SelectedValue) > 0 Then
                            Session("Experience_EmpUnkID") = drpEmployee.SelectedValue
                        End If
                        Response.Redirect(CStr(Session("servername")) & "~/HR/wPg_EmployeeExperience.aspx", False)
                    End If
                Else
                    Session.Add("ExpId", GvExpList.DataKeys(e.Item.ItemIndex))
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Session("Experience_EmpUnkID") = drpEmployee.SelectedValue
                    End If
                    Response.Redirect(CStr(Session("servername")) & "~/HR/wPg_EmployeeExperience.aspx", False)
                End If
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                Dim item = GvExpList.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(9).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Color.LightCoral
                    item.ForeColor = Color.Black
                End If
            End If
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvExpList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvExpList.DeleteCommand
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Me.ViewState.Add("ExpId", CInt(GvExpList.DataKeys(e.Item.ItemIndex)))
            'popup_DeleteReason.Show() 'SHANI [01 FEB 2015]-START popup1.Show()



            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(e.Item.Cells(10).Text)
            mblnisEmployeeApprove = objEmployee._Isapproved


            'If ExperienceApprovalFlowVal Is Nothing Then
            If ExperienceApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                'Gajanan [17-April-2019] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmJobHistory_ExperienceList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(e.Item.Cells(10).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End


                Dim item = GvExpList.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(9).Text.Trim <> "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), Me)
                    Exit Sub
                Else
                    Me.ViewState.Add("ExpId", CInt(GvExpList.DataKeys(e.Item.ItemIndex)))
                    popup_DeleteReason.Show()
                End If
            Else
                Me.ViewState.Add("ExpId", CInt(GvExpList.DataKeys(e.Item.ItemIndex)))
                popup_DeleteReason.Show()
            End If
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvExpList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GvExpList.PageIndexChanged
        Try
            GvExpList.CurrentPageIndex = e.NewPageIndex
            GetData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    'S.SANDEEP [ 16 JAN 2014 ] -- START
    'Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
    '    Try
    '        Session("ShowPending") = chkShowPending.Checked
    '        Dim dsCombos As New DataSet
    '        Dim objEmp As New clsEmployee_Master
    '        If chkShowPending.Checked = True Then

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
    '            dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        CStr(Session("UserAccessModeSetting")), False, _
    '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
    '            'Shani(24-Aug-2015) -- End
    '            Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
    '            With drpEmployee
    '                .DataValueField = "employeeunkid"
    '                'Nilay (09-Aug-2016) -- Start
    '                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                '.DataTextField = "employeename"
    '                .DataTextField = "EmpCodeName"
    '                'Nilay (09-Aug-2016) -- End
    '                .DataSource = dtTab
    '                .DataBind()
    '                Try
    '                    .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))))
    '                Catch ex As Exception
    '                    .SelectedValue = CStr(0)
    '                End Try
    '            End With
    '            BtnNew.Visible = False
    '        Else
    '            Session("ShowPending") = Nothing
    '            BtnNew.Visible = True
    '            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
    '                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        CStr(Session("UserAccessModeSetting")), True, _
    '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
    '                'Shani(24-Aug-2015) -- End


    '                With drpEmployee
    '                    .DataValueField = "employeeunkid"
    '                    'Nilay (09-Aug-2016) -- Start
    '                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                    '.DataTextField = "employeename"
    '                    .DataTextField = "EmpCodeName"
    '                    'Nilay (09-Aug-2016) -- End
    '                    .DataSource = dsCombos.Tables("Employee")
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            Else
    '                Dim objglobalassess = New GlobalAccess
    '                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '                With drpEmployee
    '                    .DataSource = objglobalassess.ListOfEmployee
    '                    .DataTextField = "loginname"
    '                    .DataValueField = "employeeunkid"
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            End If
    '        End If
    '        GvExpList.DataSource = Nothing : GvExpList.DataBind()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [ 16 JAN 2014 ] -- END

    'S.SANDEEP [20-JUN-2018] -- End

#End Region

#End Region

#Region "ToolBar Event"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployeeExperience")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddExperience")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End
    '        Response.Redirect("~\HR\wPg_EmployeeExperience.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END

        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblSupervisor.Text = Language._Object.getCaption(Me.lblSupervisor.ID, Me.lblSupervisor.Text)
        Me.lblInstitution.Text = Language._Object.getCaption(Me.lblInstitution.ID, Me.lblInstitution.Text)
        Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
        Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.ID, Me.lblEndDate.Text)
        Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.ID, Me.lblStartDate.Text)

        Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.ID, Me.lblPendingData.Text)
        Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.ID, Me.lblParentData.Text)

        Me.GvExpList.Columns(2).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(2).FooterText, Me.GvExpList.Columns(2).HeaderText)
        Me.GvExpList.Columns(3).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(3).FooterText, Me.GvExpList.Columns(3).HeaderText)
        Me.GvExpList.Columns(4).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(4).FooterText, Me.GvExpList.Columns(4).HeaderText)
        Me.GvExpList.Columns(5).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(5).FooterText, Me.GvExpList.Columns(5).HeaderText)
        Me.GvExpList.Columns(6).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(6).FooterText, Me.GvExpList.Columns(6).HeaderText)
        Me.GvExpList.Columns(7).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(7).FooterText, Me.GvExpList.Columns(7).HeaderText)
        Me.GvExpList.Columns(8).HeaderText = Language._Object.getCaption(Me.GvExpList.Columns(8).FooterText, Me.GvExpList.Columns(8).HeaderText)

        Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
