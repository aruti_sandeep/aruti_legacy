﻿<%@ Page Title="Employee Dependants" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmpDepentants.aspx.vb" Inherits="wPgEmpDepentants" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

//function pageLoad(sender, args) {
//    $("select").searchable();

//    var img = document.getElementById('<%= imgDependant.ClientID %>' + '_imgZoom');
//    if (img != null)
//        document.onmousemove = getMouseXY;

//    var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
//    if (fupld != null)
//        fileUpLoadChange();    
//
    //}

    function GetMousePosition() {
    var img = document.getElementById('<%= imgDependant.ClientID %>' + '_imgZoom');
    if (img != null)
        document.onmousemove = getMouseXY;
    }

    function FileUploadChangeEvent() {
    var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
    if (fupld != null)
        fileUpLoadChange();    
}
    </script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
            var cboEmp = $('#<%= cboEmployee.ClientID %>');
            var cboRelation = $('#<%= DrpRelation.ClientID %>');
            var txtLastname = $('#<%= txtLastName.ClientID %>');
            var txtFirstname = $('#<%= txtFirstName.ClientID %>');
                       
            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            if (parseInt(cboEmp.val()) <= 0) {
                alert('Employee is compulsory information. Please select Employee to continue.');
                cboEmp.focus();
                return false;
            }
            if (parseInt(cboRelation.val()) <= 0) {
                alert('Relation is compulsory information. Please select relation to continue.');
                cboRelation.focus();
                return false;
            }
            if (txtLastname.val().trim() == "") {
                alert('Lastname can not be blank. Lastname is compulsory information.');
                txtLastname.focus();
                return false;
            }
            if (txtFirstname.val().trim() == "") {
                alert('Firstname can not be blank. Firstname is compulsory information.');
                txtFirstname.focus();
                return false;
            }
            
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dependants"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Dependants Add/Edit"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 36%" align="left" valign="top">
                                                <table style="width: 100%;" id="tblPart1">
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblDBFirstName" runat="server" Text="First Name"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:TextBox ID="txtFirstName" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblDBMiddleName" runat="server" Text="Middle Name"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:TextBox ID="txtMiddleName" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblDBLastName" runat="server" Text="Last Name"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:TextBox ID="txtLastName" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblDBBirthDate" runat="server" Text="Birthdate"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:TextBox ID="txtAge" runat="server" Width="50%" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:DropDownList ID="drpGender" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblDBRelation" runat="server" Text="Relation"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:DropDownList ID="DrpRelation" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            &nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <asp:LinkButton ID="lnkCopyAddress" runat="server" Text="Copy Address From Employee"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <asp:CheckBox ID="chkBeneficiaries" runat="server" Text="Treat as a Beneficiaries" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 60%" align="left" valign="top">
                                                <table style="width: 100%;" id="tblPart2">
                                                    <tr>
                                                        <td style="width: 8%" valign="top">
                                                            <asp:Label ID="lblDBAddress" runat="server" Text="Address"></asp:Label>
                                                        </td>
                                                        <td colspan="2" valign="top">
                                                            <asp:TextBox ID="TxtAddress" runat="server" Rows="8" TextMode="MultiLine" />
                                                        </td>
                                                        <td style="width: 21%">
                                                            <uc8:ZoomImage ID="imgDependant" runat="server" ZoomPercentage="250" />
                                                            <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <uc9:FileUpload ID="flUpload" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            &nbsp;<asp:Button ID="btnRemoveImage" runat="server" Text="Remove Image" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 12%">
                                                            <asp:Label ID="lblDBPostCountry" runat="server" Text="Post Country"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%">
                                                            <asp:DropDownList ID="drpcountry" runat="server" AutoPostBack="True" />
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblState" runat="server" Text="Post State"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:DropDownList ID="drpState" runat="server" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBPostTown" runat="server" Text="Post Town"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:DropDownList ID="drppostTown" runat="server" AutoPostBack="True" />
                                                        </td>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBPostCode" runat="server" Text="Post Code"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:DropDownList ID="drppostcode" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBNationality" runat="server" Text="Nationality"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:DropDownList ID="drpnationality" runat="server" />
                                                        </td>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBEmail" runat="server" Text="Email"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:TextBox ID="txtemail" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBPostBox" runat="server" Text="Post Box"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:TextBox ID="txtpostbox" runat="server" />
                                                        </td>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBIdNo" runat="server" Text="ID No"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:TextBox ID="txtidno" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBResidentialNo" runat="server" Text="Res. Tel No."></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:TextBox ID="txtResNo" runat="server" />
                                                        </td>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblDBMobileNo" runat="server" Text="Mobile No"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                            <asp:TextBox ID="txtmobileno" runat="server" />
                                                        </td>
                                                    </tr>
                                                   <%-- <tr>
                                                        <td style="width: 58%" colspan="4">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblInfo" runat="server" Text="Total Beneficiaries"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 15%">
                                                                        <asp:TextBox ID="txtTotBeneficiaries" runat="server" ReadOnly="true" Style="text-align: center" />
                                                                    </td>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="lblTotalPercent" runat="server" Text="Allocated (%)"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 15%">
                                                                        <asp:TextBox ID="txtTotalPercentage" runat="server" ReadOnly="true" Style="text-align: right" />
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblTotalRemainingPercent" runat="server" Text="Remaining (%)"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 15%">
                                                                        <asp:TextBox ID="txtRemainingPercent" runat="server" ReadOnly="true" Style="text-align: right" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td style="width: 8%">
                                                           &nbsp;
                                                        </td>
                                                        <td style="width: 21%">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 8%">
                                                            <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 21%">
                                                           <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" AutoPostBack="False" /> 
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="Div1" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblAttachmentHeader" runat="server" Text="Document Attachment"></asp:Label>
                                        </div>
                                    </div>
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="2">
                                                <table style="width: 80%; text-align: left">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblAttachmentDate" runat="server" Text="Attachment Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <uc2:DateCtrl ID="dtpAttachment" runat="server" AutoPostBack="false" />
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Panel ID="pnl_ImageAdd" runat="server" Width="100%">
                                                                <div id="fileuploader">
                                                                    <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                                        value="Browse" />
                                                                </div>
                                                            </asp:Panel>
                                                            <%--'Hemant (30 Nov 2018) -- Start
                                                            'Enhancement : Including Language Settings For Scan/Attachment Button--%>
                                                            <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse"
                                                                OnClick="btnSaveAttachment_Click" />
                                                            <%--'Hemant (30 Nov 2018) -- End--%>                                                            
                                                        </td>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                        <td style="width: 20%">
                                                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                        </td>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:Panel ID="pnl_dgvDependant" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                                <asp:DataGrid ID="dgvDependant" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                    Width="69%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                    HeaderStyle-Font-Bold="false">
                                                                    <Columns>
                                                                        <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                        ToolTip="Delete"></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                        <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                            ItemStyle-HorizontalAlign="Right" />
                                                                        <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                            ItemStyle-Font-Size="22px">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete">
                                                                                                            <i class="fa fa-download"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                            Visible="false" />
                                                                        <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                            Visible="false" />
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnsave" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        <div style="float: left">
                                            <uc3:OperationButton ID="cmnuOperation" runat="server" Text="Operation" TragateControlId="data" />
                                            <div id="data" style="display: none">
                                                <asp:LinkButton ID="mnuMembershipInfo" Style="min-width: 200px" runat="server" Text="Membership Info"></asp:LinkButton>
                                                <asp:LinkButton ID="menuBenefitInfor" runat="server" Style="min-width: 200px" Text="Benefit Info"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                    <div id="Membership_Information_Popup">
                        <cc1:ModalPopupExtender ID="popup_MembershipInfo" runat="server" TargetControlID="hdf_MembershipInfo"
                            CancelControlID="hdf_MembershipInfo" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                            PopupControlID="pnl_MembershipInfo" Drag="True">
                        </cc1:ModalPopupExtender>
                        <asp:Panel ID="pnl_MembershipInfo" runat="server" CssClass="newpopup" Style="display: none;
                            width: 750px">
                            <div class="panel-primary" style="margin-bottom: 0px">
                                <div class="panel-heading">
                                    <asp:Label ID="lblmem_Title" runat="server" Text="Membership Info."></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <div id="Div2" class="panel-default">
                                        <div id="Div3" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lblmem_Header" runat="server" Text="Membership"></asp:Label>
                                            </div>
                                        </div>
                                        <div id="Div28" class="panel-body-default">
                                            <table style="vertical-align: middle; margin-left: 5px; margin-top: 5px; margin-bottom: 10px;
                                                width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblMembershipCategory" runat="server" Text="Mem. Category"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboMemCategory" runat="server" Width="175px" AutoPostBack="true"
                                                            Height="20px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblMemberNo" runat="server" Text="Mem. No."></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:TextBox ID="txtMembershipNo" runat="server" Width="100%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblDBMedicalMembershipNo" runat="server" Text="Medical Mem."></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboMedicalNo" runat="server" Width="175px" Height="20px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 50%; text-align: right;" colspan="2">
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="btn-default">
                                                <asp:Button ID="btnAddMembership" runat="server" Text="Add" CssClass="btndefault" />
                                                <asp:Button ID="btnEditMembership" runat="server" Text="Edit" CssClass="btnDefault"
                                                    Visible="false" />
                                                <asp:Button ID="btnMemClear" runat="server" Text="Clear" CssClass="btndefault" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Div4" class="panel-default">
                                        <div id="Div5" class="panel-body-default">
                                            <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 99%;
                                                overflow: auto; max-height: 250px;" class="gridscroll">
                                                <asp:Panel ID="pnl_MemList" runat="server" Width="100%" Style="text-align: center">
                                                    <asp:DataGrid ID="lvMembershipInfo" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        ItemStyle-CssClass="griviewitem" AutoGenerateColumns="false" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                                        ToolTip="Edit" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                                        ToolTip="Delete" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="Category" HeaderText="Membership Type" FooterText="colhMembershipType" />
                                                            <asp:BoundColumn DataField="membershipname" HeaderText="Membership" FooterText="colhMembership" />
                                                            <asp:BoundColumn DataField="membershipno" HeaderText="Membership No" FooterText="colhMembershipNo" />
                                                            <asp:BoundColumn DataField="membership_categoryunkid" FooterText="objcolhMembershipCatId"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="membershipunkid" FooterText="objcolhMembershipCatId"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="dpndtmembershiptranunkid" FooterText="objcolhMembershipUnkid"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhMGUID" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                            <div class="btn-default">
                                                <asp:HiddenField ID="hdf_MembershipInfo" runat="server" />
                                                <asp:Button ID="btnMem_Save" runat="server" Text="Save" CssClass="btndefault" />
                                                <asp:Button ID="btnMem_Close" runat="server" Text="Close" CssClass="btnDefault" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div id="Div6">
                        <cc1:ModalPopupExtender ID="popup_BenefitInfo" runat="server" TargetControlID="hdf_BenefitInfo"
                            CancelControlID="hdf_BenefitInfo" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                            PopupControlID="pnl_BenefitInfo" Drag="True">
                        </cc1:ModalPopupExtender>
                        <asp:Panel ID="pnl_BenefitInfo" runat="server" CssClass="newpopup" Style="display: none;
                            width: 750px">
                            <div class="panel-primary" style="margin-bottom: 0px">
                                <div class="panel-heading">
                                    <asp:Label ID="lblBenefit_Title" runat="server" Text="Benefit Info."></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <div id="Div7" class="panel-default">
                                        <div id="Div8" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lblBenefit_Header" runat="server" Text="Benefit Info"></asp:Label>
                                            </div>
                                        </div>
                                        <div id="Div9" class="panel-body-default">
                                            <table style="vertical-align: middle; margin-left: 5px; margin-top: 5px; margin-bottom: 10px;
                                                width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblBenefitGroup" runat="server" Text="Benefit Group"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboBenefitGroup" runat="server" Width="175px" Height="20px"
                                                            AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lblValueBasis" runat="server" Text="Benefit In"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%" colspan="3">
                                                        <asp:DropDownList ID="cboValueBasis" runat="server" Width="215px" Height="20px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblDBBenefitType" runat="server" Text="Benefit Type"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboBenefitType" runat="server" Width="175px" Height="20px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lblBenefitsInPercent" runat="server" Text="Perc. (%)"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:TextBox ID="txtBenefitPercent" runat="server" Width="100%" CssClass="decimal"
                                                            Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblDBBenefitInAmount" runat="server" Text="Amount"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:TextBox ID="txtBenefitAmount" runat="server" Width="100%" CssClass="decimal"
                                                            Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                
                                              <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                    </td>
                                                    <td style="width: 30%">
                                                    </td>
                                                       <td style="width: 15%">
                                                            <asp:Label ID="lblTotalPercent" runat="server" Text="Allocated (%)"></asp:Label>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:TextBox ID="txtTotalPercentage" runat="server" ReadOnly="true" Style="text-align: right"  Text = "0.00" />
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblTotalRemainingPercent" runat="server" Text="Remaining (%)"></asp:Label>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:TextBox ID="txtRemainingPercent" runat="server" ReadOnly="true" Style="text-align: right" Text ="0.00" />
                                                        </td>
                                                </tr>
                                               
                                            </table>
                                            <div class="btn-default">
                                                <asp:Button ID="btnAddBenefit" runat="server" Text="Add" CssClass="btndefault" />
                                                <asp:Button ID="btnEditBenefit" runat="server" Text="Edit" CssClass="btnDefault"
                                                    Visible="false" />
                                                <asp:Button ID="btnBenefitClear" runat="server" Text="Clear" CssClass="btndefault" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Div10" class="panel-default">
                                        <div id="Div11" class="panel-body-default">
                                            <div id="Div12" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 99%; overflow: auto;
                                                max-height: 250px;" class="gridscroll">
                                                <asp:Panel ID="pnl_BenefitList" runat="server" Width="100%" Style="text-align: center">
                                                    <asp:DataGrid ID="lvBenefit" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        ItemStyle-CssClass="griviewitem" AutoGenerateColumns="false" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                                        ToolTip="Edit" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                                        ToolTip="Delete" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="benefit_group" HeaderText="Benefit Group" FooterText="colhBenefitGroup" />
                                                            <asp:BoundColumn DataField="benefit_type" HeaderText="Benefit Type" FooterText="colhBenefitType" />
                                                            <asp:BoundColumn DataField="benefit_value" HeaderText="Value Basis" FooterText="colhValueBasis" />
                                                            <asp:BoundColumn DataField="benefit_percent" HeaderText="Percent(%)" FooterText="colhPercent"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="benefit_amount" HeaderText="Amount" FooterText="colhAmount"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="benefitgroupunkid" FooterText="objcolhBenefitGrpId" Visible="false" />
                                                            <asp:BoundColumn DataField="benefitplanunkid" FooterText="objcolhBenefitId" Visible="false" />
                                                            <asp:BoundColumn DataField="value_id" FooterText="objcolhValueBasisId" Visible="false" />
                                                            <asp:BoundColumn DataField="dpndtbenefittranunkid" Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhBGUID" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                            <div class="btn-default">
                                                <asp:HiddenField ID="hdf_BenefitInfo" runat="server" />
                                                <asp:Button ID="btnBenefit_Save" runat="server" Text="Save" CssClass="btndefault" />
                                                <asp:Button ID="btnBenefit_Close" runat="server" Text="Close" CssClass="btnDefault" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                </ContentTemplate>
                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
	    $(document).ready(function()
		{
		    ImageLoad();
			$(".ajax-upload-dragdrop").css("width","auto");
        });
		function ImageLoad(){
		    if ($(".ajax-upload-dragdrop").length <= 0){
		    $("#fileuploader").uploadFile({
			    url: "wPgEmpDepentants.aspx?uploadimage=mSEfU19VPc4=",
                method: "POST",
				dragDropStr: "",
				showStatusAfterSuccess:false,
                showAbort:false,
                showDone:false,
				fileName:"myfile",
				onSuccess:function(path,data,xhr){
				$("#<%= btnAddAttachment.ClientID %>").click();
                },
                onError:function(files,status,errMsg){
	                alert(errMsg);
                }
            });
        }
        }
        $('input[type=file]').live("click",function(){
		    if($(this).attr("class") != "flupload"){
		    return IsValidAttach();
		    }
        });
    </script>

</asp:Content>
