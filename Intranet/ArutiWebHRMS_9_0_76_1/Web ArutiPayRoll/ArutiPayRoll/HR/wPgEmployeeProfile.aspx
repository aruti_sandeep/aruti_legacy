<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="true" CodeFile="wPgEmployeeProfile.aspx.vb"
    Inherits="wPgEmployeeProfile" Title="My Profile" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>

        <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

        <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

        <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
        </script>

        <script type="text/javascript">

//function pageLoad(sender, args) {
//    $("select").searchable();

//     var img = document.getElementById('<%= imgEmp.ClientID %>'+'_imgZoom');
//    if (img != null)
//        document.onmousemove = getMouseXY;

//    var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
//    if (fupld != null)
//        fileUpLoadChange();
//}

function GetMousePosition() {
                var img = document.getElementById('<%= imgEmp.ClientID %>' + '_imgZoom');
    if (img != null)
        document.onmousemove = getMouseXY;
}

function FileUploadChangeEvent() {
                var cnt = $('.flupload').length;
                for (i = 0; i < cnt; i++) {
                    var fupld = $('.flupload')[i].id;
    if (fupld != null)
                        //fileUpLoadChange($('.flupload')[i].id);
                        fileUpLoadChange('#' + $('.flupload')[i].id);
                }
}
        </script>

        <%--<script type="text/javascript">
function onlyNumbers(evt) 
    {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        var cHeight = document.getElementById('<%=txtHeight.ClientID%>').value;
        if (cHeight.length > 0)
            if (charCode == 46)
                if (cHeight.indexOf(".") > 0)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }
    
   function onlyNum(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        var cWeight = document.getElementById('<%=txtWeight.ClientID%>').value;
        if (cWeight.length > 0)
            if (charCode == 46)
                if (cWeight.indexOf(".") > 0)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }    
</script>--%>
        <center>
            <asp:Panel ID="MainPan" runat="server" Width="100%">
                <asp:UpdatePanel ID="uppnl_main" runat="server">
                    <ContentTemplate>
                        <div class="panel-primary" style="padding-bottom: 10px">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader" runat="server" Text="My Profile"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <asp:Label ID="lblpnl_header" runat="server" Text="Employee Detail" />
                                    </div>
                                    <div class="panel-body-default" style="position: relative;">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td align="left" colspan="2">
                                                    <asp:Panel ID="pnlCombo" runat="server">
                                                        <table style="width: 100%;">
                                                            <tr style="width: 100%">
                                                                <td style="width: 20%" align="left">
                                                                </td>
                                                                <td style="width: 40%" align="left">
                                                                    <asp:CheckBox ID="chkShowPending" runat="server" AutoPostBack="true" Text="Display Pending Employee(s)" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 20%" align="left">
                                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%" align="left">
                                                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td style="width: 20%" align="left" colspan="2" rowspan="6" valign="top">
                                                    <table id="Images" runat="server">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblEPhoto" runat="server" Text="Photo" Font-Bold="true" Font-Size="X-Small"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblESign" runat="server" Text="Signature" Font-Bold="true" Font-Size="X-Small"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                    <uc8:ZoomImage ID="imgEmp" runat="server" ZoomPercentage="250" />
                                                    <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <uc9:FileUpload ID="flUpload" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                                &nbsp;<asp:Button ID="btnRemoveImage" runat="server" Text="Remove Image" style="margin-left: -8px;"/>
                                                </td>
                                                            <td valign="top">
                                                                <uc8:ZoomImage ID="imgSignature" runat="server" ZoomPercentage="250" />
                                                                <asp:UpdatePanel ID="UPUploadSig" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <uc9:FileUpload ID="flUploadsig" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                &nbsp;<asp:Button ID="btnRemoveSig" runat="server" Text="Remove signature" style="margin-left: -8px;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblEmployeeCode" runat="server" Text="Code"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtCodeValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%" align="left">
                                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                                </td>
                                                <td style="width: 40%" align="left">
                                                    <asp:DropDownList ID="cboTitle" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%" align="left">
                                                    <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
                                                </td>
                                                <td style="width: 40%" align="left">
                                                    <asp:TextBox ID="txtFirstValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%" align="left">
                                                    <asp:Label ID="lblSurname" runat="server" Text="Surname"></asp:Label>
                                                </td>
                                                <td style="width: 40%" align="left">
                                                    <asp:TextBox ID="txtSurnameValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%" align="left">
                                                    <asp:Label ID="lblOthername" runat="server" Text="Other Name"></asp:Label>
                                                </td>
                                                <td style="width: 40%" align="left">
                                                    <asp:TextBox ID="txtOtherNameValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtGenderValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                                    <asp:LinkButton ID="lnkViewAllocations" runat="server" ForeColor="#006699" Text="Allocations"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    <asp:LinkButton ID="lnkViewDates" runat="server" ForeColor="#006699" Text="Dates"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblEmploymentType" runat="server" Text="Employment Type"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtEmploymentTypeValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                                    <asp:LinkButton ID="lnkViewAddress" runat="server" ForeColor="#006699" Text="Addresses"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    <asp:LinkButton ID="lnkViewEmergency" runat="server" ForeColor="#006699" Text="Emergency Contacts"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblPaypoint" runat="server" Text="Pay Point"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtPayPointValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                                    <asp:LinkButton ID="lnkViewPersonal" runat="server" ForeColor="#006699" Text="Personal"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    <asp:LinkButton ID="lnkViewLeave" runat="server" ForeColor="#006699" Text="Leave Records"
                                                        Visible="False" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblIdNo" runat="server" Text="Identity No"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtIdValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                                    <asp:LinkButton ID="lnkViewAssessor" runat="server" ForeColor="#006699" Text="View Assessor/Reviewer"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    <asp:LinkButton ID="lnkViewLeaveApprover" runat="server" ForeColor="#006699" Text="View Leave Approver"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblShift" runat="server" Text="Shift"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtShiftValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                                    <asp:LinkButton ID="lnkViewReportingTo" runat="server" ForeColor="#006699" Text="View Reporting To"
                                                        Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                <%-- 'Pinkal (05-May-2020) -- Start
                                                'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .--%>
                                                      <asp:LinkButton ID="lnkViewClaimApprover" runat="server" ForeColor="#006699" Text="View Claim Request Approver"
                                                        Font-Underline="false"></asp:LinkButton>
                                               <%-- 'Pinkal (05-May-2020) -- End--%>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblPayType" runat="server" Text="Pay Type"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtPayValue" runat="server" ReadOnly="true" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                               <%-- 'Pinkal (05-May-2020) -- Start
                                                'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .--%>
                                                    <asp:LinkButton ID="lnkViewOTApprover" runat="server" ForeColor="#006699" Text="View OT Requisition Approver"
                                                        Font-Underline="false"></asp:LinkButton>
                                               <%-- 'Pinkal (05-May-2020) -- End--%>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="lblEmail" runat="server" Text="Company Email"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <asp:TextBox ID="txtcoEmail" runat="server" Text=""></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 19%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="btnfixedbottom" class="btn-default">
                                            <asp:Button ID="btnsave" runat="server" CssClass="btndefault" Text="Update" />
                                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <cc1:ModalPopupExtender ID="popupEmpReporting" runat="server" CancelControlID="btnEmpReportingClose"
                            PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlEmpReporting">
                        </cc1:ModalPopupExtender>
                        <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="newpopup" Style="cursor: move;
                            display: none; width: 650px;" DefaultButton="btnEmpReportingClose">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <asp:Label ID="LblReportingTo" runat="server" Text="" />
                                </div>
                                <div class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%; margin-left: 5px; margin-top: 5px; margin-bottom: 10px;">
                                                <div style="width: 100%; max-height: 300px; vertical-align: middle; overflow: auto;">
                                                <asp:GridView ID="GvReportingTo" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                    Width="99%">
                                                </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Cancel" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </center>
        <div>
            <%--<table style="width:100%" class ="ContentCantroller">
     <tr style="width:100%">
            <td align="left"  valign="top">
                <cc1:TabContainer ID="tabcEmployeeData" runat="server" ActiveTabIndex="3" 
                    Width="100%" Height="220px" Visible="False">
                    <cc1:TabPanel ID="tapbPersonal" runat="server"  Width="100%" Height = "100%"><HeaderTemplate>Address</HeaderTemplate>
                        <ContentTemplate><asp:Panel ID="pnlPersonalInfo" runat="server" ScrollBars="Vertical" Height="220px" Font-Size="Small"><table style="width:100%" class="ContentCantroller"><tr style="width:100%; background-color:#667C93"><td colspan="6"><asp:Label ID="lblPresentAddress" runat="server" Text="Present Address" ForeColor="White" Font-Bold="True"></asp:Label></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblPAddress1" runat="server" Text="Address1 :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPAddress1" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPAddress2" runat="server" Text="Address2 :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPAddress2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPCountry" runat="server" Text="Post Country :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboPCountry" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblPState" runat="server" Text="State :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboPState" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblPCity" runat="server" Text="Post Town :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboPCity" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblPCode" runat="server" Text="Post Code :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboPZipcode" runat="server" Width="155px"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblPRegion" runat="server" Text="Prov/Region :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPRegion" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPStree" runat="server" Text="Road/Street :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPStreet" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPEState" runat="server" Text="EState :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPEState" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblPPlotNo" runat="server" Text="Plot :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPPlotNo" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPMobile" runat="server" Text="Mobile :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPMobile" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPAltNo" runat="server" Text="Alt. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPAltNo" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblPTelNo" runat="server" Text="Tel. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPTelNo" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPFax" runat="server" Text="Fax :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPFax" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblPEmail" runat="server" Text="Email :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtPEmail" runat="server"></asp:TextBox></td></tr><tr style="width:100%;background-color:#667C93"><td align="left" colspan="6"><asp:Label ID="lblDomicileAddress" runat="server" Font-Bold="True" 
                                                ForeColor="White" Text="Domicile Address"></asp:Label>&#160;&#160;&#160;&#160; <asp:LinkButton 
                                ID="lnkCopyAddress" runat="server" Text="Copy Address" ForeColor="White" 
                                Visible="False"></asp:LinkButton></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblDAddress1" runat="server" Text="Address1 :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDAddress1" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDAddress2" runat="server" Text="Address2 :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDAddress2" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDCountry" runat="server" Text="Post Country :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList 
                                    ID="cboDCountry" runat="server" AutoPostBack="True" Width="155px" 
                                    Enabled="False"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblDState" runat="server" Text="State :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList 
                                    ID="cboDState" runat="server" AutoPostBack="True" Width="155px" Enabled="False"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblDCity" runat="server" Text="Post Town :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList 
                                    ID="cboDCity" runat="server" AutoPostBack="True" Width="155px" Enabled="False"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblDCode" runat="server" Text="Post Code :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList 
                                    ID="cboDZipcode" runat="server" Width="155px" Enabled="False"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblDRegion" runat="server" Text="Prov/Region :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDRegion" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDStree" runat="server" Text="Road/Street :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDStreet" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDEState" runat="server" Text="EState :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDEState" runat="server" ReadOnly="True"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblDPlotNo" runat="server" Text="Plot :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDPlotNo" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDMobile" runat="server" Text="Mobile :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDMobile" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDAltNo" runat="server" Text="Alt. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDAltNo" runat="server" ReadOnly="True"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblDTelNo" runat="server" Text="Tel. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDTelNo" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDFax" runat="server" Text="Fax :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDFax" runat="server" ReadOnly="True"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblDEmail" runat="server" Text="Email :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox 
                                    ID="txtDEmail" runat="server" ReadOnly="True"></asp:TextBox></td></tr></table></asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="tabpEmergencyContact" runat="server"  Width="100%" Height = "100%"><HeaderTemplate>Emergency Contact</HeaderTemplate>
                        <ContentTemplate><asp:Panel ID="pnlEmergency" runat="server" ScrollBars="Vertical" Height="220px"><table style="width:100%;" class="ContentCantroller"><tr style="background-color:#667C93; width:100%"><td colspan="6"><asp:Label ID="lblPhysicianContact1" runat="server" Font-Bold="True" 
                                                ForeColor="White" Text="Physician Contact 1"></asp:Label></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEFirstname" runat="server" Text="Firstname :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEFirstname" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblELastname" runat="server" Text="Lastname :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtELastname" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEAddress" runat="server" Text="Address :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEAddress" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblECountry" runat="server" Text="Country :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboECountry" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblEState" runat="server" Text="State :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboEState" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblECity" runat="server" Text="Post Town :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboECity" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblECode" runat="server" Text="Post Code :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboEZipcode" runat="server" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblERegion" runat="server" Text="Prov/Region :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtERegion" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEStreet" runat="server" Text="Road/Street :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEStreet" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEEState" runat="server" Text="EState :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEEState" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEPlotNo" runat="server" Text="Plot No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEPlotNo" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEMobile" runat="server" Text="Mobile :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEMobile" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEAltNo" runat="server" Text="Alt. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEAltNo" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblETelNo" runat="server" Text="Tel. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtETelNo" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEFax" runat="server" Text="Fax :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEFax" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEEmail" runat="server" Text="Email :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEEMail" runat="server"></asp:TextBox></td><td style="width:11%" align="left"></td><td style="width:22%" align="left"></td><td style="width:11%" align="left"></td><td style="width:22%" align="left"></td></tr><tr style="background-color:#667C93; width:100%"><td align="left" colspan="6"><asp:Label ID="lblPhysicianContact2" runat="server" Font-Bold="True" 
                                                ForeColor="White" Text="Physician Contact 2"></asp:Label>&#160;&#160;&#160;&#160; <asp:LinkButton ID="lnkCopyEAddress1" runat="server" Text="Copy Address" ForeColor="White"></asp:LinkButton></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEFirstname2" runat="server" Text="Firstname :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEFirstname2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblELastname2" runat="server" Text="Lastname :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtELastname2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEAddress2" runat="server" Text="Address :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEAddress2" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblECountry2" runat="server" Text="Country :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboECountry2" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblEState2" runat="server" Text="State :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboEState2" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblECity2" runat="server" Text="Post Town :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboECity2" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblECode2" runat="server" Text="Post Code :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboEZipcode2" runat="server" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblERegion2" runat="server" Text="Prov/Region :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtERegion2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEStreet2" runat="server" Text="Road/Street :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEStreet2" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEEState2" runat="server" Text="EState :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEEState2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEPlotNo2" runat="server" Text="Plot No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEPlotNo2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEMobile2" runat="server" Text="Mobile :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEMobile2" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEAltNo2" runat="server" Text="Alt. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEAltNo2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblETelNo2" runat="server" Text="Tel. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtETelNo2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEFax2" runat="server" Text="Fax :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEFax2" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEEmail2" runat="server" Text="Email :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEEMail2" runat="server"></asp:TextBox></td><td style="width:11%" align="left"></td><td style="width:22%" align="left"></td><td style="width:11%" align="left"></td><td style="width:22%" align="left"></td></tr><tr style="background-color:#667C93; width:100%"><td align="left" colspan="6"><asp:Label ID="lblPhysicianContact3" runat="server" Font-Bold="True" 
                                                ForeColor="White" Text="Physician Contact 3"></asp:Label>&#160;&#160;&#160;&#160; <asp:LinkButton ID="lnkCopyEAddress2" runat="server" Text="Copy Address" ForeColor="White"></asp:LinkButton></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEFirstname3" runat="server" Text="Firstname :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEFirstname3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblELastname3" runat="server" Text="Lastname :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtELastname3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEAddress3" runat="server" Text="Address :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEAddress3" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblECountry3" runat="server" Text="Country :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboECountry3" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblEState3" runat="server" Text="State :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboEState3" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblECity3" runat="server" Text="Post Town :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboECity3" runat="server" AutoPostBack="True" Width="155px"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblECode3" runat="server" Text="Post Code :"></asp:Label></td><td style="width:22%" align="left"><uc7:DropDownList ID="cboEZipcode3" runat="server" Width="155px"></uc7:DropDownList></td><td style="width:11%" align="left"><asp:Label ID="lblERegion3" runat="server" Text="Prov/Region :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtERegion3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEStreet3" runat="server" Text="Road/Street :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEStreet3" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEEState3" runat="server" Text="EState :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEEState3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEPlotNo3" runat="server" Text="Plot No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEPlotNo3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEMobile3" runat="server" Text="Mobile :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEMobile3" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEAltNo3" runat="server" Text="Alt. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEAltNo3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblETelNo3" runat="server" Text="Tel. No :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtETelNo3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"><asp:Label ID="lblEFax3" runat="server" Text="Fax :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEFax3" runat="server"></asp:TextBox></td></tr><tr style="width:100%"><td style="width:11%" align="left"><asp:Label ID="lblEEmail3" runat="server" Text="Email :"></asp:Label></td><td style="width:22%" align="left"><asp:TextBox ID="txtEEMail3" runat="server"></asp:TextBox></td><td style="width:11%" align="left"></td><td style="width:22%" align="left"></td><td style="width:11%" align="left"></td><td style="width:22%" align="left"></td></tr></table></asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="tapbAdditional" runat="server"  Width="100%" Height = "100%"><HeaderTemplate>Personal</HeaderTemplate>
                        <ContentTemplate><asp:Panel ID="pnlAdditional" runat="server" ScrollBars="Vertical" Height="220px"><table style="width:100%;" class="ContentCantroller"><tr style="background-color:#667C93; width:100%"><td colspan="4" style="width:50%" align="left"><asp:Label ID="lblBirthInfo" 
                                                runat="server" Text="Birth Info." Font-Bold="True" ForeColor="White"></asp:Label></td><td colspan="4" style="width:50%" align="left"><asp:Label ID="lblWorkPermit" 
                                                runat="server" Text="Work Permit" Font-Bold="True" ForeColor="White"></asp:Label></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblBirthCountry" runat="server" Text="Country :"></asp:Label></td><td style="width:13%" valign="middle">
                                <uc7:DropDownList ID="cboBirthCounty" runat="server" Width="120px" 
                                    AutoPostBack="True" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblBirthState" runat="server" Text="State :"></asp:Label></td><td style="width:13%" valign="middle">
                                <uc7:DropDownList ID="cboBirthState" runat="server" Width="120px" 
                                    AutoPostBack="True" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblWorkPermitNo" runat="server" Text="Permit No :"></asp:Label></td><td style="width:13%" valign="middle">
                                <asp:TextBox ID="txtWorkPermitNo" runat="server" Width="115px" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblWIssueDate" runat="server" Text="Issue Date :"></asp:Label></td><td style="width:13%" valign="middle">
                                <uc2:DateCtrl ID="dtIssueDate" runat="server" AutoPostBack="false" 
                                    ReadonlyDate="True" /></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblBirthCity" runat="server" Text="City :"></asp:Label></td><td style="width:13%" valign="middle">
                                <uc7:DropDownList ID="cboBirthCity" runat="server" Width="120px" 
                                    Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblBirthWard" runat="server" Text="Ward :"></asp:Label></td><td style="width:13%" valign="middle">
                                <asp:TextBox ID="txtBirthWard" runat="server" Width="115px" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblIssueCountry" runat="server" Text="Country :"></asp:Label></td><td style="width:13%" valign="middle">
                                <uc7:DropDownList ID="cboIssueCountry" runat="server" Width="120px" 
                                    Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblExpiryDate" runat="server" Text="Expiry Date :"></asp:Label></td><td style="width:13%" valign="middle">
                                <uc2:DateCtrl ID="dtExpiryDate" runat="server" AutoPostBack="false" 
                                    ReadonlyDate="True" /></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblBirthCertNo" runat="server" Text="Cert. No."></asp:Label></td><td style="width:13%" valign="middle">
                                <asp:TextBox ID="txtBirthCertNo" runat="server" Width="115px" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblBirthVillage" runat="server" Text="Village :"></asp:Label></td><td style="width:13%" valign="middle">
                                <asp:TextBox ID="txtBirthVillage" runat="server" Width="115px" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblIssuePlace" runat="server" Text="Issue Place :"></asp:Label></td><td style="width:13%" valign="middle">
                                <asp:TextBox ID="txtIssuePlace" runat="server" Width="115px" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle">&nbsp;</td><td style="width:13%" valign="middle">&nbsp;</td></tr><tr style="width:100%"><td valign="middle" colspan="8"><hr style="height:1px" /></td></tr><tr style="background-color:#667C93; width:100%"><td valign="middle" colspan="8"><asp:Label ID="lblOtherInfo" runat="server" Text ="Other Info." Font-Bold="True" ForeColor="White"></asp:Label></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblComplexion" runat="server" Text="Complexion :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboComplexion" runat="server" Width="120px" 
                                Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblTelExt" runat="server" Text="Tel. Ext. :"></asp:Label></td><td style="width:13%" valign="middle">
                            <asp:TextBox ID="txtExTel" runat="server" Width="115px" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblBldGrp" runat="server" Text="Blood Grp :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboBloodGrp" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblEyeColor" runat="server" Text="Eye Color :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboEyeColor" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblLanguage1" runat="server" Text="Language1 :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboLang1" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblLang2" runat="server" Text="Language2 :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboLang2" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblLang3" runat="server" Text="Language3 :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboLang3" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblLang4" runat="server" Text="Language4 :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboLang4" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblEthnicity" runat="server" Text="Ethnicity :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboEthnicity" runat="server" Width="120px" 
                                Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblNationality" runat="server" Text="Nationality :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboNationality" runat="server" Width="120px" 
                                Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblReligion" runat="server" Text="Religion :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboReligion" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblHair" runat="server" Text="Hair :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboHair" runat="server" Width="120px" Enabled="False"></uc7:DropDownList></td></tr><tr style="width:100%"><td style="width:12%" valign="middle"><asp:Label ID="lblHeight" runat="server" Text="Height :"></asp:Label></td><td style="width:13%" valign="middle">
                            <asp:TextBox ID="txtHeight" runat="server" Width="115px" 
                                onKeypress="return onlyNumbers();" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblWeight" runat="server" Text="Weight :"></asp:Label></td><td style="width:13%" valign="middle">
                            <asp:TextBox ID="txtWeight" runat="server" Width="115px" 
                                onKeypress="return onlyNum();" ReadOnly="True"></asp:TextBox></td><td style="width:12%" valign="middle"><asp:Label ID="lblMaritalStatus" runat="server" Text="Marital Status:"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc7:DropDownList ID="cboMaritalStatus" runat="server" Width="120px" 
                                Enabled="False"></uc7:DropDownList></td><td style="width:12%" valign="middle"><asp:Label ID="lblMarriedDate" runat="server" Text="Date :"></asp:Label></td><td style="width:13%" valign="middle">
                            <uc2:DateCtrl ID="dtMarriedDate" runat="server" ReadonlyDate="True" /></td></tr><tr style="width:100%"><td style="width:12%" valign="top"><asp:Label ID="lblAllergies" runat="server" Text="Allergies :"></asp:Label></td><td style="width:13%" valign="top" rowspan="8"><asp:Panel ID="pnlallergies" runat="server" ScrollBars="Auto" Height="111px" BorderStyle="Solid" BorderWidth="1px">
                                <asp:CheckBoxList ID="chkLstAllergies" runat="server" Height="111px" 
                                    Width="118px"  RepeatLayout="Flow" Enabled="False"></asp:CheckBoxList></asp:Panel></td><td style="width:12%" valign="top"><asp:Label ID="lblDisabilities" runat="server" Text="Disabilities :"></asp:Label></td><td style="width:13%" valign="top" rowspan="8"><asp:Panel ID="pnlDisabilities" runat="server" ScrollBars="Auto" Height="111px" BorderStyle="Solid" BorderWidth="1px">
                                <asp:CheckBoxList ID="chkLstDisabilities" runat="server" Height="111px" 
                                    Width="118px" RepeatLayout="Flow" Enabled="False"></asp:CheckBoxList></asp:Panel></td><td style="width:12%" valign="top"><asp:Label ID="lblSHOther" runat="server" Text="Sport/Hobbies :"></asp:Label></td><td valign="top" colspan="3" rowspan="8"><asp:TextBox ID="txtSpHob" 
                                                TextMode="MultiLine" runat="server" Height="111px" 
                                    Width="360px" ReadOnly="True"></asp:TextBox></td></td></tr><tr style="width:100%"><td style="width:12%" valign="middle">&#160;</td><td style="width:12%" valign="middle">&#160;</td><td style="width:12%" valign="middle">&#160;</td></tr><tr style="width:100%"><td style="width:12%" valign="middle">&#160;</td><td style="width:12%" valign="middle">&#160;</td><td style="width:12%" valign="middle">&#160;</td></tr><tr style="width:100%"><td style="width:12%" valign="middle">&#160;</td><td style="width:12%" valign="middle">&#160;</td><td style="width:12%" valign="middle">&#160;</td></tr></table></asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="tapbLeaveDetails" runat="server" Width="100%" Height="100%"><HeaderTemplate>Leave Details</HeaderTemplate>
                        <ContentTemplate><asp:Panel ID="pnlLeaveInfo" runat="server" ScrollBars="Vertical" Height="220px"><table style="width:100%;" class="ContentCantroller"><tr style="width:100%; background-color:#667C93"><td><asp:Label ID="lblLeaveInfo" runat="server" Text="Leave Info" ForeColor="White" 
                                                Font-Bold="True"></asp:Label></td></tr><tr style="width:100%"><td><asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" CaptionAlign="Left" HorizontalAlign="Left" PageSize="8" Width="856px" CellPadding="0"><Columns><asp:BoundField DataField="Leave" HeaderText="Leave"><HeaderStyle HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /></asp:BoundField><asp:BoundField DataField="StDate" HeaderText="Start Date"><HeaderStyle HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /></asp:BoundField><asp:BoundField DataField="EdDate" HeaderText="End Date"><HeaderStyle HorizontalAlign="Left" /><ItemStyle HorizontalAlign="Left" /></asp:BoundField><asp:BoundField DataField="Acc_Amt" HeaderText="Accrued Amount"><HeaderStyle HorizontalAlign="Right" /><ItemStyle HorizontalAlign="Right" /></asp:BoundField><asp:BoundField DataField="Iss_Amt" HeaderText="Issued Amount"><HeaderStyle HorizontalAlign="Right" /><ItemStyle HorizontalAlign="Right" /></asp:BoundField><asp:BoundField DataField="Balance" HeaderText="Balance"><HeaderStyle HorizontalAlign="Right" /><ItemStyle HorizontalAlign="Right" /></asp:BoundField></Columns></asp:GridView></td></tr></table></asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>                                        
                </cc1:TabContainer>
            </td>
        </tr>
        </table> --%>
        </div>
    </center>
</asp:Content>
