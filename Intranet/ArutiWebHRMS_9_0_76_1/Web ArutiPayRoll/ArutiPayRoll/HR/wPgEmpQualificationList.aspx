﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgEmpQualificationList.aspx.vb" Inherits="HR_wPgEmpQualificationList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Qualifications List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <%--<tr style="width: 100%">
                                            <td colspan="8">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td style="width: 7%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee : "></asp:Label>
                                            </td>
                                            <td style="width: 24%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblQualificationGroup" runat="server" Text="Qualification/Award Group :"></asp:Label>
                                            </td>
                                            <td style="width: 19%">
                                                <asp:DropDownList ID="cboQGrp" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblAwardDate" runat="server" Text="Start Date :"></asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <uc2:DateCtrl ID="dtAwarddtFrom" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 6%">
                                                <asp:Label ID="lblTo" runat="server" Text="Award Date :"></asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <uc2:DateCtrl ID="dtAwarddtTo" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 7%">
                                                <asp:Label ID="lblInstitute" runat="server" Text="Institute :"></asp:Label>
                                            </td>
                                            <td style="width: 24%">
                                                <asp:DropDownList ID="cboInstitute" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblQualification" runat="server" Text="Qualification/Award :"></asp:Label>
                                            </td>
                                            <td style="width: 19%">
                                                <asp:DropDownList ID="cboQualification" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblReferenceNo" runat="server" Text="Ref. No :"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 26%">
                                                <asp:TextBox ID="txtRefNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 32%" colspan="2">
                                                <asp:CheckBox ID="chkOtherQualification" runat="server" AutoPostBack="true" Text="Other Qualification / Short Course" />
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblOtherQualificationGrp" runat="server" Text="Other Qualif. Group "></asp:Label>
                                            </td>
                                            <td style="width: 19%">
                                                <asp:TextBox ID="txtOtherQGrp" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblOtherQualification" runat="server" Text="Other Qualif."></asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:TextBox ID="txtOtherQualification" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblOtherInstitute" runat="server" Text="Other Inst."></asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:TextBox ID="txtOtherInstitute" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" CssClass="btndefault" Text="New" Visible="False" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        
                                        
                                        <%--'Gajanan [17-DEC-2018] -- Start--%>
                                        <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                        <%--  <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail"
                                            Style="float: left" />
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                            Style="padding: 2px; font-weight: bold; float: left; margin-top: 5px; margin-left: 10px;"></asp:Label>--%>
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                            <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                        <%--'Gajanan [17-DEC-2018] -- End--%>
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_gdView" runat="server" ScrollBars="Auto" Height="300px">
                                    <asp:DataGrid ID="dgView" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="EditImg" runat="server" CssClass="gridedit" CommandName="Select"
                                                            ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                    <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                        Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                        
                                                    <%--'Gajanan [17-DEC-2018] -- End--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<0>--%>
                                            <asp:TemplateColumn>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" CommandName="Delete" ImageUrl="~/images/remove.png"
                                                        ToolTip="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<1>--%>
                                            <asp:BoundColumn HeaderText="Employee" ReadOnly="True" DataField="EmpName" FooterText="colhEmployee">
                                            </asp:BoundColumn>
                                            <%--<2>--%>
                                            <asp:BoundColumn HeaderText="Qualification Group" ReadOnly="True" DataField="QGrp"
                                                FooterText="colhQualifyGroup"></asp:BoundColumn>
                                            <%--<3>--%>
                                            <asp:BoundColumn HeaderText="Qualification" ReadOnly="True" DataField="Qualify" FooterText="colhQualification">
                                            </asp:BoundColumn>
                                            <%--<4>--%>
                                            <asp:BoundColumn HeaderText="Date" ReadOnly="True" DataField="StDate" FooterText="colhAwardDate">
                                            </asp:BoundColumn>
                                            <%--<5>--%>
                                            <asp:BoundColumn HeaderText="End Date" ReadOnly="True" DataField="EnDate" FooterText="colhAwardToDate">
                                            </asp:BoundColumn>
                                            <%--<6>--%>
                                            <asp:BoundColumn HeaderText="Institute" ReadOnly="True" DataField="Institute" FooterText="colhInstitute">
                                            </asp:BoundColumn>
                                            <%--<7>--%>
                                            <asp:BoundColumn HeaderText="Ref. No" ReadOnly="True" DataField="RefNo" FooterText="colhRefNo">
                                            </asp:BoundColumn>
                                            <%--<8>--%>
                                            <asp:BoundColumn HeaderText="QualifyId" ReadOnly="True" DataField="QualifyId" Visible="false">
                                            </asp:BoundColumn>
                                            <%--<9>--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <%--<10>--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                            </asp:BoundColumn>
                                            <%--<11>--%>
                                            <%--'Gajanan [17-DEC-2018] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                            <asp:BoundColumn DataField="QulifyTranId" HeaderText="QulifyTranId" Visible="false"
                                                FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                        </asp:BoundColumn>
                                            <%--<12>--%>
                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <uc4:DeleteReason ID="DeleteReason1" runat="server" Title="Are you sure you want to delete?"
                        ErrorMessage="Sorry! Reason can not be blank. Please enter delete reason." ErrorControlWidth="300" />
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
