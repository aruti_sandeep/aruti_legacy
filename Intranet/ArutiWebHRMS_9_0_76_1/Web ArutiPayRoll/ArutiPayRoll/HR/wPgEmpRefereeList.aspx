﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgEmpRefereeList.aspx.vb" Inherits="HR_wPgEmpRefereeList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Reference List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="6">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" Visible=false/>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" Text="Employee" runat="server" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPostCountry" Text="Country" runat="server" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpCountry" runat="server" ></asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblIDNo" Text="Company" runat="server" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtCompany" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblRefereeName" Text="Referee" runat="server" />
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:TextBox ID="TxtReferee" runat="server" />
                                            </td>
                                            <td colspan="3" style="width:55%"></td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" CssClass="btndefault" runat="server" Text="New" Visible="False" />
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                        <asp:Button ID="BtnReset" CssClass="btndefault" runat="server" Text="Reset" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_gvReferee" runat="server" ScrollBars="Auto">
                                    <asp:DataGrid ID="gvReferee" runat="server" AutoGenerateColumns="false" Width="99%"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Select"
                                                            CommandName="Select"></asp:LinkButton>
                                                        <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                        <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                            Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                        <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                        <%--'Gajanan [17-DEC-2018] -- End--%>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" />--%>
                                                </ItemTemplate>
                                                <%--<0>--%>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" />--%>
                                                </ItemTemplate>
                                                <%--<1>--%>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                            </asp:BoundColumn>
                                            <%--<2>--%>
                                            <asp:BoundColumn DataField="RefName" HeaderText="Referee" ReadOnly="True" FooterText="colhRefreeName">
                                            </asp:BoundColumn>
                                            <%--<3>--%>
                                            <asp:BoundColumn DataField="Country" HeaderText="Country" ReadOnly="True" FooterText="colhCountry">
                                            </asp:BoundColumn>
                                            <%--<4>--%>
                                            <asp:BoundColumn DataField="Company" HeaderText="Company" ReadOnly="True" FooterText="colhIdNo">
                                            </asp:BoundColumn>
                                            <%--<5>--%>
                                            <asp:BoundColumn DataField="Email" HeaderText="Email" ReadOnly="True" FooterText="colhEmail">
                                            </asp:BoundColumn>
                                            <%--<6>--%>
                                            <asp:BoundColumn DataField="telephone_no" HeaderText="Tel. No" ReadOnly="True" FooterText="colhTelNo">
                                            </asp:BoundColumn>
                                            <%--<7>--%>
                                            <asp:BoundColumn DataField="mobile_no" HeaderText="Mobile" ReadOnly="True" FooterText="colhMobile">
                                            </asp:BoundColumn>
                                            <%--<8>--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <%--<9>--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                            </asp:BoundColumn>
                                            <%--<10>--%>
                                            <%--'Gajanan [17-DEC-2018] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                            <asp:BoundColumn DataField="RefreeTranId" HeaderText="RefreeTranId" Visible="false"
                                                FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                            <%--<11>--%>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                            </asp:BoundColumn>
                                            <%--<12>--%>
                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                        </Columns>
                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are ou Sure You Want To delete?:" />
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
