<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ClaimRetirementList.aspx.vb"
    Inherits="Claim_Retirement_wPg_ClaimRetirementList" Title="Retirement List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Retirement List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 8%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblExpenseCategory" runat="server" Text="Expense Category"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:DropDownList ID="cboExpenseCategory" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 8%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 8%">
                                            <asp:Label ID="lblDateFrom" runat="server" Text="Date from"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <datectrl:uc ID="dtpDatefrom" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblDateto" runat="server" Text="Date To"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <datectrl:uc ID="dtpDateto" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <asp:Panel ID="pnlRetirementList" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                        <asp:GridView ID="gvRetirement" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="true" PageSize = "500" 
                                            PagerStyle-HorizontalAlign="Left" PagerSettings-Position="Top"  PagerStyle-Wrap="true" 
                                            DataKeyNames="IsGrp,crmasterunkid,claimretirementunkid,employeeunkid,expensetypeid,statusunkid,issubmit_approval,employeename,Approvaldate"
                                            HeaderStyle-Font-Bold="false" Width="99%">
                                             <%--'Pinkal (03-Mar-2021)-- KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance. [Approvaldate]--%>
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="25px">
                                                    <ItemTemplate>
                                                         <span class="gridiconbc" style="padding:0 10px; display:block">
                                                                <asp:LinkButton ID="lnkRetire" runat="server" CssClass="fa fa-list-alt iconsize" ToolTip = "Retire" CommandName ="Retire" > 
                                                                </asp:LinkButton>
                                                         </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="25px">
                                                    <ItemTemplate  >
                                                         <span class="gridiconbc" style="padding:0 10px; display:block">
                                                            <asp:LinkButton ID="lnkedit" runat="server"  CssClass="gridedit" ToolTip = "Edit" CommandName ="Edit">
                                                            </asp:LinkButton>
                                                         </span>   
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="claimretirementno"  HeaderText ="Retirement No." FooterText="dgcolhRetirementNo"  ItemStyle-Width="100px"/>
                                                <asp:BoundField DataField="claimrequestno"  HeaderText ="Claim No." FooterText="dgcolhClaimNo" Visible = "false"/>
                                                <asp:BoundField DataField ="expensetype"  HeaderText ="Expense Category" FooterText="dgcolhExpenseCategory"/>
                                                <asp:BoundField DataField ="employeename"  HeaderText ="Employee" FooterText="dgcolhEmployee" ItemStyle-Width="210px" Visible="false"/>
                                                <asp:BoundField DataField = "transactiondate" HeaderText ="Date" FooterText="dgcolhtransactionDate"/>
                                                <asp:BoundField DataField = "approved_amount"  HeaderText ="Approved Amount" FooterText="dgcolhApprovedAmount" ItemStyle-HorizontalAlign="Right"/>
                                                <asp:BoundField DataField  = "retirement_amount" HeaderText ="Retired  Amount" FooterText="dgcolhRetiredAmount" ItemStyle-HorizontalAlign="Right"/>
                                                <asp:BoundField DataField = "balance"  HeaderText ="Balance" FooterText="dgcolhBalance" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField  DataField = "status" HeaderText ="Status" FooterText="dgcolhStatus"/>
                                            </Columns>
                                        </asp:GridView>
                                      
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </center>
</asp:Content>
