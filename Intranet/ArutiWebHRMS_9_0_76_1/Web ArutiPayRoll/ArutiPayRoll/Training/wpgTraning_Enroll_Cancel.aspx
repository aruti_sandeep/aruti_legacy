<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wpgTraning_Enroll_Cancel.aspx.vb" Inherits="wpgTraning_Enroll_Cancel"
    Title="Enrollment And Cancellation List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Enrollment And Cancellation List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%;">
                                            <td style="width: 60%;">
                                                <table style="width: 100%;">
                                                    <tr style="width: 100%;">
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%;">
                                                            <asp:DropDownList ID="cboBranch" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 15%">
                                                            <asp:Label Text="Course Title" runat="server" ID="lblCourse"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:DropDownList runat="server" ID="cboCourseTitle" />
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                                        </td>
                                                        <td colspan="3" style="width: 85%">
                                                            <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 40%;">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%;">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblEnrollmentdateFrom" runat="server" Text="Enroll Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <uc2:DateCtrl ID="dtApplyDate" runat="server" AutoPostBack="False" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:Label ID="lblEnrollmentDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 35">
                                                            <uc2:DateCtrl ID="dtApplyDateTo" runat="server" AutoPostBack="False" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblCancellationDateFrom" runat="server" Width="100%" Text="Cancel Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <uc2:DateCtrl ID="dtCancelDate" runat="server" AutoPostBack="False" />
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:Label ID="lblCancellationDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <uc2:DateCtrl ID="dtCancelDateTo" runat="server" AutoPostBack="False" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td colspan="4" style="width: 100%;">
                                                            <asp:RadioButtonList ID="radlstOperation" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="1">Cancelled</asp:ListItem>
                                                                <asp:ListItem Value="2">Void</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="3">Show All</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" CssClass=" btnDefault" Text="Search" Width="85px" />
                                        <asp:Button ID="btnReset" runat="server" CssClass=" btnDefault" Text="Reset" Width="85px" />
                                    </div>
                                </div>
                                <asp:Panel ID="pbl_dgView" runat="server" Height="350px" ScrollBars="Auto">
                                    <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" Width="99%"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:BoundColumn DataField="Title" HeaderText="Course Title" ReadOnly="True" />
                                            <asp:BoundColumn DataField="Provider" HeaderText="Institute Provider" ReadOnly="True" />
                                            <asp:BoundColumn DataField="Venue" HeaderText="Venue" ReadOnly="True" />
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee" />
                                            <asp:BoundColumn DataField="EnrollDate" HeaderText="Enroll Date" ReadOnly="True"
                                                FooterText="colhEnrollDate" />
                                            <asp:BoundColumn DataField="CancelDate" HeaderText="Cancel Date" ReadOnly="True"
                                                FooterText="colhCancelDate" />
                                            <asp:BoundColumn DataField="STATUS" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                            <asp:BoundColumn HeaderText="Award Status" ReadOnly="true" FooterText="colhAwardStatus" />
                                            <asp:BoundColumn DataField="enroll_remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark" />
                                            <asp:BoundColumn DataField="isqualificaionupdated" HeaderText="Qua. Updated" ReadOnly="True"
                                                FooterText="colhUpdateQuali" />
                                            <asp:BoundColumn DataField="operationmodeid" HeaderText="operationmodeid" ReadOnly="True"
                                                Visible="false" />
                                        </Columns>
                                        <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
