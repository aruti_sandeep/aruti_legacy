<%@ Page Title="Transfer Leave Approver" Language="VB" MasterPageFile="~/home.master"  AutoEventWireup="false" CodeFile="Leave_Approver_Migration.aspx.vb" Inherits="Leave_Approver_Migration" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
	         if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
                    $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                  
            }
        }

        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        $("[id*=chkHeder2]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkbox2]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });


        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtFrmSearch').val().length > 0) {
                $('#<%= dgvFrmEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvFrmEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvFrmEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtFrmSearch').val() + '\')').parent().show();
            }
            else if ($('#txtFrmSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvFrmEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtFrmSearch').val('');
            $('#<%= dgvFrmEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtFrmSearch').focus();
        }


        function ToSearching() {
            if ($('#txtToSearch').val().length > 0) {
                $('#<%= dgvToEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvToEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvToEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtToSearch').val() + '\')').parent().show();
            }
            else if ($('#txtToSearch').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= dgvToEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtToSearch').val('');
            $('#<%= dgvToEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtToSearch').focus();
        }
        
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Transfer Leave Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Approver Information"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                    
                                      <%--'Pinkal (14-May-2020) -- Start
                                      'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.--%>
                                    <tr style="width: 100%">
                                            <td style="width: 15%">
                                            </td>
                                            <td style="width: 33%;">
                                                <asp:CheckBox ID = "chkShowInactiveApprovers" runat = "server" Text = "Show Inactive Approver(s)" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 4%;">
                                            </td>
                                            <td style="width: 15;">
                                            </td>
                                            <td style="width: 33%">
                                               <asp:CheckBox ID = "chkShowInActiveEmployees" runat = "server" Text = "Show Inactive Employee(s)" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                    <%--'Pinkal (14-May-2020) -- End   --%>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFromApprover" runat="server" Text="From Approver"></asp:Label>
                                            </td>
                                            <td style="width: 33%;">
                                                <asp:DropDownList ID="cboFrmApprover" runat="server" AutoPostBack="True" />
                                            </td>
                                            <td style="width: 4%;" rowspan="4">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="objbtnAssign" runat="server" Text=">>" Width="40" CssClass="btnDefault" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" Width="40" CssClass="btnDefault" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 15;">
                                                <asp:Label ID="lblToApprover" runat="server" Text="To Approver"></asp:Label>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:DropDownList ID="cboToApprover" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFrmLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:DropDownList ID="cboFrmLevel" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%;">
                                                <asp:Label ID="lblToLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboToLevel" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; padding-right: 2%">
                                            <td style="width: 48%" colspan="2">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 75%">
                                                        <%--'Pinkal (14-May-2020) -- Start
                                                                'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                                                               <asp:TextBox ID="txtFrmSearch" runat="server" AutoPostBack="true"></asp:TextBox>--%>
                                                               <input type="text" id="txtFrmSearch" name="txtSearch" placeholder = "Type To Search Text"  maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                               <%--'Pinkal (14-May-2020) -- End --%>
                                            </td>
                                                        <td style="width: 15%; text-align:right; font-weight:bold">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" Style="vertical-align: middle;
                                                                " Text="Allocation" CssClass="lnkhover"></asp:LinkButton>
                                                        </td>
                                                        <td style="width: 10%;text-align:right; font-weight:bold">
                                                            <asp:LinkButton ID="lnkReset" runat="server" Style="vertical-align: middle;"
                                                                Text="Reset" CssClass="lnkhover"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 48%;" colspan="2">
                                              <%--'Pinkal (14-May-2020) -- Start
                                                       'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                                                <asp:TextBox ID="txtToSearch" runat="server" AutoPostBack="true"></asp:TextBox>  --%>
                                                <input type="text" id="txtToSearch" name = "txtSearch"  placeholder = "Type To Search Text"  maxlength="50" style="height: 25px; font: 100" onkeyup="ToSearching();" />
                                                <%--'Pinkal (14-May-2020) -- End --%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 48%" colspan="2">
                                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                    height: 350px; overflow: auto; border: solid 1px #DDD">
                                                    <asp:GridView ID="dgvFrmEmp" runat="server" AutoGenerateColumns="False" Width="99%"
                                                        ShowFooter="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="approverunkid,leaveapprovertranunkid,leaveapproverunkid,employeeunkid,employeecode,employeename">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                 <%--'Pinkal (14-May-2020) -- Start
                                                                        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                                                                    <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />--%>
                                                                    <asp:CheckBox ID="chkHeder1" runat="server" />
                                                                    <%--'Pinkal (14-May-2020) -- End  --%>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <%--'Pinkal (14-May-2020) -- Start
                                                                        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                                                                    <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged"
                                                                        CommandArgument='<%# Container.DataItemIndex %>' AutoPostBack="true" />--%>
                                                                        <asp:CheckBox ID="chkbox1" runat="server" />
                                                                 <%--'Pinkal (14-May-2020) -- End  --%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgEmployeecode" />
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgEmployee" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                            <td style="width: 48%" colspan="2">
                                                <table style="width: 100%; vertical-align: top">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:MultiView ActiveViewIndex="0" ID="mltiview" runat="server">
                                                                <asp:View ID="vwStep1" runat="server">
                                                                    <table width="100%">
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 100%; border-radius: 0px" class="grpheader">
                                                                                <h4>
                                                                                    <asp:Label ID="lblCaption1" runat="server" Text="Migrated Employee"></asp:Label>
                                                                                </h4>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 100%;">
                                                                                <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                                                                    height: 350px; overflow: auto; border: solid 1px #DDD">
                                                                                    <asp:GridView ID="dgvToEmp" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                        ShowFooter="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="approverunkid,leaveapprovertranunkid,leaveapproverunkid,employeeunkid,employeecode,employeename">
                                                                                        <Columns>
                                                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                                                <HeaderTemplate>
                                                                                                <%--'Pinkal (14-May-2020) -- Start
                                                                                                        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                                                                                                    <asp:CheckBox ID="chkHeder2" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder2_CheckedChanged" /> --%>
                                                                                                    <asp:CheckBox ID="chkHeder2" runat="server" />
                                                                                                    <%--'Pinkal (14-May-2020) -- End  --%>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                <%--'Pinkal (14-May-2020) -- Start
                                                                                                        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                                                                                                    <asp:CheckBox ID="chkbox2" runat="server" Enabled="true" OnCheckedChanged="chkbox2_CheckedChanged"
                                                                                                        CommandArgument='<%# Container.DataItemIndex %>' AutoPostBack="true" />--%>
                                                                                                        <asp:CheckBox ID="chkbox2" runat="server" />
                                                                                                        <%--'Pinkal (14-May-2020) -- End  --%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgMigratedEmpCode" />
                                                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgMigratedEmp" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vwStep2" runat="server">
                                                                    <table id="tblStp2" runat="server" width="100%">
                                                                        <tr style="width: 100%; background: steelblue;">
                                                                            <td style="width: 100%;" class="grpheader">
                                                                                <asp:Label ID="lblCaption2" runat="server" Text="Assigned Employee"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 100%;">
                                                                                <asp:Panel ID="pnl_dgvAssignedEmp" runat="server" ScrollBars="Auto" Height="350px">
                                                                                    <asp:GridView ID="dgvAssignedEmp" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                        ShowFooter="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgAssignEmpCode" />
                                                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgAssignEmployee" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100%; text-align: right">
                                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnDefault" />
                                                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnDefault" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
