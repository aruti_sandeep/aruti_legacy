﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Leave_wPg_LeavePlanner_AddEdit
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmleaveplanner_AddEdit"
    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeavePlanner As New clsleaveplanner
    'Pinkal (11-Sep-2020) -- End

    Private mintEmployeeId As Integer = -1 'FOR ESS
    Private mstrAdvanceFilter As String = String.Empty
    Private mintLeavePlannerUnkid As Integer = -1
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Private mdtEmployee As DataTable = Nothing


    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.
    Dim dsPlannerFraction As DataSet = Nothing
    'Pinkal (30-Jun-2016) -- End


#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End`

                mintLeavePlannerUnkid = CInt(Session("LeavePlannerUnkid")) 'FOR EDIT MODE
                mintEmployeeId = CInt(Session("Employeeunkid")) 'FOR ESS

                If mintEmployeeId <= 0 Then
                    pnl_Filter.Enabled = True
                Else
                    pnl_Filter.Enabled = False
                End If

                Call FillCombo()
                Call GetValue()

                If mintLeavePlannerUnkid > 0 OrElse mintEmployeeId > 0 Then
                    Call FillList()
                Else
                    dgvEmployee.DataSource = New List(Of String)
                    dgvEmployee.DataBind()
                    lblEmployeeCount.Text = Language.getMessage(mstrModuleName, 7, "Employee Count : ") & "0"
                End If

                dgvEmployee.Columns(3).Visible = True
                dgvEmployee.Columns(4).Visible = False
                dgvEmployee.Columns(5).Visible = False
                dgvEmployee.Columns(6).Visible = False

                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                lblValue.Text = dtpFromDate.GetDate.Date & " - " & dtpToDate.GetDate.Date

            Else
                mintEmployeeId = CInt(Me.ViewState("EmployeeId"))
                mintLeavePlannerUnkid = CInt(Me.ViewState("LeavePlannerUnkid"))
                mdtFromDate = CDate(Me.ViewState("FromDate"))
                mdtToDate = CDate(Me.ViewState("ToDate"))
                mdtEmployee = CType(Me.ViewState("mdsEmployee"), DataTable)
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
                dsPlannerFraction = CType(Me.ViewState("PlannerDayFraction"), DataSet)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("EmployeeId") = mintEmployeeId
            Me.ViewState("LeavePlannerUnkid") = mintLeavePlannerUnkid
            Me.ViewState("FromDate") = mdtFromDate
            Me.ViewState("ToDate") = mdtToDate
            Me.ViewState("mdsEmployee") = mdtEmployee
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            Me.ViewState("PlannerDayFraction") = dsPlannerFraction
            'Pinkal (30-Jun-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsFill As DataSet = Nothing
            Dim dtFill As DataTable = Nothing
            Dim objMaster As New clsMasterData

            Dim objLeaveType As New clsleavetype_master

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsFill = objLeaveType.GetList("LeaveType", True, True, "ISNULL(isshowoness,1) = 1")
            Else
                dsFill = objLeaveType.GetList("LeaveType", True, True, "")
            End If
            'Pinkal (25-May-2019) -- End


            dtFill = New DataView(dsFill.Tables("LeaveType"), "ispaid=1", "", DataViewRowState.CurrentRows).ToTable

            Dim drRow As DataRow = Nothing
            drRow = dtFill.NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = Language.getMessage(mstrModuleName, 3, "Select")
            dtFill.Rows.InsertAt(drRow, 0)

            With cboLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "leavename"
                .DataSource = dtFill
                .DataBind()
                .SelectedValue = Str(0)
            End With

            dsFill = objMaster.getGenderList("Gender", True)
            With cboGender
                .DataTextField = "Name"
                .DataValueField = "id"
                .DataSource = dsFill.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsFill = objMaster.GetCondition(True)
            dsFill = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            With cboFromcondition
                .DataTextField = "Name"
                .DataValueField = "id"
                .DataSource = dsFill.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            With cboTocondition
                .DataTextField = "Name"
                .DataValueField = "id"
                .DataSource = dsFill.Tables(0).Copy
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtFill IsNot Nothing Then dtFill.Clear()
            dtFill = Nothing
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            objLeaveType = Nothing
            objMaster = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim dsList As DataSet = Nothing
            Dim strSearching As String = ""
            Dim objEmployee As New clsEmployee_Master
            Dim dtFromDate As Date = Nothing
            Dim dtToDate As Date = Nothing
            Dim blnReinstatement As Boolean = False

            If radAppointedDate.Checked = True Then
                dtFromDate = dtpFromDate.GetDate.Date
                dtToDate = dtpToDate.GetDate.Date
            Else
                dtFromDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                dtToDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                blnReinstatement = True
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                strSearching &= "AND hremployee_master.gender = " & CInt(cboGender.SelectedValue) & " "
            End If

            If radExpYear.Checked = True Then
                If CInt(txtFromYear.Text) > 0 Or CInt(txtFromMonth.Text) > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.SelectedItem.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If CInt(txtToYear.Text) > 0 Or CInt(txtToMonth.Text) > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.SelectedItem.Text & " '" & eZeeDate.convertDate(mdtFromDate) & "' "
                End If
            End If

            If radProbationDate.Checked Then
                strSearching &= "AND Convert(char(8),EPROB.probation_from_date,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' AND Convert(char(8),EPROB.probation_to_date,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If radConfirmationDate.Checked Then
                strSearching &= "AND Convert(char(8),ECNF.confirmation_date,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' AND Convert(char(8),ECNF.confirmation_date,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If


            Dim intEmployeeId As Integer
            Dim blnAllowAccessFilter As Boolean
            If mintEmployeeId > 0 Then 'FOR ESS
                intEmployeeId = mintEmployeeId
                blnAllowAccessFilter = False
            ElseIf mintLeavePlannerUnkid > 0 Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objLeavePlanner As New clsleaveplanner
                'Pinkal (11-Sep-2020) -- End
                objLeavePlanner._Leaveplannerunkid = mintLeavePlannerUnkid
                intEmployeeId = objLeavePlanner._Employeeunkid
                blnAllowAccessFilter = True
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objLeavePlanner = Nothing
                'Pinkal (11-Sep-2020) -- End
            End If

            dsList = objEmployee.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                         dtFromDate, dtToDate, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                         "Employee", CBool(Session("ShowFirstAppointmentDate")), intEmployeeId, , strSearching, blnReinstatement, blnAllowAccessFilter)


            'If mintLeavePlannerUnkid <= 0 Then 'NEW MODE
            '    dsList = objEmployee.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                 dtFromDate, dtToDate, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                 "Employee", CBool(Session("ShowFirstAppointmentDate")), , , strSearching, blnReinstatement)
            'Else
            '    objLeavePlanner = New clsleaveplanner
            '    Dim intEmployeeId As Integer
            '    Dim blnAllowAccessFilter As Boolean

            '    If mintEmployeeId > 0 Then 'FOR ESS
            '        intEmployeeId = mintEmployeeId
            '        blnAllowAccessFilter = False
            '    Else 'FOR EDIT MODE
            '        objLeavePlanner._Leaveplannerunkid = mintLeavePlannerUnkid
            '        intEmployeeId = objLeavePlanner._Employeeunkid
            '        blnAllowAccessFilter = True
            '    End If

            '    dsList = objEmployee.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                 dtFromDate, dtToDate, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                 "Employee", CBool(Session("ShowFirstAppointmentDate")), intEmployeeId, , strSearching, blnReinstatement, blnAllowAccessFilter)
            'End If

            mdtEmployee = dsList.Tables("Employee")

            If mdtEmployee.Columns.Contains("ischecked") = False Then
                mdtEmployee.Columns.Add("ischecked", Type.GetType("System.Boolean"))
                mdtEmployee.Columns("ischecked").DefaultValue = False
            End If

            dgvEmployee.AutoGenerateColumns = False
            dgvEmployee.DataSource = mdtEmployee
            dgvEmployee.DataBind()


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            If mintEmployeeId > 0 AndAlso mdtEmployee IsNot Nothing AndAlso mdtEmployee.Rows.Count > 0 Then 'FOR ESS
                Dim chk As CheckBox = CType(dgvEmployee.Controls(0).Controls(0).FindControl("ChkAll"), CheckBox) 'THIS IS USED TO FIND HEADER ROW CONTROL.
                chk.Checked = True
                ChkAll_CheckedChanged(chk, New EventArgs())
            End If
            'Pinkal (26-Dec-2018) -- End

            If radExpYear.Checked = True Then
                dgvEmployee.Columns(3).Visible = True
                dgvEmployee.Columns(4).Visible = False
                dgvEmployee.Columns(5).Visible = False
                dgvEmployee.Columns(6).Visible = False

            ElseIf radAppointedDate.Checked = True Then
                dgvEmployee.Columns(3).Visible = True
                dgvEmployee.Columns(4).Visible = False
                dgvEmployee.Columns(5).Visible = False
                dgvEmployee.Columns(6).Visible = False

            ElseIf radProbationDate.Checked = True Then
                dgvEmployee.Columns(3).Visible = False
                dgvEmployee.Columns(4).Visible = False
                dgvEmployee.Columns(5).Visible = True
                dgvEmployee.Columns(6).Visible = True

            ElseIf radConfirmationDate.Checked = True Then
                dgvEmployee.Columns(3).Visible = False
                dgvEmployee.Columns(4).Visible = True
                dgvEmployee.Columns(5).Visible = False
                dgvEmployee.Columns(6).Visible = False

            End If


            lblEmployeeCount.Text = Language.getMessage(mstrModuleName, 7, "Employee Count : ") & dgvEmployee.Items.Count

            If radAppointedDate.Checked OrElse radConfirmationDate.Checked OrElse radProbationDate.Checked Then
                lblValue.Text = dtpFromDate.GetDate.Date & " - " & dtpToDate.GetDate.Date
            Else
                If mdtFromDate.Date = Nothing Then mdtFromDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If mdtToDate.Date = Nothing Then mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date
                lblValue.Text = mdtFromDate.Date & " - " & mdtToDate.Date
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeavePlanner = New clsleaveplanner
            Dim objLeavePlanner As New clsleaveplanner
            'Pinkal (11-Sep-2020) -- End

            objLeavePlanner._Leaveplannerunkid = mintLeavePlannerUnkid
            cboLeaveType.SelectedValue = CStr(objLeavePlanner._Leavetypeunkid)

            If mintLeavePlannerUnkid > 0 Then
                dtpStartDate.SetDate = objLeavePlanner._Startdate.Date
                dtpStopDate.SetDate = objLeavePlanner._Stopdate.Date

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                Dim objPlannerFraction As New clsplannerday_fraction
                dsPlannerFraction = objPlannerFraction.GetList("List", mintLeavePlannerUnkid, True)
                objPlannerFraction = Nothing
                'Pinkal (30-Jun-2016) -- End
            Else
                If CDate(Session("fin_enddate").ToString).Date < ConfigParameter._Object._CurrentDateAndTime.Date.Date Then
                    dtpStartDate.SetDate = CDate(Session("fin_enddate").ToString).Date
                    dtpStopDate.SetDate = CDate(Session("fin_enddate").ToString).Date
                ElseIf CDate(Session("fin_enddate").ToString).Date > ConfigParameter._Object._CurrentDateAndTime.Date.Date Then
                    dtpStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date.Date
                    dtpStopDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date.Date
                End If
            End If

            txtRemarks.Text = objLeavePlanner._Remarks


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeavePlanner = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub SetValue()
    Private Sub SetValue(ByRef objLeavePlanner As clsleaveplanner)
        'Pinkal (11-Sep-2020) -- End
        Try
            objLeavePlanner = New clsleaveplanner

            objLeavePlanner._Leaveplannerunkid = mintLeavePlannerUnkid
            objLeavePlanner._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            objLeavePlanner._Startdate = dtpStartDate.GetDate.Date
            objLeavePlanner._Stopdate = dtpStopDate.GetDate.Date
            objLeavePlanner._Remarks = txtRemarks.Text.Trim

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            Dim dtTable As DataTable = New DataView(mdtEmployee, "ischecked = True", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "employeecode", "name")
            Dim dsList As New DataSet
            dsList.Tables.Add(dtTable)
            Dim xmlStr As String = GetXML(dsList)
            Dim objPlannerFraction As New clsplannerday_fraction
            Dim dtEmployeeList As DataTable = objPlannerFraction.GetPlannerDayFraction(xmlStr, CInt(cboLeaveType.SelectedValue), dtpStartDate.GetDate.Date, dtpStopDate.GetDate.Date)

            If mintLeavePlannerUnkid > 0 Then
                Dim emplist = dtEmployeeList.AsEnumerable
                Dim empPlannerList = dsPlannerFraction.Tables(0).AsEnumerable

                'RIGHT JOIN WITH PLANNER LIST TO DEFAULT GENERATE DATATABLE AND VOID NULL ROW FROM PLANNER LIST DATATABLE
                Dim rows = From r1 In empPlannerList Group Join r2 In emplist On r1.Field(Of Integer)("employeeunkid") Equals r2.Field(Of Integer)("employeeunkid") _
                                 And r1.Field(Of String)("leavedate") Equals r2.Field(Of String)("leavedate") Into cnt = Group _
                                 From dr In cnt.DefaultIfEmpty() Where dr Is Nothing OrElse dr Is DBNull.Value Select r1

                rows.ToList.ForEach(Function(x) objPlannerFraction.UpdateRow(x, "D"))
                dsPlannerFraction.AcceptChanges()

                'JOIN WITH DEFAULT GENERATE LIST TO PLANNER LIST DATATABLE AND DELETE COMMON ROW FROM DEFAULT GENERATED LIST. 
                Dim rows1 = From r1 In emplist Join r2 In empPlannerList On r1.Field(Of Integer)("employeeunkid") Equals r2.Field(Of Integer)("employeeunkid") _
                                   And r1.Field(Of String)("leavedate") Equals r2.Field(Of String)("leavedate") Where r1.Field(Of String)("AUD") <> "D" AndAlso r1.Field(Of String)("AUD") <> "" _
                                   Select r1


                rows1.ToList.ForEach(Function(x) objPlannerFraction.DeleteRow(x))
                dtEmployeeList.AcceptChanges()
                dtEmployeeList = dtEmployeeList.AsEnumerable.Union(rows.ToList().AsEnumerable()).CopyToDataTable

            End If

            objLeavePlanner._PlannerFraction = dtEmployeeList

            If CInt(Session("Employeeunkid")) > 0 Then
                objLeavePlanner._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            Else
                objLeavePlanner._Userunkid = CInt(Session("UserId"))
            End If

            objLeavePlanner._WebFormName = "frmleaveplanner_AddEdit"
            objLeavePlanner._WebClientIP = CStr(Session("IP_ADD"))
            objLeavePlanner._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuLeaveInformation"

            'Pinkal (30-Jun-2016) -- End 


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboLeaveType.SelectedValue = CStr(0)
            cboGender.SelectedValue = CStr(0)
            txtFromYear.Text = "0"
            txtFromMonth.Text = "0"
            txtToYear.Text = "0"
            txtToMonth.Text = "0"
            cboFromcondition.SelectedValue = CStr(0)
            cboTocondition.SelectedValue = CStr(0)
            mstrAdvanceFilter = ""
            radExpYear.Checked = False
            radAppointedDate.Checked = False
            radProbationDate.Checked = False
            radConfirmationDate.Checked = False
            pnl_FromYear.Enabled = True
            pnl_ToYear.Enabled = True
            pnl_Date.Enabled = True

            lblValue.Text = dtpFromDate.GetDate.Date & " - " & dtpToDate.GetDate.Date
            lblEmployeeCount.Text = Language.getMessage(mstrModuleName, 7, "Employee Count : ") & "0"

            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
            dgvEmployee.DataSource = mdtEmployee
            dgvEmployee.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If radAppointedDate.Checked = True Then
                If dtpToDate.GetDate.Date < dtpFromDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, To date cannot ne less than From date."), Me)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (CInt(txtFromYear.Text) > 0 Or CInt(txtFromMonth.Text) > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "From Condition is compulsory information.Please Select From Condition."), Me)
                    cboFromcondition.Focus()
                    Exit Sub
                ElseIf (CInt(txtToYear.Text) > 0 Or CInt(txtToMonth.Text) > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "To Condition is compulsory information.Please Select To Condition."), Me)
                    cboTocondition.Focus()
                    Exit Sub
                End If
            End If

            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeavePlanner As New clsleaveplanner
        'Pinkal (11-Sep-2020) -- End
        Try
            If mdtEmployee IsNot Nothing Then
                Dim drRow() As DataRow = mdtEmployee.Select("ischecked = True")
                If drRow.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List."), Me)
                    dgvEmployee.Focus()
                    Exit Sub
                End If
            End If
            If CInt(cboLeaveType.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                cboLeaveType.Focus()
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Call SetValue()
            Call SetValue(objLeavePlanner)
            'Pinkal (11-Sep-2020) -- End


            If mintLeavePlannerUnkid > 0 Then
                blnFlag = objLeavePlanner.Update(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
            Else
                Dim dsEmployee As New DataSet
                dsEmployee.Tables.Add(mdtEmployee)
                blnFlag = objLeavePlanner.Insert(dsEmployee, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
            End If

            If blnFlag = False Then
                If objLeavePlanner._Message <> "" Then
                    DisplayMessage.DisplayMessage(objLeavePlanner._Message, Me)
                    Exit Sub
                End If
            Else

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If mintLeavePlannerUnkid > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Leave Planner updated successfully."), Me.Page, Session("rootpath").ToString & "Leave/wPg_LeavePlannerList.aspx")
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Leave Planner saved successfully."), Me.Page, Session("rootpath").ToString & "Leave/wPg_LeavePlannerList.aspx")
                End If
                'Pinkal (04-Feb-2019) -- End

                Call ResetValue()
            End If

            'Session("LeavePlannerUnkid") = Nothing

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If mintLeavePlannerUnkid > 0 Then Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeavePlannerList.aspx", False)
            'Pinkal (04-Feb-2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeavePlanner = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("LeavePlannerUnkid") = Nothing
            If mintLeavePlannerUnkid > 0 Then mintLeavePlannerUnkid = 0
            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeavePlannerList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " TextBox's Events "

    Protected Sub txtFromYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromYear.TextChanged
        Try
            mdtFromDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(txtFromYear.Text) * -1).AddMonths(CInt(txtFromYear.Text) * -1)
            txtToYear.Text = txtFromYear.Text

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtFromYear_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtToYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToYear.TextChanged
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(txtToYear.Text) * -1).AddMonths(CInt(txtToYear.Text) * -1)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtToYear_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If mdtEmployee IsNot Nothing Then
                Dim dvEmployee As DataView = mdtEmployee.DefaultView
                If dvEmployee.Table.Rows.Count > 0 Then
                    If txtSearch.Text.Trim.Length > 0 Then
                        dvEmployee.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%' OR name like '%" & txtSearch.Text.Trim & "%' "
                    End If
                    If mstrAdvanceFilter.Trim.Length > 0 Then
                        dvEmployee.RowFilter &= IIf(dvEmployee.RowFilter.Trim.Length > 0, " AND ", "").ToString & mstrAdvanceFilter
                    End If
                    dgvEmployee.DataSource = dvEmployee.ToTable
                    dgvEmployee.DataBind()
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearch_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " RadioButton's Events "

    Protected Sub radExpYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radExpYear.CheckedChanged, _
                                                                                                         radAppointedDate.CheckedChanged, _
                                                                                                         radProbationDate.CheckedChanged, _
                                                                                                         radConfirmationDate.CheckedChanged
        Try
            If CType(sender, RadioButton).ID.ToUpper = "RADEXPYEAR" Then
                pnl_FromYear.Enabled = True
                pnl_ToYear.Enabled = True
                pnl_Date.Enabled = False

            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADAPPOINTEDDATE" Then
                pnl_Date.Enabled = True
                pnl_FromYear.Enabled = False
                pnl_ToYear.Enabled = False

            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADPROBATIONDATE" Then
                pnl_Date.Enabled = True
                pnl_FromYear.Enabled = False
                pnl_ToYear.Enabled = False

            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADCONFIRMATIONDATE" Then
                pnl_Date.Enabled = True
                pnl_FromYear.Enabled = False
                pnl_ToYear.Enabled = False

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radExpYear_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " LinkButton's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmployee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmployee.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.

                'If e.Item.Cells(3).Text.Trim <> "" Then
                '    e.Item.Cells(3).Text = (eZeeDate.convertDate(e.Item.Cells(3).Text).Date.ToString(Session("DateFormat").ToString()))
                'End If
                'If e.Item.Cells(4).Text.Trim <> "" Then
                '    e.Item.Cells(4).Text = (eZeeDate.convertDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat").ToString()))
                'End If
                'If e.Item.Cells(5).Text.Trim <> "" Then
                '    e.Item.Cells(5).Text = (eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToString(Session("DateFormat").ToString()))
                'End If
                'If e.Item.Cells(6).Text.Trim <> "" Then
                '    e.Item.Cells(6).Text = (eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToString(Session("DateFormat").ToString()))
                'End If

                If e.Item.Cells(3).Text.Trim <> "" Then
                    e.Item.Cells(3).Text = eZeeDate.convertDate(e.Item.Cells(3).Text).ToShortDateString()
                End If
                If e.Item.Cells(4).Text.Trim <> "" Then
                    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).ToShortDateString()
                End If
                If e.Item.Cells(5).Text.Trim <> "" Then
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).ToShortDateString()
                End If
                If e.Item.Cells(6).Text.Trim <> "" Then
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).ToShortDateString()
                End If

                'Pinkal (16-Apr-2016) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayMessage("" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Protected Sub ChkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim dvEmployee As DataView = mdtEmployee.DefaultView

            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    dvEmployee.RowFilter = mstrAdvanceFilter.Trim
            'Else
            '    dvEmployee.RowFilter = ""
            'End If

            For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
                Dim gvRow As DataGridItem = dgvEmployee.Items(i)
                CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = chkBox.Checked
                Dim dRow() As DataRow = mdtEmployee.Select("employeeunkid = '" & gvRow.Cells(9).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischecked") = chkBox.Checked
                End If
                dvEmployee.Table.AcceptChanges()
            Next
            mdtEmployee = dvEmployee.Table

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim drRow() As DataRow = mdtEmployee.Select("employeeunkid = " & item.Cells(9).Text)
            If drRow.Length > 0 Then
                drRow(0).Item("ischecked") = chkBox.Checked
            End If

            mdtEmployee.AcceptChanges()

            'Dim drCount() As DataRow = mdtEmployee.Select("ischecked = true")
            'If drCount.Length = mdtEmployee.Rows.Count Then
            '    CType(dgvEmployee.Items(0).Cells(0).FindControl("ChkAll"), CheckBox).Checked = True
            'Else
            '    CType(dgvEmployee.Items(0).FindControl("ChkAll"), CheckBox).Checked = False
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkgvSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List.")
            Language.setMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type.")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "From Condition is compulsory information.Please Select From Condition.")
            Language.setMessage(mstrModuleName, 5, "To Condition is compulsory information.Please Select To Condition.")
            Language.setMessage(mstrModuleName, 6, "Sorry, To date cannot ne less than From date.")
            Language.setMessage(mstrModuleName, 7, "Employee Count :")
            Language.setMessage(mstrModuleName, 8, "Leave Planner saved successfully.")
            Language.setMessage(mstrModuleName, 9, "Leave Planner updated successfully.")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
