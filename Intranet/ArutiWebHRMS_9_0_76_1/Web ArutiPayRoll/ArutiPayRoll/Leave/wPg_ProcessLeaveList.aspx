﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ProcessLeaveList.aspx.vb"
    Inherits="Leave_wPg_ProcessLeaveList" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
                
            }
            
            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
	             if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());
                        
                }
                
            }
//            $('input[type="image"]').live("click",function(){
//                alert('hiiii');
//                $(".cal_Theme1").children("div").css("z-index","99999");
//            });
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Visible="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblFormNo" runat="server" Text="Application No:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtFormNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateCtrl ID="dtpStartDate" AutoPostBack="false" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtApprover" runat="server" Enabled="False"></asp:TextBox>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblApplyDate" runat="server" Text="Apply Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateCtrl ID="dtpApplyDate" AutoPostBack="false" runat="server" />
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEndDate" runat="server" Text="Return Date:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateCtrl ID="dtpEndDate" AutoPostBack="false" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblLeaveType" runat="server" Text="Leave:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboLeaveType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status:"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%" >
                                                <asp:CheckBox ID="chkMyApprovals" runat="server" Text="My Approvals" Checked="true" />
                                            </td>
                                            <td style="width: 20%" >
                                                <asp:CheckBox ID="chkIncludeClosedFYTransactions" runat="server" Text="Include Closed FY Transactions" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                        <asp:Button ID="Btnclose" runat="server" CssClass=" btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                height: 350px; overflow: auto">
                                <asp:DataGrid ID="dgView" runat="server" Width="150%" AutoGenerateColumns="False"
                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                    
                                        <%--Pinkal (03-May-2019) -- Start
                                        Enhancement - Working on Leave UAT Changes for NMB.--%>
                                         <asp:TemplateColumn HeaderStyle-Width="70px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Left"
                                            FooterText="btnEdit" HeaderText = "Approval">                   
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc" style="padding:0 25px; display:block">
                                                    <asp:LinkButton ID="Imgchange" runat="server" CommandName="Select" 
                                                        ToolTip="Change Approval Status"> 
                                                           <%--'Pinkal (25-May-2019) -- Start
                                                           'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                                        <img src="../images/change_status.png" height="20px" width="20px" alt="Change Approval Status" />
                                                           <%--'Pinkal (25-May-2019) -- End--%>
                                                        </asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                       <%--Pinkal (03-May-2019) -- End--%>
                                        
                                        
                                        <%--0--%>
                                        <asp:TemplateColumn HeaderText="Expense" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkLeaveExpense" runat="server" CommandName="Expense" Text="Leave Expense"
                                                    Font-Underline="false" ToolTip="Leave Expense" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--1--%>
                                        <asp:TemplateColumn HeaderText="Issue Leave" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkIssueLeave" runat="server" CommandName="Issue" Text="Issue Leave"
                                                    Font-Underline="false" ToolTip="Issue Leave" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="formno" HeaderText="Application No" FooterText="colhFormNo" />
                                        <%--3--%>
                                        <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" FooterText="colhEmployeeCode" />
                                        <%--4--%>
                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee Name" FooterText="colhEmployee" />
                                        <%--5--%>
                                        <asp:BoundColumn DataField="approvername" HeaderText="Approver" FooterText="colhApprover" />
                                        <%--6--%>
                                        <asp:BoundColumn DataField="levelname" HeaderText="Approver Level" FooterText="" />
                                        <%--7--%>
                                        <asp:BoundColumn DataField="leavename" HeaderText="Leave" FooterText="colLeaveType" />
                                        <%--8--%>
                                        <asp:BoundColumn DataField="startdate" HeaderText="Start Date" FooterText="colhStartDate"
                                            HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <%--9--%>
                                        <asp:BoundColumn DataField="enddate" HeaderText="End Date" FooterText="colhEndDate"
                                            HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <%--10--%>
                                        <asp:BoundColumn DataField="days" HeaderText="Days" FooterText="colhLeaveDays" HeaderStyle-Width="65px"
                                            ItemStyle-Width="65px" />
                                        <%--11--%>
                                        <asp:BoundColumn DataField="status" HeaderText="Status" FooterText="colhStatus" />
                                        <%--12--%>
                                        <asp:BoundColumn DataField="formunkid" HeaderText="Formunkid" Visible="False" />
                                        <%--13--%>
                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" Visible="False" />
                                        <%--14--%>
                                        <asp:BoundColumn DataField="approverunkid" Visible="False" />
                                        <%--15--%>
                                        <asp:BoundColumn DataField="statusunkid" Visible="False" />
                                        <%--16--%>
                                        <asp:BoundColumn DataField="mapuserunkid" Visible="False" />
                                        <%--17--%>
                                        <asp:BoundColumn DataField="mapapproverunkid" Visible="False" />
                                        <%--18--%>
                                        <asp:BoundColumn DataField="priority" Visible="False" />
                                        <%--19--%>
                                        <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="gbRemark" />
                                        <%--20--%>
                                        <asp:BoundColumn DataField="iscancelform" Visible="false" />
                                        <%--21--%>
                                        <asp:BoundColumn DataField="leaveissueunkid" Visible="false" />
                                        <%--22--%>
                                        <asp:BoundColumn DataField="leaveapproverunkid" Visible="false" />
                                        <%--23--%>
                                        <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                        <%--24--%>
                                         <asp:BoundColumn DataField="isexternalapprover" Visible="false" />
                                        <%--25--%>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupClaimRequest" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hdf_ClaimRequest" PopupControlID="pnl_ClaimRequest" DropShadow="true"
                        CancelControlID="hdf_ClaimRequest" />
                    <asp:Panel ID="pnl_ClaimRequest" runat="server" CssClass="newpopup" Style="display: none;
                        width: 990px">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Claim & Request"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label5" runat="server" Text="Claim & Request Add/Edit"></asp:Label>
                                        </div>
                                        <%--'Pinkal (20-Nov-2018) -- Start
                                'Enhancement - Working on P2P Integration for NMB.--%>
                                        <div style="text-align: right">
                                            <asp:LinkButton ID="lnkClaimDepedents" runat="server" Text="View Depedents List"
                                                CssClass="lnkhover"></asp:LinkButton>
                                        </div>
                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                    </div>
                                    <div id="Div6" class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td style="border-right: solid 1px #DDD; width: 40%;">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                            </td>
                                                            <td style="width: 75%" colspan="3">
                                                                <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" Width="263px">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period"></asp:Label>
                                                                <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 35%">
                                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblEmpAddEdit" runat="server" Text="Employee"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboClaimEmployee" runat="server" AutoPostBack="true" Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblLeaveTypeAddEdit" runat="server" Text="Leave Type"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboClaimLeaveType" runat="server" AutoPostBack="true" Enabled="False"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="objlblValue" runat="server" Text="Leave Form"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="false" Enabled="False"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                                        <%-- <tr>
                                                            <td>
                                                            </td>
                                                            <td colspan="3" style="font-weight: bold">
                                                                <asp:LinkButton ID="lnkClaimDepedents" runat="server" ForeColor="#006699" Text="View Depedents List"
                                                                    CssClass="lnkhover"></asp:LinkButton>
                                                            </td>
                                                        </tr>--%>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                    ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </table>
                                                </td>
                                                <td style="width: 60">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                                                            </td>
                                                             <%--'Pinkal (04-Feb-2019) Start -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <td colspan="3" style="width: 90%">
                                                                <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <%--'Pinkal (04-Feb-2019) -- End --%>
                                                            
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                                           <%--   'Pinkal (30-Apr-2018) - Start
                                                             'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.--%>
                                                        <%--<tr style="width: 100%">
                                                             <td style="width: 13%">
                                                                   <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                             </td>
                                                             <td style="width: 24%">
                                                                <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                              </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>
                                                              <td style="width: 12%">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtCalimQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    AutoPostBack="true"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00"
                                                                    Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblBalanceAddEdit" runat="server" Text="Balance"></asp:Label>
                                                            </td>
                                                            <td style="width: 24%">
                                                                <asp:TextBox ID="txtClaimBalance" runat="server" CssClass="txttextalignright" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="118px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"></asp:TextBox>
                                                            </td>
                                                        </tr>--%>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%" colspan="3">
                                                                <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                             <%--'Pinkal (04-Feb-2019) Start -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--  <td style="width: 10%">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtCalimQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    AutoPostBack="true"></asp:TextBox>
                                                            </td>
                                                               <%--'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%" colspan="3">
                                                                <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--'Pinkal (04-Feb-2019) Start -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--   <td style="width: 10%">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtCalimQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    AutoPostBack="true"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"></asp:TextBox>
                                                                <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00"
                                                                    Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </td>
                                                             <%--'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 14%">
                                                                <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 11%">
                                                                <asp:Label ID="lblBalanceAddEdit" runat="server" Text="Balance"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="txtClaimBalance" runat="server" CssClass="txttextalignright" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <%--'Pinkal (04-Feb-2019) Start -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%-- <td style="width: 13%">
                                                              <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                               <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"></asp:TextBox>
                                                               <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00"
                                                                    Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                              <asp:Label ID="LblCurrency" runat="server" Text="Currency"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                            <asp:DropDownList ID="CboCurrency" runat="server"></asp:DropDownList>
                                                            </td>
                                                            <%--'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td colspan="6">
                                                                <cc1:TabContainer ID="tabmain" runat="server" ActiveTabIndex="0">
                                                                    <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                    </cc1:TabPanel>
                                                                    <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                    </cc1:TabPanel>
                                                                </cc1:TabContainer>
                                                                <asp:HiddenField ID="hdf_ClaimRequest" runat="server" />
                                                            </td>
                                                            <%--<td colspan="3" style="width: 20%" valign="top">
                                                                <asp:Label ID="LblDomicileAdd" runat="serve  r" Text="Domicile Address"></asp:Label>
                                                                <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="height: 56px;" ReadOnly="true"></asp:TextBox>
                                                            </td>--%>
                                                            <%--<td colspan="2">
                                                                <asp:HiddenField ID="hdf_ClaimRequest" runat="server" />
                                                            </td>--%>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div id="Div7" class="panel-default">
                                    <%--'Pinkal (04-Feb-2019) Start -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                    <%--<div id="Div3" class="panel-body-default">--%>
                                     <%--'Pinkal (04-Feb-2019) End.--%>
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Panel ID="pnl_dgvdata" runat="server" Height="170px" ScrollBars="Auto">
                                                        <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                                            CommandName="Preview">
                                                                            <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--0--%>
                                                                <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" FooterText="dgcolhExpense" />
                                                                <%--1--%>
                                                                <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" FooterText="dgcolhSectorRoute" />
                                                                <%--2--%>
                                                                <asp:BoundColumn DataField="uom" HeaderText="UoM" FooterText="dgcolhUoM" />
                                                                <%--3--%>
                                                                <asp:BoundColumn DataField="quantity" HeaderText="Quantity" FooterText="dgcolhQty"
                                                                    ItemStyle-CssClass="txttextalignright" />
                                                                <%--4--%>
                                                                <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" FooterText="dgcolhUnitPrice"
                                                                    ItemStyle-CssClass="txttextalignright" />
                                                                <%--5--%>
                                                                <asp:BoundColumn DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                                    ItemStyle-CssClass="txttextalignright" />
                                                                <%--6--%>
                                                                <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" FooterText="dgcolhExpenseRemark" />
                                                                <%--7--%>
                                                                <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" Visible="false" />
                                                                <%--8--%>
                                                                <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" Visible="false" />
                                                                <%--9--%>
                                                                <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" Visible="false" />
                                                                <%--10--%>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                                    <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="txttextalignright"
                                                        Width="20%"></asp:TextBox>
                                                    <asp:HiddenField ID="hdf_Claim" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="position: absolute; text-align: left;">
                                                        <asp:Button ID="btnViewScanAttchment" runat="server" Text="View Scan/Attchment" Visible="false"
                                                            CssClass="btnDefault" />
                                                    </div>
                                                    <asp:Button ID="btnClaimClose" runat="server" Text="Close" CssClass="btnDefault"
                                                        CausesValidation="false" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    <%--'Pinkal (04-Feb-2019) Start -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                   <%-- </div>--%>
                                     <%--'Pinkal (04-Feb-2019) End.--%>
                                </div>
                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hdf_claimDeletePopup" PopupControlID="pnlEmpDepedents" DropShadow="true"
                        CancelControlID="btnEmppnlEmpDepedentsClose">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px;">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div19" class="panel-default">
                                    <div id="Div20" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label6" runat="server" Text="Dependents List"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div21" class="panel-body-default">
                                        <asp:Panel ID="pvl_dgvDependance" runat="server" ScrollBars="Auto" Height="200px"
                                            Width="100%">
                                            <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" Width="99%"
                                                DataKeyField="" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                                    <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                                    <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                                    <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                                    <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                        <div class="btn-default">
                                            <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_claimDeletePopup" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--<PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />--%>
                </ContentTemplate>
                <%--'S.SANDEEP |25-JAN-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
                </Triggers>
                <%--'S.SANDEEP |25-JAN-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
