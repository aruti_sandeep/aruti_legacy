﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports eZeeCommonLib.eZeeDataType
Imports System.IO

Partial Class Leave_wPg_LeaveFormAddEdit
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName1 As String = "frmLeaveForm_AddEdit"
    Private Shared ReadOnly mstrModuleName5 As String = "frmClaims_RequestAddEdit"
    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private ReadOnly mstrModuleName2 As String = "frmDependentsList"
    'Pinkal (01-Mar-2016) -- End


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objEmployee As New clsEmployee_Master
    'Private clsleavetype As New clsleavetype_master
    'Private objLeaveForm As New clsleaveform
    'Private objLvAccrue As New clsleavebalance_tran
    'Private objClaimMaster As New clsclaim_request_master
    'Private objDayFraction As New clsleaveday_fraction
    'Pinkal (11-Sep-2020) -- End

    Private mintLeaveFormUnkid As Integer = 0
    Private mstrFolderName As String = ""
    Private mdtAttachement As DataTable = Nothing
    Private mdtFinalClaimTransaction As DataTable = Nothing

    Private mintClaimRequestMasterId As Integer = 0
    Private mintClaimRequestTranId As Integer = 0
    Private mblnClaimRequest As Boolean = False
    Private mdtLeaveExpenseTran As DataTable = Nothing
    Private mblnIsLeaveEncashment As Boolean = False
    Private mstrEditGuid As String = ""
    Private mdtDayFration As DataTable = Nothing

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Private mdtClaimAttchment As DataTable = Nothing
    Private mdtFinalClaimAttchment As DataTable = Nothing
    Private blnShowAttchmentPopup As Boolean = False
    Private mintDeleteIndex As Integer = 0
    Private mdtChildClaimAttachment As DataTable = Nothing
    'Shani (20-Aug-2016) -- End

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private intEligibleExpenseDays As Integer = 0
    'Pinkal (29-Sep-2017) -- End

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnLvTypeSkipApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnIsLvTypePaid As Boolean = False
    Private mintLvformMinDays As Integer = 0
    Private mintLvformMaxDays As Integer = 0
    'Pinkal (08-Oct-2018) -- End


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsExpenseEditClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private mblnIsClaimFormBudgetMandatory As Boolean = False
    'Pinkal (20-Nov-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsBlockLeave As Boolean = False
    Dim mblnIsStopApplyAcrossFYELC As Boolean = False
    Dim mblnIsShortLeave As Boolean = False
    Dim mintDeductedLeaveTypeID As Integer = 0
    'Pinkal (26-Feb-2019) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End

    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mblnConsiderExpense As Boolean = True
    'Pinkal (03-May-2019) -- End

    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Dim mstrP2PToken As String = ""
    'Pinkal (29-Aug-2019) -- End

    'Pinkal (18-Mar-2021) -- Start
    'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
    Private mintExpenseIndex As Integer = -1
    'Pinkal (18-Mar-2021) -- End


#End Region

#Region "Page Event(S)"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            SetLanguage()

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew'Shani (20-Aug-2016) -- End
            'If IsPostBack = False Then
            If IsPostBack = False AndAlso Request.QueryString("uploadimage") Is Nothing Then
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                If Session("IsScanAttchment") Is Nothing Then
                    'Me.Session.Remove("mdtClaimAttchment")
                    Me.Session.Remove("mdtFinalClaimAttchment")
                Else
                    Session("IsScanAttchment") = Nothing
                    mdtClaimAttchment = CType(Me.Session("mdtClaimAttchment"), DataTable)
                    mdtChildClaimAttachment = CType(Me.Session("mdtChildClaimAttachment"), DataTable)
                    mdtFinalClaimAttchment = CType(Me.Session("mdtFinalClaimAttchment"), DataTable)
                End If
                'Shani (20-Aug-2016) -- End

                Call SetProcessVisiblity()

                If Session("formunkid") IsNot Nothing Then
                    mintLeaveFormUnkid = CInt(Session("formunkid"))
                    Session("formunkid") = Nothing
                    cboEmployee.Enabled = False
                    cboLeaveType.Enabled = False
                    'Shani (20-Aug-2016) -- Start
                    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                    'ElseIf Request.QueryString.Count > 0 Then
                    '    Dim ar() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).ToString.Split(CChar("|"))
                    '    If ar.Length > 0 Then
                    '        mintLeaveFormUnkid = CInt(ar(0))
                    '    End If
                    'Shani (20-Aug-2016) -- End
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objLeaveForm As New clsleaveform
                'Pinkal (11-Sep-2020) -- End

                If mintLeaveFormUnkid > 0 Then
                    objLeaveForm._Formunkid = mintLeaveFormUnkid
                End If

                If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                    LblELCStart.Visible = True
                    LblELCStart.Text = ""
                ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                    LblELCStart.Visible = False
                End If

                'Pinkal (18-Aug-2022) -- Start
                'Bug Resolved in ESS when Leave Form Screen is  loading.
                If Session("fin_enddate") IsNot Nothing AndAlso CDate(Session("fin_enddate")) < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    dtpApplyDate.SetDate = CDate(Session("fin_enddate"))
                    dtpStartDate.SetDate = CDate(Session("fin_enddate"))
                    dtpEndDate.SetDate = CDate(Session("fin_enddate"))
                Else
                    dtpApplyDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpEndDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If
                'Pinkal (18-Aug-2022) -- End

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                ' Call FillCombo()
                'Call GetValue()
                FillCombo(objLeaveForm._Employeeunkid)
                GetValue(objLeaveForm)
                objLeaveForm = Nothing
                'Pinkal (11-Sep-2020) -- End


                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                If mdtFinalClaimAttchment Is Nothing Then
                    Dim objAttchement As New clsScan_Attach_Documents

                    'Pinkal (10-Jun-2020) -- Start
                    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                    Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                    End If
                    'Pinkal (10-Jun-2020) -- End

                    Dim strTranIds As String = ""
                    If mintLeaveFormUnkid > 0 Then
                        strTranIds = String.Join(",", mdtFinalClaimTransaction.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                    End If
                    If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                    mdtFinalClaimAttchment = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    objAttchement = Nothing
                    'Else
                    '    mdtClaimAttchment = mdtFinalClaimAttchment.Copy
                End If
                'Shani (20-Aug-2016) -- End
                mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LEAVEFORMS).Tables(0).Rows(0)("Name").ToString
            Else
                mintLeaveFormUnkid = CInt(Me.ViewState("LeaveFormUnkId"))
                mdtAttachement = CType(Me.ViewState("mdtAttachement"), DataTable)
                mintClaimRequestMasterId = CInt(Me.ViewState("ClaimRequestMasterId"))
                mintClaimRequestTranId = CInt(Me.ViewState("ClaimRequestTranId"))
                mblnClaimRequest = CBool(Me.ViewState("mblnClaimRequest"))
                mdtLeaveExpenseTran = CType(Me.ViewState("mdtLeaveExpenseTran"), DataTable)
                mblnIsLeaveEncashment = CBool(Me.ViewState("mblnIsLeaveEncashment"))
                mstrEditGuid = CStr(Me.ViewState("mstrEditGuid"))
                mdtFinalClaimTransaction = CType(Me.ViewState("mdtFinalClaimTransaction"), DataTable)
                mdtDayFration = CType(Me.ViewState("mdtDayFration"), DataTable)

                If mblnClaimRequest = True Then
                    popupExpense.Show()
                End If

                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                mdtClaimAttchment = CType(Me.Session("mdtClaimAttchment"), DataTable)
                mdtChildClaimAttachment = CType(Me.Session("mdtChildClaimAttachment"), DataTable)
                mdtFinalClaimAttchment = CType(Me.Session("mdtFinalClaimAttchment"), DataTable)
                blnShowAttchmentPopup = CBool(Me.ViewState("blnShowAttchmentPopup"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                'Shani (20-Aug-2016) -- End

                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
                intEligibleExpenseDays = CInt(Me.ViewState("intEligibleExpenseDays"))
                'Pinkal (29-Sep-2017) -- End

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mblnLvTypeSkipApproverFlow = CBool(Me.ViewState("LvTypeSkipApproverFlow"))
                'Pinkal (01-Oct-2018) -- End

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mblnIsLvTypePaid = CBool(Me.ViewState("LvTypeIsPaid"))
                mintLvformMinDays = CInt(Me.ViewState("LVDaysMinLimit"))
                mintLvformMaxDays = CInt(Me.ViewState("LVDaysMaxLimit"))
                'Pinkal (08-Oct-2018) -- End

                'Pinkal (22-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                mblnIsExpenseEditClick = CBool(Me.ViewState("mblnIsExpenseEditClick"))
                'Pinkal (22-Oct-2018) -- End

                'Pinkal (25-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
                'Pinkal (25-Oct-2018) -- End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsClaimFormBudgetMandatory"))
                'Pinkal (20-Nov-2018) -- End

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                'Pinkal (04-Feb-2019) -- End

                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsBlockLeave = CBool(Me.ViewState("IsBlockLeave"))
                mblnIsStopApplyAcrossFYELC = CBool(Me.ViewState("IsStopApplyAcrossFYELC"))
                mblnIsShortLeave = CBool(Me.ViewState("IsShortLeave "))
                mintDeductedLeaveTypeID = CInt(Me.ViewState("DeductedLeaveTypeID"))
                'Pinkal (26-Feb-2019) -- End

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(Me.ViewState("ConsiderLeaveOnTnAPeriod"))
                'Pinkal (01-Jan-2019) -- End


                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                mblnConsiderExpense = CBool(Me.ViewState("ConsiderExpense"))
                'Pinkal (03-May-2019) -- End

                'Pinkal (29-Aug-2019) -- Start
                'Enhancement NMB - Working on P2P Get Token Service URL.
                  If Me.ViewState("P2PToken") IsNot Nothing Then
                    mstrP2PToken = Me.ViewState("P2PToken").ToString()
                Else
                    mstrP2PToken = ""
                End If
                'Pinkal (29-Aug-2019) -- End


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                mintExpenseIndex = CInt(Me.ViewState("mintExpenseIndex"))
                'Pinkal (18-Mar-2021) -- End


            End If

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            If blnShowAttchmentPopup Then
                popup_ScanAttchment.Show()
            End If


            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'Shani (20-Aug-2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            If IsPostBack = False Then

                If Session("LVForm_EmployeeID") IsNot Nothing Then
                    cboEmployee.SelectedValue = Session("LVForm_EmployeeID").ToString
                    cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)
                    Session("LVForm_EmployeeID") = Nothing
                End If
                If Session("LVForm_LeaveTypeID") IsNot Nothing Then
                    cboLeaveType.SelectedValue = Session("LVForm_LeaveTypeID").ToString
                    cboLeaveType_SelectedIndexChanged(cboLeaveType, Nothing)
                    Session("LVForm_LeaveTypeID") = Nothing
                End If
                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                'If Session("LVForm_ApplyDate") IsNot Nothing Then
                '    dtpApplyDate.SetDate = CDate(Session("LVForm_ApplyDate"))
                '    dtpApplyDate_TextChanged(dtpApplyDate, Nothing)
                '    Session("LVForm_ApplyDate") = Nothing
                'End If
                'Shani (20-Aug-2016) -- End
                If Session("LVForm_StartDate") IsNot Nothing Then
                    dtpStartDate.SetDate = CDate(Session("LVForm_StartDate"))
                    dtpStartDate_TextChanged(dtpStartDate, Nothing)
                    Session("LVForm_StartDate") = Nothing
                End If

                If Session("LVForm_EndDate") IsNot Nothing Then
                    dtpEndDate.SetDate = CDate(Session("LVForm_EndDate"))
                    dtpEndDate_TextChanged(dtpEndDate, Nothing)
                    Session("LVForm_EndDate") = Nothing
                End If

                If Session("LVForm_ApplyDate") IsNot Nothing Then
                    dtpApplyDate.SetDate = CDate(Session("LVForm_ApplyDate"))
                    Session("LVForm_ApplyDate") = Nothing
                End If

                If Session("ScanAttachData") IsNot Nothing Then
                    mdtAttachement = CType(Session("ScanAttachData"), DataTable)
                    Session("ScanAttachData") = Nothing
                End If

                If Session("LVForm_ClaimExpense") IsNot Nothing Then
                    mdtFinalClaimTransaction = CType(Session("LVForm_ClaimExpense"), DataTable)
                    Session("LVForm_ClaimExpense") = Nothing
                End If

                If Session("mdtDayFration") IsNot Nothing Then
                    mdtDayFration = CType(Session("mdtDayFration"), DataTable)
                    Session("mdtDayFration") = Nothing
                End If

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                If Session("LeaveAddress") IsNot Nothing Then
                    txtLeaveAddress.Text = Session("LeaveAddress").ToString().Trim()
                    Session("LeaveAddress") = Nothing
                End If

                If Session("LeaveReason") IsNot Nothing Then
                    txtLeaveReason.Text = Session("LeaveReason").ToString().Trim()
                    Session("LeaveReason") = Nothing
                End If
                'Pinkal (03-May-2019) -- End

            End If


            Me.ViewState("LeaveFormUnkId") = mintLeaveFormUnkid
            Me.ViewState("mdtAttachement") = mdtAttachement
            Me.ViewState("ClaimRequestMasterId") = mintClaimRequestMasterId
            Me.ViewState("ClaimRequestTranId") = mintClaimRequestTranId
            Me.ViewState("mblnClaimRequest") = mblnClaimRequest
            Me.ViewState("mdtLeaveExpenseTran") = mdtLeaveExpenseTran
            Me.ViewState("mblnIsLeaveEncashment") = mblnIsLeaveEncashment
            Me.ViewState("mstrEditGuid") = mstrEditGuid
            Me.ViewState("mdtFinalClaimTransaction") = mdtFinalClaimTransaction
            Me.ViewState("mdtDayFration") = mdtDayFration

            If mdtDayFration IsNot Nothing AndAlso mdtDayFration.Rows.Count > 0 Then
                objNoofDays.Text = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD<>'D'")).ToString
            End If

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            Me.Session("mdtClaimAttchment") = mdtClaimAttchment
            Me.Session("mdtChildClaimAttachment") = mdtChildClaimAttachment
            Me.Session("mdtFinalClaimAttchment") = mdtFinalClaimAttchment
            Me.ViewState("blnShowAttchmentPopup") = blnShowAttchmentPopup
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            'Shani (20-Aug-2016) -- End

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            Me.ViewState("intEligibleExpenseDays") = intEligibleExpenseDays
            'Pinkal (29-Sep-2017) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Me.ViewState("LvTypeSkipApproverFlow") = mblnLvTypeSkipApproverFlow
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Me.ViewState("LvTypeIsPaid") = mblnIsLvTypePaid
            Me.ViewState("LVDaysMinLimit") = mintLvformMinDays
            Me.ViewState("LVDaysMaxLimit") = mintLvformMaxDays
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Me.ViewState("mblnIsExpenseEditClick") = mblnIsExpenseEditClick
            'Pinkal (22-Oct-2018) -- End

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            Me.ViewState("IsClaimFormBudgetMandatory") = mblnIsClaimFormBudgetMandatory
            'Pinkal (20-Nov-2018) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Me.ViewState("IsHRExpense") = mblnIsHRExpense
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            'Pinkal (04-Feb-2019) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Me.ViewState("IsBlockLeave") = mblnIsBlockLeave
            Me.ViewState("IsStopApplyAcrossFYELC") = mblnIsStopApplyAcrossFYELC
            Me.ViewState("IsShortLeave ") = mblnIsShortLeave
            Me.ViewState("DeductedLeaveTypeID") = mintDeductedLeaveTypeID
            'Pinkal (26-Feb-2019) -- End

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            Me.ViewState("ConsiderLeaveOnTnAPeriod") = mblnConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Me.ViewState("ConsiderExpense") = mblnConsiderExpense
            'Pinkal (03-May-2019) -- End

            'Pinkal (29-Aug-2019) -- Start
            'Enhancement NMB - Working on P2P Get Token Service URL.
            Me.ViewState("P2PToken") = mstrP2PToken
            'Pinkal (29-Aug-2019) -- End

            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            Me.ViewState("mintExpenseIndex") = mintExpenseIndex
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Privat Method(S)"

    Private Sub GetELCDates()
        Try
            LblELCStart.Text = ""
            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                Dim objLeaveAccrue As New clsleavebalance_tran
                Dim dsBalance As DataSet = objLeaveAccrue.GetList("List", _
                                                                  Session("Database_Name").ToString, _
                                                                  CInt(Session("UserId")), _
                                                                  CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), _
                                                                  Session("EmployeeAsOnDate").ToString, _
                                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                                  CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
                                                                  CInt(cboEmployee.SelectedValue), True, True)
                Dim drRow() As DataRow = dsBalance.Tables("List").Select("leavetypeunkid = " & CInt(cboLeaveType.SelectedValue) & " AND isvoid = 0 AND isopenelc = 1 AND iselc = 1")
                If drRow.Length > 0 Then

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'If Session("DateFormat").ToString <> Nothing Then
                    '    LblELCStart.Text = Language.getMessage(mstrModuleName1, 16, "ELC Start From") & Space(10) & CDate(drRow(0)("startdate")).ToString(Session("DateFormat").ToString) & "  -  " & CDate(drRow(0)("enddate")).ToString(Session("DateFormat").ToString)
                    'Else
                    '    LblELCStart.Text = Language.getMessage(mstrModuleName1, 16, "ELC Start From") & Space(10) & CDate(drRow(0)("startdate")).ToShortDateString & "  -  " & CDate(drRow(0)("enddate")).ToShortDateString
                    'End If
                    SetDateFormat()
                    LblELCStart.Text = Language.getMessage(mstrModuleName1, 16, "ELC Start From") & Space(10) & CDate(drRow(0)("startdate")).ToShortDateString & "  -  " & CDate(drRow(0)("enddate")).ToShortDateString

                    'Pinkal (16-Apr-2016) -- End

                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsBalance IsNot Nothing Then dsBalance.Clear()
                dsBalance = Nothing
                objLeaveAccrue = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetELCDates:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetEndDateForSickLeave()
        Try
            Dim objLType As New clsleavetype_master
            objLType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            If objLType._IsSickLeave = True Then
                dtpEndDate.SetDate = Nothing
            End If
            objLType = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetEndDateForSickLeave:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub GetLeaveBalance()
        Try
            Dim objclsLeaveAccrue As New clsleavebalance_tran
            Dim dsList As DataSet = Nothing

            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, CDate(Session("fin_startdate")), CDate(Session("fin_enddate")), False, False, CInt(Session("LeaveBalanceSetting")), , CInt(Session("Fin_year")))
                dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString _
                                                                                      , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                                                      , CDate(Session("fin_startdate")), CDate(Session("fin_enddate")), False, False, CInt(Session("LeaveBalanceSetting")), , CInt(Session("Fin_year")))
                'Pinkal (16-Dec-2016) -- End


            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                objclsLeaveAccrue._DBStartdate = CDate(Session("fin_startdate")).Date
                objclsLeaveAccrue._DBEnddate = CDate(Session("fin_enddate")).Date

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, Nothing, Nothing, True, True, CInt(Session("LeaveBalanceSetting")), , CInt(Session("Fin_year")))
                dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString _
                                                                                      , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                                                      , Nothing, Nothing, True, True, CInt(Session("LeaveBalanceSetting")), , CInt(Session("Fin_year")))
                'Pinkal (16-Dec-2016) -- End


            End If

            Call GetELCDates()

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objBalance.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")
            Else
                objBalance.Text = "0.00"
            End If

            Dim dsbalanceList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            Dim mdtEndDate As DateTime = Nothing
            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                dsbalanceList = objclsLeaveAccrue.GetEmployeeBalanceData(CInt(cboLeaveType.SelectedValue).ToString(), CInt(cboEmployee.SelectedValue).ToString(), True, CInt(Session("Fin_year")), False, False, False)
                If dsbalanceList IsNot Nothing AndAlso dsbalanceList.Tables(0).Rows.Count > 0 Then
                    If mdtEndDate > CDate(Session("fin_enddate")).Date OrElse mdtEndDate = Nothing Then
                        mdtEndDate = CDate(Session("fin_enddate")).Date
                    Else
                        mdtEndDate = CDate(dsbalanceList.Tables(0).Rows(0)("enddate")).Date
                    End If
                Else
                    mdtEndDate = CDate(Session("fin_enddate")).Date
                End If

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(mdtEndDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, CDate(Session("fin_startdate")), CDate(Session("fin_enddate")), False, False, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")))
                dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(mdtEndDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString _
                                                                                        , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                                                        , CDate(Session("fin_startdate")), CDate(Session("fin_enddate")), False, False, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")))
                'Pinkal (16-Dec-2016) -- End



            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                dsbalanceList = objclsLeaveAccrue.GetEmployeeBalanceData(CInt(cboLeaveType.SelectedValue).ToString(), CInt(cboEmployee.SelectedValue).ToString(), True, CInt(Session("Fin_year")), True, True, False)
                If dsbalanceList IsNot Nothing AndAlso dsbalanceList.Tables(0).Rows.Count > 0 Then
                    mdtEndDate = CDate(dsbalanceList.Tables(0).Rows(0)("enddate")).Date
                Else
                    If objEmployee._Termination_To_Date.Date <> Nothing Then
                        mdtEndDate = objEmployee._Termination_To_Date.Date
                    End If
                End If
                objclsLeaveAccrue._DBStartdate = CDate(Session("fin_startdate")).Date
                objclsLeaveAccrue._DBEnddate = CDate(Session("fin_enddate")).Date

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(mdtEndDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, Nothing, Nothing, True, True, CInt(Session("LeaveBalanceSetting")), , CInt(Session("Fin_year")))
                dsList = objclsLeaveAccrue.GetLeaveBalanceInfo(mdtEndDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString _
                                                                                       , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                                                       , Nothing, Nothing, True, True, CInt(Session("LeaveBalanceSetting")), , CInt(Session("Fin_year")))
                'Pinkal (16-Dec-2016) -- End



            End If

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objLeaveAccrue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
            Else
                objLeaveAccrue.Text = "0.00"
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsbalanceList IsNot Nothing Then dsbalanceList.Clear()
            dsbalanceList = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CalculateDays(ByVal sender As Object)

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        'Dim dtFraction As DataTable = mdtDayFration
        Dim dtFraction As DataTable = Nothing
        If mdtDayFration IsNot Nothing Then
            dtFraction = mdtDayFration.Copy()
        End If

        'Pinkal (11-Sep-2020) -- End
        Try
            Dim objLeaveType As New clsleavetype_master
            Dim blnIssueonHoliday As Boolean = False
            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)

            Dim objLeaveFraction As New clsleaveday_fraction
            dtFraction = objLeaveFraction.GetList("List", -1, True, CInt(cboEmployee.SelectedValue), -1, " AND lvleaveday_fraction.formunkid =" & mintLeaveFormUnkid).Tables(0)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveFraction = Nothing
            'Pinkal (11-Sep-2020) -- End

            Dim mdtStartDate As Date = dtpStartDate.GetDate.Date
            Dim mdtEndDate As Date = Nothing

            If Not dtpEndDate.IsNull Then
                mdtEndDate = dtpEndDate.GetDate.Date
            Else
                mdtEndDate = dtpStartDate.GetDate.Date
            End If

            Dim mintTotalDays As Integer = CInt(DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date.AddDays(1).Date))
            Dim drRow As DataRow() = dtFraction.Select("leavedate > '" & mdtEndDate.Date & "'")

            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
            End If


            drRow = dtFraction.Select("leavedate < '" & mdtStartDate.Date & "'")

            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
            End If


            Dim objEmp As New clsEmployee_Master
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim objShift As New clsNewshift_master
            Dim objShiftTran As New clsshift_tran
            Dim objEmpHoliday As New clsemployee_holiday
            Dim blnIssueonweekend As Boolean = False
            Dim blnConsiderLVHlOnWk As Boolean = False


            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)

            blnIssueonweekend = objLeaveType._Isissueonweekend
            blnIssueonHoliday = objLeaveType._Isissueonholiday
            blnConsiderLVHlOnWk = objLeaveType._IsLeaveHL_onWK

            For i As Integer = 0 To mintTotalDays - 1
                objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartDate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)))
                objShiftTran.GetShiftTran(objShift._Shiftunkid)
                Dim strWeekName As String = CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(mdtStartDate.AddDays(i).DayOfWeek)

                If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
                    If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dRow(0)("AUD") = "D"
                            dtFraction.AcceptChanges()
                        End If
                        If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

                            If objShiftTran._dtShiftday IsNot Nothing Then
                                Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")

                                If drShift.Length > 0 Then
                                    GoTo AddRecord
                                End If

                            End If

                        End If
                        Continue For
                    End If
                    Dim objReccurent As New clsemployee_holiday
                    If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), mdtStartDate.AddDays(i).Date) Then
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objReccurent = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Continue For
                    End If
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objReccurent = Nothing
                    'Pinkal (11-Sep-2020) -- End
                End If

                If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
                    If objShiftTran._dtShiftday IsNot Nothing Then
                        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                        If drShift.Length > 0 Then

                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dtFraction.AcceptChanges()
                            End If

                            If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
                                If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                                    GoTo AddRecord
                                End If

                            End If
                            Continue For

                        End If

                    End If

                End If

                If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then

                    If objShiftTran._dtShiftday IsNot Nothing Then
                        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                        If drShift.Length > 0 Then
                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dtFraction.AcceptChanges()
                            End If
                            Continue For
                        Else

                            If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                                Continue For
                            End If
                            Dim objReccurent As New clsemployee_holiday
                            If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), mdtStartDate.AddDays(i).Date) Then
                                'Pinkal (11-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                objReccurent = Nothing
                                'Pinkal (11-Sep-2020) -- End
                                Continue For
                            End If
                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objReccurent = Nothing
                            'Pinkal (11-Sep-2020) -- End
                        End If
                    End If

                End If
AddRecord:
                Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtStartDate.Date.AddDays(i).Date & "' AND AUD = ''")
                If dtRow.Length = 0 Then
                    Dim dr As DataRow = dtFraction.NewRow
                    dr("dayfractionunkid") = -1
                    dr("formunkid") = mintLeaveFormUnkid
                    dr("leavedate") = mdtStartDate.Date.AddDays(i).ToShortDateString
                    dr("dayfraction") = 1
                    If dr("AUD").ToString() = "" Then
                        dr("AUD") = "A"
                    End If
                    dr("GUID") = Guid.NewGuid.ToString()
                    dtFraction.Rows.Add(dr)
                End If
            Next

            If Not IsDBNull(dtFraction.Compute("SUM(dayfraction)", "AUD<>'D'")) Then
                objNoofDays.Text = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD<>'D'")).ToString
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtDayFration = New DataView(dtFraction, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable
                mdtDayFration = New DataView(dtFraction, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable().Copy()
                'Pinkal (11-Sep-2020) -- End
            Else
                objNoofDays.Text = "0.00"
                If dtFraction IsNot Nothing Then
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'mdtDayFration = New DataView(dtFraction, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable
                    mdtDayFration = New DataView(dtFraction, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable().Copy()
                    'Pinkal (11-Sep-2020) -- End
                End If
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveType = Nothing
            objEmpHoliday = Nothing
            objEmpShiftTran = Nothing
            objShiftTran = Nothing
            objShift = Nothing
            objEmp = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtFraction IsNot Nothing Then dtFraction.Clear()
            dtFraction = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Public Function Entryvalid() As Boolean
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objemployee As New clsEmployee_Master
        Dim objLeaveType As New clsleavetype_master
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If dtpApplyDate.GetDate > dtpStartDate.GetDate Then
            If dtpApplyDate.GetDate > dtpStartDate.GetDate AndAlso CBool(Session("AllowLvApplicationApplyForBackDates")) = False Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, Apply date should not be greater than Start date."), Me)
                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                '    dtpApplyDate.SetDate = dtpStartDate.GetDate
                'ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                '    dtpStartDate.SetDate = dtpApplyDate.GetDate
                'End If
                'Pinkal (07-Mar-2019) -- End
                'Pinkal (13-Jun-2019) -- End
                dtpStartDate.Focus()
                Return False
            End If

            If dtpEndDate.GetDate < dtpStartDate.GetDate AndAlso dtpEndDate.IsNull = False Then
                dtpEndDate.SetDate = dtpStartDate.GetDate
                DisplayMessage.DisplayMessage("End Date  Cannot be less  than Start Date . ", Me)
                dtpStartDate.Focus()
                Return False
            End If



            objemployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)


            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)

            If objLeaveType._Gender > 0 Then

                If objLeaveType._Gender <> objemployee._Gender Then
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 13, "This leave type is invalid for selected employee."), Me)
                    cboLeaveType.SelectedValue = "0"
                    cboLeaveType.Focus()
                    Return False
                End If
            End If




            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            If objLeaveType._DoNotApplyForFutureDates Then
                Language.setLanguage(mstrModuleName1)
                If dtpStartDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 57, "Sorry, you cannot apply for this leave application.Reason:As Per setting configured on leave type master you can not apply for future date(s)."), Me)
                    Return False
                ElseIf dtpEndDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 57, "Sorry, you cannot apply for this leave application.Reason:As Per setting configured on leave type master you can not apply for future date(s)."), Me)
                    Return False
                End If
            Else
                'Pinkal (20-Apr-2022) -- Start
                'Enhancement Prevision Air  - Doing Leave module Enhancement.	
                If objLeaveType._MaxFutureDaysLimit > 0 Then
                    If dtpStartDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.AddDays(objLeaveType._MaxFutureDaysLimit).Date Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 58, "Sorry, you cannot apply for this leave application.Reason: This leave type does not match maximum future days limit configured on leave type master."), Me)
                        Return False
                    ElseIf dtpEndDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.AddDays(objLeaveType._MaxFutureDaysLimit).Date Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 58, "Sorry, you cannot apply for this leave application.Reason: This leave type does not match maximum future days limit configured on leave type master."), Me)
                        Return False
                    End If
                End If
                'Pinkal (20-Apr-2022) -- End
            End If
            'Pinkal (05-Jun-2020) -- End


            'Pinkal (29-Sep-2017) -- Start
            'Bug - Solved Leave Eligility On Leave Application for PACRA.
            'If objLeaveType._EligibilityAfter > 0 Then
            '    If DateDiff(DateInterval.Day, objemployee._Appointeddate.Date, dtpStartDate.GetDate.Date) <= objLeaveType._EligibilityAfter Then
            '        Language.setLanguage(mstrModuleName1)
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 15, "You are not eligible to apply for this leave. Reason : Eligibility to apply for this Leave Type does not match with Eligibility days on leave type master."), Me)
            '        cboLeaveType.SelectedIndex = 0
            '        cboLeaveType.Focus()
            '        Exit Function
            '    End If
            'End If
            'Pinkal (29-Sep-2017) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveType = Nothing
            objemployee = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
        Return True
    End Function

    Public Function CheckMapping() As Boolean
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Employee is compulsory information.Please Select Employee."), Me)
                Return False
            End If


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            If mblnLvTypeSkipApproverFlow = False Then

                Dim dtList As DataTable = Nothing
                Dim objapprover As New clsleaveapprover_master

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                '                                         CInt(Session("Fin_year")), _
                '                                         CInt(Session("CompanyUnkId")), _
                '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                         CBool(Session("IsIncludeInactiveEmp")), CInt(Session("UserId")), _
                '                                         CInt(cboEmployee.SelectedValue), -1, -1, Session("LeaveApproverForLeaveType").ToString())

                dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    CBool(Session("IsIncludeInactiveEmp")), -1, _
                                                         CInt(cboEmployee.SelectedValue), -1, -1, Session("LeaveApproverForLeaveType").ToString())

                'Pinkal (01-Mar-2016) -- End

                If dtList.Rows.Count = 0 Then
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Please Assign Approver to this employee and also map approver to system user."), Me)
                    Return False
                End If

                If dtList.Rows.Count > 0 Then
                    Dim objUsermapping As New clsapprover_Usermapping
                    Dim isUserExist As Boolean = False
                    For Each dr As DataRow In dtList.Rows
                        objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                        If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                    Next

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objUsermapping = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    If isUserExist = False Then
                        Language.setLanguage(mstrModuleName1)
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 6, "Please Map this employee's Approver to system user."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Return False
                    End If
                End If

                If CBool(Session("LeaveApproverForLeaveType")) = True Then

                    If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboLeaveType.SelectedValue) > 0 Then

                        'Pinkal (01-Mar-2016) -- Start
                        'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                        'dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                        '                                         CInt(Session("Fin_year")), _
                        '                                         CInt(Session("CompanyUnkId")), _
                        '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                        '                                         CBool(Session("IsIncludeInactiveEmp")), CInt(Session("UserId")), _
                        '                                         CInt(cboEmployee.SelectedValue), -1, CInt(cboLeaveType.SelectedValue), _
                        '                                         Session("LeaveApproverForLeaveType").ToString)

                        dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                            CBool(Session("IsIncludeInactiveEmp")), -1, _
                                                                 CInt(cboEmployee.SelectedValue), -1, CInt(cboLeaveType.SelectedValue), _
                                                                 Session("LeaveApproverForLeaveType").ToString)


                        'Pinkal (01-Mar-2016) -- End

                        If dtList.Rows.Count = 0 Then
                            Language.setLanguage(mstrModuleName1)
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 12, "Please Map this Leave type to this employee's Approver(s)."), Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Function
                        End If
                    End If
                End If

                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
                If mdtFinalClaimTransaction IsNot Nothing AndAlso mdtFinalClaimTransaction.Rows.Count > 0 Then
                    If IsValidExpense_Eligility() = False AndAlso mdtFinalClaimTransaction.Rows.Count > 0 Then
                        Language.setLanguage(mstrModuleName1)
                        'Pinkal (05-Jun-2020) -- Start
                        'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type.Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", Me)
                        'Pinkal (05-Jun-2020) -- End
                        Exit Function
                    End If
                End If
                'Pinkal (29-Sep-2017) -- End


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtList IsNot Nothing Then dtList.Clear()
                dtList = Nothing
                objapprover = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            'Pinkal (01-Oct-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Public Sub EntrySave()
        'Dim dtExpense As DataTable
        Try
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            '  Dim dsList As DataSet = Nothing
            'Dim clsLeaveForm As New clsleaveform
            'Pinkal (11-Sep-2020) -- End


            If CInt(Session("LeaveFormNoType")) = 0 Then ' checking Whether Manual or Automatic Form No generation Selected From Config file
                If txtFormNo.Text = "" Then
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Form No cannot be blank. Form No is required information."), Me)
                    Exit Sub
                End If
            End If
            ' dtExpense = mdtFinalClaimTransaction

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnIsLvTypePaid Then
                Dim mdecTotalDays As Decimal = 0
                If mdtDayFration IsNot Nothing Then
                    mdecTotalDays = mdtDayFration.AsEnumerable().Where(Function(row) row.Field(Of String)("AUD") <> "D").Sum(Function(row) row.Field(Of Decimal)("dayfraction"))
                End If
                If mintLvformMinDays <> 0 And mintLvformMinDays > mdecTotalDays Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 41, "Sorry, you cannot apply for this leave type as it has been configured with setting for minimum") & " " & mintLvformMinDays & " " & Language.getMessage(mstrModuleName1, 43, "day(s) to be applied for this leave type."), Me)
                    Exit Sub
                End If
                If mintLvformMaxDays <> 0 And mintLvformMaxDays < mdecTotalDays Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 42, "Sorry, you cannot apply for this leave type as it has been configured with setting for maximum") & " " & mintLvformMaxDays & " " & Language.getMessage(mstrModuleName1, 43, "day(s) to be applied for this leave type."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mblnIsStopApplyAcrossFYELC Then

                If mblnIsShortLeave = False Then

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        If LeaveDateValidation(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date) = False Then Exit Sub

                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                        Dim objAccrueBalance As New clsleavebalance_tran
                        Dim dsBalanceList As DataSet = objAccrueBalance.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                , True, CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), True, True, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(IIf(mintDeductedLeaveTypeID > 0, mintDeductedLeaveTypeID, CInt(cboLeaveType.SelectedValue))) & " AND lvleavebalance_tran.isvoid = 0", Nothing)
                        objAccrueBalance = Nothing

                        If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
                            If LeaveDateValidation(CDate(dsBalanceList.Tables(0).Rows(0)("startdate")).Date, CDate(dsBalanceList.Tables(0).Rows(0)("enddate")).Date) = False Then Exit Sub
                        End If

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsBalanceList IsNot Nothing Then dsBalanceList.Clear()
                        dsBalanceList = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    End If

                ElseIf mblnIsShortLeave Then
                    If LeaveDateValidation(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date) = False Then Exit Sub
                End If  '  ElseIf mblnIsShortLeave Then

            End If '  If mblnIsStopApplyAcrossFYELC Then

            Dim mstrMsg As String = CheckforBlockLeaveValidation()
            If mstrMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(mstrMsg.Trim(), Me)
                Exit Sub
            End If

            'Pinkal (26-Feb-2019) -- End



            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                If cboEmployee.SelectedValue IsNot Nothing AndAlso CInt(cboEmployee.SelectedValue) <> 0 Then
                    If cboLeaveType.SelectedValue IsNot Nothing AndAlso CInt(cboLeaveType.SelectedValue) <> 0 Then


                        'Pinkal (03-May-2019) -- Start
                        'Enhancement - Working on Leave UAT Changes for NMB.
                        'If mblnLvTypeSkipApproverFlow = False Then
                        If mblnLvTypeSkipApproverFlow = False AndAlso mblnConsiderExpense Then
                            'Pinkal (03-May-2019) -- End
                            If mdtFinalClaimTransaction Is Nothing OrElse mdtFinalClaimTransaction.Rows.Count <= 0 Then
                                Language.setLanguage(mstrModuleName1)
                                ExpenseConfirmation.Message = Language.getMessage(mstrModuleName1, 24, "Are you sure you want to save leave form without any leave expense ?")
                                ExpenseConfirmation.Show()

                            ElseIf mdtFinalClaimTransaction.Rows.Count > 0 Then

                                Dim drRow() As DataRow = mdtFinalClaimTransaction.Select("AUD <> 'D'")
                                If drRow.Length <= 0 Then
                                    Language.setLanguage(mstrModuleName1)
                                    ExpenseConfirmation.Message = Language.getMessage(mstrModuleName1, 24, "Are you sure you want to save leave form without any leave expense ?")
                                    ExpenseConfirmation.Show()
                                Else
                                    Call Save()
                                End If  ' If drRow.Length <= 0 Then

                            End If '  If mdtFinalClaimTransaction Is Nothing OrElse mdtFinalClaimTransaction.Rows.Count <= 0 Then
                        Else
                            Save()
                        End If  'If mblnLvTypeSkipApproverFlow = False Then

                    Else
                        Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                    End If
                Else
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Employee is compulsory information.Please Select Employee."), Me)
                End If

            Else
                If cboLeaveType.SelectedValue IsNot Nothing AndAlso CInt(cboLeaveType.SelectedValue) <> 0 Then


                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    'If mblnLvTypeSkipApproverFlow = False Then
                    If mblnLvTypeSkipApproverFlow = False AndAlso mblnConsiderExpense Then
                        'Pinkal (03-May-2019) -- End
                        If mdtFinalClaimTransaction Is Nothing OrElse mdtFinalClaimTransaction.Rows.Count <= 0 Then
                            Language.setLanguage(mstrModuleName1)
                            ExpenseConfirmation.Message = Language.getMessage(mstrModuleName1, 24, "Are you sure you want to save leave form without any leave expense ?")
                            ExpenseConfirmation.Show()

                        ElseIf mdtFinalClaimTransaction.Rows.Count > 0 Then
                            Dim drRow() As DataRow = mdtFinalClaimTransaction.Select("AUD <> 'D'")
                            If drRow.Length <= 0 Then
                                Language.setLanguage(mstrModuleName1)
                                ExpenseConfirmation.Message = Language.getMessage(mstrModuleName1, 24, "Are you sure you want to save leave form without any leave expense ?")
                                ExpenseConfirmation.Show()
                            Else
                                Call Save()
                            End If '  If drRow.Length <= 0 Then

                        End If 'ElseIf mdtFinalClaimTransaction.Rows.Count > 0 Then
                    Else
                        Save()
                    End If ' If mblnLvTypeSkipApproverFlow = False Then

                Else
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub Save()
        Try
            If Not CheckMapping() Then
                Exit Sub
            End If

            If (CheckForPeriod() = False) Then
                Exit Sub
            End If

            If Entryvalid() = False Then
                Exit Sub
            End If

            If lnkScanDocuements.Visible AndAlso (mdtAttachement Is Nothing OrElse mdtAttachement.Rows.Count <= 0) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 34, "Scan/Document(s) is compulsory information.Please attach Scan/Document(s) as per this leave type setting."), Me)
                Exit Sub
            End If

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso CBool(Session("AllowToSetRelieverOnLvFormForESS")) Then
                If CInt(cboReliever.SelectedValue) <= 0 Then
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 45, "Reliever is compulsory information.Please select reliever to apply for the leave application."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (01-Oct-2018) -- End



            Dim clsLeaveForm As New clsleaveform

            Dim sMsg As String = String.Empty

            'Pinkal (17-May-2017) -- Start
            'Enhancement - Problem solved for Consecutive Days issue Reported By PACRA.
            'sMsg = clsLeaveForm.IsValid_Form(CInt(cboEmployee.SelectedValue), _
            '                          CInt(cboLeaveType.SelectedValue), _
            '                          dtpStartDate.GetDate.Date, _
            '                          CInt((DateDiff(DateInterval.Day, dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date)).ToString()))

            sMsg = clsLeaveForm.IsValid_Form(CInt(cboEmployee.SelectedValue), _
                                      CInt(cboLeaveType.SelectedValue), _
                                      dtpStartDate.GetDate.Date, _
                                     dtpEndDate.GetDate.Date, _
                                      CInt((DateDiff(DateInterval.Day, dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date)).ToString()))

            'Pinkal (17-May-2017) -- End
            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If
            sMsg = ""

            If Not dtpEndDate.IsNull Then
                sMsg = clsLeaveForm.IsValid_SickLeaveForm(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, mintLeaveFormUnkid)
            Else
                sMsg = clsLeaveForm.IsValid_SickLeaveForm(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), dtpStartDate.GetDate.Date, Nothing, mintLeaveFormUnkid)
            End If

            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If

            If Not dtpEndDate.IsNull Then
                clsLeaveForm.LeaveForm_Alert(CInt(cboEmployee.SelectedValue), dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, mintLeaveFormUnkid)
            Else
                clsLeaveForm.LeaveForm_Alert(CInt(cboEmployee.SelectedValue), dtpStartDate.GetDate.Date, Nothing, mintLeaveFormUnkid)
            End If

            If clsLeaveForm._Message <> "" Then
                DisplayMessage.DisplayMessage(clsLeaveForm._Message, Me)
                Exit Sub
            End If

            With clsLeaveForm
                If CBool(Session("IsAutomaticIssueOnFinalApproval")) = True Then

                    Dim objLeaveType As New clsleavetype_master
                    objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                    Dim objAccruetran As New clsleavebalance_tran

                    If objLeaveType._IsPaid And objLeaveType._IsAccrueAmount Then

                        Dim drAccrue() As DataRow = Nothing

                        If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                            drAccrue = objAccruetran.GetList("AccrueTran", _
                                                             Session("Database_Name").ToString, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             Session("EmployeeAsOnDate").ToString, _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), False, False).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & CInt(Session("Fin_year")))

                        ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                            drAccrue = objAccruetran.GetList("AccrueTran", _
                                                             Session("Database_Name").ToString, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             Session("EmployeeAsOnDate").ToString, _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), True, True).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & CInt(Session("Fin_year")))
                        End If
                        If drAccrue.Length = 0 Then
                            Language.setLanguage(mstrModuleName1)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 18, "Please Enter Accrue amount for this paid leave."), Me)
                            Exit Sub
                        End If

                    ElseIf objLeaveType._IsPaid And objLeaveType._IsAccrueAmount = False And objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                        Dim drAccrue() As DataRow = Nothing
                        If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                            drAccrue = objAccruetran.GetList("AccrueTran", _
                                                             Session("Database_Name").ToString, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             Session("EmployeeAsOnDate").ToString, _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), False, False).Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & CInt(Session("Fin_year")))

                        ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                            drAccrue = objAccruetran.GetList("AccrueTran", _
                                                             Session("Database_Name").ToString, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             Session("EmployeeAsOnDate").ToString, _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), True, True).Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & CInt(Session("Fin_year")))
                        End If

                        If drAccrue.Length = 0 Then
                            objLeaveType._Leavetypeunkid = objLeaveType._DeductFromLeaveTypeunkid
                            Language.setLanguage(mstrModuleName1)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 19, "This particular leave is mapped with") & objLeaveType._Leavename & Language.getMessage(mstrModuleName1, 20, " , which does not have the accrue amount. Please add accrue amount for") & objLeaveType._Leavename & Language.getMessage(mstrModuleName1, 21, " inorder to issue leave."), Me)
                            Exit Sub
                        End If

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        drAccrue = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    ElseIf objLeaveType._IsPaid And objLeaveType._IsShortLeave Then

                        Dim drShortLeave() As DataRow = Nothing

                        If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                            drShortLeave = objAccruetran.GetList("AccrueTran", _
                                                                 Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 Session("EmployeeAsOnDate").ToString, _
                                                                 Session("UserAccessModeSetting").ToString, True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), False, False).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & CInt(Session("Fin_year")))

                        ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                            drShortLeave = objAccruetran.GetList("AccrueTran", _
                                                                 Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 Session("EmployeeAsOnDate").ToString, _
                                                                 Session("UserAccessModeSetting").ToString, True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), True, True).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & CInt(Session("Fin_year")))
                        End If
                        If drShortLeave.Length = 0 Then
                            Language.setLanguage(mstrModuleName1)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 22, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency."), Me)
                            Exit Sub
                        End If

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        drShortLeave = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objAccruetran = Nothing
                    objLeaveType = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If


                'Pinkal (29-Sep-2017) -- Start
                'Bug - Solved Leave Eligility On Leave Application for PACRA.
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
                Dim objAccrue As New clsleavebalance_tran
                Dim drEligibility() As DataRow = Nothing
                If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                    drEligibility = objAccrue.GetList("AccrueTran", _
                                                     Session("Database_Name").ToString, _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     Session("EmployeeAsOnDate").ToString, _
                                                     Session("UserAccessModeSetting").ToString, True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), False, False).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & CInt(Session("Fin_year")))

                ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                    drEligibility = objAccrue.GetList("AccrueTran", _
                                                     Session("Database_Name").ToString, _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     Session("EmployeeAsOnDate").ToString, _
                                                     Session("UserAccessModeSetting").ToString, True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), True, False, False, CInt(cboEmployee.SelectedValue), True, True).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & CInt(Session("Fin_year")))
                End If
                If drEligibility.Length > 0 Then

                    Dim mdtDate As Date = Nothing
                    If objEmp._Reinstatementdate <> Nothing Then
                        If objEmp._Appointeddate.Date < objEmp._Reinstatementdate.Date AndAlso objEmp._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                            mdtDate = objEmp._Reinstatementdate.Date
                        Else
                            mdtDate = objEmp._Appointeddate.Date
                        End If
                    Else
                        mdtDate = objEmp._Appointeddate.Date
                    End If

                    If DateDiff(DateInterval.Day, mdtDate.Date, dtpStartDate.GetDate.Date) <= CInt(drEligibility(0)("eligibilityafter")) Then
                        Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 38, "You are not eligible to apply for this leave. Reason : Eligibility to apply for this Leave Type does not match with Eligibility days on leave balance."), Me)
                        Exit Sub
                    End If
                End If
                objEmp = Nothing
                drEligibility = Nothing
                objAccrue = Nothing
                'Pinkal (29-Sep-2017) -- End

                Dim intStatusId As Integer = clsLeaveForm.GetEmployeeLastLeaveFormStatus(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), mintLeaveFormUnkid)

                If intStatusId = 2 OrElse intStatusId = 1 Then
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 17, "Sorry, you cannot apply for this leave now as your previous leave form has not been issued yet."), Me)
                    Exit Sub
                End If

                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                    Dim mdtTnAStartdate As Date = Nothing
                    Dim objPaymentTran As New clsPayment_tran
                    Dim objMaster As New clsMasterData

                    'Pinkal (01-Jan-2019) -- Start
                    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                    'Dim intPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartDate.GetDate.Date)

                    'If intPeriodId > 0 Then
                    '    Dim objTnAPeriod As New clscommom_period_Tran
                    '    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                    'End If

                    Dim intPeriodId As Integer = 0
                    If mblnConsiderLeaveOnTnAPeriod Then
                        intPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartDate.GetDate.Date)
                    Else
                        intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartDate.GetDate.Date, CInt(Session("Fin_year")), 0, False, True, Nothing, True)
                    End If

                    If intPeriodId > 0 Then
                        Dim objTnAPeriod As New clscommom_period_Tran
                        If mblnConsiderLeaveOnTnAPeriod Then
                            mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        Else
                            objTnAPeriod._Periodunkid(Session("Database_Name").ToString()) = intPeriodId
                            mdtTnAStartdate = objTnAPeriod._Start_Date
                        End If
                        objTnAPeriod = Nothing
                    End If

                    'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, dtpStartDate.GetDate.Date) > 0 Then
                    If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, dtpStartDate.GetDate.Date, "", Nothing, mblnConsiderLeaveOnTnAPeriod) > 0 Then
                        'Pinkal (01-Jan-2019) -- End
                        LeaveFormConfirmation.Message = Language.getMessage(mstrModuleName1, 32, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to apply for this leave ? ")
                        LeaveFormConfirmation.Show()
                        Exit Sub
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objMaster = Nothing
                    objPaymentTran = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If


                FormDatesConfirmation.Message = Language.getMessage(mstrModuleName1, 25, "You have applied ") & cboLeaveType.SelectedItem.Text & " " & Language.getMessage(mstrModuleName1, 26, "leave for") & _
                                                  " " & objNoofDays.Text & " " & Language.getMessage(mstrModuleName1, 27, "days , from") & " " & _
                                                dtpStartDate.GetDate.Date & " " & Language.getMessage(mstrModuleName1, 28, "to") & " " & dtpEndDate.GetDate.Date & ". " & _
                                                Language.getMessage(mstrModuleName1, 29, "Are you sure of the number of days and selected dates? If yes click YES to save the application, otherwise click NO to make corrections.")

                FormDatesConfirmation.Show()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckForPeriod() As Boolean
        Try

            If CBool(Session("ClosePayrollPeriodIfLeaveIssue")) = False Then Return True

            Dim dsList As DataSet = Nothing
            Dim objperiod As New clscommom_period_Tran
            dsList = objperiod.GetList("Period", Aruti.Data.enModuleReference.Payroll, CInt(Session("Fin_year")), CDate(Session("fin_startdate")), True, Aruti.Data.modGlobal.StatusType.Close)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString).Date <= dtpApplyDate.GetDate.Date And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString).Date >= dtpApplyDate.GetDate.Date Then
                        Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 11, "Please select correct apply date. Reason:This date period is already closed."), Me)
                        Return False

                    ElseIf eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString).Date <= dtpStartDate.GetDate.Date And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString).Date >= dtpStartDate.GetDate.Date Then
                        Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 9, "Please select correct start date. Reason:This date period is already closed."), Me)
                        Return False

                    ElseIf eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString).Date <= dtpEndDate.GetDate.Date And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString).Date >= dtpEndDate.GetDate.Date Then
                        Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 10, "Please select correct end date. Reason:This date period is already closed."), Me)
                        Return False
                    End If
                Next
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objperiod = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetProcessVisiblity()
        Try
            If CInt(Session("LeaveFormNoType")) = 1 Then
                txtFormNo.Enabled = False
            Else
                txtFormNo.Enabled = True
            End If

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                LblReliever.Visible = CBool(Session("AllowToSetRelieverOnLvFormForESS"))
                cboReliever.Visible = CBool(Session("AllowToSetRelieverOnLvFormForESS"))
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                LblReliever.Visible = False
                cboReliever.Visible = False
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            'Pinkal (25-May-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub FillCombo()
    Private Sub FillCombo(Optional ByVal xEmployeeId As Integer = 0)
        'Pinkal (11-Sep-2020) -- End
        Dim dsCombos As New DataSet

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objEmployee As New clsEmployee_Master
        Dim clsleavetype As New clsleavetype_master
        'Pinkal (11-Sep-2020) -- End
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim intEmpId As Integer = 0
            Dim intEmpId As Integer = xEmployeeId
            'Pinkal (11-Sep-2020) -- End
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                        CInt(Session("UserId")), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        CStr(Session("UserAccessModeSetting")), True, _
                                        False, "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso CBool(Session("AllowToSetRelieverOnLvFormForESS")) Then
                FillReliver()
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsCombos = clsleavetype.getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", True)
            Else
                dsCombos = clsleavetype.getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", False)
            End If
            'Pinkal (25-May-2019) -- End
            With cboLeaveType
                .DataSource = dsCombos.Tables(0)
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataBind()
                .SelectedIndex = 0
            End With

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'Shani (20-Aug-2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            clsleavetype = Nothing
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub GetValue()
    Private Sub GetValue(ByRef objLeaveForm As clsleaveform)
        'Pinkal (11-Sep-2020) -- End
        Try
            txtFormNo.Text = objLeaveForm._Formno
            cboEmployee.SelectedValue = CStr(objLeaveForm._Employeeunkid)
            Call cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)
            cboLeaveType.SelectedValue = CStr(objLeaveForm._Leavetypeunkid)
            Call cboLeaveType_SelectedIndexChanged(cboLeaveType, Nothing)

            If objLeaveForm._Formunkid > 0 Then
                dtpApplyDate.SetDate = objLeaveForm._Applydate
                dtpStartDate.SetDate = objLeaveForm._Startdate
                Call dtpStartDate_TextChanged(dtpStartDate, Nothing)
                If objLeaveForm._Returndate <> Nothing Then
                    dtpEndDate.SetDate = objLeaveForm._Returndate
                Else
                    dtpEndDate.SetDate = Nothing
                End If
                Call dtpEndDate_TextChanged(dtpEndDate, Nothing)
                Dim objAttchement As New clsScan_Attach_Documents
                ', enScanAttactRefId.LEAVEFORMS, mintLeaveFormUnkid
                Call objAttchement.GetList(Session("Document_Path").ToString, "List", "", CInt(cboEmployee.SelectedValue))
                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " AND transactionunkid = " & mintLeaveFormUnkid, "", DataViewRowState.CurrentRows).ToTable
                GetLeaveExpenseValue()
                mdtFinalClaimTransaction = mdtLeaveExpenseTran
            Else
                If CDate(Session("fin_enddate")) < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    dtpApplyDate.SetDate = CDate(Session("fin_enddate"))
                    dtpStartDate.SetDate = CDate(Session("fin_enddate"))
                    dtpEndDate.SetDate = CDate(Session("fin_enddate"))
                Else
                    dtpApplyDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpEndDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If

            End If
            txtLeaveAddress.Text = objLeaveForm._Addressonleave
            txtLeaveReason.Text = objLeaveForm._Remark


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso CBool(Session("AllowToSetRelieverOnLvFormForESS")) Then
                cboReliever.SelectedValue = objLeaveForm._RelieverEmpId.ToString()
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.


            'Pinkal (25-Mar-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then dtpApplyDate.Enabled = False


            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            dtpApplyDate.Enabled = Not CBool(Session("AllowLvApplicationApplyForBackDates"))
            dtpApplyDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (13-Jun-2019) -- End


            'Pinkal (25-Mar-2019) -- End


            'Pinkal (07-Mar-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (16-Dec-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    'Private Sub GetAsOnDateAccrue()
    '    Try
    '        Dim dsList As DataSet = Nothing
    '        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            dsList = objLvAccrue.GetLeaveBalanceInfo(dtpApplyDate.GetDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, )

    '        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            objLvAccrue._DBStartdate = CDate(Session("fin_startdate")).Date
    '            objLvAccrue._DBEnddate = CDate(Session("fin_enddate")).Date
    '            dsList = objLvAccrue.GetLeaveBalanceInfo(dtpApplyDate.GetDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, Nothing, Nothing, True, True)

    '        End If

    '        If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
    '            objAsonDateAccrue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
    '            objAsonDateBalance.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) - (CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")))).ToString("#0.00")
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Private Sub GetAsOnDateAccrue()
        Try
            Dim objLvAccrue As New clsleavebalance_tran
            Dim mdecAsonDateAccrue As Decimal = 0
            Dim mdecAsonDateBalance As Decimal = 0

            'Pinkal (06-Apr-2018) -- Start
            'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
            'objLvAccrue.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
            '                                             , CInt(Session("LeaveBalanceSetting")), CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
            '                                             , dtpEndDate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
            '                                             , mdecAsonDateAccrue, mdecAsonDateBalance)

            objLvAccrue.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                         , CInt(Session("LeaveBalanceSetting")), CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
                                                         , dtpEndDate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                         , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))

            'Pinkal (06-Apr-2018) -- End


            objAsonDateAccrue.Text = mdecAsonDateAccrue.ToString()
            objAsonDateBalance.Text = mdecAsonDateBalance.ToString()
            objLvAccrue = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetAsOnDateAccrue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (16-Dec-2016) -- End

    Private Function GetNoOfHolidays(ByVal intLeaveTypeId As Integer, ByVal startdate As Date, ByVal enddate As Date) As Decimal
        Dim mdcDayCount As Decimal = 0
        Dim arHolidayDate() As String = Nothing
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim clsleavetype As New clsleavetype_master
        Dim objLeaveForm As New clsleaveform
        'Pinkal (11-Sep-2020) -- End
        Try

            clsleavetype._Leavetypeunkid = intLeaveTypeId
            If clsleavetype._Isissueonholiday = False Then
                mdcDayCount = objLeaveForm.GetNoOfHolidays(CInt(cboEmployee.SelectedValue), startdate, enddate, arHolidayDate, clsleavetype._Isissueonweekend, clsleavetype._IsLeaveHL_onWK)

                Dim objReccurent As New clsemployee_holiday
                For i As Integer = 0 To CInt(DateDiff(DateInterval.Day, startdate, enddate.AddDays(1)) - 1)
                    If arHolidayDate.Contains(eZeeDate.convertDate(startdate.AddDays(i).Date)) = False Then
                        If objReccurent.GetEmployeeReCurrentHoliday(CInt(cboEmployee.SelectedValue), startdate.AddDays(i).Date) Then
                            mdcDayCount += 1
                        End If
                    End If
                Next
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objReccurent = Nothing
                'Pinkal (11-Sep-2020) -- End
            Else
                mdcDayCount = 0
            End If

            If clsleavetype._Isissueonweekend = False Then
                mdcDayCount += objLeaveForm.GetNoOfWeekend(CInt(cboEmployee.SelectedValue), startdate, arHolidayDate, enddate, clsleavetype._Isissueonholiday, clsleavetype._IsLeaveHL_onWK)
            Else
                mdcDayCount += 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            clsleavetype = Nothing
            objLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
        Return mdcDayCount
    End Function

    Protected Sub BlankObjects()
        Try
            txtLeaveAddress.Text = ""
            txtFormNo.Text = ""
            cboEmployee.SelectedIndex = 0
            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            Call cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs)
            'Shani (20-Aug-2016) -- End
            cboLeaveType.SelectedIndex = 0
            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs)
            'Shani (20-Aug-2016) -- End
            txtLeaveReason.Text = ""
            dtpApplyDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpEndDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            objNoofDays.Text = "0"
            objBalance.Text = "0.00"
            objLeaveAccrue.Text = "0.00"
            objAsonDateAccrue.Text = "0.00"
            objAsonDateBalance.Text = "0.00"
            If cboEmployee.Enabled = False Then cboEmployee.Enabled = True
            If cboLeaveType.Enabled = False Then cboLeaveType.Enabled = True
            mdtFinalClaimTransaction = Nothing

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            mdtAttachement = Nothing
            mdtFinalClaimAttchment = Nothing
            mdtChildClaimAttachment = Nothing
            'Shani (20-Aug-2016) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso CBool(Session("AllowToSetRelieverOnLvFormForESS")) Then
                cboReliever.SelectedValue = "0"
            End If
            'Pinkal (01-Oct-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function Setvalue(ByVal clsLeaveForm As clsleaveform) As Boolean
        Try

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            Dim objBudget As New clsBudgetEmp_timesheet()
            If mdtDayFration IsNot Nothing AndAlso mdtDayFration.Rows.Count > 0 Then
                If objBudget.GetEmpTotalWorkingHrsForTheDay(dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, CInt(cboEmployee.SelectedValue)) > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 40, "You cannot apply for this leave form.Reason : This employee is already applied for employee budget timesheet for this tenure."), Me)
                    objBudget = Nothing
                    Return False
                End If
            End If
            objBudget = Nothing
            'Pinkal (13-Oct-2017) -- End


            With clsLeaveForm
                ._Formno = txtFormNo.Text
                ._Addressonleave = txtLeaveAddress.Text

                If dtpApplyDate.IsNull = True Then
                    DisplayMessage.DisplayMessage("Apply Date is Compulsory information.Please Select Apply Date.", Me)
                    dtpApplyDate.Focus()
                    Return False
                Else
                    ._Applydate = dtpApplyDate.GetDate.Date
                End If

                ._Employeeunkid = CInt(Val(cboEmployee.SelectedValue))
                ._Leavetypeunkid = CInt(Val(cboLeaveType.SelectedValue))
                ._Remark = txtLeaveReason.Text

                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = ._Leavetypeunkid

                If dtpEndDate.IsNull = True AndAlso objLeaveType._IsSickLeave = False Then
                    DisplayMessage.DisplayMessage("End Date is Compulsory information.Please Select End date.", Me)
                    dtpEndDate.Focus()
                    Return False
                ElseIf dtpEndDate.IsNull = True AndAlso objLeaveType._IsSickLeave Then
                    ._Returndate = Nothing
                Else
                    ._Returndate = dtpEndDate.GetDate.Date
                End If

                If dtpStartDate.IsNull = True Then
                    DisplayMessage.DisplayMessage("Start Date is Compulsory information.Please Select Start date.", Me)
                    dtpStartDate.Focus()
                    Return False
                Else
                    ._Startdate = dtpStartDate.GetDate.Date
                End If
                ._Statusunkid = 2
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    ._Userunkid = CInt(Session("UserId"))
                Else
                    ._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                    ._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                End If


                If mdtDayFration IsNot Nothing AndAlso mdtDayFration.Rows.Count <= 0 Then
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 23, "You cannot create this leave form.Reason : As these days are falling in holiday(s) and weekend."), Me)
                    Return False
                End If
                Dim objAccruetran As New clsleavebalance_tran
                Dim drAccrue() As DataRow = Nothing

                objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)


                'Pinkal (28-Nov-2016) -- Start
                'Enhancement - Bug Solved When employee applied for Leave Application and Leave Balance Exceeded,Intimation not promt to employee for CCK 

                'CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False))

                If objLeaveType._IsPaid And (objLeaveType._IsAccrueAmount OrElse objLeaveType._IsAccrueAmount = False) And objLeaveType._IsShortLeave Then
                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                        drAccrue = objAccruetran.GetList("AccrueTran", _
                                                         Session("Database_Name").ToString, _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         Session("EmployeeAsOnDate").ToString, _
                                                         Session("UserAccessModeSetting").ToString, True, _
                                                         CBool(Session("IsIncludeInactiveEmp")), True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), False, _
                                                         CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select(" leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))

                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                        drAccrue = objAccruetran.GetList("AccrueTran", _
                                                         CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         CStr(Session("EmployeeAsOnDate")), _
                                                         CStr(Session("UserAccessModeSetting")), True, _
                                                         CBool(Session("IsIncludeInactiveEmp")), True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), False, CInt(cboEmployee.SelectedValue), True, True).Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))
                    End If

                    'Pinkal (28-Nov-2016) -- End

                    If drAccrue.Length = 0 Then
                        objAccruetran._Yearunkid = CInt(Session("Fin_year"))
                        objAccruetran._Employeeunkid = clsLeaveForm._Employeeunkid
                        objAccruetran._Leavetypeunkid = clsLeaveForm._Leavetypeunkid
                        objAccruetran._AccrueSetting = enAccrueSetting.Issue_Balance
                        objAccruetran._AccrueAmount = objLeaveType._MaxAmount
                        objAccruetran._UptoLstYr_AccrueAmout = objLeaveType._MaxAmount
                        objAccruetran._IsshortLeave = objLeaveType._IsShortLeave
                        objAccruetran._Ispaid = objLeaveType._IsPaid
                        objAccruetran._Startdate = CDate(Session("fin_startdate")).Date
                        objAccruetran._Enddate = CDate(Session("fin_enddate")).Date
                        If CInt(Session("UserId")) > 0 Then
                            objAccruetran._Userunkid = clsLeaveForm._Userunkid
                        Else
                            objAccruetran._Userunkid = CInt(Session("EmployeeId"))
                        End If
                        If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                            objAccruetran._IsOpenELC = True
                            objAccruetran._IsELC = True
                        End If
                        If objAccruetran.Insert(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CStr(Session("EmployeeAsOnDate")), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), ) = False Then
                            DisplayMessage.DisplayMessage("Setvalue :- " & objAccruetran._Message, Me)
                            Return False
                        End If

                        Dim mdblAmount As Decimal = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                        If objLeaveType._MaxAmount < mdblAmount Then
                            Language.setLanguage(mstrModuleName1)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                            Return False
                        End If
                    Else
                        Dim mdblAmount As Decimal = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                        If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                            Language.setLanguage(mstrModuleName1)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                            Return False
                        End If
                    End If

                ElseIf objLeaveType._IsPaid AndAlso objLeaveType._IsAccrueAmount AndAlso objLeaveType._IsShortLeave = False Then


                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then


                        drAccrue = objAccruetran.GetList("AccrueTran", _
                                                         CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         CStr(Session("EmployeeAsOnDate")), _
                                                         CStr(Session("UserAccessModeSetting")), True, _
                                         CBool(Session("IsIncludeInactiveEmp")), True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))


                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                        drAccrue = objAccruetran.GetList("AccrueTran", _
                                                         CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         CStr(Session("EmployeeAsOnDate")), _
                                                         CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))

                    End If

                    'Pinkal (28-Nov-2016) -- End

                    If drAccrue.Length > 0 Then

                        If CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                            Dim mdblAmount As Decimal = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                            If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                                Language.setLanguage(mstrModuleName1)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                                Return False
                            End If


                        ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then
                            Dim mdblAmount As Decimal = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            If CDec(objAsonDateBalance.Text) < mdblAmount Then

                                'Pinkal (05-Jun-2020) -- Start
                                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 37, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type."), Me)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 37, "You cannot apply for this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                                'Pinkal (05-Jun-2020) -- End


                                Return False
                            End If

                            'Pinkal (08-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                        ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Issue_Balance OrElse CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then

                            Dim mdblAmount As Decimal = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            If CDec(drAccrue(0)("maxnegative_limit")) <> 0 AndAlso CDec(drAccrue(0)("maxnegative_limit")) > CDec(CDec(drAccrue(0)("accrue_amount")) - (CDec(drAccrue(0)("issue_amount")) + mdblAmount)) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), Me)
                                Return False
                                'Pinkal (08-Oct-2018) -- End

                            End If


                            'Pinkal (20-Apr-2022) -- Start
                            'Enhancement Prevision Air  - Doing Leave module Enhancement.	
                        ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Issue_On_Exceeding_Balance_Ason_Date Then

                            Dim mdblAmount As Decimal = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                                Language.setLanguage(mstrModuleName1)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                                Return False
                            End If

                            Dim mdecAsonDateAccrue As Decimal = 0
                            Dim mdecAsonDateBalance As Decimal = 0

                            objAccruetran.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                    , CInt(Session("LeaveBalanceSetting")), CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
                                                    , dtpEndDate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                    , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))

                            If CDec(drAccrue(0)("maxnegative_limit")) <> 0 AndAlso CDec(drAccrue(0)("maxnegative_limit")) > CDec(mdecAsonDateBalance - mdblAmount) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), Me)
                                Return False
                                'Pinkal (08-Oct-2018) -- End

                            End If
                            'Pinkal (20-Apr-2022) -- End


                        End If

                    End If

                    'Pinkal (02-Jan-2018) -- Start
                    'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                    'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                    'Control to be put on application/not just issuing. employee to be warned and stopped.

                ElseIf objLeaveType._DeductFromLeaveTypeunkid > 0 AndAlso objLeaveType._IsPaid AndAlso objLeaveType._IsAccrueAmount = False AndAlso objLeaveType._IsShortLeave = False Then


                    'Pinkal (06-Aug-2019) -- Start
                    'Defect [NMB] - Worked on Block Leave which didn't check Annual Leave Balance before annual balance exceeded and employee can able to apply leave application.

                    'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                    '    drAccrue = objAccruetran.GetList("AccrueTran", CStr(Session("Database_Name")), _
                    '                                                    CInt(Session("UserId")), _
                    '                                                    CInt(Session("Fin_year")), _
                    '                                                    CInt(Session("CompanyUnkId")), _
                    '                                                    CStr(Session("EmployeeAsOnDate")), _
                    '                                                    CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                    '                                                    CBool(Session("IsIncludeInactiveEmp")), True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._Leavetypeunkid))
                    'ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                    '    drAccrue = objAccruetran.GetList("AccrueTran", CStr(Session("Database_Name")), _
                    '                                                      CInt(Session("UserId")), _
                    '                                                      CInt(Session("Fin_year")), _
                    '                                                      CInt(Session("CompanyUnkId")), _
                    '                                                      CStr(Session("EmployeeAsOnDate")), _
                    '                                                      CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                    '                                                      CBool(Session("IsIncludeInactiveEmp")), True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._Leavetypeunkid))
                    'End If


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                    'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                    '    drAccrue = objAccruetran.GetList("AccrueTran", CStr(Session("Database_Name")), _
                    '                                                    CInt(Session("UserId")), _
                    '                                                    CInt(Session("Fin_year")), _
                    '                                                    CInt(Session("CompanyUnkId")), _
                    '                                                    CStr(Session("EmployeeAsOnDate")), _
                    '                                                    CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                    '                                                    CBool(Session("IsIncludeInactiveEmp")), True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid))
                    'ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                    '    drAccrue = objAccruetran.GetList("AccrueTran", CStr(Session("Database_Name")), _
                    '                                                      CInt(Session("UserId")), _
                    '                                                      CInt(Session("Fin_year")), _
                    '                                                      CInt(Session("CompanyUnkId")), _
                    '                                                      CStr(Session("EmployeeAsOnDate")), _
                    '                                                      CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                    '                                                      CBool(Session("IsIncludeInactiveEmp")), True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid))
                    'End If

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        drAccrue = objAccruetran.GetList("AccrueTran", CStr(Session("Database_Name")), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        CStr(Session("EmployeeAsOnDate")), _
                                                                        CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), _
                                                                        False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid))

                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                        drAccrue = objAccruetran.GetList("AccrueTran", CStr(Session("Database_Name")), _
                                                                          CInt(Session("UserId")), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          CStr(Session("EmployeeAsOnDate")), _
                                                                          CStr(IIf(CInt(Session("UserId")) > 0, CStr(Session("UserAccessModeSetting")), "")), True, _
                                                                          CBool(Session("IsIncludeInactiveEmp")), True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), _
                                                                          False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid))
                    End If



                    'Pinkal (11-Sep-2020) -- End



                    Dim mdtStartDate As Date = Nothing
                    Dim mdtEndDate As Date = Nothing

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC AndAlso drAccrue.Length > 0 Then
                        mdtStartDate = CDate(drAccrue(0)("startdate")).Date
                        mdtEndDate = CDate(drAccrue(0)("enddate")).Date
                    End If


                    Dim mdblAmount As Decimal = 0

                    If drAccrue.Length > 0 Then

                        If CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                            mdblAmount = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                                Return False
                            End If

                        ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                            mdblAmount = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            'Pinkal (11-Jun-2020) -- Start
                            'Bug  Freight Forwarders Ltd (0004733) - Leave Types Deducted from Annual Leave/Accrue Type leaves not Allowing user to Apply
                            Dim mdecAsonDateAccrue As Decimal = 0
                            Dim mdecAsonDateBalance As Decimal = 0


                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                            'objLvAccrue.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                            '                        , CInt(Session("LeaveBalanceSetting")), CInt(cboEmployee.SelectedValue), CInt(objLeaveType._DeductFromLeaveTypeunkid) _
                            '                        , dtpEndDate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                            '                        , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))

                            objAccruetran.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                    , CInt(Session("LeaveBalanceSetting")), CInt(cboEmployee.SelectedValue), CInt(objLeaveType._DeductFromLeaveTypeunkid) _
                                                    , dtpEndDate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                    , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))


                            'Pinkal (11-Sep-2020) -- End


                            'If CDec(objAsonDateBalance.Text) < mdblAmount Then
                            If mdecAsonDateBalance < mdblAmount Then
                                'Pinkal (11-Jun-2020) -- End
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 37, "You cannot apply for this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                                Return False
                            End If


                        ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Issue_Balance OrElse CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then

                            mdblAmount = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            If CDec(drAccrue(0)("maxnegative_limit")) <> 0 AndAlso CDec(drAccrue(0)("maxnegative_limit")) > CDec(CDec(drAccrue(0)("accrue_amount")) - (CDec(drAccrue(0)("issue_amount")) + mdblAmount)) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), Me)
                                Return False
                            End If


                            'Pinkal (20-Apr-2022) -- Start
                            'Enhancement Prevision Air  - Doing Leave module Enhancement.	

                        ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Issue_On_Exceeding_Balance_Ason_Date Then

                            mdblAmount = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                            If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                                Return False
                            End If

                            Dim mdecAsonDateAccrue As Decimal = 0
                            Dim mdecAsonDateBalance As Decimal = 0

                            objAccruetran.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                    , CInt(Session("LeaveBalanceSetting")), CInt(cboEmployee.SelectedValue), CInt(objLeaveType._DeductFromLeaveTypeunkid) _
                                                    , dtpEndDate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                    , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))

                            If CDec(drAccrue(0)("maxnegative_limit")) <> 0 AndAlso CDec(drAccrue(0)("maxnegative_limit")) > CDec(mdecAsonDateBalance - mdblAmount) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), Me)
                                Return False
                            End If

                            'Pinkal (20-Apr-2022) -- End


                        End If

                    Else

                        Dim mdblIssue As Decimal = 0
                        Dim objLeaveIssueTran As New clsleaveissue_Tran
                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                            mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(cboEmployee.SelectedValue), objLeaveType._Leavetypeunkid, CInt(Session("Fin_year")), Nothing, mdtStartDate, mdtEndDate)
                        Else
                            mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(cboEmployee.SelectedValue), objLeaveType._Leavetypeunkid, CInt(Session("Fin_year")))
                        End If

                        mdblAmount = CDec(mdtDayFration.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                        If objLeaveType._MaxAmount < (mdblIssue + mdblAmount) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                            Return False
                        End If

                    End If


                    'Pinkal (06-Aug-2019) -- End

                    'Pinkal (02-Jan-2018) -- End

                End If

                ._dtExpense = mdtFinalClaimTransaction
                ._ObjClaimRequestMst = CType(Session("ObjClaimMst"), clsclaim_request_master)
                ._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))


                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee AndAlso CBool(Session("AllowToSetRelieverOnLvFormForESS")) Then
                    ._RelieverEmpId = CInt(cboReliever.SelectedValue)
                End If
                ._WebFrmName = "frmLeaveForm_AddEdit"
                ._WebClientIP = CStr(Session("IP_ADD"))
                ._WebHostName = CStr(Session("HOST_NAME"))
                'Pinkal (01-Oct-2018) -- End


                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                ._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod
                'Pinkal (01-Jan-2019) -- End

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objAccruetran = Nothing
                objLeaveType = Nothing
                'Pinkal (11-Sep-2020) -- End


            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    Private Sub FillReliver()
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objEmployee As New clsEmployee_Master
        'Pinkal (11-Sep-2020) -- End
        Try
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)

            Dim mstrSearching As String = "AND hremployee_master.employeeunkid <> " & CInt(cboEmployee.SelectedValue) & " "


            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If Session("RelieverAllocation") IsNot Nothing AndAlso Session("RelieverAllocation").ToString().Length > 0 Then

                Dim arAllocations() As String = Session("RelieverAllocation").ToString().Split(CChar(","))
                If arAllocations.Length > 0 Then

                    For i As Integer = 0 To arAllocations.Length - 1

                        Select Case CType(arAllocations(i), enAllocation)

                            Case enAllocation.BRANCH

                                If objEmployee._Stationunkid > 0 Then
                                    mstrSearching &= "AND T.stationunkid = " & objEmployee._Stationunkid & " "
                                End If

                            Case enAllocation.DEPARTMENT_GROUP

                                If objEmployee._Deptgroupunkid > 0 Then
                                    mstrSearching &= "AND T.deptgroupunkid = " & objEmployee._Deptgroupunkid & " "
                                End If

                            Case enAllocation.DEPARTMENT

                                If objEmployee._Departmentunkid > 0 Then
                                    mstrSearching &= "AND T.departmentunkid = " & objEmployee._Departmentunkid & " "
                                End If

                            Case enAllocation.SECTION_GROUP

                                If objEmployee._Sectiongroupunkid > 0 Then
                                    mstrSearching &= "AND T.sectiongroupunkid = " & objEmployee._Sectiongroupunkid & " "
                                End If

                            Case enAllocation.SECTION

                                If objEmployee._Sectionunkid > 0 Then
                                    mstrSearching &= "AND T.sectionunkid = " & objEmployee._Sectionunkid & " "
                                End If

                            Case enAllocation.UNIT_GROUP

                                If objEmployee._Unitgroupunkid > 0 Then
                                    mstrSearching &= "AND T.unitgroupunkid = " & objEmployee._Unitgroupunkid & " "
                                End If

                            Case enAllocation.UNIT

                                If objEmployee._Unitunkid > 0 Then
                                    mstrSearching &= "AND T.unitunkid = " & objEmployee._Unitunkid & " "
                                End If

                            Case enAllocation.TEAM

                                If objEmployee._Teamunkid > 0 Then
                                    mstrSearching &= "AND T.teamunkid = " & objEmployee._Teamunkid & " "
                                End If

                            Case enAllocation.JOB_GROUP

                                If objEmployee._Jobgroupunkid > 0 Then
                                    mstrSearching &= "AND J.jobgroupunkid = " & objEmployee._Jobgroupunkid & " "
                                End If

                            Case enAllocation.JOBS

                                If objEmployee._Jobunkid > 0 Then
                                    mstrSearching &= "AND J.jobunkid = " & objEmployee._Jobunkid & " "
                                End If

                            Case enAllocation.CLASS_GROUP

                                If objEmployee._Classgroupunkid > 0 Then
                                    mstrSearching &= "AND T.classgroupunkid = " & objEmployee._Classgroupunkid & " "
                                End If

                            Case enAllocation.CLASSES

                                If objEmployee._Classunkid > 0 Then
                                    mstrSearching &= "AND T.classunkid = " & objEmployee._Classunkid & " "
                                End If

                            Case enAllocation.COST_CENTER

                                If objEmployee._Costcenterunkid > 0 Then
                                    mstrSearching &= "AND C.costcenterunkid = " & objEmployee._Costcenterunkid & " "
                                End If

                        End Select

                    Next

                End If

                'If objEmployee._Gradegroupunkid > 0 Then
                '    mstrSearching &= "AND G.gradegroupunkid = " & objEmployee._Gradegroupunkid & " "
                'End If

                'If objEmployee._Gradeunkid > 0 Then
                '    mstrSearching &= "AND G.gradeunkid = " & objEmployee._Gradeunkid & " "
                'End If

                'If objEmployee._Gradelevelunkid > 0 Then
                '    mstrSearching &= "AND G.gradelevelunkid = " & objEmployee._Gradelevelunkid & " "
                'End If

            End If

            'Pinkal (13-Mar-2019) -- End

            If mstrSearching.Trim.Length > 0 Then
                mstrSearching = mstrSearching.Substring(3)
            End If


            Dim dsEmpList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                    CInt(Session("UserId")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                    CStr(Session("UserAccessModeSetting")), True, _
                                    False, "Employee", True, 0, , , , , , , , , , , , , , mstrSearching, , False)

            With cboReliever
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsEmpList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsEmpList IsNot Nothing Then dsEmpList.Clear()
            dsEmpList = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Function SendEmailForSkipApproverFlow(ByVal clsleaveform As clsleaveform, ByVal iLogimModeId As Integer) As Boolean
        Try
            Dim objCommonCode As New CommonCodes
            Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
            Dim strPath As String = ""

            Dim objEmp As New clsEmployee_Master
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = clsleaveform._Employeeunkid
            clsleaveform._EmployeeCode = objEmp._Employeecode
            clsleaveform._EmployeeFirstName = objEmp._Firstname
            clsleaveform._EmployeeMiddleName = objEmp._Othername
            clsleaveform._EmployeeSurName = objEmp._Surname
            clsleaveform._EmpMail = objEmp._Email
            objEmp = Nothing


            strPath = objCommonCode.Export_ELeaveForm(Path, clsleaveform._Formno, clsleaveform._Employeeunkid, clsleaveform._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                     , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, False _
                                                                                                     , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))


            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            'clsleaveform.SendMailToEmployee(clsleaveform._Employeeunkid, cboLeaveType.SelectedItem.Text, clsleaveform._Startdate.Date, clsleaveform._Returndate.Date _
            '                                                 , clsleaveform._Statusunkid, CInt(Session("CompanyUnkId")), "", Path, strPath, iLogimModeId, 0, CInt(Session("UserId")), "", 0)

            clsleaveform.SendMailToEmployee(clsleaveform._Employeeunkid, cboLeaveType.SelectedItem.Text, clsleaveform._Startdate.Date, clsleaveform._Returndate.Date _
                                                                , clsleaveform._Statusunkid, CInt(Session("CompanyUnkId")), "", Path, strPath, iLogimModeId, 0, CInt(Session("UserId")), "", _
                                                                , clsleaveform._Formno)
            'Pinkal (01-Apr-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private Sub SendLeaveApproverNotification(ByVal objLvform As clsleaveform)
        Try
            objLvform.GetData()
            objLvform._WebFrmName = "frmLeaveForm_AddEdit"
            objLvform._WebClientIP = CStr(Session("IP_ADD"))
            objLvform._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuLeaveInformation"

            Dim iLogimModeId As Integer = CInt(Session("LoginBy"))


            objLvform.SendMailToApprover(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), _
                                            CInt(cboEmployee.SelectedValue), _
                                            CInt(cboLeaveType.SelectedValue), _
                                            objLvform._Formunkid, _
                                            objLvform._Formno, False, _
                                            CStr(Session("LeaveApproverForLeaveType")), _
                                            CStr(Session("ArutiSelfServiceURL")), _
                                            iLogimModeId, CInt(Session("Employeeunkid")), _
                                            mblnLvTypeSkipApproverFlow)

            If mblnLvTypeSkipApproverFlow Then
                SendEmailForSkipApproverFlow(objLvform, iLogimModeId)
            End If

            mintLeaveFormUnkid = 0
            mintClaimRequestMasterId = 0
            mintClaimRequestTranId = 0
            If mdtFinalClaimTransaction IsNot Nothing Then mdtFinalClaimTransaction.Rows.Clear()
            mdtDayFration = Nothing
            dgvData.DataSource = Nothing
            dgvData.DataBind()
            Session("ObjClaimMst") = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (20-Nov-2018) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Function LeaveDateValidation(ByVal mdtStartDate As Date, ByVal mdtenddate As Date) As Boolean
        Try
            If dtpApplyDate.GetDate.Date < mdtStartDate.Date OrElse dtpApplyDate.GetDate.Date > mdtenddate.Date Then

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 50, "Please select proper") & " " & lblApplyDate.Text & "." & lblApplyDate.Text & " " & Language.getMessage(mstrModuleName1, 51, "should be between") & " " & mdtStartDate.Date & " and " & mdtenddate.Date & ".", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 56, "Sorry ! This leave type cannot overlap to next year."), Me)
                'Pinkal (03-May-2019) -- End

                Return False
            ElseIf dtpStartDate.GetDate.Date < mdtStartDate.Date OrElse dtpStartDate.GetDate.Date > mdtenddate.Date Then

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 50, "Please select proper") & " " & lblStartDate.Text & "." & lblStartDate.Text & " " & Language.getMessage(mstrModuleName1, 51, "should be between") & " " & mdtStartDate.Date & " and " & mdtenddate.Date & ".", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 56, "Sorry ! This leave type cannot overlap to next year."), Me)
                'Pinkal (03-May-2019) -- End

                Return False
            ElseIf dtpEndDate.GetDate.Date < mdtStartDate.Date OrElse dtpEndDate.GetDate.Date > mdtenddate.Date Then

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 50, "Please select proper") & " " & lblReturnDate.Text & "." & lblReturnDate.Text & " " & Language.getMessage(mstrModuleName1, 51, "should be between") & " " & mdtStartDate.Date & " and " & mdtenddate.Date & ".", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 56, "Sorry ! This leave type cannot overlap to next year."), Me)
                'Pinkal (03-May-2019) -- End

                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Function CheckforBlockLeaveValidation() As String
        Dim mstrMsg As String = ""
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveForm As New clsleaveform
        'Pinkal (11-Sep-2020) -- End
        Try
            Dim objLvType As New clsleavetype_master
            Dim mstrLeaveTypeIds As String = objLvType.GetDeductdToLeaveTypeIDs(CInt(cboLeaveType.SelectedValue))
            If mstrLeaveTypeIds.Trim.Length > 0 Then
                Dim ar() As String = mstrLeaveTypeIds.Trim.ToString().Split(CChar(","))
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        objLvType._Leavetypeunkid = CInt(ar(i))
                        If objLvType._IsBlockLeave = False Then Continue For
                        If objLeaveForm.IsLeaveForm_Exists(CInt(cboEmployee.SelectedValue), CInt(ar(i))) Then
                            If objLeaveForm.GetLeaveFormForBlockLeaveStatus(CInt(cboEmployee.SelectedValue), CInt(ar(i))) <> 7 Then 'Issue
                                'Pinkal (03-May-2019) -- Start
                                'Enhancement - Working on Leave UAT Changes for NMB.
                                'mstrMsg = Language.getMessage(mstrModuleName1, 47, "You cannot apply leave form for this leave type.Reason : mandatory") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName1, 48, "has not been applied or previously applied") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName1, 49, "(s) are not issued.")
                                mstrMsg = Language.getMessage(mstrModuleName1, 54, "Sorry ! You have to apply") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName1, 55, "before applying this type of leave.")
                                'Pinkal (03-May-2019) -- End
                            End If
                        Else
                            'Pinkal (03-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.
                            'mstrMsg = Language.getMessage(mstrModuleName1, 47, "You cannot apply leave form for this leave type.Reason : mandatory") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName1, 48, "has not been applied or previously applied") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName1, 49, "(s) are not issued.")
                            mstrMsg = Language.getMessage(mstrModuleName1, 54, "Sorry ! You have to apply") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName1, 55, "before applying this type of leave.")
                            'Pinkal (03-May-2019) -- End
                            Exit For
                        End If
                    Next
                End If
            End If
            objLvType = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mstrMsg = ex.Message
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
        Return mstrMsg
    End Function

    'Pinkal (26-Feb-2019) -- End


#Region "Leave Expense"

    Private Sub SetLeaveExpenseVisibility()
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            End If

            'If mblnPaymentApprovalwithLeaveApproval AndAlso mintClaimRequestMasterId > 0 Then
            '    cboReference.Enabled = False
            '    cboEmployee.Enabled = False
            'End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLeaveExpenseVisibility" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillLeaveExpenseCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim dsCombo As New DataSet
        Try


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            Dim blnApplyFilter As Boolean = True
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnApplyFilter = False
            End If

            dsCombo = objEMaster.GetEmployeeList(CStr(Session("Database_Name")), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "List", False, _
                                                 CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , , , , blnApplyFilter)

            'Pinkal (22-Mar-2016) -- End


            With cboExpEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEMaster = Nothing

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List")

            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(enExpFromModuleID.FROM_LEAVE)
            End With
            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())



            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If CBool(Session("SectorRouteAssignToEmp")) = True AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtTable
                    .DataBind()
                End With
                objAssignEmp = Nothing

            ElseIf CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objcommonMst As New clsCommon_Master
                dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
                With cboSectorRoute
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
                objcommonMst = Nothing
            End If

            'Pinkal (20-Feb-2019) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtCostCenter As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtCostCenter = dsCombo.Tables(0)
                Else
                    dtCostCenter = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtCostCenter
                .DataBind()
            End With
            'Pinkal (20-Nov-2018) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objCostCenter = Nothing
            'Pinkal (11-Sep-2020) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillLeaveExpenCombo" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            objEMaster = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub GetLeaveExpenseValue()
        Try
            Dim objClaimMst As New clsclaim_request_master

            mintClaimRequestMasterId = objClaimMst.GetClaimRequestMstIDFromLeaveForm(CInt(IIf(mintLeaveFormUnkid > 0, mintLeaveFormUnkid, -1)))

            objClaimMst._Crmasterunkid = mintClaimRequestMasterId
            txtClaimNo.Text = objClaimMst._Claimrequestno


            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            If txtClaimRemark.Text.Trim.Length <= 0 Then
                txtClaimRemark.Text = objClaimMst._Claim_Remark
            End If
            'Pinkal (20-May-2019) -- End



            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            'If objClaimMst._Transactiondate <> Nothing Then
            '    dtpDate.SetDate = objClaimMst._Transactiondate.Date
            'Else
            '    'dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            '    dtpDate.SetDate = dtpApplyDate.GetDate.Date
            'End If

            If objClaimMst._Transactiondate <> Nothing Then
                dtpDate.SetDate = objClaimMst._Transactiondate
            Else
                dtpDate.SetDate = dtpApplyDate.GetDateTime
            End If
            'Pinkal (20-Nov-2018) -- End

            objClaimMst = Nothing
            dtpDate.Enabled = False
            'Pinkal (30-Apr-2018) - End


            Dim objClaimTran As New clsclaim_request_tran
            objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId
            mdtLeaveExpenseTran = objClaimTran._DataTable
            objClaimTran = Nothing


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            If mdtLeaveExpenseTran IsNot Nothing AndAlso mdtLeaveExpenseTran.Rows.Count > 0 Then
                Dim xBudgetMandatoryCount As Integer = mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'If xBudgetMandatoryCount > 0 Then
                Dim xNonHRExpenseCount As Integer = mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                    btnAdd.Enabled = False
                    btnEdit.Enabled = False
                    btnSaveAddEdit.Enabled = False
                    dgvData.Columns(0).Visible = False   'Edit
                    dgvData.Columns(1).Visible = False   'Delete
                    dgvData.Columns(2).Visible = False   'Attachment
                End If
                'Pinkal (04-Feb-2019) -- End
            End If
            '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            'Pinkal (20-Nov-2018) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtLeaveExpenseTran
            Dim mdtTran As DataTable = mdtLeaveExpenseTran.Copy()
            'Pinkal (11-Sep-2020) -- End

            Dim mdView As New DataView
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            If mdView.ToTable.Rows.Count > dgvData.PageSize Then
                dgvData.AllowPaging = True
            Else
                dgvData.AllowPaging = False
            End If
            dgvData.DataSource = mdView
            dgvData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D'")), CStr(Session("fmtCurrency")))
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Expense:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboExpense.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 5, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboExpense.Focus()
                Return False
            End If

            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                cboSectorRoute.Focus()
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Exit Function
                Return False
                'Pinkal (11-Sep-2020) -- End
            End If


            'Pinkal (16-Mar-2018) -- Start
            'Bug - Support Id :2098 Error when posting claim in Pacra.
            If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                'Pinkal (16-Mar-2018) -- End
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 6, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQty.Focus()
                Return False
            End If

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(txtQty.Text) Then
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                Return False
            End If
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Return False
            End If
            'Pinkal (10-Jun-2020) -- End



            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            'If IsNumeric(txtUnitPrice.Text) Then
            '    If CInt(txtUnitPrice.Text) <= 0 Then
            '        Language.setLanguage(mstrModuleName5)
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), Me)
            '        txtUnitPrice.Focus()
            '        Return False
            '    End If
            'Else
            '    Language.setLanguage(mstrModuleName5)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), Me)
            '    txtUnitPrice.Focus()
            '    Return False
            'End If
            'Pinkal (22-Oct-2018) -- End

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 46, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboCurrency.Focus()
                Return False
            End If

            'If objExpense._IsBudgetMandatory = False Then

            Dim blnIsHRExpense As Boolean = True
            If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                blnIsHRExpense = objExpense._IsHRExpense
            End If

            If blnIsHRExpense Then

                'Pinkal (04-Feb-2019) -- End

                If CBool(Session("PaymentApprovalwithLeaveApproval")) Then

                    Dim objapprover As New clsleaveapprover_master

                    Dim dtList As DataTable = objapprover.GetEmployeeApprover(CStr(Session("Database_Name")), _
                                                                              CInt(Session("Fin_year")), _
                                                                              CInt(Session("CompanyUnkId")), _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                            CBool(Session("IsIncludeInactiveEmp")), -1, _
                                                                              CInt(cboEmployee.SelectedValue))
                    'Pinkal (01-Mar-2016) -- End


                    If dtList.Rows.Count = 0 Then
                        Language.setLanguage(mstrModuleName5)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user."), Me)
                        Return False
                    End If

                    If dtList.Rows.Count > 0 Then
                        Dim objUsermapping As New clsapprover_Usermapping
                        Dim isUserExist As Boolean = False
                        For Each dr As DataRow In dtList.Rows
                            objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                            If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                        Next

                        If isUserExist = False Then
                            Language.setLanguage(mstrModuleName5)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 15, "Please Map this employee's Leave Approver to system user."), Me)
                            Return False
                        End If

                    End If

                    If CBool(Session("LeaveApproverForLeaveType")) Then

                        dtList = objapprover.GetEmployeeApprover(CStr(Session("Database_Name")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                                 -1, _
                                                                 CInt(cboEmployee.SelectedValue), -1, _
                                                                 CInt(cboLeaveType.SelectedValue))

                        If dtList.Rows.Count = 0 Then
                            Language.setLanguage(mstrModuleName5)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 16, "Please Map this Leave type to this employee's Leave Approver(s)."), Me)
                            Return False
                        End If

                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtList IsNot Nothing Then dtList.Clear()
                    dtList = Nothing
                    objapprover = Nothing
                    'Pinkal (11-Sep-2020) -- End


                ElseIf CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                    Dim objExapprover As New clsExpenseApprover_Master
                    Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(cboEmployee.SelectedValue), "List", Nothing)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                        Language.setLanguage(mstrModuleName5)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user."), Me)
                        Return False
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objExapprover = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If

            End If

            objExpense = Nothing

            'Pinkal (20-Nov-2018) -- End

            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.

            Dim sMsg As String = String.Empty

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboExpEmployee.SelectedValue), CInt(cboExpense.SelectedValue))


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboExpEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                            , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting))


            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboExpEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimRequestMasterId)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimMaster As New clsclaim_request_master
            'Pinkal (11-Sep-2020) -- End

            sMsg = objClaimMaster.IsValid_Expense(CInt(cboExpEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                      , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimRequestMasterId, True)

            'Pinkal (22-Jan-2020) -- End


            'Pinkal (07-Mar-2019) -- End

            'Pinkal (26-Feb-2019) -- End


            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                'Pinkal (11-Sep-2020) -- Start
                ''Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Exit Function
                objClaimMaster = Nothing
                Return False
                'Pinkal (11-Sep-2020) -- End
            End If
            sMsg = ""


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster = Nothing
            'Pinkal (11-Sep-2020) -- End

            'Pinkal (29-Feb-2016) -- End


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtLeaveExpenseTran IsNot Nothing Then
                Dim objExpBalance As New clsEmployeeExpenseBalance
                Dim dsBalance As DataSet = objExpBalance.Get_Balance(CInt(cboExpEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")), dtpDate.GetDate.Date _
                                                                                               , CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)), CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)))

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestTranId > 0 Then
                            xOccurrence = mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrEditGuid.Trim.Length > 0 Then
                            xOccurrence = mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrEditGuid.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            DisplayMessage.DisplayMessage(Language.getMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Language.getMessage("clsclaim_request_master", 4, " ] time(s)."), Me)
                            Return False
                        End If
                    End If
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsBalance IsNot Nothing Then dsBalance.Clear()
                dsBalance = Nothing
                objExpBalance = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
            'Pinkal (07-Mar-2019) -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboExpCategory.Enabled = iFlag
            cboEmployee.Enabled = iFlag
            cboPeriod.Enabled = iFlag
            dtpDate.Enabled = iFlag
            If mintClaimRequestMasterId > 0 Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'If objClaimMaster._Expensetypeid = enExpenseType.EXP_LEAVE Then
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    'Pinkal (11-Sep-2020) -- End
                    cboLeaveType.Enabled = iFlag
                End If
            Else
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = iFlag
                End If
            End If
            cboReference.Enabled = iFlag

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtLeaveExpenseTran IsNot Nothing Then
                If mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = CStr(mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                    cboCurrency.Enabled = True
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Enable_Disable_Controls:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = "0"

            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            cboSectorRoute.SelectedValue = "0"
            'Pinkal (22-Mar-2016) -- End

            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
            txtExpRemark.Text = ""


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtQty.Text = "0"
            txtQty.Text = "1"
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Text = "0"
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End



            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            'txtClaimRemark.Text = ""
            'Pinkal (20-May-2019) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            mintEmpMaxCountDependentsForCR = 0
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            cboCostCenter.SelectedValue = "0"
            'Pinkal (20-Nov-2018) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Clear_Controls:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    Language.setLanguage(mstrModuleName5)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If dgvData.Items.Count <= 0 Then
                Language.setLanguage(mstrModuleName5)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 8, "Please add atleast one expense in order to save."), Me)
                dgvData.Focus()
                Return False
            End If


            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            If CBool(Session("ClaimRemarkMandatoryForClaim")) AndAlso txtClaimRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 47, "Claim Remark is mandatory information. Please enter claim remark to continue."), Me)
                tabmain.ActiveTabIndex = 1
                txtClaimRemark.Focus()
                Return False
            End If
            'Pinkal (20-May-2019) -- End


            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Is_Valid:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub ClaimSetValue()
    Private Sub ClaimSetValue(ByRef objClaimMaster As clsclaim_request_master)
        'Pinkal (11-Sep-2020) -- End
        Try
            objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
            objClaimMaster._Cancel_Datetime = Nothing
            objClaimMaster._Cancel_Remark = ""
            objClaimMaster._Canceluserunkid = -1
            objClaimMaster._Iscancel = False
            objClaimMaster._Claimrequestno = txtClaimNo.Text
            objClaimMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objClaimMaster._Expensetypeid = CInt(cboExpCategory.SelectedValue)
            objClaimMaster._Isvoid = False

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            'objClaimMaster._Transactiondate = dtpDate.GetDateTime
            objClaimMaster._Transactiondate = dtpDate.GetDateTime
            'Pinkal (20-Nov-2018) -- End


            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objClaimMaster._Loginemployeeunkid = -1
                objClaimMaster._Userunkid = CInt(Session("UserId"))
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objClaimMaster._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objClaimMaster._Userunkid = -1
            End If

            objClaimMaster._Voiddatetime = Nothing
            objClaimMaster._Voiduserunkid = -1
            objClaimMaster._Claim_Remark = txtClaimRemark.Text

            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                objClaimMaster._FromModuleId = enExpFromModuleID.FROM_LEAVE
            Else
                objClaimMaster._FromModuleId = enExpFromModuleID.FROM_EXPENSE
            End If

            objClaimMaster._IsBalanceDeduct = False

            objClaimMaster._Statusunkid = 2 'DEFAULT PENDING
            objClaimMaster._Modulerefunkid = enModuleReference.Leave
            objClaimMaster._Referenceunkid = CInt(IIf(cboReference.SelectedValue = "", 0, cboReference.SelectedValue))
            objClaimMaster._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            objClaimMaster._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objClaimMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            objClaimMaster._YearId = CInt(Session("Fin_Year"))

            If Session("ObjClaimMst") Is Nothing Then
                Session.Add("ObjClaimMst", objClaimMaster)
            Else
                Session("ObjClaimMst") = objClaimMaster
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClaimSetValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtClaimAttchment Is Nothing Then Exit Sub
            dtView = New DataView(mdtClaimAttchment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            dtView = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtClaimAttchment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtClaimAttchment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Claim_Request
                dRow("scanattachrefid") = enScanAttactRefId.CLAIM_REQUEST

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("transactionunkid") = dgvData.Items(mintDeleteIndex).Cells(10).Text
                dRow("transactionunkid") = dgvData.Items(mintExpenseIndex).Cells(10).Text
                'Pinkal (18-Mar-2021) -- End


                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("GUID") = dgvData.Items(mintDeleteIndex).Cells(12).Text
                dRow("GUID") = dgvData.Items(mintExpenseIndex).Cells(12).Text
                'Pinkal (18-Mar-2021) -- End


                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName5
                dRow("userunkid") = CInt(Session("userid"))

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                dRow("Documentype") = GetMimeType(strfullpath)
                Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)
                dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                'Pinkal (20-Nov-2018) -- End

                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END

            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 33, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtClaimAttchment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing


            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            'If CInt(dgvData.Items(mintDeleteIndex).Cells(10).Text) > 0 Then
            '    xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvData.Items(mintDeleteIndex).Cells(10).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'ElseIf CStr(dgvData.Items(mintDeleteIndex).Cells(12).Text).Trim.Length > 0 Then
            '    xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvData.Items(mintDeleteIndex).Cells(12).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'End If
            If CInt(dgvData.Items(mintExpenseIndex).Cells(10).Text) > 0 Then
                xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvData.Items(mintExpenseIndex).Cells(10).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvData.Items(mintExpenseIndex).Cells(12).Text).Trim.Length > 0 Then
                xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvData.Items(mintExpenseIndex).Cells(12).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If
            'Pinkal (18-Mar-2021) -- End


            If xRow.Length <= 0 Then
                mdtChildClaimAttachment.ImportRow(dr)
            Else
                mdtChildClaimAttachment.Rows.Remove(xRow(0))
                mdtChildClaimAttachment.ImportRow(dr)
            End If
            mdtChildClaimAttachment.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName1)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
        Return True
    End Function


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub AddExpense()
        Try
            Dim dtmp() As DataRow = Nothing
            If mdtLeaveExpenseTran.Rows.Count > 0 Then
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'dtmp = mdtLeaveExpenseTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")
                dtmp = mdtLeaveExpenseTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                              "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                'Pinkal (04-Feb-2019) -- End
                If dtmp.Length > 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            dtmp = Nothing
            'Pinkal (11-Sep-2020) -- End


            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)




            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mblnIsHRExpense = objExpMaster._IsHRExpense
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If
            'Pinkal (20-Nov-2018) -- End

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                        'Sohail (23 Mar 2019) -- End
                        txtQty.Focus()
                        Exit Sub
                    End If
                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                        'Sohail (23 Mar 2019) -- End
                        txtQty.Focus()
                        Exit Sub
                    End If
                End If
            End If



            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                    iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                    iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    Language.setLanguage(mstrModuleName5)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    Exit Sub
                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    Exit Sub
                    'Pinkal (20-Nov-2018) -- End
                End If
                objExpMasterOld = Nothing
            End If

            objExpMaster = Nothing

            Dim dRow As DataRow = mdtLeaveExpenseTran.NewRow

            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimRequestMasterId
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing OrElse txtCostingTag.Value = "", 0, txtCostingTag.Value)
            dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            dRow.Item("quantity") = txtQty.Text
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("loginemployeeunkid") = -1
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            'Pinkal (20-Nov-2018) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            'Pinkal (04-Feb-2019) -- End

            mdtLeaveExpenseTran.Rows.Add(dRow) : Call Fill_Expense()
            Call Enable_Disable_Ctrls(False) : Call Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EditExpense()
        Try
            Dim iRow As DataRow() = Nothing
            If mintClaimRequestTranId > 0 Then
                iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & mintClaimRequestTranId & "' AND AUD <> 'D'")
            Else
                iRow = mdtLeaveExpenseTran.Select("GUID = '" & mstrEditGuid & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtLeaveExpenseTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Length > 0, CDec(txtQty.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtLeaveExpenseTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Length > 0, CDec(txtQty.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    Language.setLanguage(mstrModuleName5)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If
                'Pinkal (04-Feb-2019) -- End


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                dtmp = Nothing
                'Pinkal (11-Sep-2020) -- End


                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsHRExpense = objExpMaster._IsHRExpense
                'Pinkal (04-Feb-2019) -- End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""
                If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If
                'Pinkal (20-Nov-2018) -- End

                If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            Language.setLanguage(mstrModuleName5)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                        If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                            Language.setLanguage(mstrModuleName5)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    End If
                End If
                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                If objExpMaster._IsBudgetMandatory AndAlso objExpMaster._IsHRExpense = False AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                        Exit Sub
                    End If
                End If
                'Pinkal (20-Nov-2018) -- End

                If dgvData.Items.Count >= 1 Then
                    Dim objExpMasterOld As New clsExpense_Master
                    Dim iEncashment As DataRow() = Nothing

                    If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                        iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                        iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                    End If
                    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

                    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                        Language.setLanguage(mstrModuleName5)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        Exit Sub
                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                    ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        Exit Sub
                        'Pinkal (20-Nov-2018) -- End
                    End If
                    objExpMasterOld = Nothing
                End If
                objExpMaster = Nothing

                iRow(0).Item("crtranunkid") = mintClaimRequestTranId
                iRow(0).Item("crmasterunkid") = mintClaimRequestMasterId
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("quantity") = txtQty.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("loginemployeeunkid") = -1
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                'Pinkal (20-Nov-2018) -- End

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                'Pinkal (04-Feb-2019) -- End

                mdtLeaveExpenseTran.AcceptChanges()

                Call Fill_Expense() : Call Clear_Controls()

                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue))
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtDependents IsNot Nothing Then dtDependents.Clear()
            dtDependents = Nothing
            'Pinkal (11-Sep-2020) -- End

            objDependents = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    'Pinkal (25-Oct-2018) -- End    

    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try
            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If xExpenditureTypeId <> enP2PExpenditureType.None Then

                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    'Dim objEmpCC As New clsemployee_cctranhead_tran
                    'Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
                    'objEmpCC = Nothing
                    'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    '    mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
                    'End If
                    'dsList.Clear()
                    'dsList = Nothing
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If
            'Pinkal (04-Feb-2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xCostCenterCode As String, ByVal xGLCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), Me)
                Return False
            ElseIf xCostCenterCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense."), Me)
                Return False
            End If


            'Pinkal (29-Aug-2019) -- Start
            'Enhancement NMB - Working on P2P Get Token Service URL.
            mstrP2PToken = GetP2PToken()
            'Pinkal (29-Aug-2019) -- End

            If xCostCenterCode.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'Dim mstrPostData As String = "{""costCenter"" : """ & xCostCenterCode.Trim() & """ , ""glCode"":""" & xGLCode.Trim & """}"
                Dim mstrBgtRequestValidationP2PServiceURL As String = Session("BgtRequestValidationP2PServiceURL").ToString().Trim() & "?costCenter=" & xCostCenterCode.Trim() & "&glcode=" & xGLCode.Trim()
                Dim mstrGetData As String = ""
                Dim objMstData As New clsMasterData
                'If objMstData.GetSetP2PWebRequest(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), True, True, mstrPostData, mstrGetData, mstrError) = False Then

                'Pinkal (29-Aug-2019) -- Start
                'Enhancement NMB - Working on P2P Get Token Service URL.
                'If objMstData.GetSetP2PWebRequest(mstrBgtRequestValidationP2PServiceURL, True, True, "", mstrGetData, mstrError) = False Then
                If objMstData.GetSetP2PWebRequest(mstrBgtRequestValidationP2PServiceURL, True, True, "CostcenterPassing", mstrGetData, mstrError, Nothing, "", mstrToken:=mstrP2PToken) = False Then
                    'Pinkal (29-Aug-2019) -- End
                    'Pinkal (07-Mar-2019) -- End
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Return False
                End If
                objMstData = Nothing
                'mstrGetData = "{""amount"":""10000""}"

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtAmount As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtAmount IsNot Nothing AndAlso dtAmount.Rows.Count > 0 Then
                        mdecBudgetAmount = CDec(dtAmount.Rows(0)("amount"))
                    End If
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtAmount IsNot Nothing Then dtAmount.Clear()
                    dtAmount = Nothing
                    'Pinkal (11-Sep-2020) -- End
                End If

            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function UpdateDeleteAttachment(ByVal dr As DataRow, ByVal mblnDeleteAttachment As Boolean, ByVal mstrAUD As String) As Boolean
        Try
            If dr IsNot Nothing Then dr("AUD") = mstrAUD
            dr.AcceptChanges()

            If mblnDeleteAttachment Then
                Try
                    If dr IsNot Nothing Then File.Delete(dr("filepath").ToString())
                Catch ex As Exception
                    Throw ex
                End Try
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    'Pinkal (20-Nov-2018) -- End


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End

            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (04-Feb-2019) -- End

    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Private Function GetP2PToken() As String
        Dim mstrToken As String = ""
        Dim mstrError As String = ""
        Dim mstrGetData As String = ""
        Try
            If Session("GetTokenP2PServiceURL") IsNot Nothing AndAlso Session("GetTokenP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim objMstData As New clsMasterData
                Dim objCRUser As New clsCRP2PUserDetail
                If objMstData.GetSetP2PWebRequest(Session("GetTokenP2PServiceURL").ToString().Trim(), True, True, objCRUser.username, mstrGetData, mstrError, objCRUser, "", "") = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Return mstrToken
                End If
                objCRUser = Nothing

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mstrToken = dtTable.Rows(0)("token").ToString()
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dtTable IsNot Nothing Then dtTable.Clear()
                        dtTable = Nothing
                        'Pinkal (11-Sep-2020) -- End
                    End If
                End If
                objMstData = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mstrToken
    End Function
    'Pinkal (29-Aug-2019) -- End

#End Region

#Region "Day Fraction"

    '    Private Sub GetDayFraction()
    '        Dim objLeaveType As New clsleavetype_master
    '        Dim objEmpHoliday As New clsemployee_holiday
    '        Dim objShift As New clsNewshift_master
    '        Dim blnIssueonHoliday As Boolean = False

    '        Try
    '            Dim objFraction As New clsleaveday_fraction
    '            Dim mdtStartDate As Date = Nothing
    '            Dim mdtEndDate As Date = Nothing

    '            mdtStartDate = dtpStartDate.GetDate.Date

    '            If Not dtpEndDate.IsNull Then
    '                mdtEndDate = dtpEndDate.GetDate.Date
    '            Else
    '                mdtEndDate = dtpStartDate.GetDate.Date
    '            End If

    '            Dim mintTotalDays As Integer = CInt(DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date.AddDays(1)))

    '            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
    '            blnIssueonHoliday = objLeaveType._Isissueonholiday

    '            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '            Dim objShiftTran As New clsshift_tran
    '            Dim blnIssueonweekend As Boolean = False
    '            Dim blnConsiderLVHlOnWk As Boolean = False
    '            blnIssueonweekend = objLeaveType._Isissueonweekend
    '            blnConsiderLVHlOnWk = objLeaveType._IsLeaveHL_onWK

    '            Dim objEmp As New clsEmployee_Master

    '            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
    '            If Me.ViewState("Fraction") IsNot Nothing Then
    '                dtFraction = Me.ViewState("Fraction")

    '                Dim drRow As DataRow() = dtFraction.Select("leavedate < '" & mdtStartDate.Date & "' AND AUD <> 'D'")
    '                If drRow.Length > 0 Then
    '                    For i As Integer = 0 To drRow.Length - 1
    '                        drRow(i)("AUD") = "D"
    '                    Next
    '                    dtFraction.AcceptChanges()
    '                End If

    '                drRow = dtFraction.Select("leavedate >'" & mdtEndDate.Date & "' AND AUD <> 'D'")
    '                If drRow.Length > 0 Then
    '                    For i As Integer = 0 To drRow.Length - 1
    '                        drRow(i)("AUD") = "D"
    '                    Next
    '                    dtFraction.AcceptChanges()
    '                End If
    '                Dim mintDays As Integer = 0
    '                Dim mdtdate As DateTime = Nothing

    '                mintDays = CInt(DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date))
    '                mdtdate = mdtStartDate.Date
    '                For i As Integer = 0 To mintDays
    '                    objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartDate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))))
    '                    objShiftTran.GetShiftTran(objShift._Shiftunkid)
    '                    Dim strWeekName As String = CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(mdtStartDate.AddDays(i).DayOfWeek)
    '                    If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
    '                        If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
    '                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
    '                            If dRow.Length > 0 Then
    '                                dRow(0)("AUD") = "D"
    '                                dtFraction.AcceptChanges()
    '                            End If

    '                            If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

    '                                If objShiftTran._dtShiftday IsNot Nothing Then
    '                                    Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
    '                                    If drShift.Length > 0 Then
    '                                        GoTo AddRecord
    '                                    End If

    '                                End If

    '                            End If
    '                            Continue For
    '                        End If

    '                        Dim objReccurent As New clsemployee_holiday
    '                        If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date) Then
    '                            Continue For
    '                        End If
    '                    End If

    '                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
    '                        If objShiftTran._dtShiftday IsNot Nothing Then
    '                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
    '                            If drShift.Length > 0 Then
    '                                Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
    '                                If dRow.Length > 0 Then
    '                                    dRow(0)("AUD") = "D"
    '                                    dtFraction.AcceptChanges()
    '                                End If

    '                                If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
    '                                    If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
    '                                        GoTo AddRecord
    '                                    End If

    '                                End If
    '                                Continue For

    '                            End If

    '                        End If

    '                    End If

    '                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then

    '                        If objShiftTran._dtShiftday IsNot Nothing Then
    '                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
    '                            If drShift.Length > 0 Then
    '                                Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
    '                                If dRow.Length > 0 Then
    '                                    dRow(0)("AUD") = "D"
    '                                    dtFraction.AcceptChanges()
    '                                End If
    '                                Continue For
    '                            Else
    '                                If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
    '                                    Continue For
    '                                End If
    '                                Dim objReccurent As New clsemployee_holiday
    '                                If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date) Then
    '                                    Continue For
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    'AddRecord:

    '                    Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtdate.AddDays(i).Date & "'  AND AUD <> 'D' ")
    '                    If dtRow.Length = 0 Then
    '                        Dim dr As DataRow = dtFraction.NewRow()
    '                        dr("dayfractionunkid") = -1

    '                        If Me.ViewState("Formunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Formunkid")) > 0 Then
    '                            dr("formunkid") = Me.ViewState("Formunkid")
    '                        Else
    '                            dr("formunkid") = -1
    '                        End If
    '                        dr("leavedate") = mdtdate.AddDays(i)
    '                        dr("dayfraction") = 1.0
    '                        dr("GUID") = Guid.NewGuid.ToString()
    '                        dr("AUD") = "A"
    '                        dtFraction.Rows.InsertAt(dr, i)
    '                    End If
    '                Next

    '            Else

    '                If mintLeaveFormUnkid > 0 Then
    '                    dtFraction = objFraction.GetList("List", mintLeaveFormUnkid, True, CInt(cboEmployee.SelectedValue)).Tables(0)
    '                Else
    '                    dtFraction = objFraction.GetList("List", mintLeaveFormUnkid, True, CInt(cboEmployee.SelectedValue)).Tables(0).Clone
    '                End If


    '                Dim drRow As DataRow() = dtFraction.Select("leavedate > '" & mdtEndDate.Date & "'")
    '                If drRow.Length > 0 Then
    '                    For i As Integer = 0 To drRow.Length - 1
    '                        drRow(i)("AUD") = "D"
    '                    Next
    '                    dtFraction.AcceptChanges()
    '                End If

    '                For i As Integer = 0 To mintTotalDays - 1
    '                    objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartDate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))))
    '                    objShiftTran.GetShiftTran(objShift._Shiftunkid)
    '                    Dim strWeekName As String = CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(mdtStartDate.AddDays(i).DayOfWeek)
    '                    If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
    '                        If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
    '                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
    '                            If dRow.Length > 0 Then
    '                                dRow(0)("AUD") = "D"
    '                                dtFraction.AcceptChanges()
    '                            End If

    '                            If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

    '                                If objShiftTran._dtShiftday IsNot Nothing Then
    '                                    Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
    '                                    If drShift.Length > 0 Then
    '                                        GoTo EditRecord
    '                                    End If

    '                                End If

    '                            End If
    '                            Continue For
    '                        End If

    '                        Dim objReccurent As New clsemployee_holiday
    '                        If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date) Then
    '                            Continue For
    '                        End If
    '                    End If

    '                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
    '                        If objShiftTran._dtShiftday IsNot Nothing Then
    '                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
    '                            If drShift.Length > 0 Then
    '                                Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
    '                                If dRow.Length > 0 Then
    '                                    dRow(0)("AUD") = "D"
    '                                    dtFraction.AcceptChanges()
    '                                End If

    '                                If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
    '                                    If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
    '                                        GoTo EditRecord
    '                                    End If

    '                                End If
    '                                Continue For
    '                            End If
    '                        End If
    '                    End If
    '                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then
    '                        If objShiftTran._dtShiftday IsNot Nothing Then
    '                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
    '                            If drShift.Length > 0 Then
    '                                Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & mdtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
    '                                If dRow.Length > 0 Then
    '                                    dRow(0)("AUD") = "D"
    '                                    dtFraction.AcceptChanges()
    '                                End If
    '                                Continue For
    '                            Else
    '                                If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date).Rows.Count > 0 Then
    '                                    Continue For
    '                                End If
    '                                Dim objReccurent As New clsemployee_holiday
    '                                If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))), mdtStartDate.AddDays(i).Date) Then
    '                                    Continue For
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    'EditRecord:
    '                    Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtStartDate.Date.AddDays(i).Date & "' AND AUD = ''")
    '                    If dtRow.Length = 0 Then
    '                        Dim dr As DataRow = dtFraction.NewRow
    '                        dr("dayfractionunkid") = -1
    '                        dr("formunkid") = mintLeaveFormUnkid
    '                        dr("leavedate") = mdtStartDate.Date.AddDays(i).ToShortDateString
    '                        dr("dayfraction") = 1.0
    '                        If dr("AUD").ToString() = "" Then
    '                            dr("AUD") = "A"
    '                        End If
    '                        dr("GUID") = Guid.NewGuid.ToString()
    '                        dtFraction.Rows.Add(dr)
    '                    End If
    '                Next

    '            End If

    '            Dim dtDelete As DataTable = New DataView(dtFraction, "AUD = 'D'", "leavedate asc", DataViewRowState.CurrentRows).ToTable
    '            Me.ViewState("Fraction") = New DataView(dtFraction, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable
    '            Me.ViewState("DeletedFraction") = dtDelete
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    Private Sub GetFractionList()
        Try
            dgFraction.DataSource = New DataView(mdtDayFration, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable()
            dgFraction.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private Function IsValidExpense_Eligility() As Boolean
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboLeaveType.SelectedValue) > 0 Then
                Dim objlvtype As New clsleavetype_master
                objlvtype._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                If objlvtype._EligibilityOnAfter_Expense > 0 Then
                    intEligibleExpenseDays = objlvtype._EligibilityOnAfter_Expense
                    If CDec(objNoofDays.Text) >= objlvtype._EligibilityOnAfter_Expense Then
                        blnFlag = True
                    End If
                Else
                    blnFlag = True
                End If
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objlvtype = Nothing
                'Pinkal (11-Sep-2020) -- End
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function
    'Pinkal (29-Sep-2017) -- End

#End Region

#Region "Button Event(S)"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    If dtpStartDate.IsNull = False Then
            '        If dtpStartDate.GetDate().Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
            '            DisplayMessage.DisplayMessage("Sorry, you cannot apply leave application for backdate. If you want to apply for backdate, Please contact your line manager.", Me)
            '            dtpStartDate.Focus()
            '            Exit Sub
            '        End If
            '    End If
            'End If
            'Pinkal (07-Mar-2019) -- End

            'Pinkal (26-Feb-2019) -- End

            If CheckMapping() Then
                Call EntrySave()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ExpenseConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExpenseConfirmation.buttonYes_Click
        Try
            Save()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub LeaveFormConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LeaveFormConfirmation.buttonYes_Click
        Try

            FormDatesConfirmation.Message = Language.getMessage(mstrModuleName1, 25, "You have applied ") & cboLeaveType.SelectedItem.Text & " " & Language.getMessage(mstrModuleName1, 26, "leave for") & _
                                              " " & objNoofDays.Text & " " & Language.getMessage(mstrModuleName1, 27, "days , from") & " " & _
                                            dtpStartDate.GetDate.Date & " " & Language.getMessage(mstrModuleName1, 28, "to") & " " & dtpEndDate.GetDate.Date & ". " & _
                                            Language.getMessage(mstrModuleName1, 29, "Are you sure of the number of days and selected dates? If yes click YES to save the application, otherwise click NO to make corrections.")

            FormDatesConfirmation.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub FormDatesConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormDatesConfirmation.buttonYes_Click
        Try

            Dim clsLeaveForm As New clsleaveform
            With clsLeaveForm

                Dim mblnIssued As Boolean = False

                If ._Statusunkid = 7 Then  ' FOR STATUS = ISSUED
                    mblnIssued = True
                End If

                Dim mdsDoc As DataSet
                Dim mstrFolderName As String = ""
                mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                Dim strFileName As String = ""

                If mdtAttachement IsNot Nothing Then
                    For Each dRow As DataRow In mdtAttachement.Rows
                        mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
                        If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                            'Pinkal (20-Nov-2018) -- Start
                            'Enhancement - Working on P2P Integration for NMB.
                            'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            'Pinkal (20-Nov-2018) -- End
                            If File.Exists(CStr(dRow("orgfilepath"))) Then
                                Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                                If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                                End If

                                File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strPath
                                dRow("form_name") = mstrModuleName1
                                dRow.AcceptChanges()
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If

                        ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then

                            Dim strFilepath As String = CStr(dRow("filepath"))
                            Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                            If strFilepath.Contains(strArutiSelfService) Then
                                strFilepath = strFilepath.Replace(strArutiSelfService, "")
                                If Strings.Left(strFilepath, 1) <> "/" Then
                                    strFilepath = "~/" & strFilepath
                                Else
                                    strFilepath = "~" & strFilepath
                                End If

                                If File.Exists(Server.MapPath(strFilepath)) Then
                                    File.Delete(Server.MapPath(strFilepath))
                                Else
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError(ex, Me)
                                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    Exit Sub
                                End If
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                If mdtFinalClaimAttchment IsNot Nothing Then
                    mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault
                    For Each dRow As DataRow In mdtFinalClaimAttchment.Rows
                        If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                            'Pinkal (20-Nov-2018) -- Start
                            'Enhancement - Working on P2P Integration for NMB.
                            'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            'Pinkal (20-Nov-2018) -- End
                            If File.Exists(CStr(dRow("orgfilepath"))) Then
                                Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                                If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                                End If

                                File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                        ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                            Dim strFilepath As String = dRow("filepath").ToString
                            Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                            If strFilepath.Contains(strArutiSelfService) Then
                                strFilepath = strFilepath.Replace(strArutiSelfService, "")
                                If Strings.Left(strFilepath, 1) <> "/" Then
                                    strFilepath = "~/" & strFilepath
                                Else
                                    strFilepath = "~" & strFilepath
                                End If

                                If File.Exists(Server.MapPath(strFilepath)) Then
                                    File.Delete(Server.MapPath(strFilepath))
                                Else
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError(ex, Me)
                                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    Exit Sub
                                End If
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                If mintLeaveFormUnkid <= 0 Then
                    If Setvalue(clsLeaveForm) Then
                        ._dtAttachDocument = mdtAttachement
                        ._dtClaimAttchment = mdtFinalClaimAttchment


                        'Pinkal (01-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.

                        '.Insert(CStr(Session("Database_Name")), _
                        '            CInt(Session("Fin_year")), _
                        '            CInt(Session("CompanyUnkId")), _
                        '            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                        '            CBool(Session("IsIncludeInactiveEmp")), _
                        '            ConfigParameter._Object._CurrentDateAndTime, _
                        '            CType(IIf(mdtDayFration IsNot Nothing, mdtDayFration, Nothing), DataTable), _
                        '            CStr(Session("LeaveApproverForLeaveType")))

                        .Insert(CStr(Session("Database_Name")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                    CBool(Session("IsIncludeInactiveEmp")), _
                               ConfigParameter._Object._CurrentDateAndTime, mblnLvTypeSkipApproverFlow, _
                               CInt(Session("LeaveBalanceSetting")), Session("UserAccessModeSetting").ToString(), _
                                    CType(IIf(mdtDayFration IsNot Nothing, mdtDayFration, Nothing), DataTable), _
                                    CStr(Session("LeaveApproverForLeaveType")), CBool(Session("SkipEmployeeMovementApprovalFlow")), CBool(Session("CreateADUserFromEmpMst")))
                        'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
                        'Pinkal (01-Oct-2018) -- End

                        If ._Message.Length > 0 Then

                            'Pinkal (03-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.
                            'DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & ._Message, Me)
                            DisplayMessage.DisplayMessage(._Message, Me)
                            'Pinkal (03-May-2019) -- End

                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If mdtFinalClaimTransaction IsNot Nothing Then mdtFinalClaimTransaction.Clear()
                            mdtFinalClaimTransaction = Nothing
                            If mdtDayFration IsNot Nothing Then mdtDayFration.Clear()
                            mdtDayFration = Nothing
                            'Pinkal (11-Sep-2020) -- End


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            Exit Sub
                            'Pinkal (01-Oct-2018) -- End
                        Else

                            'Pinkal (20-Nov-2018) -- Start
                            'Enhancement - Working on P2P Integration for NMB.

                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                            'If mintClaimRequestMasterId <= 0 AndAlso mblnIsClaimFormBudgetMandatory AndAlso _
                            '    mblnIsHRExpense = False AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            '    'Pinkal (04-Feb-2019) -- End

                            '    Dim mstrUserName As String = ""
                            '    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                            '        mstrUserName = Session("UserName").ToString()
                            '    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            '        mstrUserName = Session("DisplayName").ToString()
                            '    End If

                            '    Dim objClaimMst As clsclaim_request_master = CType(Session("ObjClaimMst"), clsclaim_request_master)

                            '    Dim objP2P As New clsCRP2PIntegration

                            '    'Pinkal (11-Sep-2020) -- Start
                            '    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                            '    'Dim mstrP2PRequest As String = objClaimMaster.GenerateNewRequisitionRequestForP2P(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                            '    '                                                                                                                                                                       , CInt(Session("CompanyUnkId")), Session("UserAccessModeSetting").ToString() _
                            '    '                                                                                                                                                                       , objClaimMst._Employeeunkid, objClaimMst._Claimrequestno, objClaimMst._Transactiondate _
                            '    '                                                                                                                                                                       , objClaimMst._Claim_Remark, mstrUserName, mdtFinalClaimTransaction, mdtFinalClaimAttchment, objP2P)

                            '    Dim mstrP2PRequest As String = objClaimMst.GenerateNewRequisitionRequestForP2P(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                '                                                                                                                                                                       , CInt(Session("CompanyUnkId")), Session("UserAccessModeSetting").ToString() _
                                '                                                                                                                                                                       , objClaimMst._Employeeunkid, objClaimMst._Claimrequestno, objClaimMst._Transactiondate _
                                '                                                                                                                                                                       , objClaimMst._Claim_Remark, mstrUserName, mdtFinalClaimTransaction, mdtFinalClaimAttchment, objP2P)

                            '    'Pinkal (11-Sep-2020) -- End

                            '    Dim objMstData As New clsMasterData
                            '    Dim mstrGetData As String = ""
                            '    Dim mstrError As String = ""

                            '    'Pinkal (31-May-2019) -- Start
                            '    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            '    Dim mstrPostedData As String = ""
                            '    'If objMstData.GetSetP2PWebRequest(Session("NewRequisitionRequestP2PServiceURL").ToString().Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P) = False Then


                            '    'Pinkal (29-Aug-2019) -- Start
                            '    'Enhancement NMB - Working on P2P Get Token Service URL.
                            '    'If objMstData.GetSetP2PWebRequest(Session("NewRequisitionRequestP2PServiceURL").ToString().Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData) = False Then
                            '    If objMstData.GetSetP2PWebRequest(Session("NewRequisitionRequestP2PServiceURL").ToString().Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData, mstrToken:=mstrP2PToken) = False Then
                            '        'Pinkal (29-Aug-2019) -- End

                            '        'Pinkal (31-May-2019) -- End

                            '        '/* START TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.

                            '        If mdtFinalClaimAttchment IsNot Nothing AndAlso mdtFinalClaimAttchment.Rows.Count > 0 Then
                            '            Dim dr As List(Of DataRow) = mdtFinalClaimAttchment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").ToList()
                            '            If dr.Count > 0 Then
                            '                dr.ForEach(Function(x) UpdateDeleteAttachment(x, False, "D"))
                            '            End If
                            '        End If

                            '        objClaimMst._Isvoid = True
                            '        If CInt(Session("Employeeunkid")) > 0 Then
                            '            objClaimMst._VoidLoginEmployeeID = CInt(Session("Employeeunkid"))
                            '        ElseIf CInt(Session("UserId")) > 0 Then
                            '            objClaimMst._Voiduserunkid = CInt(Session("UserId"))
                            '        End If
                            '        objClaimMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            '        objClaimMst._Voidreason = "Voided due to connection fail or error from P2P system."

                            '        If objClaimMst.Delete(objClaimMst._Crmasterunkid, CInt(Session("UserId")), Session("Database_Name").ToString(), CInt(Session("Fin_year")) _
                            '                                                   , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                            '                                                   , True, mdtFinalClaimAttchment, True, Nothing, mblnIsClaimFormBudgetMandatory) = False Then

                            '            'Pinkal (11-Sep-2020) -- Start
                            '            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            '            'DisplayMessage.DisplayMessage(objClaimMaster._Message, Me)
                            '            DisplayMessage.DisplayMessage(objClaimMst._Message, Me)
                            '            'Pinkal (11-Sep-2020) -- End
                            '            Exit Sub
                            '        End If

                            '        If mdtFinalClaimAttchment IsNot Nothing AndAlso mdtFinalClaimAttchment.Rows.Count > 0 Then
                            '            Dim dr As List(Of DataRow) = mdtFinalClaimAttchment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") = "D" AndAlso x.Field(Of Integer)("scanattachtranunkid") > 0).ToList()
                            '            If dr.Count > 0 Then
                            '                dr.ForEach(Function(x) UpdateDeleteAttachment(x, True, "A"))
                            '            End If
                            '        End If

                            '        '/* END TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.
                            '        'DisplayMessage.DisplayMessage(mstrError, Me)
                            '        mintLeaveFormUnkid = clsLeaveForm._Formunkid
                            '        P2PLvFormDeleteConfirmation.Message = mstrError & Language.getMessage(mstrModuleName1, 46, "Are you sure want to save this leave application without expense application ?")
                            '        P2PLvFormDeleteConfirmation.Show()
                            '        Exit Sub

                            '    Else
                            '        '/* START TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                            '        'mstrGetData = "{""requisitionId"": ""NMBHR-011118-000012"",""message"": ""New requisition details saved successfully."",""status"": 200,""timestamp"": ""01-Nov-2018 12:58:59 PM""}"
                            '        If mstrGetData.Trim.Length > 0 Then
                            '            Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)

                            '            'Pinkal (31-May-2019) -- Start
                            '            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            '            'If objClaimMaster.UpdateP2PResponseToClaimMst(dtTable, objClaimMaster._Crmasterunkid, CInt(Session("UserId"))) = False Then

                            '            'Pinkal (11-Sep-2020) -- Start
                            '            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            '            'If objClaimMaster.UpdateP2PResponseToClaimMst(dtTable, objClaimMaster._Crmasterunkid, CInt(Session("UserId")), mstrPostedData) = False Then
                            '            '    DisplayMessage.DisplayMessage(objClaimMaster._Message, Me)
                            '            'End If

                            '            If objClaimMst.UpdateP2PResponseToClaimMst(dtTable, objClaimMst._Crmasterunkid, CInt(Session("UserId")), mstrPostedData) = False Then
                            '                DisplayMessage.DisplayMessage(objClaimMst._Message, Me)
                            '            End If
                            '            'Pinkal (11-Sep-2020) -- End
                            '        End If
                            '        '/* END TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                                        'End If
                            '    objMstData = Nothing

                            'End If  ' mintClaimRequestMasterId <= 0 AndAlso mblnIsClaimFormBudgetMandatory AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then


                            'Pinkal (07-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'DisplayMessage.DisplayMessage("Entry Saved Sucessfully." & ._Message, Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 52, "Leave Application saved successfully."), Me)
                            'Pinkal (07-Mar-2019) -- End

                            SendLeaveApproverNotification(clsLeaveForm)

                            'Pinkal (20-Nov-2018) -- End

                        End If
                        BlankObjects()
                    Else
                        Exit Sub
                    End If

                Else

                    ._Formunkid = mintLeaveFormUnkid
                    If Setvalue(clsLeaveForm) Then
                        ._dtAttachDocument = mdtAttachement
                        ._dtClaimAttchment = mdtFinalClaimAttchment


                        'Pinkal (01-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.

                        '.Update(CStr(Session("Database_Name")), _
                        '            CInt(Session("Fin_year")), _
                        '            CInt(Session("CompanyUnkId")), _
                        '            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                        '            CBool(Session("IsIncludeInactiveEmp")), mblnIssued, _
                        '            CType(IIf(mdtDayFration IsNot Nothing, mdtDayFration, Nothing), DataTable), _
                        '            ConfigParameter._Object._CurrentDateAndTime, _
                        '            CStr(Session("LeaveApproverForLeaveType")), CInt(Session("LeaveBalanceSetting")))

                        .Update(CStr(Session("Database_Name")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                    CBool(Session("IsIncludeInactiveEmp")), mblnIssued, _
                                    CType(IIf(mdtDayFration IsNot Nothing, mdtDayFration, Nothing), DataTable), _
                                    ConfigParameter._Object._CurrentDateAndTime, _
                               mblnLvTypeSkipApproverFlow, CStr(Session("UserAccessModeSetting")), _
                                    CStr(Session("LeaveApproverForLeaveType")), CInt(Session("LeaveBalanceSetting")), CBool(Session("SkipEmployeeMovementApprovalFlow")), CBool(Session("CreateADUserFromEmpMst")))
                        'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
                        'Pinkal (01-Oct-2018) -- End

                        If ._Message.Length > 0 Then
                            DisplayMessage.DisplayMessage("Entry Not Updated. Reason : " & ._Message, Me)


                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If mdtFinalClaimTransaction IsNot Nothing Then mdtFinalClaimTransaction.Clear()
                            mdtFinalClaimTransaction = Nothing
                            If mdtDayFration IsNot Nothing Then mdtDayFration.Clear()
                            mdtDayFration = Nothing
                            'Pinkal (11-Sep-2020) -- End


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            Exit Sub
                            'Pinkal (01-Oct-2018) -- End
                        Else

                            'Pinkal (07-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'DisplayMessage.DisplayMessage("Entry Updated Sucessfully." & ._Message, Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 53, "Leave Application updated successfully."), Me)
                            'Pinkal (07-Mar-2019) -- End


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.

                            If mblnLvTypeSkipApproverFlow AndAlso dtpEndDate.IsNull = False Then
                                'Pinkal (20-Nov-2018) -- Start
                                'Enhancement - Working on P2P Integration for NMB.
                                'clsLeaveForm.GetData()
                                'clsLeaveForm._WebFrmName = "frmLeaveForm_AddEdit"
                                'clsLeaveForm._WebClientIP = CStr(Session("IP_ADD"))
                                'clsLeaveForm._WebHostName = CStr(Session("HOST_NAME"))
                                'StrModuleName2 = "mnuLeaveInformation"

                                'Dim iLogimModeId As Integer = CInt(Session("LoginBy"))

                                'clsLeaveForm.SendMailToApprover(CStr(Session("Database_Name")), _
                                '                                CInt(Session("UserId")), _
                                '                                CInt(Session("Fin_year")), _
                                '                                CInt(Session("CompanyUnkId")), _
                                '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                '                                CStr(Session("UserAccessModeSetting")), True, _
                                '                                CBool(Session("IsIncludeInactiveEmp")), _
                                '                                CInt(cboEmployee.SelectedValue), _
                                '                                CInt(cboLeaveType.SelectedValue), _
                                '                                clsLeaveForm._Formunkid, _
                                '                                clsLeaveForm._Formno, False, _
                                '                                CStr(Session("LeaveApproverForLeaveType")), _
                                '                                CStr(Session("ArutiSelfServiceURL")), _
                                '                                iLogimModeId, CInt(Session("Employeeunkid")), _
                                '                                mblnLvTypeSkipApproverFlow)


                                'SendEmailForSkipApproverFlow(clsLeaveForm, iLogimModeId)

                                SendLeaveApproverNotification(clsLeaveForm)

                                'Pinkal (20-Nov-2018) -- End

                            End If

                            'Pinkal (01-Oct-2018) -- End


                            mintLeaveFormUnkid = 0
                            mdtFinalClaimTransaction = Nothing
                            mdtDayFration = Nothing
                            dgvData.DataSource = Nothing
                            dgvData.DataBind()
                            Session("ObjClaimMst") = Nothing
                            Call FillCombo()
                        End If
                    Else
                        Exit Sub
                    End If
                    BlankObjects()
                    Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeaveFormList.aspx", False)
                End If
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtAttachement IsNot Nothing Then mdtAttachement.Clear()
            mdtAttachement = Nothing
            If mdtChildClaimAttachment IsNot Nothing Then mdtChildClaimAttachment.Clear()
            mdtChildClaimAttachment = Nothing
            If mdtClaimAttchment IsNot Nothing Then mdtClaimAttchment.Clear()
            mdtClaimAttchment = Nothing
            If mdtDayFration IsNot Nothing Then mdtDayFration.Clear()
            mdtDayFration = Nothing
            If mdtFinalClaimAttchment IsNot Nothing Then mdtFinalClaimAttchment.Clear()
            mdtFinalClaimAttchment = Nothing
            If mdtFinalClaimTransaction IsNot Nothing Then mdtFinalClaimTransaction.Clear()
            mdtFinalClaimTransaction = Nothing
            If mdtLeaveExpenseTran IsNot Nothing Then mdtLeaveExpenseTran.Clear()
            mdtLeaveExpenseTran = Nothing
            'Pinkal (11-Sep-2020) -- End

            Me.Session.Remove("mdtClaimAttchment")
            Me.Session.Remove("mdtFinalClaimAttchment")
            Me.Session.Remove("mdtChildClaimAttachment")
            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeaveFormList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region "Leave Expense"

    Protected Sub btnSaveAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddEdit.Click
        Try
            If Is_Valid() = False Then Exit Sub

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            Dim objExpense As New clsExpense_Master
            Dim xRow() As DataRow = Nothing

            For Each dRow As DataRow In mdtLeaveExpenseTran.Select("AUD <> 'D'")
                objExpense._Expenseunkid = CInt(dRow.Item("expenseunkid"))
                If objExpense._IsAttachDocMandetory = True Then

                    If CInt(dRow.Item("crtranunkid")) > 0 Then
                        xRow = mdtChildClaimAttachment.Select("transactionunkid = '" & CInt(dRow.Item("crtranunkid")) & "' AND AUD <> 'D' ")
                    ElseIf CStr(dRow.Item("GUID")).Trim.Length > 0 Then
                        xRow = mdtChildClaimAttachment.Select("GUID = '" & CStr(dRow.Item("GUID")) & "' AND AUD <> 'D' ")
                    End If
                    If xRow.Count <= 0 Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(dRow.Item("expense").ToString & " " & Language.getMessage(mstrModuleName5, 22, "has mandatory document attachment. please attach document."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If
            Next
            'Shani (20-Aug-2016) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpense = Nothing
            'ClaimSetValue()
            'mdtFinalClaimTransaction = mdtLeaveExpenseTran
            Dim objClaimMaster As New clsclaim_request_master
            ClaimSetValue(objClaimMaster)

            mdtFinalClaimTransaction = mdtLeaveExpenseTran.Copy()
            If mdtLeaveExpenseTran IsNot Nothing Then mdtLeaveExpenseTran.Clear()
            'Pinkal (11-Sep-2020) -- End

            mdtLeaveExpenseTran = Nothing
            btnAdd.Visible = True
            btnEdit.Visible = False
            Clear_Controls()
            cboExpense.Enabled = True
            txtUnitPrice.Enabled = True
            txtQty.Enabled = True
            mblnClaimRequest = False
            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            mdtFinalClaimAttchment = mdtChildClaimAttachment.Copy
            'Shani (20-Aug-2016) -- End
            popupExpense.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseAddEdit.Click
        Try
            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If cboExpense.Enabled = False Then cboExpense.Enabled = True
            If btnEdit.Visible Then btnEdit.Visible = False : btnAdd.Visible = True
            Clear_Controls()
            'Pinkal (25-May-2019) -- End

            mblnClaimRequest = False
            popupExpense.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            mblnIsExpenseEditClick = False

            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName5, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Language.getMessage(mstrModuleName5, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName5, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName5, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
                'Pinkal (04-Feb-2019) -- End
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList().Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


            'Dim dtmp() As DataRow = Nothing
            'If mdtLeaveExpenseTran.Rows.Count > 0 Then
            '    dtmp = mdtLeaveExpenseTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")
            '    If dtmp.Length > 0 Then
            '        DisplayMessage.DisplayError(ex, Me)
            '        Exit Sub
            '    End If
            'End If


            'Dim objExpMaster As New clsExpense_Master
            'objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
            '            DisplayMessage.DisplayError(ex, Me)
            '            txtQty.Focus()
            '            Exit Sub
            '        End If
            '    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '        If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
            '            DisplayMessage.DisplayError(ex, Me)
            '            txtQty.Focus()
            '            Exit Sub
            '        End If
            '    End If
            'End If

            'If dgvData.Items.Count >= 1 Then
            '    Dim objExpMasterOld As New clsExpense_Master
            '    Dim iEncashment As DataRow() = Nothing
            '    If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
            '        iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '    ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
            '        iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '    End If

            '    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
            '    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '        Language.setLanguage(mstrModuleName5)
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '        Exit Sub
            '    End If
            '    objExpMasterOld = Nothing
            'End If

            'objExpMaster = Nothing

            'Dim dRow As DataRow = mdtLeaveExpenseTran.NewRow

            'dRow.Item("crtranunkid") = -1
            'dRow.Item("crmasterunkid") = mintClaimRequestMasterId
            'dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            'dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            'dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing OrElse txtCostingTag.Value = "", 0, txtCostingTag.Value)
            'dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            'dRow.Item("quantity") = txtQty.Text
            'dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            'dRow.Item("expense_remark") = txtExpRemark.Text
            'dRow.Item("isvoid") = False
            'dRow.Item("voiduserunkid") = -1
            'dRow.Item("voiddatetime") = DBNull.Value
            'dRow.Item("voidreason") = ""
            'dRow.Item("loginemployeeunkid") = -1
            'dRow.Item("AUD") = "A"
            'dRow.Item("GUID") = Guid.NewGuid.ToString
            'dRow.Item("expense") = cboExpense.SelectedItem.Text
            'dRow.Item("uom") = txtUoMType.Text
            'dRow.Item("voidloginemployeeunkid") = -1
            'dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)

            'mdtLeaveExpenseTran.Rows.Add(dRow) : Call Fill_Expense()
            'Call Enable_Disable_Ctrls(False) : Call Clear_Controls()


            'Pinkal (22-Oct-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .

            If Validation() = False Then
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End


            mblnIsExpenseEditClick = True

            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName5, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Language.getMessage(mstrModuleName5, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName5, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName5, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
                'Pinkal (04-Feb-2019) -- End
            End If



            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


            'Dim iRow As DataRow() = Nothing
            'If Validation() = False Then
            '    Exit Sub
            'End If
            'If mintClaimRequestTranId > 0 Then
            '    iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & mintClaimRequestTranId & "' AND AUD <> 'D'")
            'Else
            '    iRow = mdtLeaveExpenseTran.Select("GUID = '" & mstrEditGuid & "' AND AUD <> 'D'")
            'End If

            'If iRow IsNot Nothing Then
            '    Dim objExpMaster As New clsExpense_Master
            '    objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            '    If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '        If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '            If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
            '                Language.setLanguage(mstrModuleName5)
            '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
            '                txtQty.Focus()
            '                Exit Sub
            '            End If
            '        ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '            If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
            '                Language.setLanguage(mstrModuleName5)
            '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 19, "Sorry, you cannot set amount greater than balance set."), Me)
            '                txtQty.Focus()
            '                Exit Sub
            '            End If
            '        End If
            '    End If

            '    If dgvData.Items.Count >= 1 Then
            '        Dim objExpMasterOld As New clsExpense_Master
            '        Dim iEncashment As DataRow() = Nothing

            '        If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
            '            iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '        ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
            '            iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '        End If
            '        objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

            '        If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '            Language.setLanguage(mstrModuleName5)
            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '            Exit Sub
            '        End If
            '        objExpMasterOld = Nothing
            '    End If
            '    objExpMaster = Nothing

            '    iRow(0).Item("crtranunkid") = mintClaimRequestTranId
            '    iRow(0).Item("crmasterunkid") = mintClaimRequestMasterId
            '    iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            '    iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            '    iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
            '    iRow(0).Item("unitprice") = txtUnitPrice.Text
            '    iRow(0).Item("quantity") = txtQty.Text
            '    iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            '    iRow(0).Item("expense_remark") = txtExpRemark.Text
            '    iRow(0).Item("isvoid") = False
            '    iRow(0).Item("voiduserunkid") = -1
            '    iRow(0).Item("voiddatetime") = DBNull.Value
            '    iRow(0).Item("voidreason") = ""
            '    iRow(0).Item("loginemployeeunkid") = -1
            '    If iRow(0).Item("AUD").ToString.Trim = "" Then
            '        iRow(0).Item("AUD") = "U"
            '    End If
            '    iRow(0).Item("GUID") = Guid.NewGuid.ToString
            '    iRow(0).Item("expense") = cboExpense.SelectedItem.Text
            '    iRow(0).Item("uom") = txtUoMType.Text
            '    iRow(0).Item("voidloginemployeeunkid") = -1
            '    iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            '    mdtLeaveExpenseTran.AcceptChanges()

            '    Call Fill_Expense() : Call Clear_Controls()

            '    btnEdit.Visible = False : btnAdd.Visible = True
            '    cboExpense.Enabled = True
            '    iRow = Nothing
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelReasonYes.Click
        Try
            If mintClaimRequestTranId > 0 Then
                Dim iRow As DataRow() = Nothing
                iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & mintClaimRequestTranId & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("isvoid") = True
                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = txtClaimDelReason.Text.Trim
                    iRow(0).Item("AUD") = "D"

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        iRow(0).Item("voiduserunkid") = Session("UserId")
                        iRow(0).Item("voidloginemployeeunkid") = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        iRow(0).Item("voiduserunkid") = -1
                        iRow(0).Item("voidloginemployeeunkid") = Session("Employeeunkid")
                    End If
                    iRow(0).AcceptChanges()
                    'Shani (20-Aug-2016) -- Start
                    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                    Dim dRow() As DataRow = Nothing
                    dRow = mdtChildClaimAttachment.Select("transactionunkid = '" & mintClaimRequestTranId & "' AND AUD <> 'D'")

                    If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                        For Each dtRow As DataRow In dRow
                            dtRow.Item("AUD") = "D"
                        Next
                    End If
                    mdtFinalClaimAttchment.AcceptChanges()
                    'Shani (20-Aug-2016) -- End

                    mdtChildClaimAttachment.AcceptChanges()
                    Call Fill_Expense()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDelReasonYes_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtClaimAttchment = CType(Session("mdtClaimAttchment"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtClaimAttchment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtClaimAttchment.AcceptChanges()
                mintDeleteIndex = 0
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then
                mintDeleteIndex = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            mdtClaimAttchment.Select("").ToList().ForEach(Function(x) Add_DataRow(x, mstrModuleName1))
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtExpRemark.Text.Trim.Length <= 0 Then
                popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName5, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName5, 26, "Do you wish to continue?")
                popup_ExpRemarkYesNo.Show()
            Else
                If mblnIsExpenseEditClick Then
                    EditExpense()
                Else
                    AddExpense()
                End If
            End If
            'Pinkal (04-Feb-2019) -- End 
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (22-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Protected Sub P2PLvFormDeleteConfirmation_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles P2PLvFormDeleteConfirmation.buttonNo_Click
        Dim objLvForm As clsleaveform = Nothing
        Try
            '/* START TO DELETE WHOLE LEAVE FORM WHEN P2P REQUEST IS NOT SUCCEED.
            objLvForm = New clsleaveform
            objLvForm._Formunkid = mintLeaveFormUnkid
            objLvForm._ObjClaimRequestMst = CType(Session("ObjClaimMst"), clsclaim_request_master)
            objLvForm._Isvoid = True
            If CInt(Session("Employeeunkid")) > 0 Then
                objLvForm._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
            ElseIf CInt(Session("UserId")) > 0 Then
                objLvForm._Voiduserunkid = CInt(Session("UserId"))
            End If

            objLvForm._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objLvForm._Voidreason = "Voided due to connection fail or error from P2P system."
            If objLvForm.Delete(objLvForm._Formunkid, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                            , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), CStr(Session("UserAccessModeSetting")) _
                                                            , True, False, mdtFinalClaimAttchment, True, True, CInt(Session("LeaveBalanceSetting"))) = False Then
                DisplayMessage.DisplayMessage(objLvForm._Message, Me)
                Exit Sub
            Else
                Session("ObjClaimMst") = Nothing
                btnClose_Click(btnClose, New EventArgs())
            End If
            '/* END TO DELETE WHOLE LEAVE FORM WHEN P2P REQUEST IS NOT SUCCEED.
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLvForm = Nothing
        End Try
    End Sub

    Protected Sub P2PLvFormDeleteConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles P2PLvFormDeleteConfirmation.buttonYes_Click
        Dim objLvForm As clsleaveform = Nothing
        Try
            objLvForm = New clsleaveform
            objLvForm._Formunkid = mintLeaveFormUnkid
            SendLeaveApproverNotification(objLvForm)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLvForm = Nothing
        End Try
    End Sub

    'Pinkal (20-Nov-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick Then
                EditExpense()
            Else
                AddExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (04-Feb-2019) -- End

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtClaimAttchment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region "Day Fraction "

    Protected Sub btnFractionOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFractionOK.Click
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtTable As DataTable = mdtDayFration
            Dim dtTable As DataTable = mdtDayFration.Copy()
            'Pinkal (11-Sep-2020) -- End

            Dim dtDelete As DataTable = New DataView(mdtDayFration, "AUD = 'D'", "leavedate asc", DataViewRowState.CurrentRows).ToTable
            If dtDelete IsNot Nothing AndAlso dtDelete.Rows.Count > 0 Then
                For i As Integer = 0 To dtDelete.Rows.Count - 1
                    dtTable.ImportRow(dtDelete.Rows(i))
                Next
                dtTable.AcceptChanges()
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                objNoofDays.Text = CStr(dtTable.Compute("SUM(dayfraction)", "AUD <> 'D'"))
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtDayFration = dtTable
            mdtDayFration = dtTable.Copy()
            If dtDelete IsNot Nothing Then dtDelete.Clear()
            dtDelete = Nothing
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End
            popupFraction.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "ComboBox's Event"

    Private Sub cboLeaveType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        'Dim dsList As DataSet = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                SetEndDateForSickLeave()
            End If
            GetLeaveBalance()
            GetAsOnDateAccrue()
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            lnkScanDocuements.Visible = objLeaveType._IsCheckDocOnLeaveForm


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            mblnLvTypeSkipApproverFlow = objLeaveType._SkipApproverFlow
            lvLeaveExpense.Visible = Not mblnLvTypeSkipApproverFlow
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            mblnIsLvTypePaid = objLeaveType._IsPaid
            mintLvformMinDays = objLeaveType._LVDaysMinLimit
            mintLvformMaxDays = objLeaveType._LVDaysMaxLimit
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            mblnConsiderLeaveOnTnAPeriod = objLeaveType._ConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mblnIsBlockLeave = objLeaveType._IsBlockLeave
            mblnIsStopApplyAcrossFYELC = objLeaveType._IsStopApplyAcrossFYELC
            mblnIsShortLeave = objLeaveType._IsShortLeave
            mintDeductedLeaveTypeID = objLeaveType._DeductFromLeaveTypeunkid
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            mblnConsiderExpense = objLeaveType._ConsiderExpense
            lvLeaveExpense.Visible = mblnConsiderExpense
            'Pinkal (03-May-2019) -- End


            dtpEndDate_TextChanged(sender, e)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dsList = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            GetELCDates()
            cboLeaveType.SelectedIndex = -1
            objNoofDays.Text = "0.00"
            objLeaveAccrue.Text = "0.00"
            objAsonDateAccrue.Text = "0.00"
            objAsonDateBalance.Text = "0.00"
            SetEndDateForSickLeave()
            dtpEndDate_TextChanged(sender, e)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboExpEmployee.SelectedIndexChanged
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            Dim objLeaveType As New clsleavetype_master
            Dim mdtLeaveType As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, "ISNULL(isleaveencashment,0) = 0 AND ISNULL(cmexpense_master.isshowoness,0) = 1")
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, "ISNULL(isleaveencashment,0) = 0")
            End If

            'Dim dtTable As DataTable = Nothing
            'dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable

            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            'Pinkal (26-Dec-2018) -- End


            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

            If CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_LEAVE

                        objlblValue.Text = Language.getMessage(mstrModuleName5, 12, "Leave Form")
                        If mintClaimRequestMasterId > 0 Then
                            cboLeaveType.Enabled = False
                        Else
                            cboLeaveType.Enabled = True
                            If cboReference.DataSource IsNot Nothing Then cboReference.SelectedIndex = 0
                            cboReference.Enabled = True
                        End If


                        'Pinkal (25-May-2019) -- Start
                        'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, CStr(Session("Database_Name")), "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, CStr(Session("Database_Name")), "", False)
                        End If
                        'Pinkal (25-May-2019) -- End

                        mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(cboLeaveType.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

                        With cboExpLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                        End With
                        objLeaveType = Nothing
                        Call cboExpLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())

                End Select

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If
            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing : cboReference.Enabled = False : cboLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            If mdtLeaveType IsNot Nothing Then mdtLeaveType.Clear()
            mdtLeaveType = Nothing
            objExpMaster = Nothing
            objLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try

            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CBool(Session("SectorRouteAssignToExpense")) = True Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                End With
                objAssignExpense = Nothing
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing
                'Pinkal (11-Sep-2020) -- End
            End If
            'Pinkal (20-Feb-2019) -- End

            Dim objExpMaster As New clsExpense_Master
            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment
                cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.SelectedValue = "0"


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboCostCenter.SelectedValue = "0"
                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If
                'Pinkal (25-May-2019) -- End
                'Pinkal (04-Feb-2019) -- End

                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End
                    txtBalance.Text = "0.00"
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")))
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = CStr(Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")))
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                    'txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing


                    'Pinkal (09-Aug-2018) -- Start
                    'Bug - Solving Bug for PACRA.

                    'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                    '    dsList = objLeave.GetList("List", _
                    '                              CStr(Session("Database_Name")), _
                    '                              CInt(Session("UserId")), _
                    '                              CInt(Session("Fin_year")), _
                    '                              CInt(Session("CompanyUnkId")), _
                    '                              Session("EmployeeAsOnDate").ToString, _
                    '                              CStr(Session("UserAccessModeSetting")), True, _
                    '                               False, True, False, False, _
                    '                              CInt(cboExpEmployee.SelectedValue), False, False, False, "", Nothing)

                    'ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                    '    dsList = objLeave.GetList("List", _
                    '                              CStr(Session("Database_Name")), _
                    '                              CInt(Session("UserId")), _
                    '                              CInt(Session("Fin_year")), _
                    '                              CInt(Session("CompanyUnkId")), _
                    '                              CStr(Session("EmployeeAsOnDate")), _
                    '                              CStr(Session("UserAccessModeSetting")), True, _
                    '                              False, True, False, False, _
                    '                              CInt(cboExpEmployee.SelectedValue), True, True, False, "", Nothing)
                    'End If

                    'dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable


                    Dim mstrLeaveTypeID As String = ""
                    Dim objleavetype As New clsleavetype_master
                    mstrLeaveTypeID = objleavetype.GetDeductdToLeaveTypeIDs(CInt(objExpMaster._Leavetypeunkid))

                    If mstrLeaveTypeID.Trim.Length > 0 Then
                        mstrLeaveTypeID &= "," & CInt(objExpMaster._Leavetypeunkid).ToString()
                    Else
                        mstrLeaveTypeID = CInt(objExpMaster._Leavetypeunkid).ToString()
                    End If

                    objleavetype = Nothing

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.GetDate.Date, mstrLeaveTypeID, CInt(cboEmployee.SelectedValue).ToString(), CInt(Session("LeaveAccrueTenureSetting")) _
                                                                     , CInt(Session("LeaveAccrueDaysAfterEachMonth")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                                     , False, False, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")), Nothing)
                    Else
                        objLeave._DBStartdate = CDate(Session("fin_startdate")).Date
                        objLeave._DBEnddate = CDate(Session("fin_enddate")).Date

                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.GetDate.Date, mstrLeaveTypeID, CInt(cboEmployee.SelectedValue).ToString(), CInt(Session("LeaveAccrueTenureSetting")) _
                                                                     , CInt(Session("LeaveAccrueDaysAfterEachMonth")), Nothing, Nothing _
                                                                     , True, True, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")), Nothing)
                    End If


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'dtbalance = dsList.Tables(0)
                    dtbalance = dsList.Tables(0).Copy()
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objLeave = Nothing
                    'Pinkal (11-Sep-2020) -- End




                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        'txtBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        ''Pinkal (30-Apr-2018) - Start
                        ''Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

                        'If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then
                        '    If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = CDate(Session("fin_enddate")).Date
                        '    If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                        '        txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        '    Else
                        '        txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        '    End If

                        'ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then

                        '    'Pinkal (18-Jul-2018) -- Start
                        '    'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.

                        '    Dim mdtDate As Date = dtpDate.GetDate.Date
                        '    Dim mdtDays As Integer = 0
                        '    'If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '    '    intDiff = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '    '    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '    'Else
                        '    '    intDiff = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date))
                        '    'End If

                        '    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        '        If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                        '            mdtDate = CDate(Session("fin_enddate")).Date
                        '            'Pinkal (24-Jul-2018) -- Start
                        '            'Bug - Taking Wrong Month Count.
                        '            'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                        '            mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).AddDays(1).Date))
                        '            'Pinkal (24-Jul-2018) -- End
                        '        Else
                        '            If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '                mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '                'Pinkal (24-Jul-2018) -- Start
                        '                'Bug - Taking Wrong Month Count.
                        '                'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).AddDays(1).Date))
                        '                'Pinkal (24-Jul-2018) -- End
                        '            End If
                        '        End If

                        '    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                        '        If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '        End If
                        '        'Pinkal (24-Jul-2018) -- Start
                        '        'Bug - Taking Wrong Month Count.
                        '        'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).AddDays(1).Date))
                        '        'Pinkal (24-Jul-2018) -- End
                        '    End If

                        '    Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                        '    If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                        '        intDiff = intDiff - 1
                        '    End If

                        '    'Pinkal (18-Jul-2018) -- End

                        '    txtBalanceAsOnDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        'End If

                        'Pinkal (30-Apr-2018) - End

                        txtBalance.Text = CDec(CDec(dtbalance.Rows(0)("TotalAcccrueAmt")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))).ToString("#0.00")
                        txtBalanceAsOnDate.Text = CDec(CDec(dtbalance.Rows(0)("Accrue_amount")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))).ToString("#0.00")

                        'Pinkal (09-Aug-2018) -- End

                        txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 6, "Quantity")
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    'Pinkal (11-Sep-2020) -- End


                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")))
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    'Pinkal (30-Apr-2018) - End
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = CStr(Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")))
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    objEmpExpBal = Nothing
                End If
            Else
                txtUoMType.Text = ""
                txtUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                'Pinkal (30-Apr-2018) - Start
                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                txtBalance.Text = "0.00"
                txtBalanceAsOnDate.Text = "0.00"
                'Pinkal (30-Apr-2018) - End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                cboCostCenter.Enabled = False
                cboCostCenter.SelectedValue = "0"
                'Pinkal (20-Nov-2018) -- End


                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtQty.Text = "1"
                'Pinkal (04-Feb-2020) -- End


            End If

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
            Else
                txtQty.Text = "1"
            End If
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If objExpMaster._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                lblExpBalance.Visible = True
                lblBalanceasondate.Visible = True
                txtBalance.Visible = True
                txtBalanceAsOnDate.Visible = True
                cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                cboCurrency.Enabled = False
            Else
                lblExpBalance.Visible = False
                lblBalanceasondate.Visible = False
                txtBalance.Visible = False
                txtBalanceAsOnDate.Visible = False

                If mdtLeaveExpenseTran IsNot Nothing Then
                    If mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = CStr(mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                    End If
                End If

            End If
            'Pinkal (04-Feb-2019) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboExpense_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboExpLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform
            Dim dsLeave As New DataSet
            Dim mdtLeaveForm As DataTable = Nothing

            If CInt(IIf(cboLeaveType.SelectedValue = "", 0, cboLeaveType.SelectedValue)) = 0 Then
                dsLeave = objleave.GetLeaveFormForExpense(0, "2,7", CInt(cboEmployee.SelectedValue), True, True, mintLeaveFormUnkid)
            Else
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "2,7", CInt(cboEmployee.SelectedValue), True, True, mintLeaveFormUnkid)
            End If

            If mintLeaveFormUnkid > 0 Then
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkid, "", DataViewRowState.CurrentRows).ToTable
            Else
                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso mintLeaveFormUnkid <= 0 Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkid, "", DataViewRowState.CurrentRows).ToTable
                ElseIf CBool(Session("PaymentApprovalwithLeaveApproval")) = False AndAlso mintLeaveFormUnkid <= 0 Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkid, "", DataViewRowState.CurrentRows).ToTable
                Else
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = mdtLeaveForm.Copy
                .DataBind()
                If mintLeaveFormUnkid <= 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = CStr(mintLeaveFormUnkid)
                End If
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            If mdtLeaveForm IsNot Nothing Then mdtLeaveForm.Clear()
            mdtLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End

            objleave = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboLeaveType_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DatePicker'S Event"

    Protected Sub dtpStartDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpStartDate.TextChanged
        Try
            If CInt(cboLeaveType.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                If mintLeaveFormUnkid <= 0 Then
                    'mdtDayFration = Nothing
                    CalculateDays(sender)
                    objNoofDays.Text = (DateDiff(DateInterval.Day, dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date)).ToString()
                Else
                    CalculateDays(sender)
                End If

                If dtpStartDate.GetDate.Date > dtpEndDate.GetDate.Date Then
                    objNoofDays.Text = "0"
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (06-Dec-2016) -- Start
    'Enhancement - Working on Budget Employee timesheet Changes as Per Suzan and Andrew Requirement.
    'Protected Sub dtpApplyDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpApplyDate.TextChanged
    '    Try
    '        Call Entryvalid()
    '        Call GetAsOnDateAccrue()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Pinkal (06-Dec-2016) -- End


    Protected Sub dtpEndDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEndDate.TextChanged
        Try
            Dim mdtStartdate As Date = dtpStartDate.GetDate.Date
            Dim mdtEndDate As Date = Nothing

            If Not dtpEndDate.IsNull Then
                mdtEndDate = dtpEndDate.GetDate.Date
            Else
                mdtEndDate = dtpEndDate.GetDate.Date
            End If

            If CInt(cboLeaveType.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                If mintLeaveFormUnkid <= 0 Then
                    mdtDayFration = Nothing
                    CalculateDays(sender)
                    objNoofDays.Text = (DateDiff(DateInterval.Day, mdtStartdate.Date, mdtEndDate.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), mdtStartdate.Date, mdtEndDate.Date)).ToString()
                Else
                    CalculateDays(sender)
                End If
            End If

            If Entryvalid() = False Then
                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                GetAsOnDateAccrue()
                'Pinkal (16-Dec-2016) -- End
                Exit Sub
            End If

            If mdtStartdate.Date > mdtEndDate.Date Then
                Entryvalid()
                If Not dtpEndDate.IsNull Then

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'dtpEndDate.SetDate = CDate(FormatDateTime(CDate(Session("fin_enddate")), DateFormat.ShortDate))
                    dtpEndDate.SetDate = CDate(Session("fin_enddate")).Date
                    'Pinkal (16-Apr-2016) -- End

                End If
            Else

                If CInt(cboLeaveType.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                    If mintLeaveFormUnkid <= 0 Then
                        mdtDayFration = Nothing
                        CalculateDays(sender)
                        objNoofDays.Text = (DateDiff(DateInterval.Day, mdtStartdate.Date, mdtEndDate.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), mdtStartdate.Date, mdtEndDate.Date)).ToString()
                    Else
                        CalculateDays(sender)
                    End If
                End If
            End If
            If mdtStartdate.Date > mdtEndDate.Date Then
                objNoofDays.Text = "0"
            End If

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            GetAsOnDateAccrue()
            'Pinkal (16-Dec-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region "Leave Expense"

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, cboSectorRoute.SelectedIndexChanged
        Try
            Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
            Dim objCosting As New clsExpenseCosting
            objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.GetDate)
            txtCosting.Text = Format(CDec(iAmount), CStr(Session("fmtCurrency")))
            txtCostingTag.Value = CStr(iCostingId)
            txtUnitPrice.Text = Format(CDec(iAmount), CStr(Session("fmtCurrency")))

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Dim objExpnese As New clsExpense_Master
            objExpnese._Expenseunkid = CInt(cboExpense.SelectedValue)
            'Pinkal (25-Oct-2018) -- End
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then

                If objExpnese._Uomunkid = enExpUoM.UOM_QTY Then

                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'txtQty.Text = "0"
                    'Else
                    txtQty.Text = "1"
                    'Pinkal (04-Feb-2020) -- End
                End If

            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                txtQty.Text = "1"
            End If
            objCosting = Nothing


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If objExpnese._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End

            objExpnese = Nothing
            'Pinkal (25-Oct-2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpDate_TextChanged" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#End Region

#Region "LinkButton'S Event"

    Protected Sub lnkScanDocuements_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkScanDocuements.Click
        Try

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                cboLeaveType.Focus()
                Exit Sub
            End If
            'Shani (20-Aug-2016) -- End

            'LVForm_EmployeeID LVForm_LeaveTypeID
            Session("ScanAttchLableCaption") = "Select Employee"
            Session("ScanAttchRefModuleID") = enImg_Email_RefId.Leave_Module
            Session("ScanAttchenAction") = enAction.ADD_ONE
            Session("ScanAttchToEditIDs") = ""
            Session("LVForm_ApplyDate") = dtpApplyDate.GetDate.Date
            Session("LVForm_StartDate") = dtpStartDate.GetDate.Date
            Session("LVForm_EndDate") = dtpEndDate.GetDate.Date
            Session("LVForm_EmployeeID") = CInt(cboEmployee.SelectedValue)
            Session("LVForm_LeaveTypeID") = CInt(cboLeaveType.SelectedValue)
            Session("LVForm_ClaimExpense") = mdtFinalClaimTransaction
            Session("ScanAttachData") = mdtAttachement
            Session("mdtDayFration") = mdtDayFration


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Session("LeaveAddress") = txtLeaveAddress.Text.Trim()
            Session("LeaveReason") = txtLeaveReason.Text.Trim()
            'Pinkal (03-May-2019) -- End


            Response.Redirect(Session("rootpath").ToString & "Others_Forms/wPg_ScanOrAttachmentInfo.aspx?" & Server.UrlEncode(clsCrypto.Encrypt(mintLeaveFormUnkid & "|" & enScanAttactRefId.LEAVEFORMS)), False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkLeaveDayCount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveDayCount.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please select Employee.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If

            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please select Leave Type.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If
            If dtpStartDate.IsNull = True Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please select Start date.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objDayFraction As New clsleaveday_fraction
            'Pinkal (11-Sep-2020) -- End

            objDayFraction.GetDayFraction(mdtDayFration, _
                                          mintLeaveFormUnkid, _
                                          CInt(cboEmployee.SelectedValue), _
                                          dtpStartDate.GetDate.Date, _
                                          CInt(cboLeaveType.SelectedValue), _
                                          CStr(Session("EmployeeAsOnDate")), _
                                          dtpEndDate.GetDate.Date)
            If dgFraction.EditIndex >= 0 Then dgFraction.EditIndex = -1
            If dgFraction.PageIndex > 0 Then dgFraction.PageIndex = 0
            dgFraction.DataSource = New DataView(mdtDayFration, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
            dgFraction.DataBind()
            popupFraction.Show()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objDayFraction = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvLeaveExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLeaveExpense.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                cboLeaveType.Focus()
                Exit Sub
            End If

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            If IsValidExpense_Eligility() = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type.Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", Me)
                Exit Sub
            End If
            'Pinkal (29-Sep-2017) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If clsleavetype Is Nothing Then clsleavetype = New clsleavetype_master
            Dim clsleavetype As New clsleavetype_master
            'Pinkal (11-Sep-2020) -- End

            clsleavetype._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            If clsleavetype._IsPaid = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 35, "Sorry,You cannot apply leave expense for this leave type.Reason : This Leave Type is unpaid leave type."), Me)
                cboLeaveType.Focus()
                Exit Sub
            End If
            clsleavetype = Nothing

            txtUnitPrice.Attributes.Add("onfocusin", "select();")
            Call SetLeaveExpenseVisibility()
            Call FillLeaveExpenseCombo()
            Call GetLeaveExpenseValue()
            txtUnitPrice.Enabled = False

            If mdtFinalClaimTransaction IsNot Nothing Then
                mdtLeaveExpenseTran = mdtFinalClaimTransaction
            End If
            Call Fill_Expense()
            cboExpCategory.Focus()
            mblnClaimRequest = True
            mdtChildClaimAttachment = mdtFinalClaimAttchment.Copy
            popupExpense.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub LnkViewDependants_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewDependants.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName5, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, True, dtpDate.GetDate.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpDate.GetDate.Date)
            Else
                Exit Sub
            End If
            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()
            popupEmpDepedents.Show()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objDependant = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView(S) Event"

    Protected Sub dgvData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.DeleteCommand
        Try
            Dim iRow As DataRow() = Nothing
            txtClaimDelReason.Text = ""
            If CInt(e.Item.Cells(10).Text) > 0 Then
                iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    mintClaimRequestTranId = CInt(iRow(0).Item("crtranunkid"))
                    popupClaim.Show()
                End If
            ElseIf CStr(e.Item.Cells(12).Text) <> "" Then
                iRow = mdtLeaveExpenseTran.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                End If
                iRow(0).AcceptChanges()

                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                Dim dRow() As DataRow = Nothing
                If CStr(e.Item.Cells(12).Text) <> "" Then
                    dRow = mdtChildClaimAttachment.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                End If
                If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                    For Each dtRow As DataRow In dRow
                        dtRow.Item("AUD") = "D"
                    Next
                End If
                mdtChildClaimAttachment.AcceptChanges()
                'Shani (20-Aug-2016) -- End
                Fill_Expense()

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Clear_Controls()
                'Pinkal (04-Feb-2019) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvData_DeleteCommand:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        Try
            If e.CommandName = "Edit" Then
                Dim iRow As DataRow() = Nothing
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    iRow = mdtLeaveExpenseTran.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboExpense.SelectedValue = CStr(iRow(0).Item("expenseunkid"))
                    Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
                    cboSectorRoute.SelectedValue = CStr(iRow(0).Item("secrouteunkid"))
                    Call dtpDate_TextChanged(dtpDate, New EventArgs())

                    txtQty.Text = CStr(CDec(iRow(0).Item("quantity")))
                    txtUnitPrice.Text = CStr(CDec(Format(CDec(iRow(0).Item("unitprice")), CStr(Session("fmtCurrency")))))
                    txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))

                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    mintClaimRequestTranId = 0
                    mstrEditGuid = ""
                    'Pinkal (20-Nov-2018) -- End

                    cboExpense.Enabled = False
                    If CInt(e.Item.Cells(10).Text) > 0 Then
                        mintClaimRequestTranId = CInt(iRow(0).Item("crtranunkid"))
                    ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                        mstrEditGuid = CStr(iRow(0).Item("GUID"))
                    End If
                End If
                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            ElseIf e.CommandName.ToUpper = "ATTACHMENT" Then
                mdtClaimAttchment = mdtChildClaimAttachment.Clone
                Dim xRow() As DataRow = Nothing
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    xRow = mdtChildClaimAttachment.Select("transactionunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    xRow = mdtChildClaimAttachment.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If xRow.Count > 0 Then
                    mdtClaimAttchment = xRow.CopyToDataTable
                End If

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'mintDeleteIndex = e.Item.ItemIndex
                mintExpenseIndex = e.Item.ItemIndex
                'Pinkal (18-Mar-2021) -- End

                Call FillAttachment()
                blnShowAttchmentPopup = True
                popup_ScanAttchment.Show()
                'Shani (20-Aug-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvData_ItemCommand:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(7).Text.Trim.Length > 0 Then  'Unit Price
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), CStr(Session("fmtCurrency")))
                End If
                If e.Item.Cells(8).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), CStr(Session("fmtCurrency")))
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvData_ItemDataBound:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#Region "Day Fracation"

    Protected Sub dgFraction_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgFraction.PageIndexChanging
        Try
            dgFraction.PageIndex = e.NewPageIndex
            GetFractionList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

    Protected Sub dgFraction_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgFraction.RowDataBound
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Row.RowIndex >= 0 Then

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If Session("DateFormat").ToString <> Nothing Then
                '    e.Row.Cells(1).Text = CDate(e.Row.Cells(1).Text).Date.ToString(Session("DateFormat").ToString)
                'Else
                '    e.Row.Cells(1).Text = CStr(CDate(e.Row.Cells(1).Text).Date)
                'End If
                e.Row.Cells(1).Text = CDate(e.Row.Cells(1).Text).Date.ToShortDateString()

                'Pinkal (16-Apr-2016) -- End


                If e.Row.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Row.Cells(2).Text.Trim <> "" Then
                    e.Row.Cells(2).Text = CDec(e.Row.Cells(2).Text).ToString("#0.00")
                End If

                If e.Row.Cells(2).Controls.Count > 0 Then
                    CType(e.Row.Cells(2).Controls(0), TextBox).CssClass = "RightTextAlign"
                    CType(e.Row.Cells(2).Controls(0), TextBox).Text = CDec(CType(e.Row.Cells(2).Controls(0), TextBox).Text).ToString("#0.00")
                    CType(e.Row.Cells(2).Controls(0), TextBox).Focus()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgFraction_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgFraction.RowEditing
        Try
            dgFraction.EditIndex = e.NewEditIndex
            GetFractionList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

    Protected Sub dgFraction_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgFraction.RowUpdating
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtTab As DataTable = New DataView(mdtDayFration, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTab As DataTable = New DataView(mdtDayFration, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable().Copy()
            'Pinkal (11-Sep-2020) -- End

            For i = 0 To dgFraction.Columns.Count - 1

                If TypeOf (dgFraction.Columns(i)) Is CommandField Or TypeOf (dgFraction.Columns(i)) Is TemplateField Then Continue For

                If DirectCast(DirectCast(dgFraction.Columns(i), System.Web.UI.WebControls.DataControlField), System.Web.UI.WebControls.BoundField).ReadOnly = False Then

                    Dim cell As DataControlFieldCell = CType(dgFraction.Rows(e.RowIndex).Cells(i), DataControlFieldCell)
                    cell.HorizontalAlign = HorizontalAlign.Right
                    dgFraction.Columns(i).ExtractValuesFromCell(e.NewValues, cell, DataControlRowState.Edit, True)


                    For Each key As String In e.NewValues.Keys
                        Dim str As String = CStr(e.NewValues(key))

                        If dtTab.Columns(key).DataType Is System.Type.GetType("System.Decimal") Then
                            Dim decTemp As Decimal = 1
                            If Decimal.TryParse(str, decTemp) = True Then

                                If decTemp < 0.05 Or decTemp > 1 Then
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError(ex, Me)
                                    DisplayMessage.DisplayMessage("Please enter day fraction between 0.05 to 1.", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    e.Cancel = True
                                    cell.Focus()
                                    Exit Sub
                                End If

                                dtTab.Rows(e.RowIndex).Item(key) = decTemp.ToString("#0.00")

                                If mintLeaveFormUnkid > 0 AndAlso dtTab.Rows(e.RowIndex)("AUD").ToString <> "D" Then
                                    dtTab.Rows(e.RowIndex).Item("AUD") = "U"
                                End If

                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("Please enter proper day fraction.", Me)
                                'Sohail (23 Mar 2019) -- End
                                e.Cancel = True
                                cell.Focus()
                                Exit Sub
                            End If

                        Else
                            dtTab.Rows(e.RowIndex).Item(key) = str
                        End If

                    Next
                End If
            Next

            If e.NewValues.Count > 0 Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtDayFration = dtTab
                mdtDayFration = dtTab.Copy()

                If dtTab IsNot Nothing Then dtTab.Clear()
                dtTab = Nothing
                'Pinkal (11-Sep-2020) -- End

                dgFraction.EditIndex = -1
                GetFractionList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

    Protected Sub dgFraction_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgFraction.RowCancelingEdit
        Try
            dgFraction.EditIndex = -1
            GetFractionList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

#End Region


    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtClaimAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtClaimAttchment.Select("GUID = '" & e.Item.Cells(2).Text.Trim & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow.Length > 0 Then
                '        popup_YesNo.Message = Language.getMessage(mstrModuleName5, 39, "Are you sure you want to delete this attachment?")
                '        mintDeleteIndex = mdtClaimAttchment.Rows.IndexOf(xrow(0))
                '        popup_YesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtClaimAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtClaimAttchment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Language.getMessage(mstrModuleName5, 39, "Are you sure you want to delete this attachment?")
                        mintDeleteIndex = mdtClaimAttchment.Rows.IndexOf(xrow(0))
                        popup_YesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""
                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
                'S.SANDEEP |16-MAY-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " TextBox Event "

#Region "Leave Expense"

    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Try
            If txtQty.Text.Trim.Length > 0 Then
                If IsNumeric(txtQty.Text) = False Then

                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'txtQty.Text = "0"
                    txtQty.Text = "1"
                    'Pinkal (04-Feb-2020) -- End
                    Exit Sub
                End If
                If CInt(txtQty.Text) > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If mblnIsLeaveEncashment = False Then
                        txtUnitPrice.Enabled = True
                        If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                            txtUnitPrice.Text = CStr(CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())))
                            txtUnitPrice.Focus()
                        End If
                    Else
                        txtUnitPrice.Enabled = False
                    End If
                ElseIf CInt(txtQty.Text) <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                        txtUnitPrice.Text = CStr(CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())))
                    End If
                End If
            Else
                txtUnitPrice.Enabled = False
            End If

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            objExpense = Nothing
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtQty_TextChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub txtUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.TextChanged
        Try
            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
                Exit Sub
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtUnitPrice_TextChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

#End Region


#End Region


    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName1)

            Me.Title = Language._Object.getCaption(mstrModuleName1, "Add / Edit Leave Form")
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName1, "Add / Edit Leave Form")
            Me.lblDetialHeader.Text = Language._Object.getCaption(mstrModuleName1, "Add / Edit Leave Form")


            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.ID, Me.lblFormNo.Text)
            Me.lblApplyDate.Text = Language._Object.getCaption(Me.lblApplyDate.ID, Me.lblApplyDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblReturnDate.Text = Language._Object.getCaption(Me.lblReturnDate.ID, Me.lblReturnDate.Text)
            Me.lblLeaveAddress.Text = Language._Object.getCaption(Me.lblLeaveAddress.ID, Me.lblLeaveAddress.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.lblDays.Text = Language._Object.getCaption(Me.lblDays.ID, Me.lblDays.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lnkLeaveDayCount.Text = Language._Object.getCaption(Me.lnkLeaveDayCount.ID, Me.lnkLeaveDayCount.Text)
            Me.lvLeaveExpense.Text = Language._Object.getCaption(Me.lvLeaveExpense.ID, Me.lvLeaveExpense.Text)
            Me.LblELCStart.Text = Language._Object.getCaption(Me.LblELCStart.ID, Me.LblELCStart.Text)
            Me.lblLeaveAccrue.Text = Language._Object.getCaption(Me.lblLeaveAccrue.ID, Me.lblLeaveAccrue.Text)
            Me.LblAsonDateAccrue.Text = Language._Object.getCaption(Me.LblAsonDateAccrue.ID, Me.LblAsonDateAccrue.Text)
            Me.lblAsonDateBalance.Text = Language._Object.getCaption(Me.lblAsonDateBalance.ID, Me.lblAsonDateBalance.Text)
            Me.lnkScanDocuements.Text = Language._Object.getCaption(Me.lnkScanDocuements.ID, Me.lnkScanDocuements.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


            Language.setLanguage(mstrModuleName5)

            Me.lblpopupHeader.Text = Language._Object.getCaption(mstrModuleName5, "Claims & Request")
            Me.Label2.Text = Language._Object.getCaption(mstrModuleName5, "Claims & Request")
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblExpEmployee.Text = Language._Object.getCaption("lblEmployee", Me.lblExpEmployee.Text)
            Me.lblExpLeaveType.Text = Language._Object.getCaption("lblLeaveType", Me.lblExpLeaveType.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblExpBalance.Text = Language._Object.getCaption("lblBalance", Me.lblExpBalance.Text)
            Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Language._Object.getCaption(Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.ID, Me.lblSector.Text)
            Me.tbExpenseRemark.HeaderText = Language._Object.getCaption(Me.tbExpenseRemark.ID, Me.tbExpenseRemark.HeaderText)
            Me.tbClaimRemark.HeaderText = Language._Object.getCaption(Me.tbClaimRemark.ID, Me.tbClaimRemark.HeaderText)
            Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)
            Me.lnkViewDependants.Text = Language._Object.getCaption(Me.lnkViewDependants.ID, Me.lnkViewDependants.Text)

            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            lblBalanceasondate.Text = Language._Object.getCaption(Me.lblBalanceasondate.ID, Me.lblBalanceasondate.Text)
            'Pinkal (30-Apr-2018) - Start

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            'Pinkal (20-Nov-2018) -- End

            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
            Me.btnSaveAddEdit.Text = Language._Object.getCaption("btnSave", Me.btnSaveAddEdit.Text).Replace("&", "")
            Me.btnCloseAddEdit.Text = Language._Object.getCaption("btnClose", Me.btnCloseAddEdit.Text).Replace("&", "")


            dgvData.Columns(2).HeaderText = Language._Object.getCaption(dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            dgvData.Columns(3).HeaderText = Language._Object.getCaption(dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            dgvData.Columns(4).HeaderText = Language._Object.getCaption(dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            dgvData.Columns(5).HeaderText = Language._Object.getCaption(dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            dgvData.Columns(6).HeaderText = Language._Object.getCaption(dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)
            dgvData.Columns(7).HeaderText = Language._Object.getCaption(dgvData.Columns(7).FooterText, dgvData.Columns(7).HeaderText)
            dgvData.Columns(8).HeaderText = Language._Object.getCaption(dgvData.Columns(8).FooterText, dgvData.Columns(8).HeaderText)

            Language.setLanguage(mstrModuleName2)

            Me.LblEmpDependentsList.Text = Language._Object.getCaption(mstrModuleName2, Me.LblEmpDependentsList.Text)
            Me.Label3.Text = Language._Object.getCaption(mstrModuleName2, Me.Label3.Text)
            Me.btnEmppnlEmpDepedentsClose.Text = Language._Object.getCaption("btnClose", Me.btnEmppnlEmpDepedentsClose.Text).Replace("&", "")

            dgDepedent.Columns(0).HeaderText = Language._Object.getCaption(dgDepedent.Columns(0).FooterText, dgDepedent.Columns(0).HeaderText)
            dgDepedent.Columns(1).HeaderText = Language._Object.getCaption(dgDepedent.Columns(1).FooterText, dgDepedent.Columns(1).HeaderText)
            dgDepedent.Columns(2).HeaderText = Language._Object.getCaption(dgDepedent.Columns(2).FooterText, dgDepedent.Columns(2).HeaderText)
            dgDepedent.Columns(3).HeaderText = Language._Object.getCaption(dgDepedent.Columns(3).FooterText, dgDepedent.Columns(3).HeaderText)
            dgDepedent.Columns(4).HeaderText = Language._Object.getCaption(dgDepedent.Columns(4).FooterText, dgDepedent.Columns(4).HeaderText)



        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName1, 1, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName1, 2, "Form No cannot be blank. Form No is required information.")
            Language.setMessage(mstrModuleName1, 3, "Leave Type is compulsory information.Please Select Leave Type.")
            Language.setMessage(mstrModuleName1, 4, "Sorry, Apply date should not be greater than Start date.")
            Language.setMessage(mstrModuleName1, 5, "Please Assign Approver to this employee and also map approver to system user.")
            Language.setMessage(mstrModuleName1, 6, "Please Map this employee's Approver to system user.")
            Language.setMessage(mstrModuleName1, 9, "Please select correct start date. Reason:This date period is already closed.")
            Language.setMessage(mstrModuleName1, 10, "Please select correct end date. Reason:This date period is already closed.")
            Language.setMessage(mstrModuleName1, 11, "Please select correct apply date. Reason:This date period is already closed.")
            Language.setMessage(mstrModuleName1, 12, "Please Map this Leave type to this employee's Approver(s).")
            Language.setMessage(mstrModuleName1, 13, "This leave type is invalid for selected employee.")
            Language.setMessage(mstrModuleName1, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type.")
            Language.setMessage(mstrModuleName1, 16, "ELC Start From")
            Language.setMessage(mstrModuleName1, 17, "Sorry, you cannot apply for this leave now as your previous leave form has not been issued yet.")
            Language.setMessage(mstrModuleName1, 18, "Please Enter Accrue amount for this paid leave.")
            Language.setMessage(mstrModuleName1, 19, "This particular leave is mapped with")
            Language.setMessage(mstrModuleName1, 20, " , which does not have the accrue amount. Please add accrue amount for")
            Language.setMessage(mstrModuleName1, 21, " inorder to issue leave.")
            Language.setMessage(mstrModuleName1, 22, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency.")
            Language.setMessage(mstrModuleName1, 23, "You cannot create this leave form.Reason : As these days are falling in holiday(s) and weekend.")
            Language.setMessage(mstrModuleName1, 24, "Are you sure you want to save leave form without any leave expense ?")
            Language.setMessage(mstrModuleName1, 25, "You have applied")
            Language.setMessage(mstrModuleName1, 26, "leave for")
            Language.setMessage(mstrModuleName1, 27, "days , from")
            Language.setMessage(mstrModuleName1, 28, "to")
            Language.setMessage(mstrModuleName1, 29, "Are you sure of the number of days and selected dates? If yes click YES to save the application, otherwise click NO to make corrections.")
            Language.setMessage(mstrModuleName1, 32, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to apply for this leave ?")
            Language.setMessage(mstrModuleName1, 34, "Scan/Document(s) is compulsory information.Please attach Scan/Document(s) as per this leave type setting.")
            Language.setMessage(mstrModuleName1, 35, "Sorry,You cannot apply leave expense for this leave type.Reason : This Leave Type is unpaid leave type.")
            Language.setMessage(mstrModuleName1, 37, "You cannot apply for this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type.")
            Language.setMessage(mstrModuleName1, 38, "You are not eligible to apply for this leave. Reason : Eligibility to apply for this Leave Type does not match with Eligibility days on leave balance.")
            Language.setMessage(mstrModuleName1, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type.Eligible days set for the selected leave type is")
            Language.setMessage(mstrModuleName1, 40, "You cannot apply for this leave form.Reason : This employee is already applied for employee budget timesheet for this tenure.")
            Language.setMessage(mstrModuleName1, 41, "Sorry, you cannot apply for this leave type as it has been configured with setting for minimum")
            Language.setMessage(mstrModuleName1, 42, "Sorry, you cannot apply for this leave type as it has been configured with setting for maximum")
            Language.setMessage(mstrModuleName1, 43, "day(s) to be applied for this leave type.")
            Language.setMessage(mstrModuleName1, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type.")
            Language.setMessage(mstrModuleName1, 45, "Reliever is compulsory information.Please select reliever to apply for the leave application.")
            Language.setMessage(mstrModuleName1, 46, "Are you sure want to save this leave application without expense application ?")
            Language.setMessage(mstrModuleName1, 52, "Leave Application saved successfully.")
            Language.setMessage(mstrModuleName1, 53, "Leave Application updated successfully.")
            Language.setMessage(mstrModuleName1, 54, "Sorry ! You have to apply")
            Language.setMessage(mstrModuleName1, 55, "before applying this type of leave.")
            Language.setMessage(mstrModuleName1, 56, "Sorry ! This leave type cannot overlap to next year.")
            Language.setMessage(mstrModuleName1, 57, "Sorry, you cannot apply for this leave application.Reason:As Per setting configured on leave type master you can not apply for future date(s).")
            Language.setMessage(mstrModuleName1, 58, "Sorry, you cannot apply for this leave application.Reason: This leave type does not match maximum future days limit configured on leave type master.")

            Language.setMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Language.setMessage("clsclaim_request_master", 4, " ] time(s).")
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")

            Language.setMessage(mstrModuleName5, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
            Language.setMessage(mstrModuleName5, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Language.setMessage(mstrModuleName5, 4, "Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName5, 5, "Expense is mandatory information. Please select Expense to continue.")
            Language.setMessage(mstrModuleName5, 6, "Quantity is mandatory information. Please enter Quantity to continue.")
            Language.setMessage(mstrModuleName5, 8, "Please add atleast one expense in order to save.")
            Language.setMessage(mstrModuleName5, 10, "Sorry, you cannot add same expense again in the below list.")
            Language.setMessage(mstrModuleName5, 11, "Sorry, you cannot set quantity greater than balance set.")
            Language.setMessage(mstrModuleName5, 12, "Leave Form")
            Language.setMessage(mstrModuleName5, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user.")
            Language.setMessage(mstrModuleName5, 15, "Please Map this employee's Leave Approver to system user.")
            Language.setMessage(mstrModuleName5, 16, "Please Map this Leave type to this employee's Leave Approver(s).")
            Language.setMessage(mstrModuleName5, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user.")
            Language.setMessage(mstrModuleName5, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
            Language.setMessage(mstrModuleName5, 19, "Sorry, you cannot set amount greater than balance set.")
            Language.setMessage(mstrModuleName5, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Language.setMessage(mstrModuleName5, 22, "has mandatory document attachment. please attach document.")
            Language.setMessage(mstrModuleName5, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
            Language.setMessage(mstrModuleName5, 26, "Do you wish to continue?")
            Language.setMessage(mstrModuleName5, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Language.setMessage(mstrModuleName5, 28, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName5, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount.")
            Language.setMessage(mstrModuleName5, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
            Language.setMessage(mstrModuleName5, 33, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName5, 39, "Are you sure you want to delete this attachment?")
            Language.setMessage(mstrModuleName5, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
            Language.setMessage(mstrModuleName5, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
            Language.setMessage(mstrModuleName5, 45, "You have not set your expense remark.")
            Language.setMessage(mstrModuleName5, 46, "Currency is mandatory information. Please select Currency to continue.")
            Language.setMessage(mstrModuleName5, 47, "Claim Remark is mandatory information. Please enter claim remark to continue.")
            Language.setMessage(mstrModuleName5, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
            Language.setMessage(mstrModuleName5, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
            Language.setMessage(mstrModuleName5, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class