﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgTrainingRequisitionApprovers.aspx.vb" Inherits="Training_Requisition_wPgTrainingRequisitionApprovers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Requisition Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Approver(s) List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" />
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="drpLevel" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver" Width="100%" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpApprover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container" style="width: 100%; height: 350px; overflow: auto">
                                <asp:GridView ID="gvApproverList" DataKeyNames="mappingunkid" runat="server"
                                    AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <%--<asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit" OnClick="lnkedit_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>' ToolTip="Delete">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Active" OnClick="lnkActive_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="DeActive" OnClick="lnkDeActive_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Approver Name" DataField="approver" />
                                        <%--<asp:BoundField HeaderText="Department" DataField="departmentname" />
                                        <asp:BoundField HeaderText="Job" DataField="jobname" />--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                     <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        TargetControlID="Label3" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="Label4" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 600px;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="Label3" runat="server" Text="Add/ Edit Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label4" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="height: 290px; margin: auto; padding: auto">
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_level" runat="server" Text="Level"></asp:Label>
                                            </div>
                                            <div style="width: 50%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_level" runat="server" Width="450px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_user" runat="server" Text="User"></asp:Label>
                                            </div>
                                            <div style="width: 50%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_user" runat="server" Width="450px" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                       <%-- <div class="row">
                                            <div style="width: 80%" class="ib">
                                                <asp:TextBox ID="txtApproverUseraccess_search" runat="server"></asp:TextBox>
                                            </div>
                                            <div style="width: 10%" class="ib">
                                                <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btndefault" />
                                            </div>
                                        </div>--%>
                                        <div id="Div11" style="width: 100%; height: 200px; overflow: auto">
                                            <asp:GridView ID="TvApproverUseraccess" runat="server" ShowHeader="false" AutoGenerateColumns="False"
                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="UserAccess" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnApproverUseraccessSave" runat="server" Text="Save" CssClass="btndefault" />
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                    <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server"  Title="Are You Sure You Want Delete Approval ?" />
                    <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" Title="Confirmation" Message="Are You Sure You Want To Deactive  This Approver?" />
                    <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" Title="Confirmation" Message="Are You Sure You Want To Active This Approver?" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
