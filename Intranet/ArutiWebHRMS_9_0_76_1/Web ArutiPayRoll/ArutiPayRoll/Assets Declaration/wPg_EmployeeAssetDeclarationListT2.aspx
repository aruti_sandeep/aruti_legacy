﻿<%@ Page Title="Employee Declaration" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EmployeeAssetDeclarationListT2.aspx.vb" Inherits="HR_wPg_EmployeeAssetDeclarationListT2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">


        function closePopup() {
            $('#imgclose').click(function(evt) {
                $find('<%= popupAddEdit.ClientID %>').hide();
            });
        }

        function removeactiveclass() {
            $(".menubar ul li a").removeClass("active");
        }
        
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }



        function GetInstruct(str) {
            var dv = document.getElementById("divInstruction");
            if (dv != null && str != null)
                dv.innerHTML = str.toString();
        }
    </script>

    <center>
        <asp:Panel ID="Panel2" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Asset Declaration List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblFinalcialYear" runat="server" Text="Financial Year"></asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:DropDownList ID="drpFinalcialYear" runat="server" AutoPostBack="True" Width="150px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="False" Width="280px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:CheckBox ID="chkIncludeClosedYearTrans" runat="server" Text="Include Closed Year Transactions" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="pbl_gvList" runat="server" ScrollBars="Auto" Width="100%" Height="300px">
                                                    <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetdeclarationt2unkid, finyear_start, finyear_end,yearname, employeeunkid, Tin_No, isacknowledged, transactiondate, noofemployment, employeename, workstation, position, centerforwork, databasename">
                                                        <RowStyle BorderStyle="Solid" BorderWidth="1px" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="btnGvRemove" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnGvFinalSave" runat="server" CausesValidation="False" CommandName="FinalSave"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/Save.png" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Unlock Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnGvUnlockFinalSave" runat="server" CausesValidation="False"
                                                                        CommandName="UnlockFinalSave" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        ImageUrl="~/Images/unlock.png" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Preview">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnView3" runat="server" CausesValidation="False" CommandName="Print"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/PrintSummary_16.png" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="noofemployment" HeaderText="Employee Code" ReadOnly="True"
                                                                FooterText="colhNoOfEmployment">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                                FooterText="colhEmployee">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="position" HeaderText="Office / Position" ReadOnly="True"
                                                                FooterText="colhPosition">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="centerforwork" HeaderText="Department" ReadOnly="True"
                                                                FooterText="colhCenterForWork">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="yearname" HeaderText="Year" ReadOnly="True" FooterText="colhYearName">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <%--<asp:Button ID="btnScanAttachDocuments" runat="server" Text="Scan / Attach Document"
                                            CssClass="btnDefault" />--%>
                                         <asp:Button ID="btnScanAttachDocuments" runat="server" Text="Browse" Visible="false"
                                            CssClass="btnDefault" />
                                        <asp:Button ID="BtnNew" runat="server" Text="New" CssClass="btnDefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                        <asp:HiddenField ID="btnHidden1" runat="Server" />
                                        <asp:HiddenField ID="btnHidden2" runat="Server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupAddEdit" runat="server" TargetControlID="btnHidden"
                        PopupControlID="pnlAddEdit" BackgroundCssClass="ModalPopupBG" DropShadow="false"
                        CancelControlID="hdf_popupAddEdit" PopupDragHandleControlID="divBottom" Enabled="True">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="newpopup" Style="display: none;
                        width: 900px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-body">
                                <div id="Div2" class="panel-default" style="margin: 0px">
                                    <div id="Div5" class="panel-body-default">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="row2">
                                                    <div style="position: relative; width: 20%; height: 530px; vertical-align: top; padding: 5px;
                                                        margin: 0;" class="ib panel-heading ">
                                                        <asp:Panel ID="Panelmenu" runat="server" CssClass="menubar">
                                                            <div>
                                                                <ul>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_Instruction" runat="server" CssClass="active" position="0"
                                                                            Text="Instruction"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_BusinessDealings" runat="server" position="1" Text="Business Dealings"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_StaffOwnership" runat="server" position="2" Text="Staff Ownership"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_BankAccounts" runat="server" position="3" Text="Bank Accounts"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_Realproperties" runat="server" position="4" Text="Real properties"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_Liabilities" runat="server" position="5" Text="Liabilities"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_Relatives" runat="server" position="6" Text="Relatives in Employment"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_DependantBusiness" runat="server" position="7" Text="Dependant Business"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_DependantShares" runat="server" position="8" Text="Dependant Shares"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_DependantBank" runat="server" position="9" Text="Dependant Bank"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_DependantProperties" runat="server" position="10" Text="Dependant Real properties"></asp:LinkButton>
                                                                        <div style="border-bottom: 1px solid #DDD;">
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkmenu_SignOff" runat="server" position="11" Text="Sign Off"></asp:LinkButton>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <div style="width: 78%; margin: 0;" class="ib">
                                                        <asp:MultiView ID="mvwAssetDeclaration" runat="server" ActiveViewIndex="0">
                                                            <asp:View ID="vwStep1" runat="server">
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lblInstruction" runat="server" Text="Instructions"> </asp:Label>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div id="divInstruction" style="width: 100%; height: 220px; overflow: auto;">
                                                                    </div>
                                                                    <div id="Div4" class="panel-default" style="margin: 0px">
                                                                        <div id="Div6" class="panel-heading-default">
                                                                            <div style="float: left;">
                                                                                <asp:Label ID="gbEmployee" runat="server" Text="Employee Selection" />
                                                                            </div>
                                                                        </div>
                                                                        <div id="Div7" class="panel-body-default">
                                                                            <table style="width: 100%">
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_FinancialYear" runat="server" Text="Financial Year"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:DropDownList ID="cboStep1_FinancialYear" runat="server" AutoPostBack="True" Width="150px">
                                                                                        </asp:DropDownList>                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_Employee" runat="server" Text="Employee"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:DropDownList ID="cboStep1_Employee" runat="server" AutoPostBack="True" Width="250px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblStep1_EmployeeError" runat="server" Text="" ForeColor="Red" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_Date" runat="server" Text="Reporting Date"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <uc2:DateCtrl ID="dtpStep1_Date" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_EmployeeCode" runat="server" Text="Employee Code"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:Label ID="txtStep1_EmployeeCode" runat="server" Font-Bold="True" Text="1"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_OfficePosition" runat="server" Text="Office/Position"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:Label ID="txtStep1_OfficePosition" runat="server" Font-Bold="True" Text="General Manager"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                 <%--   Hemant (22 Nov 2018) -- Start
                                                                                        Enhancement : Changes As per Rutta Request for UAT --%>
                                                                                        <%--<asp:Label ID="lblCenterforWork" runat="server" Text="Department"></asp:Label>--%>
                                                                                        <asp:Label ID="lblStep1_CenterforWork" runat="server" Text="Functions"></asp:Label>
                                                                                        <%--Hemant (22 Nov 2018) -- End--%>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:Label ID="txtStep1_Department" runat="server" Font-Bold="True" Text="Finance and Administration"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <%--   Hemant (22 Nov 2018) -- Start
                                                                                        Enhancement : Changes As per Rutta Request for UAT --%>
                                                                                        <%--<asp:Label ID="lblCenterforWork" runat="server" Text="Department"></asp:Label>--%>
                                                                                        <tr style="width: 100%">
                                                                                            <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_Departments" runat="server" Text="Department"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 80%">
                                                                                        <asp:Label ID="txtStep1_Department2" runat="server" Font-Bold="True" Text="Finance and Administration"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <%--Hemant (22 Nov 2018) -- End--%>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_WorkStation" runat="server" Text="Work Station"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:Label ID="txtStep1_WorkStation" runat="server" Font-Bold="True" Text="Class1"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="lblStep1_Tin_No" runat="server" Text="Tin No"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 80%">
                                                                                        <asp:TextBox ID="txtStep1_Tin_No" runat="server" Font-Bold="True" Width="100px"></asp:TextBox>
                                                                                        <cc1:MaskedEditExtender ID="meStep1_Tin_No" runat="server" TargetControlID="txtStep1_Tin_No"
                                                                                            MaskType="Date" Mask="999/999/999" MessageValidatorTip="true" UserDateFormat="None"
                                                                                            UserTimeFormat="None" InputDirection="RightToLeft" ErrorTooltipEnabled="true">
                                                                                        </cc1:MaskedEditExtender>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep2" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblBusinessDealings_Step2" runat="server" Text="Business Dealings"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div8" class="panel-default">
                                                                            <div id="Div10" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%; margin-bottom: 10px;">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_Sector" runat="server" Text="Sector"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="cboStep2_Sector" runat="server" Width="150px" AutoPostBack="false">
                                                                                            </asp:DropDownList>
                                                                                            <%--<asp:Label ID="lblStep2_SectorError" runat="server" Text="" CssClass="ErrorControl" />--%>
                                                                                             <asp:RequiredFieldValidator ID="RfStep2_Sector" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="cboStep2_Sector" ErrorMessage="Please Select Sector"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_Company_Name" runat="server" Text="Company Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep2_Company_Name" runat="Server" Width="150px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="RfStep2_Company_NameError" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep2_Company_Name" ErrorMessage="Company Name can not be Blank."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%; margin-bottom: 10px;">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_company_tin_no" runat="server" Text="Company TIN No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <asp:TextBox ID="txtStep2_CompanyTinNo" runat="server" Width="150px"></asp:TextBox>
                                                                                            <cc1:MaskedEditExtender ID="meeStep2_CompanyTinNo" runat="server" TargetControlID="txtStep2_CompanyTinNo"
                                                                                            MaskType="Date" Mask="999/999/999" MessageValidatorTip="true" UserDateFormat="None"
                                                                                            UserTimeFormat="None" InputDirection="RightToLeft" ErrorTooltipEnabled="true">
                                                                                        </cc1:MaskedEditExtender>
                                                                                            <asp:RequiredFieldValidator ID="rfStep2_CompanyTinNo" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep2_CompanyTinNo" ErrorMessage="Company TIN No can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_registration_date" runat="server" Text="Registration Date"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <uc2:DateCtrl ID="dtpStep2_RegistrationDate" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%; margin-bottom: 10px;">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_address_location" runat="server" Text="Address"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <asp:TextBox ID="txtStep2_Addresslocation" runat="server" Width="153px"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep2_Addresslocation" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep2_Addresslocation" ErrorMessage="Address can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_business_type" runat="server" Text="Business Type"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep2_BusinessType" runat="server" Width="150px"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rFStep2_BusinessType" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep2_BusinessType" CssClass="ErrorControl" ErrorMessage="Bussiness Type can not be Blank"
                                                                                                ForeColor="White" Style="z-index: 1000" ValidationGroup="Business" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%; margin-bottom: 10px;">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_Position_held" runat="server" Text="Position Held"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep2_PositionHeld" runat="server" Style="margin-right: 5px;"
                                                                                                    Width="153px"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RequiredFieldValidator ID="rfStep2_PositionHeld" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep2_PositionHeld" ErrorMessage="Position Held can not be Blank"
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_Is_supplier_client" runat="server" Text="Supplier/Borrower"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:DropDownList ID="drpStep2_Is_supplier_client" runat="server" Width="150px" AutoPostBack="false">
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            <%--<asp:Label ID="lblStep2_Is_supplier_clientError" runat="server" Text="" CssClass="ErrorControl" />--%>
                                                                                             <asp:RequiredFieldValidator ID="rfStep2_Is_supplier_client" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                    ControlToValidate="drpStep2_Is_supplier_client" ErrorMessage="Position Held can not be Blank"
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_Monthly_annual_earnings" runat="server" Text="Monthly Annual Earnings"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep2_MonthlyAnnualEarnings" runat="server" Width="90px" CssClass="RightTextAlign"
                                                                                                    AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfStep2_MonthlyAnnualEarnings" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep2_MonthlyAnnualEarnings" ErrorMessage="Monthly Annual Earnings can not be Blank. "
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep2_CurrencyBusiness" runat="server" Width="50px" AutoPostBack="true">
                                                                                                </asp:DropDownList>
                                                                                                 <asp:RequiredFieldValidator ID="rfStep2_CurrencyBusiness" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                    ControlToValidate="drpStep2_CurrencyBusiness" ErrorMessage="Please select Currency."
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep2_Business_contact_no" runat="server" Text="Business Contact No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep2_BusinessContactNo" runat="server" Width="150px" CssClass="RightTextAlign"
                                                                                                onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep2_BusinessContactNo" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep2_BusinessContactNo" CssClass="ErrorControl" ErrorMessage="Business Contact No can not be Blank"
                                                                                                ForeColor="White" ValidationGroup="Business" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table>
                                                                                    <tr style="width: 100%; margin-bottom: 10px; text-align: left">
                                                                                        <td style="width: 18.5%;">
                                                                                            <asp:Label ID="colhStep2_Shareholders_names" runat="server" Text="Shareholders Names"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 81.5%">
                                                                                            <asp:TextBox ID="txtStep2_ShareholdersNames" runat="server" Rows="3"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep2_ShareholdersNames" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep2_ShareholdersNames" ErrorMessage="Shareholders Names can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Business"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="BtnAddBusiness" runat="server" Text="Add" CssClass="btnDefault" ValidationGroup="Business" />
                                                                                    <asp:Button ID="BtnUpdateBusiness" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        ValidationGroup="Business" Enabled="false" />
                                                                                    <asp:Button ID="btnResetBusiness" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="Business" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <asp:Panel ID="pnl_" runat="server" Width="100%" Height="218px" ScrollBars="Auto">
                                                                            <asp:GridView ID="gvBusiness" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetbusinessdealt2tranunkid, assetsectorunkid, is_supplier_client, countryunkid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Delete">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="assetsectorname" HeaderText="Sector" ReadOnly="True"
                                                                                        FooterText="colhStep2_Sector">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="company_org_name" HeaderText="Company Name" ReadOnly="True"
                                                                                        FooterText="colhStep2_Company_Name">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="company_tin_no" HeaderText="Company Tin No" ReadOnly="True"
                                                                                        FooterText="colhStep2_company_tin_no">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep2_CurrencyBusiness">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="registration_date" HeaderText="Registration Date" ReadOnly="True"
                                                                                        FooterText="colhStep2_registration_date">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="address_location" HeaderText="Address" ReadOnly="True"
                                                                                        FooterText="colhStep2_address_location">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="business_type" HeaderText="Business Type" ReadOnly="True"
                                                                                        FooterText="colhStep2_business_type">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="position_held" HeaderText="Position Held" ReadOnly="True"
                                                                                        FooterText="colhStep2_Position_held">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="is_supplier_clientname" HeaderText="Supplier/Borrower" ReadOnly="True"
                                                                                        FooterText="colhStep2_Is_supplier_client">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="monthly_annual_earnings" HeaderText="Monthly Annual Earnings"
                                                                                        ReadOnly="True" FooterText="colhStep2_Monthly_annual_earnings">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="business_contact_no" HeaderText="Business Contact No"
                                                                                        ReadOnly="True" FooterText="colhStep2_Business_contact_no">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="shareholders_names" HeaderText="Shareholders Names" ReadOnly="True"
                                                                                        FooterText="colhStep2_Shareholders_names">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep3" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblStaffOwnerShip_step3" runat="server" Text="Staff Ownership of Shares/Stocks or Bonds" />
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div9" class="panel-default">
                                                                            <div id="Div12" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%; margin-bottom: 10px;">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep3_Securitiestype" runat="server" Text="Securities"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep3_Securitiestype" runat="server" Width="150px" AutoPostBack="false">
                                                                                            </asp:DropDownList>
                                                                                              <asp:RequiredFieldValidator ID="rfStep3_Securitiestype" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep3_Securitiestype" ErrorMessage="Please Select Securities Type"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep3_Certificate_no" runat="server" Text="Certificate No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep3_Certificate_no" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep3_Certificate_no" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep3_Certificate_no" ErrorMessage="Certificate No can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%; margin-bottom: 10px;">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep3_No_of_shares_step3" runat="server" Text="No of Shares"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep3_No_of_shares" runat="Server" AutoPostBack = "true" CssClass="RightTextAlign"
                                                                                                Width="153px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep3_No_of_shares" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep3_No_of_shares" ErrorMessage="No of Shares can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep3_Company_business_name" runat="server" Text="Company Business Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep3_Company_business_name" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep3_Company_business_name" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep3_Company_business_name" ErrorMessage="Company Business Name can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep3_Marketvalue_step3" runat="server" Text="Current Market Value"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%;">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep3_Marketvalue" runat="server" Style="margin-right: 5px;"
                                                                                                    Width="90px" AutoPostBack="true" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfStep3_Marketvalue" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep3_Marketvalue" ErrorMessage="Current Market Value can not be Blank"
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep3_CurrencyShare" AutoPostBack="true" runat="server"
                                                                                                    Width="50px">
                                                                                                </asp:DropDownList>
                                                                                                  <asp:RequiredFieldValidator ID="rfStep3_CurrencyShare" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                    ControlToValidate="drpStep3_CurrencyShare" ErrorMessage="Please select Currency."
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="StaffOwnerShip"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                                
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep3_Acquisition_date" runat="server" Text="Acquisition Date"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%;">
                                                                                            <uc2:DateCtrl ID="dtpStep3_Acquisition_date" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddStaffOwnerShip" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="StaffOwnerShip" />
                                                                                    <asp:Button ID="btnUpdateStaffOwnerShip" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        ValidationGroup="StaffOwnerShip" Enabled="false" />
                                                                                    <asp:Button ID="btnResetStaffOwnerShip" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="StaffOwnerShip" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                            <asp:Panel ID="Panel3" runat="server" Width="100%" Height="295px" ScrollBars="Auto">
                                                                                <asp:GridView ID="gvStaffOwnerShip" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetsecuritiest2tranunkid,securitiestypeunkid,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="securitiestypename" HeaderText="Securities Type" ReadOnly="True"
                                                                                            FooterText="colhStep3_Securitiestype">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="certificate_no" HeaderText="Certificate No" ReadOnly="True"
                                                                                            FooterText="colhStep3_Certificate_no">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="no_of_shares" HeaderText="No of Shares" ReadOnly="True"
                                                                                            FooterText="colhStep3_No_of_shares_step3">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_business_name" HeaderText="Company Business name"
                                                                                            ReadOnly="True" FooterText="colhStep3_Company_business_name">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep3_CurrencyShare">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="market_value" HeaderText="Market Value" ReadOnly="True"
                                                                                            FooterText="colhStep3_Marketvalue_step3">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep3_Acquisition_date">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep4" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblBankAccounts_step4" runat="server" Text="Bank Account – Local and International"> </asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div11" class="panel-default">
                                                                            <div id="Div14" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep4_BankName" runat="server" Text="Bank Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <asp:TextBox ID="txtStep4_BankName" runat="server" Width="153px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep4_BankName" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep4_BankName" ErrorMessage="Bank Name can not be Blank"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep4_AccountTypeBank" runat="server" Text="Account Type"></asp:Label>
                                                                                        </td>
                                                                                        <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                        <%--<td style="width: 25%">--%>
                                                                                        <td style="width: 25%; display: none">
                                                                                            <%--Hemant (19 Nov 2018) -- End--%>
                                                                                            <asp:TextBox ID="txtStep4_AccountTypeBankAccounts" runat="server" Width="153px"></asp:TextBox>
                                                                                            <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                            <%--<br />                                                                                                                                                                                        
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtAccountTypeBankAccounts" ErrorMessage="Account Type can not be Blank"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                                                                                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:DropDownList ID="drpStep4_AccountTypeBankAccounts" runat="server" Width="150px"
                                                                                                AutoPostBack="false">
                                                                                            </asp:DropDownList>
                                                                                           <asp:RequiredFieldValidator ID="rfStep4_AccountTypeBankAccounts" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep4_AccountTypeBankAccounts" ErrorMessage="Please Select Account Type"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <%--Hemant (19 Nov 2018) -- End--%>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep4_AccountNo" runat="server" Text="Account No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep4_AccountNoBank" runat="server" Width="90px"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RequiredFieldValidator ID="rfStep4_AccountNoBank" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep4_AccountNoBank" ErrorMessage="Account No. can not be Blank "
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep4_CurrencyBank" AutoPostBack="true" runat="server" Width="50px">
                                                                                                </asp:DropDownList>
                                                                                               <asp:RequiredFieldValidator ID="rfStep4_CurrencyBank" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                    ControlToValidate="drpStep4_CurrencyBank" ErrorMessage="Please select Currency."
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep4_Specify" runat="server" Text="Specify"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep4_BankSpecify" runat="server" Width="153px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep4_DepositsSourceBank" runat="server" Text="Deposits Source"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep4_DepositsSourceBankAccounts" runat="server" Width="153px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep4_DepositsSourceBankAccounts" runat="server"
                                                                                                Display="Dynamic" ControlToValidate="txtStep4_DepositsSourceBankAccounts" ErrorMessage="Deposits Source can not be Blank "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="BankAccounts"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddBankAccounts" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="BankAccounts" />
                                                                                    <asp:Button ID="btnUpdateBankAccounts" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        ValidationGroup="BankAccounts" Enabled="false" />
                                                                                    <asp:Button ID="btnResetBankAccounts" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="BankAccounts" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <asp:Panel ID="Panel5" runat="server" Width="100%" Height="218px" ScrollBars="Auto">
                                                                            <asp:GridView ID="gvBankAccounts" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetbankt2tranunkid,accounttypeunkid,countryunkid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Delete">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="bank_name" HeaderText="Bank Name" ReadOnly="True" FooterText="colhStep4_BankName">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                    <%--<asp:BoundField DataField="account_type" HeaderText="Account Type" ReadOnly="True"
                                                                                        FooterText="colhAccountType_step4Bank">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>--%>
                                                                                    <asp:BoundField DataField="accounttype_name" HeaderText="Account Type" ReadOnly="True"
                                                                                        FooterText="colhStep4_AccountTypeBank">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                   <%-- Hemant (19 Nov 2018) -- End--%>
                                                                                    <asp:BoundField DataField="account_no" HeaderText="Account No" ReadOnly="True" FooterText="colhStep4_AccountNo">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep4_CurrencyBank">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="deposits_source" HeaderText="Deposits Source" ReadOnly="True"
                                                                                        FooterText="colhStep4_DepositsSourceBank">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="specify" HeaderText="Specify" ReadOnly="True" FooterText="colhStep4_Specify" NullDisplayText = " ">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep5" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblRealproperties_step5" runat="server" Text="Real properties"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div13" class="panel-default">
                                                                            <div id="Div16" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep5_AssetNameProperties" runat="server" Text="Asset Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep5_AssetName" runat="server" Width="153px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep5_AssetName" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep5_AssetName" ErrorMessage="Asset Name can not be Blank "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep5_LocationProperties" runat="server" Text="Location"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <asp:TextBox ID="txtStep5_Location" runat="server" Width="153px"></asp:TextBox>
                                                                                            <%--Hemant (13 Nov 2018) -- Start
                                                                                                    Enhancement : Changes for Asset Declaration template2
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtLocation" ErrorMessage="Location can not be Blank " CssClass="ErrorControl"
                                                                                                ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                                Hemant (13 Nov 2018) -- End--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep5_RegistrationTitleNoProperties" runat="server" Text="Registration Title No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep5_RegistrationTitleNoRealProperties" runat="server" Width="153px"></asp:TextBox>
                                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtRegistrationTitleNoRealProperties" ErrorMessage="Registration Title No. can not be Blank "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep5_EstimatedValueProperties" runat="server" Text="Estimated Value"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep5_EstimatedValueRealProperties" runat="server" AutoPostBack="true"
                                                                                                    Width="90px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RequiredFieldValidator ID="rfStep5_EstimatedValueRealProperties" runat="server"
                                                                                                    Display="Dynamic" ControlToValidate="txtStep5_EstimatedValueRealProperties" ErrorMessage="Estimated Value can not be Blank"
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep5_CurrencyProperties" AutoPostBack="true" runat="server"
                                                                                                    Width="50px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:RequiredFieldValidator ID="rfStep5_CurrencyProperties" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep5_CurrencyProperties" ErrorMessage="Please select Currency."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep5_FundsSourceProperties" runat="server" Text="Funds Source"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtStep5_FundsSourceRealProperties" runat="server" Width="153px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep5_FundsSourceRealProperties" runat="server"
                                                                                                Display="Dynamic" ControlToValidate="txtStep5_FundsSourceRealProperties" ErrorMessage="Funds Source can not be Blank "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%; display: none">
                                                                                            <asp:Label ID="colhStep5_AssetLocationProperties" runat="server" Text="Asset Location"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%; display: none">
                                                                                            <asp:TextBox ID="txtStep5_AssetLocationRealProperties" runat="server" Width="153px" ></asp:TextBox>
                                                                                            <asp:Label ID="lblStep5_RealPropertiesError" runat="server" Text="" CssClass="ErrorControl"  />
                                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtAssetLocationRealProperties" ErrorMessage="Asset Location can not be Blank "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep5_AcquisitionDateProperties" runat="server" Text="Acquisition Date"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%;">
                                                                                            <uc2:DateCtrl ID="dtpStep5_AcquisitionDateProperties" runat="server" />
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep5_AssetUseProperties" runat="server" Text="Asset Use"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep5_AssetUseRealProperties" runat="server" Style="float: left;
                                                                                                    margin-right: 5px;" Width="153px"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfStep5_AssetUseRealProperties" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep5_AssetUseRealProperties" ErrorMessage="Asset Use can not be Blank"
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="RealProperties"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddRealProperties" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="RealProperties" />
                                                                                    <asp:Button ID="btnUpdateRealProperties" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        ValidationGroup="RealProperties" Enabled="false" />
                                                                                    <asp:Button ID="btnResetRealProperties" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="RealProperties" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <asp:Panel ID="Panel6" runat="server" Width="100%" Height="230px" ScrollBars="Auto">
                                                                            <asp:GridView ID="gvRealProperties" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="countryunkid,assetpropertiest2tranunkid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Delete">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="asset_name" HeaderText="Asset Name" ReadOnly="True" FooterText="colhStep5_AssetNameProperties">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhStep5_LocationProperties" NullDisplayText = " ">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="registration_title_no" HeaderText="Registration Title No"
                                                                                        ReadOnly="True" FooterText="colhStep5_RegistrationTitleNoProperties" NullDisplayText = " ">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="estimated_value" HeaderText="Estimated Value" ReadOnly="True"
                                                                                        FooterText="colhStep5_EstimatedValueProperties">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep5_CurrencyProperties">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="funds_source" HeaderText="Funds Source" ReadOnly="True"
                                                                                        FooterText="colhStep5_FundsSourceProperties" HtmlEncode="false">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="asset_location" HeaderText="Asset Location" ReadOnly="True"
                                                                                        FooterText="colhAssetLocation_step5Properties" Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="asset_use" HeaderText="Asset Use" ReadOnly="True" FooterText="colhStep5_AssetUseProperties">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                        FooterText="colhStep5_AcquisitionDateProperties">
                                                                                        <HeaderStyle HorizontalAlign="Right" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep6" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblStep6_Liabilities" runat="server" Text="Liabilities"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div15" class="panel-default">
                                                                            <div id="Div18" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_InstitutionNameLiabilities" runat="server" Text="Institution Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep6_InstitutionName" runat="server" Width="175px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep6_InstitutionName" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep6_InstitutionName" ErrorMessage="Institution Name can not be Blank"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_LiabilityTypeLiabilities" runat="server" Text="Liability Type"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep6_LiabilityTypeLiabilities" runat="server" Width="178px"
                                                                                                AutoPostBack="false">
                                                                                            </asp:DropDownList>
                                                                                             <asp:RequiredFieldValidator ID="rfStep6_LiabilityTypeLiabilities" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep6_LiabilityTypeLiabilities" ErrorMessage="Please select Liability Type."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_OriginalBalanceLiabilities" runat="server" Text="Original Balance"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep6_OriginalBalanceLiabilities" runat="server" AutoPostBack="true"
                                                                                                    Style="float: left; margin-right: 5px;" Width="110px" CssClass="RightTextAlign"
                                                                                                    onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RequiredFieldValidator ID="rfStep6_OriginalBalanceLiabilities" runat="server"
                                                                                                    Display="Dynamic" ControlToValidate="txtStep6_OriginalBalanceLiabilities" ErrorMessage="Original Balance can not be Blank "
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep6_OrigCurrencyLiabilities" AutoPostBack="true" runat="server"
                                                                                                    Width="50px" />
                                                                                                <asp:RequiredFieldValidator ID="rfStep6_OrigCurrencyLiabilities" runat="server" InitialValue="0"
                                                                                                    Display="Dynamic" ControlToValidate="drpStep6_OrigCurrencyLiabilities" ErrorMessage="Please select Original Currency."
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_OutstandingBalanceLiabilities" runat="server" Text="Outstanding Balance"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep6_OutstandingBalanceLiabilities" runat="server" AutoPostBack="true"
                                                                                                    Style="float: left; margin-right: 5px;" Width="110px" CssClass="RightTextAlign"
                                                                                                    onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <br />
                                                                                                <asp:RequiredFieldValidator ID="rfStep6_OutstandingBalanceLiabilities" runat="server"
                                                                                                    Display="Dynamic" ControlToValidate="txtStep6_OutstandingBalanceLiabilities"
                                                                                                    ErrorMessage="Outstanding Balance can not be Blank. " CssClass="ErrorControl"
                                                                                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep6_OutCurrencyLiabilities" AutoPostBack="true" runat="server"
                                                                                                    Width="50px" />
                                                                                                  <asp:RequiredFieldValidator ID="rfStep6_OutCurrencyLiabilities" runat="server" InitialValue="0"
                                                                                                    Display="Dynamic" ControlToValidate="drpStep6_OutCurrencyLiabilities"
                                                                                                    ErrorMessage="Please select OutStanding Currency." CssClass="ErrorControl"
                                                                                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_DurationLiabilities" runat="server" Text="Duration(In Years)"></asp:Label></label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep6_Duration" runat="server" Width="175px" CssClass="RightTextAlign"
                                                                                                AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep6_Duration" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep6_Duration" ErrorMessage="Duration can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_LoanPurposeLiabilities" runat="server" Text="Loan Purpose"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep6_LoanPurposeLiabilities" runat="server" Width="175px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep6_LoanPurposeLiabilities" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep6_LoanPurposeLiabilities" ErrorMessage="Loan Purpose can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_Liability_dateLiabilities" runat="server" Text="Date of Liability"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <uc2:DateCtrl ID="dtStep6_Liability_dateLiabilities" runat="server" AutoPostBack="false" />
                                                                                           
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_Payoff_dateLiabilities" runat="server" Text="Payoff Date"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <uc2:DateCtrl ID="dtStep6_Payoff_dateLiabilities" runat="server" AutoPostBack="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep6_Remark_step6Liabilities" runat="server" Text="Ramark"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:TextBox ID="txtStep6_RemarkLiabilities" runat="server" Width="175px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep6_RemarkLiabilities" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep6_RemarkLiabilities" ErrorMessage="Ramark can not be Blank"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Liabilities"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddLiabilities" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="Liabilities" />
                                                                                    <asp:Button ID="btnUpdateLiabilities" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        ValidationGroup="Liabilities" Enabled="false" />
                                                                                    <asp:Button ID="btnResetLiabilities" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="Liabilities" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <asp:Panel ID="Panel7" runat="server" Width="100%" Height="300px" ScrollBars="Auto">
                                                                            <asp:GridView ID="gvLiabilities" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetliabilitiest2tranunkid, liabilitytypeunkid, liabilitydate, payoffdate, origcountryunkid, outcountryunkid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Delete">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="institution_name" HeaderText="Institution Name" ReadOnly="True"
                                                                                        FooterText="colhStep6_InstitutionNameLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="liability_typename" HeaderText="Liability Type" ReadOnly="True"
                                                                                        FooterText="colhStep6_LiabilityTypeLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="original_balance" HeaderText="Original Balance" ReadOnly="True"
                                                                                        FooterText="colhStep6_OriginalBalanceLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="origcurrency_sign" HeaderText="Currency" ReadOnly="True"
                                                                                        FooterText="colhStep6_origCurrencyLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="outstanding_balance" HeaderText="Outstanding Balance"
                                                                                        ReadOnly="True" FooterText="colhStep6_OutstandingBalanceLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="outcurrency_sign" HeaderText="Currency" ReadOnly="True"
                                                                                        FooterText="colhStep6_outCurrencyLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="duration" HeaderText="Duration" ReadOnly="True" FooterText="colhStep6_DurationLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="loan_purpose" HeaderText="Loan Purpose" ReadOnly="True"
                                                                                        FooterText="colhStep6_LoanPurposeLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="liability_type" HeaderText="Remark" ReadOnly="True" FooterText="colhStep6_Remark_step6Liabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="liabilitydate" HeaderText="Liability Date" ReadOnly="True"
                                                                                        FooterText="colhStep6_Liability_dateLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="payoffdate" HeaderText="Payoff Date" ReadOnly="True" FooterText="colhStep6_Payoff_dateLiabilities">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep7" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblRelativesInEmployment_Step7" runat="server" Text="Relatives in employment"> </asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div17" class="panel-default">
                                                                            <div id="Div20" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep7_RelativeEmployeeRelatives" runat="server" Text="Relatives in employment"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 35%">
                                                                                            <asp:DropDownList ID="drpStep7_RelativeEmployee" runat="server" AutoPostBack="False"
                                                                                                Width="200px">
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            <asp:RequiredFieldValidator ID="rfStep7_RelativeEmployee" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep7_RelativeEmployee" ErrorMessage="Please select Relative Employee."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Relatives"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep7_relationshipRelatives" runat="server" Text="Relationship"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:DropDownList ID="drpStep7_Relationship" runat="server" AutoPostBack="False"
                                                                                                Width="120px">
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                           <asp:RequiredFieldValidator ID="rfStep7_Relationship" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep7_Relationship" ErrorMessage="Please select Relationship."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Relatives"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <td style="width: 45%">
                                                                                                    <asp:CheckBox ID="chkOthers" runat="server" Text="Relative not in full time employment"
                                                                                                        OnCheckedChanged="chkOthers_CheckedChanged" AutoPostBack="true" />
                                                                                                </td>
                                                                                                <td style="width: 45%">
                                                                                                    <asp:TextBox ID="txtRelativeEmployee" runat="server" Width="280px"></asp:TextBox>
                                                                                                     <asp:RequiredFieldValidator ID="rftxtRelativeEmployee" runat="server" Display="Dynamic" 
                                                                                                ControlToValidate="txtRelativeEmployee" ErrorMessage="Please Add Employee Name."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Relatives"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="chkOthers" EventName="CheckedChanged" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddRelatives" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="Relatives" />
                                                                                    <asp:Button ID="btnUpdateRelatives" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        ValidationGroup="Relatives" Enabled="false" />
                                                                                    <asp:Button ID="btnResetRelatives" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="Relatives" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <asp:Panel ID="Panel8" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                                                            <asp:GridView ID="gvRelatives" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetrelativest2tranunkid, relationshipunkid,relative_employeeunkid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Delete">
                                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="relative_employeename" HeaderText="Relative Employeename"
                                                                                        ReadOnly="True" FooterText="colhRelativeEmployee_step7Relatives">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="relationshipname" HeaderText="Relationship" ReadOnly="True"
                                                                                        FooterText="colhrelationship_step7Relatives">
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep8" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblDependantBusinessTitle_Step8" runat="server" Text="Business or Commercial undertaking(s) or operation(s) owned by the Dependant (Spouse/Child under 18 years)"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div28" class="panel-default">
                                                                            <div id="Div29" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep8_RelationShip" runat="server" Text="RelationShip"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep8_RelationShip" runat="server" AutoPostBack="true" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_RelationShip" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep8_RelationShip" ErrorMessage="Please Select RelationShip."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep8_DependantName" runat="server" Text="Dependant Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep8_DependantName" runat="server" Width="175px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_DependantName" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep8_DependantName" ErrorMessage="Please Select Dependant Name."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>                                                                                        
                                                                                    </tr>
                                                                                    <%--Hemant (03 Dec 2018) -- End--%>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep8_Sector" runat="server" Text="Sector"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:DropDownList ID="drpStep8_Sector" runat="server" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Sector" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep8_Sector" ErrorMessage="Please Select Sector"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_CompanyName" runat="server" Text="Name of Company / Organisation"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep8_Companyname" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Companyname" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_Companyname" ErrorMessage="Company Name can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_CompanyTinNo" runat="server" Text="Company TIN No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep8_Companytinno" runat="Server"></asp:TextBox>
                                                                                            <cc1:MaskedEditExtender ID="meStep8_CompnayTinno" runat="server" TargetControlID="txtStep8_Companytinno"
                                                                                                MaskType="Date" Mask="999/999/999" MessageValidatorTip="true" UserDateFormat="None"
                                                                                            UserTimeFormat="None" InputDirection="RightToLeft" ErrorTooltipEnabled="true">
                                                                                        </cc1:MaskedEditExtender>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Companytinno" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_Companytinno" ErrorMessage="Company TIN No can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_RegDate" runat="server" Text="Date of Registration"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <uc2:DateCtrl ID="dtStep8_DateofRegistration" runat="server" AutoPostBack="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_BusinessType" runat="server" Text="Type / Nature of Business"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep8_BussinessType" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_BussinessType" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_BussinessType" ErrorMessage="Business Type can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_position_held" runat="server" Text="Position Held"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep8_Position" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Position" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_Position" ErrorMessage="Position Held can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_monthlyearning" runat="server" Text="Monthly Earning"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep8_MonthlyEarning" runat="server" AutoPostBack="true" Width="115px"
                                                                                                    CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfStep8_MonthlyEarning" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep8_MonthlyEarning" ErrorMessage="Monthly Earning can not be Blank. "
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep8_CurrencyMonthlyEarning" AutoPostBack="true" runat="server"
                                                                                                    Width="50px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:RequiredFieldValidator ID="rfStep8_CurrencyMonthlyEarning" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                    ControlToValidate="drpStep8_CurrencyMonthlyEarning" ErrorMessage="Please select Currency."
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_is_supplier_client" runat="server" Text="Client Type"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:DropDownList ID="drpStep8_SupplierClient" runat="server" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_SupplierClient" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                    ControlToValidate="drpStep8_SupplierClient" ErrorMessage="Please select Client Type."
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_contactno" runat="server" Text="Business Contact No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep8_Contactno" runat="Server" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Contactno" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_Contactno" ErrorMessage="Business Contact No can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_shareholders_names" runat="server" Text="Shareholders Names"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep8_Shareholder" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Shareholder" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_Shareholder" ErrorMessage="Shareholders Names can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep8_Address" runat="server" Text="Physical Address /Location"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 80%;">
                                                                                            <asp:TextBox ID="txtStep8_Address" runat="Server" TextMode="MultiLine"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep8_Address" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep8_Address" ErrorMessage="Address can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBusiness"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddDependantBusiness_step8" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="DeptBusiness" />
                                                                                    <asp:Button ID="btnUpdateDependantBusiness_step8" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        Enabled="false" ValidationGroup="DeptBusiness" />
                                                                                    <asp:Button ID="btnResetDependantBusiness_step8" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        ValidationGroup="DeptBusiness" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                            <asp:Panel ID="Panel12" runat="server" Width="100%" Height="255px" ScrollBars="Auto">
                                                                                <asp:GridView ID="gvDependantBusiness" runat="server" AutoGenerateColumns="False"
                                                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetbusinessdealt2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,assetsectorunkid,countryunkid,is_supplier_client">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
											                                            <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep8_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep8_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>
                                                                                        <asp:BoundField DataField="assetsectorname" HeaderText="Sector" FooterText="colhStep8_Sector">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_org_name" HeaderText="Company Name" FooterText="colhStep8_CompanyName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_tin_no" HeaderText="Company TIN No" ReadOnly="True"
                                                                                            FooterText="colhStep8_CompanyTinNo">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="registration_date" HeaderText="Registration Date" ReadOnly="True"
                                                                                            FooterText="colhStep8_RegDate">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="address_location" HeaderText="Address" ReadOnly="True"
                                                                                            FooterText="colhStep8_Address">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="business_type" HeaderText="Business Type" ReadOnly="True"
                                                                                            FooterText="colhStep8_BusinessType">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="position_held" HeaderText="Position Held" ReadOnly="True"
                                                                                            FooterText="colhStep8_position_held">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="is_supplier_clientname" HeaderText="Client Type" ReadOnly="True"
                                                                                            FooterText="colhStep8_is_supplier_client">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep8_CurrencyShare">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="monthly_annual_earnings" HeaderText="Monthly Earning"
                                                                                            ReadOnly="True" FooterText="colhStep8_monthlyearning">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="business_contact_no" HeaderText="Contact No" ReadOnly="True"
                                                                                            FooterText="colhStep8_contactno">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="shareholders_names" HeaderText="Shareholders Names" ReadOnly="True"
                                                                                            FooterText="colhStep8_shareholders_names">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep9" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblDependantShareTitle_Step9" runat="server" Text="Shares/Stocks or Bonds owned by the Dependant (Spouse/Child under 18 years)"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div25" class="panel-default">
                                                                            <div id="Div27" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep9_Relationship" runat="server" Text="Relationship"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:DropDownList ID="drpStep9_DependantRelationship" runat="server"  AutoPostBack="true" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                               <asp:RequiredFieldValidator ID="rfStep9_DependantRelationship" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep9_DependantRelationship" ErrorMessage="Please select Relationship."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep9_DependantName" runat="server" Text="Dependant Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep9_DependantName" runat="server" Width="180px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep9_DependantName" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep9_DependantName" ErrorMessage="Please Select Dependant Name."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td> 
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>  
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep9_Securitytype" runat="server" Text="Security Type"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:DropDownList ID="DrpStep9_DependantSecurityType" runat="server" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                           <asp:RequiredFieldValidator ID="rfStep9_DependantSecurityType" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="DrpStep9_DependantSecurityType" ErrorMessage="Please select Security Type."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep9_certificateno" runat="server" Text="Certificate No"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep9_DependantCertificateno" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep9_DependantCertificateno" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep9_DependantCertificateno" ErrorMessage="Certificate No can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep9_noofshares" runat="server" Text="No Of Shares"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep9_Dependantno_of_shares" runat="server" AutoPostBack="true" Width="153px" CssClass="RightTextAlign"
                                                                                                onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep9_Dependantno_of_shares" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep9_Dependantno_of_shares" ErrorMessage="No Of Shares can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep9_businessname" runat="server" Text="Business Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtDependantbusinessname_step9" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfDependantbusinessname_step9" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtDependantbusinessname_step9" ErrorMessage="Business Name can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep9_acquisition_date" runat="server" Text="Date of Acquisition"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <uc2:DateCtrl ID="dtStep9_Dependantacquisition_date" runat="server" AutoPostBack="false" />
                                                                                           
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep9_market_value" runat="server" Text="Market Value"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep9_Dependantmarketvalue" runat="server" AutoPostBack="true"
                                                                                                    Width="115px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfStep9_Dependantmarketvalue" runat="server" Display="Dynamic"
                                                                                                    ControlToValidate="txtStep9_Dependantmarketvalue" ErrorMessage="Market Value can not be Blank. "
                                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare"
                                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep9_Dependantmarketvalue" AutoPostBack="true" runat="server"
                                                                                                    Width="50px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:RequiredFieldValidator ID="rfStep9_DependantmarketvalueCurrency" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="drpStep9_Dependantmarketvalue" ErrorMessage="Please select Currency."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptShare" InitialValue="0"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddDependantShare_step9" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="DeptShare" />
                                                                                    <asp:Button ID="btnUpdateDependantShare_step9" runat="server" Text="Update" CssClass="btnDefault"
                                                                                        Enabled="false" ValidationGroup="DeptShare" />
                                                                                    <asp:Button ID="btnResetDependantShare_step9" runat="server" Text="Reset" CausesValidation="false"
                                                                                        CssClass="btnDefault" />
                                                                                </div>
                                                                            </div>
                                                                            <asp:Panel ID="Panel4" runat="server" Width="100%" Height="265px" ScrollBars="Auto">
                                                                                <asp:GridView ID="gvDependantShare" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetsecuritiest2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,securitiestypeunkid,acquisition_date,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep9_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep9_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                        <asp:BoundField DataField="securitiestypename" HeaderText="Security Type" ReadOnly="True"
                                                                                            FooterText="colhStep9_Securitytype">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="certificate_no" HeaderText="Certificate Number" ReadOnly="True"
                                                                                            FooterText="colhStep9_certificateno">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="no_of_shares" HeaderText="No Of Shares" ReadOnly="True"
                                                                                            FooterText="colhStep9_noofshares">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="company_business_name" HeaderText="Business Name" ReadOnly="True"
                                                                                            FooterText="colhStep9_businessname">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep9_Currency">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="market_value" HeaderText="Market Value" ReadOnly="True"
                                                                                            FooterText="colhStep9_market_value">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep9_acquisition_date">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep10" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblDependantBankTitle_Step10" runat="server" Text="Dependant Bank"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div30" class="panel-default">
                                                                            <div id="Div31" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep10_RelationShip" runat="server" Text="RelationShip"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep10_RelationShip" runat="server"  AutoPostBack="true" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep10_RelationShip" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep10_RelationShip" ErrorMessage="Please Select RelationShip."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep10_DependantName" runat="server" Text="Dependant Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep10_DependantName" runat="server" Width="175px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep10_DependantName" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep10_DependantName" ErrorMessage="Please Select Dependant Name"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>                                                                                        
                                                                                    </tr>
                                                                                    <%--Hemant (03 Dec 2018) -- End--%>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep10_bankname" runat="server" Text="Bank Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep10_DependantBankname" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep10_DependantBankname" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep10_DependantBankname" ErrorMessage="Bank Name can not be Blank."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep10_accounttype" runat="server" Text="Account Type"></asp:Label>
                                                                                        </td>
                                                                                        <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                            <%--<td style="width: 30%;">--%>
                                                                                        <td style="width: 30%; display: none;">
                                                                                            <asp:TextBox ID="txtStep10_DependantAccounttype" runat="Server"></asp:TextBox>
                                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtDependantAccounttype_step10" CssClass="ErrorControl" ErrorMessage="Bank Type can not be Blank."
                                                                                                ForeColor="White" ValidationGroup="DeptBank" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                                                                <%--  Hemant (19 Nov 2018) -- End--%>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:DropDownList ID="drpStep10_DependantAccounttype" runat="server" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep10_DependantAccounttype" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep10_DependantAccounttype" ErrorMessage="Please Select Account Type"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptBank"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep10_accountno" runat="server" Text="Account Number"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep10_DependantAccountno" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep10_DependantAccountno" runat="server" Display="Dynamic"
                                                                                                ControlToValidate="txtStep10_DependantAccountno" CssClass="ErrorControl" ErrorMessage="Bank Account Number can not be Blank."
                                                                                                ForeColor="White" ValidationGroup="DeptBank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep10_Specify" runat="server" Text="Specify"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 25%">
                                                                                            <asp:TextBox ID="txtcolhStep10_Specify" runat="server" Width="153px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep10_countryunkid" runat="server" Text="Currency Held"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:DropDownList ID="drpStep10_DependantBankCurrency" AutoPostBack="true" runat="server"
                                                                                                Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep10_DependantBankCurrency" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep10_DependantBankCurrency" CssClass="ErrorControl" ErrorMessage="Please select Currency."
                                                                                                ForeColor="White" ValidationGroup="DeptBank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddDependantBankAccount_step10" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="DeptBank" />
                                                                                    <asp:Button ID="btnUpdateDependantBankAccount_step10" runat="server" Text="Update"
                                                                                        CssClass="btnDefault" Enabled="false" ValidationGroup="DeptBank" />
                                                                                    <asp:Button ID="btnResetDependantBankAccount_step10" runat="server" Text="Reset"
                                                                                        CssClass="btnDefault" CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                            <asp:Panel ID="Panel13" runat="server" Width="100%" Height="330px" ScrollBars="Auto">
                                                                                <asp:GridView ID="gvDependantBank" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetbankt2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,accounttypeunkid,countryunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
											                                            <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep10_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep10_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>
                                                                                        <asp:BoundField DataField="bank_name" HeaderText="Name of Bank" FooterText="colhStep10_bankname">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (19 Nov 2018) -- Start
                                                                                            Enhancement : Changes for NMB Requirement--%>
                                                                                        <%--<asp:BoundField DataField="account_type" HeaderText="Account Type" ReadOnly="True"
                                                                                            FooterText="colhaccounttype_step10">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>--%>
                                                                                        <asp:BoundField DataField="accounttype_name" HeaderText="Account Type" ReadOnly="True"
                                                                                            FooterText="colhStep10_accounttype">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (19 Nov 2018) -- End--%>
                                                                                        <asp:BoundField DataField="account_no" HeaderText="Account Number" ReadOnly="True"
                                                                                            FooterText="colhStep10_accountno">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep10_countryunkid">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="amount" Visible="false" HeaderText="amount" ReadOnly="True"
                                                                                            FooterText="colhStep10_amount">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="specify" HeaderText="Specify" ReadOnly="True" FooterText="colhStep10_Specify" NullDisplayText = " ">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep11" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblDependantPropertiesTitle_Step11" runat="server" Text="Dependant Real properties (such as Residential House, Commercial Property, lands, vehicles, etc.)"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div32" class="panel-default">
                                                                            <div id="Div33" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <%--Hemant (03 Dec 2018) -- Start--%>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep11_RelationShip" runat="server" Text="RelationShip"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep11_RelationShip" runat="server"  AutoPostBack="true" Width="150px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep11_RelationShip" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep11_RelationShip" ErrorMessage="Please Select RelationShip."
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep11_DependantName" runat="server" Text="Dependant Name"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%">
                                                                                            <asp:DropDownList ID="drpStep11_DependantName" runat="server" Width="175px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfStep11_DependantName" runat="server" Display="Dynamic" InitialValue="0"
                                                                                                ControlToValidate="drpStep11_DependantName" ErrorMessage="Please Select Dependant Name"
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>                                                                                        
                                                                                    </tr>
                                                                                    <%--Hemant (03 Dec 2018) -- End--%>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep11_asset_name" runat="server" Text="Name of Asset"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtStep11_DependantPropertiesAsset" runat="Server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfStep11_DependantPropertiesAsset" runat="server"
                                                                                                Display="Dynamic" ControlToValidate="txtStep11_DependantPropertiesAsset" ErrorMessage="Asset can not be Blank. "
                                                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="DeptProperties"
                                                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep11_location" runat="server" Text="Location"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtDependantPropertiesLocation_step11" runat="Server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 20%">
                                                                                            <asp:Label ID="colhStep11_registrationtitleno" runat="server" Text="Registration/Title Number"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <asp:TextBox ID="txtDependantPropertiesRegistrationNo_step11" runat="Server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep11_estimated_value" runat="server" Text="Estimated Value"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <div style="float: left">
                                                                                                <asp:TextBox ID="txtStep11_DependantPropertiesEstimatevalue" runat="server" AutoPostBack="true"
                                                                                                    Width="85px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfStep11_DependantPropertiesEstimatevalue" runat="server"
                                                                                                    Display="Dynamic" ControlToValidate="txtStep11_DependantPropertiesEstimatevalue"
                                                                                                    ErrorMessage="Estimated Value can not be Blank. " CssClass="ErrorControl" ForeColor="White"
                                                                                                    Style="z-index: 1000" ValidationGroup="DeptProperties" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                            <div style="float: left">
                                                                                                <asp:DropDownList ID="drpStep11_DependantPropertiesEstimatevalue" AutoPostBack="true"
                                                                                                    runat="server" Width="50px">
                                                                                                </asp:DropDownList>
                                                                                                <asp:RequiredFieldValidator ID="rfStep11_DependantPropertiesEstimatevalueCurrency" runat="server" InitialValue="0"
                                                                                                    Display="Dynamic" ControlToValidate="drpStep11_DependantPropertiesEstimatevalue"
                                                                                                    ErrorMessage="Please select Currency." CssClass="ErrorControl" ForeColor="White"
                                                                                                    Style="z-index: 1000" ValidationGroup="DeptProperties" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--Hemant (23 Nov 2018) -- Start
                                                                                    Enhancement : Changes As per Rutta Request for UAT --%>
                                                                                     <tr style="width: 100%">
                                                                                    <td style="width: 20%;">
                                                                                            <asp:Label ID="colhStep11_AcquisitionDateDepnProperties" runat="server" Text="Acquisition Date"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                            <uc2:DateCtrl ID="dtpStep11_AcquisitionDateDepnProperties" runat="server" />
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                        </td>
                                                                                        <td style="width: 30%;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--Hemant (23 Nov 2018) -- End  --%>
                                                                                </table>
                                                                                <div class="btn-default">
                                                                                    <asp:Button ID="btnAddDependantProperties_step11" runat="server" Text="Add" CssClass="btnDefault"
                                                                                        ValidationGroup="DeptProperties" />
                                                                                    <asp:Button ID="btnUpdateDependantProperties_step11" runat="server" Text="Update"
                                                                                        CssClass="btnDefault" Enabled="false" ValidationGroup="DeptProperties" />
                                                                                    <asp:Button ID="btnResetDependantProperties_step11" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                        CausesValidation="false" />
                                                                                </div>
                                                                            </div>
                                                                            <asp:Panel ID="Panel14" runat="server" Width="100%" Height="330px" ScrollBars="Auto">
                                                                                <asp:GridView ID="gvDependantProperties" runat="server" AutoGenerateColumns="False"
                                                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetpropertiest2depntranunkid,assetdeclarationt2unkid,relationshipunkid,dependantunkid,countryunkid,acquisition_date">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Edit">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--Hemant (03 Dec 2018) -- Start--%>
											                                            <asp:BoundField DataField="relationshipname" HeaderText="Relationship" FooterText="colhStep11_Relationship">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="dependantname" HeaderText="Dependant Name" FooterText="colhStep11_DependantName">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--Hemant (03 Dec 2018) -- End--%>
                                                                                        <asp:BoundField DataField="asset_name" HeaderText="Asset Name" FooterText="colhStep11_asset_name">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhStep11_location" NullDisplayText = " ">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="registration_title_no" HeaderText="Registration No" ReadOnly="True"
                                                                                            FooterText="colhStep11_registrationtitleno" NullDisplayText = " ">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhStep11_Currency">
                                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="estimated_value" HeaderText="Monthly Earning" ReadOnly="True"
                                                                                            FooterText="colhStep11_estimated_value">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                       <%-- Hemant (23 Nov 2018) -- Start
                                                                                         Enhancement : Changes As per Rutta Request for UAT --%>
                                                                                         <asp:BoundField DataField="acquisition_date" HeaderText="Acquisition Date" ReadOnly="True"
                                                                                            FooterText="colhStep11_AcquisitionDateDepnProperties">
                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                        </asp:BoundField>
                                                                                        <%--'Hemant (23 Nov 2018) -- End--%>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                            <asp:View ID="vwStep12" runat="server">
                                                                <div class="panel-primary" style="margin-bottom: 0px">
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblSighOffTitle_Step12" runat="server" Text="Sign Off"></asp:Label>
                                                                    </div>
                                                                    <div class="panel-body" style="height: 500px; overflow: auto">
                                                                        <div id="Div19" class="panel-default">
                                                                            <div id="Div21" class="panel-body-default">
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 100%; text-align: center;">
                                                                                            <asp:Label ID="txtHeader_Step12" runat="server" Font-Bold="true" Font-Underline="true"
                                                                                                Font-Size="Medium" Text="Declaration"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 100%">
                                                                                            <asp:Label ID="txtPara1_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 100%">
                                                                                            <asp:Label ID="txtPara2_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 100%">
                                                                                            <asp:Label ID="txtPara3_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 100%">
                                                                                            <asp:Label ID="txtPara4_Step12" runat="server" TextMode="MultiLine"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br />
                                                                                <br />
                                                                                <table style="width: 100%" cellpadding="3">
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 60%">
                                                                                            <asp:Label ID="txtSignatureLine_Step12" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 40%">
                                                                                            <asp:Label ID="txtDate_Step12" runat="server"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="width: 100%">
                                                                                        <td style="width: 60%">
                                                                                            <asp:CheckBox ID="chkAcknowledgement_Step13" runat="server" Text="Acknowledgement" />
                                                                                        </td>
                                                                                        <td style="width: 40%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <div class="btn-default">
                                            <div style="float: left">
                                                <b>
                                                    <asp:Label ID="lblExchangeRate" runat="server" Text="" Style="text-align: center;"></asp:Label>
                                                </b>
                                            </div>
                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnDefault" />
                                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnDefault" ValidationGroup="Step" />
                                            <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="btnDefault"
                                                ValidationGroup="Step" />
                                            <asp:Button ID="btnFinalSave" runat="server" Text="Final Save" CssClass="btnDefault" ValidationGroup="Step" />
                                            <asp:Button ID="btnCloseAddEdit" runat="server" Text="Close" CssClass="btnDefault"
                                                CausesValidation="false" />
                                            <asp:HiddenField ID="hdf_popupAddEdit" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc8:ConfirmYesNo ID="popupYesNo" runat="server" Title="Are you sure you want to Save this Asset Declaration?"
                        Message="Are you sure you want to Save this Asset Declaration?" />
                    <uc8:ConfirmYesNo ID="popupFinalYesNo" runat="server" Title="Are you sure you want to Final Save this Asset Declaration?"
                        Message="After Final Save You can not Edit / Delete Asset Declaration. Are you sure you want to Final Save this Asset Declaration?" />
                    <uc9:DeleteReason ID="popup1" runat="server" Title="Are ou Sure You Want To delete?:" />
                    <uc8:ConfirmYesNo ID="popupUnlockFinalYesNo" runat="server" Title="Unlock Final Save"
                        Message="Are you sure you want to Unlock Final Save this Asset Declaration?" />
                    <uc9:DeleteReason ID="popupDeleteBusinessDeal" runat="server" Title="Are you sure you want to delete selected business deal?" ValidationGroup="DeleteBusinessDeal" />
                    <uc9:DeleteReason ID="popupDeleteStaffOwnership" runat="server" Title="Are you sure you want to delete selected staff Ownership?" ValidationGroup="DeleteStaffownership" />
                    <uc9:DeleteReason ID="popupDeleteBankAccount" runat="server" Title="Are you sure you want to delete selected bank account?" ValidationGroup="DeleteBankAccount" />
                    <uc9:DeleteReason ID="popupDeleteProperties" runat="server" Title="Are you sure you want to delete selected properties?" ValidationGroup="DeleteProperties" />                    
                    <uc9:DeleteReason ID="popupDeleteLiabilities" runat="server" Title="Are you sure you want to delete selected liabilities?" ValidationGroup="DeleteLiabilities" />
                     <uc9:DeleteReason ID="popupDeleteRelatives" runat="server" Title="Are you sure you want to delete selected relatives?" ValidationGroup="DeleteRelatives" />
                    <uc9:DeleteReason ID="popupDeleteDependantBusinessDeal" runat="server" Title="Are you sure you want to delete selected Dependant Business Deal?" ValidationGroup="DeleteDependantBusinessDeal" />
                    <uc9:DeleteReason ID="popupDeleteDependantShare" runat="server" Title="Are you sure you want to delete selected dependant share?" ValidationGroup="DeleteDependantShare" />
                    <uc9:DeleteReason ID="popupDeleteDependantBankAccount" runat="server" Title="Are you sure you want to delete selected Dependant Bank Account?" ValidationGroup="DeleteDependantBankAccount" />
                    <uc9:DeleteReason ID="popupDeleteDependantProperties" runat="server" Title="Are you sure you want to delete selected dependant properties?" ValidationGroup="DeleteDependantProperties" />
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
