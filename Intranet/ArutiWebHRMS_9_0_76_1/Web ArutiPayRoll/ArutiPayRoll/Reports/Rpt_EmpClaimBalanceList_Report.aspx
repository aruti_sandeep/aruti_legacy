﻿<%@ Page Language="VB" MasterPageFile="~/home.master"  Title="Employee Claim Expense Balance Listing Report" AutoEventWireup="false" CodeFile="Rpt_EmpClaimBalanceList_Report.aspx.vb" Inherits="Reports_Rpt_EmpClaimBalanceList_Report" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
   <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>
    

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>
    
      <script type="text/javascript">

      function onlyNumbers(txtBox, e) {
          if (window.event)
              var charCode = window.event.keyCode;       // IE
          else
              var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

          if (charCode == 13)
              return false;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

        return true;
    }
    </script>
    
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                         <div class="panel-primary">
                                <div class="panel-heading">
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Claim Expense Balance Listing Report"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <div id="FilterCriteria" class="panel-default">
                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                            </div>
                                            <div style="text-align: right">
                                                <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                            </div>
                                        </div>
                                     <div id="FilterCriteriaBody" class="panel-body-default">
                                            <table width="100%">
                                                 <tr style="width: 100%">
                                                    <td style="width: 25%">
                                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                                    </td>
                                                    <td style="width: 15%; text-align: left">
                                                        <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                                    </td>
                                               </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="LblExpenseCategory" runat="server" Text="Expense Category"></asp:Label>
                                                    </td>
                                                    <td  colspan="3">
                                                        <asp:DropDownList ID = "cboExpenseCategory" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                    </td>
                                               </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                    </td>
                                                    <td  colspan="3">
                                                        <asp:DropDownList ID = "cboEmployee" runat="server" AutoPostBack="false"></asp:DropDownList>
                                                    </td>
                                               </tr>
                                               <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                                    </td>
                                                    <td  colspan="3">
                                                        <asp:DropDownList ID = "cboExpense" runat="server" AutoPostBack="false"></asp:DropDownList>
                                                    </td>
                                               </tr>
                                            </table>
                                            <table>
                                                    <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                    <asp:Label ID="LblPayableAmount" runat="server" Text="Payable Amount"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                     <asp:DropDownList ID = "cboFromCondition" runat="server" AutoPostBack="false"></asp:DropDownList>
                                                            </td>
                                                             <td style="width: 18%">
                                                                    <asp:TextBox ID="txtFromExpenseAmt" runat="server" Text="0" style="text-align:right" onKeypress="return onlyNumbers(this, event)"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%">
                                                                     <asp:DropDownList ID = "cboToCondition" runat="server" AutoPostBack="false"></asp:DropDownList>
                                                            </td>
                                                            <td style="width: 22%">
                                                                    <asp:TextBox ID="txtToExpenseAmt" runat="server" Text="0" style="text-align:right" onKeypress="return onlyNumbers(this, event)"></asp:TextBox>
                                                            </td>
                                                    </tr>
                                            </table>
                                             <div class="btn-default">
                                                <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                                <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                                <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                            </div>
                                      </div>  
                                </div>
                                 <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                          </div>      
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
  </center>    
</asp:Content>