﻿<%@ Page Title="Employee Duration Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Employee_Duration.aspx.vb" Inherits="Reports_Rpt_Employee_Duration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Duration Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position:relative">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblRType" runat="server" Text="Report Type"></asp:Label>
                                                </td>
                                                <td style="width: 100%">
                                                    <asp:DropDownList ID="drpReportType" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="False">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                    </table>
                                    <asp:Panel ID="pnlRpt1" runat="server">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="LblJob" runat="server" Text="Job"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="drpJob" runat="server" AutoPostBack="False">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblFromYear" runat="server" Text="From Year"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:TextBox ID="TxtFromYear" runat="server" Enabled="false" Style="text-align: right;
                                                        background-color: White"></asp:TextBox>
                                                    <cc1:NumericUpDownExtender ID="nudFromYear" runat="server" Width="70" Minimum="0" Maximum="100"
                                                        TargetControlID="txtFromYear">
                                                    </cc1:NumericUpDownExtender>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblToYear" runat="server" Text="To Year"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:TextBox ID="TxtToYear" runat="server" Enabled="false" Style="text-align: right;
                                                        background-color: White"></asp:TextBox>
                                                    <cc1:NumericUpDownExtender ID="nudToYear" runat="server" Width="70" Minimum="0" Maximum="100"
                                                        TargetControlID="TxtToYear">
                                                    </cc1:NumericUpDownExtender>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                        Checked="false" CssClass="chkbx" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:CheckBox ID="chkFromCurrDate" runat="server" Text="Generate from Current Title"
                                                        Checked="false" CssClass="chkbx" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <%--'Sohail (24 Dec 2018) -- Start--%>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblAsOnDate" runat="server" Text="As On Date"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <uc2:DateCtrl ID="dtpAsOnDate" runat="server" />
                                                </td>
                                            </tr>
                                            <%--'Sohail (24 Dec 2018) -- End--%>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlRpt2" runat="server">
                                        <table style="width: 100%; border-width: 0px;">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblAllocation" runat="server" Text="Allocation"></asp:Label>
                                                </td>
                                                <td style="width: 75%" colspan="5">
                                                    <asp:DropDownList ID="drpAllocation" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:CheckBox ID="chkCurrentAllocation" runat="server" Text="Generate Based On Current Allocation"
                                                        Checked="false" CssClass="chkbx" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:TextBox ID="txtSearch" runat="server" Text="" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:Panel ID="pnlAlloc" runat="server" ScrollBars="auto">
                                                        <div style="border: 1px solid #DDD; height: 200px; overflow: auto; margin-top: 10px;
                                                            margin-bottom: 10px">
                                                            <asp:CheckBoxList ID="chkAllocation" runat="server">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:CheckBox ID="chkYear" Text="Year" runat="server" />
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblYFrom" runat="server" Text="From"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAllocFYear" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%; text-align: center">
                                                                <asp:Label ID="lblYTo" runat="server" Text="To"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAllocTYear" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:CheckBox ID="chkMonth" Text="Month" runat="server" />
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblMFrom" runat="server" Text="From"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAllocFMonth" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%; text-align: center">
                                                                <asp:Label ID="lblMTo" runat="server" Text="To"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAllocTMonth" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:CheckBox ID="chkDays" Text="Days" runat="server" />
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblDFrom" runat="server" Text="From"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAllocFDay" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%; text-align: center">
                                                                <asp:Label ID="lblDTo" runat="server" Text="To"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAllocTDay" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
