﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Reports_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../App_Themes/blue/blue.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" style="text-align: center">
    <script language="jscript" type="text/jscript">
        function Delete() {
            var myObject;            
            var strPath = '<%= Session("ExFileName") %>';
            if (strPath != null) {
                myObject = new ActiveXObject("Scripting.FileSystemObject");
                var f = myObject.GetFile(strPath);
                f.Delete();
            }
        }
    </script>
    
   <div style="width: 100%;height: 130px;overflow: auto;text-align: center;padding:10px 10px;">
    <h2>File Exported Successfully.</h2>
    <h3>Click 'Save' button to Save File</h3>    
    </div>
        <asp:Button ID="btnOpen" runat="server" Text="Save" style="margin-left:10px" CssClass="btndefault" />
    </form>
</body>
</html>
