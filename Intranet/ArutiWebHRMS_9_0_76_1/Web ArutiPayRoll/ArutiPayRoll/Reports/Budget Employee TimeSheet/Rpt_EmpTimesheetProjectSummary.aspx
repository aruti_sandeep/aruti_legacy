﻿<%@ Page Title="Employee Timesheet Project Summary Report" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="Rpt_EmpTimesheetProjectSummary.aspx.vb" Inherits="Reports_Budget_Employee_TimeSheet_Rpt_EmpTimesheetProjectSummary" %>

<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 45%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Timesheet Project Summary Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                             <%--  'Pinkal (28-Jul-2018) -- Start
                                                   'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                             <%--  'Pinkal (28-Jul-2018) -- End --%>
                                            <td style="width: 10%">
                                                <asp:Label ID="LblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblProjectCode" runat="server" Text="Project Code"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboProjectCode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="4">
                                                <asp:Label ID="LblTransactionHead" runat="server" Text="Transaction Head"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="4">
                                                <asp:TextBox ID="txtTransactionHeadFilter" AutoPostBack="true" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%; vertical-align: top" colspan="4">
                                                <asp:Panel ID="pnlTranHead" runat="server" Width="100%" ScrollBars="Auto" Height="200px">
                                                    <asp:DataGrid ID="dgTransactionHead" runat="server" Width="100%" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnEdit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" Enabled="true" AutoPostBack="true"
                                                                        OnCheckedChanged="chkSelectAll_Checkchanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkSelect_Checkchanged" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="code" HeaderText="Code" FooterText="dgcolhTranheadcode" />
                                                            <asp:BoundColumn DataField="name" HeaderText="Transaction Head" FooterText="dgcolhTransactionHead" />
                                                            <asp:BoundColumn DataField="tranheadunkid" HeaderText="TranHeadID" FooterText="objdgcolhTranHeadID"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                         <%--  'Pinkal (28-Jul-2018) -- Start
                                                   'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                        <tr style="width: 100%">
                                            <td align="left" colspan="2">
                                                <asp:CheckBox ID="chkShowReportInHtml" Text="Show Report In HTML" runat="server"/>
                                            </td>
                                            <td align="left" colspan="2">
                                                <asp:CheckBox ID="chkShowEmpIdentifyNo" Text="Show Employee Identify No " runat="server"/>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" colspan="2">
                                                <asp:CheckBox ID="chkShowLeaveTypeHrs" Text="Show Leave Type Hours" runat="server"/>
                                            </td>
                                            <td align="left" colspan="2">
                                                <asp:CheckBox ID="chkShowHolidayHrs" Text="Show Holiday Hours" runat="server"/>
                                            </td>
                                        </tr>
                                        
                                  <%--      'Pinkal (13-Aug-2018) -- Start
                                              'Enhancement - Changes For PACT [Ref #249,252]--%>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkShowExtraHrs" Text="Show Extra Hours" runat="server"/>
                                            </td>
                                            <td align="left" colspan="2">
                                                <asp:CheckBox ID="chkIgnoreZeroLeaveTypehrs" Text="Ignore Zero hours Leave Type" runat="server"/>
                                            </td>
                                         </tr>   
                                  <%-- 'Pinkal (13-Aug-2018) -- End--%>
                                         
                                         <tr style="width: 100%">
                                             <td style="text-align: right; font-weight: bold" colspan="4">
                                                <asp:LinkButton ID="lnkSave" Text="Save Settings" runat="server" CssClass="lnkhover"></asp:LinkButton>
                                            </td>
                                        </tr>
                                         <%--  'Pinkal (28-Jul-2018) -- End --%>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc1:AnalysisBy ID="popupAnalysisBy" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
