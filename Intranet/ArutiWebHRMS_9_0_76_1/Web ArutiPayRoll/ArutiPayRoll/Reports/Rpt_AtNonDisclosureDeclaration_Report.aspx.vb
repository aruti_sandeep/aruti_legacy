﻿#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
#End Region

Partial Class Reports_Rpt_AtNonDisclosureDeclaration_Report
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplinarySuspensionReport"
    Private objATNonDisclouserDeclarationReport As clsATNonDisclouserDeclarationReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            dtpDeclarationFromDate.SetDate = Nothing
            dtpDeclarationToDate.SetDate = Nothing

            dtpAuditToDate.SetDate = Nothing
            dtpAuditFromDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objATNonDisclouserDeclarationReport._EmployeeId = cboEmployee.SelectedValue
            objATNonDisclouserDeclarationReport._EmployeeName = cboEmployee.SelectedItem.Text

            If dtpDeclarationFromDate.IsNull = False AndAlso dtpDeclarationToDate.IsNull = False Then
                objATNonDisclouserDeclarationReport._DeclarationDateFrom = dtpDeclarationFromDate.GetDate
                objATNonDisclouserDeclarationReport._DeclarationDateTo = dtpDeclarationToDate.GetDate
            End If

            If dtpAuditFromDate.IsNull = False AndAlso dtpAuditToDate.IsNull = False Then
                objATNonDisclouserDeclarationReport._AuditDateFrom = dtpAuditFromDate.GetDate
                objATNonDisclouserDeclarationReport._AuditDateTo = dtpAuditToDate.GetDate
            End If

            objATNonDisclouserDeclarationReport._ViewByIds = mstrStringIds
            objATNonDisclouserDeclarationReport._ViewIndex = mintViewIdx
            objATNonDisclouserDeclarationReport._ViewByName = mstrStringName
            objATNonDisclouserDeclarationReport._Analysis_Fields = mstrAnalysis_Fields
            objATNonDisclouserDeclarationReport._Analysis_Join = mstrAnalysis_Join
            objATNonDisclouserDeclarationReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objATNonDisclouserDeclarationReport._Report_GroupName = mstrReport_GroupName

            objATNonDisclouserDeclarationReport._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objATNonDisclouserDeclarationReport._OpenAfterExport = False


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = objATNonDisclouserDeclarationReport._ReportName
            Me.lblPageHeader.Text = objATNonDisclouserDeclarationReport._ReportName
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblDeclarationDateFrom.Text = Language._Object.getCaption(Me.lblDeclarationDateFrom.ID, Me.lblDeclarationDateFrom.Text)
            Me.lblDeclarationTo.Text = Language._Object.getCaption(Me.lblDeclarationTo.ID, Me.lblDeclarationTo.Text)
            Me.lblAuditDateFrom.Text = Language._Object.getCaption(Me.lblAuditDateFrom.ID, Me.lblAuditDateFrom.Text)
            Me.lblAuditTo.Text = Language._Object.getCaption(Me.lblAuditTo.ID, Me.lblAuditTo.Text)

            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text)
            Me.BtnReport.Text = Language._Object.getCaption(Me.BtnReport.ID, Me.BtnReport.Text)
            Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text)

        Catch Ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Hemant (13 Aug 2020) -- End
        End Try
    End Sub

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objATNonDisclouserDeclarationReport = New clsATNonDisclouserDeclarationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = Me.ViewState("mstrStringIds")
                mstrStringName = Me.ViewState("mstrStringName")
                mintViewIdx = Me.ViewState("mintViewIdx")
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy")
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try

            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objATNonDisclouserDeclarationReport._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objATNonDisclouserDeclarationReport._UserUnkId = CInt(Session("UserId"))

            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            objATNonDisclouserDeclarationReport.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CStr(Session("ExportReportPath")), _
                                                CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))


            If objATNonDisclouserDeclarationReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objATNonDisclouserDeclarationReport._FileNameAfterExported
                Export.Show()
            End If

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy._Hr_EmployeeTable_Alias = "EM"
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
