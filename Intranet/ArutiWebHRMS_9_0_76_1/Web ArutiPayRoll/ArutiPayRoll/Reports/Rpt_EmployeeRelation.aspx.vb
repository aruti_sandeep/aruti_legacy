﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmployeeRelation
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objRelationReport As clsEmployeeRelationReport
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private ReadOnly mstrModuleName As String = "frmEmployeeRelationReport"
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjCMaster As New clsCommon_Master
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)

            'Shani(24-Aug-2015) -- End
            With drpEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-21}
            'dsList = ObjCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "RELATION")
            'With drpRelation
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("RELATION")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'S.SANDEEP [12-Apr-2018] -- END
            

            drpReportType.Items.Clear()
            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'ENHANCEMENT : LANGUAGE CHANGES
            'drpReportType.Items.Add("Dependants Wise")
            'drpReportType.Items.Add("Referee Wise")

            Language.setLanguage(mstrModuleName)
            drpReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Dependants Wise"))
            drpReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Referee Wise"))
            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-21}
            Call drpReportType_SelectedIndexChanged(New Object, New EventArgs)
            'S.SANDEEP [12-Apr-2018] -- END

            'S.SANDEEP [ 19 JUN 2014 ] -- END

            
            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(False, True, False, False)
            dsList = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 5
            End With
            'Pinkal (16-AUG-2014) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            ObjEmp = Nothing
            ObjCMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpEmployee.SelectedValue = 0
            drpRelation.SelectedValue = 0
            drpReportType.SelectedIndex = 0
            txtCode.Text = ""
            TxtName.Text = ""
            TxtLastName.Text = ""
            chkInActiveEmp.Checked = False
            'S.SANDEEP [ 06 JUN 2014 ] -- START
            chkshowdependantswithoutphotos.Checked = False
            'S.SANDEEP [ 06 JUN 2014 ] -- END

            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            dtpAsonDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboCondition.SelectedValue = 5
            chkIncludeInactiveDependents.Checked = False
            chkShowExemptedDependents.Checked = False
            chkShowInactiveDependents.Checked = False
            rdInActiveOption.SelectedIndex = 0
            'Pinkal (16-AUG-2014) -- End

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Call drpReportType_SelectedIndexChanged(drpReportType, New System.EventArgs)
            'Nilay (01-Feb-2015) -- End

            'S.SANDEEP [16-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 16
            chkShowSubGrandTotal.Checked = False
            'S.SANDEEP [16-SEP-2017] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objRelationReport.SetDefaultValue()

            objRelationReport._EmployeeId = drpEmployee.SelectedValue
            objRelationReport._EmployeeName = drpEmployee.SelectedItem.Text

            objRelationReport._RelationId = drpRelation.SelectedValue
            objRelationReport._RelationName = drpRelation.SelectedItem.Text

            objRelationReport._ReportTypeId = drpReportType.SelectedIndex
            objRelationReport._ReportTypeName = drpReportType.SelectedItem.Text

            'S.SANDEEP [16-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 16
            objRelationReport._ShowSubAndGrandTotalRelationWise = chkShowSubGrandTotal.Checked
            'S.SANDEEP [16-SEP-2017] -- END

            Select Case drpReportType.SelectedIndex
                Case 0  'Dependants
                    objRelationReport._FirstName = TxtName.Text.Trim
                    objRelationReport._LastName = TxtLastName.Text.Trim

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

                    objRelationReport._AsonDate = dtpAsonDate.GetDate.Date
                    objRelationReport._Condition = cboCondition.SelectedItem.Text

                    If chkIncludeInactiveDependents.Checked Then
                        objRelationReport._IncludeInactiveDependantsText = chkIncludeInactiveDependents.Text
                    End If
                    objRelationReport._IncludeInactiveDependants = chkIncludeInactiveDependents.Checked

                    If chkShowExemptedDependents.Checked Then
                        objRelationReport._ShowOnlyExemptedDependentsText = chkShowExemptedDependents.Text
                    End If
                    objRelationReport._ShowOnlyExemptedDependents = chkShowExemptedDependents.Checked

                    If chkShowInactiveDependents.Checked Then
                        objRelationReport._ShowOnlyInActiveDependantsText = chkShowInactiveDependents.Text
                    End If
                    objRelationReport._ShowOnlyInactiveDependants = chkShowInactiveDependents.Checked

                    If rdInActiveOption.Enabled Then
                        If rdInActiveOption.SelectedIndex = 0 Then
                            objRelationReport._AgeLimitWiseText = rdInActiveOption.SelectedItem.Text
                            objRelationReport._AgeLimitWise = True
                        End If
                        If rdInActiveOption.SelectedIndex = 1 Then
                            objRelationReport._ExemptionWiseText = rdInActiveOption.SelectedItem.Text
                            objRelationReport._ExemptionWise = True
                        End If

                    End If

                    'Pinkal (16-AUG-2014) -- End

                Case 1  'Referee
                    objRelationReport._FirstName = TxtName.Text.Trim
            End Select

            objRelationReport._IsActive = chkInActiveEmp.Checked

            'S.SANDEEP [ 06 JUN 2014 ] -- START
            Select Case drpReportType.SelectedIndex
                Case 0  'DEPENDANTS
                    objRelationReport._ShowDepedantsWithoutImage = chkshowdependantswithoutphotos.Checked
                    objRelationReport._ShowDepedantsWithoutImageText = chkshowdependantswithoutphotos.Text
                    objRelationReport._IsImageInDatabase = Session("IsImgInDataBase")
            End Select
            'S.SANDEEP [ 06 JUN 2014 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objRelationReport._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objRelationReport = New clsEmployeeRelationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'ENHANCEMENT : LANGUAGE CHANGES
            Call SetLanguage()
            'S.SANDEEP [ 19 JUN 2014 ] -- END

            If Not IsPostBack Then
                Call FillCombo()
                drpReportType_SelectedIndexChanged(sender, e)


                'Pinkal (16-AUG-2014) -- Start
                'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                rdInActiveOption.Enabled = False
                'Pinkal (16-AUG-2014) -- End


            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objRelationReport._CompanyUnkId = Session("CompanyUnkId")
            objRelationReport._UserUnkId = Session("UserId")
            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objRelationReport._UserAccessFilter = Session("AccessLevelFilterString")
            'S.SANDEEP [ 12 NOV 2012 ] -- END
            objRelationReport.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRelationReport.generateReport(0, enPrintAction.None, enExportAction.None)
            objRelationReport.generateReportNew(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("ExportReportPath"), _
                                                Session("OpenAfterExport"), 0, _
                                                enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objRelationReport._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Protected Sub drpReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpReportType.SelectedIndexChanged
        Try
            Select Case drpReportType.SelectedIndex
                Case 0  'Depandants

                    'Pinkal (09-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                    'LblName.Visible = False
                    'TxtName.Visible = False
                    'LblFirstName.Visible = True
                    'TxtFirstName.Visible = True
                    'TxtLastName.Enabled = True
                    pnlfname.Visible = True
                    pnlname.Visible = False

                    'Pinkal (09-Jan-2013) -- End

                    'S.SANDEEP [ 06 JUN 2014 ] -- START
                    chkshowdependantswithoutphotos.Enabled = True
                    'S.SANDEEP [ 06 JUN 2014 ] -- END


                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                    dtpAsonDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpAsonDate.Enabled = True
                    cboCondition.SelectedValue = 5
                    cboCondition.Enabled = True
                    chkIncludeInactiveDependents.Checked = False
                    chkIncludeInactiveDependents.Enabled = True
                    chkShowExemptedDependents.Checked = False
                    chkShowExemptedDependents.Enabled = True
                    chkShowInactiveDependents.Checked = False
                    chkShowInactiveDependents.Enabled = True
                    'Pinkal (16-AUG-2014) -- End


                Case 1  'Referee

                    'Pinkal (09-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                    'TxtLastName.Text = ""
                    'LblName.Visible = True
                    'TxtName.Visible = True
                    'LblFirstName.Visible = False
                    'TxtFirstName.Visible = False
                    'TxtLastName.Enabled = False
                    pnlname.Visible = True
                    pnlfname.Visible = False

                    'Pinkal (09-Jan-2013) -- End

                    'S.SANDEEP [ 06 JUN 2014 ] -- START
                    chkshowdependantswithoutphotos.Checked = False
                    chkshowdependantswithoutphotos.Enabled = False
                    'S.SANDEEP [ 06 JUN 2014 ] -- END

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                    dtpAsonDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpAsonDate.Enabled = False
                    cboCondition.SelectedValue = 5
                    cboCondition.Enabled = False
                    chkIncludeInactiveDependents.Checked = False
                    chkIncludeInactiveDependents.Enabled = False
                    chkShowExemptedDependents.Checked = False
                    chkShowExemptedDependents.Enabled = False
                    chkShowInactiveDependents.Checked = False
                    chkShowInactiveDependents.Enabled = False
                    'Pinkal (16-AUG-2014) -- End

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    rdInActiveOption.Enabled = False
                    'Shani(24-Aug-2015) -- End

            End Select

            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-21}
            Dim dsList As New DataSet
            Dim objCMaster As New clsCommon_Master
            Select Case drpReportType.SelectedIndex
                Case 0
                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "RELATION")
                Case 1
                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "RELATION", -1, True)
            End Select
            With drpRelation
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("RELATION")
                .DataBind()
                .SelectedValue = 0
            End With
            'S.SANDEEP [12-Apr-2018] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpReportType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    'Pinkal (16-AUG-2014) -- Start
    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

#Region "Checkbox Event"

    Protected Sub chkShowExemptedDependents_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowExemptedDependents.CheckedChanged
        Try
            If chkShowExemptedDependents.Checked Then
                chkIncludeInactiveDependents.Checked = False
                chkIncludeInactiveDependents.Enabled = False
                chkShowInactiveDependents.Checked = False
                chkShowInactiveDependents.Enabled = False
                cboCondition.SelectedValue = 5 'EQUAL TO
                rdInActiveOption.Enabled = False
            Else
                chkIncludeInactiveDependents.Checked = False
                chkIncludeInactiveDependents.Enabled = True
                chkShowInactiveDependents.Checked = False
                chkShowInactiveDependents.Enabled = True
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkShowExemptedDependents_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub chkIncludeInactiveDependents_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeInactiveDependents.CheckedChanged, chkShowInactiveDependents.CheckedChanged
        Try
            If CType(sender, CheckBox).ID = "chkIncludeInactiveDependents" AndAlso CType(sender, CheckBox).Checked Then
                chkShowInactiveDependents.Checked = False
                rdInActiveOption.Enabled = True
                cboCondition.Enabled = False
                cboCondition.SelectedValue = 1 '>
            ElseIf CType(sender, CheckBox).ID = "chkShowInactiveDependents" AndAlso CType(sender, CheckBox).Checked Then
                chkIncludeInactiveDependents.Checked = False
                rdInActiveOption.Enabled = True
                cboCondition.Enabled = False
                cboCondition.SelectedValue = 2 '<
            Else
                cboCondition.SelectedValue = 5 'EQUAL TO
                cboCondition.Enabled = True
                rdInActiveOption.Enabled = False
            End If
            rdInActiveOption.SelectedIndex = 0
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkIncludeInactiveDependents_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Radiobutton Event"

    Protected Sub rdInActiveOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdInActiveOption.SelectedIndexChanged
        Try
            If rdInActiveOption.SelectedIndex = 0 Then
                cboCondition.SelectedValue = 1 '>
            ElseIf rdInActiveOption.SelectedIndex = 1 Then
                cboCondition.SelectedValue = 2 '<
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("rdInActiveOption_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (16-AUG-2014) -- End

    Protected Sub drpEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.DataBound
        Try         'Hemant (13 Aug 2020)
        If drpEmployee.Items.Count > 0 Then
            For Each lstItem As ListItem In drpEmployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            drpEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        ' Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
        Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.ID, Me.lblFirstname.Text)
        Me.lblLastname.Text = Language._Object.getCaption(Me.lblLastname.ID, Me.lblLastname.Text)
        Me.lblName.Text = Language._Object.getCaption(Me.lblName.ID, Me.lblName.Text)
        Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.ID, Me.lblRelation.Text)
        Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.ID, Me.lblReportType.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
        Me.chkIncludeInactiveDependents.Text = Language._Object.getCaption(Me.chkIncludeInactiveDependents.ID, Me.chkIncludeInactiveDependents.Text)
        Me.chkshowdependantswithoutphotos.Text = Language._Object.getCaption(Me.chkshowdependantswithoutphotos.ID, Me.chkshowdependantswithoutphotos.Text)
        Me.chkShowExemptedDependents.Text = Language._Object.getCaption(Me.chkShowExemptedDependents.ID, Me.chkShowExemptedDependents.Text)
        Me.chkShowInactiveDependents.Text = Language._Object.getCaption(Me.chkShowInactiveDependents.ID, Me.chkShowInactiveDependents.Text)
        Me.LblAsonDate.Text = Language._Object.getCaption(Me.LblAsonDate.ID, Me.LblAsonDate.Text)
        Me.LblCondition.Text = Language._Object.getCaption(Me.LblCondition.ID, Me.LblCondition.Text)
        Me.lnInActiveDependentOption.Text = Language._Object.getCaption(Me.lnInActiveDependentOption.ID, Me.lnInActiveDependentOption.Text)
        Me.rdInActiveOption.Items(0).Text = Language._Object.getCaption("rdAgeLimitWise", Me.rdInActiveOption.Items(0).Text)
        Me.rdInActiveOption.Items(1).Text = Language._Object.getCaption("rdExemptionWise", Me.rdInActiveOption.Items(1).Text)

        Me.BtnReport.Text = Language._Object.getCaption(Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
        Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
        Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END

End Class
