﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_EmployeeClaimForm.aspx.vb"
    Inherits="Reports_Rpt_EmployeeClaimForm" Title="Employee Claim Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Claim Form"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative;">
                                    <table width="100%">
                                        <tr>
                                               <td style="width: 30%">
                                                    <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                                </td>
                                               <td style="width: 70%">
                                                    <table width="100%">
                                                        <tr style="width:100%">
                                                            <td>
                                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="true" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LblToDate" runat="server" Text="To"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblExpenseCategory" runat="server" Text="Expense Category"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack ="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblClaimform" runat="server" Text="Claim Form"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboClaimForm" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblSpecialAllowance" runat="server" Text="Select Special Allowance(s)"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:TextBox ID="txtFilterAllowances" runat="server"> </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            &nbsp;
                                            </td>
                                            <td style="width: 48%; vertical-align: top;">
                                            <asp:Panel ID = "pnlTranHead" runat="server">
                                                <div id="scrollable-container" style="width: 100%; height: 150px; overflow: auto;
                                                    vertical-align: top" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgTranHeads" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <ItemStyle CssClass="griviewitem" />
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="code" HeaderText="code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                FooterText="dgcolhTranHeadCode">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Tran. Head" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                FooterText="dgcolhTranHead">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="tranheadunkid" HeaderText="tranheadunkid" ReadOnly="true"
                                                                Visible="false"></asp:BoundColumn>
                                                            
                                                        </Columns>
                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                    </asp:DataGrid>
                                                </div>
                                                </asp:Panel>
                                                <%--<asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />--%>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
