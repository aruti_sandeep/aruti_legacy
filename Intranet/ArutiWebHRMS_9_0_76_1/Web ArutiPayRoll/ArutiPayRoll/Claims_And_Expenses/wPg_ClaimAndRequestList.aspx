﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ClaimAndRequestList.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_ClaimAndRequestList" Title="Claim & Request List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Claim & Request List"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 10%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 12%">
                                            <asp:Label ID="lblExpCategory" runat="server" Text="Expense Category"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="cboExpCategory" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 5%; text-align: center">
                                            <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                        </td>
                                        <td style="width: 33%">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 45%">
                                                        <uc2:DateCtrl ID="dtpFDate" runat="server" AutoPostBack="False" />
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>  
                                                    </td>
                                                    <td style="width: 45%">
                                                        <uc2:DateCtrl ID="dtpTDate" runat="server" AutoPostBack="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 10%">
                                            <asp:Label ID="lblClaimNo" runat="server" Text="Claim NO"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:TextBox ID="txtClaimNo"  runat="server"></asp:TextBox>
                                        </td>
                                        <td style="width: 12%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 10%">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period" Visible="false"></asp:Label>
                                        </td>
                                        <td style="width: 28%">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Visible="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    <asp:HiddenField ID="btnHidden" runat="Server" />
                                </div>
                            </div>
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                    <td style="width: 100%">
                                        <asp:Panel ID="pnl_lvClaimRequestList" Style="margin-top: 5px; max-height: 300px"
                                            ScrollBars="Auto" runat="server">
                                            <asp:DataGrid ID="lvClaimRequestList" runat="server" AutoGenerateColumns="False"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                    CssClass="griddelete"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Cancel" HeaderStyle-HorizontalAlign="Center" FooterText="mnuCancelExpenseForm">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" ToolTip="Cancel" CommandName="Cancel"
                                                                CssClass="lnkhover"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="claimrequestno" HeaderText="Claim No" ReadOnly="true"
                                                        FooterText="colhclaimno" />
                                                    <asp:BoundColumn DataField="expensetype" HeaderText="Expense Cat." ReadOnly="true"
                                                        FooterText="colhExpType" />
                                                    <asp:BoundColumn DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhemployee" />
                                                    <asp:BoundColumn DataField="tdate" HeaderText="Date" ReadOnly="true" FooterText="colhdate" />
                                                    <asp:BoundColumn DataField="" HeaderText="Amount" ReadOnly="true" FooterText="colhamount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="" HeaderText="Aprroved Amt." ReadOnly="true" FooterText="colhApprovedAmount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="status" HeaderText="Status" ReadOnly="true" FooterText="colhstatus" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                        FooterText="objColhStatusunkid" Visible="false" />
                                                    <asp:BoundColumn DataField="expensetypeid" HeaderText="ExpenseTypeId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="crmasterunkid" ReadOnly="true"
                                                        Visible="false" />
                                                        
                                                      <%--'Pinkal (13-Mar-2019) -- Start Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                        <asp:BoundColumn DataField="p2prequisitionid" HeaderText="p2prequisitionid" ReadOnly="true"
                                                        Visible="false" />
                                                   <%--     'Pinkal (13-Mar-2019) -- End--%>
                                                   
                                                  <%-- 'Pinkal (28-Apr-2020) -- Start
                                                   'Optimization  - Working on Process Optimization and performance for require module.--%>
                                                     <asp:BoundColumn DataField="ReqAmount" HeaderText="ReqAmount" ReadOnly="true"
                                                        Visible="false" />
                                                          <asp:BoundColumn DataField="ApprovedAmt" HeaderText="ApprovedAmt" ReadOnly="true"
                                                        Visible="false" />
                                                  <%-- 'Pinkal (28-Apr-2020) -- End--%>
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                    <td style="width: 100%">
                                        <asp:Panel ID="pnlCancelPopup" runat="server" Width="800px" CssClass="newpopup" Style="display: none;"
                                            DefaultButton="btnCancelSave">
                                            <div class="panel-primary" style="margin:0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblpopupHeader" runat="server" Text="Cancel Expense Form Information"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblTitle" runat="server" Text="Filter Criteria"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="lblCancelExpCategory" runat="server" Text="Exp. Cat."></asp:Label>
                                                                    </td>
                                                                    <td style="width: 45%" colspan="3">
                                                                        <asp:DropDownList ID="cboCancelExpCategory" runat="server" Enabled="false" Width="370px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td rowspan="3" style="vertical-align: top; width: 15%">
                                                                        <asp:Label ID="lblCancelRemark" runat="server" Text="Cancel Remark"></asp:Label>
                                                                    </td>
                                                                    <td rowspan="3" style="vertical-align: top; width: 30%">
                                                                        <asp:TextBox ID="txtCancelRemark" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="lblCancelPeriod" runat="server" Text="Period" Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblName" runat="server" Text="Claim No."></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:DropDownList ID="cboCancelPeriod" runat="server" Visible="false" Enabled="false">
                                                                        </asp:DropDownList>
                                                                        <asp:TextBox ID="txtCancelClaimNo" runat="server" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="lblCancelDate" runat="server" Text="Date"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <uc2:DateCtrl ID="dtpCancelDate" runat="server" AutoPostBack="False" Enabled="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 10%">
                                                                        <asp:Label ID="lblCancelEmployee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3" style="width: 45%">
                                                                        <asp:TextBox ID="txtCancelEmployee" runat="server" Enabled="false"></asp:TextBox>
                                                                        <asp:HiddenField ID="txtCancelEmployeeid" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Panel ID="pnl_dgvdata" Style="margin-top: 10px" runat="server" Height="300px"
                                                                            ScrollBars="Auto">
                                                                            <asp:GridView ID="dgvData" runat="server" Style="margin: auto" AutoGenerateColumns="False" DataKeyNames = "crapprovaltranunkid"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkCancelAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkCancelAll_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkCancelSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkCancelSelect_CheckedChanged" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="Expense" HeaderText="Claim/Expense Desc" ReadOnly="true" FooterText="dgcolhExpense"
                                                                                        HeaderStyle-HorizontalAlign="Left" />
                                                                                    <asp:BoundField DataField="UoM" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM"
                                                                                        HeaderStyle-HorizontalAlign="Left" />
                                                                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                                    <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                                    <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                                        
                                                                                            <%--Pinkal (04-Feb-2019) -- Start Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                                                      <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="true" FooterText="dgcolhCurrency" />
                                                                                            <%--Pinkal (04-Feb-2019) -- End --%>
                                                                                        
                                                                                    <asp:BoundField DataField="expense_remark" HeaderText="" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="GridViewRowVisibleFalse" ItemStyle-CssClass="GridViewRowVisibleFalse" />
                                                                                    <asp:BoundField DataField="crapprovaltranunkid" HeaderText="" HeaderStyle-HorizontalAlign="Right"
                                                                                        ReadOnly="true" HeaderStyle-CssClass="GridViewRowVisibleFalse" ItemStyle-CssClass="GridViewRowVisibleFalse" />
                                                                                    <asp:BoundField DataField="crmasterunkid" HeaderText="" ReadOnly="true" HeaderStyle-HorizontalAlign="Right"
                                                                                        HeaderStyle-CssClass="GridViewRowVisibleFalse" ItemStyle-CssClass="GridViewRowVisibleFalse" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                            <div style="margin-top:10px" align="right">
                                                                                <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                                                                <asp:TextBox ID="txtGrandTotal" Style="text-align: right" Width="20%" runat="server"
                                                                                    Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnCancelSave" runat="server" Text="Save" CssClass="btnDefault" />
                                                                <asp:Button ID="btnCancelClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupCancel" runat="server" CancelControlID="btnCancelClose"
                    DropShadow="true" BackgroundCssClass="ModalPopupBG" PopupControlID="pnlCancelPopup"
                    TargetControlID="btnHidden">
                </cc1:ModalPopupExtender>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server"  />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
