﻿<%@ Page Title="Global Approve Expense" Language="VB" MasterPageFile="~/home.master"
AutoEventWireup="false" CodeFile="wPg_GlobalApproveExpense.aspx.vb" Inherits="Claims_And_Expenses_wPg_GlobalApproveExpense" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
 <%--  'Pinkal (22-Oct-2018) -- Start
          'Enhancement - Implementing Claim & Request changes For NMB .--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<%--  'Pinkal (22-Oct-2018) -- End--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>
      
    <script type="text/javascript">
        function setscrollPosition(sender) {
            $(".101010").scrollLeft(document.getElementById("<%=hdf_leftposition.ClientID%>").value);
            $(".101010").scrollTop(document.getElementById("<%=hdf_topposition.ClientID%>").value)
          
        }
        function getscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = $(".101010").scrollTop();
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = $(".101010").scrollLeft();
            document.getElementById("<%=hdf_pagetop.ClientID%>").value = $(window).scrollTop();
        }
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
               
        function endRequestHandler(sender, args) {
            $(window).scrollTop(document.getElementById("<%=hdf_pagetop.ClientID%>").value);
        }
        
        function selectall(chk) {
            $("[id*='chkSelect']").attr('checked', chk.checked);
            }

            function selectallgroupchild(chk) {
                if ($(chk).closest('tr').next('tr').children('td').length > $(chk).closest('tr').children('td').length) {
                    selectnextrow($(chk).closest('tr').next('tr'), chk);
                }
            }

            function selectnextrow(obj, chk) {
                $(obj).find("[id*='chkSelect']").attr('checked', chk.checked);
                if ($(obj).next('tr').children('td').length == $(obj).children('td').length) {
                    selectnextrow($(obj.next('tr')), chk);              
                }
            }
    </script>
    
<center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Approve Expense"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                     <table style="width: 100%; margin-top: -10px; margin-bottom: -10px">
                                    <tr style="width: 100%">
                                        <td style="width: 50%" valign="top">
                                            <div class="panel-body">
                                                <div id="Div1" class="panel-default" style="margin-left: -2px; margin-right: -2px">
                                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="gbFilter" runat="server" Text="Filter Criteria"></asp:Label>
                                                        </div>
                                                        <div style="float: right">
                                                                 <asp:LinkButton ID="lnkAllocation" Style="font-weight: bold; font-size: 12px; margin-right: 5px"
                                                                  Font-Underline="false" runat="server" Text="Allocation"></asp:LinkButton>
                                                                 <asp:LinkButton ID="lnkReset" Style="font-weight: bold; font-size: 12px; margin-right: 5px"
                                                                  Font-Underline="false" runat="server" Text="Reset"></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                     <div id="Div2" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                     <tr style="width: 100%">
                                                                              <td style="width: 30%">
                                                                                    <asp:Label ID="LblExpenseCategory" runat="server" Text="Expense Category"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                     </tr>
                                                                     <tr style="width: 100%">
                                                                              <td style="width: 30%">
                                                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="false">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                     </tr>
                                                                     <tr style="width: 100%">
                                                                              <td style="width: 30%">
                                                                                    <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                     </tr>
                                                                     <tr style="width: 100%">
                                                                              <td style="width: 30%">
                                                                                    <asp:Label ID="lblSector" runat="server" Text="Sector/Route" Visible="false"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="false" Visible="false">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                     </tr>
                                                            </table>
                                                    </div> 
                                                  </div> 
                                              </div>
                                         </td>
                                         <td style="width: 50%" valign="top">
                                             <div class="panel-body">
                                                <div id="Div3" class="panel-default" style="margin-right: -2px">
                                                      <div id="Div4" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbInfo" runat="server" Text="Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                      <div id="Div5" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                              <td style="width: 30%">
                                                                                    <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboApprover" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                     </tr>
                                                                     <tr style="width: 100%">
                                                                              <td style="width: 30%">
                                                                                    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="false">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                     </tr>
                                                                      <tr style="width: 100%">
                                                                    <td style="width: 30%" valign="top">
                                                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                                             </td>
                                                                            <td style="width: 70%">
                                                                        <asp:TextBox ID="txtRemarks" runat="server" Rows="3" TextMode="MultiLine">
                                                                                </asp:TextBox>
                                                                            </td>
                                                                     </tr>
                                                            </table>
                                                    </div>
                                                </div>
                                             </div>
                                         </td> 
                                     </tr> 
                                 </table>
                               </div>
                               <div id="Div6" class="panel-default">
                                    <div id="Div7" class="panel-heading-default">
                                        <asp:Label ID="gbPending" Text="Pending Claim Expense Application(s)." runat="server"></asp:Label>
                                    </div>
                                     <div id="Div8" class="panel-body-default">
                                        <div id="scrollable-container" class="101010" onscroll="$(scroll.Y).val(this.scrollTop);"
                                            style="overflow: auto; height: 350px; vertical-align: top">
                                            <asp:UpdatePanel ID="DgUpdatePanel" ChildrenAsTriggers="true" runat="server">
                                            <ContentTemplate>
                                                                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                    HeaderStyle-Font-Bold="false" Width="175%">
                                                                    <ItemStyle CssClass="griviewitem" />
                                                                            <Columns>
                                                            <asp:TemplateColumn ItemStyle-Width="25">
                                                                                        <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" Enabled="true" onclick="selectall(this)" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" Enabled="true" onclick="selectallgroupchild(this)" />
                                                                                        </ItemTemplate>
                                                                                   </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="ClaimNo" HeaderText="Claim No" FooterText="dgcolhClaimNo" />
                                                                                    <asp:BoundColumn DataField="Expense" HeaderText="Claim/Expense Desc" FooterText="dgcolhClaimExpense" />     
                                                                                   <asp:BoundColumn DataField="Sector" HeaderText="Sector/Route" FooterText="dgcolhSectorRoute" />     
                                                                                   <asp:BoundColumn DataField="UOM" HeaderText="UOM" FooterText="dgcolhUOM" />     
                                                            <asp:BoundColumn DataField="Balance" HeaderText="Balance" FooterText="dgcolhBalance"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                                                       <%--Pinkal (04-Feb-2019) -- Start Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <asp:TemplateColumn HeaderText="Cost Center" HeaderStyle-Width="150px" ItemStyle-Width="150px"
                                                                FooterText="dgcolhCostCenter">
                                                                                               <ItemTemplate>
                                                                    <asp:DropDownList ID="dgcolhCostCenter" runat="server">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hdcolhCostCenter" runat="server" Value='<%# Eval("costcenterunkid") %>' />
                                                                                               </ItemTemplate>
                                                                                                 <ItemStyle Width="150px" />
                                                                                     </asp:TemplateColumn>
                                                                                   <%--Pinkal (04-Feb-2019) -- End--%>
                                                            <asp:BoundColumn DataField="AppliedQty" HeaderText="Applied Quantity" FooterText="dgcolhAppliedQty"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="AppliedAmount" HeaderText="Applied Amount" FooterText="dgcolhAppliedAmount"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                                                     <%--Pinkal (04-Feb-2019) -- Start Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <asp:TemplateColumn HeaderText="Currency" HeaderStyle-Width="120px" ItemStyle-Width="120px"
                                                                FooterText="dgcolhCurrency">
                                                                                               <ItemTemplate>
                                                                    <asp:DropDownList ID="dgcolhCurrency" runat="server">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hdcolhCurrency" runat="server" Value='<%# Eval("countryunkid") %>' />
                                                                                               </ItemTemplate>
                                                                                                 <ItemStyle Width="120px" />
                                                                                     </asp:TemplateColumn>
                                                                                   <%--Pinkal (04-Feb-2019) -- End--%>
                                                            <asp:BoundColumn DataField="Claim_Remark" HeaderText="Claim Remark" FooterText="dgcolhClaimRemark"
                                                                ItemStyle-Wrap="true" ItemStyle-Width="200px" ItemStyle-VerticalAlign="Top" />
                                                            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="150px"
                                                                ItemStyle-Width="150px" FooterText="dgcolhQuantity">
                                                                                               <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgcolhQuantity" Style="text-align: right" runat="server" AutoPostBack="True"
                                                                        Text='<%# Eval("quantity") %>' OnTextChanged="txtdgcolhQuantity_TextChanged"
                                                                        onblur="getscrollPosition()"></asp:TextBox>
                                                                                               </ItemTemplate>
                                                                                              <HeaderStyle HorizontalAlign="Right" />
                                                                                                    <ItemStyle Width="150px" />
                                                                                </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="150px"
                                                                ItemStyle-Width="150px" FooterText="dgcolhUnitPrice">
                                                                                             <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgcolhUnitPrice" Style="text-align: right" runat="server" AutoPostBack="True"
                                                                        Text='<%# Eval("unitprice") %>' OnTextChanged="txtdgcolhUnitPrice_TextChanged"
                                                                        onblur="getscrollPosition()"></asp:TextBox>
                                                                                             </ItemTemplate>
                                                                                             <HeaderStyle HorizontalAlign="Right" />
                                                                                            <ItemStyle Width="150px" />
                                                                              </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:TemplateColumn HeaderText="Expense Remark" HeaderStyle-Width="200px" ItemStyle-Width="200px"
                                                                FooterText="dgcolhExpenseRemark">
                                                                                  <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgcolhExpenseRemark" runat="server" Text='<%# Eval("Expense_Remark") %>'
                                                                        onblur="getscrollPosition()"></asp:TextBox>
                                                                                  </ItemTemplate>
                                                                                 <ItemStyle Width="200px" />
                                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="IsGrp" HeaderText="IsGrp" FooterText="objdgcolhIsGrp"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GrpId" HeaderText="Group Id" FooterText="objdgcolhGrpId"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="Employee ID" FooterText="objdgcolhEmployeeunkid"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="isleaveencashment" HeaderText="Leave Encashment" FooterText="objdgcolhIsLeaveEncashment"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="isaccrue" HeaderText="Is Accrue" FooterText="objdgcolhIsAccrue"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="expenseunkid" HeaderText="ExpenseID" FooterText="objdgcolhExpenseID"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="CrMasterID" FooterText="objdgcolhCrmasterunkid"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crapprovaltranunkid" HeaderText="CrApprovalTranunkid"
                                                                FooterText="objdgcolhCrapprovaltranunkid" Visible="false" />
                                                            <asp:BoundColumn DataField="uomunkid" HeaderText="UOMID" FooterText="objdgcolhUOMID"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="CLno" HeaderText="CLno" FooterText="objdgcolhCLno" Visible="false" />

 <%--'Pinkal (20-May-2022) -- Start
                                                          'Optimize Global Claim Request for NMB.--%>
                                                            <asp:BoundColumn DataField="referenceunkid" HeaderText="referenceunkid" FooterText="objdgcolhreferenceunkid"
                                                                Visible="false" />
                                                             <asp:BoundColumn DataField="frommoduleid" HeaderText="frommoduleid" FooterText="objdgcolhfrommoduleid"
                                                                Visible="false" />
                                                                <asp:BoundColumn DataField="modulerefunkid" HeaderText="modulerefunkid" FooterText="objdgcolhmodulerefunkid"
                                                                Visible="false" />   
                                                            <%--'Pinkal (20-May-2022) -- End  --%>

                                                                </Columns>
                                                       <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                   </asp:DataGrid>
                                               </ContentTemplate>
                                           </asp:UpdatePanel>
                                        </div>
                                                <div id="btnfixedbottom" class="btn-default">
                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                                </div>
                                        </div>
                                </div>
                     </div> 
                            <asp:HiddenField ID="hdf_topposition" runat="server" />
                            <asp:HiddenField ID="hdf_leftposition" runat="server" />
                            <asp:HiddenField ID="hdf_pagetop" runat="server" />
                     <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                         <%--  'Pinkal (22-Oct-2018) -- Start
                         'Enhancement - Implementing Claim & Request changes For NMB .--%>
                            <uc3:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" Message="" />
                        <%--  'Pinkal (22-Oct-2018) -- End --%>
                  </ContentTemplate> 
            </asp:UpdatePanel> 
         </asp:Panel> 
</center>        
</asp:Content> 