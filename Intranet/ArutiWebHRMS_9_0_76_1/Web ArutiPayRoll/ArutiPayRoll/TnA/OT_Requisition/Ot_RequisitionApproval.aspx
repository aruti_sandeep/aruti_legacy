﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Ot_RequisitionApproval.aspx.vb"
    Inherits="TnA_OT_Requisition_Ot_RequisitionApproval" Title="OT Requisition Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
        
        // Pinkal (10-Jan-2020) -- Start
        //  Enhancements -  Working on OT Requisistion for NMB.

        $("[id*=chkSelectAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkSelect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        // Pinkal (10-Jan-2020) -- End
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="OT Requisition Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                      <%--  <div style="width: 10%;" class="ib">
                                            <asp:Label ID="lblListPeriod" runat="server" Text="Period" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 20%;" class="ib">
                                            <asp:DropDownList ID="cboListPeriod" runat="server" Width="220px" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>--%>
                                         <div style="width: 12%;" class="ib">
                                            <asp:Label ID="lblListEmployee" runat="server" Text="Employee" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 24%;" class="ib">
                                            <asp:DropDownList ID="cboListEmployee" runat="server" Width="250px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 10%;" class="ib">
                                            <asp:Label ID="lblListOTReqFromDate" runat="server" Text="Req. From Date " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <uc1:DateCtrl ID="dtpListOTFromDate" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                       <div style="width: 12%;" class="ib">
                                            <asp:Label ID="lblRequisitionStatus" runat="server" Text="Requisition Status" Width="100%"></asp:Label>
                                        </div>
                                       <div style="width: 24%;" class="ib">
                                            <asp:DropDownList ID="cboRequisitionStatus" runat="server" Width="250px">
                                            </asp:DropDownList>
                                         </div>
                                        <div style="width: 10%;" class="ib">
                                            <asp:Label ID="lblListOTReqToDate" runat="server" Text="Req. To Date " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <uc1:DateCtrl ID="dtpListOTReqToDate" runat="server" AutoPostBack="true" />
                                        </div>
                                          <div style="width: 20%;" class="ib">
                                         <asp:CheckBox ID="chkMyApprovals" runat="server" Width="220px" Text = "My Approval" Checked ="true" >
                                            </asp:CheckBox>
                                         </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%; margin-top: 5px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server" Style="margin-top: 5px;
                                                    height: 300px">
                                                    <asp:GridView ID="gvOTRequisitionApproverList" runat="server" DataKeyNames="IsGrp,Employee,Particulars,ischecked,employeeunkid,otrequisitiontranunkid,otrequestapprovaltranunkid,RDate,tnalevelunkid,tnapriority,mapuserunkid,OTRequisitionStatusId,ApprovalStatusId"
                                                        AutoGenerateColumns="False" Width="145%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                        ShowFooter="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                 <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                  <%-- <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />--%>
                                                                     <asp:CheckBox ID="chkSelectAll" runat="server"/>
                                                                 <%--Pinkal (10-Jan-2020) -- End--%>  
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                 <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                  <%--<asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />--%>
                                                                  <asp:CheckBox ID="chkSelect" runat="server"/>
                                                                    <%--Pinkal (10-Jan-2020) -- End--%>  
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Particulars" HeaderText="Particulars" ItemStyle-VerticalAlign="Top"
                                                                FooterText="colhotParticulars" Visible="false" />
                                                            <asp:BoundField DataField="Approver" HeaderText="Approver" ItemStyle-VerticalAlign="Top"
                                                                FooterText="colhotreqapprover" ItemStyle-Width="350px" />
                                                             <asp:BoundField DataField="plannedstart_time" HeaderText="Planned Start Time" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="230px" FooterText="colhotreqplannedstart_time" />
                                                            <asp:BoundField DataField="plannedend_time" HeaderText="Planned End Time" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="230px" FooterText="colhotreqplannedend_time" />
                                                            <asp:BoundField DataField="plannedot_hours" HeaderText="Planned OT Hrs" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="125px" FooterText="colhotreqplannedOTHrs" />
                                                             <asp:BoundField DataField="approvaldate" HeaderText="Approval Date" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="170px" FooterText="colhotapprovaldate" />
                                                            <asp:BoundField DataField="actualstart_time" HeaderText="Actual Start Time" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="220px" FooterText="colhotreqactualstart_time" />
                                                            <asp:BoundField DataField="actualend_time" HeaderText="Actual End Time" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="250px" FooterText="colhotreqactualend_time" />
                                                            <asp:BoundField DataField="actualot_hours" HeaderText="Actual OT Hrs" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="90px" FooterText="colhotactualHrs" />
                                                             <asp:BoundField DataField="TotalApprovedHrs" HeaderText="Total Approved Hrs" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="100px" FooterText="colhotTotalapprovedhrs" />
                                                            <asp:BoundField DataField="status" HeaderText="Status" ItemStyle-VerticalAlign="Top"
                                                                ItemStyle-Width="300px" FooterText="colhotreqstatus" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnChangeStatus" runat="server" CssClass="btnDefault" Text="Change Status" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupOTRequisition" BackgroundCssClass="modalBackground"
                        TargetControlID="lblReasonForAdjustment" runat="server" PopupControlID="pnlOTRequisition"
                        DropShadow="false" CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlOTRequisition" runat="server" CssClass="newpopup" Style="display: none;
                        width: 1000px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblOTApprovalTitle" Text="OT Approval" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <div class="row2">
                                       <%-- <div style="width: 10%;" class="ib">
                                            <asp:Label ID="LblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 30%;" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Enabled="false" Width="260px">
                                            </asp:DropDownList>
                                        </div>--%>
                                         <div style="width: 10%;" class="ib">
                                            <asp:Label ID="LblFromDate" runat="server" Text="Req. From Date " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <uc1:DateCtrl ID="dtpFromDate" runat="server"  />
                                        </div>
                                        <div style="width: 10%;" class="ib">
                                            <asp:Label ID="LblToDate" runat="server" Text="Req. To Date " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <uc1:DateCtrl ID="dtpToDate" runat="server"  />
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblReasonForAdjustment" runat="server" Text="Adjustment Reason"></asp:Label>
                                        </div>
                                        <div style="width: 26%;" class="ib">
                                            <asp:TextBox ID="txtReasonForAdjustment" runat="server" Width="98%" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlOTAppval" ScrollBars="Auto" runat="server" Style="margin-top: 5px;
                                        height: 400px">
                                        <asp:GridView ID="gvOTRequisitionApproval" runat="server" DataKeyNames="IsGrp,Particulars,Employee,ischecked,employeeunkid,actualstart_time,actualend_time"
                                            AutoGenerateColumns="False" Width="140%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            ShowFooter="false">
                                            <Columns>
                                                <asp:BoundField DataField="Particulars" HeaderText="Particulars" ItemStyle-VerticalAlign="Top"
                                                    FooterText="colhApprovalotreqemp" Visible="false" />
                                                <asp:BoundField DataField="Approver" HeaderText="Approver" ItemStyle-VerticalAlign="Top"
                                                    FooterText="colhApprovalotreqapprover" ItemStyle-Width="320px" />
                                                <asp:BoundField DataField="requestdate" HeaderText="Request Date" ItemStyle-VerticalAlign="Top"
                                                    ItemStyle-Width="120px" FooterText="colhApprovalotrequestdate" />
                                                <asp:BoundField DataField="plannedstart_time" HeaderText="Planned Start Time" ItemStyle-VerticalAlign="Top"
                                                    ItemStyle-Width="180px" FooterText="colhApprovalotreqplannedstart_time" />
                                                <asp:BoundField DataField="plannedend_time" HeaderText="Planned End Time" ItemStyle-VerticalAlign="Top"
                                                    ItemStyle-Width="180px" FooterText="colhApprovalotreqplannedend_time" />
                                                <asp:BoundField DataField="plannedot_hours" HeaderText="Planned OT Hrs" ItemStyle-VerticalAlign="Top"
                                                    ItemStyle-Width="140px" FooterText="colhApprovalotreqplannedOTHrs" />
                                                  <asp:BoundField DataField="request_reason" HeaderText="Request Reason" ItemStyle-VerticalAlign="Top"
                                                    ItemStyle-Width="120px" FooterText="colhApprovalotreqreason" />    
                                                  <asp:BoundField DataField="TotalApprovedHrs" HeaderText="Total Approved Hrs" ItemStyle-VerticalAlign="Top"
                                                   ItemStyle-Width="80px" FooterText="colhApprovalotTotalapprovedhrs" />  
                                                <asp:TemplateField FooterText="colhApprovalActualStartTime" HeaderText="Actual Start Time"
                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div class="row2">
                                                            <div style="width: 32%;" class="ib">
                                                                <asp:DropDownList ID="cboActualStartTimehr" runat="server" Width="45px" AutoPostBack="true" OnSelectedIndexChanged="cboActualStartTimehr_SelectedIndexChanged" />
                                                            </div>
                                                            <div style="width: 10%;" class="ibwm" >
                                                                <asp:Label ID="lblActualStarTimecolon" runat="server" Text=" : " />
                                                            </div>
                                                            <div style="width: 32%;" class="ib">
                                                                <asp:DropDownList ID="cboActualStartTimemin" runat="server" Width="45px" AutoPostBack="true" OnSelectedIndexChanged="cboActualStartTimehr_SelectedIndexChanged" /> 
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="140px" />
                                                    <ItemStyle Width="140px" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField FooterText="colhApprovalActualEndTime" HeaderText="Actual End Time" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div style="width: 32%;" class="ib">
                                                                <asp:DropDownList ID="cboActualEndTimehr" runat="server" Width="45px" AutoPostBack="true" OnSelectedIndexChanged="cboActualEndTimehr_SelectedIndexChanged" />
                                                        </div>
                                                        <div style="width: 10%;" class="ibwm" >
                                                                <asp:Label ID="lblActualEndTimecolon" runat="server" Text=" : " />
                                                         </div>      
                                                         <div style="width: 32%;" class="ib"> 
                                                            <asp:DropDownList ID="cboActualEndTimemin" runat="server" Width="45px" AutoPostBack="true" OnSelectedIndexChanged="cboActualEndTimehr_SelectedIndexChanged" />
                                                         </div>   
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="150px" />
                                                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnOtApprove" runat="server" CssClass="btnDefault" Text="Approve" />
                                        <asp:Button ID="btnOtDisApprove" runat="server" CssClass="btnDefault" Text="Disapprove" />
                                        <asp:Button ID="btnpopupOtClose" runat="server" CssClass="btnDefault" Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    
                     <%--'Pinkal (23-Nov-2019) -- Start
                     'Enhancement NMB - Working On OT Enhancement for NMB.--%>
                    <uc2:Confirmation ID="popupConfirmationForOTCap" Title="" runat="server" Message="" />
                  <%--  'Pinkal (23-Nov-2019) -- End--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
