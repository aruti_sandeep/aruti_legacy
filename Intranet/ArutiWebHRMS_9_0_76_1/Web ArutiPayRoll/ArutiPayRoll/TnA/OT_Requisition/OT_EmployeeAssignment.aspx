﻿<%@ Page  Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" Title ="Employee OT Assignment"
    CodeFile="OT_EmployeeAssignment.aspx.vb" Inherits="TnA_OT_Requisition_OT_EmployeeAssignment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
     
            $("[id*=ChkAll]").live("click", function() {
               debugger;
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function() {
                    if (chkHeader.is(":checked")) {
                        debugger;
                        $(this).attr("checked", "checked");

                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=ChkgvSelect]").live("click", function() {
                debugger;
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkHeader]", grid);
                var row = $(this).closest("tr")[0];

                debugger;
                if (!$(this).is(":checked")) {
                    var row = $(this).closest("tr")[0];
                    chkHeader.removeAttr("checked");
                } else {

                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            //called when key is pressed in textbox
            $(".OnlyNumbers").keypress(function(e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });

        //Pinkal (29-Apr-2020) -- Start
        //Error  -  Solved Error when User Filter anything from list screen and search it was giving error.--%>
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function Searching() {
            if ($('#txtSearch').val().length > 0) {
                $('#<%= dgvEmployee.ClientID %> tbody tr').hide();
                $('#<%= dgvEmployee.ClientID %> tbody tr:first').show();
                $('#<%= dgvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
                var TotalCount = $('#<%= dgvEmployee.ClientID %> tr:visible').length - 1;
                $('#<%= lblEmployeeCount.ClientID %>').html('Employee Count : ' + TotalCount);
            }
            else if ($('#txtSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearch').val('');
            $('#<%= dgvEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearch').focus();
            var TotalCount = $('#<%= dgvEmployee.ClientID %> tr:visible').length - 1;
            $('#<%= lblEmployeeCount.ClientID %>').html('Employee Count : ' + TotalCount);
        }
        //Pinkal (29-Apr-2020) -- End
        
    </script>

    <script>
        function numbersLimit(input) {
            if (input.value < 0) input.value = 0;
            if (input.value > 12) {
                alert("Months cannot be greater than 12");
                input.value = 12;
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee OT Assignment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <asp:Panel ID="pnl_Filter" runat="server">
                                <div id="FilterCriteria" class="panel-default">
                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                        </div>
                                        <div style="text-align: right;">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" Style="font-weight: bold;
                                                font-size: 12px; vertical-align: top; margin-right: 10px" CssClass="lnkhover"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 18%">
                                                    <asp:RadioButton ID="radExpYear" runat="server" Text="Year of Experience" AutoPostBack="true"
                                                        GroupName="Group" />
                                                </td>
                                                <td style="width: 18%">
                                                    <asp:RadioButton ID="radAppointedDate" runat="server" Text="Appointment Date" AutoPostBack="true"
                                                        GroupName="Group" />
                                                </td>
                                                <td style="width: 18%">
                                                    <asp:RadioButton ID="radProbationDate" runat="server" Text="Probation Date" AutoPostBack="true"
                                                        GroupName="Group" />
                                                </td>
                                                <td style="width: 18%">
                                                    <asp:RadioButton ID="radConfirmationDate" runat="server" Text="Confirmation Date"
                                                        AutoPostBack="true" GroupName="Group" />
                                                </td>
                                                <td style="width: 28%">
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width: 100%; margin-top: 10px">
                                            <tr style="width: 100%">
                                                <td style="width: 50%">
                                                    <asp:Panel ID="pnl_FromYear" runat="server">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="lblFromYear" runat="server" Text="From Year"></asp:Label>
                                                                </td>
                                                                <td style="width: 16%">
                                                                    <asp:TextBox ID="txtFromYear" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                                        CssClass="OnlyNumbers"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 28%">
                                                                    <asp:Label ID="lblFromMonth" runat="server" Style="margin-left: 25px" Text="From Month"></asp:Label>
                                                                </td>
                                                                <td style="width: 16%">
                                                                    <asp:TextBox ID="txtFromMonth" runat="server" Style="text-align: right" Text="0"
                                                                        onchange="numbersLimit(this);" CssClass="OnlyNumbers"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:DropDownList ID="cboFromcondition" Style="margin-left: 10px" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:Panel ID="pnl_Date" runat="server">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 16%">
                                                                </td>
                                                                <td style="width: 17%">
                                                                    <asp:Label ID="lblFromDate" runat="server" Style="margin-left: 10px" Text="From Date"></asp:Label>
                                                                </td>
                                                                <td style="width: 27%">
                                                                    <uc1:DateCtrl ID="dtpFromDate" runat="server" />
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                                                </td>
                                                                <td style="width: 27%">
                                                                    <uc1:DateCtrl ID="dtpToDate" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 50%">
                                                    <asp:Panel ID="pnl_ToYear" runat="server">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="lblToYear" runat="server" Text="To Year"></asp:Label>
                                                                </td>
                                                                <td style="width: 16%">
                                                                    <asp:TextBox ID="txtToYear" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                                        CssClass="OnlyNumbers"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 28%">
                                                                    <asp:Label ID="lblToMonth" runat="server" Style="margin-left: 25px" Text="To Month"></asp:Label>
                                                                </td>
                                                                <td style="width: 16%">
                                                                    <asp:TextBox ID="txtToMonth" runat="server" Style="text-align: right" Text="0" onchange="numbersLimit(this);"
                                                                        CssClass="OnlyNumbers"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:DropDownList ID="cboTocondition" Style="margin-left: 10px" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td style="width: 50%">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 16%">
                                                            </td>
                                                            <td style="width: 17%">
                                                                <asp:Label ID="lblGender" runat="server" Style="margin-left: 10px" Text="Gender"></asp:Label>
                                                            </td>
                                                            <td style="width: 27%">
                                                                <asp:DropDownList ID="cboGender" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 13%">
                                                            </td>
                                                            <td style="width: 27%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 35%; padding-left: 8px">
                                             <%--  'Pinkal (29-Apr-2020) -- Start
                                                        'Error  -  Solved Error when User Filter anything from list screen and search it was giving error.
                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"></asp:TextBox>--%>
                                                <input type="text" id="txtSearch" name="txtSearch" placeholder = "Type To Search Text"  maxlength="100" style="height: 25px; font: 100" onkeyup="Searching();" />
                                               <%-- 'Pinkal (29-Apr-2020) -- End--%>
                                            </td>
                                            <td style="width: 3%">
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="lblFrom" runat="server" Style="font-weight: bold" Text="From:"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:Label ID="lblValue" runat="server" Style="font-weight: bold" Text="#Value"></asp:Label>
                                            </td>
                                            <td style="width: 25%; text-align: right">
                                                <asp:Label ID="lblEmployeeCount" runat="server" Style="font-weight: bold; margin-right: 10px"
                                                    Text="#Count"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="5">
                                                <div id="scrollable-container" style="overflow: auto; vertical-align: top; max-height: 400px;
                                                    margin-bottom: 10px" onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvEmployee" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" DataKeyField = "employeeunkid"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <ItemStyle CssClass="griviewitem" />
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                <%--Pinkal (10-Jan-2020) -- Start
                                                                        Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                    <%--<asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAll_CheckedChanged" />--%>
                                                                    <asp:CheckBox ID="ChkAll" runat="server" />
                                                                    <%--Pinkal (10-Jan-2020) -- End--%>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <%--Pinkal (10-Jan-2020) -- Start
                                                                        Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                    <%--<asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged" />--%>
                                                                    <asp:CheckBox ID="ChkgvSelect" runat="server"/>
                                                                    <%--Pinkal (10-Jan-2020) -- End--%>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="30px" />
                                                                <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="30px" />
                                                            </asp:TemplateColumn>

                                                            <asp:BoundColumn DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgColhEmpCode" />
                                                            <asp:BoundColumn DataField="Employee Name" HeaderText="Employee" ReadOnly="true" FooterText="dgColhEmployee" />
                                                            <asp:BoundColumn DataField="Appointed Date" HeaderText="Appointment Date" ReadOnly="true"
                                                                FooterText="dgcolhAppointedDate" ItemStyle-Width = "130px"  />
                                                            <asp:BoundColumn DataField="Cofirmation Date" HeaderText="Confirmation Date" ReadOnly="true"
                                                                FooterText="dgcolhConfirmationDate" ItemStyle-Width = "130px"  />
                                                            <asp:BoundColumn DataField="Probation From Date" HeaderText="Probation From Date"
                                                                ReadOnly="true" FooterText="dgcolhProbationFromDate" ItemStyle-Width = "130px" />
                                                            <asp:BoundColumn DataField="Probation To Date" HeaderText="Probation To Date" ReadOnly="true"
                                                                FooterText="dgcolhProbationToDate" ItemStyle-Width = "130px"  />
                                                            <asp:BoundColumn DataField="job" HeaderText="Job Title" ReadOnly="true" FooterText="dgcolhjob">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ischecked" HeaderText="" Visible="false" ReadOnly="true"
                                                                FooterText="objSelect" />
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="" Visible="false" ReadOnly="true" />
                                                            <%-- 'Pinkal (29-Apr-2020) -- End--%>
                                                        </Columns>
                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--<tr style="width: 100%">
                                            <td style="width: 100%" colspan="5">
                                                <div id="Div2" class="panel-default">
                                                    <div id="Div4" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblOTInfo" runat="server" Text="OT Information"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div5" class="panel-body-default">
                                                        <table style="width: 50%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 18%">
                                                                    <asp:Label ID="lblStartDate" runat="server" Style="margin-left: 20px" Text="Start Date"></asp:Label>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <uc1:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="false" />
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:Label ID="lblStopDate" runat="server" Text="Stop Date"></asp:Label>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <uc1:DateCtrl ID="dtpStopDate" runat="server" AutoPostBack="false" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>--%>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
