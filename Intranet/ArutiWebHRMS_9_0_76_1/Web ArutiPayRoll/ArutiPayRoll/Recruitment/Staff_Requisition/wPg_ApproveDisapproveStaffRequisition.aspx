﻿<%@ Page MasterPageFile="~/home.master" Language="VB" AutoEventWireup="false" CodeFile="wPg_ApproveDisapproveStaffRequisition.aspx.vb"
    Inherits="Recruitment_Staff_Requisition_wPg_ApproveDisapproveStaffRequisition"
    Title="Approve / Disapprove Staff Requisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/Delete_Reason.ascx" TagName="DeleteReason" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

  function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }
       
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Approve / Disapprove Staff Requisition"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 60%">
                                            <div id="Div1" class="panel-default">
                                                <div id="Div2" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="gbStaffRequisitionList" runat="server" Text="Staff Requisition Information"></asp:Label>
                                                    </div>
                                                </div>
                                                <div id="Div3" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblFormNo" runat="server" Text="Form No." Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtFormNo" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblStatus" runat="server" Text="Requisition Type" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtStatus" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblAllocation" runat="server" Text="Requisition By" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtAllocation" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblName" runat="server" Text="Name" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtName" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblClassGroup" runat="server" Text="Class Group" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtClassGroup" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblClass" runat="server" Text="Class" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtClass" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblJob" runat="server" Text="Job Title" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtJob" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2" width="50%">
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblJobDesc" runat="server" Text="Job Description & Indicators" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%" colspan="3">
                                                                <asp:TextBox ID="txtJobDesc" runat="server" Width="90%" ReadOnly="true" TextMode="MultiLine"
                                                                    Rows="2"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td width="20%">
                                                            </td>
                                                            <td width="80%" colspan="3">
                                                                <asp:LinkButton ID="lnkViewJobDescription" runat="server" Text="View Job Description"
                                                                    CssClass="lnkhover" Style="color: Blue; vertical-align: top; float: left;"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%" colspan="4">
                                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee to be replaced" Width="100%"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%" colspan="4">
                                                                <asp:Panel ID="dgv_dgvEmployeeList" runat="server" Width="100%" ScrollBars="Auto"
                                                                    Height="350px">
                                                                    <asp:DataGrid ID="dgvEmployeeList" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        Width="99%">
                                                                        <Columns>
                                                                            <asp:BoundColumn DataField="employeeunkid" Visible="false" FooterText="objcolhEmpID">
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="colhEmpCode">
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="name" HeaderText="name" FooterText="colhEmpName"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="grade" HeaderText="Grade" FooterText="colhGrade"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="gradelevel" HeaderText="Salary Band" FooterText="colhSalaryBand">
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="actionreasonunkid" Visible="false" FooterText="objcolhActionReasonId">
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblLeavingReason" runat="server" Text="Leaving Reason" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%" colspan="3">
                                                                <asp:TextBox ID="txtLeavingReason" runat="server" Width="90%" ReadOnly="true" Height="18px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblAddStaffReason" runat="server" Text="Additional Staff Reason" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%" colspan="3">
                                                                <asp:TextBox ID="txtAddStaffReason" runat="server" Width="90%" ReadOnly="true" TextMode="MultiLine"
                                                                    Rows="2"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblNoofPosition" runat="server" Text="No. of Position" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtNoofPosition" runat="server" Width="90%" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblWorkStartDate" runat="server" Text="Date to Start Work" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtWorkStartDate" runat="server" Width="90%" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 40%; vertical-align: top">
                                            <div id="Div4" class="panel-default">
                                                <div id="Div5" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="gbStaffReqApprovalnfo" runat="server" Text="Staff Requisition Approval Information"></asp:Label>
                                                    </div>
                                                </div>
                                                <div id="Div6" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%;">
                                                            <td colspan="2" style="width: 100; padding-bottom: 10px; border-bottom: 2px solid #DDD">
                                                                <table style="width: 100%; font-weight: bold; border: 2px solid #DDD; border-collapse: collapse"
                                                                    border="1" bordercolor="#DDD" width="100%">
                                                                    <tr style="width: 100%">
                                                                        <td class="grpheader" style="border-radius: 0px; width: 100%;" colspan="2" height="20px">
                                                                            <asp:Label ID="lblJobHeadCountInfo" runat="server" Text="Job Head Count Information"
                                                                                Width="100%" Font-Size="Small" ForeColor="White"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 60%">
                                                                            <asp:Label ID="lblPlanned" runat="server" Text="Planned Head Counts" Width="100%"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%; text-align: right; padding-right: 10px">
                                                                            <asp:Label ID="objlblPlanned" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 60%;">
                                                                            <asp:Label ID="lblAvailable" runat="server" Text="Available Head Counts"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%; text-align: right; padding-right: 10px">
                                                                            <asp:Label ID="objlblAvailable" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 60%;">
                                                                            <asp:Label ID="lblVariation" runat="server" Text="Variations" Width="100%"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%; text-align: right; padding-right: 10px">
                                                                            <asp:Label ID="objlblVariation" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%;">
                                                            <td colspan="2" style="width: 100%; padding-bottom: 10px; padding-top: 10px; border-bottom: 2px solid #DDD"">
                                                                <table style="font-weight: bold; border: 2px solid #DDD; border-collapse: collapse"
                                                                    border="1" width="100%">
                                                                    <tr style="width: 100%">
                                                                        <td class="grpheader" style="border-radius: 0px; width: 100%" colspan="2" height="20px">
                                                                            <asp:Label ID="lblApproverInfo" runat="server" Text="Approver Information" Width="100%"
                                                                                Font-Size="Small" ForeColor="White"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 40%">
                                                                            <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                                                        </td>
                                                                        <td width="60%">
                                                                            <asp:Label ID="objApproverName" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 40%;">
                                                                            <asp:Label ID="lblApproverLevel" runat="server" Text="Approver Level"></asp:Label>
                                                                        </td>
                                                                        <td width="60%">
                                                                            <asp:Label ID="objApproverLevelVal" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 40%;">
                                                                            <asp:Label ID="lblLevelPriority" runat="server" Text="Level Priority"></asp:Label>
                                                                        </td>
                                                                        <td width="60%">
                                                                            <asp:Label ID="objLevelPriorityVal" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%; padding-top: 10px">
                                                                <asp:Label ID="lblApprovalstatus" runat="server" Text="Status" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%; padding-top: 10px">
                                                                <asp:DropDownList ID="cboStatus" runat="server" Width="100%" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks" Width="100%"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%">
                                                                <asp:TextBox ID="txtRemarks" runat="server" Width="97%" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="Div7" class="panel-default">
                                                <div id="Div8" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="Label1" runat="server" Text="Attach Document"></asp:Label>
                                                    </div>
                                                </div>
                                                <div id="Div9" class="panel-body-default">
                                                    <asp:Panel ID="pnl_staffRequisitionAttachment" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                        <asp:DataGrid ID="dgvstaffRequisitionAttachment" runat="server" AllowPaging="false"
                                                            AutoGenerateColumns="False" Width="98%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                            ItemStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcohDownload" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" CommandArgument='<%#Container.ItemIndex %>'
                                                                            ToolTip="Download" OnClick="lnkDownloadAttachment_Click">
                                                                                                            <i class="fa fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhfilepath" DataField="filepath" FooterText="objcolhfilepath"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhlocalpath" DataField="localpath" FooterText="objcolhlocalpath"
                                                                    Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                    <div class="row2" style="text-align: center">
                                                        <asp:Label ID="lblEmptyAttachment" runat="server" ForeColor="Red" Text="No Document Attach"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btnDefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc4:Confirmation ID="ApproveDisapprove" runat="server" Message="" Title="Confirmation" />
                    <uc6:DeleteReason ID="DeleteReason" runat="server" Title="Void Reason" ReasonType="STAFF_REQUISITION" />
                </ContentTemplate>
                <Triggers>
                <asp:PostBackTrigger ControlID="dgvstaffRequisitionAttachment" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
