﻿<%@ Page Title="Interview Analysis List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgInterviewAnalysisList.aspx.vb" Inherits="wPgInterviewAnalysisList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirm" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popOperation" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnpopClose" PopupControlID="pnlOperation" TargetControlID="btnHide"
                    Drag="true" PopupDragHandleControlID="pnlOperation">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlOperation" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;" DefaultButton="btnpopClose">
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="lblCaption" runat="server" Text="Approve/Diapprove Applicant Eligiblilty"
                                CssClass="form-label" />
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height:500px;">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 200px;">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Width="100%">
                                                <asp:GridView ID="dgvViewData" runat="server" AllowPaging="false" Width="99%" HeaderStyle-Font-Bold="false"
                                                    ShowFooter="false" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="interview_type" HeaderText="Interview Type" FooterText="dgcolhInterviewType_AD" />
                                                        <asp:BoundField DataField="analysis_date" HeaderText="Analysis Date" FooterText="dgcolhAnalysisDate_AD" />
                                                        <asp:BoundField DataField="reviewer" HeaderText="Reviewer" FooterText="dgcolhReviewer_AD" />
                                                        <asp:BoundField DataField="score" HeaderText="Score" FooterText="dgcolhScore_AD" />
                                                        <asp:BoundField DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark_AD" />
                                                        <asp:BoundField DataField="analysisunkid" HeaderText="analysisunkid" Visible="false" />
                                                        <asp:BoundField DataField="appbatchscheduletranunkid" HeaderText="appbatchscheduletranunkid"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="applicantunkid" HeaderText="applicantunkid" Visible="false" />
                                                        <asp:BoundField DataField="grp_id" HeaderText="grp_id" Visible="false" />
                                                        <asp:BoundField DataField="sort_id" HeaderText="sort_id" Visible="false" />
                                                        <asp:BoundField DataField="vacancyunkid" HeaderText="vacancyunkid" Visible="false" />
                                                        <asp:BoundField DataField="is_grp" HeaderText="is_grp" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label" />
                                        <uc2:DateCtrl ID="dtApprDate" runat="server" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btn btn-default" />
                                <asp:Button ID="btnpopClose" runat="server" Text="Cancel" CssClass="btn btn-default" />
                                <asp:HiddenField ID="btnHide" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc4:Confirm ID="popConfirm1" runat="server" Title="" Message="" />
                <uc4:Confirm ID="popConfirm2" runat="server" Title="" Message="" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Interview Analysis List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVacanyType" runat="server" Text="Vacancy Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboVacType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVacancy" runat="server" Text="Vacancy" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboVacancy" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboBatch" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResult" runat="server" Text="Result" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboResult" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplicant" runat="server" Text="Applicant" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboApplicant" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInterviewType" runat="server" Text="Interview Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboInterviewType" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAnalysisDateFrom" runat="server" Text="Analysis Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl runat="server" ID="dtFromDate" AutoPostBack="true" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbAnalysisDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl AutoPostBack="false" runat="server" ID="dtToDate" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                            <asp:CheckBox ID="chkEligible" runat="server" Text="Eligible" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkNotEligible" runat="server" Text="Not Eligible" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnEligible" runat="server" CssClass="btn btn-default" Text="Eligible Operation" />
                                <asp:Button ID="btnNEligible" runat="server" CssClass="btn btn-default" Text="Not Eligible Operation" />
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px;">
                                            <asp:Panel ID="pnlGridDate" runat="server" ScrollBars="Auto" Width="100%">
                                                <asp:GridView ID="dgvData" runat="server" AllowPaging="false" Width="99%" CellPadding="3"
                                                    ShowFooter="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>'
                                                                    AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="interview_type" HeaderText="Interview Type" FooterText="dgcolhInterviewType" />
                                                        <asp:BoundField DataField="analysis_date" HeaderText="Analysis Date" FooterText="dgcolhAnalysisDate" />
                                                        <asp:BoundField DataField="reviewer" HeaderText="Reviewer" FooterText="dgcolhReviewer" />
                                                        <asp:BoundField DataField="score" HeaderText="Score" FooterText="dgcolhScore" />
                                                        <asp:BoundField DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                                        <asp:BoundField DataField="analysisunkid" HeaderText="analysisunkid" Visible="false" />
                                                        <asp:BoundField DataField="appbatchscheduletranunkid" HeaderText="appbatchscheduletranunkid"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="applicantunkid" HeaderText="applicantunkid" Visible="false" />
                                                        <asp:BoundField DataField="grp_id" HeaderText="grp_id" Visible="false" />
                                                        <asp:BoundField DataField="sort_id" HeaderText="sort_id" Visible="false" />
                                                        <asp:BoundField DataField="vacancyunkid" HeaderText="vacancyunkid" Visible="false" />
                                                        <asp:BoundField DataField="is_grp" HeaderText="is_grp" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
