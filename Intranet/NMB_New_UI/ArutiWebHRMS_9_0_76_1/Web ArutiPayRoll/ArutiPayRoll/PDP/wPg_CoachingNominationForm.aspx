﻿<%@ Page Title="Coaching Nomination Form" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_CoachingNominationForm.aspx.vb" Inherits="PDP_wPg_CoachingNominationForm" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Coaching Nomination Form" CssClass="form-label"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <asp:Panel ID="pnlCoachee" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblProposedCoacheeDetails" runat="server" Text="Proposed Coachee Details"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix d--f ai--c">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
                                                        <asp:Image ID="imgCoacheeProfilePic" runat="server" Width="150px" Height="150px"
                                                            Style="border-radius: 50%" />
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoacheeEmployee" runat="server" Text="Name of the Coachee" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="cboCoacheeEmployee" runat="server"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoacheeJob" runat="server" Text="Designation" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtCoacheeJob" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoacheeWorkStation" runat="server" Text="Work Station" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtCoacheeWorkStation" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoacheeGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtCoacheeGender" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoacheeDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtCoacheeDepartment" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                         <div class="divider">
                                                        </div>
                                                        <asp:Panel ID="pnlCoacheeSpecificWorkProcess" runat="server">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblCoacheeSpecificWorkProcess" runat="server" Text="Specific Work Process"
                                                                        CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtCoacheeSpecificWorkProcess" runat="server" CssClass="form-control"
                                                                                TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlCoach" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblProposedCoachDetails" runat="server" Text="Proposed Coach Details"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix  d--f ai--c">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
                                                        <asp:Image ID="imgProposedCoachProfilePic" runat="server" Width="150px" Height="150px"
                                                            Style="border-radius: 50%" />
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoachEmployee" runat="server" Text="Name of the Coach" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="cboCoachEmployee" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoachJob" runat="server" Text="Designation" CssClass="form-label"></asp:Label>
                                                                <%--<div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoachJob" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>--%>
                                                                <asp:Label ID="txtCoachJob" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoachWorkStation" runat="server" Text="Work Station" CssClass="form-label"></asp:Label>
                                                                <%--<div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoachWorkStation" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>--%>
                                                                <asp:Label ID="txtCoachWorkStation" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoachGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                                               <%-- <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoachGender" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>--%>
                                                                <asp:Label ID="txtCoachGender" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                         <div class="divider">
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoachSpecificWorkProcess" runat="server" Text="Specific Work Process"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoachSpecificWorkProcess" runat="server" CssClass="form-control"
                                                                            TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlNewCoach" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblNewCoachDetails" runat="server" Text="Proposed Coach Details" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix d--f ai--c">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
                                                        <asp:Image ID="imgNewCoachProfilePic" runat="server" Width="150px" Height="150px"
                                                            Style="border-radius: 50%" />
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNewCoachEmployee" runat="server" Text="Name of the Coach" CssClass="form-label"></asp:Label>
                                                                <asp:DropDownList data-live-search="true" ID="cboNewCoachEmployee" runat="server"
                                                                    AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNewCoachWorkStation" runat="server" Text="Work Station" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtNewCoachWorkStation" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNewCoachGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtNewCoachGender" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="divider">
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNewCoachSpecificWorkProcess" runat="server" Text="Specific Work Process"
                                                                    CssClass="form-label"></asp:Label>
                                                                <asp:TextBox ID="txtNewCoachSpecificWorkProcess" runat="server" CssClass="form-control"
                                                                    TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblNewCoachReasonsForChangingCoach" runat="server" Text="Reasons For Changing Coach"
                                                                    CssClass="form-label"></asp:Label>
                                                                <asp:TextBox ID="txtNewCoachReasonsForChangingCoach" runat="server" CssClass="form-control"
                                                                    TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlApprovalInfo" runat="server" Visible="false">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblApprovalData" runat="server" Text="Approval Info" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRole" runat="server" Text="Role" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpRole" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtApproverLevel" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprRejectRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtApprRejectRemark" runat="server" TextMode="MultiLine" Rows="2"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnDisapprove" runat="server" Text="Reject" class="btn btn-default"
                                Visible="false" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit for Approval" CssClass="btn btn-default" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <cnf:Confirmation ID="cnfApprovDisapprove" runat="server" Title="Aruti" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
