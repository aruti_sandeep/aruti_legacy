﻿<%@ Page Title="Coaching Nomination Form List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_CoachingNominationFormList.aspx.vb" Inherits="PDP_wPg_CoachingNominationFormList" %>
    
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Coaching Nomination Form List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCoachEmployee" runat="server" Text="Name of the Coach" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCoachEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCoacheeEmployee" runat="server" Text="Name of the Coachee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCoacheeEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Approval Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:GridView ID="dgvCoachingNominationList" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered" DataKeyNames= "coachingnominationunkid,statusunkid,issubmit_approval,allowreplacement,formtypeid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Select" ToolTip="Edit"
                                                                    OnClick="lnkEdit_Click">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" OnClick="lnkDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnReplacement">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgReplacement" runat="server" ToolTip="Replacement" OnClick="lnkReplacement_Click">
                                                                        <i class="fas fa-retweet" style="font-size:20px"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnCoachingFormReport">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgCoachingFormReport" runat="server" ToolTip="Coaching Form Report" OnClick="lnkCoachingFormReport_Click">
                                                                        <i class="fas fa-print style="font-size:15px; color:Black" ></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="refno" HeaderText="Ref. No" FooterText="dgcolhRefNo" />
                                                    <asp:BoundField DataField="application_date" HeaderText="Application Date" FooterText="dgcolhApplicationDate" />
                                                    <asp:BoundField DataField="Coach" HeaderText="Coach" ReadOnly="true" FooterText="dgcolhCoach" />
                                                    <%--<asp:BoundField DataField="Coachee" HeaderText="Coachee" ReadOnly="true" FooterText="dgcolhCoachee" />--%>
                                                    <asp:BoundField DataField="FormType" HeaderText="Form Type" ReadOnly="true" FooterText="dgcolFormType" />
                                                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
