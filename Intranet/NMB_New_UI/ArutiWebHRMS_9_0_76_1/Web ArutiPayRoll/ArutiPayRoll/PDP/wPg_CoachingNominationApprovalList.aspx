﻿<%@ Page Title="Coaching Form Approval List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_CoachingNominationApprovalList.aspx.vb"
    Inherits="PDP_wPg_CoachingNominationApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Coaching Form Approval List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCoacheeEmployee" runat="server" Text="Coachee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCoacheeEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRole" runat="server" Text="Role" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRole" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Approval Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Checked="true"
                                            Font-Bold="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px;">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="gvCoachingApprovalList" runat="server" AutoGenerateColumns="false"
                                                    AllowPaging="false" Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="pendingcoachingtranunkid,IsGrp,mapuserunkid,statusunkid,coachingnominationunkid,approverunkid">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkChangeStatus" runat="server" ToolTip="Change Status" OnClick="lnkChangeStatus_Click"
                                                                        CommandArgument='<%#Eval("coachingnominationunkid")%>'>
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="Employee" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />--%>
                                                        <asp:BoundField DataField="application_date" HeaderText="Application Date" ReadOnly="true"
                                                            FooterText="colhapplication_date" />
                                                        <asp:BoundField DataField="Coach" HeaderText="Coach" ReadOnly="true" FooterText="colhCoach" />
                                                        <asp:BoundField DataField="FormType" HeaderText="Form Type" ReadOnly="true" FooterText="colhFormType" />
                                                        <asp:BoundField DataField="ApproverName" HeaderText="Approver Name" ReadOnly="true"
                                                            FooterText="colhApproverName" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="colhStatus" />
                                                        <asp:BoundField DataField="Isgrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close"
                                    Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
