﻿Imports Aruti.Data
Imports System.Data
Imports System.Drawing

Partial Class PDP_wPg_CoachingNominationFormList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmCoachingNominationFormList"
    Private mintCoachingNominationunkid As Integer
    Private mstrConfirmationAction As String = ""

#End Region

#Region " Page Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()
                Call FillCombo()
                SetVisibility()
            Else
                mintCoachingNominationunkid = CInt(Me.ViewState("mintCoachingNominationunkid"))
                mstrConfirmationAction = Me.ViewState("mstrConfirmationAction")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintCoachingNominationunkid") = mintCoachingNominationunkid
            Me.ViewState("mstrConfirmationAction") = mstrConfirmationAction
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCoachingNominationForm As New clsCoaching_Nomination_Form
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    CStr(Session("UserAccessModeSetting")), _
                                                    True, CBool(Session("IsIncludeInactiveEmp")), _
                                                    "EmpList")

            Dim dRow As DataRow = dsList.Tables(0).NewRow
            dRow.Item("employeeunkid") = 0
            dRow.Item("EmpCodeName") = "Select"
            dsList.Tables(0).Rows.InsertAt(dRow, 0)

            With cboCoachEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboCoacheeEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objCoachingNominationForm.getStatusComboList("List", True)
            With cboStatus
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objCoachingNominationForm = Nothing
        End Try
    End Sub

    Private Sub FillList(Optional ByVal blnBindGridView As Boolean = True)
        Dim objCoachingNominationForm As New clsCoaching_Nomination_Form
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Try
            If CBool(Session("AllowToViewCoachingForm")) = False Then Exit Sub

            If CInt(cboCoachEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND pdpcoaching_nomination_form.coach_employeeunkid = " & CInt(cboCoachEmployee.SelectedValue) & " "
            End If

            If CInt(cboCoacheeEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND pdpcoaching_nomination_form.coachee_employeeunkid = " & CInt(cboCoacheeEmployee.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND pdpcoaching_nomination_form.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsList = objCoachingNominationForm.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), _
                                                    True, CBool(Session("IsIncludeInactiveEmp")), "Coaching", StrSearching, , True)

            Dim dtEmployeeView As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "coachee_employeeunkid", "coachee")
            If dsList.Tables(0).Columns.Contains("IsGrp") = False Then
                Dim dtCol As New DataColumn
                dtCol = New DataColumn
                dtCol.ColumnName = "IsGrp"
                dtCol.Caption = "IsGrp"
                dtCol.DataType = System.Type.GetType("System.Boolean")
                dtCol.DefaultValue = False
                dsList.Tables(0).Columns.Add(dtCol)

            End If
            Dim dRow As DataRow = Nothing
            For Each drEmployee As DataRow In dtEmployeeView.Rows
                Dim drRow() As DataRow = dsList.Tables(0).Select("coachee_employeeunkid = " & CInt(drEmployee.Item("coachee_employeeunkid")) & " ")
                If drRow.Length > 0 Then
                    dRow = dsList.Tables(0).NewRow()
                    dRow.Item("coachingnominationunkid") = -1
                    dRow.Item("coachee_employeeunkid") = CInt(drEmployee.Item("coachee_employeeunkid"))
                    dRow.Item("coachee") = CStr(drEmployee.Item("coachee"))
                    dRow.Item("IsGrp") = 1
                    dsList.Tables(0).Rows.Add(dRow)
                End If

            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables(0), "", "coachee_employeeunkid  , coachingnominationunkid  ", DataViewRowState.CurrentRows).ToTable.Copy

            If blnBindGridView = True Then
                dgvCoachingNominationList.AutoGenerateColumns = False
                dgvCoachingNominationList.DataSource = dtTable
                dgvCoachingNominationList.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingNominationForm = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByRef objNomination As clsCoaching_Nomination_Form)
        Try
            objNomination._CoachingNominationunkid = mintCoachingNominationunkid
            objNomination._AuditUserId = CInt(Session("UserId"))
            objNomination._ClientIP = CStr(Session("IP_ADD"))
            objNomination._FormName = mstrModuleName
            objNomination._IsWeb = True
            objNomination._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToFillCoachingForm"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview's Events"

    Protected Sub dgvCoachingNominationList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvCoachingNominationList.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then

                        Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                        lnkEdit.Visible = False

                        Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                        lnkDelete.Visible = False

                        Dim lnkReplacement As LinkButton = TryCast(e.Row.FindControl("ImgReplacement"), LinkButton)
                        lnkReplacement.Visible = False

                        Dim lnkCoachingFormReport As LinkButton = TryCast(e.Row.FindControl("ImgCoachingFormReport"), LinkButton)
                        lnkCoachingFormReport.Visible = False


                        e.Row.Cells(4).Text = DataBinder.Eval(e.Row.DataItem, "Coachee").ToString
                        e.Row.Cells(4).ColumnSpan = e.Row.Cells.Count - 1
                        e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                        e.Row.Font.Bold = True

                        For i As Integer = 5 To e.Row.Cells.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                    Else
                        If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "dgcolhApplicationDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "dgcolhApplicationDate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "dgcolhApplicationDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "dgcolhApplicationDate", False, True)).Text).Date.ToShortDateString
                        End If

                        Dim dgCoachingNomination As GridView = CType(sender, GridView)
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                        Dim lnkreplacement As LinkButton = TryCast(e.Row.FindControl("ImgReplacement"), LinkButton)
                        lnkedit.Visible = False
                        lnkdelete.Visible = False
                        lnkreplacement.Visible = False
                        lnkedit.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Edit")
                        lnkdelete.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Delete")
                        lnkreplacement.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Replacement")
                        If CBool(dgCoachingNomination.DataKeys(e.Row.RowIndex)("issubmit_approval").ToString) = False Then
                            lnkedit.Visible = CBool(Session("AllowToEditCoachingForm"))
                            lnkdelete.Visible = CBool(Session("AllowToDeleteCoachingForm"))
                        End If

                        If dgCoachingNomination.DataKeys(e.Row.RowIndex)("allowreplacement") IsNot Nothing AndAlso CBool(dgCoachingNomination.DataKeys(e.Row.RowIndex)("allowreplacement").ToString) = True _
                           AndAlso CInt(dgCoachingNomination.DataKeys(e.Row.RowIndex)("statusunkid").ToString) = enPDPCoachingApprovalStatus.APPROVED Then
                            lnkreplacement.Visible = CBool(Session("AllowToFillReplacementForm"))
                        End If

                        'Hemant (10 Nov 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1461 - Provide a different color code for coach nomination form icon and coaches replacement form icon
                        Dim lnkcoachingformreport As LinkButton = TryCast(e.Row.FindControl("ImgCoachingFormReport"), LinkButton)
                        If CInt(dgCoachingNomination.DataKeys(e.Row.RowIndex)("formtypeid").ToString) = 1 Then
                            lnkcoachingformreport.Text = "<i class=""fas fa-print"" style=""font-size:15px; color:Green""></i >"
                        Else
                            lnkcoachingformreport.Text = "<i class=""fas fa-print"" style=""font-size:15px; color:Blue""></i >"
                        End If
                        'Hemant (10 Nov 2023) -- End

                        
                    End If

                    
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button's Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("FormTypeId") = enPDPCoachingFormType.NOMINANTION_FORM
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "PDP/wPg_CoachingNominationForm.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try

            cboCoachEmployee.SelectedIndex = 0
            cboCoacheeEmployee.SelectedIndex = 0
            cboStatus.SelectedValue = "1"

            dgvCoachingNominationList.DataSource = New List(Of String)
            dgvCoachingNominationList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link Button's Events"
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            Dim dgCoachingNomination As GridView = TryCast(row.NamingContainer, GridView)
            mintCoachingNominationunkid = CInt(dgCoachingNomination.DataKeys(row.RowIndex)("coachingnominationunkid"))

            If CInt(dgCoachingNomination.DataKeys(row.RowIndex)("statusunkid")) <> enPDPCoachingApprovalStatus.PENDING Then
                DisplayMessage.DisplayMessage("You cannot Edit this Coaching Nomination. Reason: This Coaching Nomination is in " & dgCoachingNomination.DataKeys(row.RowIndex)("status") & " status.", Me)
                Exit Sub
            End If

            If CInt(dgCoachingNomination.DataKeys(row.RowIndex)("statusunkid")) = enPDPCoachingApprovalStatus.PENDING Then
                'Dim objApprovalProcessTran As New clstrainingapproval_process_tran
                'If CBool(objApprovalProcessTran.IsPendingTrainingRequest(mintCoachingNominationunkid)) = False Then
                '    DisplayMessage.DisplayMessage("You cannot Edit this Coaching Nomination. Reason: It is already in approval process.", Me)
                '    Exit Sub
                'End If
            End If

            Session("CoachingNominationunkid") = mintCoachingNominationunkid
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "PDP/wPg_CoachingNominationForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            Dim dgvCoachingNominationList As GridView = TryCast(row.NamingContainer, GridView)
            mintCoachingNominationunkid = CInt(dgvCoachingNominationList.DataKeys(row.RowIndex)("coachingnominationunkid"))

            If CInt(dgvCoachingNominationList.DataKeys(row.RowIndex)("statusunkid")) <> enTrainingRequestStatus.PENDING Then
                DisplayMessage.DisplayMessage("You cannot Delete this Coaching Nomination. Reason: This Coaching Nomination is in " & dgvCoachingNominationList.DataKeys(row.RowIndex)("status") & " status.", Me)
                Exit Sub
            End If

            If CInt(dgvCoachingNominationList.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.PENDING Then
                'Dim objApprovalProcessTran As New clstrainingapproval_process_tran
                'If CBool(objApprovalProcessTran.IsPendingTrainingRequest(mintCoachingNominationunkid)) = False Then
                '    DisplayMessage.DisplayMessage("You cannot Delete this Training Request. Reason: It is already in approval process.", Me)
                '    Exit Sub
                'End If
            End If

            mstrConfirmationAction = "delNomination"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Are you sure you want to delete ?")
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub lnkReplacement_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            Dim dgCoachingNomination As GridView = TryCast(row.NamingContainer, GridView)
            Dim intPrev_CoachingNominationunkid = CInt(dgCoachingNomination.DataKeys(row.RowIndex)("coachingnominationunkid"))

            Session("FormTypeId") = enPDPCoachingFormType.REPLACEMENT_FORM
            Session("CoachingNominationunkid") = Nothing
            Session("Prev_CoachingNominationunkid") = intPrev_CoachingNominationunkid
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "PDP/wPg_CoachingNominationForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkCoachingFormReport_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objCoachingNominationForm As New clsCoaching_Nomination_Form
        Try
            Dim objCoachingNominationFormRpt As New ArutiReports.clsCoachingNominationFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            Dim lnkcoachingformreport As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkcoachingformreport).NamingContainer, GridViewRow)
            Dim dgvCoachingNominationList As GridView = TryCast(row.NamingContainer, GridView)
            mintCoachingNominationunkid = CInt(dgvCoachingNominationList.DataKeys(row.RowIndex)("coachingnominationunkid"))

            objCoachingNominationForm._CoachingNominationunkid = mintCoachingNominationunkid
            objCoachingNominationFormRpt._CoachingNominationunkid = mintCoachingNominationunkid
            objCoachingNominationFormRpt._FormTypeId = CInt(objCoachingNominationForm._FormTypeId)
            objCoachingNominationFormRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))

            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                Dim strUserName As String = String.Empty
                objUser._Userunkid = CInt(Session("UserId"))
                strUserName = objUser._Firstname & " " & objUser._Lastname
                If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
                objCoachingNominationFormRpt._User = strUserName
                objUser = Nothing
            End If

            objCoachingNominationFormRpt.generateReportNew(CStr(Session("Database_Name")), _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             Session("ExportReportPath").ToString, _
                                                             CBool(Session("OpenAfterExport")), _
                                                             0, enPrintAction.None, enExportAction.None, _
                                                             CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objCoachingNominationFormRpt._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            objCoachingNominationFormRpt = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingNominationForm = Nothing
        End Try
    End Sub
#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELNOMINATION"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Enter the Reason for deleting this Coaching Nomination")
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Delete Reason"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Dim objCoachingNominationForm As New clsCoaching_Nomination_Form
        Try
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELNOMINATION"
                    SetValue(objCoachingNominationForm)
                    objCoachingNominationForm._Isvoid = True
                    objCoachingNominationForm._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objCoachingNominationForm._Voiduserunkid = CInt(Session("UserId"))
                    objCoachingNominationForm._Voidreason = delReason.Reason
                    objCoachingNominationForm._Userunkid = CInt(Session("UserId"))

                    blnFlag = objCoachingNominationForm.Delete(mintCoachingNominationunkid)
                    If blnFlag = False AndAlso objCoachingNominationForm._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objCoachingNominationForm._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Coaching Nomination deleted successfully"), Me)
                        mintCoachingNominationunkid = 0
                        FillList()
                    End If

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingNominationForm = Nothing
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoachEmployee.ID, Me.lblCoachEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeEmployee.ID, Me.lblCoacheeEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNew.ID, Me.btnNew.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.BtnSearch.ID, Me.BtnSearch.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.BtnReset.ID, Me.BtnReset.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvCoachingNominationList.Columns(4).FooterText, Me.dgvCoachingNominationList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvCoachingNominationList.Columns(5).FooterText, Me.dgvCoachingNominationList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvCoachingNominationList.Columns(6).FooterText, Me.dgvCoachingNominationList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvCoachingNominationList.Columns(7).FooterText, Me.dgvCoachingNominationList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvCoachingNominationList.Columns(8).FooterText, Me.dgvCoachingNominationList.Columns(8).HeaderText)
            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Me.lblCoachEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoachEmployee.ID, Me.lblCoachEmployee.Text)
            Me.lblCoacheeEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeEmployee.ID, Me.lblCoacheeEmployee.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text.Replace("&", ""))
            Me.BtnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnSearch.ID, Me.BtnSearch.Text.Replace("&", ""))
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

            Me.dgvCoachingNominationList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvCoachingNominationList.Columns(4).FooterText, Me.dgvCoachingNominationList.Columns(4).HeaderText)
            Me.dgvCoachingNominationList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvCoachingNominationList.Columns(5).FooterText, Me.dgvCoachingNominationList.Columns(5).HeaderText)
            Me.dgvCoachingNominationList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvCoachingNominationList.Columns(6).FooterText, Me.dgvCoachingNominationList.Columns(6).HeaderText)
            Me.dgvCoachingNominationList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvCoachingNominationList.Columns(7).FooterText, Me.dgvCoachingNominationList.Columns(7).HeaderText)
            Me.dgvCoachingNominationList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvCoachingNominationList.Columns(8).FooterText, Me.dgvCoachingNominationList.Columns(8).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Coaching Nomination deleted successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Enter the Reason for deleting this Coaching Nomination")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Edit")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Delete")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Replacement")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Are you sure you want to delete ?")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
