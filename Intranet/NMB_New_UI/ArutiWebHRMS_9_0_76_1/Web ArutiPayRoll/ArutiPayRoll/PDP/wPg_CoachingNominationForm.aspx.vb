﻿Option Strict On
#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns

#End Region
Partial Class PDP_wPg_CoachingNominationForm
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private objCoachingNomination As New clsCoaching_Nomination_Form
    Private ReadOnly mstrModuleName As String = "frmCoachingNominationForm"
    Private mintFormTypeId As Integer
    Private mintCoachingNominationunkid As Integer
    Private mintPrev_CoachingNominationunkid As Integer
    Private mdtApplcationDate As Date
    Private mblnFromApproval As Boolean
    Private mintMappingUnkid As Integer = 0
    Private mintPendingCoachingTranunkid As Integer
    Private mblnIsApplyReplacement As Boolean
    Private objCONN As SqlConnection
#End Region

#Region " Enum "
    Private Enum enEmployeeTypeId
        Coach = 1
        Coachee = 2
        NewCoach = 3
    End Enum
#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                mintCoachingNominationunkid = CInt(arr(0))
                ViewState("CoachingNominationunkid") = mintCoachingNominationunkid
                HttpContext.Current.Session("CompanyUnkId") = CInt(arr(1))
                HttpContext.Current.Session("UserId") = CInt(arr(2))
                Session("PendingCoachingTranunkid") = CInt(arr(3))
                Session("mintMappingUnkid") = CInt(arr(4))
                If CBool(arr(5)) Then
                    Session("mblnFromApproval") = True
                End If


                'Pinkal (23-Feb-2024) -- Start
                '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                Dim objConfig As New clsConfigOptions
                Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                objConfig = Nothing

                If mblnATLoginRequiredToApproveApplications = False Then
                    Dim objBasePage As New Basepage
                    objBasePage.GenerateAuthentication()
                    objBasePage = Nothing
                End If

                If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                        Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                        Session("ApproverUserId") = CInt(Session("UserId"))
                        DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                        Exit Sub
                    Else

                        If mblnATLoginRequiredToApproveApplications = False Then

                Dim strError As String = ""
                If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                    Exit Sub
                End If

                HttpContext.Current.Session("mdbname") = Session("Database_Name")
                gobjConfigOptions = New clsConfigOptions

                ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))


                CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                ArtLic._Object = New ArutiLic(False)
                If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                    Dim objGroupMaster As New clsGroup_Master
                    objGroupMaster._Groupunkid = 1
                    ArtLic._Object.HotelName = objGroupMaster._Groupname
                End If

                If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If

                If ConfigParameter._Object._IsArutiDemo Then
                    If ConfigParameter._Object._IsExpire Then
                        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                        Exit Try
                    Else
                        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        End If
                    End If
                End If

                Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                Else
                    Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                End If

                Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim()


                Try
                    If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                    Else
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                    End If

                Catch ex As Exception
                    HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                    HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                End Try

                Call GetDatabaseVersion()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(Session("UserId"))
                Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                Call GetDatabaseVersion()
                Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                HttpContext.Current.Session("clsuser") = clsuser
                HttpContext.Current.Session("UserName") = clsuser.UserName
                HttpContext.Current.Session("Firstname") = clsuser.Firstname
                HttpContext.Current.Session("Surname") = clsuser.Surname
                HttpContext.Current.Session("MemberName") = clsuser.MemberName
                HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                HttpContext.Current.Session("UserId") = clsuser.UserID
                HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                HttpContext.Current.Session("Password") = clsuser.password
                HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                strError = ""
                If SetUserSessions(strError) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                    Exit Sub
                End If

                strError = ""
                If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                    Exit Sub
                End If

                        End If 'If mblnATLoginRequiredToApproveApplications = False Then

                    End If ' If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                Else
                    Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                    Session("ApproverUserId") = CInt(Session("UserId"))
                    DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Exit Sub
                End If 'If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then



                If CInt(arr(3)) > 0 Then
                    Dim dsList As DataSet = Nothing
                    Dim objApprovaltran As New clscoachingapproval_process_tran
                    dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                                 -1, mintCoachingNominationunkid)

                    Dim dRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("pendingcoachingtranunkid") = CInt(arr(3)))

                    If CInt(dRow(0).Item("statusunkid")) <> 1 Then
                        If CInt(dRow(0).Item("statusunkid")) = 3 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "You can't Edit this coaching form detail. Reason: This coaching form is already rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                            'Pinkal (23-Feb-2024) -- End


                            Exit Sub
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "You can't Edit this coaching form detail. Reason: This coaching form is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                            'Pinkal (23-Feb-2024) -- End

                            Exit Sub
                        End If
                    End If

                    Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= CInt(dRow(0).Item("priority")) AndAlso x.Field(Of Integer)("statusunkid") <> 1)
                    If dtRow.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You can't Edit this coaching form detail. Reason: This coaching form is already approved/reject or assign"), Me.Page, Session("rootpath").ToString & "Index.aspx")

                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Session("ApprovalLink") = Nothing
                        Session("ApproverUserId") = Nothing

                        If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                Session.Abandon()
                                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                    Response.Cookies("ASP.NET_SessionId").Value = ""
                                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                End If

                                If Request.Cookies("AuthToken") IsNot Nothing Then
                                    Response.Cookies("AuthToken").Value = ""
                                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                End If

                            End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                        'Pinkal (23-Feb-2024) -- End

                        Exit Sub
                    End If
                    objApprovaltran = Nothing
                End If

                CType(Me.Master.FindControl("pnlMenuWrapper"), Panel).Visible = False

                'Else
                '    If Not Session("CoachingNominationunkid") Is Nothing Then
                '        ViewState("CoachingNominationunkid") = CInt(Session("CoachingNominationunkid"))
                '        Session.Remove("CoachingNominationunkid")
                '    End If

            End If
            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count <= 0 Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                If Session("mblnFromApproval") IsNot Nothing Then
                    ViewState("mblnFromApproval") = CBool(Session("mblnFromApproval"))
                    Session.Remove("mblnFromApproval")
                End If

                mblnFromApproval = CBool(ViewState("mblnFromApproval"))

                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()
                Clear_Controls()
                FillCombo()

                mdtApplcationDate = ConfigParameter._Object._CurrentDateAndTime

                If Session("FormTypeId") IsNot Nothing Then
                    mintFormTypeId = CInt(Session("FormTypeId"))
                    Session("FormTypeId") = Nothing
                End If

                If Session("CoachingNominationunkid") IsNot Nothing Then
                    mintCoachingNominationunkid = CInt(Session("CoachingNominationunkid"))
                    Session("CoachingNominationunkid") = Nothing
                    Call GetValue(mintCoachingNominationunkid)
                End If

                If Session("Prev_CoachingNominationunkid") IsNot Nothing Then
                    mintPrev_CoachingNominationunkid = CInt(Session("Prev_CoachingNominationunkid"))
                    Session("Prev_CoachingNominationunkid") = Nothing
                    mblnIsApplyReplacement = True
                    Call GetValue(mintPrev_CoachingNominationunkid)
                    cboCoacheeEmployee.Enabled = False
                End If

                If Session("mintMappingUnkid") IsNot Nothing Then
                    ViewState("mintMappingUnkid") = Session("mintMappingUnkid")
                    Session.Remove("mintMappingUnkid")
                    mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                End If

                If Session("PendingCoachingTranunkid") IsNot Nothing Then
                    mintPendingCoachingTranunkid = CInt(Session("PendingCoachingTranunkid"))
                    Session("PendingCoachingTranunkid") = Nothing
                End If

                If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                    lblPageHeader.Text = "Coaching Nomination Form"
                    lblProposedCoacheeDetails.Text = "Proposed Coachee Details"
                    lblProposedCoachDetails.Text = "Proposed Coach Details"
                    pnlNewCoach.Visible = False
                ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                    lblPageHeader.Text = "Coaching Replacement Form"
                    lblProposedCoacheeDetails.Text = "Coachee Details"
                    lblProposedCoachDetails.Text = "Current Coach Details"
                    cboCoachEmployee.Enabled = False
                    pnlCoacheeSpecificWorkProcess.Visible = False
                End If

                If mblnFromApproval = True Then

                    cboCoacheeEmployee.Enabled = False

                    btnSave.Visible = False
                    btnSubmit.Visible = False
                    btnApprove.Visible = True
                    btnDisapprove.Visible = True
                    pnlCoach.Enabled = False
                    pnlCoachee.Enabled = False
                    pnlNewCoach.Enabled = False
                    pnlApprovalInfo.Visible = True

                    drpRole.SelectedValue = CStr(mintMappingUnkid)
                    drpRole_SelectedIndexChanged(New Object, New EventArgs)
                    drpRole.Enabled = False
                    txtApproverLevel.Enabled = False

                End If

                'If mblnIsApplyReplacement = True Then
                '    Call GetValue(mintPrev_CoachingNominationunkid)
                'Else
                '    Call GetValue(mintCoachingNominationunkid)
                'End If



            Else
                mintFormTypeId = CInt(Me.ViewState("FormTypeId"))
                mdtApplcationDate = CDate(Me.ViewState("ApplcationDate"))
                mintCoachingNominationunkid = CInt(ViewState("CoachingNominationunkid"))
                mintPrev_CoachingNominationunkid = CInt(ViewState("Prev_CoachingNominationunkid"))
                mblnFromApproval = CBool(ViewState("mblnFromApproval"))
                mintPendingCoachingTranunkid = CInt(Me.ViewState("PendingCoachingTranunkid"))
                mblnIsApplyReplacement = CBool(Me.ViewState("mblnIsApplyReplacement"))
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("FormTypeId") = mintFormTypeId
            Me.ViewState("ApplcationDate") = mdtApplcationDate
            Me.ViewState("CoachingNominationunkid") = mintCoachingNominationunkid
            Me.ViewState("mblnFromApproval") = mblnFromApproval
            Me.ViewState("PendingCoachingTranunkid") = mintPendingCoachingTranunkid
            Me.ViewState("Prev_CoachingNominationunkid") = mintPrev_CoachingNominationunkid
            Me.ViewState("mblnIsApplyReplacement") = mblnIsApplyReplacement
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objApprover As New clscoaching_role_mapping
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Coachee", True)

            With cboCoacheeEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables("Coachee").Copy()
                .DataBind()
                .SelectedValue = CStr(Session("EmpUnkid"))
            End With

            dsCombo = objEmployee.GetEmployeeWithMSSList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "Coach", True)

            With cboCoachEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables("Coach")
                .DataBind()
                .SelectedValue = CStr(Session("EmpUnkid"))
            End With

            'Dim dtCoacheeEmployee As DataTable = dsCombo.Tables(0).Select("employeeunkid")


            With cboNewCoachEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables("Coach").Copy()
                .DataBind()
                .SelectedValue = CStr(Session("EmpUnkid"))
            End With

            dsCombo = objApprover.GetList("List", True, "", Nothing)
            With drpRole
                .DataValueField = "mappingunkid"
                .DataTextField = "role"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub FillEmployeeDetail(ByVal mintCurrentEmpId As Integer, ByVal intEmployeeTypeId As enEmployeeTypeId)
        Dim objEmployee As New clsEmployee_Master
        Try
            'Fill Employee Detail
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintCurrentEmpId


            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Class_Group & "," & clsEmployee_Master.EmpColEnum.Col_Class & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Group & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Level & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Grade & "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Gender_Type

            Dim dsEmpDetail As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, CStr(Session("Database_Name")), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               CStr(Session("UserAccessModeSetting")), True, _
                                               CBool(Session("IsIncludeInactiveEmp")), "List", mintCurrentEmpId)


            'Hemant (22 Dec 2023) -- Start
            dsEmpDetail.Tables(0).Columns(5).ColumnName = "colDepartment"
            dsEmpDetail.Tables(0).Columns(14).ColumnName = "colClass"
            dsEmpDetail.Tables(0).Columns(12).ColumnName = "colJob"
            dsEmpDetail.Tables(0).Columns(20).ColumnName = "colGenderType"
            'Hemant (22 Dec 2023) -- End


            If dsEmpDetail.Tables(0).Rows.Count > 0 Then
                If intEmployeeTypeId = enEmployeeTypeId.Coach Then
                    txtCoachJob.Text = dsEmpDetail.Tables(0).Rows(0)("colJob").ToString
                    txtCoachWorkStation.Text = dsEmpDetail.Tables(0).Rows(0)("colClass").ToString
                    txtCoachGender.Text = dsEmpDetail.Tables(0).Rows(0)("colGenderType").ToString
                ElseIf intEmployeeTypeId = enEmployeeTypeId.Coachee Then
                    txtCoacheeJob.Text = dsEmpDetail.Tables(0).Rows(0)("colJob").ToString
                    txtCoacheeWorkStation.Text = dsEmpDetail.Tables(0).Rows(0)("colClass").ToString
                    txtCoacheeGender.Text = dsEmpDetail.Tables(0).Rows(0)("colGenderType").ToString
                    txtCoacheeDepartment.Text = dsEmpDetail.Tables(0).Rows(0)("colDepartment").ToString
                ElseIf intEmployeeTypeId = enEmployeeTypeId.NewCoach Then
                    txtNewCoachWorkStation.Text = dsEmpDetail.Tables(0).Rows(0)("colClass").ToString
                    txtNewCoachGender.Text = dsEmpDetail.Tables(0).Rows(0)("colGenderType").ToString
                End If

            End If

            Dim strNoimage As String = "data:image/png;base64," & ImageToBase64()

            If intEmployeeTypeId = enEmployeeTypeId.Coachee Then
                If objEmployee._blnImgInDb Then
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                        If objEmployee._Photo IsNot Nothing Then
                            imgCoacheeProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                        Else
                            imgCoacheeProfilePic.ImageUrl = strNoimage
                        End If
                    End If
                Else
                    imgCoacheeProfilePic.ImageUrl = strNoimage
                End If
            ElseIf intEmployeeTypeId = enEmployeeTypeId.Coach Then
                If objEmployee._blnImgInDb Then
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                        If objEmployee._Photo IsNot Nothing Then
                            imgProposedCoachProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                        Else
                            imgProposedCoachProfilePic.ImageUrl = strNoimage
                        End If
                    End If
                Else
                    imgProposedCoachProfilePic.ImageUrl = strNoimage
                End If
            ElseIf intEmployeeTypeId = enEmployeeTypeId.NewCoach Then
                If objEmployee._blnImgInDb Then
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                        If objEmployee._Photo IsNot Nothing Then
                            imgNewCoachProfilePic.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                        Else
                            imgNewCoachProfilePic.ImageUrl = strNoimage
                        End If
                    End If
                Else
                    imgNewCoachProfilePic.ImageUrl = strNoimage
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Protected Sub Save_Click()
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            SetValue(objCoachingNomination)

            If objCoachingNomination.Save(Session("Database_Name").ToString, _
                                     CInt(Session("UserId")), _
                                     CInt(Session("Fin_year")), _
                                      CInt(Session("CompanyUnkId")), _
                                     CStr(Session("UserAccessModeSetting")), _
                                     Session("EmployeeAsOnDate").ToString, _
                                     Nothing) = False Then
                DisplayMessage.DisplayMessage(objCoachingNomination._Message, Me)
            Else
                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                If objCoachingNomination._IsSubmitApproval = True Then
                    objCoachingNomination.Send_Notification_Approver(Session("Database_Name").ToString, _
                                                                CInt(cboCoacheeEmployee.SelectedValue), _
                                                                CInt(IIf(objCoachingNomination._MinApprovedPriority > 0, objCoachingNomination._MinApprovedPriority, -1)), _
                                                                CInt(Session("CompanyUnkId")), cboCoachEmployee.SelectedItem.Text, cboCoacheeEmployee.SelectedItem.Text, _
                                                                cboNewCoachEmployee.SelectedItem.Text, _
                                                                objCoachingNomination._Application_Date.Date, _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("ArutiSelfServiceURL")), _
                                                                enLoginMode, intLoginByEmployeeId, intUserId)
                End If
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "PDP/wPg_CoachingNominationFormList.aspx")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try

            If CInt(cboCoachEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Name of Coach is mandatory information. Please select Name of Coach to continue"), Me)
                Return False
            End If

            If CInt(cboCoacheeEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, Name of Coachee is mandatory information. Please select Name of Coachee to continue"), Me)
                Return False
            End If

            If CInt(cboCoachEmployee.SelectedValue) = CInt(cboCoacheeEmployee.SelectedValue) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Coachee and Coach should not be Same. Please select another Coach or Coachee to continue"), Me)
                Return False
            End If

            If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then

                If txtCoacheeSpecificWorkProcess.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Specific Work Process for Coachee is mandatory information. Please enter Specific Work Process for Coachee to continue"), Me)
                    Return False
                End If

                If txtCoachSpecificWorkProcess.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, Specific Work Process for Coach is mandatory information. Please enter Specific Work Process for Coach to continue"), Me)
                    Return False
                End If

                If (New clsCoaching_Nomination_Form).isExist(CInt(cboCoacheeEmployee.SelectedValue), mintCoachingNominationunkid) = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, Coach is already nominated for this Coachee. You can replace the Coach Only"), Me)
                    Return False
                End If

            End If

            If mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                If CInt(cboNewCoachEmployee.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Name of New Coach is mandatory information. Please select Name of New Coach Coach to continue"), Me)
                    Return False
                End If

                If CInt(cboCoacheeEmployee.SelectedValue) = CInt(cboNewCoachEmployee.SelectedValue) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Coachee and New Coach should not be Same. Please select another New Coach to continue"), Me)
                    Return False
                End If

                If CInt(cboCoachEmployee.SelectedValue) = CInt(cboNewCoachEmployee.SelectedValue) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Current Coach and New Coach should not be Same. Please select another New Coach to continue"), Me)
                    Return False
                End If

                If txtCoachSpecificWorkProcess.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sorry, Specific Work Process for Current Coach is mandatory information. Please enter Specific Work Process for Current Coach to continue"), Me)
                    Return False
                End If

                If txtNewCoachSpecificWorkProcess.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, Specific Work Process for New Coach is mandatory information. Please enter Specific Work Process for New Coach to continue"), Me)
                    Return False
                End If

                If txtNewCoachReasonsForChangingCoach.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Reasons For Changing for New Coach is mandatory information. Please enter Reasons For Changing for New Coach to continue"), Me)
                    Return False
                End If
            End If

            If mblnFromApproval = True Then
                If CInt(txtApprRejectRemark.Text.Length) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry, Approver Remark is mandatory information. Please enter Approver Remark to continue"), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub GetValue(ByVal intCoachingNominationunkid As Integer)
        Dim objCoachingNominationForm As New clsCoaching_Nomination_Form
        Try
            objCoachingNominationForm._CoachingNominationunkid = intCoachingNominationunkid
            If mblnIsApplyReplacement = True Then
                mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM
            Else
                mintFormTypeId = objCoachingNominationForm._FormTypeId
            End If

            cboCoacheeEmployee.SelectedValue = CStr(objCoachingNominationForm._Coachee_Employeeunkid)

            If mblnIsApplyReplacement = True OrElse mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                cboCoachEmployee.SelectedValue = CStr(objCoachingNominationForm._Coach_Employeeunkid)
                cboNewCoachEmployee.SelectedValue = CStr(0)
            Else
                cboCoachEmployee.SelectedValue = CStr(objCoachingNominationForm._Prev_Coach_Employeeunkid)
                cboNewCoachEmployee.SelectedValue = CStr(objCoachingNominationForm._Coach_Employeeunkid)
            End If


            If CInt(cboCoacheeEmployee.SelectedValue) > 0 Then
                FillEmployeeDetail(CInt(cboCoacheeEmployee.SelectedValue), enEmployeeTypeId.Coachee)
            End If

            If CInt(cboCoachEmployee.SelectedValue) > 0 Then
                FillEmployeeDetail(CInt(cboCoachEmployee.SelectedValue), enEmployeeTypeId.Coach)
            End If

            If CInt(cboNewCoachEmployee.SelectedValue) > 0 Then
                FillEmployeeDetail(CInt(cboNewCoachEmployee.SelectedValue), enEmployeeTypeId.NewCoach)
            End If

            If mblnIsApplyReplacement = True Then
                txtCoachSpecificWorkProcess.Text = ""
                txtCoacheeSpecificWorkProcess.Text = ""
                txtNewCoachSpecificWorkProcess.Text = ""
                txtNewCoachReasonsForChangingCoach.Text = ""
            Else
                txtCoachSpecificWorkProcess.Text = objCoachingNominationForm._CoachWorkProcess
                txtCoacheeSpecificWorkProcess.Text = objCoachingNominationForm._CoacheeWorkProcess
                txtNewCoachSpecificWorkProcess.Text = objCoachingNominationForm._NewCoachWorkProcess
                txtNewCoachReasonsForChangingCoach.Text = objCoachingNominationForm._ReasonForChange
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingNominationForm = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByRef objCoachingNomination As clsCoaching_Nomination_Form)
        Try
            objCoachingNomination._CoachingNominationunkid = mintCoachingNominationunkid
            objCoachingNomination._Refno = objCoachingNomination.getNextRefNo().ToString
            objCoachingNomination._Application_Date = mdtApplcationDate

            If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                objCoachingNomination._Coach_Employeeunkid = CInt(cboCoachEmployee.SelectedValue)
            ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                objCoachingNomination._Coach_Employeeunkid = CInt(cboNewCoachEmployee.SelectedValue)
            End If
            objCoachingNomination._Coachee_Employeeunkid = CInt(cboCoacheeEmployee.SelectedValue)

            objCoachingNomination._CoachWorkProcess = CStr(txtCoachSpecificWorkProcess.Text)
            objCoachingNomination._CoacheeWorkProcess = CStr(txtCoacheeSpecificWorkProcess.Text)

            objCoachingNomination._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            objCoachingNomination._Effective_Date = mdtApplcationDate

            objCoachingNomination._NewCoachWorkProcess = CStr(txtNewCoachSpecificWorkProcess.Text)
            objCoachingNomination._ReasonForChange = CStr(txtNewCoachReasonsForChangingCoach.Text)

            If mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                objCoachingNomination._Prev_Coach_Employeeunkid = CInt(cboCoachEmployee.SelectedValue)
                objCoachingNomination._Prev_CoachingNominationunkid = mintPrev_CoachingNominationunkid

            Else
                objCoachingNomination._Prev_Coach_Employeeunkid = -1
                objCoachingNomination._Prev_CoachingNominationunkid = -1
            End If

            If Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" Then
                objCoachingNomination._IsSubmitApproval = True
            Else
                objCoachingNomination._IsSubmitApproval = False
            End If
            objCoachingNomination._FormTypeId = mintFormTypeId

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objCoachingNomination._Userunkid = CInt(Session("UserId"))
            Else
                objCoachingNomination._Userunkid = -1
            End If
            objCoachingNomination._IsWeb = True
            objCoachingNomination._Isvoid = False
            objCoachingNomination._Voiddatetime = Nothing
            objCoachingNomination._Voidreason = ""
            objCoachingNomination._Voiduserunkid = -1
            objCoachingNomination._ClientIP = CStr(Session("IP_ADD"))
            objCoachingNomination._FormName = mstrModuleName
            objCoachingNomination._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValueTrainingApproval(ByRef objCoachingApproval As clscoachingapproval_process_tran)
        Try
            objCoachingApproval._PendingCoachingTranunkid = mintPendingCoachingTranunkid
            objCoachingApproval._Approvertranunkid = CInt(drpRole.Attributes("mappingunkid"))
            objCoachingApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime
            objCoachingApproval._Userunkid = CInt(Session("UserId"))
            objCoachingApproval._IsWeb = True
            objCoachingApproval._Isvoid = False
            objCoachingApproval._Voiddatetime = Nothing
            objCoachingApproval._Voidreason = ""
            objCoachingApproval._Voiduserunkid = -1
            objCoachingApproval._ClientIP = CStr(Session("IP_ADD"))
            objCoachingApproval._FormName = mstrModuleName
            objCoachingApproval._HostName = CStr(Session("HOST_NAME"))
            objCoachingApproval._Mapuserunkid = CInt(Session("UserId"))
            objCoachingApproval._Remark = txtApprRejectRemark.Text

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Private Sub Clear_Controls()
        Try
            imgCoacheeProfilePic.ImageUrl = "data:image/png;base64," & ImageToBase64()
            imgProposedCoachProfilePic.ImageUrl = "data:image/png;base64," & ImageToBase64()
            imgNewCoachProfilePic.ImageUrl = "data:image/png;base64," & ImageToBase64()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSubmit.Click
        Try
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper

            Call Save_Click()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Request.QueryString.Count <= 0 Then
                If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                    Response.Redirect(Session("ReturnURL").ToString, False)
                    Session("ReturnURL") = Nothing
                Else
                    Response.Redirect("~\UserHome.aspx", False)
                End If
            Else
                'Pinkal (23-Feb-2024) -- Start
                '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If
                'Pinkal (23-Feb-2024) -- End
                Response.Redirect("~/Index.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, _
                                                                                            btnDisapprove.Click
        Try
            Select Case CType(sender, Button).ID.ToUpper
                Case btnDisapprove.ID.ToUpper
                    If txtApprRejectRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, Remark is mandatory information. Please add remark to continue."), Me)
                        Exit Sub
                    End If
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Are you sure you want to reject this Coaching Nomination/Replacement Form?")
                Case btnApprove.ID.ToUpper
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Are you sure you want to approve this Coaching Nomination/Replacement Form?")
            End Select
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            cnfApprovDisapprove.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox's Events "
    Protected Sub cboCoachEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCoachEmployee.SelectedIndexChanged
        Try
            FillEmployeeDetail(CInt(cboCoachEmployee.SelectedValue), enEmployeeTypeId.Coach)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboCoacheeEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCoacheeEmployee.SelectedIndexChanged
        Try
            FillEmployeeDetail(CInt(cboCoacheeEmployee.SelectedValue), enEmployeeTypeId.Coachee)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboNewCoachEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewCoachEmployee.SelectedIndexChanged
        Try
            FillEmployeeDetail(CInt(cboNewCoachEmployee.SelectedValue), enEmployeeTypeId.NewCoach)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub drpRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpRole.SelectedIndexChanged
        Try
            If CInt(IIf(drpRole.SelectedValue = "", 0, drpRole.SelectedValue)) > 0 Then
                Dim objlevel As New clsCoaching_Approver_Level_master
                Dim objApprover As New clscoaching_role_mapping
                objApprover._Mappingunkid = CInt(IIf(drpRole.SelectedValue = "", 0, drpRole.SelectedValue))
                objlevel._Levelunkid = objApprover._Levelunkid
                txtApproverLevel.Text = objlevel._Levelname
                txtApproverLevel.Attributes.Add("levelunkid", objlevel._Levelunkid.ToString())
                txtApproverLevel.Attributes.Add("priority", objlevel._Priority.ToString())
                drpRole.Attributes.Add("mappingunkid", objApprover._Mappingunkid.ToString())

                objlevel = Nothing : objApprover = Nothing
            Else
                txtApproverLevel.Attributes.Remove("levelunkid")
                txtApproverLevel.Attributes.Remove("priority")
                drpRole.Attributes.Remove("mappingunkid")

                txtApproverLevel.Text = ""
                txtApprRejectRemark.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Confirmation"
    Protected Sub cnfApprovDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfApprovDisapprove.buttonYes_Click
        Dim objCoachingApproval As New clscoachingapproval_process_tran
        Dim objCoachingForm As New clsCoaching_Nomination_Form
        Try
            Dim blnFlag As Boolean = False

            If IsValidate() = False Then Exit Sub
            Call SetValueTrainingApproval(objCoachingApproval)
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnApprove.ID.ToUpper, btnDisapprove.ID.ToUpper
                    If Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper Then
                        objCoachingApproval._Statusunkid = enPDPCoachingApprovalStatus.APPROVED
                    ElseIf Me.ViewState("Sender").ToString().ToUpper() = btnDisapprove.ID.ToUpper Then
                        objCoachingApproval._Statusunkid = enPDPCoachingApprovalStatus.REJECTED
                    End If

                    If mintPendingCoachingTranunkid > 0 Then
                        blnFlag = objCoachingApproval.Update(Session("Database_Name").ToString, _
                                                             eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))))
                    Else
                        blnFlag = objCoachingApproval.Insert()
                    End If

                    If blnFlag = False And objCoachingApproval._Message <> "" Then

                        DisplayMessage.DisplayMessage(objCoachingApproval._Message, Me)
                    Else
                        objCoachingForm._CoachingNominationunkid = mintCoachingNominationunkid
                        If objCoachingApproval._Statusunkid = enPDPCoachingApprovalStatus.APPROVED Then
                            objCoachingForm.Send_Notification_Approver(Session("Database_Name").ToString, _
                                                                       CInt(cboCoacheeEmployee.SelectedValue), _
                                                                       CInt(IIf(CInt(txtApproverLevel.Attributes("priority")) > 0, CInt(txtApproverLevel.Attributes("priority")), -1)), _
                                                                       CInt(Session("CompanyUnkId")), cboCoachEmployee.SelectedItem.Text, _
                                                                       cboCoacheeEmployee.SelectedItem.Text, cboNewCoachEmployee.SelectedItem.Text, _
                                                                       objCoachingForm._Application_Date.Date, _
                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                       CStr(Session("ArutiSelfServiceURL")), _
                                                                       enLogin_Mode.DESKTOP, _
                                                                       0, CInt(Session("UserId")), , _
                                                                       )
                        End If
                    End If

                    If Request.QueryString.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Information saved successfully."), Me, "../PDP/wPg_CoachingNominationApprovalList.aspx")
                    Else
                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")

                        Session.Clear()
                        Session.Abandon()
                        Session.RemoveAll()
                        If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                            Response.Cookies("ASP.NET_SessionId").Value = ""
                            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                        End If

                        If Request.Cookies("AuthToken") IsNot Nothing Then
                            Response.Cookies("AuthToken").Value = ""
                            Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                        End If
                        'Pinkal (23-Feb-2024) -- End

                    End If
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblProposedCoacheeDetails.ID, Me.lblProposedCoacheeDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeEmployee.ID, Me.lblCoacheeEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeJob.ID, Me.lblCoacheeJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeWorkStation.ID, Me.lblCoacheeWorkStation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeGender.ID, Me.lblCoacheeGender.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeDepartment.ID, Me.lblCoacheeDepartment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeSpecificWorkProcess.ID, Me.lblCoacheeSpecificWorkProcess.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblProposedCoachDetails.ID, Me.lblProposedCoachDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoachEmployee.ID, Me.lblCoachEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoachJob.ID, Me.lblCoachJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoachWorkStation.ID, Me.lblCoachWorkStation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoachGender.ID, Me.lblCoachGender.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoachSpecificWorkProcess.ID, Me.lblCoachSpecificWorkProcess.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNewCoachDetails.ID, Me.lblNewCoachDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNewCoachEmployee.ID, Me.lblNewCoachEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNewCoachWorkStation.ID, Me.lblNewCoachWorkStation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNewCoachGender.ID, Me.lblNewCoachGender.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNewCoachSpecificWorkProcess.ID, Me.lblNewCoachSpecificWorkProcess.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblNewCoachReasonsForChangingCoach.ID, Me.lblNewCoachReasonsForChangingCoach.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApprovalData.ID, Me.lblApprovalData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRole.ID, Me.lblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLevel.ID, Me.lblLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApprRejectRemark.ID, Me.lblApprRejectRemark.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnApprove.ID, Me.btnApprove.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnDisapprove.ID, Me.btnDisapprove.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSave.ID, Me.btnSave.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSubmit.ID, Me.btnSubmit.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblProposedCoacheeDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblProposedCoacheeDetails.ID, Me.lblProposedCoacheeDetails.Text)
            Me.lblCoacheeEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeEmployee.ID, Me.lblCoacheeEmployee.Text)
            Me.lblCoacheeJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeJob.ID, Me.lblCoacheeJob.Text)
            Me.lblCoacheeWorkStation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeWorkStation.ID, Me.lblCoacheeWorkStation.Text)
            Me.lblCoacheeGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeGender.ID, Me.lblCoacheeGender.Text)
            Me.lblCoacheeDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeDepartment.ID, Me.lblCoacheeDepartment.Text)
            Me.lblCoacheeSpecificWorkProcess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeSpecificWorkProcess.ID, Me.lblCoacheeSpecificWorkProcess.Text)
            Me.lblProposedCoachDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblProposedCoachDetails.ID, Me.lblProposedCoachDetails.Text)
            Me.lblCoachEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoachEmployee.ID, Me.lblCoachEmployee.Text)
            Me.lblCoachJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoachJob.ID, Me.lblCoachJob.Text)
            Me.lblCoachWorkStation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoachWorkStation.ID, Me.lblCoachWorkStation.Text)
            Me.lblCoachGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoachGender.ID, Me.lblCoachGender.Text)
            Me.lblCoachSpecificWorkProcess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoachSpecificWorkProcess.ID, Me.lblCoachSpecificWorkProcess.Text)
            Me.lblNewCoachDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewCoachDetails.ID, Me.lblNewCoachDetails.Text)
            Me.lblNewCoachEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewCoachEmployee.ID, Me.lblNewCoachEmployee.Text)
            Me.lblNewCoachWorkStation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewCoachWorkStation.ID, Me.lblNewCoachWorkStation.Text)
            Me.lblNewCoachGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewCoachGender.ID, Me.lblNewCoachGender.Text)
            Me.lblNewCoachSpecificWorkProcess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewCoachSpecificWorkProcess.ID, Me.lblNewCoachSpecificWorkProcess.Text)
            Me.lblNewCoachReasonsForChangingCoach.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewCoachReasonsForChangingCoach.ID, Me.lblNewCoachReasonsForChangingCoach.Text)
            Me.lblApprovalData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprovalData.ID, Me.lblApprovalData.Text)
            Me.lblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRole.ID, Me.lblRole.Text)
            Me.lblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblApprRejectRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprRejectRemark.ID, Me.lblApprRejectRemark.Text)

            
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text.Replace("&", ""))
            Me.btnDisapprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnDisapprove.ID, Me.btnDisapprove.Text.Replace("&", ""))
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text.Replace("&", ""))
            Me.btnSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSubmit.ID, Me.btnSubmit.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "You can't Edit this coaching form detail. Reason: This coaching form is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "You can't Edit this coaching form detail. Reason: This coaching form is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You can't Edit this coaching form detail. Reason: This coaching form is already approved/reject or assign")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Information saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, Name of Coach is mandatory information. Please select Name of Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, Name of Coachee is mandatory information. Please select Name of Coachee to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, Coachee and Coach should not be Same. Please select another Coach or Coachee to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, Specific Work Process for Coachee is mandatory information. Please enter Specific Work Process for Coachee to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, Specific Work Process for Coach is mandatory information. Please enter Specific Work Process for Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, Coach is already nominated for this Coachee. You can replace the Coach Only")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, Name of New Coach is mandatory information. Please select Name of New Coach Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, Coachee and New Coach should not be Same. Please select another New Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, Current Coach and New Coach should not be Same. Please select another New Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Sorry, Specific Work Process for Current Coach is mandatory information. Please enter Specific Work Process for Current Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Sorry, Specific Work Process for New Coach is mandatory information. Please enter Specific Work Process for New Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Sorry, Reasons For Changing for New Coach is mandatory information. Please enter Reasons For Changing for New Coach to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Sorry, Approver Remark is mandatory information. Please enter Approver Remark to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, Remark is mandatory information. Please add remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Are you sure you want to reject this Coaching Nomination/Replacement Form?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Are you sure you want to approve this Coaching Nomination/Replacement Form?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Information saved successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
