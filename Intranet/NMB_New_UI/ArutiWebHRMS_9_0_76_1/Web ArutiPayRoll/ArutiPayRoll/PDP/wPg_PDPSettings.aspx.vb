﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Partial Class PDP_wPg_PDPSettings
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPDPSettings"
    Private ReadOnly mstrModuleName1 As String = "frmPDPReviewerLevelAddEdit"
    Private ReadOnly mstrModuleName2 As String = "frmPDPEvaluatorsAddEdit"
    Private ReadOnly mstrModuleName3 As String = "frmPDPReviewerAddEdit"
    Private ReadOnly mstrModuleName4 As String = "frmPDPActionPlanCategoriesAddEdit"
    Private ReadOnly mstrModuleName5 As String = "frmPDPCategoriesAddEdit"
    Private ReadOnly mstrModuleName6 As String = "frmPDPItemsAddEdit"
    Private ReadOnly mstrModuleName7 As String = "frmPDPCategoryItemMapping"
    Private ReadOnly mstrModuleName8 As String = "frmPDPCategoryInstruction"

    Private DisplayMessage As New CommonCodes
    Private mintReviewerLevelUnkid As Integer = 0
    Private mintReviewerUnkid As Integer = 0
    Private mintActionPlanCategoryUnkid As Integer = 0
    Private mintCategoryUnkid As Integer = 0
    Private mintCategoryItemmappingid As Integer = 0
    Private mintItemUnkid As Integer = 0
    Private mstrDeleteAction As String = ""
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                Call FillReviewerLevel()
                Call FillEvaluatorsCombo()
                Call FillEvaluatorsSetting()
                Call FillReviewersCombo()
                Call FillReviewers()
                Call FillPDPCategoriesCombo()
                Call FillPDPCategories()
                Call FillActionPlanCategories()
                Call FillPDPItemsCombo()
                Call FillPDPItems()
                Call FillPDPCategoryItemMapping()
                Call FillInstructionSetting()
            Else
                mintReviewerLevelUnkid = CInt(Me.ViewState("mintReviewerLevelUnkid"))
                mintReviewerUnkid = CInt(Me.ViewState("mintReviewerUnkid"))
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintActionPlanCategoryUnkid = CInt(Me.ViewState("mintActionPlanCategoryUnkid"))
                mintCategoryUnkid = CInt(Me.ViewState("mintCategoryUnkid"))
                mintItemUnkid = CInt(Me.ViewState("mintItemUnkid"))
                mintCategoryItemmappingid = CInt(Me.ViewState("mintCategoryItemmappingid"))


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintReviewerLevelUnkid") = mintReviewerLevelUnkid
            Me.ViewState("mintReviewerUnkid") = mintReviewerUnkid
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintActionPlanCategoryUnkid") = mintActionPlanCategoryUnkid
            Me.ViewState("mintCategoryUnkid") = mintCategoryUnkid
            Me.ViewState("mintItemUnkid") = mintItemUnkid
            Me.ViewState("mintCategoryItemmappingid") = mintCategoryItemmappingid

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Reviewer Levels "
#Region " Button's Events "
    Protected Sub btnLSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLSave.Click
        Dim objReviewerLevel As New clspdpreviewerlevel_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidReviewerLevel() = False Then
                Exit Sub
            End If
            SetReviewerLevelValue(objReviewerLevel)
            If mintReviewerLevelUnkid > 0 Then
                blnFlag = objReviewerLevel.Update()
            Else
                blnFlag = objReviewerLevel.Insert()
            End If

            If blnFlag = False AndAlso objReviewerLevel._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objReviewerLevel._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Level defined successfully."), Me)
                FillReviewerLevel()
                ClearReviewerLevelCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objReviewerLevel = Nothing
        End Try
    End Sub

    Protected Sub btnLReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLReset.Click
        Try
            Call ClearReviewerLevelCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub ClearReviewerLevelCtrls()
        Try
            mintReviewerLevelUnkid = 0
            txtLlevelcode.Text = ""
            txtLlevelname.Text = ""
            txtLlevelpriority.Text = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillReviewerLevel()
        Dim objReviewerLevel As New clspdpreviewerlevel_master
        Dim dsList As New DataSet
        Try
            dsList = objReviewerLevel.GetList("List")
            gvReviewerLevel.DataSource = dsList.Tables(0)
            gvReviewerLevel.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objReviewerLevel = Nothing
        End Try
    End Sub

    Private Sub SetReviewerLevelValue(ByRef objReviewerLevel As clspdpreviewerlevel_master)
        Try
            objReviewerLevel._Levelunkid = mintReviewerLevelUnkid
            objReviewerLevel._Levelcode = txtLlevelcode.Text
            objReviewerLevel._Levelname = txtLlevelname.Text
            objReviewerLevel._Priority = CInt(txtLlevelpriority.Text)
            objReviewerLevel._AuditUserid = CInt(Session("UserId"))
            objReviewerLevel._ClientIp = CStr(Session("IP_ADD"))

            objReviewerLevel._DatabaseName = CStr(Session("Database_Name"))
            objReviewerLevel._FormName = mstrModuleName1
            objReviewerLevel._IsFromWeb = True
            objReviewerLevel._HostName = CStr(Session("HOST_NAME"))
            objReviewerLevel._Isactive = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidReviewerLevel() As Boolean

        Try
            If txtLlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Please enter Level code to continue."), Me)
                Return False
            End If

            If txtLlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Please enter Level name to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)


        End Try
    End Function

    Private Sub GetReviewerLevelValue(ByVal intReviewerLevelId As Integer)
        Dim objpdpreviewerlevel_master As New clspdpreviewerlevel_master
        Try
            objpdpreviewerlevel_master._Levelunkid = mintReviewerLevelUnkid
            txtLlevelcode.Text = CStr(objpdpreviewerlevel_master._Levelcode)
            txtLlevelname.Text = objpdpreviewerlevel_master._Levelname
            txtLlevelpriority.Text = CStr(objpdpreviewerlevel_master._Priority)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpreviewerlevel_master = Nothing
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkLEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpdpreviewerlevel_master As New clspdpreviewerlevel_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintReviewerLevelUnkid = CInt(gvReviewerLevel.DataKeys(row.RowIndex)("Levelunkid"))

            'If objpdpreviewerlevel_master.IsTalentStartedForCycle(mintReviewerLevelUnkid) Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1000, "Sorry you can't edit this Question,Reason: Talent Process is already started for this cycle."), Me)
            '    Exit Sub
            'End If

            mintReviewerLevelUnkid = CInt(gvReviewerLevel.DataKeys(row.RowIndex)("levelunkid"))
            GetReviewerLevelValue(mintReviewerLevelUnkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpreviewerlevel_master = Nothing
        End Try
    End Sub

    Protected Sub lnkLDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objpdpreviewerlevel_master As New clspdpreviewerlevel_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintReviewerLevelUnkid = CInt(gvReviewerLevel.DataKeys(row.RowIndex)("Cycleunkid"))

            'If objpdpreviewerlevel_master.IsTalentStartedForCycle(mintReviewerLevelUnkid) Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1001, "Sorry you can't delete this Question,Reason: Talent Process is already started for this cycle."), Me)
            '    Exit Sub
            'End If
            mintReviewerLevelUnkid = CInt(gvReviewerLevel.DataKeys(row.RowIndex)("levelunkid"))
            mstrDeleteAction = "dellevel"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "You are about to delete this Level. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpreviewerlevel_master = Nothing
        End Try
    End Sub

#End Region
#End Region

#Region " Evaluators "

#Region " Private Methods "
    Private Sub FillEvaluatorsCombo()
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objMst.GetEAllocation_Notification("Allocation")
            Dim dr As DataRow = dsCombo.Tables("Allocation").NewRow
            dr.Item("Id") = 0
            dr.Item("NAME") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 1, "Select")
            dsCombo.Tables("Allocation").Rows.InsertAt(dr, 0)

            With drpAllocationBy
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Allocation")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Function IsValidEvaluators() As Boolean
        Dim objpdpsettings_master As New clspdpsettings_master
        Try
            If chkPeers.Checked AndAlso CInt(drpAllocationBy.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 2, "Please select at lease one allocation to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpsettings_master = Nothing
        End Try
    End Function

    Private Sub SetEvaluatorsValue(ByRef objsetting As clspdpsettings_master)
        Try
            objsetting._AuditUserId = CInt(Session("UserId"))
            objsetting._ClientIP = CStr(Session("IP_ADD"))
            objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            objsetting._FormName = mstrModuleName2
            objsetting._FromWeb = True
            objsetting._HostName = CStr(Session("HOST_NAME"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillEvaluatorsSetting()
        Try
            Dim objpdpsettings_master As New clspdpsettings_master
            Dim blnFlag As Boolean = False
            Try
                Dim PDPSetting As Dictionary(Of clspdpsettings_master.enPDPConfiguration, String) = objpdpsettings_master.GetSetting()
                If IsNothing(PDPSetting) = False Then
                    For Each kvp As KeyValuePair(Of clspdpsettings_master.enPDPConfiguration, String) In PDPSetting
                        Select Case kvp.Key
                            Case clspdpsettings_master.enPDPConfiguration.SELF
                                chkSelf.Checked = CBool(kvp.Value)

                            Case clspdpsettings_master.enPDPConfiguration.LINE_MANAGER
                                chkLineManager.Checked = CBool(kvp.Value)

                            Case clspdpsettings_master.enPDPConfiguration.PEERS

                                Dim allocation As String = kvp.Value.ToString()

                                If allocation.Length > 0 Then
                                    chkPeers.Checked = True
                                    chkPeers_CheckedChanged(Nothing, Nothing)
                                    If chkPeers.Checked Then
                                        drpAllocationBy.SelectedValue = allocation
                                    Else
                                        drpAllocationBy.SelectedValue = CStr(0)
                                    End If

                                End If
                        End Select
                    Next
                End If



            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            Finally
                objpdpsettings_master = Nothing
            End Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event "
    Protected Sub btnESave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnESave.Click
        Dim objpdpsettings As New clspdpsettings_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidEvaluators() = False Then
                Exit Sub
            End If

            SetEvaluatorsValue(objpdpsettings)

            Dim pdpSetting As New Dictionary(Of clspdpsettings_master.enPDPConfiguration, String)

            'pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.SELF, chkSelf.Checked.ToString())
            'pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.LINE_MANAGER, chkLineManager.Checked.ToString())
            'If chkPeers.Checked Then
            '    pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.PEERS, drpAllocationBy.SelectedValue.ToString())
            'Else
            '    pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.PEERS, "0")
            'End If

            pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.SELF, False.ToString())
            pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.LINE_MANAGER, False.ToString())
            If False Then
                pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.PEERS, drpAllocationBy.SelectedValue.ToString())
            Else
                pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.PEERS, "0")
            End If


            objpdpsettings._DatabaseName = CStr(Session("Database_Name"))

            blnFlag = objpdpsettings.SavePDPSetting(pdpSetting)

            If blnFlag = False AndAlso objpdpsettings._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objpdpsettings._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 3, "Feedback settings saved successfully."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpsettings = Nothing
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Checkbox Event "
    Protected Sub chkPeers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPeers.CheckedChanged
        Try
            pnlPeers.Enabled = chkPeers.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
#End Region

#Region " Reviewers "
#Region " Private Methods "
    Private Sub FillReviewersCombo()
        Dim objpdpreviewerlevel As New clspdpreviewerlevel_master
        Dim objUser As New clsUserAddEdit
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objpdpreviewerlevel.getListForCombo("List", True)

            With drpRLevel
                .DataValueField = "levelunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), , CInt(Session("Fin_year")), True)
            With drpRUser
                .DataSource = dsCombo.Tables("User")
                .DataTextField = "name"
                .DataValueField = "userunkid"
                .DataBind()
            End With

            dsCombo = objMst.GetEAllocation_Notification("Allocation")
            Dim dr As DataRow = dsCombo.Tables("Allocation").NewRow
            dr.Item("Id") = 0
            dr.Item("NAME") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 1, "Select")
            dsCombo.Tables("Allocation").Rows.InsertAt(dr, 0)

            With drpRAllocation
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Allocation")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpreviewerlevel = Nothing
            objUser = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearReviewersCtrls()
        Try
            mintReviewerUnkid = 0
            drpRLevel.SelectedValue = CStr(0)
            drpRUser.SelectedValue = CStr(0)
            drpRAllocation.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillReviewers()
        Dim objReviewer As New clspdpreviewer_master
        Dim dsList As New DataSet
        Try
            dsList = objReviewer.GetList("List", False)
            gvReviewers.DataSource = dsList.Tables(0)
            gvReviewers.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objReviewer = Nothing
        End Try
    End Sub

    Private Function IsValidReviewers() As Boolean
        Try
            'If CInt(drpRLevel.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 1002, "Sorry, Reviewer Level is mandatory information. Please Select Reviewer Level to continue."), Me)
            '    Return False
            'End If

            If CInt(drpRUser.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 2, "Please Select User to continue."), Me)
                Return False
            End If

            'If CInt(drpRAllocation.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 1003, "Sorry, Allocation is mandatory information. Please Select Allocation to continue."), Me)
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Function

    Private Sub SetReviewersValue(ByRef objReviewers As clspdpreviewer_master)
        Try

            objReviewers._Reviewermstunkid = mintReviewerUnkid
            'objReviewers._Levelunkid = CInt(drpRLevel.SelectedValue)
            objReviewers._Levelunkid = -1
            objReviewers._Mapuserunkid = CInt(drpRUser.SelectedValue)
            'objReviewers._AllocationId = CInt(drpRAllocation.SelectedValue)
            objReviewers._AllocationId = -1

            objReviewers._Isactive = True
            objReviewers._Userunkid = CInt(Session("UserId"))
            objReviewers._AuditUserId = CInt(Session("UserId"))
            objReviewers._ClientIP = CStr(Session("IP_ADD"))
            objReviewers._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objReviewers._DatabaseName = CStr(Session("Database_Name"))
            objReviewers._FormName = mstrModuleName3
            objReviewers._FromWeb = True
            objReviewers._HostName = CStr(Session("HOST_NAME"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetReviewersValue(ByVal intReviewerId As Integer)
        Dim objReviewer As New clspdpreviewer_master
        Try
            objReviewer._Reviewermstunkid = mintReviewerUnkid
            drpRLevel.SelectedValue = CStr(objReviewer._Levelunkid)
            drpRUser.SelectedValue = CStr(objReviewer._Userunkid)
            drpRAllocation.SelectedValue = CStr(objReviewer._AllocationId)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objReviewer = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub bntRSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bntRSave.Click
        Dim objReviewer As New clspdpreviewer_master
        Dim blnFlag As Boolean = False
        Dim dsGetList As DataSet = Nothing
        Try
            If IsValidReviewers() = False Then
                Exit Sub
            End If
            SetReviewersValue(objReviewer)

            dsGetList = objReviewer.GetList("List")

            If mintReviewerUnkid > 0 Then
                blnFlag = objReviewer.Update()
            Else
                blnFlag = objReviewer.Insert()
            End If

            If blnFlag = False AndAlso objReviewer._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objReviewer._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 3, "Reviewer defined successfully."), Me)
                FillReviewers()
                ClearReviewersCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objReviewer = Nothing
            dsGetList = Nothing
        End Try
    End Sub

    Protected Sub bntRReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bntRReset.Click
        Try
            Call ClearReviewersCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkREdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintReviewerUnkid = CInt(gvReviewers.DataKeys(row.RowIndex)("reviewermstunkid"))

            GetReviewersValue(mintReviewerUnkid)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkRDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintReviewerUnkid = CInt(gvReviewers.DataKeys(row.RowIndex)("reviewermstunkid"))
            mstrDeleteAction = "delRev"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 4, "You are about to delete this Reviewer. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub RInActive_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintReviewerUnkid = CInt(gvReviewers.DataKeys(row.RowIndex)("reviewermstunkid"))
            mstrDeleteAction = "InactiveRev"

            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 5, "You are about to deactivate this reviewer. Are you sure you want to deactivate?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub

    Protected Sub RActive_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintReviewerUnkid = CInt(gvReviewers.DataKeys(row.RowIndex)("reviewermstunkid"))
            mstrDeleteAction = "ActiveRev"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 6, "You are about to activate this reviewer. Are you sure you want to activate?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Gridview Event"
    Protected Sub gvReviewers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReviewers.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("RActive"), LinkButton)
                Dim lnkInActive As LinkButton = TryCast(e.Row.FindControl("RInActive"), LinkButton)


                If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                    If CBool(dt.Rows(e.Row.RowIndex)("isactive").ToString()) = True Then
                        lnkactive.Visible = False
                        lnkInActive.Visible = True
                    Else
                        lnkactive.Visible = True
                        lnkInActive.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
#End Region

#Region " PDP Categories "
#Region " Private Method(s)"



    Private Sub FillPDPCategoriesCombo()
      
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objMst.GetPDPCategoryType(True, "List")
            With drpParameterViewType
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearPDPCategoriesCtrls()
        Try
            mintCategoryUnkid = 0
            txtPCategory.Text = ""
            txtPSortOrder.Text = "0"
            drpParameterViewType.SelectedValue = "0"
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            chkIncludeInPM.Checked = False
            chkMakeMandatory.Checked = False
            If chkIncludeInPM.Enabled = False Then chkIncludeInPM.Enabled = True
            'S.SANDEEP |03-MAY-2021| -- END
            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillPDPCategories()
        Dim objCategory As New clspdpcategory_master
        Dim dsList As New DataSet
        Try
            dsList = objCategory.GetList("List")
            gvCategory.DataSource = dsList.Tables(0)
            gvCategory.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub

    Private Function IsValidPDPCategories() As Boolean
        Dim objpdpcategory_master As New clspdpcategory_master
        Try
            If txtPCategory.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 1, "Please enter Parameter to continue."), Me)
                Return False
            End If


            If txtPSortOrder.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 2, "Please enter sort order to continue."), Me)
                Return False
            End If


            If CInt(txtPSortOrder.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 3, "Sorry, sort order value should be greater than 0."), Me)
                Return False
            End If

            If CInt(drpParameterViewType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 4, "Please enter Parameter Type to continue."), Me)
                Return False
            End If

            If objpdpcategory_master.isSortOrderExist(CInt(txtPSortOrder.Text), mintCategoryUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 5, "Sorry, this sort order is already selected for another parameter."), Me)
                Return False
            End If

            Dim dslist As DataSet = objpdpcategory_master.isPDPTransectionStarted(mintCategoryUnkid)

            If IsNothing(dslist) = False AndAlso dslist.Tables(0).Rows.Count > 0 Then
                If CInt(drpParameterViewType.SelectedValue) <> CInt(dslist.Tables(0).Rows(0)("viewtype")) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 6, "Sorry, this parameter is already in use and its parameter type cannot be changed."), Me)
                    Return False
                End If
            End If
           
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetPDPCategoriesValue(ByRef objCategory As clspdpcategory_master)
        Try
            objCategory._Categoryunkid = mintCategoryUnkid
            objCategory._Category = txtPCategory.Text
            objCategory._CategoryTypeId = CInt(drpParameterViewType.SelectedValue)
            objCategory._SortOrder = CInt(txtPSortOrder.Text)
            objCategory._AuditUserId = CInt(Session("UserId"))
            objCategory._ClientIP = CStr(Session("IP_ADD"))
            objCategory._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objCategory._DatabaseName = CStr(Session("Database_Name"))
            objCategory._FormName = mstrModuleName4
            objCategory._FromWeb = True
            objCategory._HostName = CStr(Session("HOST_NAME"))
            objCategory._Isactive = True

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objCategory._Isincludeinpm = chkIncludeInPM.Checked
            objCategory._Ismandatory = chkMakeMandatory.Checked
            'S.SANDEEP |03-MAY-2021| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetPDPCategoriesValue(ByVal intQuestionnaireId As Integer)
        Dim objCategory As New clspdpcategory_master
        Try
            objCategory._Categoryunkid = mintCategoryUnkid
            txtPCategory.Text = CStr(objCategory._Category)
            txtPSortOrder.Text = CStr(objCategory._SortOrder)
            drpParameterViewType.SelectedValue = objCategory._CategoryTypeId.ToString()
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            chkIncludeInPM.Checked = objCategory._Isincludeinpm
            chkMakeMandatory.Checked = objCategory._Ismandatory
            'S.SANDEEP |03-MAY-2021| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnPSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Dim objCategory As New clspdpcategory_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidPDPCategories() = False Then
                Exit Sub
            End If
            SetPDPCategoriesValue(objCategory)
            If mintCategoryUnkid > 0 Then
                blnFlag = objCategory.Update()
            Else
                blnFlag = objCategory.Insert()
            End If

            If blnFlag = False AndAlso objCategory._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objCategory._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 7, "PDP parameter defined successfully."), Me)
                FillPDPCategories()
                ClearPDPCategoriesCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub

    Protected Sub btnPReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPReset.Click
        Try
            Call ClearPDPCategoriesCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkPEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintCategoryUnkid = CInt(gvCategory.DataKeys(row.RowIndex)("categoryunkid"))
            GetPDPCategoriesValue(mintCategoryUnkid)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            If chkIncludeInPM.Enabled = False Then chkIncludeInPM.Enabled = True
            If drpParameterViewType.Enabled = False Then drpParameterViewType.Enabled = True
            If CInt(gvCategory.DataKeys(row.RowIndex)("cdel")) > 0 Then
                chkIncludeInPM.Enabled = False
                drpParameterViewType.Enabled = False
            Else
                chkIncludeInPM.Enabled = True
                drpParameterViewType.Enabled = True
            End If
            'S.SANDEEP |03-MAY-2021| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objpdpcategory_master As New clspdpcategory_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            If CInt(gvCategory.DataKeys(row.RowIndex)("cdel")) > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 8, "Sorry, you cannot delete this category, This category is generated by system and will be linked to performance module."), Me)
                Exit Sub
            End If
            'S.SANDEEP |03-MAY-2021| -- END

            mintCategoryUnkid = CInt(gvCategory.DataKeys(row.RowIndex)("categoryunkid"))

            If objpdpcategory_master.isUsed(mintCategoryUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 9, "Sorry, this parameter is already in use."), Me)
                Exit Sub
            End If


            mstrDeleteAction = "delcat"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 10, "You are about to delete this parameter. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

#End Region
#End Region

#Region " PDP Items "
#Region " Private Methods "
    Private Sub FillPDPItemsCombo()
        Dim objpdpcategory As New clspdpcategory_master
        'S.SANDEEP |03-MAY-2021| -- START
        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
        'Dim objCItems As New clsassess_custom_items
        Dim objPItems As New clspdpitem_master
        'S.SANDEEP |03-MAY-2021| -- END
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objpdpcategory.getListForCombo("List", True)
            With drpICategory
                .DataValueField = "categoryunkid"
                .DataTextField = "category"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            With drpCategoryItemMappingCategory
                .DataValueField = "categoryunkid"
                .DataTextField = "category"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            'dsCombo = objCItems.GetList_CustomTypes("List", True)
            dsCombo = objPItems.GetList_CustomTypes("List", True)
            'S.SANDEEP |03-MAY-2021| -- END
            With drpIItemType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            'dsCombo = objCItems.GetList_SelectionMode("List", True)
            dsCombo = objPItems.GetList_SelectionMode("List", True)
            'S.SANDEEP |03-MAY-2021| -- END

            Dim dt As DataTable = New DataView(dsCombo.Tables(0), "ID NOT IN ( " & clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES & "," & clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS & ", " & clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM & " ) ", "", DataViewRowState.CurrentRows).ToTable

            With drpISection
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dt
                .SelectedIndex = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpcategory = Nothing
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            'objCItems = Nothing
            objPItems = Nothing
            'S.SANDEEP |03-MAY-2021| -- END

            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearPDPItemsCtrls()
        Try
            mintItemUnkid = 0
            drpICategory.SelectedValue = CStr(0)
            drpIItemType.SelectedValue = CStr(0)
            drpISection.SelectedValue = CStr(0)
            txtIItemText.Text = ""
            txtISortOrder.Text = "0"
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            pnlvisibility.Visible = False

            If drpIItemType.Enabled = False Then drpIItemType.Enabled = True
            If drpISection.Enabled = False Then drpISection.Enabled = True
            If drpICategory.Enabled = False Then drpICategory.Enabled = True
            'S.SANDEEP |03-MAY-2021| -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillPDPItems()
        Dim objItem As New clspdpitem_master
        Dim dsList As New DataSet
        Try
            dsList = objItem.GetList("List")
            gvPDPItem.DataSource = dsList.Tables(0)
            gvPDPItem.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objItem = Nothing
        End Try
    End Sub

    Private Function IsValidPDPItems() As Boolean
        Dim objpdpitem_master As New clspdpitem_master
        Try
            If CInt(drpICategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 1, "Please Select parameter to continue."), Me)
                Return False
            End If

            If CInt(drpIItemType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 2, "Please Select Item Type to continue."), Me)
                Return False
            End If

            If CInt(drpIItemType.SelectedValue) = clsassess_custom_items.enCustomType.SELECTION AndAlso CInt(drpISection.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 3, "Please choose selection type."), Me)
                Return False
            End If

            If CInt(txtIItemText.Text.Trim.Length) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 4, "Please Enter Item Text to continue."), Me)
                Return False
            End If

            If txtISortOrder.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 5, "Please select sort order."), Me)
                Return False
            End If

            If CInt(txtISortOrder.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 6, "Sorry, sort order value should be greater than 0."), Me)
                Return False
            End If

            If objpdpitem_master.isSortOrderExist(CInt(txtISortOrder.Text), CInt(drpICategory.SelectedValue), mintItemUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 7, "Sorry, this sort order is already defined to another item. Please choose another sort order."), Me)
                Return False
            End If


            Dim dslist As DataSet = objpdpitem_master.isPDPTransectionStarted(mintItemUnkid)
            If IsNothing(dslist) = False AndAlso dslist.Tables(0).Rows.Count > 0 Then
                If CInt(drpIItemType.SelectedValue) <> CInt(dslist.Tables(0).Rows(0)("itemtypeid")) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 8, "Sorry, this item is already in use and its item type cannot be changed."), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Function

    Private Sub SetPDPItemsValue(ByRef objItem As clspdpitem_master)
        Try

            objItem._Itemunkid = mintItemUnkid
            objItem._Categoryunkid = CInt(drpICategory.SelectedValue)
            objItem._ItemTypeId = CInt(drpIItemType.SelectedValue)
            objItem._SelectionModeId = CInt(drpISection.SelectedValue)
            objItem._SortOrder = CInt(txtISortOrder.Text)
            objItem._Item = txtIItemText.Text
            objItem._IsCompetencySelectionSet = CBool(chkIIsCompetencySelectionset.Checked)


            objItem._Isactive = True
            objItem._AuditUserId = CInt(Session("UserId"))
            objItem._ClientIP = CStr(Session("IP_ADD"))
            objItem._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objItem._DatabaseName = CStr(Session("Database_Name"))
            objItem._FormName = mstrModuleName3
            objItem._FromWeb = True
            objItem._HostName = CStr(Session("HOST_NAME"))

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            If pnlvisibility.Visible Then
                Select Case radItemvisiblity.SelectedValue
                    Case "1"
                        objItem._Visibletoid = enAssessmentMode.SELF_ASSESSMENT
                    Case "2"
                        objItem._Visibletoid = enAssessmentMode.APPRAISER_ASSESSMENT
                    Case "3"
                        objItem._Visibletoid = enAssessmentMode.REVIEWER_ASSESSMENT
                End Select
            Else
                objItem._Visibletoid = 0
            End If
            'S.SANDEEP |03-MAY-2021| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetPDPItemsValue(ByVal intItemId As Integer)
        Dim objItem As New clspdpitem_master
        Try
            objItem._Itemunkid = mintItemUnkid
            drpICategory.SelectedValue = CStr(objItem._Categoryunkid)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            drpICategory_SelectedIndexChanged(Nothing, Nothing)
            'S.SANDEEP |03-MAY-2021| -- END        
            drpIItemType.SelectedValue = CStr(objItem._ItemTypeId)
            drpIItemType_SelectedIndexChanged(Nothing, Nothing)
            drpISection.SelectedValue = CStr(objItem._SelectionModeId)
            txtISortOrder.Text = CStr(objItem._SortOrder)
            txtIItemText.Text = objItem._Item
            chkIIsCompetencySelectionset.Checked = CBool(objItem._IsCompetencySelectionSet)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            Select Case objItem._Visibletoid
                Case enAssessmentMode.SELF_ASSESSMENT
                    radItemvisiblity.Items(0).Selected = True
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    radItemvisiblity.Items(1).Selected = True
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    radItemvisiblity.Items(2).Selected = True
            End Select
            'S.SANDEEP |03-MAY-2021| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objItem = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Protected Sub btnISave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISave.Click
        Dim objItem As New clspdpitem_master
        Dim blnFlag As Boolean = False
        Dim dsGetList As DataSet = Nothing
        Try
            If IsValidPDPItems() = False Then
                Exit Sub
            End If
            SetPDPItemsValue(objItem)

            dsGetList = objItem.GetList("List")

            If mintItemUnkid > 0 Then
                blnFlag = objItem.Update()
            Else
                blnFlag = objItem.Insert()
            End If

            If blnFlag = False AndAlso objItem._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objItem._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 9, "PDP Item defined successfully."), Me)
                FillPDPItems()
                ClearPDPItemsCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objItem = Nothing
            dsGetList = Nothing
        End Try
    End Sub

    Protected Sub btnIReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIReset.Click
        Try
            Call ClearPDPItemsCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkIEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintItemUnkid = CInt(gvPDPItem.DataKeys(row.RowIndex)("itemunkid"))

            GetPDPItemsValue(mintItemUnkid)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            If drpIItemType.Enabled = False Then drpIItemType.Enabled = True
            If drpISection.Enabled = False Then drpISection.Enabled = True
            If drpICategory.Enabled = False Then drpICategory.Enabled = True
            If pnlvisibility.Enabled = False Then pnlvisibility.Enabled = True

            If CInt(gvPDPItem.DataKeys(row.RowIndex)("idel")) > 0 Then
                drpIItemType.Enabled = False
                drpISection.Enabled = False
                drpICategory.Enabled = False
                pnlvisibility.Enabled = False
            Else
                drpIItemType.Enabled = True
                drpISection.Enabled = True
                drpICategory.Enabled = True
                pnlvisibility.Enabled = True
            End If
            'S.SANDEEP |03-MAY-2021| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkIDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objpdpitem_master As New clspdpitem_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)


            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            If CInt(gvPDPItem.DataKeys(row.RowIndex)("idel")) > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 10, "Sorry, you cannot delete this item, This item is generated by system and will be linked to performance module."), Me)
                Exit Sub
            End If
            'S.SANDEEP |03-MAY-2021| -- END

            mintItemUnkid = CInt(gvPDPItem.DataKeys(row.RowIndex)("itemunkid"))

            If objpdpitem_master.isUsed(mintItemUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 11, "Sorry, Item value is already in used."), Me)
                Exit Sub
            End If

            mstrDeleteAction = "delite"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 12, "You are about to delete this Item. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpitem_master = Nothing
        End Try
    End Sub
#End Region

#Region "Combo Box's Events"

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private Sub drpICategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpICategory.SelectedIndexChanged
        Dim objCategory As New clspdpcategory_master
        Try
            If CInt(drpICategory.SelectedValue) > 0 Then
                objCategory._Categoryunkid = CInt(drpICategory.SelectedValue)
                If objCategory._Isincludeinpm Then
                    pnlvisibility.Visible = True
                Else
                    pnlvisibility.Visible = False
                End If
            Else
                pnlvisibility.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub
    'S.SANDEEP |03-MAY-2021| -- END

    Private Sub drpIItemType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpIItemType.SelectedIndexChanged
        Try
                drpISection.Enabled = False
                drpISection.SelectedIndex = 0
                chkIIsCompetencySelectionset.Enabled = False
                chkIIsCompetencySelectionset.Checked = False

            If CInt(drpIItemType.SelectedValue) = clsassess_custom_items.enCustomType.SELECTION Then
                drpISection.Enabled = True
            ElseIf CInt(drpIItemType.SelectedValue) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                chkIIsCompetencySelectionset.Enabled = True
                'Else
                '    drpISection.Enabled = False
                '    drpISection.SelectedIndex = 0
                '    chkIIsCompetencySelectionset.Enabled = False
                '    chkIIsCompetencySelectionset.Checked = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Gridview Events"
    Protected Sub gvPDPItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPDPItem.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim hfiscompetencyselectionset As HiddenField = CType(e.Row.FindControl("hfiscompetencyselectionset"), HiddenField)
                Dim lblIscompetencyselectionsetTrue As Label = CType(e.Row.FindControl("lblIscompetencyselectionsetTrue"), Label)
                Dim lblIscompetencyselectionsetFalse As Label = CType(e.Row.FindControl("lblIscompetencyselectionsetFalse"), Label)

                If CBool(hfiscompetencyselectionset.Value) Then
                    lblIscompetencyselectionsetTrue.Visible = True
                    lblIscompetencyselectionsetFalse.Visible = False
                Else
                    lblIscompetencyselectionsetTrue.Visible = False
                    lblIscompetencyselectionsetFalse.Visible = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


#End Region

#Region " Action Plan Categories "
#Region " Private Method(s)"

    Private Sub ClearActionPlanCategoriesCtrls()
        Try
            mintActionPlanCategoryUnkid = 0
            txtACategory.Text = ""
            txtASortOrder.Text = "0"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillActionPlanCategories()
        Dim objActionPlanCategory As New clspdpaction_plan_category
        Dim dsList As New DataSet
        Try
            dsList = objActionPlanCategory.GetList("List")
            gvActionPlanCategory.DataSource = dsList.Tables(0)
            gvActionPlanCategory.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objActionPlanCategory = Nothing
        End Try
    End Sub

    Private Function IsValidActionPlanCategories() As Boolean
        Dim objpdpaction_plan_category As New clspdpaction_plan_category
        Try
            If txtACategory.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 1, "Please enter Action Plan to continue."), Me)
                Return False
            End If

            If txtASortOrder.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 2, "Sorry, sort order value should be greater than 0."), Me)
                Return False
            End If


            If CInt(txtASortOrder.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 2, "Sorry, sort order value should be greater than 0."), Me)
                Return False
            End If

            If objpdpaction_plan_category.isSortOrderExist(CInt(txtASortOrder.Text), mintActionPlanCategoryUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 3, "Sorry, this sort order is already defined to another action plan category. Please choose new sort order to continue."), Me)
                Return False
            End If

            Dim dslist As DataSet = objpdpaction_plan_category.isPDPTransectionStarted(mintActionPlanCategoryUnkid)

            If IsNothing(dslist) = False AndAlso dslist.Tables(0).Rows.Count > 0 Then
                If CInt(drpParameterViewType.SelectedValue) <> CInt(dslist.Tables(0).Rows(0)("viewtype")) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 4, "Sorry, this action plan category is already in use and cannot be changed."), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpaction_plan_category = Nothing
        End Try
    End Function

    Private Sub SetActionPlanCategoriesValue(ByRef objActionPlanCategory As clspdpaction_plan_category)
        Try
            objActionPlanCategory._ActionPlanCategoryunkid = mintActionPlanCategoryUnkid
            objActionPlanCategory._Category = txtACategory.Text
            objActionPlanCategory._SortOrder = CInt(txtASortOrder.Text)
            objActionPlanCategory._AuditUserId = CInt(Session("UserId"))
            objActionPlanCategory._ClientIP = CStr(Session("IP_ADD"))
            objActionPlanCategory._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objActionPlanCategory._DatabaseName = CStr(Session("Database_Name"))
            objActionPlanCategory._FormName = mstrModuleName4
            objActionPlanCategory._FromWeb = True
            objActionPlanCategory._HostName = CStr(Session("HOST_NAME"))
            objActionPlanCategory._Isactive = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetActionPlanCategoriesValue(ByVal intActionPlanCategoryId As Integer)
        Dim objActionPlanCategory As New clspdpaction_plan_category
        Try
            objActionPlanCategory._ActionPlanCategoryunkid = mintActionPlanCategoryUnkid
            txtACategory.Text = CStr(objActionPlanCategory._Category)
            txtASortOrder.Text = CStr(objActionPlanCategory._SortOrder)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objActionPlanCategory = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Protected Sub btnASave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnASave.Click
        Dim objActionPlanCategory As New clspdpaction_plan_category
        Dim blnFlag As Boolean = False
        Try
            If IsValidActionPlanCategories() = False Then
                Exit Sub
            End If
            SetActionPlanCategoriesValue(objActionPlanCategory)
            If mintActionPlanCategoryUnkid > 0 Then
                blnFlag = objActionPlanCategory.Update()
            Else
                blnFlag = objActionPlanCategory.Insert()
            End If

            If blnFlag = False AndAlso objActionPlanCategory._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objActionPlanCategory._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 5, "Action Plan Category defined successfully."), Me)
                FillActionPlanCategories()
                ClearActionPlanCategoriesCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objActionPlanCategory = Nothing
        End Try
    End Sub

    Protected Sub btnAReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAReset.Click
        Try
            Call ClearActionPlanCategoriesCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintActionPlanCategoryUnkid = CInt(gvActionPlanCategory.DataKeys(row.RowIndex)("actionplancategoryunkid"))
            GetActionPlanCategoriesValue(mintActionPlanCategoryUnkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkADelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objpdpaction_plan_category As New clspdpaction_plan_category
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintActionPlanCategoryUnkid = CInt(gvActionPlanCategory.DataKeys(row.RowIndex)("actionplancategoryunkid"))

            If objpdpaction_plan_category.isUsed(mintActionPlanCategoryUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 6, "Sorry, Category is already in used."), Me)
                Exit Sub
            End If


            mstrDeleteAction = "delapcat"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 7, "You are about to delete this action plan category. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdpaction_plan_category = Nothing
        End Try
    End Sub

#End Region
#End Region

#Region "Category/Item Mapping "
#Region "Private Method"
    'Private Sub FillCategoryItemMappingSetting()
    '    Try
    '        Dim objpdpsettings_master As New clspdpsettings_master
    '        Dim blnFlag As Boolean = False
    '        Try
    '            Dim PDPSetting As Dictionary(Of clspdpsettings_master.enPDPConfiguration, String) = objpdpsettings_master.GetSetting()
    '            If IsNothing(PDPSetting) = False Then
    '                For Each kvp As KeyValuePair(Of clspdpsettings_master.enPDPConfiguration, String) In PDPSetting
    '                    Select Case kvp.Key
    '                        Case clspdpsettings_master.enPDPConfiguration.CategoryItemMapping

    '                            Dim mapping As String = kvp.Value.ToString()
    '                            Dim mappingValues As String() = mapping.Split(CChar("|"))

    '                            If mappingValues(0).Length > 0 Then
    '                                drpCategoryItemMappingCategory.SelectedValue = mappingValues(0)
    '                                drpCategoryItemMappingCategory_SelectedIndexChanged(Nothing, Nothing)
    '                                drpCategoryItemMappingItems.SelectedValue = mappingValues(1)
    '                            End If
    '                    End Select
    '                Next
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            objpdpsettings_master = Nothing
    '        End Try

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Private Sub SaveCategoryItemMappingSetting()
    '    Dim objsetting As New clspdpsettings_master
    '    Dim blnFlag As Boolean = False
    '    Try
    '        objsetting._AuditUserId = CInt(Session("UserId"))
    '        objsetting._ClientIP = CStr(Session("IP_ADD"))
    '        objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
    '        objsetting._DatabaseName = CStr(Session("Database_Name"))
    '        objsetting._FormName = mstrModuleName2
    '        objsetting._FromWeb = True
    '        objsetting._HostName = CStr(Session("HOST_NAME"))

    '        Dim pdpSetting As New Dictionary(Of clspdpsettings_master.enPDPConfiguration, String)

    '        pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.CategoryItemMapping, drpCategoryItemMappingCategory.SelectedValue & "|" & drpCategoryItemMappingItems.SelectedValue)

    '        objsetting._DatabaseName = CStr(Session("Database_Name"))
    '        blnFlag = objsetting.SavePDPSetting(pdpSetting)

    '        If blnFlag = False AndAlso objsetting._Message.Trim.Length > 0 Then
    '            DisplayMessage.DisplayMessage(objsetting._Message, Me)
    '        Else
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 1011, "Mapping save successfully."), Me)
    '        End If


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objsetting = Nothing
    '    End Try

    'End Sub



    Private Sub ClearPDPCategoryItemMapping()
            Try
            mintCategoryItemmappingid = 0
            drpCategoryItemMappingCategory.SelectedValue = "0"
            drpCategoryItemMappingItems.SelectedValue = "0"

            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            End Try
    End Sub

    Private Sub FillPDPCategoryItemMapping()
        Dim objpdp_categoryitem_mapping As New clspdp_categoryitem_mapping
        Dim dsList As New DataSet
        Try
            dsList = objpdp_categoryitem_mapping.GetList("List")
            gvPDPCIMapping.DataSource = dsList.Tables(0)
            gvPDPCIMapping.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdp_categoryitem_mapping = Nothing
        End Try
    End Sub

    Private Function IsValidPDPCategoryItemMapping() As Boolean
        Try
            If drpCategoryItemMappingCategory.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 1, "Please select Parameter to continue."), Me)
                Return False
            End If


            If drpCategoryItemMappingItems.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 2, "Please select Item to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetPDPCategoryItemMappingValue(ByRef objpdp_categoryitem_mapping As clspdp_categoryitem_mapping)
        Try
            objpdp_categoryitem_mapping._CategoryItemMappingid = mintCategoryUnkid
            objpdp_categoryitem_mapping._CategoryId = CInt(drpCategoryItemMappingCategory.SelectedValue)
            objpdp_categoryitem_mapping._ItemId = CInt(drpCategoryItemMappingItems.SelectedValue)
            objpdp_categoryitem_mapping._IsVoid = False
            SetAT_PDPCategoryItemMappingValue(objpdp_categoryitem_mapping)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAT_PDPCategoryItemMappingValue(ByRef objpdp_categoryitem_mapping As clspdp_categoryitem_mapping)
        Try
            objpdp_categoryitem_mapping._AuditUserId = CInt(Session("UserId"))
            objpdp_categoryitem_mapping._ClientIP = CStr(Session("IP_ADD"))
            objpdp_categoryitem_mapping._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objpdp_categoryitem_mapping._DatabaseName = CStr(Session("Database_Name"))
            objpdp_categoryitem_mapping._FormName = mstrModuleName4
            objpdp_categoryitem_mapping._FromWeb = True
            objpdp_categoryitem_mapping._HostName = CStr(Session("HOST_NAME"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Sub GetPDPCategoryItemMappingValue(ByVal intCategoryItemMappingid As Integer)
        Dim objpdp_categoryitem_mapping As New clspdp_categoryitem_mapping
        Try
            objpdp_categoryitem_mapping._CategoryItemMappingid = intCategoryItemMappingid
            drpCategoryItemMappingCategory.SelectedValue = CStr(objpdp_categoryitem_mapping._CategoryId)
            drpCategoryItemMappingCategory_SelectedIndexChanged(Nothing, Nothing)
            drpCategoryItemMappingItems.SelectedValue = CStr(objpdp_categoryitem_mapping._ItemId)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdp_categoryitem_mapping = Nothing
        End Try
    End Sub

#End Region

#Region " Button Method(s)"

    'Protected Sub btnCategoryItemMappingSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategoryItemMappingSave.Click
    '    Dim objpdpitem_master As New clspdpitem_master
    '    Dim blnFlag As Boolean = False
    '    Try

    '        objpdpitem_master._Itemunkid = CInt(drpCategoryItemMappingItems.SelectedValue)
    '        If CInt(clspdpitem_master.enPdpCustomType.FREE_TEXT) <> objpdpitem_master._ItemTypeId Then
    '            cnfConfirmationMapping.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 1011, "Are you sure this item is not free text still you want to save this mapping.")
    '            cnfConfirmationMapping.Show()
    '            Exit Sub
    '        Else
    '            SaveCategoryItemMappingSetting()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objpdpitem_master = Nothing
    '    End Try
    'End Sub

    Protected Sub btnCategoryItemMappingSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategoryItemMappingSave.Click
        Dim objpdp_categoryitem_mapping As New clspdp_categoryitem_mapping
        Dim blnFlag As Boolean = False
        Try
            If IsValidPDPCategoryItemMapping() = False Then
                Exit Sub
            End If
            SetPDPCategoryItemMappingValue(objpdp_categoryitem_mapping)
            If mintCategoryItemmappingid > 0 Then
                blnFlag = objpdp_categoryitem_mapping.Update()
            Else
                blnFlag = objpdp_categoryitem_mapping.Insert()
            End If

            If blnFlag = False AndAlso objpdp_categoryitem_mapping._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objpdp_categoryitem_mapping._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 3, "Parameter/Item Mapping defined successfully."), Me)
                FillPDPCategoryItemMapping()
                ClearPDPCategoryItemMapping()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpdp_categoryitem_mapping = Nothing
        End Try
    End Sub

    Protected Sub btnCategoryItemMappingReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategoryItemMappingReset.Click
        Try
            Call ClearPDPCategoriesCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Drropdown Method(s)"
    Protected Sub drpCategoryItemMappingCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpCategoryItemMappingCategory.SelectedIndexChanged
        Dim objpdpitem_master As New clspdpitem_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objpdpitem_master.getComboList("List", True, CInt(drpCategoryItemMappingCategory.SelectedValue))
            With drpCategoryItemMappingItems
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Link Event(s) "

    Protected Sub lnkCIEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintCategoryItemmappingid = CInt(gvPDPCIMapping.DataKeys(row.RowIndex)("categoryitemmappingid"))
            GetPDPCategoryItemMappingValue(mintCategoryItemmappingid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkCIDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objppdp_categoryitem_mapping As New clspdp_categoryitem_mapping
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintCategoryItemmappingid = CInt(gvPDPCIMapping.DataKeys(row.RowIndex)("categoryitemmappingid"))
            objppdp_categoryitem_mapping._CategoryItemMappingid = mintCategoryItemmappingid

            If objppdp_categoryitem_mapping.isUsed(objppdp_categoryitem_mapping._CategoryId, objppdp_categoryitem_mapping._ItemId) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 4, "Sorry, this mapping is already in use."), Me)
                Exit Sub
            End If

            mstrDeleteAction = "delcimapping"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 5, "You are about to delete this mapping. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

#End Region
#End Region

#Region "Instrunction "
#Region "Private Method"
    Private Sub FillInstructionSetting()
        Try
            Dim objpdpsettings_master As New clspdpsettings_master
            Dim blnFlag As Boolean = False
            Try
                Dim PDPSetting As Dictionary(Of clspdpsettings_master.enPDPConfiguration, String) = objpdpsettings_master.GetSetting()
                If IsNothing(PDPSetting) = False Then
                    For Each kvp As KeyValuePair(Of clspdpsettings_master.enPDPConfiguration, String) In PDPSetting
                        Select Case kvp.Key
                            Case clspdpsettings_master.enPDPConfiguration.INSTRUCTION
                                txtInstruction.Text = kvp.Value.ToString()
                        End Select
                    Next
                End If
            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            Finally
                objpdpsettings_master = Nothing
            End Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidPDPInstruction() As Boolean
        Try
            If txtInstruction.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName8, 2, "Please add instruction to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button Method(s)"
    Protected Sub btnInstruction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInstruction.Click
        Dim objsetting As New clspdpsettings_master
        Dim blnFlag As Boolean = False
        Try

            If IsValidPDPInstruction() = False Then
                Exit Sub
            End If

            objsetting._AuditUserId = CInt(Session("UserId"))
            objsetting._ClientIP = CStr(Session("IP_ADD"))
            objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            objsetting._FormName = mstrModuleName2
            objsetting._FromWeb = True
            objsetting._HostName = CStr(Session("HOST_NAME"))

            Dim pdpSetting As New Dictionary(Of clspdpsettings_master.enPDPConfiguration, String)

            pdpSetting.Add(clspdpsettings_master.enPDPConfiguration.INSTRUCTION, txtInstruction.Text)

            objsetting._DatabaseName = CStr(Session("Database_Name"))
            blnFlag = objsetting.SavePDPSetting(pdpSetting)

            If blnFlag = False AndAlso objsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objsetting._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName8, 1, "Instruction Saved Successfully."), Me)
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsetting = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region "Confirmation"
    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELLEVEL"
                    Dim objpdpreviewerlevel_master As New clspdpreviewerlevel_master
                    SetReviewerLevelValue(objpdpreviewerlevel_master)
                    objpdpreviewerlevel_master._Isactive = False
                    blnFlag = objpdpreviewerlevel_master.Delete(mintReviewerLevelUnkid)
                    If blnFlag = False AndAlso objpdpreviewerlevel_master._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objpdpreviewerlevel_master._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Reviewer Level deleted successfully."), Me)
                        ClearReviewerLevelCtrls()
                        FillReviewerLevel()
                    End If
                    objpdpreviewerlevel_master = Nothing
                Case "INACTIVEREV"
                    Dim objReviewer As New clspdpreviewer_master
                    SetReviewersValue(objReviewer)
                    blnFlag = objReviewer.ActiveInactiveReviewer(mintReviewerUnkid, False)
                    If blnFlag = False AndAlso objReviewer._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objReviewer._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 8, "Reviewer deactivated successfully."), Me)
                        ClearReviewersCtrls()
                        FillReviewers()
                    End If
                    objReviewer = Nothing

                Case "ACTIVEREV"
                    Dim objReviewer As New clspdpreviewer_master
                    SetReviewersValue(objReviewer)
                    blnFlag = objReviewer.ActiveInactiveReviewer(mintReviewerUnkid, True)
                    If blnFlag = False AndAlso objReviewer._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objReviewer._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 9, "Reviewer Activated successfully."), Me)
                        ClearReviewersCtrls()
                        FillReviewers()
                    End If
                    objReviewer = Nothing

                Case "DELREV"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 7, "Reviewer delete reason.")
                    delReason.Show()

                Case "DELCAT"
                    Dim objCategory As New clspdpcategory_master
                    SetPDPCategoriesValue(objCategory)
                    objCategory._Isactive = False
                    blnFlag = objCategory.Delete(mintCategoryUnkid)
                    If blnFlag = False AndAlso objCategory._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objCategory._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName5, 11, "PDP parameter deleted successfully."), Me)
                        ClearPDPCategoriesCtrls()
                        FillPDPCategories()
                    End If
                    objCategory = Nothing
                Case "DELAPCAT"
                    Dim objActionPlanCategory As New clspdpaction_plan_category
                    SetActionPlanCategoriesValue(objActionPlanCategory)
                    objActionPlanCategory._Isactive = False
                    blnFlag = objActionPlanCategory.Delete(mintActionPlanCategoryUnkid)
                    If blnFlag = False AndAlso objActionPlanCategory._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objActionPlanCategory._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 8, "Action Plan Category deleted successfully."), Me)
                        ClearActionPlanCategoriesCtrls()
                        FillActionPlanCategories()
                    End If
                    objActionPlanCategory = Nothing
                Case "DELITE"
                    Dim objItem As New clspdpitem_master
                    SetPDPItemsValue(objItem)
                    objItem._Isactive = False
                    blnFlag = objItem.Delete(mintItemUnkid)
                    If blnFlag = False AndAlso objItem._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objItem._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName6, 13, "PDP Item deleted successfully."), Me)
                        ClearPDPItemsCtrls()
                        FillPDPItems()
                    End If
                    objItem = Nothing

                Case "DELCIMAPPING"
                    Dim objpdp_categoryitem_mapping As New clspdp_categoryitem_mapping

                    SetAT_PDPCategoryItemMappingValue(objpdp_categoryitem_mapping)
                    objpdp_categoryitem_mapping._IsVoid = True

                    blnFlag = objpdp_categoryitem_mapping.Delete(mintCategoryItemmappingid)
                    If blnFlag = False AndAlso objpdp_categoryitem_mapping._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objpdp_categoryitem_mapping._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName7, 6, "Mapping deleted successfully."), Me)
                        ClearPDPCategoryItemMapping()
                        FillPDPCategoryItemMapping()

                    End If
                    objpdp_categoryitem_mapping = Nothing

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Delete Reason"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()

                Case "DELREV"
                    Dim objReviewer As New clspdpreviewer_master
                    SetReviewersValue(objReviewer)
                    objReviewer._Isvoid = True
                    objReviewer._Voidreason = delReason.Reason
                    objReviewer._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objReviewer._Voiduserunkid = CInt(Session("UserId"))
                    blnFlag = objReviewer.Delete(mintReviewerUnkid)
                    If blnFlag = False AndAlso objReviewer._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objReviewer._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName3, 10, "Reviewer deleted successfully."), Me)
                        ClearReviewersCtrls()
                        FillReviewers()
                    End If
                    objReviewer = Nothing

                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPDPInstruction.ID, Me.lblPDPInstruction.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblReviewerLevels.ID, Me.lblReviewerLevels.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEvaluators.ID, Me.lblEvaluators.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblReviewers.ID, Me.lblReviewers.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPDPCategories.ID, Me.lblPDPCategories.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPDPItems.ID, Me.lblPDPItems.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPDPItemsMapping.ID, Me.lblPDPItemsMapping.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblActionPlanCategories.ID, Me.lblActionPlanCategories.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.BtnClose.ID, Me.BtnClose.Text)

            'Instructions
            'Language.setLanguage(mstrModuleName8)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName8, Me.lblInstructionHeader.ID, Me.lblInstructionHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName8, Me.btnInstruction.ID, Me.btnInstruction.Text)

            'Reviewer Levels
            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblLLevel.ID, Me.lblLLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblLlevelcode.ID, Me.lblLlevelcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblLlevelname.ID, Me.lblLlevelname.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblLlevelpriority.ID, Me.lblLlevelpriority.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnLSave.ID, Me.btnLSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnLReset.ID, Me.btnLReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.gvReviewerLevel.Columns(2).FooterText, Me.gvReviewerLevel.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.gvReviewerLevel.Columns(3).FooterText, Me.gvReviewerLevel.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.gvReviewerLevel.Columns(4).FooterText, Me.gvReviewerLevel.Columns(4).HeaderText)
            
            'Evaluators
            'Language.setLanguage(mstrModuleName2)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblEEvaluators.ID, Me.lblEEvaluators.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.lblPeerTitle.ID, Me.lblPeerTitle.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.chkSelf.ID, Me.chkSelf.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.chkLineManager.ID, Me.chkLineManager.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.chkPeers.ID, Me.chkPeers.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.btnESave.ID, Me.btnESave.Text)
            
            'Reviewers
            'Language.setLanguage(mstrModuleName3)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblRReviewer.ID, Me.lblRReviewer.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblRLevel.ID, Me.lblRLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblUser.ID, Me.lblUser.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.lblAllocation.ID, Me.lblAllocation.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, bntRSave.ID, Me.bntRSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, bntRReset.ID, Me.bntRReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.gvReviewers.Columns(3).FooterText, Me.gvReviewers.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.gvReviewers.Columns(4).FooterText, Me.gvReviewers.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, Me.gvReviewers.Columns(5).FooterText, Me.gvReviewers.Columns(5).HeaderText)

            'Categories
            'Language.setLanguage(mstrModuleName5)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblPDPCategory.ID, Me.lblPDPCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblPCategory.ID, Me.lblPCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblParameterViewType.ID, Me.lblParameterViewType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.lblPSortOrder.ID, Me.lblPSortOrder.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.chkIncludeInPM.ID, Me.chkIncludeInPM.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.chkMakeMandatory.ID, Me.chkMakeMandatory.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.btnPSave.ID, Me.btnPSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.btnPReset.ID, Me.btnPReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.gvCategory.Columns(2).FooterText, Me.gvCategory.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.gvCategory.Columns(3).FooterText, Me.gvCategory.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, Me.gvCategory.Columns(4).FooterText, Me.gvCategory.Columns(4).HeaderText)

            'Items
            'Language.setLanguage(mstrModuleName6)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblIPDPItem.ID, Me.lblIPDPItem.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblICategory.ID, Me.lblICategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblIItemType.ID, Me.lblIItemType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblISortOrder.ID, Me.lblISortOrder.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblIItemText.ID, Me.lblIItemText.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.lblItemVisibilty.ID, Me.lblItemVisibilty.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.chkIIsCompetencySelectionset.ID, Me.chkIIsCompetencySelectionset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.btnISave.ID, Me.btnISave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.btnIReset.ID, Me.btnIReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.gvPDPItem.Columns(2).FooterText, Me.gvPDPItem.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.gvPDPItem.Columns(3).FooterText, Me.gvPDPItem.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.gvPDPItem.Columns(4).FooterText, Me.gvPDPItem.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.gvPDPItem.Columns(5).FooterText, Me.gvPDPItem.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.gvPDPItem.Columns(7).FooterText, Me.gvPDPItem.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, Me.gvPDPItem.Columns(8).FooterText, Me.gvPDPItem.Columns(8).HeaderText)
            
            'Parameter/Item Mapping
            'Language.setLanguage(mstrModuleName7)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.lblPDPCategoryItemMapping.ID, Me.lblPDPCategoryItemMapping.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.lblCategoryItemMappingCategory.ID, Me.lblCategoryItemMappingCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.lblCategoryItemMappingItems.ID, Me.lblCategoryItemMappingItems.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.btnCategoryItemMappingSave.ID, Me.btnCategoryItemMappingSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.btnCategoryItemMappingReset.ID, Me.btnCategoryItemMappingReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.gvPDPCIMapping.Columns(2).FooterText, Me.gvPDPCIMapping.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, Me.gvPDPCIMapping.Columns(3).FooterText, Me.gvPDPCIMapping.Columns(3).HeaderText)

            'Action Plan Categories
            'Language.setLanguage(mstrModuleName4)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblActionPlanCategory.ID, Me.lblActionPlanCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblACategory.ID, Me.lblACategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.lblASortOrder.ID, Me.lblASortOrder.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.btnASave.ID, Me.btnASave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.btnAReset.ID, Me.btnAReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.gvActionPlanCategory.Columns(2).FooterText, Me.gvActionPlanCategory.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, Me.gvActionPlanCategory.Columns(3).FooterText, Me.gvActionPlanCategory.Columns(3).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPDPInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPDPInstruction.ID, Me.lblPDPInstruction.Text)
            Me.lblReviewerLevels.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReviewerLevels.ID, Me.lblReviewerLevels.Text)
            Me.lblEvaluators.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEvaluators.ID, Me.lblEvaluators.Text)
            Me.lblReviewers.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReviewers.ID, Me.lblReviewers.Text)
            Me.lblPDPCategories.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPDPCategories.ID, Me.lblPDPCategories.Text)
            Me.lblPDPItems.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPDPItems.ID, Me.lblPDPItems.Text)
            Me.lblPDPItemsMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPDPItemsMapping.ID, Me.lblPDPItemsMapping.Text)
            Me.lblActionPlanCategories.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblActionPlanCategories.ID, Me.lblActionPlanCategories.Text)

            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text)

            'Instructions
            'Language.setLanguage(mstrModuleName8)
            Me.lblInstructionHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName8, CInt(HttpContext.Current.Session("LangId")), Me.lblInstructionHeader.ID, Me.lblInstructionHeader.Text)

            Me.btnInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName8, CInt(HttpContext.Current.Session("LangId")), Me.btnInstruction.ID, Me.btnInstruction.Text)

            'Reviewer Levels
            'Language.setLanguage(mstrModuleName1)
            Me.lblLLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblLLevel.ID, Me.lblLLevel.Text)
            Me.lblLlevelcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblLlevelcode.ID, Me.lblLlevelcode.Text)
            Me.lblLlevelname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblLlevelname.ID, Me.lblLlevelname.Text)
            Me.lblLlevelpriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblLlevelpriority.ID, Me.lblLlevelpriority.Text)

            Me.btnLSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnLSave.ID, Me.btnLSave.Text)
            Me.btnLReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnLReset.ID, Me.btnLReset.Text)

            Me.gvReviewerLevel.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.gvReviewerLevel.Columns(2).FooterText, Me.gvReviewerLevel.Columns(2).HeaderText)
            Me.gvReviewerLevel.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.gvReviewerLevel.Columns(3).FooterText, Me.gvReviewerLevel.Columns(3).HeaderText)
            Me.gvReviewerLevel.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.gvReviewerLevel.Columns(4).FooterText, Me.gvReviewerLevel.Columns(4).HeaderText)

            'Evaluators
            'Language.setLanguage(mstrModuleName2)
            Me.lblEEvaluators.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblEEvaluators.ID, Me.lblEEvaluators.Text)
            Me.lblPeerTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.lblPeerTitle.ID, Me.lblPeerTitle.Text)

            Me.chkSelf.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.chkSelf.ID, Me.chkSelf.Text)
            Me.chkLineManager.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.chkLineManager.ID, Me.chkLineManager.Text)
            Me.chkPeers.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.chkPeers.ID, Me.chkPeers.Text)

            Me.btnESave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.btnESave.ID, Me.btnESave.Text)

            'Reviewers
            'Language.setLanguage(mstrModuleName3)
            Me.lblRReviewer.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblRReviewer.ID, Me.lblRReviewer.Text)
            Me.lblRLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblRLevel.ID, Me.lblRLevel.Text)
            Me.lblUser.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblUser.ID, Me.lblUser.Text)
            Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocation.ID, Me.lblAllocation.Text)

            Me.bntRSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), bntRSave.ID, Me.bntRSave.Text)
            Me.bntRReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), bntRReset.ID, Me.bntRReset.Text)

            Me.gvReviewers.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.gvReviewers.Columns(3).FooterText, Me.gvReviewers.Columns(3).HeaderText)
            Me.gvReviewers.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.gvReviewers.Columns(4).FooterText, Me.gvReviewers.Columns(4).HeaderText)
            Me.gvReviewers.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, CInt(HttpContext.Current.Session("LangId")), Me.gvReviewers.Columns(5).FooterText, Me.gvReviewers.Columns(5).HeaderText)

            'Categories
            'Language.setLanguage(mstrModuleName5)
            Me.lblPDPCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblPDPCategory.ID, Me.lblPDPCategory.Text)
            Me.lblPCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblPCategory.ID, Me.lblPCategory.Text)
            Me.lblParameterViewType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblParameterViewType.ID, Me.lblParameterViewType.Text)
            Me.lblPSortOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.lblPSortOrder.ID, Me.lblPSortOrder.Text)

            Me.chkIncludeInPM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeInPM.ID, Me.chkIncludeInPM.Text)
            Me.chkMakeMandatory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.chkMakeMandatory.ID, Me.chkMakeMandatory.Text)

            Me.btnPSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.btnPSave.ID, Me.btnPSave.Text)
            Me.btnPReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.btnPReset.ID, Me.btnPReset.Text)

            Me.gvCategory.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.gvCategory.Columns(2).FooterText, Me.gvCategory.Columns(2).HeaderText)
            Me.gvCategory.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.gvCategory.Columns(3).FooterText, Me.gvCategory.Columns(3).HeaderText)
            Me.gvCategory.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, CInt(HttpContext.Current.Session("LangId")), Me.gvCategory.Columns(4).FooterText, Me.gvCategory.Columns(4).HeaderText)

            'Items
            'Language.setLanguage(mstrModuleName6)
            Me.lblIPDPItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblIPDPItem.ID, Me.lblIPDPItem.Text)
            Me.lblICategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblICategory.ID, Me.lblICategory.Text)
            Me.lblIItemType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblIItemType.ID, Me.lblIItemType.Text)
            Me.lblISortOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblISortOrder.ID, Me.lblISortOrder.Text)
            Me.lblIItemText.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblIItemText.ID, Me.lblIItemText.Text)
            Me.lblItemVisibilty.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.lblItemVisibilty.ID, Me.lblItemVisibilty.Text)

            Me.chkIIsCompetencySelectionset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.chkIIsCompetencySelectionset.ID, Me.chkIIsCompetencySelectionset.Text)

            Me.btnISave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.btnISave.ID, Me.btnISave.Text)
            Me.btnIReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.btnIReset.ID, Me.btnIReset.Text)

            Me.gvPDPItem.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPItem.Columns(2).FooterText, Me.gvPDPItem.Columns(2).HeaderText)
            Me.gvPDPItem.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPItem.Columns(3).FooterText, Me.gvPDPItem.Columns(3).HeaderText)
            Me.gvPDPItem.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPItem.Columns(4).FooterText, Me.gvPDPItem.Columns(4).HeaderText)
            Me.gvPDPItem.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPItem.Columns(5).FooterText, Me.gvPDPItem.Columns(5).HeaderText)
            Me.gvPDPItem.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPItem.Columns(7).FooterText, Me.gvPDPItem.Columns(7).HeaderText)
            Me.gvPDPItem.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPItem.Columns(8).FooterText, Me.gvPDPItem.Columns(8).HeaderText)

            'Parameter/Item Mapping
            'Language.setLanguage(mstrModuleName7)
            Me.lblPDPCategoryItemMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.lblPDPCategoryItemMapping.ID, Me.lblPDPCategoryItemMapping.Text)
            Me.lblCategoryItemMappingCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.lblCategoryItemMappingCategory.ID, Me.lblCategoryItemMappingCategory.Text)
            Me.lblCategoryItemMappingItems.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.lblCategoryItemMappingItems.ID, Me.lblCategoryItemMappingItems.Text)

            Me.btnCategoryItemMappingSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.btnCategoryItemMappingSave.ID, Me.btnCategoryItemMappingSave.Text)
            Me.btnCategoryItemMappingReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.btnCategoryItemMappingReset.ID, Me.btnCategoryItemMappingReset.Text)

            Me.gvPDPCIMapping.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPCIMapping.Columns(2).FooterText, Me.gvPDPCIMapping.Columns(2).HeaderText)
            Me.gvPDPCIMapping.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, CInt(HttpContext.Current.Session("LangId")), Me.gvPDPCIMapping.Columns(3).FooterText, Me.gvPDPCIMapping.Columns(3).HeaderText)

            'Action Plan Categories
            'Language.setLanguage(mstrModuleName4)
            Me.lblActionPlanCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblActionPlanCategory.ID, Me.lblActionPlanCategory.Text)
            Me.lblACategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblACategory.ID, Me.lblACategory.Text)
            Me.lblASortOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblASortOrder.ID, Me.lblASortOrder.Text)

            Me.btnASave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.btnASave.ID, Me.btnASave.Text)
            Me.btnAReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.btnAReset.ID, Me.btnAReset.Text)

            Me.gvActionPlanCategory.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.gvActionPlanCategory.Columns(2).FooterText, Me.gvActionPlanCategory.Columns(2).HeaderText)
            Me.gvActionPlanCategory.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.gvActionPlanCategory.Columns(3).FooterText, Me.gvActionPlanCategory.Columns(3).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Please enter Level code to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Please enter Level name to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Level defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "You are about to delete this Level. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 5, "Reviewer Level deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 1, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 2, "Please select at lease one allocation to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 3, "Feedback settings saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 1, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 2, "Please Select User to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 3, "Reviewer defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 4, "You are about to delete this Reviewer. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 5, "You are about to deactivate this reviewer. Are you sure you want to deactivate?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 6, "You are about to activate this reviewer. Are you sure you want to activate?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 7, "Reviewer delete reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 8, "Reviewer deactivated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 9, "Reviewer Activated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName3, 10, "Reviewer deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 1, "Please enter Action Plan to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 2, "Sorry, sort order value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 3, "Sorry, this sort order is already defined to another action plan category. Please choose new sort order to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 4, "Sorry, this action plan category is already in use and cannot be changed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 5, "Action Plan Category defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 6, "Sorry, Category is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 7, "You are about to delete this action plan category. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 8, "Action Plan Category deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 1, "Please enter Parameter to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 2, "Please enter sort order to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 3, "Sorry, sort order value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 4, "Please enter Parameter Type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 5, "Sorry, this sort order is already selected for another parameter.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 6, "Sorry, this parameter is already in use and its parameter type cannot be changed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 7, "PDP parameter defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 8, "Sorry, you cannot delete this category, This category is generated by system and will be linked to performance module.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 9, "Sorry, this parameter is already in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 10, "You are about to delete this parameter. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName5, 11, "PDP parameter deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 1, "Please Select parameter to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 2, "Please Select Item Type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 3, "Please choose selection type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 4, "Please Enter Item Text to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 5, "Please select sort order.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 6, "Sorry, sort order value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 7, "Sorry, this sort order is already defined to another item. Please choose another sort order.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 8, "Sorry, this item is already in use and its item type cannot be changed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 9, "PDP Item defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 10, "Sorry, you cannot delete this item, This item is generated by system and will be linked to performance module.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 11, "Sorry, Item value is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 12, "You are about to delete this Item. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName6, 13, "PDP Item deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, 1, "Please select Parameter to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, 2, "Please select Item to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, 3, "Parameter/Item Mapping defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, 4, "Sorry, this mapping is already in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, 5, "You are about to delete this mapping. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName7, 6, "Mapping deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName8, 1, "Instruction Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName8, 2, "Please add instruction to continue.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
