﻿#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region
Partial Class PDP_wPg_CoachingNominationApprovalList
    Inherits Basepage

#Region " Private Variable "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmCoachingNominationApprovalList"

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()

                FillCombo()
                SetApprovalData()
                FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objCoachingNominationForm As New clsCoaching_Nomination_Form
        Dim dsCombo As New DataSet
        Try
            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                           Session("UserAccessModeSetting").ToString(), True, _
                                           False, "List", True)
            With cboCoacheeEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objCoachingNominationForm.getStatusComboList("List", True)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing
            objCoachingNominationForm = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objApprovaltran As New clscoachingapproval_process_tran
        Dim dtApprovalList As New DataTable
        Dim mstrEmployeeIDs As String = ""
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing


            If CInt(cboCoacheeEmployee.SelectedValue) > 0 Then
                strSearch &= "AND pdpcoaching_nomination_form.coachee_employeeunkid = " & CInt(cboCoacheeEmployee.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND pdpcoaching_nomination_form.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If


            strSearch &= "AND pdpcoachingapproval_process_tran.visibleid <> -1 " & " "

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objApprovaltran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                              "List", strSearch, "")

            Dim mintCoachNominationunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each drRow As DataRow In dsList.Tables(0).Rows

                    If CInt(drRow("pendingcoachingtranunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintCoachNominationunkid <> CInt(drRow("coachingnominationunkid")) Then
                        dList = New DataView(dsList.Tables(0), "coachee_employeeunkid = " & CInt(drRow("coachee_employeeunkid")) & " AND coachingnominationunkid = " & CInt(drRow("coachingnominationunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintCoachNominationunkid = CInt(drRow("coachingnominationunkid"))
                        mstrStaus = ""
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 1 Then

                                    If CInt(dr(i)("statusunkid")) = 2 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Rejected By :-  ") & drRow("ApproverName").ToString()
                                    Exit For

                                End If

                            Next
                            

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next

            End If

            dtApprovalList = dsList.Tables(0).Clone

            dtApprovalList.Columns("application_date").DataType = GetType(Object)

            Dim dtRow As DataRow

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dtRow = dtApprovalList.NewRow
                For Each dtCol As DataColumn In dtApprovalList.Columns
                    dtRow(dtCol.ColumnName) = dRow(dtCol.ColumnName)
                Next

                dtApprovalList.Rows.Add(dtRow)
            Next

            dtApprovalList.AcceptChanges()

            Dim dtTable As New DataTable
            If chkMyApproval.Checked Then
                dtTable = New DataView(dtApprovalList, "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dtApprovalList, "", "", DataViewRowState.CurrentRows).ToTable
            End If


            Dim xRow() As DataRow = Nothing
            Dim dt As New DataTable
            Dim xTable As New DataTable
            Dim dtFinalTable As New DataTable
            dt = New DataView(dtTable, "mapuserunkid = " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable
            Dim dtTemp As DataTable = New DataView(dt, "", "", DataViewRowState.CurrentRows).ToTable(True, "coachingnominationunkid", "coachee_employeeunkid")
            For Each drRow As DataRow In dtTemp.Rows
                xRow = dtTable.Select("coachingnominationunkid =" & drRow.Item("coachingnominationunkid"))
                If xRow.Length > 0 Then
                    xTable = xRow.CopyToDataTable()
                    dtFinalTable.Merge(xTable, True)
                End If
            Next
            dt = Nothing
            xTable = Nothing

            gvCoachingApprovalList.AutoGenerateColumns = False
            gvCoachingApprovalList.DataSource = dtFinalTable
            gvCoachingApprovalList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
        End Try
    End Sub

    Private Sub SetApprovalData()
        Dim dsList As New DataSet
        Try
            Dim objApprover As New clscoaching_role_mapping
            dsList = objApprover.GetList("List", True, "", Nothing, CInt(Session("RoleId")))

            If dsList.Tables("List").Rows.Count > 0 Then
                txtRole.Text = dsList.Tables("List").Rows(0).Item("role").ToString()
                txtRole.Attributes.Add("mappingunkid", dsList.Tables("List").Rows(0).Item("mappingunkid").ToString())

            End If
            objApprover = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            dsList.Dispose()
        End Try
    End Sub


#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "
    Protected Sub lnkChangeStatus_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objApprovaltran As New clscoachingapproval_process_tran
        Dim objCoachingRoleMapping As New clscoaching_role_mapping
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            If CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("uempid")) = CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("employeeunkid")) AndAlso CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("ecompid")) = CInt(Session("CompanyUnkId")) Then
                DisplayMessage.DisplayMessage("Sorry, you cannot approve/reject your own coaching nomination. Please contact aruti administrator to make new approver with same level.", Me)
            End If

            If CInt(Session("UserId")) <> CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, this coaching nomination is a pending task for a different approver and cannot be edited from your account"), Me)
                Exit Sub
            End If

            objCoachingRoleMapping._Mappingunkid = CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("approverunkid"))

            Dim dsList As DataSet
            Dim dtList As DataTable

            Dim objapproverlevel As New clsCoaching_Approver_Level_master
            objapproverlevel._Levelunkid = objCoachingRoleMapping._Levelunkid

            dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                         CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("employeeunkid")), _
                                                         CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("coachingnominationunkid")), _
                                                         " pdpcoachingapproval_process_tran.mapuserunkid <> " & CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                        dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                        If dtList.Rows.Count > 0 Then Continue For

                        If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, this coaching nomination is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, this coaching nomination is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If

                    ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                        If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, this coaching nomination is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, this coaching nomination is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("statusunkid")) = enPDPCoachingApprovalStatus.APPROVED Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, this coaching nomination is already approved and cannot be edited"), Me)
                Exit Sub
            End If

            If CInt(gvCoachingApprovalList.DataKeys(row.RowIndex)("statusunkid")) = enPDPCoachingApprovalStatus.REJECTED Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, this coaching nomination is already rejected and cannot be edited"), Me)
                Exit Sub
            End If

            Session("mblnIsAddMode") = True
            Session("mblnFromApproval") = True

            Session("mintMappingUnkid") = gvCoachingApprovalList.DataKeys(row.RowIndex)("approverunkid")
            Session("PendingCoachingTranunkid") = gvCoachingApprovalList.DataKeys(row.RowIndex)("pendingcoachingtranunkid")
            Session("CoachingNominationunkid") = Convert.ToString(lnk.CommandArgument.ToString())
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "PDP\wPg_CoachingNominationForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
            objCoachingRoleMapping = Nothing
        End Try
    End Sub
#End Region

#Region "GridView Events"
    Protected Sub gvCoachingApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCoachingApprovalList.RowDataBound
        Try
            SetDateFormat()
            If e.Row.RowIndex >= 0 Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If CBool(gvCoachingApprovalList.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True = True Then
                        CType(e.Row.Cells(0).FindControl("lnkChangeStatus"), LinkButton).Visible = False

                        e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "employee").ToString
                        e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                        e.Row.BackColor = Color.Silver
                        e.Row.ForeColor = Color.Black
                        e.Row.Font.Bold = True

                        For i = 2 To e.Row.Cells.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next

                        'e.Row.Cells(2).CssClass = "group-header"
                        'e.Row.Cells(2).Style.Add("text-align", "left")

                    Else
                        If e.Row.Cells(getColumnID_Griview(gvCoachingApprovalList, "colhapplication_date", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(gvCoachingApprovalList, "colhapplication_date", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(gvCoachingApprovalList, "colhapplication_date", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(gvCoachingApprovalList, "colhapplication_date", False, True)).Text).Date.ToShortDateString
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCoacheeEmployee.ID, Me.lblCoacheeEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRole.ID, Me.lblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkMyApproval.ID, Me.chkMyApproval.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvCoachingApprovalList.Columns(1).FooterText, Me.gvCoachingApprovalList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvCoachingApprovalList.Columns(2).FooterText, Me.gvCoachingApprovalList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvCoachingApprovalList.Columns(3).FooterText, Me.gvCoachingApprovalList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvCoachingApprovalList.Columns(4).FooterText, Me.gvCoachingApprovalList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvCoachingApprovalList.Columns(5).FooterText, Me.gvCoachingApprovalList.Columns(5).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Sub GetControlCaptions()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Me.lblCoacheeEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCoacheeEmployee.ID, Me.lblCoacheeEmployee.Text)
            Me.lblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRole.ID, Me.lblRole.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)

            Me.chkMyApproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkMyApproval.ID, Me.chkMyApproval.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

            Me.gvCoachingApprovalList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvCoachingApprovalList.Columns(1).FooterText, Me.gvCoachingApprovalList.Columns(1).HeaderText)
            Me.gvCoachingApprovalList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvCoachingApprovalList.Columns(2).FooterText, Me.gvCoachingApprovalList.Columns(2).HeaderText)
            Me.gvCoachingApprovalList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvCoachingApprovalList.Columns(3).FooterText, Me.gvCoachingApprovalList.Columns(3).HeaderText)
            Me.gvCoachingApprovalList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvCoachingApprovalList.Columns(4).FooterText, Me.gvCoachingApprovalList.Columns(4).HeaderText)
            Me.gvCoachingApprovalList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvCoachingApprovalList.Columns(5).FooterText, Me.gvCoachingApprovalList.Columns(5).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Approved By :-  ")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Rejected By :-  ")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, this coaching nomination is a pending task for a different approver and cannot be edited from your account")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, this coaching nomination is already approved and cannot be edited")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, this coaching nomination is already rejected and cannot be edited")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
