﻿Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

Partial Class PDP_wPg_CoachingRoleMapping
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private mdicETS As Dictionary(Of String, Integer)
    Private mstrModuleName As String = "frmCoachingRoleMappingList"
    Private mstrModuleName1 As String = "frmCoachingRoleMappingAddEdit"
    Private mintMappingunkid As Integer
    Private currentId As String = ""
    Private Index As Integer
    Private mblnShowpopupCoachingMapping As Boolean = False

#End Region

#Region "Form Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            mdicETS = GvCoachingRoleList.Columns.Cast(Of DataControlField).ToDictionary(Function(x) x.FooterText, Function(x) GvCoachingRoleList.Columns.IndexOf(x))

            If IsPostBack = False Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()
                ListFillCombo()
                FillList(True)
                SetVisibility()
            Else
                mintMappingunkid = CType(ViewState("mintMappingunkid"), Integer)
                mblnShowpopupCoachingMapping = Convert.ToBoolean(ViewState("ShowpopupCoachingMapping").ToString())

                If mblnShowpopupCoachingMapping Then
                    popupCoachingMapping.Show()
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintMappingunkid") = mintMappingunkid
            ViewState("ShowpopupCoachingMapping") = mblnShowpopupCoachingMapping
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "   For List"

#Region "   Private Function"

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsCoaching As DataSet = Nothing
        Dim dtLevel As DataTable = Nothing
        Dim strSearching As String = ""
        Dim objCoachingRoleMapping As New clscoaching_role_mapping

        Try

            If isblank Then
                strSearching = "AND 1 = 2 "
            End If

            If CInt(cboFormTypeList.SelectedValue) > 0 Then
                strSearching &= "AND pdpcoaching_role_mapping.formtypeid = " & CInt(cboFormTypeList.SelectedValue) & " "
            End If

            If CInt(cboRoleList.SelectedValue) > 0 Then
                strSearching &= "AND pdpcoaching_role_mapping.roleunkid = " & CInt(cboRoleList.SelectedValue) & " "
            End If

            If CInt(cboLevelList.SelectedValue) > 0 Then
                strSearching &= "AND pdpcoaching_role_mapping.levelunkid = " & CInt(cboLevelList.SelectedValue) & " "
            End If


            dsCoaching = objCoachingRoleMapping.GetList("List", True, strSearching)

            If dsCoaching.Tables(0).Rows.Count <= 0 Then
                isblank = True
                Dim drRow As DataRow = dsCoaching.Tables(0).NewRow()
                drRow("Level") = ""
                drRow("levelunkid") = 0
                drRow("mappingunkid") = 0
                dsCoaching.Tables(0).Rows.Add(drRow)
            End If

            If dsCoaching IsNot Nothing Then
                dtLevel = New DataView(dsCoaching.Tables(0), "", "formtypeid,priority", DataViewRowState.CurrentRows).ToTable()

                Dim strLevelName As String = ""
                Dim dtTable As DataTable = dtLevel.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.Boolean"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtLevel.Rows
                    If CStr(drow("Level")).Trim <> strLevelName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLevelName = drow("Level").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtLevel.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

                dtTable.AcceptChanges()
                GvCoachingRoleList.DataSource = dtTable
                GvCoachingRoleList.DataBind()

                If isblank = True Then
                    GvCoachingRoleList.Rows(0).Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingRoleMapping = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ListFillCombo()
        Dim objRole As clsUserRole_Master
        Dim objLevel As clsCoaching_Approver_Level_master
        Try

            objRole = New clsUserRole_Master
            Dim dsList As DataSet = objRole.getComboList("List", True)
            With cboRoleList
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            With cboRole
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            dsList.Clear()
            dsList = Nothing

            objLevel = New clsCoaching_Approver_Level_master
            dsList = objLevel.getListForCombo(-1, "List", True)
            With cboLevelList
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            With cboLevel
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            dsList.Clear()
            dsList = Nothing

            dsList = (New clsMasterData).getComboListForPDPCoachingFormType("List", True)
            With cboFormTypeList
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            With cboFormType
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRole = Nothing
            objLevel = Nothing
        End Try
    End Sub

    Private Sub SetAtValue(ByVal xFormName As String, ByVal objCoachingMapping As clscoaching_role_mapping)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objCoachingMapping._AuditUserId = CInt(Session("UserId"))
            End If
            objCoachingMapping._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objCoachingMapping._ClientIP = CStr(Session("IP_ADD"))
            objCoachingMapping._HostName = CStr(Session("HOST_NAME"))
            objCoachingMapping._FormName = xFormName
            objCoachingMapping._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboFormType_SelectedIndexChanged(ByVal sender As System.Object, ByVal cboLevel As DropDownList)
        Dim objLevel As New clsCoaching_Approver_Level_master
        Dim dsList As DataSet
        Try
            Dim cboFormType As DropDownList = CType(sender, DropDownList)
            If CInt(cboFormType.SelectedValue) > 0 Then
                dsList = objLevel.getListForCombo(CInt(cboFormType.SelectedValue), "List", True)
                cboLevel.DataTextField = "name"
                cboLevel.DataValueField = "levelunkid"
                cboLevel.DataSource = dsList.Tables(0)
                cboLevel.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing
            objLevel = Nothing
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            Reset()
            cboFormType.Enabled = True
            cboRole.Enabled = True
            popupCoachingMapping.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboFormTypeList.SelectedValue = "0"
            cboRoleList.SelectedValue = "0"
            cboLevelList.SelectedValue = "0"
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objCoachingMapping As clscoaching_role_mapping

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            objCoachingMapping = New clscoaching_role_mapping
            objCoachingMapping._Mappingunkid = lnkedit.CommandArgument.ToString()
            mintMappingunkid = lnkedit.CommandArgument.ToString()
            GetValue()
            mblnShowpopupCoachingMapping = True
            popupCoachingMapping.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingMapping = Nothing
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objCoaching As clscoaching_role_mapping
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objCoaching = New clscoaching_role_mapping

            objCoaching._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingunkid = CInt(lnkdelete.CommandArgument.ToString())

            If objCoaching.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, You cannot delete this mapping of coaching with role . Reason: This Mapping is in use."), Me)
                Exit Sub
            End If

            popup_YesNo.Show()
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Are you sure you want to delete ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoaching = Nothing
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Dim objCoaching As clscoaching_role_mapping
        Try

            objCoaching = New clscoaching_role_mapping

            SetAtValue(mstrModuleName, objCoaching)

            objCoaching._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objCoaching._FormName = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objCoaching._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objCoaching.Delete(mintMappingunkid) = False Then
                DisplayMessage.DisplayMessage(objCoaching._Message, Me)
                Exit Sub
            End If

            mintMappingunkid = 0

            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoaching = Nothing
        End Try
    End Sub

#End Region

#Region "  Gridview Events"

    Protected Sub GvCoachingRoleList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvCoachingRoleList.RowDataBound
        Dim oldid As String
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then

                    Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    lnkEdit.Visible = False

                    Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkDelete.Visible = False

                    e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "Level").ToString
                    e.Row.Cells(3).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Row.Font.Bold = True

                    For i As Integer = 4 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                Else
                    'Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                    'lnkdelete.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Delete")

                    'If CBool(Session("AllowToDeleteOTRequisitionApprover")) Then
                    '    lnkdelete.Visible = True
                    'Else
                    '    lnkdelete.Visible = False
                    'End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

#End Region

#Region "ComboBox Events"
    Private Sub cboFormTypeList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFormTypeList.SelectedIndexChanged
        Try
            cboFormType_SelectedIndexChanged(sender, cboLevelList)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region "   For Add/Edit"

#Region "   Private Function"

    Private Function Validation() As Boolean
        Try
            If CInt(cboFormType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Sorry, Form Type is mandatory information. Please select Form Type to continue"), Me)
                cboFormType.Focus()
                Exit Function
            ElseIf CInt(cboRole.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Role is  compulsory information.Please select Role."), Me)
                cboRole.Focus()
                Return False
            ElseIf CInt(cboLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Level is  compulsory information.Please select Level."), Me)
                cboLevel.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub GetValue()
        Dim objCoachingMapping As clscoaching_role_mapping
        Try
            objCoachingMapping = New clscoaching_role_mapping
            If mintMappingunkid > 0 Then
                objCoachingMapping._Mappingunkid = mintMappingunkid
            End If
            cboFormType.SelectedValue = objCoachingMapping._FormTypeId.ToString
            cboFormType.Enabled = False
            cboFormType_SelectedIndexChanged(cboFormType, cboLevel)
            cboRole.SelectedValue = objCoachingMapping._Roleunkid.ToString()
            cboRole.Enabled = False
            cboLevel.SelectedValue = objCoachingMapping._Levelunkid.ToString()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingMapping = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objCoachingMapping As clscoaching_role_mapping)
        Try
            If mintMappingunkid > 0 Then objCoachingMapping._Mappingunkid = mintMappingunkid
            objCoachingMapping._FormTypeId = CInt(cboFormType.SelectedValue)
            objCoachingMapping._Roleunkid = CInt(cboRole.SelectedValue)
            objCoachingMapping._Levelunkid = CInt(cboLevel.SelectedValue)
            objCoachingMapping._Userunkid = CInt(Session("UserId"))
            SetAtValue(mstrModuleName, objCoachingMapping)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset()
        Try
            cboFormType.SelectedValue = "0"
            cboRole.SelectedValue = "0"
            cboLevel.SelectedValue = "0"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnSaveCoachingRoleMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCoachingRoleMapping.Click
        Dim objCoachingMapping As New clscoaching_role_mapping
        Try

            If Validation() = False Then
                mblnShowpopupCoachingMapping = True
                popupCoachingMapping.Show()
                Exit Sub
            End If


            SetValue(objCoachingMapping)

            Dim blnFlag As Boolean = False
            If mintMappingunkid > 0 Then
                blnFlag = objCoachingMapping.Update()
            ElseIf mintMappingunkid <= 0 Then
                blnFlag = objCoachingMapping.Insert()
            End If

            If blnFlag = False And objCoachingMapping._Message <> "" Then
                DisplayMessage.DisplayMessage(objCoachingMapping._Message, Me)
                mblnShowpopupCoachingMapping = True
                popupCoachingMapping.Show()
                Exit Sub
            Else
                FillList(False)
                Reset()
                mintMappingunkid = 0
                ViewState("mintMappingunkid") = mintMappingunkid
                mblnShowpopupCoachingMapping = False
                popupCoachingMapping.Hide()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCoachingMapping = Nothing
        End Try
    End Sub

    Protected Sub btnCloseCoachingRoleMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseCoachingRoleMapping.Click
        Try
            Reset()
            mblnShowpopupCoachingMapping = False
            popupCoachingMapping.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox Events"
    Private Sub cboFormType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFormType.SelectedIndexChanged
        Try
            cboFormType_SelectedIndexChanged(sender, cboLevel)
            popupCoachingMapping.Show()
            mblnShowpopupCoachingMapping = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCaption.ID, Me.lblCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFormTypeList.ID, Me.LblFormTypeList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRoleList.ID, Me.LblRoleList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLevelList.ID, Me.LblLevelList.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvCoachingRoleList.Columns(2).FooterText, Me.GvCoachingRoleList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvCoachingRoleList.Columns(3).FooterText, Me.GvCoachingRoleList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvCoachingRoleList.Columns(4).FooterText, Me.GvCoachingRoleList.Columns(4).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.LblAddEditPageHeader.ID, Me.LblAddEditPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.LblFormType.ID, Me.LblFormType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.LblRole.ID, Me.LblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.LblLevel.ID, Me.LblLevel.Text)
            
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveCoachingRoleMapping.ID, Me.btnSaveCoachingRoleMapping.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCloseCoachingRoleMapping.ID, Me.btnCloseCoachingRoleMapping.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCaption.ID, Me.lblCaption.Text)
            Me.LblFormTypeList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFormTypeList.ID, Me.LblFormTypeList.Text)
            Me.LblRoleList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRoleList.ID, Me.LblRoleList.Text)
            Me.LblLevelList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLevelList.ID, Me.LblLevelList.Text)
            
            Me.GvCoachingRoleList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvCoachingRoleList.Columns(2).FooterText, Me.GvCoachingRoleList.Columns(2).HeaderText)
            Me.GvCoachingRoleList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvCoachingRoleList.Columns(3).FooterText, Me.GvCoachingRoleList.Columns(3).HeaderText)
            Me.GvCoachingRoleList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvCoachingRoleList.Columns(4).FooterText, Me.GvCoachingRoleList.Columns(4).HeaderText)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))


            Me.LblAddEditPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblAddEditPageHeader.ID, Me.LblAddEditPageHeader.Text)
            Me.LblFormType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblFormType.ID, Me.LblFormType.Text)
            Me.LblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblRole.ID, Me.LblRole.Text)
            Me.LblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblLevel.ID, Me.LblLevel.Text)

            Me.btnSaveCoachingRoleMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveCoachingRoleMapping.ID, Me.btnSaveCoachingRoleMapping.Text.Replace("&", ""))
            Me.btnCloseCoachingRoleMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseCoachingRoleMapping.ID, Me.btnCloseCoachingRoleMapping.Text.Replace("&", ""))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, You cannot delete this mapping of coaching with role . Reason: This Mapping is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Are you sure you want to delete ?")


            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Role is  compulsory information.Please select Role.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Level is  compulsory information.Please select Level.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "Sorry, Form Type is mandatory information. Please select Form Type to continue")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
