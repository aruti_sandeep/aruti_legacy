﻿<%@ Page Title="Coaching Role Mapping" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_CoachingRoleMapping.aspx.vb" Inherits="PDP_wPg_CoachingRoleMapping" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Coaching Scheme Role Mapping"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Coaching Scheme Role Mapping List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFormTypeList" runat="server" Text="Form Type" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboFormTypeList" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblRoleList" runat="server" Text="Role" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboRoleList" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblLevelList" runat="server" Text="Level" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboLevelList" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 520px">
                                            <asp:GridView ID="GvCoachingRoleList" DataKeyNames="mappingunkid" runat="server"
                                                AutoGenerateColumns="False" Width="99%" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderText="Edit" FooterText="dgcolhedit">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("mappingunkid") %>'>
                                                                <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderText="Delete" FooterText="dgcolhdelete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("mappingunkid") %>'> 
                                                                    <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Level" HeaderText="Level" FooterText="dgcolhLevel" Visible="false">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Role" HeaderText="Role" FooterText="dgcolhRole"></asp:BoundField>
                                                    <asp:BoundField DataField="formtype" HeaderText="Form Type" FooterText="dgcolhFormType">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupCoachingMapping" BackgroundCssClass="modal-backdrop"
                    TargetControlID="LblAddEditPageHeader" runat="server" PopupControlID="pnlCoachingRoleMapping"
                    DropShadow="false" CancelControlID="LblAddEditPageHeader">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCoachingRoleMapping" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblAddEditPageHeader" Text="Add/Edit Coaching Scheme Role Mapping" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblFormType" runat="server" Text="Form Type" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cboFormType" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblRole" runat="server" Text="Role" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRole" runat="server" AutoPostBack="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblLevel" runat="server" Text="Level" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cboLevel" runat="server" AutoPostBack="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveCoachingRoleMapping" runat="server" CssClass="btn btn-primary"
                            Text="Save" />
                        <asp:Button ID="btnCloseCoachingRoleMapping" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                    </div>
                </asp:Panel>
                <uc1:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
