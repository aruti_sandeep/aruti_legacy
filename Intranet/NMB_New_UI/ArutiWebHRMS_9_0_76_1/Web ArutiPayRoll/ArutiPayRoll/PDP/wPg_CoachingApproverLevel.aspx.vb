﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class PDP_wPg_CoachingApproverLevel
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmCoachingApproverLevelList"
    Private ReadOnly mstrModuleName1 As String = "frmCoachingApproverLevelAddEdit"
    Private mintLevelmstid As Integer
    Private blnpopupApproverLevel As Boolean = False
    Private objCoachingApproverLevel As New clsCoaching_Approver_Level_master
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()
                FillCombo()
                If CBool(Session("AllowToViewCoachingApproverLevel")) Then
                    FillList(False)
                Else
                    FillList(True)

                End If
                SetVisibility()
            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If

                If ViewState("blnpopupApproverLevel") IsNot Nothing Then
                    blnpopupApproverLevel = Convert.ToBoolean(ViewState("blnpopupApproverLevel").ToString())
                End If

                If blnpopupApproverLevel Then
                    popupApproverLevel.Show()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupApproverLevel") = blnpopupApproverLevel
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objMasterData As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objMasterData.getComboListForPDPCoachingFormType("List", True)
            With cboFormType
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMasterData = Nothing
        End Try
    End Sub


    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As New DataSet
        Dim dsCalendarList As New DataSet
        Dim strfilter As String = ""
        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If

            dsList = objCoachingApproverLevel.GetList("List", True, False, strfilter)
            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = objCoachingApproverLevel.GetList("List", True, True, "1 = 2")
                isblank = True
            End If

            GvApprLevelList.DataSource = dsList.Tables("List")
            GvApprLevelList.DataBind()


            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Visible = CBool(Session("AllowToAddCoachingApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If mintLevelmstid > 0 Then
                objCoachingApproverLevel._Levelunkid = mintLevelmstid
            End If
            objCoachingApproverLevel._FormTypeId = CInt(cboFormType.SelectedValue)
            objCoachingApproverLevel._Levelcode = txtlevelcode.Text.Trim
            objCoachingApproverLevel._Levelname = txtlevelname.Text.Trim
            objCoachingApproverLevel._Priority = Convert.ToInt32(txtlevelpriority.Text)

            Call SetAtValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objCoachingApproverLevel._AuditUserId = CInt(Session("UserId"))
            End If
            objCoachingApproverLevel._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objCoachingApproverLevel._ClientIP = CStr(Session("IP_ADD"))
            objCoachingApproverLevel._HostName = CStr(Session("HOST_NAME"))
            objCoachingApproverLevel._FormName = mstrModuleName1
            objCoachingApproverLevel._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub GetValue()
        Try
            If mintLevelmstid > 0 Then
                objCoachingApproverLevel._Levelunkid = mintLevelmstid
            End If
            cboFormType.SelectedValue = CStr(objCoachingApproverLevel._FormTypeId)
            txtlevelcode.Text = objCoachingApproverLevel._Levelcode
            txtlevelname.Text = objCoachingApproverLevel._Levelname
            txtlevelpriority.Text = objCoachingApproverLevel._Priority.ToString()
            cboFormType.Enabled = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearData()
        Try
            cboFormType.SelectedValue = CStr(0)
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboFormType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Sorry, Form Type is mandatory information. Please select Form Type to continue"), Me)
                cboFormType.Focus()
                Exit Function
            ElseIf txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
                txtlevelcode.Focus()
                Exit Function
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
                txtlevelname.Focus()
                Exit Function
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
                txtlevelpriority.Focus()
                Exit Function
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Buttons Methods "

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            cboFormType.Enabled = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Validation() = False Then
                blnpopupApproverLevel = True
                popupApproverLevel.Show()
                Exit Sub
            End If

            Call SetValue()
            If mintLevelmstid > 0 Then
                blnFlag = objCoachingApproverLevel.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objCoachingApproverLevel.Insert()
            End If

            If blnFlag = False And objCoachingApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objCoachingApproverLevel._Message, Me)
                blnpopupApproverLevel = True
                popupApproverLevel.Show()
            Else
                ClearData()
                FillList(False)
                mintLevelmstid = 0
                blnpopupApproverLevel = False
                popupApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValue()
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())

            If objCoachingApproverLevel.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()
            popup_YesNo.Title = "Confirmation"
            popup_YesNo.Message = "Are You Sure You Want To Delete This Approver Level ?"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False

            SetAtValue()
            blnFlag = objCoachingApproverLevel.Delete(mintLevelmstid)

            If blnFlag = False And objCoachingApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objCoachingApproverLevel._Message, Me)
            Else
                FillList(False)
                mintLevelmstid = 0
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAClose.Click
        Try
            ClearData()
            blnpopupApproverLevel = False
            popupApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ClearData()
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("levelunkid").ToString) > 0 Then
                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                        lnkedit.Visible = CBool(Session("AllowToEditCoachingApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteCoachingApproverLevel"))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnnew.ID, btnnew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvApprLevelList.Columns(2).FooterText, Me.GvApprLevelList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvApprLevelList.Columns(3).FooterText, Me.GvApprLevelList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvApprLevelList.Columns(4).FooterText, Me.GvApprLevelList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvApprLevelList.Columns(5).FooterText, Me.GvApprLevelList.Columns(5).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, lblPageHeader2.ID, lblPageHeader2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, lblFormType.ID, lblFormType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, lbllevelcode.ID, lbllevelcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, lbllevelname.ID, lbllevelname.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, lbllevelpriority.ID, lbllevelpriority.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, btnSave.ID, btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, btnAClose.ID, btnAClose.Text)
           

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, lblPageHeader.Text)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnnew.ID, btnnew.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")

            Me.GvApprLevelList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvApprLevelList.Columns(2).FooterText, Me.GvApprLevelList.Columns(2).HeaderText)
            Me.GvApprLevelList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvApprLevelList.Columns(3).FooterText, Me.GvApprLevelList.Columns(3).HeaderText)
            Me.GvApprLevelList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvApprLevelList.Columns(4).FooterText, Me.GvApprLevelList.Columns(4).HeaderText)
            Me.GvApprLevelList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvApprLevelList.Columns(5).FooterText, Me.GvApprLevelList.Columns(5).HeaderText)

            Me.lblPageHeader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), lblPageHeader2.ID, lblPageHeader2.Text)
            Me.lblFormType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), lblFormType.ID, lblFormType.Text)
            Me.lbllevelcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), lbllevelcode.ID, lbllevelcode.Text)
            Me.lbllevelname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), lbllevelname.ID, lbllevelname.Text)
            Me.lbllevelpriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), lbllevelpriority.ID, lbllevelpriority.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), btnSave.ID, btnSave.Text).Replace("&", "")
            Me.btnAClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), btnAClose.ID, btnAClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 5, "Sorry, Form Type is mandatory information. Please select Form Type to continue")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
