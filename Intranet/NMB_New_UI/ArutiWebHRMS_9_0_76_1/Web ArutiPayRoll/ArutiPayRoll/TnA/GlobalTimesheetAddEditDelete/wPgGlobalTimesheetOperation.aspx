<%@ Page Title="Global Timesheet Add/Edit/Delete" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgGlobalTimesheetOperation.aspx.vb" Inherits="wPgGlobalTimesheetOperation" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/TimeCtrl.ascx" TagName="TimeCtrl" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 <script type="text/javascript">



     $("body").on("click", "[id*=chkHeder1]", function() {
         var chkHeader = $(this);
         debugger;
         var grid = $(this).closest("table");
         $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
     });

     $("body").on("click", "[id*=chkbox1]", function() {
         var grid = $(this).closest("table");
         var chkHeader = $("[id*=chkHeder1]", grid);
         debugger;
         if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
             chkHeader.prop("checked", true);
         }
         else {
             chkHeader.prop("checked", false);
         }
     });

   $.expr[":"].containsNoCase = function(el, i, m) {
           var search = m[3];
           if (!search) return false;
           return eval("/" + search + "/i").test($(el).text());
       };

       function Searching() {
           if ($('#txtSearch').val().length > 0) {
               $('#<%= dgvEmp.ClientID %> tbody tr').hide();
               $('#<%= dgvEmp.ClientID %> tbody tr:first').show();
               $('#<%= dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
           }
           else if ($('#txtSearch').val().length == 0) {
               resetFromSearchValue();
           }
           if ($('#<%= dgvEmp.ClientID %> tr:visible').length == 1) {
               $('.norecords').remove();
           }

           if (event.keyCode == 27) {
               resetFromSearchValue();
           }
       }
       function resetFromSearchValue() {
           $('#txtSearch').val('');
           $('#<%= dgvEmp.ClientID %> tr').show();
           $('.norecords').remove();
           $('#txtSearch').focus();
       }
</script>


    <asp:Panel ID="pnlMain" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Global Timesheet Edit/Delete"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOprType" runat="server" Text="Operation Type" CssClass="form-label"></asp:Label>
                                                        <asp:RadioButtonList ID="radOperation" runat="server" RepeatDirection="Vertical"
                                                            AutoPostBack="true">
                                                            <asp:ListItem Text="Add Timesheet" Selected="True" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Edit Timesheet" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Delete Timesheet" Value="3"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkOverWriteIfExist" runat="server" Text="Overwrite If Exist" />
                                                        <br />
                                                        <asp:CheckBox ID="chkIncludeHoliday" runat="server" Text="Include Holiday" />
                                                        <br />
                                                        <asp:CheckBox ID="chkIncludeWeekend" runat="server" Text="Include Weekend" />
                                                        <br />
                                                        <asp:CheckBox ID="chkIncludeDayOff" runat="server" Text="Include DayOff" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFdate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="true" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblInTime" runat="server" Text="In Time" CssClass="form-label"></asp:Label>
                                                        <uc4:TimeCtrl ID="dtpInTime" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl AutoPostBack="true" runat="server" ID="dtpToDate" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOutTime" runat="server" Text="Out Time" CssClass="form-label"></asp:Label>
                                                        <uc4:TimeCtrl ID="dtpOutTime" runat="server" AutoPostBack="true"></uc4:TimeCtrl>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmplyee" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblShift" runat="server" Text="Shift" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboShift" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPolicy" runat="server" Text="Policy" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPolicy" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" />
                                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                     <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                         <div class="form-group">
                                                                <div class="form-line">
                                                                          <input type="text" id="txtSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                                maxlength="50" class="form-control" onkeyup="Searching();" />
                                                                 </div>
                                                            </div>            
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
                                                         <div class="table-responsive" style="height: 300px">
                                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" DataKeyNames = "employeeunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkHeder1" runat="server" CssClass="filled-in" Text=" "  Enabled="true"  />   <%--AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged"--%>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkbox1" runat="server" CssClass="filled-in" Text=" "  CommandArgument="<%# Container.DataItemIndex %>" Enabled="true"  />  <%-- AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged"--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" />
                                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" />
                                                                            <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                            </div>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="panel-primary">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                       
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%; margin-bottom: 10px">
                                            <td style="width: 20%; vertical-align: top">
                                                
                                            </td>
                                            <td style="width: 30%; vertical-align: top">
                                               
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                               
                                            </td>
                                            <td style="width: 35%">
                                               
                                            </td>
                                            <td style="width: 15%">
                                               
                                            </td>
                                            <td style="width: 30%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                               
                                            </td>
                                            <td style="width: 35%">
                                                
                                            </td>
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 30%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                               
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                               
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                                
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                               
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%; margin-top: 5px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server" Style="margin-top: 5px;
                                                    max-height: 300px">
                                                   
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                <uc8:DeleteReason ID="voidReason" runat="server" Title="Are you sure you want to delete selected employee data for the selected date range?" />
                <uc9:Confirmation ID="DateCofirmantion" runat="server" Title="Confirmation" Message="Out Time is less than In time.So Out Time will be considered for next day.Are you sure you want to continue ?" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
