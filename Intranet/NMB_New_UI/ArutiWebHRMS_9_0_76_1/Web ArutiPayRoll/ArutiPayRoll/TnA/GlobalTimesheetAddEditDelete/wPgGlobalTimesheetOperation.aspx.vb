﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class wPgGlobalTimesheetOperation
    Inherits Basepage

#Region " Private Variables "

    Private clsuser As New User
    Private cmsg As New CommonCodes
    Private dtEmployee As DataTable


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmGlobalEditDelete_Timesheet"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                'Anjan [20 February 2016] -- End
                cmsg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            If IsPostBack = False Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                If dtpFromDate.IsNull = True Then dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If dtpToDate.IsNull = True Then dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

                dtpInTime.SetDate = ConfigParameter._Object._CurrentDateAndTime
                dtpOutTime.SetDate = ConfigParameter._Object._CurrentDateAndTime
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                Call SetVisibility()
                'Varsha Rana (17-Oct-2017) -- End
                Call FillCombo()
                Call Fill_Employee_List()
            End If



        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , , , )
            'Else
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , )
            'End If

            dsList = objEMaster.GetEmployeeList(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                False, "Emp", True)

            'Shani(24-Aug-2015) -- End
            With cboEmplyee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With
            dsList = objShift.getListForCombo("Shift", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Shift")
                .DataBind()
                .SelectedValue = "0"
            End With
            dsList = objPolicy.getListForCombo("Policy", True)
            With cboPolicy
                .DataValueField = "policyunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Policy")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
            objEMaster = Nothing : objShift = Nothing : objPolicy = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_Employee_List()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsEmployee As New DataSet
        Try
            'Pinkal (07-Oct-2015) -- Start
            'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.

            'dsEmployee = ObjEmp.GetEmployeeShift_PolicyList(dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, IIf(cboEmplyee.SelectedValue = "", 0, cboEmplyee.SelectedValue), IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue), IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = ObjEmp.GetEmployeeShift_PolicyList(dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, IIf(cboEmplyee.SelectedValue = "", 0, cboEmplyee.SelectedValue) _
            '                                                                                              , IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue), IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue) _
            '                                                                                              , "", Session("AccessLevelFilterString"))
            dsEmployee = ObjEmp.GetEmployeeShift_PolicyList(Session("Database_Name").ToString(), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            Session("UserAccessModeSetting").ToString(), _
                                                            False, "List", dtpFromDate.GetDate.Date, _
                                                            dtpToDate.GetDate.Date, _
                                                            CBool(Session("PolicyManagementTNA")), _
                                                            CInt(IIf(cboEmplyee.SelectedValue = "", 0, cboEmplyee.SelectedValue)), _
                                                            CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                                                            CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue)), "")
            'Shani(20-Nov-2015) -- End

            'Pinkal (07-Oct-2015) -- End

            If dsEmployee.Tables(0).Columns.Contains("ischeck") = False Then
                dsEmployee.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            End If

            If dsEmployee.Tables(0).Columns.Contains("shiftunkid") Then
                dsEmployee.Tables(0).Columns.Remove("shiftunkid")
            End If

            If dsEmployee.Tables(0).Columns.Contains("shiftname") Then
                dsEmployee.Tables(0).Columns.Remove("shiftname")
            End If

            If dsEmployee.Tables(0).Columns.Contains("Shiftrowno") Then
                dsEmployee.Tables(0).Columns.Remove("Shiftrowno")
            End If

            If dsEmployee.Tables(0).Columns.Contains("ShitEffectivedate") Then
                dsEmployee.Tables(0).Columns.Remove("ShitEffectivedate")
            End If

            If dsEmployee.Tables(0).Columns.Contains("policyunkid") Then
                dsEmployee.Tables(0).Columns.Remove("policyunkid")
            End If

            If dsEmployee.Tables(0).Columns.Contains("policyname") Then
                dsEmployee.Tables(0).Columns.Remove("policyname")
            End If

            If dsEmployee.Tables(0).Columns.Contains("Policyrowno") Then
                dsEmployee.Tables(0).Columns.Remove("Policyrowno")
            End If

            If dsEmployee.Tables(0).Columns.Contains("PolicyEffectivedate") Then
                dsEmployee.Tables(0).Columns.Remove("PolicyEffectivedate")
            End If

            dtEmployee = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "employeecode", "employeename", "ischeck")

            If Me.ViewState("dtEmployee") Is Nothing Then
                Me.ViewState.Add("dtEmployee", dtEmployee)
            Else
                Me.ViewState("dtEmployee") = dtEmployee
            End If

            dgvEmp.AutoGenerateColumns = False
            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()

        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        Finally
            If dtEmployee.Rows.Count <= 0 Then
                Dim r As DataRow = dtEmployee.NewRow

                r.Item("employeecode") = "" ' To Hide the row and display only Row Header
                r.Item("employeename") = ""

                dtEmployee.Rows.Add(r)
            End If

            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()
            ObjEmp = Nothing : dsEmployee.Dispose()
        End Try
    End Sub

    Private Function iValidation() As Boolean
        Try
            If dtpFromDate.IsNull = True Or dtpToDate.IsNull = True Then
                cmsg.DisplayMessage("Sorry, dates are mandatory information. Please set the dates.", Me)
                Return False
            End If

            If IsDate(dtpFromDate.GetDate.Date) = False Or IsDate(dtpToDate.GetDate.Date) = False Then
                cmsg.DisplayMessage("Sorry, datesformat are not valid. Please set the correct dates.", Me)
                Return False
            End If

            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 

            If dtpFromDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date OrElse dtpToDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                cmsg.DisplayMessage("Sorry, You cannot set future date to do operation further operation on it.", Me)
                Return False
            End If

            'Pinkal (13-Jan-2015) -- End

            Return True
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SaveTimeSheet()
        Try

            Dim mdtFromDate As DateTime = Nothing
            Dim mdtToDate As DateTime = Nothing

            If dtpInTime.IsNull = False Then
                mdtFromDate = CDate(dtpFromDate.GetDate.Date & " " & dtpInTime.GetDate.ToShortTimeString)
            End If

            If dtpOutTime.IsNull = False Then
                If dtpOutTime.GetDate.ToString().Contains("AM") AndAlso dtpOutTime.GetDate < dtpInTime.GetDate Then
                    mdtToDate = CDate(dtpFromDate.GetDate.Date.AddDays(1) & " " & dtpOutTime.GetDate.ToShortTimeString)
                Else
                    mdtToDate = CDate(dtpFromDate.GetDate.Date & " " & dtpOutTime.GetDate.ToShortTimeString)
                End If
            End If


            Dim objTimeSheet As New clslogin_Tran

            Blank_ModuleName()
            objTimeSheet._WebFormName = "frmGlobalEditDelete_Timesheet"
            StrModuleName1 = "mnuTnAInfo"
            StrModuleName2 = ""
            StrModuleName3 = ""
            objTimeSheet._WebClientIP = Session("IP_ADD").ToString()
            objTimeSheet._WebHostName = Session("HOST_NAME").ToString()
            objTimeSheet._Userunkid = CInt(Session("UserId"))

            objTimeSheet._Early_Coming = 0
            objTimeSheet._Late_Coming = 0
            objTimeSheet._Early_Going = 0
            objTimeSheet._Late_Going = 0


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Dim mstrEmployeeIDs As String = Me.ViewState("EmployeeIDs").ToString()

            Dim mstrEmployeeIDs As String = ""
            Dim drRow = dgvEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True).ToList()

            If drRow.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please select employee(s) in order to Edit/Delete timings."), Me)
                Exit Sub
            End If

            If drRow.Count > 0 Then
                mstrEmployeeIDs = String.Join(",", (From p In drRow Select (dgvEmp.DataKeys(p.RowIndex)("employeeunkid").ToString())).ToArray())
            End If
            'Gajanan [17-Sep-2020] -- End



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            objTimeSheet._CompanyId = CInt(Session("CompanyUnkId"))
            'Pinkal (06-Jan-2016) -- End


            Select Case radOperation.SelectedValue
                Case "1"  'ADD

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objTimeSheet.GlobalAddTimesheetData(mstrEmployeeIDs, dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date _
                    '                                                                , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                    '                                                                , mdtFromDate, mdtToDate, chkOverWriteIfExist.Checked _
                    '                                                                , CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then


                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.

                    'If objTimeSheet.GlobalAddTimesheetData(mstrEmployeeIDs, _
                    '                                       dtpFromDate.GetDate.Date, _
                    '                                       dtpToDate.GetDate.Date, _
                    '                                       chkIncludeHoliday.Checked, _
                    '                                       chkIncludeWeekend.Checked, _
                    '                                       chkIncludeDayOff.Checked, _
                    '                                       mdtFromDate, _
                    '                                       mdtToDate, _
                    '                                       chkOverWriteIfExist.Checked, _
                    '                                       Session("Database_Name"), _
                    '                                       Session("UserId"), _
                    '                                       Session("Fin_year"), _
                    '                                       Session("CompanyUnkId"), _
                    '                                       Session("UserAccessModeSetting"), True, _
                    '                                       Session("IsIncludeInactiveEmp"), _
                    '                                       Session("PolicyManagementTNA"), _
                    '                                       Session("DonotAttendanceinSeconds"), _
                    '                                       Session("FirstCheckInLastCheckOut"), True, , Nothing, , "", _
                    '                                       CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                    '                                       CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then

                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'If objTimeSheet.GlobalAddTimesheetData(mstrEmployeeIDs, _
                    '                                dtpFromDate.GetDate.Date, _
                    '                                dtpToDate.GetDate.Date, _
                    '                                chkIncludeHoliday.Checked, _
                    '                                chkIncludeWeekend.Checked, _
                    '                                chkIncludeDayOff.Checked, _
                    '                                mdtFromDate, _
                    '                                mdtToDate, _
                    '                                chkOverWriteIfExist.Checked, _
                    '                                Session("Database_Name").ToString(), _
                    '                                CInt(Session("UserId")), _
                    '                                CInt(Session("Fin_year")), _
                    '                                CInt(Session("CompanyUnkId")), _
                    '                                Session("UserAccessModeSetting").ToString(), True, _
                    '                                 False, CBool(Session("PolicyManagementTNA")), _
                    '                                CBool(Session("DonotAttendanceinSeconds")), _
                    '                                CBool(Session("FirstCheckInLastCheckOut")), True, -1, Nothing, , "", _
                    '                                CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                    '                                CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then


                    If objTimeSheet.GlobalAddTimesheetData(mstrEmployeeIDs, _
                                                    dtpFromDate.GetDate.Date, _
                                                    dtpToDate.GetDate.Date, _
                                                    chkIncludeHoliday.Checked, _
                                                    chkIncludeWeekend.Checked, _
                                                    chkIncludeDayOff.Checked, _
                                                    mdtFromDate, _
                                                    mdtToDate, _
                                                    chkOverWriteIfExist.Checked, _
                                                    Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    Session("UserAccessModeSetting").ToString(), True, _
                                                     False, CBool(Session("PolicyManagementTNA")), _
                                                    CBool(Session("DonotAttendanceinSeconds")), _
                                                  CBool(Session("FirstCheckInLastCheckOut")), _
                                                  CBool(Session("IsHolidayConsiderOnWeekend")), _
                                                  CBool(Session("IsDayOffConsiderOnWeekend")), _
                                                  CBool(Session("IsHolidayConsiderOnDayoff")), _
                                                  True, -1, Nothing, , "", _
                                                    CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                                                    CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then


                        'Pinkal (11-AUG-2017) -- End


                        'Pinkal (06-Jan-2016) -- End


                        'Shani(20-Nov-2015) -- End

                        'Language.setLanguage(mstrModuleName)
                        cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Global Timesheet Added successfully for selected employee(s)."), Me)
                        btnReset_Click(New Object(), New EventArgs())
                    Else
                        cmsg.DisplayMessage(objTimeSheet._Message, Me)
                        Exit Sub
                    End If
                Case "2"  'EDIT

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objTimeSheet.GlobalEditTimesheetData(mstrEmployeeIDs, dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date _
                    '                                            , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                    '                                            , mdtFromDate, mdtToDate _
                    '                                            , CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then


                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.

                    'If objTimeSheet.GlobalEditTimesheetData(mstrEmployeeIDs, _
                    '                                        dtpFromDate.GetDate.Date, _
                    '                                        dtpToDate.GetDate.Date, _
                    '                                        chkIncludeHoliday.Checked, _
                    '                                        chkIncludeWeekend.Checked, _
                    '                                        chkIncludeDayOff.Checked, _
                    '                                        mdtFromDate, _
                    '                                        mdtToDate, _
                    '                                        Session("Database_Name"), _
                    '                                        Session("UserId"), _
                    '                                        Session("Fin_year"), _
                    '                                        Session("CompanyUnkId"), _
                    '                                        Session("UserAccessModeSetting"), True, _
                    '                                        Session("IsIncludeInactiveEmp"), _
                    '                                        Session("PolicyManagementTNA"), _
                    '                                        Session("DonotAttendanceinSeconds"), _
                    '                                        Session("FirstCheckInLastCheckOut"), True, , Nothing, "", "", _
                    '                                        CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                    '                                        CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'If objTimeSheet.GlobalEditTimesheetData(mstrEmployeeIDs, _
                    '                                       dtpFromDate.GetDate.Date, _
                    '                                       dtpToDate.GetDate.Date, _
                    '                                       chkIncludeHoliday.Checked, _
                    '                                       chkIncludeWeekend.Checked, _
                    '                                       chkIncludeDayOff.Checked, _
                    '                                       mdtFromDate, _
                    '                                       mdtToDate, _
                    '                                       Session("Database_Name").ToString(), _
                    '                                       CInt(Session("UserId")), _
                    '                                       CInt(Session("Fin_year")), _
                    '                                       CInt(Session("CompanyUnkId")), _
                    '                                       Session("UserAccessModeSetting").ToString(), True, _
                    '                                       CBool(Session("IsIncludeInactiveEmp")), _
                    '                                       CBool(Session("PolicyManagementTNA")), _
                    '                                       CBool(Session("DonotAttendanceinSeconds")), _
                    '                                       CBool(Session("FirstCheckInLastCheckOut")), True, -1, Nothing, "", "", _
                    '                                       CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                    '                                       CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then

                    If objTimeSheet.GlobalEditTimesheetData(mstrEmployeeIDs, _
                                                           dtpFromDate.GetDate.Date, _
                                                           dtpToDate.GetDate.Date, _
                                                           chkIncludeHoliday.Checked, _
                                                           chkIncludeWeekend.Checked, _
                                                           chkIncludeDayOff.Checked, _
                                                           mdtFromDate, _
                                                           mdtToDate, _
                                                           Session("Database_Name").ToString(), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           Session("UserAccessModeSetting").ToString(), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), _
                                                           CBool(Session("PolicyManagementTNA")), _
                                                           CBool(Session("DonotAttendanceinSeconds")), _
                                                      CBool(Session("FirstCheckInLastCheckOut")), _
                                                      CBool(Session("IsHolidayConsiderOnWeekend")), _
                                                      CBool(Session("IsDayOffConsiderOnWeekend")), _
                                                      CBool(Session("IsHolidayConsiderOnDayoff")), _
                                                      True, -1, Nothing, "", "", _
                                                           CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                                                           CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then

                        'Pinkal (11-AUG-2017) -- End

                        'Pinkal (06-Jan-2016) -- End

                        'Shani(20-Nov-2015) -- End

                        'Language.setLanguage(mstrModuleName)
                        cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Global Timesheet Edited successfully for selected employee(s)."), Me)
                        btnReset_Click(New Object(), New EventArgs())
                    Else
                        cmsg.DisplayMessage(objTimeSheet._Message, Me)
                        Exit Sub
                    End If
                Case "3"  'DELETE
                    voidReason.Show()
            End Select
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = CBool(Session("AllowToAddEditDeleteGlobalTimesheet"))
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            cmsg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Button's Events "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        cmsg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            radOperation.SelectedValue = "1"
            radOperation_SelectedIndexChanged(New Object, New EventArgs())
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmplyee.SelectedValue = "0"
            cboPolicy.SelectedValue = "0"
            cboShift.SelectedValue = "0"
            chkIncludeDayOff.Checked = False
            chkIncludeHoliday.Checked = False
            chkIncludeWeekend.Checked = False
            chkOverWriteIfExist.Checked = False

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'txtSearch.Text = ""
            'Gajanan [17-Sep-2020] -- End

            Call Fill_Employee_List()
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If iValidation() = False Then Exit Sub
            Call Fill_Employee_List()
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If iValidation() = False Then Exit Sub



            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'If Me.ViewState("dtEmployee") Is Nothing Then
            '    'Language.setLanguage(mstrModuleName)
            '    cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "There is no employee(s) to Edit/Delete timings."), Me)
            '    Exit Sub
            'End If

            'dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
            'Dim drRow() As DataRow = dtEmployee.Select("ischeck=true")
            'If drRow.Length <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please select employee(s) in order to Edit/Delete timings."), Me)
            '    Exit Sub
            'End If

            If dgvEmp.Rows.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "There is no employee(s) to Edit/Delete timings."), Me)
                Exit Sub
            End If

            Dim drRow = dgvEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True).ToList()
            If drRow.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please select employee(s) in order to Edit/Delete timings."), Me)
                Exit Sub
            End If

            'Gajanan [17-Sep-2020] -- End



            Select Case radOperation.SelectedValue
                Case "1"  'ADD

                    If dtpInTime.IsNull = True Then
                        cmsg.DisplayMessage("Sorry, InTime is mandatory information. Please set proper InTime.", Me)
                        Exit Sub
                    End If

                    If dtpOutTime.IsNull = True Then
                        cmsg.DisplayMessage("Sorry, OutTime is mandatory information. Please set proper OutTime.", Me)
                        Exit Sub
                    End If

                Case "2"  'EDIT

                    If dtpInTime.IsNull AndAlso dtpOutTime.IsNull Then
                        'Language.setLanguage(mstrModuleName)
                        cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please Enter Intime or Outtime to edit timesheet for selected employee(s)."), Me)
                        Exit Sub
                    End If

                Case "3  'DELETE"

            End Select


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Dim lstEmpoyeeId As List(Of String) = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
            'Dim mstrEmployeeIDs As String = String.Join(",", lstEmpoyeeId.ToArray)
            'If Me.ViewState("EmployeeIDs") Is Nothing Then
            '    Me.ViewState.Add("EmployeeIDs", mstrEmployeeIDs)
            'Else
            '    Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            'End If
            Dim mstrEmployeeIDs As String = ""
            If drRow.Count > 0 Then
                mstrEmployeeIDs = String.Join(",", (From p In drRow Select (dgvEmp.DataKeys(p.RowIndex)("employeeunkid").ToString())).ToArray())
            End If

            'Gajanan [17-Sep-2020] -- End

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse CBool(Session("IsArutiDemo")) Then
                Dim objMstdata As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran
                Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, dtpFromDate.GetDate.Date, 0, 0, True)
                objPeriod._Periodunkid(Session("Database_Name").ToString) = intPeriod
                If objPeriod._Statusid = 2 Then
                    'Language.setLanguage(mstrModuleName)
                    cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry,you can't Add/Edit/Delete employee(s) timings .Reason:Period is already over."), Me)
                    Exit Sub
                End If

                If intPeriod > 0 Then

                    Dim mdtTnADate As DateTime = Nothing
                    If objPeriod._TnA_EndDate.Date < dtpFromDate.GetDate.Date Then
                        mdtTnADate = dtpFromDate.GetDate.Date
                    Else
                        mdtTnADate = objPeriod._TnA_EndDate.Date
                    End If

                    Dim objLeaveTran As New clsTnALeaveTran
                    If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(Session("Database_Name").ToString), mstrEmployeeIDs, mdtTnADate.Date) Then
                        'Language.setLanguage(mstrModuleName)
                        cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry,you can't Add/Edit/Delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
                        Exit Sub
                    End If

                End If
            End If

            If (CInt(radOperation.SelectedValue) = 1 OrElse CInt(radOperation.SelectedValue) = 2) AndAlso (dtpInTime.IsNull = False AndAlso dtpOutTime.IsNull = False) Then
                If CDate(dtpInTime.GetDate) > CDate(dtpOutTime.GetDate) Then
                    DateCofirmantion.Show()
                Else
                    SaveTimeSheet()
                End If
            Else
                SaveTimeSheet()
            End If


        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub voidReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles voidReason.buttonDelReasonYes_Click
        Try
            Dim objTimeSheet As New clslogin_Tran

            Blank_ModuleName()
            objTimeSheet._WebFormName = "frmGlobalEditDelete_Timesheet"
            StrModuleName1 = "mnuTnAInfo"
            StrModuleName2 = ""
            StrModuleName3 = ""
            objTimeSheet._WebClientIP = Session("IP_ADD").ToString()
            objTimeSheet._WebHostName = Session("HOST_NAME").ToString()

            objTimeSheet._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objTimeSheet._Voidreason = voidReason.Reason
            objTimeSheet._Voiduserunkid = CInt(Session("UserId"))



            'Gajanan [17-Sep-2020] -- Start
            'New UI Change

            'If objTimeSheet.GlobalVoidtTimesheetData(Me.ViewState("EmployeeIDs").ToString(), _
            '                                       dtpFromDate.GetDate.Date, _
            '                                       dtpToDate.GetDate.Date, _
            '                                       chkIncludeHoliday.Checked, _
            '                                       chkIncludeWeekend.Checked, _
            '                                       chkIncludeDayOff.Checked, _
            '                                       Session("Database_Name").ToString(), _
            '                                       CInt(Session("UserId")), _
            '                                       CInt(Session("Fin_year")), _
            '                                       CInt(Session("CompanyUnkId")), _
            '                                       Session("UserAccessModeSetting").ToString(), True, _
            '                                        False, CBool(Session("PolicyManagementTNA")), _
            '                                       CBool(Session("DonotAttendanceinSeconds")), _
            '                                     CBool(Session("FirstCheckInLastCheckOut")), _
            '                                     CBool(Session("IsHolidayConsiderOnWeekend")), _
            '                                     CBool(Session("IsDayOffConsiderOnWeekend")), _
            '                                     CBool(Session("IsHolidayConsiderOnDayoff")), _
            '                                     True, -1, Nothing, "", "", _
            '                                       CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
            '                                       CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then

            Dim mstrEmployeeIDs As String = ""
            Dim drRow = dgvEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True).ToList()

            If drRow.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please select employee(s) in order to Edit/Delete timings."), Me)
                Exit Sub
            End If

            If drRow.Count > 0 Then
                mstrEmployeeIDs = String.Join(",", (From p In drRow Select (dgvEmp.DataKeys(p.RowIndex)("employeeunkid").ToString())).ToArray())
            End If
            'Gajanan [17-Sep-2020] -

            If objTimeSheet.GlobalVoidtTimesheetData(mstrEmployeeIDs.Trim(), _
                                                 dtpFromDate.GetDate.Date, _
                                                 dtpToDate.GetDate.Date, _
                                                 chkIncludeHoliday.Checked, _
                                                 chkIncludeWeekend.Checked, _
                                                 chkIncludeDayOff.Checked, _
                                                 Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                  False, CBool(Session("PolicyManagementTNA")), _
                                                 CBool(Session("DonotAttendanceinSeconds")), _
                                               CBool(Session("FirstCheckInLastCheckOut")), _
                                               CBool(Session("IsHolidayConsiderOnWeekend")), _
                                               CBool(Session("IsDayOffConsiderOnWeekend")), _
                                               CBool(Session("IsHolidayConsiderOnDayoff")), _
                                               True, -1, Nothing, "", "", _
                                                 CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue)), _
                                                 CInt(IIf(cboPolicy.SelectedValue = "", 0, cboPolicy.SelectedValue))) Then



                'Gajanan [17-Sep-2020] -- End



                'Language.setLanguage(mstrModuleName)
                cmsg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Global Timesheet Deleted done successfully for selected employee(s)."), Me)
                btnReset_Click(sender, e)
            Else
                cmsg.DisplayMessage(objTimeSheet._Message, Me)
                Exit Sub
            End If

        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub DateCofirmantion_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateCofirmantion.buttonYes_Click
        Try
            SaveTimeSheet()
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            'Response.Redirect("~/Userhome.aspx")
            Response.Redirect("~/Userhome.aspx", False)
            'Pinkal (11-Feb-2022) -- End
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    'Gajanan [17-Sep-2020] -- Start
    'New UI Change

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvEmp.Rows.Count <= 0 Then Exit Sub
    '        Dim dvEmployee As DataView = CType(Me.ViewState("dtEmployee"), DataTable).DefaultView
    '        For i As Integer = 0 To dgvEmp.Rows.Count - 1
    '            Dim gvRow As GridViewRow = dgvEmp.Rows(i)
    '            CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '        Next
    '        Me.ViewState("dtEmployee") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        cmsg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        If gvr.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("employeecode = '" & gvr.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            CType(Me.ViewState("dtEmployee"), DataTable).AcceptChanges()


    '            Dim drRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("ischeck = 1")

    '            If CType(Me.ViewState("dtEmployee"), DataTable).Rows.Count = drRow.Length Then
    '                CType(dgvEmp.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = True
    '            Else
    '                CType(dgvEmp.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = False
    '            End If

    '        End If
    '    Catch ex As Exception
    '        cmsg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Gajanan [17-Sep-2020] -- End

#End Region

#Region "TextBox Event"

    'Gajanan [17-Sep-2020] -- Start
    'New UI Change
    'Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
    '    Try
    '        Dim dView As DataView = CType(Me.ViewState("dtEmployee"), DataTable).DefaultView
    '        Dim StrSearch As String = String.Empty
    '        If txtSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "employeecode LIKE '%" & txtSearch.Text & "%' OR employeename LIKE '%" & txtSearch.Text & "%'"
    '        End If
    '        dView.RowFilter = StrSearch
    '        dgvEmp.AutoGenerateColumns = False
    '        dgvEmp.DataSource = dView
    '        dgvEmp.DataBind()
    '        txtSearch.Focus()
    '    Catch ex As Exception
    '        cmsg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Gajanan [17-Sep-2020] -- End

#End Region

#Region "RadioButton Event"

    Protected Sub radOperation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOperation.SelectedIndexChanged
        Try
            Select Case radOperation.SelectedValue
                Case "1"   'ADD
                    dtpInTime.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    dtpOutTime.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    dtpInTime.Enabled = True
                    dtpOutTime.Enabled = True
                    chkOverWriteIfExist.Enabled = True
                    chkOverWriteIfExist.Checked = False

                Case "2"   'EDIT
                    dtpInTime.SetDate = Nothing
                    dtpOutTime.SetDate = Nothing
                    dtpInTime.Enabled = True
                    dtpOutTime.Enabled = True
                    chkOverWriteIfExist.Enabled = False
                    chkOverWriteIfExist.Checked = False

                Case "3"  'DELETE
                    dtpInTime.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    dtpOutTime.SetDate = ConfigParameter._Object._CurrentDateAndTime
                    dtpInTime.Enabled = False
                    dtpOutTime.Enabled = False
                    chkOverWriteIfExist.Enabled = False
                    chkOverWriteIfExist.Checked = False

            End Select

        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " GridView's Events "

    Protected Sub dgvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvEmp.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(1).Text = "&nbsp;" AndAlso e.Row.Cells(2).Text = "&nbsp;" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblOprType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOprType.ID, Me.lblOprType.Text)

            Me.radOperation.Items(0).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "radAddTimesheet", Me.radOperation.Items(0).Text)
            Me.radOperation.Items(1).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "radEditTimesheet", Me.radOperation.Items(1).Text)
            Me.radOperation.Items(2).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "radDeleteTimesheet", Me.radOperation.Items(2).Text)
            Me.chkOverWriteIfExist.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkOverWriteIfExist.ID, Me.chkOverWriteIfExist.Text)
            Me.chkIncludeHoliday.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeHoliday.ID, Me.chkIncludeHoliday.Text)
            Me.chkIncludeWeekend.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeWeekend.ID, Me.chkIncludeWeekend.Text)
            Me.chkIncludeDayOff.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeDayOff.ID, Me.chkIncludeDayOff.Text)
            Me.lblFdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFdate.ID, Me.lblFdate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblInTime.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInTime.ID, Me.lblInTime.Text)
            Me.lblOutTime.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOutTime.ID, Me.lblOutTime.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblShift.ID, Me.lblShift.Text)
            Me.lblPolicy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPolicy.ID, Me.lblPolicy.Text)

            Me.dgvEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
            Me.dgvEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            cmsg.DisplayError(ex, Me)
        End Try

    End Sub

End Class
