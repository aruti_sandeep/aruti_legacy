﻿Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Partial Class TnA_OT_Requisition_OTRequisitionApprover_AddEdit
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName1 As String = "frmOTRequisitionApproverAddedit"
    Private mstrEmployeeIDs As String = ""
    Dim mblnpopupOTApproverAddEdit As Boolean = False
    Private dtAssignEmpView As DataView = Nothing
    Private mintTnaMappingunkid As Integer
    Private mdtApproverList As DataTable
    Dim mdtPopupEmpAddEditList As DataTable
    Dim mdtPopupAddeditSelectEmpList As DataTable
    Private currentId As String = ""
    Private Index As Integer
    Private mintActiveDeactiveApprId As Integer = 0
    Private mblActiveDeactiveApprStatus As Boolean
    Private mstrAdvanceSearch As String = ""

#End Region

#Region "Form Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Pinkal (27-Oct-2022) -- End

            If IsPostBack = False Then
                GC.Collect()

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()
                FillAddEditCombo()

                If Session("OTApproverUnkId") IsNot Nothing Then
                    mintTnaMappingunkid = CInt(Session("OTApproverUnkId"))
                End If

                Dim objclsTnaapprover_master As New clsTnaapprover_master
                Dim objclsTnaapprover_tran As New clsTnaapprover_Tran
                objclsTnaapprover_master._Tnamappingunkid = mintTnaMappingunkid
                objclsTnaapprover_tran._TnAMappingUnkId = mintTnaMappingunkid
                objclsTnaapprover_tran._EmployeeAsonDate = ConfigParameter._Object._CurrentDateAndTime.Date
                objclsTnaapprover_tran.Get_Data()
                mdtPopupAddeditSelectEmpList = objclsTnaapprover_tran._DataTable
                GetValue(objclsTnaapprover_master)
                objclsTnaapprover_tran = Nothing
                objclsTnaapprover_master = Nothing
            Else

                mintActiveDeactiveApprId = CInt(ViewState("mintActiveDeactiveApprId"))
                mblActiveDeactiveApprStatus = CBool(ViewState("mblActiveDeactiveApprStatus"))

                mblnpopupOTApproverAddEdit = ViewState("blnpopupOTApproverAddEdit")
                If ViewState("EmpList") IsNot Nothing Then
                    mdtPopupEmpAddEditList = CType(ViewState("EmpList"), DataTable)
                End If

                If ViewState("SelectedEmpList") IsNot Nothing Then
                    mdtPopupAddeditSelectEmpList = CType(ViewState("SelectedEmpList"), DataTable)
                End If

                If ViewState("mintTnaMappingunkid") IsNot Nothing Then
                    mintTnaMappingunkid = CType(ViewState("mintTnaMappingunkid"), Integer)
                End If

                If Me.ViewState("EmployeeIDs") IsNot Nothing Then
                    mstrEmployeeIDs = Me.ViewState("EmployeeIDs").ToString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupOTApproverAddEdit") = mblnpopupOTApproverAddEdit
            ViewState("EmpList") = mdtPopupEmpAddEditList
            ViewState("SelectedEmpList") = mdtPopupAddeditSelectEmpList
            ViewState("mdtApproverList") = mdtApproverList
            ViewState("mintTnaMappingunkid") = mintTnaMappingunkid
            ViewState("mintActiveDeactiveApprId") = mintActiveDeactiveApprId
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus
            Me.ViewState("EmpAdvanceSearch") = mstrAdvanceSearch
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Function"

    Private Sub FillAddEditCombo()
        Dim objEmployee As clsEmployee_Master
        Dim objLevel As clsTna_approverlevel_master
        Dim objUser As clsUserAddEdit
        Dim dsList As DataSet = Nothing
        Try
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0

            objLevel = New clsTna_approverlevel_master
            objUser = New clsUserAddEdit

            dsList = objLevel.getListForCombo("List", True)
            drpAddEditApproverLevel.DataSource = dsList
            drpAddEditApproverLevel.DataTextField = "name"
            drpAddEditApproverLevel.DataValueField = "tnalevelunkid"
            drpAddEditApproverLevel.DataBind()


            If chkAddEditExtApprover.Checked = False Then
                objEmployee = New clsEmployee_Master
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)
                drpAddEditApprover.DataSource = dsList
                drpAddEditApprover.DataTextField = "EmpCodeName"
                drpAddEditApprover.DataValueField = "employeeunkid"
                drpAddEditApprover.DataBind()
            Else

                dsList = objUser.GetExternalApproverList("List", _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, enUserPriviledge.AllowtoApproveOTRequisition)

                Dim drRow As DataRow = dsList.Tables("List").NewRow
                drRow("name") = "Select"
                drRow("userunkid") = 0
                dsList.Tables("List").Rows.InsertAt(drRow, 0)

                drpAddEditApprover.DataSource = dsList.Tables("List")
                drpAddEditApprover.DataTextField = "name"
                drpAddEditApprover.DataValueField = "userunkid"
                drpAddEditApprover.DataBind()
            End If


            dsList = Nothing
            dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), enUserPriviledge.AllowtoApproveOTRequisition, CInt(Session("Fin_year")), True)
            drpAddEditUser.DataSource = dsList.Tables("User")
            drpAddEditUser.DataTextField = "name"
            drpAddEditUser.DataValueField = "userunkid"
            drpAddEditUser.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList.Dispose()
            objLevel = Nothing
            objEmployee = Nothing
            objUser = Nothing
        End Try
    End Sub

    Private Sub FillEmployeeList(ByVal isblank As Boolean)
        Dim dsEmployee As DataSet = Nothing
        Dim strSearch As String = String.Empty
        Dim objEmployee As clsEmployee_Master
        Try
            Dim blnInActiveEmp As Boolean = False


            If isblank Then
                strSearch = "AND 1=2"
            End If

            If chkAddEditExtApprover.Checked = False Then
                If CInt(drpAddEditApprover.SelectedValue) > 0 Then
                    strSearch &= "AND hremployee_master.employeeunkid <> " & CInt(drpAddEditApprover.SelectedValue) & " "
                End If
            End If

            If CInt(drpAddEditApprover.SelectedValue) <= 0 Then
                strSearch &= "AND hremployee_master.employeeunkid = " & CInt(drpAddEditApprover.SelectedValue) & " "
            End If

            If mstrAdvanceSearch.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceSearch
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If

            objEmployee = New clsEmployee_Master

            Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center



            dsEmployee = objEmployee.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            blnInActiveEmp, _
                                         "Employee", -1, False, strSearch, CBool(Session("ShowFirstAppointmentDate")), False, False, True)

            dsEmployee.Tables(0).Columns(1).ColumnName = "employeecode"
            dsEmployee.Tables(0).Columns(2).ColumnName = "name"

            dsEmployee.Tables(0).Columns(3).ColumnName = "departmentname"
            dsEmployee.Tables(0).Columns(4).ColumnName = "jobname"


            If dsEmployee.Tables(0).Columns.Contains("IsCheck") = False Then
                Dim dccolumn As New DataColumn("IsCheck")
                dccolumn.DataType = Type.GetType("System.Boolean")
                dccolumn.DefaultValue = False
                dsEmployee.Tables(0).Columns.Add(dccolumn)
            End If


            If mstrEmployeeIDs.Trim.Length > 0 Then
                mdtPopupEmpAddEditList = New DataView(dsEmployee.Tables(0), "employeeunkid not in (" & mstrEmployeeIDs.Trim & ")", "", DataViewRowState.CurrentRows).ToTable()
            Else
                mdtPopupEmpAddEditList = dsEmployee.Tables(0)
            End If


            If mdtPopupEmpAddEditList.Rows.Count <= 0 Then
                mdtPopupEmpAddEditList.Rows.Add(mdtPopupEmpAddEditList.NewRow())
                isblank = True
            End If

            gvAddEditEmployee.DataSource = mdtPopupEmpAddEditList
            gvAddEditEmployee.DataBind()

            If isblank Then
                gvAddEditEmployee.Rows(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(drpAddEditApprover.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "OT Approver is mandatory information. Please provide OT Approver to continue."), Me)
                drpAddEditApprover.Focus()
                Return False
            End If

            If CInt(drpAddEditApproverLevel.SelectedValue) <= 0 Then
                ''Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "OT Approver Level is mandatory information. Please provide OT Approver Level to continue"), Me)
                drpAddEditApproverLevel.Focus()
                Return False
            End If

            If chkAddEditExtApprover.Checked = False Then
                If CInt(drpAddEditUser.SelectedValue) <= 0 Then
                    ''Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 3, "User is mandatory information. Please provide User to continue"), Me)
                    drpAddEditUser.Focus()
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub SelectedEmplyeeList(ByVal IsBlank As Boolean)
        Dim strSearch As String = String.Empty
        Try

            If mdtPopupAddeditSelectEmpList Is Nothing Then Exit Sub

            If mdtPopupAddeditSelectEmpList.Columns.Contains("IsCheck") = False Then
                mdtPopupAddeditSelectEmpList.Columns.Add("IsCheck", Type.GetType("System.Boolean"))
                mdtPopupAddeditSelectEmpList.Columns("IsCheck").DefaultValue = False
            End If

            If mdtPopupAddeditSelectEmpList.Rows.Count <= 0 Then
                mdtPopupAddeditSelectEmpList.Rows.Add(mdtPopupAddeditSelectEmpList.NewRow())
            End If

            dtAssignEmpView = mdtPopupAddeditSelectEmpList.DefaultView

            If IsBlank = False Then dtAssignEmpView.RowFilter = " AUD <> 'D' "

            GvSelectedEmployee.AutoGenerateColumns = False

            'If txtAssignedEmpSearch.Text.Trim.Length > 0 Then
            '    If dtAssignEmpView.RowFilter.Length > 0 Then
            '        dtAssignEmpView.RowFilter &= " AND ename like '%" & txtAssignedEmpSearch.Text.Trim() & "%' "
            '    Else
            '        dtAssignEmpView.RowFilter = "ename like '%" & txtAssignedEmpSearch.Text.Trim() & "%' "
            '    End If
            'End If

            GvSelectedEmployee.DataSource = dtAssignEmpView
            GvSelectedEmployee.DataBind()

            If IsBlank Then
                GvSelectedEmployee.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue(ByVal objclsTnaapprover_master As clsTnaapprover_master)
        Try
            If mintTnaMappingunkid > 0 Then
                objclsTnaapprover_master._Tnamappingunkid = mintTnaMappingunkid
            End If

            objclsTnaapprover_master._Tnalevelunkid = Convert.ToInt32(drpAddEditApproverLevel.SelectedValue)

            If chkAddEditExtApprover.Checked = False Then
                objclsTnaapprover_master._ApproverEmployeeId = CInt(drpAddEditApprover.SelectedValue)
                objclsTnaapprover_master._Mapuserunkid = Convert.ToInt32(drpAddEditUser.SelectedValue)
            Else
                objclsTnaapprover_master._ApproverEmployeeId = Convert.ToInt32(drpAddEditApprover.SelectedValue)
                objclsTnaapprover_master._Mapuserunkid = Convert.ToInt32(drpAddEditApprover.SelectedValue)
            End If

            objclsTnaapprover_master._Tnatypeid = enTnAApproverType.OT_Requisition_Approver
            objclsTnaapprover_master._Isotcap_Hod = Convert.ToBoolean(chkHodCapForOT.Checked)
            objclsTnaapprover_master._Userunkid = CInt(Session("UserId"))
            objclsTnaapprover_master._Isactive = True
            objclsTnaapprover_master._IsSwap = False
            objclsTnaapprover_master._IsExternalApprover = chkAddEditExtApprover.Checked

            Call SetAtValue(mstrModuleName1, objclsTnaapprover_master)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub SetEditValue(ByVal objclsTnaapprover_master As clsTnaapprover_master)
        Dim objclsTnaapprover_tran As clsTnaapprover_Tran
        Try
            chkAddEditExtApprover.Checked = objclsTnaapprover_master._IsExternalApprover
            chkAddEditExtApprover_CheckedChanged(New Object(), New EventArgs())

            If chkAddEditExtApprover.Checked Then
                drpAddEditApprover.SelectedValue = objclsTnaapprover_master._Mapuserunkid
            Else
                drpAddEditApprover.SelectedValue = objclsTnaapprover_master._ApproverEmployeeId
                drpAddEditUser.SelectedValue = objclsTnaapprover_master._Mapuserunkid
            End If

            drpAddEditApprover.Enabled = False
            chkAddEditExtApprover.Enabled = False

            drpAddEditApproverLevel.SelectedValue = objclsTnaapprover_master._Tnalevelunkid
            chkHodCapForOT.Checked = objclsTnaapprover_master._Isotcap_Hod

            objclsTnaapprover_tran = New clsTnaapprover_Tran

            objclsTnaapprover_tran._TnAMappingUnkId = mintTnaMappingunkid
            objclsTnaapprover_tran._EmployeeAsonDate = ConfigParameter._Object._CurrentDateAndTime.Date

            drpAddEditApprover_SelectedIndexChanged(New Object, New EventArgs())

            objclsTnaapprover_tran.Get_Data()
            mdtPopupAddeditSelectEmpList = objclsTnaapprover_tran._DataTable

            SelectedEmplyeeList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objclsTnaapprover_tran = Nothing
        End Try
    End Sub

    Private Sub CheckAll(ByVal gv As GridView, ByVal dt As DataTable, ByVal chkid As String, ByVal filter As String)
        Try
            Dim head As GridViewRow = gv.HeaderRow
            Dim chkall As CheckBox = TryCast(head.FindControl(chkid), CheckBox)
            If gv.Rows.Count <> dt.Select(filter + "=True").Length Then
                chkall.Checked = False
            ElseIf gv.Rows.Count = dt.Select(filter + "=True").Length Then
                chkall.Checked = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub emptyGrid(ByVal gv As GridView, ByVal dt As DataTable)
        Try
            If dt Is Nothing Then
                dt.Rows.Add(dt.NewRow())
                gv.DataSource = dt
                gv.DataBind()
                gv.Rows(0).Visible = False
            Else
                If dt.Rows.Count <= 0 Then
                    dt.Rows.Add(dt.NewRow())
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.Rows(0).Visible = False
                Else
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue(ByVal xFormName As String, ByVal objclsTnaapprover_master As clsTnaapprover_master)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsTnaapprover_master._AuditUserId = CInt(Session("UserId"))
            End If
            objclsTnaapprover_master._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objclsTnaapprover_master._ClientIP = CStr(Session("IP_ADD"))
            objclsTnaapprover_master._LoginEmployeeunkid = -1
            objclsTnaapprover_master._HostName = CStr(Session("HOST_NAME"))
            objclsTnaapprover_master._FormName = xFormName
            objclsTnaapprover_master._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetAddedit()
        Try
            chkAddEditExtApprover.Checked = False
            drpAddEditApprover.SelectedIndex = 0
            drpAddEditApproverLevel.SelectedIndex = 0
            drpAddEditUser.SelectedIndex = 0
            chkHodCapForOT.Checked = False
            mblnpopupOTApproverAddEdit = False
            SelectedEmplyeeList(True)
            mintTnaMappingunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try
            If mdtPopupAddeditSelectEmpList Is Nothing Then Exit Sub
            Dim mdtRow As DataRow = Nothing
            Dim dtAssignEmp() As DataRow = Nothing
            dtAssignEmp = mdtPopupAddeditSelectEmpList.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtAssignEmp.Length <= 0 Then
                mdtRow = mdtPopupAddeditSelectEmpList.NewRow
                mdtRow.Item("ischeck") = False
                mdtRow.Item("tnaapprovertranunkid") = -1
                mdtRow.Item("tnamappingunkid") = mintTnaMappingunkid
                mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                mdtRow.Item("userunkid") = CInt(Session("UserId"))
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtRow.Item("ename") = dRow.Item("employeecode").ToString() + " - " + dRow.Item("name").ToString()
                mdtRow.Item("edept") = dRow.Item("departmentname")
                mdtRow.Item("ejob") = dRow.Item("jobname")
                mdtPopupAddeditSelectEmpList.Rows.Add(mdtRow)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue(ByVal objclsTnaapprover_master As clsTnaapprover_master)
        Try
            chkAddEditExtApprover.Checked = objclsTnaapprover_master._IsExternalApprover
            chkAddEditExtApprover_CheckedChanged(chkAddEditExtApprover, New EventArgs())

            If mintTnaMappingunkid > 0 Then
                chkAddEditExtApprover.Enabled = False
            End If

            drpAddEditApprover.SelectedValue = objclsTnaapprover_master._ApproverEmployeeId
            drpAddEditApprover_SelectedIndexChanged(drpAddEditApprover, New EventArgs())

            If CInt(drpAddEditApprover.SelectedValue) > 0 Then
                SelectedEmplyeeList(False)
            End If

            drpAddEditUser.SelectedValue = objclsTnaapprover_master._Mapuserunkid
            drpAddEditApproverLevel.SelectedValue = objclsTnaapprover_master._Tnalevelunkid


            'Pinkal (14-Dec-2021)-- Start
            'NMB OT TnA Approver Issue.
            chkHodCapForOT.Checked = objclsTnaapprover_master._Isotcap_Hod
            'Pinkal (14-Dec-2021) -- End



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Dropdown Event"

    Protected Sub drpAddEditApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpAddEditApprover.SelectedIndexChanged
        Dim objclsTnaapprover_master As New clsTnaapprover_master
        Try
            mstrEmployeeIDs = objclsTnaapprover_master.GetApproverEmployeeId(CInt(drpAddEditApprover.SelectedValue), chkAddEditExtApprover.Checked, chkHodCapForOT.Checked)
            FillEmployeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objclsTnaapprover_master = Nothing
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Protected Sub chkAddEditExtApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAddEditExtApprover.CheckedChanged
        Try
            PnlApproverUser.Visible = Not chkAddEditExtApprover.Checked
            drpAddEditUser.SelectedValue = "0"
            Call FillAddEditCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkSelectAddEditEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(mdtPopupEmpAddEditList, DataTable).Select("employeecode = '" & gvAddEditEmployee.DataKeys(gvRow.RowIndex)("employeecode").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkAllAddEditEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvAddEditEmployee.Rows.Count <= 0 Then Exit Sub
            If gvAddEditEmployee.Rows.Count > 0 Then
                For i As Integer = 0 To gvAddEditEmployee.Rows.Count - 1
                    If mdtPopupEmpAddEditList.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = mdtPopupEmpAddEditList.Select("employeecode = '" & gvAddEditEmployee.Rows(i).Cells(1).Text.Trim & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = gvAddEditEmployee.Rows(i)
                        CType(gvRow.FindControl("ChkSelectAddEditEmployee"), CheckBox).Checked = cb.Checked
                    End If
                    mdtPopupEmpAddEditList.AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(mdtPopupAddeditSelectEmpList, DataTable).Select("employeeunkid = '" & GvSelectedEmployee.DataKeys(gvRow.RowIndex)("employeeunkid").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkAllSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If GvSelectedEmployee.Rows.Count <= 0 Then Exit Sub
            If GvSelectedEmployee.Rows.Count > 0 Then
                For i As Integer = 0 To GvSelectedEmployee.Rows.Count - 1
                    If mdtPopupAddeditSelectEmpList.Rows.Count - 1 < i Then Exit For

                    Dim drRow As DataRow() = mdtPopupAddeditSelectEmpList.Select("employeeunkid = '" & CInt(GvSelectedEmployee.DataKeys(i)("employeeunkid")) & "'")

                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = GvSelectedEmployee.Rows(i)
                        CType(gvRow.FindControl("chkSelectedEmp"), CheckBox).Checked = cb.Checked
                    End If
                    mdtPopupAddeditSelectEmpList.AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnAddEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEmployee.Click
        Try
            If Validation() = False Then Exit Sub

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvAddEditEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelectAddEditEmployee"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                Dim dRow As DataRow() = mdtPopupEmpAddEditList.Select("employeeunkid = " & CInt(gvAddEditEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                    Add_DataRow(dRow(0))
                End If
            Next
            SelectedEmplyeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDeleteEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteEmployee.Click
        Try
            If mdtPopupEmpAddEditList Is Nothing Then Exit Sub
            confirmationSelectedEmp.Title = "Aruti"
            ''Language.setLanguage(mstrModuleName1)
            confirmationSelectedEmp.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Are you sure you want to delete Selected Employee?")
            confirmationSelectedEmp.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnpopupclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupclose.Click
        Try
            'txtAssignedEmpSearch.Text = ""
            mblnpopupOTApproverAddEdit = False
            chkHodCapForOT.Checked = False

            Session("OTApproverUnkId") = Nothing

            If mdtPopupEmpAddEditList IsNot Nothing Then mdtPopupEmpAddEditList.Clear()
            mdtPopupEmpAddEditList = Nothing

            If mdtPopupAddeditSelectEmpList IsNot Nothing Then mdtPopupAddeditSelectEmpList.Clear()
            mdtPopupAddeditSelectEmpList = Nothing

            Response.Redirect(Session("rootpath").ToString() & "TnA/OT_Requisition/OTRequisitionApprover.aspx", False)

            'ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnpopupsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupsave.Click
        Dim blnFlag As Boolean = False
        Dim objclsTnaapprover_master As clsTnaapprover_master

        Try
            If Validation() = False Then Exit Sub

            Dim xcount As Integer = mdtPopupAddeditSelectEmpList.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").DefaultIfEmpty().Count

            If xcount = 0 OrElse mdtPopupAddeditSelectEmpList.Rows.Count <= 0 OrElse GvSelectedEmployee.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 5, "Employee is compulsory information.Please assigned atleast One Employee to this approver."), Me)
                Exit Sub
            End If

            objclsTnaapprover_master = New clsTnaapprover_master
            SetValue(objclsTnaapprover_master)

            If mintTnaMappingunkid > 0 Then
                blnFlag = objclsTnaapprover_master.Update(mdtPopupAddeditSelectEmpList)
            Else
                blnFlag = objclsTnaapprover_master.Insert(mdtPopupAddeditSelectEmpList)
            End If

            If blnFlag = False And objclsTnaapprover_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsTnaapprover_master._Message, Me)
                Exit Sub
            End If

            If blnFlag Then
                mdtPopupEmpAddEditList.Rows.Clear()
                mdtPopupAddeditSelectEmpList.Rows.Clear()
                dtAssignEmpView = Nothing
            End If

            Session("OTApproverUnkId") = Nothing

            If mdtPopupEmpAddEditList IsNot Nothing Then mdtPopupEmpAddEditList.Clear()
            mdtPopupEmpAddEditList = Nothing

            If mdtPopupAddeditSelectEmpList IsNot Nothing Then mdtPopupAddeditSelectEmpList.Clear()
            mdtPopupAddeditSelectEmpList = Nothing

            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 6, "Approver Saved Successfully."), Me, Session("rootpath").ToString() & "TnA/OT_Requisition/OTRequisitionApprover.aspx")
            'ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objclsTnaapprover_master = Nothing
        End Try
    End Sub

    Protected Sub confirmationSelectedEmp_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmationSelectedEmp.buttonYes_Click
        Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = GvSelectedEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                ''Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 7, "Please check atleast one of the employee to unassigned."), Me)
                Exit Sub
            End If

            Dim drRow As DataRow() = Nothing
            Dim xCount As Integer = -1
            Dim objOtRequisition As New clsOT_Requisition_Tran

            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                If objOtRequisition.GetApproverPendingOTRequisition(CInt(GvSelectedEmployee.DataKeys(gRow(xCount).DataItemIndex)("tnamappingunkid")), CInt(GvSelectedEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")).ToString()).Trim().Length <= 0 Then
                    drRow = mdtPopupAddeditSelectEmpList.Select("employeeunkid = " & CInt(GvSelectedEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) & " AND AUD <> 'D'")
                Else
                    ''Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 8, "This Employee has Pending OT Requisition application form.You cannot delete this employee."), Me)
                    Exit For
                End If

                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    drRow(0).Item("AUD") = "D"
                    drRow(0).Item("isvoid") = True
                    drRow(0).Item("voiddatetime") = DateAndTime.Now
                    drRow(0).Item("voiduserunkid") = Session("UserId")
                    drRow(0).Item("voidreason") = ""
                    drRow(0).AcceptChanges()
                End If

            Next
            mdtPopupAddeditSelectEmpList.AcceptChanges()

            SelectedEmplyeeList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceSearch = popupAdvanceFilter._GetFilterString
            FillEmployeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "TextBox Event"

    'Protected Sub txtSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmployee.TextChanged
    '    Try
    '        If mdtPopupEmpAddEditList IsNot Nothing Then
    '            Dim dvEmployee As DataView = mdtPopupEmpAddEditList.DefaultView()
    '            If dvEmployee.Table.Rows.Count > 0 Then
    '                If txtSearchEmployee.Text.Trim.Length > 0 Then
    '                    dvEmployee.RowFilter = "employeecode like '%" & txtSearchEmployee.Text.Trim() & "%' OR name like '%" & txtSearchEmployee.Text.Trim() & "%' "
    '                End If
    '                gvAddEditEmployee.DataSource = dvEmployee.ToTable()
    '                gvAddEditEmployee.DataBind()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub txtAssignedEmpSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAssignedEmpSearch.TextChanged
    '    Try
    '        SelectedEmplyeeList(False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region "LinkButton Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            If CInt(drpAddEditApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "OT Approver is mandatory information. Please provide OT Approver to continue."), Me)
                Exit Sub
            Else
                popupAdvanceFilter.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            'txtSearchEmployee.Text = ""
            'If mdtPopupEmpAddEditList IsNot Nothing Then
            '    Dim dRow() As DataRow = mdtPopupEmpAddEditList.Select("isCheck = True")
            '    If dRow.Length > 0 Then
            '        For i As Integer = 0 To dRow.Length - 1
            '            dRow(i).Item("isCheck") = False
            '            dRow(i).AcceptChanges()
            '        Next
            '    End If
            'End If

            mstrAdvanceSearch = ""
            If gvAddEditEmployee.Rows.Count > 0 Then
                Call FillEmployeeList(False)
            Else
                Call FillEmployeeList(True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            ''Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.chkAddEditExtApprover.ID, Me.chkAddEditExtApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblApproverName.ID, Me.lblApproverName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblApproverlevel.ID, Me.lblApproverlevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblApproverUser.ID, Me.lblApproverUser.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.chkHodCapForOT.ID, Me.chkHodCapForOT.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkReset.ID, Me.lnkReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkAllocation.ID, Me.lnkAllocation.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkReset.ID, Me.lnkReset.ToolTip)
            'Gajanan [17-Sep-2020] -- End


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvAddEditEmployee.Columns(1).FooterText, Me.gvAddEditEmployee.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.gvAddEditEmployee.Columns(2).FooterText, Me.gvAddEditEmployee.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.GvSelectedEmployee.Columns(1).FooterText, Me.GvSelectedEmployee.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.GvSelectedEmployee.Columns(2).FooterText, Me.GvSelectedEmployee.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.GvSelectedEmployee.Columns(3).FooterText, Me.GvSelectedEmployee.Columns(3).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnAddEmployee.ID, Me.btnAddEmployee.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnDeleteEmployee.ID, Me.btnDeleteEmployee.Text.Replace("&", ""))

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnpopupsave.ID, Me.btnpopupsave.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnpopupclose.ID, Me.btnpopupclose.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName1)
            Me.lblPageHeader1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Me.lblApproverInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Me.chkAddEditExtApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.chkAddEditExtApprover.ID, Me.chkAddEditExtApprover.Text)
            Me.lblApproverName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblApproverName.ID, Me.lblApproverName.Text)
            Me.lblApproverlevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblApproverlevel.ID, Me.lblApproverlevel.Text)
            Me.lblApproverUser.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblApproverUser.ID, Me.lblApproverUser.Text)
            Me.chkHodCapForOT.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.chkHodCapForOT.ID, Me.chkHodCapForOT.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkReset.ID, Me.lnkReset.Text)
            'Me.lnkReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkReset.ID, Me.lnkReset.Text)
            Me.lnkAllocation.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkAllocation.ID, Me.lnkAllocation.ToolTip)
            Me.lnkReset.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkReset.ID, Me.lnkReset.ToolTip)
            'Gajanan [17-Sep-2020] -- End


            Me.gvAddEditEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvAddEditEmployee.Columns(1).FooterText, Me.gvAddEditEmployee.Columns(1).HeaderText)
            Me.gvAddEditEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.gvAddEditEmployee.Columns(2).FooterText, Me.gvAddEditEmployee.Columns(2).HeaderText)

            Me.GvSelectedEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.GvSelectedEmployee.Columns(1).FooterText, Me.GvSelectedEmployee.Columns(1).HeaderText)
            Me.GvSelectedEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.GvSelectedEmployee.Columns(2).FooterText, Me.GvSelectedEmployee.Columns(2).HeaderText)
            Me.GvSelectedEmployee.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.GvSelectedEmployee.Columns(3).FooterText, Me.GvSelectedEmployee.Columns(3).HeaderText)

            Me.btnAddEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnAddEmployee.ID, Me.btnAddEmployee.Text.Replace("&", ""))
            Me.btnDeleteEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnDeleteEmployee.ID, Me.btnDeleteEmployee.Text.Replace("&", ""))

            Me.btnpopupsave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnpopupsave.ID, Me.btnpopupsave.Text.Replace("&", ""))
            Me.btnpopupclose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnpopupclose.ID, Me.btnpopupclose.Text.Replace("&", ""))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 1, "OT Approver is mandatory information. Please provide OT Approver to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 2, "OT Approver Level is mandatory information. Please provide OT Approver Level to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 3, "User is mandatory information. Please provide User to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 4, "Are you sure you want to delete Selected Employee?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 5, "Employee is compulsory information.Please assigned atleast One Employee to this approver.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 6, "Approver Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 7, "Please check atleast one of the employee to unassigned.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 8, "This Employee has Pending OT Requisition application form.You cannot delete this employee.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>





End Class
