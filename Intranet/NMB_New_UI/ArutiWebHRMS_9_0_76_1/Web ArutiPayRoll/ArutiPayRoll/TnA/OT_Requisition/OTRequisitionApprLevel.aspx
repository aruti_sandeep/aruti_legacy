﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="OTRequisitionApprLevel.aspx.vb" Inherits="TnA_OT_Requisition_OTRequisitionApprLevel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="OT Requisition Approver Level List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader2" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:GridView ID="GvApprLevelList" DataKeyNames="tnalevelunkid" runat="server" AutoGenerateColumns="False"
                                                Width="99%" CssClass="table table-hover table-bordered" AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderText="Edit" FooterText="colhedit">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("tnalevelunkid") %>'>
                                                                <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderText="Delete" FooterText="colhdelete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("tnalevelunkid") %>'> 
                                                                    <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="tnalevelcode" HeaderText="Level Code" HeaderStyle-Width="20%"
                                                        FooterText="colhtnalevelcode"></asp:BoundField>
                                                    <asp:BoundField DataField="tnalevelname" HeaderText="Level Name" HeaderStyle-Width="25%"
                                                        FooterText="colhtnalevelname"></asp:BoundField>
                                                    <asp:BoundField DataField="tnapriority" HeaderText="Priority Level" HeaderStyle-Width="15%"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterText="colhtnapriority">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" CssClass="btn btn-primary" Text="New" OnClick="btnnew_Click" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupOTRequiApproverLevel" BackgroundCssClass="modal-backdrop"
                    TargetControlID="txtlevelcode" runat="server" PopupControlID="pnlOTRequiApprover"
                    DropShadow="false" CancelControlID="lblCancelText1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlOTRequiApprover" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="OT Requisition Approver Level Add/ Edit" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelcode" runat="server" Text="Level Code" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelcode" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelname" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelpriority" runat="server" Text="Priority" CssClass="form-label"/>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelpriority" runat="server" Enabled="false" ></asp:TextBox>
                                        <cc1:NumericUpDownExtender ID="nudYear" runat="server" Width="100" Minimum="0" TargetControlID="txtlevelpriority">
                                        </cc1:NumericUpDownExtender>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveOTRequiApprover" runat="server" CssClass="btn btn-primary"
                            Text="Save" />
                        <asp:Button ID="btnCloseOTRequiApprover" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                    </div>
                </asp:Panel>
                <uc7:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
