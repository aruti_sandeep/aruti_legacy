﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" Title="Employee OT Assignment"
    CodeFile="OT_EmployeeAssignment.aspx.vb" Inherits="TnA_OT_Requisition_OT_EmployeeAssignment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAll]", grid);
            debugger;
            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            //called when key is pressed in textbox
            $(".OnlyNumbers").keypress(function(e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });

        //Pinkal (29-Apr-2020) -- Start
        //Error  -  Solved Error when User Filter anything from list screen and search it was giving error.--%>
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function Searching() {
            if ($('#txtSearch').val().length > 0) {
                $('#<%= dgvEmployee.ClientID %> tbody tr').hide();
                $('#<%= dgvEmployee.ClientID %> tbody tr:first').show();
                $('#<%= dgvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
                var TotalCount = $('#<%= dgvEmployee.ClientID %> tr:visible').length - 1;
                $('#<%= lblEmployeeCount.ClientID %>').html('Employee Count : ' + TotalCount);
            }
            else if ($('#txtSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearch').val('');
            $('#<%= dgvEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearch').focus();
            var TotalCount = $('#<%= dgvEmployee.ClientID %> tr:visible').length - 1;
            $('#<%= lblEmployeeCount.ClientID %>').html('Employee Count : ' + TotalCount);
        }
        //Pinkal (29-Apr-2020) -- End
        
    </script>

    <script type="text/javascript">

        function numbersLimit(input) {
            if (input.value < 0) input.value = 0;
            if (input.value > 12) {
                alert("Months cannot be greater than 12");
                input.value = 12;
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee OT Assignment"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                 <ul class="header-dropdown m-r--5">
                                         <li class="dropdown">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocation">
                                                   <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                      </li>
                                 </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radExpYear" runat="server" Text="Year of Experience" AutoPostBack="true"
                                            GroupName="Group" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radAppointedDate" runat="server" Text="Appointment Date" AutoPostBack="true"
                                            GroupName="Group" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radProbationDate" runat="server" Text="Probation Date" AutoPostBack="true"
                                            GroupName="Group" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radConfirmationDate" runat="server" Text="Confirmation Date"
                                            AutoPostBack="true" GroupName="Group" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnl_FromYear" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromYear" runat="server" Text="From Year" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtFromYear" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                        CssClass="form-control OnlyNumbers"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromMonth" runat="server" Style="margin-left: 25px" Text="From Month"
                                                CssClass="form-label"></asp:Label>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtFromMonth" runat="server" Style="text-align: right" Text="0"
                                                            onchange="numbersLimit(this);" CssClass="form-control OnlyNumbers"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboFromcondition" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_Date" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateCtrl ID="dtpFromDate" runat="server" />
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateCtrl ID="dtpToDate" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnl_ToYear" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToYear" runat="server" Text="To Year" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtToYear" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                        CssClass="form-control OnlyNumbers"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToMonth" runat="server" Style="margin-left: 25px" Text="To Month"
                                                CssClass="form-label"></asp:Label>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtToMonth" runat="server" Style="text-align: right" Text="0" onchange="numbersLimit(this);"
                                                            CssClass="form-control OnlyNumbers"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboTocondition" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGender" runat="server" CssClass="form-label" Text="Gender"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboGender" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="txtSearch" name="txtSearch" placeholder="Type To Search Text"
                                                    maxlength="100" onkeyup="Searching();" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15">
                                        <asp:Label ID="lblFrom" runat="server" Text="From:" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-t-15">
                                        <asp:Label ID="lblValue" runat="server" Text="#Value" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-15">
                                        <asp:Label ID="lblEmployeeCount" runat="server" Text="#Count" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="dgvEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                DataKeyField="employeeunkid" CssClass="table table-hover table-bordered">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAll" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="code" HeaderText="Code" ReadOnly="true" FooterText="dgColhEmpCode" />
                                                    <asp:BoundColumn DataField="Employee Name" HeaderText="Employee" ReadOnly="true"
                                                        FooterText="dgColhEmployee" />
                                                    <asp:BoundColumn DataField="Appointed Date" HeaderText="Appointment Date" ReadOnly="true"
                                                        FooterText="dgcolhAppointedDate" ItemStyle-Width="130px" />
                                                    <asp:BoundColumn DataField="Cofirmation Date" HeaderText="Confirmation Date" ReadOnly="true"
                                                        FooterText="dgcolhConfirmationDate" ItemStyle-Width="130px" />
                                                    <asp:BoundColumn DataField="Probation From Date" HeaderText="Probation From Date"
                                                        ReadOnly="true" FooterText="dgcolhProbationFromDate" ItemStyle-Width="130px" />
                                                    <asp:BoundColumn DataField="Probation To Date" HeaderText="Probation To Date" ReadOnly="true"
                                                        FooterText="dgcolhProbationToDate" ItemStyle-Width="130px" />
                                                    <asp:BoundColumn DataField="job" HeaderText="Job Title" ReadOnly="true" FooterText="dgcolhjob">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ischecked" HeaderText="" Visible="false" ReadOnly="true"
                                                        FooterText="objSelect" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="" Visible="false" ReadOnly="true" />
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
