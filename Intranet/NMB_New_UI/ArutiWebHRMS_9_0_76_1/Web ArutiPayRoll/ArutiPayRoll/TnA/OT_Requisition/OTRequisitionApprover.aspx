﻿<%@ Page Title="OT Requisition Approver" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="OTRequisitionApprover.aspx.vb" Inherits="TnA_OT_Requisition_OTRequisitionApprover" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

 
//        'Gajanan [17-Sep-2020] -- Start
//        'New UI Change
//        
//        $("[id*=ChkAllAddEditEmployee]").live("click", function() {
//            var chkHeader = $(this);
//            var grid = $(this).closest("table");
//            $("input[type=checkbox]", grid).each(function() {
//                if (chkHeader.is(":checked")) {
//                    debugger;
//                    $(this).attr("checked", "checked");

//                } else {
//                    $(this).removeAttr("checked");
//                }
//            });
//        });

//        $("[id*=ChkSelectAddEditEmployee]").live("click", function() {
//            var grid = $(this).closest("table");
//            var chkHeader = $("[id*=chkHeader]", grid);
//            var row = $(this).closest("tr")[0];

//            debugger;
//            if (!$(this).is(":checked")) {
//                var row = $(this).closest("tr")[0];
//                chkHeader.removeAttr("checked");
//            } else {

//                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                    chkHeader.attr("checked", "checked");
//                }
//            }
//        });


//        $("[id*=ChkAllSelectedEmp]").live("click", function() {
//            var chkHeader = $(this);
//            var grid = $(this).closest("table");
//            $("input[type=checkbox]", grid).each(function() {
//                if (chkHeader.is(":checked")) {
//                    debugger;
//                    $(this).attr("checked", "checked");

//                } else {
//                    $(this).removeAttr("checked");
//                }
//            });
//        });

//        $("[id*=ChkSelectedEmp]").live("click", function() {
//            var grid = $(this).closest("table");
//            var chkHeader = $("[id*=chkHeader]", grid);
//            var row = $(this).closest("tr")[0];

//            debugger;
//            if (!$(this).is(":checked")) {
//                var row = $(this).closest("tr")[0];
//                chkHeader.removeAttr("checked");
//            } else {

//                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                    chkHeader.attr("checked", "checked");
//                }
//            }
//        });

//       'Gajanan [17-Sep-2020] -- End
    
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="OT Approver"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="OT Approver List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Approver Level"  CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpLevel" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver"  CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpApprover" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="gvApproverList" DataKeyNames="tnamappingunkid" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit" OnClick="lnkedit_Click"
                                                                CommandArgument='<%#Eval("tnamappingunkid") %>'>
                                                                 <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click"
                                                                CommandArgument='<%#Eval("tnamappingunkid") %>' ToolTip="Delete">
                                                                <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Active" OnClick="lnkActive_Click"
                                                                CommandArgument='<%#Eval("tnamappingunkid") %>'>
                                                                <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="DeActive" OnClick="lnkDeActive_Click"
                                                                CommandArgument='<%#Eval("tnamappingunkid") %>'>
                                                                 <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Approver Name" DataField="Name" FooterText="dgcolhName" />
                                                    <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhdepartmentname" />
                                                    <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhjobname" />
                                                    <asp:BoundField HeaderText="Mapped User" DataField="MappedUser" FooterText="dgcolhMappedUser" />
                                                    <asp:BoundField HeaderText="External Approver" DataField="exappr" FooterText="dgcolhexappr" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
   <%--             <cc1:ModalPopupExtender ID="popupOTApproverAddEdit" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblCaption" runat="server" PopupControlID="pnlOTAddEditApprover"
                    CancelControlID="lblApproverInfo" />
                <asp:Panel ID="pnlOTAddEditApprover" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPageHeader1" runat="server" Text="Add/ Edit OT Approver"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblApproverInfo" runat="server" Text="Approvers Info" />
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkAddEditExtApprover" runat="server" AutoPostBack="true" CssClass="filled-in"
                                                    Text="Make External Approver" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblApproverName" runat="server" Text="Name" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="drpAddEditApprover" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblApproverlevel" runat="server" Text="Level" CssClass="form-label" />
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="drpAddEditApproverLevel" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <asp:Panel ID="PnlApproverUser" runat="server">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblApproverUser" runat="server" Text="User" CssClass="form-label" />
                                                    <div class="form-group">
                                                        <asp:DropDownList data-live-search="true" ID="drpAddEditUser" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkHodCapForOT" runat="server" AutoPostBack="False" CssClass="filled-in"
                                                    Text="HOD Cap for OT" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtSearchEmployee" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 300px">
                                                    <asp:GridView ID="gvAddEditEmployee" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                        DataKeyNames="employeeunkid,employeecode" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkAllAddEditEmployee" runat="server" CssClass="filled-in" Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkSelectAddEditEmployee" runat="server" CssClass="filled-in" Text=" " />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Employee Code" DataField="employeecode" FooterText="dgcolhEcode" />
                                                            <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEname" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnAddEmployee" runat="server" Text="Add" CssClass="btn btn-default" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="LblOTAddEditAssignedEmp" runat="server" Text="Selected Employee" Font-Bold="true" />
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtAssignedEmpSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="false"
                                                    CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="employeeunkid,tnamappingunkid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Employee" DataField="ename" FooterText="dgcolhEmpName" />
                                                        <asp:BoundField HeaderText="Department" DataField="edept" FooterText="dgcolhEDept" />
                                                        <asp:BoundField HeaderText="Job" DataField="ejob" FooterText="dgcolhEJob" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnDeleteEmployee" runat="server" Text="Delete" CssClass="btn btn-default" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer"> 
                        <asp:Button ID="btnpopupsave" runat="server" Text="Save" CssClass="btn btn-primary" /> 
                        <asp:Button ID="btnpopupclose" runat="server" Text="Close" CssClass="btn btn-default" /> 
                    </div> 
                    <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </asp:Panel>--%>
                <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                <uc9:ConfirmYesNo ID="confirmationSelectedEmp" runat="server" />
                <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" />
                <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" />
                <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
