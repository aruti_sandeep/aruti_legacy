﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ShiftGlobalAssign.aspx.vb"
    Inherits="TnA_Shift_Policy_Global_Assign_wPg_ShiftGlobalAssign" Title="Global Shift Assign" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">


        $("body").on("click", "[id*=chkEmpAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkEmpSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=chkEmpSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkEmpAllSelect]", grid);
            debugger;
            if ($("[id*=chkEmpSelect]", grid).length == $("[id*=chkEmpSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $("body").on("click", "[id*=chkShiftAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkShiftSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=chkShiftSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkShiftAllSelect]", grid);
            debugger;
            if ($("[id*=chkShiftSelect]", grid).length == $("[id*=chkShiftSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function Searching() {
            if ($('#txtSearch').val().length > 0) {
                $('#<%= dgvEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
            }
            else if ($('#txtSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearch').val('');
            $('#<%= dgvEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearch').focus();
        }
    
    
    
    
    
    
    
    
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Shift/Policy Assignment"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="LblFilterShiftinfo" runat="server" Text="Filter Shift"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblFilterShiftType" runat="server" Text="Shift Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboFilterShiftType" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblFilterShift" runat="server" Style="margin-left: 10px" Text="Shift"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboFilterShift" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Shift Assignment"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="LblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="LblShift" runat="server" Style="margin-left: 10px" Text="Shift" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboShift" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                               <%-- <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"></asp:TextBox>--%>
                                                                 <input type="text" id="txtSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                        maxlength="50" class="form-control" onkeyup="Searching();" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="table-responsive" style="height: 400px">
                                                            <asp:UpdatePanel ID="UpdateEmployeeList" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="false" AllowPaging="false" DataKeyNames = "employeeunkid"
                                                                        CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkEmpAllSelect" runat="server" CssClass="filled-in" Text=" "  />   <%--AutoPostBack="true"  OnCheckedChanged="chkEmpSelectAll_CheckedChanged"  --%>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkEmpSelect" runat="server" CssClass="filled-in" Text=" "  /> <%--AutoPostBack="true"  OnCheckedChanged="chkEmpSelect_CheckedChanged"   --%>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhEcode" />
                                                                            <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="True" FooterText="dgcolhEName" />
                                                                            <asp:BoundField DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                                                Visible="false" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="table-responsive" style="height: 400px">
                                                            <asp:UpdatePanel ID="UpdateAssigedShift" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="dgvAssignedShift" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                                        CssClass="table table-hover table-bordered">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkShiftAllSelect" runat="server" CssClass="filled-in" Text=" " />  <%--AutoPostBack="true" OnCheckedChanged="chkShiftSelectAll_CheckedChanged"--%>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkShiftSelect" runat="server" CssClass="filled-in" Text=" " />  <%--AutoPostBack="true" OnCheckedChanged="chkShiftSelect_CheckedChanged"--%>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="effectivedate" HeaderText="Effective Date" ReadOnly="True"
                                                                                FooterText="colhEffectiveDate" />
                                                                            <asp:BoundField DataField="shifttype" HeaderText="Shift Type" ReadOnly="True" FooterText="colhShiftType" />
                                                                            <asp:BoundField DataField="shiftname" HeaderText="Shift" ReadOnly="True" FooterText="colhShiftName" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAssignShift" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnDeleteShift" runat="server" Text="Delete" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="panel-primary">
                        <div class="panel-heading">
                           
                        </div>
                        <div class="panel-body">
                        <div id="FilterShiftCriteria" class="panel-default">
                                <div id="Div1" class="panel-heading-default">
                                    <div style="float: left;">
                                       
                                    </div>
                                </div>
                                
                                <div id="FilterShiftCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 35%">
                                                 
                                            </td>
                                            <td style="width: 10%">
                                                
                                            </td>
                                            <td style="width: 40%; text-align: left; padding-right: 15px">
                                                
                                            </td>
                                        </tr>
                                 </table>
                                 </div>
                            </div>
                            
                          <%--  Pinkal (06-Nov-2017) -- End
                        
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                      
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 35%">
                                                
                                            </td>
                                            <td style="width: 10%">
                                               
                                            </td>
                                            <td style="width: 40%; text-align: left; padding-right: 15px">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; padding-bottom: 10px">
                                            <td style="width: 100%" colspan="4" />
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="2">
                                               
                                            </td>
                                            <td style="width: 50%" colspan="2" align="right">
                                              
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%; padding-right: 5px" colspan="2">
                                                <div id="scrollable-container" style="overflow: auto; height: 400px; vertical-align: top"
                                                    onscroll="$(scroll.Y).val(this.scrollTop);">
                                                   
                                                </div>
                                            </td>
                                            <td style="width: 50%; padding-left: 5px" colspan="2">
                                                <div id="scrollable-container1" style="overflow: auto; height: 400px; vertical-align: top"
                                                    onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
