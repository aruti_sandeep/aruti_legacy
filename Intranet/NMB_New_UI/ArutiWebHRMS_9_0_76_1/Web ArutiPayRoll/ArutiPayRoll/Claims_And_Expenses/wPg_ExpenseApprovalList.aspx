﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ExpenseApprovalList.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_ExpenseApprovalList" Title="Expense Approval List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Expense Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExpCategory" runat="server" Text="Expense Cat." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpCategory" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFDate" AutoPostBack="false" runat="server" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpTDate" AutoPostBack="false" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                        <asp:Label ID="lblPeriod" runat="server" Text="Priod" Visible="false" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtExpApprover" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-35">
                                        <asp:CheckBox ID="ChkMyApprovals" runat="server" Text="My Approvals" Checked="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnSearch" CssClass="btn btn-primary" runat="server" Text="Search" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="lvClaimRequestList" runat="server" AllowPaging="true" PagerStyle-Mode="NumericPages" AutoGenerateColumns = "false"
                                                PageSize="250" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="Top" PagerStyle-Wrap="true"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center"  FooterText="btnChangeStatus" HeaderStyle-Width="50px" ItemStyle-Width = "50px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Approval"  ToolTip = "Change Status" >
                                                                 <img src="../images/change_status.png" height="20px" width="20px" alt="Change Status" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="claimrequestno" HeaderText="Claim No" ReadOnly="true"
                                                        FooterText="colhclaimno" />
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Emp. Code" ReadOnly="true"
                                                        FooterText="colhEmployeecode" />
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhemployee" />
                                                    <asp:BoundColumn DataField="approvername" HeaderText="Approver" ReadOnly="true" FooterText="colhExpenseApprover" />
                                                    <asp:BoundColumn DataField="period" HeaderText="Period" ReadOnly="true" FooterText="colhperiod"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="expensetype" HeaderText="Expense Cat." ReadOnly="true"
                                                        FooterText="colhExpType" />
                                                    <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" ReadOnly="true"
                                                        FooterText="colhdate" />
                                                    <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="colhamount"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <%--'Pinkal (13-Aug-2020) -- Start
                                                                  'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                                                                   <asp:BoundColumn DataField="status" HeaderText="Status" ReadOnly="true" FooterText="colhStatus" />--%>
                                                    <asp:BoundColumn DataField="AppprovalStatus" HeaderText="Status" ReadOnly="true"
                                                        FooterText="colhStatus" />
                                                    <%--'Pinkal (13-Aug-2020) -- End--%>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeunkId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="crapproverunkid" HeaderText="approverunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="approveremployeeunkid" HeaderText="approverEmpID" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="iscancel" HeaderText="IsCancel" ReadOnly="true" Visible="false" />
                                                    <asp:BoundColumn DataField="mapuserunkid" HeaderText="Map UserId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="crpriority" HeaderText="Priority" ReadOnly="true" Visible="false" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="crmasterunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="crlevelname" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="tdate" HeaderText="objcolhTranscationDate" Visible="false" />
                                                    <asp:BoundColumn DataField="isexternalapprover" Visible="false" />
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false" FooterText="objcolhIsGrp" />
                                                    <asp:BoundColumn DataField="crstatusunkid" Visible="false" FooterText="objcolhcrstatusunkid" />
                                                     <asp:BoundColumn DataField="base_amount" FooterText="dgcolhBasicAmount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Base Amount" ItemStyle-HorizontalAlign="Right" Visible = "false" />
                                                </Columns>
                                            </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
