﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
#End Region

Partial Class Claims_And_Expenses_wPg_GlobalApproveExpense
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmGlobalApproveExpense"
    Private mstrEmployeeIDs As String = ""
    Dim mblnIsExternalApprover As Boolean = False
    'Gajanan [20-July-2020] -- Start
    'Enhancement: Optimize For NMB
    'Dim dtList As DataTable = Nothing
    'Gajanan [20-July-2020] -- End
    Dim mintPriority As Integer = -1

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsExpenseSaveClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Pinkal (25-Jan-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    Dim objClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (25-Jan-2022) -- End


#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If IsPostBack = False Then
                SetLanguage()
                GC.Collect()
                Call FillCombo()
            Else
                mstrEmployeeIDs = Me.ViewState("mstrEmployeeIDs").ToString()
                mblnIsExternalApprover = CBool(Me.ViewState("ExternalApprover"))
                mintPriority = CInt(Me.ViewState("Priority"))
                mblnIsExpenseSaveClick = CBool(Me.ViewState("mblnIsExpenseSaveClick"))
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrEmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("ExternalApprover") = mblnIsExternalApprover
            Me.ViewState("Priority") = mintPriority
            Me.ViewState("mblnIsExpenseSaveClick") = mblnIsExpenseSaveClick
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            Dim objMasterData As New clsMasterData
            Dim dtab As DataTable = Nothing
            If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" Then
                dsCombo = objMasterData.getLeaveStatusList("List", "", False, False, True)
                dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (1,3,8)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            Else
                dsCombo = objMasterData.getLeaveStatusList("List", "")
                dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (1,3)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            End If
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()
            End With
            objMasterData = Nothing


            Dim objcommonMst As New clsCommon_Master
            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSectorRoute
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objcommonMst = Nothing

            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing

            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            cboExpenseCategory_SelectedIndexChanged(New Object, New EventArgs())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        Dim intMode As Integer = 0
        Dim mstrSearch As String = ""
        Try

            Dim objApproval As New clsclaim_request_approval_tran

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_request_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_request_master.crmasterunkid IN (" & objApproval.GetClaimFormFromExpSector(CInt(cboExpense.SelectedValue), 0) & ")"
            End If

            If CInt(cboSectorRoute.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_approval_tran.secrouteunkid = " & CInt(cboSectorRoute.SelectedValue) & " "
            End If

            If ViewState("EmpAdvanceSearch") IsNot Nothing Then
                mstrSearch &= "AND " & ViewState("EmpAdvanceSearch").ToString()
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If

            Dim dtList As DataTable = objApproval.GetGlobalApprovalData(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                    , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                                    , Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(Session("LeaveBalanceSetting")) _
                                                                                                    , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboExpenseCategory.SelectedValue) _
                                                                                                    , CInt(cboApprover.SelectedValue), mstrSearch, True, mblnIsExternalApprover)

            dgvData.DataSource = dtList
            dgvData.DataBind()

            If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Visible = True
            Else
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GlobalSave()
        Try
            Dim objLeaveApprover As New clsleaveapprover_master
            Dim objExpApproverTran As New clsclaim_request_approval_tran
            Dim objExpAppr As New clsExpenseApprover_Master
            Dim objClaimMst As New clsclaim_request_master
            Dim mintMaxPriority As Integer = -1
            Dim mstrRejectRemark As String = ""
            Dim blnLastApprover As Boolean = False
            Dim mblnIssued As Boolean = False
            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING


            Dim dtDate As Date = Now

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text) = False)


            Dim mblnWarning As Boolean = False

            For Each dgrow As DataGridItem In gRow

                Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
                                                                                                     , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpenseCategory.SelectedValue) _
                                                                                                     , False, True, -1, "", CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text))

                Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintPriority, "crpriority asc", DataViewRowState.CurrentRows).ToTable
                mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhmodulerefunkid", False, True)).Text) = enModuleReference.Leave Then
                    objLeaveApprover = New clsleaveapprover_master

                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhreferenceunkid", False, True)).Text)
                    If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = mintPriority) Then mblnIssued = True
                    objLeaveForm = Nothing

                End If

                Dim objApproval As New clsclaim_request_approval_tran
                Dim mstrSearch As String = ""

                mstrSearch = "AND cmclaim_request_master.crmasterunkid = " & CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text)

                If mstrSearch.Trim.Length > 0 Then
                    mstrSearch = mstrSearch.Trim.Substring(3)
                End If

                Dim dtFilter As DataTable = objApproval.GetGlobalApprovalData(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                                        , Session("UserAccessModeSetting").ToString(), True, False, "List", CInt(Session("LeaveBalanceSetting")) _
                                                                                                        , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboExpenseCategory.SelectedValue) _
                                                                                                        , CInt(cboApprover.SelectedValue), mstrSearch, True, mblnIsExternalApprover)
                objApproval = Nothing

                If dtFilter IsNot Nothing AndAlso dtFilter.Rows.Count <= 0 Then Continue For

                Dim mblnReturntoApplicant As Boolean = False
                Dim mblnIsRejected As Boolean = False
                Dim mintApproverApplicationStatusId As Integer = 2

                For j As Integer = 0 To dtApproverTable.Rows.Count - 1
                    blnLastApprover = False
                    Dim mintApproverEmpID As Integer = -1
                    Dim mintApproverID As Integer = -1
                    Dim mdtApprovalDate As Date = Nothing

                    If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhmodulerefunkid", False, True)).Text) = enModuleReference.Leave Then

                        objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))

                        If CInt(cboApprover.SelectedValue) = objLeaveApprover._Approverunkid Then
                            mintStatusID = CInt(cboStatus.SelectedValue)
                            mintVisibleID = CInt(cboStatus.SelectedValue)
                            mintApproverApplicationStatusId = mintStatusID
                            mdtApprovalDate = DateTime.Now

                            If CInt(cboStatus.SelectedValue) = 1 Then
                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True

                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                                
                            ElseIf CInt(cboStatus.SelectedValue) = 8 Then  'Return to Applicant
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                                mblnReturntoApplicant = True
                            End If

                        ElseIf CInt(cboApprover.SelectedValue) <> objLeaveApprover._Approverunkid Then

                            Dim mintCurrentPriority As Integer = -1

                            Dim dRow As DataRow() = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

                            If dRow.Length > 0 Then
                                mintStatusID = 2
                                If mblnReturntoApplicant Then mintVisibleID = -1 Else mintVisibleID = 1
                                mintApproverApplicationStatusId = mintStatusID

                            Else
                                mintCurrentPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

                                If mintCurrentPriority <= -1 Then
                                    mintStatusID = 2
                                    mintVisibleID = 1
                                    mintApproverApplicationStatusId = mintStatusID
                                    GoTo AssignApprover

                                ElseIf mblnReturntoApplicant Then
                                    mintStatusID = 2
                                    mintVisibleID = -1

                                ElseIf mblnIsRejected Then
                                    mintVisibleID = -1
                                    mintStatusID = -1

                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 1
                                    mintStatusID = 1

                                ElseIf mintCurrentPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 2
                                    mintStatusID = 2

                                Else
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                End If


                                mintApproverApplicationStatusId = mintStatusID
                            End If
                            mdtApprovalDate = Nothing
                        End If

AssignApprover:
                        mintApproverEmpID = objLeaveApprover._leaveapproverunkid
                        mintApproverID = objLeaveApprover._Approverunkid

                    Else

                        objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))
                        If CInt(cboApprover.SelectedValue) = objExpAppr._crApproverunkid Then

                            mintStatusID = CInt(cboStatus.SelectedValue)
                            mintVisibleID = CInt(cboStatus.SelectedValue)
                            mintApproverApplicationStatusId = mintStatusID
                            mdtApprovalDate = DateTime.Now
                            If CInt(cboStatus.SelectedValue) = 1 Then
                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then blnLastApprover = True

                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                                mblnIsRejected = True

                            ElseIf CInt(cboStatus.SelectedValue) = 8 Then  'Return to Applicant
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                                mblnReturntoApplicant = True

                            End If

                        ElseIf CInt(cboApprover.SelectedValue) <> objExpAppr._crApproverunkid Then
                            Dim mintCurrentPriority As Integer = -1

                            Dim dRow As DataRow() = dtApproverTable.Select("crpriority = " & mintPriority & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

                            If dRow.Length > 0 Then
                                mintStatusID = 2
                                If mblnReturntoApplicant Then mintVisibleID = -1 Else mintVisibleID = 1
                                mintApproverApplicationStatusId = mintStatusID

                            Else

                                mintCurrentPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

                                If mintCurrentPriority <= -1 Then GoTo AssignApprover1

                                If mblnReturntoApplicant Then
                                    mintStatusID = 2
                                    mintVisibleID = -1

                                ElseIf mblnIsRejected Then
                                    mintStatusID = -1
                                    mintVisibleID = -1

                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 1
                                    mintStatusID = 1

                                ElseIf mintCurrentPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 2
                                    mintStatusID = 2

                                Else

                                    mintVisibleID = -1
                                    mintStatusID = -1
                                End If

                                mintApproverApplicationStatusId = mintStatusID
                                mdtApprovalDate = Nothing
                            End If


                        End If

AssignApprover1:
                        mintApproverEmpID = objExpAppr._Employeeunkid
                        mintApproverID = objExpAppr._crApproverunkid

                    End If

                    
                    'Dim drFilter As DataRow() = dtFilter.Select("IsGrp = 0 AND crmasterunkid = '" & CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text) & "'")
                    Dim drFilter As DataRow() = dtFilter.Select("IsGrp = 0 AND crmasterunkid = '" & CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text) & "'")



                    If IsNothing(drFilter) = False AndAlso drFilter.Length > 0 Then
                        drFilter(0)("amount") = dgrow.Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text
                        drFilter(0)("unitprice") = CType(dgrow.Cells(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).FindControl("txtdgcolhUnitPrice"), TextBox).Text
                        drFilter(0)("quantity") = CType(dgrow.Cells(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).FindControl("txtdgcolhQuantity"), TextBox).Text
                        drFilter(0)("Expense_Remark") = CType(dgrow.Cells(getColumnId_Datagrid(dgvData, "dgcolhExpenseRemark", False, True)).FindControl("txtdgcolhExpenseRemark"), TextBox).Text
                        drFilter(0)("base_amount") = CDec(dgrow.Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text)
                        drFilter(0)("exchange_rate") = CDec(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhExchangeRate", False, True)).Text)
                        drFilter(0).AcceptChanges()
                    End If


                    Dim drExpense = dtFilter.Select("IsGrp = 0 AND crmasterunkid = " & CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text) & " AND expenseunkid > 0")

                    Dim sMsg As String = String.Empty

                    sMsg = objClaimMst.IsValid_Expense(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), CInt(drExpense(0)("expenseunkid")), CInt(Session("Fin_year")) _
                                                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CDate(drExpense(0)("ClaimDate")).Date _
                                                                                      , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text), False)
                  

                    If sMsg <> "" Then
                        DisplayMessage.DisplayMessage(sMsg, Me)
                        mblnWarning = True
                        Continue For
                    End If

                    Dim drRow() As DataRow = dtFilter.Select("IsGrp = False AND AUD = ''")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            dr("AUD") = "U"
                            dr.AcceptChanges()
                        Next
                    End If

                    Dim dFilter As DataTable = New DataView(dtFilter, "IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable()

                    objExpApproverTran._DataTable = dFilter.Copy()
                    objExpApproverTran._CompanyID = CInt(Session("CompanyUnkId"))
                    objExpApproverTran._CompanyCode = Session("CompanyCode").ToString()
                    objExpApproverTran._YearId = CInt(Session("Fin_year"))
                    objExpApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                    objExpApproverTran._WebFormName = mstrModuleName
                    objExpApproverTran._WebClientIP = CStr(Session("IP_ADD"))
                    objExpApproverTran._WebHostName = CStr(Session("HOST_NAME"))
                    objClaimMst._WebFormName = mstrModuleName
                    objClaimMst._WebClientIP = CStr(Session("IP_ADD"))
                    objClaimMst._WebHostName = CStr(Session("HOST_NAME"))

                    objExpApproverTran._EmployeeID = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text)

                    If objExpApproverTran.Insert_Update_ApproverData(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                              , False, CBool(Session("PaymentApprovalwithLeaveApproval")), mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, CInt(Session("UserId")) _
                                                                                              , Now.Date, CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text), Nothing, mstrRejectRemark _
                                                                                              , blnLastApprover, mdtApprovalDate) = False Then
                        Exit For
                    End If

                    objClaimMst._Crmasterunkid = CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text)

                    
                    If CInt(Session("EFTIntegration")) = enEFTIntegration.DYNAMICS_NAVISION Then

                        If objClaimMst._Statusunkid = 1 Then
                            Dim objCurrentPeriodId As New clsMasterData
                            Dim objPeriod As New clscommom_period_Tran
                            Dim mintCurrentPeriodId As Integer = objCurrentPeriodId.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1, , True)
                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = mintCurrentPeriodId
                            objClaimMst.SendToDynamicNavision(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text), CInt(Session("UserId")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, _
                                                                   CStr(Session("SQLDataSource")), _
                                                                   CStr(Session("SQLDatabaseName")), _
                                                                   CStr(Session("SQLDatabaseOwnerName")), _
                                                                   CStr(Session("SQLUserName")), _
                                                                   CStr(Session("SQLUserPassword")), _
                                                                   CStr(Session("FinancialYear_Name")))

                        End If

                    End If
               
                    If mintApproverApplicationStatusId = 8 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email
                        objClaimMst.SendMailToEmployee(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), objClaimMst._Claimrequestno, mintApproverApplicationStatusId, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRemarks.Text.Trim, True)
                        objEmployee = Nothing
                    End If

                    If objClaimMst._Statusunkid = 2 AndAlso mintApproverApplicationStatusId = 1 Then
                        objExpApproverTran.SendMailToApprover(CInt(cboExpenseCategory.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text) _
                                                                                    , dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCLno", False, True)).Text, CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), mintPriority, 1, "crpriority > " & mintPriority, Session("Database_Name").ToString() _
                                                                                    , Session("EmployeeAsOnDate").ToString(), CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString() _
                                                                                            , enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), "", Nothing, True)
                    End If

                    If objClaimMst._Statusunkid = 1 AndAlso mintApproverApplicationStatusId = 1 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email

                        If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
                            Dim objLeaveForm As New clsleaveform
                            objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                            objLeaveForm._EmployeeCode = objEmployee._Employeecode
                            objLeaveForm._EmployeeFirstName = objEmployee._Firstname
                            objLeaveForm._EmployeeMiddleName = objEmployee._Othername
                            objLeaveForm._EmployeeSurName = objEmployee._Surname
                            objLeaveForm._EmpMail = objEmployee._Email

                            If objLeaveForm._Statusunkid = 7 Then 'only Issued
                                Dim objCommonCode As New CommonCodes
                                Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
                                Dim strPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid _
                                                                                                                            , CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date _
                                                                                                                            , CDate(Session("fin_enddate")).Date, True, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                                objCommonCode = Nothing

                                Dim objLeaveType As New clsleavetype_master
                                objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid

                                objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate _
                                                                                   , objLeaveForm._Returndate, objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "" _
                                                                                   , Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "" _
                                                                                   , CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCrmasterunkid", False, True)).Text), objLeaveForm._Formno)
                                objLeaveType = Nothing
                                objLeaveForm = Nothing

                            End If
                        Else

                            objClaimMst.SendMailToEmployee(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), _
                                                                               "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "", True)
                        End If
                        objEmployee = Nothing

                    ElseIf objClaimMst._Statusunkid = 3 AndAlso mintApproverApplicationStatusId = 3 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email

                        objClaimMst.SendMailToEmployee(CInt(dgrow.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", _
                                                                           enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRemarks.Text.Trim, True)

                        objEmployee = Nothing
                    End If

                Next

                If dtApproverTable IsNot Nothing Then dtApproverTable.Clear()
                dtApproverTable = Nothing
                If dtFilter IsNot Nothing Then dtFilter.Clear()
                dtFilter = Nothing
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing

            Next

            If objExpApproverTran._ClaimEmailList IsNot Nothing AndAlso objExpApproverTran._ClaimEmailList.Count > 0 Then
                objClaimEmailList.AddRange(objExpApproverTran._ClaimEmailList)
            End If

            If objClaimMst._ClaimEmailList IsNot Nothing AndAlso objClaimMst._ClaimEmailList.Count > 0 Then
                objClaimEmailList.AddRange(objClaimMst._ClaimEmailList)
            End If

            If objClaimEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = CInt(Session("CompanyUnkId"))
                    objThread.Start(arr)
                Else
                    Call Send_Notification(CInt(Session("CompanyUnkId")))
                End If
            End If

            objLeaveApprover = Nothing
            objExpApproverTran = Nothing
            objExpAppr = Nothing
            objClaimMst = Nothing
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

            If mblnWarning = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Some of the Claim Expense Form(s) cannot be Approved. Reason : It is exceeded the current occurrence limit and will be highlighted in red."), Me)
                Exit Sub
            Else
                Call FillGrid()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAdvanceFilter.Hide()
        End Try
    End Sub

    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            If objClaimEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objClaimEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If
                    objSendMail._Form_Name = mstrModuleName
                    objSendMail._WebClientIP = Session("IP_ADD").ToString()
                    objSendMail._WebHostName = Session("HOST_NAME").ToString()
                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = intCompanyID(0)
                    End If
                    objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), objEmail._ExportReportPath)
                Next
                If objClaimEmailList.Count > 0 Then objClaimEmailList.Clear()
            End If

        Catch ex As Exception
           DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetFormControls()
        Try
            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                If CInt(IIf(cboExpenseCategory.SelectedValue = "", 0, cboExpenseCategory.SelectedValue)) > 0 Then
                    Select Case CInt(IIf(cboExpenseCategory.SelectedValue = "", 0, cboExpenseCategory.SelectedValue))
                        Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = True

                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhClaimExpense", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhSectorRoute", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUOM", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBalance", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhCostCenter", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAppliedQty", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhCurrency", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhClaimRemark", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhExpenseRemark", False, True)).Visible = False
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Visible = False

                        Case Else
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhClaimExpense", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhSectorRoute", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUOM", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBalance", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhCostCenter", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAppliedQty", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhCurrency", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhClaimRemark", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhExpenseRemark", False, True)).Visible = True
                            'dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Visible = True
                    End Select
                Else
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                End If
            Else
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByVal mdecUnitPrice As Decimal, ByVal mdecQuantity As Decimal) As Decimal
        Dim mdecBaseAmount As Decimal = 0
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecBaseAmount = (mdecUnitPrice * mdecQuantity) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            objExchange = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCurrencyRate", mstrModuleName)
        End Try
        Return mdecBaseAmount
    End Function

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Dim dsCombo As DataSet = Nothing
        Try
            Call SetFormControls()
            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", 0, False)
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objExpMaster = Nothing


            If CBool(Session("PaymentApprovalwithLeaveApproval")) And CInt(cboExpenseCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                Dim objLeaveApprover As New clsleaveapprover_master
                dsCombo = objLeaveApprover.GetEmployeeFromUser(Session("Database_Name").ToString(), _
                                                                                             CInt(Session("Fin_year")), _
                                                                                             CInt(Session("CompanyUnkId")), _
                                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                             True, CInt(Session("UserId")), CBool(Session("AllowtoApproveLeave")), CBool(Session("AllowIssueLeave")))
                Dim lstIDs As List(Of String) = (From p In dsCombo.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

                objLeaveApprover = Nothing
            Else

                Dim objEmployee As New clsEmployee_Master
                dsCombo = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                         CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                         Session("UserAccessModeSetting").ToString(), _
                                         True, False, "List", True)

                objEmployee = Nothing
            End If

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            Dim objExpApproverMst As New clsExpenseApprover_Master
            dsCombo = objExpApproverMst.GetApproverWithFromUserLogin(CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(Session("UserId")), CInt(cboExpenseCategory.SelectedValue))
            objExpApproverMst = Nothing
            If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsCombo.Tables(0).NewRow
                drRow("approverunkid") = 0
                drRow("Approver") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Select")
                dsCombo.Tables(0).Rows.Add(drRow)
                drRow = Nothing
            End If

            With cboApprover
                .DataValueField = "approverunkid"
                .DataTextField = "Approver"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            cboApprover_SelectedIndexChanged(New Object, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
        End Try
    End Sub

    Protected Sub cboApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.SelectedIndexChanged
        Try
            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                Dim objLeaveapprover As New clsleaveapprover_master
                objLeaveapprover._Approverunkid = CInt(cboApprover.SelectedValue)
                mblnIsExternalApprover = objLeaveapprover._Isexternalapprover

                Dim objApproverLevel As New clsapproverlevel_master
                objApproverLevel._Levelunkid = objLeaveapprover._Levelunkid
                mintPriority = objApproverLevel._Priority

                objApproverLevel = Nothing
                objLeaveapprover = Nothing

            Else
                Dim objExApproverMaster As New clsExpenseApprover_Master
                objExApproverMaster._crApproverunkid = CInt(cboApprover.SelectedValue)
                mblnIsExternalApprover = objExApproverMaster._Isexternalapprover

                Dim objExapproverLevel As New clsExApprovalLevel
                objExapproverLevel._Crlevelunkid = objExApproverMaster._crLevelunkid
                mintPriority = objExapproverLevel._Crpriority
                objExApproverMaster = Nothing
                objExapproverLevel = Nothing
            End If
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Links Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            Me.ViewState("EmpAdvanceSearch") = Nothing
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("GlobalApproval") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Dim strAdvanceSearch As String
        Try
            strAdvanceSearch = popupAdvanceFilter._GetFilterString
            Me.ViewState.Add("EmpAdvanceSearch", strAdvanceSearch)
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), Me)
                cboApprover.Focus()
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text) = True)


            If gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select atleast one Claim Expense Form to Approve/Reject."), Me)
                Exit Sub
            End If

            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

            If (CInt(cboStatus.SelectedValue) = 3 OrElse CInt(cboStatus.SelectedValue) = 8) AndAlso txtRemarks.Text.Trim.Length <= 0 Then  'Rejected
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Remark is compulsory information.Please Enter Remark."), Me)
                txtRemarks.Focus()
                Exit Sub
            End If

            mblnIsExpenseSaveClick = True

            If dgvData.Items.Count > 0 Then
                Dim drUnit As IEnumerable(Of DataGridItem) = Nothing
                drUnit = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And _
                                                                 CBool(x.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text) = False And _
                                                                 CInt(IIf(CType(x.FindControl("txtdgcolhUnitPrice"), TextBox).Text = "", 0, CType(x.FindControl("txtdgcolhUnitPrice"), TextBox).Text)) <= 0)
                If drUnit.Count > 0 Then
                    popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Some of the expense(s) had 0 unit price for checked employee(s).") & vbCrLf & _
                                                                        Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Do you wish to continue?")
                    popup_UnitPriceYesNo.Show()
                    If drUnit IsNot Nothing Then drUnit.ToList().Clear()
                    drUnit = Nothing
                    Exit Sub
                Else
                    GlobalSave()
                End If
                If drUnit IsNot Nothing Then drUnit.ToList().Clear()
                drUnit = Nothing
            Else
                GlobalSave()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAdvanceFilter.Hide()
        End Try
    End Sub

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If mblnIsExpenseSaveClick Then
                GlobalSave()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGridView Event"

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try

            If e.Item.ItemIndex >= 0 Then

                If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.SelectedItem Then
                    
                    If e.Item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text.Trim.Length > 0 AndAlso _
                        CBool(e.Item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text.Trim) = False AndAlso _
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhIsGrp", False, True)).Text.Trim <> "&nbsp;" Then  'Is Grp                                                
                        
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).Text
                        Dim chk As CheckBox = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhCheck", False, True)).FindControl("chkSelect"), CheckBox)

                        Select Case CInt(e.Item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhexpensetypeid", False, True)).Text)
                            Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                                Dim txtQ As TextBox = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).FindControl("txtdgcolhQuantity"), TextBox)  'Quantity
                                txtQ.Enabled = False
                                txtQ = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).FindControl("txtdgcolhUnitPrice"), TextBox)   'Unit Price
                                txtQ.Enabled = False
                        End Select

                        chk.Enabled = False
                    Else
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).ColumnSpan = e.Item.Cells.Count - 1
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).Style.Add("text-align", "left")
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhClaimNo", False, True)).CssClass = "group-header"

                        For i = 2 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).ColumnSpan = e.Item.Cells.Count - 1
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Style.Add("text-align", "left")
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).CssClass = "group-header"

                        For i = getColumnId_Datagrid(dgvData, "dgcolhCurrency", False, True) To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                    End If

             
                    If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBalance", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBalance", False, True)).Text.Trim <> "&nbsp;" Then  'Balance
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBalance", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBalance", False, True)).Text), "#0.00")
                    End If


                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = True Then
                        If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text = CDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text).ToShortDateString()
                        End If
                    End If

                    If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedQty", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedQty", False, True)).Text.Trim <> "&nbsp;" Then  'Applied quantity
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedQty", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedQty", False, True)).Text), Session("fmtCurrency").ToString())
                    End If

                    If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Text.Trim <> "&nbsp;" Then  'Applied Amount" Then
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmount", False, True)).Text), Session("fmtCurrency").ToString())
                    End If


                    Dim Txtbox As TextBox = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).FindControl("txtdgcolhQuantity"), TextBox)  'Quantity
                    If Txtbox.Text.Trim <> "" Then
                        Txtbox.Text = Format(CDec(Txtbox.Text), Session("fmtCurrency").ToString)
                    End If
                    Txtbox = Nothing

                    Txtbox = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).FindControl("txtdgcolhUnitPrice"), TextBox)   'Unit Price
                    If Txtbox.Text.Trim <> "" Then
                        Txtbox.Text = Format(CDec(Txtbox.Text), Session("fmtCurrency").ToString)
                    End If

                    If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
                        Txtbox.Enabled = False
                    Else
                        Dim objExpense As New clsExpense_Master
                        objExpense._Expenseunkid = CInt(e.Item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhExpenseID", False, True)).Text)
                        Txtbox.Enabled = objExpense._IsUnitPriceEditable
                        objExpense = Nothing
                    End If

                    If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
                        Dim txtQ As TextBox = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).FindControl("txtdgcolhQuantity"), TextBox)
                        txtQ.Enabled = False
                    End If

                    If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text.Trim <> "&nbsp;" Then  'Amount
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())
                    End If

                    If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text.Trim <> "&nbsp;" Then  'Base Amount
                        e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text), Session("fmtCurrency").ToString())
                    End If

                    ' For Cost Center
                    Dim dgcolhCostCenter As DropDownList = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhCostCenter", False, True)).FindControl("dgcolhCostCenter"), DropDownList)  'Cost Center
                    Dim objCostCenter As New clscostcenter_master
                    Dim dtTable As DataTable = Nothing
                    Dim dsCombo As DataSet = objCostCenter.getComboList("List", False)
                    objCostCenter = Nothing

                    With dgcolhCostCenter
                        .DataValueField = "costcenterunkid"
                        .DataTextField = "costcentername"
                        If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            dtTable = dsCombo.Tables(0)
                        Else
                            Dim dRow As DataRow = dsCombo.Tables(0).NewRow
                            dRow("costcenterunkid") = "0"
                            dRow("costcentername") = ""
                            dsCombo.Tables(0).Rows.InsertAt(dRow, 0)
                            dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                        End If
                        .DataSource = dtTable
                        .DataBind()
                        .SelectedValue = CType(e.Item.FindControl("hdcolhCostCenter"), HiddenField).Value
                    End With
                    dgcolhCostCenter.Enabled = False


                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing


                    'Currency
                    Dim dgcolhCurrency As DropDownList = CType(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhCurrency", False, True)).FindControl("dgcolhCurrency"), DropDownList)  'Currency
                    Dim objExchange As New clsExchangeRate
                    dsCombo = objExchange.getComboList("List", True, False)
                    With dgcolhCurrency
                        .DataValueField = "countryunkid"
                        .DataTextField = "currency_sign"
                        .DataSource = dsCombo.Tables(0)
                        .DataBind()
                        .SelectedValue = CType(e.Item.FindControl("hdcolhCurrency"), HiddenField).Value
                    End With
                    dgcolhCurrency.Enabled = False


                    If dsCombo IsNot Nothing Then dsCombo.Clear()
                    dsCombo = Nothing
                    objExchange = Nothing

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Protected Sub txtdgcolhUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtUnit As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtUnit.NamingContainer, DataGridItem)

            If txtUnit.Text.Trim = "" Then txtUnit.Text = "0"

            txtUnit.Text = Format(CDec(txtUnit.Text), Session("fmtCurrency").ToString())
            
            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(CType(item.Cells(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).FindControl("txtdgcolhQuantity"), TextBox).Text) * CDec(txtUnit.Text), Session("fmtCurrency").ToString())
            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())
            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text = Format(GetCurrencyRate(CInt(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhcountryunkid", False, True)).Text), CDate(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhClaimDate", False, True)).Text).Date, CDec(txtUnit.Text), CDec(CType(item.Cells(getColumnId_Datagrid(dgvData, "dgcolhQuantity", False, True)).FindControl("txtdgcolhQuantity"), TextBox).Text)), Session("fmtCurrency").ToString())

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtdgcolhQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            
            Dim txtQty As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtQty.NamingContainer, DataGridItem)
            If txtQty.Text.Trim = "" OrElse txtQty.Text = "0" Then
                txtQty.Text = CStr(1)
            End If

            Dim objExpense As New clsExpense_Master

            objExpense._Expenseunkid = CInt(item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhExpenseID", False, True)).Text)

            If objExpense._IsConsiderDependants Then

                Dim mintEmpDepedentsCoutnForCR As Integer = 0
                Dim objDependents As New clsDependants_Beneficiary_tran
                Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(item.Cells(getColumnId_Datagrid(dgvData, "objdgcolhEmployeeunkid", False, True)).Text))
                mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1
                If dtDependents IsNot Nothing Then dtDependents.Clear()
                dtDependents = Nothing
                objDependents = Nothing

                If mintEmpDepedentsCoutnForCR < CDec(txtQty.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit for this selected employee."), Me)
                    txtQty.Focus()
                    objExpense = Nothing
                    Exit Sub
                End If
            End If


            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmExpenseApproval", 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                objExpense = Nothing
                Exit Sub
            End If


            objExpense = Nothing

            txtQty.Text = Format(CDec(txtQty.Text), Session("fmtCurrency").ToString())

            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(txtQty.Text) * CDec(CType(item.Cells(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).FindControl("txtdgcolhUnitPrice"), TextBox).Text), Session("fmtCurrency").ToString())
            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())
            dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "dgcolhBaseAmount", False, True)).Text = Format(GetCurrencyRate(CInt(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhcountryunkid", False, True)).Text), CDate(dgvData.Items(item.ItemIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhClaimDate", False, True)).Text).Date, CDec(CType(item.Cells(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).FindControl("txtdgcolhUnitPrice"), TextBox).Text), CDec(txtQty.Text)), Session("fmtCurrency").ToString())

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"
    'Gajanan [20-July-2020] -- Start
    'Enhancement: Optimize For NMB

    'Protected Sub chkSelect_Checkchanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)


    '        'Pinkal (04-Feb-2019) -- Start
    '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '        'Dim dRow() As DataRow = dtList.Select("crmasterunkid =" & CInt(item.Cells(19).Text))
    '        Dim dRow() As DataRow = dtList.Select("crmasterunkid =" & CInt(item.Cells(21).Text)) 'crmasterunkid
    '        'Pinkal (04-Feb-2019) -- End

    '        If dRow.Length > 0 Then
    '            For Each row In dRow
    '                row.Item("IsChecked") = chkSelect.Checked
    '                CType(dgvData.Items(dtList.Rows.IndexOf(row)).Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkSelect.Checked
    '            Next
    '            dtList.AcceptChanges()
    '        End If
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelect_Checkchanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkSelectAll_Checkchanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgvData.Items.Count <= 0 Then Exit Sub
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelectAll.NamingContainer, DataGridItem)

    '        Dim Query = From Row In dtList.AsEnumerable() Select Row
    '        For Each rows In Query
    '            rows.Item("ischecked") = chkSelectAll.Checked
    '            CType(dgvData.Items(dtList.Rows.IndexOf(rows)).Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '        Next
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JsStatus", "setscrollPosition('scrollable-container');", True)
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelectAll_Checkchanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    'Gajanan [20-July-2020] -- End

#End Region

    Private Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.gbFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbFilter.ID, Me.gbFilter.Text)
            Me.gbInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbInfo.ID, Me.gbInfo.Text)
            Me.gbPending.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbPending.ID, Me.gbPending.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lnkAllocation.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.ToolTip)
            Me.lblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblSector.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSector.ID, Me.lblSector.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            dgvData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(1).FooterText, dgvData.Columns(1).HeaderText)
            dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)
            dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(7).FooterText, dgvData.Columns(7).HeaderText)
            dgvData.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(8).FooterText, dgvData.Columns(8).HeaderText)
            dgvData.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(9).FooterText, dgvData.Columns(9).HeaderText)
            dgvData.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(10).FooterText, dgvData.Columns(10).HeaderText)
            dgvData.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(11).FooterText, dgvData.Columns(11).HeaderText)
            dgvData.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(12).FooterText, dgvData.Columns(12).HeaderText)
            dgvData.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvData.Columns(13).FooterText, dgvData.Columns(13).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

End Class
