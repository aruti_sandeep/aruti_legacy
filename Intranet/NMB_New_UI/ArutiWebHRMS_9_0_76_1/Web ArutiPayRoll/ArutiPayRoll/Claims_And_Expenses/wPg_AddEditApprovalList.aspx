﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AddEditApprovalList.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_AddEditApprovalList" Title="Add/Edit Expense Approval" %>

<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        
        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAll]", grid);
            debugger;
            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $("body").on("click", "[id*=ChkAsgAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkAsgSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkAsgSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAsgAll]", grid);
            debugger;
            if ($("[id*=ChkAsgSelect]", grid).length == $("[id*=ChkAsgSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

              
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtSearchEmp').val().length > 0) {
                $('#<%= dgvAEmployee.ClientID %> tbody tr').hide();
                $('#<%= dgvAEmployee.ClientID %> tbody tr:first').show();
                $('#<%= dgvAEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmp').val() + '\')').parent().show();
            }
            else if ($('#txtSearchEmp').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvAEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearchEmp').val('');
            $('#<%= dgvAEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchEmp').focus();
        }


        function ToSearching() {
            if ($('#txtSearchTo').val().length > 0) {
                $('#<%= dgvAssessor.ClientID %> tbody tr').hide();
                $('#<%= dgvAssessor.ClientID %> tbody tr:first').show();
                $('#<%= dgvAssessor.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchTo').val() + '\')').parent().show();
            }
            else if ($('#txtSearchTo').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= dgvAssessor.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtSearchTo').val('');
            $('#<%= dgvAssessor.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchTo').focus();
        }
        
        
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Expense Approval"></asp:Label>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <%-- <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>--%>
                                                    <asp:Label ID="lblInfo" runat="server" Text="Approver Information"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkExternalApprover" runat="server" Text="Make External Approver"
                                                            AutoPostBack="true"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblExpenseCat" runat="server" Text="Expense Cat." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExCategory" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproverName" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproveLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApproveLevel" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUser" runat="server" Text="User" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboUser" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 p-l-0">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <input type="text" id="txtSearchEmp" name="txtSearchEmp" placeholder="Type To Search Text"
                                                                        maxlength="50" class="form-control" onkeyup="FromSearching();" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15 p-l-0">
                                                            <ul class="header-dropdown">
                                                                <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations">
                                                                     <i class="fas fa-sliders-h"></i>
                                                                </asp:LinkButton>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15  p-l-0">
                                                            <ul class="header-dropdown">
                                                                <asp:LinkButton ID="lnkReset" runat="server" ToolTip="Reset" CssClass="lnkhover">
                                                                      <i class="fas fa-sync-alt"></i>
                                                                </asp:LinkButton>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 350px">
                                                            <asp:GridView ID="dgvAEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAll" runat="server" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" CssClass="filled-in" Text=" " />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="employeecode" HeaderText="Employee Code" ReadOnly="true"
                                                                        FooterText="dgcolhEcode" />
                                                                    <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhEName" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAssignedEmployee" runat="server" Text="Assigned Employee"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" id="txtSearchTo" name="txtSearchTo" placeholder="Type To Search Text"
                                                                    maxlength="50" class="form-control" onkeyup="ToSearching();" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 685px">
                                                            <asp:GridView ID="dgvAssessor" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                DataKeyNames="crapproverunkid,crapprovertranunkid,employeeunkid" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAsgAll" runat="server" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkAsgSelect" runat="server" CssClass="filled-in" Text=" " />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ecode" HeaderText="Employee Code" ReadOnly="true" FooterText="dgcolhaCode" />
                                                                    <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhaEmp" />
                                                                    <asp:BoundField DataField="edept" HeaderText="Department" ReadOnly="true" FooterText="dgcolhaDepartment" />
                                                                    <asp:BoundField DataField="eJob" HeaderText="Job" ReadOnly="true" FooterText="dgcolhaJob" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="crapproverunkid" HeaderText="AMasterId" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="crapprovertranunkid" HeaderText="ATranId" ReadOnly="true"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnDeleteA" runat="server" Text="Delete" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc8:ConfirmYesNo ID="popYesNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
