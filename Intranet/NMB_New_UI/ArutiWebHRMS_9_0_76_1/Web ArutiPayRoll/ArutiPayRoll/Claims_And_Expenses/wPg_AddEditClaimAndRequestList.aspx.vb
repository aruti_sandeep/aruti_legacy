﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO
Imports System.Net
Imports System.Threading

#End Region

Partial Class Claims_And_Expenses_wPg_AddEditClaimAndRequestList
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimMaster As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Pinkal (05-Sep-2020) -- End

    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private Shared ReadOnly mstrModuleName As String = "frmClaims_RequestAddEdit"
    Private ReadOnly mstrModuleName1 As String = "frmDependentsList"
    Private mintDeleteIndex As Integer = -1
    Private mdtAttchment As DataTable = Nothing
    Private blnShowAttchmentPopup As Boolean = False
    Private mdtFinalAttchment As DataTable = Nothing

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsEditClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private mblnIsClaimFormBudgetMandatory As Boolean = False
    'Pinkal (20-Nov-2018) -- End


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestMasterId As Integer = 0
    Dim mintClaimRequestExpenseTranId As Integer = 0
    Dim mstrClaimRequestExpenseGUID As String = ""
    'Pinkal (07-Mar-2019) -- End

    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Dim mstrP2PToken As String = ""
    'Pinkal (29-Aug-2019) -- End



    'Pinkal (18-Mar-2021) -- Start
    'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
    Private mintExpenseIndex As Integer = -1
    'Pinkal (18-Mar-2021) -- End


    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    'Pinkal (25-Jan-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    'Dim objClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (25-Jan-2022) -- End
    'Pinkal (20-May-2022) -- End

    'S.SANDEEP |03-SEP-2022| -- START
    'ISSUE/ENHANCEMENT : Sprint_2022-13
    Private mintSelectedPassenger As Integer = 0
    'S.SANDEEP |03-SEP-2022| -- END

    'Pinkal (24-Jun-2024) -- Start
    'NMB Enhancement : P2P & Expense Category Enhancements.
    Private mstrBaseCurrencysign As String = ""
    'Pinkal (24-Jun-2024) -- End

    Private mdtTranDetail As DataTable = Nothing

    Private mblnIsLeaveEncashment As Boolean = False
    Private mblnIsSecRouteMandatory As Boolean = False
    Private mblnIsConsiderDependants As Boolean = False
    Private mblnDoNotAllowToApplyForBackDate As Boolean = False
    Private mintGLCodeId As Integer = 0
    Private mstrDescription As String = ""
    Private mintExpenditureTypeId As Integer = 0
    Private mblnIsaccrue As Boolean = False
    Private mintUomunkid As Integer = 0
    Private mintAccrueSettingId As Integer = 0
    Private mintExpense_MaxQuantity As Integer = 0
    Private mblnIsAttachDocMandetory As Boolean = False
    Private mblnIsUnitPriceEditable As Boolean = True

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If IsPostBack = False AndAlso Request.QueryString("uploadimage") Is Nothing Then

                'SetLanguage()

                Session.Remove("mdtFinalAttchment")
                Session.Remove("mdtAttchment")
                Me.ViewState.Add("mintExpenseCategoryID", 0)
                Me.ViewState.Add("ClaimRequestMasterId", 0)
                Me.ViewState.Add("mintEmployeeID", 0)
                Me.ViewState.Add("mintLeaveTypeId", 0)
                Me.ViewState.Add("MaxDate", CDate(eZeeDate.convertDate("99981231")))
                Me.ViewState.Add("MinDate", CDate(eZeeDate.convertDate("17530101")))
                Me.ViewState.Add("PDate", ConfigParameter._Object._CurrentDateAndTime.Date)
                Me.ViewState.Add("mintLeaveFormID", -1)
                Me.ViewState.Add("mblnIsLeaveEncashment", False)
                Me.ViewState.Add("EditGuid", "Null")
                Me.ViewState.Add("EditCrtranunkid", 0)
                txtUnitPrice.Attributes.Add("onfocusin", "select();")
                SetVisibility()

                FillCombo()
                Dim objClaimMaster As New clsclaim_request_master
                Dim objClaimTran As New clsclaim_request_tran

                If Session("mintClaimRequestMasterId") IsNot Nothing Then
                    mintClaimRequestMasterId = CInt(Session("mintClaimRequestMasterId"))
                    objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
                    Session("mintClaimRequestMasterId") = Nothing

                    If objClaimMaster._Crmasterunkid > 0 Then
                        Enable_Disable_Ctrls(False)
                    End If
                    If objClaimMaster._Referenceunkid > 0 Then
                        Me.ViewState("mintLeaveFormID") = objClaimMaster._Referenceunkid
                    End If
                End If

                GetValue(objClaimMaster)
                objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId
                'mdtTran = objClaimTran._DataTable
                'mdtTranDetail = mdtTran.Copy()
                mdtTranDetail = objClaimTran._DataTable.Copy()

                Fill_Expense()

                objClaimTran = Nothing
                objClaimMaster = Nothing

                If mintClaimRequestMasterId > 0 Then
                    If Session("ClaimRequest_EmpUnkId") IsNot Nothing Then
                        cboEmployee.SelectedValue = Session("ClaimRequest_EmpUnkId").ToString()
                    End If
                End If
                cboExpCategory.Focus()

                If mdtFinalAttchment Is Nothing Then
                    Dim objAttchement As New clsScan_Attach_Documents
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "ISNULL(hrdocuments_tran.scanattachrefid,-1) =  " & enScanAttactRefId.CLAIM_REQUEST, CInt(cboEmployee.SelectedValue) _
                                                                , False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                    End If
                    Dim strTranIds As String = String.Join(",", mdtTranDetail.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                    If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                    mdtFinalAttchment = New DataView(objAttchement._Datatable, "transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    objAttchement = Nothing
                Else
                    mdtAttchment = mdtFinalAttchment.Copy
                End If

            Else
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                blnShowAttchmentPopup = CBool(Me.ViewState("blnShowAttchmentPopup"))
                mdtFinalAttchment = CType(Me.Session("mdtFinalAttchment"), DataTable)
                mblnIsEditClick = CBool(Me.ViewState("mblnIsEditClick"))
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
                mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsClaimFormBudgetMandatory"))
                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mintClaimRequestMasterId = CInt(Me.ViewState("ClaimRequestMasterId"))
                mintClaimRequestExpenseTranId = CInt(Me.ViewState("ClaimRequestExpenseTranId"))
                mstrClaimRequestExpenseGUID = CStr(Me.ViewState("ClaimRequestExpenseGUID"))
                If Me.ViewState("P2PToken") IsNot Nothing Then
                    mstrP2PToken = Me.ViewState("P2PToken").ToString()
                Else
                    mstrP2PToken = ""
                End If

                mintExpenseIndex = CInt(Me.ViewState("mintExpenseIndex"))
                mintSelectedPassenger = CInt(Me.ViewState("mintSelectedPassenger"))
                mstrBaseCurrencysign = Me.ViewState("currency_sign")
                mdtTranDetail = CType(Session("mdtTranDetail"), DataTable)

                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
                mblnIsLeaveEncashment = CBool(Me.ViewState("mblnIsLeaveEncashment"))
                mblnIsSecRouteMandatory = CBool(Me.ViewState("IsSecRouteMandatory"))
                mblnIsConsiderDependants = CBool(Me.ViewState("IsConsiderDependants"))
                mblnDoNotAllowToApplyForBackDate = CBool(Me.ViewState("DoNotAllowToApplyForBackDate"))
                mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsBudgetMandatory"))
                mintGLCodeId = Me.ViewState("GLCodeId")
                mstrDescription = Me.ViewState("Description")
                mintExpenditureTypeId = Me.ViewState("ExpenditureTypeId")
                mblnIsaccrue = CBool(Me.ViewState("Isaccrue"))
                mintUomunkid = Me.ViewState("Uomunkid")
                mintAccrueSettingId = Me.ViewState("AccrueSetting")
                mintExpense_MaxQuantity = Me.ViewState("Expense_MaxQuantity")
                mblnIsAttachDocMandetory = Me.ViewState("IsAttachDocMandetory")
                mblnIsUnitPriceEditable = Me.ViewState("IsUnitPriceEditable")
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'Dim objGroup As New clsGroup_Master
            'objGroup._Groupunkid = 1
            'If objGroup._Groupname.ToUpper = "NMB PLC" Then
            '    txtClaimRemark.Attributes.Add("maxlength", "80")
            'End If
            'objGroup = Nothing
            'Pinkal (24-Jun-2024) -- End

            If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
                txtClaimRemark.Attributes.Add("maxlength", "80")
            End If

            If blnShowAttchmentPopup Then
                popup_ScanAttchment.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            Session.Remove("mintClaimRequestMasterId")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            Me.Session("mdtAttchment") = mdtAttchment
            Me.ViewState("blnShowAttchmentPopup") = blnShowAttchmentPopup
            Me.Session("mdtFinalAttchment") = mdtFinalAttchment
            Me.ViewState("mblnIsEditClick") = mblnIsEditClick
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
            Me.ViewState("IsClaimFormBudgetMandatory") = mblnIsClaimFormBudgetMandatory
            Me.ViewState("IsHRExpense") = mblnIsHRExpense
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("ClaimRequestMasterId") = mintClaimRequestMasterId
            Me.ViewState("ClaimRequestExpenseTranId") = mintClaimRequestExpenseTranId
            Me.ViewState("ClaimRequestExpenseGUID") = mstrClaimRequestExpenseGUID
            Me.ViewState("P2PToken") = mstrP2PToken
            Me.ViewState("mintExpenseIndex") = mintExpenseIndex
            Me.ViewState("mintSelectedPassenger") = mintSelectedPassenger
            Me.ViewState("currency_sign") = mstrBaseCurrencysign
            Me.Session("mdtTranDetail") = mdtTranDetail

            Me.ViewState("IsHRExpense") = mblnIsHRExpense
            Me.ViewState("mblnIsLeaveEncashment") = mblnIsLeaveEncashment
            Me.ViewState("IsSecRouteMandatory") = mblnIsSecRouteMandatory
            Me.ViewState("IsConsiderDependants") = mblnIsConsiderDependants
            Me.ViewState("DoNotAllowToApplyForBackDate") = mblnDoNotAllowToApplyForBackDate
            Me.ViewState("IsBudgetMandatory") = mblnIsClaimFormBudgetMandatory
            Me.ViewState("GLCodeId") = mintGLCodeId
            Me.ViewState("Description") = mstrDescription
            Me.ViewState("ExpenditureTypeId") = mintExpenditureTypeId
            Me.ViewState("Isaccrue") = mblnIsaccrue
            Me.ViewState("Uomunkid") = mintUomunkid
            Me.ViewState("AccrueSetting") = mintAccrueSettingId
            Me.ViewState("Expense_MaxQuantity") = mintExpense_MaxQuantity
            Me.ViewState("IsAttachDocMandetory") = mblnIsAttachDocMandetory
            Me.ViewState("IsUnitPriceEditable") = mblnIsUnitPriceEditable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEMaster As New clsEmployee_Master
        Try
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CInt(Me.ViewState("mintEmployeeID")) > 0 Then
                    blnSelect = False
                    intEmpId = CInt(Me.ViewState("mintEmployeeID"))
                End If
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = Me.ViewState("mintEmployeeID").ToString()
            End With
            objEMaster = Nothing

            Dim objExpenseCategory As New clsexpense_category_master
            Select Case CInt(Me.ViewState("mintExpenseCategoryID"))
                Case enExpenseType.EXP_LEAVE
                    dsCombo = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), False, False, False, "List", False, False)
                Case Else
                    dsCombo = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True)
            End Select
            objExpenseCategory = Nothing

            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = Me.ViewState("mintExpenseCategoryID").ToString()
            End With

            'Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())

            Dim objcommonMst As New clsCommon_Master
            'If CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
            '    dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            '    With cboSectorRoute
            '        .DataValueField = "masterunkid"
            '        .DataTextField = "name"
            '        .DataSource = dsCombo.Tables(0)
            '        .DataBind()
            '    End With

            '    If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
            '        With cboTravelFrom
            '            .DataValueField = "masterunkid"
            '            .DataTextField = "name"
            '            .DataSource = dsCombo.Tables(0).Copy
            '            .DataBind()
            '            .SelectedIndex = 0
            '        End With
            '        With cboTravelTo
            '            .DataValueField = "masterunkid"
            '            .DataTextField = "name"
            '            .DataSource = dsCombo.Tables(0).Copy
            '            .DataBind()
            '            .SelectedIndex = 0
            '        End With
            '    End If
            'End If

            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With
            objcommonMst = Nothing

            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    'S.SANDEEP |27-MAY-2022| -- START
                    'ISSUE/ENHANCEMENT : AC2-391
                    'dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                    If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                        dtTable = dsCombo.Tables(0)
                    Else
                        dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                    End If
                    'S.SANDEEP |27-MAY-2022| -- END

                End If
                .DataSource = dtTable
                .DataBind()
            End With
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objCostCenter = Nothing
            'Pinkal (05-Sep-2020) -- End



            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                    'Pinkal (24-Jun-2024) -- Start
                    'NMB Enhancement : P2P & Expense Category Enhancements.
                    mstrBaseCurrencysign = drRow(0)("currency_sign").ToString()
                    'Pinkal (24-Jun-2024) -- End
                End If
                drRow = Nothing
            End With
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (05-Sep-2020) -- End


            dsCombo = clsExpCommonMethods.GetRebatePercentage(False, "List")
            With cboRebatePercent
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 1
                .DataBind()
            End With

            dsCombo = clsExpCommonMethods.GetRebateSeatStatus(True, "List")
            With cboSeatStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            End If


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If mblnPaymentApprovalwithLeaveApproval AndAlso CInt(Me.ViewState("ClaimRequestMasterId")) > 0 Then
            If mblnPaymentApprovalwithLeaveApproval AndAlso mintClaimRequestMasterId > 0 Then
                'Pinkal (07-Mar-2019) -- End
                cboReference.Enabled = False
                cboEmployee.Enabled = False
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub GetValue()
    Private Sub GetValue(ByVal objClaimMaster As clsclaim_request_master)
        'Pinkal (05-Sep-2020) -- End
        Try
            txtClaimNo.Text = objClaimMaster._Claimrequestno

            If CInt(Me.ViewState("mintEmployeeID")) > 0 Then
                cboEmployee.SelectedValue = Me.ViewState("mintEmployeeID").ToString()
            Else
                cboEmployee.SelectedValue = objClaimMaster._Employeeunkid.ToString()
            End If

            If CInt(Me.ViewState("mintExpenseCategoryID")) > 0 Then
                cboExpCategory.SelectedValue = Me.ViewState("mintExpenseCategoryID").ToString()
            Else
                cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid.ToString()
            End If
            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())

            If objClaimMaster._Transactiondate <> Nothing Then
                dtpDate.SetDate = objClaimMaster._Transactiondate
            Else
                dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            End If

            Call dtpDate_TextChanged(dtpDate, New EventArgs)

            txtClaimRemark.Text = objClaimMaster._Claim_Remark

            If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                cboLeaveType.SelectedValue = Me.ViewState("mintLeaveTypeId").ToString()
            Else
                cboLeaveType.SelectedValue = objClaimMaster._LeaveTypeId.ToString()
            End If

            If cboLeaveType.SelectedValue IsNot Nothing AndAlso cboLeaveType.SelectedValue <> "" Then
                Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs()) '2Time
            End If

            cboReference.SelectedValue = objClaimMaster._Referenceunkid.ToString()

            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            cboRebatePercent.SelectedValue = objClaimMaster._RebateId
            cboSeatStatus.SelectedValue = objClaimMaster._SeatTypeId
            'S.SANDEEP |10-MAR-2022| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try
            'Dim mdtTran As DataTable = Nothing

            If mdtTranDetail Is Nothing Then Exit Sub
            'If mdtTranDetail IsNot Nothing Then
            '    mdtTran = mdtTranDetail.Copy()
            'End If


            Dim mdView As New DataView
            mdView = mdtTranDetail.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            If mdView.ToTable.Rows.Count > dgvData.PageSize Then
                dgvData.AllowPaging = True
            Else
                dgvData.AllowPaging = False
            End If
            dgvData.DataSource = mdView
            dgvData.DataBind()

            If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhcurrency", False, True)).Visible = True
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Visible = True
            Else
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhcurrency", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Visible = False
            End If

            If mdtTranDetail.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTranDetail.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                        txtGrandTotal.Text = Format(CDec(mdtTranDetail.Compute("SUM(base_amount)", "AUD<>'D'")), Session("fmtCurrency").ToString())
                    Else
                        txtGrandTotal.Text = Format(CDec(mdtTranDetail.Compute("SUM(amount)", "AUD<>'D'")), Session("fmtCurrency").ToString())
                    End If
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If
            If dgvData.Items.Count <= 0 Then
                dtpDate.Enabled = True
            End If

            If dgvData.Items.Count > 0 Then
                If mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbeneficetranunkid") <> 0).Count > 0 Then
                    txtOthers.Enabled = False
                Else
                    txtOthers.Enabled = True
                End If

                If mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("other_person") <> "").Count > 0 Then
                    cboDependant.SelectedValue = 0
                    cboDependant.Enabled = False
                Else
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.SelectedValue = 0
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.Enabled = True
                End If
            Else

                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.SelectedValue = 0
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.Enabled = True
                txtOthers.Enabled = True
            End If

            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing

            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                        If dgvData.Items.Count > 0 Then
                            If cboRebatePercent.Enabled = True Then cboRebatePercent.Enabled = False
                        Else
                            If cboRebatePercent.Enabled = False Then cboRebatePercent.Enabled = True
                        End If
                End Select
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If CInt(cboExpense.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboExpense.Focus()
                Return False
            End If

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT

                Case Else
                    If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                        txtQty.Focus()
                        Return False
                    End If
            End Select


            'Dim objExpense As New clsExpense_Master
            'objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 400, "Travel from is mandatory information. Please enter Travel from to continue."), Me)
                        cboTravelFrom.Focus()
                        'objExpense = Nothing
                        Return False
                    End If

                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 401, "Travel to is mandatory information. Please enter Travel to to continue."), Me)
                        cboTravelFrom.Focus()
                        'objExpense = Nothing
                        Return False
                    End If
                    If CInt(cboTravelFrom.SelectedValue) = CInt(cboTravelTo.SelectedValue) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 402, "Travel From and Travel To cannot be same. Please select proper travel from and travel to to continue."), Me)
                        Return False
                    End If
                Case Else
                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                        cboTravelTo.Focus()
                        'objExpense = Nothing
                        Return False
                    End If
            End Select

            'If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(txtQty.Text) Then
            If mblnIsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                Return False
            End If

            'If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
            If mintExpense_MaxQuantity > 0 AndAlso mintExpense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Return False
            End If


            If CBool(Session("ClaimRemarkMandatoryForClaim")) AndAlso txtClaimRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 47, "Claim Remark is mandatory information. Please enter claim remark to continue."), Me)
                txtClaimRemark.Focus()
                Return False
            End If

            Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                Case Else
                    If CInt(cboCurrency.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 46, "Currency is mandatory information. Please select Currency to continue."), Me)
                        cboCurrency.Focus()
                        Return False
                    End If
            End Select


            Dim blnIsHRExpense As Boolean = True
            If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                blnIsHRExpense = mblnIsHRExpense
            End If

            If blnIsHRExpense Then

                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then

                    Dim objapprover As New clsleaveapprover_master
                    Dim dtList As DataTable = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                                              CInt(Session("Fin_year")), _
                                                                              CInt(Session("CompanyUnkId")), _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                           False, -1, _
                                                                              CInt(cboEmployee.SelectedValue), -1, -1, Session("LeaveApproverForLeaveType").ToString(), Nothing)

                    If dtList.Rows.Count = 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user."), Me)
                        objapprover = Nothing
                        Return False
                    End If

                    If dtList.Rows.Count > 0 Then
                        Dim objUsermapping As New clsapprover_Usermapping
                        Dim isUserExist As Boolean = False
                        For Each dr As DataRow In dtList.Rows
                            objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                            If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                        Next

                        If isUserExist = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Please Map this employee's Leave Approver to system user."), Me)
                            objUsermapping = Nothing
                            Return False
                        End If
                        objUsermapping = Nothing
                    End If

                    If CBool(Session("LeaveApproverForLeaveType")) Then
                        dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                                                     CInt(Session("Fin_year")), _
                                                                                     CInt(Session("CompanyUnkId")), _
                                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                    False, -1, CInt(cboEmployee.SelectedValue), -1, CInt(cboLeaveType.SelectedValue), _
                                                                                     Session("LeaveApproverForLeaveType").ToString(), Nothing)

                        If dtList.Rows.Count = 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Please Map this Leave type to this employee's Leave Approver(s)."), Me)
                            If dtList IsNot Nothing Then dtList.Clear()
                            dtList = Nothing
                            objapprover = Nothing
                            Return False
                        End If

                    End If

                    If dtList IsNot Nothing Then dtList.Clear()
                    dtList = Nothing
                    objapprover = Nothing


                Else
                    'If CBool(Session("PaymentApprovalwithLeaveApproval")) = False  ---------- REMOVED TO SATISFY ALL CATEGORY EXCEPT LEAVE

                    Dim objExapprover As New clsExpenseApprover_Master

                    Dim mdecTotalClaimAmount As Decimal = 0
                    If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                        Dim mdtExpense As DataTable = mdtTranDetail.Copy()

                        Dim mdecBaseAmount As Decimal = 0
                        If mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
                            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, 0, 0, mdecBaseAmount)
                            If mintClaimRequestExpenseTranId > 0 Then
                                mdecTotalClaimAmount = mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" AndAlso x.Field(Of Integer)("crtranunkid") <> mintClaimRequestExpenseTranId).Select(Function(x) x.Field(Of Decimal)("base_amount")).DefaultIfEmpty().Sum() + mdecBaseAmount
                            ElseIf mstrClaimRequestExpenseGUID.Trim.Length > 0 Then
                                mdecTotalClaimAmount = mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" AndAlso x.Field(Of String)("GUID") <> mstrClaimRequestExpenseGUID).Select(Function(x) x.Field(Of Decimal)("base_amount")).DefaultIfEmpty().Sum() + mdecBaseAmount
                            Else
                                mdecTotalClaimAmount = mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Decimal)("base_amount")).DefaultIfEmpty().Sum() + mdecBaseAmount
                            End If
                        Else
                            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, 0, 0, mdecBaseAmount)
                            mdecTotalClaimAmount = mdecBaseAmount
                        End If

                        If mdtExpense IsNot Nothing Then mdtExpense.Clear()
                        mdtExpense = Nothing

                    End If

                    Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(cboEmployee.SelectedValue), "List", Nothing, -1, False, mdecTotalClaimAmount)

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user."), Me)
                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objExapprover = Nothing
                        Return False
                    End If
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objExapprover = Nothing
                End If

            End If

            'objExpense = Nothing

            Dim sMsg As String = String.Empty
            Dim objClaimMaster As New clsclaim_request_master

            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimRequestMasterId, True _
                                                , CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue)))

            objClaimMaster = Nothing
            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Function
            End If
            sMsg = ""

            If mdtTranDetail Is Nothing Then Return False


            If mdtTranDetail IsNot Nothing Then
                Dim objExpBalance As New clsEmployeeExpenseBalance

                Dim intExpenseId As Integer = 0
                intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                    If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                End If
                Dim dsBalance As DataSet = objExpBalance.Get_Balance(CInt(cboEmployee.SelectedValue), intExpenseId, CInt(Session("Fin_year")), dtpDate.GetDate.Date _
                                                                                               , CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)), _
                                                                                               CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)))

                objExpBalance = Nothing

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestExpenseTranId > 0 Then
                            xOccurrence = mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestExpenseTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestExpenseGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestExpenseGUID.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                            Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsclaim_request_master", 4, " ] time(s)."), Me)
                            Return False

                        End If  'If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then

                    End If  'If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                End If  ' If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then

                If dsBalance IsNot Nothing Then dsBalance.Clear()
                dsBalance = Nothing

            End If  '  If mdtTran IsNot Nothing Then

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If CInt(cboLeaveType.SelectedValue) > 0 Then
                    If cboReference.Enabled = True AndAlso CInt(cboReference.SelectedValue) > 0 Then
                        Dim objlvtype As New clsleavetype_master : Dim objlvform As New clsleaveform
                        objlvtype._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                        If objlvtype._EligibilityOnAfter_Expense > 0 Then
                            objlvform._Formunkid = CInt(cboReference.SelectedValue)
                            Dim objlvissuemst As New clsleaveissue_master : Dim intIssueDays As Decimal = 0
                            intIssueDays = objlvissuemst.GetEmployeeTotalIssue(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), True, Nothing, Nothing, False, objlvform._Formunkid, -1)
                            If intIssueDays < objlvtype._EligibilityOnAfter_Expense Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type."), Me)
                                objlvform = Nothing : objlvtype = Nothing : objlvissuemst = Nothing
                                Return False
                            End If
                            objlvissuemst = Nothing
                        End If
                        objlvform = Nothing : objlvtype = Nothing
                    End If
                End If
            End If


            If pnlRebate.Visible = True Then
                If txtFlightNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Sorry, Flight number is mandatory information. Please provide flight number to continue."), Me)
                    txtFlightNo.Focus()
                    Return False
                End If

                If txtAirline.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Sorry, Airline is mandatory information. Please provide airline to continue."), Me)
                    txtAirline.Focus()
                    Return False
                End If

                If dtTravelDate.IsNull = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "Sorry, Travel date is mandatory information. Please provide travel date to continue."), Me)
                    dtTravelDate.Focus()
                    Return False
                End If

                If cboDependant.Enabled = True AndAlso cboDependant.SelectedValue = 0 AndAlso txtOthers.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 302, "Sorry, Passenger is mandatory information. Please provide passenger to continue."), Me)
                    cboDependant.Focus()
                    Return False
                End If
                If cboDependant.Enabled = True AndAlso txtOthers.Enabled = True Then
                    If cboDependant.SelectedValue <> 0 AndAlso txtOthers.Text.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 304, "Sorry, Passenger and Other cannot be used at the same time. Please provide either passenger or other to continue."), Me)
                        cboDependant.Focus()
                        Return False
                    End If
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboExpCategory.Enabled = iFlag
            cboEmployee.Enabled = iFlag
            cboPeriod.Enabled = iFlag
            dtpDate.Enabled = iFlag

            If mintClaimRequestMasterId > 0 Then
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = iFlag
                End If
            Else
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = iFlag
                End If
            End If
            cboReference.Enabled = iFlag

            If mdtTranDetail Is Nothing Then Exit Sub


            If mdtTranDetail IsNot Nothing Then
                If mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = CStr(mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    Select Case CInt(cboExpCategory.SelectedValue)
                        Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        Case Else
                            cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                            cboCurrency.Enabled = True
                    End Select
                End If


                If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    cboCurrency.Enabled = True
                End If


                If mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbeneficetranunkid") <> 0).Count > 0 Then
                    txtOthers.Enabled = False
                Else
                    txtOthers.Enabled = True
                End If

                If mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("other_person") <> "").Count > 0 Then
                    cboDependant.SelectedValue = 0
                    cboDependant.Enabled = False
                Else
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.SelectedValue = 0
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.Enabled = True

                End If
            End If


            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing

            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                        If cboRebatePercent.Enabled = True Then cboRebatePercent.Enabled = False
                End Select
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = "0"
            cboSectorRoute.SelectedValue = "0"
            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
            txtExpRemark.Text = ""
            txtQty.Text = "1"
            txtUnitPrice.Text = "1.00"
            mintEmpMaxCountDependentsForCR = 0
            cboCostCenter.SelectedValue = "0"
            mintClaimRequestExpenseTranId = 0
            mstrClaimRequestExpenseGUID = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If dgvData.Items.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please add atleast one expense in order to save."), Me)
                dgvData.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetValue(ByRef objClaimMaster As clsclaim_request_master)
        Try
            objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            'Pinkal (05-Sep-2020) -- End
            objClaimMaster._Cancel_Datetime = Nothing
            objClaimMaster._Cancel_Remark = ""
            objClaimMaster._Canceluserunkid = -1
            objClaimMaster._Iscancel = False
            objClaimMaster._Claimrequestno = txtClaimNo.Text
            objClaimMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objClaimMaster._Expensetypeid = CInt(cboExpCategory.SelectedValue)
            objClaimMaster._Isvoid = False
            objClaimMaster._Loginemployeeunkid = -1
            objClaimMaster._Transactiondate = dtpDate.GetDateTime
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                objClaimMaster._Userunkid = CInt(Session("UserId"))
                objClaimMaster._Loginemployeeunkid = -1
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objClaimMaster._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objClaimMaster._Userunkid = -1
            End If
            objClaimMaster._Voiddatetime = Nothing
            objClaimMaster._Voiduserunkid = -1
            objClaimMaster._Claim_Remark = txtClaimRemark.Text
            objClaimMaster._FromModuleId = enExpFromModuleID.FROM_EXPENSE
            objClaimMaster._IsBalanceDeduct = False

            objClaimMaster._Statusunkid = 2 'DEFAULT PENDING

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_LEAVE
                    objClaimMaster._Modulerefunkid = enModuleReference.Leave
                Case enExpenseType.EXP_MEDICAL
                    objClaimMaster._Modulerefunkid = enModuleReference.Medical
                Case enExpenseType.EXP_TRAINING
                    objClaimMaster._Modulerefunkid = enModuleReference.Training
                Case enExpenseType.EXP_MISCELLANEOUS
                    objClaimMaster._Modulerefunkid = enModuleReference.Miscellaneous
            End Select

            objClaimMaster._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_MISCELLANEOUS Then

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                If cboLeaveType.SelectedValue IsNot Nothing AndAlso cboLeaveType.SelectedValue <> "" Then
                    objClaimMaster._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
                End If
                If cboReference.SelectedValue IsNot Nothing AndAlso cboReference.SelectedValue <> "" Then
                    objClaimMaster._Referenceunkid = CInt(cboReference.SelectedValue)
                End If
                'Pinkal (10-Feb-2021) -- End

            Else
                objClaimMaster._LeaveTypeId = 0
                objClaimMaster._Referenceunkid = 0
            End If

            objClaimMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            objClaimMaster._YearId = CInt(Session("Fin_Year"))

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmClaims_RequestAddEdit"
            StrModuleName2 = "mnuUtilitiesMain"
            StrModuleName3 = "mnuClaimsExpenses"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            objClaimMaster._CompanyID = CInt(Session("CompanyUnkId"))
            objClaimMaster._ArutiSelfServiceURL = Session("ArutiSelfServiceURL").ToString()
            If CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                objClaimMaster._LoginMode = enLogin_Mode.EMP_SELF_SERVICE
            ElseIf CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                objClaimMaster._LoginMode = enLogin_Mode.MGR_SELF_SERVICE
            End If

            'Pinkal (22-Oct-2021)-- Start
            'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
            objClaimMaster._WebFormName = "frmClaims_RequestAddEdit"
            objClaimMaster._WebClientIP = Session("IP_ADD").ToString()
            objClaimMaster._WebHostName = Session("HOST_NAME").ToString()
            'Pinkal (22-Oct-2021)-- End

            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            objClaimMaster._RebateId = CInt(cboRebatePercent.SelectedValue)
            objClaimMaster._RebatePercentage = CDec(cboRebatePercent.SelectedItem.Text)
            objClaimMaster._SeatTypeId = CInt(cboSeatStatus.SelectedValue)
            objClaimMaster._BaggaeValue = lblFreeBaggage.Text
            'S.SANDEEP |10-MAR-2022| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttchment Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttchment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "FillAttachment", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtAttchment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtAttchment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Claim_Request
                dRow("scanattachrefid") = enScanAttactRefId.CLAIM_REQUEST


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("transactionunkid") = dgvData.Items(mintDeleteIndex).Cells(10).Text
                dRow("transactionunkid") = dgvData.Items(mintExpenseIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text
                'Pinkal (18-Mar-2021) -- End

                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("GUID") = dgvData.Items(mintDeleteIndex).Cells(12).Text
                dRow("GUID") = dgvData.Items(mintExpenseIndex).Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text
                'Pinkal (18-Mar-2021) -- End

                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))


                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                dRow("Documentype") = GetMimeType(strfullpath)
                Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)

                'S.SANDEEP |15-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : Log Error Only
                'dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try
                'S.SANDEEP |15-AUG-2020| -- END

                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                'Pinkal (20-Nov-2018) -- End

                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END

            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtAttchment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub AddExpense()
        'Dim mdtTran As DataTable = Nothing
        Try

            'If mdtTranDetail IsNot Nothing Then
            '    mdtTran = mdtTranDetail.Copy()
            'End If

            If mdtTranDetail Is Nothing Then Exit Sub


            Dim dtmp() As DataRow = Nothing
            If mdtTranDetail.Rows.Count > 0 Then

                dtmp = mdtTranDetail.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                              "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim().Replace("'", "''") & "' AND AUD <> 'D'")

                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If


                dtmp = Nothing
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                        If mdtTranDetail.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("dpndtbeneficetranunkid")) <> 0).Count() > 0 Then
                            dtmp = mdtTranDetail.Select("dpndtbeneficetranunkid = '" & CInt(cboDependant.SelectedValue) & "' AND quantity > 0 AND AUD <> 'D'")
                        ElseIf mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("other_person").ToString().Trim().Length > 0).Count() > 0 Then
                            dtmp = mdtTranDetail.Select("other_person = '" & txtOthers.Text.Trim().Replace("'", "''") & "' AND quantity > 0 AND AUD <> 'D'")
                        End If
                        If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then If txtQty.Text.Trim.Length > 0 Then txtQty.Text = 0
                End Select

            End If

            'Dim objExpMaster As New clsExpense_Master
            Dim intExpenseId As Integer = 0

            If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
            Else
                intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
            End If
            'objExpMaster._Expenseunkid = intExpenseId


            'If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
            '    dtpDate.Focus()
            '    Exit Sub
            'End If

            'mblnIsHRExpense = objExpMaster._IsHRExpense
            'mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
            'Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            'Dim mstrGLCode As String = ""
            'Dim mstrGLDescription As String = objExpMaster._Description

            'If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
            '    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
            '        cboCostCenter.Focus()
            '        Exit Sub
            '    End If
            '    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            'End If

            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '    Dim iValue As Decimal = 0
            '    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
            '            Select Case CInt(cboExpCategory.SelectedValue)
            '                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
            '                    If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
            '                        iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
            '                    Else
            '                        iValue = CDec(txtBalance.Text)
            '                    End If
            '                Case Else
            '                    iValue = CDec(txtQty.Text)
            '            End Select
            '            If iValue > CDec(txtBalance.Text) Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
            '                txtQty.Focus()
            '                objExpMaster = Nothing
            '                Exit Sub
            '            End If
            '        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
            '            Select Case CInt(cboExpCategory.SelectedValue)
            '                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
            '                    If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
            '                        iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
            '                    Else
            '                        iValue = CDec(txtBalance.Text)
            '                    End If
            '                Case Else
            '                    iValue = CDec(txtQty.Text)
            '            End Select
            '            If iValue > CDec(txtBalance.Text) Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
            '                txtQty.Focus()
            '                objExpMaster = Nothing
            '                Exit Sub
            '            End If
            '        End If
            '    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '        Select Case CInt(cboExpCategory.SelectedValue)
            '            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
            '                If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
            '                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
            '                Else
            '                    iValue = CDec(txtBalance.Text)
            '                End If
            '            Case Else
            '                iValue = CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text))
            '        End Select
            '        If iValue > CDec(txtBalance.Text) Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
            '            txtQty.Focus()
            '            objExpMaster = Nothing
            '            Exit Sub
            '        End If
            '    End If
            'End If

            'If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
            '    Dim mdecBudgetAmount As Decimal = 0
            '    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrGLCode, mstrCostCenterCode, mdecBudgetAmount) = False Then Exit Sub
            '    If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
            '        objExpMaster = Nothing
            '        Exit Sub
            '    End If
            'End If

            If mblnDoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If

            'mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
            'mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsBudgetMandatory"))
            'Dim mintGLCodeID As Integer = CInt(Me.ViewState("GLCodeId"))
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = mstrDescription
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""

            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If mintExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(mintExpenditureTypeId, mintGLCodeId, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            If mblnIsaccrue OrElse mblnIsLeaveEncashment Then
                Dim iValue As Decimal = 0
                If mintUomunkid = enExpUoM.UOM_QTY Then
                    If mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
                                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
                                Else
                                    iValue = CDec(txtBalance.Text)
                                End If
                            Case Else
                                iValue = CDec(txtQty.Text)
                        End Select
                        If iValue > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    ElseIf mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
                                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
                                Else
                                    iValue = CDec(txtBalance.Text)
                                End If
                            Case Else
                                iValue = CDec(txtQty.Text)
                        End Select
                        If iValue > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    End If
                ElseIf mintUomunkid = enExpUoM.UOM_AMOUNT Then
                    Select Case CInt(cboExpCategory.SelectedValue)
                        Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                            If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
                                iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
                            Else
                                iValue = CDec(txtBalance.Text)
                            End If
                        Case Else
                            iValue = CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text))
                    End Select
                    If iValue > CDec(txtBalance.Text) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtQty.Focus()
                        Exit Sub
                    End If
                End If
            End If

            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrGLCode, mstrCostCenterCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                    Exit Sub
                End If
            End If


            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master

                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then   'crtranunkid
                    iEncashment = mdtTranDetail.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then  'GUID
                    iEncashment = mdtTranDetail.Select("GUID = '" & dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                'If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                If mblnIsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    objExpMasterOld = Nothing
                    Exit Sub

                    'ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                ElseIf mblnIsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    objExpMasterOld = Nothing
                    Exit Sub
                End If
                objExpMasterOld = Nothing
            End If

            'objExpMaster = Nothing

            If Session("MaxRoutePerExpCategory") IsNot Nothing Then
                If CType(Session("MaxRoutePerExpCategory"), Dictionary(Of Integer, Integer)).ContainsKey(CInt(cboExpCategory.SelectedValue)) = True Then
                    Dim iMaxRouteAllow As Integer = CType(Session("MaxRoutePerExpCategory"), Dictionary(Of Integer, Integer))(CInt(cboExpCategory.SelectedValue))
                    If iMaxRouteAllow > 0 Then
                        If dgvData.Items.Count = iMaxRouteAllow Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Sorry, you cannot add this transaction. Reason : Maximum route allowed has been reached for the selected expense category. Maximum route allowed for the selected category is [ " & iMaxRouteAllow.ToString() & " ]."), Me)
                            Exit Sub
                        End If
                    End If
                End If
            End If

            If CheckImpreseRetireOrNot(mintClaimRequestMasterId) = False Then Exit Sub

            Dim dRow As DataRow = mdtTranDetail.NewRow

            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimRequestMasterId
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
            dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            dRow.Item("quantity") = txtQty.Text
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("loginemployeeunkid") = -1
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeId
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            dRow.Item("airline") = txtAirline.Text.Trim
            If cboTravelFrom.SelectedItem IsNot Nothing Then dRow.Item("travelfrom") = cboTravelFrom.SelectedItem.Text
            If cboTravelTo.SelectedItem IsNot Nothing Then dRow.Item("travelto") = cboTravelTo.SelectedItem.Text

            dRow.Item("flightno") = txtFlightNo.Text.Trim
            If dtTravelDate.IsNull = False Then
                dRow.Item("traveldate") = dtTravelDate.GetDate()
            End If

            If txtOthers.Text.Trim.Length > 0 Then
                dRow.Item("name") = txtOthers.Text.Trim
                dRow.Item("relation") = ""
                dRow.Item("psgtype") = Language.getMessage("clsclaim_request_tran", 101, "Other")
            ElseIf CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue)) > 0 Then
                dRow.Item("name") = cboDependant.SelectedItem.Text
                Dim objCMstr As New clsCommon_Master
                Dim objDpndt As New clsDependants_Beneficiary_tran
                objDpndt._Dpndtbeneficetranunkid = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
                objCMstr._Masterunkid = objDpndt._Relationunkid
                dRow.Item("relation") = objCMstr._Name
                objCMstr = Nothing
                dRow.Item("psgtype") = Language.getMessage("clsclaim_request_tran", 100, "Dependant")
            Else
                dRow.Item("name") = cboEmployee.SelectedItem.Text
                dRow.Item("relation") = ""
                dRow.Item("psgtype") = Language.getMessage("clsclaim_request_tran", 102, "Self")
            End If
            dRow.Item("dpndtbeneficetranunkid") = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
            If txtOthers.Text.Trim.Length > 0 Then
                dRow.Item("other_person") = txtOthers.Text.Trim
            Else
                dRow.Item("other_person") = ""
            End If
            dRow.Item("fromrouteid") = CInt(IIf(cboTravelFrom.SelectedValue = "", 0, cboTravelFrom.SelectedValue))
            dRow.Item("torouteid") = CInt(IIf(cboTravelTo.SelectedValue = "", 0, cboTravelTo.SelectedValue))
            dRow.Item("currency") = cboCurrency.SelectedItem.Text
            dRow.Item("base_currency") = mstrBaseCurrencysign
            mdtTranDetail.Rows.Add(dRow)
            'mdtTranDetail = mdtTran.Copy()
            Fill_Expense()
            Enable_Disable_Ctrls(False)
            Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing
        End Try
    End Sub

    Private Sub EditExpense()
        'Dim mdtTran As DataTable = Nothing
        Try
            Dim iRow As DataRow() = Nothing

            'If mdtTranDetail IsNot Nothing Then
            '    mdtTran = mdtTranDetail.Copy()
            'End If

            If mdtTranDetail Is Nothing Then Exit Sub

            If CInt(Me.ViewState("EditCrtranunkid")) > 0 Then
                iRow = mdtTranDetail.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
            Else
                iRow = mdtTranDetail.Select("GUID = '" & CStr(Me.ViewState("EditGuid")) & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtTranDetail.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Length > 0, CDec(txtQty.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim().Replace("'", "''") & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtTranDetail.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Length > 0, CDec(txtQty.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim().Replace("'", "''") & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If

                'Dim objExpMaster As New clsExpense_Master
                Dim intExpenseId As Integer = 0
                If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                    If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                Else
                    intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                End If

                'objExpMaster._Expenseunkid = intExpenseId

                'mblnIsHRExpense = objExpMaster._IsHRExpense

                'mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
                'Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                'Dim mstrGLCode As String = ""
                'Dim mstrGLDescription As String = objExpMaster._Description
                'Dim mintCostCenterID As Integer = 0
                'Dim mstrCostCenterCode As String = ""
                'If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                '    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                '        cboCostCenter.Focus()
                '        objExpMaster = Nothing
                '        Exit Sub
                '    End If
                '    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                'End If

                'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                '    Dim iValue As Decimal = 0
                '    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                '        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                '            Select Case CInt(cboExpCategory.SelectedValue)
                '                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                '                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) - 1
                '                Case Else
                '                    iValue = CDec(txtQty.Text)
                '            End Select
                '            If iValue > CDec(txtBalance.Text) Then
                '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                '                txtQty.Focus()
                '                objExpMaster = Nothing
                '                Exit Sub
                '            End If
                '        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                '            Select Case CInt(cboExpCategory.SelectedValue)
                '                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                '                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) - 1
                '                Case Else
                '                    iValue = CDec(txtQty.Text)
                '            End Select
                '            If iValue > CDec(txtBalance.Text) Then
                '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                '                txtQty.Focus()
                '                objExpMaster = Nothing
                '                Exit Sub
                '            End If
                '        End If

                '    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                '        Select Case CInt(cboExpCategory.SelectedValue)
                '            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                '                iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) - 1
                '            Case Else
                '                iValue = CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text))
                '        End Select
                '        If iValue > CDec(txtBalance.Text) Then
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                '            txtQty.Focus()
                '            objExpMaster = Nothing
                '            Exit Sub
                '        End If
                '    End If
                'End If

                'If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                '    Dim mdecBudgetAmount As Decimal = 0
                '    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrGLCode, mstrCostCenterCode, mdecBudgetAmount) = False Then Exit Sub
                '    If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                '        objExpMaster = Nothing
                '        Exit Sub
                '    End If
                'End If

                'Dim mintGLCodeID As Integer = CInt(Me.ViewState("GLCodeId"))
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = mstrDescription
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""
                If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If mintExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(mintExpenditureTypeId, mintGLCodeId, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If

                If mblnIsaccrue OrElse mblnIsLeaveEncashment Then
                    Dim iValue As Decimal = 0
                    If mintUomunkid = enExpUoM.UOM_QTY Then
                        If mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                            Select Case CInt(cboExpCategory.SelectedValue)
                                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) - 1
                                Case Else
                                    iValue = CDec(txtQty.Text)
                            End Select
                            If iValue > CDec(txtBalance.Text) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                                txtQty.Focus()
                                Exit Sub
                            End If
                        ElseIf mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                            Select Case CInt(cboExpCategory.SelectedValue)
                                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) - 1
                                Case Else
                                    iValue = CDec(txtQty.Text)
                            End Select
                            If iValue > CDec(txtBalance.Text) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                                txtQty.Focus()
                                Exit Sub
                            End If
                        End If

                    ElseIf mintUomunkid = enExpUoM.UOM_AMOUNT Then
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) - 1
                            Case Else
                                iValue = CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text))
                        End Select
                        If iValue > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                            txtQty.Focus()
                            Exit Sub
                        End If
                    End If
                End If



                If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrGLCode, mstrCostCenterCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                        Exit Sub
                    End If
                End If

                If dgvData.Items.Count >= 1 Then
                    Dim objExpMasterOld As New clsExpense_Master
                    Dim iEncashment As DataRow() = Nothing

                    If CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then    'crtranunkid
                        iEncashment = mdtTranDetail.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then  'GUID
                        iEncashment = mdtTranDetail.Select("GUID = '" & dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                    End If

                    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                    'If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    If mblnIsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        objExpMasterOld = Nothing
                        Exit Sub
                        'ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    ElseIf mblnIsHRExpense <> objExpMasterOld._IsHRExpense Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        objExpMasterOld = Nothing
                        Exit Sub
                    End If
                    objExpMasterOld = Nothing
                End If
                'objExpMaster = Nothing

                iRow(0).Item("crtranunkid") = iRow(0).Item("crtranunkid")
                iRow(0).Item("crmasterunkid") = mintClaimRequestMasterId
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("quantity") = txtQty.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("loginemployeeunkid") = -1
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeId
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                iRow(0).Item("airline") = txtAirline.Text.Trim
                If cboTravelFrom.SelectedItem IsNot Nothing Then iRow(0).Item("travelfrom") = cboTravelFrom.SelectedItem.Text
                If cboTravelTo.SelectedItem IsNot Nothing Then iRow(0).Item("travelto") = cboTravelTo.SelectedItem.Text

                iRow(0).Item("flightno") = txtFlightNo.Text.Trim
                If dtTravelDate.IsNull = False Then
                    iRow(0).Item("traveldate") = dtTravelDate.GetDate()
                End If
                If txtOthers.Text.Trim.Length > 0 AndAlso txtOthers.Text.Trim <> "&nbsp;" Then
                    iRow(0).Item("name") = txtOthers.Text.Trim
                    iRow(0).Item("relation") = ""
                    iRow(0).Item("psgtype") = Language.getMessage("clsclaim_request_tran", 101, "Other")
                ElseIf CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue)) > 0 Then
                    iRow(0).Item("name") = cboDependant.SelectedItem.Text
                    Dim objCMstr As New clsCommon_Master
                    Dim objDpndt As New clsDependants_Beneficiary_tran
                    objDpndt._Dpndtbeneficetranunkid = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
                    objCMstr._Masterunkid = objDpndt._Relationunkid
                    iRow(0).Item("relation") = objCMstr._Name
                    objCMstr = Nothing
                    iRow(0).Item("psgtype") = Language.getMessage("clsclaim_request_tran", 100, "Dependant")
                Else
                    iRow(0).Item("name") = cboEmployee.SelectedItem.Text
                    iRow(0).Item("relation") = ""
                    iRow(0).Item("psgtype") = Language.getMessage("clsclaim_request_tran", 102, "Self")
                End If
                iRow(0).Item("dpndtbeneficetranunkid") = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
                iRow(0).Item("other_person") = txtOthers.Text.Trim
                iRow(0).Item("fromrouteid") = CInt(IIf(cboTravelFrom.SelectedValue = "", 0, cboTravelFrom.SelectedValue))
                iRow(0).Item("torouteid") = CInt(IIf(cboTravelTo.SelectedValue = "", 0, cboTravelTo.SelectedValue))
                iRow(0).Item("currency") = cboCurrency.SelectedItem.Text
                iRow(0).Item("base_currency") = mstrBaseCurrencysign
                iRow(0).AcceptChanges()

                'mdtTranDetail = mdtTran.Copy()

                Fill_Expense()
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- End

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date)
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try

            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xGLCode As String, ByVal xCostCeneterCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Dim xResponseData As String = ""
        Dim xPostedData As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), Me)
                Return False
            End If

            Dim objMaster As New clsMasterData
            mstrP2PToken = objMaster.GetP2PToken(CInt(Session("CompanyUnkId")), Nothing)

            If mstrP2PToken.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

                Dim mstrDimensionValue As String = ""

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboEmployee.SelectedValue)

                Dim objUnitGroup As New clsUnitGroup
                objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                Dim mstrUnitGroupCode As String = objUnitGroup._Code.ToString().Trim()
                objUnitGroup = Nothing

                'Dim objDepartment As New clsDepartment
                'objDepartment._Departmentunkid = objEmployee._Departmentunkid
                'Dim mstrDepartmentCode As String = objDepartment._Code.ToString().Trim
                'objDepartment = Nothing

                'Dim objSectionGroup As New clsSectionGroup
                'objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                'Dim mstrSectionGroupCode As String = objSectionGroup._Code.ToString().Trim
                'objSectionGroup = Nothing

                Dim objUnits As New clsUnits
                objUnits._Unitunkid = objEmployee._Unitunkid
                Dim mstrUnitCode As String = objUnits._Code.ToString().Trim
                objUnits = Nothing


                Dim objClassGroup As New clsClassGroup
                objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                Dim mstrClassGroupCode As String = objClassGroup._Code.ToString().Trim()
                objClassGroup = Nothing

                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                Dim mstrClassCode As String = objClass._Code.ToString().Trim()
                objClass = Nothing

                Dim objCostCenter As New clscostcenter_master
                objCostCenter._Costcenterunkid = objEmployee._Costcenterunkid
                Dim mstrCostCenterCode As String = objCostCenter._Costcentercode.ToString().Trim()
                Dim mstrCostCenterDescription As String = objCostCenter._Description.ToString().Trim()
                objCostCenter = Nothing

                objEmployee = Nothing



                ' Dimensionvalue : NMB – SUB UNIT I – FUNCTION – DEPARTMENT – ZONE – BRANCH

                'NMB(-Hardcoded)
                'Sub UNIT I - Unit Group
                'FUNCTION - Department
                'Department - Section Group
                'Zone - Class Group
                'Branch - Class

                'mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrDepartmentCode.Trim() & "-" & mstrSectionGroupCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()


                'GL- SubUnit I - SubUnit II - CostCenter - Zone - Branch
                'GL - GL Code
                'Sub UNIT I - Unit Group
                'Sub UNIT II - Unit
                'CostCenter - CostCenter
                'Zone - Class Group
                'Branch - Class


                'Pinkal (14-Jun-2024) -- Start
                'A1X -2649  - NMB - P2P API changes 

                'mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & xCostCeneterCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                If mstrCostCenterCode.Trim() <> mstrClassCode.Trim() AndAlso mstrCostCenterDescription.Trim.ToUpper = "ZONAL OFFICE" Then
                    mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & mstrCostCenterCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                ElseIf mstrCostCenterCode.Trim() <> mstrClassCode.Trim() AndAlso mstrCostCenterDescription.Trim.ToUpper <> "ZONAL OFFICE" Then
                    mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & mstrCostCenterCode.Trim() & "--"
                Else
                    mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & mstrCostCenterCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()
                End If

                'Pinkal (14-Jun-2024) -- End


                Dim objP2PBudgetBalanceService As New clsP2PBudgetBalanceService
                objP2PBudgetBalanceService.budgetBalanceRequest.CompanyCode = Session("CompanyCode").ToString()
                objP2PBudgetBalanceService.budgetBalanceRequest.GLCode = xGLCode.Trim
                objP2PBudgetBalanceService.budgetBalanceRequest.DimensionValue = mstrDimensionValue

                'Pinkal (20-Dec-2024) -- Start
                'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 

                'objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Format(New Date(2024, 1, 1), "dd/MM/yyyy")
                'objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = CDate(Session("fin_startdate")).Day.ToString() & "-" & CDate(Session("fin_startdate")).Month.ToString() & "-" & CDate(Session("fin_startdate")).Year.ToString()
                objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Now.Day.ToString() & "-" & Now.Month.ToString() & "-" & Now.Year.ToString()


                'objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY2024"
                'objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY_" & CDate(Session("fin_startdate")).Year.ToString()
                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY" & CDate(Session("fin_startdate")).Year.ToString()


                'objP2PBudgetBalanceService.budgetBalanceRequest.CalenderCode = "NMB-STD"
                objP2PBudgetBalanceService.budgetBalanceRequest.CalenderCode = "NMB - STD"

                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetCycleCode = "Fiscal"
                'Pinkal (20-Dec-2024) -- End

                Try
                    If objMaster.GetSetP2PWebRequest(strServiceURL.Trim(), True, True, "BudgetService", xResponseData, mstrError, objP2PBudgetBalanceService, xPostedData, mstrP2PToken.Trim) = False Then
                        DisplayMessage.DisplayMessage(mstrError, Me)
                        Return False
                    End If
                Catch wbex As WebException
                    DisplayMessage.DisplayMessage(wbex.Message, Me)
                Catch ex As Exception
                    DisplayMessage.DisplayMessage(ex.Message, Me)
                End Try

                If xResponseData.Trim.Length > 0 Then
                    Dim dtAmount As DataTable = JsonStringToDataTable(xResponseData)
                    If dtAmount IsNot Nothing AndAlso dtAmount.Rows.Count > 0 Then
                        If dtAmount.Columns.Contains("TotalFundsAvailableAmountMST") Then
                            mdecBudgetAmount = CDec(dtAmount.Rows(0)("TotalFundsAvailableAmountMST"))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 53, "There is no Budget for this request."), Me)
                            Return False
                        End If
                    End If
                    If dtAmount IsNot Nothing Then dtAmount.Clear()
                    dtAmount = Nothing
                End If

            End If  '  If mstrP2PToken.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function UpdateDeleteAttachment(ByVal dr As DataRow, ByVal mblnDeleteAttachment As Boolean, ByVal mstrAUD As String) As Boolean
        Try
            If dr IsNot Nothing Then dr("AUD") = mstrAUD
            dr.AcceptChanges()

            If mblnDeleteAttachment Then
                Try
                    If dr IsNot Nothing Then File.Delete(dr("filepath").ToString())
                Catch ex As Exception
                End Try
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
                objEmpCC = Nothing
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
                End If
                dsList.Clear()
                dsList = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetP2PToken() As String
        Dim mstrToken As String = ""
        Dim mstrError As String = ""
        Dim mstrGetData As String = ""
        Try
            If Session("GetTokenP2PServiceURL") IsNot Nothing AndAlso Session("GetTokenP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim objMstData As New clsMasterData
                Dim objCRUser As New clsCRP2PUserDetail
                If objMstData.GetSetP2PWebRequest(Session("GetTokenP2PServiceURL").ToString().Trim(), True, True, objCRUser.username, mstrGetData, mstrError, objCRUser, "", "") = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objMstData = Nothing
                    objCRUser = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Return mstrToken
                End If
                objCRUser = Nothing

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mstrToken = dtTable.Rows(0)("token").ToString()
                    End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End

                End If
                objMstData = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mstrToken
    End Function

    Private Sub SetFormControls()
        Try
            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    pnlRebateForNonEmployee.Visible = False
                Else
                    pnlRebateForNonEmployee.Visible = CBool(Session("AllowToAddRebateForNonEmployee"))
                End If
                'S.SANDEEP |29-MAR-2022| -- START
                'ISSUE/ENHANCEMENT : OLD-580-V2
                txtAirline.Text = Session("CompanyGroupName").ToString().ToUpper()
                txtAirline.Enabled = False
                'S.SANDEEP |29-MAR-2022| -- END
                If CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                    Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                        Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                            pnlRebate.Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = True

                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhExpense", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhSectorRoute", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUoM", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhQty", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Visible = False

                        Case Else
                            pnlRebate.Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhExpense", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhSectorRoute", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUoM", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhQty", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Visible = True
                    End Select
                Else
                    pnlRebate.Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                End If
            Else
                pnlRebate.Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveData()
        Dim blnFlag As Boolean = False
        Dim mstrFolderName As String = ""
        Dim strFileName As String = ""
        Dim objClaimMaster As New clsclaim_request_master
        Try

            'Pinkal (10-Jun-2022) -- Start
            'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
            If CheckImpreseRetireOrNot(mintClaimRequestMasterId) = False Then Exit Sub
            'Pinkal (10-Jun-2022) -- End

            'Dim mdtTran As DataTable = Nothing
            'If mdtTranDetail IsNot Nothing Then
            '    mdtTran = mdtTranDetail.Copy()
            'End If

            If mdtTranDetail Is Nothing Then Exit Sub


            Call SetValue(objClaimMaster)

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            If mintClaimRequestMasterId > 0 Then
                Dim objApproverTran As New clsclaim_request_approval_tran
                Dim dsList As DataSet = objApproverTran.GetApproverExpesneList("List", False, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                              , Session("EmployeeAsOnDate").ToString(), CInt(cboExpCategory.SelectedValue), False, True, -1, "", mintClaimRequestMasterId, Nothing)
                objApproverTran = Nothing

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim dr = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleid") = 2).ToList()   'Pending
                    If dr IsNot Nothing AndAlso dr.Count <= 0 Then
                        'Pinkal (27-Sep-2024) -- Start
                        'NMB Enhancement : (A1X-2787) NMB - Credit report development.
                        If mdtTranDetail IsNot Nothing Then
                            For Each drRow As DataRow In mdtTranDetail.Rows
                                drRow("AUD") = "U"
                                drRow.AcceptChanges()
                            Next
                        End If
                        'Pinkal (27-Sep-2024) -- End
                    End If
                End If
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                objApproverTran = Nothing
            End If
            'Pinkal (24-Jun-2024) -- End


            Dim objExpense As New clsExpense_Master
            Dim xRow() As DataRow = Nothing

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            If mdtTranDetail IsNot Nothing Then
                For Each dRow As DataRow In mdtTranDetail.Select("AUD <> 'D'")
                    objExpense._Expenseunkid = CInt(dRow.Item("expenseunkid"))
                    If objExpense._IsAttachDocMandetory = True Then
                        If CInt(dRow.Item("crtranunkid")) > 0 Then
                            xRow = mdtFinalAttchment.Select("transactionunkid = '" & CInt(dRow.Item("crtranunkid")) & "' AND AUD <> 'D' ")
                        ElseIf CStr(dRow.Item("GUID")).Trim.Length > 0 Then
                            xRow = mdtFinalAttchment.Select("GUID = '" & CStr(dRow.Item("GUID")) & "' AND AUD <> 'D' ")
                        End If
                        If xRow.Count <= 0 Then
                            DisplayMessage.DisplayMessage(dRow.Item("expense").ToString & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "has mandatory document attachment. please attach document."), Me)
                            objExpense = Nothing
                            Exit Sub
                        End If
                    End If
                Next
            End If
            'Pinkal (27-Sep-2024) -- End

            objExpense = Nothing

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                mblnPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            End If

            If mdtFinalAttchment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                For Each dRow As DataRow In mdtFinalAttchment.Rows
                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
                If mdsDoc IsNot Nothing Then mdsDoc.Clear()
                mdsDoc = Nothing
            End If

            If mintClaimRequestMasterId > 0 Then
                blnFlag = objClaimMaster.Update(Session("Database_Name").ToString, _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 True, mdtTranDetail, ConfigParameter._Object._CurrentDateAndTime, mdtFinalAttchment, Nothing, mblnPaymentApprovalwithLeaveApproval)

            Else

                blnFlag = objClaimMaster.Insert(Session("Database_Name").ToString(), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 False, _
                                                mdtTranDetail, ConfigParameter._Object._CurrentDateAndTime, mdtFinalAttchment, Nothing, mblnPaymentApprovalwithLeaveApproval)

            End If

            If blnFlag = False And objClaimMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objClaimMaster._Message, Me)
                Exit Sub
            End If

            If blnFlag = True Then

                If mdtFinalAttchment IsNot Nothing Then mdtFinalAttchment.Clear()
                mdtFinalAttchment = Nothing

                If mdtAttchment IsNot Nothing Then mdtAttchment.Clear()
                mdtAttchment = Nothing

                'If mdtTran IsNot Nothing Then mdtTran.Clear()
                'mdtTran = Nothing

                If mdtTranDetail IsNot Nothing Then mdtTranDetail.Clear()
                mdtTranDetail = Nothing

                If Me.Session("mdtAttchment") IsNot Nothing Then Session("mdtAttchment") = Nothing
                If Me.Session("mdtFinalAttchment") IsNot Nothing Then Session("mdtFinalAttchment") = Nothing
                If Me.Session("mdtTranDetail") IsNot Nothing Then Session("mdtTranDetail") = Nothing

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Claim Request saved successfully."), Me.Page, Session("rootpath").ToString & "Claims_And_Expenses/wPg_ClaimAndRequestList.aspx")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objClaimMaster = Nothing
        End Try
    End Sub

    Private Sub UpdateRebateInformation(ByVal iSrcRow As DataRow, ByVal iDstRow As DataRow)
        Try
            iDstRow("base_amount") = iSrcRow("base_amount")
            iDstRow("exchangerateunkid") = iSrcRow("exchangerateunkid")
            iDstRow("exchange_rate") = iSrcRow("exchange_rate")
            iDstRow("unitprice") = iSrcRow("unitprice")
            iDstRow("quantity") = iSrcRow("quantity")
            iDstRow("amount") = iSrcRow("amount")
            iDstRow("countryunkid") = iSrcRow("countryunkid")
            iDstRow("base_countryunkid") = iSrcRow("base_countryunkid")
            iDstRow.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckImpreseRetireOrNot(ByVal xClaimRequestMasterId As Integer) As Boolean
        Dim mblnFlag As Boolean = True
        Try
            Dim mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory As String = Session("DoNotAllowToApplyImprestIfUnRetiredForExpCategory").ToString()
            If mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory.Trim.Length > 0 Then
                Dim ar() = mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory.Trim.Split(CChar(","))
                If Array.Exists(ar, Function(x) x = CInt(cboExpCategory.SelectedValue)) Then
                    Dim xClaimRequestId As Integer = -1
                    Dim objClaimMaster As New clsclaim_request_master
                    Dim xStatusId As Integer = objClaimMaster.GetEmployeeLastClaimFormStatus(CInt(cboExpCategory.SelectedValue), CInt(cboEmployee.SelectedValue), xClaimRequestId, xClaimRequestMasterId)
                    If xStatusId = 2 Then    'Pending
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 51, "Sorry, you cannot apply for this claim application now as your previous claim application has not been approved for this expense category yet."), Me)
                        mblnFlag = False
                        objClaimMaster = Nothing
                        Return mblnFlag

                    ElseIf xStatusId = 1 Then    'Approved
                        Dim xClaimRetirementStatusId As Integer = 2
                        Dim objRetirement As New clsclaim_retirement_master
                        Dim mintclaimRetirementId As Integer = objRetirement.GetRetirementFromClaimRequest(xClaimRequestId, xClaimRetirementStatusId, Nothing)
                        If xClaimRetirementStatusId = 2 Then  'Pending
                            If mintclaimRetirementId <= 0 Then
                                Dim objRetirementProcess As New clsretire_process_tran
                                Dim dsRetirement As DataSet = objRetirementProcess.GetUnRetiredList("List", False, True, -1, "cmclaim_request_master.crmasterunkid = " & xClaimRequestId, Nothing)
                                If dsRetirement IsNot Nothing AndAlso dsRetirement.Tables(0).Rows.Count > 0 Then
                                    If dsRetirement.Tables(0).Rows(0)("periodunkid") <= 0 AndAlso CBool(dsRetirement.Tables(0).Rows(0)("isposted")) = False AndAlso CInt(dsRetirement.Tables(0).Rows(0)("crmasterunkid")) > 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet."), Me)
                                        mblnFlag = False
                                        objRetirement = Nothing
                                        Return mblnFlag
                                    End If  'If dsRetirement.Tables(0).Rows(0)("periodunkid") <= 0 AndAlso CBool(dsRetirement.Tables(0).Rows(0)("isposted")) = False AndAlso CInt(dsRetirement.Tables(0).Rows(0)("crmasterunkid")) > 0 Then 
                                    objRetirementProcess = Nothing
                                End If  'If dsRetirement IsNot Nothing AndAlso dsRetirement.Tables(0).Rows.Count > 0 Then
                            ElseIf mintclaimRetirementId > 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet."), Me)
                                mblnFlag = False
                                objRetirement = Nothing
                                Return mblnFlag
                            End If '     If mintclaimRetirementId <= 0 Then
                            objRetirement = Nothing

                        ElseIf xClaimRetirementStatusId = 3 Then  'Rejected
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet."), Me)
                            mblnFlag = False
                            Return mblnFlag
                        End If '    If xClaimRetirementStatusId = 2 Then  'Pending

                    End If '    ElseIf xStatusId = 1 Then    'Approved
                    objClaimMaster = Nothing
                End If ' If Array.Exists(ar, Function(x) x = CInt(cboExpCategory.SelectedValue)) Then

            End If '  If mstrDoNotAllowToApplyImprestIfUnRetiredForExpCategory.Trim.Length > 0 Then
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mblnFlag
    End Function

    Public Function GetPrivilegeExpenseBalance(ByVal intExpensCategoryId As Integer) As Integer
        Dim objExpense As New clsExpense_Master
        Dim intExpenseId As Integer = 0
        Try
            If intExpensCategoryId = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                objExpense.isExist("", "", enExpenseType.EXP_REBATE_PRIVILEGE, -1, intExpenseId, "isaccrue = 1")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objExpense = Nothing
        End Try
        Return intExpenseId
    End Function

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            If mdtFinalAttchment IsNot Nothing Then mdtFinalAttchment.Clear()
            mdtFinalAttchment = Nothing

            If mdtAttchment IsNot Nothing Then mdtAttchment.Clear()
            mdtAttchment = Nothing

            If mdtTranDetail IsNot Nothing Then mdtTranDetail.Clear()
            mdtTranDetail = Nothing

            If Me.Session("mdtAttchment") IsNot Nothing Then Session("mdtAttchment") = Nothing
            If Me.Session("mdtFinalAttchment") IsNot Nothing Then Session("mdtFinalAttchment") = Nothing
            If Me.Session("mdtTranDetail") IsNot Nothing Then Session("mdtTranDetail") = Nothing

            Session("mintClaimRequestMasterId") = Nothing
            Response.Redirect("~/Claims_And_Expenses/wPg_ClaimAndRequestList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                If gRow IsNot Nothing Then gRow.ToList.Clear()
                gRow = Nothing
                Exit Sub
            End If

            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

            mblnIsEditClick = False
            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "1.00"

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            If Validation() = False Then
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                If gRow IsNot Nothing Then gRow.ToList.Clear()
                gRow = Nothing
                Exit Sub
            End If

            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

            mblnIsEditClick = True

            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "1.00"

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Is_Valid() = False Then Exit Sub

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    'Dim mdtTran As DataTable = Nothing
                    'If mdtTranDetail IsNot Nothing Then
                    '    mdtTran = mdtTranDetail.Copy()
                    'End If
                    If mdtTranDetail Is Nothing Then Exit Sub

                    Dim mValidData As Boolean = True
                    For Each mRow As DataRow In mdtTranDetail.Rows
                        Dim dt1 As Date = Nothing : Dim dt2 As Date = Nothing
                        If CDate(mRow("traveldate")).Date < dtpDate.GetDate().Date Then
                            dt1 = CDate(mRow("traveldate")).Date
                            dt2 = dtpDate.GetDate().Date
                        Else
                            dt1 = dtpDate.GetDate().Date
                            dt2 = CDate(mRow("traveldate")).Date
                        End If
                        If (DateDiff(DateInterval.Day, dt1, dt2) + 1) < 2 Then
                            mValidData = False
                            Exit For
                        End If
                    Next
                    If mValidData = False Then
                        popup_RebateCnf.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 51, "This Application is Late,Supposed to Apply 2 days before travel date so it may delay, so press yes to continue or No to cancel.")
                        popup_RebateCnf.Show()
                    Else
                        SaveData()
                    End If
                    'If mdtTran IsNot Nothing Then mdtTran.Clear()
                    'mdtTran = Nothing
                Case Else
                    SaveData()
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Dim mdtTran As DataTable = Nothing
        Try
            Dim iRow As DataRow() = Nothing

            'If mdtTranDetail IsNot Nothing Then
            '    mdtTran = mdtTranDetail.Copy()
            'End If

            If mdtTranDetail Is Nothing Then Exit Sub


            If CInt(Me.ViewState("EditCrtranunkid")) > 0 Then
                If popup_DeleteReason.Reason.Trim.Length > 0 Then 'Nilay (01-Feb-2015) -- txtreasondel.Text.Trim.Length > 0
                    iRow = mdtTranDetail.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")

                    If iRow IsNot Nothing AndAlso iRow.Length > 0 Then
                        If CInt(iRow(0)("quantity")) > 0 Then
                            Dim dtmp As DataRow() = Nothing
                            Select Case CInt(cboExpCategory.SelectedValue)
                                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                    If mdtTranDetail.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("dpndtbeneficetranunkid")) <> 0).Count() > 0 Then
                                        dtmp = mdtTranDetail.Select("dpndtbeneficetranunkid = '" & CInt(iRow(0)("dpndtbeneficetranunkid")) & "' AND crtranunkid <> '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                                    ElseIf mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("other_person").ToString().Trim().Length > 0).Count() > 0 Then
                                        dtmp = mdtTranDetail.Select("other_person = '" & iRow(0)("other_person").ToString().Replace("'", "''") & "' AND crtranunkid <> '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                                    End If
                                    If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                                        UpdateRebateInformation(iRow(0), dtmp(0))
                                    End If
                            End Select
                        End If
                    End If

                    iRow(0).Item("isvoid") = True
                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text
                    iRow(0).Item("AUD") = "D"
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        iRow(0).Item("voiduserunkid") = Session("UserId")
                        iRow(0).Item("voidloginemployeeunkid") = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        iRow(0).Item("voiduserunkid") = -1
                        iRow(0).Item("voidloginemployeeunkid") = Session("Employeeunkid")
                    End If
                    iRow(0).AcceptChanges()

                    Dim dRow() As DataRow = Nothing
                    If CInt(Me.ViewState("EditCrtranunkid")) > 0 Then
                        dRow = mdtFinalAttchment.Select("transactionunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                    End If
                    If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                        For Each dtRow As DataRow In dRow
                            dtRow.Item("AUD") = "D"
                        Next
                    End If
                    mdtFinalAttchment.AcceptChanges()
                    'mdtTranDetail = mdtTran.Copy
                    Me.ViewState("EditCrtranunkid") = -1
                    Fill_Expense()
                    Clear_Controls()
                    btnEdit.Visible = False : btnAdd.Visible = True
                    cboExpense.Enabled = True
                    iRow = Nothing
                Else
                    popup_DeleteReason.Show() 'Nilay (01-Feb-2015) --  popup1.Show()
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Expense Delete process fail.", Me)
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing
        End Try
    End Sub

    Protected Sub btnScanAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanAttchment.Click
        Try
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            If dgvData.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please add atleast one expense in order to save."), Me)
                dgvData.Focus()
                Exit Sub
            End If

            'If mdtFinalAttchment Is Nothing Then
            '    Dim objAttchement As New clsScan_Attach_Documents
            '    mdtAttchment = objAttchement.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.CLAIM_REQUEST, mintClaimRequestMasterId, CStr(Session("Document_Path"))).Copy
            '    'Pinkal (05-Sep-2020) -- Start
            '    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            '    objAttchement = Nothing
            '    'Pinkal (05-Sep-2020) -- End
            'Else
            '    mdtAttchment = mdtFinalAttchment.Copy
            'End If
            If mdtFinalAttchment Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                mdtAttchment = objAttchement.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.CLAIM_REQUEST, mintClaimRequestMasterId, CStr(Session("Document_Path"))).Copy
                objAttchement = Nothing
                'Pinkal (05-Sep-2020) -- End
            Else
                mdtAttchment = mdtFinalAttchment.Copy
            End If

            Call FillAttachment()
            blnShowAttchmentPopup = True
            popup_ScanAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Is_Valid() = False Then Exit Sub
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtAttchment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtAttchment.AcceptChanges()
                mintDeleteIndex = 0
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then
                mintDeleteIndex = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            If mdtAttchment Is Nothing Then Exit Sub
            mdtAttchment.Select("").ToList().ForEach(Function(x) Add_DataRow(x, mstrModuleName))
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing

            If CInt(dgvData.Items(mintExpenseIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                xRow = mdtFinalAttchment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvData.Items(mintExpenseIndex).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvData.Items(mintExpenseIndex).Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text).Trim.Length > 0 Then
                xRow = mdtFinalAttchment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvData.Items(mintExpenseIndex).Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If

            If xRow.Length <= 0 Then
                mdtFinalAttchment.ImportRow(dr)
            Else
                mdtFinalAttchment.Rows.Remove(xRow(0))
                mdtFinalAttchment.ImportRow(dr)
            End If


            mdtFinalAttchment.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If txtExpRemark.Text.Trim.Length <= 0 Then
                popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Do you wish to continue?")
                popup_ExpRemarkYesNo.Show()
            Else
                If mblnIsEditClick Then
                    EditExpense()
                Else
                    AddExpense()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsEditClick Then
                EditExpense()
            Else
                AddExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtAttchment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
        Catch tex As ThreadAbortException
            'Pinkal (27-Sep-2024) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |29-MAR-2022| -- START
    'ISSUE/ENHANCEMENT : OLD-580-V2
    Protected Sub popup_RebateCnf_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_RebateCnf.buttonYes_Click
        Try
            SaveData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |29-MAR-2022| -- END

#End Region

#Region "Links Events"

    Protected Sub LnkViewDependants_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewDependants.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, True, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, True, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
                'SHANI (06 JUN 2015) -- Start
                'Enhancement : Changes in C & R module given by Mr.Andrew.
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
                'SHANI (06 JUN 2015) -- End 
            Else
                Exit Sub
            End If
            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()
            popupEmpDepedents.Show()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"


    Protected Sub dgvData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.DeleteCommand
        '    Dim mdtTran As DataTable = Nothing
        Try
            Dim iRow As DataRow() = Nothing

            'If mdtTranDetail IsNot Nothing Then
            '    mdtTran = mdtTranDetail.Copy()
            'End If

            If mdtTranDetail Is Nothing Then Exit Sub

            If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                iRow = mdtTranDetail.Select("crtranunkid = '" & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    Me.ViewState("EditCrtranunkid") = iRow(0).Item("crtranunkid")
                    popup_DeleteReason.Reason = ""
                    popup_DeleteReason.Show()
                End If
            ElseIf CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) <> "" Then
                iRow = mdtTranDetail.Select("GUID = '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")

                If iRow IsNot Nothing AndAlso iRow.Length > 0 Then
                    If CInt(iRow(0)("quantity")) > 0 Then
                        Dim dtmp As DataRow() = Nothing
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                If mdtTranDetail.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("dpndtbeneficetranunkid")) <> 0).Count() > 0 Then
                                    dtmp = mdtTranDetail.Select("dpndtbeneficetranunkid = '" & CInt(iRow(0)("dpndtbeneficetranunkid")) & "' AND GUID <> '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")
                                ElseIf mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("other_person").ToString().Trim().Length > 0).Count() > 0 Then
                                    dtmp = mdtTranDetail.Select("other_person = '" & iRow(0)("other_person").ToString().Replace("'", "''") & "' AND GUID <> '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")
                                End If
                                If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                                    UpdateRebateInformation(iRow(0), dtmp(0))
                                End If
                        End Select
                    End If
                End If
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                End If

            End If
            iRow(0).AcceptChanges()

            Dim dRow() As DataRow = Nothing
            If CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) <> "" Then
                dRow = mdtFinalAttchment.Select("GUID = '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")
            End If
            If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                For Each dtRow As DataRow In dRow
                    dtRow.Item("AUD") = "D"
                Next
            End If
            mdtFinalAttchment.AcceptChanges()
            'mdtTranDetail = mdtTran.Copy()
            Fill_Expense()
            Clear_Controls()
            btnEdit.Visible = False : btnAdd.Visible = True
            cboExpense.Enabled = True
            iRow = Nothing

            If dgvData.Items.Count <= 0 Then
                If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                    Select Case CInt(cboExpCategory.SelectedValue)
                        Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                            If cboRebatePercent.Enabled = False Then cboRebatePercent.Enabled = True
                            If cboDependant.Enabled = False Then cboDependant.Enabled = True
                    End Select
                End If
                If txtOthers.Enabled = False Then txtOthers.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        'Dim mdtTran As DataTable = Nothing
        Try
            If e.CommandName = "Edit" Then

                'If mdtTranDetail IsNot Nothing Then
                '    mdtTran = mdtTranDetail.Copy()
                'End If

                If mdtTranDetail Is Nothing Then Exit Sub


                Dim iRow As DataRow() = Nothing

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                    iRow = mdtTranDetail.Select("crtranunkid = '" & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then
                    iRow = mdtTranDetail.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboExpense.SelectedValue = iRow(0).Item("expenseunkid").ToString
                    Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

                    cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid").ToString()
                    Call dtpDate_TextChanged(dtpDate, New EventArgs())

                    txtQty.Text = CDec(iRow(0).Item("quantity")).ToString
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString())).ToString()
                    txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboExpense.Enabled = False

                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    cboCurrency.SelectedValue = iRow(0).Item("countryunkid").ToString()
                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                        Me.ViewState("EditCrtranunkid") = CStr(iRow(0).Item("crtranunkid"))
                    ElseIf e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then
                        Me.ViewState("EditGuid") = CStr(iRow(0).Item("GUID"))
                    End If
                    mintClaimRequestExpenseTranId = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text)
                    mstrClaimRequestExpenseGUID = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString()
                    txtAirline.Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Text.ToString()
                    cboTravelFrom.SelectedValue = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhfromrouteid", False, True)).Text)
                    cboTravelTo.SelectedValue = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhtorouteid", False, True)).Text)
                    txtFlightNo.Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Text.ToString()
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.ToString().Trim.Length > 0 AndAlso _
                       e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.ToString() <> "&nbsp;" Then
                        dtTravelDate.SetDate = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.ToString()
                    End If
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhother_person", False, True)).Text.Trim.Length > 0 AndAlso _
                       e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhother_person", False, True)).Text.Trim <> "&nbsp;" Then
                        txtOthers.Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhother_person", False, True)).Text.ToString()
                    End If
                    cboDependant.SelectedValue = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhdpndtbeneficetranunkid", False, True)).Text.ToString()

                End If

            ElseIf e.CommandName = "attachment" Then
                mdtAttchment = mdtFinalAttchment.Clone

                Dim xRow() As DataRow = Nothing
                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                    xRow = mdtFinalAttchment.Select("transactionunkid = '" & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then
                    xRow = mdtFinalAttchment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                End If

                If xRow.Count > 0 Then
                    mdtAttchment = xRow.CopyToDataTable
                End If

                mintExpenseIndex = e.Item.ItemIndex

                Call FillAttachment()
                cboScanDcoumentType.SelectedIndex = 0
                blnShowAttchmentPopup = True
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'If mdtTran IsNot Nothing Then mdtTran.Clear()
            'mdtTran = Nothing
        End Try
    End Sub

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Text.Trim.Length > 0 Then  'Unit Price  
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text.Trim.Length > 0 Then  'Amount 
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())
                End If


                If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Text.Trim.Length > 0 Then  'Base Amount 
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Text), Session("fmtCurrency").ToString())
                    End If
                End If

                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = True Then
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text = CDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text).ToShortDateString()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.CommandName = "Delete" Then
                    popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                    mintDeleteIndex = e.Item.ItemIndex
                    popup_YesNo.Show()

                ElseIf e.CommandName = "Download" Then
                    Dim xrow() As DataRow = Nothing

                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                        xrow = mdtAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                    Else
                        xrow = mdtAttchment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "'")
                    End If

                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox Event(s)"

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Dim dsCombo As New DataSet
        Dim objExpMaster As New clsExpense_Master
        Dim objLeaveType As New clsleavetype_master
        Dim mdtLeaveType As DataTable = Nothing
        Try

            If CType(sender, DropDownList).ID.ToUpper() = "CBOEXPCATEGORY" Then
                SetFormControls()
            End If


            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                    LnkViewDependants.Visible = False
                Case Else
                    LnkViewDependants.Visible = True
            End Select

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    cboCurrency.SelectedValue = 0
                    cboCurrency.Enabled = False

                    txtQty.Enabled = False
                    txtUnitPrice.Enabled = False
                Case Else
                    cboCurrency.Enabled = True

                    txtQty.Enabled = True
                    txtUnitPrice.Enabled = True
            End Select

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse _
               CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                cboSeatStatus.SelectedValue = enRebateSeat.RB_SA
                cboSeatStatus.Enabled = False
                cboRebatePercent.SelectedValue = enRebatePercent.RB_100
                cboRebatePercent.Enabled = True
                cboDependant.Enabled = True

            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_DUTY Then
                cboSeatStatus.SelectedValue = enRebateSeat.RB_FIRM
                cboSeatStatus.Enabled = False
                cboDependant.Enabled = False
                cboRebatePercent.SelectedValue = enRebatePercent.RB_100
                cboRebatePercent.Enabled = False
            End If


            If CInt(cboExpCategory.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, "ISNULL(cr_expinvisible,0) = 0 AND ISNULL(cmexpense_master.isshowoness,0) = 1")
                Else
                    dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, "ISNULL(cr_expinvisible,0) = 0")
                End If
            Else
                dsCombo = New DataSet()
                dsCombo.Tables.Add("Expense")
                dsCombo.Tables(0).Columns.Add("Id", Type.GetType("System.Int32"))
                dsCombo.Tables(0).Columns.Add("Name", Type.GetType("System.String"))

                Dim drRow As DataRow = dsCombo.Tables(0).NewRow()
                drRow("Id") = 0
                drRow("Name") = "Select"
                dsCombo.Tables(0).Rows.Add(drRow)
            End If


            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            If cboExpense.SelectedValue IsNot Nothing AndAlso cboExpense.SelectedValue <> "" Then
                Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
            End If


            'If CBool(Session("SectorRouteAssignToEmp")) Then
            '    Dim objAssignEmp As New clsassignemp_sector
            '    Dim dtSector As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
            '    With cboSectorRoute
            '        .DataValueField = "secrouteunkid"
            '        .DataTextField = "Sector"
            '        .DataSource = dtSector
            '        .DataBind()
            '    End With

            '    If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
            '        With cboTravelFrom
            '            .DataValueField = "secrouteunkid"
            '            .DataTextField = "Sector"
            '            .DataSource = dtSector.Copy
            '            .DataBind()
            '            .SelectedIndex = 0
            '        End With
            '        With cboTravelTo
            '            .DataValueField = "secrouteunkid"
            '            .DataTextField = "Sector"
            '            .DataSource = dtSector.Copy
            '            .DataBind()
            '            .SelectedIndex = 0
            '        End With
            '    End If
            'End If


            If CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE
                        objlblValue.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Leave Form")

                        If mintClaimRequestMasterId > 0 Then
                            cboLeaveType.Enabled = False
                        Else
                            cboLeaveType.Enabled = True
                            If cboReference.DataSource IsNot Nothing Then cboReference.SelectedIndex = 0
                            cboReference.Enabled = True
                        End If


                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", False)
                        End If

                        If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(Me.ViewState("mintLeaveTypeId")), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = Me.ViewState("mintLeaveTypeId").ToString()
                        End With
                        objLeaveType = Nothing

                    Case enExpenseType.EXP_MEDICAL
                        If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(Me.ViewState("mintLeaveTypeId")), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "Id"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = Me.ViewState("mintLeaveTypeId").ToString
                        End With
                        Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())

                        cboLeaveType.Enabled = False
                        objlblValue.Text = ""
                        cboReference.SelectedIndex = 0
                        cboReference.Enabled = False

                    Case enExpenseType.EXP_TRAINING
                        If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(Me.ViewState("mintLeaveTypeId")), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "Id"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = Me.ViewState("mintLeaveTypeId").ToString()
                        End With
                        Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())

                        cboLeaveType.SelectedIndex = 0
                        cboLeaveType.Enabled = False
                        objlblValue.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Training")
                        cboReference.DataSource = Nothing
                        dsCombo = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(CInt(cboEmployee.SelectedValue), True)
                        With cboReference
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With

                    Case enExpenseType.EXP_MISCELLANEOUS
                        cboLeaveType.SelectedValue = "0"
                        cboReference.SelectedValue = "0"
                        cboLeaveType.Enabled = False
                        cboReference.Enabled = False
                        objlblValue.Text = ""

                End Select

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If

            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing : cboReference.Enabled = False : cboLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If

            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 AndAlso Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                dsCombo = New DataSet : Dim objDependant As New clsDependants_Beneficiary_tran
                dsCombo = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date, blnFlag:=True)
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE Then
                    Dim dtView As DataTable
                    dtView = New DataView(dsCombo.Tables(0), "dependent_Id <=0 ", "dependent_Id", DataViewRowState.CurrentRows).ToTable().Copy()
                    dsCombo.Tables.RemoveAt(0) : dsCombo.Tables.Add(dtView.Copy())
                    Dim sRow As DataRow = dsCombo.Tables(0).NewRow
                    sRow("dependent_id") = -999
                    sRow("dependants") = Language.getMessage("clsclaim_request_tran", 102, "Self")
                    sRow("age") = 0
                    sRow("Months") = 0
                    sRow("relation") = ""
                    sRow("gender") = ""
                    dsCombo.Tables(0).Rows.InsertAt(sRow, 1)
                    With cboDependant
                        .DataValueField = "dependent_Id"
                        .DataTextField = "dependants"
                        .DataSource = dsCombo.Tables(0)
                        Try
                            .SelectedIndex = mintSelectedPassenger
                        Catch ex As Exception
                            .SelectedValue = 0
                        End Try
                        .DataBind()
                    End With
                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                    With cboDependant
                        .DataValueField = "dependent_Id"
                        .DataTextField = "dependants"
                        .DataSource = dsCombo.Tables(0)
                        Try
                            .SelectedIndex = mintSelectedPassenger
                        Catch ex As Exception
                            .SelectedValue = 0
                        End Try
                        .DataBind()
                    End With
                Else
                    cboDependant.Enabled = False : cboDependant.DataSource = Nothing
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            If mdtLeaveType IsNot Nothing Then mdtLeaveType.Clear()
            mdtLeaveType = Nothing
            objExpMaster = Nothing
            objLeaveType = Nothing
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Dim objExpMaster As New clsExpense_Master
        Try


            If CBool(Session("SectorRouteAssignToExpense")) Then

                Dim dtSector As DataTable = Nothing

                If CInt(cboExpense.SelectedValue) > 0 Then
                    Dim objAssignExpense As New clsassignexpense_sector
                    dtSector = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                    objAssignExpense = Nothing
                Else
                    dtSector = New DataTable()
                    dtSector.Columns.Add("secrouteunkid", Type.GetType("System.Int32"))
                    dtSector.Columns.Add("Sector", Type.GetType("System.String"))
                    Dim drRow As DataRow = dtSector.NewRow()
                    drRow("secrouteunkid") = 0
                    drRow("Sector") = "Select"
                    dtSector.Rows.Add(drRow)
                End If

                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                    .SelectedIndex = 0
                End With

                If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                    With cboTravelFrom
                        .DataValueField = "secrouteunkid"
                        .DataTextField = "Sector"
                        .DataSource = dtSector.Copy
                        .DataBind()
                        .SelectedIndex = 0
                    End With
                    With cboTravelTo
                        .DataValueField = "secrouteunkid"
                        .DataTextField = "Sector"
                        .DataSource = dtSector.Copy
                        .DataBind()
                        .SelectedIndex = 0
                    End With
                End If

                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing


            End If


            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment
                mblnIsSecRouteMandatory = objExpMaster._IsSecRouteMandatory
                mblnIsConsiderDependants = objExpMaster._IsConsiderDependants
                mblnDoNotAllowToApplyForBackDate = objExpMaster._DoNotAllowToApplyForBackDate
                mblnIsHRExpense = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
                mintGLCodeId = objExpMaster._GLCodeId
                mstrDescription = objExpMaster._Description
                mintExpenditureTypeId = objExpMaster._ExpenditureTypeId
                mblnIsaccrue = objExpMaster._Isaccrue
                mintUomunkid = objExpMaster._Uomunkid
                mintAccrueSettingId = objExpMaster._AccrueSetting
                mintExpense_MaxQuantity = objExpMaster._Expense_MaxQuantity
                mblnIsAttachDocMandetory = objExpMaster._IsAttachDocMandetory
                mblnIsUnitPriceEditable = objExpMaster._IsUnitPriceEditable

                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        cboSectorRoute.Enabled = False
                    Case Else
                        cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                End Select


                cboSectorRoute.SelectedValue = "0"


                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    Case Else
                        cboCostCenter.SelectedValue = "0"
                End Select

                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If

                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    txtUnitPrice.Text = "1.00"
                    txtBalance.Text = "0.00"
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet

                    Dim intExpenseId As Integer = 0
                    If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                        If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                    Else
                        intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                    End If

                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), intExpenseId, CInt(Session("Fin_Year")), dtpDate.GetDate.Date)

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing


                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing
                    Dim blnApplyFilter As Boolean = True

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        blnApplyFilter = False
                    End If

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                        dsList = objLeave.GetList("List", _
                                                  Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                 False, True, blnApplyFilter, False, CInt(cboEmployee.SelectedValue), False, False, False, "", Nothing)

                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                        dsList = objLeave.GetList("List", _
                                                  Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                  False, True, blnApplyFilter, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing)

                    End If

                    dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        txtBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then

                            If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = CDate(Session("fin_enddate")).Date
                            If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            Else
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            End If

                        ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then

                            Dim mdtDate As Date = dtpDate.GetDate.Date
                            Dim mdtDays As Integer = 0

                            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                                If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                                    mdtDate = CDate(Session("fin_enddate")).Date
                                    mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                                Else
                                    If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                        mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                                    End If
                                End If

                            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                                If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                End If
                                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                            End If

                            Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                            If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                                intDiff = intDiff - 1
                            End If

                            txtBalanceAsOnDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        End If

                        txtUoMType.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 6, "Quantity")
                    End If

                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objLeave = Nothing

                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    txtUnitPrice.Text = "1.00"
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet

                    Dim intExpenseId As Integer = 0
                    If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                        If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                    Else
                        intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                    End If

                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), intExpenseId, CInt(Session("Fin_Year")), dtpDate.GetDate.Date)

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = Math.Round(Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")), 6).ToString()
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    objEmpExpBal = Nothing
                End If
            Else

                mblnIsLeaveEncashment = False
                mblnIsSecRouteMandatory = False
                mblnIsConsiderDependants = False
                mblnDoNotAllowToApplyForBackDate = False
                mblnIsHRExpense = False
                mblnIsClaimFormBudgetMandatory = False
                mblnIsaccrue = False
                mblnIsAttachDocMandetory = False
                mintGLCodeId = 0
                mstrDescription = ""
                mintExpenditureTypeId = 0
                mintUomunkid = 0
                mintAccrueSettingId = 0
                mintExpense_MaxQuantity = 0
                mblnIsUnitPriceEditable = True


                txtUoMType.Text = ""
                txtUnitPrice.Enabled = True
                txtUnitPrice.Text = "1.00"
                txtBalance.Text = "0.00"
                txtBalanceAsOnDate.Text = "0.00"
                cboCostCenter.Enabled = False
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    Case Else
                        cboCostCenter.SelectedValue = "0"
                End Select
                txtQty.Text = "1"
            End If

            If objExpMaster._IsConsiderDependants Then
                txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
            Else
                txtQty.Text = "1"
            End If

            If objExpMaster._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True

            'txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            txtUnitPrice.Enabled = mblnIsUnitPriceEditable

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                pnlBalAsonDate.Visible = True

                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    Case Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                End Select
            Else
                If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                    pnlBalAsonDate.Visible = False
                Else
                    pnlBalAsonDate.Visible = True
                End If

                If mdtTranDetail Is Nothing Then Exit Sub

                If mdtTranDetail IsNot Nothing Then
                    If mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            cboCurrency.Enabled = True
                        Else
                            cboCurrency.Enabled = False
                            cboCurrency.SelectedValue = CStr(mdtTranDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                        End If
                    Else
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                            Case Else
                                cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                                cboCurrency.Enabled = True
                        End Select
                    End If
                End If

                'If mdtTran IsNot Nothing Then mdtTran.Clear()
                'mdtTran = Nothing

            End If

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    txtQty.Enabled = False
                    txtUnitPrice.Enabled = False

                    If cboDependant.Items.Count > 0 Then
                        Try
                            cboDependant.SelectedValue = mintSelectedPassenger
                        Catch ex As Exception
                            cboDependant.SelectedValue = 0
                        End Try
                    End If

            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objExpMaster = Nothing
        End Try
    End Sub

    Protected Sub cboLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        Dim objleave As New clsleaveform
        Dim dsLeave As New DataSet
        Dim mdtLeaveForm As DataTable = Nothing
        Try
            If CInt(IIf(cboLeaveType.SelectedValue = "", 0, cboLeaveType.SelectedValue)) = 0 Then
                dsLeave = objleave.GetLeaveFormForExpense(0, "7", CInt(cboEmployee.SelectedValue), True, True, CInt(Me.ViewState("mintLeaveFormID")))
            Else
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "7", CInt(cboEmployee.SelectedValue), True, True, CInt(Me.ViewState("mintLeaveFormID")))
            End If

            If CInt(Session("mintLeaveFormID")) > 0 Then
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & CInt(Session("mintLeaveFormID")), "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = mdtLeaveForm.Copy
                .DataBind()
                If CInt(Me.ViewState("mintLeaveFormID")) < 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = Me.ViewState("mintLeaveFormID").ToString
                End If
            End With
            objleave = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            If mdtLeaveForm IsNot Nothing Then mdtLeaveForm.Clear()
            mdtLeaveForm = Nothing
            objleave = Nothing
        End Try
    End Sub

    Protected Sub cboDependant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDependant.SelectedIndexChanged
        Try
            mintSelectedPassenger = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DatePicker Event(s)"

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, cboSectorRoute.SelectedIndexChanged
        Try
            If CDate(Me.ViewState("MinDate")).Date <= dtpDate.GetDate.Date And CDate(Me.ViewState("MaxDate")).Date >= dtpDate.GetDate.Date Then
                Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
                Dim objCosting As New clsExpenseCosting

                If cboSectorRoute.SelectedValue <> "" AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    objCosting.GetDefaultCosting(CInt(IIf(cboSectorRoute.SelectedValue = "", "0", cboSectorRoute.SelectedValue)), iCostingId, iAmount, dtpDate.GetDate)
                End If

                txtCosting.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString)
                txtCostingTag.Value = iCostingId.ToString()

                If iAmount > 0 Then
                    txtUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString)
                Else
                    txtUnitPrice.Text = "1.00"
                End If


                'Dim objExpnese As New clsExpense_Master
                'objExpnese._Expenseunkid = CInt(IIf(cboExpense.SelectedValue = "", 0, cboExpense.SelectedValue))

                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    txtQty.Text = "1"

                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                    txtQty.Text = "1"
                End If

                If (sender.GetType().FullName = "System.Web.UI.WebControls.TextBox" AndAlso CType(sender, System.Web.UI.WebControls.TextBox).Parent.ID.ToUpper() = dtpDate.ID.ToUpper()) AndAlso CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboExpense.SelectedValue) > 0 Then
                    cboExpense_SelectedIndexChanged(New Object(), New EventArgs())
                End If

                objCosting = Nothing

                'If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                '    txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                '    mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
                'End If

                'If objExpnese._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
                'txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable

                'objExpnese = Nothing

                If mblnIsConsiderDependants AndAlso mblnIsSecRouteMandatory Then
                    txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                    mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
                End If

                If mblnIsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
                txtUnitPrice.Enabled = mblnIsUnitPriceEditable

                'objExpnese = Nothing

            Else
                dtpDate.SetDate = CDate(Me.ViewState("MinDate"))
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Sorry, Date should be between ") & CDate(Me.ViewState("MinDate")).Date & " and " & CDate(Me.ViewState("MaxDate")).Date & ".", Me)
            End If

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    txtQty.Enabled = False
                    txtUnitPrice.Enabled = False
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Try
            If txtQty.Text.Trim.Length > 0 Then
                If IsNumeric(txtQty.Text) = False Then
                    txtQty.Text = "1"
                    Exit Sub
                End If

                If CInt(txtQty.Text) > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If mblnIsLeaveEncashment = False Then
                        txtUnitPrice.Enabled = True
                        If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                            txtUnitPrice.Text = CStr(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString()))
                            txtUnitPrice.Focus()
                        End If
                    Else
                        txtUnitPrice.Enabled = False
                    End If
                ElseIf CInt(txtQty.Text) <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                        txtUnitPrice.Text = CStr(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString()))
                    End If
                End If
            Else
                txtUnitPrice.Enabled = False
            End If

            If mblnIsUnitPriceEditable = False Then txtUnitPrice.Enabled = mblnIsUnitPriceEditable

            'Dim objExpense As New clsExpense_Master
            'objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            'If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            'objExpense = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub txtUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.TextChanged
        Try
            If IsNumeric(txtUnitPrice.Text) = False Then
                txtUnitPrice.Text = "1.00"
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region


    Private Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbExpenseInformation", Me.lblDetialHeader.Text).Replace("&&", "&")
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblQty.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.lblSector.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSector.ID, Me.lblSector.Text)
            Me.LnkViewDependants.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LnkViewDependants.ID, Me.LnkViewDependants.ToolTip)
            Me.tbExpenseRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.tbExpenseRemark.ID, Me.tbExpenseRemark.Text)
            Me.tbClaimRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.tbClaimRemark.ID, Me.tbClaimRemark.Text)
            Me.LblDomicileAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)
            Me.lblBalanceasondate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalanceasondate.ID, Me.lblBalanceasondate.Text)
            Me.lblCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Me.btnAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)
            Me.dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)
            Me.dgvData.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(8).FooterText, Me.dgvData.Columns(8).HeaderText)

            btnScanAttchment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnScanAttchment.ID, Me.btnScanAttchment.Text).Replace("&", "")
            btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnScanAttchment.ID, Me.btnScanAttchment.Text).Replace("&", "")


            Me.LblEmpDependentsList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.LblEmpDependentsList.Text)
            Me.dgDepedent.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(0).FooterText, Me.dgDepedent.Columns(0).HeaderText)
            Me.dgDepedent.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(1).FooterText, Me.dgDepedent.Columns(1).HeaderText)
            Me.dgDepedent.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(2).FooterText, Me.dgDepedent.Columns(2).HeaderText)
            Me.dgDepedent.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(3).FooterText, Me.dgDepedent.Columns(3).HeaderText)
            Me.dgDepedent.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(4).FooterText, Me.dgDepedent.Columns(4).HeaderText)
            Me.btnEmppnlEmpDepedentsClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnEmppnlEmpDepedentsClose.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Expense is mandatory information. Please select Expense to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Quantity is mandatory information. Please enter Quantity to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Please add atleast one expense in order to save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Claim Request saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Leave Form")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Training")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Please Map this employee's Leave Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Please Map this Leave type to this employee's Leave Approver(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "has mandatory document attachment. please attach document.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Configuration Path does not Exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 32, "Sorry, Date should be between")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 45, "You have not set your expense remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 46, "Currency is mandatory information. Please select Currency to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 47, "Claim Remark is mandatory information. Please enter claim remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 51, "Sorry, you cannot apply for this claim application now as your previous claim application has not been approved for this expense category yet.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 52, "Sorry, you cannot apply for this claim application now as your previous claim application has not been retire for this expense category yet.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 53, "There is no Budget for this request.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsclaim_request_master", 4, " ] time(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsExpCommonMethods", 6, "Quantity")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
