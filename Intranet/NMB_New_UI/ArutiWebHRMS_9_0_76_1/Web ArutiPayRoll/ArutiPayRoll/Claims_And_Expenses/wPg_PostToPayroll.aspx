﻿<%@ Page Title="Post To Payroll" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_PostToPayroll.aspx.vb" Inherits="wPg_PostToPayroll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkselect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkselect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkselect]", grid).length == $("[id*=chkselect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
              
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Post To Payroll"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblViewBy" runat="server" Text="View Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboViewBy" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExpCategory" runat="server" Text="Exp. Cat." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpCategory" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpense" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblTranHead" runat="server" Text="Tran. Head" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTranhead" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblUOM" runat="server" Text="UOM" CssClass="form-label"> </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboUOM" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30">
                                        <asp:RadioButtonList ID="radOperation" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Text="Qty. Mapping" Selected="True" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Amount Mapping" Selected="False" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnUnposting" runat="server" Text="UnPost" CssClass="btn btn-primary" />
                                <asp:Button ID="btnPosting" runat="server" Text="Post" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:GridView ID="dgvClaimPosting" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                AllowPaging="false" ShowFooter="false" DataKeyNames="crprocesstranunkid,Isgroup,crmasterunkid,qtytranheadunkid,amttranheadunkid,employeeunkid">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="25">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" Enabled="true" CssClass="filled-in"
                                                                Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkselect" runat="server" Enabled="true" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Claim" HeaderText="Particulars" FooterText="dgcolhParticulars" />
                                                    <asp:BoundField DataField="Expense" HeaderText="Expenses" FooterText="dgcolhExpense" />
                                                    <asp:BoundField DataField="Sector" HeaderText="Sector" FooterText="dgcolhSector" />
                                                    <asp:BoundField DataField="currency_sign" HeaderText="Currency" FooterText="dgcolhCurrency" />
                                                    <asp:BoundField DataField="unitprice" HeaderText="Unit Price" FooterText="dgcolhUnitprice"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="quantity" HeaderText="Quantity" FooterText="dgcolhQuantity"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                        
                                                      <asp:BoundField DataField="base_amount" HeaderText="Base Amount" FooterText="dgcolhBaseAmount"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                        
                                                    <asp:BoundField DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                    
                                                    <asp:BoundField DataField="Qtytrnhead" HeaderText="Qty Transaction Head" FooterText="colhQtytrnhead" />
                                                    <asp:BoundField DataField="Amttrnhead" HeaderText="Amt Transaction Head" FooterText="colhAmttrnhead" />
                                                    <asp:BoundField DataField="ischange" HeaderText="ischange" FooterText="objcolhischange"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--  <div class="panel-primary">
                        <div class="panel-heading">
                          
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                       
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                              
                                            </td>
                                            <td style="width: 23%">
                                                
                                            </td>
                                            <td style="width: 10%">
                                               
                                            </td>
                                            <td style="width: 23%">
                                               
                                            </td>
                                            <td style="width: 11%">
                                               
                                            </td>
                                            <td style="width: 23%">
                                              
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                               
                                            </td>
                                            <td style="width: 23%">
                                                
                                            </td>
                                            <td style="width: 10%">
                                               
                                            </td>
                                            <td style="width: 23%">
                                             
                                            </td>
                                            <td style="width: 11%">
                                               
                                            </td>
                                            <td style="width: 23%">
                                              
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                               
                                            </td>
                                            <td style="width: 23%">
                                               
                                            </td>
                                            <td style="width: 10%">
                                                
                                            </td>
                                            <td style="width: 23%">
                                              
                                            </td>
                                            <td style="width: 34%" colspan="2">
                                              
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <%--<div id="Div2" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="Label2" runat="server" Text="Filter Criteria"></asp:Label>
                                                </div>
                                            </div
                <div id="Div3" class="panel-body-default" style="position: relative">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_dgvdata" runat="server" Height="350px" ScrollBars="Auto" Width="100%">
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <div id="btnfixedbottom" class="btn-default">
                    </div>
                </div>
                </div> </div> </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
