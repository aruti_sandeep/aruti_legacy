﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading

#End Region
Partial Class Claims_And_Expenses_wPg_ClaimAndRequestList
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimReqestMaster As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Private objApproverTran As New clsclaim_request_approval_tran
    'Pinkal (05-Sep-2020) -- End

    Private Shared ReadOnly mstrModuleName As String = "frmClaims_RequestList"
    Private Shared ReadOnly mstrModuleName1 As String = "frmCancelExpenseForm"

    'Pinkal (04-Feb-2020) -- Start
    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
    Private objCONN As SqlConnection
    Private mintCrMasterId As Integer = 0
    'Pinkal (04-Feb-2020) -- End

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    Private mintCancelExpEmployeeId As Integer = 0
    'Pinkal (05-Sep-2020) -- End

    Private mdtTranDetail As DataTable = Nothing

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    GC.Collect()
                    'Pinkal (05-Sep-2020) -- End

                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If


                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 4 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        Me.ViewState.Add("ClaimFormEmpId", CInt(arr(1)))
                        Me.ViewState.Add("crmasterunkid", CInt(arr(2)))
                        Me.ViewState.Add("ExpenseCategoryID", CInt(arr(3)))
                        mintCrMasterId = CInt(arr(2))

                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If

                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Me.ViewState("ClaimFormEmpId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub
                            Else

                                If mblnATLoginRequiredToApproveApplications = False Then

                                    Dim strError As String = ""
                                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                                        Exit Sub
                                    End If

                                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                                    gobjConfigOptions = New clsConfigOptions
                                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                                    ArtLic._Object = New ArutiLic(False)
                                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                                        Dim objGroupMaster As New clsGroup_Master
                                        objGroupMaster._Groupunkid = 1
                                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                                    End If

                                    If ConfigParameter._Object._IsArutiDemo Then
                                        If ConfigParameter._Object._IsExpire Then
                                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                            Exit Try
                                        Else
                                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                                Exit Try
                                            End If
                                        End If
                                    End If

                                    Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                                    Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                                    Session("PaymentApprovalwithLeaveApproval") = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
                                    Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                                    If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                                    Else
                                        Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                                    End If

                                    Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim

                                    Try
                                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                                        Else
                                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                                        End If

                                    Catch ex As Exception
                                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                                    End Try

                                    Dim base As New Basepage
                                    If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(arr(1))) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page)
                                        Exit Try
                                    End If

                                    Call GetDatabaseVersion()

                                    Dim objEmployee As New clsEmployee_Master
                                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(arr(1))

                                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                                    HttpContext.Current.Session("UserId") = -1
                                    HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
                                    HttpContext.Current.Session("UserName") = "ID " & " : " & objEmployee._Employeecode & vbCrLf & "Employee : " & objEmployee._Firstname & " " & objEmployee._Surname 'objEmp._Displayname
                                    HttpContext.Current.Session("Password") = objEmployee._Password
                                    HttpContext.Current.Session("LeaveBalances") = 0
                                    HttpContext.Current.Session("MemberName") = "Emp. : (" & objEmployee._Employeecode & ") " & objEmployee._Firstname & " " & objEmployee._Surname
                                    HttpContext.Current.Session("RoleID") = 0
                                    HttpContext.Current.Session("LangId") = 1
                                    HttpContext.Current.Session("Firstname") = objEmployee._Firstname
                                    HttpContext.Current.Session("Surname") = objEmployee._Surname
                                    HttpContext.Current.Session("DisplayName") = objEmployee._Displayname
                                    HttpContext.Current.Session("Theme_id") = objEmployee._Theme_Id
                                    HttpContext.Current.Session("Lastview_id") = objEmployee._LastView_Id

                                    objEmployee = Nothing

                                    strError = ""
                                    If base.SetUserSessions(strError) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "Index.aspx")
                                        Exit Try
                                    End If

                                    strError = ""
                                    If base.SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "Index.aspx")
                                        Exit Try
                                    End If

                                End If  '   If mblnATLoginRequiredToApproveApplications = False Then

                            End If  ' If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Me.ViewState("ClaimFormEmpId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub
                        End If  ' If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                    End If  '      If arr.Length = 4 Then

                End If   ' If Request.QueryString.Count > 0 Then

            End If  ' If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then


            'Pinkal (23-Feb-2024) -- End


            If IsPostBack = False Then

                SetLanguage()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                SetDateFormat()
                'Pinkal (27-Aug-2020) -- End

                FillCombo()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                'objClaimReqestMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
                'Pinkal (05-Sep-2020) -- End

                Me.ViewState.Add("crmasterunkid", 0)
                Me.ViewState.Add("FirstRecordNo", 0)
                Me.ViewState.Add("LastRecordNo", 0)

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    lvClaimRequestList.Columns(2).Visible = True
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    lvClaimRequestList.Columns(2).Visible = False
                End If


                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                'If Session("ClaimRequest_EmpUnkId") IsNot Nothing Then
                '    cboEmployee.SelectedValue = Session("ClaimRequest_EmpUnkId").ToString()
                '    Session.Remove("ClaimRequest_EmpUnkId")
                '    If CInt(cboEmployee.SelectedValue) > 0 Then
                '        Call btnSearch_Click(btnSearch, Nothing)
                '    End If
                'End If
                'Pinkal (27-Aug-2020) -- End



                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                If Request.QueryString.Count > 0 AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    Fill_List()
                End If
                'Pinkal (04-Feb-2020) -- End

                SetVisibility()

            Else
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                mintCancelExpEmployeeId = CInt(Me.ViewState("CancelExpEmployeeId"))
                'Pinkal (05-Sep-2020) -- End

                'Pinkal (09-Sep-2024) -- Start
                'NMB Freezing Issue.
                mdtTranDetail = CType(Me.ViewState("mdtTran"), DataTable)
                'Pinkal (09-Sep-2024) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("CancelExpEmployeeId") = mintCancelExpEmployeeId
            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            Me.ViewState("mdtTran") = mdtTranDetail
            'Pinkal (09-Sep-2024) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (05-Sep-2020) -- End


#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End


            cboExpCategory.DataValueField = "id"
            cboExpCategory.DataTextField = "name"
            cboExpCategory.DataSource = dsCombo.Tables(0)
            cboExpCategory.DataBind()

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objEMaster = Nothing
            objPrd = Nothing
            objMasterData = Nothing
            'Pinkal (05-Sep-2020) -- End`
        End Try
    End Sub

    Private Sub Fill_List(Optional ByVal blnstatus As Boolean = False)
        Dim dsList As New DataSet
        'Pinkal (12-Oct-2021)-- Start
        'NMB OT Requisition Performance Issue.
        'Dim dTable As DataTable = Nothing
        'Pinkal (12-Oct-2021)-- End
        Dim StrSearch As String = String.Empty

        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimReqestMaster As New clsclaim_request_master
        'Pinkal (05-Sep-2020) -- End

        Try

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("AllowtoViewClaimExpenseFormList ")) = False Then Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.expensetypeid = '" & CInt(cboExpCategory.SelectedValue) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.statusunkid = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If txtClaimNo.Text.Trim.Length > 0 Then
                StrSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text & "%' "
            End If


            If dtpFDate.IsNull = False AndAlso dtpTDate.IsNull = False Then
                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.
                'StrSearch &= "AND cmclaim_request_master.transactiondate >= '" & eZeeDate.convertDate(dtpFDate.GetDate) & "' AND cmclaim_request_master.transactiondate <= '" & eZeeDate.convertDate(dtpTDate.GetDate) & "' "
                StrSearch &= "AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) >= '" & eZeeDate.convertDate(dtpFDate.GetDate) & "' AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) <= '" & eZeeDate.convertDate(dtpTDate.GetDate) & "' "
                'Pinkal (12-Oct-2021)-- End
            End If


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee AndAlso Request.QueryString.Count > 0 Then
                StrSearch &= "AND cmclaim_request_master.crmasterunkid = " & mintCrMasterId
            End If
            'Pinkal (04-Feb-2020) -- End



            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If


            dsList = objClaimReqestMaster.GetList("List", CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                    , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                    , True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), StrSearch)

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'dTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            'lvClaimRequestList.AllowPaging = False
            If lvClaimRequestList.PageCount < lvClaimRequestList.CurrentPageIndex Then lvClaimRequestList.CurrentPageIndex = 0
            'Pinkal (12-Oct-2021)-- End

            lvClaimRequestList.AutoGenerateColumns = False
            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.
            'lvClaimRequestList.DataSource = dsList.Tables("List")
            lvClaimRequestList.DataSource = dsList.Tables("List").Copy
            'Pinkal (20-May-2022) -- End
            lvClaimRequestList.DataKeyField = "crmasterunkid"
            lvClaimRequestList.DataBind()



        Catch ex As Exception
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                lvClaimRequestList.CurrentPageIndex = 0
                lvClaimRequestList.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        Finally
            'If dTable IsNot Nothing Then dTable.Clear()
            'dTable = Nothing
            'Pinkal (12-Oct-2021)-- End
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objClaimReqestMaster = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                btnNew.Visible = CBool(Session("AllowtoAddClaimExpenseForm"))

                'S.SANDEEP |25-APR-2022| -- START
                'ISSUE/ENHANCEMENT : AC2-143V2
                'dgvData.Columns(0).Visible = CBool(Session("AllowtoEditClaimExpenseForm"))
                'dgvData.Columns(1).Visible = CBool(Session("AllowtoDeleteClaimExpenseForm"))
                'dgvData.Columns(2).Visible = CBool(Session("AllowtoCancelClaimExpenseForm"))

                lvClaimRequestList.Columns(0).Visible = CBool(Session("AllowtoEditClaimExpenseForm"))
                lvClaimRequestList.Columns(1).Visible = CBool(Session("AllowtoDeleteClaimExpenseForm"))
                lvClaimRequestList.Columns(2).Visible = CBool(Session("AllowtoCancelClaimExpenseForm"))
                'S.SANDEEP |25-APR-2022| -- END


                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee AndAlso Request.QueryString.Count > 0 Then
                btnNew.Visible = False
                btnSearch.Visible = False
                btnReset.Visible = False
                lvClaimRequestList.Columns(0).Visible = False
                lvClaimRequestList.Columns(1).Visible = False
                lvClaimRequestList.Columns(2).Visible = False
                'Pinkal (04-Feb-2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CancelFillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try

            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")).Date, "List", True, 1)
            cboCancelPeriod.DataValueField = "periodunkid"
            cboCancelPeriod.DataTextField = "name"
            cboCancelPeriod.DataSource = dsCombo.Tables(0)
            cboCancelPeriod.DataBind()
            cboCancelPeriod.SelectedValue = "0"


            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End


            cboCancelExpCategory.DataValueField = "id"
            cboCancelExpCategory.DataTextField = "name"
            cboCancelExpCategory.DataSource = dsCombo.Tables(0)
            cboCancelExpCategory.DataBind()
            cboCancelExpCategory.SelectedValue = "0"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub CancelGetValue()
    Private Sub CancelGetValue(ByRef objClaimReqestMaster As clsclaim_request_master)
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objClaimReqestMaster._Employeeunkid
            txtCancelEmployee.Text = objEmployee._Employeecode & " - " & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtCancelEmployeeid.Value = objClaimReqestMaster._Employeeunkid.ToString()
            txtCancelClaimNo.Text = objClaimReqestMaster._Claimrequestno
            cboCancelExpCategory.SelectedValue = objClaimReqestMaster._Expensetypeid.ToString()
            dtpCancelDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            objClaimReqestMaster._Transactiondate = dtpCancelDate.GetDate
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmployee = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Dim mdt As DataTable = (CType(ViewState("mdtTran"), DataTable))
            Dim mdt As DataTable = mdtTranDetail.Copy()
            'Pinkal (09-Sep-2024) -- End


            If mdt IsNot Nothing AndAlso mdt.Rows.Count > 0 Then

                If mdt.Columns.Contains("IsPosted") = False Then
                    mdt.Columns.Add("IsPosted", Type.GetType("System.Boolean"))
                    mdt.Columns("IsPosted").DefaultValue = False
                End If

                Dim mstrApprovalTranunkid As String = ""
                Dim objClaimProces As New clsclaim_process_Tran
                Dim dsList As DataSet = objClaimProces.GetList("List", True, , "cmclaim_process_tran.crmasterunkid = " & CInt(Me.ViewState("mintClaimRequestMasterId")))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        If CBool(dsList.Tables(0).Rows(i)("IsPosted")) Then
                            objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(dsList.Tables(0).Rows(i)("periodunkid"))

                            Dim drRow() As DataRow = mdt.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If objPeriod._Statusid = 2 Then 'enStatusType.Close
                                If drRow.Length > 0 Then
                                    drRow(0).Delete()
                                End If
                            Else
                                If drRow.Length > 0 Then
                                    drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                                End If
                            End If
                        Else
                            Dim drRow() As DataRow = mdt.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If drRow.Length > 0 Then
                                drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                            End If
                        End If
                        mstrApprovalTranunkid &= CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")) & ","
                    Next

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objPeriod = Nothing
                    'Pinkal (05-Sep-2020) -- End



                    If mstrApprovalTranunkid.Trim.Length > 0 Then mstrApprovalTranunkid = mstrApprovalTranunkid.Trim.Substring(0, mstrApprovalTranunkid.Trim.Length - 1)

                    Dim dRows() As DataRow = mdt.Select("crapprovaltranunkid Not in ( " & mstrApprovalTranunkid & ")")
                    If dRows.Length > 0 Then
                        For j As Integer = 0 To dRows.Length - 1
                            dRows(j)("IsPosted") = False
                        Next
                    End If
                Else
                    For Each dRow As DataRow In mdt.Rows
                        dRow("IsPosted") = False
                    Next
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                objClaimProces = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If



            mdt.AcceptChanges()

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Me.ViewState("CanceExpTable") = mdt
            'Pinkal (09-Sep-2024) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdt = New DataView(mdt, "iscancel = 0", "", DataViewRowState.CurrentRows).ToTable
            mdt = New DataView(mdt, "iscancel = 0", "", DataViewRowState.CurrentRows).ToTable().Copy()
            'Pinkal (05-Sep-2020) -- End

            dgvData.DataSource = mdt
            dgvData.DataBind()
            If mdt.Rows.Count > 0 Then
                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.
                If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhBaseAmount", False, True)).Visible = True
                    txtGrandTotal.Text = Format(CDec(mdt.Compute("SUM(base_amount)", "AUD<>'D' AND iscancel = 0")), Session("fmtCurrency").ToString())
                Else
                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhBaseAmount", False, True)).Visible = False
                    txtGrandTotal.Text = Format(CDec(mdt.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0")), Session("fmtCurrency").ToString())
                End If
                'Pinkal (24-Jun-2024) -- End
            Else
                txtGrandTotal.Text = "0.00"
            End If

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Me.ViewState("mdtTran") = mdt.Copy()
            mdtTranDetail = mdt.Copy()
            'Pinkal (09-Sep-2024) -- End

            If mdt IsNot Nothing Then mdt.Clear()
            mdt = Nothing


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If txtCancelRemark.Text.Trim.Length <= 0 Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Cancel Remark cannot be blank.Cancel Remark is required information."), Me)
                txtCancelRemark.Focus()
                Return False
            End If

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'If Me.ViewState("mdtTran") IsNot Nothing Then
            '    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            'End If
            If mdtTranDetail IsNot Nothing Then
                mdtTran = mdtTranDetail.Copy()
            End If
            'Pinkal (09-Sep-2024) -- End


            If mdtTran Is Nothing Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Please Select atleast One Expense to cancel."), Me)
                Return False
            End If
            'Pinkal (05-Sep-2020) -- End

            Dim drRow() As DataRow = mdtTran.Select("Ischeck = True")
            If drRow.Length <= 0 Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Please Select atleast One Expense to cancel."), Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Function

    'Pinkal (07-Mar-2019) -- End

    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    '    Private Sub Send_Notification(ByVal intCompanyID As Object)
    '        Try
    '            If gobjEmailList.Count > 0 Then
    '                Dim objSendMail As New clsSendMail
    'SendEmail:
    '                For Each objEmail In gobjEmailList
    '                    objSendMail._ToEmail = objEmail._EmailTo
    '                    objSendMail._Subject = objEmail._Subject
    '                    objSendMail._Message = objEmail._Message
    '                    objSendMail._Form_Name = objEmail._Form_Name
    '                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
    '                    objSendMail._OperationModeId = objEmail._OperationModeId
    '                    objSendMail._UserUnkid = objEmail._UserUnkid
    '                    objSendMail._SenderAddress = objEmail._SenderAddress
    '                    objSendMail._ModuleRefId = objEmail._ModuleRefId

    '                    If objEmail._FileName.ToString.Trim.Length > 0 Then
    '                        objSendMail._AttachedFiles = objEmail._FileName
    '                    End If

    '                    objSendMail._Form_Name = "frmCancelExpenseForm"
    '                    objSendMail._WebClientIP = Session("IP_ADD").ToString()
    '                    objSendMail._WebHostName = Session("HOST_NAME").ToString()

    '                    Dim intCUnkId As Integer = 0
    '                    If TypeOf intCompanyID Is Integer Then
    '                        intCUnkId = CInt(intCompanyID)
    '                    Else
    '                        intCUnkId = intCompanyID(0)
    '                    End If
    '                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
    '                                                objEmail._ExportReportPath).ToString.Length > 0 Then
    '                        gobjEmailList.Remove(objEmail)
    '                        GoTo SendEmail
    '                    End If
    '                Next
    '                gobjEmailList.Clear()
    '            End If

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    'Pinkal (20-May-2022) -- End

#End Region

#Region "Button's Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            If cboEmployee.SelectedValue Is Nothing OrElse cboEmployee.SelectedValue.ToString() = "" Then Exit Sub
            'Pinkal (27-Sep-2024) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Session("ClaimRequest_EmpUnkId") = cboEmployee.SelectedValue
            End If
            Response.Redirect("~/Claims_And_Expenses/wPg_AddEditClaimAndRequestList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Expense Category is compulsory information.Please Select Expense Category. "), Me)
                cboExpCategory.Focus()
                Exit Sub

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
            ElseIf CInt(cboStatus.SelectedValue) <= 0 AndAlso CInt(Session("Employeeunkid")) <= 0 Then
                'Pinkal (10-Feb-2021) -- End
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Status is compulsory information.Please Select Status. "), Me)
                cboStatus.Focus()
                Exit Sub
            End If
            'Pinkal (27-Aug-2020) -- End

            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Dim dtExpense As DataTable = Nothing
        Dim mdtAttachement As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimReqestMaster As clsclaim_request_master
        'Pinkal (05-Sep-2020) -- End

        Try
            If (popup_DeleteReason.Reason.Trim = "") Then
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                popup_DeleteReason.Show()
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmClaims_RequestList"
            StrModuleName2 = "mnuUtilitiesMain"
            StrModuleName3 = "mnuClaimsExpenses"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimReqestMaster = New clsclaim_request_master
            objClaimReqestMaster._Crmasterunkid = CInt(Me.ViewState("crmasterunkid"))
            objClaimReqestMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            'Pinkal (05-Sep-2020) -- End

            With objClaimReqestMaster
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text
                If CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                    ._VoidLoginEmployeeID = -1
                ElseIf CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                    ._VoidLoginEmployeeID = CInt(Session("Employeeunkid"))
                    ._Voiduserunkid = -1
                End If
            End With
            objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            objClaimReqestMaster._CompanyID = CInt(Session("CompanyUnkId"))
            objClaimReqestMaster._ArutiSelfServiceURL = Session("ArutiSelfServiceURL").ToString()
            If CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                objClaimReqestMaster._LoginMode = enLogin_Mode.EMP_SELF_SERVICE
            ElseIf CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                objClaimReqestMaster._LoginMode = enLogin_Mode.MGR_SELF_SERVICE
            End If
            objClaimReqestMaster._Userunkid = CInt(Session("UserId"))
            objClaimReqestMaster._WebFormName = mstrModuleName

            Dim objClaimTran As New clsclaim_request_tran
            objClaimTran._ClaimRequestMasterId = CInt(Me.ViewState("crmasterunkid"))
            dtExpense = objClaimTran._DataTable
            objClaimTran = Nothing

            Dim objAttchement As New clsScan_Attach_Documents

            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", objClaimReqestMaster._Employeeunkid)
            Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", objClaimReqestMaster._Employeeunkid, False, Nothing, Nothing, "", CBool(IIf(CInt(objClaimReqestMaster._Employeeunkid) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim strTranIds As String = String.Join(",", dtExpense.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
            If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
            mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
            objAttchement = Nothing

            For Each dRow As DataRow In mdtAttachement.Rows
                dRow.Item("AUD") = "D"
            Next
            mdtAttachement.AcceptChanges()

            If mdtAttachement IsNot Nothing Then

                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                For Each dRow As DataRow In mdtAttachement.Rows
                    If dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            objClaimReqestMaster.Delete(CInt(Me.ViewState("crmasterunkid")), _
                                        CInt(Session("UserId")), _
                                        Session("Database_Name").ToString(), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        Session("EmployeeAsOnDate").ToString(), _
                                        Session("UserAccessModeSetting").ToString(), True, mdtAttachement, True, Nothing)

            If (objClaimReqestMaster._Message.Length > 0) Then
                DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & objClaimReqestMaster._Message, Me)
            Else
                DisplayMessage.DisplayMessage("Entry Successfully deleted." & objClaimReqestMaster._Message, Me)
                Me.ViewState("crmasterunkid") = Nothing
            End If


            popup_DeleteReason.Dispose()
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dtExpense IsNot Nothing Then dtExpense.Clear()
            dtExpense = Nothing
            If mdtAttachement IsNot Nothing Then mdtAttachement.Clear()
            mdtAttachement = Nothing
            objClaimReqestMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnCancelSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSave.Click
        Try
            If Is_Valid() = False Then
                popupCancel.Show()
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmCancelExpenseForm"
            StrModuleName2 = "mnuUtilitiesMain"
            StrModuleName3 = "mnuClaimsExpenses"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objApproverTran As New clsclaim_request_approval_tran
            objApproverTran._YearId = CInt(Session("Fin_year"))
            objApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Dim blnflag As Boolean = objApproverTran.CancelExpense(CType(ViewState("mdtTran"), DataTable), enExpenseType.EXP_NONE, txtCancelRemark.Text, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, Nothing)
            Dim blnflag As Boolean = objApproverTran.CancelExpense(mdtTranDetail, enExpenseType.EXP_NONE, txtCancelRemark.Text, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, Nothing)
            'Pinkal (09-Sep-2024) -- End

            objApproverTran = Nothing
            'Pinkal (05-Sep-2020) -- End

            If blnflag Then
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                Dim objClaimMaster As New clsclaim_request_master
                Dim objEmployee As New clsEmployee_Master

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objClaimReqestMaster._Employeeunkid
                'objClaimMaster._Crmasterunkid = CInt(Me.ViewState("crmasterunkid"))
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintCancelExpEmployeeId
                objClaimMaster._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
                'Pinkal (05-Sep-2020) -- End

                objClaimMaster._EmployeeCode = objEmployee._Employeecode
                objClaimMaster._EmployeeFirstName = objEmployee._Firstname
                objClaimMaster._EmployeeMiddleName = objEmployee._Othername
                objClaimMaster._EmployeeSurName = objEmployee._Surname
                objClaimMaster._EmpMail = objEmployee._Email

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'objClaimMaster.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, CInt(Session("UserId")), txtCancelRemark.Text.Trim)

                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                'objClaimMaster.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtCancelClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, CInt(Session("UserId")), txtCancelRemark.Text.Trim)
                objClaimMaster.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtCancelClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, CInt(Session("UserId")), txtCancelRemark.Text.Trim, False)
                'Pinkal (20-May-2022) -- End

                'Pinkal (05-Sep-2020) -- End


                'Pinkal (20-May-2022) -- Start
                'Optimize Global Claim Request for NMB.
                'If gobjEmailList.Count > 0 Then
                '    Dim objThread As Threading.Thread
                '    If HttpContext.Current Is Nothing Then
                '        objThread = New Threading.Thread(AddressOf Send_Notification)
                '        objThread.IsBackground = True
                '        Dim arr(1) As Object
                '        arr(0) = CInt(Session("CompanyUnkId"))
                '        objThread.Start(arr)
                '    Else
                '        Call Send_Notification(CInt(Session("CompanyUnkId")))
                '    End If
                'End If
                'Pinkal (20-May-2022) -- End


                'Sohail (30 Nov 2017) -- End

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objClaimMaster = Nothing
                objEmployee = Nothing
                'Pinkal (05-Sep-2020) -- End

                'Pinkal (09-Sep-2024) -- Start
                'NMB Freezing Issue.
                If mdtTranDetail IsNot Nothing Then mdtTranDetail.Clear()
                mdtTranDetail = Nothing
                'Pinkal (09-Sep-2024) -- End

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Expense cancellation process completed successfully."), Me)
                popupCancel.Hide()
                Fill_List()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Sorry, Expense cancellation process fail."), Me)
                popupCancel.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'FillCombo()
            'Fill_List(True)
            lvClaimRequestList.DataSource = Nothing
            lvClaimRequestList.DataBind()
            'Pinkal (05-Sep-2020) -- End

            txtClaimNo.Text = ""
            dtpFDate.SetDate = Nothing
            dtpTDate.SetDate = Nothing

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            cboExpCategory.SelectedValue = "0"


            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                cboEmployee.SelectedValue = "0"
            End If
            'Pinkal (11-Feb-2022) -- End


            txtClaimNo.Text = ""
            cboStatus.SelectedValue = "0"
            'Gajanan [17-Sep-2020] -- End

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            If mdtTranDetail IsNot Nothing Then mdtTranDetail.Clear()
            mdtTranDetail = Nothing
            'Pinkal (09-Sep-2024) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub lvClaimRequestList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvClaimRequestList.DeleteCommand
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimReqestMaster As New clsclaim_request_master
            If objClaimReqestMaster.isUsed(CInt(lvClaimRequestList.DataKeys(e.Item.ItemIndex))) = True Then
                DisplayMessage.DisplayMessage(objClaimReqestMaster._Message, Me)
                objClaimReqestMaster = Nothing
                Exit Sub
            End If
            objClaimReqestMaster = Nothing
            'Pinkal (05-Sep-2020) -- End

            If CInt(e.Item.Cells(10).Text) = 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, you cannot delete selected data. Reason : It is already approved."), Me)
                Exit Sub

            ElseIf CInt(e.Item.Cells(10).Text) = 2 Then

                Dim objClaimApproval As New clsclaim_request_approval_tran
                Dim dsPendingList As DataSet = objClaimApproval.GetApproverExpesneList("Approval", False, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                                    , Session("Database_Name").ToString(), CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString() _
                                                                                                                                    , CInt(e.Item.Cells(11).Text.ToString()), False, True, -1 _
                                                                                                                                    , " cmclaim_approval_tran.statusunkid <> 2 ", CInt(e.Item.Cells(13).Text.ToString()))
                objClaimApproval = Nothing

                If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, you cannot delete selected data. Reason:It is already approved by one or more approver(s)."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                    dsPendingList = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                dsPendingList = Nothing
                'Pinkal (05-Sep-2020) -- End


                '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Length > 0 Then
                    Dim objClaim As New clsclaim_request_tran
                    objClaim._ClaimRequestMasterId = CInt(e.Item.Cells(13).Text.ToString())
                    Dim dtTable As DataTable = objClaim._DataTable
                    objClaim = Nothing
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        'If xBudgetMandatoryCount > 0 Then
                        Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                        If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry, you cannot delete selected data. Reason:It is already approved or pending for approval in P2P system."), Me)
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtTable IsNot Nothing Then dtTable.Clear()
                            dtTable = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If


                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.


                Me.ViewState("crmasterunkid") = CInt(lvClaimRequestList.DataKeys(e.Item.ItemIndex))
                'Language.setLanguage(mstrModuleName)
                popup_DeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Are you sure you want to delete this Claim Request?")
                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()

            ElseIf CInt(e.Item.Cells(10).Text) = 3 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sorry, you cannot delete selected data. Reason : It is already rejected."), Me)
                Exit Sub

            ElseIf CInt(e.Item.Cells(10).Text) = 6 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, you cannot delete selected data. Reason : It is already cancelled."), Me)
                Exit Sub

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvClaimRequestList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvClaimRequestList.ItemCommand
        Try
            If e.CommandName = "Cancel" Then

                If CInt(e.Item.Cells(10).Text) <> 1 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, you cannot cancel selected data. Reason : It is not in approved status."), Me)
                    Exit Sub
                End If


                'Pinkal (10-Jun-2022) -- Start
                'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
                Dim xClaimRetirementStatusId As Integer = -1
                Dim objRetirement As New clsclaim_retirement_master
                Dim mintclaimRetirementId As Integer = objRetirement.GetRetirementFromClaimRequest(CInt(e.Item.Cells(13).Text), xClaimRetirementStatusId, Nothing)
                If mintclaimRetirementId <= 0 Then
                    Dim objRetirementProcess As New clsretire_process_tran
                    Dim dsRetirement As DataSet = objRetirementProcess.GetUnRetiredList("List", False, True, -1, "cmclaim_request_master.crmasterunkid = " & CInt(e.Item.Cells(13).Text), Nothing)
                    If dsRetirement IsNot Nothing AndAlso dsRetirement.Tables(0).Rows.Count > 0 Then
                        If dsRetirement.Tables(0).Rows(0)("periodunkid") > 0 AndAlso CBool(dsRetirement.Tables(0).Rows(0)("isposted")) = True AndAlso CInt(dsRetirement.Tables(0).Rows(0)("crmasterunkid")) > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, you cannot cancel this claim application because it is already linked with claim retirement."), Me)
                            Exit Sub
                        End If
                    End If
                ElseIf mintclaimRetirementId > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, you cannot cancel this claim application because it is already linked with claim retirement."), Me)
                    Exit Sub
                End If
                'Pinkal (10-Jun-2022) -- End


                '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Length > 0 Then
                    Dim objClaim As New clsclaim_request_tran
                    objClaim._ClaimRequestMasterId = CInt(e.Item.Cells(13).Text.ToString())
                    Dim dtTable As DataTable = objClaim._DataTable
                    objClaim = Nothing
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                        If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, you cannot cancel selected data. Reason:It is already approved in P2P system."), Me)
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtTable IsNot Nothing Then dtTable.Clear()
                            dtTable = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If
                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

                'Pinkal (04-Feb-2019) -- End


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objClaimReqestMaster As New clsclaim_request_master
                Dim objClaimTran As New clsclaim_request_tran
                Dim objApproverTran As New clsclaim_request_approval_tran
                Dim dtApprover As DataTable = Nothing
                Dim mdtTran As New DataTable
                'Pinkal (05-Sep-2020) -- End



                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.
                'dtApprover = objApproverTran.GetMaxApproverForClaimForm(CInt(e.Item.Cells(12).Text), CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(13).Text) _
                '                                                           , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(e.Item.Cells(10).Text))

                dtApprover = objApproverTran.GetMaxApproverForClaimForm(CInt(e.Item.Cells(12).Text), CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(13).Text) _
                                                                                                        , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(e.Item.Cells(10).Text) _
                                                                                                        , -1, CDec(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "colhApprovedAmount", False, True)).Text))

                'Pinkal (24-Jun-2024) -- End

              
                If dtApprover.Rows.Count > 0 Then
                    CancelFillCombo()
                    Me.ViewState.Add("mintClaimRequestMasterId", CInt(e.Item.Cells(13).Text))
                    Me.ViewState.Add("mintClaimApproverId", CInt(dtApprover.Rows(0)("crapproverunkid")))
                    Me.ViewState.Add("mintClaimApproverEmpID", CInt(dtApprover.Rows(0)("approveremployeeunkid")))
                    Me.ViewState.Add("mintApproverPriority", CInt(dtApprover.Rows(0)("crpriority")))
                    objClaimReqestMaster._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'CancelGetValue()
                    objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                    objClaimReqestMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
                    mintCancelExpEmployeeId = objClaimReqestMaster._Employeeunkid
                    CancelGetValue(objClaimReqestMaster)
                    'Pinkal (05-Sep-2020) -- End

                    objClaimTran._ClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))
                    mdtTran = objApproverTran.GetApproverExpesneList("List", False, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                , Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                , Session("EmployeeAsOnDate").ToString(), CInt(cboCancelExpCategory.SelectedValue) _
                                                                                                , False, True, CInt(Me.ViewState("mintClaimApproverId")), "" _
                                                                                                , CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing).Tables(0)


                    'Pinkal (09-Sep-2024) -- Start
                    'NMB Freezing Issue.
                    'Me.ViewState.Add("mdtTran", mdtTran.Copy())
                    mdtTranDetail = mdtTran.Copy()
                    'Pinkal (09-Sep-2024) -- End

                    Fill_Expense()
                    txtCancelRemark.Text = ""


                    'S.SANDEEP |25-APR-2022| -- START
                    'ISSUE/ENHANCEMENT : AC2-143V2
                    If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                        If CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                            Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAirline", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelfrom", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelto", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhflightno", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgref", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhrelation", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgtype", False, True)).Visible = True

                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUoM", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhQty", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUnitPrice", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAmount", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhCurrency", False, True)).Visible = False
                                    pnlGTotal.Visible = False
                                Case Else
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAirline", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelto", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhflightno", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgref", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhrelation", False, True)).Visible = False
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUoM", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhQty", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUnitPrice", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAmount", False, True)).Visible = True
                                    dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhCurrency", False, True)).Visible = True
                                    pnlGTotal.Visible = True
                            End Select
                        Else
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAirline", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelto", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhflightno", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgref", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhrelation", False, True)).Visible = False
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUoM", False, True)).Visible = True
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhQty", False, True)).Visible = True
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUnitPrice", False, True)).Visible = True
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAmount", False, True)).Visible = True
                            dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhCurrency", False, True)).Visible = True
                            pnlGTotal.Visible = True
                        End If
                    Else
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAirline", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtravelto", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhflightno", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgref", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhrelation", False, True)).Visible = False
                        dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhpsgtype", False, True)).Visible = False
                        pnlGTotal.Visible = True
                    End If
                    'S.SANDEEP |25-APR-2022| -- END

                    popupCancel.Show()
                Else
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "There is no Expense Data to Cancel the Expense Form."), Me)
                End If


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtApprover IsNot Nothing Then dtApprover.Clear()
                dtApprover = Nothing
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing
                objApproverTran = Nothing
                objClaimTran = Nothing
                objClaimReqestMaster = Nothing
                'Pinkal (05-Sep-2020) -- End


            ElseIf e.CommandName = "Edit" Then

                If CInt(e.Item.Cells(10).Text) = 1 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, you cannot edit selected data. Reason:It is already approved."), Me)
                    Exit Sub

                ElseIf CInt(e.Item.Cells(10).Text) = 2 Then


                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    Dim objClaimApproval As New clsclaim_request_approval_tran
                    Dim dsPendingList As DataSet = objClaimApproval.GetApproverExpesneList("Approval", False, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                                        , Session("Database_Name").ToString(), CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString() _
                                                                                                                                        , CInt(e.Item.Cells(11).Text.ToString()), False, True, -1 _
                                                                                                                                        , " cmclaim_approval_tran.statusunkid <> 2 ", CInt(e.Item.Cells(13).Text.ToString()))
                    objClaimApproval = Nothing

                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot edit selected data. Reason:It is already approved by one or more approver(s)."), Me)
                        Exit Sub
                    End If

                    '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                    If Session("NewRequisitionRequestP2PServiceURL").ToString().Length > 0 Then
                        Dim objClaim As New clsclaim_request_tran
                        objClaim._ClaimRequestMasterId = CInt(e.Item.Cells(13).Text.ToString())
                        Dim dtTable As DataTable = objClaim._DataTable
                        objClaim = Nothing

                        If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                            Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                            If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, you cannot edit selected data. Reason:It is already approved or pending for approval in P2P system."), Me)
                                'Pinkal (05-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                If dtTable IsNot Nothing Then dtTable.Clear()
                                dtTable = Nothing
                                'Pinkal (05-Sep-2020) -- End
                                Exit Sub
                            End If
                            'Pinkal (04-Feb-2019) -- End
                        End If
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dtTable IsNot Nothing Then dtTable.Clear()
                        dtTable = Nothing
                        'Pinkal (05-Sep-2020) -- End
                    End If
                    '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

                    'Pinkal (20-Nov-2018) -- End

                    Session.Add("mintClaimRequestMasterId", CInt(lvClaimRequestList.DataKeys(e.Item.ItemIndex)))

                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Session("ClaimRequest_EmpUnkId") = cboEmployee.SelectedValue
                    End If
                    Response.Redirect("~/Claims_And_Expenses/wPg_AddEditClaimAndRequestList.aspx", False)

                ElseIf CInt(e.Item.Cells(10).Text) = 3 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you cannot edit selected data. Reason:It is already rejected."), Me)
                    Exit Sub

                ElseIf CInt(e.Item.Cells(10).Text) = 6 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot edit selected data. Reason: It is already cancelled."), Me)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvClaimRequestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvClaimRequestList.ItemDataBound
        Try

            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Call SetDateFormat()
            'Pinkal (27-Aug-2020) -- End
            If e.Item.Cells.Count > 0 And e.Item.ItemIndex >= 0 Then

                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'Dim lnkCancel As LinkButton = CType(e.Item.Cells(2).FindControl("lnkCancel"), LinkButton)
                'lnkCancel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lvClaimRequestList.Columns(2).FooterText, Me.lvClaimRequestList.Columns(2).HeaderText)
                'If CBool(Session("AllowToCancelLeaveButRetainExpense")) AndAlso CInt(e.Item.Cells(11).Text) = enExpenseType.EXP_LEAVE AndAlso CInt(e.Item.Cells(10).Text) = 1 Then
                '    lnkCancel.Visible = False
                'End If
                Dim lnkCancel As LinkButton = CType(e.Item.Cells(2).FindControl("ImgCancel"), LinkButton)
                If CBool(Session("AllowToCancelLeaveButRetainExpense")) AndAlso CInt(e.Item.Cells(11).Text) = enExpenseType.EXP_LEAVE AndAlso CInt(e.Item.Cells(10).Text) = 1 Then
                    lnkCancel.Visible = False
                Else
                    lnkCancel.Visible = True
                End If
                'Gajanan [17-Sep-2020] -- End


                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.

                ''Pinkal (28-Apr-2020) -- Start
                ''Optimization  - Working on Process Optimization and performance for require module.	
                'Dim mdcAppliedAmount As Decimal = 0
                'If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                '    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString())
                'Else
                '    Dim objClaimReqestTran As New clsclaim_request_tran
                '    mdcAppliedAmount = objClaimReqestTran.GetAppliedExpenseAmount(CInt(e.Item.Cells(13).Text))
                '    e.Item.Cells(7).Text = Format(mdcAppliedAmount, Session("fmtCurrency").ToString())
                '    'Pinkal (05-Sep-2020) -- Start
                '    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                '    objClaimReqestTran = Nothing
                '    'Pinkal (05-Sep-2020) -- End
                'End If
                ''Pinkal (28-Apr-2020) -- End


                ''Pinkal (28-Apr-2020) -- Start
                ''Optimization  - Working on Process Optimization and performance for require module.	
                'If (e.Item.Cells(14).Text.Trim.Length > 0 AndAlso e.Item.Cells(14).Text <> "&nbsp;") AndAlso CInt(e.Item.Cells(10).Text) = 1 Then  'P2P REQUISTION ID
                '    If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                '        e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString())
                '    Else
                '        e.Item.Cells(8).Text = Format(mdcAppliedAmount, Session("fmtCurrency").ToString())
                '    End If
                'Else
                '    If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                '        e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(16).Text), Session("fmtCurrency").ToString())
                '    Else
                '        Dim objClaimReqestMaster As New clsclaim_request_master
                '        objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                '        Dim mdcApprovedAmount As Decimal = objClaimReqestMaster.GetFinalApproverApprovedAmount(CInt(e.Item.Cells(12).Text), CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(13).Text))
                '        e.Item.Cells(8).Text = Format(mdcApprovedAmount, Session("fmtCurrency").ToString())
                '        objClaimReqestMaster = Nothing
                '    End If
                'End If
                ''Pinkal (28-Apr-2020) -- End

                Dim mdcAppliedAmount As Decimal = 0
                Dim mdecAppliedBaseAmount As Decimal = 0
                If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                    If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                        e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhReqbaseAmount", False, True)).Text), Session("fmtCurrency").ToString())
                    Else
                        e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString())
                    End If
                Else
                    Dim objClaimReqestTran As New clsclaim_request_tran
                    mdcAppliedAmount = objClaimReqestTran.GetAppliedExpenseAmount(CInt(e.Item.Cells(13).Text), mdecAppliedBaseAmount)
                    e.Item.Cells(7).Text = Format(mdcAppliedAmount, Session("fmtCurrency").ToString())
                    e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhReqbaseAmount", False, True)).Text = Format(mdecAppliedBaseAmount, Session("fmtCurrency").ToString())
                    objClaimReqestTran = Nothing
                End If

                If (e.Item.Cells(14).Text.Trim.Length > 0 AndAlso e.Item.Cells(14).Text <> "&nbsp;") AndAlso CInt(e.Item.Cells(10).Text) = 1 Then  'P2P REQUISTION ID
                    If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                        If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhApprovedbaseAmt", False, True)).Text), Session("fmtCurrency").ToString())
                        Else
                            e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString())
                        End If
                    Else
                        If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhApprovedbaseAmt", False, True)).Text), Session("fmtCurrency").ToString())
                        Else
                            e.Item.Cells(8).Text = Format(mdcAppliedAmount, Session("fmtCurrency").ToString())
                        End If
                    End If
                Else
                    If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                        If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhApprovedbaseAmt", False, True)).Text), Session("fmtCurrency").ToString())
                        Else
                            e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(16).Text), Session("fmtCurrency").ToString())
                        End If
                    Else
                        Dim mdecApprovedBaseAmount As Decimal = 0
                        Dim objClaimReqestMaster As New clsclaim_request_master
                        objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                        Dim mdcApprovedAmount As Decimal = objClaimReqestMaster.GetFinalApproverApprovedAmount(CInt(e.Item.Cells(12).Text), CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(13).Text), False, 0, mdecApprovedBaseAmount)
                        If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            e.Item.Cells(8).Text = Format(mdecApprovedBaseAmount, Session("fmtCurrency").ToString())
                        Else
                            e.Item.Cells(8).Text = Format(mdcApprovedAmount, Session("fmtCurrency").ToString())
                        End If
                        objClaimReqestMaster = Nothing
                    End If
                End If
                'Pinkal (24-Jun-2024) -- End

                If e.Item.Cells(6).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(6).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToShortDateString
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.Cells.Count > 0 And e.Row.RowIndex >= 0 Then
                'S.SANDEEP |25-APR-2022| -- START
                'ISSUE/ENHANCEMENT : AC2-143V2
                'e.Row.Cells(3).Text = CDec(e.Row.Cells(3).Text).ToString
                'e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency").ToString())
                'e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency").ToString)

                If dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhQty", False, True)).Visible = True Then
                    e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhQty", False, True)).Text = CDec(e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhQty", False, True)).Text).ToString
                End If
                If dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhUnitPrice", False, True)).Visible = True Then
                    e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhUnitPrice", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhUnitPrice", False, True)).Text), Session("fmtCurrency").ToString())
                End If
                If dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhAmount", False, True)).Visible = True Then
                    e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString)
                End If

                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.
                If dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhBaseAmount", False, True)).Visible = True Then
                    e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhBaseAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhBaseAmount", False, True)).Text), Session("fmtCurrency").ToString)
                End If
                'Pinkal (24-Jun-2024) -- End


                If dgvData.Columns(getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Visible = True Then
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Text.ToString().Trim.Length > 0 AndAlso _
                                           e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Text.ToString() <> "&nbsp;" Then
                        e.Row.Cells(getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvData, "dgcolhtraveldate", False, True)).Text)
                    End If
                End If
                'S.SANDEEP |25-APR-2022| -- END                
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lvClaimRequestList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles lvClaimRequestList.PageIndexChanged
        Try
            lvClaimRequestList.CurrentPageIndex = e.NewPageIndex
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Control Event"

    Protected Sub chkCancelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If dgvData.Rows.Count <= 0 Then Exit Sub

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
            Dim mdtTran As DataTable = mdtTranDetail.Copy
            'Pinkal (09-Sep-2024) -- End

            For Each xRow As GridViewRow In dgvData.Rows
                If CBool(mdtTran.Rows(xRow.RowIndex)("IsPosted")) = True Then
                    CType(xRow.FindControl("ChkCancelSelect"), CheckBox).Checked = False
                Else
                    mdtTran.Rows(xRow.RowIndex)("ischeck") = CBool(cb.Checked)
                    CType(xRow.FindControl("ChkCancelSelect"), CheckBox).Checked = cb.Checked
                End If
            Next
            mdtTran.AcceptChanges()

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Me.ViewState("mdtTran") = mdtTran.Copy
            mdtTranDetail = mdtTran.Copy
            'Pinkal (09-Sep-2024) -- End


            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing

            popupCancel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkCancelSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then

                'Pinkal (09-Sep-2024) -- Start
                'NMB Freezing Issue.
                'Dim dRow() As DataRow = CType(Me.ViewState("mdtTran"), DataTable).Select("crapprovaltranunkid = '" & CInt(dgvData.DataKeys(gvr.RowIndex)("crapprovaltranunkid")) & "'")  'crapprovaltranunkid
                Dim dRow() As DataRow = mdtTranDetail.Select("crapprovaltranunkid = '" & CInt(dgvData.DataKeys(gvr.RowIndex)("crapprovaltranunkid")) & "'")  'crapprovaltranunkid
                'Pinkal (09-Sep-2024) -- End



                If dRow.Length > 0 Then
                    If CBool(dRow(0)("IsPosted")) = True Then
                        'Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period."), Me)
                        cb.Checked = False
                    Else
                        dRow(0).Item("ischeck") = cb.Checked
                        dRow(0).AcceptChanges()
                    End If
                End If
            End If

            'Pinkal (09-Sep-2024) -- Start
            'NMB Freezing Issue.
            'Dim drRow() As DataRow = CType(Me.ViewState("mdtTran"), DataTable).Select("ischeck = True")
            Dim drRow() As DataRow = mdtTranDetail.Select("ischeck = True")
            'Pinkal (09-Sep-2024) -- End

            If drRow.Length > 0 Then
                If drRow.Length = dgvData.Rows.Count Then
                    CType(dgvData.HeaderRow.FindControl("chkCancelAll"), CheckBox).Checked = True
                Else
                    CType(dgvData.HeaderRow.FindControl("chkCancelAll"), CheckBox).Checked = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupCancel.Show()
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "eZeeHeader_Title", Me.lblPageHeader.Text).Replace("&&", "&")
            Me.lblExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.lblTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTo.ID, Me.lblTo.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lvClaimRequestList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lvClaimRequestList.Columns(0).FooterText, Me.lvClaimRequestList.Columns(0).HeaderText)
            'Me.lvClaimRequestList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lvClaimRequestList.Columns(1).FooterText, Me.lvClaimRequestList.Columns(1).HeaderText).Replace("&", "")
            'Me.lvClaimRequestList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lvClaimRequestList.Columns(2).FooterText, Me.lvClaimRequestList.Columns(2).HeaderText)
            'Gajanan [17-Sep-2020] -- End

            Me.lvClaimRequestList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(3).FooterText, Me.lvClaimRequestList.Columns(3).HeaderText)
            Me.lvClaimRequestList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(4).FooterText, Me.lvClaimRequestList.Columns(4).HeaderText)
            Me.lvClaimRequestList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(5).FooterText, Me.lvClaimRequestList.Columns(5).HeaderText)
            Me.lvClaimRequestList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(6).FooterText, Me.lvClaimRequestList.Columns(6).HeaderText)
            Me.lvClaimRequestList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(7).FooterText, Me.lvClaimRequestList.Columns(7).HeaderText)
            Me.lvClaimRequestList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(8).FooterText, Me.lvClaimRequestList.Columns(8).HeaderText)
            Me.lvClaimRequestList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvClaimRequestList.Columns(9).FooterText, Me.lvClaimRequestList.Columns(9).HeaderText)


            'Language.setLanguage(mstrModuleName1)

            Me.lblpopupHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.lblpopupHeader.Text)
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lblTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),"gbExpenseInformation", Me.lblTitle.Text)
            'Gajanan [17-Sep-2020] -- End

            Me.lblCancelDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblDate", Me.lblCancelDate.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblCancelEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblEmployee", Me.lblCancelEmployee.Text)
            Me.lblCancelExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblExpCategory", Me.lblCancelExpCategory.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblCancelRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCancelRemark.ID, Me.lblCancelRemark.Text)
            Me.lblGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.btnCancelSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnSave", Me.btnCancelSave.Text).Replace("&", "")
            Me.btnCancelClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnCancelClose.Text).Replace("&", "")

            Me.dgvData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(1).FooterText, Me.dgvData.Columns(1).HeaderText)
            Me.dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Me.dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)
            'Pinkal (24-Jun-2024) -- End




        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Cancel Remark cannot be blank.Cancel Remark is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Please Select atleast One Expense to cancel.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 4, "Expense cancellation process completed successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 5, "Sorry, Expense cancellation process fail.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Are you sure you want to delete this Claim Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "There is no Expense Data to Cancel the Expense Form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Configuration Path does not Exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, you cannot cancel selected data. Reason : It is not in approved status.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, you cannot edit selected data. Reason:It is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, you cannot edit selected data. Reason:It is already approved by one or more approver(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, you cannot edit selected data. Reason:It is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, you cannot edit selected data. Reason: It is already cancelled.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, you cannot delete selected data. Reason : It is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, you cannot delete selected data. Reason:It is already approved by one or more approver(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Sorry, you cannot delete selected data. Reason : It is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Sorry, you cannot delete selected data. Reason : It is already cancelled.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Sorry, you cannot edit selected data. Reason:It is already approved or pending for approval in P2P system.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Sorry, you cannot delete selected data. Reason:It is already approved or pending for approval in P2P system.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, you cannot cancel selected data. Reason:It is already approved in P2P system.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Expense Category is compulsory information.Please Select Expense Category.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Status is compulsory information.Please Select Status.")


        Catch Ex As Exception
            'Sohail (18 Mar 2019) -- Start
            'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (18 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
