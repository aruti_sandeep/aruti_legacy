﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading
Imports ArutiReports
Imports System

#End Region

Public Class Claims_And_Expenses_wPg_ExpenseApproval
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimMaster As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Private objExpApproverTran As New clsclaim_request_approval_tran
    'Pinkal (05-Sep-2020) -- End

    Private Shared ReadOnly mstrModuleName As String = "frmExpenseApproval"
    Private objCONN As SqlConnection
    Dim mintClaimFormEmpId As Integer = 0
    Private mblnIsExternalApprover As Boolean = False


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsExpenseEditClick As Boolean = False
    Private mblnIsExpenseApproveClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mblnIsClaimFormBudgetMandatory As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End

    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestApprovalExpTranId As Integer = 0
    Dim mstrClaimRequestApprovalExpGUID As String = ""
    'Pinkal (07-Mar-2019) -- End


    'Pinkal (25-Jan-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    Dim objClaimEmailList As New List(Of clsEmailCollection)
    'Pinkal (25-Jan-2022) -- End

    'S.SANDEEP |03-SEP-2022| -- START
    'ISSUE/ENHANCEMENT : Sprint_2022-13
    Private mintSelectedPassenger As Integer = 0
    'S.SANDEEP |03-SEP-2022| -- END

    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.
    Dim mstrP2PToken As String = ""
    Dim mintGLCodeID As Integer = 0
    Dim mstrGLCode As String = ""
    Dim mstrGLDescription As String = ""
    Dim mintCostCenterID As Integer = 0
    Dim mstrCostCenterCode As String = ""
    'Pinkal (16-Oct-2023) -- End


    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Private mstrJournal As String = String.Empty
    Private mstrPaymentMode As String = String.Empty
    Private mblnIsDebit As Boolean = False
    Private mblnIsCredit As Boolean = False
    'Pinkal (23-Dec-2023) -- End

    'Pinkal (24-Jun-2024) -- Start
    'NMB Enhancement : P2P & Expense Category Enhancements.
    Private mblnReturntoApplicant As Boolean = False
    'Pinkal (24-Jun-2024) -- End

    'Pinkal (27-Sep-2024) -- Start
    'NMB Enhancement : (A1X-2787) NMB - Credit report development.
    Dim mstrGroupName As String = ""
    'Pinkal (27-Sep-2024) -- End

    Private mblnIsLeaveEncashment As Boolean = False
    Private mblnIsSecRouteMandatory As Boolean = False
    Private mblnIsConsiderDependants As Boolean = False
    Private mblnDoNotAllowToApplyForBackDate As Boolean = False
    Private mstrDescription As String = ""
    Private mintExpenditureTypeId As Integer = 0
    Private mblnIsaccrue As Boolean = False
    Private mintUomunkid As Integer = 0
    Private mintAccrueSettingId As Integer = 0
    Private mintExpense_MaxQuantity As Integer = 0
    Private mblnIsAttachDocMandetory As Boolean = False
    Private mblnIsUnitPriceEditable As Boolean = True
    Private mintJournalunkid As Integer = 0
    Private mintPaymentmodeunkid As Integer = 0

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsApprovalList As DataSet = Nothing
        Try

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count > 0 Then

                    KillIdleSQLSessions()
                    GC.Collect()

                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If


                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 7 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))

                        Me.ViewState.Add("ClaimFormEmpId", CInt(arr(2)))
                        Me.ViewState.Add("FormApproverId", CInt(arr(3)))
                        Me.ViewState.Add("crmasterunkid", CInt(arr(4)))
                        Me.ViewState.Add("crapprovaltranunkid", CInt(arr(5)))
                        Me.ViewState.Add("ExpenseCategoryID", CInt(arr(6)))
                        mintClaimFormEmpId = CInt(arr(2))

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If

                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Session("UserId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub
                            Else

                                If mblnATLoginRequiredToApproveApplications = False Then

                                    Dim strError As String = ""
                                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                                        Exit Sub
                                    End If

                                    HttpContext.Current.Session("mdbname") = Session("Database_Name")

                                    gobjConfigOptions = New clsConfigOptions
                                    'ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString) 'Sohail (28 Oct 2013)

                                    ArtLic._Object = New ArutiLic(False)
                                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                                        Dim objGroupMaster As New clsGroup_Master
                                        objGroupMaster._Groupunkid = 1
                                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                                    End If

                                    If ConfigParameter._Object._IsArutiDemo Then
                                        If ConfigParameter._Object._IsExpire Then
                                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                            Exit Try
                                        Else
                                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                                Exit Try
                                            End If
                                        End If
                                    End If

                                    'Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                                    'Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                                    'Session("PaymentApprovalwithLeaveApproval") = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
                                    'Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                                    'If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                                    '    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                                    'Else
                                    '    Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                                    'End If
                                    'Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim

                                    'Try
                                    '    If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                    '        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                    '        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                                    '    Else
                                    '        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                    '        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                                    '    End If

                                    'Catch ex As Exception
                                    '    HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                    '    HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                                    'End Try


                                    strError = ""
                                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                                        Exit Sub
                                    End If


                                    Dim objUser As New clsUserAddEdit
                                    objUser._Userunkid = CInt(Session("UserId"))
                                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                                    Call GetDatabaseVersion()
                                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname").ToString())
                                    HttpContext.Current.Session("clsuser") = clsuser
                                    HttpContext.Current.Session("UserName") = clsuser.UserName
                                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                                    HttpContext.Current.Session("Surname") = clsuser.Surname
                                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                                    HttpContext.Current.Session("UserId") = clsuser.UserID
                                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                                    HttpContext.Current.Session("Password") = clsuser.password
                                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                                    objUser = Nothing

                                    strError = ""
                                    If SetUserSessions(strError) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                                        Exit Sub
                                    End If

                                    'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                                    'gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                                    'gobjLocalization._LangId = CInt(HttpContext.Current.Session("LangId"))


                                End If  ' If mblnATLoginRequiredToApproveApplications = False Then

                            End If  'If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub

                        End If  '   If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                        'Pinkal (23-Feb-2024) -- End


                        Dim objExpApproverTran As New clsclaim_request_approval_tran

                        Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                            , Session("Database_Name").ToString(), CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString() _
                                                                                                                            , CInt(Me.ViewState("ExpenseCategoryID")), False, True, -1 _
                                                                                                                            , "crapprovaltranunkid = " & CInt(ViewState("crapprovaltranunkid")), CInt(Me.ViewState("crmasterunkid")), Nothing)
                        If dsList.Tables("List").Rows.Count > 0 Then

                            If CInt(dsList.Tables("List").Rows(0)("visibleid")) = 1 Then
                                Session.Abandon()
                                DisplayMessage.DisplayMessage("You cannot Edit this claim form detail. Reason: This Claim form  was already approved.", Me, "../Index.aspx")
                                If dsList IsNot Nothing Then dsList.Clear()
                                dsList = Nothing
                                objExpApproverTran = Nothing

                                'Pinkal (23-Feb-2024) -- Start
                                '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                Session("ApprovalLink") = Nothing
                                Session("ApproverUserId") = Nothing

                                If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                        Session.Abandon()
                                        If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                            Response.Cookies("ASP.NET_SessionId").Value = ""
                                            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                        End If

                                        If Request.Cookies("AuthToken") IsNot Nothing Then
                                            Response.Cookies("AuthToken").Value = ""
                                            Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                        End If

                                    End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                                'Pinkal (23-Feb-2024) -- End

                                Exit Sub
                            End If
                            Session("crmasterunkid") = CInt(dsList.Tables("List").Rows(0)("crmasterunkid"))
                            Session("approverunkid") = CInt(dsList.Tables("List").Rows(0)("crapproverunkid"))
                            Session("approverEmpID") = CInt(dsList.Tables("List").Rows(0)("approveremployeeunkid"))
                            Session("Priority") = CInt(dsList.Tables("List").Rows(0)("crpriority"))
                            Session("isexternalapprover") = CBool(dsList.Tables("List").Rows(0)("isexternalapprover"))
                        End If

                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objExpApproverTran = Nothing

                    End If  'If arr.Length = 7 Then

                Else
                    Exit Sub

                End If  '   If Request.QueryString.Count > 0 Then

            End If  ' If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

            If IsPostBack = False Then

                SetLanguage()
                GC.Collect()

                If Session("CompName").ToString().ToUpper() = "KADCO" Then
                    lnkShowFuelConsumptionReport.Visible = True
                Else
                    lnkShowFuelConsumptionReport.Visible = False
                End If

                If Session("crmasterunkid") IsNot Nothing AndAlso Session("approverunkid") IsNot Nothing AndAlso Session("approverEmpID") IsNot Nothing AndAlso Session("Priority") IsNot Nothing Then
                    Me.ViewState.Add("mintClaimRequestMasterId", Session("crmasterunkid"))
                    Me.ViewState.Add("mintClaimApproverId", Session("approverunkid"))
                    Me.ViewState.Add("mintClaimApproverEmpID", Session("approverEmpID"))
                    Me.ViewState.Add("mintApproverPriority", Session("Priority"))
                    If Session("blnIsFromLeave") IsNot Nothing Then
                        Me.ViewState.Add("mblnIsFromLeave", Session("blnIsFromLeave"))
                    Else
                        Me.ViewState.Add("mblnIsFromLeave", False)
                    End If
                    Dim mdtTran As DataTable = Nothing
                    Me.ViewState.Add("mdtTran", mdtTran)
                    Me.ViewState.Add("MaxDate", CDate(eZeeDate.convertDate("99981231")))
                    Me.ViewState.Add("MinDate", CDate(eZeeDate.convertDate("17530101")))
                    Me.ViewState.Add("PDate", ConfigParameter._Object._CurrentDateAndTime.Date)
                    Me.ViewState.Add("mblnIsLeaveEncashment", False)
                    Me.ViewState.Add("EditGuid", "Null")
                    Me.ViewState.Add("EditCrtranunkid", 0)

                    SetVisibility()
                    txtUnitPrice.Enabled = False
                    cboExpCategory.Enabled = False
                    cboPeriod.Enabled = False
                    dtpDate.Enabled = False

                    Dim objClaimMaster As New clsclaim_request_master
                    Dim objClaimTran As New clsclaim_request_tran
                    Dim objExpApproverTran As New clsclaim_request_approval_tran

                    objClaimMaster._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
                    FillCombo(objClaimMaster._Employeeunkid)

                    If CBool(Me.ViewState("mblnIsFromLeave")) Then
                        Enable_Disable_Ctrls(False)
                    Else
                        Enable_Disable_Ctrls(True)
                    End If

                    Call GetValue(objClaimMaster)

                    objClaimTran._ClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))

                    mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
                                                                                                    , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpCategory.SelectedValue) _
                                                                                                    , True, True, CInt(Me.ViewState("mintClaimApproverId")), "", CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing).Tables(0)

                    cboLeaveType.Enabled = False
                    mblnIsExternalApprover = CBool(Session("IsExternalApprover"))
                    Me.ViewState("mdtTran") = mdtTran.Copy()

                    Call Fill_Expense()

                    If mdtTran IsNot Nothing Then mdtTran.Clear()
                    mdtTran = Nothing

                    objExpApproverTran = Nothing
                    objClaimTran = Nothing
                    objClaimMaster = Nothing

                Else
                    Response.Redirect("~/Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
                End If

                'Dim objGroup As New clsGroup_Master
                'objGroup._Groupunkid = 1
                'mstrGroupName = objGroup._Groupname.ToUpper.Trim()
                'If mstrGroupName.ToUpper().Trim() = "NMB PLC" Then
                '    txtClaimRemark.Attributes.Add("maxlength", "80")
                '    btnReturntoApplicant.Visible = True
                '    btnAdd.Visible = False
                '    dgvData.Columns(getColumnId_Datagrid(dgvData, "objdgcolhDelete", False, True)).Visible = False
                'Else
                '    btnReturntoApplicant.Visible = False
                'End If
                'objGroup = Nothing

                If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
                    txtClaimRemark.Attributes.Add("maxlength", "80")
                    btnReturntoApplicant.Visible = True
                    btnAdd.Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "objdgcolhDelete", False, True)).Visible = False
                Else
                    btnReturntoApplicant.Visible = False
                End If

            Else
                mintClaimFormEmpId = CInt(Me.ViewState("ClaimFormEmpId"))
                mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))
                mblnIsExpenseEditClick = CBool(Me.ViewState("mblnIsExpenseEditClick"))
                mblnIsExpenseApproveClick = CBool(Me.ViewState("mblnIsExpenseApproveClick"))
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
                mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsClaimFormBudgetMandatory"))
                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mintClaimRequestApprovalExpTranId = CInt(Me.ViewState("ClaimRequestApprovalExpTranId"))
                mstrClaimRequestApprovalExpGUID = Me.ViewState("ClaimRequestApprovalExpGUID").ToString()
                mintSelectedPassenger = CInt(Me.ViewState("mintSelectedPassenger"))
                mstrJournal = Me.ViewState("Journal").ToString()
                mstrPaymentMode = Me.ViewState("PaymentMode").ToString()
                mblnIsDebit = CBool(Me.ViewState("IsDebit"))
                mblnIsCredit = CBool(Me.ViewState("IsCredit"))
                mblnReturntoApplicant = CBool(Me.ViewState("ReturntoApplicant"))
                mstrGroupName = Me.ViewState("GroupName").ToString()

                mblnIsLeaveEncashment = CBool(Me.ViewState("mblnIsLeaveEncashment"))
                mblnIsSecRouteMandatory = CBool(Me.ViewState("IsSecRouteMandatory"))
                mblnIsConsiderDependants = CBool(Me.ViewState("IsConsiderDependants"))
                mblnDoNotAllowToApplyForBackDate = CBool(Me.ViewState("DoNotAllowToApplyForBackDate"))
                mintGLCodeID = Me.ViewState("GLCodeId")
                mstrDescription = Me.ViewState("Description")
                mintExpenditureTypeId = Me.ViewState("ExpenditureTypeId")
                mblnIsaccrue = CBool(Me.ViewState("Isaccrue"))
                mintUomunkid = Me.ViewState("Uomunkid")
                mintAccrueSettingId = Me.ViewState("AccrueSetting")
                mintExpense_MaxQuantity = Me.ViewState("Expense_MaxQuantity")
                mblnIsAttachDocMandetory = Me.ViewState("IsAttachDocMandetory")
                mblnIsUnitPriceEditable = Me.ViewState("IsUnitPriceEditable")
                mintJournalunkid = Me.ViewState("Journalunkid")
                mintPaymentmodeunkid = Me.ViewState("Paymentmodeunkid")
                mblnIsDebit = Me.ViewState("IsDebit")
                mblnIsCredit = Me.ViewState("IsCredit")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'Pinkal (13-Aug-2020) -- Start
        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Try
            Session.Remove("crmasterunkid")
            Session.Remove("approverunkid")
            Session.Remove("approverEmpID")
            Session.Remove("Priority")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("ClaimFormEmpId", mintClaimFormEmpId)

            If Session("IsExternalApprover") IsNot Nothing Then
                mblnIsExternalApprover = CBool(Session("IsExternalApprover"))
                Session("IsExternalApprover") = Nothing
            End If
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover
            Me.ViewState("mblnIsExpenseEditClick") = mblnIsExpenseEditClick
            Me.ViewState("mblnIsExpenseApproveClick") = mblnIsExpenseApproveClick
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
            Me.ViewState("IsClaimFormBudgetMandatory") = mblnIsClaimFormBudgetMandatory
            Me.ViewState("IsHRExpense") = mblnIsHRExpense
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("ClaimRequestApprovalExpTranId") = mintClaimRequestApprovalExpTranId
            Me.ViewState("ClaimRequestApprovalExpGUID") = mstrClaimRequestApprovalExpGUID
            Me.ViewState("mintSelectedPassenger") = mintSelectedPassenger
            Me.ViewState("Journal") = mstrJournal
            Me.ViewState("PaymentMode") = mstrPaymentMode
            Me.ViewState("IsDebit") = mblnIsDebit
            Me.ViewState("IsCredit") = mblnIsCredit
            Me.ViewState("ReturntoApplicant") = mblnReturntoApplicant
            Me.ViewState("GroupName") = mstrGroupName

            Me.ViewState("mblnIsLeaveEncashment") = mblnIsLeaveEncashment
            Me.ViewState("IsSecRouteMandatory") = mblnIsSecRouteMandatory
            Me.ViewState("IsConsiderDependants") = mblnIsConsiderDependants
            Me.ViewState("DoNotAllowToApplyForBackDate") = mblnDoNotAllowToApplyForBackDate
            Me.ViewState("GLCodeId") = mintGLCodeID
            Me.ViewState("Description") = mstrDescription
            Me.ViewState("ExpenditureTypeId") = mintExpenditureTypeId
            Me.ViewState("Isaccrue") = mblnIsaccrue
            Me.ViewState("Uomunkid") = mintUomunkid
            Me.ViewState("AccrueSetting") = mintAccrueSettingId
            Me.ViewState("Expense_MaxQuantity") = mintExpense_MaxQuantity
            Me.ViewState("IsAttachDocMandetory") = mblnIsAttachDocMandetory
            Me.ViewState("IsUnitPriceEditable") = mblnIsUnitPriceEditable
            Me.ViewState("Journalunkid") = mintJournalunkid
            Me.ViewState("Paymentmodeunkid") = mintPaymentmodeunkid
            Me.ViewState("IsDebit") = mblnIsDebit
            Me.ViewState("IsCredit") = mblnIsCredit

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo(ByVal xEmployeeId As Integer)
        'Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim dsCombo As New DataSet
        Try

            Dim objExpenseCategory As New clsexpense_category_master
            dsCombo = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing

            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())


            'If CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
            '    Dim objcommonMst As New clsCommon_Master
            '    dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            '    With cboSectorRoute
            '        .DataValueField = "masterunkid"
            '        .DataTextField = "name"
            '        .DataSource = dsCombo.Tables(0)
            '        .DataBind()
            '    End With

            '    If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
            '        With cboTravelFrom
            '            .DataValueField = "masterunkid"
            '            .DataTextField = "name"
            '            .DataSource = dsCombo.Tables(0).Copy
            '            .DataBind()
            '            .SelectedIndex = 0
            '        End With
            '        With cboTravelTo
            '            .DataValueField = "masterunkid"
            '            .DataTextField = "name"
            '            .DataSource = dsCombo.Tables(0).Copy
            '            .DataBind()
            '            .SelectedIndex = 0
            '        End With
            '    End If

            'End If

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            True, "List", True, xEmployeeId, , , , , , , , , , , , , , , , False)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = xEmployeeId.ToString()
            End With

            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
                .DataBind()
            End With
            objCostCenter = Nothing

            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()

                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            objExchange = Nothing

            dsCombo = clsExpCommonMethods.GetRebatePercentage(False, "List")
            With cboRebatePercent
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 1
                .DataBind()
            End With

            dsCombo = clsExpCommonMethods.GetRebateSeatStatus(True, "List")
            With cboSeatStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'objPrd = Nothing
            objEMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_Expense()
        Dim mdtTran As DataTable = Nothing
        Try
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If

            Dim mdView As DataView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D' AND iscancel = 0 "
            dgvData.DataSource = mdView
            dgvData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D' AND iscancel = 0")
                If dTotal.Length > 0 Then
                    If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                        dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Visible = True
                        dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhcurrency", False, True)).Visible = True
                        txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(base_amount)", "AUD<>'D' AND iscancel = 0 ")), Session("fmtCurrency").ToString())
                    Else
                        dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Visible = False
                        dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhcurrency", False, True)).Visible = False
                        txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0 ")), Session("fmtCurrency").ToString())
                    End If
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If

            If dgvData.Items.Count <= 0 Then
                dtpDate.Enabled = True
            End If
            
            If dgvData.Items.Count > 0 Then
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbeneficetranunkid") <> 0).Count > 0 Then
                    txtOthers.Enabled = False
                Else
                    txtOthers.Enabled = True
                End If

                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("other_person") <> "").Count > 0 Then
                    cboDependant.SelectedValue = 0
                    cboDependant.Enabled = False
                Else
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.SelectedValue = 0
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.Enabled = True
                End If
            Else
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.SelectedValue = 0
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.Enabled = True
                txtOthers.Enabled = True
            End If

            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                        If dgvData.Items.Count > 0 Then
                            If cboRebatePercent.Enabled = True Then cboRebatePercent.Enabled = False
                            If cboSeatStatus.Enabled = True Then cboSeatStatus.Enabled = False
                        Else
                            If cboRebatePercent.Enabled = False Then cboRebatePercent.Enabled = True
                            If cboSeatStatus.Enabled = False Then cboSeatStatus.Enabled = True
                        End If
                End Select
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
        End Try
    End Sub

    Private Sub GetValue(ByRef objClaimMaster As clsclaim_request_master)
        Try

            If Me.ViewState("EmployeeId") Is Nothing Then
                Me.ViewState.Add("EmployeeId", objClaimMaster._Employeeunkid)
            Else
                Me.ViewState("EmployeeId") = objClaimMaster._Employeeunkid
            End If

            txtClaimNo.Text = objClaimMaster._Claimrequestno

            'If CBool(Session("SectorRouteAssignToEmp")) Then
            '    Dim objAssignEmp As New clsassignemp_sector
            '    Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
            '    With cboSectorRoute
            '        .DataValueField = "secrouteunkid"
            '        .DataTextField = "Sector"
            '        .DataSource = dtTable
            '        .DataBind()
            '        .SelectedIndex = 0
            '    End With
            '    objAssignEmp = Nothing
            '    If dtTable IsNot Nothing Then dtTable.Clear()
            '    dtTable = Nothing
            'End If


            cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid.ToString()
            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())

            dtpDate.SetDate = objClaimMaster._Transactiondate.Date
            Call dtpDate_TextChanged(dtpDate, Nothing)

            txtClaimRemark.Text = objClaimMaster._Claim_Remark
            cboLeaveType.SelectedValue = objClaimMaster._LeaveTypeId.ToString()

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                Call cboLeaveType_SelectedIndexChanged(Nothing, Nothing)
            End If
            cboReference.SelectedValue = objClaimMaster._Referenceunkid.ToString()

            cboRebatePercent.SelectedValue = objClaimMaster._RebateId
            cboSeatStatus.SelectedValue = objClaimMaster._SeatTypeId

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            End If
            btnApprove.Visible = Not CBool(Me.ViewState("mblnIsFromLeave"))
            btnReject.Visible = Not CBool(Me.ViewState("mblnIsFromLeave"))
            btnOK.Visible = CBool(Me.ViewState("mblnIsFromLeave"))
            objlblValue.Visible = False
            cboReference.Visible = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Dim mdtTran As DataTable = Nothing
        Try
            cboReference.Enabled = iFlag
            cboLeaveType.Enabled = iFlag

            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If

            If mdtTran IsNot Nothing Then
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then

                    If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                        cboCurrency.Enabled = True
                    Else
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First().ToString()
                    End If

                Else
                    Select Case CInt(cboExpCategory.SelectedValue)
                        Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        Case Else
                            cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                            cboCurrency.Enabled = True
                    End Select

                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbeneficetranunkid") <> 0).Count > 0 Then
                        txtOthers.Enabled = False
                    Else
                        txtOthers.Enabled = True
                    End If

                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("other_person") <> "").Count > 0 Then
                        cboDependant.SelectedValue = 0
                        cboDependant.Enabled = False
                    Else
                        If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.SelectedValue = 0
                        If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then cboDependant.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = "0"
            cboSectorRoute.SelectedValue = "0"
            Call cboExpense_SelectedIndexChanged(cboExpense, Nothing)
            txtExpRemark.Text = ""
            txtQty.Text = "1"
            txtUnitPrice.Text = "1.00"
            mintEmpMaxCountDependentsForCR = 0
            mintClaimRequestApprovalExpTranId = 0
            mstrClaimRequestApprovalExpGUID = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If dgvData.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please add atleast one expense in order to do futher operation."), Me)
                dgvData.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function SaveData(ByVal sender As Object) As Boolean
        Dim blnFlag As Boolean = False
        Dim blnLastApprover As Boolean = False
        Dim mstrRejectRemark As String = ""
        Dim mintMaxPriority As Integer = -1
        Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
        Dim objExpApproverTran As New clsclaim_request_approval_tran
        Dim objClaimMst As New clsclaim_request_master

        Try



            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING
            Dim mdtApprovalDate As DateTime = Nothing

            objExpApproverTran._DataTable = mdtTran

            Dim drRow() As DataRow = mdtTran.Select("AUD = ''")

            If drRow.Length > 0 Then
                For Each dr As DataRow In drRow
                    dr("AUD") = "U"
                    dr.AcceptChanges()
                Next
            End If

            If mdtTran.Columns.Contains("crapprovaltranunkid") Then
                mdtTran.Columns.Remove("crapprovaltranunkid")
            End If

            If CBool(Me.ViewState("mblnIsFromLeave")) Then Return True

            Dim mblnIssued As Boolean = False
            Dim objLeaveApprover As clsleaveapprover_master = Nothing
            Dim objExpAppr As New clsExpenseApprover_Master

            Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                  , Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                                  , Session("EmployeeAsOnDate").ToString(), CInt(cboExpCategory.SelectedValue) _
                                                                                                                  , False, True, -1, "", CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing)



            Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & CInt(Me.ViewState("mintApproverPriority")), "", DataViewRowState.CurrentRows).ToTable().Copy()

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing

            mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            objClaimMst._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
            objExpApproverTran._WebFormName = "frmExpenseApproval"
            objExpApproverTran._WebClientIP = Session("IP_ADD").ToString()
            objExpApproverTran._WebHostName = Session("HOST_NAME").ToString()

            objClaimMst._WebFormName = "frmExpenseApproval"
            objClaimMst._WebClientIP = Session("IP_ADD").ToString()
            objClaimMst._WebHostName = Session("HOST_NAME").ToString()



            If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                objLeaveApprover = New clsleaveapprover_master
                objLeaveApprover._Approverunkid = CInt(Me.ViewState("mintClaimApproverId"))

                Dim objLeaveForm As New clsleaveform
                objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = CInt(Me.ViewState("mintApproverPriority"))) Then mblnIssued = True

                objLeaveForm = Nothing
            End If


            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmExpenseApproval"
            StrModuleName2 = "mnuUtilitiesMain"
            StrModuleName3 = "mnuClaimsExpenses"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            Dim mintApproverApplicationStatusId As Integer = 2
            Dim mblnIsRejected As Boolean = False

            For i As Integer = 0 To dtApproverTable.Rows.Count - 1
                blnLastApprover = False
                Dim mintApproverEmpID As Integer = -1
                Dim mintApproverID As Integer = -1

                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                    objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))
                    If CInt(Me.ViewState("mintClaimApproverId")) = objLeaveApprover._Approverunkid Then

                        If CType(sender, Button).ID.ToUpper = "BTNAPPROVE" Then
                            mintStatusID = 1
                            mintVisibleID = 1
                            mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID

                        ElseIf CType(sender, Button).ID.ToUpper = "BTNREMARKOK" Then

                            If mblnReturntoApplicant = False Then
                                mintStatusID = 3
                                mintVisibleID = 3
                                mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                                blnLastApprover = True
                                mblnIsRejected = True
                                mintApproverApplicationStatusId = mintStatusID
                                mstrRejectRemark = txtRejectRemark.Text.Trim

                            ElseIf mblnReturntoApplicant AndAlso mblnIsRejected = False Then
                                mintStatusID = 8
                                mintVisibleID = 8
                                mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                                blnLastApprover = True
                                mblnIsRejected = False
                                mintApproverApplicationStatusId = mintStatusID
                                mstrRejectRemark = txtRejectRemark.Text.Trim
                            End If

                        End If

                    Else

                        Dim mintPriority As Integer = -1

                        Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & CInt(Me.ViewState("mintApproverPriority")) & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

                        If dRow.Length > 0 Then
                            mintStatusID = 2
                            If mblnReturntoApplicant Then mintVisibleID = -1 Else mintVisibleID = 1
                            mdtApprovalDate = Nothing

                        Else
                            mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))))
                            If mintPriority <= -1 Then
                                GoTo AssignApprover
                                mintStatusID = 2
                                mintVisibleID = 1
                                GoTo AssignApprover

                            ElseIf mblnIsRejected OrElse mblnReturntoApplicant Then
                                mintVisibleID = -1

                            ElseIf CInt(Me.ViewState("mintApproverPriority")) = CInt(dtApproverTable.Rows(i)("crpriority")) Then 'Pinkal (13-Jul-2015) -- End
                                mintVisibleID = 1
                            ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                                mintVisibleID = 2
                            Else
                                mintVisibleID = -1
                            End If
                            mintStatusID = 2
                            mdtApprovalDate = Nothing
                        End If

                    End If
AssignApprover:
                    mintApproverEmpID = objLeaveApprover._leaveapproverunkid
                    mintApproverID = objLeaveApprover._Approverunkid


                Else    'If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then

                    objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))

                    If CInt(Me.ViewState("mintClaimApproverId")) = objExpAppr._crApproverunkid Then

                        If CType(sender, Button).ID.ToUpper = btnApprove.ID.ToUpper Then
                            mintStatusID = 1
                            mintVisibleID = 1
                            mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID

                        ElseIf CType(sender, Button).ID.ToUpper = btnRemarkOk.ID.ToUpper Then

                            If mblnReturntoApplicant = False Then
                                mintStatusID = 3
                                mintVisibleID = 3
                                mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                                blnLastApprover = True
                                mblnIsRejected = True
                                mintApproverApplicationStatusId = mintStatusID
                                mstrRejectRemark = txtRejectRemark.Text.Trim

                            ElseIf mblnReturntoApplicant AndAlso mblnIsRejected = False Then
                                mintStatusID = 8
                                mintVisibleID = 8
                                mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                                blnLastApprover = True
                                mblnIsRejected = False
                                mintApproverApplicationStatusId = mintStatusID
                                mstrRejectRemark = txtRejectRemark.Text.Trim
                            End If

                        End If

                    Else
                        Dim mintPriority As Integer = -1

                        Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & CInt(Me.ViewState("mintApproverPriority")) & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

                        If dRow.Length > 0 Then
                            mintStatusID = 2
                            If mblnReturntoApplicant Then mintVisibleID = -1 Else mintVisibleID = 1
                            mdtApprovalDate = Nothing

                        Else
                            mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))))

                            If mintPriority <= -1 Then GoTo AssignApprover1

                            If mblnIsRejected OrElse mblnReturntoApplicant Then
                                mintVisibleID = -1

                            ElseIf CInt(Me.ViewState("mintApproverPriority")) = CInt(dtApproverTable.Rows(i)("crpriority")) Then 'Pinkal (13-Jul-2015) -- End
                                mintVisibleID = 1

                            ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                                mintVisibleID = 2

                            Else
                                mintVisibleID = -1
                            End If

                            mintStatusID = 2
                            mdtApprovalDate = Nothing
                        End If

                    End If
AssignApprover1:
                    mintApproverEmpID = objExpAppr._Employeeunkid
                    mintApproverID = objExpAppr._crApproverunkid
                End If

                objExpApproverTran._YearId = CInt(Session("Fin_Year"))
                objExpApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                objExpApproverTran._EmployeeID = CInt(Me.ViewState("EmployeeId"))
                objExpApproverTran._RebateId = CInt(cboRebatePercent.SelectedValue)
                objExpApproverTran._RebatePercentage = CDec(cboRebatePercent.SelectedItem.Text)
                objExpApproverTran._CompanyID = CInt(Session("CompanyUnkId"))
                objExpApproverTran._CompanyCode = Session("CompanyCode")

                If blnLastApprover AndAlso mintStatusID = 1 Then
                    If mdtTran IsNot Nothing Then
                        For Each dRow As DataRow In mdtTran.Rows
                            Dim sMsg As String = String.Empty

                            sMsg = objClaimMst.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                        , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), False)
                            If sMsg <> "" Then
                                DisplayMessage.DisplayMessage(sMsg, Me)
                                Return False
                            End If
                            sMsg = ""
                        Next
                    End If
                End If

                If objExpApproverTran.Insert_Update_ApproverData(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                           , False, CBool(Session("PaymentApprovalwithLeaveApproval")), mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, CInt(Session("UserId")), Now.Date _
                                                                                           , CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing, mstrRejectRemark _
                                                                                           , blnLastApprover, mdtApprovalDate) = False Then
                    Return False
                End If

            Next

            objClaimMst._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))

            If CInt(Session("EFTIntegration")) = enEFTIntegration.DYNAMICS_NAVISION Then

                If objClaimMst._Statusunkid = 1 Then
                    Dim objCurrentPeriodId As New clsMasterData
                    Dim objPeriod As New clscommom_period_Tran
                    Dim mintCurrentPeriodId As Integer = objCurrentPeriodId.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1, , True)
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = mintCurrentPeriodId
                    objClaimMst.SendToDynamicNavision(CInt(Me.ViewState("mintClaimRequestMasterId")), CInt(Session("UserId")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, _
                                                           CStr(Session("SQLDataSource")), _
                                                           CStr(Session("SQLDatabaseName")), _
                                                           CStr(Session("SQLDatabaseOwnerName")), _
                                                           CStr(Session("SQLUserName")), _
                                                           CStr(Session("SQLUserPassword")), _
                                                           CStr(Session("FinancialYear_Name")))

                End If

            End If

            If mintApproverApplicationStatusId = 8 Then  'Return To Applicant
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(Me.ViewState("EmployeeId"))
                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email
                objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), txtClaimNo.Text.Trim, mintApproverApplicationStatusId, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), mstrRejectRemark.Trim, True)
                objEmployee = Nothing
            End If

            If objClaimMst._Statusunkid = 2 AndAlso mintApproverApplicationStatusId = 1 Then
                objExpApproverTran._WebClientIP = Session("IP_ADD").ToString()
                objExpApproverTran._WebHostName = Session("HOST_NAME").ToString()
                objExpApproverTran._WebFormName = "frmExpenseApproval"

                objExpApproverTran.SendMailToApprover(CInt(cboExpCategory.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(Me.ViewState("mintClaimRequestMasterId")) _
                                                                            , txtClaimNo.Text.Trim, CInt(Me.ViewState("EmployeeId")), CInt(Me.ViewState("mintApproverPriority")), 1, "crpriority > " & CInt(Me.ViewState("mintApproverPriority")) _
                                                                            , Session("Database_Name").ToString(), Session("EmployeeAsOnDate").ToString(), CInt(Session("CompanyUnkId")) _
                                                                            , Session("ArutiSelfServiceURL").ToString, enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), mstrModuleName, Nothing, True)
            End If


            GUI.fmtCurrency = Session("fmtCurrency").ToString
            If objClaimMst._Statusunkid = 1 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objClaimMst._Employeeunkid

                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email

                If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                    objLeaveForm._EmployeeCode = objEmployee._Employeecode
                    objLeaveForm._EmployeeFirstName = objEmployee._Firstname
                    objLeaveForm._EmployeeMiddleName = objEmployee._Othername
                    objLeaveForm._EmployeeSurName = objEmployee._Surname
                    objLeaveForm._EmpMail = objEmployee._Email
                    objLeaveForm._WebFrmName = "frmExpenseApproval"
                    objLeaveForm._WebClientIP = Session("IP_ADD").ToString()
                    objLeaveForm._WebHostName = Session("HOST_NAME").ToString()

                    If objLeaveForm._Statusunkid = 7 Then 'only Issued
                        Dim objCommonCode As New CommonCodes
                        Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
                        Dim mstrPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                                     , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, True _
                                                                                                                     , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))


                        objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, cboLeaveType.Text, objLeaveForm._Startdate, objLeaveForm._Returndate, objLeaveForm._Statusunkid _
                                                                            , CInt(Session("CompanyUnkId")), "", Path, mstrPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "" _
                                                                            , CInt(Me.ViewState("mintClaimRequestMasterId")), objLeaveForm._Formno)

                        objCommonCode = Nothing

                    End If
                    objLeaveForm = Nothing
                Else
                    Dim strFilePath As String = ""
                    Dim strFileName As String = ""
                    If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_DUTY OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                        strFilePath = My.Computer.FileSystem.SpecialDirectories.Temp

                        Dim objClaimForm As New clsEmployeeClaimForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
                        objClaimForm._ExpenseCategoryId = objClaimMst._Expensetypeid
                        objClaimForm._EmployeeId = objClaimMst._Employeeunkid
                        objClaimForm._ClaimFormId = objClaimMst._Crmasterunkid
                        objClaimForm.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                 , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CStr(Session("UserAccessModeSetting")) _
                                                                 , True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), 0, Aruti.Data.enPrintAction.None, enExportAction.None, 0)

                        Session("objRpt") = objClaimForm._Rpt

                        strFileName = "RebateClaimForm" & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".pdf"

                        If objClaimForm._Rpt IsNot Nothing Then
                            Dim oStream As New IO.MemoryStream
                            oStream = objClaimForm._Rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
                            Dim fs As New System.IO.FileStream(strFilePath & "\" & strFileName, IO.FileMode.Create, IO.FileAccess.Write)
                            Dim data As Byte() = oStream.ToArray()
                            fs.Write(data, 0, data.Length)
                            fs.Close()
                            fs.Dispose()
                        End If
                        objClaimForm = Nothing
                    End If


                    objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), strFilePath, strFileName, enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "", True)
                    
                End If
                objEmployee = Nothing

            ElseIf objClaimMst._Statusunkid = 3 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objClaimMst._Employeeunkid
                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email
                objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRejectRemark.Text.Trim, True)

                objEmployee = Nothing
            End If

            If blnLastApprover AndAlso mintStatusID = 1 Then
                If mdtTran IsNot Nothing Then
                    objExpApproverTran.SendMailToExpensewiseUser(mdtTran, CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), "", True)
                End If
            End If

            If objExpApproverTran._ClaimEmailList IsNot Nothing AndAlso objExpApproverTran._ClaimEmailList.Count > 0 Then
                objClaimEmailList.AddRange(objExpApproverTran._ClaimEmailList)
            End If

            If objClaimMst._ClaimEmailList IsNot Nothing AndAlso objClaimMst._ClaimEmailList.Count > 0 Then
                objClaimEmailList.AddRange(objClaimMst._ClaimEmailList)
            End If

            If objClaimEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = CInt(Session("CompanyUnkId"))
                    objThread.Start(arr)
                Else
                    Call Send_Notification(CInt(Session("CompanyUnkId")))
                End If
            End If

            If dtApproverTable IsNot Nothing Then dtApproverTable.Clear()
            dtApproverTable = Nothing
            objLeaveApprover = Nothing
            objExpAppr = Nothing

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            objClaimMst = Nothing
            objExpApproverTran = Nothing
        End Try
    End Function

    Private Sub AddExpense()
        Dim mdtTran As DataTable = Nothing
        Try

            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            Dim dtmp() As DataRow = Nothing


            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                            "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim().Replace("'", "''") & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below."), Me)
                    Exit Sub
                End If


                dtmp = Nothing
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                        If mdtTran.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("dpndtbeneficetranunkid")) <> 0).Count() > 0 Then
                            dtmp = mdtTran.Select("dpndtbeneficetranunkid = '" & CInt(cboDependant.SelectedValue) & "' AND quantity > 0 AND AUD <> 'D'")
                        ElseIf mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("other_person").ToString().Trim().Length > 0).Count() > 0 Then
                            dtmp = mdtTran.Select("other_person = '" & txtOthers.Text.Trim().Replace("'", "''") & "' AND quantity > 0 AND AUD <> 'D'")
                        End If
                        If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then If txtQty.Text.Trim.Length > 0 Then txtQty.Text = 0
                End Select

            End If

            'Dim objExpMaster As New clsExpense_Master
            'objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)



            'If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
            If mblnDoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If

            'mblnIsHRExpense = objExpMaster._IsHRExpense

            'mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

            'mintGLCodeID = objExpMaster._GLCodeId
            mstrGLCode = ""
            'mstrGLDescription = objExpMaster._Description
            mstrGLDescription = mstrDescription
            mintCostCenterID = 0
            mstrCostCenterCode = ""

            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                'If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                If mintExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    'objExpMaster = Nothing
                    Exit Sub
                End If
                'GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                GetP2PRequireData(mintExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If


            'Dim objCommon As New clsCommon_Master
            'objCommon._Masterunkid = objExpMaster._Journalunkid
            'mstrJournal = objCommon._Name
            'objCommon._Masterunkid = objExpMaster._Paymentmodeunkid
            'mstrPaymentMode = objCommon._Name
            'mblnIsDebit = objExpMaster._P2PDebit
            'mblnIsCredit = objExpMaster._P2PCredit
            'objCommon = Nothing

            Dim objCommon As New clsCommon_Master
            objCommon._Masterunkid = mintJournalunkid
            mstrJournal = objCommon._Name
            objCommon._Masterunkid = mintPaymentmodeunkid
            mstrPaymentMode = objCommon._Name
            objCommon = Nothing



            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            If mblnIsaccrue OrElse mblnIsLeaveEncashment Then
                Dim iValue As Decimal = 0
                'If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                If mintUomunkid = enExpUoM.UOM_QTY Then
                    'If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                    If mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
                                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
                                Else
                                    iValue = CDec(txtBalance.Text)
                                End If
                            Case Else
                                iValue = CDec(txtQty.Text)
                        End Select
                        If iValue > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            'objExpMaster = Nothing
                            Exit Sub
                        End If
                        'ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                    ElseIf mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
                                    iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
                                Else
                                    iValue = CDec(txtBalance.Text)
                                End If
                            Case Else
                                iValue = CDec(txtQty.Text)
                        End Select
                        If iValue > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtQty.Focus()
                            'objExpMaster = Nothing
                            Exit Sub
                        End If
                    End If
                    'ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                ElseIf mintUomunkid = enExpUoM.UOM_AMOUNT Then
                    Select Case CInt(cboExpCategory.SelectedValue)
                        Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                            If CInt(IIf(cboRebatePercent.SelectedItem.Text = "", 0, cboRebatePercent.SelectedItem.Text)) >= 100 Then
                                iValue = CDec(IIf(txtGrandTotal.Text.Trim.Length <= 0, 0, txtGrandTotal.Text)) + CDec(txtQty.Text)
                            Else
                                iValue = CDec(txtBalance.Text)
                            End If
                        Case Else
                            iValue = CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text))
                    End Select
                    If CDec(txtBalance.Text) < iValue Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtQty.Focus()
                        'objExpMaster = Nothing
                        Exit Sub
                    End If
                End If
            End If

            Dim sMsg As String = String.Empty
            Dim objClaimMaster As New clsclaim_request_master
            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), True)
            objClaimMaster = Nothing
            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If
            sMsg = ""

            If CheckForOccurrence(mdtTran) = False Then Exit Sub


            'If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrGLCode, mstrCostCenterCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(IIf(txtQty.Text.Trim.Trim.Length > 0, CDec(txtQty.Text), 0)) * CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0))) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), Me)
                    Exit Sub
                End If
            End If

            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then   'crtranunkid
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then  'GUID
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

                'If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                If mblnIsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    objExpMasterOld = Nothing
                    Exit Sub

                    'ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                ElseIf mblnIsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    objExpMasterOld = Nothing
                    Exit Sub
                End If
                objExpMasterOld = Nothing
            End If

            'objExpMaster = Nothing

            Dim dRow As DataRow = mdtTran.NewRow
            dRow.Item("crapprovaltranunkid") = -1
            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = Me.ViewState("mintClaimRequestMasterId")
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
            dRow.Item("unitprice") = txtUnitPrice.Text
            dRow.Item("quantity") = txtQty.Text
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("iscancel") = False
            dRow.Item("canceluserunkid") = -1
            dRow.Item("cancel_datetime") = DBNull.Value
            dRow.Item("cancel_remark") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate

            If mdtTran.Columns.Contains("tdate") Then
                dRow.Item("tdate") = eZeeDate.convertDate(dtpDate.GetDate.Date).ToString()
            End If


            dRow.Item("airline") = txtAirline.Text.Trim


            If cboTravelFrom.SelectedItem IsNot Nothing Then dRow.Item("travelfrom") = cboTravelFrom.SelectedItem.Text
            If cboTravelTo.SelectedItem IsNot Nothing Then dRow.Item("travelto") = cboTravelTo.SelectedItem.Text

            dRow.Item("flightno") = txtFlightNo.Text.Trim
            If dtTravelDate.IsNull = False Then dRow.Item("traveldate") = dtTravelDate.GetDate()
            If txtOthers.Text.Trim.Length > 0 Then
                dRow.Item("name") = txtOthers.Text.Trim
                dRow.Item("relation") = ""
                dRow.Item("psgtype") = Language.getMessage("clsclaim_request_tran", 101, "Other")

            ElseIf CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue)) > 0 Then
                dRow.Item("name") = cboDependant.SelectedItem.Text
                Dim objCMstr As New clsCommon_Master
                Dim objDpndt As New clsDependants_Beneficiary_tran
                objDpndt._Dpndtbeneficetranunkid = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
                objCMstr._Masterunkid = objDpndt._Relationunkid
                dRow.Item("relation") = objCMstr._Name
                objCMstr = Nothing
                dRow.Item("psgtype") = Language.getMessage("clsclaim_request_tran", 100, "Dependant")

            Else
                dRow.Item("name") = cboEmployee.SelectedItem.Text
                dRow.Item("relation") = ""
                dRow.Item("psgtype") = Language.getMessage("clsclaim_request_tran", 102, "Self")

            End If

            dRow.Item("dpndtbeneficetranunkid") = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
            If txtOthers.Text.Trim.Length > 0 Then
                dRow.Item("other_person") = txtOthers.Text.Trim
            Else
                dRow.Item("other_person") = ""
            End If

            dRow.Item("fromrouteid") = CInt(IIf(cboTravelFrom.SelectedValue = "", 0, cboTravelFrom.SelectedValue))
            dRow.Item("torouteid") = CInt(IIf(cboTravelTo.SelectedValue = "", 0, cboTravelTo.SelectedValue))

            Dim intExpenseId As Integer = 0

            If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
            Else
                intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
            End If

            dRow.Item("deductexpenseunkid") = intExpenseId
            dRow.Item("expensetypeid") = CInt(cboExpCategory.SelectedValue)

            dRow.Item("claimrequestno") = txtClaimNo.Text.Trim
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("claim_remark") = txtClaimRemark.Text.Trim
            dRow.Item("journal") = mstrJournal
            dRow.Item("paymentmode") = mstrPaymentMode
            dRow.Item("isdebit") = mblnIsDebit
            dRow.Item("iscredit") = mblnIsCredit
            dRow.Item("currency_sign") = cboCurrency.SelectedItem.Text
            mdtTran.Rows.Add(dRow)
            Me.ViewState("mdtTran") = mdtTran.Copy()
            Fill_Expense()
            Enable_Disable_Ctrls(False)
            Clear_Controls()

            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                        If cboRebatePercent.Enabled = True Then cboRebatePercent.Enabled = False
                End Select
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
        End Try
    End Sub

    Private Sub EditExpense()
        Dim mdtTran As DataTable = Nothing
        Try
            Dim iRow As DataRow() = Nothing
            mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()

            'Dim objExpMaster As New clsExpense_Master
            'objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            'Dim mintGLCodeID As Integer = 0
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = ""
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""

            'mblnIsHRExpense = objExpMaster._IsHRExpense
            'mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

            'mintGLCodeID = objExpMaster._GLCodeId
            'mstrGLDescription = objExpMaster._Description
            mstrGLDescription = mstrDescription
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                'If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                If mintExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    'objExpMaster = Nothing
                    Exit Sub
                End If
                'GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                GetP2PRequireData(mintExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            'If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim, mstrGLCode, mstrCostCenterCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) * CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0))) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), Me)
                    'objExpMaster = Nothing
                    Exit Sub
                End If
            End If


            'Dim objCommon As New clsCommon_Master
            'objCommon._Masterunkid = objExpMaster._Journalunkid
            'mstrJournal = objCommon._Name
            'objCommon._Masterunkid = objExpMaster._Paymentmodeunkid
            'mstrPaymentMode = objCommon._Name
            'mblnIsDebit = objExpMaster._P2PDebit
            'mblnIsCredit = objExpMaster._P2PCredit
            'objCommon = Nothing

            Dim objCommon As New clsCommon_Master
            objCommon._Masterunkid = mintJournalunkid
            mstrJournal = objCommon._Name
            objCommon._Masterunkid = mintPaymentmodeunkid
            mstrPaymentMode = objCommon._Name
            objCommon = Nothing



            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            If mblnIsaccrue OrElse mblnIsLeaveEncashment Then
                'If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                If mintUomunkid = enExpUoM.UOM_QTY Then
                    'If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                    If mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            'objExpMaster = Nothing
                            Exit Sub

                        End If

                        'ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                    ElseIf mintAccrueSettingId = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtQty.Focus()
                            'objExpMaster = Nothing
                            Exit Sub
                        End If
                    End If

                    'ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                ElseIf mintUomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtQty.Focus()
                        'objExpMaster = Nothing
                        Exit Sub
                    End If
                End If
            End If


            Dim sMsg As String = String.Empty
            Dim objClaimMaster As New clsclaim_request_master
            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                      , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), False)
            objClaimMaster = Nothing

            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If
            sMsg = ""

            If CheckForOccurrence(mdtTran) = False Then Exit Sub

            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then   'crtranunkid
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then  'GUID
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

                'If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                If mblnIsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    objExpMasterOld = Nothing
                    Exit Sub

                    'ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                ElseIf mblnIsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    objExpMasterOld = Nothing
                    Exit Sub
                End If
                objExpMasterOld = Nothing
            End If

            'objExpMaster = Nothing


            If CInt(Me.ViewState("EditCrtranunkid")) <> 0 Then
                iRow = mdtTran.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
            Else
                iRow = mdtTran.Select("GUID = '" & CStr(Me.ViewState("EditGuid")) & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing AndAlso iRow.Length > 0 Then
                Dim dtmp() As DataRow = Nothing
                If iRow IsNot Nothing Then
                    If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                        dtmp = mdtTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                             "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim().Replace("'", "''") & "' AND AUD <> 'D'")
                    Else
                        dtmp = mdtTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                          "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim().Replace("'", "''") & "' AND AUD <> 'D'")
                    End If

                    If dtmp.Length > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below."), Me)
                        Exit Sub
                    End If
                    dtmp = Nothing

                End If

                iRow(0).Item("crapprovaltranunkid") = iRow(0).Item("crapprovaltranunkid")
                iRow(0).Item("crmasterunkid") = Me.ViewState("mintClaimRequestMasterId")
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("quantity") = txtQty.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("iscancel") = False
                iRow(0).Item("canceluserunkid") = -1
                iRow(0).Item("cancel_datetime") = DBNull.Value
                iRow(0).Item("cancel_remark") = ""
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)

                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense

                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)

                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                iRow(0).Item("airline") = txtAirline.Text.Trim
                If cboTravelFrom.SelectedItem IsNot Nothing Then iRow(0).Item("travelfrom") = cboTravelFrom.SelectedItem.Text
                If cboTravelTo.SelectedItem IsNot Nothing Then iRow(0).Item("travelto") = cboTravelTo.SelectedItem.Text

                iRow(0).Item("flightno") = txtFlightNo.Text.Trim
                If dtTravelDate.IsNull = False Then iRow(0).Item("traveldate") = dtTravelDate.GetDate()
                If txtOthers.Text.Trim.Length > 0 AndAlso txtOthers.Text.Trim <> "&nbsp;" Then
                    iRow(0).Item("name") = txtOthers.Text.Trim
                    iRow(0).Item("relation") = ""
                    iRow(0).Item("psgtype") = Language.getMessage("clsclaim_request_tran", 101, "Other")
                ElseIf CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue)) > 0 Then
                    iRow(0).Item("name") = cboDependant.SelectedItem.Text
                    Dim objCMstr As New clsCommon_Master
                    Dim objDpndt As New clsDependants_Beneficiary_tran
                    objDpndt._Dpndtbeneficetranunkid = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
                    objCMstr._Masterunkid = objDpndt._Relationunkid
                    iRow(0).Item("relation") = objCMstr._Name
                    objCMstr = Nothing
                    iRow(0).Item("psgtype") = Language.getMessage("clsclaim_request_tran", 100, "Dependant")
                Else
                    iRow(0).Item("name") = cboEmployee.SelectedItem.Text
                    iRow(0).Item("relation") = ""
                    iRow(0).Item("psgtype") = Language.getMessage("clsclaim_request_tran", 102, "Self")
                End If
                iRow(0).Item("dpndtbeneficetranunkid") = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
                iRow(0).Item("other_person") = txtOthers.Text.Trim
                iRow(0).Item("fromrouteid") = CInt(IIf(cboTravelFrom.SelectedValue = "", 0, cboTravelFrom.SelectedValue))
                iRow(0).Item("torouteid") = CInt(IIf(cboTravelTo.SelectedValue = "", 0, cboTravelTo.SelectedValue))
                iRow(0).Item("claimrequestno") = txtClaimNo.Text.Trim
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("claim_remark") = txtClaimRemark.Text.Trim
                iRow(0).Item("journal") = mstrJournal
                iRow(0).Item("paymentmode") = mstrPaymentMode
                iRow(0).Item("isdebit") = mblnIsDebit
                iRow(0).Item("iscredit") = mblnIsCredit
                iRow(0).Item("currency_sign") = cboCurrency.SelectedItem.Text

                mdtTran.AcceptChanges()
                Me.ViewState("mdtTran") = mdtTran.Copy()

                Fill_Expense()
                Clear_Controls()
                btnEdit.Visible = False

                If mstrGroupName.ToUpper().Trim() = "NMB PLC" Then
                    btnAdd.Visible = False
                Else
                    btnAdd.Visible = True
                End If

                cboExpense.Enabled = True

                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
        End Try
    End Sub

    Private Sub ApproveExpense()
        Try
            If SaveData(btnApprove) Then
                If mintClaimFormEmpId > 0 Then
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True _
                                            , Session("mdbname").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")) _
                                            , Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())

                    Session.Abandon()
                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If
                    Response.Redirect("../Index.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue))
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date)
            'Sohail (18 May 2019) -- End
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing
            If dtDependents IsNot Nothing Then dtDependents.Clear()
            dtDependents = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try

            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xGLCode As String, ByVal xCostCeneterCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), Me)
                Return False
            End If

            Dim objMaster As New clsMasterData
            mstrP2PToken = objMaster.GetP2PToken(CInt(Session("CompanyUnkId")), Nothing)

            If mstrP2PToken.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

                Dim mstrDimensionValue As String = ""

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboEmployee.SelectedValue)

                Dim objUnitGroup As New clsUnitGroup
                objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                Dim mstrUnitGroupCode As String = objUnitGroup._Code.ToString().Trim()
                objUnitGroup = Nothing

                'Dim objDepartment As New clsDepartment
                'objDepartment._Departmentunkid = objEmployee._Departmentunkid
                'Dim mstrDepartmentCode As String = objDepartment._Code.ToString().Trim
                'objDepartment = Nothing

                'Dim objSectionGroup As New clsSectionGroup
                'objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                'Dim mstrSectionGroupCode As String = objSectionGroup._Code.ToString().Trim
                'objSectionGroup = Nothing

                Dim objUnits As New clsUnits
                objUnits._Unitunkid = objEmployee._Unitunkid
                Dim mstrUnitCode As String = objUnits._Code.ToString().Trim
                objUnits = Nothing

                Dim objClassGroup As New clsClassGroup
                objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                Dim mstrClassGroupCode As String = objClassGroup._Code.ToString().Trim()
                objClassGroup = Nothing

                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                Dim mstrClassCode As String = objClass._Code.ToString().Trim()
                objClass = Nothing

                Dim objCostCenter As New clscostcenter_master
                objCostCenter._Costcenterunkid = objEmployee._Costcenterunkid
                Dim mstrCostCenterCode As String = objCostCenter._Costcentercode.ToString().Trim()
                Dim mstrCostCenterDescription As String = objCostCenter._Description.ToString().Trim()
                objCostCenter = Nothing

                objEmployee = Nothing


                ' Dimensionvalue : NMB – SUB UNIT I – FUNCTION – DEPARTMENT – ZONE – BRANCH

                'NMB(-Hardcoded)
                'Sub UNIT I - Unit Group
                'FUNCTION - Department
                'Department - Section Group
                'Zone - Class Group
                'Branch - Class

                'mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrDepartmentCode.Trim() & "-" & mstrSectionGroupCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                'GL- SubUnit I - SubUnit II - CostCenter - Zone - Branch
                'GL - GL Code
                'Sub UNIT I - Unit Group
                'Sub UNIT II - Unit
                'CostCenter - CostCenter
                'Zone - Class Group
                'Branch - Class


                'Pinkal (14-Jun-2024) -- Start
                'A1X -2649  - NMB - P2P API changes 

                'mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & xCostCeneterCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                If mstrCostCenterCode.Trim() <> mstrClassCode.Trim() AndAlso mstrCostCenterDescription.Trim.ToUpper = "ZONAL OFFICE" Then
                    mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & mstrCostCenterCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                ElseIf mstrCostCenterCode.Trim() <> mstrClassCode.Trim() AndAlso mstrCostCenterDescription.Trim.ToUpper <> "ZONAL OFFICE" Then
                    mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & mstrCostCenterCode.Trim() & "--"

                Else
                    mstrDimensionValue = xGLCode.Trim & "-" & mstrUnitGroupCode.Trim() & "-" & mstrUnitCode.Trim() & "-" & mstrCostCenterCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()
                End If

                'Pinkal (14-Jun-2024) -- End


                Dim xResponseData As String = ""
                Dim xPostedData As String = ""
                Dim objP2PBudgetBalanceService As New clsP2PBudgetBalanceService
                objP2PBudgetBalanceService.budgetBalanceRequest.CompanyCode = Session("CompanyCode").ToString()
                objP2PBudgetBalanceService.budgetBalanceRequest.GLCode = xGLCode.Trim
                objP2PBudgetBalanceService.budgetBalanceRequest.DimensionValue = mstrDimensionValue

                'Pinkal (20-Dec-2024) -- Start
                'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 

                'objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Format(New Date(2024, 1, 1).ToString(), "dd/MM/yyyy")
                'objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = CDate(Session("fin_startdate")).Day.ToString() & "-" & CDate(Session("fin_startdate")).Month.ToString() & "-" & CDate(Session("fin_startdate")).Year.ToString()
                objP2PBudgetBalanceService.budgetBalanceRequest.StartingDate = Now.Day.ToString() & "-" & Now.Month.ToString() & "-" & Now.Year.ToString()

                'objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY2024"
                'objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY_" & CDate(Session("fin_startdate")).Year.ToString()
                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetModelCode = "FY" & CDate(Session("fin_startdate")).Year.ToString()

                'objP2PBudgetBalanceService.budgetBalanceRequest.CalenderCode = "NMB-STD"
                objP2PBudgetBalanceService.budgetBalanceRequest.CalenderCode = "NMB - STD"

                objP2PBudgetBalanceService.budgetBalanceRequest.BudgetCycleCode = "Fiscal"
                'Pinkal (20-Dec-2024) -- End


                If objMaster.GetSetP2PWebRequest(strServiceURL.Trim(), True, True, "BudgetService", xResponseData, mstrError, objP2PBudgetBalanceService, xPostedData, mstrP2PToken.Trim) = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Return False
                End If

                If xResponseData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(xResponseData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        If dtTable.Columns.Contains("TotalFundsAvailableAmountMST") Then
                            mdecBudgetAmount = CDec(dtTable.Rows(0)("TotalFundsAvailableAmountMST"))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "There is no Budget for this request."), Me)
                            Return False
                        End If
                    End If
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                End If  '  If xResponseData.Trim.Length > 0 Then

                objP2PBudgetBalanceService = Nothing

            End If  ' If mstrP2PToken.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then

            objMaster = Nothing

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckForOccurrence(ByVal mdtTran As DataTable) As Boolean
        Dim objExpBalance As New clsEmployeeExpenseBalance
        Dim dsBalance As DataSet = Nothing
        Try
            If mdtTran IsNot Nothing Then

                Dim intExpenseId As Integer = 0
                If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                    If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                Else
                    intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                End If

                dsBalance = objExpBalance.Get_Balance(CInt(cboEmployee.SelectedValue), intExpenseId, CInt(Session("Fin_year")), dtpDate.GetDate.Date _
                                                                                               , CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)), CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)))


                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestApprovalExpTranId > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestApprovalExpTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestApprovalExpGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestApprovalExpGUID.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsclaim_request_master", 4, " ] time(s)."), Me)
                            Return False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            If dsBalance IsNot Nothing Then dsBalance.Clear()
            dsBalance = Nothing
            objExpBalance = Nothing
        End Try
        Return True
    End Function

    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.

    '    Private Sub Send_Notification(ByVal intCompanyID As Object)
    '        Try

    '            'Pinkal (25-Jan-2022) -- Start
    '            'Enhancement NMB  - Language Change in PM Module.	
    '            'If gobjEmailList.Count > 0 Then
    '            If objClaimEmailList.Count > 0 Then
    '                'Pinkal (25-Jan-2022) -- End
    '                Dim objSendMail As New clsSendMail
    'SendEmail:

    '                'Pinkal (25-Jan-2022) -- Start
    '                'Enhancement NMB  - Language Change in PM Module.	
    '                'For Each objEmail In gobjEmailList
    '                For Each objEmail In objClaimEmailList
    '                    gobjEmailList.Remove(objEmail)
    '                    'Pinkal (25-Jan-2022) -- End
    '                    objSendMail._ToEmail = objEmail._EmailTo
    '                    objSendMail._Subject = objEmail._Subject
    '                    objSendMail._Message = objEmail._Message
    '                    objSendMail._Form_Name = objEmail._Form_Name
    '                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
    '                    objSendMail._OperationModeId = objEmail._OperationModeId
    '                    objSendMail._UserUnkid = objEmail._UserUnkid
    '                    objSendMail._SenderAddress = objEmail._SenderAddress
    '                    objSendMail._ModuleRefId = objEmail._ModuleRefId

    '                    If objEmail._FileName.ToString.Trim.Length > 0 Then
    '                        objSendMail._AttachedFiles = objEmail._FileName
    '                    End If

    '                    objSendMail._Form_Name = "frmExpenseApproval"
    '                    objSendMail._WebClientIP = Session("IP_ADD").ToString()
    '                    objSendMail._WebHostName = Session("HOST_NAME").ToString()

    '                    Dim intCUnkId As Integer = 0
    '                    If TypeOf intCompanyID Is Integer Then
    '                        intCUnkId = CInt(intCompanyID)
    '                    Else
    '                        intCUnkId = intCompanyID(0)
    '                    End If
    '                    'Pinkal (25-Jan-2022) -- Start
    '                    'Enhancement NMB  - Language Change in PM Module.	
    '                    'If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
    '                    '                            objEmail._ExportReportPath).ToString.Length > 0 Then

    '                    '    gobjEmailList.Remove(objEmail)
    '                    '    GoTo SendEmail
    '                    'End If
    '                    objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), objEmail._ExportReportPath)
    '                    'Pinkal (25-Jan-2022) -- End
    '                Next
    '                'Pinkal (25-Jan-2022) -- Start
    '                'Enhancement NMB  - Language Change in PM Module.	
    '                'gobjEmailList.Clear()
    '                objClaimEmailList.Clear()
    '                'Pinkal (25-Jan-2022) -- End
    '            End If

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        End Try
    '    End Sub

    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try

            If objClaimEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objClaimEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    objSendMail._Form_Name = "frmExpenseApproval"
                    objSendMail._WebClientIP = Session("IP_ADD").ToString()
                    objSendMail._WebHostName = Session("HOST_NAME").ToString()

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = intCompanyID(0)
                    End If
                    objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), objEmail._ExportReportPath)
                Next
                If objClaimEmailList.Count > 0 Then objClaimEmailList.Clear()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetFormControls()
        Try
            If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    pnlRebateForNonEmployee.Visible = False
                Else
                    pnlRebateForNonEmployee.Visible = CBool(Session("AllowToAddRebateForNonEmployee"))
                End If
                txtAirline.Text = Session("CompanyGroupName").ToString().ToUpper()
                txtAirline.Enabled = False

                If CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                    Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                        Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                            pnlRebate.Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = True

                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhExpense", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhSectorRoute", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUoM", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhQty", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Visible = False

                        Case Else
                            pnlRebate.Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhExpense", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhSectorRoute", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUoM", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhQty", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Visible = True
                            dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Visible = True
                    End Select
                Else
                    pnlRebate.Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                    dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

                End If
            Else
                pnlRebate.Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelfrom", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtravelto", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgref", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhDispname", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhrelation", False, True)).Visible = False
                dgvData.Columns(getColumnId_Datagrid(dgvData, "dgcolhpsgtype", False, True)).Visible = False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub UpdateRebateInformation(ByVal iSrcRow As DataRow, ByVal iDstRow As DataRow)
        Try
            iDstRow("base_amount") = iSrcRow("base_amount")
            iDstRow("exchangerateunkid") = iSrcRow("exchangerateunkid")
            iDstRow("exchange_rate") = iSrcRow("exchange_rate")
            iDstRow("unitprice") = iSrcRow("unitprice")
            iDstRow("quantity") = iSrcRow("quantity")
            iDstRow("amount") = iSrcRow("amount")
            iDstRow("countryunkid") = iSrcRow("countryunkid")
            iDstRow("base_countryunkid") = iSrcRow("base_countryunkid")
            iDstRow.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function GetPrivilegeExpenseBalance(ByVal intExpensCategoryId As Integer) As Integer
        Dim objExpense As New clsExpense_Master
        Dim intExpenseId As Integer = 0
        Try
            If intExpensCategoryId = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                objExpense.isExist("", "", enExpenseType.EXP_REBATE_PRIVILEGE, -1, intExpenseId, "isaccrue = 1")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objExpense = Nothing
        End Try
        Return intExpenseId
    End Function

#End Region

#Region "Button Event"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Dim objExpense As New clsExpense_Master
        Dim gRow As IEnumerable(Of DataGridItem) = Nothing
        Try

            If CInt(cboExpense.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboExpense.Focus()
                Exit Sub
            End If

            'objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
        
            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END

                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 400, "Travel from is mandatory information. Please enter Travel from to continue."), Me)
                        cboTravelFrom.Focus()
                        'objExpense = Nothing
                        Exit Sub
                    End If

                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 401, "Travel to is mandatory information. Please enter Travel to to continue."), Me)
                        cboTravelFrom.Focus()
                        'objExpense = Nothing
                        Exit Sub
                    End If

                    If CInt(cboTravelFrom.SelectedValue) = CInt(cboTravelTo.SelectedValue) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 402, "Travel From and Travel To cannot be same. Please select proper travel from and travel to to continue."), Me)
                        Exit Sub
                    End If

                Case Else
                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                        cboSectorRoute.Focus()
                        Exit Sub
                    End If
            End Select

            If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            'If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) Then
            If mblnIsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            'If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
            If mintExpense_MaxQuantity > 0 AndAlso mintExpense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            'objExpense = Nothing

            Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                Case Else
                    If CInt(cboCurrency.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue."), Me)
                        cboCurrency.Focus()
                        Exit Sub
                    End If
            End Select

            If pnlRebate.Visible = True Then
                If txtFlightNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Sorry, Flight number is mandatory information. Please provide flight number to continue."), Me)
                    txtFlightNo.Focus()
                    Exit Sub
                End If

                If txtAirline.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Sorry, Airline is mandatory information. Please provide airline to continue."), Me)
                    txtAirline.Focus()
                    Exit Sub
                End If

                If dtTravelDate.IsNull = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "Sorry, Travel date is mandatory information. Please provide travel date to continue."), Me)
                    dtTravelDate.Focus()
                    Exit Sub
                End If

                If cboDependant.Enabled = True AndAlso cboDependant.SelectedValue = 0 AndAlso txtOthers.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 302, "Sorry, Passenger is mandatory information. Please provide passenger to continue."), Me)
                    cboDependant.Focus()
                    Exit Sub
                End If
                If cboDependant.Enabled = True AndAlso txtOthers.Enabled = True Then
                    If cboDependant.SelectedValue <> 0 AndAlso txtOthers.Text.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 304, "Sorry, Passenger and Other cannot be used at the same time. Please provide either passenger or other to continue."), Me)
                        cboDependant.Focus()
                        Exit Sub
                    End If
                End If

                If Session("MaxRoutePerExpCategory") IsNot Nothing Then
                    If CType(Session("MaxRoutePerExpCategory"), Dictionary(Of Integer, Integer)).ContainsKey(CInt(cboExpCategory.SelectedValue)) = True Then
                        Dim iMaxRouteAllow As Integer = CType(Session("MaxRoutePerExpCategory"), Dictionary(Of Integer, Integer))(CInt(cboExpCategory.SelectedValue))
                        If iMaxRouteAllow > 0 Then
                            If dgvData.Items.Count = iMaxRouteAllow Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Sorry, you cannot add this transaction. Reason : Maximum route allowed has been reached for the selected expense category. Maximum route allowed for the selected category is [ " & iMaxRouteAllow.ToString() & " ]."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                End If

            End If

            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If

            mblnIsExpenseEditClick = False
            mblnIsExpenseApproveClick = False

            If IsNumeric(txtUnitPrice.Text) = False Then
                txtUnitPrice.Text = "1.00"
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'objExpense = Nothing
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'Dim objExpense As New clsExpense_Master
        Dim gRow As IEnumerable(Of DataGridItem) = Nothing
        Try

            'objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
        
            Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                Case Else
                    'If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                    If mblnIsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                        cboSectorRoute.Focus()
                        Exit Sub
                    End If
            End Select
            
            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT

                Case Else
                    If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                        txtQty.Focus()
                        Exit Sub
                    End If
            End Select



            'If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) Then
            If mblnIsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            
            'If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
            If mintExpense_MaxQuantity > 0 AndAlso mintExpense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Exit Sub
            End If
          
            Select Case CInt(IIf(cboExpCategory.SelectedValue = "", 0, cboExpCategory.SelectedValue))
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                Case Else
                    If CInt(cboCurrency.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue."), Me)
                        cboCurrency.Focus()
                        Exit Sub
                    End If
            End Select

            If pnlRebate.Visible = True Then
                If txtFlightNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Sorry, Flight number is mandatory information. Please provide flight number to continue."), Me)
                    txtFlightNo.Focus()
                    Exit Sub
                End If

                If txtAirline.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Sorry, Airline is mandatory information. Please provide airline to continue."), Me)
                    txtAirline.Focus()
                    Exit Sub
                End If

                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        'If objExpense._IsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                        If mblnIsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 400, "Travel from is mandatory information. Please enter Travel from to continue."), Me)
                            cboTravelFrom.Focus()
                            'objExpense = Nothing
                            Exit Sub
                        End If

                        'If objExpense._IsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                        If mblnIsSecRouteMandatory AndAlso CInt(cboTravelFrom.SelectedValue) <= 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 401, "Travel to is mandatory information. Please enter Travel to to continue."), Me)
                            cboTravelFrom.Focus()
                            'objExpense = Nothing
                            Exit Sub
                        End If

                        If CInt(cboTravelFrom.SelectedValue) = CInt(cboTravelTo.SelectedValue) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 402, "Travel From and Travel To cannot be same. Please select proper travel from and travel to to continue."), Me)
                            Exit Sub
                        End If
                    Case Else

                        'If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                        If mblnIsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                            cboTravelTo.Focus()
                            'objExpense = Nothing
                            Exit Sub
                        End If
                End Select


                If dtTravelDate.IsNull = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "Sorry, Travel date is mandatory information. Please provide travel date to continue."), Me)
                    dtTravelDate.Focus()
                    Exit Sub
                End If
            End If

            mblnIsExpenseEditClick = True
            mblnIsExpenseApproveClick = False
            If IsNumeric(txtUnitPrice.Text) = False Then
                txtUnitPrice.Text = "1.00"
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'objExpense = Nothing
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click, btnReturntoApplicant.Click
        Try
            txtRejectRemark.Text = ""
            If CType(sender, Button).ID.ToUpper() = "BTNRETURNTOAPPLICANT" Then
                lblTitle.Text = "Return To Applicant Remark"
                mblnReturntoApplicant = True
            Else
                lblTitle.Text = "Rejection Remark"
                mblnReturntoApplicant = False
            End If
            popupRejectRemark.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = Nothing
        Try
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            If CInt(Me.ViewState("EditCrtranunkid")) <> 0 Then

                If popup_DeleteReason.Reason.Trim.Length > 0 Then
                    iRow = mdtTran.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")

                    If iRow IsNot Nothing AndAlso iRow.Length > 0 Then
                        If CInt(iRow(0)("quantity")) > 0 Then
                            Dim dtmp As DataRow() = Nothing
                            Select Case CInt(cboExpCategory.SelectedValue)
                                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                    If mdtTran.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("dpndtbeneficetranunkid")) <> 0).Count() > 0 Then
                                        dtmp = mdtTran.Select("dpndtbeneficetranunkid = '" & CInt(iRow(0)("dpndtbeneficetranunkid")) & "' AND crtranunkid <> '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                                    ElseIf mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("other_person").ToString().Trim().Length > 0).Count() > 0 Then
                                        dtmp = mdtTran.Select("other_person = '" & iRow(0)("other_person").ToString().Replace("'", "''") & "' AND crtranunkid <> '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                                    End If
                                    If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                                        UpdateRebateInformation(iRow(0), dtmp(0))
                                    End If
                            End Select
                        End If
                    End If

                    iRow(0).Item("isvoid") = True

                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = popup_DeleteReason.Reason.Trim  'Nilay (01-Feb-2015) -- txtReason.Text
                    iRow(0).Item("AUD") = "D"
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        iRow(0).Item("voiduserunkid") = Session("UserId")
                        iRow(0).Item("voidloginemployeeunkid") = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        iRow(0).Item("voiduserunkid") = -1
                        iRow(0).Item("voidloginemployeeunkid") = Session("Employeeunkid")
                    End If
                    iRow(0).AcceptChanges()
                    Me.ViewState("mdtTran") = mdtTran.Copy()
                    Fill_Expense()
                    Clear_Controls()
                    btnEdit.Visible = False

                    If mstrGroupName.ToUpper().Trim() = "NMB PLC" Then
                        btnAdd.Visible = False
                    Else
                        btnAdd.Visible = True
                    End If
                    cboExpense.Enabled = True
                    iRow = Nothing
                Else
                    popup_DeleteReason.Show() 'Nilay (01-Feb-2015) --  popupDelete.Show()
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Expense Delete process fail.", Me)
                Clear_Controls()
                btnEdit.Visible = False
                If mstrGroupName.ToUpper().Trim() = "NMB PLC" Then
                    btnAdd.Visible = False
                Else
                    btnAdd.Visible = True
                End If
                cboExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            iRow = Nothing
        End Try
    End Sub

    Protected Sub btnRemarkOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemarkOk.Click
        Try
            If txtRejectRemark.Text.Trim.Length <= 0 Then
                If mblnReturntoApplicant Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Return To Applicant Remark cannot be blank. Return To Applicant Remark is required information."), Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Rejection Remark cannot be blank. Rejection Remark is required information."), Me)
                End If
                txtRejectRemark.Focus()
                popupRejectRemark.Show()
                Exit Sub
            End If

            If Is_Valid() = False Then Exit Sub

            If SaveData(sender) Then
                mblnReturntoApplicant = False
                If mintClaimFormEmpId > 0 Then
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True, Session("mdbname").ToString() _
                                            , CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    Session.Abandon()
                    Response.Redirect("../Index.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim mdtTran As DataTable = Nothing
        Try
            mblnReturntoApplicant = False
            If Is_Valid() = False Then Exit Sub

            mblnIsExpenseEditClick = False
            mblnIsExpenseApproveClick = True


            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim drUnit() As DataRow = mdtTran.Select("unitprice <= 0")
                If drUnit.Length > 0 Then
                    popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Some of the expense(s) had 0 unit price for selected employee.") & vbCrLf & _
                                                                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                    popup_UnitPriceYesNo.Show()
                    Exit Sub
                Else
                    ApproveExpense()
                End If
                drUnit = Nothing
            Else
                ApproveExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If mintClaimFormEmpId > 0 Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True _
                                            , Session("mdbname").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")) _
                                            , Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If
                Response.Redirect("../Index.aspx", False)
            Else
                Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnViewAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAttchment.Click
        Dim mstrPreviewIds As String = ""
        Dim dtTable As New DataTable
        Try

            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmployee.SelectedValue), _
                                                                             enScanAttactRefId.CLAIM_REQUEST, _
                                                                             enImg_Email_RefId.Claim_Request, _
                                                                             CInt(Me.ViewState("mintClaimRequestMasterId")))

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            popup_ShowAttchment._Webpage = Me
            popup_ShowAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
        End Try
    End Sub

    Protected Sub popup_ShowAttchment_btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ShowAttchment.btnPreviewClose_Click
        Try
            popup_ShowAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick = True AndAlso mblnIsExpenseApproveClick = False Then
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = False Then
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = True Then
                ApproveExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick = True AndAlso mblnIsExpenseApproveClick = False Then
                EditExpense()
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = False Then
                AddExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewEmployeeAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewEmployeeAllocation.Click
        Try
            ViewEmployeeAllocation._EmployeeId = CInt(cboEmployee.SelectedValue)
            ViewEmployeeAllocation.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkShowFuelConsumptionReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowFuelConsumptionReport.Click
        Dim objFuelConsumption As New clsFuelConsumptionReport(CInt(Session("LangId")), CInt(Session("CompanyUnkId")))
        Try
            objFuelConsumption.SetDefaultValue()
            objFuelConsumption.setDefaultOrderBy(0)
            objFuelConsumption._FromDate = New DateTime(dtpDate.GetDate.Year, dtpDate.GetDate.Month, 1)
            objFuelConsumption._ToDate = New DateTime(dtpDate.GetDate.Year, dtpDate.GetDate.Month, DateTime.DaysInMonth(dtpDate.GetDate.Year, dtpDate.GetDate.Month))
            objFuelConsumption._ExpCateId = CInt(cboExpCategory.SelectedValue)
            objFuelConsumption._ExpCateName = cboExpCategory.SelectedItem.Text
            objFuelConsumption._EmpUnkId = CInt(cboEmployee.SelectedValue)
            objFuelConsumption._EmpName = cboEmployee.SelectedItem.Text
            objFuelConsumption._StatusId = 1
            objFuelConsumption._StatusName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 110, "Approved")
            objFuelConsumption._ShowRequiredQuanitty = True
            objFuelConsumption._FirstNamethenSurname = CBool(Session("FirstNamethenSurname"))
            objFuelConsumption._UserUnkid = CInt(Session("UserId"))
            objFuelConsumption._CompanyUnkId = CInt(Session("CompanyUnkId"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            objFuelConsumption.generateReportNew(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                         Session("ExportReportPath").ToString, _
                                         CBool(Session("OpenAfterExport")), _
                                         0, Aruti.Data.enPrintAction.None, enExportAction.None)

            Session("objRpt") = objFuelConsumption._Rpt

            If objFuelConsumption._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFuelConsumption = Nothing
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub dgvData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.DeleteCommand
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
        Try
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    Me.ViewState("EditCrtranunkid") = iRow(0).Item("crtranunkid")
                    popup_DeleteReason.Show()
                    Exit Sub
                End If

            ElseIf CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) <> "" Then
                iRow = mdtTran.Select("GUID = '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")

                If iRow IsNot Nothing AndAlso iRow.Length > 0 Then
                    If CInt(iRow(0)("quantity")) > 0 Then
                        Dim dtmp As DataRow() = Nothing
                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                If mdtTran.AsEnumerable().Where(Function(x) CInt(x.Field(Of Integer)("dpndtbeneficetranunkid")) <> 0).Count() > 0 Then
                                    dtmp = mdtTran.Select("dpndtbeneficetranunkid = '" & CInt(iRow(0)("dpndtbeneficetranunkid")) & "' AND GUID <> '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")
                                ElseIf mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("other_person").ToString().Trim().Length > 0).Count() > 0 Then
                                    dtmp = mdtTran.Select("other_person = '" & iRow(0)("other_person").ToString().Replace("'", "''") & "' AND GUID <> '" & CStr(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text) & "' AND AUD <> 'D'")
                                End If
                                If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                                    UpdateRebateInformation(iRow(0), dtmp(0))
                                End If
                        End Select
                    End If
                End If

                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                    iRow(0).AcceptChanges()
                End If
            End If

            Me.ViewState("mdtTran") = mdtTran.Copy()

            If dgvData.Items.Count <= 0 Then
                If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                    Select Case CInt(cboExpCategory.SelectedValue)
                        Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                            If cboRebatePercent.Enabled = False Then cboRebatePercent.Enabled = True
                            If cboDependant.Enabled = False Then cboDependant.Enabled = True
                    End Select
                End If
                If txtOthers.Enabled = False Then txtOthers.Enabled = True
            End If

            Fill_Expense()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            iRow = Nothing
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        Dim mdtTran As DataTable = Nothing
        Try
            If e.CommandName = "Edit" Then

                If Me.ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
                End If
                Dim iRow As DataRow() = Nothing

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) > 0 Then
                    iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then
                    iRow = mdtTran.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboExpense.SelectedValue = iRow(0).Item("expenseunkid").ToString()
                    Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

                    cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid").ToString()
                    Call dtpDate_TextChanged(dtpDate, New EventArgs())

                    txtQty.Text = CDec(iRow(0).Item("quantity")).ToString()
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString())).ToString()
                    txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboExpense.Enabled = False
                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    cboCostCenter.Enabled = False

                    txtAirline.Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAirline", False, True)).Text.ToString()
                    cboTravelFrom.SelectedValue = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhfromrouteid", False, True)).Text)
                    cboTravelTo.SelectedValue = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhtorouteid", False, True)).Text)

                    txtFlightNo.Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhflightno", False, True)).Text.ToString()
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.ToString().Trim.Length > 0 AndAlso _
                       e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.ToString() <> "&nbsp;" Then
                        dtTravelDate.SetDate = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.ToString()
                    End If
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhother_person", False, True)).Text.Trim.Length > 0 AndAlso _
                       e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhother_person", False, True)).Text.Trim <> "&nbsp;" Then
                        txtOthers.Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhother_person", False, True)).Text.ToString()
                    End If
                    cboDependant.SelectedValue = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhdpndtbeneficetranunkid", False, True)).Text.ToString()
                    cboCurrency.SelectedValue = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhcountryunkid", False, True)).Text.ToString()
                    mintClaimRequestApprovalExpTranId = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text)
                    mstrClaimRequestApprovalExpGUID = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString()

                    Enable_Disable_Ctrls(False)

                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text) <> 0 Then
                        Me.ViewState("EditCrtranunkid") = CStr(iRow(0).Item("crtranunkid"))
                    ElseIf e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhGUID", False, True)).Text.ToString <> "" Then
                        Me.ViewState("EditGuid") = CStr(iRow(0).Item("GUID"))
                    End If
                End If


            ElseIf e.CommandName.ToUpper = "PREVIEW" Then

                Dim mstrPreviewIds As String = ""
                Dim dtTable As New DataTable
           
                dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmployee.SelectedValue), _
                                                                                     enScanAttactRefId.CLAIM_REQUEST, _
                                                                                     enImg_Email_RefId.Claim_Request, _
                                                                                     CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "objdgcolhTranId", False, True)).Text))

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
                End If

                popup_ShowAttchment.ScanTranIds = mstrPreviewIds
                popup_ShowAttchment._Webpage = Me
                popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                popup_ShowAttchment.Show()
                If dtTable IsNot Nothing Then dtTable.Clear()
                dtTable = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
        End Try
    End Sub

    Protected Sub dgvData_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgvData.PageIndexChanged
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Text.Trim.Length > 0 Then  'Unit Price  
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhUnitPrice", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Text.Trim.Length > 0 Then  'Base Amount 
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhBasicAmount", False, True)).Text), Session("fmtCurrency").ToString())
                    End If
                End If

                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text.Trim.Length > 0 Then  'Amount 
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Visible = True Then
                    If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.Trim.Length > 0 AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text = CDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvData, "dgcolhtraveldate", False, True)).Text).ToShortDateString()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DatePicker Event(s)"

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, cboSectorRoute.SelectedIndexChanged
        Try

            If CDate(Me.ViewState("MinDate")).Date <= dtpDate.GetDate.Date And CDate(Me.ViewState("MaxDate")).Date >= dtpDate.GetDate.Date Then
                Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
                Dim objCosting As New clsExpenseCosting
                'objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.GetDate)
                objCosting.GetDefaultCosting(CInt(IIf(cboSectorRoute.SelectedValue = "", "0", cboSectorRoute.SelectedValue)), iCostingId, iAmount, dtpDate.GetDate)
                txtCosting.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())
                txtCostingTag.Value = iCostingId.ToString()
                txtUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())

                'Dim objExpnese As New clsExpense_Master
                'objExpnese._Expenseunkid = CInt(cboExpense.SelectedValue)

                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    txtQty.Text = "1"

                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                    txtQty.Text = "1"
                End If


                If (sender.GetType().FullName = "System.Web.UI.WebControls.TextBox" AndAlso CType(sender, System.Web.UI.WebControls.TextBox).Parent.ID.ToUpper() = dtpDate.ID.ToUpper()) AndAlso CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboExpense.SelectedValue) > 0 Then
                    cboExpense_SelectedIndexChanged(New Object(), New EventArgs())
                End If

                objCosting = Nothing

                'If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                If mblnIsConsiderDependants AndAlso mblnIsSecRouteMandatory Then
                    txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                    mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
                End If

                txtUnitPrice.Enabled = mblnIsUnitPriceEditable
                'txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable
                'objExpnese = Nothing
            Else
                dtpDate.SetDate = CDate(Me.ViewState("PDate"))
                DisplayMessage.DisplayMessage("Sorry, Date should be between " & CDate(Me.ViewState("MinDate")).ToShortDateString & " and " & CDate(Me.ViewState("MaxDate")).ToShortDateString & ".", Me)
            End If

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    txtQty.Enabled = False
                    txtUnitPrice.Enabled = False
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Control Event"

#End Region

#Region "ComboBox Event(s)"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
                Me.ViewState("MinDate") = CDate(eZeeDate.convertDate("17530101"))
                Me.ViewState("MaxDate") = CDate(eZeeDate.convertDate("99981231"))

                dtpDate.SetDate = objPeriod._Start_Date
                Me.ViewState("PDate") = objPeriod._Start_Date
                Me.ViewState("MinDate") = Session("fin_startdate")

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objPeriod = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Dim dsCombo As New DataSet
        Try

            If CType(sender, DropDownList).ID.ToUpper() = "CBOEXPCATEGORY" Then
                SetFormControls()
            End If


            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    cboCurrency.SelectedValue = 0
                    cboCurrency.Enabled = False

                    txtQty.Enabled = False
                    txtUnitPrice.Enabled = False
                Case Else
                    cboCurrency.Enabled = True

                    txtQty.Enabled = True
                    txtUnitPrice.Enabled = True
            End Select

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                cboSeatStatus.SelectedValue = enRebateSeat.RB_SA
                cboSeatStatus.Enabled = False
                cboRebatePercent.SelectedValue = enRebatePercent.RB_100
                cboRebatePercent.Enabled = True
                cboDependant.SelectedValue = 0
                cboDependant.Enabled = True
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_DUTY Then
                cboSeatStatus.SelectedValue = enRebateSeat.RB_FIRM
                cboSeatStatus.Enabled = False
                cboDependant.SelectedValue = 0
                cboDependant.Enabled = False
                cboRebatePercent.SelectedValue = enRebatePercent.RB_100
                cboRebatePercent.Enabled = False
            End If

            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpCategory.SelectedValue), True, "List", CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), True)
            objExpMaster = Nothing

            Dim dtTable As DataTable = Nothing
            If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtTable = New DataView(dsCombo.Tables(0), "cr_expinvisible = 0", "", DataViewRowState.CurrentRows).ToTable()
            End If
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing

            If cboExpense.SelectedValue IsNot Nothing AndAlso cboExpense.SelectedValue <> "" Then
                Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE

                        objlblValue.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Leave Form")

                        Dim objLeaveType As New clsleavetype_master

                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", False)
                        End If

                        objLeaveType = Nothing

                        With cboLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedIndex = 0
                        End With

                        'If IsPostBack = True Then
                        Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())
                        'End If

                    Case enExpenseType.EXP_MEDICAL
                        'Dim obj
                    Case enExpenseType.EXP_TRAINING
                        dsCombo = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(CInt(cboEmployee.SelectedValue), True)
                        With cboReference
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With

                    Case enExpenseType.EXP_MISCELLANEOUS
                        cboLeaveType.Enabled = False
                        cboReference.Enabled = False
                End Select

                If CInt(cboEmployee.SelectedValue) > 0 Then

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If
            Else
                objlblValue.Text = ""
                cboLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If

            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 AndAlso Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                dsCombo = New DataSet : Dim objDependant As New clsDependants_Beneficiary_tran
                dsCombo = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date, blnFlag:=True)
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE Then
                    dsCombo.Tables(0).Rows.Clear()
                    Dim sRow As DataRow = dsCombo.Tables(0).NewRow
                    sRow("dependent_id") = -999
                    sRow("dependants") = Language.getMessage("clsclaim_request_tran", 102, "Self")
                    sRow("age") = 0
                    sRow("Months") = 0
                    sRow("relation") = ""
                    sRow("gender") = ""
                    dsCombo.Tables(0).Rows.InsertAt(sRow, 1)
                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                    With cboDependant
                        .DataValueField = "dependent_Id"
                        .DataTextField = "dependants"
                        .DataSource = dsCombo.Tables(0)
                        Try
                            .SelectedIndex = mintSelectedPassenger
                        Catch ex As Exception
                            .SelectedValue = 0
                        End Try
                        .DataBind()
                    End With
                Else
                    cboDependant.Enabled = False : cboDependant.DataSource = Nothing
                End If

                'If CBool(Session("SectorRouteAssignToEmp")) Then
                '    Dim objAssignEmp As New clsassignemp_sector
                '    Dim dtSector As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
                '    With cboSectorRoute
                '        .DataValueField = "secrouteunkid"
                '        .DataTextField = "Sector"
                '        .DataSource = dtSector
                '        .DataBind()
                '    End With
                '    If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                '        With cboTravelFrom
                '            .DataValueField = "secrouteunkid"
                '            .DataTextField = "Sector"
                '            .DataSource = dtSector.Copy
                '            .DataBind()
                '            .SelectedIndex = 0
                '        End With
                '        With cboTravelTo
                '            .DataValueField = "secrouteunkid"
                '            .DataTextField = "Sector"
                '            .DataSource = dtSector.Copy
                '            .DataBind()
                '            .SelectedIndex = 0
                '        End With
                '    End If
                'End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try
            If CBool(Session("SectorRouteAssignToExpense")) Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                    .SelectedIndex = 0
                End With

                If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                    With cboTravelFrom
                        .DataValueField = "secrouteunkid"
                        .DataTextField = "Sector"
                        .DataSource = dtSector.Copy
                        .DataBind()
                        .SelectedIndex = 0
                    End With
                    With cboTravelTo
                        .DataValueField = "secrouteunkid"
                        .DataTextField = "Sector"
                        .DataSource = dtSector.Copy
                        .DataBind()
                        .SelectedIndex = 0
                    End With
                End If

                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing

                objAssignExpense = Nothing
            End If

            Dim objExpMaster As New clsExpense_Master

            If CInt(cboExpense.SelectedValue) > 0 Then

                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)


                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment
                mblnIsSecRouteMandatory = objExpMaster._IsSecRouteMandatory
                mblnIsConsiderDependants = objExpMaster._IsConsiderDependants
                mblnDoNotAllowToApplyForBackDate = objExpMaster._DoNotAllowToApplyForBackDate
                mblnIsHRExpense = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
                mintGLCodeID = objExpMaster._GLCodeId
                mstrDescription = objExpMaster._Description
                mintExpenditureTypeId = objExpMaster._ExpenditureTypeId
                mblnIsaccrue = objExpMaster._Isaccrue
                mintUomunkid = objExpMaster._Uomunkid
                mintAccrueSettingId = objExpMaster._AccrueSetting
                mintExpense_MaxQuantity = objExpMaster._Expense_MaxQuantity
                mblnIsAttachDocMandetory = objExpMaster._IsAttachDocMandetory
                mblnIsUnitPriceEditable = objExpMaster._IsUnitPriceEditable
                mintJournalunkid = objExpMaster._Journalunkid
                mintPaymentmodeunkid = objExpMaster._Paymentmodeunkid
                mblnIsDebit = objExpMaster._P2PDebit
                mblnIsCredit = objExpMaster._P2PCredit

                'Me.ViewState("mblnIsLeaveEncashment") = objExpMaster._IsLeaveEncashment

                'cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.Enabled = mblnIsSecRouteMandatory
                cboSectorRoute.SelectedValue = "0"
                cboCostCenter.SelectedValue = "0"

                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    'If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                    If mintExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf mintExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf mintExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If

                'If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                If mblnIsaccrue = False AndAlso mblnIsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    txtUnitPrice.Text = "1.00"
                    txtBalance.Text = "0.00"

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet

                    Dim intExpenseId As Integer = 0

                    If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                        If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                    Else
                        intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                    End If

                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), intExpenseId, CInt(Session("Fin_Year")), dtpDate.GetDate.Date)

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing

                    objEmpExpBal = Nothing

                    'ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                ElseIf mblnIsaccrue = False AndAlso mblnIsLeaveEncashment = True Then
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing
                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                        dsList = objLeave.GetList("List", Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                  True, _
                                                  True, True, False, CInt(cboEmployee.SelectedValue), _
                                               False, False, False, "", Nothing, mblnIsExternalApprover)


                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                        dsList = objLeave.GetList("List", Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                 Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                  True, _
                                                  True, True, False, CInt(cboEmployee.SelectedValue), _
                                                 True, True, False, "", Nothing, mblnIsExternalApprover)

                    End If

                    dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        txtBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then

                            If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = CDate(Session("fin_enddate")).Date
                            If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            Else
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            End If

                        ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then

                            Dim mdtDate As Date = dtpDate.GetDate.Date
                            Dim mdtDays As Integer = 0

                            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                                If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                                    mdtDate = CDate(Session("fin_enddate")).Date
                                    mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                                Else
                                    If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                        mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                                    End If
                                End If

                            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                                If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                End If
                                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                            End If

                            Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                            If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                                intDiff = intDiff - 1
                            End If

                            txtBalanceAsOnDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        End If

                        txtUoMType.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 6, "Quantity")
                    End If

                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objLeave = Nothing

                    'ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                ElseIf mblnIsaccrue = True AndAlso mblnIsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    txtUnitPrice.Text = "1.00"

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance
                    Dim dsBal As New DataSet

                    Dim intExpenseId As Integer = 0
                    If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                        If intExpenseId <= 0 Then intExpenseId = CInt(cboExpense.SelectedValue)
                    Else
                        intExpenseId = GetPrivilegeExpenseBalance(CInt(cboExpCategory.SelectedValue))
                    End If

                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), intExpenseId, CInt(Session("Fin_Year")), dtpDate.GetDate.Date)


                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = CStr(Math.Round(Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")), 6))  'SHANI (06 JUN 2015) -- Start
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    objEmpExpBal = Nothing
                End If
            Else


                mblnIsLeaveEncashment = False
                mblnIsSecRouteMandatory = False
                mblnIsConsiderDependants = False
                mblnDoNotAllowToApplyForBackDate = False
                mblnIsHRExpense = False
                mblnIsClaimFormBudgetMandatory = False
                mblnIsaccrue = False
                mblnIsAttachDocMandetory = False
                mintGLCodeID = 0
                mstrDescription = ""
                mintExpenditureTypeId = 0
                mintUomunkid = 0
                mintAccrueSettingId = 0
                mintExpense_MaxQuantity = 0
                mblnIsUnitPriceEditable = True


                txtUoMType.Text = ""
                txtUnitPrice.Enabled = True
                txtUnitPrice.Text = "1.00"
                txtBalance.Text = "0.00"
                txtBalanceAsOnDate.Text = "0.00"
                cboCostCenter.Enabled = False
                cboCostCenter.SelectedValue = "0"
                txtQty.Text = "1"
            End If

            'If objExpMaster._IsConsiderDependants Then
            If mblnIsConsiderDependants Then
                txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
            Else
                txtQty.Text = "1"
            End If

            'txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            txtUnitPrice.Enabled = mblnIsUnitPriceEditable

            If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
                txtQty.Enabled = False
            End If


            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            If mblnIsaccrue OrElse mblnIsLeaveEncashment Then
                pnlBalAsonDate.Visible = True
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    Case Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                End Select

            Else
                pnlBalAsonDate.Visible = False

                Dim mdtTran As DataTable = Nothing
                If Me.ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
                End If

                If mdtTran IsNot Nothing Then
                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        If Session("CompanyGroupName").ToString().Trim.ToUpper = "NMB PLC" AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                            cboCurrency.Enabled = True
                        Else
                            cboCurrency.Enabled = False
                            cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First().ToString()
                        End If

                    Else

                        Select Case CInt(cboExpCategory.SelectedValue)
                            Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                            Case Else
                                cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                                cboCurrency.Enabled = True
                        End Select
                    End If
                End If

                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                        txtQty.Enabled = False
                        txtUnitPrice.Enabled = False
                End Select

                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing

            End If

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT 'S.SANDEEP |27-MAY-2022| -- START {AC2-389} -- END
                    cboSectorRoute.Enabled = False
                Case Else
                    cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
            End Select
            objExpMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform : Dim dsLeave As New DataSet
            dsLeave = objleave.getListForCombo(CInt(cboLeaveType.SelectedValue), "7", CInt(cboEmployee.SelectedValue), True, True)

            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = dsLeave.Tables("List")
                .DataBind()
                .SelectedIndex = 0
            End With

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            'Pinkal (05-Sep-2020) -- End

            objleave = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |03-SEP-2022| -- START
    'ISSUE/ENHANCEMENT : Sprint_2022-13
    Protected Sub cboDependant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDependant.SelectedIndexChanged
        Try
            mintSelectedPassenger = CInt(IIf(cboDependant.SelectedValue = "", 0, cboDependant.SelectedValue))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |03-SEP-2022| -- END

#End Region

#Region "TextBox Event"
    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Dim decQty As Decimal = 0
        Try
            Decimal.TryParse(txtQty.Text, decQty)
            If txtQty.Text.Trim.Length > 0 Then
                If IsNumeric(txtQty.Text) = False Then
                    txtQty.Text = "1"
                    Exit Sub
                End If

                If CInt(txtQty.Text) > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then

                    ' If CBool(Me.ViewState("mblnIsLeaveEncashment")) = False Then
                    If mblnIsLeaveEncashment = False Then
                        txtUnitPrice.Enabled = True
                        If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                            txtUnitPrice.Text = CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())).ToString
                            txtUnitPrice.Focus()
                        End If
                    Else
                        txtUnitPrice.Enabled = False
                    End If
                ElseIf decQty <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                        txtUnitPrice.Text = CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())).ToString()
                    End If
                End If
            Else
                txtUnitPrice.Enabled = False
            End If

            If mblnIsUnitPriceEditable = False Then txtUnitPrice.Enabled = mblnIsUnitPriceEditable

            'Dim objExpense As New clsExpense_Master
            'objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            'If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            'objExpense = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub txtUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.TextChanged
        Try
            If IsNumeric(txtUnitPrice.Text) = False Then
                txtUnitPrice.Text = "1.00"
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region


    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbExpenseInformation", Me.lblDetialHeader.Text)

            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblQty.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.btnRemarkClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnRemarkClose.ID, Me.btnRemarkClose.Text)
            Me.btnRemarkOk.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnRemarkOk.ID, Me.btnRemarkOk.Text)
            Me.btnOK.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnOK.ID, Me.btnOK.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.tbExpenseRemark.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbExpenseRemark.ID, Me.tbExpenseRemark.HeaderText)
            'Me.tbClaimRemark.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.tbClaimRemark.ID, Me.tbClaimRemark.HeaderText)
            Me.tbExpenseRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.tbExpenseRemark.ID, Me.tbExpenseRemark.Text)
            Me.tbClaimRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.tbClaimRemark.ID, Me.tbClaimRemark.Text)
            Me.lnkViewEmployeeAllocation.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkViewEmployeeAllocation.ID, Me.lnkViewEmployeeAllocation.ToolTip)
            'Gajanan [17-Sep-2020] -- End



            Me.lblSector.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSector.ID, Me.lblSector.Text)

            'Pinkal (23-Oct-2015) -- Start
            'Enhancement - Putting Domicile Address for TRA as per Dennis Requirement
            Me.LblDomicileAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)
            'Pinkal (23-Oct-2015) -- End

            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            lblBalanceasondate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalanceasondate.ID, Me.lblBalanceasondate.Text)
            'Pinkal (30-Apr-2018) - Start

            'Pinkal (20-Nov-2020) -- Start
            'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
            lnkShowFuelConsumptionReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkShowFuelConsumptionReport.ID, Me.lnkShowFuelConsumptionReport.Text)
            'Pinkal (20-Nov-2020) -- End

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Me.btnReturntoApplicant.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReturntoApplicant.ID, Me.btnReturntoApplicant.Text).Replace("&", "")
            'Pinkal (24-Jun-2024) -- End


            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


            Me.dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)
            Me.dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)
            Me.dgvData.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(8).FooterText, Me.dgvData.Columns(8).HeaderText)
            Me.dgvData.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(9).FooterText, Me.dgvData.Columns(9).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:-" & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Expense is mandatory information. Please select Expense to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Please add atleast one expense in order to do futher operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Rejection Remark cannot be blank. Rejection Remark is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, you cannot set amount greater than balance set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Leave Form")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Some of the expense(s) had 0 unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "You have not set your expense remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "There is no Budget for this request.")


            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsclaim_request_master", 4, " ] time(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsExpCommonMethods", 6, "Quantity")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
