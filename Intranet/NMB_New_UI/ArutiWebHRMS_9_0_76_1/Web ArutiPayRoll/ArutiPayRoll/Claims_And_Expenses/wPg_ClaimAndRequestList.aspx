﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ClaimAndRequestList.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_ClaimAndRequestList" Title="Claim & Request List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Claim & Request List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExpCategory" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExpCategory" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFDate" runat="server" AutoPostBack="False" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpTDate" runat="server" AutoPostBack="False" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" Visible="false" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="lvClaimRequestList" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="brnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit">
                                                                     <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                           <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="mnuCancelExpenseForm">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                           <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgCancel" runat="server" Text="Cancel" ToolTip="Cancel" CommandName="Cancel">
                                                                    <i class="fas fa-ban text-danger style="font-size:20px;color:Red"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="claimrequestno" HeaderText="Claim No" ReadOnly="true"
                                                        FooterText="colhclaimno" />
                                                    <asp:BoundColumn DataField="expensetype" HeaderText="Expense Cat." ReadOnly="true"
                                                        FooterText="colhExpType" />
                                                    <asp:BoundColumn DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhemployee" />
                                                    <asp:BoundColumn DataField="tdate" HeaderText="Date" ReadOnly="true" FooterText="colhdate" />
                                                    <asp:BoundColumn DataField="" HeaderText="Amount" ReadOnly="true" FooterText="colhamount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="" HeaderText="Aprroved Amt." ReadOnly="true" FooterText="colhApprovedAmount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="status" HeaderText="Status" ReadOnly="true" FooterText="colhstatus" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                        FooterText="objColhStatusunkid" Visible="false" />
                                                    <asp:BoundColumn DataField="expensetypeid" HeaderText="ExpenseTypeId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="crmasterunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="p2prequisitionid" HeaderText="p2prequisitionid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="ReqAmount" HeaderText="ReqAmount" ReadOnly="true" Visible="false" FooterText = "objcolhReqAmount"  />
                                                    <asp:BoundColumn DataField="ApprovedAmt" HeaderText="ApprovedAmt" ReadOnly="true"
                                                        Visible="false" FooterText = "objcolhApprovedAmt" />
                                                     <asp:BoundColumn DataField="ReqbaseAmount" HeaderText="ReqbaseAmount" ReadOnly="true" Visible="false" FooterText = "objcolhReqbaseAmount" />
                                                    <asp:BoundColumn DataField="ApprovedbaseAmt" HeaderText="ApprovedbaseAmt" ReadOnly="true" Visible="false" FooterText = "objcolhApprovedbaseAmt" />
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupCancel" runat="server" CancelControlID="btnCancelClose"
                    DropShadow="false" BackgroundCssClass="modal-backdrop" PopupControlID="pnlCancelPopup"
                    TargetControlID="btnHidden">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCancelPopup" runat="server" CssClass="card modal-dialog-lg" Style="display: none; max-width:1024px"
                    DefaultButton="btnCancelSave">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Cancel Expense Form Information"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelExpCategory" runat="server" Text="Exp. Cat." CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboCancelExpCategory" runat="server" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                     <asp:Label ID="lblCancelDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                      <uc2:DateCtrl ID="dtpCancelDate" runat="server" AutoPostBack="False" Enabled="false" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelPeriod" runat="server" Text="Period" Visible="false" CssClass="form-label"></asp:Label>
                                <asp:Label ID="lblName" runat="server" Text="Claim No." CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCancelClaimNo" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboCancelPeriod" runat="server" Visible="false" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <asp:Label ID="lblCancelEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCancelEmployee" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:HiddenField ID="txtCancelEmployeeid" runat="server" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelRemark" runat="server" Text="Cancel Remark" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCancelRemark" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 150px;">
                                    <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        CssClass="table table-hover table-bordered" DataKeyNames="crapprovaltranunkid">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="ChkCancelAll" runat="server" AutoPostBack="true" CssClass="filled-in"
                                                        Text=" " OnCheckedChanged="chkCancelAll_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkCancelSelect" runat="server" AutoPostBack="true" CssClass="filled-in"
                                                        Text=" " OnCheckedChanged="ChkCancelSelect_CheckedChanged" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- 'S.SANDEEP |10-MAR-2022| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : AC2-143V2--%>
                                            <asp:TemplateField FooterText="dgcolhpsgref" HeaderText="Passenger Ref">
                                                <ItemTemplate>
                                                    <asp:Label ID="objlblIndx" runat="server" Text="<%# Container.DataItemIndex  + 1%>"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="relation" FooterText="dgcolhrelation" HeaderText="Relation" />
                                            <asp:BoundField DataField="psgtype" FooterText="dgcolhpsgtype" HeaderText="Passenger Type" />
                                            <asp:BoundField DataField="airline" FooterText="dgcolhAirline" HeaderText="Airline" />
                                            <asp:BoundField DataField="travelfrom" FooterText="dgcolhtravelfrom" HeaderText="Travel From" />
                                            <asp:BoundField DataField="travelto" FooterText="dgcolhtravelto" HeaderText="Travel To" />
                                            <asp:BoundField DataField="flightno" FooterText="dgcolhflightno" HeaderText="Flight No." />
                                            <asp:BoundField DataField="traveldate" FooterText="dgcolhtraveldate" HeaderText="Travel Date" />
                                            <%--'S.SANDEEP |10-MAR-2022| -- END--%>
                                            <asp:BoundField DataField="Expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                FooterText="dgcolhExpense" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="UoM" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM"
                                                HeaderStyle-HorizontalAlign="Left" />
                                                
                                             <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="true" FooterText="dgcolhCurrency" />
                                                
                                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                
                                            <asp:BoundField DataField="base_amount" HeaderText="Base Amount" ReadOnly="true" FooterText="dgcolhBaseAmount" />
                                                
                                            <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                           
                                            <asp:BoundField DataField="expense_remark" HeaderText="" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                Visible="false" />
                                            <asp:BoundField DataField="crapprovaltranunkid" HeaderText="" HeaderStyle-HorizontalAlign="Right"
                                                ReadOnly="true" Visible="false" FooterText="objdgcolhcrapprovaltranunkid" />
                                            <asp:BoundField DataField="crmasterunkid" HeaderText="" ReadOnly="true" HeaderStyle-HorizontalAlign="Right"
                                                Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--'S.SANDEEP |25-APR-2022| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : AC2-143V2--%>
                    <%--<div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtGrandTotal" Style="text-align: right" runat="server" Enabled="false"
                                        CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <asp:Panel ID="pnlGTotal" runat="server">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtGrandTotal" Style="text-align: right" runat="server" Enabled="false"
                                        CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    </asp:Panel>
                    <%--'S.SANDEEP |25-APR-2022| -- END--%>
                    <div class="footer">
                        <asp:Button ID="btnCancelSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCancelClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <%--    <div class="panel-primary">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                   
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 10%">
                                           
                                        </td>
                                        <td style="width: 20%">
                                            
                                        </td>
                                        <td style="width: 12%">
                                          
                                        </td>
                                        <td style="width: 20%">
                                          
                                        </td>
                                        <td style="width: 5%; text-align: center">
                                            
                                        </td>
                                        <td style="width: 33%">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 45%">
                                                        
                                                    </td>
                                                    <td style="width: 10%">
                                                       
                                                    </td>
                                                    <td style="width: 45%">
                                                       
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 10%">
                                            
                                        </td>
                                        <td style="width: 20%">
                                            
                                        </td>
                                        <td style="width: 12%">
                                            
                                        </td>
                                        <td style="width: 20%">
                                            
                                        </td>
                                        <td style="width: 10%">
                                           
                                        </td>
                                        <td style="width: 28%">
                                           
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    
                                </div>
                            </div>
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                    <td style="width: 100%">
                                        <asp:Panel ID="pnl_lvClaimRequestList" Style="margin-top: 5px; max-height: 300px"
                                            ScrollBars="Auto" runat="server">
                                            
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                    <td style="width: 100%">
                                     
                                            <div class="panel-primary" style="margin:0px">
                                                <div class="panel-heading">
                                                    
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblTitle" runat="server" Text="Filter Criteria"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 10%">
                                                                        
                                                                    </td>
                                                                    <td style="width: 45%" colspan="3">
                                                                      
                                                                    </td>
                                                                    <td rowspan="3" style="vertical-align: top; width: 15%">
                                                                      
                                                                    </td>
                                                                    <td rowspan="3" style="vertical-align: top; width: 30%">
                                                                      
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 10%">
                                                                       
                                                                        
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                       
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                       
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 10%">
                                                                     
                                                                    </td>
                                                                    <td colspan="3" style="width: 45%">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Panel ID="pnl_dgvdata" Style="margin-top: 10px" runat="server" Height="300px"
                                                                            ScrollBars="Auto">
                                                                            
                                                                            <div style="margin-top:10px" align="right">
                                                                              
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>--%>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
