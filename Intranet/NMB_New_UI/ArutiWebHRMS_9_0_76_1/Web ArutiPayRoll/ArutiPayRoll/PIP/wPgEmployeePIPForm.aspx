﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmployeePIPForm.aspx.vb"
    Inherits="PIP_wPgEmployeePIPForm" Title="Employee PIP" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirm" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
            RetriveCollapse();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblMainHeader" runat="server" Text="Employee PIP"></asp:Label>
                                </h2>
                                <ul class="header-dropdown">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkPipInstruction" runat="server" ToolTip="Personal Improvement Instruction">
                                                <i class="fas fa-info-circle"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblPIPFormNo" runat="server" Text="Form No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPIPFormNo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFormCreationDate" runat="server" Text="Form Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpFormCreationDate" runat="server" AutoPostBack="false" Enabled="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                            </div>
                            <%-- <asp:Panel ID="pnlNoEmployeeSelected" runat="server" CssClass="footer text-left">
                                <asp:Label ID="lblNoEmployeeSelected" CssClass="label label-info" Text="Please select employee to continue"
                                    runat="server" />
                            </asp:Panel>--%>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlData" runat="server" CssClass="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card profile-detail">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#PIPEmployeeDetail" aria-controls="PIPEmployeeDetail"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPIPEmployeeDetailTab" runat="server" Text="Employee Detail"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPAreaOfConcern" aria-controls="PIPAreaOfConcern"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPIPAresofConcernTab" runat="server" Text="Areas of Challenge and Expectations"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPImprovementActionPlan" aria-controls="PIPImprovementActionPlan"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPIPImprovementActionPlanTab" runat="server" Text="Improvement Action Plan"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPProgressMonitoring" aria-controls="PIPProgressMonitoring"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPIPProgressMonitoringTab" runat="server" Text="Progress monitoring"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPOverallAssessment" aria-controls="PIPOverallAssessment"
                                        role="tab" data-toggle="tab">
                                        <asp:Label ID="lblPIPOverallAssessmentTab" runat="server" Text="Overall Assessment"></asp:Label>
                                    </a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="PIPEmployeeDetail">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix d--f ai--c jc--c">
                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                        <div class="image-area m-r-10">
                                                            <asp:Image ID="imgEmp" runat="server" Width="120px" Height="120px" Style="border-radius: 50%;" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblempcode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtCodeValue" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="objlblEmployeeName" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblJob" runat="server" Text="Current Job Title" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="objlblJob" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="objlblDepartment" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="divider">
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblDeptGroup" runat="server" Text="Department Group" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtDepartmentGroup" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtClass" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblLineManagerCoach" runat="server" Text="Line Manager" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtLineManagerCoach" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblPIPFormDate" runat="server" Text="Last PIP Form Date" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="txtPIPFormDate" runat="server" Text="-"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="divider">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPAreaOfConcern">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <asp:DataList ID="dlPIPParameter" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Panel ID="pnlPIPParameterContrainer" runat="server">
                                                                    <div class="panel-group" id="pnlPIPParameter" role="tablist" aria-multiselectable="true">
                                                                        <div class="panel" style="border: 1px solid #ddd">
                                                                            <div class="panel-heading" role="tab" id="PIPParameterHeading_<%# Eval("parameterunkid") %>">
                                                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                                                    <a role="button" data-toggle="collapse" class="PIPCollapseParameter" href="#PIPParameter_<%# Eval("parameterunkid") %>"
                                                                                        aria-expanded="false" aria-controls="PIPParameter_<%# Eval("parameterunkid") %>"
                                                                                        style="flex: 1; background: #F5F5F5">
                                                                                        <asp:Label ID="lblParameterName" Text='<%# Eval("parameter") %>' runat="server" />
                                                                                    </a>
                                                                                    <asp:HiddenField ID="hfparameterunkid" runat="server" Value='<%# Eval("parameterunkid") %>' />
                                                                                    <asp:HiddenField ID="hfparameterViewtypeId" runat="server" Value='<%# Eval("viewtypeid") %>' />
                                                                                    <asp:LinkButton runat="server" ID="lnkAddPIPItem" OnClick="OnGvAddPIPItem" Style="background: #F5F5F5">
                                                                                        <i class="fas fa-plus-circle text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="PIPParameter_<%# Eval("parameterunkid") %>" class="panel-collapse collapse"
                                                                                role="tabpanel" aria-labelledby="PIPParameterHeading_<%# Eval("parameterunkid") %>">
                                                                                <div class="panel-body table-responsive" style="max-height: 300px">
                                                                                    <asp:GridView ID="dgvPIPItems" runat="server" runat="server" Width="99%" AllowPaging="false"
                                                                                        CssClass="table table-hover table-bordered" OnRowDataBound="dgvPIPItems_RowDataBound"
                                                                                        DataKeyNames="itemtypeid,selectionmodeid,GUID,Header_Id,Header_Name,pipformunkid">
                                                                                        <Columns>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                    <asp:DataList ID="dlPIPItems" runat="server" Width="100%" OnItemDataBound="dlPIPItems_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <asp:Panel ID="pnlPIPItemContrainer" runat="server">
                                                                                                <div class="panel-group" id="pnlPIPItem" role="tablist" aria-multiselectable="true">
                                                                                                    <div class="panel" style="border: 1px solid #ddd">
                                                                                                        <div class="panel-heading" role="tab" id="PIPItemHeading_<%# Eval("itemunkid") %>">
                                                                                                            <h4 class="panel-title d--f ai--c jc--sb">
                                                                                                                <a role="button" data-toggle="collapse" class="PIPItemCollapse" href="#PIPItems_<%# Eval("itemunkid") %>"
                                                                                                                    aria-expanded="false" aria-controls="PIPItems_<%# Eval("itemunkid") %>" style="flex: 1">
                                                                                                                    <asp:Label ID="lblItemName" Text='<%# Eval("item") %>' runat="server" />
                                                                                                                </a>
                                                                                                                <asp:LinkButton runat="server" ID="lnkAddPIPItem" OnClick="OnAddConcernItem">
                                                                                                                <i class="fas fa-plus-circle text-success"></i>
                                                                                                                </asp:LinkButton>
                                                                                                                <asp:HiddenField ID="hfparameterunkid" runat="server" Value='<%# Eval("parameterunkid") %>' />
                                                                                                                <asp:HiddenField ID="hfitemunkid" runat="server" Value='<%# Eval("itemunkid") %>' />
                                                                                                                <asp:HiddenField ID="hfselectionmodeid" runat="server" Value='<%# Eval("selectionmodeid") %>' />
                                                                                                                <asp:HiddenField ID="hfitemtypeid" runat="server" Value='<%# Eval("itemtypeid") %>' />
                                                                                                                <asp:HiddenField ID="hfisapplyworkprocess" runat="server" Value='<%# Eval("isapplyworkprocess") %>' />
                                                                                                            </h4>
                                                                                                        </div>
                                                                                                        <div id="PIPItems_<%# Eval("itemunkid") %>" class="panel-collapse collapse" role="tabpanel"
                                                                                                            aria-labelledby="PIPItems_<%# Eval("itemunkid") %>">
                                                                                                            <div class="panel-body">
                                                                                                                <div class="table-responsive" style="max-height: 300px">
                                                                                                                    <asp:DataList ID="dlPIPItemsList" runat="server" Width="100%" OnItemDataBound="dlPIPItemsList_ItemDataBound">
                                                                                                                        <ItemTemplate>
                                                                                                                             <asp:Panel ID="pnlConcernList" runat="server" CssClass="panel detail-box m-b-20">
                                                                                                                                <div class="body">
                                                                                                                                    <div class="row clearfix">
                                                                                                                                         <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                                                                                                            <div>
                                                                                                                                                <asp:Label ID="lblCategoryItemName" Text='<%# Eval("custom_value") %>' runat="server" />
                                                                                                                                                <%--Style="display: inline-block; flex: 1"--%>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                                                                                                                                            <asp:LinkButton runat="server" ID="lnkConcernItemsListEdit" CssClass="m-l-10" OnClick="OnEditConcernItem">
                                                                                                                                                     <i class="fas fa-pencil-alt text-success"></i>
                                                                                                                                            </asp:LinkButton>
                                                                                                                                            <asp:LinkButton runat="server" ID="lnkConcernItemsListDelete" CssClass="m-l-10" OnClick="OnDeleteConcernItem">
                                                                                                                                                     <i class="fas fa-trash text-danger"></i>
                                                                                                                                            </asp:LinkButton>
                                                                                                                                        </div>
                                                                                                                                        <asp:HiddenField ID="hfpipformunkid" runat="server" Value='<%# Eval("pipformunkid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfconcernunkid" runat="server" Value='<%# Eval("concernunkid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfcustomitemunkid" runat="server" Value='<%# Eval("customitemunkid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfitemtypeid" runat="server" Value='<%# Eval("itemtypeid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfselectionmodeid" runat="server" Value='<%# Eval("selectionmodeid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfparameterunkid" runat="server" Value='<%# Eval("parameterunkid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfitemgrpguid" runat="server" Value='<%# Eval("itemgrpguid") %>' />
                                                                                                                                        <asp:HiddenField ID="hfisapplyworkprocess" runat="server" Value='<%# Eval("isapplyworkprocess") %>' />
                                                                                                                                        <asp:HiddenField ID="hfworkprocessunkid" runat="server" Value='<%# Eval("workprocessunkid") %>' />
                                                                                                                                    </div>
                                                                                                                                    <div class="row clearfix">
                                                                                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                                            <div class="title">
                                                                                                                                                <asp:Label ID="lblListWorkProcess" runat="server" Text="Work Process"></asp:Label>
                                                                                                                                            </div>
                                                                                                                                            <div class="content">
                                                                                                                                                <asp:Label ID="txtListWorkProcess" Text='<%# Eval("WorkProcess") %>' runat="server" />
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                             </asp:Panel>   
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:DataList>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </asp:Panel>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPImprovementActionPlan">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <asp:DataList ID="dlImprovementActionPlan" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Panel ID="pnlImprovementPlanList" runat="server">
                                                                    <div class="panel-group" id="pnlImprovementPlan" role="tablist" aria-multiselectable="true">
                                                                        <div class="panel" style="border: 1px solid #ddd">
                                                                            <div class="panel-heading" role="tab" id="ImprovementPlanheadingOne_<%# Eval("itemunkid") %>">
                                                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                                                    <a role="button" data-toggle="collapse" class="ImprovementPlanCollapse" href="#ImprovementPlancollapseOne_<%# Eval("itemunkid") %>"
                                                                                        aria-expanded="false" aria-controls="ImprovementPlancollapseOne_<%# Eval("itemunkid") %>"
                                                                                        style="flex: 1">
                                                                                        <asp:Label ID="lblImprovementPlanName" Text='<%# Eval("item") %>' runat="server" />
                                                                                    </a>
                                                                                    <asp:LinkButton runat="server" ID="lnkAddImprovementActionPlanItem" OnClick="AddImprovementActionPlanItem"
                                                                                        ToolTip="Add New Action Plan">
                                                                                        <i class="fas fa-plus-circle text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                    <asp:HiddenField ID="hfImprovementParameterunkid" runat="server" Value='<%# Eval("parameterunkid") %>' />
                                                                                    <asp:HiddenField ID="hfImprovementItemunkid" runat="server" Value='<%# Eval("itemunkid") %>' />
                                                                                </h4>
                                                                            </div>
                                                                            <div id="ImprovementPlancollapseOne_<%# Eval("itemunkid") %>" class="panel-collapse collapse"
                                                                                role="tabpanel" aria-labelledby="ImprovementPlanheadingOne_<%# Eval("itemunkid") %>">
                                                                                <div class="panel-body">
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                                            <div class="table-responsive" style="max-height: 500px">
                                                                                                <asp:DataList ID="dgvImprovementPlanList" runat="server" Width="100%" OnItemDataBound="dgvImprovementPlanList_ItemDataBound">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="hfimprovementplanunkid" Value='<%# Eval("improvementplanunkid") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfitemunkid" Value='<%# Eval("itemunkid") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfImprovementissubmit" Value='<%# Eval("issubmit") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfImprovementListParameterunkid" runat="server" Value='<%# Eval("parameterunkid") %>' />
                                                                                                        <asp:Panel ID="pnlImprovementPlanItem" runat="server" CssClass="panel detail-box m-b-20">
                                                                                                            <div class="body">
                                                                                                                <div class="row clearfix">
                                                                                                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblImprovementPlanGoal" runat="server" Text="Improvement Goal"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtImprovementPlanGoal" Text='<%# Eval("improvement_goal") %>' runat="server" />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-25 text-right">
                                                                                                                        <asp:LinkButton runat="server" ID="lnkEditImprovementActionPlanItem" OnClick="EditImprovementActionPlanItem"
                                                                                                                            ToolTip="Edit Improvement Action Plan" CssClass="m-r-10" Visible="false">
                                                                                                                                    <i class="fas fa-pencil-alt text-success"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                        <asp:LinkButton runat="server" ID="lnkDeleteImprovementActionPlanItem" OnClick="DeleteImprovementActionPlanItem"
                                                                                                                            ToolTip="Delete Improvement Action Plan" Visible="false">
                                                                                                                                <i class="fas fa-trash text-danger"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row clearfix">
                                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblImprovementPlanGoalDescription" runat="server" Text="Action/Activities"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtImprovementPlanGoalDescription" Text='<%# Eval("improvement_value") %>'
                                                                                                                                runat="server" />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblactionPlanDueDate" runat="server" Text="Due Date"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtactionPlanDueDate" runat="server" Text='<%# Eval("duedate") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </asp:Panel>
                                                                                                    </ItemTemplate>
                                                                                                </asp:DataList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnImprovementSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPProgressMonitoring">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <asp:DataList ID="dlProgressMonitoring" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <asp:Panel ID="pnlProgessMonitoringList" runat="server">
                                                                    <div class="panel-group" id="pnlProgessMonitoring" role="tablist" aria-multiselectable="true">
                                                                        <div class="panel" style="border: 1px solid #ddd">
                                                                            <div class="panel-heading" role="tab" id="ProgessMonitoringheadingOne_<%# Eval("itemunkid") %>">
                                                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                                                    <a role="button" data-toggle="collapse" class="ProgessMonitoringCollapse" href="#ProgessMonitoringcollapseOne_<%# Eval("itemunkid") %>"
                                                                                        aria-expanded="false" aria-controls="ProgessMonitoringcollapseOne_<%# Eval("itemunkid") %>"
                                                                                        style="flex: 1">
                                                                                        <asp:Label ID="lblMonitoringItemName" Text='<%# Eval("item") %>' runat="server" />
                                                                                    </a>
                                                                                    <%-- <asp:LinkButton runat="server" ID="lnkAddProgressMonitoringItem" OnClick="AddProgressMonitoringItem"
                                                                                        ToolTip="Add New Action Plan">
                                                                                        <i class="fas fa-plus-circle text-success"></i>
                                                                                    </asp:LinkButton>--%>
                                                                                   <%-- <asp:HiddenField ID="hfProgressimprovementplanunkid" runat="server" Value='<%# Eval("improvementplanunkid") %>' />--%>
                                                                                    <asp:HiddenField ID="hfProgressParameterunkid" runat="server" Value='<%# Eval("parameterunkid") %>' />
                                                                                    <asp:HiddenField ID="hfProgressItemunkid" runat="server" Value='<%# Eval("itemunkid") %>' />
                                                                                </h4>
                                                                            </div>
                                                                            <div id="ProgessMonitoringcollapseOne_<%# Eval("itemunkid") %>" class="panel-collapse collapse"
                                                                                role="tabpanel" aria-labelledby="ProgessMonitoringheadingOne_<%# Eval("itemunkid") %>">
                                                                                <div class="panel-body">
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                                            <div class="table-responsive" style="max-height: 500px">
                                                                                                <asp:DataList ID="dgvProgessMonitoringList" runat="server" Width="100%" OnItemDataBound="dgvProgessMonitoringList_ItemDataBound">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:HiddenField ID="hfMonitoringimprovementplanunkid" Value='<%# Eval("improvementplanunkid") %>'
                                                                                                            runat="server" />
                                                                                                        <asp:HiddenField ID="hfmonitoringparameterunkid" Value='<%# Eval("parameterunkid") %>'
                                                                                                            runat="server" />
                                                                                                        <asp:HiddenField ID="hfmonitoringitemunkid" Value='<%# Eval("itemunkid") %>' runat="server" />
                                                                                                        <asp:HiddenField ID="hfmonitoringunkid" Value='<%# Eval("pipmoniteringunkid") %>'
                                                                                                            runat="server" />
                                                                                                        <asp:HiddenField ID="hfmonitoringissubmit" Value='<%# Eval("issubmit") %>' runat="server" />
                                                                                                        <asp:Panel ID="pnlProgressMonitoringItem" runat="server" CssClass="panel detail-box m-b-20">
                                                                                                            <div class="body">
                                                                                                                <div class="row clearfix">
                                                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblMonitoringGoal" runat="server" Text="Improvement Goal"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtMonitoringGoal" Text='<%# Eval("improvement_goal") %>' runat="server" />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblMonitoringStatus" runat="server" Text="Status"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtMonitoringStatus" runat="server" CssClass="label label-primary"
                                                                                                                                Text='<%# Eval("status") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-25 text-right">
                                                                                                                        <asp:LinkButton runat="server" ID="lnkAddProgressMonitoringItem" OnClick="AddProgressMonitoringItem"
                                                                                                                            ToolTip="Add Progress" CssClass="m-r-10" Visible="false">
                                                                                                                                    <%--<i class="fas fa-pencil-alt text-success"></i>--%>
                                                                                                                                    <i class="fa fa-spinner" aria-hidden="true" 
                                                                                                                            style="__designer:mapid=&quot;aac7&quot;"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                        <asp:LinkButton runat="server" ID="lnkUndoProgressMonitoringItem" OnClick="UndoProgressMonitoringItem"
                                                                                                                            ToolTip="Undo Progress" Visible="false">
                                                                                                                                <%--<i class="fas fa-trash text-danger"></i>--%>
                                                                                                                                <i class="fa fa-undo" aria-hidden="true" style="color:Red"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="row clearfix">
                                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblMonitoringActivity" runat="server" Text="Action/Activities"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtMonitoringActivity" Text='<%# Eval("improvement_value") %>' runat="server" />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblMonitoringDueDate" runat="server" Text="Due Date"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtMonitoringDueDate" runat="server" Text='<%# Eval("duedate") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                                                                        <div class="title">
                                                                                                                            <asp:Label ID="lblMonitoringUpdatedOn" runat="server" Text="Updated On"></asp:Label>
                                                                                                                        </div>
                                                                                                                        <div class="content">
                                                                                                                            <asp:Label ID="txtMonitoringUpdatedOn" runat="server" Text='<%# Eval("progressdate") %>'></asp:Label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </asp:Panel>
                                                                                                    </ItemTemplate>
                                                                                                </asp:DataList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnMonitoringSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPOverallAssessment">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFinalFormStatus" runat="server" Text="Assessment Status"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboFinalAssementStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <asp:Label ID="LblFinalAssessmentRemark" runat="server" Text="Assessment Remark"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtFinalAssessmentRemark" runat="server" CssClass="form-control"
                                                                    TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnUndoSaveAssessment" runat="server" Text="Undo Assessment" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnSaveFinalAssessment" runat="server" Text="Save Final Assessment"
                                                    CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popupPIPInstruction" runat="server" CancelControlID="btnPIPInstructionClose"
                    PopupControlID="pnlPIPInstruction" TargetControlID="hfPIPInstruction" Drag="true"
                    PopupDragHandleControlID="pnlPIPInstruction" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlPIPInstruction" runat="server" CssClass="modal-dialog card" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPIPInstruction" runat="server" Text="Disclaimer/Instructions" />
                        </h2>
                    </div>
                    <div class="body" style="height: 250px">
                        <div class="form-group">
                            <div class="form-line">
                                <asp:TextBox ID="txtPIPInstruction" runat="server" CssClass="form-control" TextMode="MultiLine"
                                    Rows="7" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnPIPInstructionClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfPIPInstruction" runat="server" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_ListCItemAddEdit" runat="server" TargetControlID="hdf_ListcItem"
                    CancelControlID="hdf_ListcItem" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_ListCItemAddEdit"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ListCItemAddEdit" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCItem" runat="server" Text="Areas of Challenge and Expectations"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 180px">
                        <div class="row clearfix" style="display: none">
                            <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Panel ID="pnlWorkProcess" runat="server" Visible="false">
                                    <asp:Label ID="LblWorkProcess" runat="server" CssClass="form-label" Text="Work Process"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboWorkProcess" runat="server" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <asp:Label ID="lblItemName" runat="server" CssClass="form-label"></asp:Label>
                                <asp:Panel ID="pnlItemNameFreeText" runat="server" Visible="false" CssClass="d--f ai--c">
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 p-l-0">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtItemNameFreeText" runat="server" TextMode="MultiLine" Rows="3"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlItemNameSelection" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboItemNameSelection" runat="server" AutoPostBack="false">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlItemNameDtp" runat="server" Visible="false">
                                    <div class="form-group">
                                        <uc1:DateCtrl ID="dtpItemName" runat="server" AutoPostBack="false" />
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlItemNameNum" runat="server" Visible="false">
                                    <uc4:NumericText ID="txtItemNameNUM" runat="server" Width="100%" AutoPostBack="false" />
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ListcItem" runat="server" />
                        <asp:Button ID="btnListConcernSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnListConcernClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                    CancelControlID="hdf_cItem" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_CItemAddEdit"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblGItem" runat="server" Text="Areas of Challenge and Expectations"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 450px">
                        <div class="row clearfix" style="display: none">
                            <uc1:DateCtrl ID="DateCtrl1" runat="server" AutoPostBack="false" />
                        </div>
                        <asp:DataList ID="dgv_Citems" runat="server" Width="100%">
                            <ItemTemplate>
                                <div class="clearfix d--f ai--c" style="border: 1px solid rgba(204, 204, 204, 0.35);
                                    padding: 10px; margin-bottom: 10px">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("item") %>' CssClass="form-label"></asp:Label>
                                        <asp:HiddenField ID="hfselectedid" runat="server" Value='<%# Eval("selectedid") %>' />
                                        <asp:HiddenField ID="hfselectionmodeid" runat="server" Value='<%# Eval("selectionmodeid") %>' />
                                        <asp:HiddenField ID="hfddate" runat="server" Value='<%# Eval("ddate") %>' />
                                        <asp:HiddenField ID="hfcustomitemunkid" runat="server" Value='<%# Eval("customitemunkid") %>' />
                                        <asp:HiddenField ID="hfitemtypeid" runat="server" Value='<%# Eval("itemtypeid") %>' />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 d--f ai--c">
                                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                            <asp:TextBox ID="txtFreetext" runat="server" TextMode="MultiLine" Rows="3" Visible="false"
                                                Text='<%# Eval("custom_value") %>' class="form-control"></asp:TextBox>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboSelection" runat="server" AutoPostBack="false" Visible="false">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <uc1:DateCtrl ID="dtpSelection" runat="server" AutoPostBack="false" Visible="false" />
                                            </div>
                                            <uc4:NumericText ID="txtNUM" runat="server" Width="100%" AutoPostBack="false" Visible="false"
                                                Text='<%# Eval("custom_value") %>' />
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_cItem" runat="server" />
                        <asp:Button ID="btnConcernSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnConcernClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_ImprovementPlanAddEdit" runat="server" TargetControlID="hdf_ImprovementActionPlan"
                    CancelControlID="hdf_ImprovementActionPlan" BackgroundCssClass="modal-backdrop bd-l2"
                    PopupControlID="pnlImprovementPlanAddEdit" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImprovementPlanAddEdit" runat="server" CssClass="card modal-dialog modal-lg modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblImprovementActionPlanAddEdit" runat="server" Text="Add/Edit Improvement Action Plan"></asp:Label>
                            <small>
                                <asp:Label ID="lblImprovementActionPlanAddEditItem" runat="server" Text=""></asp:Label>
                            </small>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 525px;">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImprovementPlanDueDate" runat="server" Text="Due Date" CssClass="form-label"></asp:Label>
                                <uc1:DateCtrl ID="dtpImprovementPlanDueDate" runat="server" AutoPostBack="false" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblimprovementPlanGoal" runat="server" Text="Improvement Goal" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtImprovementPlanGoal" runat="server" CssClass="form-control" TextMode="MultiLine"
                                            Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblImprovementPlanGoalDescription" runat="server" Text="Action/Activities"
                                    CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtImprovementPlanGoalDescription" runat="server" CssClass="form-control"
                                            TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ImprovementActionPlan" runat="server" />
                        <asp:Button ID="btnSaveImprovementActionPlan" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseImprovementActionPlan" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_ProgressMonitoring" runat="server" TargetControlID="hdf_ProgressMonitoring"
                    CancelControlID="hdf_ProgressMonitoring" BackgroundCssClass="modal-backdrop bd-l2"
                    PopupControlID="pnlProgressMonitoringAddEdit" Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlProgressMonitoringAddEdit" runat="server" CssClass="card modal-dialog modal-lg modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblProgressMonitoringAddEdit" runat="server" Text="Add/Edit Progress"></asp:Label>
                            <small>
                                <asp:Label ID="lblProgressMonitoringAddEditItem" runat="server" Text=""></asp:Label>
                            </small>
                        </h2>
                    </div>
                    <div class="body" style="max-height: 525px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblMonitoringGoal" runat="server" Text="Improvement Goal" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="TxtMonitoringGoal" runat="server" CssClass="form-control" TextMode="MultiLine"
                                            Enabled="false" Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMonitoringGoalDescription" runat="server" Text="Action/Activities"
                                    CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="TxtMonitoringGoalDescription" runat="server" CssClass="form-control"
                                            TextMode="MultiLine" Rows="2" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMonitoringDueDate" runat="server" Text="Due Date" CssClass="form-label"></asp:Label>
                                <uc1:DateCtrl ID="dtpMonitoringDueDate" runat="server" AutoPostBack="false" Enabled="false" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMonitoringStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboMonitoringStatus" runat="server" AutoPostBack="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMonitoringRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMonitoringRemark" runat="server" CssClass="form-control" TextMode="MultiLine"
                                            Rows="2"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ProgressMonitoring" runat="server" />
                        <asp:Button ID="btnUndoProgressMonitoring" runat="server" Text="Undo Progress" CssClass="btn btn-primary" />
                        <asp:Button ID="btnSaveProgressMonitoring" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseProgressMonitoring" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                
                <uc5:DelReason ID="delReason" runat="server" Title="Please enter delete reason?" />
                <asp:HiddenField ID="TabName" runat="server" />
                <asp:HiddenField ID="hfCommentScrollPosition" Value="0" runat="server" />
                <asp:HiddenField ID="CollapsePIPItem" runat="server" />
                <asp:HiddenField ID="CollapsePIPParameter" runat="server" />
                <asp:HiddenField ID="CollapseImprovementPlan" runat="server" />
                <asp:HiddenField ID="CollapseProgressMonitoring" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>

        $(document).ready(function() {
            RetriveTab();
            RetriveCollapse();
        });


        function RetriveTab() {

            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "PIPEmployeeDetail";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }

        function RetriveCollapse() {
            var PIPItemcollapseName = $("[id*=CollapsePIPItem]").val();
            if (PIPItemcollapseName != "") {
                $("#" + PIPItemcollapseName + "").collapse('show');
            }

            $(".PIPItemCollapse").click(function() {
                $("[id*=CollapsePIPItem]").val($(this).attr("href").replace("#", ""));
            });


            var PIPParameterCollapseName = $("[id*=CollapsePIPParameter]").val();
            if (PIPParameterCollapseName != "") {
                $("#" + PIPParameterCollapseName + "").collapse('show');
            }

            $(".PIPCollapseParameter").click(function() {
                $("[id*=CollapsePIPParameter]").val($(this).attr("href").replace("#", ""));
            });

            var ImprovementPlanCollapseName = $("[id*=CollapseImprovementPlan]").val();
            if (ImprovementPlanCollapseName != "") {
                $("#" + ImprovementPlanCollapseName + "").collapse('show');
            }

            $(".ImprovementPlanCollapse").click(function() {
                $("[id*=CollapseImprovementPlan]").val($(this).attr("href").replace("#", ""));
            });


            var ProgressMonitoringCollapseName = $("[id*=CollapseProgressMonitoring]").val();
            if (ProgressMonitoringCollapseName != "") {
                $("#" + ProgressMonitoringCollapseName + "").collapse('show');
            }

            $(".ProgessMonitoringCollapse").click(function() {
                $("[id*=CollapseProgressMonitoring]").val($(this).attr("href").replace("#", ""));
            });


        }
    </script>

</asp:Content>
