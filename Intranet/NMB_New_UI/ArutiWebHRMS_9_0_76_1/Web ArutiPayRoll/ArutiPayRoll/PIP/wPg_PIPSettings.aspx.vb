﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Drawing

Partial Class PIP_wPg_PIPSettings
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPIPSettings"
    Private DisplayMessage As New CommonCodes
    Private mintParameterunkid As Integer = 0
    Private mintParameterItemViewTypeId As Integer = 0
    Private mintWorkProcessunkid As Integer = 0
    Private mintItemUnkid As Integer = 0
    Private mintRatingSetupunkid As Integer = 0
    Private mstrDeleteAction As String = ""
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If Not IsPostBack Then
                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                Call SetLanguage()

                Call FillInstructionSetting()
                Call FillDepartmentCombo()
                Call FillPIPWorkProcess()
                Call FillPIPViewTypeCombo()
                Call FillPIPParameters()
                Call FillPIPItemsCombo()
                Call FillPIPItems()
                Call FillPIPRatingSetUp()
                Call FillRatings()
                Call FillRoleList()

                Call GetProgressMonitoring()
                Call GetAssessorMapping()
                Call cboIItemType_SelectedIndexChanged(cboIItemType, New EventArgs())
                Call rdLineManagerMapping_CheckedChanged(rdLineManagerMapping, New EventArgs())
                Call ClearRatingSetupCtrls()
            Else
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintParameterunkid = CInt(Me.ViewState("mintParameterunkid"))
                mintWorkProcessunkid = CInt(Me.ViewState("mintWorkProcessunkid"))
                mintItemUnkid = CInt(Me.ViewState("mintItemUnkid"))
                mintRatingSetupunkid = CInt(Me.ViewState("mintRatingSetupunkid"))
                mintParameterItemViewTypeId = CInt(Me.ViewState("mintParameterItemViewTypeId"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintParameterunkid") = mintParameterunkid
            Me.ViewState("mintWorkProcessunkid") = mintWorkProcessunkid
            Me.ViewState("mintItemUnkid") = mintItemUnkid
            Me.ViewState("mintRatingSetupunkid") = mintRatingSetupunkid
            Me.ViewState("mintParameterItemViewTypeId") = mintParameterItemViewTypeId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Instrunction "

#Region "Private Method"

    Private Sub FillInstructionSetting()
        Try
            Dim objpipsettings_master As New clspipsettings_master
            Dim blnFlag As Boolean = False
            Try
                Dim PDPSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objpipsettings_master.GetSetting()
                If IsNothing(PDPSetting) = False Then
                    For Each kvp As KeyValuePair(Of clspipsettings_master.enPIPConfiguration, String) In PDPSetting
                        Select Case kvp.Key
                            Case clspipsettings_master.enPIPConfiguration.INSTRUCTION
                                txtInstruction.Text = kvp.Value.ToString()
                        End Select
                    Next
                End If
            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            Finally
                objpipsettings_master = Nothing
            End Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidPDPInstruction() As Boolean
        Try
            If txtInstruction.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please add instruction to continue."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button Method(s)"

    Protected Sub btnInstruction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInstruction.Click
        Dim objsetting As New clspipsettings_master
        Dim blnFlag As Boolean = False
        Try

            If IsValidPDPInstruction() = False Then
                Exit Sub
            End If

            objsetting._AuditUserId = CInt(Session("UserId"))
            objsetting._ClientIP = CStr(Session("IP_ADD"))
            objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            objsetting._FormName = mstrModuleName
            objsetting._FromWeb = True
            objsetting._HostName = CStr(Session("HOST_NAME"))

            Dim pipSetting As New Dictionary(Of clspipsettings_master.enPIPConfiguration, String)

            pipSetting.Add(clspipsettings_master.enPIPConfiguration.INSTRUCTION, txtInstruction.Text)

            objsetting._Roleunkids = ""
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            blnFlag = objsetting.SavePIPSetting(pipSetting)

            If blnFlag = False AndAlso objsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objsetting._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Instruction Saved Successfully."), Me)
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objsetting = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region " PIP Parameters "

#Region " Private Method(s)"

    Private Sub FillPIPViewTypeCombo()
        Dim objpipparameter_master As New clspipparameter_master
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objMst.GetPDPCategoryType(True, "List")
            With cboParameterViewType
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objpipparameter_master.GetList_ApplicableMode("List", True)
            With cboApplicableTo
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearPIPParameterCtrls()
        Try
            mintParameterunkid = 0
            txtParameter.Text = ""
            cboApplicableTo.SelectedValue = CStr(0)
            txtPSortOrder.Text = "0"
            cboParameterViewType.SelectedValue = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillPIPParameters()
        Dim objParameter As New clspipparameter_master
        Dim dsList As New DataSet
        Try
            dsList = objParameter.GetList("List")
            gvParameters.DataSource = dsList.Tables(0)
            gvParameters.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objParameter = Nothing
        End Try
    End Sub

    Private Function IsValidPIPParameter() As Boolean
        Dim objparameter As New clspipparameter_master
        Try
            If txtParameter.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please enter Parameter to continue."), Me)
                Return False

            ElseIf CInt(cboApplicableTo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Please Select Applicable To to continue."), Me)
                cboApplicableTo.Focus()
                Return False

            ElseIf txtPSortOrder.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please enter sort order to continue."), Me)
                Return False

            ElseIf CInt(txtPSortOrder.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, sort order value should be greater than 0."), Me)
                Return False

            ElseIf CInt(cboParameterViewType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please enter Parameter Type to continue."), Me)
                Return False

            ElseIf objparameter.isSortOrderExist(CInt(txtPSortOrder.Text), mintParameterunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, this sort order is already selected for another parameter."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetPIPParameter(ByRef objparameter As clspipparameter_master)
        Try
            objparameter._Parameterunkid = mintParameterunkid
            objparameter._Parameter = txtParameter.Text
            objparameter._ViewTypeId = CInt(cboParameterViewType.SelectedValue)
            objparameter._ApplicableId = CInt(cboApplicableTo.SelectedValue)
            objparameter._SortOrder = CInt(txtPSortOrder.Text)
            objparameter._AuditUserId = CInt(Session("UserId"))
            objparameter._ClientIP = CStr(Session("IP_ADD"))
            objparameter._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objparameter._DatabaseName = CStr(Session("Database_Name"))
            objparameter._FormName = mstrModuleName
            objparameter._FromWeb = True
            objparameter._HostName = CStr(Session("HOST_NAME"))
            objparameter._Isactive = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetPIPParameter(ByVal xParameterId As Integer)
        Dim objParameter As New clspipparameter_master
        Try
            objParameter._Parameterunkid = xParameterId
            txtParameter.Text = CStr(objParameter._Parameter)
            cboApplicableTo.SelectedValue = objParameter._ApplicableId.ToString()
            txtPSortOrder.Text = CStr(objParameter._SortOrder)
            cboParameterViewType.SelectedValue = objParameter._ViewTypeId.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objParameter = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnPSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Dim objparameter As New clspipparameter_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidPIPParameter() = False Then
                Exit Sub
            End If
            SetPIPParameter(objparameter)
            If mintParameterunkid > 0 Then
                blnFlag = objparameter.Update()
            Else
                blnFlag = objparameter.Insert()
            End If

            If blnFlag = False AndAlso objparameter._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objparameter._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "PIP parameter defined successfully."), Me)
                FillPIPParameters()
                ClearPIPParameterCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objparameter = Nothing
        End Try
    End Sub

    Protected Sub btnPReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPReset.Click
        Try
            Call ClearPIPParameterCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkPEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintParameterunkid = CInt(gvParameters.DataKeys(row.RowIndex)("parameterunkid"))
            GetPIPParameter(mintParameterunkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objParameter As New clspipparameter_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintParameterunkid = CInt(gvParameters.DataKeys(row.RowIndex)("parameterunkid"))

            If objParameter.isUsed(mintParameterunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, this parameter is already in use."), Me)
                Exit Sub
            End If

            mstrDeleteAction = "delcat"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "You are about to delete this parameter. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

#End Region

#End Region

#Region "PIP  Work Process"

#Region " Private Method(s)"

    Private Sub FillDepartmentCombo()
        Dim objDepartment As New clsDepartment
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objDepartment.getComboList("List", True)
            With cboDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMst = Nothing
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub ClearPIPWorkProcessCtrls()
        Try
            mintWorkProcessunkid = 0
            cboDepartment.SelectedValue = CStr(0)
            txtName.Text = ""
            txtWDescription.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
        End Try
    End Sub

    Private Sub FillPIPWorkProcess()
        Dim objworkProcess As New clspipworkprocess_master
        Dim dsList As New DataSet
        Try
            dsList = objworkProcess.GetList("List", True)
            gvWorkProcess.DataSource = dsList.Tables(0)
            gvWorkProcess.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objworkProcess = Nothing
        End Try
    End Sub

    Private Function IsValidPIPWorkProcess() As Boolean
        Dim objparameter As New clspipparameter_master
        Try
            If CInt(cboDepartment.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Department is compulsory information.Please Select Department."), Me)
                cboDepartment.Focus()
                Return False

            ElseIf txtName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Name is compulsory information.Please Enter name."), Me)
                txtName.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetPIPWorkProcess(ByRef objWorkProcess As clspipworkprocess_master)
        Try
            objWorkProcess._Workprocessunkid = mintWorkProcessunkid
            objWorkProcess._Departmentunkid = CInt(cboDepartment.SelectedValue)
            objWorkProcess._Name = txtName.Text.Trim
            objWorkProcess._Description = txtWDescription.Text.Trim
            objWorkProcess._AuditUserId = CInt(Session("UserId"))
            objWorkProcess._ClientIP = CStr(Session("IP_ADD"))
            objWorkProcess._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objWorkProcess._DatabaseName = CStr(Session("Database_Name"))
            objWorkProcess._FormName = mstrModuleName
            objWorkProcess._FromWeb = True
            objWorkProcess._HostName = CStr(Session("HOST_NAME"))
            objWorkProcess._Isactive = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetPIPWorkProcess(ByVal xWorkProcessId As Integer)
        Dim objWorkProcess As New clspipworkprocess_master
        Try
            objWorkProcess._Workprocessunkid = xWorkProcessId
            cboDepartment.SelectedValue = objWorkProcess._Departmentunkid.ToString()
            txtName.Text = objWorkProcess._Name.Trim
            txtWDescription.Text = objWorkProcess._Description.Trim
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objWorkProcess = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnWSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWSave.Click
        Dim objWorkProcess As New clspipworkprocess_master
        Dim blnFlag As Boolean = False
        Try
            If IsValidPIPWorkProcess() = False Then
                Exit Sub
            End If
            SetPIPWorkProcess(objWorkProcess)
            If mintWorkProcessunkid > 0 Then
                blnFlag = objWorkProcess.Update()
            Else
                blnFlag = objWorkProcess.Insert()
            End If

            If blnFlag = False AndAlso objWorkProcess._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objWorkProcess._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "PIP work process defined successfully."), Me)
                ClearPIPWorkProcessCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
            objWorkProcess = Nothing
        End Try
    End Sub

    Protected Sub btnWReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWReset.Click
        Try
            Call ClearPIPWorkProcessCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Link Event(s) "

    Protected Sub lnkWEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintWorkProcessunkid = CInt(gvWorkProcess.DataKeys(row.RowIndex)("workprocessunkid"))
            GetPIPWorkProcess(mintWorkProcessunkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
        End Try
    End Sub

    Protected Sub lnkWDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objWorkProcess As New clspipworkprocess_master
        Try
            Dim lnkDelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkDelete).NamingContainer, GridViewRow)

            mintWorkProcessunkid = CInt(gvWorkProcess.DataKeys(row.RowIndex)("workprocessunkid"))

            If objWorkProcess.isUsed(mintWorkProcessunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, this work process is already in use."), Me)
                Exit Sub
            End If

            mstrDeleteAction = "DelWkProcess"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "You are about to delete this work process. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
        End Try
    End Sub

#End Region

#Region "Gridview Events"


    Protected Sub gvWorkProcess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvWorkProcess.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then

                If e.Row.RowType = DataControlRowType.DataRow Then

                    If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        Dim lnkWEdit As LinkButton = TryCast(e.Row.FindControl("lnkWEdit"), LinkButton)
                        lnkWEdit.Visible = False

                        Dim lnkWDelete As LinkButton = TryCast(e.Row.FindControl("lnkWDelete"), LinkButton)
                        lnkWDelete.Visible = False

                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count - 2
                        e.Row.Cells(0).Text = info1.ToTitleCase(DataBinder.Eval(e.Row.DataItem, "department").ToString.ToLower())
                        e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                        e.Row.Font.Bold = True
                        e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        For i As Integer = 3 To e.Row.Cells.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                    End If '  If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then

                End If  '   If e.Row.RowType = DataControlRowType.DataRow Then

            End If '   If e.Row.RowIndex >= 0 Then
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " PIP Items "

#Region " Private Methods "

    Private Sub FillPIPItemsCombo()
        Dim objpipparameter_master As New clspipparameter_master
        Dim objPItems As New clspipitem_master
        Dim objMst As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objpipparameter_master.getListForCombo("List", True)
            With cboIParameter
                .DataValueField = "parameterunkid"
                .DataTextField = "parameter"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objPItems.GetList_CustomTypes("List", True)
            With cboIItemType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With

            dsCombo = objPItems.GetList_SelectionMode("List", True)
            With cboISelection
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipparameter_master = Nothing
            objPItems = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub ClearPIPItemsCtrls()
        Try
            mintItemUnkid = 0
            cboIParameter.SelectedValue = CStr(0)
            cboIItemType.SelectedValue = CStr(0)
            cboISelection.SelectedValue = CStr(0)
            txtIItemText.Text = ""
            txtISortOrder.Text = "0"
            chkApplyWorkProcess.Checked = False
            chkApplyWorkProcess.Enabled = True
            chkImprovementGoal.Checked = False
            chkImprovementGoal.Enabled = True
            If cboIItemType.Enabled = False Then cboIItemType.Enabled = True
            If cboIParameter.Enabled = False Then cboIParameter.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
        End Try
    End Sub

    Private Sub FillPIPItems()
        Dim objItem As New clspipitem_master
        Dim dsList As New DataSet
        Try
            objItem._DatabaseName = Session("Database_Name").ToString()
            dsList = objItem.GetList("List", True, 0)
            gvPIPItem.DataSource = dsList.Tables(0)
            gvPIPItem.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objItem = Nothing
        End Try
    End Sub

    Private Function IsValidPIPItems() As Boolean
        Dim objpipitem_master As New clspipitem_master
        Try
            If CInt(cboIParameter.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Please Select parameter to continue."), Me)
                cboIParameter.Focus()
                Return False

            ElseIf CInt(cboIItemType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Please Select Item Type to continue."), Me)
                cboIItemType.Focus()
                Return False

            ElseIf CInt(cboIItemType.SelectedValue) = clspipitem_master.enPIPCustomType.SELECTION AndAlso CInt(cboISelection.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please choose selection type."), Me)
                cboISelection.Focus()
                Return False

            ElseIf CInt(txtIItemText.Text.Trim.Length) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Please Enter Item Text to continue."), Me)
                txtIItemText.Focus()
                Return False

            ElseIf txtISortOrder.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Please select sort order."), Me)
                txtISortOrder.Focus()
                Return False

            ElseIf CInt(txtISortOrder.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, sort order value should be greater than 0."), Me)
                txtISortOrder.Focus()
                Return False
            End If

            If objpipitem_master.isSortOrderExist(CInt(txtISortOrder.Text), CInt(cboIParameter.SelectedValue), mintItemUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, this sort order is already defined to another item. Please choose another sort order."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetPIPItemsValue(ByRef objItem As clspipitem_master)
        Try

            objItem._Itemunkid = mintItemUnkid
            objItem._Parameterunkid = CInt(cboIParameter.SelectedValue)
            objItem._ItemTypeId = CInt(cboIItemType.SelectedValue)
            objItem._SelectionModeId = CInt(cboISelection.SelectedValue)
            objItem._SortOrder = CInt(txtISortOrder.Text)
            objItem._Item = txtIItemText.Text
            objItem._IsApplyWorkProcess = chkApplyWorkProcess.Checked
            objItem._IsImprovementGoal = chkImprovementGoal.Checked

            objItem._Isactive = True
            objItem._AuditUserId = CInt(Session("UserId"))
            objItem._ClientIP = CStr(Session("IP_ADD"))
            objItem._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objItem._DatabaseName = CStr(Session("Database_Name"))
            objItem._FormName = mstrModuleName
            objItem._FromWeb = True
            objItem._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetPIPItemsValue(ByVal intItemId As Integer)
        Dim objItem As New clspipitem_master
        Try
            objItem._Itemunkid = mintItemUnkid

            cboIParameter.SelectedValue = CStr(objItem._Parameterunkid)
            cboIParameter_SelectedIndexChanged(cboIParameter, New EventArgs())

            cboIItemType.SelectedValue = CStr(objItem._ItemTypeId)
            cboIItemType_SelectedIndexChanged(cboIItemType, New EventArgs())
            cboISelection.SelectedValue = objItem._SelectionModeId.ToString()
            txtISortOrder.Text = CStr(objItem._SortOrder)
            txtIItemText.Text = objItem._Item

            chkApplyWorkProcess.Checked = objItem._IsApplyWorkProcess
            chkApplyWorkProcess_CheckedChanged(chkApplyWorkProcess, New EventArgs())

            chkImprovementGoal.Checked = objItem._IsImprovementGoal
            chkImprovementGoal_CheckedChanged(chkImprovementGoal, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objItem = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnISave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISave.Click
        Dim objItem As New clspipitem_master
        Dim blnFlag As Boolean = False
        Dim dsGetList As DataSet = Nothing
        Try
            If IsValidPIPItems() = False Then Exit Sub

            SetPIPItemsValue(objItem)

            If mintItemUnkid > 0 Then
                blnFlag = objItem.Update()
            Else
                blnFlag = objItem.Insert()
            End If

            If blnFlag = False AndAlso objItem._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objItem._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "PIP Item defined successfully."), Me)
                ClearPIPItemsCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
            objItem = Nothing
            dsGetList = Nothing
        End Try
    End Sub

    Protected Sub btnIReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIReset.Click
        Try
            Call ClearPIPItemsCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkIEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintItemUnkid = CInt(gvPIPItem.DataKeys(row.RowIndex)("itemunkid"))

            GetPIPItemsValue(mintItemUnkid)

            If cboIItemType.Enabled = False Then cboIItemType.Enabled = True
            If cboIParameter.Enabled = False Then cboIParameter.Enabled = True

            If mintItemUnkid > 0 Then
                cboIItemType.Enabled = False
                cboIParameter.Enabled = False
            Else
                cboIItemType.Enabled = True
                cboIParameter.Enabled = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
        End Try
    End Sub

    Protected Sub lnkIDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objpdpitem_master As New clspipitem_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintItemUnkid = CInt(gvPIPItem.DataKeys(row.RowIndex)("itemunkid"))

            If objpdpitem_master.isUsed(mintItemUnkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sorry, Item value is already in used."), Me)
                Exit Sub
            End If

            mstrDeleteAction = "delite"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "You are about to delete this Item. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
            objpdpitem_master = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Protected Sub cboIParameter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIParameter.SelectedIndexChanged
        Dim objParameter As New clspipparameter_master
        Try
            If CInt(cboIParameter.SelectedValue) > 0 Then
                objParameter._Parameterunkid = CInt(cboIParameter.SelectedValue)
                mintParameterItemViewTypeId = objParameter._ViewTypeId
                If objParameter._ViewTypeId = enPDPCustomItemViewType.Table Then
                    chkApplyWorkProcess.Enabled = False
                    chkApplyWorkProcess.Checked = False
                Else
                    chkApplyWorkProcess.Enabled = True
                End If
            Else
                mintParameterItemViewTypeId = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
            objParameter = Nothing
        End Try
    End Sub

    Private Sub cboIItemType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIItemType.SelectedIndexChanged
        Try
            If CInt(cboIItemType.SelectedValue) = clspipitem_master.enPIPCustomType.SELECTION Then
                cboISelection.Enabled = True
            Else
                cboISelection.SelectedValue = "0"
                cboISelection.Enabled = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Protected Sub chkApplyWorkProcess_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkApplyWorkProcess.CheckedChanged
        Try
            chkImprovementGoal.Enabled = Not chkApplyWorkProcess.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
        End Try
    End Sub

    Protected Sub chkImprovementGoal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkImprovementGoal.CheckedChanged
        Try
            chkApplyWorkProcess.Enabled = Not chkImprovementGoal.Checked
            If mintParameterItemViewTypeId = enPDPCustomItemViewType.Table Then chkApplyWorkProcess.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPItems()
            FillPIPWorkProcess()
            FillRatings()
        End Try
    End Sub

#End Region

#Region "Gridview Events"

    Protected Sub gvPIPItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPIPItem.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then

                If e.Row.RowType = DataControlRowType.DataRow Then

                    If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("lnkIEdit"), LinkButton)
                        lnkEdit.Visible = False

                        Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("lnkIDelete"), LinkButton)
                        lnkDelete.Visible = False

                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count - 2
                        e.Row.Cells(0).Text = info1.ToTitleCase(DataBinder.Eval(e.Row.DataItem, "parameter").ToString.ToLower())
                        e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                        e.Row.Font.Bold = True
                        e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        For i As Integer = 3 To e.Row.Cells.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next

                    End If '  If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then

                End If  '   If e.Row.RowType = DataControlRowType.DataRow Then

            End If '   If e.Row.RowIndex >= 0 Then
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Progress Monitoring "

#Region "Private Method"

    Private Sub GetProgressMonitoring()
        Try
            Dim objpipsettings_master As New clspipsettings_master
            Dim blnFlag As Boolean = False
            Try
                Dim PIPSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objpipsettings_master.GetSetting()
                If IsNothing(PIPSetting) = False Then

                    For Each kvp As KeyValuePair(Of clspipsettings_master.enPIPConfiguration, String) In PIPSetting
                        Select Case kvp.Key
                            Case clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING
                                If kvp.Value = CInt(clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER).ToString() Then
                                    rdLineManagerMonitoring.Checked = True
                                ElseIf kvp.Value = CInt(clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH).ToString() Then
                                    rdNominatedCoachMonitoring.Checked = True
                                End If
                        End Select
                    Next

                End If
            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            Finally
                objpipsettings_master = Nothing
            End Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button Method(s)"

    Protected Sub btnProgressMonitoringSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProgressMonitoringSave.Click
        Dim objsetting As New clspipsettings_master
        Dim blnFlag As Boolean = False
        Try

            If IsValidPDPInstruction() = False Then
                Exit Sub
            End If

            objsetting._AuditUserId = CInt(Session("UserId"))
            objsetting._ClientIP = CStr(Session("IP_ADD"))
            objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            objsetting._FormName = mstrModuleName
            objsetting._FromWeb = True
            objsetting._HostName = CStr(Session("HOST_NAME"))

            Dim pipSetting As New Dictionary(Of clspipsettings_master.enPIPConfiguration, String)

            If rdLineManagerMonitoring.Checked Then
                pipSetting.Add(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING, CInt(clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER).ToString())
            ElseIf rdNominatedCoachMonitoring.Checked Then
                pipSetting.Add(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING, CInt(clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH).ToString())
            End If

            objsetting._Roleunkids = ""
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            blnFlag = objsetting.SavePIPSetting(pipSetting)

            If blnFlag = False AndAlso objsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objsetting._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Progress Monitoring Saved Successfully."), Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
            objsetting = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region "Assessor Monitoring "

#Region "Private Method"

    Private Sub GetAssessorMapping()
        Try
            Dim objpipsettings_master As New clspipsettings_master
            Dim blnFlag As Boolean = False
            Try
                Dim PIPSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objpipsettings_master.GetSetting()
                If IsNothing(PIPSetting) = False Then

                    For Each kvp As KeyValuePair(Of clspipsettings_master.enPIPConfiguration, String) In PIPSetting
                        Select Case kvp.Key
                            Case clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING
                                If kvp.Value = CInt(clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER).ToString() Then
                                    rdLineManagerMapping.Checked = True
                                    Call rdLineManagerMapping_CheckedChanged(rdLineManagerMapping, New EventArgs())
                                ElseIf kvp.Value = CInt(clspipsettings_master.enPIPProgressMappingId.ROLE_BASED).ToString() Then
                                    rdRoleBasedMapping.Checked = True
                                    Call rdLineManagerMapping_CheckedChanged(rdRoleBasedMapping, New EventArgs())
                                End If
                        End Select
                    Next

                    Dim dsList As DataSet = objpipsettings_master.GetList("List", clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        cboRole.SelectedValue = dsList.Tables(0).Rows(0)("roleunkids").ToString()
                    End If
                    dsList.Dispose()
                    dsList = Nothing
                End If
            Catch ex As Exception
                DisplayMessage.DisplayError(ex, Me)
            Finally
                objpipsettings_master = Nothing
            End Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillRoleList()
        Dim objRole As New clsUserRole_Master
        Dim dsList As DataSet = Nothing
        Try
            dsList = objRole.getComboList("List", True)
            With cboRole
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
            dsList = Nothing
            objRole = Nothing
        End Try
    End Sub

#End Region

#Region " Button Method(s)"

    Protected Sub btnAssessorMappingSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssessorMappingSave.Click
        Dim objsetting As New clspipsettings_master
        Dim blnFlag As Boolean = False
        Try

            If IsValidPDPInstruction() = False Then
                Exit Sub
            End If

            objsetting._AuditUserId = CInt(Session("UserId"))
            objsetting._ClientIP = CStr(Session("IP_ADD"))
            objsetting._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            objsetting._FormName = mstrModuleName
            objsetting._FromWeb = True
            objsetting._HostName = CStr(Session("HOST_NAME"))

            Dim pipSetting As New Dictionary(Of clspipsettings_master.enPIPConfiguration, String)

            If rdLineManagerMapping.Checked Then
                pipSetting.Add(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING, CInt(clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER).ToString())
            ElseIf rdRoleBasedMapping.Checked Then
                pipSetting.Add(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING, CInt(clspipsettings_master.enPIPProgressMappingId.ROLE_BASED).ToString())
            End If

            objsetting._Roleunkids = cboRole.SelectedValue.ToString()
            objsetting._DatabaseName = CStr(Session("Database_Name"))
            blnFlag = objsetting.SavePIPSetting(pipSetting)

            If blnFlag = False AndAlso objsetting._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objsetting._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Assessor Mapping Saved Successfully."), Me)
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
            objsetting = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region "Rating Setup"

#Region " Private Method(s)"

    Private Sub FillPIPRatingSetUp()
        Dim objRating_master As New clspiprating_master
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objRating_master.GetList_RatingTypes("List", True)
            With cboRatingType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRating_master = Nothing
        End Try
    End Sub

    Private Sub ClearRatingSetupCtrls()
        Try
            mintRatingSetupunkid = 0
            cboRatingType.SelectedValue = CStr(0)
            TxtOptionValue.Text = ""
            TxtDescription.Text = ""
            txtRatingColor.Text = "#000000"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
        End Try
    End Sub

    Private Sub FillRatings(Optional ByVal xCycleId As Integer = 0)
        Dim objRatings As New clspiprating_master
        Dim dsList As New DataSet
        Try
            dsList = objRatings.GetList("List", True, xCycleId)
            gvRatingSetup.DataSource = dsList.Tables(0)
            gvRatingSetup.DataBind()

            'gvRatingSetup.Columns(0).Visible = CBool(Session("AllowToModifyTalentSetting"))
            'gvRatingSetup.Columns(1).Visible = CBool(Session("AllowToModifyTalentSetting"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

    Private Sub SetRatingsValue(ByRef objRatings As clspiprating_master)
        Try
            objRatings._PIPRatingunkid = mintRatingSetupunkid
            objRatings._RatingTypeunkid = CInt(cboRatingType.SelectedValue)
            objRatings._OptionValue = TxtOptionValue.Text.Trim
            objRatings._Description = TxtDescription.Text
            objRatings._Color = txtRatingColor.Text
            objRatings._Userunkid = CInt(Session("UserId"))
            objRatings._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objRatings._DatabaseName = CStr(Session("Database_Name"))

            objRatings._AuditUserId = CInt(Session("UserId"))
            objRatings._ClientIP = CStr(Session("IP_ADD"))
            objRatings._FormName = mstrModuleName
            objRatings._HostName = CStr(Session("HOST_NAME"))
            objRatings._IsWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetRatingsValue(ByVal intRatingsId As Integer)
        Dim objRatings As New clspiprating_master
        Try
            objRatings._PIPRatingunkid = mintRatingSetupunkid
            cboRatingType.SelectedValue = CStr(objRatings._RatingTypeunkid)
            TxtOptionValue.Text = objRatings._OptionValue.ToString()
            TxtDescription.Text = objRatings._Description
            txtRatingColor.Text = objRatings._Color

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatings = Nothing
        End Try
    End Sub

    Private Function IsValidRatings() As Boolean
        Try
            If CInt(cboRatingType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Sorry, Rating Type is mandatory information. Please select Rating Type to continue."), Me)
                cboRatingType.Focus()
                Return False
            ElseIf TxtOptionValue.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, Option Value is mandatory information. Please enter Option Value to continue."), Me)
                TxtOptionValue.Focus()
                Return False
            ElseIf txtRatingColor.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, Color is mandatory information. Please select Color to continue."), Me)
                txtRatingColor.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
        End Try
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnRatingSetupSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRatingSetupSave.Click
        Dim objRating As New clspiprating_master
        Dim blnFlag As Boolean = False
        Dim dsGetList As DataSet = Nothing
        Try
            If IsValidRatings() = False Then Exit Sub

            SetRatingsValue(objRating)

            If mintRatingSetupunkid > 0 Then
                blnFlag = objRating.Update()
            Else
                blnFlag = objRating.Insert()
            End If

            If blnFlag = False AndAlso objRating._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objRating._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "PIP Rating Setup defined successfully."), Me)
                ClearRatingSetupCtrls()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
            objRating = Nothing
            dsGetList = Nothing
        End Try
    End Sub

    Protected Sub btnRatingSetupReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRatingSetupReset.Click
        Try
            Call ClearRatingSetupCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkRatingEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            mintRatingSetupunkid = CInt(gvRatingSetup.DataKeys(row.RowIndex)("pipratingunkid"))
            GetRatingsValue(mintRatingSetupunkid)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
        End Try
    End Sub

    Protected Sub lnkRatingDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objRating_master As New clspiprating_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            mintRatingSetupunkid = CInt(gvRatingSetup.DataKeys(row.RowIndex)("pipratingunkid"))

            If objRating_master.isUsed(mintRatingSetupunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry, Rating Setup is already in used."), Me)
                Exit Sub
            End If

            mstrDeleteAction = "Delete"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "You are about to delete this Rating Setup. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            FillPIPWorkProcess()
            FillPIPItems()
            FillRatings()
            objRating_master = Nothing
        End Try
    End Sub

#End Region

#Region "Gridview Events"

    Protected Sub gvRatingSetup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRatingSetup.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then

            If e.Row.RowType = DataControlRowType.DataRow Then

                    If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        Dim lnkRatingEdit As LinkButton = TryCast(e.Row.FindControl("lnkRatingEdit"), LinkButton)
                        lnkRatingEdit.Visible = False

                        Dim lnkRatingDelete As LinkButton = TryCast(e.Row.FindControl("lnkRatingDelete"), LinkButton)
                        lnkRatingDelete.Visible = False

                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count - 2
                        e.Row.Cells(0).Text = info1.ToTitleCase(DataBinder.Eval(e.Row.DataItem, "RatingType").ToString.ToLower())
                        e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                        e.Row.Font.Bold = True
                        e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        For i As Integer = 3 To e.Row.Cells.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                    Else
                Dim lblColor As Label = TryCast(e.Row.FindControl("objLblcolor"), Label)
                Dim hfColor As HiddenField = TryCast(e.Row.FindControl("hfColor"), HiddenField)
                lblColor.BackColor = Drawing.ColorTranslator.FromHtml(hfColor.Value)
            End If

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()

                Case "DELCAT"
                    Dim objParameter As New clspipparameter_master
                    SetPIPParameter(objParameter)
                    objParameter._Isactive = False
                    blnFlag = objParameter.Delete(mintParameterunkid)
                    If blnFlag = False AndAlso objParameter._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objParameter._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "PIP parameter deleted successfully."), Me)
                        ClearPIPParameterCtrls()
                        FillPIPParameters()
                    End If
                    objParameter = Nothing

                Case "DELWKPROCESS"
                    Dim objWorkProcess As New clspipworkprocess_master
                    SetPIPWorkProcess(objWorkProcess)
                    objWorkProcess._Isactive = False
                    blnFlag = objWorkProcess.Delete(mintWorkProcessunkid)
                    If blnFlag = False AndAlso objWorkProcess._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objWorkProcess._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "PIP work Process deleted successfully."), Me)
                        ClearPIPWorkProcessCtrls()
                    End If
                    objWorkProcess = Nothing

                Case "DELITE"
                    Dim objItem As New clspipitem_master
                    SetPIPItemsValue(objItem)
                    objItem._Isactive = False
                    blnFlag = objItem.Delete(mintItemUnkid)
                    If blnFlag = False AndAlso objItem._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objItem._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "PIP Item deleted successfully."), Me)
                        ClearPIPItemsCtrls()
                    End If
                    objItem = Nothing

                Case "DELETE"
                    Dim objRating As New clspiprating_master
                    SetRatingsValue(objRating)
                    objRating._Isvoid = True
                    objRating._Voiduserunkid = CInt(Session("UserId"))
                    objRating._Voiddatetime = Now

                    blnFlag = objRating.Delete(mintRatingSetupunkid)
                    If blnFlag = False AndAlso objRating._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objRating._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "PIP Rating Setup deleted successfully."), Me)
                        ClearRatingSetupCtrls()
                    End If
                    objRating = Nothing

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region

#Region "Button Events"

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Radio Buttons"

    Protected Sub rdLineManagerMapping_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdLineManagerMapping.CheckedChanged, rdRoleBasedMapping.CheckedChanged
        Try
            If CType(sender, RadioButton).ID.ToUpper() = rdLineManagerMapping.ID.ToUpper() Then
                cboRole.SelectedValue = "0"
                cboRole.Enabled = False
            ElseIf CType(sender, RadioButton).ID.ToUpper() = rdRoleBasedMapping.ID.ToUpper() Then
                cboRole.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPInstruction.ID, Me.lblPIPInstruction.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPParameters.ID, Me.lblPIPParameters.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPItems.ID, Me.lblPIPItems.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPProgressMonitoring.ID, Me.lblPIPProgressMonitoring.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPAssessorMapping.ID, Me.lblPIPAssessorMapping.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblPIPRatingSetups.ID, Me.LblPIPRatingSetups.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.BtnClose.ID, Me.BtnClose.Text)

            'Instructions
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblInstructionHeader.ID, Me.lblInstructionHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnInstruction.ID, Me.btnInstruction.Text)

            'Parameters
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPParameters.ID, Me.lblPIPParameters.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblParameter.ID, lblParameter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblParameterViewType.ID, Me.lblParameterViewType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPSortOrder.ID, Me.lblPSortOrder.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnPSave.ID, Me.btnPSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnPReset.ID, Me.btnPReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvParameters.Columns(2).FooterText, Me.gvParameters.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvParameters.Columns(3).FooterText, Me.gvParameters.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvParameters.Columns(4).FooterText, Me.gvParameters.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvParameters.Columns(5).FooterText, Me.gvParameters.Columns(5).HeaderText)


            'Work Process
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPWorkProcesses.ID, Me.lblPIPWorkProcesses.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lbldepartment.ID, lbldepartment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblName.ID, Me.LblName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblWDescription.ID, Me.LblWDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnWSave.ID, Me.btnWSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnWReset.ID, Me.btnWReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvWorkProcess.Columns(2).FooterText, Me.gvWorkProcess.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvWorkProcess.Columns(3).FooterText, Me.gvWorkProcess.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvWorkProcess.Columns(4).FooterText, Me.gvWorkProcess.Columns(4).HeaderText)


            'Items
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblIPIPItems.ID, Me.lblIPIPItems.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblIParameter.ID, Me.lblIParameter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblIItemType.ID, Me.lblIItemType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblSelectionMode.ID, Me.LblSelectionMode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblISortOrder.ID, Me.lblISortOrder.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblIItemText.ID, Me.lblIItemText.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkApplyWorkProcess.ID, Me.chkApplyWorkProcess.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkImprovementGoal.ID, Me.chkImprovementGoal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnISave.ID, Me.btnISave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnIReset.ID, Me.btnIReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvPIPItem.Columns(2).FooterText, Me.gvPIPItem.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvPIPItem.Columns(3).FooterText, Me.gvPIPItem.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvPIPItem.Columns(4).FooterText, Me.gvPIPItem.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvPIPItem.Columns(5).FooterText, Me.gvPIPItem.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvPIPItem.Columns(6).FooterText, Me.gvPIPItem.Columns(6).HeaderText)

            'Progress Monitoring
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPProgressMonitoring.ID, Me.lblPIPProgressMonitoring.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.rdLineManagerMonitoring.ID, Me.rdLineManagerMonitoring.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.rdNominatedCoachMonitoring.ID, Me.rdNominatedCoachMonitoring.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnProgressMonitoringSave.ID, Me.btnProgressMonitoringSave.Text)


            'Assessor Mapping
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPAssessorMapping.ID, Me.lblPIPAssessorMapping.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.rdLineManagerMapping.ID, Me.rdLineManagerMapping.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.rdRoleBasedMapping.ID, Me.rdRoleBasedMapping.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAssessorMappingSave.ID, Me.btnAssessorMappingSave.Text)

            'Rating Setup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblPIPRating_Setup.ID, Me.LblPIPRating_Setup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRatingType.ID, Me.LblRatingType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOptionValue.ID, Me.LblOptionValue.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblDescription.ID, Me.LblDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblColor.ID, Me.LblColor.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblDescription.ID, Me.LblDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnRatingSetupSave.ID, Me.btnRatingSetupSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnRatingSetupReset.ID, Me.btnRatingSetupReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvRatingSetup.Columns(2).FooterText, Me.gvRatingSetup.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvRatingSetup.Columns(3).FooterText, Me.gvRatingSetup.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvRatingSetup.Columns(4).FooterText, Me.gvRatingSetup.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gvRatingSetup.Columns(5).FooterText, Me.gvRatingSetup.Columns(5).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPIPInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPInstruction.ID, Me.lblPIPInstruction.Text)
            Me.lblPIPParameters.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPParameters.ID, Me.lblPIPParameters.Text)
            Me.lblPIPItems.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPItems.ID, Me.lblPIPItems.Text)
            Me.lblPIPProgressMonitoring.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPProgressMonitoring.ID, Me.lblPIPProgressMonitoring.Text)
            Me.lblPIPAssessorMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPAssessorMapping.ID, Me.lblPIPAssessorMapping.Text)
            Me.LblPIPRatingSetups.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblPIPRatingSetups.ID, Me.LblPIPRatingSetups.Text)
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text)

            'Instructions
            Me.lblInstructionHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInstructionHeader.ID, Me.lblInstructionHeader.Text)
            Me.btnInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnInstruction.ID, Me.btnInstruction.Text)

            'Parameters
            Me.lblPIPParameters.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPParameters.ID, Me.lblPIPParameters.Text)
            Me.lblParameter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParameter.ID, Me.lblParameter.Text)
            Me.lblParameterViewType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParameterViewType.ID, Me.lblParameterViewType.Text)
            Me.lblPSortOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPSortOrder.ID, Me.lblPSortOrder.Text)
            Me.btnPSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnPSave.ID, Me.btnPSave.Text)
            Me.btnPReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnPReset.ID, Me.btnPReset.Text)
            Me.gvParameters.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvParameters.Columns(2).FooterText, Me.gvParameters.Columns(2).HeaderText)
            Me.gvParameters.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvParameters.Columns(3).FooterText, Me.gvParameters.Columns(3).HeaderText)
            Me.gvParameters.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvParameters.Columns(4).FooterText, Me.gvParameters.Columns(4).HeaderText)
            Me.gvParameters.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvParameters.Columns(5).FooterText, Me.gvParameters.Columns(5).HeaderText)


            'Work Process
            Me.lblPIPWorkProcesses.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPWorkProcesses.ID, Me.lblPIPWorkProcesses.Text)
            Me.lbldepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lbldepartment.ID, lbldepartment.Text)
            Me.LblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblName.ID, Me.LblName.Text)
            Me.LblWDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblWDescription.ID, Me.LblWDescription.Text)
            Me.btnWSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnWSave.ID, Me.btnWSave.Text)
            Me.btnWReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnWReset.ID, Me.btnWReset.Text)
            Me.gvWorkProcess.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvWorkProcess.Columns(2).FooterText, Me.gvWorkProcess.Columns(2).HeaderText)
            Me.gvWorkProcess.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvWorkProcess.Columns(3).FooterText, Me.gvWorkProcess.Columns(3).HeaderText)
            Me.gvWorkProcess.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvWorkProcess.Columns(4).FooterText, Me.gvWorkProcess.Columns(4).HeaderText)


            'Items
            Me.lblIPIPItems.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIPIPItems.ID, Me.lblIPIPItems.Text)
            Me.lblIParameter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIParameter.ID, Me.lblIParameter.Text)
            Me.lblIItemType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIItemType.ID, Me.lblIItemType.Text)
            Me.LblSelectionMode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSelectionMode.ID, Me.LblSelectionMode.Text)
            Me.lblISortOrder.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblISortOrder.ID, Me.lblISortOrder.Text)
            Me.lblIItemText.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIItemText.ID, Me.lblIItemText.Text)
            Me.chkApplyWorkProcess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkApplyWorkProcess.ID, Me.chkApplyWorkProcess.Text)
            Me.chkImprovementGoal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkImprovementGoal.ID, Me.chkImprovementGoal.Text)
            Me.btnISave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnISave.ID, Me.btnISave.Text)
            Me.btnIReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnIReset.ID, Me.btnIReset.Text)
            Me.gvPIPItem.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPIPItem.Columns(2).FooterText, Me.gvPIPItem.Columns(2).HeaderText)
            Me.gvPIPItem.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPIPItem.Columns(3).FooterText, Me.gvPIPItem.Columns(3).HeaderText)
            Me.gvPIPItem.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPIPItem.Columns(4).FooterText, Me.gvPIPItem.Columns(4).HeaderText)
            Me.gvPIPItem.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPIPItem.Columns(5).FooterText, Me.gvPIPItem.Columns(5).HeaderText)
            Me.gvPIPItem.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvPIPItem.Columns(6).FooterText, Me.gvPIPItem.Columns(6).HeaderText)

            'Progress Monitoring
            Me.lblPIPProgressMonitorings.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPProgressMonitorings.ID, Me.lblPIPProgressMonitorings.Text)
            Me.rdLineManagerMonitoring.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.rdLineManagerMonitoring.ID, Me.rdLineManagerMonitoring.Text)
            Me.rdNominatedCoachMonitoring.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.rdNominatedCoachMonitoring.ID, Me.rdNominatedCoachMonitoring.Text)
            Me.btnProgressMonitoringSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnProgressMonitoringSave.ID, Me.btnProgressMonitoringSave.Text)

            'Assessor Mapping
            Me.lblPIPAssessorMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPAssessorMapping.ID, Me.lblPIPAssessorMapping.Text)
            Me.rdLineManagerMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.rdLineManagerMapping.ID, Me.rdLineManagerMapping.Text)
            Me.rdRoleBasedMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.rdRoleBasedMapping.ID, Me.rdRoleBasedMapping.Text)
            Me.btnAssessorMappingSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAssessorMappingSave.ID, Me.btnAssessorMappingSave.Text)


            'Rating Setup
            Me.LblPIPRating_Setup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblPIPRating_Setup.ID, Me.LblPIPRating_Setup.Text)
            Me.LblRatingType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRatingType.ID, Me.LblRatingType.Text)
            Me.LblOptionValue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOptionValue.ID, Me.LblOptionValue.Text)
            Me.LblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDescription.ID, Me.LblDescription.Text)
            Me.LblColor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblColor.ID, Me.LblColor.Text)
            Me.LblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDescription.ID, Me.LblDescription.Text)
            Me.btnRatingSetupSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnRatingSetupSave.ID, Me.btnRatingSetupSave.Text)
            Me.btnRatingSetupReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnRatingSetupReset.ID, Me.btnRatingSetupReset.Text)
            Me.gvRatingSetup.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvRatingSetup.Columns(2).FooterText, Me.gvRatingSetup.Columns(2).HeaderText)
            Me.gvRatingSetup.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvRatingSetup.Columns(3).FooterText, Me.gvRatingSetup.Columns(3).HeaderText)
            Me.gvRatingSetup.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvRatingSetup.Columns(4).FooterText, Me.gvRatingSetup.Columns(4).HeaderText)
            Me.gvRatingSetup.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gvRatingSetup.Columns(5).FooterText, Me.gvRatingSetup.Columns(5).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Instruction Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please add instruction to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Please enter Parameter to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Please enter sort order to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, sort order value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Please enter Parameter Type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, this sort order is already selected for another parameter.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "PIP parameter defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, this parameter is already in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "You are about to delete this parameter. Are you sure you want to delete?")


            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "PIP parameter deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Please Select parameter to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Please Select Item Type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Please choose selection type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Please Select Applicable To to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Please Enter Item Text to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Please select sort order.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, this sort order is already defined to another item. Please choose another sort order.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "PIP Item defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Sorry, Item value is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "You are about to delete this Item. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "PDP Item deleted successfully.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Sorry, Rating Type is mandatory information. Please select Rating Type to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, Option Value is mandatory information. Please enter Option Value to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Sorry, Color is mandatory information. Please select Color to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "PIP Rating Setup defined successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Sorry, Rating Setup is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "You are about to delete this Rating Setup. Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "PIP Rating Setup deleted successfully.")


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

