﻿Imports System.Data
Imports Aruti.Data
Imports System.Drawing

Partial Class PIP_wPgEmployeePIPFormList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmployeePIPFormList"
    Private mintPIPFormunkid As Integer
    Private mstrConfirmationAction As String = ""
    Private mstrAdvanceFilter As String = String.Empty


#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call GetControlCaptions()
                Call SetMessages()
                Call FillCombo()
                Call FillList(True)
            Else
                mintPIPFormunkid = CInt(Me.ViewState("mintPIPFormunkid"))
                mstrConfirmationAction = Me.ViewState("mstrConfirmationAction")
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
                FillList(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintPIPFormunkid") = mintPIPFormunkid
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("mstrConfirmationAction") = mstrConfirmationAction
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objRatingSetup As New clspiprating_master
        Dim dsList As New DataSet
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "EmpList")

                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("employeeunkid") = 0
                dRow.Item("EmpCodeName") = "Select"
                dsList.Tables(0).Rows.InsertAt(dRow, 0)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsList = objRatingSetup.getComboList("List", True, clspiprating_master.enPIPRatingType.Final_Assessment)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objRatingSetup = Nothing
        End Try
    End Sub

    Private Sub FillList(Optional ByVal isBlank As Boolean = True)
        Dim dtList As New DataTable
        Dim objPIPFormMaster As New clspipform_master
        Dim StrSearching As String = String.Empty
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND pipform_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(txtPIPForm.Text.Trim.Length) > 0 Then
                StrSearching &= "AND pipform_master.pipformno = '" & CStr(txtPIPForm.Text.Trim) & "' "
            End If

            If dtpFormDate.GetDate <> Nothing Then
                StrSearching &= "AND CONVERT(CHAR(8),pipform_master.pipformdate,112) = " & eZeeDate.convertDate(dtpFormDate.GetDate.Date) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND pipform_master.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If dtpStartDate.GetDate <> Nothing Then
                StrSearching &= "AND CONVERT(CHAR(8),pipform_master.startdate,112) >= " & eZeeDate.convertDate(dtpStartDate.GetDate.Date) & " "
            End If

            If dtpEndDate.GetDate <> Nothing Then
                StrSearching &= "AND CONVERT(CHAR(8),pipform_master.startdate,112) <= " & eZeeDate.convertDate(dtpEndDate.GetDate.Date) & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If

            If isBlank Then
                StrSearching = "AND 1 = 2 "
            End If


            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            'dtList = objPIPFormMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")) _
            '                                          , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
            '                                          , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")) _
            '                                          , True, CBool(Session("IsIncludeInactiveEmp")), "List", True, StrSearching _
            '                                          , CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False))).Tables(0)

            dtList = objPIPFormMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                         , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                                         , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")) _
                                                         , True, CBool(Session("IsIncludeInactiveEmp")), "List", True, StrSearching _
                                                         , False).Tables(0)


            If dtList.Rows.Count <= 0 Then isBlank = True

            dgvPIPFormList.AutoGenerateColumns = False
            dgvPIPFormList.DataSource = dtList
            dgvPIPFormList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

            If dtList IsNot Nothing AndAlso dtList.Rows.Count <= 0 Then
                Dim drRow As DataRow = dtList.NewRow
                drRow("IsGrp") = True
                drRow("pipformunkid") = 0
                dtList.Rows.Add(drRow)
                dgvPIPFormList.DataSource = dtList.Copy()
                dgvPIPFormList.DataBind()

                dgvPIPFormList.Rows(0).Visible = False
            End If
            If dtList IsNot Nothing Then dtList.Clear()
            dtList.Dispose()
            objPIPFormMaster = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByRef objForm As clspipform_master)
        Try
            objForm._PIPFormunkid = mintPIPFormunkid
            objForm._AuditUserId = CInt(Session("UserId"))
            objForm._ClientIP = CStr(Session("IP_ADD"))
            objForm._FormName = mstrModuleName
            objForm._IsWeb = True
            objForm._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            txtPIPForm.Text = ""
            dtpFormDate.SetDate = Nothing
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            mstrAdvanceFilter = ""
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "PIP/wPgEmployeePIPForm.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview's Events"

    Protected Sub dgvPIPFormList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvPIPFormList.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then

                If e.Row.RowType = DataControlRowType.DataRow Then
                    If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                        lnkEdit.Visible = False

                        Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                        lnkDelete.Visible = False

                        Dim ImgPreview As LinkButton = TryCast(e.Row.FindControl("ImgPreview"), LinkButton)
                        ImgPreview.Visible = False

                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count - 7
                        e.Row.Cells(0).Text = info1.ToTitleCase(DataBinder.Eval(e.Row.DataItem, "EmpCodeName").ToString.ToLower())
                        e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                        e.Row.Font.Bold = True
                        e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        For i As Integer = 3 To e.Row.Cells.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                    Else
                        If e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).Text.Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).Text <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).Text).ToShortDateString()
                        End If

                        If e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).Text.Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).Text <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).Text).ToShortDateString()
                        End If

                        If dgvPIPFormList.DataKeys(e.Row.RowIndex)("statusunkid") <> clspipform_master.enPIPStatus.Pending Then
                            Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                            lnkEdit.Visible = False

                            Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                            lnkDelete.Visible = False
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link Button's Events"

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintPIPFormunkid = CInt(dgvPIPFormList.DataKeys(row.RowIndex)("pipformunkid"))
            Session("PIPFormunkid") = mintPIPFormunkid
            Session("LMUserId") = CInt(dgvPIPFormList.DataKeys(row.RowIndex)("LMUserId"))
            Session("CoachUserId") = CInt(dgvPIPFormList.DataKeys(row.RowIndex)("CoachUserId"))
            Response.Redirect(Session("rootpath").ToString & "PIP/wPgEmployeePIPForm.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            Dim dgvPIPFormList As GridView = TryCast(row.NamingContainer, GridView)

            mintPIPFormunkid = CInt(dgvPIPFormList.DataKeys(row.RowIndex)("pipformunkid"))

            mstrConfirmationAction = "delform"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Are you sure you want to delete ?")
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkPreview As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkPreview).NamingContainer, GridViewRow)

            SetDateFormat()

            Dim objPIPFormRpt As New ArutiReports.clsPIPFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objPIPFormRpt._EmployeeUnkid = CInt(dgvPIPFormList.DataKeys(row.RowIndex)("employeeunkid"))
            objPIPFormRpt._EmployeeName = dgvPIPFormList.Rows(row.RowIndex).Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhEmployee", False, True)).Text
            objPIPFormRpt._PIPFormId = CInt(dgvPIPFormList.DataKeys(row.RowIndex)("pipformunkid"))
            objPIPFormRpt._PIPFormNo = dgvPIPFormList.Rows(row.RowIndex).Cells(getColumnID_Griview(dgvPIPFormList, "dgcolhFormNo", False, True)).Text
            objPIPFormRpt._EmployeeAsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            objPIPFormRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objPIPFormRpt._UserName = Session("DisplayName").ToString()
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objPIPFormRpt._UserUnkId = CInt(Session("UserId"))
            End If


            objPIPFormRpt.generateReportNew(CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              Session("UserAccessModeSetting").ToString, True, _
                                              Session("ExportReportPath").ToString, _
                                              CBool(Session("OpenAfterExport")), _
                                              0, enPrintAction.None, enExportAction.None, _
                                              CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objPIPFormRpt._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            objPIPFormRpt = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "emp."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELFORM"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Enter the Reason for deleting this PIP Form.")
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Delete Reason"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Dim objPIPFormMaster As New clspipform_master
        Try
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELFORM"
                    SetValue(objPIPFormMaster)
                    objPIPFormMaster._Isvoid = True
                    objPIPFormMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objPIPFormMaster._Voiduserunkid = CInt(Session("UserId"))
                    objPIPFormMaster._Voidreason = delReason.Reason
                    objPIPFormMaster._Userunkid = CInt(Session("UserId"))

                    blnFlag = objPIPFormMaster.Delete()
                    If blnFlag = False AndAlso objPIPFormMaster._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objPIPFormMaster._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "PIP Form deleted successfully."), Me)
                        mintPIPFormunkid = 0
                        FillList()
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPIPFormMaster = Nothing
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPFormNo.ID, Me.lblPIPFormNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFormDate.ID, Me.lblFormDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStartDate.ID, Me.lblStartDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEndDate.ID, Me.lblEndDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNew.ID, Me.btnNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhFormNo", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhFormNo", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhLineManager", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhLineManager", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhCoach", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhCoach", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStatus", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStatus", False, True)).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPIPFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPFormNo.ID, Me.lblPIPFormNo.Text)
            Me.lblFormDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFormDate.ID, Me.lblFormDate.Text)
            Me.lblFormDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStartDate.ID, Me.lblFormDate.Text)
            Me.lblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEndDate.ID, Me.lblEndDate.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)

            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEmployee", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEmployee", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEmployee", False, True)).HeaderText)
            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhFormNo", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhFormNo", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhFormNo", False, True)).HeaderText)
            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStartDate", False, True)).HeaderText)
            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhEndDate", False, True)).HeaderText)
            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhLineManager", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhLineManager", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhLineManager", False, True)).HeaderText)
            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhCoach", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhCoach", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhCoach", False, True)).HeaderText)
            Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStatus", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStatus", False, True)).FooterText, Me.dgvPIPFormList.Columns(getColumnID_Griview(dgvPIPFormList, "dgcolhStatus", False, True)).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Are you sure you want to delete ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Enter the Reason for deleting this PIP Form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "PIP Form deleted successfully.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
