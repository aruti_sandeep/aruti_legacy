﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.IO
Imports System.Drawing

#End Region

Partial Class PIP_wPgEmployeePIPForm
    Inherits Basepage


#Region "Enum"

    Private Enum enDeleteAction
        DeleteConcernExpectation = 1
        DeleteImprovementPlan = 2
        DeleteProgressMonitoring = 3
        DeleteOverallAssessment = 4
    End Enum

    Private Enum enPIPFormType
        EmployeeDetail = 1
        ConcernExpectation = 2
        ImprovementActionPlan = 3
        ProgressMonitoring = 4
        OverallAssessment = 5
    End Enum

#End Region

#Region "Private Variables"

    'General 
    Private DisplayMessage As New CommonCodes
    Private clsuser As New User
    Private Shared ReadOnly mstrModuleName As String = "frmEmployeePIPForm"
    Private mintFormunkId As Integer = -1
    Private mstrPIPFormNo As String = ""
    Private mintDeleteActionId As Integer = -1
    Private mintConfirmationActionId As Integer = -1
    Private mintFormStatusUnkid As Integer = 0
    Private mstrEmployeeCode As String = ""
    Private mstrEmployee As String = ""
    Private mintPIPConfiguration As Integer = 0
    Private mintLineManagerUserID As Integer = 0
    Private mintCoachUserId As Integer = 0
    Private mintDepartmetunkid As Integer = 0
    Dim enProgressMapping As clspipsettings_master.enPIPProgressMappingId
    Dim enFinalAssessment As clspipsettings_master.enPIPProgressMappingId

    'Personal Analysis Tab
    Private mintParameterId As Integer = -1
    Private mintItemId As Integer = -1
    Private mintConcernTranId As Integer = -1
    Private mintItemtypeid As Integer = -1
    Private mblnIsConcernItemData As Boolean = False
    Private mstrConcernEditGUID As String = ""

    'Personal Analysis Popup-Add/Edit
    Private mblnConcernExpectationListPopup As Boolean = False
    Private mblnConcernExpectationTablePopup As Boolean = False


    ' Improvement Plan Tab
    Private mintImprovementPlanunkid As Integer = -1
    Private mintImprovementPlanParameterId As Integer = -1
    Private mintImprovementPlanItemId As Integer = -1
    Private mblnImprovementPlanPopup As Boolean = False
    Private mblnIsImprovementPlanSubmitted As Boolean = False

    ' Progress Monitoring Tab
    Private mintMonitoringunkid As Integer = -1
    Private mintMonitoringImprovementPlanunkid As Integer = -1
    Private mintMonitoringParameterId As Integer = -1
    Private mintMonitoringItemId As Integer = -1
    Private mblnMonitoringPopup As Boolean = False
    Private mblnIsMonitoringSubmitted As Boolean = False

    Private objCONN As SqlConnection
    Dim mintDirectEmployeeUnkid As Integer = -1
    Dim mblnIsLineManager As Boolean = False


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objRatingSetup As New clspiprating_master
        Try

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then   'MSS

                Dim strFilter As String = " 1=1"

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                            True, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strFilter, False, False)


                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Emp")
                    .SelectedValue = CStr(0)
                    .DataBind()
                End With

            Else  'ESS

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                            -1, _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            False, "Employee", False, Session("Employeeunkid"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", , False)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Employee")
                    .SelectedValue = CStr(Session("Employeeunkid"))
                    .DataBind()
                End With
                Call cboEmployee_SelectedIndexChanged(Nothing, Nothing)

            End If


            dsCombos = objRatingSetup.getComboList("List", True, clspiprating_master.enPIPRatingType.Progress_Monitoring)
            With cboMonitoringStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With


            dsCombos = objRatingSetup.getComboList("List", True, clspiprating_master.enPIPRatingType.Final_Assessment)
            With cboFinalAssementStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRatingSetup = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using

    End Function

    Private Sub Fill_Info(ByVal intEmpId As Integer)
        Dim objCommon As New clsCommon_Master
        Dim objEmployee As New clsEmployee_Master
        Dim objJob As New clsJobs
        Dim objDep As New clsDepartment
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objClass As New clsClass
        Dim dsData As New DataSet
        Dim dtData As New DataTable
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then
                'pnlNoEmployeeSelected.Visible = False
                pnlData.Visible = True

                objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
                objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = intEmpId

                mstrEmployeeCode = objEmployee._Employeecode.ToString()
                mstrEmployee = objEmployee._Firstname.ToString() & " " & objEmployee._Surname.ToString()

                objlblEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                objJob._Jobunkid = objEmployee._Jobunkid
                objlblJob.Text = objJob._Job_Name

                objDeptGroup._Deptgroupunkid = objEmployee._Deptgroupunkid
                txtDepartmentGroup.Text = objDeptGroup._Name

                objDep._Departmentunkid = objEmployee._Departmentunkid
                objlblDepartment.Text = objDep._Name
                mintDepartmetunkid = objEmployee._Departmentunkid

                objClass._Classesunkid = objEmployee._Classunkid
                txtClass.Text = objClass._Name

                txtCodeValue.Text = objEmployee._Employeecode
                objCommon._Masterunkid = objEmployee._Employmenttypeunkid



                If objEmployee._blnImgInDb Then
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                        If objEmployee._Photo IsNot Nothing Then
                            imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                        Else
                            imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
                        End If
                    End If
                End If


                dtData = objEmployee.GetEmpCurrentAndPastExperience(Session("CompanyUnkId"), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")))


                Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))
                If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsList.Tables(0).Rows(0)("employee").ToString().Trim.Length > 0 Then
                    txtLineManagerCoach.Text = dsList.Tables(0).Rows(0)("employee")
                    'mintActionPlanCoachId = CInt(dsList.Tables(0).Rows(0)("reporttoemployeeunkid"))

                    'If CInt(Session("E_Employeeunkid")) = mintActionPlanCoachId Then
                    '    mblnIsLineManager = True
                    'Else
                    '    mblnIsLineManager = False
                    'End If

                Else
                    txtLineManagerCoach.Text = "-"
                    'mintActionPlanCoachId = -1
                    mblnIsLineManager = False
                End If

                '/* START TO GET LAST PIP FORM DATE
                Dim objPIPForm As New clspipform_master
                Dim dsLastPIPForm As DataSet = objPIPForm.getLastEmployeePIPForm(CInt(cboEmployee.SelectedValue), mintFormunkId, "")
                If dsLastPIPForm IsNot Nothing AndAlso dsLastPIPForm.Tables(0).Rows.Count > 0 Then
                    txtPIPFormDate.Text = CDate(dsLastPIPForm.Tables(0).Rows(0)("pipformdate")).ToShortDateString()
                End If
                dsLastPIPForm.Clear()
                dsLastPIPForm.Dispose()
                dsLastPIPForm = Nothing
                objPIPForm = Nothing
                '/* END TO GET LAST PIP FORM DATE


                '/* START CHECKING MONITORING IS SUBMITTED OR NOT
                Dim objImprovmentPlan As New clspipimprovement_plantran
                Dim dsImprovementPlan As DataSet = objImprovmentPlan.GetList("List", mintFormunkId, True, 0, 0, "pipimprovement_plantran.issubmit = 1")
                If dsImprovementPlan IsNot Nothing AndAlso dsImprovementPlan.Tables(0).Rows.Count > 0 Then mblnIsImprovementPlanSubmitted = True
                objImprovmentPlan = Nothing

                If mblnIsImprovementPlanSubmitted Then btnImprovementSubmit.Visible = False
                '/* END CHECKING MONITORING IS SUBMITTED OR NOT

                '/* START CHECKING MONITORING IS SUBMITTED OR NOT
                Dim objMonitoring As New clspipmonitoring_tran
                Dim dsMonitoring As DataSet = objMonitoring.GetList("List", mintFormunkId, True, 0, "pipmonitoring_tran.issubmit = 1")
                If dsMonitoring IsNot Nothing AndAlso dsMonitoring.Tables(0).Rows.Count > 0 Then mblnIsMonitoringSubmitted = True
                objMonitoring = Nothing

                If mblnIsMonitoringSubmitted Then btnMonitoringSubmit.Visible = False
                '/* END CHECKING MONITORING IS SUBMITTED OR NOT



                '/* START TO GET SETTING FOR PROGRESS MONITORING AND FINAL ASSESSMENT

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then   'MSS 
                    Dim objSetting As New clspipsettings_master
                    Dim pipSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objSetting.GetSetting()


                    If pipSetting IsNot Nothing AndAlso pipSetting.Keys.Contains(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING) Then
                        enProgressMapping = pipSetting(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING)
                    End If

                    '/* START CHECKING FOR PROGRESS MAPPING
                    If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then
                        If mblnIsImprovementPlanSubmitted AndAlso mblnIsMonitoringSubmitted Then EnableDisableProgressMonitoringControls(False) Else EnableDisableProgressMonitoringControls(True)
                    ElseIf enProgressMapping = clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH AndAlso mintCoachUserId = CInt(Session("UserId")) Then
                        If mblnIsImprovementPlanSubmitted AndAlso mblnIsMonitoringSubmitted Then EnableDisableProgressMonitoringControls(False) Else EnableDisableProgressMonitoringControls(True)
                    Else
                        btnMonitoringSubmit.Visible = False
                        EnableDisableProgressMonitoringControls(False)
                    End If  ' If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then
                    '/* END CHECKING FOR PROGRESS MAPPING

                    '/* START CHECKING FOR ASSESSOR MAPPING
                    If pipSetting IsNot Nothing AndAlso pipSetting.Keys.Contains(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING) Then
                        enFinalAssessment = pipSetting(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
                    End If

                    If enFinalAssessment = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then
                        If mblnIsImprovementPlanSubmitted AndAlso mblnIsMonitoringSubmitted Then EnableDisableFinalAssessmentControls(True) Else EnableDisableFinalAssessmentControls(False)

                    ElseIf enFinalAssessment = clspipsettings_master.enPIPProgressMappingId.ROLE_BASED Then
                        Dim mstrRoleIds As String = ""
                        Dim dsRoleList As DataSet = objSetting.GetList("List", clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
                        If dsRoleList IsNot Nothing AndAlso dsRoleList.Tables(0).Rows.Count > 0 Then
                            mstrRoleIds = dsRoleList.Tables(0).Rows(0)("roleunkids").ToString()
                        End If

                        If mstrRoleIds.Trim.Length > 0 Then
                            Dim objUserList As New clsUserAddEdit
                            Dim dsUserList As DataSet = objUserList.GetList("List", "hrmsConfiguration..cfuser_master.roleunkid IN (" & mstrRoleIds & ")")
                            objUserList = Nothing

                            If dsUserList IsNot Nothing AndAlso dsUserList.Tables(0).Rows.Count > 0 Then
                                Dim dr = dsUserList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = CInt(Session("UserId")))
                                If dr IsNot Nothing AndAlso dr.Count > 0 Then
                                    If mblnIsImprovementPlanSubmitted AndAlso mblnIsMonitoringSubmitted Then EnableDisableFinalAssessmentControls(True) Else EnableDisableFinalAssessmentControls(False)
                                Else
                                    EnableDisableFinalAssessmentControls(False)
                                End If
                            Else
                                EnableDisableFinalAssessmentControls(False)
                            End If  'If dsUserList IsNot Nothing AndAlso dsUserList.Tables(0).Rows.Count > 0 Then
                        Else
                            EnableDisableFinalAssessmentControls(False)
                        End If '  If mstrRoleIds.Trim.Length > 0 Then 
                    Else
                        EnableDisableFinalAssessmentControls(False)
                    End If ' If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then

                    '/* END CHECKING FOR ASSESSOR MAPPING

                    objSetting = Nothing

                End If '  If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then   'MSS 

                '/* END TO GET SETTING FOR PROGRESS MONITORING AND FINAL ASSESSMENT

                FillPIPParameters()
                FillImprovementPlanItems()
                If mblnIsImprovementPlanSubmitted Then FillProgressMonitoringItems()
            Else
                'pnlNoEmployeeSelected.Visible = True
                pnlData.Visible = False
            End If  '  If CInt(cboEmployee.SelectedValue) > 0 Then

            'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then   'MSS 
            '    If mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING Then
            '        EnableDisableFinalAssessmentControls(False)
            '    ElseIf mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING Then
            '        EnableDisableProgressMonitoringControls(False)
            '    End If
            'ElseIf (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then   'Employee
            '    btnMonitoringSubmit.Visible = False
            '    EnableDisableProgressMonitoringControls(False)
            '    EnableDisableFinalAssessmentControls(False)
            'End If


            '/* START TO HIDING CONTROLS WHEN REQUESTING FOR PROGRESS MONITORING AND FINAL ASSESSMENT.
            'If Me.Request.QueryString.Count > 0 Then
            '    dtpStartDate.Enabled = False
            '    dtpEndDate.Enabled = False
            '    btnImprovementSubmit.Visible = False
            '    If mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING Then
            '        EnableDisableFinalAssessmentControls(False)
            '    ElseIf mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING Then
            '        EnableDisableProgressMonitoringControls(False)
            '        btnMonitoringSubmit.Visible = False
            '    End If
            'ElseIf mblnIsMonitoringSubmitted Then
            '    Dim objMonitoring As New clspipmonitoring_tran
            '    Dim dsMonitoring As DataSet = objMonitoring.GetList("List", mintFormunkId, True, 0, "pipmonitoring_tran.issubmit = 1")
            '    If dsMonitoring IsNot Nothing AndAlso dsMonitoring.Tables(0).Rows.Count > 0 Then
            '        mblnIsMonitoringSubmitted = True
            '        dtpStartDate.Enabled = False
            '        dtpEndDate.Enabled = False
            '        btnImprovementSubmit.Visible = False
            '        btnMonitoringSubmit.Visible = False
            '        EnableDisableProgressMonitoringControls(False)
            '    End If
            '    objMonitoring = Nothing
            'End If
            '/* END TO HIDING CONTROLS WHEN REQUESTING FOR PROGRESS MONITORING AND FINAL ASSESSMENT.


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objClass = Nothing
            objJob = Nothing
            objDep = Nothing
            objDeptGroup = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Dim objPIPForm As New clspipform_master
        Try
            If mintFormunkId > 0 Then
                objPIPForm._PIPFormunkid = mintFormunkId
                txtPIPFormNo.Text = objPIPForm._PIPFormNo.ToString()
                mstrPIPFormNo = objPIPForm._PIPFormNo.ToString()
                cboEmployee.SelectedValue = objPIPForm._Employeeunkid.ToString()
                dtpFormCreationDate.SetDate = objPIPForm._PIPFormDate.Date
                dtpStartDate.SetDate = objPIPForm._StartDate.Date
                dtpEndDate.SetDate = objPIPForm._EndDate.Date
                cboEmployee.Enabled = False
                If objPIPForm._Statusunkid > 0 Then
                    cboFinalAssementStatus.SelectedValue = objPIPForm._Statusunkid
                    TxtFinalAssessmentRemark.Text = objPIPForm._FinalAssessmentRemark
                    btnUndoSaveAssessment.Visible = False
                    btnSaveFinalAssessment.Visible = True
                Else
                    cboFinalAssementStatus.SelectedValue = "0"
                    TxtFinalAssessmentRemark.Text = ""
                    btnUndoSaveAssessment.Visible = False
                End If
            Else
                cboEmployee.Enabled = True
                txtPIPFormNo.Text = ""
                dtpFormCreationDate.SetDate = Now.Date
                cboEmployee.SelectedValue = "0"
                dtpStartDate.SetDate = Nothing
                dtpEndDate.SetDate = Nothing
                cboFinalAssementStatus.SelectedValue = "0"
                TxtFinalAssessmentRemark.Text = ""
                btnUndoSaveAssessment.Visible = False
                btnSaveFinalAssessment.Visible = False
                btnMonitoringSubmit.Visible = False
                EnableDisableProgressMonitoringControls(False)
                EnableDisableFinalAssessmentControls(False)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPIPForm = Nothing
        End Try
    End Sub

    Private Function IsPIPFormDataValid() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information.Please select employee."), Me)
                cboEmployee.Focus()
                Return False
            ElseIf dtpStartDate.IsNull Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Start Date is compulsory information.Please set Start Date."), Me)
                dtpStartDate.Focus()
                Return False
            ElseIf dtpEndDate.IsNull Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "End Date is compulsory information.Please set End Date."), Me)
                dtpEndDate.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

#Region "Area of Concern And Expectation"

    Private Sub Generate_TableCItemPopup_Data(ByVal xParameterUnkid As Integer, ByVal xmstriEditingGUID As String, ByVal xHeaderName As String)
        Dim dtCItems As New DataTable
        Dim objpipform_master As New clspipform_master
        Dim intPeriodid As Integer = -1
        Try
            dtCItems = objpipform_master.Get_CTableItems_ForAddEdit(xParameterUnkid, xmstriEditingGUID)
            If dtCItems IsNot Nothing AndAlso dtCItems.Rows.Count <= 0 Then Exit Sub

            dgv_Citems.DataSource = dtCItems
            dgv_Citems.DataBind()
            mblnConcernExpectationTablePopup = True
            popup_CItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtCItems.Dispose()
        End Try
    End Sub

    Private Sub FillPIPParameters()
        Dim dsData As New DataSet
        Dim objParamater As New clspipparameter_master
        Try
            Dim objitem As New clspipitem_master
            Dim dsItem As DataSet = objitem.GetList("List", False, 0, 0)
            objitem = Nothing
            Dim dtTable As DataTable = New DataView(dsItem.Tables(0), "isimprovementgoal = 0", "", DataViewRowState.CurrentRows).ToTable(True, "parameterunkid")

            'Pinkal (21-Oct-2024) -- Start
            'NMB Enhancement : (A1X-2807) HFC Bank - Automatically log in users with multiple company access to the company that has their employee profiles.
            Dim mstrParameterIds As String = ""
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrParameterIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("parameterunkid").ToString()).ToArray())
            dsData = objParamater.GetList("Parameter", False, "parameterunkid in (" & mstrParameterIds & ")")
            Else
                dsData = objParamater.GetList("Parameter", False, "")
            End If
            dlPIPParameter.DataSource = dsData.Tables("Parameter")
            dlPIPParameter.DataBind()
            'Pinkal (21-Oct-2024) -- End
        
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objParamater = Nothing
        End Try
    End Sub

    Private Sub ClearConcernData()
        Try
            mintParameterId = -1
            mintItemId = -1
            mintItemtypeid = -1
            mintConcernTranId = -1

            mstrConcernEditGUID = ""

            pnlWorkProcess.Visible = False
            cboWorkProcess.SelectedValue = 0

            pnlItemNameFreeText.Visible = False
            txtItemNameFreeText.Text = ""
            txtItemNameFreeText.Enabled = True

            pnlItemNameDtp.Visible = False
            dtpItemName.SetDate = Nothing

            pnlItemNameNum.Visible = False
            txtItemNameNUM.Text = 0

            pnlItemNameSelection.Visible = False
            cboItemNameSelection.SelectedValue = 0

            mintConcernTranId = -1
            mblnIsConcernItemData = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveTablePIPFormMaster(ByVal DtCategoryItems As DataTable)
        Dim objpipform_master As New clspipform_master
        Dim blnFlag As Boolean = False

        Try
            Dim xGuid As String = ""
            If mstrConcernEditGUID.Length > 0 Then
                objpipform_master._PIPFormunkid = mintFormunkId
                xGuid = mstrConcernEditGUID
            Else
                xGuid = Guid.NewGuid().ToString()
                objpipform_master._PIPFormunkid = mintFormunkId
            End If
            objpipform_master._Itemgrpguid = xGuid

            objpipform_master._Employeeunkid = CInt(cboEmployee.SelectedValue)

            objpipform_master._PIPFormDate = dtpFormCreationDate.GetDate.Date
            objpipform_master._StartDate = dtpStartDate.GetDate.Date
            objpipform_master._EndDate = dtpEndDate.GetDate.Date
            objpipform_master._Statusunkid = clspipform_master.enPIPStatus.Pending   '-999
            objpipform_master._FinalAssessmentRemark = ""
            objpipform_master._Parameterid = mintParameterId
            objpipform_master._Concernunkid = mintConcernTranId
            objpipform_master._ViewType = enPDPCustomItemViewType.Table
            objpipform_master._IsConcernData = True
            objpipform_master._DtAreaofConcernItems = DtCategoryItems


            If mblnIsConcernItemData Then

                Dim iCount As Integer = 0
                For Each drrow As DataRow In DtCategoryItems.Rows
                    Dim objconcern As New clspipconcern_expectationTran
                    objconcern._Concern_Value = drrow("fieldvalue").ToString()
                    If objconcern.isValueExist(drrow("fieldvalue").ToString(), xGuid, mintParameterId, CInt(drrow("itemunkid").ToString()), mintFormunkId) Then
                        iCount += 1
                    End If
                Next

                If iCount = DtCategoryItems.Rows.Count Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Item value is already exist."), Me)
                    ClearConcernData()
                    Exit Sub
                End If
            End If


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpipform_master._Userunkid = CInt(Session("UserId"))
                objpipform_master._AuditUserId = CInt(Session("UserId"))
            Else
                objpipform_master._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objpipform_master._ClientIP = CStr(Session("IP_ADD"))
            objpipform_master._FormName = mstrModuleName
            objpipform_master._IsWeb = True
            objpipform_master._HostName = CStr(Session("HOST_NAME"))
            objpipform_master._DatabaseName = CStr(Session("Database_Name"))
            objpipform_master._Isvoid = False


            blnFlag = objpipform_master.SavePIPEmployeeData(CInt(Session("CompanyUnkId")))

            If blnFlag = False AndAlso objpipform_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpipform_master._Message, Me)
            Else
                mintFormunkId = objpipform_master._PIPFormunkid
                mstrPIPFormNo = objpipform_master._PIPFormNo
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Areas of Challenge and Expectations Saved Successfully."), Me)
                ClearConcernData()
                FillPIPParameters()

                If mintFormunkId <= 0 Then
                    mintFormunkId = objpipform_master._PIPFormunkid
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipform_master = Nothing
        End Try
    End Sub

    Private Function SaveListPIPFormMaster(ByVal xFieldValue As String, Optional ByVal xWorkProcessId As Integer = 0, Optional ByVal mblnExpectation As Boolean = True) As Boolean
        Dim objpipform_master As New clspipform_master
        Dim blnFlag As Boolean = False

        Try
            Dim xGuid As String = ""
            If mstrConcernEditGUID.Length > 0 Then
                objpipform_master._PIPFormunkid = mintFormunkId
                xGuid = mstrConcernEditGUID
            Else
                xGuid = Guid.NewGuid().ToString()
                objpipform_master._PIPFormunkid = mintFormunkId
            End If
            objpipform_master._Itemgrpguid = xGuid

            objpipform_master._Employeeunkid = CInt(cboEmployee.SelectedValue)

            objpipform_master._PIPFormDate = dtpFormCreationDate.GetDate.Date
            objpipform_master._StartDate = dtpStartDate.GetDate.Date
            objpipform_master._EndDate = dtpEndDate.GetDate.Date
            objpipform_master._Statusunkid = clspipform_master.enPIPStatus.Pending   '-999
            objpipform_master._FinalAssessmentRemark = ""
            objpipform_master._Parameterid = mintParameterId
            objpipform_master._Concernunkid = mintConcernTranId
            objpipform_master._ViewType = enPDPCustomItemViewType.List
            objpipform_master._IsConcernData = True

            If mblnIsConcernItemData Then

                Dim objconcern As New clspipconcern_expectationTran
                objconcern._Concern_Value = xFieldValue

                If objconcern.isValueExist(xFieldValue, xGuid, mintParameterId, mintItemId, mintFormunkId) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, this data cannot be saved because it's a duplication of an existing record. Please enter something new to continue."), Me)
                    Return False
                End If

                objpipform_master._Workprocessunkid = xWorkProcessId
                objpipform_master._Parameterid = mintParameterId
                objpipform_master._ConcernItemunkid = mintItemId
                objpipform_master._Concernvalue = xFieldValue
                objpipform_master._Concernunkid = mintConcernTranId

            End If

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpipform_master._Userunkid = CInt(Session("UserId"))
                objpipform_master._AuditUserId = CInt(Session("UserId"))
            Else
                objpipform_master._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objpipform_master._ClientIP = CStr(Session("IP_ADD"))
            objpipform_master._FormName = mstrModuleName
            objpipform_master._IsWeb = True
            objpipform_master._HostName = CStr(Session("HOST_NAME"))
            objpipform_master._DatabaseName = CStr(Session("Database_Name"))
            objpipform_master._Isvoid = False

            blnFlag = objpipform_master.SavePIPEmployeeData(CInt(Session("CompanyUnkId")))


            If blnFlag = False AndAlso objpipform_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpipform_master._Message, Me)
                Return False
            Else
                mintFormunkId = objpipform_master._PIPFormunkid
                mstrPIPFormNo = objpipform_master._PIPFormNo
                If mblnExpectation Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Areas of Challenge and Expectations Saved Successfully."), Me)
                End If
                ClearConcernData()
                FillPIPParameters()
                Return True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            objpipform_master = Nothing
        End Try
    End Function

    Private Sub Add_GridColumns(ByVal dgvPIPItems As GridView)
        Try
            Dim iTempField As New TemplateField()

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objEdit"
            iTempField.HeaderText = "Edit"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvPIPItems.Columns.Count > 0 Then
                If dgvPIPItems.Columns(0).FooterText <> iTempField.FooterText Then
                    dgvPIPItems.Columns.Add(iTempField)
                End If
            Else
                dgvPIPItems.Columns.Add(iTempField)
            End If

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objDelete"
            iTempField.HeaderText = "Delete"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvPIPItems.Columns.Count > 1 Then
                If dgvPIPItems.Columns(1).FooterText <> iTempField.FooterText Then
                    dgvPIPItems.Columns.Add(iTempField)
                End If
            Else
                dgvPIPItems.Columns.Add(iTempField)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Improvement Action Plan "

    Private Sub FillImprovementPlanItems()
        Dim dsData As New DataSet
        Dim objpipitem_master As New clspipitem_master
        Try
            dsData = objpipitem_master.GetList("List", False, 0, 1)   'IsimprovementGoal = 1
            Dim dtTable As DataTable = New DataView(dsData.Tables(0), "", "item", DataViewRowState.CurrentRows).ToTable()
            dlImprovementActionPlan.DataSource = dtTable.Copy
            dlImprovementActionPlan.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsData IsNot Nothing Then dsData.Clear()
            dsData = Nothing
            objpipitem_master = Nothing
        End Try
    End Sub

    Private Sub SaveImprovementPlan()
        Dim objpipImprovement As New clspipimprovement_plantran
        Dim blnFlag As Boolean = False
        Try
            If mintImprovementPlanunkid > 0 Then
                objpipImprovement._Improvementplanunkid = mintImprovementPlanunkid
            Else
                objpipImprovement._Improvementplanunkid = -1
            End If

            objpipImprovement._Pipformunkid = mintFormunkId
            objpipImprovement._Parameterunkid = mintImprovementPlanParameterId
            objpipImprovement._Itemunkid = mintImprovementPlanItemId
            objpipImprovement._Improvement_Goal = txtImprovementPlanGoal.Text
            objpipImprovement._Improvement_Value = txtImprovementPlanGoalDescription.Text
            objpipImprovement._IsSubmit = False
            objpipImprovement._Duedate = dtpImprovementPlanDueDate.GetDateTime()

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpipImprovement._AuditUserId = CInt(Session("UserId"))
                objpipImprovement._Userunkid = CInt(Session("UserId"))
            Else
                objpipImprovement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objpipImprovement._ClientIP = CStr(Session("IP_ADD"))
            objpipImprovement._FormName = mstrModuleName
            objpipImprovement._IsWeb = True
            objpipImprovement._HostName = CStr(Session("HOST_NAME"))
            objpipImprovement._DatabaseName = CStr(Session("Database_Name"))
            objpipImprovement._Isvoid = False

            If objpipImprovement.isExist(mintImprovementPlanItemId, mintFormunkId, mintImprovementPlanunkid, txtImprovementPlanGoal.Text.Trim, Nothing) = False Then
                If objpipImprovement.Insert(Nothing) = False Then
                    DisplayMessage.DisplayMessage(objpipImprovement._Message, Me)
                    Exit Sub
                End If
            Else
                If objpipImprovement.Update(Nothing) = False Then
                    DisplayMessage.DisplayMessage(objpipImprovement._Message, Me)
                    Exit Sub
                End If
            End If

            If blnFlag = False AndAlso objpipImprovement._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpipImprovement._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Improvement Action plan Saved Successfully."), Me)
                FillImprovementPlanItems()
                mblnImprovementPlanPopup = False
                popup_ImprovementPlanAddEdit.Hide()
                ClearImprovementPlanPopupControlsAndValues()

                'FillProgressMonitoringItems()  'Progress Monitoring
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipImprovement = Nothing
        End Try
    End Sub

    Private Function IsImprovementActionPlanPopUpDataValid() As Boolean
        Try
            If txtImprovementPlanGoal.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Improvement Goal is compulsory information.Please enter improvement goal."), Me)
                popup_ImprovementPlanAddEdit.Show()
                txtImprovementPlanGoal.Focus()
                Return False

            ElseIf txtImprovementPlanGoalDescription.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Improvement Activity is compulsory information.Please enter improvement activity."), Me)
                popup_ImprovementPlanAddEdit.Show()
                txtImprovementPlanGoalDescription.Focus()
                Return False

            ElseIf dtpImprovementPlanDueDate.IsNull() Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Due Date is mandatory information. Please select due date to continue."), Me)
                popup_ImprovementPlanAddEdit.Show()
                dtpImprovementPlanDueDate.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Sub FillEditActionPlanPopUpDetail()
        Dim dsListItem As New DataSet
        Dim objImprovement As New clspipimprovement_plantran
        Try

            Dim mstrFilter As String = ""

            If mintImprovementPlanunkid > 0 Then
                mstrFilter = "pipimprovement_plantran.improvementplanunkid = " & mintImprovementPlanunkid
            End If

            dsListItem = objImprovement.GetList("ImprovementPlan", mintFormunkId, True, mintImprovementPlanParameterId, mintImprovementPlanItemId, mstrFilter)
            'If IsPDPDataValid(enPDPFormType.DevelopmentActionPlan, enAuditType.EDIT) = False Then Exit Sub

            If IsNothing(dsListItem) = False AndAlso dsListItem.Tables("ImprovementPlan").Rows.Count > 0 Then
                txtImprovementPlanGoal.Text = dsListItem.Tables("ImprovementPlan").Rows(0)("improvement_goal")
                txtImprovementPlanGoalDescription.Text = dsListItem.Tables("ImprovementPlan").Rows(0)("improvement_value")
                dtpImprovementPlanDueDate.SetDate = CDate(dsListItem.Tables("ImprovementPlan").Rows(0)("duedate")).Date
            End If
            mblnImprovementPlanPopup = True
            popup_ImprovementPlanAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearImprovementPlanIds()
        Try
            mintImprovementPlanunkid = -1
            mintImprovementPlanParameterId = -1
            mintImprovementPlanItemId = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearImprovementPlanPopupControlsAndValues()
        Try
            ClearImprovementPlanIds()
            txtImprovementPlanGoal.Text = ""
            txtImprovementPlanGoalDescription.Text = ""
            dtpImprovementPlanDueDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub EnableDisableImprovementPlanControls(Optional ByVal mblnIsEnable As Boolean = True)
        Try
            dlImprovementActionPlan.Enabled = mblnIsEnable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Progress Monitoring"

    Private Sub EnableDisableProgressMonitoringControls(Optional ByVal mblnIsEnable As Boolean = True)
        Try
            dlProgressMonitoring.Enabled = mblnIsEnable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillProgressMonitoringItems()
        Dim dsData As New DataSet
        Dim objImprovementItem As New clspipimprovement_plantran
        Try
            dsData = objImprovementItem.GetList("List", mintFormunkId, True, 0, 0, "")
            Dim dtTable As DataTable = New DataView(dsData.Tables(0), "", "item", DataViewRowState.CurrentRows).ToTable(True, "parameterunkid", "itemunkid", "item")
            dlProgressMonitoring.DataSource = dtTable.Copy()
            dlProgressMonitoring.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objImprovementItem = Nothing
        End Try
    End Sub

    Private Function IsProgressMonitoringPopUpDataValid(ByVal mblnUndoProgress As Boolean) As Boolean
        Try
            If CInt(cboMonitoringStatus.SelectedValue) <= 0 AndAlso mblnUndoProgress = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Status is compulsory information.Please Select Status."), Me)
                popup_ProgressMonitoring.Show()
                cboMonitoringStatus.Focus()
                Return False

            ElseIf txtMonitoringRemark.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Remark is compulsory information.Please enter Remark."), Me)
                popup_ProgressMonitoring.Show()
                txtMonitoringRemark.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Sub FillEditProgressMonitoringPopUpDetail(ByVal mblnIsUndoProgress As Boolean)
        Dim dsListItem As New DataSet
        Dim objMonitoring As New clspipmonitoring_tran
        Try
            dsListItem = objMonitoring.GetList("Monitoring", mintFormunkId, True, mintMonitoringImprovementPlanunkid)

            If IsNothing(dsListItem) = False AndAlso dsListItem.Tables("Monitoring").Rows.Count > 0 Then
                TxtMonitoringGoal.Text = dsListItem.Tables("Monitoring").Rows(0)("improvement_goal")
                TxtMonitoringGoalDescription.Text = dsListItem.Tables("Monitoring").Rows(0)("improvement_value")
                dtpMonitoringDueDate.SetDate = CDate(dsListItem.Tables("Monitoring").Rows(0)("duedate")).Date
                cboMonitoringStatus.SelectedValue = CInt(dsListItem.Tables("Monitoring").Rows(0)("statusunkid"))
                If mblnIsUndoProgress Then cboMonitoringStatus.Enabled = False Else cboMonitoringStatus.Enabled = True
            End If

            mblnMonitoringPopup = True

            btnSaveProgressMonitoring.Visible = Not mblnIsUndoProgress
            btnUndoProgressMonitoring.Visible = mblnIsUndoProgress

            popup_ProgressMonitoring.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveProgressMonitoring(ByVal mblnUndoProgress As Boolean)
        Dim objpipMonitoring As New clspipmonitoring_tran
        Dim blnFlag As Boolean = False
        Try
            If mintMonitoringunkid > 0 Then
                objpipMonitoring._Pipmoniteringunkid = mintMonitoringunkid
            Else
                objpipMonitoring._Pipmoniteringunkid = -1
            End If

            If mblnUndoProgress = False Then
                objpipMonitoring._Pipformunkid = mintFormunkId
                objpipMonitoring._Improvementplanunkid = mintMonitoringImprovementPlanunkid
                objpipMonitoring._Parameterunkid = mintMonitoringParameterId
                objpipMonitoring._Itemunkid = mintMonitoringItemId
                objpipMonitoring._ProgressDate = Now
                objpipMonitoring._IsSubmit = False
                objpipMonitoring._Statusunkid = CInt(cboMonitoringStatus.SelectedValue)
                objpipMonitoring._Remark = txtMonitoringRemark.Text.Trim
            End If


            objpipMonitoring._ClientIP = CStr(Session("IP_ADD"))
            objpipMonitoring._FormName = mstrModuleName
            objpipMonitoring._IsWeb = True
            objpipMonitoring._HostName = CStr(Session("HOST_NAME"))
            objpipMonitoring._DatabaseName = CStr(Session("Database_Name"))

            If mblnUndoProgress = False Then
                objpipMonitoring._Isvoid = False
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objpipMonitoring._AuditUserId = CInt(Session("UserId"))
                    objpipMonitoring._Userunkid = CInt(Session("UserId"))
                End If
            Else
                objpipMonitoring._Isvoid = True
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objpipMonitoring._AuditUserId = CInt(Session("UserId"))
                    objpipMonitoring._Voiduserunkid = CInt(Session("UserId"))
                End If
            End If


            If mblnUndoProgress Then
                If objpipMonitoring.Delete(mintMonitoringunkid, Nothing) = False Then
                    DisplayMessage.DisplayMessage(objpipMonitoring._Message, Me)
                    Exit Sub
                End If
            Else
                If objpipMonitoring.Insert(Nothing) = False Then
                    DisplayMessage.DisplayMessage(objpipMonitoring._Message, Me)
                    Exit Sub
                End If
            End If


            If blnFlag = False AndAlso objpipMonitoring._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpipMonitoring._Message, Me)
                Exit Sub
            Else
                If mblnUndoProgress Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Undo Progress done Successfully."), Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Progress Monitoring Saved Successfully."), Me)
                End If

                FillProgressMonitoringItems()
                mblnMonitoringPopup = False
                popup_ProgressMonitoring.Hide()
                ClearProgressUpdatingPopupControlsAndValues()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipMonitoring = Nothing
        End Try
    End Sub

    Private Sub ClearProgressUpdateIds()
        Try
            mintMonitoringunkid = -1
            mintMonitoringImprovementPlanunkid = -1
            mintMonitoringParameterId = -1
            mintMonitoringItemId = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearProgressUpdatingPopupControlsAndValues()
        Try
            ClearProgressUpdateIds()
            TxtMonitoringGoal.Text = ""
            TxtMonitoringGoalDescription.Text = ""
            dtpMonitoringDueDate.SetDate = Nothing
            cboMonitoringStatus.SelectedValue = "0"
            txtMonitoringRemark.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Final Assessment"

    Private Sub EnableDisableFinalAssessmentControls(Optional ByVal mblnIsEnable As Boolean = True)
        Try
            cboFinalAssementStatus.Enabled = mblnIsEnable
            TxtFinalAssessmentRemark.Enabled = mblnIsEnable
            btnSaveFinalAssessment.Enabled = mblnIsEnable
            btnUndoSaveAssessment.Enabled = mblnIsEnable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsFinalAssessmentDataValid() As Boolean
        Try
            If CInt(cboFinalAssementStatus.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Assessment Status is compulsory information.Please select Assessment Status."), Me)
                cboFinalAssementStatus.Focus()
                Return False
            ElseIf TxtFinalAssessmentRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Assessment Remark is compulsory information.Please enter Assessment Remark."), Me)
                TxtFinalAssessmentRemark.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function IsProgressMonitoringExist() As Boolean
        Dim mblnFlag As Boolean = False
        Dim objMonitoring As New clspipmonitoring_tran
        Try
            mblnFlag = objMonitoring.isExist(mintFormunkId, 0, 0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMonitoring = Nothing
        End Try
        Return mblnFlag
    End Function

#End Region

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count <= 0 Then Exit Sub

                GC.Collect()

                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length > 0 Then
                    mintPIPConfiguration = CInt(arr(0))                                         'PIPConfiguration
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(1))    'Company
                    HttpContext.Current.Session("Employeeunkid") = CInt(arr(2))    'Employee
                    HttpContext.Current.Session("UserId") = CInt(arr(3))                'Approver User Id
                    HttpContext.Current.Session("PIPFormunkid") = CInt(arr(4))    'FormId


                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                    Dim objConfig As New clsConfigOptions
                    Dim mblnATLoginRequiredToApproveApplications As Boolean = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing)
                    objConfig = Nothing

                    If mblnATLoginRequiredToApproveApplications = False Then
                        Dim objBasePage As New Basepage
                        objBasePage.GenerateAuthentication()
                        objBasePage = Nothing
                    End If


                    If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                        If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub

                        Else

                            If mblnATLoginRequiredToApproveApplications = False Then

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If


                    HttpContext.Current.Session("mdbname") = Session("Database_Name")

                    gobjConfigOptions = New clsConfigOptions
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If


                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                    Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate

                    If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Session("ArutiSelfServiceURL") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                    Else
                        Session("ArutiSelfServiceURL") = ConfigParameter._Object._ArutiSelfServiceURL
                    End If

                    Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim
                    Session("DateFormat") = ConfigParameter._Object._CompanyDateFormat
                    Session("DateSeparator") = ConfigParameter._Object._CompanyDateSeparator


                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                    objUser = Nothing

                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                            End If  '   If mblnATLoginRequiredToApproveApplications = False Then

                        End If 'If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then 

                    Else
                        Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                        Session("ApproverUserId") = CInt(Session("UserId"))
                        DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                        Exit Sub

                    End If '  If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                    'Pinkal (23-Feb-2024) -- End


                    Dim objPIPForm As New clspipform_master
                    Dim dsForm As DataSet = objPIPForm.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                                                                      , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "Emp", False, "pipform_master.pipformunkid = " & CInt(Session("PIPFormunkid")), False)

                    If dsForm IsNot Nothing AndAlso dsForm.Tables(0).Rows.Count > 0 Then
                        Session("LMUserId") = CInt(dsForm.Tables(0).Rows(0)("LMUserId"))
                        Session("CoachUserId") = CInt(dsForm.Tables(0).Rows(0)("CoachUserId"))
                        If CInt(dsForm.Tables(0).Rows(0)("statusunkid")) <> clspipform_master.enPIPStatus.Pending Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "This PIP Form is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            objPIPForm = Nothing
                            dsForm.Dispose()
                            dsForm = Nothing

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                            'Pinkal (23-Feb-2024) -- End

                            Exit Sub
                        End If
                    End If
                    dsForm.Dispose()
                    dsForm = Nothing
                    objPIPForm = Nothing


                    If mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING Then
                        TabName.Value = "PIPProgressMonitoring"
                        Dim objMonitoring As New clspipmonitoring_tran
                        Dim dsList As DataSet = objMonitoring.GetList("List", CInt(Session("PIPFormunkid")), True, 0, "pipmonitoring_tran.issubmit = 1")
                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            btnMonitoringSubmit.Visible = False
                            EnableDisableProgressMonitoringControls(False)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Progress review already submitted for this pip form."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            objMonitoring = Nothing

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                            'Pinkal (23-Feb-2024) -- End


                            Exit Sub
                        End If
                        objMonitoring = Nothing
                    ElseIf mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING Then
                        TabName.Value = "PIPOverallAssessment"
                    Else
                        TabName.Value = ""
                    End If

                End If  'If arr.Length > 0 Then

            End If 'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then



            If (Page.IsPostBack = False) Then
                GC.Collect()
                SetControlCaptions()
                SetLanguage()
                SetMessages()
                Call FillCombo()
                imgEmp.Visible = CBool(Session("IsImgInDataBase"))

                If Session("PIPFormunkid") IsNot Nothing AndAlso CInt(Session("PIPFormunkid")) > 0 Then
                    mintFormunkId = CInt(Session("PIPFormunkid"))
                End If

                If Session("LMUserId") IsNot Nothing AndAlso CInt(Session("LMUserId")) > 0 Then
                    mintLineManagerUserID = CInt(Session("LMUserId"))
                End If

                If Session("CoachUserId") IsNot Nothing AndAlso CInt(Session("CoachUserId")) > 0 Then
                    mintCoachUserId = CInt(Session("CoachUserId"))
                End If

                If mintFormunkId <= 0 Then
                    dtpFormCreationDate.SetDate = Now.Date
                ElseIf mintFormunkId > 0 Then
                    dtpStartDate.Enabled = False
                    dtpEndDate.Enabled = False
                End If

                GetValue()
                Call cboEmployee_SelectedIndexChanged(sender, e)

            Else

                If Me.ViewState("mintPIPConfiguration") IsNot Nothing Then
                    mintPIPConfiguration = CInt(Me.ViewState("mintPIPConfiguration"))
                End If

                If Me.ViewState("enProgressMapping") IsNot Nothing Then
                    enProgressMapping = Me.ViewState("enProgressMapping")
                End If

                If Me.ViewState("enFinalAssessment") IsNot Nothing Then
                    enFinalAssessment = Me.ViewState("enFinalAssessment")
                End If

                If Me.ViewState("mblnConcernExpectationTablePopup") IsNot Nothing Then
                    mblnConcernExpectationTablePopup = Me.ViewState("mblnConcernExpectationTablePopup")
                    If mblnConcernExpectationTablePopup Then
                        popup_CItemAddEdit.Show()
                    End If
                End If

                If Me.ViewState("mblnConcernExpectationListPopup") IsNot Nothing Then
                    mblnConcernExpectationListPopup = Me.ViewState("mblnConcernExpectationListPopup")
                    If mblnConcernExpectationListPopup Then
                        popup_ListCItemAddEdit.Show()
                    End If
                End If

                If Me.ViewState("mblnImprovementPlanPopup") IsNot Nothing Then
                    mblnImprovementPlanPopup = Me.ViewState("mblnImprovementPlanPopup")
                    If mblnImprovementPlanPopup Then
                        popup_ImprovementPlanAddEdit.Show()
                    End If
                End If

                If Me.ViewState("mstrConcernEditGUID") IsNot Nothing Then
                    mstrConcernEditGUID = Me.ViewState("mstrConcernEditGUID")
                End If

                If Me.ViewState("mintFormunkId") IsNot Nothing Then
                    mintFormunkId = CInt(Me.ViewState("mintFormunkId"))
                End If

                If Me.ViewState("mintLineManagerUserID") IsNot Nothing Then
                    mintLineManagerUserID = CInt(Me.ViewState("mintLineManagerUserID"))
                End If

                If Me.ViewState("mintCoachUserId") IsNot Nothing Then
                    mintCoachUserId = CInt(Me.ViewState("mintCoachUserId"))
                End If

                If Me.ViewState("mstrPIPFormNo") IsNot Nothing Then
                    mstrPIPFormNo = Me.ViewState("mstrPIPFormNo").ToString()
                End If

                If Me.ViewState("mstrEmployeeCode") IsNot Nothing Then
                    mstrEmployeeCode = Me.ViewState("mstrEmployeeCode").ToString()
                End If

                If Me.ViewState("mstrEmployee") IsNot Nothing Then
                    mstrEmployee = Me.ViewState("mstrEmployee").ToString()
                End If

                If Me.ViewState("mintDepartmetunkid") IsNot Nothing Then
                    mintDepartmetunkid = Me.ViewState("mintDepartmetunkid").ToString()
                End If

                If Me.ViewState("mintFormStatusUnkid") IsNot Nothing Then
                    mintFormStatusUnkid = CInt(Me.ViewState("mintFormStatusUnkid"))
                End If

                If Me.ViewState("mintDeleteActionId") IsNot Nothing Then
                    mintDeleteActionId = CInt(Me.ViewState("mintDeleteActionId"))
                End If

                If Me.ViewState("mintConfirmationActionId") IsNot Nothing Then
                    mintConfirmationActionId = CInt(Me.ViewState("mintConfirmationActionId"))
                End If

                If Me.ViewState("mintParameterId") IsNot Nothing Then
                    mintParameterId = CInt(ViewState("mintParameterId"))
                End If

                If Me.ViewState("mintItemId") IsNot Nothing Then
                    mintItemId = CInt(ViewState("mintItemId"))
                End If

                If Me.ViewState("mintConcernTranId") IsNot Nothing Then
                    mintConcernTranId = CInt(ViewState("mintConcernTranId"))
                End If

                If Me.ViewState("mblnIsConcernItemData") IsNot Nothing Then
                    mblnIsConcernItemData = CBool(Me.ViewState("mblnIsConcernItemData"))
                End If

                If Me.ViewState("mintItemtypeid") IsNot Nothing Then
                    mintItemtypeid = CInt(Me.ViewState("mintItemtypeid"))
                End If

                If Me.ViewState("mintImprovementPlanunkid") IsNot Nothing Then
                    mintImprovementPlanunkid = CInt(Me.ViewState("mintImprovementPlanunkid"))
                End If

                If Me.ViewState("mintImprovementPlanParameterId") IsNot Nothing Then
                    mintImprovementPlanParameterId = Me.ViewState("mintImprovementPlanParameterId")
                End If

                If Me.ViewState("mintImprovementPlanItemId") IsNot Nothing Then
                    mintImprovementPlanItemId = Me.ViewState("mintImprovementPlanItemId")
                End If

                If Me.ViewState("mblnIsImprovementPlanSubmitted") IsNot Nothing Then
                    mblnIsImprovementPlanSubmitted = Me.ViewState("mblnIsImprovementPlanSubmitted")
                End If

                If Me.ViewState("mblnIsLineManager") IsNot Nothing Then
                    mblnIsLineManager = CBool(ViewState("mblnIsLineManager"))
                End If

                If Me.ViewState("mintMonitoringunkid") IsNot Nothing Then
                    mintMonitoringunkid = CInt(ViewState("mintMonitoringunkid"))
                End If

                If Me.ViewState("mintMonitoringImprovementPlanunkid") IsNot Nothing Then
                    mintMonitoringImprovementPlanunkid = CInt(ViewState("mintMonitoringImprovementPlanunkid"))
                End If

                If Me.ViewState("mintMonitoringParameterId") IsNot Nothing Then
                    mintMonitoringParameterId = CInt(ViewState("mintMonitoringParameterId"))
                End If

                If Me.ViewState("mintMonitoringItemId") IsNot Nothing Then
                    mintMonitoringItemId = CInt(ViewState("mintMonitoringItemId"))
                End If

                If Me.ViewState("mblnMonitoringPopup") IsNot Nothing Then
                    mblnMonitoringPopup = CBool(ViewState("mblnMonitoringPopup"))
                End If

                If Me.ViewState("mblnIsMonitoringSubmitted") IsNot Nothing Then
                    mblnIsMonitoringSubmitted = CBool(ViewState("mblnIsMonitoringSubmitted"))
                End If

                FillPIPParameters()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintPIPConfiguration") = mintPIPConfiguration


            Me.ViewState("mblnImprovementPlanPopup") = mblnImprovementPlanPopup
            Me.ViewState("mstrConcernEditGUID") = mstrConcernEditGUID
            Me.ViewState("mintParameterId") = mintParameterId
            Me.ViewState("mintItemId") = mintItemId
            Me.ViewState("mintFormunkId") = mintFormunkId
            Me.ViewState("mstrPIPFormNo") = mstrPIPFormNo
            Me.ViewState("mintFormStatusUnkid") = mintFormStatusUnkid
            Me.ViewState("mstrEmployeeCode") = mstrEmployeeCode
            Me.ViewState("mstrEmployee") = mstrEmployee
            Me.ViewState("mintDepartmetunkid") = mintDepartmetunkid
            Me.ViewState("mblnIsLineManager") = mblnIsLineManager
            Me.ViewState("mintLineManagerUserID") = mintLineManagerUserID
            Me.ViewState("mintCoachUserId") = mintCoachUserId
            Me.ViewState("enProgressMapping") = enProgressMapping
            Me.ViewState("enFinalAssessment") = enFinalAssessment


            Me.ViewState("mblnConcernExpectationTablePopup") = mblnConcernExpectationTablePopup
            Me.ViewState("mblnConcernExpectationListPopup") = mblnConcernExpectationListPopup
            Me.ViewState("mblnIsConcernItemData") = mblnIsConcernItemData
            Me.ViewState("mintItemtypeid") = mintItemtypeid
            Me.ViewState("mintConcernTranId") = mintConcernTranId


            Me.ViewState("mintImprovementPlanunkid") = mintImprovementPlanunkid
            Me.ViewState("mintImprovementPlanParameterId") = mintImprovementPlanParameterId
            Me.ViewState("mintImprovementPlanItemId") = mintImprovementPlanItemId
            Me.ViewState("mintDeleteActionId") = mintDeleteActionId
            Me.ViewState("mintConfirmationActionId") = mintConfirmationActionId
            Me.ViewState("mblnIsImprovementPlanSubmitted") = mblnIsImprovementPlanSubmitted

            Me.ViewState("mintMonitoringunkid") = mintMonitoringunkid
            Me.ViewState("mintMonitoringImprovementPlanunkid") = mintMonitoringImprovementPlanunkid
            Me.ViewState("mintMonitoringParameterId") = mintMonitoringParameterId
            Me.ViewState("mintMonitoringItemId") = mintMonitoringItemId
            Me.ViewState("mblnMonitoringPopup") = mblnMonitoringPopup
            Me.ViewState("mblnIsMonitoringSubmitted") = mblnIsMonitoringSubmitted

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combo Events "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Call Fill_Info(CInt(cboEmployee.SelectedValue))
                Else
                    Call Fill_Info(CInt(Session("Employeeunkid")))
                End If
            Else
                imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
                btnMonitoringSubmit.Visible = False
                btnSaveFinalAssessment.Visible = False
                btnUndoSaveAssessment.Visible = False
                EnableDisableProgressMonitoringControls(False)
                EnableDisableFinalAssessmentControls(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Button/Link Events"

#Region "Instruction"

    Protected Sub lnkPipInstruction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPipInstruction.Click
        Dim objpipsettings_master As New clspipsettings_master
        Dim mdicPIPSetting As New Dictionary(Of clspipsettings_master.enPIPConfiguration, String)
        Try
            mdicPIPSetting = objpipsettings_master.GetSetting()
            If IsNothing(mdicPIPSetting) = False AndAlso mdicPIPSetting.ContainsKey(clspipsettings_master.enPIPConfiguration.INSTRUCTION) Then
                txtPIPInstruction.Text = mdicPIPSetting(clspipsettings_master.enPIPConfiguration.INSTRUCTION)
            End If
            popupPIPInstruction.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Area of Concern and Expectation"

    'ADD PIP Item - Table
    Protected Sub OnGvAddPIPItem(ByVal sender As Object, ByVal e As EventArgs)
        Try

            If IsPIPFormDataValid() = False Then Exit Sub

            'If IsPDPDataValid(enPDPFormType.PersonalAnalysisandGoals, enAuditType.ADD) = False Then Exit Sub
            'ClearPersolnalGoalPopupData()
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = dlPIPParameter.Items(item.ItemIndex)
            Dim hfparameterunkid As HiddenField = TryCast(row.FindControl("hfparameterunkid"), HiddenField)

            mintParameterId = hfparameterunkid.Value
            mintConcernTranId = -1
            mblnIsConcernItemData = True

            Generate_TableCItemPopup_Data(hfparameterunkid.Value, "", "Add Data")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub OnGvEditPIPItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsPIPFormDataValid() = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim row As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)
            Dim Childgrid As GridView = TryCast(row.Parent.Parent, GridView)

            Dim strGuid = Childgrid.DataKeys(row.RowIndex)("GUID")
            Dim intParameterunkid = Childgrid.DataKeys(row.RowIndex)("Header_Id")
            Dim intFormunkid = Childgrid.DataKeys(row.RowIndex)("pipformunkid")


            If strGuid.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, there is no data to be edited."), Me)
                Exit Sub
            End If

            mintParameterId = intParameterunkid
            mstrConcernEditGUID = strGuid
            mintFormunkId = intFormunkid

            Generate_TableCItemPopup_Data(intParameterunkid, strGuid, "Add Data")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub OnGvDeletePIPItem(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim row As GridViewRow = TryCast((TryCast(sender, LinkButton)).NamingContainer, GridViewRow)
            Dim Childgrid As GridView = TryCast(row.Parent.Parent, GridView)

            Dim strGuid = Childgrid.DataKeys(row.RowIndex)("GUID")
            Dim intParameterunkid = Childgrid.DataKeys(row.RowIndex)("Header_Id")
            Dim intFormunkid = Childgrid.DataKeys(row.RowIndex)("pipformunkid")

            If strGuid.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sorry, there is no data to be deleted."), Me)
                Exit Sub
            End If

            mintParameterId = intParameterunkid
            mstrConcernEditGUID = strGuid
            mintFormunkId = intFormunkid

            delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Please enter delete reason")
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeleteConcernExpectation)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Concern And Expectation -List
    Protected Sub OnAddConcernItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpipparameter_master As New clspipparameter_master
        Dim objpipitem_master As New clspipitem_master
        Try

            If IsPIPFormDataValid() = False Then Exit Sub

            ClearConcernData()

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim hfparameterunkid As HiddenField = TryCast(item.FindControl("hfparameterunkid"), HiddenField)
            Dim hfitemtypeid As HiddenField = TryCast(item.FindControl("hfitemtypeid"), HiddenField)
            Dim hfitemunkid As HiddenField = TryCast(item.FindControl("hfitemunkid"), HiddenField)
            Dim hfselectionmodeid As HiddenField = TryCast(item.FindControl("hfselectionmodeid"), HiddenField)
            Dim hfisapplyworkprocess As HiddenField = TryCast(item.FindControl("hfisapplyworkprocess"), HiddenField)

            mintParameterId = hfparameterunkid.Value
            mintItemId = hfitemunkid.Value
            mintItemtypeid = hfitemtypeid.Value
            mintConcernTranId = -1
            mblnIsConcernItemData = True


            If CBool(hfisapplyworkprocess.Value) Then
                pnlWorkProcess.Visible = True
                LblWorkProcess.Visible = True
                cboWorkProcess.Visible = True

                Dim objWorkProcess As New clspipworkprocess_master
                Dim dsWorkProcess As DataSet = objWorkProcess.getListForCombo(mintDepartmetunkid, "List", True, Nothing)
                With cboWorkProcess
                    .DataTextField = "name"
                    .DataValueField = "workprocessunkid"
                    .DataSource = dsWorkProcess.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                If dsWorkProcess Is Nothing Then dsWorkProcess.Clear()
                dsWorkProcess.Dispose()
                dsWorkProcess = Nothing
                objWorkProcess = Nothing
            Else
                pnlWorkProcess.Visible = False
                LblWorkProcess.Visible = False
                cboWorkProcess.Visible = False
            End If

            If CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.FREE_TEXT) Then
                pnlItemNameFreeText.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.DATE_SELECTION) Then
                pnlItemNameDtp.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.NUMERIC_DATA) Then
                pnlItemNameNum.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.SELECTION) Then
                pnlItemNameSelection.Visible = True

                Select Case CInt(hfselectionmodeid.Value)
                    Case clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES, clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES
                        Dim dtab As DataTable = Nothing
                        Dim dsList As New DataSet

                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                        objCMaster = Nothing

                        If CInt(hfselectionmodeid.Value) = clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                        ElseIf CInt(hfselectionmodeid.Value) = clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                        End If
                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dtab
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With

                End Select


            End If

            objpipparameter_master._Parameterunkid = CInt(hfparameterunkid.Value)
            objpipitem_master._Itemunkid = CInt(hfitemunkid.Value)
            'lblCItem.Text = objpipparameter_master._Parameter
            lblItemName.Text = objpipitem_master._Item

            mblnConcernExpectationListPopup = True
            popup_ListCItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipparameter_master = Nothing
            objpipitem_master = Nothing
        End Try
    End Sub

    Protected Sub OnEditConcernItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpipitem_master As New clspipitem_master
        Dim objpipform_master As New clspipform_master
        Try

            If IsPIPFormDataValid() = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim hfpipformunkid As HiddenField = TryCast(item.FindControl("hfpipformunkid"), HiddenField)
            Dim hfconcernunkid As HiddenField = TryCast(item.FindControl("hfconcernunkid"), HiddenField)
            Dim hfcustomitemunkid As HiddenField = TryCast(item.FindControl("hfcustomitemunkid"), HiddenField)
            Dim hfitemtypeid As HiddenField = TryCast(item.FindControl("hfitemtypeid"), HiddenField)
            Dim hfselectionmodeid As HiddenField = TryCast(item.FindControl("hfselectionmodeid"), HiddenField)
            Dim hfparameterunkid As HiddenField = TryCast(item.FindControl("hfparameterunkid"), HiddenField)
            Dim hfitemgrpguid As HiddenField = TryCast(item.FindControl("hfitemgrpguid"), HiddenField)
            Dim hfisapplyworkprocess As HiddenField = TryCast(item.FindControl("hfisapplyworkprocess"), HiddenField)
            Dim hfworkprocessunkid As HiddenField = TryCast(item.FindControl("hfworkprocessunkid"), HiddenField)


            If CBool(hfisapplyworkprocess.Value) Then
                pnlWorkProcess.Visible = True
                LblWorkProcess.Visible = True
                cboWorkProcess.Visible = True

                Dim objWorkProcess As New clspipworkprocess_master
                Dim dsWorkProcess As DataSet = objWorkProcess.getListForCombo(mintDepartmetunkid, "List", True, Nothing)
                With cboWorkProcess
                    .DataTextField = "name"
                    .DataValueField = "workprocessunkid"
                    .DataSource = dsWorkProcess.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                If dsWorkProcess Is Nothing Then dsWorkProcess.Clear()
                dsWorkProcess.Dispose()
                dsWorkProcess = Nothing
                objWorkProcess = Nothing

                cboWorkProcess.SelectedValue = CInt(hfworkprocessunkid.Value)

            Else
                pnlWorkProcess.Visible = False
                LblWorkProcess.Visible = False
                cboWorkProcess.Visible = False
            End If

            objpipitem_master._Itemunkid = CInt(hfcustomitemunkid.Value)
            lblItemName.Text = objpipitem_master._Item
            mstrConcernEditGUID = hfitemgrpguid.Value
            mintParameterId = hfparameterunkid.Value
            mintItemId = hfcustomitemunkid.Value

            mintFormunkId = hfpipformunkid.Value
            mintConcernTranId = hfconcernunkid.Value
            mintItemtypeid = hfitemtypeid.Value


            If CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.FREE_TEXT) Then
                pnlItemNameFreeText.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.DATE_SELECTION) Then
                pnlItemNameDtp.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.NUMERIC_DATA) Then
                pnlItemNameNum.Visible = True
            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.SELECTION) Then
                pnlItemNameSelection.Visible = True

                Select Case CInt(hfselectionmodeid.Value)

                    Case clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES, clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES
                        Dim dtab As DataTable = Nothing
                        Dim dsList As New DataSet

                        Dim objCMaster As New clsCommon_Master
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                        objCMaster = Nothing

                        If CInt(hfselectionmodeid.Value) = clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                        ElseIf CInt(hfselectionmodeid.Value) = clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                            dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                        End If
                        With cboItemNameSelection
                            .DataValueField = "masterunkid"
                            .DataTextField = "name"
                            .DataSource = dtab
                            .ToolTip = "name"
                            .SelectedValue = 0
                            .DataBind()
                        End With
                End Select

            End If

            Dim dtCustomItemValue As New DataTable
            dtCustomItemValue = objpipform_master.Get_CListItems_ForAddEdit(hfconcernunkid.Value)


            If CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.FREE_TEXT) Then
                txtItemNameFreeText.Enabled = True
                txtItemNameFreeText.Text = dtCustomItemValue.Rows(0)("customvalueId").ToString()

            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.DATE_SELECTION) Then
                dtpItemName.SetDate() = eZeeDate.convertDate(dtCustomItemValue.Rows(0)("customvalueId").ToString).Date

            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.NUMERIC_DATA) Then
                txtItemNameNUM.Text = dtCustomItemValue.Rows(0)("customvalueId").ToString()

            ElseIf CInt(hfitemtypeid.Value) = CInt(clspipitem_master.enPIPCustomType.SELECTION) Then
                cboItemNameSelection.SelectedValue = dtCustomItemValue.Rows(0)("customvalueId").ToString()

            End If
            mblnIsConcernItemData = True
            mblnConcernExpectationListPopup = True
            popup_ListCItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipitem_master = Nothing
            objpipform_master = Nothing
        End Try
    End Sub

    Public Sub OnDeleteConcernItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)

            Dim hfconcernunkid As HiddenField = TryCast(item.FindControl("hfconcernunkid"), HiddenField)
            Dim hfcustomitemunkid As HiddenField = TryCast(item.FindControl("hfcustomitemunkid"), HiddenField)
            Dim hfitemtypeid As HiddenField = TryCast(item.FindControl("hfitemtypeid"), HiddenField)
            Dim hfselectionmodeid As HiddenField = TryCast(item.FindControl("hfselectionmodeid"), HiddenField)
            Dim hfparameterunkid As HiddenField = TryCast(item.FindControl("hfparameterunkid"), HiddenField)
            Dim hfitemgrpguid As HiddenField = TryCast(item.FindControl("hfitemgrpguid"), HiddenField)
            Dim hfpipformunkid As HiddenField = TryCast(item.FindControl("hfpipformunkid"), HiddenField)

            mstrConcernEditGUID = hfitemgrpguid.Value
            mintFormunkId = hfpipformunkid.Value
            mintParameterId = hfparameterunkid.Value
            mintConcernTranId = hfconcernunkid.Value

            If hfitemgrpguid.Value.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, No goal item has been added in order to delete."), Me)
                Exit Sub
            End If

            delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Please enter delete reason")
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeleteConcernExpectation)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Improvement Plan"

    Protected Sub AddImprovementActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpipitem_master As New clspipitem_master
        Try

            If IsPIPFormDataValid() = False Then Exit Sub


            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim row As DataListItem = dlImprovementActionPlan.Items(item.ItemIndex)
            Dim hfImprovementParameterunkid As HiddenField = TryCast(row.FindControl("hfImprovementParameterunkid"), HiddenField)
            Dim hfImprovementItemunkid As HiddenField = TryCast(row.FindControl("hfImprovementItemunkid"), HiddenField)

            mintImprovementPlanParameterId = hfImprovementParameterunkid.Value
            mintImprovementPlanItemId = hfImprovementItemunkid.Value

            objpipitem_master._Itemunkid = mintImprovementPlanItemId
            lblImprovementActionPlanAddEditItem.Text = objpipitem_master._Item
            mblnIsConcernItemData = False
            mblnImprovementPlanPopup = True
            popup_ImprovementPlanAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub EditImprovementActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If IsPIPFormDataValid() = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvImprovementPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfitemunkid As HiddenField = CType(dgvImprovementPlanList.Items(item.ItemIndex).FindControl("hfitemunkid"), HiddenField)
            Dim hfimprovementplanunkid As HiddenField = CType(dgvImprovementPlanList.Items(item.ItemIndex).FindControl("hfimprovementplanunkid"), HiddenField)
            Dim hfImprovementListParameterunkid As HiddenField = TryCast(dgvImprovementPlanList.Items(item.ItemIndex).FindControl("hfImprovementListParameterunkid"), HiddenField)

            mintImprovementPlanParameterId = CInt(hfImprovementListParameterunkid.Value)
            mintImprovementPlanItemId = CInt(hfitemunkid.Value)
            mintImprovementPlanunkid = CInt(hfimprovementplanunkid.Value)
            FillEditActionPlanPopUpDetail()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub DeleteImprovementActionPlanItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim dsList As New DataSet
        Dim objImprovementPlan As New clspipimprovement_plantran
        Try
            'If IsPDPDataValid(enPIPFormType.DevelopmentActionPlan, enAuditType.DELETE) = False Then Exit Sub

            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvActionPlanList As DataList = TryCast(item.Parent, DataList)

            Dim hfimprovementplanunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfimprovementplanunkid"), HiddenField)
            Dim hfitemunkid As HiddenField = CType(dgvActionPlanList.Items(item.ItemIndex).FindControl("hfitemunkid"), HiddenField)

            mintImprovementPlanunkid = CInt(hfimprovementplanunkid.Value)
            mintImprovementPlanItemId = CInt(hfitemunkid.Value)

            If objImprovementPlan.isUsed(mintImprovementPlanunkid, True) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "You can not delete this improvement plan.Reason : This Improvement Plan is already used in progress monitoring."), Me)
                Exit Sub
            End If

            delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please Enter Delete Reason For Deleting Action Plan")
            delReason.Show()
            mintDeleteActionId = CInt(enDeleteAction.DeleteImprovementPlan)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objImprovementPlan = Nothing
        End Try

    End Sub

#End Region

#Region "Progress Monitoring"

    Protected Sub AddProgressMonitoringItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpipitem_master As New clspipitem_master
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvProgessMonitoringList As DataList = TryCast(item.Parent, DataList)

            Dim hfMonitoringimprovementplanunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfMonitoringimprovementplanunkid"), HiddenField)
            Dim hfmonitoringparameterunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfmonitoringparameterunkid"), HiddenField)
            Dim hfmonitoringitemunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfmonitoringitemunkid"), HiddenField)

            mintMonitoringImprovementPlanunkid = CInt(hfMonitoringimprovementplanunkid.Value)
            mintMonitoringParameterId = CInt(hfmonitoringparameterunkid.Value)
            mintMonitoringItemId = CInt(hfmonitoringitemunkid.Value)

            objpipitem_master._Itemunkid = mintMonitoringItemId
            lblProgressMonitoringAddEditItem.Text = objpipitem_master._Item

            FillEditProgressMonitoringPopUpDetail(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipitem_master = Nothing
        End Try
    End Sub

    Protected Sub UndoProgressMonitoringItem(ByVal sender As Object, ByVal e As EventArgs)
        Dim objpipitem_master As New clspipitem_master
        Try
            Dim item As DataListItem = TryCast((TryCast(sender, LinkButton)).NamingContainer, DataListItem)
            Dim dgvProgessMonitoringList As DataList = TryCast(item.Parent, DataList)

            Dim hfmonitoringunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfmonitoringunkid"), HiddenField)
            Dim hfMonitoringimprovementplanunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfMonitoringimprovementplanunkid"), HiddenField)
            Dim hfmonitoringparameterunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfmonitoringparameterunkid"), HiddenField)
            Dim hfmonitoringitemunkid As HiddenField = CType(dgvProgessMonitoringList.Items(item.ItemIndex).FindControl("hfmonitoringitemunkid"), HiddenField)

            mintMonitoringunkid = CInt(hfmonitoringunkid.Value)
            mintMonitoringImprovementPlanunkid = CInt(hfMonitoringimprovementplanunkid.Value)
            mintMonitoringParameterId = CInt(hfMonitoringimprovementplanunkid.Value)
            mintMonitoringItemId = CInt(hfmonitoringitemunkid.Value)

            objpipitem_master._Itemunkid = mintMonitoringItemId
            lblProgressMonitoringAddEditItem.Text = objpipitem_master._Item

            FillEditProgressMonitoringPopUpDetail(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objpipitem_master = Nothing
        End Try
    End Sub

#End Region

#End Region

#Region " Button Events "

#Region "Area of Concern and Expectation"

    'Concern And Expectation Table-Save 
    Protected Sub btnConcernSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConcernSave.Click
        Dim mDtConcernItems As New DataTable
        Dim objpdiform_master As New clspipform_master
        Try

            If IsPIPFormDataValid() = False Then Exit Sub


            Dim strMsg As String = String.Empty
            mDtConcernItems = objpdiform_master._DtAreaofConcernItems.Copy

            For Each dgItem As DataListItem In dgv_Citems.Items
                Dim drCategoryItems As DataRow
                drCategoryItems = mDtConcernItems.NewRow
                Dim hfcustomitemunkid As HiddenField = CType(dgItem.FindControl("hfcustomitemunkid"), HiddenField)
                Dim hfitemtypeid As HiddenField = CType(dgItem.FindControl("hfitemtypeid"), HiddenField)

                If CInt(hfitemtypeid.Value) = CInt(clsassess_custom_items.enCustomType.FREE_TEXT) Then
                    If dgItem.FindControl("txtFreetext") IsNot Nothing Then
                        Dim xtxt As TextBox = CType(dgItem.FindControl("txtFreetext"), TextBox)

                        If xtxt.Visible = True Then
                            If xtxt.Text.Trim.Length > 0 Then
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If
                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = xtxt.Text
                                mDtConcernItems.Rows.Add(drCategoryItems)

                            ElseIf xtxt.Enabled = True AndAlso xtxt.ReadOnly = False AndAlso xtxt.Text.Trim.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, you must complete all fields to save."), Me)
                                Exit Sub
                            End If
                        End If
                    End If

                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dgItem.FindControl("dtpSelection") IsNot Nothing Then
                        Dim xdtp As Controls_DateCtrl = CType(dgItem.FindControl("dtpSelection"), Controls_DateCtrl)
                        If xdtp.Visible Then
                            If xdtp.Enabled = True AndAlso xdtp.IsNull = False Then

                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If

                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = eZeeDate.convertDate(xdtp.GetDateTime()).ToString()
                                mDtConcernItems.Rows.Add(drCategoryItems)

                            ElseIf xdtp.Enabled = True AndAlso xdtp.IsNull Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, all field value is mandatory information."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.SELECTION Then
                    If dgItem.FindControl("cboSelection") IsNot Nothing Then
                        Dim xCbo As DropDownList = CType(dgItem.FindControl("cboSelection"), DropDownList)
                        If xCbo.Visible Then
                            If xCbo.Enabled = True AndAlso xCbo.SelectedValue > 0 Then
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If

                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = xCbo.SelectedValue
                                mDtConcernItems.Rows.Add(drCategoryItems)

                            ElseIf xCbo.Enabled = True AndAlso xCbo.SelectedValue <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, all field value is mandatory information."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    If dgItem.FindControl("txtNUM") IsNot Nothing Then
                        Dim xtxt As Controls_NumericTextBox = CType(dgItem.FindControl("txtNUM"), Controls_NumericTextBox)
                        If xtxt.Visible Then
                            If xtxt.Enabled = True AndAlso xtxt.Text.Trim.Length > 0 Then
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    Exit Sub
                                End If

                                drCategoryItems.Item("itemunkid") = hfcustomitemunkid.Value
                                drCategoryItems.Item("fieldvalue") = xtxt.Text
                                mDtConcernItems.Rows.Add(drCategoryItems)

                            ElseIf xtxt.Enabled = True AndAlso xtxt.Read_Only = False AndAlso xtxt.Text.Trim.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, all field value is mandatory information."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            Next

            SaveTablePIPFormMaster(mDtConcernItems)
            ClearConcernData()
            mblnConcernExpectationTablePopup = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnConcernClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConcernClose.Click
        Try
            ClearConcernData()
            mblnConcernExpectationTablePopup = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Concern And Expectation List-Save
    Protected Sub btnListConcernSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListConcernSave.Click
        Dim mDtParameterItems As New DataTable
        Dim objpipform_master As New clspipform_master
        Try

            If IsPIPFormDataValid() = False Then Exit Sub


            Dim strMsg As String = String.Empty
            Dim FieldValue As String = ""
            Dim xWorkProcessId As Integer = 0

            If pnlWorkProcess.Visible AndAlso cboWorkProcess.Visible Then
                If CInt(cboWorkProcess.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Work Process is compulsory information.Please Select Work Process."), Me)
                    cboWorkProcess.Focus()
                    Exit Sub
                End If
                xWorkProcessId = CInt(cboWorkProcess.SelectedValue)
            End If

            Select Case mintItemtypeid
                Case CInt(clspipitem_master.enPIPCustomType.FREE_TEXT)
                    If txtItemNameFreeText.Text.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Please add something in the field to save."), Me)
                        Exit Sub
                    End If

                    FieldValue = txtItemNameFreeText.Text


                Case CInt(clspipitem_master.enPIPCustomType.DATE_SELECTION)
                    If dtpItemName.IsNull = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Please add something in the field to save."), Me)
                        Exit Sub
                    End If

                    FieldValue = eZeeDate.convertDate(dtpItemName.GetDateTime()).ToString()

                Case CInt(clspipitem_master.enPIPCustomType.NUMERIC_DATA)
                    If txtItemNameNUM.Text.Length <= 0 AndAlso CInt(txtItemNameNUM.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Please add something in the field to save."), Me)
                        Exit Sub
                    End If

                    If CInt(txtItemNameNUM.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Field value should be greater than 0."), Me)
                        Exit Sub
                    End If
                    FieldValue = txtItemNameNUM.Text
                Case CInt(clspipitem_master.enPIPCustomType.SELECTION)

                    If CInt(cboItemNameSelection.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, field value is required information."), Me)
                        Exit Sub
                    End If

                    FieldValue = cboItemNameSelection.SelectedValue
            End Select

            If SaveListPIPFormMaster(FieldValue, xWorkProcessId, True) Then
                ClearConcernData()
                mblnConcernExpectationListPopup = False
                popup_ListCItemAddEdit.Hide()
                FillPIPParameters()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnListConcernClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListConcernClose.Click
        Try
            ClearConcernData()
            mblnConcernExpectationListPopup = False
            popup_ListCItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Improvement Plan "

    Protected Sub btnImprovementSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprovementSubmit.Click
        Dim objImprovement As New clspipimprovement_plantran
        Try
            'Dim dsList As DataSet = objImprovement.GetImprovementItemList("List", mintFormunkId, CInt(cboEmployee.SelectedValue))

            Dim objItems As New clspipitem_master
            Dim dsItems As DataSet = objItems.GetList("List", False, 0, 1)  'Improvement Plan
            objItems = Nothing

            If dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsItems.Tables(0).Rows
                    Dim dsList As DataSet = objImprovement.GetList("List", mintFormunkId, True, CInt(dr("parameterunkid")), CInt(dr("itemunkid")), "")
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Some of the improvement plan(s) are still pending.Please fill all the improvement plan(s)."), Me)
                    Exit Sub
                    End If ' If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Next
            End If  'If dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0 Then

            'If (dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0) AndAlso (dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0) Then


            '    If dsList.Tables(0).Rows.Count < dsItems.Tables(0).Rows.Count Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Some of the improvement plan(s) are still pending.Please fill all the improvement plan(s)."), Me)
            '        Exit Sub
            '    End If
            'End If

            Dim objSetting As New clspipsettings_master
            Dim pipSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objSetting.GetSetting()
            objSetting = Nothing

            Dim enProgressMonitoring As clspipsettings_master.enPIPProgressMappingId
            If pipSetting.Keys.Contains(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING) Then
                enProgressMonitoring = pipSetting(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING)
            End If


            Dim dtProgressMonitoring As DataTable = Nothing
            Dim mstrApprover As String = ""
            Dim mstrApproverDisplayName As String = ""
            Dim mstrApproverEmail As String = ""

            If enProgressMonitoring = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER Then
                Dim objLineManager As New clsReportingToEmployee
                objLineManager._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
                If objLineManager._RDataTable IsNot Nothing AndAlso objLineManager._RDataTable.Rows.Count > 0 Then
                    mstrApprover = objLineManager._RDataTable.Rows(0)("fename").ToString() & " " & objLineManager._RDataTable.Rows(0)("lename").ToString()
                    mstrApproverEmail = objLineManager._RDataTable.Rows(0)("ReportingToEmail").ToString()
                    mstrApproverDisplayName = objLineManager._RDataTable.Rows(0)("ReportingToEmpDisplayName").ToString()
                End If
                dtProgressMonitoring = objLineManager._RDataTable.Copy
            ElseIf enProgressMonitoring = clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH Then
                Dim objCoach As New clsCoaching_Nomination_Form
                dtProgressMonitoring = objCoach.GetEmployeeCoach(dtpFormCreationDate.GetDate.Date, CInt(cboEmployee.SelectedValue))
                objCoach = Nothing

                If dtProgressMonitoring IsNot Nothing AndAlso dtProgressMonitoring.Rows.Count > 0 Then
                    mstrApprover = dtProgressMonitoring.Rows(0)("CoachName").ToString()
                    mstrApproverEmail = dtProgressMonitoring.Rows(0)("CoachEmail").ToString()
                    mstrApproverDisplayName = dtProgressMonitoring.Rows(0)("CoachDisplayName").ToString()
                End If
            End If

            If dtProgressMonitoring IsNot Nothing AndAlso dtProgressMonitoring.Rows.Count > 0 Then

                Dim dsImprovementPlan As DataSet = objImprovement.GetList("List", mintFormunkId, True, 0, 0, "pipimprovement_plantran.issubmit = 0")

                If dsImprovementPlan IsNot Nothing AndAlso dsImprovementPlan.Tables(0).Rows.Count > 0 Then

                    For p As Integer = 0 To dsImprovementPlan.Tables(0).Rows.Count - 1
                        objImprovement._Improvementplanunkid = CInt(dsImprovementPlan.Tables(0).Rows(p)("improvementplanunkid"))
                        objImprovement._IsSubmit = True
                        objImprovement._ClientIP = CStr(Session("IP_ADD"))
                        objImprovement._FormName = mstrModuleName
                        objImprovement._IsWeb = True
                        objImprovement._HostName = CStr(Session("HOST_NAME"))
                        objImprovement._DatabaseName = CStr(Session("Database_Name"))

                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            objImprovement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                            objImprovement._Userunkid = 0
                        ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            objImprovement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                            objImprovement._Userunkid = CInt(Session("UserId"))
                        End If

                        If objImprovement.Update(Nothing) = False Then
                            DisplayMessage.DisplayMessage(objImprovement._Message, Me)
                            Exit Sub
                        End If

                    Next
                    mblnIsImprovementPlanSubmitted = True
                End If  '    If dsImprovementPlan IsNot Nothing AndAlso dsImprovementPlan.Tables(0).Rows.Count > 0 Then



                Dim objUser As New clsUserAddEdit
                Dim mintApproverUserID As Integer = objUser.Return_UserId(mstrApproverDisplayName, "hrmsconfiguration", enLoginMode.USER)
                objUser = Nothing

                objImprovement._FormName = mstrModuleName
                objImprovement._ClientIP = Session("IP_ADD").ToString()
                objImprovement._HostName = Session("HOST_NAME").ToString()

                Dim iLoginTypeId As enLogin_Mode

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE
                    objImprovement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    objImprovement._Userunkid = 0
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    iLoginTypeId = enLogin_Mode.MGR_SELF_SERVICE
                    objImprovement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    objImprovement._Userunkid = CInt(Session("UserId"))
                End If
                objImprovement.SendNotificationForProgressMonitoring(mintFormunkId, mstrPIPFormNo, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, mstrEmployee, dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, mintApproverUserID, mstrApprover, mstrApproverEmail, CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString(), iLoginTypeId)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Improvement Action Plan submitted successfully."), Me)
                If mblnIsImprovementPlanSubmitted Then
                    FillPIPParameters()
                    FillImprovementPlanItems()
                    FillProgressMonitoringItems()
                    btnImprovementSubmit.Visible = False
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "There is no line manager / Coach to do progress monitoring."), Me)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objImprovement = Nothing
        End Try
    End Sub

    Protected Sub btnSaveImprovementActionPlan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImprovementActionPlan.Click
        Dim objPdpgoals_master As New clsPdpgoals_master
        Try

            If IsPIPFormDataValid() = False Then Exit Sub

            If IsImprovementActionPlanPopUpDataValid() = False Then Exit Sub

            If SaveListPIPFormMaster("", 0, False) Then
                SaveImprovementPlan()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
        End Try
    End Sub

    Protected Sub btnCloseImprovementActionPlan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseImprovementActionPlan.Click
        Try
            mblnImprovementPlanPopup = False
            popup_ImprovementPlanAddEdit.Hide()
            ClearImprovementPlanPopupControlsAndValues()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Progress Monitoring"

    Protected Sub btnMonitoringSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMonitoringSubmit.Click
        Dim objSetting As New clspipsettings_master
        Dim objMonitoring As New clspipmonitoring_tran
        Try
            'Dim dsList As DataSet = objMonitoring.GetList("List", mintFormunkId, True, 0, "pipmonitoring_tran.issubmit = 0")

            Dim objImprovementItems As New clspipimprovement_plantran
            Dim dsItems As DataSet = objImprovementItems.GetList("List", mintFormunkId, True, 0, 0, "")
            objImprovementItems = Nothing

            If dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsItems.Tables(0).Rows
                    Dim dsList As DataSet = objMonitoring.GetList("List", mintFormunkId, True, CInt(dr("improvementplanunkid")), "pipmoniteringunkid > 0")
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "Some of the progress review(s) are still pending.Please complete all the progress review(s)."), Me)
                    Exit Sub
                    End If 'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Next
            End If 'If dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0 Then

            'If (dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0) AndAlso (dsItems IsNot Nothing AndAlso dsItems.Tables(0).Rows.Count > 0) Then
            '    If dsList.Tables(0).Rows.Count < dsItems.Tables(0).Rows.Count Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "Some of the progress review(s) are still pending.Please complete all the progress review(s)."), Me)
            '        Exit Sub
            '    End If
            'End If

            'Dim pipSetting As Dictionary(Of clspipsettings_master.enPIPConfiguration, String) = objSetting.GetSetting()

            'Dim enFinalAssessment As clspipsettings_master.enPIPProgressMappingId
            'If pipSetting.Keys.Contains(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING) Then
            '    enFinalAssessment = pipSetting(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
            'End If

            Dim dtFinalAssessment As DataTable = Nothing
            Dim mstrApprover As String = ""
            Dim mstrApproverDisplayName As String = ""
            Dim mstrApproverEmail As String = ""

            If enFinalAssessment = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER Then
                Dim objLineManager As New clsReportingToEmployee
                objLineManager._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
                dtFinalAssessment = objLineManager._RDataTable.Copy()
            ElseIf enFinalAssessment = clspipsettings_master.enPIPProgressMappingId.ROLE_BASED Then
                Dim mstrRoleIds As String = ""
                Dim dsRoleList As DataSet = objSetting.GetList("List", clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
                If dsRoleList IsNot Nothing AndAlso dsRoleList.Tables(0).Rows.Count > 0 Then
                    mstrRoleIds = dsRoleList.Tables(0).Rows(0)("roleunkids").ToString()
                End If

                If mstrRoleIds.Trim.Length > 0 Then
                    Dim objUserList As New clsUserAddEdit
                    Dim dsUserList As DataSet = objUserList.GetList("List", "hrmsConfiguration..cfuser_master.roleunkid IN (" & mstrRoleIds & ")")
                    objUserList = Nothing
                    dtFinalAssessment = dsUserList.Tables(0).Copy
                End If
            End If

            If dtFinalAssessment IsNot Nothing AndAlso dtFinalAssessment.Rows.Count > 0 Then

                Dim dsMonitoring As DataSet = objMonitoring.GetList("List", mintFormunkId, True, 0, "pipmonitoring_tran.issubmit = 0")

                If dsMonitoring IsNot Nothing AndAlso dsMonitoring.Tables(0).Rows.Count > 0 Then

                    For p As Integer = 0 To dsMonitoring.Tables(0).Rows.Count - 1
                        objMonitoring._Pipmoniteringunkid = CInt(dsMonitoring.Tables(0).Rows(p)("pipmoniteringunkid"))
                        objMonitoring._IsSubmit = True
                        objMonitoring._ClientIP = CStr(Session("IP_ADD"))
                        objMonitoring._FormName = mstrModuleName
                        objMonitoring._IsWeb = True
                        objMonitoring._HostName = CStr(Session("HOST_NAME"))
                        objMonitoring._DatabaseName = CStr(Session("Database_Name"))

                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            objMonitoring._AuditUserId = CInt(Session("UserId"))
                            objMonitoring._Userunkid = CInt(Session("UserId"))
                        End If

                        If objMonitoring.Update(Nothing) = False Then
                            DisplayMessage.DisplayMessage(objMonitoring._Message, Me)
                            Exit Sub
                        End If

                    Next

                    mblnIsMonitoringSubmitted = True

                End If  '    If dsMonitoring IsNot Nothing AndAlso dsMonitoring.Tables(0).Rows.Count > 0 Then


                For i As Integer = 0 To dtFinalAssessment.Rows.Count - 1

                    If enFinalAssessment = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER Then
                        mstrApprover = dtFinalAssessment.Rows(i)("fename").ToString() & " " & dtFinalAssessment.Rows(i)("lename").ToString()
                        mstrApproverEmail = dtFinalAssessment.Rows(i)("ReportingToEmail").ToString()

                    ElseIf enFinalAssessment = clspipsettings_master.enPIPProgressMappingId.ROLE_BASED Then
                        If dtFinalAssessment.Rows(i)("firstname").ToString().Trim.Length <= 0 Then
                            mstrApprover = dtFinalAssessment.Rows(i)("username").ToString()
                        Else
                            mstrApprover = dtFinalAssessment.Rows(i)("firstname").ToString() & " " & dtFinalAssessment.Rows(i)("lastname").ToString()
                        End If

                        mstrApproverEmail = dtFinalAssessment.Rows(i)("email").ToString()
                    End If

                    objMonitoring._FormName = mstrModuleName
                    objMonitoring._ClientIP = Session("IP_ADD").ToString()
                    objMonitoring._HostName = Session("HOST_NAME").ToString()

                    Dim iLoginTypeId As enLogin_Mode

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        iLoginTypeId = enLogin_Mode.MGR_SELF_SERVICE
                    End If
                    objMonitoring.SendNotificationForFinalAssessment(mintFormunkId, mstrPIPFormNo, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, mstrEmployee, CInt(dtFinalAssessment.Rows(i)("userunkid")), mstrApprover, mstrApproverEmail, CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString(), iLoginTypeId)
                Next

                If Me.Request.QueryString.Count > 0 AndAlso mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING Then
                    Session("LMUserId") = Nothing
                    Session("CoachUserId") = Nothing
                    Session("PIPFormunkid") = Nothing
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "Progress Review submitted successfully."), Me, Session("rootpath").ToString & "Index.aspx")

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()
                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If
                    'Pinkal (23-Feb-2024) -- End

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "Progress Review submitted successfully."), Me)
                    If mblnIsMonitoringSubmitted Then
                        FillPIPParameters()
                        FillImprovementPlanItems()
                        FillProgressMonitoringItems()
                        EnableDisableProgressMonitoringControls(False)
                        btnMonitoringSubmit.Visible = False

                        If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then
                            EnableDisableFinalAssessmentControls(True)
                        ElseIf enProgressMapping = clspipsettings_master.enPIPProgressMappingId.ROLE_BASED AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then
                            Dim mstrRoleIds As String = ""
                            Dim dsRoleList As DataSet = objSetting.GetList("List", clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING)
                            If dsRoleList IsNot Nothing AndAlso dsRoleList.Tables(0).Rows.Count > 0 Then
                                mstrRoleIds = dsRoleList.Tables(0).Rows(0)("roleunkids").ToString()
                            End If

                            If mstrRoleIds.Trim.Length > 0 Then
                                Dim objUserList As New clsUserAddEdit
                                Dim dsUserList As DataSet = objUserList.GetList("List", "hrmsConfiguration..cfuser_master.roleunkid IN (" & mstrRoleIds & ")")
                                objUserList = Nothing

                                Dim drRow = dsUserList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = CInt(Session("UserId")))
                                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                                    EnableDisableFinalAssessmentControls(True)
                                End If 'If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

                            End If ' If mstrRoleIds.Trim.Length > 0 Then

                        End If  'If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then

                    End If  '   If mblnIsMonitoringSubmitted Then

                End If ' If Me.Request.QueryString.Count > 0 AndAlso mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING Then

            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "There is no line manager / role to do final assessment."), Me)
                Exit Sub
            End If  '   If dtFinalAssessment IsNot Nothing AndAlso dtFinalAssessment.Rows.Count > 0 Then

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSetting = Nothing
            objMonitoring = Nothing
        End Try
    End Sub

    Protected Sub btnSaveProgressMonitoring_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveProgressMonitoring.Click
        Dim objPdpgoals_master As New clsPdpgoals_master
        Try
            If IsPIPFormDataValid() = False Then Exit Sub
            If IsProgressMonitoringPopUpDataValid(False) = False Then Exit Sub
            SaveProgressMonitoring(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
        End Try
    End Sub

    Protected Sub btnUndoProgressMonitoring_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUndoProgressMonitoring.Click
        Dim objPdpgoals_master As New clsPdpgoals_master
        Try
            If IsPIPFormDataValid() = False Then Exit Sub
            If IsProgressMonitoringPopUpDataValid(True) = False Then Exit Sub
            SaveProgressMonitoring(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPdpgoals_master = Nothing
        End Try
    End Sub

    Protected Sub btnCloseProgressMonitoring_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseProgressMonitoring.Click
        Try
            mblnMonitoringPopup = False
            popup_ProgressMonitoring.Hide()
            ClearProgressUpdatingPopupControlsAndValues()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Final Assessment"

    Protected Sub btnSaveFinalAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveFinalAssessment.Click
        Dim objpipform_master As New clspipform_master
        Dim blnFlag As Boolean = False
        Try

            If IsPIPFormDataValid() = False Then Exit Sub

            If IsProgressMonitoringExist() = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "You cann not do final assessment.Reason : Progress Monitoring is still pending."), Me)
                Exit Sub
            End If

            If IsFinalAssessmentDataValid() = False Then Exit Sub

            objpipform_master._PIPFormunkid = mintFormunkId
            objpipform_master._IsConcernData = False
            objpipform_master._Statusunkid = CInt(cboFinalAssementStatus.SelectedValue)
            objpipform_master._FinalAssessmentRemark = TxtFinalAssessmentRemark.Text.Trim

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpipform_master._Userunkid = CInt(Session("UserId"))
                objpipform_master._AuditUserId = CInt(Session("UserId"))
            End If

            objpipform_master._ClientIP = CStr(Session("IP_ADD"))
            objpipform_master._FormName = mstrModuleName
            objpipform_master._IsWeb = True
            objpipform_master._HostName = CStr(Session("HOST_NAME"))
            objpipform_master._DatabaseName = CStr(Session("Database_Name"))
            objpipform_master._Isvoid = False

            blnFlag = objpipform_master.SavePIPEmployeeData(CInt(Session("CompanyUnkId")))

            If blnFlag = False AndAlso objpipform_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpipform_master._Message, Me)
                Exit Sub
            Else

                If Me.Request.QueryString.Count > 0 AndAlso mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING Then
                    Session("LMUserId") = Nothing
                    Session("CoachUserId") = Nothing
                    Session("PIPFormunkid") = Nothing
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "Final Assessment done Successfully."), Me, Session("rootpath").ToString & "Index.aspx")

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()
                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If
                    'Pinkal (23-Feb-2024) -- End

                Else
                    mintFormunkId = objpipform_master._PIPFormunkid
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "Final Assessment done Successfully."), Me, Session("rootpath") & "PIP/wPgEmployeePIPFormList.aspx")
                    If mintFormunkId <= 0 Then
                        mintFormunkId = objpipform_master._PIPFormunkid
                    End If
                    GetValue()
                End If  ' If Me.Request.QueryString.Count > 0 AndAlso mintPIPConfiguration = clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING Then

            End If '    If blnFlag = False AndAlso objpipform_master._Message.Length > 0 Then
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUndoSaveAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUndoSaveAssessment.Click
        Dim objpipform_master As New clspipform_master
        Dim blnFlag As Boolean = False
        Try

            If IsPIPFormDataValid() = False Then Exit Sub

            If IsProgressMonitoringExist() = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "You cann not do final assessment.Reason : Progress Monitoring is still pending."), Me)
                Exit Sub
            End If

            If IsFinalAssessmentDataValid() = False Then Exit Sub

            objpipform_master._PIPFormunkid = mintFormunkId
            objpipform_master._IsConcernData = False
            objpipform_master._Statusunkid = clspipform_master.enPIPStatus.Pending
            objpipform_master._FinalAssessmentRemark = ""

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                objpipform_master._Userunkid = CInt(Session("UserId"))
                objpipform_master._AuditUserId = CInt(Session("UserId"))
            End If

            objpipform_master._ClientIP = CStr(Session("IP_ADD"))
            objpipform_master._FormName = mstrModuleName
            objpipform_master._IsWeb = True
            objpipform_master._HostName = CStr(Session("HOST_NAME"))
            objpipform_master._DatabaseName = CStr(Session("Database_Name"))
            objpipform_master._Isvoid = False

            blnFlag = objpipform_master.SavePIPEmployeeData(CInt(Session("CompanyUnkId")))

            If blnFlag = False AndAlso objpipform_master._Message.Length > 0 Then
                DisplayMessage.DisplayMessage(objpipform_master._Message, Me)
                Exit Sub
            Else
                mintFormunkId = objpipform_master._PIPFormunkid
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Undo Final Assessment done Successfully."), Me)
                If mintFormunkId <= 0 Then
                    mintFormunkId = objpipform_master._PIPFormunkid
                End If
                GetValue()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'General
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("LMUserId") = Nothing
            Session("CoachUserId") = Nothing
            Session("PIPFormunkid") = Nothing

            If Me.Request.QueryString.Count > 0 Then

                'Pinkal (23-Feb-2024) -- Start
                '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()
                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If

                Response.Redirect("~/Index.aspx", False)
                'Pinkal (23-Feb-2024) -- End
            Else
                Response.Redirect(Session("rootpath") & "PIP/wPgEmployeePIPFormList.aspx", False)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Griview Event"

#Region "Area of Concern And Expectation"

    Protected Sub dlPIPParameter_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPIPParameter.ItemDataBound
        Dim objpipform_master As New clspipform_master
        Dim objpipitem_master As New clspipitem_master
        Dim dtCustomTabularGrid As New DataTable
        Dim dsList As New DataSet

        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hfparameterunkid As HiddenField = TryCast(e.Item.FindControl("hfparameterunkid"), HiddenField)
                Dim hfparameterViewtypeId As HiddenField = TryCast(e.Item.FindControl("hfparameterViewtypeId"), HiddenField)
                Dim lnkAddPIPItem As LinkButton = TryCast(e.Item.FindControl("lnkAddPIPItem"), LinkButton)

                Dim dgvPIPItems As GridView = TryCast(e.Item.FindControl("dgvPIPItems"), GridView)
                Dim dlPIPItems As DataList = TryCast(e.Item.FindControl("dlPIPItems"), DataList)


                If hfparameterViewtypeId.Value = CInt(enPDPCustomItemViewType.List) Then
                    lnkAddPIPItem.Visible = False
                    dsList = objpipitem_master.GetList("List", False, CInt(hfparameterunkid.Value), 0)
                    If IsNothing(dsList) = False Then
                        dlPIPItems.DataSource = dsList.Tables("List")
                        dlPIPItems.DataBind()
                    End If
                    dgvPIPItems.Visible = False
                Else
                    dlPIPItems.Visible = False
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        If mblnIsImprovementPlanSubmitted OrElse mblnIsMonitoringSubmitted Then lnkAddPIPItem.Visible = False
                    Else
                        If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                            lnkAddPIPItem.Visible = True
                        End If
                    End If

                    dtCustomTabularGrid = objpipform_master.Get_Custom_Items_Table(mintFormunkId, hfparameterunkid.Value, CInt(cboEmployee.SelectedValue), False)
                    Dim objParameter As New clspipparameter_master
                    objParameter._Parameterunkid = CInt(hfparameterunkid.Value)
                    Call Add_GridColumns(dgvPIPItems)
                    objParameter = Nothing

                    dgvPIPItems.AutoGenerateColumns = False
                    Dim iColName As String = String.Empty
                    For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                        iColName = "" : iColName = "obj" & dCol.ColumnName
                        Dim dgvCol As New BoundField()
                        dgvCol.FooterText = iColName
                        dgvCol.ReadOnly = True
                        dgvCol.DataField = dCol.ColumnName
                        dgvCol.HeaderText = dCol.Caption
                        If dgvPIPItems.Columns.Contains(dgvCol) = True Then Continue For
                        If dCol.Caption.Length <= 0 Then
                            dgvCol.Visible = False
                        End If
                        dgvPIPItems.Columns.Add(dgvCol)
                        If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
                    Next
                    Call SetDateFormat()

                    dgvPIPItems.DataSource = dtCustomTabularGrid
                    dgvPIPItems.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgv_Citems.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfitemtypeid As HiddenField = CType(e.Item.FindControl("hfitemtypeid"), HiddenField)
                Dim hfselectedid As HiddenField = CType(e.Item.FindControl("hfselectedid"), HiddenField)
                Dim hfselectionmodeid As HiddenField = CType(e.Item.FindControl("hfselectionmodeid"), HiddenField)
                Dim hfddate As HiddenField = CType(e.Item.FindControl("hfddate"), HiddenField)

                If CInt(hfitemtypeid.Value) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    If e.Item.FindControl("txtFreetext") IsNot Nothing Then
                        Dim txt As TextBox = CType(e.Item.FindControl("txtFreetext"), TextBox)
                        txt.Visible = True
                    End If

                ElseIf CInt(hfitemtypeid.Value) = clspipitem_master.enPIPCustomType.DATE_SELECTION Then
                    If e.Item.FindControl("dtpSelection") IsNot Nothing Then
                        Dim dtp As Controls_DateCtrl = CType(e.Item.FindControl("dtpSelection"), Controls_DateCtrl)
                        dtp.Visible = True
                        If hfddate.Value.Trim.Length > 0 AndAlso _
                           hfddate.Value.Trim <> "&nbsp;" Then
                            dtp.SetDate = CDate(hfddate.Value)
                        End If
                    End If

                ElseIf CInt(hfitemtypeid.Value) = clspipitem_master.enPIPCustomType.NUMERIC_DATA Then
                    If e.Item.FindControl("txtNUM") IsNot Nothing Then
                        Dim txt As Controls_NumericTextBox = CType(e.Item.FindControl("txtNUM"), Controls_NumericTextBox)
                        txt.Visible = True
                    End If

                ElseIf CInt(hfitemtypeid.Value) = clspipitem_master.enPIPCustomType.SELECTION Then

                    If e.Item.FindControl("cboSelection") IsNot Nothing Then
                        Dim cbo As DropDownList = CType(e.Item.FindControl("cboSelection"), DropDownList)

                        cbo.Visible = True
                        Select Case CInt(hfselectionmodeid.Value)

                            Case clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES, clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES
                                Dim dtab As DataTable = Nothing
                                Dim dsList As New DataSet

                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing

                                If CInt(hfselectionmodeid.Value) = clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                                ElseIf CInt(hfselectionmodeid.Value) = clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                                End If
                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dtab
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With

                        End Select
                        If hfselectedid.Value.Length > 0 AndAlso _
                           hfselectedid.Value.Trim <> "&nbsp;" Then
                            cbo.SelectedValue = CInt(hfselectedid.Value)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvPIPItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim imgEdit As New LinkButton
                imgEdit.ID = "imgEdit"
                imgEdit.CommandName = "objEdit"
                imgEdit.ToolTip = "Edit"
                imgEdit.CssClass = "lnEdit"
                imgEdit.Text = "<i class='fas fa-pencil-alt text-primary'></i>"

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    If mblnIsImprovementPlanSubmitted OrElse mblnIsMonitoringSubmitted Then imgEdit.Visible = False
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        imgEdit.Visible = True
                    End If
                End If

                If mblnIsImprovementPlanSubmitted = False AndAlso mblnIsMonitoringSubmitted = False Then
                    AddHandler imgEdit.Click, AddressOf Me.OnGvEditPIPItem
                    e.Row.Cells(0).Controls.Add(imgEdit)
                End If

                Dim imgDelete As New LinkButton
                imgDelete.ID = "imgDelete"
                imgDelete.CommandName = "objDelete"
                imgDelete.ToolTip = "Delete"
                imgDelete.CssClass = "lnDelt"
                imgDelete.Text = "<i class='fas fa-trash text-danger'></i>"

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    If mblnIsImprovementPlanSubmitted OrElse mblnIsMonitoringSubmitted Then imgDelete.Visible = False
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        imgDelete.Visible = True
                    End If
                End If

                If mblnIsImprovementPlanSubmitted = False AndAlso mblnIsMonitoringSubmitted = False Then
                    AddHandler imgDelete.Click, AddressOf Me.OnGvDeletePIPItem
                    e.Row.Cells(1).Controls.Add(imgDelete)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#End Region

#Region "Datalist Event"

#Region "Area of Concern And Expectation"

    Protected Sub dlPIPItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lnkAddPIPItem As LinkButton = TryCast(e.Item.FindControl("lnkAddPIPItem"), LinkButton)
                Dim dlPIPItemsList As DataList = TryCast(e.Item.FindControl("dlPIPItemsList"), DataList)
                Dim hfparameterunkid As HiddenField = TryCast(e.Item.FindControl("hfparameterunkid"), HiddenField)
                Dim hfitemunkid As HiddenField = TryCast(e.Item.FindControl("hfitemunkid"), HiddenField)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    If mblnIsImprovementPlanSubmitted OrElse mblnIsMonitoringSubmitted Then lnkAddPIPItem.Visible = False
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkAddPIPItem.Visible = True
                    End If
                End If

                Dim objpipform_master As New clspipform_master
                Dim dsList As New DataSet
                dsList = objpipform_master.Get_Custom_Items_List(mintFormunkId, CInt(hfparameterunkid.Value), CInt(hfitemunkid.Value), (cboEmployee.SelectedValue))

                dlPIPItemsList.DataSource = dsList.Tables(0)
                dlPIPItemsList.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dlPIPItemsList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lnkConcernItemsListEdit As LinkButton = TryCast(e.Item.FindControl("lnkConcernItemsListEdit"), LinkButton)
                Dim lnkConcernItemsListDelete As LinkButton = TryCast(e.Item.FindControl("lnkConcernItemsListDelete"), LinkButton)
                Dim hfisapplyworkprocess As HiddenField = TryCast(e.Item.FindControl("hfisapplyworkprocess"), HiddenField)
                Dim lblListWorkProcess As Label = TryCast(e.Item.FindControl("lblListWorkProcess"), Label)
                Dim txtListWorkProcess As Label = TryCast(e.Item.FindControl("txtListWorkProcess"), Label)

                If CBool(hfisapplyworkprocess.Value) Then
                    lblListWorkProcess.Visible = True
                    txtListWorkProcess.Visible = True
                Else
                    lblListWorkProcess.Visible = False
                    txtListWorkProcess.Visible = False
                End If

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    If mblnIsMonitoringSubmitted OrElse mblnIsImprovementPlanSubmitted Then
                        lnkConcernItemsListEdit.Visible = False
                        lnkConcernItemsListDelete.Visible = False
                    End If
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkConcernItemsListEdit.Visible = True
                        lnkConcernItemsListDelete.Visible = True
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Improvement Plan"

    Protected Sub dlImprovementActionPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlImprovementActionPlan.ItemDataBound
        Dim objImprovement As New clspipimprovement_plantran
        Dim dsList As New DataSet
        Try

            If e.Item.ItemIndex > -1 Then
                Dim hfImprovementItemunkid As HiddenField = CType(e.Item.FindControl("hfImprovementItemunkid"), HiddenField)
                Dim dgvImprovementPlanList As DataList = CType(e.Item.FindControl("dgvImprovementPlanList"), DataList)
                Dim lnkAddImprovementActionPlanItem As LinkButton = CType(e.Item.FindControl("lnkAddImprovementActionPlanItem"), LinkButton)


                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkAddImprovementActionPlanItem.Visible = True
                    End If
                End If

                If mblnIsImprovementPlanSubmitted Then
                    lnkAddImprovementActionPlanItem.Visible = False
                End If

                AddHandler lnkAddImprovementActionPlanItem.Click, AddressOf Me.AddImprovementActionPlanItem
                dsList = objImprovement.GetList("ItemList", mintFormunkId, True, 0, CInt(hfImprovementItemunkid.Value))

                dgvImprovementPlanList.DataSource = dsList.Tables("ItemList")
                dgvImprovementPlanList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Protected Sub dgvImprovementPlanList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try

            If e.Item.ItemIndex > -1 Then
                Dim hfimprovementplanunkid As HiddenField = CType(e.Item.FindControl("hfimprovementplanunkid"), HiddenField)
                Dim hfImprovementissubmit As HiddenField = CType(e.Item.FindControl("hfImprovementissubmit"), HiddenField)
                Dim lnkEditImprovementActionPlanItem As LinkButton = CType(e.Item.FindControl("lnkEditImprovementActionPlanItem"), LinkButton)
                Dim lnkDeleteImprovementActionPlanItem As LinkButton = CType(e.Item.FindControl("lnkDeleteImprovementActionPlanItem"), LinkButton)

                Dim txtImprovementPlanGoal As Label = CType(e.Item.FindControl("txtImprovementPlanGoal"), Label)
                Dim txtImprovementPlanGoalDescription As Label = CType(e.Item.FindControl("txtImprovementPlanGoalDescription"), Label)
                Dim txtactionPlanDueDate As Label = CType(e.Item.FindControl("txtactionPlanDueDate"), Label)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                Else
                    If CInt(cboEmployee.SelectedValue) = CInt(Session("Employeeunkid")) Then
                        lnkEditImprovementActionPlanItem.Visible = True
                        lnkDeleteImprovementActionPlanItem.Visible = True
                    End If
                End If

                If CBool(hfImprovementissubmit.Value) Then
                    lnkEditImprovementActionPlanItem.Visible = False
                    lnkDeleteImprovementActionPlanItem.Visible = False
                Else
                    lnkEditImprovementActionPlanItem.Visible = True
                    lnkDeleteImprovementActionPlanItem.Visible = True
                End If

                If txtactionPlanDueDate.Text.Trim().Length > 0 Then
                    txtactionPlanDueDate.Text = CDate(txtactionPlanDueDate.Text).ToShortDateString()
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

#End Region

#Region "Progress Monitoring"

    Protected Sub dlProgressMonitoring_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlProgressMonitoring.ItemDataBound
        Dim objMonitoring As New clspipmonitoring_tran
        Dim dsList As New DataSet
        Try

            If e.Item.ItemIndex > -1 Then
                'Dim hfProgressimprovementplanunkid As HiddenField = CType(e.Item.FindControl("hfProgressimprovementplanunkid"), HiddenField)
                Dim hfProgressItemunkid As HiddenField = CType(e.Item.FindControl("hfProgressItemunkid"), HiddenField)
                Dim dgvProgessMonitoringList As DataList = CType(e.Item.FindControl("dgvProgessMonitoringList"), DataList)

                'mintMonitoringImprovementPlanunkid = CInt(hfProgressimprovementplanunkid.Value)

                'dsList = objMonitoring.GetList("ItemList", mintFormunkId, True, CInt(hfProgressimprovementplanunkid.Value), "pipimprovement_plantran.itemunkid = " & CInt(hfProgressItemunkid.Value))

                dsList = objMonitoring.GetList("ItemList", mintFormunkId, True, 0, "pipimprovement_plantran.itemunkid = " & CInt(hfProgressItemunkid.Value))

                dgvProgessMonitoringList.DataSource = dsList.Tables("ItemList")
                dgvProgessMonitoringList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMonitoring = Nothing
        End Try
    End Sub

    Protected Sub dgvProgessMonitoringList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfmonitoringunkid As HiddenField = CType(e.Item.FindControl("hfmonitoringunkid"), HiddenField)
                Dim hfMonitoringimprovementplanunkid As HiddenField = CType(e.Item.FindControl("hfMonitoringimprovementplanunkid"), HiddenField)
                Dim hfmonitoringparameterunkid As HiddenField = CType(e.Item.FindControl("hfmonitoringparameterunkid"), HiddenField)
                Dim hfmonitoringitemunkid As HiddenField = CType(e.Item.FindControl("hfmonitoringitemunkid"), HiddenField)
                Dim hfmonitoringissubmit As HiddenField = CType(e.Item.FindControl("hfmonitoringissubmit"), HiddenField)

                Dim lnkAddProgressMonitoringItem As LinkButton = CType(e.Item.FindControl("lnkAddProgressMonitoringItem"), LinkButton)
                Dim lnkUndoProgressMonitoringItem As LinkButton = CType(e.Item.FindControl("lnkUndoProgressMonitoringItem"), LinkButton)

                Dim txtMonitoringGoal As Label = CType(e.Item.FindControl("txtMonitoringGoal"), Label)
                Dim txtMonitoringActivity As Label = CType(e.Item.FindControl("txtMonitoringActivity"), Label)
                Dim txtMonitoringDueDate As Label = CType(e.Item.FindControl("txtMonitoringDueDate"), Label)

                Dim lblMonitoringStatus As Label = CType(e.Item.FindControl("lblMonitoringStatus"), Label)
                Dim txtMonitoringStatus As Label = CType(e.Item.FindControl("txtMonitoringStatus"), Label)

                If txtMonitoringStatus.Text.Trim.Length > 0 Then
                    lblMonitoringStatus.Visible = True
                    txtMonitoringStatus.Visible = True
                Else
                    lblMonitoringStatus.Visible = False
                    txtMonitoringStatus.Visible = False
                End If

                Dim lblMonitoringUpdatedOn As Label = CType(e.Item.FindControl("lblMonitoringUpdatedOn"), Label)
                Dim txtMonitoringUpdatedOn As Label = CType(e.Item.FindControl("txtMonitoringUpdatedOn"), Label)

                If txtMonitoringUpdatedOn.Text.Trim.Length > 0 Then
                    lblMonitoringUpdatedOn.Visible = True
                    txtMonitoringUpdatedOn.Visible = True
                    txtMonitoringUpdatedOn.Text = CDate(txtMonitoringUpdatedOn.Text).ToShortDateString()
                Else
                    lblMonitoringUpdatedOn.Visible = False
                    txtMonitoringUpdatedOn.Visible = False
                End If

                mblnIsMonitoringSubmitted = CBool(hfmonitoringissubmit.Value)

                mintMonitoringunkid = CInt(hfmonitoringunkid.Value)
                mintMonitoringImprovementPlanunkid = CInt(hfMonitoringimprovementplanunkid.Value)
                mintMonitoringParameterId = CInt(hfmonitoringparameterunkid.Value)
                mintMonitoringItemId = CInt(hfmonitoringitemunkid.Value)


                If enProgressMapping = clspipsettings_master.enPIPProgressMappingId.LINE_MANAGER AndAlso mintLineManagerUserID = CInt(Session("UserId")) Then
                    lnkAddProgressMonitoringItem.Visible = True
                    lnkUndoProgressMonitoringItem.Visible = True
                ElseIf enProgressMapping = clspipsettings_master.enPIPProgressMappingId.NOMINATED_COACH AndAlso mintCoachUserId = CInt(Session("UserId")) Then
                    lnkAddProgressMonitoringItem.Visible = True
                    lnkUndoProgressMonitoringItem.Visible = True
                End If

                If CBool(hfmonitoringissubmit.Value) Then
                    lnkAddProgressMonitoringItem.Visible = False
                    lnkUndoProgressMonitoringItem.Visible = False
                End If

                If txtMonitoringDueDate.Text.Trim().Length > 0 Then
                    txtMonitoringDueDate.Text = CDate(txtMonitoringDueDate.Text).ToShortDateString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

#End Region

#End Region

#Region "Controls event"

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            If (delReason.Reason.Trim = "") Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Please enter delete reason."), Me)
                Exit Sub
            End If

            Select Case mintDeleteActionId

                Case CInt(enDeleteAction.DeleteConcernExpectation)   'CONCERN AND EXPECTATION
                    Dim objconcern As New clspipconcern_expectationTran

                    If mstrConcernEditGUID.Length > 0 Then
                        objconcern._ItemGrpGuId = mstrConcernEditGUID
                        objconcern._Pipformunkid = mintFormunkId
                    End If

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objconcern._AuditUserId = CInt(Session("UserId"))
                        objconcern._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objconcern._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                    End If


                    objconcern._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objconcern._Voidreason = delReason.Reason.Trim
                    objconcern._Isvoid = True

                    objconcern._ClientIP = CStr(Session("IP_ADD"))
                    objconcern._FormName = mstrModuleName
                    objconcern._IsWeb = True
                    objconcern._HostName = CStr(Session("HOST_NAME"))
                    objconcern._DatabaseName = CStr(Session("Database_Name"))

                    If mstrConcernEditGUID.Length > 0 Then
                        If objconcern.isUsed(-1, mstrConcernEditGUID, True) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Sorry, this item is already in used."), Me)
                            ClearConcernData()
                            Exit Sub
                        End If
                    Else
                        If objconcern.isUsed(mintConcernTranId) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Sorry, this item is already in used."), Me)
                            ClearConcernData()
                            Exit Sub
                        End If
                    End If

                    blnFlag = objconcern.Delete(mintConcernTranId, mstrConcernEditGUID, mintParameterId)

                    If blnFlag = False AndAlso objconcern._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objconcern._Message, Me)
                        objconcern = Nothing
                        Exit Sub
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 41, "Areas of Challenge and Expectations item deleted successfully."), Me)
                        ClearConcernData()
                        FillPIPParameters()
                    End If
                    objconcern = Nothing


                Case CInt(enDeleteAction.DeleteImprovementPlan)   'IMPROVEMENT PLAN 

                    Dim objImprovement As New clspipimprovement_plantran
                    objImprovement._Improvementplanunkid = mintImprovementPlanunkid
                    objImprovement._Pipformunkid = mintFormunkId

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objImprovement._AuditUserId = CInt(Session("UserId"))
                        objImprovement._Userunkid = CInt(Session("UserId"))
                        objImprovement._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objImprovement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                        objImprovement._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

                    End If
                    objImprovement._ClientIP = CStr(Session("IP_ADD"))
                    objImprovement._FormName = mstrModuleName
                    objImprovement._IsWeb = True
                    objImprovement._HostName = CStr(Session("HOST_NAME"))
                    objImprovement._DatabaseName = CStr(Session("Database_Name"))

                    objImprovement._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                    objImprovement._Voidreason = delReason.Reason.Trim
                    objImprovement._Isvoid = True

                    blnFlag = objImprovement.Delete(mintImprovementPlanunkid, mintImprovementPlanItemId, mintFormunkId)

                    If blnFlag = False AndAlso objImprovement._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objImprovement._Message, Me)
                        objImprovement = Nothing
                        Exit Sub
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, "Improvement plan deleted successfully."), Me)
                        delReason.Reason = ""
                        ClearImprovementPlanIds()
                        FillImprovementPlanItems()
                        FillProgressMonitoringItems()  'Progress Monitoring
                    End If
                    objImprovement = Nothing
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            delReason.Reason = ""
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.Title, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMainHeader.ID, Me.lblMainHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkPipInstruction.ID, Me.lnkPipInstruction.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblPIPFormNo.ID, Me.LblPIPFormNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFormCreationDate.ID, Me.LblFormCreationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployee.ID, Me.LblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblStartDate.ID, Me.LblStartDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEndDate.ID, Me.LblEndDate.Text)


            'TAB
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPEmployeeDetailTab.ID, Me.lblPIPEmployeeDetailTab.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPAresofConcernTab.ID, Me.lblPIPAresofConcernTab.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPImprovementActionPlanTab.ID, Me.lblPIPImprovementActionPlanTab.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPProgressMonitoringTab.ID, Me.lblPIPProgressMonitoringTab.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPOverallAssessmentTab.ID, Me.lblPIPOverallAssessmentTab.Text)

            'Employee Details
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.objlblempcode.ID, Me.objlblempcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblName.ID, Me.lblName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblJob.ID, Me.lblJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDepartment.ID, Me.lblDepartment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.objlblDeptGroup.ID, Me.objlblDeptGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.objlblClass.ID, Me.objlblClass.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.objlblLineManagerCoach.ID, Me.objlblLineManagerCoach.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.objlblPIPFormDate.ID, Me.objlblPIPFormDate.Text)


            'Improvement Plan
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnImprovementSubmit.ID, Me.btnImprovementSubmit.Text)


            'Progress Monitoring
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnMonitoringSubmit.ID, Me.btnMonitoringSubmit.Text)


            'Final Assessment
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFinalFormStatus.ID, Me.lblFinalFormStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFinalAssessmentRemark.ID, Me.LblFinalAssessmentRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnUndoSaveAssessment.ID, Me.btnUndoSaveAssessment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveFinalAssessment.ID, Me.btnSaveFinalAssessment.Text)


            'Instruction Popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPIPInstruction.ID, Me.lblPIPInstruction.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnPIPInstructionClose.ID, Me.btnPIPInstructionClose.Text)

            'Area of Challange and Expectation List Popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCItem.ID, Me.lblCItem.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblWorkProcess.ID, Me.LblWorkProcess.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnListConcernSave.ID, Me.btnListConcernSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnListConcernClose.ID, Me.btnListConcernClose.Text)


            'Area of Challange and Expectation Grid Popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblGItem.ID, Me.lblGItem.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnConcernSave.ID, Me.btnConcernSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnConcernClose.ID, Me.btnConcernClose.Text)

            'Improvement Plan Popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprovementActionPlanAddEdit.ID, Me.lblImprovementActionPlanAddEdit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblimprovementPlanGoal.ID, Me.lblimprovementPlanGoal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprovementPlanGoalDescription.ID, Me.lblImprovementPlanGoalDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprovementPlanDueDate.ID, Me.lblImprovementPlanDueDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveImprovementActionPlan.ID, Me.btnSaveImprovementActionPlan.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCloseImprovementActionPlan.ID, Me.btnCloseImprovementActionPlan.Text)

            'Progess Monitoring Popup
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblProgressMonitoringAddEdit.ID, Me.LblProgressMonitoringAddEdit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblMonitoringGoal.ID, Me.LblMonitoringGoal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMonitoringGoalDescription.ID, Me.lblMonitoringGoalDescription.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMonitoringDueDate.ID, Me.lblMonitoringDueDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMonitoringStatus.ID, Me.lblMonitoringStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMonitoringRemark.ID, Me.lblMonitoringRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnUndoProgressMonitoring.ID, Me.btnUndoProgressMonitoring.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveProgressMonitoring.ID, Me.btnSaveProgressMonitoring.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCloseProgressMonitoring.ID, Me.btnCloseProgressMonitoring.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblMainHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMainHeader.ID, Me.lblMainHeader.Text)
            Me.lnkPipInstruction.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkPipInstruction.ID, Me.lnkPipInstruction.ToolTip)
            Me.LblPIPFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblPIPFormNo.ID, Me.LblPIPFormNo.Text)
            Me.LblFormCreationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFormCreationDate.ID, Me.LblFormCreationDate.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblStartDate.ID, Me.LblStartDate.Text)
            Me.LblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEndDate.ID, Me.LblEndDate.Text)


            'TAB
            Me.lblPIPEmployeeDetailTab.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPEmployeeDetailTab.ID, Me.lblPIPEmployeeDetailTab.Text)
            Me.lblPIPAresofConcernTab.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPAresofConcernTab.ID, Me.lblPIPAresofConcernTab.Text)
            Me.lblPIPImprovementActionPlanTab.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPImprovementActionPlanTab.ID, Me.lblPIPImprovementActionPlanTab.Text)
            Me.lblPIPProgressMonitoringTab.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPProgressMonitoringTab.ID, Me.lblPIPProgressMonitoringTab.Text)
            Me.lblPIPOverallAssessmentTab.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPOverallAssessmentTab.ID, Me.lblPIPOverallAssessmentTab.Text)

            'Employee Details
            Me.objlblempcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.objlblempcode.ID, Me.objlblempcode.Text)
            Me.lblName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblName.ID, Me.lblName.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.objlblDeptGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.objlblDeptGroup.ID, Me.objlblDeptGroup.Text)
            Me.objlblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.objlblClass.ID, Me.objlblClass.Text)
            Me.objlblLineManagerCoach.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.objlblLineManagerCoach.ID, Me.objlblLineManagerCoach.Text)
            Me.objlblPIPFormDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.objlblPIPFormDate.ID, Me.objlblPIPFormDate.Text)


            'Improvement Plan
            Me.btnImprovementSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnImprovementSubmit.ID, Me.btnImprovementSubmit.Text)


            'Progress Monitoring
            Me.btnMonitoringSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnMonitoringSubmit.ID, Me.btnMonitoringSubmit.Text)


            'Final Assessment
            Me.lblFinalFormStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFinalFormStatus.ID, Me.lblFinalFormStatus.Text)
            Me.LblFinalAssessmentRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFinalAssessmentRemark.ID, Me.LblFinalAssessmentRemark.Text)
            Me.btnUndoSaveAssessment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnUndoSaveAssessment.ID, Me.btnUndoSaveAssessment.Text)
            Me.btnSaveFinalAssessment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveFinalAssessment.ID, Me.btnSaveFinalAssessment.Text)


            'Instruction Popup
            Me.lblPIPInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPInstruction.ID, Me.lblPIPInstruction.Text)
            Me.btnPIPInstructionClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnPIPInstructionClose.ID, Me.btnPIPInstructionClose.Text)

            'Area of Challange and Expectation List Popup
            Me.lblCItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCItem.ID, Me.lblCItem.Text)
            Me.LblWorkProcess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblWorkProcess.ID, Me.LblWorkProcess.Text)
            Me.btnListConcernSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnListConcernSave.ID, Me.btnListConcernSave.Text)
            Me.btnListConcernClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnListConcernClose.ID, Me.btnListConcernClose.Text)


            'Area of Challange and Expectation Grid Popup
            Me.lblGItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGItem.ID, Me.lblGItem.Text)
            Me.btnConcernSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnConcernSave.ID, Me.btnConcernSave.Text)
            Me.btnConcernClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnConcernClose.ID, Me.btnConcernClose.Text)

            'Improvement Plan Popup
            Me.lblImprovementActionPlanAddEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprovementActionPlanAddEdit.ID, Me.lblImprovementActionPlanAddEdit.Text)
            Me.lblimprovementPlanGoal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblimprovementPlanGoal.ID, Me.lblimprovementPlanGoal.Text)
            Me.lblImprovementPlanGoalDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprovementPlanGoalDescription.ID, Me.lblImprovementPlanGoalDescription.Text)
            Me.lblImprovementPlanDueDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprovementPlanDueDate.ID, Me.lblImprovementPlanDueDate.Text)
            Me.btnSaveImprovementActionPlan.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveImprovementActionPlan.ID, Me.btnSaveImprovementActionPlan.Text)
            Me.btnCloseImprovementActionPlan.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseImprovementActionPlan.ID, Me.btnCloseImprovementActionPlan.Text)

            'Progess Monitoring Popup
            Me.LblProgressMonitoringAddEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblProgressMonitoringAddEdit.ID, Me.LblProgressMonitoringAddEdit.Text)
            Me.LblMonitoringGoal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblMonitoringGoal.ID, Me.LblMonitoringGoal.Text)
            Me.lblMonitoringGoalDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMonitoringGoalDescription.ID, Me.lblMonitoringGoalDescription.Text)
            Me.lblMonitoringDueDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMonitoringDueDate.ID, Me.lblMonitoringDueDate.Text)
            Me.lblMonitoringStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMonitoringStatus.ID, Me.lblMonitoringStatus.Text)
            Me.lblMonitoringRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMonitoringRemark.ID, Me.lblMonitoringRemark.Text)
            Me.btnUndoProgressMonitoring.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnUndoProgressMonitoring.ID, Me.btnUndoProgressMonitoring.Text)
            Me.btnSaveProgressMonitoring.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveProgressMonitoring.ID, Me.btnSaveProgressMonitoring.Text)
            Me.btnCloseProgressMonitoring.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseProgressMonitoring.ID, Me.btnCloseProgressMonitoring.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information.Please select employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Start Date is compulsory information.Please set Start Date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "End Date is compulsory information.Please set End Date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Item value is already exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Areas of Challenge and Expectations Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, this data cannot be saved because it's a duplication of an existing record. Please enter something new to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Improvement Action plan Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Improvement Goal is compulsory information.Please enter improvement goal.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Improvement Activity is compulsory information.Please enter improvement activity.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Due Date is mandatory information. Please select due date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Status is compulsory information.Please Select Status.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Remark is compulsory information.Please enter Remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Undo Progress done Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Progress Monitoring Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Assessment Status is compulsory information.Please select Assessment Status.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Assessment Remark is compulsory information.Please enter Assessment Remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "This PIP Form is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Progress review already submitted for this pip form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Sorry, there is no data to be edited.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Sorry, there is no data to be deleted.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Sorry, No goal item has been added in order to delete.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "You can not delete this improvement plan.Reason : This Improvement Plan is already used in progress monitoring.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Please Enter Delete Reason For Deleting Action Plan")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Please enter delete reason")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Sorry, you must complete all fields to save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Sorry, all field value is mandatory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Work Process is compulsory information.Please Select Work Process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Please add something in the field to save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Field value should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Sorry, field value is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Some of the improvement plan(s) are still pending.Please fill all the improvement plan(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 32, "Improvement Action Plan submitted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "There is no line manager / Coach to do progress monitoring.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 34, "Some of the progress review(s) are still pending.Please complete all the progress review(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 35, "Progress Review submitted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 36, "There is no line manager / role to do final assessment.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 37, "You cann not do final assessment.Reason : Progress Monitoring is still pending.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 38, "Final Assessment done Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Undo Final Assessment done Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "Sorry, this item is already in used.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 41, "Areas of Challenge and Expectations item deleted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 42, "Improvement plan deleted successfully.")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
