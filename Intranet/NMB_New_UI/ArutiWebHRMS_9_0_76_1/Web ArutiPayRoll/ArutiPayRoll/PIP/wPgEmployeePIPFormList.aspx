﻿<%@ Page Title="Employee PIP Form List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmployeePIPFormList.aspx.vb" Inherits="PIP_wPgEmployeePIPFormList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc4:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee PIP Form List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                            <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPIPFormNo" runat="server" Text="PIP Form No" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPIPForm" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFormDate" runat="server" Text="Form Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpFormDate" runat="server" CssClass="form-control" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpStartDate" runat="server" CssClass="form-control" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpEndDate" runat="server" CssClass="form-control" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:GridView ID="dgvPIPFormList" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                ShowHeader="true" CssClass="table table-hover table-bordered" DataKeyNames="pipformunkid,employeeunkid,LMUserId,CoachUserId,statusunkid" sh>
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                                        ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Select" ToolTip="Edit"
                                                                    OnClick="lnkEdit_Click">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                                        ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" OnClick="lnkDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                                        ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" FooterText="btnPreview">
                                                        <ItemTemplate >
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgPreview" runat="server" ToolTip="Preview" OnClick = "lnkPreview_Click">
                                                                        <i class="fa fa-print" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="EmpCodeName" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhEmployee"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="pipformno" HeaderText="Form No" FooterText="dgcolhFormNo" HeaderStyle-Width = "200px" />
                                                    <asp:BoundField DataField="startdate" HeaderText="Start Date" FooterText="dgcolhStartDate" HeaderStyle-Width = "200px" />
                                                    <asp:BoundField DataField="enddate" HeaderText="End Date" FooterText="dgcolhEndDate" HeaderStyle-Width = "200px" />
                                                    <asp:BoundField DataField="LineManager" HeaderText="Line Manager" ReadOnly="true"
                                                        FooterText="dgcolhLineManager" HeaderStyle-Width = "200px" />
                                                    <asp:BoundField DataField="Coach" HeaderText="Coach" ReadOnly="true" FooterText="dgcolhCoach" HeaderStyle-Width = "200px"/>
                                                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" HeaderStyle-Width = "200px" />
                                                    <asp:BoundField DataField="LineManagerId" HeaderText="LineManagerId" ReadOnly="true" FooterText="objdgcolhLineManagerId" Visible ="false" />
                                                    <asp:BoundField DataField="LMUserId" HeaderText="LMUserId" ReadOnly="true" FooterText="objdgcolhLMUserId" Visible ="false" />
                                                    <asp:BoundField DataField="coach_employeeunkid" HeaderText="CoachEmployeeunkid" ReadOnly="true" FooterText="objdgcolhCoachEmployeeunkid" Visible = "false" />
                                                    <asp:BoundField DataField="CoachUserId" HeaderText="CoachUserId" ReadOnly="true" FooterText="objdgcolhCoachUserId" Visible = "false" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
