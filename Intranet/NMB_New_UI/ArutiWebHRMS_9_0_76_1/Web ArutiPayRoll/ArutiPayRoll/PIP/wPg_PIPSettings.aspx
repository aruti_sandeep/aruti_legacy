﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_PIPSettings.aspx.vb"
    Inherits="PIP_wPg_PIPSettings" Title="Performance Improvement Plan Settings" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ColorPickerTextbox.ascx" TagName="ColorPickerTextbox"
    TagPrefix="colt" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Performance Improvement Plan Settings"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#PIPInstruction" data-toggle="tab">
                                        <asp:Label ID="lblPIPInstruction" runat="server" Text="Instructions"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPParameters" data-toggle="tab">
                                        <asp:Label ID="lblPIPParameter" runat="server" Text="Parameters"></asp:Label>
                                    </a></li>
                                      <li role="presentation"><a href="#PIPWorkProcess" data-toggle="tab">
                                        <asp:Label ID="lblPIPWorkProcess" runat="server" Text="Work Process"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPItems" data-toggle="tab">
                                        <asp:Label ID="lblPIPItems" runat="server" Text="Items"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPProgressMonitoring" data-toggle="tab">
                                        <asp:Label ID="lblPIPProgressMonitoring" runat="server" Text="PIP Progress Monitoring"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPAssessorMapping" data-toggle="tab">
                                        <asp:Label ID="lblPIPAssessorMapping" runat="server" Text="PIP Assessor Mapping"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#PIPRatingSetup" data-toggle="tab">
                                        <asp:Label ID="LblPIPRatingSetups" runat="server" Text="PIP Rating Setup"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="PIPInstruction">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblInstructionHeader" runat="server" Text="Instruction"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtInstruction" runat="server" CssClass="form-control" Rows="3"
                                                                            TextMode="MultiLine" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnInstruction" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPParameters">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblPIPParameters" runat="server" Text="Add/Edit Parameters"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblParameter" Text="Parameters" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtParameter" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblParameterViewType" Text="Parameter Type" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboParameterViewType" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12">
                                                                <asp:Label ID="LblApplicableTo" Text="Applicable To" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApplicableTo" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPSortOrder" Text="Sort Order" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtPSortOrder" runat="server" Type="Numeric" Max="9999" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnPSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnPReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvParameters" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="parameterunkid,viewtype">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkPEdit" runat="server" ToolTip="Edit" OnClick="lnkPEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkPDelete" runat="server" ToolTip="Delete" OnClick="lnkPDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="parameter" HeaderText="Parameters" ReadOnly="True" FooterText="colhParameter">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="viewtype" HeaderText="Parameter Type" ReadOnly="True"
                                                                                FooterText="colhPviewtype"></asp:BoundField>
                                                                            <asp:BoundField DataField="applicablemode" HeaderText="Applicable To" ReadOnly="True"
                                                                                FooterText="colhIApplicaleMode"></asp:BoundField>
                                                                            <asp:BoundField DataField="SortOrder" HeaderText="Sort Order" ReadOnly="True" FooterText="colhPSortOrder">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPWorkProcess">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblPIPWorkProcesses" runat="server" Text="Add/Edit Work Process"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lbldepartment" Text="Department" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboDepartment" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblName" Text="Name" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                 </div>
                                                            </div>     
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblWDescription" Text="Description" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtWDescription" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnWSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnWReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvWorkProcess" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="workprocessunkid,departmentunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkWEdit" runat="server" ToolTip="Edit" OnClick="lnkWEdit_Click">                                                                                             
                                                                                          <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkWDelete" runat="server" ToolTip="Delete" OnClick="lnkWDelete_Click"> 
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="department" HeaderText="Department" ReadOnly="True" FooterText="colhdepartment" Visible = "false" />
                                                                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" FooterText="colhName" HeaderStyle-Width="400px" ItemStyle-Width="400px" />
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True" FooterText="colhDescription" HeaderStyle-Width="450px" ItemStyle-Width="450px">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPItems">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblIPIPItems" runat="server" Text="Add/Edit Item"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIParameter" Text="Parameter" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboIParameter" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIItemType" Text="Item Type" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboIItemType" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblSelectionMode" Text="Selection Mode" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboISelection" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblISortOrder" Text="Sort Order" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtISortOrder" runat="server" Type="Numeric" Max="9999" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIItemText" Text="Item Text" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtIItemText" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-40">
                                                                <asp:CheckBox ID="chkApplyWorkProcess" runat="server" AutoPostBack="true" Text="Apply Work Process" />
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-40">
                                                                <asp:CheckBox ID="chkImprovementGoal" runat="server" AutoPostBack="true" Text="Mark As Improvement Goal" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnISave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnIReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvPIPItem" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="itemunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkIEdit" runat="server" ToolTip="Edit" OnClick="lnkIEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkIDelete" runat="server" ToolTip="Delete" OnClick="lnkIDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="parameter" HeaderText="Parameter" ReadOnly="True" FooterText="colhIParameter" Visible = "false">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="item" HeaderText="Item" ReadOnly="True" FooterText="colhIItem" HeaderStyle-Width="300px" ItemStyle-Width="300px">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="itemtype" HeaderText="Item Type" ReadOnly="True" FooterText="colhIItemtype" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="selectionmode" HeaderText="Selection Mode" ReadOnly="True" FooterText="colhISectionMode" HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundField>
                                                                            <asp:BoundField DataField="SortOrder" HeaderText="Sort Order" ReadOnly="True" FooterText="colhISortOrder" HeaderStyle-Width="75px" ItemStyle-Width="75px">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPProgressMonitoring">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblPIPProgressMonitorings" runat="server" Text="PIP Progress Monitoring shall be done by :"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:RadioButton ID="rdLineManagerMonitoring" runat="server" Checked="true" Text="Line Manager"
                                                                    GroupName="Monitoring" />
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:RadioButton ID="rdNominatedCoachMonitoring" runat="server" Checked="false" Text="Nominated Coach"
                                                                    GroupName="Monitoring" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnProgressMonitoringSave" CssClass="btn btn-primary" runat="server"
                                                            Text="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPAssessorMapping">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblPIPAssessorMappings" runat="server" Text="PIP Assessment shall be done by :"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:RadioButton ID="rdLineManagerMapping" runat="server" Checked="true" Text="Line Manager"
                                                                    AutoPostBack="true" GroupName="Mapping" />
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:RadioButton ID="rdRoleBasedMapping" runat="server" Checked="false" Text="Role Based"
                                                                    AutoPostBack="true" GroupName="Mapping" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblRole" Text="Role" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboRole" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnAssessorMappingSave" CssClass="btn btn-primary" runat="server"
                                                            Text="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="PIPRatingSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="LblPIPRating_Setup" runat="server" Text="Rating Setup"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblRatingType" Text="Rating Type" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboRatingType" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblOptionValue" Text="Option Value" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="TxtOptionValue" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblDescription" Text="Description" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="TxtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                                <asp:Label ID="LblColor" Text="Color" runat="server" CssClass="form-label" />
                                                                <colt:ColorPickerTextbox ID="txtRatingColor" runat="server" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnRatingSetupSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnRatingSetupReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvRatingSetup" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="pipratingunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkRatingEdit" runat="server" ToolTip="Edit" OnClick="lnkRatingEdit_Click"> 
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkRatingDelete" runat="server" ToolTip="Delete" OnClick="lnkRatingDelete_Click"> 
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="RatingType" HeaderText="Rating Type" ReadOnly="True" FooterText="colhRatingType" Visible = "false">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="optionvalue" HeaderText="Option Value" ReadOnly="True" FooterText="colhOptionValue" HeaderStyle-Width="350px" ItemStyle-Width="350px"></asp:BoundField>
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True" FooterText="colhDescription" HeaderStyle-Width="400px" ItemStyle-Width="400px"></asp:BoundField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="50px" HeaderText="Color"  FooterText="colhColor">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="objLblcolor" runat="server" Height="30px" Width="30px" CssClass="colorbox">
                                                                                    </asp:Label>
                                                                                    <asp:HiddenField ID="hfColor" runat="server" Value='<%# Eval("color") %>'></asp:HiddenField>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <asp:HiddenField ID="TabName" runat="server" />
                <cnf:Confirmation ID="cnfConfirmationMapping" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>

        $(document).ready(function() {
            RetriveTab();

        });
        function RetriveTab() {

            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "PIPInstruction";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                debugger;
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }
    </script>

</asp:Content>
