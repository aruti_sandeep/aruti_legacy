﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="EmployeeGrievance.aspx.vb"
    Inherits="Grievance_EmployeeGrievance" Title="Employee Grievance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script>
        function IsValidAttach() {
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            ImageLoad();
            RetriveTab();
            $(".ajax-upload-dragdrop").css("width","auto");
        }

    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="My Grievance"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListGrDatefrom" runat="server" Text="Grievance Date From" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="DtListGrDatefrom" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListGrType" runat="server" Text="Grievance Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpListGrType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListGrDateto" runat="server" Text="Grievance Date To" Width="100%"
                                            CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="DtListGrDateto" runat="server" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListGrrefno" runat="server" Text="Ref No." Width="100%" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtListGrrefno" runat="server" Width="100%" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblListGrGrievanceLevel" runat="server" Text="Grievance Level" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboListGrGrievanceLevel" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnListNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 350px">
                                            <asp:GridView ID="gvGrievanceList" runat="server" DataKeyNames="grievancemasterunkid,issubmitted"
                                                AutoGenerateColumns="False" AllowPaging="false" ShowFooter="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit" ItemStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%#Eval("grievancemasterunkid")%>'
                                                                ToolTip="Edit" OnClick="lnkedit_Click">
                                                 <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" ItemStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" ToolTip="Delete" OnClick="lnkdelete_Click"
                                                                CommandArgument='<%#Eval("grievancemasterunkid")%>'>
                                                 <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" ItemStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkviewresponse" runat="server" ToolTip="View Response" OnClick="lnkviewreport_Click"
                                                                CommandArgument='<%#Eval("grievancemasterunkid")%>'>
                                                <i class="fas fa-eye text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Grievance Ref No." DataField="Grievancerefno" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhgrierefno" ItemStyle-Width="5%" />
                                                    <asp:BoundField HeaderText="Grievance Type" DataField="Grievancetype" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhgrietype" ItemStyle-Width="5%" />
                                                    <asp:BoundField HeaderText="Date" DataField="grievancedate" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhgriedate" ItemStyle-Width="8%" />
                                                    <asp:BoundField HeaderText="Against Employee" DataField="Againstemployee" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhgrieagainstemp" ItemStyle-Width="8%" />
                                                    <asp:BoundField HeaderText="Status" DataField="Status" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhgriestatus" ItemStyle-Width="8%" />
                                                    <asp:BoundField HeaderText="Grievance Description" DataField="grievance_description"
                                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="15%" FooterText="colhgriedesc" />
                                                    <asp:BoundField HeaderText="Grievance Relief Wanted" DataField="grievance_reliefwanted"
                                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="15%" FooterText="colhgriereliefwanted" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupEmployeeGrievance" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblpopupEmployee" runat="server" PopupControlID="pnlGreApprover"
                    DropShadow="false" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGreApprover" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Grievance Add/ Edit" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 480px; overflow: auto">
                        <div class="row clearfix">
                            <asp:Panel ID="pnlpopupGrievanceLevel" runat="server" Visible="false">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblpopupGrievanceLevel" runat="server" Text="Grievance Level" CssClass="form-label"></asp:Label>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                        <asp:RadioButton ID="radpopupEmployee" runat="server" Text="Employee" GroupName="GrievanceLevel"
                                            AutoPostBack="true" Checked="true" CssClass="with-gap" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                        <asp:RadioButton ID="radpopupCompany" runat="server" Text="Company" GroupName="GrievanceLevel"
                                            AutoPostBack="true" CssClass="with-gap" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblpopupdate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                <uc1:DateCtrl ID="popupGrievancedate" runat="server" AutoPostBack="false" />
                            </div>
                        </div>
                        <asp:Panel ID="pnlPopUpEmployee" runat="server">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblPopUpEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="drppopupEmployee" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        </asp:Panel>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblpopupGreType" runat="server" Text="Grievance Type" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="drppopupGreType" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblpopupRefno" runat="server" Text="Ref No." CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtpopupRefno" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#GrievanceDescription" data-toggle="tab">
                                        <asp:Label ID="TabGre" Text="Grievance Description" runat="server" />
                                    </a></li>
                                    <li role="presentation"><a href="#ReliefWanted" data-toggle="tab">
                                        <asp:Label ID="TabRelifWanted" Text="Relief Wanted" runat="server" />
                                    </a></li>
                                    <li role="presentation"><a href="#AttachDocument" data-toggle="tab">
                                        <asp:Label ID="TabAttachDocument" Text="Attach Document" runat="server" />
                                    </a></li>
                                    <li role="presentation"><a href="#AppealRemark" data-toggle="tab">
                                        <asp:Label ID="TabApplealRemark" Text="Appeal Remark" runat="server" />
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="GrievanceDescription">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtpopupGreDesc" runat="server" CssClass="form-control" Rows="10"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="ReliefWanted">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtpopupRelifWanted" runat="server" CssClass="form-control" Rows="10"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="AttachDocument">
                                        <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 d--f m-t-30">
                                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                            <div id="fileuploader">
                                                                <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                                    value="Browse" />
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" CssClass="btn btn-default"
                                                            Text="Browse" />
                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-default" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 200px;">
                                                            <asp:GridView ID="gvGrievanceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                CssClass="table table-hover table-bordered" DataKeyNames="GUID,scanattachtranunkid">
                                                                <Columns>
                                                                    <asp:TemplateField FooterText="objcohDelete" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CommandName="RemoveAttachment" ToolTip="Delete">
                                                                                <i class="fas fa-trash text-danger"></i>
                                                                                </asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--0--%>
                                                                    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                    <asp:TemplateField FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fas fa-download"></i></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--1--%>
                                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                    <%--2--%>
                                                                    <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                        ItemStyle-HorizontalAlign="Right" />
                                                                    <%--3--%>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="gvGrievanceAttachment" />
                                                <asp:PostBackTrigger ControlID="btnDownloadAll" />
                                            </Triggers>
                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Appeal Remark">
                                        <asp:TextBox ID="txtpopupApplealRemark" runat="server" Rows="3" TextMode="MultiLine">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveGreApprover" runat="server" CssClass="btn btn-primary" Text="Save" />
                        <asp:Button ID="btnSaveSubmitGreApprover" runat="server" CssClass="btn btn-default"
                            Text="Save And Submit" />
                        <asp:Button ID="btnCloseGreApprover" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
                <uc7:Confirmation ID="PopupSubmitGrievance" runat="server" />
                <uc7:Confirmation ID="popupAttachment_YesNo" runat="server" />
                <ucDel:DeleteReason ID="popupDeleteGrievanceReason" runat="server" />
                <uc7:Confirmation ID="popupConfirmationGrievanceReason" runat="server" />
                <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
        $(document).ready(function() {
		    ImageLoad();
            
        });
        
        
        
		function ImageLoad(){
		debugger;
		    if ($(".ajax-upload-dragdrop").length <= 0){
		    $("#fileuploader").uploadFile({
			    url: "EmployeeGrievance.aspx?uploadimage=mSEfU19VPc4=",
                method: "POST",
				dragDropStr: "",
				showStatusAfterSuccess:false,
                showAbort:false,
                showDone:false,
                maxFileSize: 1024 * 1024,
				fileName:"myfile",
				onSuccess:function(path,data,xhr){
				    $("#<%= btnSaveAttachment.ClientID %>").click();
                },
                onError:function(files,status,errMsg){
	                alert(errMsg);
                }
            });
        }
        }
        $('body').on("click","input[type=file]",function(){
		    if($(this).attr("class") != "flupload"){
		    return IsValidAttach();
		    }
        });

 function RetriveTab() {
 debugger;
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "GrievanceDescription";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
}
    </script>

</asp:Content>
