<%@ Page Title="Add / Edit Resolution Step" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_ResolutionStep.aspx.vb" Inherits="Grievance_wPg_ResolutionStep" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Resolution Step"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="btnViewPreviousResolution" runat="server" Visible="false" ToolTip="view lower level resolution">
                                <i class="fas fa-comments" style="color:Blue !important; font-size: 20px;" ></i>
                                    </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpApprover" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnl_Approverlevel" runat="server" CssClass="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLevel" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </asp:Panel>
                                   
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlpopupGrievanceLevel" runat="server" Visible="false">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblpopupGrievanceLevel" runat="server" Text="Grievance Level" CssClass="form-label"></asp:Label>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                                <asp:RadioButton ID="radpopupEmployee" runat="server" Text="Employee" GroupName="GrievanceLevel"
                                                    AutoPostBack="true" Checked="true" CssClass="with-gap" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                <asp:RadioButton ID="radpopupCompany" runat="server" Text="Company" GroupName="GrievanceLevel"
                                                    AutoPostBack="true" CssClass="with-gap" />
                                            </div>
                                    </div>
                                    </asp:Panel>
                                </div>
                                 <asp:Panel ID="pnlAgainstEmployee" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Against Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpName" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRefNo" runat="server" Text="Ref No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboRefNo" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrievanceDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <uc3:DateCtrl ID="dtGrievanceDate" runat="server" AutoPostBack="false" Enabled="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrievanceType" runat="server" Text="Grievance Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboGrievanceType" runat="server" AutoPostBack="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAgainstEmp" runat="server" Text="Raised By" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAgainstEmployee" ReadOnly="true" runat="server" Enabled="false"
                                                    CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li role="presentation" class="active"><a href="#GrievanceDescription" data-toggle="tab">
                                                <asp:Label ID="tbGrievanceDesc" runat="server" Text="Grievance Description" CssClass="form-label"></asp:Label></a>
                                            </li>
                                            <li role="presentation"><a href="#ReliefRequested" data-toggle="tab">
                                                <asp:Label ID="tbReliefReq" runat="server" Text="Relief Requested" CssClass="form-label"></asp:Label></a>
                                            </li>
                                            <li role="presentation"><a href="#AttachDocument" data-toggle="tab">
                                                <asp:Label ID="TabAttachDocument" runat="server" Text="Attach Document" CssClass="form-label"></asp:Label></a>
                                            </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="GrievanceDescription">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtGrievanceDesc" runat="server" TextMode="MultiLine" Rows="5" Enabled="false"
                                                                    CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="ReliefRequested">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtReliefReq" runat="server" TextMode="MultiLine" Rows="5" Enabled="false"
                                                                    CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AttachDocument">
                                                <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 250px">
                                                                    <asp:GridView ID="gvGrievanceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                        CssClass="table table-hover table-bordered" DataKeyNames="GUID,scanattachtranunkid,localpath">
                                                                        <Columns>
                                                                            <asp:TemplateField FooterText="objcohdownload" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="DownloadLink" runat="server" ToolTip="Delete" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                                            OnClick="lnkdownloadAttachment_Click">
                                                                                   <i class="fas fa-download text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                            <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                                ItemStyle-HorizontalAlign="Right" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Button ID="btndownloadall" runat="server" Text="Download All" CssClass="btn btn-default" />
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="gvGrievanceAttachment" />
                                                        <asp:PostBackTrigger ControlID="btndownloadall" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlData" runat="server" CssClass="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblResponseType" runat="server" Text="Response Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRepsonseType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRespnonseRemark" runat="server" Text="Respnonse Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRespnonseRemark" runat="server" TextMode="MultiLine" Rows="3"
                                                                    CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblQualifingRemark" runat="server" Text="Qualifing Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtQualifingRemark" runat="server" TextMode="MultiLine" Rows="3"
                                                                    CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMeetingDate" runat="server" Text="Meeting Date" CssClass="form-label"></asp:Label>
                                                        <uc3:DateCtrl ID="dtMeetingDate" runat="server" AutoPostBack="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 200px;">
                                                            <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                AllowPaging="false" DataKeyNames="employeeunkid">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Text=" " Enabled="true"
                                                                                OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" Text=" " CommandArgument="<%# Container.DataItemIndex %>"
                                                                                Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="employeecode" FooterText="dgcolhEcode" HeaderText="Code" />
                                                                    <asp:BoundField DataField="employeename" FooterText="dgcolhEName" HeaderText="Employee" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                                </Columns>
                                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblError" runat="server" Text="" CssClass="label labe-primary pull-left"></asp:Label>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSubmit" runat="server" Text="Save && Submit" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:Cnf_YesNo ID="Cnf_changeResponse_onedit" runat="server" />
                <uc2:Cnf_YesNo ID="cnfSubmit" runat="server" />
                <ucDel:DeleteReason ID="DeleteResolutionEmpReason" runat="server" />
                <cc1:ModalPopupExtender ID="popupGrePreviousResponse" BackgroundCssClass="" TargetControlID="lblPopupheader"
                    runat="server" PopupControlID="pnlGrePreviousResponse" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGrePreviousResponse" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPopupheader" Text="Previous Lower Level Approver Resolution" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblPopupReferanceNo" runat="server" Text="Referance Number"></asp:Label>
                                <asp:Label ID="txtPopupReferanceNo" runat="server" Text="0" Font-Bold="true"></asp:Label>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="max-height: 400px">
                                    <asp:GridView ID="GvPreviousResponse" runat="server" AutoGenerateColumns="False"
                                        CssClass="table table-hover table-bordered" AllowPaging="false">
                                        <Columns>
                                            <asp:BoundField DataField="levelname" HeaderText="Approver Level" HeaderStyle-Width="10%"
                                                FooterText="colhlevelname"></asp:BoundField>
                                            <asp:BoundField DataField="name" HeaderText="Approver Name" HeaderStyle-Width="10%"
                                                FooterText="colhApproverName"></asp:BoundField>
                                            <asp:BoundField DataField="priority" HeaderText="Priority" HeaderStyle-Width="2%"
                                                FooterText="colhPriority"></asp:BoundField>
                                            <asp:BoundField DataField="qualifyremark" HeaderText="Qualification Remark" HeaderStyle-Width="20%"
                                                FooterText="colhqualifyremark"></asp:BoundField>
                                            <asp:BoundField DataField="responseremark" HeaderText="Response Remark" HeaderStyle-Width="20%"
                                                FooterText="colhresponseremark"></asp:BoundField>
                                            <asp:BoundField DataField="EmpResponseRemark" HeaderText="Employee Response" HeaderStyle-Width="20%"
                                                FooterText="colhempresponseremark"></asp:BoundField>
                                            <asp:BoundField DataField="EmployeeStatus" HeaderText="Employee Status" HeaderStyle-Width="20%"
                                                FooterText="colhempstatus"></asp:BoundField>
                                            <asp:BoundField DataField="EmployeeIds" HeaderText="Comitee Members" HeaderStyle-Width="20%"
                                                FooterText="colhcomiteemember"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnCloseGrePreviousResponse" runat="server" CssClass="btn btn-primary"
                            Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
