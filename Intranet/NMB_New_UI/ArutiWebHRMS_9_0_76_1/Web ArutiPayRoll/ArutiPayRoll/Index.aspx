﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="Ui_Index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="YesNobutton" TagPrefix="uc2" %>
<html>
<head runat="server">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
        name="viewport" />
    <title>Aruti Human Resource And Payroll Managment System</title>
	<link rel="icon" href="Ui/images/aruti.ico" type="image/x-icon">
    <!-- Favicon-->
    <link href="Ui/css/fontawesome-free/css/all.css" rel="stylesheet" />
    <!-- Bootstrap Core Css -->
    <link href="Ui/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Waves Effect Css -->
    <link href="Ui/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="Ui/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="Ui/css/style.css" rel="stylesheet" />
    <link href="Ui/css/custom.css" rel="stylesheet" />
    <!-- AdminBSB Themes. You can choose a theme from ../css/themes instead of get all themes -->
    <link href="Ui/css/themes/all-themes.css" rel="stylesheet" />
    <link href="Help/alert/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="Ui/css/maven-pro.css" rel="stylesheet" />
</head>
<body class="theme-red login-box">
    <form id="form1" runat="server" style="width: 100%" defaultbutton="btnlogin" autocomplete="off">

    <script type="text/javascript">
        function hidecode(bln) {
            if (bln == true) {
                document.getElementById('rowccode').style.display = 'none';
            }
            else {
                document.getElementById('rowccode').style.display = '';
//                document.getElementById('rowforgotccode').style.display = '';
            }
        }
        function Checkvalidation() {
            var usename = document.getElementById('<%= txtloginname.ClientID  %>');
            var cmpnycode = document.getElementById('<%= txtCompanyCode.ClientID  %>');
            var psswrd = document.getElementById('<%= txtPwd.ClientID  %>');
            if (usename.value == '' || usename.value == 'User Name') {

                //S.SANDEEP [21-NOV-2018] -- START
                //alert('User Name can not be blank.');
                alert('Please enter a valid Username and Password');
                //S.SANDEEP [21-NOV-2018] -- END                

                return false;
            }
            if (cmpnycode != null && (cmpnycode.value == '' || cmpnycode.value == 'Company Code')) {
                alert('Company Code can not be blank.');
                return false;
            }
        }
        
           
 
        
    </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
        <ProgressTemplate>
            <div class="UpdtProgress">
                <div id="loader">
                </div>
                <div style="margin-top: 45px; margin-left: -165px; width: 300px; position: absolute;
                    left: 50%; top: 50%;">
                    Please wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="display: flex; align-items: center; justify-content: center; height: 100%;">
            <div class="main-container">
                <div class="login-container">
                    <div class="login-slider-container col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div id="carousel-example-generic_2" class="carousel slide" data-ride="carousel">
                                <%--<li data-target="#carousel-example-generic_2" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic_2" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic_2" data-slide-to="2"></li>--%>
                                <ol id="olcarousel" class="carousel-indicators">
                                    <asp:Repeater ID="repSlide" runat="server">
                                        <ItemTemplate>
                                            <li runat="server" id="liParent"></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                    <asp:Repeater ID="repAnnouncement" runat="server">
                                        <ItemTemplate>
                                            <asp:Panel runat="server" ID="pnlItem" CssClass="item active" Style="height: 360px;">
                                                <asp:Image ID="imgAnnouncement" runat="server" Height="100%" CssClass="imgAnnouncement" />
                                    <div class="carousel-caption">
                                                    <h4>
                                                        <asp:Label ID="LblTitle" runat="server" Text='<%# Eval("title") %>' />
                                                    </h4>
                                        <p>
                                                        <div class="announcement-text">
                                                            <asp:Label ID="LblAnnouncement" runat="server" CssClass="announcement-text" />
                                                        </div>
                                                            <asp:LinkButton ID="lnkSeeMore" runat="server" Text="See More" ForeColor="White" CssClass="see-more"
                                                            Style="margin-left: 85%" Font-Bold="true" OnClick="lnkSeeMore_Click"></asp:LinkButton>
                                        </p>
                                    </div>
                                                <asp:HiddenField ID="hdAnnouncementId" Value='<%# Eval("announcementunkid")%>' runat="server" />
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:Repeater>
                            </div>
                            <!-- Controls -->
                        </div>
                    </div>
                    <div class="login-input-container col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="login-input-body" style="background-color:#ffffff">
                        <div class="login-input-container-header">
                            <svg width="1131" height="441" class="main-logo" viewbox="0 0 1131 441" class="main-logo"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                      <g>
                        <rect width="1131" height="441" fill="transparent" />
                        <path
                          d="M125.548 130.538L151.808 186.911L54.3548 400.278L-3.24352e-05 400.278L125.548 130.538Z"
                          fill="#4F4F4F"
                          class="main-logo-a-l"
                        />
                        <path
                          d="M128.893 126.277L160 58.5113L315.5 400.315L253.237 400.248L128.893 126.277Z"
                          fill="#4F4F4F"
                          class="main-logo-a-r"
                        />
                        <circle
                          cx="107"
                          cy="354.815"
                          r="12.5"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <path
                          d="M134.5 400.315C134.5 400.315 121.474 400.315 107 400.315C88.9079 400.315 79.5 400.315 79.5 400.315C79.5 382.089 91.8121 367.315 107 367.315C122.188 367.315 134.5 382.089 134.5 400.315Z"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <circle
                          cx="156"
                          cy="321.815"
                          r="12.5"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <path
                          d="M183.5 367.315C183.5 367.315 170.474 367.315 156 367.315C137.908 367.315 128.5 367.315 128.5 367.315C128.5 349.089 140.812 334.315 156 334.315C171.188 334.315 183.5 349.089 183.5 367.315Z"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <circle
                          cx="207"
                          cy="354.815"
                          r="12.5"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <path
                          d="M234.5 400.315C234.5 400.315 221.474 400.315 207 400.315C188.908 400.315 179.5 400.315 179.5 400.315C179.5 382.089 191.812 367.315 207 367.315C222.188 367.315 234.5 382.089 234.5 400.315Z"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <rect
                          x="361.5"
                          y="183.465"
                          width="38"
                          height="217"
                          rx="19"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <rect
                          x="762.5"
                          y="125.315"
                          width="38"
                          height="275"
                          rx="19"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <rect
                          x="890"
                          y="182"
                          width="38"
                          height="218"
                          rx="19"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <rect
                          x="713.5"
                          y="212.315"
                          width="30"
                          height="135"
                          rx="15"
                          transform="rotate(-90 713.5 212.315)"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <g filter="url(#filter0_d)">
                          <path
                            d="M509.5 201.315C509.5 190.821 518.007 182.315 528.5 182.315C538.993 182.315 547.5 190.821 547.5 201.315V332.315H509.5V201.315Z"
                            fill="#265FD5"
                            class="main-logo-primary"
                          />
                        </g>
                        <path
                          d="M634.5 201.315C634.5 190.821 643.007 182.315 653.5 182.315C663.993 182.315 672.5 190.821 672.5 201.315V332.315H634.5V201.315Z"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <path
                          d="M672.5 329.315C672.5 338.376 670.392 347.349 666.296 355.72C662.2 364.091 656.197 371.698 648.629 378.105C641.061 384.512 632.077 389.595 622.189 393.063C612.301 396.53 601.703 398.315 591 398.315C580.297 398.315 569.699 396.53 559.811 393.062C549.923 389.595 540.939 384.512 533.371 378.105C525.803 371.698 519.8 364.091 515.704 355.72C511.608 347.349 509.5 338.376 509.5 329.315L547.536 329.315C547.536 334.147 548.66 338.932 550.845 343.397C553.029 347.861 556.23 351.918 560.266 355.335C564.302 358.752 569.094 361.462 574.367 363.311C579.64 365.161 585.292 366.113 591 366.113C596.708 366.113 602.36 365.161 607.633 363.311C612.906 361.462 617.698 358.752 621.734 355.335C625.77 351.918 628.971 347.861 631.155 343.397C633.34 338.932 634.464 334.147 634.464 329.315H672.5Z"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <circle
                          cx="908.5"
                          cy="144.315"
                          r="19"
                          fill="#265FD5"
                          class="main-logo-primary"
                        />
                        <path
                          d="M428.791 203.546C447.143 186.071 466.967 186.254 476 193.5C483.647 201.096 486.29 213.169 472.473 229.957C459.427 245.807 408.775 241.768 398.82 305.989C398.136 310.404 392.676 312.057 390.555 308.124C368.921 268.005 411.872 219.657 428.791 203.546Z"
                          fill="#265FD5"
                          class="main-logo-primary"
                          class="main-logo-primary"
                        />
                        <path
                          d="M954.993 155.315C989.891 173.68 1016.07 205.157 1027.76 242.82C1039.45 280.483 1035.7 321.248 1017.33 356.147C998.967 391.046 967.49 417.219 929.827 428.91C892.164 440.601 851.399 436.852 816.5 418.486C846.164 434.097 894.486 422.437 926.5 412.5C959.514 398.563 981.984 375.424 997.594 345.76C1013.2 316.096 1015.62 278.311 1006.45 246.603C998.045 215.201 984.657 170.925 954.993 155.315Z"
                          fill="#FF9A32"
                          class="main-logo-primary"
                        />
                        <path
                          d="M685.936 185.93C697.235 127.854 731.142 76.6445 780.198 43.568C829.254 10.4916 889.441 -1.74271 947.517 9.5566C1005.59 20.8559 1056.8 54.7632 1089.88 103.819C1122.96 152.875 1135.19 213.062 1123.89 271.138C1133.5 221.773 1098.03 165.133 1069.92 123.435C1035.81 81.9518 990.492 52.0077 941.127 42.4033C891.762 32.7988 839.442 39.0592 797.853 65.9396C756.371 91.5853 695.54 136.565 685.936 185.93Z"
                          fill="#31353E"
                          class="main-logo-primary"
                        />
                        <path
                          d="M838.412 99.8606C878.97 78.3005 926.432 73.7352 970.356 87.1691C1014.28 100.603 1051.07 130.935 1072.63 171.494C1094.19 212.052 1098.75 259.514 1085.32 303.438C1071.89 347.363 1041.55 384.152 1001 405.712C1035.47 387.386 1049.51 333.476 1062.92 296.141C1069.35 258.944 1068.02 218.162 1049.69 183.688C1031.36 149.213 1001.42 121.199 964.885 109.243C929.144 96.7475 872.886 81.5345 838.412 99.8606Z"
                          fill="#D71A21"
                        />
                      </g>
                      <defs>
                        <filter
                          id="filter0_d"
                          x="505.5"
                          y="182.315"
                          width="46"
                          height="158"
                          filterUnits="userSpaceOnUse"
                          color-interpolation-filters="sRGB"
                        >
                          <feFlood
                            flood-opacity="0"
                            result="BackgroundImageFix"
                            class="main-logo-primary"
                          />
                          <feColorMatrix
                            in="SourceAlpha"
                            type="matrix"
                            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                            class="main-logo-primary"
                          />
                          <feOffset dy="4" class="main-logo-primary" />
                          <feGaussianBlur stdDeviation="2" class="main-logo-primary" />
                          <feColorMatrix
                            type="matrix"
                            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
                            class="main-logo-primary"
                          />
                          <feBlend
                            mode="normal"
                            in2="BackgroundImageFix"
                            result="effect1_dropShadow"
                            class="main-logo-primary"
                          />
                          <feBlend
                            mode="normal"
                            in="SourceGraphic"
                            in2="effect1_dropShadow"
                            result="shape"
                            class="main-logo-primary"
                          />
                        </filter>
                        <clipPath id="clip0">
                          </clippath>
                          <rect
                            width="1131"
                            height="441"
                            fill="white"
                            class="main-logo-primary"
                          />
                          <rect
                            width="1131"
                            height="441"
                            fill="white"
                            class="main-logo-primary"
                          />
                      </defs>
                    </svg>
                            <asp:Image ID="imgIBank" runat="server" ImageUrl="~/images/IslamicBank.png" Style="width: 15%;" />
                        </div>
                        <div class="login-input-container-body">
                            <h3 class="login-title">
                                Welcome To <span class="loginarutitext">Aruti</span>
                            </h3>
                            <asp:Panel ID="pnlMain" runat="server" >
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblloginname" runat="server" Text="User Name" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtloginname" runat="server" CssClass="form-control" ValidationGroup="login"
                                                placeholder="Enter User Name"></asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                            ControlToValidate="txtloginname" ErrorMessage="User name cannot be blank. " CssClass="error"
                                            SetFocusOnError="True" ValidationGroup="login"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblPwd" runat="server" Text="Password" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtPwd" runat="server" Text="" TextMode="Password" CssClass="form-control"
                                                placeholder="Enter Password"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix" id="rowccode">
                                <asp:Panel ID="pnlCompanyCode" runat="server" Visible="false">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyCode" runat="server" Text="Company Code" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCompanyCode" runat="server" CssClass="form-control" ValidationGroup="login"
                                                    placeholder="Enter Company Code"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:RequiredFieldValidator ID="rfvCompanyCode" runat="server" Display="Dynamic"
                                            ControlToValidate="txtCompanyCode" ErrorMessage="Company Code cannot be blank. "
                                            CssClass="error" SetFocusOnError="True" ValidationGroup="login"></asp:RequiredFieldValidator>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="row clearfix d--f ai--fe">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Button ID="btnForgotPassword" runat="server" CssClass="btn btn-link" Text="Forgot Password" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                    <asp:Button ID="btnlogin" runat="server" CssClass="btn btn-primary m-t-15 waves-effect"
                                        Text="Login" ValidationGroup="login" CausesValidation="true" />
                                </div>
                            </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlError" runat="server" Visible="false">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h5>
                                            <asp:Label ID="lblErrorMsg" runat="server" Text="" CssClass="form-label" ></asp:Label>
                                        </h5>
                                    </div>
                                </div>
                                <div class="row clearfix d--f ai--fe">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">                                        
                                        <asp:Button ID="btnRetry" runat="server" CssClass="btn btn-primary m-t-15 waves-effect" Text="Retry again..." OnClientClick="location.reload(true);" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <asp:Panel ID="pnlBottom" runat="server">
                            <asp:Image ID="imgCompLogo" runat="server" ImageUrl="~/images/amn.png" />
                        </asp:Panel>
                    </div>
                </div>
            </div>
            </div>
            </div>
            <asp:Button ID="bttnHidden" runat="Server" Style="display: none" />
            <asp:Button ID="btnHidde1" runat="Server" Style="display: none" />
            <asp:Button ID="btnChUName" runat="server" Style="display: none" />
            <uc2:YesNobutton ID="radYesNo" runat="server" Title="Password Expired" Message="Sorry, you cannot login to Aruti Self Service. Reason : Your password has been expired. Do you want to change your password now?"
                ValidationGroup="ChangePassYesNo" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="bttnHidden"
                CancelControlID="btnCancel" PopupControlID="pnlForgotRefNo" PopupDragHandleControlID="pnlForgotRefNo"
                BackgroundCssClass="modal-backdrop" Drag="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlForgotRefNo" runat="server" CssClass="card modal-dialog" Style="display: none;"
                DefaultButton="btnSubmit">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Forgot Password"></asp:Label>
                    </h2>
                </div>
                <div class="body">
                    <asp:Label ID="lblEmployeeCode" runat="server" Text="* Employee Code :" Visible="false"></asp:Label>
                    <asp:Label ID="lblForgetCompanyCode" runat="server" CssClass="label" Text="* Company Code:"
                        Visible="false"></asp:Label>
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtEmpCode" runat="server"  ValidationGroup="RefNo"
                                        CssClass="form-control" placeholder="Employee Code"></asp:TextBox>
                                </div>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvEmpCode" runat="server" Display="Dynamic" ControlToValidate="txtEmpCode"
                                ErrorMessage="Employee Code cannot be Blank." CssClass="error" SetFocusOnError="True"
                                ValidationGroup="RefNo"></asp:RequiredFieldValidator>
                           <%-- <cc1:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="rfvEmpCode"
                                            Width="300px">
                            </cc1:ValidatorCalloutExtender>--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnForgotPassword" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                    <asp:Panel ID="pnlForgetCompany" runat="server">
                        <div class="form-group">
                            <div class="form-line">
                                <asp:TextBox ID="txtForgetCompanyCode" runat="server" TabIndex="4" ValidationGroup="RefNo"
                                    placeholder="Company Code" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </asp:Panel>
                                <asp:RequiredFieldValidator ID="rfvForgetCompanyCode" runat="server" Display="None"
                                    ControlToValidate="txtForgetCompanyCode" ErrorMessage="Company Code cannot be blank. "
                                    CssClass="error" SetFocusOnError="True" ValidationGroup="RefNo"></asp:RequiredFieldValidator>
                                <cc1:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="rfvForgetCompanyCode">
                                </cc1:ValidatorCalloutExtender>
                </div>
                <div class="footer">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                            CausesValidation="true" ValidationGroup="RefNo" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default"
                            CausesValidation="true" ValidationGroup="RefNoCancel" />
                    </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnHidde1"
                CancelControlID="btnCancel" PopupControlID="pnlChangePasswd" PopupDragHandleControlID="pnlChangePasswd"
                BackgroundCssClass="modal-backdrop" Drag="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlChangePasswd" runat="server" CssClass="card modal-dialog" Style="display: none;"
                DefaultButton="btnSave">
                <div class="header">
                    <h2>
                        <asp:Label ID="Label1" runat="server" Text="Change Password"></asp:Label>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblUserCP" runat="server" Text="Username :" Visible="false" CssClass="form-label"></asp:Label>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtUsername" runat="server" ReadOnly="true" ValidationGroup="ChangePwd" CssClass="form-control"></asp:TextBox>
                                        </div>
                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnlogin" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                                </div>
                            </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblOldPwd" runat="server" Text="Old Password :" Visible="false" CssClass="form-label"></asp:Label>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtOldPasswd" runat="server" TextMode="Password" placeholder="Old Password" CssClass="form-control"
                                                ValidationGroup="ChangePwd"></asp:TextBox>
                                        </div>
                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnlogin" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblNewPwd" runat="server" Text="New Password :" Visible="false" CssClass="form-label"></asp:Label>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtNewPasswd" runat="server" TextMode="Password" placeholder="New Password" CssClass="form-control"
                                                ValidationGroup="ChangePwd"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnlogin" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblConfirmPwd" runat="server" Text="Confirm Password :" Visible="false"
                                CssClass="form-label"></asp:Label>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtConfirmPasswd" runat="server" TextMode="Password" placeholder="Confirm Password" CssClass="form-control"
                                                ValidationGroup="ChangePwd"></asp:TextBox>
                                </div>
                            </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnlogin" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="$find('ModalPopupExtender2').hide(); return true;"
                        CausesValidation="true" ValidationGroup="ChangePwd" />
                    <asp:Button ID="btnCancelCP" runat="server" Text="Cancel" CssClass="btn btn-default"
                        CausesValidation="true" ValidationGroup="NoChangePwd" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" TargetControlID="btnChUName"
                CancelControlID="btnUCancel" PopupControlID="pnlChangeUName" PopupDragHandleControlID="pnlChangeUName"
                BackgroundCssClass="modal-backdrop" Drag="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlChangeUName" runat="server" Style="cursor: move; display: none;
                background-color: #FFFFFF; border-style: solid; border-width: thin;" Width="440px"
                DefaultButton="btnUSubmit">
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/icon_close.gif"
                    AlternateText="Close" CssClass="rightcorner" OnClientClick="$find('ModalPopupExtender3').hide(); return false;" />
                <div id="div3" style="background-color: #FFFFFF; width: 100%;">
                    <div id="Div4" style="margin: 40px auto 40px auto; width: 80%;">
                        <table id="Table2" style="border-style: solid; width: 100%;">
                            <tr class="row">
                                <td class="col1">
                                    <asp:Label ID="lblOldUName" runat="server" Text="Old Username :"></asp:Label>
                                </td>
                                <td class="col2">
                                    <asp:TextBox ID="txtOldUName" runat="server" ValidationGroup="ChangeUsr"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="row">
                                <td class="col1">
                                    <asp:Label ID="lblNewUName" runat="server" Text="New Username :"></asp:Label>
                                </td>
                                <td class="col2">
                                    <asp:TextBox ID="txtNewUName" runat="server" ValidationGroup="ChangeUsr"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="row">
                                <td class="col1">
                                    <asp:Label ID="lblConfUName" runat="server" Text="Confirm Username :"></asp:Label>
                                </td>
                                <td class="col2">
                                    <asp:TextBox ID="txtConfUName" runat="server" ValidationGroup="ChangeUsr"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="row">
                                <td class="col1">
                                    <asp:Label ID="lblUPassword" runat="server" Text="Password :"></asp:Label>
                                </td>
                                <td class="col2">
                                    <asp:TextBox ID="txtUPassword" runat="server" TextMode="Password" ValidationGroup="ChangeUsr"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="row">
                                <td colspan="2" align="right" style="padding-top: 10px; padding-bottom: 5px; padding-right: 0px;">
                                    <asp:Button ID="btnUSubmit" runat="server" Text="Submit" CssClass="btnDefault" OnClientClick="$find('ModalPopupExtender3').hide(); return true;"
                                        CausesValidation="true" ValidationGroup="ChangeUsr" />
                                    <asp:Button ID="btnUCancel" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="true"
                                        ValidationGroup="NoChangeUsr" />
                                </td>
                                <td class="col2">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
            <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="staticlbl"
                Visible="false" TabIndex="2">
            </asp:DropDownList>
            <%--  Sohail (17 Dec 2018) -- End  --%>
            <cc1:ModalPopupExtender ID="popup_Announcement" runat="server" TargetControlID="hdf_Announcement"
                CancelControlID="btnpopupAnnouncementClose" PopupControlID="pnlAnnouncement"
                BackgroundCssClass="modal-backdrop">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlAnnouncement" runat="server" CssClass="card modal-dialog modal-lg"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="LblPopupTitle" runat="server" Text=""></asp:Label>
                    </h2>
                </div>
                <div class="body" style="max-height: 450px;">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <%--<div>--%>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                <asp:Image ID="imgPopupAnnouncement" runat="server" Height="310px" />
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                                    <asp:Label ID="LblPopupAnnouncement" runat="server" />
                                </div>
                            <%--</div>--%>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <asp:CheckBox ID="chkAcknowledge" runat="server" Text="I acknowledge that I have read this news."
                                AutoPostBack="true" CssClass="text-bold" />
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div style="float: left">
                        <asp:Button ID="btnpopupAnnouncementDownLoadAll" runat="server" Text="DownLoad All"
                            CssClass="btn btn-primary" />
                    </div>
                    <asp:Button ID="btnpopupAnnouncementOk" runat="server" Text="Ok" CssClass="btn btn-primary"
                        Enabled="false" />
                    <asp:Button ID="btnpopupAnnouncementClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    <asp:HiddenField ID="hdf_Announcement" runat="server" />
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnpopupAnnouncementDownLoadAll" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- Jquery Core Js -->

    <script type="text/javascript" src="Ui/plugins/jquery/jquery.min.js"></script>

    <script type="text/javascript" src="Ui/plugins/bootstrap/js/bootstrap.js"></script>

    <script type="text/javascript" src="Ui/plugins/node-waves/waves.js"></script>

    <script type="text/javascript" src="Ui/js/admin.js"></script>

    <script type="text/javascript" src="Ui/js/demo.js"></script>

    <script src="Help/alert/sweetalert.min.js" type="text/javascript"></script>

    <script src="Help/alert/dialogs.js" type="text/javascript"></script>

    </form>
</body>
</html>
