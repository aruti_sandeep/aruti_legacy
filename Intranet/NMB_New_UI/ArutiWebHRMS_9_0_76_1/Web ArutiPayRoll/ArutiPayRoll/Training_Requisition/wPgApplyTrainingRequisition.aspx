﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgApplyTrainingRequisition.aspx.vb" Inherits="Training_Requisition_wPgApplyTrainingRequisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DReason" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>--%>

    <script>
        function FileUploadChangeEvent() {
            var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
            if (fupld != null)
                fileUpLoadChange();

            var fupcld = document.getElementById('<%= flcUpload.ClientID %>' + '_image_file');
            if (fupcld != null)
                fileUpLoadChange();
        }
    </script>

    <%--  <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script>
        function IsValidAttachDoc() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <script>

        $(document).ready(function() {
            ImageLoad();
        });

        function ImageLoad() {
            debugger;
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#cfileuploader").uploadFile({
                    url: "wPgApplyTrainingRequisition.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        debugger;
                        $("#<%= btnSaveCAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });

                $("#fileuploader").uploadFile({
                    url: "wPgApplyTrainingRequisition.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        debugger;
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        //$('input[type=file]').live("click", function() {
        $("body").on("click", 'input[type=file]', function() {
            if ($(this).attr("class") != "flupload") {
                debugger;
                return IsValidAttach();
            }
        });
    </script>

    <script>
        function IsValidAttach() {
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <script type="text/javascript">
     $(document).ready(function()
		{
		    RetriveTab();
            
        });
      function  RetriveTab () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "tabpOtherInfo";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });        
       }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc1:DReason ID="popupDelete" runat="server" Title="Enter Reason to delete" />
                <uc2:Cnf_YesNo ID="cnfApprovDisapprove" runat="server" Title="Aruti" Message="" />
                <uc2:Cnf_YesNo ID="cnfDeleteImage" runat="server" Title="Aruti" Message="Are you sure you want to remove attached document?" />
                <uc2:Cnf_YesNo ID="cnfDeleteOtraining" runat="server" Title="Aruti" Message="Are you sure you want to remove added other training?" />
                <uc2:Cnf_YesNo ID="cnfDeleteOResources" runat="server" Title="Aruti" Message="Are you sure you want to remove added other resources?" />
                <uc2:Cnf_YesNo ID="cnfUnitPrice" runat="server" Title="Aruti" />
                <uc2:Cnf_YesNo ID="CnfCosting" runat="server" Title="Aruti" Message="Are you sure you want to remove added other resources?" />
                <uc2:Cnf_YesNo ID="cnfDeleteCostingFile" runat="server" Title="Aruti" Message="Are you sure you want to delete added file?" />
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" TargetControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:HiddenField ID="TabName" runat="server" />
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment" CssClass="form-label"></asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body" style="max-height: 500px;">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="False">
                                            <ContentTemplate>
                                                <uc9:FileUpload ID="flcUpload" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:Panel ID="pnl_cImageAdd" runat="server" Width="100%">
                                            <div id="cfileuploader">
                                                <input type="button" id="btnCAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttachDoc()"
                                                    value="Save Attachment" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Button ID="btnSaveCAttachment" runat="server" Style="display: none" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 200px;">
                                            <asp:GridView ID="dgv_Attchment" runat="server" AutoGenerateColumns="False" Width="99%"
                                                CssClass="table table-hover table-bordered" RowStyle-Wrap="false" DataKeyNames="scanattachtranunkid,GUID">
                                                <Columns>
                                                    <asp:TemplateField FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="DeleteImg" runat="server" CommandName="rDelete" ToolTip="Delete">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField FooterText="objcohdownload" Visible="false">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="colhDownloadImage" runat="server" ToolTip="Download" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                    OnClick="lnkdownloadClaimAttachmentAttachment_Click">
                                                                                   <i class="fas fa-download"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                    <asp:BoundField DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                    <asp:BoundField DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                </Columns>
                                                <HeaderStyle Font-Bold="False" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary"
                                    Visible="false" />
                                <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Request" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Training Request Form" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <asp:Panel ID="pnlPart1" runat="server" Width="100%">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpEmpName" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblOpt1" runat="server" Text="Is Training Aligned with your current role?"
                                                                CssClass="form-label"></asp:Label>
                                                            <asp:RadioButtonList ID="radYes" runat="server" RepeatDirection="Horizontal" Width="100%"
                                                                AutoPostBack="True">
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblPAPeriod" runat="server" Text="Performance Period" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpPeriod" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblJob" runat="server" Text="Title" Width="100%" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtJob" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="TrOptions" runat="server" Text="Training Options" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <asp:Panel ID="pnlPart2" runat="server" Width="100%">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="TrName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpTrainingCourse" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblIntName" runat="server" Text="Vendor" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="DrpVendorName" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblMode" runat="server" Text="Training Mode" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpTrainingMode" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblapplydate" runat="server" Text="Apply Date" CssClass="form-label"></asp:Label>
                                                            <uc3:DateCtrl ID="dtapplydate" runat="server" Enabled="false" />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblListTrDatefrom" runat="server" Text="Training Date From" CssClass="form-label"></asp:Label>
                                                            <uc3:DateCtrl ID="dttrainingFromdate" runat="server" AutoPostBack="true" />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblListTrDateto" runat="server" Text="To " CssClass="form-label"></asp:Label>
                                                            <uc3:DateCtrl ID="dttrainingTodate" runat="server" />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblDuration" runat="server" Text="Duration" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtDuration" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblVenue" runat="server" Text="Venue Name" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtVenue" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                        Rows="3"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblTrainingtype" runat="server" Text="Training type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpTrainingType" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="LblReqRemark" runat="server" Text="Requisition Remark" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtReqRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblOtherInformation" runat="server" Text="Other Training Data" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 690px;">
                                                            <ul class="nav nav-tabs" role="tablist" id="Tabs">
                                                                <li role="presentation" class="active"><a href="#tabpOtherInfo" data-toggle="tab">Other
                                                                    Information </a></li>
                                                                <li role="presentation"><a href="#tabpResourceInfo" data-toggle="tab">Requisition Resources</a></li>
                                                                <li role="presentation"><a href="#tabpCosting" data-toggle="tab">Training Costing</a></li>
                                                                <li role="presentation"><a href="#tabpDocuments" data-toggle="tab">Attach Document</a></li>
                                                            </ul>
                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane fade in active" id="tabpOtherInfo">
                                                                    <asp:Panel ID="pnlPart3" runat="server" Width="100%" Height="100%">
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="LblOthTrainings" runat="server" Text="Have you attended any other training/trainings for the past one (1) year?"
                                                                                    CssClass="form-label"></asp:Label>
                                                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                                                    Width="100%">
                                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </div>
                                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="LblTrMode1" runat="server" Text="Training Mode" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <asp:DropDownList ID="drpOtherTrainingMode" runat="server" AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="LblTrainings" runat="server" Text=" Trainings" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtTrRemark" runat="server" Rows="2" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-30">
                                                                                <asp:Button ID="btnAddOthTraining" runat="server" CssClass="btn btn-primary" Text="Add" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="table-responsive" style="max-height: 228px;">
                                                                                    <asp:GridView ID="gvOtherTrainingList" runat="server" AutoGenerateColumns="False"
                                                                                        Width="100%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                                        <Columns>
                                                                                            <asp:TemplateField FooterText="colhedit">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%#Eval("tranguid")%>'
                                                                                                        ToolTip="Edit">
                                                                                                    <i class="fas fa-pencil-alt"></i>
                                                                                                    </asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle VerticalAlign="Top" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField FooterText="colhdelete">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("tranguid")%>'
                                                                                                        OnClick="lnkOtherTrainingdelete_Click" ToolTip="Delete">
                                                                                                     <i class="fas fa-trash text-danger"></i>
                                                                                                    </asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle VerticalAlign="Top" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField DataField="trainingmode" FooterText="colhoTrainingMode" HeaderText="Training Mode">
                                                                                                <ItemStyle VerticalAlign="Top" />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="item_description" FooterText="colhoTrainingName" HeaderText="Other Training">
                                                                                                <ItemStyle VerticalAlign="Top" />
                                                                                            </asp:BoundField>
                                                                                        </Columns>
                                                                                        <HeaderStyle Font-Bold="False" />
                                                                                        <RowStyle />
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane fade" id="tabpResourceInfo">
                                                                    <asp:Panel ID="pnlPart4" runat="server" Width="100%" Height="100%">
                                                                        <div class="body">
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                                                    <asp:Label ID="LblReqRes" runat="server" Text="Requisition Resources" CssClass="form-label"></asp:Label>
                                                                                    <div class="form-group">
                                                                                        <asp:DropDownList ID="drpTrainingResources" runat="server">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                                                    <asp:Label ID="LblResRemark" runat="server" Text="Resource Remark" CssClass="form-label"></asp:Label>
                                                                                    <div class="form-group">
                                                                                        <div class="form-line">
                                                                                            <asp:TextBox ID="txtResRemark" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-30">
                                                                                    <asp:Button ID="btnAddResources" runat="server" Text="Add" CssClass="btn btn-primary">
                                                                                    </asp:Button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="table-responsive" style="max-height: 200px;">
                                                                                        <asp:GridView ID="gvResourcesList" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" ShowFooter="false" CssClass="table table-hover table-bordered"
                                                                                            RowStyle-Wrap="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" CommandArgument='<%#Eval("tranguid")%>'
                                                                                                            ToolTip="Edit" OnClick="lnkResourceEdit_Click">
                                                                                                            <i class="fas fa-pencil-alt"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="lnkdelete" runat="server" ToolTip="Delete" CommandArgument='<%#Eval("tranguid")%>'
                                                                                                            OnClick="lnkResourceDelete_Click">
                                                                                                            <i class="fas fa-trash text-danger"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField HeaderText="Training Resource" DataField="resourcetype" ItemStyle-VerticalAlign="Top"
                                                                                                    FooterText="colhResourcetype" />
                                                                                                <asp:BoundField HeaderText="Resource Description" DataField="item_description" ItemStyle-VerticalAlign="Top"
                                                                                                    FooterText="colhResourceDescription" />
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane fade" id="tabpCosting">
                                                                    <asp:Panel ID="pnlPart7" runat="server" Width="100%" Height="100%">
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No." CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblClaimDate" runat="server" Text="Claim Date" CssClass="form-label"></asp:Label>
                                                                                        <uc3:DateCtrl ID="dtpClaimDate" runat="server" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="lblClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="lblDomicileAddress" runat="server" Text="Domiclie Address" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtDomicileAddress" runat="server" ReadOnly="True" TextMode="MultiLine"
                                                                                            CssClass="form-control" Rows="3"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="True">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblUoMType" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtUoMType" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                                                                    CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblSectorRoute" runat="server" Text="Sector / Route" CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <asp:DropDownList ID="cboSecRoute" runat="server" AutoPostBack="true">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtUnitPrice" CssClass="form-control" runat="server" Style="text-align: right"
                                                                                                    onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="lblExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label"></asp:Label>
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtExpenseRemark" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="footer">
                                                                                    <asp:Button ID="btnAddExpense" runat="server" Text="Add" class="btn btn-primary" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 380px;">
                                                                                            <asp:Panel ID="plnCostingData" runat="server" Width="100%">
                                                                                                <asp:GridView ID="gvCosting" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" DataKeyNames="GUID,crtranunkid,crmasterunkid">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField FooterText="colhedit">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:LinkButton ID="lnkCEdit" runat="server" OnClick="lnkEditExpense_Click" CommandArgument='<%#Eval("crtranunkid")%>'
                                                                                                                    ToolTip="Edit">
                                                                                                    <i class="fas fa-pencil-alt"></i>
                                                                                                                </asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center">
                                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                                            <ItemTemplate>
                                                                                                                <asp:LinkButton ID="imgView" runat="server" CommandArgument='<%#Eval("crtranunkid")%>'
                                                                                                                    ToolTip="Attachment" OnClick="lnkClaimAttach_Click">
                                                                                                                        <i class="fas fa-paperclip"></i>
                                                                                                                </asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:TemplateField FooterText="colhdelete">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:LinkButton ID="lnkCDelete" runat="server" OnClick="lnkDeleteExpense_Click" ToolTip="Delete"
                                                                                                                    CommandArgument='<%#Eval("crtranunkid")%>'>
                                                                                                                    <i class="fas fa-trash text-danger"></i>
                                                                                                                </asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:BoundField DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc" />
                                                                                                        <asp:BoundField DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route" />
                                                                                                        <asp:BoundField DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" />
                                                                                                        <asp:BoundField DataField="quantity" FooterText="dgcolhQty" HeaderText="Quantity">
                                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                                        </asp:BoundField>
                                                                                                        <asp:BoundField DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderText="Unit Price">
                                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                                        </asp:BoundField>
                                                                                                        <asp:BoundField DataField="amount" FooterText="dgcolhAmount" HeaderText="Amount">
                                                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                                                        </asp:BoundField>
                                                                                                    </Columns>
                                                                                                    <HeaderStyle Font-Bold="False" />
                                                                                                    <RowStyle />
                                                                                                </asp:GridView>
                                                                                            </asp:Panel>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                                                                                        <h3>
                                                                                            <asp:Label ID="lblTotalCosting" runat="server" Text="Total" Font-Bold="True" CssClass="form-label"></asp:Label>
                                                                                        </h3>
                                                                                    </div>
                                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtTotalCosting" ReadOnly="True" runat="server" Style="text-align: right"
                                                                                                    CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane fade" id="tabpDocuments">
                                                                    <asp:Panel ID="pnlPart6" runat="server" Width="100%" Height="100%">
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                                                <div class="form-group">
                                                                                    <asp:DropDownList ID="cboDocumentType" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-30">
                                                                                <asp:UpdatePanel ID="UPUpload" runat="server" Visible="False">
                                                                                    <ContentTemplate>
                                                                                        <uc9:FileUpload ID="flUpload" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                                <asp:Panel ID="pnl_ImageAdd" runat="server" Width="100%">
                                                                                    <div id="fileuploader">
                                                                                        <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                                                            value="Save Attachment" />
                                                                                    </div>
                                                                                </asp:Panel>
                                                                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="row clearfix">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="table-responsive" style="max-height: 200px;">
                                                                                    <asp:GridView ID="gvGrievanceAttachment" runat="server" AutoGenerateColumns="False"
                                                                                        CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%"
                                                                                        DataKeyNames="GUID,scanattachtranunkid">
                                                                                        <Columns>
                                                                                            <asp:TemplateField FooterText="objcohDelete">
                                                                                                <ItemTemplate>
                                                                                                    <span class="gridiconbc">
                                                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CommandName="RemoveAttachment" ToolTip="Delete">
                                                                                                            <i class="fas fa-trash text-danger"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </span>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle />
                                                                                                <ItemStyle />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField FooterText="objcohdownload" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <span class="gridiconbc">
                                                                                                        <asp:LinkButton ID="DownloadLink" runat="server" ToolTip="Download" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                                                            OnClick="lnkdownloadAttachment_Click">
                                                                                                             <i class="fas fa-download"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </span>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                            <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize">
                                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                                            </asp:BoundField>
                                                                                        </Columns>
                                                                                        <HeaderStyle Font-Bold="False" />
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlPart5" runat="server" Visible="false">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card inner-card">
                                                <div class="header">
                                                    <h2>
                                                        <asp:Label ID="lblApprovalData" runat="server" Text="Approval Info" CssClass="form-label"></asp:Label>
                                                    </h2>
                                                </div>
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="drpApprover" Width="367px" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtApproverLevel" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblApprRejectRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Button ID="btnApprove" runat="server" Text="Approve" class="btn btn-default"
                                        Visible="false" />
                                    <asp:Button ID="btnDisapprove" runat="server" Text="Reject" class="btn btn-default"
                                        Visible="false" />
                                </div>
                                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
