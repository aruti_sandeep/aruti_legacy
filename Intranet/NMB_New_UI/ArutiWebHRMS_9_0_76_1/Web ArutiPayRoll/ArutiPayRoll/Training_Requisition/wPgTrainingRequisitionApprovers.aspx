﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgTrainingRequisitionApprovers.aspx.vb" Inherits="Training_Requisition_wPgTrainingRequisitionApprovers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modal-backdrop"
                    TargetControlID="Label3" runat="server" PopupControlID="PanelApproverUseraccess"
                    CancelControlID="Label4" />
                <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="Label3" runat="server" Text="Add/ Edit Approver" CssClass="form-label"></asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="Label4" runat="server" Text="Approvers Info" Font-Bold="true" CssClass="form-label" />
                                </h2>
                            </div>
                            <div class="body" style="max-height: 525px;">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApproverUseraccess_level" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpApproverUseraccess_level" runat="server" Width="450px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApproverUseraccess_user" runat="server" Text="User" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpApproverUseraccess_user" runat="server" Width="450px" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 200px;">
                                            <asp:GridView ID="TvApproverUseraccess" runat="server" ShowHeader="false" AutoGenerateColumns="False"
                                                Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="UserAccess" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApproverUseraccessSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" Title="Are You Sure You Want Delete Approval ?" />
                <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" Title="Confirmation"
                    Message="Are You Sure You Want To Deactive  This Approver?" />
                <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" Title="Confirmation"
                    Message="Are You Sure You Want To Active This Approver?" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Requisition Approver"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Approver(s) List" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpLevel" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" Width="100%" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpApprover" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 350px;">
                                            <asp:GridView ID="gvApproverList" DataKeyNames="mappingunkid" runat="server" AutoGenerateColumns="False"
                                                Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <%--<asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit" OnClick="lnkedit_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click"
                                                                CommandArgument='<%#Eval("mappingunkid") %>' ToolTip="Delete">
                                                                <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Active" OnClick="lnkActive_Click"
                                                                CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="DeActive" OnClick="lnkDeActive_Click"
                                                                CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Approver Name" DataField="approver" />
                                                    <%--<asp:BoundField HeaderText="Department" DataField="departmentname" />
                                        <asp:BoundField HeaderText="Job" DataField="jobname" />--%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
