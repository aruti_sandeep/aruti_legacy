﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgTrainingRequisitionApproverLevel.aspx.vb" Inherits="Training_Requisition_wPgTrainingRequisitionApproverLevel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupApproverLevel" BackgroundCssClass="modal-backdrop"
                    TargetControlID="txtlevelcode" runat="server" PopupControlID="pnlApprover" DropShadow="false"
                    CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlApprover" runat="server" CssClass="card modal-dialog" Style="display: none;">
                 <%--//Pinkal (08-Apr-2021)-- Start
                         //Enhancement  -  Working on Employee Claim Form Report.--%>
                    <div class="header">
                    <%-- //Pinkal (08-Apr-2021)-- End--%>
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Approver Level Add/ Edit" runat="server" CssClass="form-label" />
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar" Width="100%"
                                            CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpTrainingCalendar" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevelcode" runat="server" Text="Level Code" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtlevelcode" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtlevelname" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <%--//Pinkal (08-Apr-2021)-- Start
                                       //Enhancement  -  Working on Employee Claim Form Report.--%>
                                        <asp:Label ID="lbllevelpriority" runat="server" Text="Priority (Smaller number means lower level approver)" CssClass="form-label" />
                                        <%--//Pinkal (08-Apr-2021)-- Start
                                        
                                        <%--<asp:TextBox ID="txtlevelpriority" runat="server" Enabled="false" Style="text-align: right;"
                                                    CssClass="form-control"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="nudYear" runat="server" Width="100" Minimum="0" TargetControlID="txtlevelpriority">
                                                </cc1:NumericUpDownExtender>--%>
                                        <uc9:txtNumeric ID="txtlevelpriority" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSaveApprover" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClosApprover" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc7:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="Are You Sure You Want To Delete This Approver Level?" />
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%--<asp:Label ID="lblPageHeader2" runat="server" Text="Approver Level List" CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Approver Level" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 350px;">
                                            <asp:Panel ID="pnl_dgView" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="GvApprLevelList" DataKeyNames="levelunkid" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("levelunkid") %>'>
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("levelunkid") %>'> 
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="levelcode" HeaderText="Level Code" HeaderStyle-Width="20%">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="levelname" HeaderText="Level Name" HeaderStyle-Width="25%">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="priority" HeaderText="Priority Level" HeaderStyle-Width="15%"
                                                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="calendar" HeaderText="Calendar" HeaderStyle-Width="25%">
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" CssClass="btn btn-primary" Text="New" OnClick="btnnew_Click" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
