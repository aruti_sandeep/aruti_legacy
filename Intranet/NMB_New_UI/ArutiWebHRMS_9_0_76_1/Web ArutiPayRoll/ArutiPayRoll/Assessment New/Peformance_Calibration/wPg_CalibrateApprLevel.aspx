﻿<%@ Page Title="Approval Level Master" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_CalibrateApprLevel.aspx.vb" Inherits="wPg_CalibrateApprLevel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approver Level"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader2" runat="server" Text="Approver Level List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:GridView ID="GvApprLevelList" DataKeyNames="levelunkid" runat="server" Style="margin: auto"
                                                AutoGenerateColumns="False" CssClass="table table-hover table-bordered" Width="99%">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("levelunkid") %>'>
                                                            <i class="fas fa-pencil-alt text-primary" ></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("levelunkid") %>'>
                                                            <i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="levelcode" HeaderText="Level Code" FooterText="dgcolhCode">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="levelname" HeaderText="Level Name" FooterText="dgcolhLevel">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="priority" HeaderText="Priority Level" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhPriority"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" CssClass="btn btn-primary" Text="New" OnClick="btnnew_Click" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupApproverLevel" BackgroundCssClass="modal-backdrop"
                    TargetControlID="txtlevelcode" runat="server" PopupControlID="pnlApprover" DropShadow="false"
                    CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlApprover" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Approver Level Add/ Edit" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <asp:Label ID="lbllevelcode" runat="server" Text="Level Code" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelcode" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <asp:Label ID="lbllevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelname" runat="server" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <asp:Label ID="lbllevelpriority" runat="server" Text="Priority" CssClass="form-label" />
                                <nut:NumericText ID="txtlevelpriority" runat="server" Max="9999" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveApprover" runat="server" CssClass="btn btn-primary" Text="Save" />
                        <asp:Button ID="btnCloseApprover" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                </asp:Panel>
                <%--OLD--%>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="Div1" class="panel-default">
                            <div id="Div3" class="panel-heading-default">
                                <div style="float: left;">
                                </div>
                            </div>
                            <div id="Div2" class="panel-body-default">
                                <asp:Panel ID="pnl_dgView" runat="server" Height="350px" ScrollBars="Auto">
                                </asp:Panel>
                                <div class="btn-default">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <uc7:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="Are You Sure You Want To Delete This Approver Level?" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
