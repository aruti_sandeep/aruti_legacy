﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_CalibrationApprovalList.aspx.vb"
    Inherits="Assessment_New_Peformance_Calibration_wPg_CalibrationApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>--%>
    <style>
        .vl
        {
            border-left: 1px solid black;
            height: 15px;
        }
    </style>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script language="javascript" type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };
        function GetSelected() {
            var ischkboxfound = false;
            var grid = document.getElementById("<%=gvFilterRating.ClientID%>");
            var checkBoxes = grid.getElementsByTagName("INPUT");
            $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
            $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i] != undefined && checkBoxes[i].checked) {
                    var row = $(checkBoxes[i]).closest("tr").find("td:eq(1)").attr('title');
                    ischkboxfound = true;
                    var res = row.split(" - ");
                    RangeSearch(parseFloat(res[0]), parseFloat(res[1]));
                }
            }
            if (ischkboxfound == false) {
                $('#<%= gvApplyCalibration.ClientID %> tr').show();
            }
        }

        function RangeSearch(val1, val2) {
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "pScore") {
                        if (parseFloat($(col).html()) >= val1 && parseFloat($(col).html()) <= val2) {
                            $(row).show();
                        }
                    }
                }
            }
        }

        function ListSeaching() {
            if ($('#<%= txtClistSearch.ClientID %>').val().length > 0) {
                $('#<%= gvCalibrateList.ClientID %> tbody tr').hide();
                $('#<%= gvCalibrateList.ClientID %> tbody tr:first').show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtClistSearch.ClientID %>').val() + '\')').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.MainGroupHeaderStyleLeft').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.GroupHeaderStylecompLeft').parent().show();
            }
            else if ($('#<%= txtClistSearch.ClientID %>').val().length == 0) {
                resetListSeaching();
            }
            if ($('#<%= gvCalibrateList.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetListSeaching();
            }
        }
        function resetListSeaching() {
            $('#<%= txtClistSearch.ClientID %>').val('');
            $('#<%= gvCalibrateList.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtClistSearch.ClientID %>').focus();
        }

        function FromSearching() {
            if ($('#<%= txtListSearch.ClientID %>').val().length > 0) {
                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtListSearch.ClientID %>').val() + '\')').parent().show();
            }
            else if ($('#<%= txtListSearch.ClientID %>').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= gvApplyCalibration.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#<%= txtListSearch.ClientID %>').val('');
            $('#<%= gvApplyCalibration.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtListSearch.ClientID %>').focus();
        }
        //        $("[id*=chkHeder1]").live("click", function() {
        //            var chkHeader = $(this);
        //            var grid = $(this).closest("table");
        //            $("input[type=checkbox]", grid).each(function() {
        //                if (chkHeader.is(":checked")) {

        //                    if ($(this).is(":visible")) {
        //                        $(this).attr("checked", "checked");
        //                    }
        //                } else {
        //                    $(this).removeAttr("checked");
        //                }
        //            });
        //        });

        //        $("[id*=chkbox1]").live("click", function() {
        //            var grid = $(this).closest("table");
        //            var chkHeader = $("[id*=chkHeader]", grid);
        //            var row = $(this).closest("tr")[0];
        //            if (!$(this).is(":checked")) {
        //                var row = $(this).closest("tr")[0];
        //                chkHeader.removeAttr("checked");

        //            } else {

        //                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
        //                    chkHeader.attr("checked", "checked");
        //                }
        //            }

        //        });

        $("body").on("click", "[id*=chkHeder1]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkbox1]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeder1]", grid);
            debugger;
            if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        function RatingOperation() {
            var ratingid = 0;
            var rid = document.getElementById("<%=cbocRating.ClientID%>");
            ratingid = parseInt($(rid).val());
            if (ratingid <= 0) {

                swal({ title: '', text: "Sorry, Rating is mandatory information. Please select rating to continue." });
                return;
            }
            var grid = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            if (grid == null) {
                swal({ title: '', text: "Sorry, No records found in order to perform selected operation." });
                return;
            }
            var counter = 0;
            var txt = document.getElementById("<%=txtIRemark.ClientID%>").value;
            $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    counter++;
                }
            });
            if (counter == 0) {
                swal({ title: '', text: "Sorry, Please check atleast one information in order to calibrate score." });
                return;
            }

            $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    $(this).closest("tr").find("td[Id='cRem']").html(txt);
                    $(this).closest("tr").find("td[Id='cRate']").html(rid.options[rid.selectedIndex].innerHTML);
                    $(this).closest("tr").find("td[Id='iChng']").html("1");
                }
            });
            document.getElementById("<%=txtIRemark.ClientID%>").value = "";
            rid.selectedIndex = 0;
            swal({ title: '', type: "success", text: "Selected rating applied successfully." });
        }

        function ApproveReject(ival) {
            var flg = false;
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");

            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "cRate") {
                        if ($(col).html() == '&nbsp;') {
                            flg = true;
                            break;
                        }
                    }
                }
                if (flg == true) { break; }
            }
            if (flg == true) {
                swal({ title: '', text: "Sorry, you have not calibrate some of the employee(s). Please calibrated them in order to perform submit operation." });
                return;
            }

            var amark = document.getElementById("<%=txtApprRemark.ClientID%>").value;

            if (amark.trim().length <= 0 && ival == 0) {
                swal({ title: '', text: "Sorry, remark is mandatory information. Please enter remark to continue." });
                return;
            }
            var txt = "";
            var eOpr = 0;
            switch (ival) {
                case 0: // REJECT
                    txt = "Are you sure you want to reject selected score calibration number?";
                    eOpr = 3;
                    break;
                case 1: // APPROVE
                    txt = "Are you sure you want to approve selected score calibration number?";
                    eOpr = 2;
                    break;
            }
            var blnYes = false;
            swal({
                title: '',
                text: txt,
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {
                    blnYes = true;
                    if (blnYes == true) {
                        var dictRate = []; var dictRem = [];
                        //var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
                        for (var i = 1, row; row = table.rows[i]; i++) {
                            for (var j = 1, col; col = row.cells[j]; j++) {
                                if ($(col).attr('Id') != undefined && $(col).attr('Id') == "trgid") {
                                    dictRate.push(
                                        $(row).closest("tr").find("td[Id='trgid']").text() + "|" + $(row).closest("tr").find("td[Id='cRate']").text()
                                    );
                                    dictRem.push(
                                        $(row).closest("tr").find("td[Id='trgid']").text() + "|" + $(row).closest("tr").find("td[Id='cRem']").text()
                                    );
                                }
                            }
                        }

                        var coyid = '<%= Session("CompanyUnkId") %>';
                        var ipadd = '<%= Session("IP_ADD") %>';
                        var hstname = '<%= Session("HOST_NAME") %>';
                        var sndradd = '<%= Session("Senderaddress") %>';
                        var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
                        var usrid = '<%= Session("UserId") %>';
                        var yrid = '<%= Session("Fin_year") %>';
                        var empdate = '<%= Session("EmployeeAsOnDate") %>';
                        var cprip = document.getElementById('<%= hdfpriority.ClientID %>').value;
                        var db = '<%= Session("Database_Name") %>';

                        swal({ title: '', type: "info", text: "Sending Notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });

                        PageMethods.PerfApprRejOperation(eOpr, $(pid).val(), pid.options[pid.selectedIndex].text, db, usrid, yrid, coyid, empdate.toString(), empdate.toString(), dictRate, dictRem, amark, cprip, ipadd, hstname, "frmApproveRejectCalibrationAddEdit", sndradd, onSuccess, onFailure);
                        //PageMethods.PerfApprRejOperation(eOpr, $(pid).val(), pid.options[pid.selectedIndex].text, db, usrid, yrid, coyid, empdate.toString(), empdate.toString(), amark, cprip, ipadd, hstname, "frmApproveRejectCalibrationAddEdit", sndradd, onSuccess, onFailure);

                        function onSuccess(str) {
                            if (str == "1") {
                                if (ival == 0)
                                { swal({ title: '', type: "success", text: "Batch has been rejected successfully." }); }
                                else
                                { swal({ title: '', type: "success", text: "Batch has beed approved successfully." }); }
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                                document.getElementById('<%=btnIApprove.ClientID %>').style.display = 'none';
                                document.getElementById('<%=btnIReject.ClientID %>').style.display = 'none';
                                document.getElementById("<%= hdfEditedcNum.ClientID %>").value = "";
                                document.getElementById("<%=txtApprRemark.ClientID%>").value = "";
                                //                                var calno = document.getElementById('<%= hdfEditedcNum.ClientID %>').value;
                                //                                document.getElementById("<%= hdfEditedcNum.ClientID %>").value = "";                                
                                return;
                            }
                        }

                        function onFailure(err) {
                            //alert(err.get_message());
                            swal({ title: '', type: "warning", text: err.get_message() });
                            return;
                        }
                    }
                }
            });
            return;
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Score Calibration Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria/Approver Info."></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r-50 p-l-0">
                                    <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                        Visible="false"><i class="fas fa-sliders-h"></i></asp:LinkButton>
                                </ul>
                                <ul class="header-dropdown m-r-25 p-l-0">
                                    <asp:LinkButton ID="lnklstInfo" runat="server" ToolTip="View Rating Information"
                                        Text="Show Ratings" Font-Underline="false" Visible="false"></asp:LinkButton>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprover" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLevel" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="hdfpriority" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdfEditedcNum" runat="server" />
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblRemark" runat="server" Text="Calib. Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCalibrationNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblPScoreFrom" runat="server" Text="Prov. Rating From" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cbopRatingFrm" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblPScTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cbopRatingTo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblCScoreForm" runat="server" Text="Calb. Rating From" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cbocRatingFrm" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblCScTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cbocRatingTo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Font-Bold="true"
                                    Checked="true" AutoPostBack="true" Visible="false" CssClass="pull-left" />
                                <asp:Button ID="btnShowAll" runat="server" Text="My Report" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnDisplayCurve" runat="server" Text="Display HPO Curve" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtClistSearch" runat="server" AutoPostBack="false" class="form-control"
                                                    onkeyup="ListSeaching();"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 250px">
                                            <asp:GridView ID="gvCalibrateList" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,lusername,allocation,calibuser,ispgrp,pgrpid,total"
                                                runat="server" AutoGenerateColumns="false" Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                                ToolTip="Approve/Reject" OnClick="lnkedit_Click"><i class="fa fa-tasks" style="font-size:14px; color:Red;"></i> </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgEmployee" />
                                                    <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhlstpRating" />
                                                    <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhsacrating" />
                                                    <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhsacremark" />
                                                    <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhlstcRating" />
                                                    <asp:BoundField DataField="apcalibremark" HeaderText="Calib. Remark" FooterText="dgcolheRemark" />
                                                    <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhsApprovalRemark" />
                                                    <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgiStatus" />
                                                    <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgLevelName" />
                                                    <asp:BoundField DataField="calibration_no" HeaderText="" FooterText="objdgcolhcbno" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                    BehaviorID="mpe" TargetControlID="lblCalibrateHeading" runat="server" PopupControlID="PanelApproverUseraccess"
                    CancelControlID="lblFilter" />
                <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCalibrateHeading" runat="server" Text="BSC Calibration"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblFilter" runat="server" Text="Search Criteria" />
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblSPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboSPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblCRating" runat="server" Text="Calibration Rating" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cbocRating" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblIRemark" runat="server" Text="Calibration Remark" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtIRemark" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-25">
                                                <asp:Button ID="btnIApply" runat="server" Text="Apply" OnClientClick="RatingOperation(1); return false;"
                                                    CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblFilterData" runat="server" Text="Search" CssClass="form-label"
                                                    Visible="false"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtListSearch" runat="server" onkeyup="FromSearching();" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 170px">
                                                            <asp:GridView ID="gvFilterRating" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                Width="95%" DataKeyNames="scrf,scrt,id">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkFRating" runat="server" Enabled="true" CssClass="filled-in"
                                                                                Text=" " CommandArgument='<%# Container.DataItemIndex %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="name" HeaderText="Filter By Calib. Rating" FooterText="dgcolhFRating" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Button ID="btnFilter" OnClientClick="GetSelected(); return false;" runat="server"
                                                            Text="Filter" CssClass="btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 200px">
                                                            <asp:GridView ID="gvApplyCalibration" DataKeyNames="employeeunkid,periodunkid,calibratnounkid,calibratescore,code,lusername,tranguid"
                                                                runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                Width="99%" AllowPaging="false">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CssClass="filled-in" Text=" "
                                                                                CommandArgument='<%# Container.DataItemIndex %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhSEmployee" />
                                                                    <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="dgcolhSJob" />
                                                                    <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhprovRating" />
                                                                    <asp:BoundField DataField="lrating" HeaderText="" FooterText="dgcolhlastlvlrating" />
                                                                    <asp:BoundField DataField="lcalibremark" HeaderText="" FooterText="dgcolhlastlvlremark" />
                                                                    <asp:BoundField DataField="acrating" HeaderText="Calib. Rating" FooterText="dgcolhcalibRating" />
                                                                    <asp:BoundField DataField="acremark" HeaderText="Calibration Remark" FooterText="dgcolhcalibrationremark"
                                                                        ItemStyle-Wrap="true" />
                                                                    <asp:BoundField DataField="calibratescore" HeaderText="" FooterText="objdgcalibratescore"
                                                                        ItemStyle-Wrap="true" />
                                                                    <asp:BoundField DataField="ichanged" HeaderText="" FooterText="objdgichanged" />
                                                                    <asp:BoundField DataField="tranguid" HeaderText="" FooterText="objdgtranguid" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblApprRemark" runat="server" Text="Remark" CssClass="form-label"
                                                    Visible="false"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtApprRemark" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnIApprove" runat="server" Text="Approve" CssClass="btn btn-primary"
                            OnClientClick="ApproveReject(1); return false;" />
                        <asp:Button ID="btnIReject" runat="server" Text="Reject" CssClass="btn btn-primary"
                            OnClientClick="ApproveReject(0); return false;" />
                        <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupMyReport" BackgroundCssClass="modalBackground" TargetControlID="lblMyReport"
                    runat="server" PopupControlID="pnlMyReport" CancelControlID="lblMyReport" />
                <asp:Panel ID="pnlMyReport" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblMyReport" runat="server" Text="My Report"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r-50 p-l-0">
                            <asp:LinkButton ID="lnkRptAdvFilter" Font-Underline="false" runat="server" ToolTip="Advance Filter"
                                Visible="false"><i class="fas fa-sliders-h"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblRptPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRptPeriod" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblRptCalibNo" runat="server" Text="Calibration No" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRptCalibNo" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblRptEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRptEmployee" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnRptShow" runat="server" Text="Show" CssClass="btn btn-primary" />
                        <asp:Button ID="btnRptReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 325px">
                                            <asp:GridView ID="gvMyReport" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,allocation,calibuser,total"
                                                runat="server" AutoGenerateColumns="false" Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhMEmployee" />
                                                    <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhMprovRating" />
                                                    <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhMacrating" />
                                                    <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhMacremark" />
                                                    <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhMcalibRating" />
                                                    <asp:BoundField DataField="apcalibremark" HeaderText="Calibration Remark" FooterText="dgcolhMcalibrationremark"
                                                        ItemStyle-Wrap="true" />
                                                    <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhMApprovalRemark" />
                                                    <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgcolhMiStatus" />
                                                    <asp:BoundField DataField="username" HeaderText="Approver" FooterText="dgcolhMusername" />
                                                    <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgcolhMlevelname" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnExportMyReport" runat="server" Text="Export" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnCloseMyReport" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modalBackground" TargetControlID="lblHPOCurver"
                    runat="server" PopupControlID="pnlHPOChart" CancelControlID="lblHPOCurver" />
                <asp:Panel ID="pnlHPOChart" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHPOCurver" runat="server" Text="HPO Chart"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r-50 p-l-0">
                            <asp:LinkButton ID="lnkChAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                Visible="false"><i class="fas fa-sliders-h"></i></asp:LinkButton>
                        </ul>
                        <asp:Label ID="lblChGenerate" runat="server" Text="Generate Criteria" Visible="false" />
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="lblchPeriod" runat="server" Text="Period" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cboChPeriod" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="lblChDisplayType" runat="server" Text="Display Type" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cbochDisplay" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnChGenerate" runat="server" Text="Generate" CssClass="btn btn-primary" />
                        <asp:Button ID="btnChReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="body" style="height: 330px">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 165px">
                                            <asp:DataGrid ID="dgvCalibData" runat="server" Width="99%" AllowPaging="false" ShowHeader="false"
                                                CssClass="table table-hover table-bordered">
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                            BorderlineWidth="2" Width="917px" Style="margin-left: 7px" Height="300px" AntiAliasing="All">
                                            <Titles>
                                                <asp:Title Name="Title1">
                                                </asp:Title>
                                            </Titles>
                                            <Series>
                                                <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                    MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                                </asp:Series>
                                                <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                    MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1">
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnChClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExportMyReport" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:content>
