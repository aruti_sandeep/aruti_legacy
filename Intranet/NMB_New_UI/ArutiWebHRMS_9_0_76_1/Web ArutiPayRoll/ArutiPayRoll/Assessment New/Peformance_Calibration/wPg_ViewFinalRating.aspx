<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_ViewFinalRating.aspx.vb" Inherits="wPg_ViewFinalRating" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="View Final Rating"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r-20 p-l-0">
                                    <asp:CheckBox ID="chkShowPreviousRating" runat="server" Text="Show previous ratings"
                                        Font-Bold="true" />
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployeeName" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                        <asp:Label ID="lblYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboYear" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 240px">
                                            <asp:GridView ID="gvCalibrateList" DataKeyNames="yid,pid,empid,fscr,dbname" runat="server"
                                                AutoGenerateColumns="False" CssClass="table table-hover table-bordered" Width="99%"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="fy" HeaderText="FY" FooterText="dgFY" HeaderStyle-Width="10%" />
                                                    <asp:BoundField DataField="pname" HeaderText="Period" FooterText="dgPeriod" HeaderStyle-Width="25%" />
                                                    <asp:BoundField DataField="rating" HeaderText="Rating" FooterText="dgRating" HeaderStyle-Width="62%" />
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDescription" runat="server" ToolTip="View
                Rating Info" OnClick="lnkInfo_Click"><i class="fa fa-info-circle" style="font-size:19px;
                color:Blue;"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupinfo" BackgroundCssClass="modalBackground" TargetControlID="lblRatingInfo"
                    runat="server" PopupControlID="pnlRatingInfo" CancelControlID="btnCloseRating" />
                <asp:Panel ID="pnlRatingInfo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblRatingInfo" runat="server" Text="Ratings Information"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <div class="table-responsive" style="height: 200px">
                                    <asp:GridView ID="gvRating" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                        CssClass="table table-hover table-bordered" Width="99%">
                                        <Columns>
                                            <asp:BoundField DataField="scrf" HeaderText="Score From" FooterText="dgcolhScrFrm"
                                                HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="scrt" HeaderText="Score
                To" FooterText="dgcolhScrTo" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnCloseRating" runat="server" Text="Close" CssClass="btn btn-primary" />
                    </div>
                </asp:Panel>
                <%--OLD--%>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                </div>
                                <div style="float: right; width: 20%">
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                <div class="row2">
                                    <div style="width: 10%" class="ib">
                                    </div>
                                    <div style="width: 26%" class="ib">
                                    </div>
                                    <div style="width: 8%" class="ib">
                                    </div>
                                    <div style="width: 13%" class="ib">
                                    </div>
                                    <div style="width: 5%" class="ib">
                                    </div>
                                    <div style="width: 20%" class="ib">
                                    </div>
                                    <div style="width: 9%" class="ib">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="scrollable-container" style="width: 100%; height: 240px; overflow: auto">
                        </div>
                        <div style="width: 100%" class="ib">
                            <div id="btnfixedbottom" class="btn-default">
                            </div>
                        </div>
                    </div>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
