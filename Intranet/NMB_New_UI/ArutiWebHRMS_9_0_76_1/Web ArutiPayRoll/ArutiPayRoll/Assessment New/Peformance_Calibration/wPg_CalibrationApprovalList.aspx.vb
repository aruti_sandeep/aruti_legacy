﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.Services

#End Region

Partial Class Assessment_New_Peformance_Calibration_wPg_CalibrationApprovalList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmApproveRejectCalibrationList"
    Private ReadOnly mstrModuleName1 As String = "frmApproveRejectCalibrationAddEdit"
    Private blnShowApplyCalibrationpopup As Boolean = False
    Private objScoreCalibrateApproval As New clsScoreCalibrationApproval
    Private mlbnIsRejected As Boolean = False
    Private mdtListDataTable As DataTable = Nothing
    Private mintCalibrationUnkid As Integer = 0
    Private mintEmployeeUnkid As Integer = 0
    Private mintPeriodUnkid As Integer = 0
    Private mblnIsGrp As Boolean = False
    Private mintPriority As Integer = 0
    Private mintMappingUnkid As Integer = 0
    Private mintgrpId As Int64 = 0
    Private mblnIsExpand As Boolean = False
    Private mblnShowHPOCurve As Boolean = False
    Private mstrPreviousUser As String = String.Empty
    Private mblnShowMyReport As Boolean = False

#End Region

#Region " Common Part "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clscalibrate_approver_tran
        Dim objRating As New clsAppraisal_Rating
        Try
            dsList = objEmp.GetMappedEmployeecombolist(Session("Database_Name"), _
                                            Session("UserId"), _
                                            False, _
                                            Session("EmployeeAsOnDate"), "List", True)
            With cboEmployee
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
                .DataBind()
            End With

            With cboRptEmployee
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            With cboSPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With

            With cboChPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With

            With cboRptPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            Call cboRptPeriod_SelectedIndexChanged(cboRptPeriod, New EventArgs())

            Dim objCScoreMaster As New clsComputeScore_master
            dsList = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cbochDisplay
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing

            dsList = objScoreCalibrateApproval.GetStatusList("List", True, Nothing, True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objRating.getComboList("List", True)
            With cbocRating
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            With cbopRatingFrm
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            With cbopRatingTo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            With cbocRatingFrm
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            With cbocRatingTo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetApproverInfo()
        Dim dsAppr As New DataSet
        Try
            dsAppr = objScoreCalibrateApproval.GetApproverInfo(Session("UserId"))
            If dsAppr IsNot Nothing AndAlso dsAppr.Tables(0).Rows.Count > 0 Then
                txtApprover.Text = dsAppr.Tables(0).Rows(0).Item("username").ToString()
                txtLevel.Text = dsAppr.Tables(0).Rows(0).Item("levelname").ToString()
                mintPriority = CInt(dsAppr.Tables(0).Rows(0).Item("priority"))
                mintMappingUnkid = CInt(dsAppr.Tables(0).Rows(0).Item("mappingunkid"))
                hdfpriority.Value = mintPriority
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_FRating()
        Try
            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False)
            gvFilterRating.AutoGenerateColumns = False
            gvFilterRating.DataSource = dsList
            gvFilterRating.DataBind()
            objRating = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetTitle()
        Dim dsList As New DataSet
        Dim objRating As New clsAppraisal_Rating
        Try
            dsList = objRating.getComboList("List", True)
            For Each item As ListItem In cbocRating.Items
                Dim irow As DataRow() = Nothing
                irow = dsList.Tables(0).Select("name = '" & item.Text & "' ")
                If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                    item.Attributes.Add("title", irow(0)("scrf").ToString() & " - " & irow(0)("scrt").ToString())
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing : objRating = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
'Call Language._Object.SaveValue()
                Call SetLanguage()
                Call FillCombo() : SetApproverInfo()
            Else
                mintPriority = Me.ViewState("mintPriority")
                blnShowApplyCalibrationpopup = Me.ViewState("blnShowApplyCalibrationpopup")
                mlbnIsRejected = Me.ViewState("mlbnIsRejected")
                mdtListDataTable = Me.ViewState("mdtListDataTable")
                mintCalibrationUnkid = Me.ViewState("mintCalibrationUnkid")
                mintEmployeeUnkid = Me.ViewState("mintEmployeeUnkid")
                mintPeriodUnkid = Me.ViewState("mintPeriodUnkid")
                mblnIsGrp = Me.ViewState("mblnIsGrp")
                mintMappingUnkid = Me.ViewState("mintMappingUnkid")
                mintgrpId = Me.ViewState("mintgrpId")
                mblnIsExpand = Me.ViewState("mlbnIsExpand")
                mblnShowHPOCurve = Me.ViewState("mblnShowHPOCurve")
                mstrPreviousUser = Me.ViewState("mstrPreviousUser")
                mblnShowMyReport = Me.ViewState("mblnShowMyReport")
            End If

            If blnShowApplyCalibrationpopup Then
                popupApproverUseraccess.Show()
            End If

            If mblnShowHPOCurve Then
                popupHPOChart.Show()
            End If

            If mblnShowMyReport Then
                popupMyReport.Show()
            End If
            cbocRating.Enabled = Session("AllowToEditCalibratedScore")
            txtIRemark.Enabled = Session("AllowToEditCalibratedScore")
            btnIApply.Enabled = Session("AllowToEditCalibratedScore")
            SetTitle()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintPriority") = mintPriority

            Me.ViewState("blnShowApplyCalibrationpopup") = blnShowApplyCalibrationpopup
            Me.ViewState("mlbnIsRejected") = mlbnIsRejected
            Me.ViewState("mdtListDataTable") = mdtListDataTable

            Me.ViewState("mintCalibrationUnkid") = mintCalibrationUnkid
            Me.ViewState("mintEmployeeUnkid") = mintEmployeeUnkid
            Me.ViewState("mintPeriodUnkid") = mintPeriodUnkid
            Me.ViewState("mblnIsGrp") = mblnIsGrp
            Me.ViewState("mintMappingUnkid") = mintMappingUnkid
            Me.ViewState("mintgrpId") = mintgrpId
            Me.ViewState("mlbnIsExpand") = mblnIsExpand
            Me.ViewState("mstrPreviousUser") = mstrPreviousUser
            Me.ViewState("mblnShowMyReport") = mblnShowMyReport
            Me.ViewState("mblnShowHPOCurve") = mblnShowHPOCurve
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " List Methods "

#Region " Button's Event(s) "

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            txtRemark.Text = ""
            cbopRatingFrm.SelectedValue = 0
            cbopRatingTo.SelectedValue = 0
            cbocRatingFrm.SelectedValue = 0
            cbocRatingTo.SelectedValue = 0
            txtCalibrationNo.Text = ""
            gvCalibrateList.DataSource = Nothing
            gvCalibrateList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue"), Me)
                Exit Sub
            End If

            If txtApprover.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 100, "Sorry, No approver defined with the selected login."), Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Private Methods "

    Private Sub FillGrid()
        Dim strFilter As String = String.Empty
        Dim objRating As New clsAppraisal_Rating
        Try
            If txtCalibrationNo.Text.Trim.Length > 0 Then
                strFilter &= "AND iData.calibration_no LIKE '%" & txtCalibrationNo.Text & "%'"
            End If

            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) & "' "
            End If

            If CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) > 0 Then
                strFilter &= "AND CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END = '" & CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) & "' "
            End If

            Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0
            If cbopRatingFrm.SelectedIndex > 0 AndAlso cbopRatingTo.SelectedIndex > 0 Then
                If cbopRatingTo.SelectedIndex < cbopRatingFrm.SelectedIndex Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 11, "Sorry, To rating cannot be less than From rating. Please select higher rating to continue."), Me)
                    Exit Sub
                Else
                    objRating.GetScoringRangeById(cbopRatingFrm.SelectedItem.Text, mDecFromScore, 0)
                    objRating.GetScoringRangeById(cbopRatingTo.SelectedItem.Text, 0, mDecToScore)
                    If strFilter.Trim.Length > 0 Then
                        strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & " AND iData.povisionscore <= " & mDecToScore & ") "
                    Else
                        strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & " AND iData.povisionscore <= " & mDecToScore & ") "
                    End If
                End If
            ElseIf cbopRatingFrm.SelectedIndex > 0 Then
                objRating.GetScoringRangeById(cbopRatingFrm.SelectedItem.Text, mDecFromScore, 0)
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & ") "
                Else
                    strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & ") "
                End If
            ElseIf cbopRatingTo.SelectedIndex > 0 Then
                objRating.GetScoringRangeById(cbopRatingTo.SelectedItem.Text, 0, mDecToScore)
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (iData.povisionscore <= " & mDecToScore & ") "
                Else
                    strFilter &= "AND (iData.povisionscore <= " & mDecToScore & ") "
                End If
            End If

            If cbocRatingFrm.SelectedIndex > 0 AndAlso cbocRatingTo.SelectedIndex > 0 Then
                objRating.GetScoringRangeById(cbocRatingFrm.SelectedItem.Text, mDecFromScore, 0)
                objRating.GetScoringRangeById(cbocRatingTo.SelectedItem.Text, 0, mDecToScore)
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & " AND ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                Else
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & " AND ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                End If
            ElseIf cbocRatingFrm.SelectedIndex > 0 Then
                objRating.GetScoringRangeById(cbocRatingFrm.SelectedItem.Text, mDecFromScore, 0)
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & ") "
                Else
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & ") "
                End If
            ElseIf cbocRatingTo.SelectedIndex > 0 Then
                objRating.GetScoringRangeById(cbocRatingTo.SelectedItem.Text, 0, mDecToScore)
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                Else
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                End If
            End If

            If txtRemark.Text.Trim.Length > 0 Then
                strFilter &= "AND CASE WHEN ISNULL(iData.calibration_remark,'') = '' THEN ISNULL(iData.c_remark,'') ELSE ISNULL(iData.calibration_remark,'') END LIKE '%" & txtRemark.Text & "%' "
            End If

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If

            'mdtListDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), chkMyApproval.Checked, Nothing, mintPriority, True, strFilter, "", , True)
            mdtListDataTable = objScoreCalibrateApproval.GetEmployeeApprovalDataNew(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), True, False, Nothing, mintPriority, True, strFilter, "", , True)

            gvCalibrateList.AutoGenerateColumns = False
            gvCalibrateList.DataSource = mdtListDataTable
            gvCalibrateList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Gridview Event(s) "

    Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim intCount As Integer = 2 : Dim strCaption As String = String.Empty
                If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("ispgrp").ToString) = True Then
                    For i = 2 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(1).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(1).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(2).CssClass = "MainGroupHeaderStyleLeft"
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    For i = 2 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(1).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(1).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(2).CssClass = "GroupHeaderStylecompLeft"

                    If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
                        strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                        Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 12, "Date") & " [" & oDate & "] "
                    End If
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text & " - (" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 13, "Total") & " : " & _
                    gvCalibrateList.DataKeys(e.Row.RowIndex)("total").ToString() & ") "

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = False Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
                    End If
                End If
                If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("iStatusId").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.Submitted Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If e.Row.RowType = DataControlRowType.DataRow Or e.Row.RowType = DataControlRowType.Header Then
                e.Row.Cells(getColumnID_Griview(gvCalibrateList, "objdgcolhcbno", False, True)).Attributes.Add("Id", "clbno")
                e.Row.Cells(getColumnID_Griview(gvCalibrateList, "objdgcolhcbno", False, True)).Attributes.Add("style", "display:none")
            End If        
        End Try
    End Sub

#End Region

#Region " Links Event(s) "

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CBool(gvCalibrateList.DataKeys(row.RowIndex)("isgrp").ToString) Then
                Dim blnFlag As Boolean = False
                blnFlag = objScoreCalibrateApproval.IsValidApprover(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), mintPriority, gvCalibrateList.DataKeys(row.RowIndex)("grpid"), Nothing, True, "", "")
                If blnFlag = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, you cannot do approve/reject operation. Reason selected calibration batch is still pending for lower level approver."), Me)
                    Exit Sub
                End If
                blnFlag = False
                blnFlag = objScoreCalibrateApproval.IsValidOperation(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), mintPriority, gvCalibrateList.DataKeys(row.RowIndex)("grpid"), Nothing, True, "", "")
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, you cannot do approve/reject operation. Reason the selected calibration batch is already approved or rejected."), Me)
                    Exit Sub
                End If
                cbocRating.SelectedValue = 0
                cboSPeriod.SelectedValue = gvCalibrateList.DataKeys(row.RowIndex)("periodunkid")
                mintCalibrationUnkid = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
                txtIRemark.Text = ""
                cboSPeriod.Enabled = False
                hdfEditedcNum.Value = gvCalibrateList.DataKeys(row.RowIndex)("calibration_no")
                Call Fill_FRating()
                blnShowApplyCalibrationpopup = True
                txtApprRemark.Text = ""
                popupApproverUseraccess.Show()
                FillList(CInt(gvCalibrateList.DataKeys(row.RowIndex)("grpid")))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " Add/Edit Methods "

#Region " Button's Event(s) "

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            blnShowApplyCalibrationpopup = False
            popupApproverUseraccess.Hide()
            If hdfEditedcNum.Value.Trim.Length <= 0 Then
                FillGrid()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Method(s) "

    Private Sub FillList(ByVal intGrpId As Integer)
        Try
            mdtListDataTable = objScoreCalibrateApproval.GetEmployeeApprovalDataNew(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), True, False, Nothing, mintPriority, True, "", "", , False)
            mdtListDataTable = New DataView(mdtListDataTable, "grpid = '" & intGrpId.ToString & "' AND isgrp = 0 AND userunkid = '" & Session("UserId") & "' AND iStatusId = '" & CInt(clsScoreCalibrationApproval.enCalibrationStatus.Submitted) & "'", "", DataViewRowState.CurrentRows).ToTable
            mstrPreviousUser = mdtListDataTable.Rows(0).Item("lusername").ToString()
            Dim xCol As New DataColumn
            With xCol
                .DataType = GetType(System.String)
                .DefaultValue = ""
                .ColumnName = "ichanged"
            End With
            mdtListDataTable.Columns.Add(xCol)
            If mstrPreviousUser.Trim.Length <= 0 Then
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlrating", False, True)).Visible = False
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlremark", False, True)).Visible = False
            Else
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlrating", False, True)).HeaderText = mstrPreviousUser & " - [" & gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).HeaderText & "] "
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlremark", False, True)).HeaderText = mstrPreviousUser & " - [" & gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).HeaderText & "]"
            End If

            gvApplyCalibration.AutoGenerateColumns = False
            gvApplyCalibration.DataSource = mdtListDataTable
            gvApplyCalibration.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    <WebMethod()> _
    Public Shared Function PerfApprRejOperation(ByVal eOprTypeId As Integer, _
                                           ByVal xPeriodId As Integer, _
                                           ByVal strPeriodName As String, _
                                           ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As String, _
                                           ByVal xPeriodEnd As String, _
                                           ByVal xdicGuidRating As String(), _
                                           ByVal xdicGuidRemark As String(), _
                                           ByVal strApprlRemark As String, _
                                           ByVal intCurrentPriority As Integer, _
                                           ByVal strIPAddress As String, _
                                           ByVal strHostName As String, _
                                           ByVal strScreenName As String, _
                                           ByVal strSenderAddress As String) As String



        Try
            Dim blnFlag As Boolean = False
            Dim objApprlTran As New clsScoreCalibrationApproval

            Dim xdictRating As Dictionary(Of String, String) = xdicGuidRating.AsEnumerable().Select(Function(x) New With {Key .attribute1_name = x.Split("|")(0).ToString, Key .attribute2_name = x.Split("|")(1).ToString}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
            Dim xdictRemark As Dictionary(Of String, String) = xdicGuidRemark.AsEnumerable().Select(Function(x) New With {Key .attribute1_name = x.Split("|")(0).ToString, Key .attribute2_name = x.Split("|")(1).ToString}).Distinct().ToDictionary(Function(s) s.attribute1_name, Function(s) s.attribute2_name)
            blnFlag = objApprlTran.ApproveRejectBatch(eOprTypeId, _
                                                      xPeriodId, _
                                                      strPeriodName, _
                                                      xDatabaseName, _
                                                      xUserUnkid, _
                                                      xYearUnkid, _
                                                      xCompanyUnkid, _
                                                      xPeriodStart, _
                                                      xPeriodEnd, _
                                                      xdictRating, _
                                                      xdictRemark, _
                                                      strApprlRemark, _
                                                      intCurrentPriority, _
                                                      strIPAddress, _
                                                      strHostName, _
                                                      strScreenName, _
                                                      True, _
                                                      strSenderAddress)
            If blnFlag Then
                Return "1"
            Else
                Return "0"
            End If

        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

#End Region

#Region " Grid Event(s) "

    Protected Sub gvFilterRating_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFilterRating.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(getColumnID_Griview(gvFilterRating, "dgcolhFRating", False, True)).ToolTip = gvFilterRating.DataKeys(e.Row.RowIndex)("scrf").ToString() & " - " & gvFilterRating.DataKeys(e.Row.RowIndex)("scrt").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvApplyCalibration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApplyCalibration.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSEmployee", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSEmployee", False, True)).Text.ToString.Replace("/r/n", "")
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If e.Row.RowType = DataControlRowType.DataRow Or e.Row.RowType = DataControlRowType.Header Then
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgcalibratescore", False, True)).Attributes.Add("Id", "pScore")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgcalibratescore", False, True)).Attributes.Add("style", "display:none")

                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgtranguid", False, True)).Attributes.Add("Id", "trgid")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgtranguid", False, True)).Attributes.Add("style", "display:none")

                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgichanged", False, True)).Attributes.Add("Id", "iChng")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgichanged", False, True)).Attributes.Add("style", "display:none")

                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhprovRating", False, True)).Attributes.Add("Id", "pRate")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Attributes.Add("Id", "cRate")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).Attributes.Add("Id", "cRem")

            End If
        End Try
    End Sub

#End Region

#End Region

#Region " Report Methods "

#Region " Button's Events "

    Protected Sub btnShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAll.Click
        Try
            If txtApprover.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 100, "Sorry, No approver defined with the selected login."), Me)
                Exit Sub
            End If

            Call btnRptReset_Click(btnRptReset, New EventArgs())
            mblnShowMyReport = True
            popupMyReport.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRptShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptShow.Click
        Try
            Dim strFilter As String = String.Empty
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilter &= "AND iData.calibratnounkid = '" & CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) & "'"
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            Dim mdtDataTable As DataTable
            mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalDataNew(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, False, Nothing, mintPriority, True, strFilter, "", True, False, "")
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = mdtDataTable
            gvMyReport.DataBind()
            popupMyReport.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRptReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptReset.Click
        Try
            cboRptCalibNo.SelectedValue = 0
            cboRptEmployee.SelectedValue = 0
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = Nothing
            gvMyReport.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExportMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportMyReport.Click
        Try
            If gvMyReport.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "Sorry, there is no data in order to export."), Me)
                Exit Sub
            End If

            Dim strFilter As String = String.Empty
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilter &= "AND iData.calibratnounkid = '" & CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) & "'"
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            Dim mdtDataTable As DataTable
            mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalDataNew(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, False, Nothing, mintPriority, True, strFilter, "", True, False, "")
            Dim objRpt As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objRpt.Export_Calibration_Report(Session("UserId"), _
                                             Session("CompanyUnkId"), _
                                             mdtDataTable, strFilter, _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "Approver Wise Status"), _
                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If objRpt._FileNameAfterExported.Trim <> "" Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.ClearContent()
                response.Clear()
                response.ContentType = "text/plain"
                response.AddHeader("Content-Disposition", "attachment; filename=" + objRpt._FileNameAfterExported + ";")
                response.TransmitFile(IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objRpt._FileNameAfterExported)
                response.Flush()
                response.Buffer = False
                response.End()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseMyReport.Click
        Try
            mblnShowMyReport = False
            popupMyReport.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Protected Sub cboRptPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRptPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objScoreCalibrateApproval.GetComboListCalibrationNo("List", CInt(cboRptPeriod.SelectedValue), True)
            With cboRptCalibNo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridViewEvent "

    Protected Sub gvMyReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMyReport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(gvMyReport.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    Dim intCount As Integer = 1
                    For i = 1 To gvMyReport.Columns.Count - 1
                        If gvMyReport.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(0).ColumnSpan = intCount

                    Dim strCaption As String = ""
                    Dim strUser As String = gvMyReport.DataKeys(e.Row.RowIndex)("calibuser").ToString()
                    Dim strallocation As String = gvMyReport.DataKeys(e.Row.RowIndex)("allocation").ToString()

                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text & " - (" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 13, "Total") & " : " & _
                                 gvMyReport.DataKeys(e.Row.RowIndex)("total").ToString() & ") "

                    If strallocation.Trim <> "" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 19, "By") & " " & strUser & " (" & strallocation & ") "
                    Else
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 19, "By") & " " & strUser
                    End If
                    'S.SANDEEP |04-OCT-2019| -- END

                Else
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " Chart Methods "

    Protected Sub btnChReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChReset.Click
        Try
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDisplayCurve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplayCurve.Click
        Try
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
            mblnShowHPOCurve = True
            popupHPOChart.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnChGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChGenerate.Click
        Try
            If cboChPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboChPeriod.Focus()
                Exit Sub
            End If

            Dim dsDataSet As New DataSet
            Dim objPerformance As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            dsDataSet = objPerformance.GenerateChartData(CInt(cboChPeriod.SelectedValue), _
                                                            Session("Database_Name").ToString, _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            False, 2, "List", , , , , IIf(cbochDisplay.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cbochDisplay.SelectedValue))


            If dsDataSet.Tables.Count > 0 Then
                dgvCalibData.DataSource = dsDataSet.Tables(0)
                dgvCalibData.DataBind()

                If dgvCalibData.Items.Count > 0 Then
                    Dim gvr As DataGridItem = Nothing

                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "N").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Blue
                    End If
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "A").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Tomato
                    End If
                    Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(6))
                    If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                        For Each item In xItems
                            item.Visible = False
                        Next
                    End If
                End If

                chHpoCurve.ChartAreas(0).AxisX.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 100, "Ratings")
                chHpoCurve.ChartAreas(0).AxisY.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "%")
                chHpoCurve.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Format = "{0.##}%"

                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Interval = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffset = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.MajorGrid.Enabled = False
                chHpoCurve.ChartAreas(0).AxisY.Maximum = 105
                chHpoCurve.ChartAreas(0).AxisY.Minimum = 0
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 0

                chHpoCurve.Series(0).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "np")
                chHpoCurve.Series(1).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "ap")

                chHpoCurve.Titles(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 200, "Performance HPO Curve") & vbCrLf & _
                                      cboChPeriod.SelectedItem.Text

                chHpoCurve.Titles(0).Font = New Font(chHpoCurve.Font.Name, 11, FontStyle.Bold)

                chHpoCurve.Series(0).LabelFormat = "{0.##}%"
                chHpoCurve.Series(1).LabelFormat = "{0.##}%"

                chHpoCurve.Series(0).IsVisibleInLegend = False
                chHpoCurve.Series(1).IsVisibleInLegend = False

                chHpoCurve.Series(0).Color = Color.Blue
                chHpoCurve.Series(1).Color = Color.Tomato

                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Normal Distribution")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chHpoCurve.Series(0).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                If dsDataSet.Tables("Chart").Rows.Count > 0 Then
                    legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Actual Performance") & dsDataSet.Tables("Chart").Rows(0)("litem").ToString()
                Else
                    legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Actual Performance")
                End If
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chHpoCurve.Series(1).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem2)

                chHpoCurve.Series(0).ToolTip = chHpoCurve.Legends(0).CustomItems(0).Name & " : is #VAL "
                chHpoCurve.Series(1).ToolTip = chHpoCurve.Legends(0).CustomItems(1).Name & " : is #VAL "

                For i As Integer = 0 To chHpoCurve.Series.Count - 1
                    For Each dp As DataPoint In chHpoCurve.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnChClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChClose.Click
        Try
            mblnShowHPOCurve = False
            popupHPOChart.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCaption.ID, Me.lblCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lnklstInfo.ID, Me.lnklstInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPeriod.ID, Me.lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblApprover.ID, Me.lblApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblLevel.ID, Me.lblLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblRemark.ID, Me.lblRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPScoreFrom.ID, Me.lblPScoreFrom.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPScTo.ID, Me.lblPScTo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCScoreForm.ID, Me.lblCScoreForm.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCScTo.ID, Me.lblCScTo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnShowAll.ID, Me.btnShowAll.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblMyReport.ID, Me.lblMyReport.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblRptPeriod.ID, Me.lblRptPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblRptCalibNo.ID, Me.lblRptCalibNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnRptShow.ID, Me.btnRptShow.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblRptEmployee.ID, Me.lblRptEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnRptReset.ID, Me.btnRptReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnExportMyReport.ID, Me.btnExportMyReport.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(2).FooterText, gvCalibrateList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(0).FooterText, gvMyReport.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(1).FooterText, gvMyReport.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(2).FooterText, gvMyReport.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(3).FooterText, gvMyReport.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(4).FooterText, gvMyReport.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(5).FooterText, gvMyReport.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(6).FooterText, gvMyReport.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvMyReport.Columns(7).FooterText, gvMyReport.Columns(7).HeaderText)

            'Language.setLanguage(mstrModuleName1)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblFilter.ID, Me.lblFilter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblSPeriod.ID, Me.lblSPeriod.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblFilterData.ID, Me.lblFilterData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvFilterRating.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblICalibrationNo.ID, Me.lblICalibrationNo.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblpRatingFrm.ID, Me.lblpRatingFrm.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblpRatingTo.ID, Me.lblpRatingTo.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkFilterData.ID, Me.lnkFilterData.Text)
            'S.SANDEEP |26-AUG-2019| -- END
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCRating.ID, Me.lblCRating.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnIApply.ID, Me.btnIApply.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblApprRemark.ID, Me.lblApprRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnIApprove.ID, Me.btnIApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnIReject.ID, Me.btnIReject.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnIClose.ID, Me.btnIClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnShowAll.ID, Me.btnShowAll.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(6).FooterText, gvApplyCalibration.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(7).FooterText, gvApplyCalibration.Columns(7).HeaderText)
            'S.SANDEEP |26-AUG-2019| -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lnklstInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnklstInfo.ID, Me.lnklstInfo.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblPScoreFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPScoreFrom.ID, Me.lblPScoreFrom.Text)
            Me.lblPScTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPScTo.ID, Me.lblPScTo.Text)
            Me.lblCScoreForm.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCScoreForm.ID, Me.lblCScoreForm.Text)
            Me.lblCScTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCScTo.ID, Me.lblCScTo.Text)
            Me.lblCalibrationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Me.btnShowAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnShowAll.ID, Me.btnShowAll.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text)
            Me.lblMyReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblMyReport.ID, Me.lblMyReport.Text)

            gvCalibrateList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(2).FooterText, gvCalibrateList.Columns(2).HeaderText)
            gvCalibrateList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            gvCalibrateList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            gvCalibrateList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            gvCalibrateList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            gvCalibrateList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            gvCalibrateList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)

            gvMyReport.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(0).FooterText, gvMyReport.Columns(0).HeaderText)
            gvMyReport.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(1).FooterText, gvMyReport.Columns(1).HeaderText)
            gvMyReport.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(2).FooterText, gvMyReport.Columns(2).HeaderText)
            gvMyReport.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(3).FooterText, gvMyReport.Columns(3).HeaderText)
            gvMyReport.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(4).FooterText, gvMyReport.Columns(4).HeaderText)
            gvMyReport.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(5).FooterText, gvMyReport.Columns(5).HeaderText)
            gvMyReport.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(6).FooterText, gvMyReport.Columns(6).HeaderText)
            gvMyReport.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvMyReport.Columns(7).FooterText, gvMyReport.Columns(7).HeaderText)

            'Language.setLanguage(mstrModuleName1)

            Me.lblCalibrateHeading.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            Me.lblFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblFilter.ID, Me.lblFilter.Text)
            Me.lblSPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'Me.lblICalibrationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblICalibrationNo.ID, Me.lblICalibrationNo.Text)
            'Me.lblpRatingFrm.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblpRatingFrm.ID, Me.lblpRatingFrm.Text)
            'Me.lblpRatingTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblpRatingTo.ID, Me.lblpRatingTo.Text)
            'Me.lnkFilterData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkFilterData.ID, Me.lnkFilterData.Text)
            'S.SANDEEP |26-AUG-2019| -- END
            Me.lblCRating.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCRating.ID, Me.lblCRating.Text)
            Me.btnIApply.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnIApply.ID, Me.btnIApply.Text)
            Me.lblApprRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblApprRemark.ID, Me.lblApprRemark.Text)
            Me.btnIApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnIApprove.ID, Me.btnIApprove.Text)
            Me.btnIReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnIReject.ID, Me.btnIReject.Text)
            Me.btnIClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnIClose.ID, Me.btnIClose.Text)
            Me.btnShowAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnShowAll.ID, Me.btnShowAll.Text)

            gvApplyCalibration.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            gvApplyCalibration.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            gvApplyCalibration.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            gvApplyCalibration.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            gvApplyCalibration.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |27-JUL-2019| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage("frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage("frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Sorry, you cannot do approve/reject operation. Reason selected calibration batch is still pending for lower level approver.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Sorry, you cannot do approve/reject operation. Reason the selected calibration batch is already approved or rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 20, "Period :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 21, "Calibration No :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 22, "Employee :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 23, "Approver :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 24, "Approver Wise Status")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 25, "Sorry, there is no data in order to export.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 100, "Ratings")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 101, "%")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 102, "Normal Distribution")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 103, "Actual Performance")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 200, "Performance HPO Curve")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 7, "Sorry, remark is mandatory information. Please enter remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 8, "Information rejected successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 9, "Information approved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 10, "Problem in rejection selected calibration process")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 11, "Sorry, To rating cannot be less than From rating. Please select higher rating to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 12, "Date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 13, "Total")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 14, "Are you sure you want to approve selected score calibration number?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 15, "Are you sure you want to reject selected score calibration number?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 16, "Problem in approving selected calibration process")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 18, "Sorry, calibration rating is mandatory information. Please set calibration rating to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 19, "By")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 100, "Sorry, No approver defined with the selected login.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
