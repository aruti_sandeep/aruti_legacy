﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ReviewerEvaluationList.aspx.vb"
    Inherits="Assessment_New_Performance_Evaluation_wPg_ReviewerEvaluationList" Title="Performance Evaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, args) {
            SetGeidScrolls();
        }
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }

        function HideCollapseAll() {
            //            var table = document.getElementById("<%=dgvGE.ClientID%>");
            //            for (var i = 1, row; row = table.rows[i]; i++) {
            //                //                if ($(row).closest("tr").find("td[Id='IsMgrp']").text() == 'True') {
            //                //                    if ($(val)[0].text == "-") {
            //                //                        $(val)[0].text = "+";
            //                //                    }
            //                //                    else {
            //                //                        $(val)[0].text = "-";
            //                //                    }
            //                //                }
            //                if ($(row).closest("tr").find("td[Id='IsMgrp']").text() == 'False') {
            //                    //                    if (table.rows[i].style.display == 'none') {
            //                    //                        table.rows[i].style.display = '';
            //                    //                    }
            //                    //                    else {
            //                    table.rows[i].style.display = 'none';
            //                    //                    }
            //                }
            //            }
        }
        function HideCollapse(val, data1, data2) {
            //alert($(val)[0].id.endsWith("_lnkcmptPlus"));
            //            if ($(val)[0].id.endsWith("_lnkcmptPlus") == true) {
            //                var table = document.getElementById("<%=dgvGE.ClientID%>");
            //                for (var i = 1, row; row = table.rows[i]; i++) {
            //                    if (data1 == 1) {
            //                        if ($(row).closest("tr").find("td[Id='IsMgrp']").text() == 'True') {
            //                            if ($(val)[0].text == "-") {
            //                                $(val)[0].text = "+";
            //                            }
            //                            else {
            //                                $(val)[0].text = "-";
            //                            }
            //                        }
            //                    }
            //                    if ($(row).closest("tr").find("td[Id='IsMgrp']").text() == 'False') {
            //                        for (var j = 1, col; col = row.cells[j]; j++) {
            //                            if ($(col).attr('Id') != undefined && $(col).attr('Id') == "EmpId") {
            //                                if ($(col).html() == $(val).closest("tr").find("td[Id='EmpId']").text()) {
            //                                    //                                if (show == 1) {
            //                                    //                                    table.rows[i].style.display = 'none';
            //                                    //                                }
            //                                    //                                else {
            //                                    //                                    table.rows[i].style.display = '';
            //                                    //                                }
            //                                    if (data2 == 1) {
            //                                        if (table.rows[i].style.display == 'none') {
            //                                            table.rows[i].style.display = '';
            //                                        }
            //                                        else {
            //                                            table.rows[i].style.display = 'none';
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }            
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Performance Evaluation"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r-10 p-l-0">
                                    <asp:LinkButton ID="lnkAllocation" runat="server" Tootip="Allocation"><i class="fas fa-sliders-h"></i></asp:LinkButton>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblReviewer" runat="server" Text="Reviewer" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReviewer" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblYear" runat="server" Text="Year" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboYear" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblViewType" runat="server" Text="View Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboViewType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:CheckBox ID="chkShowcommitted" runat="server" Text="Show committed" Visible="false" />
                                        <asp:CheckBox ID="chkShowUncommitted" runat="server" Text="Show Uncommitted" Visible="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSummary" runat="server" Text="Show Summary" CssClass="btn btn-default" />
                                <asp:Button ID="btnNew" runat="server" Text="Start New Assessment" CssClass="btn btn-primary"
                                    Visible="False" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search for Existing Assessment" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlAssmtList" runat="server" Width="100%" Height="100%" Visible="false">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="lvAssesmentList" runat="server" AutoGenerateColumns="false" Width="99%"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundColumn DataField="viewmode" HeaderText="Mode" FooterText="" />
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkAssessEmp" runat="server" CommandName="Assess" Text="Assess Employee"
                                                                    Style="text-decoration: none;"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgDelete" runat="server" ToolTip="Delete" CommandName="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgUnlock" runat="server" ToolTip="Unlock" CommandName="Unlock"><i class="fas fa-unlock-alt"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employee" HeaderText="Employee" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="year" HeaderText="Year" FooterText="" />
                                                    <asp:BoundColumn DataField="rl_assessmentdate" HeaderText="Assessment Date" FooterText="" />
                                                    <asp:BoundColumn DataField="sl_score" HeaderText="Self Score" FooterText="" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="al_score" HeaderText="Assessor Score" FooterText="" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="rl_score" HeaderText="Reviewer Score" FooterText="" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="sl_analysisunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="al_analysisunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="rl_analysisunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="periodunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="yearunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="assessgroupunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="assessmodeid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="smodeid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="iscommitted" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="assessormasterunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="assessoremployeeunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="userunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="reviewerunkid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="operationviewid" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="sl_iscommitted" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="al_iscommitted" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="rl_iscommitted" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="sl_committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="al_committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="rl_committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="sl_assessmentdate" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="al_assessmentdate" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:BoundColumn DataField="assessmentdate" HeaderText="" FooterText="" Visible="false" />
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objcolhViewRating">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkDescription" runat="server" ToolTip="View Rating Info" OnClick="lnkInfo_Click"><i class="fa fa-info-circle" style="font-size:19px; color:Blue;"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </asp:Panel>
                <asp:Panel ID="pnlCmptList" runat="server" Width="100%" Height="100%" Visible="false">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                            <div class="table-responsive" style="height: 400px">
                                                <asp:DataGrid ID="dgvGE" runat="server" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" AllowPaging="true" PagerStyle-Mode="NextPrev" PagerStyle-Position="Top"
                                                    PagerStyle-Font-Bold="true" PagerStyle-Font-Size="Medium" PagerStyle-HorizontalAlign="Right"
                                                    PageSize="20" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:TemplateColumn FooterText="objdgcolhHideCollapse" Visible="false">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkcmptPlus" runat="server" Style="text-decoration: none; color: Yellow;
                                                                        font-weight: bold" OnClientClick="HideCollapse(this,1,1); return false;" Text="+"
                                                                        Font-Size="30px"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--0--%>
                                                        <asp:TemplateColumn>
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkcmptAssessEmp" runat="server" CommandName="Assess" Text="Assess Employee"
                                                                        OnClientClick="HideCollapse(this,0,0);" Style="text-decoration: none; font-weight: bold"
                                                                        ForeColor="Yellow"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--1--%>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgCmptDelete" runat="server" OnClientClick="HideCollapse(this,1,0);"
                                                                        Tootip="Delete" CommandName="Delete"><i class="fas fa-trash text-danger" style="color:Yellow"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--2--%>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgCmptUnlock" runat="server" ToolTip="Unlock" OnClientClick="HideCollapse(this,1,0);"
                                                                        CommandName="Unlock"><i class="fas fa-unlock-alt" style="color:Yellow"></i></asp:LinkButton> 
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--3--%>
                                                        <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                            ItemStyle-Width="55%" HeaderStyle-Width="55%" />
                                                        <%--4--%>
                                                        <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                            ItemStyle-Width="10%" HeaderStyle-Width="10%" />
                                                        <%--5--%>
                                                        <asp:BoundColumn DataField="eself" HeaderText="Self -Score" ItemStyle-HorizontalAlign="Right"
                                                            FooterText="dgcolheselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <%--6--%>
                                                        <asp:BoundColumn DataField="" HeaderText="Final Score" FooterText="objdgcolhedisplayGE"
                                                            Visible="false" />
                                                        <%--7--%>
                                                        <asp:BoundColumn DataField="eremark" HeaderText="Self -Remark" FooterText="dgcolheremarkGE"
                                                            ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                        <%--8--%>
                                                        <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" ItemStyle-HorizontalAlign="Right"
                                                            FooterText="dgcolhaselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <%--9--%>
                                                        <asp:BoundColumn DataField="ascore" HeaderText="Final Score" FooterText="objdgcolhadisplayGE"
                                                            Visible="false" />
                                                        <%--10--%>
                                                        <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkGE"
                                                            ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                        <%--11--%>
                                                        <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreGE" />
                                                        <%--12--%>
                                                        <asp:BoundColumn DataField="rself" HeaderText="Reviewer - Score" ItemStyle-HorizontalAlign="Right"
                                                            FooterText="dgcolhrselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <%--13--%>
                                                        <asp:BoundColumn DataField="rremark" HeaderText="Reviewer - Remark" FooterText="dgcolhrremarkGE"
                                                            ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                        <%--14--%>
                                                        <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhscalemasterunkidGE"
                                                            Visible="false" />
                                                        <%--15--%>
                                                        <asp:BoundColumn DataField="competenciesunkid" FooterText="objdgcolhcompetenciesunkidGE"
                                                            Visible="false" />
                                                        <%--16--%>
                                                        <asp:BoundColumn DataField="assessgroupunkid" FooterText="objdgcolhassessgroupunkidGE"
                                                            Visible="false" />
                                                        <%--17--%>
                                                        <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                        <%--18--%>
                                                        <asp:BoundColumn DataField="IsPGrp" FooterText="objdgcolhIsPGrpGE" Visible="false" />
                                                        <%--19--%>
                                                        <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                        <%--20--%>
                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                            HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCol" runat="server" CommandName="viewdescription" Font-Underline="false"
                                                                    Enabled="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--21--%>
                                                        <asp:BoundColumn DataField="" FooterText="objdgcolhMaxScale" Visible="false" />
                                                        <%--22--%>
                                                        <asp:BoundColumn DataField="analysistranunkid" FooterText="objdgcolhanalysistranunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                        <%--23--%>
                                                        <asp:BoundColumn DataField="IsMGrp" FooterText="objdgcolhIsMGrpGE" />
                                                        <%--24--%>
                                                        <asp:BoundColumn DataField="iEmployeeId" FooterText="objdgcolhemployeeunkidGE" />
                                                        <%--25--%>                                                        
                                                        <asp:BoundColumn DataField="sl_analysisunkid" HeaderText="" FooterText="objsl_analysisunkid"
                                                            Visible="false" />
                                                        <%--26--%>
                                                        <asp:BoundColumn DataField="al_analysisunkid" HeaderText="" FooterText="objal_analysisunkid"
                                                            Visible="false" />
                                                        <%--27--%>
                                                        <asp:BoundColumn DataField="rl_analysisunkid" HeaderText="" FooterText="objrl_analysisunkid"
                                                            Visible="false" />
                                                        <%--28--%>                                                        
                                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="" FooterText="objemployeeunkid"
                                                            Visible="false" />
                                                        <%--29--%>
                                                        <asp:BoundColumn DataField="periodunkid" HeaderText="" FooterText="objperiodunkid"
                                                            Visible="false" />
                                                        <%--30--%>
                                                        <asp:BoundColumn DataField="statusunkid" HeaderText="" FooterText="objstatusunkid"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="assessgroupunkid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="assessmodeid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="smodeid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="iscommitted" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="assessormasterunkid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="assessoremployeeunkid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="userunkid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="reviewerunkid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="operationviewid" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="" Visible="false" />
                                                        <asp:BoundColumn DataField="sl_iscommitted" HeaderText="" FooterText="objsl_iscommitted"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="al_iscommitted" HeaderText="" FooterText="objal_iscommitted"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="rl_iscommitted" HeaderText="" FooterText="objrl_iscommitted"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="sl_committeddatetime" HeaderText="" FooterText="objsl_committeddatetime"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="al_committeddatetime" HeaderText="" FooterText="objal_committeddatetime"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="rl_committeddatetime" HeaderText="" FooterText="objrl_committeddatetime"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="sl_assessmentdate" HeaderText="" FooterText="objsl_assessmentdate"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="rl_assessmentdate" HeaderText="" FooterText="objrl_assessmentdate"
                                                            Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_YesNo" runat="server" runat="server" CssClass="card modal-dialog"
                    Style="display: none;" DefaultButton="btnYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMessage" runat="server" Text="Message :" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Style="resize: none"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                        <asp:HiddenField ID="hdf_analysisunkid" runat="server" />
                        <asp:HiddenField ID="hdf_PeriodID" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ComputeYesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btn_ValidNo" PopupControlID="pnl_ComputeValid" TargetControlID="hdf_Valid">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ComputeValid" runat="server" CssClass="card modal-dialog" Style="display: none;"
                    DefaultButton="btn_ValidYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblComputeTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblComputeMessage" runat="server" Text="Message :" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtVoidMessage" runat="server" TextMode="MultiLine" Style="resize: none"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btn_ValidYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btn_ValidNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_Valid" runat="server" />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdf_ItemIndex" runat="server" />
                <asp:HiddenField ID="hdf_CommondName" runat="server" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc3:ConfirmYesNo ID="cnfEditOperation" runat="server" />
                <cc1:ModalPopupExtender ID="popupinfo" BackgroundCssClass="modalBackground" TargetControlID="lblRatingInfo"
                    runat="server" PopupControlID="pnlRatingInfo" CancelControlID="btnCloseRating" />
                <asp:Panel ID="pnlRatingInfo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblRatingInfo" runat="server" Text="Ratings Information"></asp:Label>
                        </h2>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 200px">
                                        <asp:GridView ID="gvRating" runat="server" AutoGenerateColumns="false" Width="99%"
                                            AllowPaging="false" CssClass="table table-hover table-bordered">
                                            <Columns>
                                                <asp:BoundField DataField="scrf" HeaderText="Score From" FooterText="dgcolhScrFrm"
                                                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="scrt" HeaderText="Score To" FooterText="dgcolhScrTo" HeaderStyle-Width="100px"
                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnCloseRating" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupEmpAssessForm" runat="server" CancelControlID="btnEmpReportingClose"
                    PopupControlID="pnlEmpReporting" TargetControlID="HiddenField1" PopupDragHandleControlID="pnlEmpReporting">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="height: 750px; width:90%; left:9%" DefaultButton="btnEmpReportingClose" ScrollBars="Auto">
                    <div class="header">
                        <h2>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <%--<asp:Button ID="btnPlanningPring" runat="server" Text="Print" CssClass="btn btn-primary"
                            OnClientClick="javascript:Print()" />--%>
                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
