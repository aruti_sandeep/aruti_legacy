﻿<%@ Page Title="Custom Item View" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_CustomeItemView.aspx.vb" Inherits="Assessment_New_Common_wPg_CustomeItemView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, args) {
            SetGeidScrolls();
        }
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Custom Item View"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCustomHeader" runat="server" Text="Header" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboHeader" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReport" runat="server" Text="View Report" CssClass="btn btn-primary pull-left" />
                                <% If Session("CompanyGroupName") <> "NMB PLC" Then %>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <% End If%>
                                <asp:Button ID="btnclose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="dgvData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" Width="99%">
                                                <Columns>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                    CancelControlID="hdf_cItem" DropShadow="false" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnl_CItemAddEdit" Drag="false">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCCustomeItem" runat="server" Text="Custome Items"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 360px">
                                    <asp:DataGrid ID="dgv_Citems" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn HeaderText="Custom Items" DataField="custom_item" FooterText="clmItem"
                                                HeaderStyle-Width="450px" ItemStyle-Width="450px"></asp:BoundColumn>
                                            <asp:TemplateColumn FooterText="clmValue" HeaderText="Custom Value" HeaderStyle-Width="250px"
                                                ItemStyle-Width="250px"></asp:TemplateColumn>
                                            <asp:BoundColumn DataField="itemtypeid" Visible="false" FooterText="clmCntType">
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn FooterText="clmValue" Visible="false"></asp:TemplateColumn>
                                            <asp:BoundColumn DataField="customitemunkid" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="selectionmodeid" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="isdefaultentry" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_cItem" runat="server" />
                        <asp:Button ID="btnIAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                        <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <%--<div class="panel-primary">
                            <div class="panel-heading">
                                <div style="display: none">
                                    <uc1:DateCtrl ID="dtpdate" runat="server" />
                                </div>
                            </div>
                            <div class="panel-body">
                                <div id="FilterCriteria" class="panel-default">
                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left;">
                                        </div>
                                    </div>
                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%;">
                                                <td style="width: 10%">
                                                </td>
                                                <td style="width: 30%">
                                                </td>
                                                <td style="width: 2%">
                                                </td>
                                                <td style="width: 10%;">
                                                </td>
                                                <td style="width: 30%">
                                                </td>
                                                <td style="width: 18%">
                                                </td>
                                            </tr>
                                            <tr style="width: 100%;">
                                                <td style="width: 10%">
                                                </td>
                                                <td colspan="4">
                                                </td>
                                                <td style="width: 18%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                        </div>
                                    </div>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvdata" ScrollBars="Auto" Height="400px" Width="100%" runat="server"
                                                    CssClass="gridscroll">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div10" class="panel-default">
                                            <div id="Div11" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div12" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:Panel ID="pnl_dgv_Citems" runat="server" Style="height: 360px !important" ScrollBars="Auto">
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popup_CItemReason" runat="server" BackgroundCssClass="modal-backdrop"
                        CancelControlID="btnNo" PopupControlID="pnl_cItemReason" TargetControlID="hdf_cItemReason">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_cItemReason" runat="server" CssClass="newpopup" Style="display: none;
                        width: 300px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="Title" />
                            </div>
                            <div class="panel-body">
                                <div id="Div13" class="panel-default">
                                    <div id="Div15" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblMessage" runat="server" Text="Message :" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                            <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_cItemReason" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    </div> </div>--%>
                <uc2:DeleteReason ID="popupDelete" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
