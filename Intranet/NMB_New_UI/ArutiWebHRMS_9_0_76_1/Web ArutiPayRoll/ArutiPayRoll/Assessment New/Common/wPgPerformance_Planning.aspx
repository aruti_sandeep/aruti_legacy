﻿<%@ Page Title="Approve/Reject Performance Planning" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgPerformance_Planning.aspx.vb" Inherits="wPgPerformance_Planning" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cofirm" TagPrefix="cnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);
        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });
        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
            SetGeidScrolls();
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    
    </script>

    <%--'S.SANDEEP |04-DEC-2019| -- START--%>
    <%--'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB--%>

    <script type="text/javascript">
        function Print() {
            var printWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
            printWin.document.write(document.getElementById("<%=divhtml.ClientID %>").innerHTML);
            printWin.document.close();
            printWin.focus();
            printWin.print();
            printWin.close();
        }
    </script>

    <%--'S.SANDEEP |04-DEC-2019| -- END--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approve/Reject Performance Planning"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblPeriod" Style="margin-left: 10px" runat="server" Text="Period"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblCurrentStatus" runat="server" CssClass="form-label pull-left"></asp:Label>
                                <asp:Label ID="objlblTotalWeight" runat="server" CssClass="form-label"></asp:Label>
                                <% If Session("CompanyGroupName") <> "NMB PLC" Then%>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <% End If%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <asp:MultiView ID="mvPlanning" runat="server" ActiveViewIndex="0">
                                            <asp:View ID="BSC" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <asp:Label ID="lblBSC" runat="server" Text="Employee Goals" CssClass="form-label"></asp:Label>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 400px">
                                                        <asp:GridView ID="dgvPData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                            CssClass="table table-hover table-bordered" Width="99%">
                                                            <Columns>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="Competency" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <asp:Label ID="lblCmpt" runat="server" Text="Employee Competencies" CssClass="form-label"></asp:Label>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 400px">
                                                        <asp:DataGrid ID="dgvCData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                            CssClass="table table-hover table-bordered" Width="99%">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="ccategory" HeaderText="Competence Category" FooterText="objcolhCGroup"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="competencies" HeaderText="Competencies" FooterText="" />
                                                                <asp:BoundColumn DataField="assigned_weight" HeaderStyle-HorizontalAlign="Right"
                                                                    ItemStyle-HorizontalAlign="Right" HeaderText="Weight" FooterText="" />
                                                                <asp:BoundColumn DataField="isfinal" Visible="false" />
                                                                <asp:BoundColumn DataField="opstatusid" HeaderText="" FooterText="" Visible="false" />
                                                                <asp:BoundColumn DataField="masterunkid" Visible="false" />
                                                                <asp:BoundColumn DataField="competenciesunkid" Visible="false" />
                                                                <asp:BoundColumn DataField="isGrp" Visible="false" />
                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderText="Info." ItemStyle-Width="90px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                            Font-Underline="false"><i class="fa
                fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="CustomItems" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <asp:Label ID="lblCustomItemHeader" runat="server" Text="Custom Items" CssClass="form-label"></asp:Label>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 400px">
                                                        <asp:GridView ID="dgvItems" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                            CssClass="table table-hover table-bordered" Width="99%">
                                                            <Columns>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                    <asp:LinkButton ID="btnBack" runat="server" Text="Back" CssClass="form-label" />
                                                    <asp:LinkButton ID="btnForward" runat="server" Text="Next" CssClass="form-label" />
                                                </div>
                                            </asp:View>
                                        </asp:MultiView>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left;" class="m-t-5">
                                <asp:Button ID="btnPrevious" runat="server" Text="Previous" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary pull-left" />
                                    <asp:Button ID="btnSubmitforApproval" runat="server" Text="Submit for Approval" CssClass="btn btn-primary" />
                                <asp:Button ID="btnApproveSubmitted" runat="server" Text="Approve/Reject Planning"
                                        CssClass="btn btn-primary" />
                                    <asp:Button ID="btnUnlockFinalSave" runat="server" Text="Unlock Final Approved" CssClass="btn btn-primary" />
                                    <asp:LinkButton ID="btnCommitGoals" runat="server" ToolTip="Commit Performance Plans"
                                        CssClass="pull-left" Style="padding-left: 20px;"> <i style="font-size:25px; color:Green;" class="fas fa-bookmark"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnOpenGoals" runat="server" ToolTip="Open Performance Plans"
                                        CssClass="pull-left" Style="padding-left: 20px;"> <i style="font-size:25px;" class="fas fa-book-open"></i>
                                    </asp:LinkButton>
                                    <asp:Button ID="btnViewTemplate" runat="server" Text="View/Print Template" CssClass="btn btn-primary" />
                                <asp:Button ID="btnViewPlanningReport" runat="server" Text="View/Print Planning"
                                        CssClass="btn btn-primary" />
                                    <% If Session("CompanyGroupName") = "NMB PLC" Then%>
                                    <asp:Button ID="lnkApproveRejectUpdateProgress" runat="server" Text="Approve/Reject Update Progress"
                                        CssClass="btn btn-primary" />
                                    <asp:Button ID="lnkAssessorEvaluationList" runat="server" Text="Assess Employee"
                                        CssClass="btn btn-primary" />
                                    <asp:Panel ID="pnlReviewEmployeeAssessment" runat="server" CssClass="pull-left">
                                        <asp:LinkButton ID="lnkReviewerEvaluationList" runat="server" ToolTip="Review Employee List"
                                            CssClass="pull-left" Style="padding-left: 20px;">
                                       <i style="font-size:25px;" class="fas fa-list"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lnkReviewerEvaluation" runat="server" ToolTip="Review Employee"
                                            CssClass="pull-left" Style="padding-left: 20px;">
                                       <i style="font-size:25px;" class="fas fa-street-view"></i>
                                        </asp:LinkButton>
                                    </asp:Panel>
                                    <asp:Button ID="lnkPerformanceResult" runat="server" Text="Performance Result" CssClass="btn btn-primary" />
                                    <% End If%>
                                </div>
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_SubmitApproval" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnSubmitNo" PopupControlID="pnl_SubmitApproval" TargetControlID="hdf_SubmitApproval">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_SubmitApproval" runat="server" CssClass="card modal-dialog" Style="display: none;"
                    DefaultButton="btnSubmitYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblSubmitTitle" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblSubmitMessage" runat="server" Text="Message :" CssClass="form-label" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSubmitYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnSubmitNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_SubmitApproval" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ApproverRejYesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnAppRejNo" PopupControlID="pnl_ApproverRejYesNo" TargetControlID="hdf_ApproverRejYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ApproverRejYesNo" runat="server" CssClass="card modal-dialog"
                    Style="display: none;" DefaultButton="btnAppRejYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblAppRejTitle" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAppRejMessage" runat="server" CssClass="form-label" Text="Message :"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnAppRejYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnAppRejNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ApproverRejYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ApprovrReject" runat="server" TargetControlID="hdf_ApprovrReject"
                    CancelControlID="hdf_ApprovrReject" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_ApprovrReject"
                    BehaviorID="popupApprovr">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ApprovrReject" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblApproveRejectPlanningHeader" runat="server" Text="Approve/Reject Performance Planning"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblComment" runat="server" Text="Comment" CssClass="form-label"></asp:Label>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtComments" CssClass="form-control" TextMode="MultiLine" Rows="4"
                                            runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ApprovrReject" runat="server" />
                        <asp:Button ID="btnApRejFinalSave" runat="server" Text="Approve" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOpen_Changes" runat="server" Text="Reject" CssClass="btn btn-primary" />
                        <asp:Button ID="btnApRejClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ApprovrFinalYesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnFinalNo" PopupControlID="pnl_ApprovrFinalYesNo" TargetControlID="hdf_ApprvoerFinalYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ApprovrFinalYesNo" runat="server" CssClass="card modal-dialog"
                    Style="display: none;" DefaultButton="btnAppRejYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblFinalTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblFinalMessage" runat="server" CssClass="form-label" Text="Message :"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnFinalYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOpenYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnFinalNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ApprvoerFinalYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_UnclokFinalSave" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnUnlockNo" PopupControlID="pnl_UnlocakFinalSave" TargetControlID="hdf_btnUnlockYesNO">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_UnlocakFinalSave" runat="server" CssClass="card modal-dialog"
                    Style="display: none;" DefaultButton="btnUnlockYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblUnlockTitel" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblUnlockMessage" runat="server" Text="Message :" CssClass="form-label" />
                                <asp:Panel ID="pnlReason" runat="server">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtUnlockReason" CssClass="form-control" runat="server" TextMode="MultiLine" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnUnlockYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnUnlockNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_btnUnlockYesNO" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_VoidUpdateProgress" runat="server" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnl_VoidUpdateProgress" TargetControlID="hdf_btnVUPYesNO">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_VoidUpdateProgress" runat="server" CssClass="card modal-dialog"
                    Style="display: none;" DefaultButton="btnVUPYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblVoidUpdateProgress" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblVoidUpdateProgressMsg" runat="server" CssClass="form-label" Text="Message :"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnVUPYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnVUPNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_btnVUPYesNO" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupCnf" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="" PopupControlID="pnl_popupcnf" TargetControlID="hdf_popupcnf">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_popupcnf" runat="server" CssClass="card modal-dialog" Style="display: none;"
                    DefaultButton="btncnfYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblcnfTitle" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblcnfTMessage" runat="server" CssClass="form-label" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btncnfYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btncnfNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_popupcnf" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ComInfo" runat="server" BackgroundCssClass="ModalPopupBG2"
                    CancelControlID="btnSClose" PopupControlID="pnl_CompInfo" TargetControlID="hdnfieldDelReason">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CompInfo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblTitle" runat="server" Text="Description" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtData" runat="server" CssClass="form-control" TextMode="MultiLine"
                                            ReadOnly="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupEmpAssessForm" runat="server" CancelControlID="btnEmpReportingClose"
                    PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2" PopupDragHandleControlID="pnlEmpReporting">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="height: 600px" DefaultButton="btnEmpReportingClose" ScrollBars="Auto">
                    <div class="header">
                        <h2>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnPlanningPring" runat="server" Text="Print" CssClass="btn btn-primary"
                            OnClientClick="javascript:Print()" />
                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ApproveReject_Objectives" runat="server" TargetControlID="hdf_ApproveReject_Objectives"
                    CancelControlID="hdf_ApproveReject_Objectives" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnl_ApproveReject_Objectives">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ApproveReject_Objectives" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblApproveRejectObjectives" runat="server" Text="Approve/Reject Performance Objectives"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="max-height: 400px">
                                    <%--<asp:GridView ID="dgvObjectiveData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                        CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="empfield1unkid">
                                        <Columns>
                                            <asp:BoundField DataField="" HeaderText="" FooterText="objdgcolhfname"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rdbApprove" runat="server" Text="Approve" GroupName="1" />
                                                    <asp:RadioButton ID="rdbReject" runat="server" Text="Reject" GroupName="1" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>--%>
                                    <asp:DataList ID="rptObjectiveData" runat="server" Width="100%">
                                        <ItemTemplate>
                                            <div class="card inner-card m-b-10">
                                                <div class="body">
                                                    <div class="row clearfix d--f ai--c">
                                                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblField" runat="server" Text='<%# Eval("Field") %>' CssClass="form-label"></asp:Label>
                                                            <asp:HiddenField ID="hfEmpFieldunkid" runat="server" Value='<%# Eval("EmpFieldunkid") %>' />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                            <%--<div class="row clearfix">
                                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                                    
                                                                </div>
                                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="float:left;">
                                                                    
                                                                </div>
                                                            </div>--%>
                                                            <asp:Label ID="lblWeight" runat="server" Text="Weight :" CssClass="form-label"></asp:Label>
                                                                    <asp:HiddenField ID="hfWeight" runat="server" Value='<%# Eval("Weight") %>' />
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:HiddenField ID="hfGoalStatusTypeId" runat="server" Value='<%# Eval("GoalStatusTypeId") %>' />
                                                            <asp:RadioButton ID="rdbApprove" runat="server" Text="Approve" GroupName="1" />
                                                            <asp:RadioButton ID="rdbReject" runat="server" Text="Reject" GroupName="1" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_ApproveReject_Objectives" runat="server" />
                        <asp:Button ID="btnApRejObjectivesSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnApRejObjectivesClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <%--OLD--%>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default" style="display: none">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                <table style="width: 100%">
                                    <tr style="width: 100%;">
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 30%;">
                                            
                                        </td>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 30%;">
                                            
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <div style="float: left;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Div17" class="panel-default" style="padding: 0px">
                            <div id="Div19" class="panel-body-default">
                                <div id="btnfixedbottom" class="btn-default">
                                    <div style="float: left">
                                        <asp:Panel ID="btnNavigation" runat="server">
                                            </asp:Panel>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <table style="width: 100%">
                    <tr style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr id="cnfCommitGoals" style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                    <tr id="Tr1" style="width: 100%">
                        <td style="width: 100%">
                        </td>
                    </tr>
                </table>--%>
                <cnf:Cofirm ID="cnfGoalsCommit" runat="server" />
                <cnf:Cofirm ID="cnfGoalsOpen" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
