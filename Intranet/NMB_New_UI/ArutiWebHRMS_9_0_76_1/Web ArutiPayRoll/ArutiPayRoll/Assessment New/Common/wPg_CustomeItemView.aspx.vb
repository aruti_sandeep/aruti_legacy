﻿#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
#End Region

Partial Class Assessment_New_Common_wPg_CustomeItemView
    Inherits Basepage

#Region "Private Variables"
    Dim DisplayMessage As New CommonCodes

    'Shani(24-Dec-2015) -- Start
    'Enhancement - CCBRT WORK
    Private mblnIsAddColumn As Boolean = False
    Private mdtCustomTabularGrid As DataTable = Nothing
    Private mintItemRowIndex As Integer = 0
    Private mstriEditingGUID As String = ""
    Private mblnItemAddEdit As Boolean = False
    Private mdtCustomEvaluation As New DataTable
    Private dtCItems As DataTable = Nothing
    'Private mblnSerchRecord As Boolean = False
    Private ReadOnly mstrModuleName As String = "frmCustomItemView"

    'Shani(24-Dec-2015) -- End

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mblnIsmatchWithComp As Boolean = False
    Private mblnIsIncludePlaning As Boolean = False
    'Shani (26-Sep-2016) -- End

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call Fill_Combo()
                'Dim dt As New DataTable
                'Me.ViewState.Add("customheaderunkid", Session("customheaderunkid"))
                ''Shani(24-Dec-2015) -- Start
                ''Enhancement - CCBRT WORK

                'Dim objCustomHeader As New clsassess_custom_header
                'objCustomHeader._Customheaderunkid = CInt(Session("customheaderunkid"))
                'mblnIsmatchWithComp = objCustomHeader._IsMatch_With_Competency
                'lblPageHeader.Text = objCustomHeader._Name
                ''Me.Title = objCustomHeader._Name
                ''Shani (26-Sep-2016) -- Start
                ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'mblnIsIncludePlaning = objCustomHeader._IsInclude_Planning


                'Blank_ModuleName()
                'clsCommonATLog._WebFormName = "frmCustomItemView"
                'StrModuleName1 = "mnuHumanResource"
                'StrModuleName2 = "mnuAssessment"
                'StrModuleName3 = "mnuSetups"
                'clsCommonATLog._WebClientIP = Session("IP_ADD")
                'clsCommonATLog._WebHostName = Session("HOST_NAME")

                ''Shani (26-Sep-2016) -- End
                'objCustomHeader = Nothing

            Else
                mblnIsAddColumn = Me.ViewState("mblnIsAddColumn")
                mdtCustomTabularGrid = Me.ViewState("mdtCustomTabularGrid")
                mintItemRowIndex = Me.ViewState("mintItemRowIndex")
                mstriEditingGUID = Me.ViewState("mstriEditingGUID")
                mblnItemAddEdit = Me.ViewState("mblnItemAddEdit")
                mdtCustomEvaluation = Me.ViewState("mdtCustomEvaluation")
                mblnIsmatchWithComp = Me.ViewState("mblnIsmatchWithComp")
                dtCItems = Me.ViewState("dtCItems")
                mblnIsIncludePlaning = Me.ViewState("mblnIsIncludePlaning")

                dgvData.DataSource = mdtCustomTabularGrid
                dgvData.DataBind()

                If mblnItemAddEdit Then
                    dgv_Citems.DataSource = dtCItems
                    dgv_Citems.DataBind()
                    popup_CItemAddEdit.Show()
                End If
                'Shani(24-Dec-2015) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1 : " & ex.Message, Me)
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (01 Feb 2020) -- End
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Shani(24-Dec-2015) -- Start
    'Enhancement - CCBRT WORK
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mblnIsAddColumn") = mblnIsAddColumn
            Me.ViewState("mdtCustomTabularGrid") = mdtCustomTabularGrid
            Me.ViewState("mintItemRowIndex") = mintItemRowIndex
            Me.ViewState("mstriEditingGUID") = mstriEditingGUID
            Me.ViewState("mblnItemAddEdit") = mblnItemAddEdit
            Me.ViewState("mdtCustomEvaluation") = mdtCustomEvaluation
            Me.ViewState("mblnIsmatchWithComp") = mblnIsmatchWithComp
            Me.ViewState("dtCItems") = dtCItems
            Me.ViewState("mblnIsIncludePlaning") = mblnIsIncludePlaning
        Catch ex As Exception
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (01 Feb 2020) -- End
        End Try
    End Sub
    'Shani(24-Dec-2015) -- End

#End Region

#Region "Private Methods"
    Private Sub Fill_Combo()
        Try
            Dim objPriod As New clscommom_period_Tran
            Dim dsList As New DataSet

            'S.SANDEEP [17 NOV 2015] -- START
            'dsList = objPriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))
            dsList = objPriod.getListForCombo(enModuleReference.Assessment, 0, _
                                              Session("Database_Name"), _
                                              Session("fin_startdate"), _
                                              "List", True, 1)
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END


            'S.SANDEEP [29 DEC 2015] -- START


            'Shani (09-May-2016) -- Start
            'Integer.TryParse(New DataView(dsList.Tables(0), "end_date <= " & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime), "end_date DESC", DataViewRowState.CurrentRows).ToTable.Rows(0)("periodunkid").ToString, intPeriodUnkID)
            'Dim intPeriodUnkID As Integer = 0
            Dim intPeriodUnkID As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            'S.SANDEEP [29 DEC 2015] -- END


            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                'S.SANDEEP [29 DEC 2015] -- START
                .SelectedValue = intPeriodUnkID
                'S.SANDEEP [29 DEC 2015] -- END
            End With

            dsList = Nothing
            Dim objEmp As New clsEmployee_Master

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    dsList = objEmp.GetEmployeeList("Emp", False, True, Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            'S.SANDEEP [10 DEC 2015] -- START
            Dim blnApplyUACFilter As Boolean = False 'Shani(14-APR-2016) --[True]
            'S.SANDEEP [10 DEC 2015] -- END

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                'S.SANDEEP [10 DEC 2015] -- START
                blnApplyUACFilter = False
                'S.SANDEEP [10 DEC 2015] -- END
            End If

            'S.SANDEEP [04 Jan 2016] -- START
            Dim strFilterQry As String = String.Empty
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Shani(14-APR-2016) -- Start
                'If Session("SkipApprovalFlowInPlanning") = True Then
                'Shani(14-APR-2016) -- End 
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master

                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                If dsMapEmp.Tables("List").Rows.Count > 0 Then

                    dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                    strFilterQry = " hremployee_master.employeeunkid IN "
                    csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                    'Shani(14-APR-2016) -- Start
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                    'Shani(14-APR-2016) -- End 
                End If

                'Shani(14-APR-2016) -- Start
                'End If
                'Shani(14-APR-2016) -- End
            End If
            'S.SANDEEP [04 Jan 2016] -- END

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyUACFilter) 'S.SANDEEP [10 DEC 2015] -- START {blnApplyUACFilter} -- END
            'S.SANDEEP [04 Jan 2016] -- START {strFilterQry} -- END

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End

                .DataSource = dsList.Tables(0)
                .DataBind()

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If (Session("LoginBy") = Global.User.en_loginby.User) Then
                '    .SelectedValue = 0
                'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                '    .SelectedValue = Session("Employeeunkid")
                'End If
                .SelectedValue = intEmpId
                'Shani(20-Nov-2015) -- End
            End With
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboPeriod_SelectedIndexChanged(New Object(), New EventArgs())
            'S.SANDEEP |12-FEB-2019| -- END


            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
            Dim objCHeader As New clsassess_custom_header
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                dsList = objCHeader.Get_Headers_For_Menu(False, True)
            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                dsList = objCHeader.Get_Headers_For_Menu(True, False)
            End If
            Dim dr As DataRow = dsList.Tables(0).NewRow()
            dr.Item("customheaderunkid") = 0
            dr.Item("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsassess_custom_header", 8, "Select Header")
            dsList.Tables(0).Rows.InsertAt(dr, 0)
            With cboHeader
                .DataValueField = "customheaderunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objCHeader = Nothing
            'S.SANDEEP [01-OCT-2018] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Combo : " & ex.Message, Me)
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (01 Feb 2020) -- End
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_Planing_Grid()
        Try

            '************* ADD
            Dim iTempField As New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "ObjAdd"
            iTempField.HeaderText = "Add"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvData.Columns.Add(iTempField)

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objEdit"
            iTempField.HeaderText = "Edit"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvData.Columns.Add(iTempField)

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objDelete"
            iTempField.HeaderText = "Delete"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvData.Columns.Add(iTempField)
        Catch ex As Exception
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (01 Feb 2020) -- End
        End Try
    End Sub

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

    'Private Sub Fill_Gird()
    '    Dim objEAnalysisMst As New clsevaluation_analysis_master
    '    Dim objPlaningCutomItem As New clsassess_plan_customitem_tran
    '    Try
    '        Dim dsItems As New DataSet : Dim objCusItem As New clsassess_custom_items
    '        dsItems = objCusItem.getComboList(cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), False, "List")
    '        If dsItems.Tables(0).Rows.Count <= 0 Then
    '            DisplayMessage.DisplayMessage("Sorry, no custom items define for the selected period.", Me)
    '            dsItems.Dispose() : objCusItem = Nothing
    '            Exit Sub
    '        End If
    '        If dsItems IsNot Nothing Then dsItems.Dispose()
    '        If objCusItem IsNot Nothing Then objCusItem = Nothing

    '        If Check_IsAssesst() = False Then
    '            mdtCustomTabularGrid = objPlaningCutomItem.GetList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Me.ViewState("customheaderunkid"), "List", CBool(Session("IsCompanyNeedReviewer")))
    '        Else
    '            mdtCustomTabularGrid = objEAnalysisMst.GetCustom_Items_Data(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), Me.ViewState("customheaderunkid"))
    '        End If

    '        If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
    '            dgvData.Columns.Clear()
    '            dgvData.AutoGenerateColumns = False
    '            Dim iColName As String = String.Empty
    '            If mblnSerchRecord = True AndAlso Check_IsAssesst() = False Then
    '                If mdtCustomTabularGrid.AsEnumerable().Count <= 0 Then
    '                    mdtCustomTabularGrid.Rows.Add(mdtCustomTabularGrid.NewRow())
    '                End If
    '                Call Fill_Planing_Grid()
    '                mblnIsAddColumn = True
    '            Else
    '                If mblnIsAddColumn = True Then
    '                    Call Fill_Planing_Grid()
    '                End If
    '            End If

    '            For Each dCol As DataColumn In mdtCustomTabularGrid.Columns
    '                iColName = "" : iColName = "obj" & dCol.ColumnName
    '                Dim dgvCol As New BoundField()
    '                dgvCol.FooterText = iColName
    '                dgvCol.ReadOnly = True
    '                dgvCol.DataField = dCol.ColumnName
    '                dgvCol.HeaderText = dCol.Caption
    '                If dgvData.Columns.Contains(dgvCol) = True Then Continue For
    '                If dgvCol.HeaderText.Length > 0 Then
    '                    dgvData.Columns.Add(dgvCol)
    '                Else
    '                    dgvCol = Nothing
    '                End If

    '                'If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
    '            Next
    '            dgvData.DataSource = mdtCustomTabularGrid
    '            dgvData.DataKeyNames = New String() {"GUID"}
    '            dgvData.DataBind()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Fill_Gird" & ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub Fill_Gird()
        Dim dsItems As New DataSet
        'Dim dtCustomTabularGrid As DataTable = Nothing
        Try

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            If CInt(IIf(cboHeader.SelectedValue = "", 0, cboHeader.SelectedValue)) <= 0 Then Exit Sub
            'S.SANDEEP |25-OCT-2019| -- END

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}

            Dim dt As New DataTable
            If Me.ViewState("customheaderunkid") Is Nothing Then
                Me.ViewState.Add("customheaderunkid", CInt(IIf(cboHeader.SelectedValue = "", 0, cboHeader.SelectedValue)))
            Else
                Me.ViewState("customheaderunkid") = CInt(IIf(cboHeader.SelectedValue = "", 0, cboHeader.SelectedValue))
            End If
            Dim objCustomHeader As New clsassess_custom_header
            objCustomHeader._Customheaderunkid = CInt(Me.ViewState("customheaderunkid"))
            mblnIsmatchWithComp = objCustomHeader._IsMatch_With_Competency
            mblnIsIncludePlaning = objCustomHeader._IsInclude_Planning


            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmCustomItemView"
            StrModuleName1 = "mnuHumanResource"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuSetups"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            objCustomHeader = Nothing
            'S.SANDEEP [01-OCT-2018] -- END


            dsItems = (New clsassess_custom_items).getComboList(cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), False, "List")
            If dsItems.Tables(0).Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, no custom items define for the selected period.", Me)
                dsItems.Dispose()
                Exit Sub
            End If
            Dim dCol1 As DataColumn : mdtCustomTabularGrid = New DataTable
            For Each drow As DataRow In dsItems.Tables(0).Select("", "isdefaultentry DESC")
                dCol1 = New DataColumn
                With dCol1
                    .ColumnName = "CItem_" & drow.Item("Id").ToString
                    .Caption = drow.Item("Name").ToString
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = ""
                End With
                mdtCustomTabularGrid.Columns.Add(dCol1)

                dCol1 = New DataColumn
                With dCol1
                    .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                    .ExtendedProperties.Add("IsSystemGen", drow.Item("isdefaultentry"))
                End With
                mdtCustomTabularGrid.Columns.Add(dCol1)
            Next

            dCol1 = New DataColumn
            With dCol1
                .ColumnName = "GUID"
                .Caption = ""
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtCustomTabularGrid.Columns.Add(dCol1)

            dCol1 = New DataColumn
            With dCol1
                .ColumnName = "IsManual"
                .Caption = ""
                .DataType = GetType(System.Boolean)
                .DefaultValue = True
            End With
            mdtCustomTabularGrid.Columns.Add(dCol1)

            If dsItems IsNot Nothing Then dsItems.Dispose()

            mdtCustomEvaluation = (New clsassess_plan_customitem_tran).GetCustomeDataHeaderWise(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("customheaderunkid")), CBool(Session("IsCompanyNeedReviewer")))
            Dim dFRow As DataRow = Nothing
            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                Dim strGuidArray() As String = mdtCustomEvaluation.AsEnumerable().Select(Function(x) x.Field(Of String)("plancustomguid")).Distinct().ToArray
                For Each StrGId As String In strGuidArray
                    dFRow = mdtCustomTabularGrid.NewRow
                    mdtCustomTabularGrid.Rows.Add(dFRow)
                    For Each dRow As DataRow In mdtCustomEvaluation.Select("plancustomguid = '" & StrGId & "'")
                        Select Case CInt(dRow.Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                            Case clsassess_custom_items.enCustomType.SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    Select Case CInt(dRow.Item("selectionmodeid"))
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            Dim objCMaster As New clsassess_competencies_master
                                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            Dim objEmpField1 As New clsassess_empfield1_master
                                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                            objEmpField1 = Nothing

                                            'S.SANDEEP |16-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                            'S.SANDEEP |16-AUG-2019| -- END

                                    End Select
                                End If
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dRow.Item("custom_value")) Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                    End If
                                End If
                        End Select
                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                        dFRow.Item("GUID") = dRow.Item("plancustomguid")
                        If IsDBNull(dRow.Item("transactiondate")) Then
                            dRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                        End If
                        dFRow.Item("IsManual") = dRow.Item("ismanual")
                    Next
                Next
                mdtCustomEvaluation.AcceptChanges()
            End If
            If mdtCustomTabularGrid.Rows.Count <= 0 Then
                mdtCustomTabularGrid.Rows.Add(mdtCustomTabularGrid.NewRow)
            End If
            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
                dgvData.Columns.Clear()
                dgvData.AutoGenerateColumns = False
                Dim iColName As String = String.Empty
                If mblnIsIncludePlaning = True AndAlso Check_IsAssesst() = False Then
                    If mdtCustomTabularGrid.AsEnumerable().Count <= 0 Then
                        mdtCustomTabularGrid.Rows.Add(mdtCustomTabularGrid.NewRow())
                    End If
                    Call Fill_Planing_Grid()
                    mblnIsAddColumn = True
                Else
                    If mblnIsAddColumn = True Then
                        Call Fill_Planing_Grid()
                    End If
                End If

                For Each dCol As DataColumn In mdtCustomTabularGrid.Columns
                    iColName = "" : iColName = "obj" & dCol.ColumnName
                    Dim dgvCol As New BoundField()
                    dgvCol.FooterText = iColName
                    dgvCol.ReadOnly = True
                    dgvCol.DataField = dCol.ColumnName
                    dgvCol.HeaderText = dCol.Caption
                    If dgvData.Columns.Contains(dgvCol) = True Then Continue For
                    If dgvCol.HeaderText.Length > 0 Then
                        dgvData.Columns.Add(dgvCol)
                    Else
                        dgvCol = Nothing
                    End If
                Next
                dgvData.DataSource = mdtCustomTabularGrid
                dgvData.DataKeyNames = New String() {"GUID"}
                dgvData.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Gird" & ex.Message, Me)
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (01 Feb 2020) -- End
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani (26-Sep-2016) -- End

    'Shani(24-Dec-2015) -- Start
    'Enhancement - CCBRT WORK


    'S.SANDEEP [12 OCT 2016] -- START
    'ENHANCEMENT : ACB REPORT CHANGES




    'Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
    '    Try
    '        dtCItems = (New clsassess_plan_customitem_tran).Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid)

    '        If mintItemRowIndex > -1 AndAlso mstriEditingGUID <> "" Then 'mdtCustomEvaluation

    '            Dim dsItems As New DataSet
    '            Dim objPlanning As New clsassess_plan_customitem_tran
    '            dsItems = objPlanning.GetList(Session("Database_Name"), _
    '                                          Session("UserId"), _
    '                                          Session("Fin_year"), _
    '                                          Session("CompanyUnkId"), _
    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                          Session("UserAccessModeSetting"), True, _
    '                                          Session("IsIncludeInactiveEmp"), "Cus_Items", _
    '                                          cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), "plancustomguid = '" & mstriEditingGUID & "'")

    '            objPlanning = Nothing
    '            If dsItems.Tables(0).Rows.Count > 0 Then
    '                For Each rows As DataRow In dsItems.Tables(0).Rows
    '                    Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & rows.Item("customitemunkid") & "'")
    '                    If xRow.Length > 0 Then
    '                        Select Case CInt(rows.Item("itemtypeid"))
    '                            Case clsassess_custom_items.enCustomType.SELECTION
    '                                Select Case CInt(rows.Item("selectionmodeid"))
    '                                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
    '                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
    '                                        Dim objCMaster As New clsCommon_Master
    '                                        objCMaster._Masterunkid = rows.Item("custom_value")
    '                                        'rows.Item("display_value") = objCMaster._Name
    '                                        objCMaster = Nothing
    '                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
    '                                        Dim objCompetency As New clsassess_competencies_master
    '                                        objCompetency._Competenciesunkid = rows.Item("custom_value")
    '                                        'rows.Item("display_value") = objCompetency._Name
    '                                        objCompetency = Nothing
    '                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
    '                                        Dim objEmpField1 As New clsassess_empfield1_master
    '                                        objEmpField1._Empfield1unkid = rows.Item("custom_value")
    '                                        'rows.Item("display_value") = objEmpField1._Field_Data
    '                                        objEmpField1 = Nothing
    '                                End Select
    '                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
    '                                If rows.Item("custom_value").ToString.Trim.Length > 0 Then
    '                                    xRow(0).Item("ddate") = eZeeDate.convertDate(rows.Item("custom_value"))
    '                                    rows.Item("display_value") = xRow(0).Item("ddate")
    '                                End If
    '                        End Select
    '                        If rows.Item("display_value").ToString.Trim.Length > 0 Then
    '                            xRow(0).Item("custom_value") = rows.Item("display_value")
    '                        Else
    '                            xRow(0).Item("custom_value") = rows.Item("custom_value")
    '                        End If
    '                    End If
    '                Next
    '            Else

    '            End If

    '            'Dim dtmp() As DataRow = mdtCustomTabularGrid.Select("GUID = '" & mstriEditingGUID & "' AND AUD <> 'D'")
    '            'If dtmp.Length > 0 Then
    '            '    For iEdit As Integer = 0 To dtmp.Length - 1
    '            '        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
    '            '        If xRow.Length > 0 Then
    '            '            Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
    '            '                Case clsassess_custom_items.enCustomType.SELECTION
    '            '                    Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
    '            '                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
    '            '                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
    '            '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '            '                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
    '            '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '            '                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
    '            '                    End Select
    '            '                Case clsassess_custom_items.enCustomType.DATE_SELECTION
    '            '                    If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
    '            '                        xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
    '            '                    End If
    '            '            End Select
    '            '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
    '            '        End If
    '            '    Next
    '            'End If
    '        End If

    '        dgv_Citems.AutoGenerateColumns = False
    '        Dim objCHeader As New clsassess_custom_header
    '        objCHeader._Customheaderunkid = xHeaderUnkid
    '        If Me.ViewState("Header") IsNot Nothing Then
    '            Me.ViewState("Header") = objCHeader._Name
    '        Else
    '            Me.ViewState.Add("Header", objCHeader._Name)
    '        End If
    '        objCHeader = Nothing
    '        dgv_Citems.DataSource = dtCItems
    '        dgv_Citems.DataBind()
    '        mblnItemAddEdit = True
    '        popup_CItemAddEdit.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
        Try
            dtCItems = (New clsassess_plan_customitem_tran).Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid)
            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
            If mstriEditingGUID <> "" Then
                Dim strFilter As String = "plancustomguid = '" & mstriEditingGUID & "' AND AUD <> 'D' "

                If mintItemRowIndex <= -1 Then
                    strFilter &= " AND isdefaultentry = TRUE"
                End If

                For Each rows As DataRow In mdtCustomEvaluation.Select(strFilter)
                    Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & rows.Item("customitemunkid") & "'")
                    If xRow.Length > 0 Then
                        Select Case CInt(rows.Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.SELECTION
                                Select Case CInt(rows.Item("selectionmodeid"))
                                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                        xRow(0).Item("selectedid") = rows.Item("custom_value")

                                        'S.SANDEEP |16-AUG-2019| -- START
                                        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
                                        'S.SANDEEP |16-AUG-2019| -- END
                                End Select
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If rows.Item("custom_value").ToString.Trim.Length > 0 Then
                                    xRow(0).Item("ddate") = eZeeDate.convertDate(rows.Item("custom_value"))
                                    rows.Item("display_value") = xRow(0).Item("ddate")
                                End If
                        End Select
                        If rows.Item("display_value").ToString.Trim.Length > 0 Then
                            xRow(0).Item("custom_value") = rows.Item("display_value")
                        Else
                            xRow(0).Item("custom_value") = rows.Item("custom_value")
                        End If
                    End If
                Next
            End If
            dgv_Citems.AutoGenerateColumns = False
            Dim objCHeader As New clsassess_custom_header
            objCHeader._Customheaderunkid = xHeaderUnkid
            If Me.ViewState("Header") IsNot Nothing Then
                Me.ViewState("Header") = objCHeader._Name
            Else
                Me.ViewState.Add("Header", objCHeader._Name)
            End If

            objCHeader = Nothing
            dgv_Citems.DataSource = dtCItems
            dgv_Citems.DataBind()
            mblnItemAddEdit = True
            popup_CItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [12 OCT 2016] -- END
    Private Function Check_IsAssesst() As Boolean
        Dim bln As Boolean = False
        Dim objAnalysis_Master As New clsevaluation_analysis_master
        Try
            'S.SANDEEP |22-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
            'If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
            '    bln = True
            '    Return bln
            'End If

            'If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
            '    bln = True
            '    Return bln
            'End If

            'If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
            '    bln = True
            '    Return bln
            'End If

            If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                bln = True
                Return bln
            End If

            If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                bln = True
                Return bln
            End If

            If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                bln = True
                Return bln
            End If
            'S.SANDEEP |22-JUL-2019| -- END


            Dim intStatusTranId As Integer = 0
            Dim intStatusId As Integer = (New clsassess_empstatus_tran).Get_Last_StatusId(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), intStatusTranId)
            If intStatusId = enObjective_Status.FINAL_SAVE OrElse intStatusId = enObjective_Status.SUBMIT_APPROVAL Then
                bln = True
                Return bln
            End If
            Return bln
        Catch ex As Exception
            Throw ex
        Finally
            If objAnalysis_Master IsNot Nothing Then objAnalysis_Master = Nothing
        End Try
    End Function
    'Shani(24-Dec-2015) -- End

#End Region

#Region "Button Event"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'S.SANDEEP [29 DEC 2015] -- START
            'If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Please set following information [Employee,Period] to view assessment", Me)
            '    Exit Sub
            'End If

            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
               CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    DisplayMessage.DisplayMessage("Period is mandatory information. Please select Period to continue.", Me)  '--ADDED NEW LANAGUAGE.
                Else
                    DisplayMessage.DisplayMessage("Please set following information [Employee,Period] to view assessment", Me)
                    Exit Sub
                End If
                'Pinkal (15-Mar-2019) -- End
            End If
            'S.SANDEEP [29 DEC 2015] -- END


            'Shani(24-Dec-2015) -- Start
            'Enhancement - CCBRT WORK
            mblnIsAddColumn = False
            'Shani(24-Dec-2015) -- End

            Fill_Gird()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = 0
            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                cboEmployee.SelectedValue = Session("Employeeunkid")
            End If
            cboPeriod.SelectedValue = 0
            dgvData.DataSource = Nothing
            dgvData.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~/Userhome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Closebotton1_CloseButton_click :" & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani(24-Dec-2015) -- Start
    'Enhancement - CCBRT WORK
    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim xRow As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            mintItemRowIndex = -1
            If mblnIsmatchWithComp Then
                mstriEditingGUID = mdtCustomTabularGrid.Rows(xRow.RowIndex).Item("GUID")
            Else
                mstriEditingGUID = ""
            End If

            'Shani(23-FEB-2017) -- Start
            btnIAdd.Text = "Add"
            'Shani(23-FEB-2017) -- End

            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
                Dim xtmp() As DataRow = mdtCustomTabularGrid.Select("GUID <> ''")
                Dim blnAllowMultiple As Boolean = False
                Dim objCHeader As New clsassess_custom_header
                objCHeader._Customheaderunkid = Me.ViewState("customheaderunkid")
                blnAllowMultiple = objCHeader._Is_Allow_Multiple
                objCHeader = Nothing

                Dim objPlanning As New clsassess_plan_customitem_tran
                Dim strMsg As String = String.Empty
                strMsg = objPlanning.IsFinalSaved(mstriEditingGUID, cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), 1)
                objPlanning = Nothing
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    Exit Sub
                End If

                If xtmp.Length > 0 AndAlso blnAllowMultiple = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
                    Exit Sub
                End If
            End If
            Call Generate_Popup_Data(Me.ViewState("customheaderunkid"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            mintItemRowIndex = row.RowIndex
            mstriEditingGUID = mdtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")

            If mstriEditingGUID.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, no custom item added in the list to edit.", Me)
            End If

            Dim objPlanning As New clsassess_plan_customitem_tran
            Dim strMsg As String = String.Empty
            strMsg = objPlanning.IsFinalSaved(mstriEditingGUID, cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), 2)
            objPlanning = Nothing
            If strMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(strMsg, Me)
                Exit Sub
            End If
            'Shani(23-FEB-2017) -- Start
            btnIAdd.Text = "Save"
            'Shani(23-FEB-2017) -- End
            Call Generate_Popup_Data(Me.ViewState("customheaderunkid"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            mintItemRowIndex = row.RowIndex
            mstriEditingGUID = mdtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")

            If mstriEditingGUID.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, no custom item added in the list to delete.", Me)
            End If

            Dim objPlanning As New clsassess_plan_customitem_tran
            Dim strMsg As String = String.Empty
            strMsg = objPlanning.IsFinalSaved(mstriEditingGUID, cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), 3)
            objPlanning = Nothing
            If strMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(strMsg, Me)
                Exit Sub
            End If

            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
                popupDelete.Title = "Enter valid reason to void custom item planning."
                popupDelete.Reason = ""
                popupDelete.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            mblnItemAddEdit = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

    'Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
    '    Dim objPlaningItem As New clsassess_plan_customitem_tran
    '    Try

    '        Dim dgv As DataGridItem = Nothing
    '        For Each xRow As DataRow In dtCItems.Rows
    '            For i As Integer = 0 To dgv_Citems.Items.Count - 1
    '                dgv = dgv_Citems.Items(i)
    '                Select Case xRow("itemtypeid")
    '                    Case clsassess_custom_items.enCustomType.FREE_TEXT
    '                        If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
    '                            Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
    '                            xRow.Item("custom_value") = xtxt.Text
    '                            Exit For
    '                        End If
    '                    Case clsassess_custom_items.enCustomType.SELECTION
    '                        If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
    '                            Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
    '                            If xCbo.SelectedValue > 0 Then
    '                                xRow.Item("custom_value") = xCbo.SelectedValue
    '                                xRow.Item("selectedid") = xCbo.SelectedValue
    '                                Exit For
    '                            End If
    '                        End If
    '                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
    '                        If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
    '                            If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
    '                                xRow.Item("custom_value") = eZeeDate.convertDate(CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate)
    '                                xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
    '                                Exit For
    '                            End If
    '                        End If
    '                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
    '                        If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
    '                            Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
    '                            xRow.Item("custom_value") = xtxt.Text
    '                            Exit For
    '                        End If
    '                End Select
    '            Next
    '        Next
    '        Dim dtmp() As DataRow = Nothing
    '        If dtCItems IsNot Nothing Then
    '            dtCItems.AcceptChanges()
    '            dtmp = dtCItems.Select("custom_value <> ''")
    '            If dtmp.Length <= 0 Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), Me)
    '                Exit Sub
    '            End If
    '            dtmp = dtCItems.Select("")
    '        End If

    '        If dtmp.Length > 0 Then
    '            If mintItemRowIndex <= -1 Then
    '                Dim iGUID As String = ""
    '                iGUID = Guid.NewGuid.ToString
    '                For iR As Integer = 0 To dtmp.Length - 1
    '                    objPlaningItem._customitemunkid = dtmp(iR)("customitemunkid")
    '                    objPlaningItem._custom_value = dtmp(iR)("custom_value")
    '                    objPlaningItem._customheaderunkid = Me.ViewState("customheaderunkid")
    '                    objPlaningItem._EmployeeUnkid = cboEmployee.SelectedValue
    '                    objPlaningItem._Isvoid = False
    '                    objPlaningItem._Periodunkid = cboPeriod.SelectedValue
    '                    objPlaningItem._plancustomguid = iGUID
    '                    objPlaningItem._transactiondate = ConfigParameter._Object._CurrentDateAndTime
    '                    objPlaningItem._voiddatetime = Nothing
    '                    objPlaningItem._voidreason = ""
    '                    objPlaningItem._voiduserunkid = -1
    '                    If objPlaningItem.Insert(Session("UserId")) = False Then
    '                        DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
    '                        Exit Sub
    '                    End If
    '                Next
    '            Else
    '                If dtmp.Length > 0 Then
    '                    Dim dsItems = New DataSet
    '                    dsItems = objPlaningItem.GetList(Session("Database_Name"), _
    '                                        Session("UserId"), _
    '                                        Session("Fin_year"), _
    '                                        Session("CompanyUnkId"), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                        Session("UserAccessModeSetting"), True, _
    '                                        Session("IsIncludeInactiveEmp"), "Cus_Items", _
    '                                        cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), "plancustomguid = '" & mstriEditingGUID & "'")

    '                    For iR As Integer = 0 To dtmp.Length - 1
    '                        Dim xRow() As DataRow = dsItems.Tables(0).Select("customitemunkid = '" & dtmp(iR).Item("customitemunkid") & "' AND plancustomguid = '" & mstriEditingGUID & "'")

    '                        objPlaningItem._plancustomunkid = xRow(0)("plancustomunkid")
    '                        objPlaningItem._customitemunkid = dtmp(iR)("customitemunkid")
    '                        objPlaningItem._custom_value = dtmp(iR)("custom_value")
    '                        objPlaningItem._customheaderunkid = Me.ViewState("customheaderunkid")
    '                        objPlaningItem._EmployeeUnkid = cboEmployee.SelectedValue
    '                        objPlaningItem._Isvoid = False
    '                        objPlaningItem._Periodunkid = cboPeriod.SelectedValue
    '                        objPlaningItem._plancustomguid = mstriEditingGUID
    '                        objPlaningItem._transactiondate = ConfigParameter._Object._CurrentDateAndTime
    '                        objPlaningItem._voiddatetime = Nothing
    '                        objPlaningItem._voidreason = ""
    '                        objPlaningItem._voiduserunkid = -1
    '                        If objPlaningItem.Update(Session("UserId")) = False Then
    '                            DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
    '                            Exit Sub
    '                        End If

    '                        'If xRow.Length > 0 Then

    '                        '    xRow(0).Item("customanalysistranguid") = xRow(0).Item("customanalysistranguid")
    '                        '    xRow(0).Item("analysisunkid") = xRow(0).Item("analysisunkid")
    '                        '    xRow(0).Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
    '                        '    xRow(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '                        '    Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
    '                        '        Case clsassess_custom_items.enCustomType.FREE_TEXT
    '                        '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
    '                        '        Case clsassess_custom_items.enCustomType.SELECTION
    '                        '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("selectedid")
    '                        '        Case clsassess_custom_items.enCustomType.DATE_SELECTION
    '                        '            If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
    '                        '                xRow(0).Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
    '                        '            Else
    '                        '                xRow(0).Item("custom_value") = ""
    '                        '            End If
    '                        '        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
    '                        '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
    '                        '    End Select
    '                        '    xRow(0).Item("custom_header") = Me.ViewState("Header")
    '                        '    xRow(0).Item("custom_item") = dtmp(iEdit).Item("custom_item")
    '                        '    xRow(0).Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
    '                        '    xRow(0).Item("AUD") = "U"
    '                        '    xRow(0).Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
    '                        '    xRow(0).Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
    '                        'End If
    '                    Next
    '                End If
    '            End If
    '        End If
    '        mblnItemAddEdit = False
    '        popup_CItemAddEdit.Hide()
    '        Call Fill_Gird()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
        Dim objPlaningItem As New clsassess_plan_customitem_tran
        Try
            Dim dgv As DataGridItem = Nothing
            For Each xRow As DataRow In dtCItems.Rows
                For i As Integer = 0 To dgv_Citems.Items.Count - 1
                    dgv = dgv_Citems.Items(i)
                    Select Case CInt(xRow("itemtypeid"))
                        Case clsassess_custom_items.enCustomType.FREE_TEXT
                            If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
                                xRow.Item("custom_value") = xtxt.Text
                                Exit For
                            End If
                        Case clsassess_custom_items.enCustomType.SELECTION
                            If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
                                If xCbo.SelectedValue > 0 Then
                                    xRow.Item("custom_value") = xCbo.SelectedValue
                                    xRow.Item("selectedid") = xCbo.SelectedValue
                                    Exit For
                                End If
                            End If
                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
                            If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
                                If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
                                    xRow.Item("custom_value") = eZeeDate.convertDate(CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate)
                                    xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
                                    Exit For
                                End If
                            End If
                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                            If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
                                xRow.Item("custom_value") = xtxt.Text
                                Exit For
                            End If
                    End Select
                Next
            Next
            Dim dtmp() As DataRow = Nothing

            If dtCItems IsNot Nothing Then
                dtCItems.AcceptChanges()
                dtmp = dtCItems.Select("custom_value <> ''")
                If dtmp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), Me)
                    Exit Sub
                End If
                dtmp = dtCItems.Select("")

                Dim dRow As DataRow = Nothing
                If mintItemRowIndex <= -1 Then
                    Dim iGUID As String = Guid.NewGuid.ToString
                    For Each iR As DataRow In dtmp
                        dRow = mdtCustomEvaluation.NewRow()
                        dRow.Item("plancustomunkid") = -1
                        dRow.Item("plancustomguid") = iGUID
                        dRow.Item("customitemunkid") = iR.Item("customitemunkid")
                        dRow.Item("custom_value") = iR.Item("custom_value")
                        dRow.Item("periodunkid") = cboPeriod.SelectedValue
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("display_value") = iR.Item("custom_value")
                        dRow.Item("itemtypeid") = iR.Item("itemtypeid")
                        dRow.Item("selectionmodeid") = iR.Item("selectedid")
                        dRow.Item("AUD") = "A"
                        dRow.Item("isdefaultentry") = iR.Item("isdefaultentry")
                        dRow.Item("customheaderunkid") = Me.ViewState("customheaderunkid")
                        dRow.Item("isfinal") = 0
                        dRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                        dRow.Item("ismanual") = True
                        mdtCustomEvaluation.Rows.Add(dRow)
                    Next
                Else
                    If mstriEditingGUID <> "" Then
                        For Each iR As DataRow In dtmp
                            Dim dtRow() As DataRow = mdtCustomEvaluation.Select("plancustomguid ='" & mstriEditingGUID & "' AND customitemunkid = '" & iR.Item("customitemunkid") & "'")
                            If dtRow.Length > 0 Then
                                dtRow(0).Item("custom_value") = iR.Item("custom_value")
                                dtRow(0).Item("display_value") = iR.Item("custom_value")
                                dtRow(0).Item("selectionmodeid") = iR.Item("selectedid")
                                If dtRow(0).Item("AUD").ToString.Trim = "" Then
                                    dtRow(0).Item("AUD") = "U"
                                End If
                                dtRow(0).Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                            Else
                                dRow = mdtCustomEvaluation.NewRow()
                                dRow.Item("plancustomunkid") = -1
                                dRow.Item("plancustomguid") = mstriEditingGUID
                                dRow.Item("customitemunkid") = iR.Item("customitemunkid")
                                dRow.Item("custom_value") = iR.Item("custom_value")
                                dRow.Item("periodunkid") = cboPeriod.SelectedValue
                                dRow.Item("isvoid") = False
                                dRow.Item("voiduserunkid") = -1
                                dRow.Item("voiddatetime") = DBNull.Value
                                dRow.Item("voidreason") = ""
                                dRow.Item("display_value") = iR.Item("custom_value")
                                dRow.Item("itemtypeid") = iR.Item("itemtypeid")
                                dRow.Item("selectionmodeid") = iR.Item("selectedid")
                                dRow.Item("AUD") = "A"
                                dRow.Item("isdefaultentry") = False
                                dRow.Item("customheaderunkid") = Me.ViewState("customheaderunkid")
                                dRow.Item("isfinal") = 0
                                dRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                                dRow.Item("ismanual") = CBool(mdtCustomTabularGrid.Rows(mintItemRowIndex)("ismanual"))
                                mdtCustomEvaluation.Rows.Add(dRow)
                            End If
                        Next
                    End If
                End If
                mdtCustomEvaluation.AcceptChanges()
            End If
            objPlaningItem._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objPlaningItem._Periodunkid = CInt(cboPeriod.SelectedValue)
            If objPlaningItem.InsertUpdateDelete_CustomAnalysisTran(mdtCustomEvaluation, Session("UserId")) = False Then
                DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
                Exit Sub
            Else
                mblnItemAddEdit = False
                popup_CItemAddEdit.Hide()
                Call Fill_Gird()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
    '    Try
    '        If popupDelete.Reason.Trim.Length > 0 Then
    '            Dim objPlaningItem As New clsassess_plan_customitem_tran
    '            Dim dsItems As New DataSet
    '            dsItems = objPlaningItem.GetList(Session("Database_Name"), _
    '                                        Session("UserId"), _
    '                                        Session("Fin_year"), _
    '                                        Session("CompanyUnkId"), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                        Session("UserAccessModeSetting"), True, _
    '                                        Session("IsIncludeInactiveEmp"), "Cus_Items", _
    '                                        cboEmployee.SelectedValue, cboPeriod.SelectedValue, _
    '                                        Me.ViewState("customheaderunkid"), "plancustomguid = '" & mstriEditingGUID & "'")

    '            For Each row In dsItems.Tables(0).Rows
    '                objPlaningItem._plancustomunkid = row("plancustomunkid")
    '                objPlaningItem._voiduserunkid = Session("UserId")
    '                objPlaningItem._voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                objPlaningItem._Isvoid = True
    '                objPlaningItem._voidreason = popupDelete.Reason.Trim
    '                objPlaningItem.Delete()
    '            Next

    '            objPlaningItem = Nothing
    '        End If
    '        Call Fill_Gird()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
        Try
            If popupDelete.Reason.Trim.Length > 0 Then
                Dim objPlaningItem As New clsassess_plan_customitem_tran

                Dim dtRow() As DataRow = Nothing
                If CBool(mdtCustomTabularGrid.Rows(mintItemRowIndex)("ismanual")) Then
                    dtRow = mdtCustomEvaluation.Select("plancustomguid = '" & mstriEditingGUID & "'")
                Else
                    dtRow = mdtCustomEvaluation.Select("plancustomguid = '" & mstriEditingGUID & "' AND isdefaultentry = 'False' ")
                End If

                For Each row In dtRow
                    row.Item("AUD") = "D"
                    row.Item("voiduserunkid") = Session("UserId")
                    row.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    row.Item("voidreason") = popupDelete.Reason.Trim
                    row.Item("isvoid") = True
                Next
                mdtCustomEvaluation.AsEnumerable()

                objPlaningItem._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                objPlaningItem._Periodunkid = CInt(cboPeriod.SelectedValue)
                If objPlaningItem.InsertUpdateDelete_CustomAnalysisTran(mdtCustomEvaluation, Session("UserId")) = False Then
                    DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
                    Exit Sub
                Else
                    mblnItemAddEdit = False
                    popup_CItemAddEdit.Hide()
                    Call Fill_Gird()
                End If

                objPlaningItem = Nothing
            End If
            Call Fill_Gird()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani (26-Sep-2016) -- End

    'Shani(24-Dec-2015) -- End

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Sorry, Please select Employee and Period to view Performance Planning."), Me)
                Exit Sub
            End If

            Dim objCustomHeader As New ArutiReports.clsCustomItemValueReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objCustomHeader._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
            objCustomHeader._PeriodName = cboPeriod.SelectedItem.Text
            objCustomHeader._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
            objCustomHeader._EmployeeName = cboEmployee.SelectedItem.Text
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                objCustomHeader._AddUserAccessLevel = False
            End If
            objCustomHeader.generateReportNew(Session("Database_Name"), _
                                              Session("UserId"), _
                                              Session("Fin_year"), _
                                              Session("CompanyUnkId"), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                              Session("UserAccessModeSetting"), True, _
                                              Session("ExportReportPath"), _
                                              Session("OpenAfterExport"), 0, enPrintAction.None, _
                                              enExportAction.None, _
                                              Session("Base_CurrencyId"))

            Session("objRpt") = objCustomHeader._Rpt
            If objCustomHeader._Rpt IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab1();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Protected Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP |12-FEB-2019| -- END

#End Region

#Region "GridView Event(S)"

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Shani(24-Dec-2015) -- Start
                'Enhancement - CCBRT WORK
                'If dgvData.DataKeys(e.Row.RowIndex).Value.ToString.Trim.Length <= 0 Then
                '    e.Row.CssClass = "GroupHeaderStyle"
                '    e.Row.Cells(0).ColumnSpan = dgvData.Columns.Count
                '    For i As Integer = 1 To dgvData.Columns.Count - 1
                '        e.Row.Cells(i).Visible = False
                '    Next
                'End If
                If mblnIsAddColumn = True Then

                    'Shani(24-Feb-2016) -- Start
                    'If dgvData.Rows.Count > 0 Then
                    '    Dim imgAdd As New ImageButton()
                    '    imgAdd.ID = "imgAdd"
                    '    imgAdd.Attributes.Add("Class", "objAddBtn")
                    '    imgAdd.CommandName = "objAdd"
                    '    imgAdd.ImageUrl = "~/images/add_16.png"
                    '    imgAdd.ToolTip = "New"
                    '    AddHandler imgAdd.Click, AddressOf imgAdd_Click
                    '    e.Row.Cells(0).Controls.Add(imgAdd)

                    '    Dim imgEdit As New ImageButton()
                    '    imgEdit.ID = "imgEdit"
                    '    imgEdit.Attributes.Add("Class", "objAddBtn")
                    '    imgEdit.CommandName = "objEdit"
                    '    imgEdit.ImageUrl = "~/images/Edit.png"
                    '    imgEdit.ToolTip = "Edit"
                    '    AddHandler imgEdit.Click, AddressOf imgEdit_Click
                    '    e.Row.Cells(1).Controls.Add(imgEdit)


                    '    Dim imgDelete As New ImageButton()
                    '    imgDelete.ID = "imgDelete"
                    '    imgDelete.Attributes.Add("Class", "objAddBtn")
                    '    imgDelete.CommandName = "objDelete"
                    '    imgDelete.ImageUrl = "~/images/remove.png"
                    '    imgDelete.ToolTip = "Delete"
                    '    AddHandler imgDelete.Click, AddressOf imgDelete_Click
                    '    e.Row.Cells(2).Controls.Add(imgDelete)
                    'End If

                    Dim imgAdd As New ImageButton()
                    imgAdd.ID = "imgAdd"
                    imgAdd.Attributes.Add("Class", "objAddBtn")
                    imgAdd.CommandName = "objAdd"
                    imgAdd.ImageUrl = "~/images/add_16.png"
                    imgAdd.ToolTip = "New"
                    AddHandler imgAdd.Click, AddressOf imgAdd_Click
                    e.Row.Cells(0).Controls.Add(imgAdd)

                    Dim imgEdit As New ImageButton()
                    imgEdit.ID = "imgEdit"
                    imgEdit.Attributes.Add("Class", "objAddBtn")
                    imgEdit.CommandName = "objEdit"
                    imgEdit.ImageUrl = "~/images/Edit.png"
                    imgEdit.ToolTip = "Edit"
                    AddHandler imgEdit.Click, AddressOf imgEdit_Click
                    e.Row.Cells(1).Controls.Add(imgEdit)


                    Dim imgDelete As New ImageButton()
                    imgDelete.ID = "imgDelete"
                    imgDelete.Attributes.Add("Class", "objAddBtn")
                    imgDelete.CommandName = "objDelete"
                    imgDelete.ImageUrl = "~/images/remove.png"
                    imgDelete.ToolTip = "Delete"
                    AddHandler imgDelete.Click, AddressOf imgDelete_Click
                    e.Row.Cells(2).Controls.Add(imgDelete)

                    'Shani(24-Feb-2016) -- End
                Else
                    If dgvData.DataKeys(e.Row.RowIndex).Value.ToString.Trim.Length <= 0 Then
                        e.Row.CssClass = "group-header"
                        e.Row.Cells(0).ColumnSpan = dgvData.Columns.Count
                        For i As Integer = 1 To dgvData.Columns.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                    End If
                End If
                'Shani(24-Dec-2015) -- End
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgv_Citems.ItemDataBound
        Try
            'dtCItems = Me.Session("dtCItems")
            If e.Item.ItemIndex > -1 Then
                If CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    Dim txt As New TextBox
                    txt.ID = "txt" & e.Item.Cells(4).Text
                    txt.TextMode = TextBoxMode.MultiLine
                    txt.Rows = 7 'Shani(23-FEB-2017) 
                    txt.Style.Add("resize", "none")
                    txt.Width = Unit.Percentage(100)
                    txt.CssClass = "removeTextcss"
                    'If CBool(e.Item.Cells(3).Text) Then
                    '    txt.ReadOnly = True
                    'End If
                    If mintItemRowIndex > -1 OrElse mstriEditingGUID <> "" Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    End If
                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    If CBool(e.Item.Cells(6).Text) = True Then
                        txt.ReadOnly = True
                    End If
                    'Shani (26-Sep-2016) -- End
                    e.Item.Cells(1).Controls.Add(txt)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    Dim dtp As Control
                    dtp = LoadControl("~/Controls/DateCtrl.ascx")
                    dtp.ID = "dtp" & e.Item.Cells(4).Text
                    CType(dtp, Controls_DateCtrl).AutoPostBack = True
                    'If CBool(e.Item.Cells(3).Text) Then
                    '    CType(dtp, Controls_DateCtrl).Enabled = False
                    'End If
                    If mintItemRowIndex > -1 Then
                        If dtCItems.Rows(e.Item.ItemIndex)("custom_value").ToString().Trim.Length > 0 Then
                            CType(dtp, Controls_DateCtrl).SetDate = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                        End If

                    End If
                    AddHandler CType(dtp, Controls_DateCtrl).TextChanged, AddressOf dtpCustomItem_TextChanged
                    e.Item.Cells(1).Controls.Add(dtp)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
                    Dim cbo As New DropDownList
                    cbo.ID = "cbo" & e.Item.Cells(4).Text
                    cbo.Width = Unit.Pixel(250)
                    'If CBool(e.Item.Cells(3).Text) Then
                    '    cbo.Enabled = False
                    'End If
                    Select Case CInt(e.Item.Cells(5).Text)
                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                            Dim objCMaster As New clsCommon_Master
                            Dim dsList As New DataSet
                            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objCMaster = Nothing
                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                            Dim objCompetency As New clsassess_competencies_master
                            Dim dsList As New DataSet
                            dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)

                            With cbo
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objCompetency = Nothing
                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                            Dim objEmpField1 As New clsassess_empfield1_master
                            Dim dsList As New DataSet
                            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True)
                            With cbo
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objEmpField1 = Nothing


                            'S.SANDEEP |16-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                            Dim objCMaster As New clsCommon_Master
                            Dim dsList As New DataSet
                            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objCMaster = Nothing
                            'S.SANDEEP |16-AUG-2019| -- END


                    End Select
                    If mintItemRowIndex > -1 Then
                        cbo.SelectedValue = IIf(dtCItems.Rows(e.Item.ItemIndex)("custom_value") = "", 0, dtCItems.Rows(e.Item.ItemIndex)("custom_value"))
                    End If
                    e.Item.Cells(1).Controls.Add(cbo)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    Dim txt As New TextBox
                    txt.ID = "txtnum" & e.Item.Cells(4).Text
                    'If CBool(e.Item.Cells(3).Text) Then
                    '    txt.ReadOnly = True
                    'End If
                    txt.Style.Add("text-align", "right")
                    txt.Attributes.Add("onKeypress", "return onlyNumbers(this, event);")
                    txt.Width = Unit.Percentage(100)
                    txt.CssClass = "removeTextcss"

                    If mintItemRowIndex > -1 Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    End If
                    e.Item.Cells(1).Controls.Add(txt)
                End If




            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
#Region " Combobox Event(s) "

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 AndAlso CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                Call btnSearch_Click(New Object(), New EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |12-FEB-2019| -- END

#Region "Controls"

    Protected Sub dtpCustomItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If CDate(dtpAssessdate.GetDate) >= CDate(CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).GetDate) Then
            '    CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).SetDate = Nothing
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
            '    CType(sender, Controls_DateCtrl).Focus()
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Protected Sub cboHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboHeader.SelectedIndexChanged
        Try
            dgvData.DataSource = Nothing
            dgvData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [01-OCT-2018] -- END

#End Region

End Class

'S.SANDEEP [01-OCT-2018] -- START {Ref#2586|ARUTI-} -- END
'
'

'Partial Class Assessment_New_Common_wPg_CustomeItemView
'    Inherits Basepage

'#Region "Private Variables"
'    Dim DisplayMessage As New CommonCodes

'    'Shani(24-Dec-2015) -- Start
'    'Enhancement - CCBRT WORK
'    Private mblnIsAddColumn As Boolean = False
'    Private mdtCustomTabularGrid As DataTable = Nothing
'    Private mintItemRowIndex As Integer = 0
'    Private mstriEditingGUID As String = ""
'    Private mblnItemAddEdit As Boolean = False
'    Private mdtCustomEvaluation As New DataTable
'    Private dtCItems As DataTable = Nothing
'    'Private mblnSerchRecord As Boolean = False
'    Private ReadOnly mstrModuleName As String = "frmCustomItemView"

'    'Shani(24-Dec-2015) -- End

'    'Shani (26-Sep-2016) -- Start
'    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'    Private mblnIsmatchWithComp As Boolean = False
'    Private mblnIsIncludePlaning As Boolean = False
'    'Shani (26-Sep-2016) -- End

'#End Region

'#Region "Page Event"

'    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            If IsPostBack = False Then
'                Call Fill_Combo()
'                Dim dt As New DataTable
'                Me.ViewState.Add("customheaderunkid", Session("customheaderunkid"))
'                'Shani(24-Dec-2015) -- Start
'                'Enhancement - CCBRT WORK

'                Dim objCustomHeader As New clsassess_custom_header
'                objCustomHeader._Customheaderunkid = CInt(Session("customheaderunkid"))
'                mblnIsmatchWithComp = objCustomHeader._IsMatch_With_Competency
'                lblPageHeader.Text = objCustomHeader._Name
'                'Me.Title = objCustomHeader._Name
'                'Shani (26-Sep-2016) -- Start
'                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                mblnIsIncludePlaning = objCustomHeader._IsInclude_Planning


'                Blank_ModuleName()
'                clsCommonATLog._WebFormName = "frmCustomItemView"
'                StrModuleName1 = "mnuHumanResource"
'                StrModuleName2 = "mnuAssessment"
'                StrModuleName3 = "mnuSetups"
'                clsCommonATLog._WebClientIP = Session("IP_ADD")
'                clsCommonATLog._WebHostName = Session("HOST_NAME")

'                'Shani (26-Sep-2016) -- End
'                objCustomHeader = Nothing

'            Else
'                mblnIsAddColumn = Me.ViewState("mblnIsAddColumn")
'                mdtCustomTabularGrid = Me.ViewState("mdtCustomTabularGrid")
'                mintItemRowIndex = Me.ViewState("mintItemRowIndex")
'                mstriEditingGUID = Me.ViewState("mstriEditingGUID")
'                mblnItemAddEdit = Me.ViewState("mblnItemAddEdit")
'                mdtCustomEvaluation = Me.ViewState("mdtCustomEvaluation")
'                mblnIsmatchWithComp = Me.ViewState("mblnIsmatchWithComp")
'                dtCItems = Me.ViewState("dtCItems")
'                mblnIsIncludePlaning = Me.ViewState("mblnIsIncludePlaning")

'                dgvData.DataSource = mdtCustomTabularGrid
'                dgvData.DataBind()

'                If mblnItemAddEdit Then
'                    dgv_Citems.DataSource = dtCItems
'                    dgv_Citems.DataBind()
'                    popup_CItemAddEdit.Show()
'                End If
'                'Shani(24-Dec-2015) -- End
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayMessage("Page_Load1 : " & ex.Message, Me)
'        End Try
'    End Sub

'    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
'        Me.IsLoginRequired = True
'    End Sub

'    'Shani(24-Dec-2015) -- Start
'    'Enhancement - CCBRT WORK
'    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
'        Try
'            Me.ViewState("mblnIsAddColumn") = mblnIsAddColumn
'            Me.ViewState("mdtCustomTabularGrid") = mdtCustomTabularGrid
'            Me.ViewState("mintItemRowIndex") = mintItemRowIndex
'            Me.ViewState("mstriEditingGUID") = mstriEditingGUID
'            Me.ViewState("mblnItemAddEdit") = mblnItemAddEdit
'            Me.ViewState("mdtCustomEvaluation") = mdtCustomEvaluation
'            Me.ViewState("mblnIsmatchWithComp") = mblnIsmatchWithComp
'            Me.ViewState("dtCItems") = dtCItems
'            Me.ViewState("mblnIsIncludePlaning") = mblnIsIncludePlaning
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub
'    'Shani(24-Dec-2015) -- End

'#End Region

'#Region "Private Methods"
'    Private Sub Fill_Combo()
'        Try
'            Dim objPriod As New clscommom_period_Tran
'            Dim dsList As New DataSet

'            'S.SANDEEP [17 NOV 2015] -- START
'            'dsList = objPriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsList = objPriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))
'            dsList = objPriod.getListForCombo(enModuleReference.Assessment, 0, _
'                                              Session("Database_Name"), _
'                                              Session("fin_startdate"), _
'                                              "List", True, 1)
'            'Shani(20-Nov-2015) -- End

'            'S.SANDEEP [17 NOV 2015] -- END


'            'S.SANDEEP [29 DEC 2015] -- START


'            'Shani (09-May-2016) -- Start
'            'Integer.TryParse(New DataView(dsList.Tables(0), "end_date <= " & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime), "end_date DESC", DataViewRowState.CurrentRows).ToTable.Rows(0)("periodunkid").ToString, intPeriodUnkID)
'            'Dim intPeriodUnkID As Integer = 0
'            Dim intPeriodUnkID As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
'            'Shani (09-May-2016) -- End


'            'S.SANDEEP [29 DEC 2015] -- END


'            With cboPeriod
'                .DataValueField = "periodunkid"
'                .DataTextField = "name"
'                .DataSource = dsList.Tables(0)
'                .DataBind()
'                'S.SANDEEP [29 DEC 2015] -- START
'                .SelectedValue = intPeriodUnkID
'                'S.SANDEEP [29 DEC 2015] -- END
'            End With

'            dsList = Nothing
'            Dim objEmp As New clsEmployee_Master

'            'Shani(24-Aug-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
'            '    dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
'            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'            '    dsList = objEmp.GetEmployeeList("Emp", False, True, Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
'            'End If
'            Dim blnSelect As Boolean = True
'            Dim intEmpId As Integer = 0
'            'S.SANDEEP [10 DEC 2015] -- START
'            Dim blnApplyUACFilter As Boolean = False 'Shani(14-APR-2016) --[True]
'            'S.SANDEEP [10 DEC 2015] -- END

'            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                blnSelect = False
'                intEmpId = CInt(Session("Employeeunkid"))
'                'S.SANDEEP [10 DEC 2015] -- START
'                blnApplyUACFilter = False
'                'S.SANDEEP [10 DEC 2015] -- END
'            End If

'            'S.SANDEEP [04 Jan 2016] -- START
'            Dim strFilterQry As String = String.Empty
'            If (Session("LoginBy") = Global.User.en_loginby.User) Then
'                'Shani(14-APR-2016) -- Start
'                'If Session("SkipApprovalFlowInPlanning") = True Then
'                'Shani(14-APR-2016) -- End 
'                Dim csvIds As String = String.Empty
'                Dim dsMapEmp As New DataSet
'                Dim objEval As New clsevaluation_analysis_master

'                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
'                                                        Session("UserId"), _
'                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                    True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
'                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
'                If dsMapEmp.Tables("List").Rows.Count > 0 Then

'                    dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
'                                                              Session("UserId"), _
'                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
'                                                              Session("IsIncludeInactiveEmp"), _
'                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


'                    strFilterQry = " hremployee_master.employeeunkid IN "
'                    csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
'                    If csvIds.Trim.Length > 0 Then
'                        strFilterQry &= "(" & csvIds & ")"
'                    Else
'                        strFilterQry &= "(0)"
'                    End If
'                    'Shani(14-APR-2016) -- Start
'                Else
'                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
'                    'Shani(14-APR-2016) -- End 
'                End If

'                'Shani(14-APR-2016) -- Start
'                'End If
'                'Shani(14-APR-2016) -- End
'            End If
'            'S.SANDEEP [04 Jan 2016] -- END

'            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
'                                            Session("UserId"), _
'                                            Session("Fin_year"), _
'                                            Session("CompanyUnkId"), _
'                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                            Session("UserAccessModeSetting"), True, _
'                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyUACFilter) 'S.SANDEEP [10 DEC 2015] -- START {blnApplyUACFilter} -- END
'            'S.SANDEEP [04 Jan 2016] -- START {strFilterQry} -- END

'            'Shani(24-Aug-2015) -- End

'            With cboEmployee
'                .DataValueField = "employeeunkid"
'                'Nilay (09-Aug-2016) -- Start
'                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
'                '.DataTextField = "employeename"
'                .DataTextField = "EmpCodeName"
'                'Nilay (09-Aug-2016) -- End

'                .DataSource = dsList.Tables(0)
'                .DataBind()

'                'Shani(20-Nov-2015) -- Start
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'If (Session("LoginBy") = Global.User.en_loginby.User) Then
'                '    .SelectedValue = 0
'                'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                '    .SelectedValue = Session("Employeeunkid")
'                'End If
'                .SelectedValue = intEmpId
'                'Shani(20-Nov-2015) -- End
'            End With
'        Catch ex As Exception
'            DisplayMessage.DisplayMessage("Fill_Combo : " & ex.Message, Me)
'        End Try
'    End Sub

'    Private Sub Fill_Planing_Grid()
'        Try

'            '************* ADD
'            Dim iTempField As New TemplateField()
'            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.FooterText = "ObjAdd"
'            iTempField.HeaderText = "Add"
'            iTempField.ItemStyle.Width = Unit.Pixel(40)
'            dgvData.Columns.Add(iTempField)

'            '************* EDIT
'            iTempField = New TemplateField()
'            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.FooterText = "objEdit"
'            iTempField.HeaderText = "Edit"
'            iTempField.ItemStyle.Width = Unit.Pixel(40)
'            dgvData.Columns.Add(iTempField)

'            '************* DELETE
'            iTempField = New TemplateField()
'            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.FooterText = "objDelete"
'            iTempField.HeaderText = "Delete"
'            iTempField.ItemStyle.Width = Unit.Pixel(40)
'            dgvData.Columns.Add(iTempField)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    'Shani (26-Sep-2016) -- Start
'    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

'    'Private Sub Fill_Gird()
'    '    Dim objEAnalysisMst As New clsevaluation_analysis_master
'    '    Dim objPlaningCutomItem As New clsassess_plan_customitem_tran
'    '    Try
'    '        Dim dsItems As New DataSet : Dim objCusItem As New clsassess_custom_items
'    '        dsItems = objCusItem.getComboList(cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), False, "List")
'    '        If dsItems.Tables(0).Rows.Count <= 0 Then
'    '            DisplayMessage.DisplayMessage("Sorry, no custom items define for the selected period.", Me)
'    '            dsItems.Dispose() : objCusItem = Nothing
'    '            Exit Sub
'    '        End If
'    '        If dsItems IsNot Nothing Then dsItems.Dispose()
'    '        If objCusItem IsNot Nothing Then objCusItem = Nothing

'    '        If Check_IsAssesst() = False Then
'    '            mdtCustomTabularGrid = objPlaningCutomItem.GetList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Me.ViewState("customheaderunkid"), "List", CBool(Session("IsCompanyNeedReviewer")))
'    '        Else
'    '            mdtCustomTabularGrid = objEAnalysisMst.GetCustom_Items_Data(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), Me.ViewState("customheaderunkid"))
'    '        End If

'    '        If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
'    '            dgvData.Columns.Clear()
'    '            dgvData.AutoGenerateColumns = False
'    '            Dim iColName As String = String.Empty
'    '            If mblnSerchRecord = True AndAlso Check_IsAssesst() = False Then
'    '                If mdtCustomTabularGrid.AsEnumerable().Count <= 0 Then
'    '                    mdtCustomTabularGrid.Rows.Add(mdtCustomTabularGrid.NewRow())
'    '                End If
'    '                Call Fill_Planing_Grid()
'    '                mblnIsAddColumn = True
'    '            Else
'    '                If mblnIsAddColumn = True Then
'    '                    Call Fill_Planing_Grid()
'    '                End If
'    '            End If

'    '            For Each dCol As DataColumn In mdtCustomTabularGrid.Columns
'    '                iColName = "" : iColName = "obj" & dCol.ColumnName
'    '                Dim dgvCol As New BoundField()
'    '                dgvCol.FooterText = iColName
'    '                dgvCol.ReadOnly = True
'    '                dgvCol.DataField = dCol.ColumnName
'    '                dgvCol.HeaderText = dCol.Caption
'    '                If dgvData.Columns.Contains(dgvCol) = True Then Continue For
'    '                If dgvCol.HeaderText.Length > 0 Then
'    '                    dgvData.Columns.Add(dgvCol)
'    '                Else
'    '                    dgvCol = Nothing
'    '                End If

'    '                'If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
'    '            Next
'    '            dgvData.DataSource = mdtCustomTabularGrid
'    '            dgvData.DataKeyNames = New String() {"GUID"}
'    '            dgvData.DataBind()
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayMessage("Fill_Gird" & ex.Message, Me)
'    '    End Try
'    'End Sub

'    Private Sub Fill_Gird()
'        Dim dsItems As New DataSet
'        'Dim dtCustomTabularGrid As DataTable = Nothing
'        Try

'            dsItems = (New clsassess_custom_items).getComboList(cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), False, "List")
'            If dsItems.Tables(0).Rows.Count <= 0 Then
'                DisplayMessage.DisplayMessage("Sorry, no custom items define for the selected period.", Me)
'                dsItems.Dispose()
'                Exit Sub
'            End If
'            Dim dCol1 As DataColumn : mdtCustomTabularGrid = New DataTable
'            For Each drow As DataRow In dsItems.Tables(0).Select("", "isdefaultentry DESC")
'                dCol1 = New DataColumn
'                With dCol1
'                    .ColumnName = "CItem_" & drow.Item("Id").ToString
'                    .Caption = drow.Item("Name").ToString
'                    .DataType = System.Type.GetType("System.String")
'                    .DefaultValue = ""
'                End With
'                mdtCustomTabularGrid.Columns.Add(dCol1)

'                dCol1 = New DataColumn
'                With dCol1
'                    .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
'                    .Caption = ""
'                    .DataType = System.Type.GetType("System.Int32")
'                    .DefaultValue = -1
'                    .ExtendedProperties.Add("IsSystemGen", drow.Item("isdefaultentry"))
'                End With
'                mdtCustomTabularGrid.Columns.Add(dCol1)
'            Next

'            dCol1 = New DataColumn
'            With dCol1
'                .ColumnName = "GUID"
'                .Caption = ""
'                .DataType = System.Type.GetType("System.String")
'                .DefaultValue = ""
'            End With
'            mdtCustomTabularGrid.Columns.Add(dCol1)

'            dCol1 = New DataColumn
'            With dCol1
'                .ColumnName = "IsManual"
'                .Caption = ""
'                .DataType = GetType(System.Boolean)
'                .DefaultValue = True
'            End With
'            mdtCustomTabularGrid.Columns.Add(dCol1)

'            If dsItems IsNot Nothing Then dsItems.Dispose()

'            mdtCustomEvaluation = (New clsassess_plan_customitem_tran).GetCustomeDataHeaderWise(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("customheaderunkid")), CBool(Session("IsCompanyNeedReviewer")))
'            Dim dFRow As DataRow = Nothing
'            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'                Dim strGuidArray() As String = mdtCustomEvaluation.AsEnumerable().Select(Function(x) x.Field(Of String)("plancustomguid")).Distinct().ToArray
'                For Each StrGId As String In strGuidArray
'                    dFRow = mdtCustomTabularGrid.NewRow
'                    mdtCustomTabularGrid.Rows.Add(dFRow)
'                    For Each dRow As DataRow In mdtCustomEvaluation.Select("plancustomguid = '" & StrGId & "'")
'                        Select Case CInt(dRow.Item("itemtypeid"))
'                            Case clsassess_custom_items.enCustomType.FREE_TEXT
'                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
'                            Case clsassess_custom_items.enCustomType.SELECTION
'                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                    Select Case CInt(dRow.Item("selectionmodeid"))
'                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
'                                            Dim objCMaster As New clsCommon_Master
'                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
'                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                                            objCMaster = Nothing
'                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                                            Dim objCMaster As New clsassess_competencies_master
'                                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
'                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                                            objCMaster = Nothing
'                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                                            Dim objEmpField1 As New clsassess_empfield1_master
'                                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
'                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
'                                            objEmpField1 = Nothing
'                                    End Select
'                                End If
'                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
'                                End If
'                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                    If IsNumeric(dRow.Item("custom_value")) Then
'                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
'                                    End If
'                                End If
'                        End Select
'                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
'                        dFRow.Item("GUID") = dRow.Item("plancustomguid")
'                        If IsDBNull(dRow.Item("transactiondate")) Then
'                            dRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
'                        End If
'                        dFRow.Item("IsManual") = dRow.Item("ismanual")
'                    Next
'                Next
'                mdtCustomEvaluation.AcceptChanges()
'            End If
'            If mdtCustomTabularGrid.Rows.Count <= 0 Then
'                mdtCustomTabularGrid.Rows.Add(mdtCustomTabularGrid.NewRow)
'            End If
'            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
'                dgvData.Columns.Clear()
'                dgvData.AutoGenerateColumns = False
'                Dim iColName As String = String.Empty
'                If mblnIsIncludePlaning = True AndAlso Check_IsAssesst() = False Then
'                    If mdtCustomTabularGrid.AsEnumerable().Count <= 0 Then
'                        mdtCustomTabularGrid.Rows.Add(mdtCustomTabularGrid.NewRow())
'                    End If
'                    Call Fill_Planing_Grid()
'                    mblnIsAddColumn = True
'                Else
'                    If mblnIsAddColumn = True Then
'                        Call Fill_Planing_Grid()
'                    End If
'                End If

'                For Each dCol As DataColumn In mdtCustomTabularGrid.Columns
'                    iColName = "" : iColName = "obj" & dCol.ColumnName
'                    Dim dgvCol As New BoundField()
'                    dgvCol.FooterText = iColName
'                    dgvCol.ReadOnly = True
'                    dgvCol.DataField = dCol.ColumnName
'                    dgvCol.HeaderText = dCol.Caption
'                    If dgvData.Columns.Contains(dgvCol) = True Then Continue For
'                    If dgvCol.HeaderText.Length > 0 Then
'                        dgvData.Columns.Add(dgvCol)
'                    Else
'                        dgvCol = Nothing
'                    End If
'                Next
'                dgvData.DataSource = mdtCustomTabularGrid
'                dgvData.DataKeyNames = New String() {"GUID"}
'                dgvData.DataBind()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayMessage("Fill_Gird" & ex.Message, Me)
'        End Try
'    End Sub
'    'Shani (26-Sep-2016) -- End

'    'Shani(24-Dec-2015) -- Start
'    'Enhancement - CCBRT WORK


'    'S.SANDEEP [12 OCT 2016] -- START
'    'ENHANCEMENT : ACB REPORT CHANGES




'    'Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
'    '    Try
'    '        dtCItems = (New clsassess_plan_customitem_tran).Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid)

'    '        If mintItemRowIndex > -1 AndAlso mstriEditingGUID <> "" Then 'mdtCustomEvaluation

'    '            Dim dsItems As New DataSet
'    '            Dim objPlanning As New clsassess_plan_customitem_tran
'    '            dsItems = objPlanning.GetList(Session("Database_Name"), _
'    '                                          Session("UserId"), _
'    '                                          Session("Fin_year"), _
'    '                                          Session("CompanyUnkId"), _
'    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                          Session("UserAccessModeSetting"), True, _
'    '                                          Session("IsIncludeInactiveEmp"), "Cus_Items", _
'    '                                          cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), "plancustomguid = '" & mstriEditingGUID & "'")

'    '            objPlanning = Nothing
'    '            If dsItems.Tables(0).Rows.Count > 0 Then
'    '                For Each rows As DataRow In dsItems.Tables(0).Rows
'    '                    Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & rows.Item("customitemunkid") & "'")
'    '                    If xRow.Length > 0 Then
'    '                        Select Case CInt(rows.Item("itemtypeid"))
'    '                            Case clsassess_custom_items.enCustomType.SELECTION
'    '                                Select Case CInt(rows.Item("selectionmodeid"))
'    '                                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'    '                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
'    '                                        Dim objCMaster As New clsCommon_Master
'    '                                        objCMaster._Masterunkid = rows.Item("custom_value")
'    '                                        'rows.Item("display_value") = objCMaster._Name
'    '                                        objCMaster = Nothing
'    '                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'    '                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
'    '                                        Dim objCompetency As New clsassess_competencies_master
'    '                                        objCompetency._Competenciesunkid = rows.Item("custom_value")
'    '                                        'rows.Item("display_value") = objCompetency._Name
'    '                                        objCompetency = Nothing
'    '                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'    '                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
'    '                                        Dim objEmpField1 As New clsassess_empfield1_master
'    '                                        objEmpField1._Empfield1unkid = rows.Item("custom_value")
'    '                                        'rows.Item("display_value") = objEmpField1._Field_Data
'    '                                        objEmpField1 = Nothing
'    '                                End Select
'    '                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
'    '                                If rows.Item("custom_value").ToString.Trim.Length > 0 Then
'    '                                    xRow(0).Item("ddate") = eZeeDate.convertDate(rows.Item("custom_value"))
'    '                                    rows.Item("display_value") = xRow(0).Item("ddate")
'    '                                End If
'    '                        End Select
'    '                        If rows.Item("display_value").ToString.Trim.Length > 0 Then
'    '                            xRow(0).Item("custom_value") = rows.Item("display_value")
'    '                        Else
'    '                            xRow(0).Item("custom_value") = rows.Item("custom_value")
'    '                        End If
'    '                    End If
'    '                Next
'    '            Else

'    '            End If

'    '            'Dim dtmp() As DataRow = mdtCustomTabularGrid.Select("GUID = '" & mstriEditingGUID & "' AND AUD <> 'D'")
'    '            'If dtmp.Length > 0 Then
'    '            '    For iEdit As Integer = 0 To dtmp.Length - 1
'    '            '        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
'    '            '        If xRow.Length > 0 Then
'    '            '            Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
'    '            '                Case clsassess_custom_items.enCustomType.SELECTION
'    '            '                    Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
'    '            '                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'    '            '                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'    '            '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'    '            '                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'    '            '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'    '            '                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'    '            '                    End Select
'    '            '                Case clsassess_custom_items.enCustomType.DATE_SELECTION
'    '            '                    If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
'    '            '                        xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
'    '            '                    End If
'    '            '            End Select
'    '            '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
'    '            '        End If
'    '            '    Next
'    '            'End If
'    '        End If

'    '        dgv_Citems.AutoGenerateColumns = False
'    '        Dim objCHeader As New clsassess_custom_header
'    '        objCHeader._Customheaderunkid = xHeaderUnkid
'    '        If Me.ViewState("Header") IsNot Nothing Then
'    '            Me.ViewState("Header") = objCHeader._Name
'    '        Else
'    '            Me.ViewState.Add("Header", objCHeader._Name)
'    '        End If
'    '        objCHeader = Nothing
'    '        dgv_Citems.DataSource = dtCItems
'    '        dgv_Citems.DataBind()
'    '        mblnItemAddEdit = True
'    '        popup_CItemAddEdit.Show()
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex, Me)
'    '    Finally
'    '    End Try
'    'End Sub

'    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
'        Try
'            dtCItems = (New clsassess_plan_customitem_tran).Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid)
'            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
'            If mstriEditingGUID <> "" Then
'                Dim strFilter As String = "plancustomguid = '" & mstriEditingGUID & "' AND AUD <> 'D' "

'                If mintItemRowIndex <= -1 Then
'                    strFilter &= " AND isdefaultentry = TRUE"
'                End If

'                For Each rows As DataRow In mdtCustomEvaluation.Select(strFilter)
'                    Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & rows.Item("customitemunkid") & "'")
'                    If xRow.Length > 0 Then
'                        Select Case CInt(rows.Item("itemtypeid"))
'                            Case clsassess_custom_items.enCustomType.SELECTION
'                                Select Case CInt(rows.Item("selectionmodeid"))
'                                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
'                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
'                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                                        xRow(0).Item("selectedid") = rows.Item("custom_value")
'                                End Select
'                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                If rows.Item("custom_value").ToString.Trim.Length > 0 Then
'                                    xRow(0).Item("ddate") = eZeeDate.convertDate(rows.Item("custom_value"))
'                                    rows.Item("display_value") = xRow(0).Item("ddate")
'                                End If
'                        End Select
'                        If rows.Item("display_value").ToString.Trim.Length > 0 Then
'                            xRow(0).Item("custom_value") = rows.Item("display_value")
'                        Else
'                            xRow(0).Item("custom_value") = rows.Item("custom_value")
'                        End If
'                    End If
'                Next
'            End If
'            dgv_Citems.AutoGenerateColumns = False
'            Dim objCHeader As New clsassess_custom_header
'            objCHeader._Customheaderunkid = xHeaderUnkid
'            If Me.ViewState("Header") IsNot Nothing Then
'                Me.ViewState("Header") = objCHeader._Name
'            Else
'                Me.ViewState.Add("Header", objCHeader._Name)
'            End If

'            objCHeader = Nothing
'            dgv_Citems.DataSource = dtCItems
'            dgv_Citems.DataBind()
'            mblnItemAddEdit = True
'            popup_CItemAddEdit.Show()
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [12 OCT 2016] -- END
'    Private Function Check_IsAssesst() As Boolean
'        Dim bln As Boolean = False
'        Dim objAnalysis_Master As New clsevaluation_analysis_master
'        Try
'            If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
'                bln = True
'                Return bln
'            End If

'            If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
'                bln = True
'                Return bln
'            End If

'            If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
'                bln = True
'                Return bln
'            End If

'            Dim intStatusTranId As Integer = 0
'            Dim intStatusId As Integer = (New clsassess_empstatus_tran).Get_Last_StatusId(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), intStatusTranId)
'            If intStatusId = enObjective_Status.FINAL_SAVE OrElse intStatusId = enObjective_Status.SUBMIT_APPROVAL Then
'                bln = True
'                Return bln
'            End If
'            Return bln
'        Catch ex As Exception
'            Throw ex
'        Finally
'            If objAnalysis_Master IsNot Nothing Then objAnalysis_Master = Nothing
'        End Try
'    End Function
'    'Shani(24-Dec-2015) -- End

'#End Region

'#Region "Button Event"

'    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
'        Try
'            'S.SANDEEP [29 DEC 2015] -- START
'            'If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboPeriod.SelectedValue) <= 0 Then
'            '    DisplayMessage.DisplayMessage("Please set following information [Employee,Period] to view assessment", Me)
'            '    Exit Sub
'            'End If

'            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
'               CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
'                DisplayMessage.DisplayMessage("Please set following information [Employee,Period] to view assessment", Me)
'                Exit Sub
'            End If
'            'S.SANDEEP [29 DEC 2015] -- END


'            'Shani(24-Dec-2015) -- Start
'            'Enhancement - CCBRT WORK
'            mblnIsAddColumn = False
'            'Shani(24-Dec-2015) -- End

'            Fill_Gird()
'        Catch ex As Exception
'            DisplayMessage.DisplayMessage("btnSearch_Click" & ex.Message, Me)
'        End Try
'    End Sub

'    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
'        Try
'            If (Session("LoginBy") = Global.User.en_loginby.User) Then
'                cboEmployee.SelectedValue = 0
'            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                cboEmployee.SelectedValue = Session("Employeeunkid")
'            End If
'            cboPeriod.SelectedValue = 0
'            dgvData.DataSource = Nothing
'            dgvData.DataBind()
'        Catch ex As Exception
'            DisplayMessage.DisplayMessage("" & ex.Message, Me)
'        End Try
'    End Sub

'    'Nilay (01-Feb-2015) -- Start
'    'Enhancement - REDESIGN SELF SERVICE.
'    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
'    '    Try
'    '        Response.Redirect("~/Userhome.aspx")
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayMessage("Closebotton1_CloseButton_click :" & ex.Message, Me)
'    '    End Try
'    'End Sub
'    'Nilay (01-Feb-2015) -- End

'    'Shani(24-Dec-2015) -- Start
'    'Enhancement - CCBRT WORK
'    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
'        Try
'            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
'            Dim xRow As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
'            mintItemRowIndex = -1
'            If mblnIsmatchWithComp Then
'                mstriEditingGUID = mdtCustomTabularGrid.Rows(xRow.RowIndex).Item("GUID")
'            Else
'                mstriEditingGUID = ""
'            End If

'            'Shani(23-FEB-2017) -- Start
'            btnIAdd.Text = "Add"
'            'Shani(23-FEB-2017) -- End

'            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
'                Dim xtmp() As DataRow = mdtCustomTabularGrid.Select("GUID <> ''")
'                Dim blnAllowMultiple As Boolean = False
'                Dim objCHeader As New clsassess_custom_header
'                objCHeader._Customheaderunkid = Me.ViewState("customheaderunkid")
'                blnAllowMultiple = objCHeader._Is_Allow_Multiple
'                objCHeader = Nothing

'                Dim objPlanning As New clsassess_plan_customitem_tran
'                Dim strMsg As String = String.Empty
'                strMsg = objPlanning.IsFinalSaved(mstriEditingGUID, cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), 1)
'                objPlanning = Nothing
'                If strMsg.Trim.Length > 0 Then
'                    DisplayMessage.DisplayMessage(strMsg, Me)
'                    Exit Sub
'                End If

'                If xtmp.Length > 0 AndAlso blnAllowMultiple = False Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
'                    Exit Sub
'                End If
'            End If
'            Call Generate_Popup_Data(Me.ViewState("customheaderunkid"))
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
'        Try
'            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
'            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
'            mintItemRowIndex = row.RowIndex
'            mstriEditingGUID = mdtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")

'            If mstriEditingGUID.Trim.Length <= 0 Then
'                DisplayMessage.DisplayMessage("Sorry, no custom item added in the list to edit.", Me)
'            End If

'            Dim objPlanning As New clsassess_plan_customitem_tran
'            Dim strMsg As String = String.Empty
'            strMsg = objPlanning.IsFinalSaved(mstriEditingGUID, cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), 2)
'            objPlanning = Nothing
'            If strMsg.Trim.Length > 0 Then
'                DisplayMessage.DisplayMessage(strMsg, Me)
'                Exit Sub
'            End If
'            'Shani(23-FEB-2017) -- Start
'            btnIAdd.Text = "Save"
'            'Shani(23-FEB-2017) -- End
'            Call Generate_Popup_Data(Me.ViewState("customheaderunkid"))
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
'        Try
'            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
'            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
'            mintItemRowIndex = row.RowIndex
'            mstriEditingGUID = mdtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")

'            If mstriEditingGUID.Trim.Length <= 0 Then
'                DisplayMessage.DisplayMessage("Sorry, no custom item added in the list to delete.", Me)
'            End If

'            Dim objPlanning As New clsassess_plan_customitem_tran
'            Dim strMsg As String = String.Empty
'            strMsg = objPlanning.IsFinalSaved(mstriEditingGUID, cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), 3)
'            objPlanning = Nothing
'            If strMsg.Trim.Length > 0 Then
'                DisplayMessage.DisplayMessage(strMsg, Me)
'                Exit Sub
'            End If

'            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count > 0 Then
'                popupDelete.Title = "Enter valid reason to void custom item planning."
'                popupDelete.Reason = ""
'                popupDelete.Show()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
'        Try
'            mblnItemAddEdit = False
'            popup_CItemAddEdit.Hide()
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    'Shani (26-Sep-2016) -- Start
'    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

'    'Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
'    '    Dim objPlaningItem As New clsassess_plan_customitem_tran
'    '    Try

'    '        Dim dgv As DataGridItem = Nothing
'    '        For Each xRow As DataRow In dtCItems.Rows
'    '            For i As Integer = 0 To dgv_Citems.Items.Count - 1
'    '                dgv = dgv_Citems.Items(i)
'    '                Select Case xRow("itemtypeid")
'    '                    Case clsassess_custom_items.enCustomType.FREE_TEXT
'    '                        If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
'    '                            Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
'    '                            xRow.Item("custom_value") = xtxt.Text
'    '                            Exit For
'    '                        End If
'    '                    Case clsassess_custom_items.enCustomType.SELECTION
'    '                        If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
'    '                            Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
'    '                            If xCbo.SelectedValue > 0 Then
'    '                                xRow.Item("custom_value") = xCbo.SelectedValue
'    '                                xRow.Item("selectedid") = xCbo.SelectedValue
'    '                                Exit For
'    '                            End If
'    '                        End If
'    '                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
'    '                        If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
'    '                            If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
'    '                                xRow.Item("custom_value") = eZeeDate.convertDate(CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate)
'    '                                xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
'    '                                Exit For
'    '                            End If
'    '                        End If
'    '                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'    '                        If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
'    '                            Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
'    '                            xRow.Item("custom_value") = xtxt.Text
'    '                            Exit For
'    '                        End If
'    '                End Select
'    '            Next
'    '        Next
'    '        Dim dtmp() As DataRow = Nothing
'    '        If dtCItems IsNot Nothing Then
'    '            dtCItems.AcceptChanges()
'    '            dtmp = dtCItems.Select("custom_value <> ''")
'    '            If dtmp.Length <= 0 Then
'    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), Me)
'    '                Exit Sub
'    '            End If
'    '            dtmp = dtCItems.Select("")
'    '        End If

'    '        If dtmp.Length > 0 Then
'    '            If mintItemRowIndex <= -1 Then
'    '                Dim iGUID As String = ""
'    '                iGUID = Guid.NewGuid.ToString
'    '                For iR As Integer = 0 To dtmp.Length - 1
'    '                    objPlaningItem._customitemunkid = dtmp(iR)("customitemunkid")
'    '                    objPlaningItem._custom_value = dtmp(iR)("custom_value")
'    '                    objPlaningItem._customheaderunkid = Me.ViewState("customheaderunkid")
'    '                    objPlaningItem._EmployeeUnkid = cboEmployee.SelectedValue
'    '                    objPlaningItem._Isvoid = False
'    '                    objPlaningItem._Periodunkid = cboPeriod.SelectedValue
'    '                    objPlaningItem._plancustomguid = iGUID
'    '                    objPlaningItem._transactiondate = ConfigParameter._Object._CurrentDateAndTime
'    '                    objPlaningItem._voiddatetime = Nothing
'    '                    objPlaningItem._voidreason = ""
'    '                    objPlaningItem._voiduserunkid = -1
'    '                    If objPlaningItem.Insert(Session("UserId")) = False Then
'    '                        DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
'    '                        Exit Sub
'    '                    End If
'    '                Next
'    '            Else
'    '                If dtmp.Length > 0 Then
'    '                    Dim dsItems = New DataSet
'    '                    dsItems = objPlaningItem.GetList(Session("Database_Name"), _
'    '                                        Session("UserId"), _
'    '                                        Session("Fin_year"), _
'    '                                        Session("CompanyUnkId"), _
'    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                        Session("UserAccessModeSetting"), True, _
'    '                                        Session("IsIncludeInactiveEmp"), "Cus_Items", _
'    '                                        cboEmployee.SelectedValue, cboPeriod.SelectedValue, Me.ViewState("customheaderunkid"), "plancustomguid = '" & mstriEditingGUID & "'")

'    '                    For iR As Integer = 0 To dtmp.Length - 1
'    '                        Dim xRow() As DataRow = dsItems.Tables(0).Select("customitemunkid = '" & dtmp(iR).Item("customitemunkid") & "' AND plancustomguid = '" & mstriEditingGUID & "'")

'    '                        objPlaningItem._plancustomunkid = xRow(0)("plancustomunkid")
'    '                        objPlaningItem._customitemunkid = dtmp(iR)("customitemunkid")
'    '                        objPlaningItem._custom_value = dtmp(iR)("custom_value")
'    '                        objPlaningItem._customheaderunkid = Me.ViewState("customheaderunkid")
'    '                        objPlaningItem._EmployeeUnkid = cboEmployee.SelectedValue
'    '                        objPlaningItem._Isvoid = False
'    '                        objPlaningItem._Periodunkid = cboPeriod.SelectedValue
'    '                        objPlaningItem._plancustomguid = mstriEditingGUID
'    '                        objPlaningItem._transactiondate = ConfigParameter._Object._CurrentDateAndTime
'    '                        objPlaningItem._voiddatetime = Nothing
'    '                        objPlaningItem._voidreason = ""
'    '                        objPlaningItem._voiduserunkid = -1
'    '                        If objPlaningItem.Update(Session("UserId")) = False Then
'    '                            DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
'    '                            Exit Sub
'    '                        End If

'    '                        'If xRow.Length > 0 Then

'    '                        '    xRow(0).Item("customanalysistranguid") = xRow(0).Item("customanalysistranguid")
'    '                        '    xRow(0).Item("analysisunkid") = xRow(0).Item("analysisunkid")
'    '                        '    xRow(0).Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
'    '                        '    xRow(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'    '                        '    Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
'    '                        '        Case clsassess_custom_items.enCustomType.FREE_TEXT
'    '                        '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
'    '                        '        Case clsassess_custom_items.enCustomType.SELECTION
'    '                        '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("selectedid")
'    '                        '        Case clsassess_custom_items.enCustomType.DATE_SELECTION
'    '                        '            If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
'    '                        '                xRow(0).Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
'    '                        '            Else
'    '                        '                xRow(0).Item("custom_value") = ""
'    '                        '            End If
'    '                        '        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'    '                        '            xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
'    '                        '    End Select
'    '                        '    xRow(0).Item("custom_header") = Me.ViewState("Header")
'    '                        '    xRow(0).Item("custom_item") = dtmp(iEdit).Item("custom_item")
'    '                        '    xRow(0).Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
'    '                        '    xRow(0).Item("AUD") = "U"
'    '                        '    xRow(0).Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
'    '                        '    xRow(0).Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
'    '                        'End If
'    '                    Next
'    '                End If
'    '            End If
'    '        End If
'    '        mblnItemAddEdit = False
'    '        popup_CItemAddEdit.Hide()
'    '        Call Fill_Gird()
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex, Me)
'    '    End Try
'    'End Sub

'    Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
'        Dim objPlaningItem As New clsassess_plan_customitem_tran
'        Try
'            Dim dgv As DataGridItem = Nothing
'            For Each xRow As DataRow In dtCItems.Rows
'                For i As Integer = 0 To dgv_Citems.Items.Count - 1
'                    dgv = dgv_Citems.Items(i)
'                    Select Case CInt(xRow("itemtypeid"))
'                        Case clsassess_custom_items.enCustomType.FREE_TEXT
'                            If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
'                                Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
'                                xRow.Item("custom_value") = xtxt.Text
'                                Exit For
'                            End If
'                        Case clsassess_custom_items.enCustomType.SELECTION
'                            If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
'                                Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
'                                If xCbo.SelectedValue > 0 Then
'                                    xRow.Item("custom_value") = xCbo.SelectedValue
'                                    xRow.Item("selectedid") = xCbo.SelectedValue
'                                    Exit For
'                                End If
'                            End If
'                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                            If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
'                                If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
'                                    xRow.Item("custom_value") = eZeeDate.convertDate(CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate)
'                                    xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
'                                    Exit For
'                                End If
'                            End If
'                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                            If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
'                                Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
'                                xRow.Item("custom_value") = xtxt.Text
'                                Exit For
'                            End If
'                    End Select
'                Next
'            Next
'            Dim dtmp() As DataRow = Nothing

'            If dtCItems IsNot Nothing Then
'                dtCItems.AcceptChanges()
'                dtmp = dtCItems.Select("custom_value <> ''")
'                If dtmp.Length <= 0 Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), Me)
'                    Exit Sub
'                End If
'                dtmp = dtCItems.Select("")

'                Dim dRow As DataRow = Nothing
'                If mintItemRowIndex <= -1 Then
'                    Dim iGUID As String = Guid.NewGuid.ToString
'                    For Each iR As DataRow In dtmp
'                        dRow = mdtCustomEvaluation.NewRow()
'                        dRow.Item("plancustomunkid") = -1
'                        dRow.Item("plancustomguid") = iGUID
'                        dRow.Item("customitemunkid") = iR.Item("customitemunkid")
'                        dRow.Item("custom_value") = iR.Item("custom_value")
'                        dRow.Item("periodunkid") = cboPeriod.SelectedValue
'                        dRow.Item("isvoid") = False
'                        dRow.Item("voiduserunkid") = -1
'                        dRow.Item("voiddatetime") = DBNull.Value
'                        dRow.Item("voidreason") = ""
'                        dRow.Item("display_value") = iR.Item("custom_value")
'                        dRow.Item("itemtypeid") = iR.Item("itemtypeid")
'                        dRow.Item("selectionmodeid") = iR.Item("selectedid")
'                        dRow.Item("AUD") = "A"
'                        dRow.Item("isdefaultentry") = iR.Item("isdefaultentry")
'                        dRow.Item("customheaderunkid") = Me.ViewState("customheaderunkid")
'                        dRow.Item("isfinal") = 0
'                        dRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
'                        dRow.Item("ismanual") = True
'                        mdtCustomEvaluation.Rows.Add(dRow)
'                    Next
'                Else
'                    If mstriEditingGUID <> "" Then
'                        For Each iR As DataRow In dtmp
'                            Dim dtRow() As DataRow = mdtCustomEvaluation.Select("plancustomguid ='" & mstriEditingGUID & "' AND customitemunkid = '" & iR.Item("customitemunkid") & "'")
'                            If dtRow.Length > 0 Then
'                                dtRow(0).Item("custom_value") = iR.Item("custom_value")
'                                dtRow(0).Item("display_value") = iR.Item("custom_value")
'                                dtRow(0).Item("selectionmodeid") = iR.Item("selectedid")
'                                If dtRow(0).Item("AUD").ToString.Trim = "" Then
'                                    dtRow(0).Item("AUD") = "U"
'                                End If
'                                dtRow(0).Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
'                            Else
'                                dRow = mdtCustomEvaluation.NewRow()
'                                dRow.Item("plancustomunkid") = -1
'                                dRow.Item("plancustomguid") = mstriEditingGUID
'                                dRow.Item("customitemunkid") = iR.Item("customitemunkid")
'                                dRow.Item("custom_value") = iR.Item("custom_value")
'                                dRow.Item("periodunkid") = cboPeriod.SelectedValue
'                                dRow.Item("isvoid") = False
'                                dRow.Item("voiduserunkid") = -1
'                                dRow.Item("voiddatetime") = DBNull.Value
'                                dRow.Item("voidreason") = ""
'                                dRow.Item("display_value") = iR.Item("custom_value")
'                                dRow.Item("itemtypeid") = iR.Item("itemtypeid")
'                                dRow.Item("selectionmodeid") = iR.Item("selectedid")
'                                dRow.Item("AUD") = "A"
'                                dRow.Item("isdefaultentry") = False
'                                dRow.Item("customheaderunkid") = Me.ViewState("customheaderunkid")
'                                dRow.Item("isfinal") = 0
'                                dRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
'                                dRow.Item("ismanual") = CBool(mdtCustomTabularGrid.Rows(mintItemRowIndex)("ismanual"))
'                                mdtCustomEvaluation.Rows.Add(dRow)
'                            End If
'                        Next
'                    End If
'                End If
'                mdtCustomEvaluation.AcceptChanges()
'            End If
'            objPlaningItem._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
'            objPlaningItem._Periodunkid = CInt(cboPeriod.SelectedValue)
'            If objPlaningItem.InsertUpdateDelete_CustomAnalysisTran(mdtCustomEvaluation, Session("UserId")) = False Then
'                DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
'                Exit Sub
'            Else
'                mblnItemAddEdit = False
'                popup_CItemAddEdit.Hide()
'                Call Fill_Gird()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    'Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
'    '    Try
'    '        If popupDelete.Reason.Trim.Length > 0 Then
'    '            Dim objPlaningItem As New clsassess_plan_customitem_tran
'    '            Dim dsItems As New DataSet
'    '            dsItems = objPlaningItem.GetList(Session("Database_Name"), _
'    '                                        Session("UserId"), _
'    '                                        Session("Fin_year"), _
'    '                                        Session("CompanyUnkId"), _
'    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                        Session("UserAccessModeSetting"), True, _
'    '                                        Session("IsIncludeInactiveEmp"), "Cus_Items", _
'    '                                        cboEmployee.SelectedValue, cboPeriod.SelectedValue, _
'    '                                        Me.ViewState("customheaderunkid"), "plancustomguid = '" & mstriEditingGUID & "'")

'    '            For Each row In dsItems.Tables(0).Rows
'    '                objPlaningItem._plancustomunkid = row("plancustomunkid")
'    '                objPlaningItem._voiduserunkid = Session("UserId")
'    '                objPlaningItem._voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'    '                objPlaningItem._Isvoid = True
'    '                objPlaningItem._voidreason = popupDelete.Reason.Trim
'    '                objPlaningItem.Delete()
'    '            Next

'    '            objPlaningItem = Nothing
'    '        End If
'    '        Call Fill_Gird()
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex, Me)
'    '    End Try
'    'End Sub

'    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
'        Try
'            If popupDelete.Reason.Trim.Length > 0 Then
'                Dim objPlaningItem As New clsassess_plan_customitem_tran

'                Dim dtRow() As DataRow = Nothing
'                If CBool(mdtCustomTabularGrid.Rows(mintItemRowIndex)("ismanual")) Then
'                    dtRow = mdtCustomEvaluation.Select("plancustomguid = '" & mstriEditingGUID & "'")
'                Else
'                    dtRow = mdtCustomEvaluation.Select("plancustomguid = '" & mstriEditingGUID & "' AND isdefaultentry = 'False' ")
'                End If

'                For Each row In dtRow
'                    row.Item("AUD") = "D"
'                    row.Item("voiduserunkid") = Session("UserId")
'                    row.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
'                    row.Item("voidreason") = popupDelete.Reason.Trim
'                    row.Item("isvoid") = True
'                Next
'                mdtCustomEvaluation.AsEnumerable()

'                objPlaningItem._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
'                objPlaningItem._Periodunkid = CInt(cboPeriod.SelectedValue)
'                If objPlaningItem.InsertUpdateDelete_CustomAnalysisTran(mdtCustomEvaluation, Session("UserId")) = False Then
'                    DisplayMessage.DisplayMessage(objPlaningItem._Message, Me)
'                    Exit Sub
'                Else
'                    mblnItemAddEdit = False
'                    popup_CItemAddEdit.Hide()
'                    Call Fill_Gird()
'                End If

'                objPlaningItem = Nothing
'            End If
'            Call Fill_Gird()
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'    'Shani (26-Sep-2016) -- End

'    'Shani(24-Dec-2015) -- End

'#End Region

'#Region "GridView Event(S)"

'    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
'        Try
'            If e.Row.RowType = DataControlRowType.DataRow Then
'                'Shani(24-Dec-2015) -- Start
'                'Enhancement - CCBRT WORK
'                'If dgvData.DataKeys(e.Row.RowIndex).Value.ToString.Trim.Length <= 0 Then
'                '    e.Row.CssClass = "GroupHeaderStyle"
'                '    e.Row.Cells(0).ColumnSpan = dgvData.Columns.Count
'                '    For i As Integer = 1 To dgvData.Columns.Count - 1
'                '        e.Row.Cells(i).Visible = False
'                '    Next
'                'End If
'                If mblnIsAddColumn = True Then

'                    'Shani(24-Feb-2016) -- Start
'                    'If dgvData.Rows.Count > 0 Then
'                    '    Dim imgAdd As New ImageButton()
'                    '    imgAdd.ID = "imgAdd"
'                    '    imgAdd.Attributes.Add("Class", "objAddBtn")
'                    '    imgAdd.CommandName = "objAdd"
'                    '    imgAdd.ImageUrl = "~/images/add_16.png"
'                    '    imgAdd.ToolTip = "New"
'                    '    AddHandler imgAdd.Click, AddressOf imgAdd_Click
'                    '    e.Row.Cells(0).Controls.Add(imgAdd)

'                    '    Dim imgEdit As New ImageButton()
'                    '    imgEdit.ID = "imgEdit"
'                    '    imgEdit.Attributes.Add("Class", "objAddBtn")
'                    '    imgEdit.CommandName = "objEdit"
'                    '    imgEdit.ImageUrl = "~/images/Edit.png"
'                    '    imgEdit.ToolTip = "Edit"
'                    '    AddHandler imgEdit.Click, AddressOf imgEdit_Click
'                    '    e.Row.Cells(1).Controls.Add(imgEdit)


'                    '    Dim imgDelete As New ImageButton()
'                    '    imgDelete.ID = "imgDelete"
'                    '    imgDelete.Attributes.Add("Class", "objAddBtn")
'                    '    imgDelete.CommandName = "objDelete"
'                    '    imgDelete.ImageUrl = "~/images/remove.png"
'                    '    imgDelete.ToolTip = "Delete"
'                    '    AddHandler imgDelete.Click, AddressOf imgDelete_Click
'                    '    e.Row.Cells(2).Controls.Add(imgDelete)
'                    'End If

'                    Dim imgAdd As New ImageButton()
'                    imgAdd.ID = "imgAdd"
'                    imgAdd.Attributes.Add("Class", "objAddBtn")
'                    imgAdd.CommandName = "objAdd"
'                    imgAdd.ImageUrl = "~/images/add_16.png"
'                    imgAdd.ToolTip = "New"
'                    AddHandler imgAdd.Click, AddressOf imgAdd_Click
'                    e.Row.Cells(0).Controls.Add(imgAdd)

'                    Dim imgEdit As New ImageButton()
'                    imgEdit.ID = "imgEdit"
'                    imgEdit.Attributes.Add("Class", "objAddBtn")
'                    imgEdit.CommandName = "objEdit"
'                    imgEdit.ImageUrl = "~/images/Edit.png"
'                    imgEdit.ToolTip = "Edit"
'                    AddHandler imgEdit.Click, AddressOf imgEdit_Click
'                    e.Row.Cells(1).Controls.Add(imgEdit)


'                    Dim imgDelete As New ImageButton()
'                    imgDelete.ID = "imgDelete"
'                    imgDelete.Attributes.Add("Class", "objAddBtn")
'                    imgDelete.CommandName = "objDelete"
'                    imgDelete.ImageUrl = "~/images/remove.png"
'                    imgDelete.ToolTip = "Delete"
'                    AddHandler imgDelete.Click, AddressOf imgDelete_Click
'                    e.Row.Cells(2).Controls.Add(imgDelete)

'                    'Shani(24-Feb-2016) -- End
'                Else
'                    If dgvData.DataKeys(e.Row.RowIndex).Value.ToString.Trim.Length <= 0 Then
'                        e.Row.CssClass = "GroupHeaderStyle"
'                        e.Row.Cells(0).ColumnSpan = dgvData.Columns.Count
'                        For i As Integer = 1 To dgvData.Columns.Count - 1
'                            e.Row.Cells(i).Visible = False
'                        Next
'                    End If
'                End If
'                'Shani(24-Dec-2015) -- End
'            End If
'        Catch ex As Exception

'        End Try
'    End Sub

'    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgv_Citems.ItemDataBound
'        Try
'            'dtCItems = Me.Session("dtCItems")
'            If e.Item.ItemIndex > -1 Then
'                If CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
'                    Dim txt As New TextBox
'                    txt.ID = "txt" & e.Item.Cells(4).Text
'                    txt.TextMode = TextBoxMode.MultiLine
'                    txt.Rows = 7 'Shani(23-FEB-2017) 
'                    txt.Style.Add("resize", "none")
'                    txt.Width = Unit.Percentage(100)
'                    txt.CssClass = "removeTextcss"
'                    'If CBool(e.Item.Cells(3).Text) Then
'                    '    txt.ReadOnly = True
'                    'End If
'                    If mintItemRowIndex > -1 OrElse mstriEditingGUID <> "" Then
'                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                    End If
'                    'Shani (26-Sep-2016) -- Start
'                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                    If CBool(e.Item.Cells(6).Text) = True Then
'                        txt.ReadOnly = True
'                    End If
'                    'Shani (26-Sep-2016) -- End
'                    e.Item.Cells(1).Controls.Add(txt)
'                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
'                    Dim dtp As Control
'                    dtp = LoadControl("~/Controls/DateCtrl.ascx")
'                    dtp.ID = "dtp" & e.Item.Cells(4).Text
'                    CType(dtp, Controls_DateCtrl).AutoPostBack = True
'                    'If CBool(e.Item.Cells(3).Text) Then
'                    '    CType(dtp, Controls_DateCtrl).Enabled = False
'                    'End If
'                    If mintItemRowIndex > -1 Then
'                        If dtCItems.Rows(e.Item.ItemIndex)("custom_value").ToString().Trim.Length > 0 Then
'                            CType(dtp, Controls_DateCtrl).SetDate = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                        End If

'                    End If
'                    AddHandler CType(dtp, Controls_DateCtrl).TextChanged, AddressOf dtpCustomItem_TextChanged
'                    e.Item.Cells(1).Controls.Add(dtp)
'                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
'                    Dim cbo As New DropDownList
'                    cbo.ID = "cbo" & e.Item.Cells(4).Text
'                    cbo.Width = Unit.Pixel(250)
'                    'If CBool(e.Item.Cells(3).Text) Then
'                    '    cbo.Enabled = False
'                    'End If
'                    Select Case CInt(e.Item.Cells(5).Text)
'                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'                            Dim objCMaster As New clsCommon_Master
'                            Dim dsList As New DataSet
'                            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
'                            With cbo
'                                .DataValueField = "masterunkid"
'                                .DataTextField = "name"
'                                .DataSource = dsList.Tables(0)
'                                .ToolTip = "name"
'                                .SelectedValue = 0
'                                .DataBind()
'                            End With
'                            objCMaster = Nothing
'                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                            Dim objCompetency As New clsassess_competencies_master
'                            Dim dsList As New DataSet
'                            dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)

'                            With cbo
'                                .DataValueField = "Id"
'                                .DataTextField = "Name"
'                                .DataSource = dsList.Tables(0)
'                                .ToolTip = "name"
'                                .SelectedValue = 0
'                                .DataBind()
'                            End With
'                            objCompetency = Nothing
'                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                            Dim objEmpField1 As New clsassess_empfield1_master
'                            Dim dsList As New DataSet
'                            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True)
'                            With cbo
'                                .DataValueField = "Id"
'                                .DataTextField = "Name"
'                                .DataSource = dsList.Tables(0)
'                                .ToolTip = "name"
'                                .SelectedValue = 0
'                                .DataBind()
'                            End With
'                            objEmpField1 = Nothing
'                    End Select
'                    If mintItemRowIndex > -1 Then
'                        cbo.SelectedValue = IIf(dtCItems.Rows(e.Item.ItemIndex)("custom_value") = "", 0, dtCItems.Rows(e.Item.ItemIndex)("custom_value"))
'                    End If
'                    e.Item.Cells(1).Controls.Add(cbo)
'                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
'                    Dim txt As New TextBox
'                    txt.ID = "txtnum" & e.Item.Cells(4).Text
'                    'If CBool(e.Item.Cells(3).Text) Then
'                    '    txt.ReadOnly = True
'                    'End If
'                    txt.Style.Add("text-align", "right")
'                    txt.Attributes.Add("onKeypress", "return onlyNumbers(this, event);")
'                    txt.Width = Unit.Percentage(100)
'                    txt.CssClass = "removeTextcss"

'                    If mintItemRowIndex > -1 Then
'                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                    End If
'                    e.Item.Cells(1).Controls.Add(txt)
'                End If




'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'#End Region

'#Region "Controls"

'    Protected Sub dtpCustomItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            'If CDate(dtpAssessdate.GetDate) >= CDate(CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).GetDate) Then
'            '    CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).SetDate = Nothing
'            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
'            '    CType(sender, Controls_DateCtrl).Focus()
'            'End If

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex, Me)
'        End Try
'    End Sub

'#End Region

'End Class
