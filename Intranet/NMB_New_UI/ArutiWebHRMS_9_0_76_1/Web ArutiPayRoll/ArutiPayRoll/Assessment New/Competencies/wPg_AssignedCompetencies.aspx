﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AssignedCompetencies.aspx.vb"
    Inherits="wPg_AssignedCompetencies" Title="Assigned Competencies List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="EmployeeList" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <script>
        //$(".itemweight").live("focusout", function() {
        $("body").on("focusout", '.itemweight', function() {
            if ($(this).val() == "") {
                $(this).val("0.00");
            } else {
                if (parseFloat($(this).val()) <= 0) {
                    $(this).val("0.00");
                }
                else {
                    var dectotalCout = 0;
                    var decweight = '#<%= txtWeight.ClientID %>'
                    $(".itemweight").each(function(index, element) {
                        dectotalCout = dectotalCout + parseFloat($(element).val());
                    });
                    if (dectotalCout > parseFloat($(decweight).val())) {
                        alert('Sorry you cannot assign more items. Reason the selected group exceeds the total weight of ' + $(decweight).val());
                        $(this).focus();
                    } else {
                        $(this).val(parseFloat($(this).val()).toFixed(2));
                    }
                }
            }
        })
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Assigned Competencies List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <asp:CheckBox ID="chkJobCompetencies" runat="server" Text="Assign by Job Competencies"
                                            Font-Bold="true" Checked="true" Font-Size="12px" Visible="false" CssClass="pull-right" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblAssessGroup" runat="server" Text="Assess Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAssessGroup" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="lblWeight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtWeight" runat="server" ReadOnly="true" AutoPostBack="true" Style="text-align: right;"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:CheckBox ID="chkZeroCompetenceWeight" runat="server" Text="Allow Competence Weightage to be Zero"
                                    Font-Bold="true" Font-Size="12px" CssClass="pull-left" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="lvData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" Width="99%">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkItemSelect" runat="server" CssClass="filled-in" Text=" " AutoPostBack="true"
                                                                Checked='<%#Eval("ischeck") %>' OnCheckedChanged="chkItemSelect_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="competence" HeaderText="Competencies" FooterText="dgcolhCompetencies"
                                                        ItemStyle-Width="871px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="scale" HeaderText="Scale" FooterText="dgcolhScale" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Weightage" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtItemWeight" runat="server" CssClass="form-control decimal" Text='<%# Eval("weight") %>'
                                                                Width="95%" Style="text-align: right;" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="GrpId" HeaderText="GrpId" FooterText="objdgcolhGrpId"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsGrp" HeaderText="IsGrp" FooterText="objdgcolhIsGrp"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="assigncompetenceunkid" HeaderText="assigncompetenceunkid"
                                                        FooterText="objdgcolhAssignMasterId" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="assigncompetencetranunkid" HeaderText="assigncompetencetranunkid"
                                                        FooterText="objdgcolhAssignTranId" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="" HeaderText="GUID" FooterText="objdgcolhGUID" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="competenciesunkid" FooterText="" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsPGrp" FooterText="" Visible="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                        HeaderText="Info." ItemStyle-Width="90px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                Font-Underline="false"><i
                class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="hidden" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc3:DeleteReason ID="delReason" runat="server" CancelControlName="hidden" Title="Aruti" />
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalPopupBG2"
                    CancelControlID="btnSClose" PopupControlID="Panel1" TargetControlID="hdnfieldDelReason">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblTitle" runat="server" Text="Description" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtData" runat="server" CssClass="form-control" TextMode="MultiLine"
                                            ReadOnly="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSOk" runat="server" Text="Ok" Width="70px" SCssClass="btn btn-primary"
                            Visible="false" />
                        <asp:Button ID="btnSClose" runat="server" Text="Close" Width="70px" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                    </div>
                </asp:Panel>
                <%--OLD--%>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                        </div>
                                        <div style="float: right;">
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%;">
                                                <td style="width: 5%;">
                                                </td>
                                                <td style="width: 25%;">
                                                </td>
                                                <td style="width: 10%;">
                                                </td>
                                                <td style="width: 25%;">
                                                </td>
                                                <td style="width: 5%;">
                                                </td>
                                                <td style="width: 15%;">
                                                </td>
                                                <td style="width: 5%;">
                                                </td>
                                                <td style="width: 10%;">
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                        </div>
                                    </div>
                                </div>
                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                    height: 400px; overflow: auto" class="gridscroll">
                                    <asp:Panel ID="pnl_lvData" runat="server">
                                    </asp:Panel>
                                </div>
                                <div class="btn-default">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
