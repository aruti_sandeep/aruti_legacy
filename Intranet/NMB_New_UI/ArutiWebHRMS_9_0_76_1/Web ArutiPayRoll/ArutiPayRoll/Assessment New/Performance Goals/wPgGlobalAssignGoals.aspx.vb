﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
#End Region

Partial Class Assessment_New_Performance_Goals_wPgGlobalAssignGoals
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private objOwrOwner As New clsassess_owrowner_tran
    Private ReadOnly mstrModuleName As String = "frmGlobalAssignGoals"

    'SHANI (11 May 2015) -- Start
    Dim dtFinalTab As DataTable = Nothing
    'SHANI (11 May 2015) -- End 

#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                If Session("iOwnerTypeId") IsNot Nothing Then
                    Dim mintOwrFldTypeId As Integer = -1
                    Me.ViewState.Add("OwnerTypeId", Session("iOwnerTypeId"))
                    Me.ViewState.Add("mintSelectedPeriodId", 0)
                    FillCombo()
                    Dim mdtOwner As New DataTable
                    Dim dtOwner As New DataTable
                    mdtOwner = objOwrOwner.Get_Data(-1, mintOwrFldTypeId)
                    Me.ViewState.Add("mdtOwner", mdtOwner)
                    Me.ViewState.Add("dtOwner", dtOwner)
                    Me.ViewState.Add("dtFinalTab", dtOwner)
                    Me.ViewState.Add("mintExOrder", 0)
                    Me.ViewState.Add("AdvanceFilter", "")
                    Me.ViewState.Add("strLinkedFld", "")
                    Me.ViewState.Add("OwrFldTypeId", mintOwrFldTypeId)
                Else
                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
                End If
            Else
                dtFinalTab = Me.ViewState("dtFinalTab")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If IsPostBack = False Then
            Session.Remove("iOwnerTypeId")
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Try
            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)


            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1, False, False, True)
            'S.SANDEEP |02-MAR-2021| -- END


            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                Try
                .SelectedValue = intCurrentPeriodId
                Catch ex As Exception
                    .SelectedValue = 0
                End Try                
            End With
            'Shani (09-May-2016) -- [.SelectedValue = intCurrentPeriodId]

            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(Me.ViewState("OwnerTypeId"))
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "stationunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0).Copy
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "deptgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "departmentunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "sectiongroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "sectionunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "unitgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "unitunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "teamunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "jobgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "jobunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "classgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "classesunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Set_Caption()
        Dim mintLinkedFld As Integer
        Dim mintExOrder As Integer
        Dim mintOwrFldTypeId As Integer
        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(cboPeriod.SelectedValue)
            mintLinkedFld = objFMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            Dim objFMaster As New clsAssess_Field_Master
            mintExOrder = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMaster = Nothing : objFMapping = Nothing
            lblOwrItemHeader.Text = strLinkedFld & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Information")
            Me.ViewState("mintExOrder") = mintExOrder
            Me.ViewState("strLinkedFld") = strLinkedFld
            Me.ViewState("OwrFldTypeId") = mintOwrFldTypeId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data(ByVal dtStDate As Date, ByVal dtEdDate As Date)

        Dim mstrDefaultFilter As String = ""
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwner As DataTable
        Dim dtOwnerView As DataView
        Dim mstrAdvanceFilter As String = CType(Me.ViewState("AdvanceFilter"), String)
        mstrDefaultFilter = "hremployee_master.isapproved = 1"
        Try
            Select Case CInt(Me.ViewState("OwnerTypeId"))
                Case enAllocation.BRANCH
                    mstrDefaultFilter &= " AND hremployee_master.stationunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    mstrDefaultFilter &= " AND hremployee_master.deptgroupunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.DEPARTMENT
                    mstrDefaultFilter &= " AND hremployee_master.departmentunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.SECTION_GROUP
                    mstrDefaultFilter &= " AND hremployee_master.sectiongroupunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.SECTION
                    mstrDefaultFilter &= " AND hremployee_master.sectionunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.UNIT_GROUP
                    mstrDefaultFilter &= " AND hremployee_master.unitgroupunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.UNIT
                    mstrDefaultFilter &= " AND hremployee_master.unitunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.TEAM
                    mstrDefaultFilter &= " AND hremployee_master.teamunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.JOB_GROUP
                    mstrDefaultFilter &= " AND hremployee_master.jobgroupunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.JOBS
                    mstrDefaultFilter &= " AND hremployee_master.jobunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.CLASS_GROUP
                    mstrDefaultFilter &= " AND hremployee_master.classgroupunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                Case enAllocation.CLASSES
                    mstrDefaultFilter &= " AND hremployee_master.classunkid = '" & CInt(cboOwner.SelectedValue) & "' "
            End Select

            If dtStDate = Nothing Then eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            If dtEdDate = Nothing Then eZeeDate.convertDate(Session("EmployeeAsOnDate"))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, dtStDate, dtEdDate, , Session("AccessLevelFilterString"), , mstrDefaultFilter & IIf(mstrAdvanceFilter.Trim.Length > 0, " AND " & mstrAdvanceFilter, "").ToString)
            dList = objEmployee.GetList(Session("Database_Name"), _
                                        Session("UserId"), _
                                        Session("Fin_year"), _
                                        Session("CompanyUnkId"), _
                                        dtStDate, dtEdDate, _
                                         Session("UserAccessModeSetting"), _
                                        True, False, "iList", _
                                        Session("ShowFirstAppointmentDate"), , , _
                                        mstrDefaultFilter & IIf(mstrAdvanceFilter.Trim.Length > 0, " AND " & mstrAdvanceFilter, "").ToString)
            'Shani(24-Aug-2015) -- End
            If dList.Tables(0).Rows.Count > 0 Then
                dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            End If
            dtOwner = dList.Tables(0)
            Dim xRow = From iRow In dtOwner.AsEnumerable() Where iRow.IsNull("ischeck") = True Select iRow
            For Each mRow As DataRow In xRow
                mRow.Item("ischeck") = False
            Next
            dtOwnerView = dtOwner.DefaultView
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            "name LIKE '%" & txtSearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvOwner.AutoGenerateColumns = False
            dgvOwner.DataSource = dtOwnerView
            dgvOwner.DataBind()
            Me.ViewState("dtOwner") = dtOwner

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Items_Grid()
        'Dim dtFinalTab As New DataTable
        Dim dtFinalTabView As DataView
        Try

            Dim objOwner As New clsassess_owrfield1_master
            dtFinalTab = objOwner.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinalTab.Columns.Add("fcheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dgvItems.AutoGenerateColumns = False
            Select Case CInt(Me.ViewState("mintExOrder"))
                Case enWeight_Types.WEIGHT_FIELD1
                    dtFinalTab.Columns("Field1").ColumnName = "OwrFieldItem"
                Case enWeight_Types.WEIGHT_FIELD2
                    dtFinalTab.Columns("Field2").ColumnName = "OwrFieldItem"
                Case enWeight_Types.WEIGHT_FIELD3
                    dtFinalTab.Columns("Field3").ColumnName = "OwrFieldItem"
                Case enWeight_Types.WEIGHT_FIELD4
                    dtFinalTab.Columns("Field4").ColumnName = "OwrFieldItem"
                Case enWeight_Types.WEIGHT_FIELD5
                    dtFinalTab.Columns("Field5").ColumnName = "OwrFieldItem"
            End Select
            Dim xRow = From iRow In dtFinalTab.AsEnumerable() Where iRow.IsNull("fcheck") = True Select iRow
            For Each mRow As DataRow In xRow
                mRow.Item("fcheck") = False
            Next
            dtFinalTabView = dtFinalTab.DefaultView
            Dim strSearch As String = ""

            'SHANI (11 May 2015) -- Start
            'Enhancement - 
            'If txtSearchItems.Text.Trim.Length > 0 Then
            '    strSearch = "OwrFieldItem LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "Ed_Date LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "CStatus LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "St_Date LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "Weight LIKE '%" & txtSearchItems.Text & "%'"
            '    dtFinalTabView.RowFilter = strSearch
            'End If
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If

            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            If dgvItems.Columns.Count > 0 Then dgvItems.Columns.Clear()
            'Pinkal (16-Apr-2016) -- End


            For Each dCol As DataColumn In dtFinalTab.Columns
                If dCol.ColumnName = "fcheck" Then Continue For
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                dgvCol.HtmlEncode = False
                If dgvItems.Columns.Contains(dgvCol) = True Then Continue For

                If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    If dgvCol.DataField.StartsWith("Owr") Then
                        dgvCol.Visible = False
                    End If

                End If

                If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                Else
                    If dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        Dim decColumnWidth As Decimal = 0
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If

                'S.SANDEEP [01 JUL 2015] -- START
                If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [01 JUL 2015] -- END

                dgvItems.Columns.Add(dgvCol)

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties.Add("index", dgvItems.Columns.IndexOf(dgvCol))
                'Pinkal (16-Apr-2016) -- End


            Next
            'SHANI (11 May 2015) -- End
            dgvItems.DataSource = dtFinalTabView
            dgvItems.DataBind()
            'Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidOwner(ByVal dtFinalTab As DataTable, ByVal dtOwner As DataTable) As Boolean
        Try
            dtFinalTab.AcceptChanges() : dtOwner.AcceptChanges()
            Dim xRow() As DataRow = Nothing
            xRow = dtFinalTab.Select("fcheck = true")
            If xRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, please check atleast one of the") & " " & Me.ViewState("strLinkedFld") & " " & _
                                Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "goal to assign owner."), Me)
                Return False
            End If

            xRow = dtOwner.Select("ischeck = true")
            If xRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Sorry, please check atleast one of the owners to assign them goals."), Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END
    Protected Sub btnSeach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSeach.Click
        Dim mdtStDate As Date
        Dim mdtEdDate As Date
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Please select Period and Owner to do global assign operation."), Me)
                Exit Sub
            End If
            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            mdtStDate = objPrd._Start_Date
            mdtEdDate = objPrd._End_Date
            objPrd = Nothing
            cboOwner.Focus()
            Call Fill_Data(mdtStDate.Date, mdtEdDate.Date)
            Call Fill_Items_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Dim mdtOwner As DataTable = CType(Me.ViewState("mdtOwner"), DataTable)
        Try
            Dim iFlag As Boolean = False
            If dtFinalTab IsNot Nothing AndAlso dtOwner IsNot Nothing Then
                If IsValidOwner(dtFinalTab, dtOwner) = False Then Exit Sub
                Dim dGoals(), dOwner() As DataRow
                dGoals = dtFinalTab.Select("fcheck = true")
                If dGoals.Length > 0 Then
                    dOwner = dtOwner.Select("ischeck = true")
                    If dOwner.Length > 0 Then
                        Dim iOwnerFldId As Integer = 0
                        For mGoalIdx As Integer = 0 To dGoals.Length - 1
                            'objlnkCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Processing") & " " & (mGoalIdx + 1).ToString & "/" & dGoals.Length.ToString
                            For mEmpIdx As Integer = 0 To dOwner.Length - 1
                                Dim dRow As DataRow = mdtOwner.NewRow
                                dRow.Item("ownertranunkid") = -1
                                Select Case CInt(Me.ViewState("mintExOrder"))
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        dRow.Item("owrfieldunkid") = dGoals(mGoalIdx).Item("owrfield1unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("owrfield1unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        dRow.Item("owrfieldunkid") = dGoals(mGoalIdx).Item("owrfield2unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("owrfield2unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        dRow.Item("owrfieldunkid") = dGoals(mGoalIdx).Item("owrfield3unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("owrfield3unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD4
                                        dRow.Item("owrfieldunkid") = dGoals(mGoalIdx).Item("owrfield4unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("owrfield4unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        dRow.Item("owrfieldunkid") = dGoals(mGoalIdx).Item("owrfield5unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("owrfield5unkid"))
                                End Select
                                dRow.Item("employeeunkid") = dOwner(mEmpIdx).Item("employeeunkid")
                                dRow.Item("owrfieldtypeid") = Me.ViewState("OwrFldTypeId")
                                dRow.Item("AUD") = "A"
                                dRow.Item("GUID") = Guid.NewGuid.ToString
                                mdtOwner.Rows.Add(dRow)

                                If mdtOwner IsNot Nothing Then
                                    Dim objOwner As New clsassess_owrowner_tran
                                    objOwner._DatTable = mdtOwner.Copy
                                    If objOwner.GlobalAssign_Owner(Session("UserId"), iOwnerFldId, Me.ViewState("OwrFldTypeId")) = False Then
                                        objOwner = Nothing : iFlag = False
                                        Exit Sub
                                    Else
                                        iFlag = True
                                    End If
                                    objOwner = Nothing
                                End If
                            Next
                        Next
                    End If
                    If iFlag = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Goals assigned successfully."), Me)
                        Dim xRowItem = From iRow In dtFinalTab.AsEnumerable() Where iRow.IsNull("fcheck") = True Select iRow
                        For Each mRow As DataRow In xRowItem
                            mRow.Item("fcheck") = False
                        Next

                        Dim xRowEmp = From iRow In dtOwner.AsEnumerable() Where iRow.IsNull("ischeck") = True Select iRow
                        For Each mRow As DataRow In xRowEmp
                            mRow.Item("ischeck") = False
                        Next

                        Dim objPrd As New clscommom_period_Tran

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
                        objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                        'Shani(20-Nov-2015) -- End

                        Dim mdtStDate As Date = objPrd._Start_Date
                        Dim mdtEdDate As Date = objPrd._End_Date
                        objPrd = Nothing
                        txtSearchEmp.Text = ""

                        'SHANI (11 May 2015) -- Start
                        'Enhancement - 
                        'txtSearchItems.Text = ""
                        'SHANI (11 May 2015) -- End

                        Call Fill_Data(mdtStDate.Date, mdtEdDate.Date)
                        Call Fill_Items_Grid()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Control Event"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboOwner.SelectedIndexChanged
        'Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Dim mdtOwner As DataTable = CType(Me.ViewState("mdtOwner"), DataTable)
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                dgvItems.DataSource = Nothing : dgvItems.DataBind()
                dgvOwner.DataSource = Nothing : dgvOwner.DataBind()
                If dtFinalTab IsNot Nothing Then dtFinalTab.Rows.Clear()
                If dtOwner IsNot Nothing Then dtOwner.Rows.Clear()
                If mdtOwner IsNot Nothing Then mdtOwner.Rows.Clear()
            ElseIf CInt(cboPeriod.SelectedValue) > 0 Then
                Call Set_Caption()
            End If
            'Me.ViewState("dtFinalTab") = dtFinalTab
            Me.ViewState("dtOwner") = dtOwner
            Me.ViewState("mdtOwner") = mdtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgbtnReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnReset.Click
        Dim mdtStDate As Date
        Dim mdtEdDate As Date
        Try
            Me.ViewState("AdvanceFilter") = ""
            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            mdtStDate = objPrd._Start_Date
            mdtEdDate = objPrd._End_Date
            objPrd = Nothing
            Call Fill_Data(mdtStDate.Date, mdtEdDate.Date)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Dim strAdvanceSearch As String
        Dim mdtStDate As Date
        Dim mdtEdDate As Date
        Try
            strAdvanceSearch = popupAdvanceFilter._GetFilterString
            Me.ViewState("AdvanceFilter") = strAdvanceSearch
            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            mdtStDate = objPrd._Start_Date
            mdtEdDate = objPrd._End_Date
            objPrd = Nothing
            Call Fill_Data(mdtStDate, mdtEdDate)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkSelect"), CheckBox)
                chk.Checked = chkAll.Checked
                dtOwner.Rows(iRow.ItemIndex)("ischeck") = chkAll.Checked
            Next
            dtOwner.AcceptChanges()
            Me.ViewState("dtOwner") = dtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkItemAllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            For Each iRow As GridViewRow In dgvItems.Rows
                Dim chk As CheckBox = CType(iRow.FindControl("chkItemSelect"), CheckBox)
                chk.Checked = chkAll.Checked
                dtFinalTab.Rows(iRow.RowIndex)("fcheck") = chkAll.Checked
            Next
            dtFinalTab.AcceptChanges()
            'Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim xRow As DataGridItem = CType(chk.NamingContainer, DataGridItem)
            If xRow.ItemIndex >= 0 Then
                dtOwner.Rows(xRow.ItemIndex)("ischeck") = chk.Checked
            End If
            dtOwner.AcceptChanges()
            Me.ViewState("dtOwner") = dtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkItemSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            Dim chkItem As CheckBox = CType(sender, CheckBox)
            Dim xRow As GridViewRow = CType(chkItem.NamingContainer, GridViewRow)
            If xRow.RowIndex >= 0 Then
                dtFinalTab.Rows(xRow.RowIndex)("fcheck") = chkItem.Checked
            End If
            dtFinalTab.AcceptChanges()
            'Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtSearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Dim mdtStDate As Date
        Dim mdtEdDate As Date
        Try
            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            mdtStDate = objPrd._Start_Date
            mdtEdDate = objPrd._End_Date
            objPrd = Nothing
            Call Fill_Data(mdtStDate.Date, mdtEdDate.Date)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'SHANI (11 May 2015) -- Start
    'Enhancement - 
    'Protected Sub txtSearchItems_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchItems.TextChanged
    '    Try
    '        Call Fill_Items_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI (11 May 2015) -- End 

#End Region

#Region "GridView Event(S)"

    'SHANI (11 May 2015) -- Start
    'Enhancement - 
    'Protected Sub dgvItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvItems.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Header Then
    '            Dim lbl As Label = CType(e.Item.FindControl("lblOwrFieldGridHeader"), Label)
    '            If lbl Is Nothing Then Exit Sub
    '            lbl.Text = Me.ViewState("strLinkedFld")
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI (11 May 2015) -- End

    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Protected Sub dgvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Call SetDateFormat()
                Dim intIndex As Integer = 0
                intIndex = dtFinalTab.Columns("St_Date").ExtendedProperties("index")
                If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                End If

                intIndex = dtFinalTab.Columns("Ed_Date").ExtendedProperties("index")
                If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (16-Apr-2016) -- End

#End Region

   
End Class
