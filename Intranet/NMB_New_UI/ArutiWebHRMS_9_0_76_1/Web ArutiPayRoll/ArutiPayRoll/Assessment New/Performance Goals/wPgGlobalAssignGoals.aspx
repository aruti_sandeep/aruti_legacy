<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgGlobalAssignGoals.aspx.vb"
    Inherits="Assessment_New_Performance_Goals_wPgGlobalAssignGoals" Title="Owners Global Assign Goals" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Owners Global Assign Goals"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Goal Owner And Period Detail"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblOwner" runat="server" Text="Owner" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboOwner" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnSeach" runat="server" Text="Search" CssClass="btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <asp:Label ID="lblAssignedTo" runat="server" Text="Assigned To"></asp:Label>
                                                        <ul class="header-dropdown m-r-25  p-l-0">
                                                            <asp:LinkButton ID="lnkAllocation" Font-Underline="false" runat="server" ToolTip="Advance Filter">
                                                                          <i class="fas fa-sliders-h"></i>
                                                            </asp:LinkButton>
                                                        </ul>
                                                        <ul class="header-dropdown m-r--14  p-l-0">
                                                            <asp:LinkButton ID="imgbtnReset" runat="server" ToolTip="Reset"><i class="fas fa-sync-alt"></i></asp:LinkButton>
                                                        </ul>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 215px">
                                                                    <asp:DataGrid ID="dgvOwner" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                        Width="99%" AllowPaging="false">
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" CssClass="filled-in"
                                                                                        Text=" " OnCheckedChanged="chkAllSelect_CheckedChanged" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" CssClass="filled-in"
                                                                                        Text=" " Checked='<%# Eval("ischeck")%>' OnCheckedChanged="chkSelect_CheckedChanged" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                HeaderStyle-Width="80px" ItemStyle-Width="80px"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                HeaderStyle-Width="239px" ItemStyle-Width="239px"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="employeeunkid" Visible="false" FooterText="objdgcolhEmpId">
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblOwrItemHeader" runat="server" Text=""></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 580px">
                                                                    <asp:GridView ID="dgvItems" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                        Width="99%" AllowPaging="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkItemAllSelect" runat="server" CssClass="filled-in" Text=" "
                                                                                        AutoPostBack="true" OnCheckedChanged="chkItemAllSelect_CheckedChanged" /></HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkItemSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("fcheck") %>'
                                                                                        CssClass="filled-in" Text=" " OnCheckedChanged="chkItemSelect_CheckedChanged" /></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
