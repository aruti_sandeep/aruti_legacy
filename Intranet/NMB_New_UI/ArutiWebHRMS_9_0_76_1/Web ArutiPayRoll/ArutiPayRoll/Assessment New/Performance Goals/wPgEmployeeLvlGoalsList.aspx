﻿<%@ Page Title="Employee Goals List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmployeeLvlGoalsList.aspx.vb" Inherits="wPgEmployeeLvlGoalsList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfDialog" TagPrefix="cf1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <%--<link href="../../App_Themes/PA_Style.css" type="text/css" />

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        //        $(".objAddBtn").live("mousedown", function(e) {
        //            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
        //            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        //        });

        $("body").on("mousedown", ".lnAdd", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
        $("body").on("mousedown", ".lnEdit", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
        $("body").on("mousedown", ".lnDelt", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
    </script>

    <script type="text/javascript">
        //$(".popMenu").live("click", function(e) {
        $("body").on("click", '.popMenu', function(e) {
            var id = jQuery(this).attr('id').replace('_backgroundElement', '');
            $find(id).hide();
        });
    </script>

    <script type="text/javascript">
        function ClosePopup(ctrl) {
            $find(id).hide();
        }

        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        //$("input[id$=btnOperation]").live("click", function() {
        $("body").on("click", 'input[id$=btnOperation]', function() {
            x = $("#<%= btnOperation.ClientID %>").offset();
            $("#<%= hdf_locationx.ClientID %>").val(x.top);
            $("#<%= hdf_locationy.ClientID %>").val(x.left);
        });
    </script>

    <script type="text/javascript">
        function setscrollPosition(sender) {
            sender.scrollLeft = document.getElementById("<%=hdf_leftposition.ClientID%>").value;
            sender.scrollTop = document.getElementById("<%=hdf_topposition.ClientID%>").value;
        }
        function getscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("pnl_dgvData").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("pnl_dgvData").scrollLeft;
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
            $(".lnAdd").html("<i class='fas fa-plus'></i>");
            $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
            $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
        }
        $(document).ready(function() {
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
            $(".lnAdd").html("<i class='fas fa-plus'></i>");
            $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
            $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
        });
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }

        //        $("[id*=chkHeder1]").live("click", function() {
        //            var chkHeader = $(this);
        //            var grid = $(this).closest("table");
        //            $("input[type=checkbox]", grid).each(function() {
        //                if (chkHeader.is(":checked")) {
        //                    debugger;
        //                    $(this).attr("checked", "checked");

        //                } else {
        //                    $(this).removeAttr("checked");
        //                }
        //            });
        //        });
        //        $("[id*=chkbox1]").live("click", function() {
        //            var grid = $(this).closest("table");
        //            var chkHeader = $("[id*=chkHeader]", grid);
        //            var row = $(this).closest("tr")[0];

        //            debugger;
        //            if (!$(this).is(":checked")) {
        //                var row = $(this).closest("tr")[0];
        //                chkHeader.removeAttr("checked");

        //            } else {

        //                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
        //                    chkHeader.attr("checked", "checked");
        //            }
        //        }    

        //        });    
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server" EnableViewState="true">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Goals List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                    <%--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="row clearfix">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblMHeader" runat="server" Text="Mandatory Filters" />
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                        </div>
                                                        <div class="row clearfix">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                                <% If Session("CompanyGroupName") <> "NMB PLC" Then%>
                                <div class="row clearfix">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="lblPerspective" runat="server" Text="Perspective" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboPerspective" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="objlblField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboFieldValue1" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="objlblField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboFieldValue2" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="objlblField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboFieldValue3" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="objlblField4" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboFieldValue4" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="objlblField5" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboFieldValue5" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-xs-4 col-sm-4 col-md-4 col-xs-4">
                                                                <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8 col-md-8 col-xs-8">
                                                            </div>
                                                        </div>
                                <%--<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblOHeader" runat="server" Text="Optional Filters" />
                                                </h2>
                                                    </div>
                                            <div class="body">
                                                </div>
                                            </div>
                                        </div>
                                </div>--%>
                                <% End If%>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblCurrentStatus" runat="server" Font-Bold="true" Font-Size="Small"
                                    CssClass="pull-left" ForeColor="Red"></asp:Label>
                                <asp:Label ID="objlblTotalWeight" runat="server" Text="" Font-Bold="true"></asp:Label>
                                <asp:Button ID="btnOperation" runat="server" Text="Operation" CssClass="btn btn-primary"
                                    Visible="False" />
                                <%--'S.SANDEEP |03-SEP-2022| -- START--%>
                                <%--'ISSUE/ENHANCEMENT : Sprint_2022-13--%>
                                <%-- <asp:Button ID="btnSubmitforApproval" runat="server" Text="Submit for Approval" CssClass="btn btn-primary" />--%>
                                <%--'S.SANDEEP |03-SEP-2022| -- END--%>
                                <%--<asp:Button ID="btnGlobalAssign" runat="server" CssClass="btn btn-primary" Text="Global Assign Goals" />--%>
                                <% If Session("CompanyGroupName") <> "NMB PLC" Then%>
                                <asp:Button ID="BtnSearch" runat="server" CssClass="btn btn-primary" Text="Search" />
                                <asp:Button ID="BtnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <% End If%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                 <h2>
                                        <asp:Label ID="lblHeader" runat="server" Text="Objectives/Goals List"></asp:Label>
                                    </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown" style="padding-right: 60px;">
                                            <asp:Label ID="lblViewList" Text="" runat="server" CssClass="form-label pull-left" />
                                            <asp:RadioButton ID="rdbAll" runat="server" AutoPostBack="true" CssClass="with-gap"
                                                GroupName="View" Text="All" Checked="true"  />
                                            <asp:RadioButton ID="rdbApprove" runat="server" AutoPostBack="true" CssClass="with-gap"
                                                GroupName="View" Text="Approved"  />
                                            <asp:RadioButton ID="rdbReject" runat="server" AutoPostBack="true" CssClass="with-gap"
                                                GroupName="View" Text="Rejected" />
                                        </li>
                                   
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div id="pnl_dgvData" class="table-responsive" onscroll="$(scroll1.Y).val(this.scrollTop);"
                                            style="height: 350px">
                                            <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="false" Width="99%"
                                                AllowPaging="false" CssClass="table table-hover table-bordered" DataKeyNames="isfinal,UoMType,empfield1unkid,empfield2unkid,empfield3unkid,empfield4unkid,empfield5unkid,goaltypeid,goalvalue,Field1,Field2,Field3,Field4,Field5,opstatusid,GoalStatusTypeId">
                                                <Columns>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="btn-group m-l-0" style="float: left">
                                    <asp:LinkButton ID="cmnuOperation" runat="server" Text="Operation" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Operation<span class="caret"></span>
                                    </asp:LinkButton>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <asp:LinkButton ID="btnUnlockEmployee" runat="server" Text="Unlock Employee(s)"></asp:LinkButton></li>
                                    </ul>
                                </div>
                                <%--<uc3:OperationButton ID="cmnuOperation" class="btn btn-primary pull-left" runat="server"
                                    Text="Operation" TragateControlId="data" />
                                <div id="data" style="display: none">
                                    <asp:LinkButton ID="btnUnlockEmployee" runat="server" Style="min-width: 200px" Text="Unlock Employee(s)"></asp:LinkButton>
                                </div>--%>
                                <div style="float: left;" class="m-t-5">
                                    <asp:Button ID="btnSubmitforApproval" runat="server" Text="Submit for Approval" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnGlobalAssign" runat="server" CssClass="btn btn-primary" Text="Global Assign Goals" />
                                    <% If Session("CompanyGroupName") = "NMB PLC" Then%>
                                    <asp:Button ID="btnUpdateGoalProgress" runat="server" CssClass="btn btn-primary"
                                        Text="Update Progress" />
                                    <asp:Button ID="lnkSelfEvaluationList" runat="server" CssClass="btn btn-primary"
                                        Text="Self Performance Assessment List" />
                                    <asp:Button ID="lnkSelfEvaluation" runat="server" CssClass="btn btn-primary" Text="Self Performance Assessment" />
                                    <div style="display: none;">
                                        <asp:LinkButton ID="lnkAppRejAssessment" runat="server" ToolTip="Agree/Disagree Assessment"
                                            CssClass="pull-left" Style="padding-left: 20px;">
                                       <i style="font-size:25px;" class="fas fa-check-circle"></i>
                                        </asp:LinkButton>
                                    </div>
                                    <asp:Button ID="lnkPerformanceResult" runat="server" CssClass="btn btn-primary" Text="Assessment Result" />
                                    <asp:Button ID="btnFinalRating" runat="server" CssClass="btn btn-primary" Text="Final Rating" />
                                     <asp:Button ID="btnSubmitFinalProgress" runat="server" CssClass="btn btn-default"
                                        Text="Submit Final Progress" />
                                    <% End If%>
                                </div>
                                <asp:Button ID="btnclose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdfv" runat="server" />
                <cc1:ModalPopupExtender ID="popAddButton" BehaviorID="MPE" runat="server" TargetControlID="hdfv"
                    CancelControlID="btnAddCancel" BackgroundCssClass="popMenu" PopupControlID="pnlImageAdd">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImageAdd" runat="server" CssClass="card">
                    <div class="header">
                        <ul class="header-dropdown m-r-10 p-l-0">
                            <asp:LinkButton ID="lnkaclose" runat="server" OnClientClick="ClosePopup('popAddButton');return false;"><i class="fas fa-window-close"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: auto">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dlmnuAdd" runat="server" ItemStyle-Wrap="true">
                                    <AlternatingItemStyle BackColor="Transparent" />
                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID='objadd' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                            OnClick="lnkAdd_Click" Font-Underline="false" CssClass="form-label" Font-Bold="true" />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:HiddenField ID="btnAddCancel" runat="server" />
                                <asp:HiddenField ID="btndlmnuAdd" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popEditButton" runat="server" TargetControlID="btndlmnuEdit"
                    CancelControlID="btnEditCancel" BackgroundCssClass="popMenu" PopupControlID="pnlImageEdit">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImageEdit" runat="server" CssClass="card">
                    <div class="header">
                        <ul class="header-dropdown m-r-10 p-l-0">
                            <asp:LinkButton ID="lnkeclose" runat="server" OnClientClick="ClosePopup('popEditButton');return false;"><i class="fas fa-window-close"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: auto">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dlmnuEdit" runat="server">
                                    <AlternatingItemStyle BackColor="Transparent" />
                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID='objEdit' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                            Font-Underline="false" CssClass="form-label" Font-Bold="true" OnClick="lnkEdit_Click" />
                                        <asp:HiddenField ID="objEditOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:HiddenField ID="btnEditCancel" runat="server" />
                                <asp:HiddenField ID="btndlmnuEdit" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popDeleteButton" runat="server" TargetControlID="btndlmnuDelete"
                    CancelControlID="btnDeleteCancel" BackgroundCssClass="popMenu" PopupControlID="pnlImageDelete">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImageDelete" runat="server" CssClass="card">
                    <div class="header">
                        <ul class="header-dropdown m-r-10 p-l-0">
                            <asp:LinkButton ID="lnkdclose" runat="server" OnClientClick="ClosePopup('popDeleteButton');return false;"><i class="fas fa-window-close"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: auto">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dlmnuDelete" runat="server">
                                    <AlternatingItemStyle BackColor="Transparent" />
                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID='objDelete' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                            Font-Underline="false" CssClass="form-label" Font-Bold="true" OnClick="lnkDelete_Click" />
                                        <asp:HiddenField ID="objDeleteOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:HiddenField ID="btnDeleteCancel" runat="server" />
                                <asp:HiddenField ID="btndlmnuDelete" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdf_locationx" runat="server" />
                <asp:HiddenField ID="hdf_locationy" runat="server" />
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField1" runat="server" TargetControlID="hdf_btnEmpField1Save"
                    CancelControlID="hdf_btnEmpField1Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditEmpField1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditEmpField1" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblheaderEmpField1" runat="server" Text="AddEditEmpField1"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: auto; max-height: 500px;">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblEmpField1Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtEmpField1Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblEmpField1Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtEmpField1EmployeeName" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdf_txtEmpField1EmployeeName" runat="server" />
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblEmpField1Perspective" runat="server" Text="Perspective" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboEmpField1Perspective" runat="server" Height="20px" Width="225px">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnlEmpField1lblField1" runat="server">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="objEmpField1lblField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField1FieldValue1" runat="server" Height="20px" Width="225px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </asp:Panel>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="objEmpField1lblEmpField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtEmpField1EmpField1" runat="server" TextMode="MultiLine" Style="resize: none;"
                                            class="form-control"></asp:TextBox>
                                        <asp:HiddenField ID="hdf_txtEmpField1EmpField1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnl_RightEmpField1" runat="server">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField1StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField1StartDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField1EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField1EndDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none">
                                    <asp:Label ID="lblEmpField1Status" runat="server" Text="Status"></asp:Label>
                                    <asp:DropDownList ID="cboEmpField1Status" runat="server" Height="25px" Width="200px"
                                        Enabled="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblEmpField1Percentage" runat="server" Text="% Completed"></asp:Label>
                                    <asp:TextBox ID="txtEmpField1Percent" runat="server" Style="text-align: right" Text="0.00"
                                        Width="125px" onKeypress="return
                    onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Panel ID="objEmpField1tabcRemarks" runat="server">
                                        <%--Tab-Headers--%>
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li id="objEmpField1tabpRemark1" runat="server" role="presentation" class="active"><a
                                                href="#InfFld6" data-toggle="tab">
                                                <asp:Label ID="lblEmpField1Remark1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField1Remark1" runat="server" />
                                            </a></li>
                                            <li id="objEmpField1tabpRemark2" runat="server" role="presentation"><a href="#InfFld7"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField1Remark2" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField1Remark2" runat="server" />
                                            </a></li>
                                            <li id="objEmpField1tabpRemark3" runat="server" role="presentation"><a href="#InfFld8"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField1Remark3" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField1Remark3" runat="server" />
                                            </a></li>
                                        </ul>
                                        <%--Tab-Panes--%>
                                        <div class="tab-content p-b-0">
                                            <div role="tabpanel" class="tab-pane fade in active" id="InfFld6">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField1Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="InfFld7">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField1Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="InfFld8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField1Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField1Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField1Weight" runat="server" Style="text-align: right" Text="0.00"
                                                onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField1GoalType" runat="server" Text="Goal Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField1GoalType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField1UoMType" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField1UomType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField1GoalValue" runat="server" Text="Goal Value" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField1GoalValue" runat="server" Style="text-align: right"
                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField1SearchEmp" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 100px">
                                        <asp:Panel ID="pnl_EmpField1dgvower" runat="server">
                                            <asp:DataGrid ID="dgvEmpField1Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkEmpField1allSelect" CssClass="filled-in" Text=" " runat="server"
                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField1AllSelect_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEmpField1select" CssClass="filled-in" Text=" " runat="server"
                                                                Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField1select_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName"
                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnEmpField1Save" runat="server" />
                        <asp:Button ID="btnEmpField1Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnEmpField1Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField2" runat="server" TargetControlID="hdf_btnEmpField2Save"
                    CancelControlID="hdf_btnEmpField2Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditEmpField2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditEmpField2" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderEmpField2" runat="server" Text="AddEditEmpField2"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: auto; max-height: 500px;">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField2Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField2Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField2Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField2EmployeeName" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField2lblEmpField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpField2EmpFieldValue1" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:HiddenField ID="hdf_cboEmpField2EmpFieldValue1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField2lblEmpField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField2EmpField2" runat="server" TextMode="MultiLine" Rows="2"
                                                    Style="resize: none" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hdf_txtEmpField2EmpField2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnl_RightEmpField2" runat="server" Style="height: auto">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField2StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField2StartDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField2EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField2EndDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none">
                                    <asp:Label ID="lblEmpField2Status" runat="server" Text="Status"></asp:Label>
                                    <asp:DropDownList ID="cboEmpField2Status" runat="server" Height="25px" Width="200px"
                                        Enabled="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblEmpField2Percentage" runat="server" Text="% Completed"></asp:Label>
                                    <asp:TextBox ID="txtEmpField2Percent" runat="server" Style="text-align: right" Text="0.00"
                                        Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Panel ID="objEmpField2tabcRemarks" runat="server">
                                        <%--Tab-Headers--%>
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li id="objEmpField2tabpRemark1" runat="server" role="presentation" class="active"><a
                                                href="#Inf2Fld6" data-toggle="tab">
                                                <asp:Label ID="lblEmpField2Remark1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField2Remark1" runat="server" />
                                            </a></li>
                                            <li id="objEmpField2tabpRemark2" runat="server" role="presentation"><a href="#Inf2Fld7"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField2Remark2" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField2Remark2" runat="server" />
                                            </a></li>
                                            <li id="objEmpField2tabpRemark3" runat="server" role="presentation"><a href="#Inf2Fld8"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField2Remark3" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField2Remark3" runat="server" />
                                            </a></li>
                                        </ul>
                                        <%--Tab-Panes--%>
                                        <div class="tab-content p-b-0">
                                            <div role="tabpanel" class="tab-pane fade in active" id="Inf2Fld6">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField2Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf2Fld7">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField2Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf2Fld8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField2Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField2Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField2Weight" runat="server" Style="text-align: right" Text="0.00"
                                                onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField2GoalType" runat="server" Text="Goal Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField2GoalType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField2UoMType" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField2UomType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField2GoalValue" runat="server" Text="Goal Value" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField2GoalValue" runat="server" Style="text-align: right"
                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField2SearchEmp" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 100px">
                                        <asp:Panel ID="pnl_EmpField2dgvower" runat="server">
                                            <asp:DataGrid ID="dgvEmpField2Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkEmpField2allSelect" CssClass="filled-in" Text=" " runat="server"
                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField2AllSelect_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEmpField2select" CssClass="filled-in" Text=" " runat="server"
                                                                Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField2select_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnEmpField2Save" runat="server" />
                        <asp:Button ID="btnEmpField2Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnEmpField2Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField3" runat="server" TargetControlID="hdf_btnEmpField3Save"
                    CancelControlID="hdf_btnEmpField3Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditEmpField3">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditEmpField3" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderEmpField3" runat="server" Text="AddEditEmpField3"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: auto; max-height: 500px;">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField3Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField3Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField3Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField3Employee" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField3lblEmpField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objEmpField3txtEmpField1" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField3lblEmpField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpField3EmpFieldValue2" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:HiddenField ID="hdf_cboEmpField3EmpFieldValue2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="objEmpField3lblOwrField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtEmpField3EmpField3" runat="server" TextMode="MultiLine" Rows="7"
                                            Style="resize: none" class="form-control"></asp:TextBox>
                                        <asp:HiddenField ID="hdf_txtEmpField3EmpField3" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnl_RightEmpField3" runat="server" Style="height: auto">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField3StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField3StartDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField3EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField3EndDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none">
                                    <asp:Label ID="lblEmpField3Status" runat="server" Text="Status"></asp:Label>
                                    <asp:DropDownList ID="cboEmpField3Status" runat="server" Height="25px" Width="200px"
                                        Enabled="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblEmpField3Percentage" runat="server" Text="% Completed"></asp:Label>
                                    <asp:TextBox ID="txtEmpField3Percent" runat="server" Style="text-align: right" Text="0.00"
                                        Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Panel ID="objEmpField3tabcRemarks" runat="server">
                                        <%--Tab-Headers--%>
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li id="objEmpField3tabpRemark1" runat="server" role="presentation" class="active"><a
                                                href="#Inf3Fld6" data-toggle="tab">
                                                <asp:Label ID="lblEmpField3Remark1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField3Remark1" runat="server" />
                                            </a></li>
                                            <li id="objEmpField3tabpRemark2" runat="server" role="presentation"><a href="#Inf3Fld7"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField3Remark2" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField3Remark2" runat="server" />
                                            </a></li>
                                            <li id="objEmpField3tabpRemark3" runat="server" role="presentation"><a href="#Inf3Fld8"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField3Remark3" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_lblEmpField3Remark3" runat="server" />
                                            </a></li>
                                        </ul>
                                        <%--Tab-Panes--%>
                                        <div class="tab-content p-b-0">
                                            <div role="tabpanel" class="tab-pane fade in active" id="Inf3Fld6">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField3Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf3Fld7">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField3Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf3Fld8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField3Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField3Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField3Weight" runat="server" Style="text-align: right" Text="0.00"
                                                onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField3GoalType" runat="server" Text="Goal Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField3GoalType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField3UoMType" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField3UomType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField3GoalValue" runat="server" Text="Goal Value" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField3GoalValue" runat="server" Style="text-align: right"
                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField3SearchEmp" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 100px">
                                        <asp:Panel ID="pnl_EmpField3dgvOwner" runat="server">
                                            <asp:DataGrid ID="dgvEmpField3Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkEmpField3allSelect" CssClass="filled-in" Text=" " runat="server"
                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField3AllSelect_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEmpField3select" CssClass="filled-in" Text=" " runat="server"
                                                                Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField3select_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnEmpField3Save" runat="server" />
                        <asp:Button ID="btnEmpField3Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnEmpField3Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField4" runat="server" TargetControlID="hdf_btnEmpField4Save"
                    CancelControlID="hdf_btnEmpField4Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditEmpField4">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditEmpField4" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderEmpField4" runat="server" Text="AddEditOwrField4"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: auto; max-height: 500px;">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField4Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField4Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField4Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField4Employee" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField4lblEmpField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objEmpField4txtEmpField1" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField4lblEmpField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objEmpField4txtEmpField2" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="objEmpField4lblEmpField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboEmpField4EmpFieldValue3" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <asp:HiddenField ID="hdf_cboEmpField4EmpFieldValue3" runat="server" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="objEmpField4lblEmpField4" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="objEmpField4txtEmpField4" runat="server" TextMode="MultiLine" Style="resize: none"
                                            Rows="2" class="form-control"></asp:TextBox>
                                        <asp:HiddenField ID="hdf_objEmpField4txtEmpField4" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnl_RightEmpField4" runat="server" Style="height: auto">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField4StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField4StartDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField4EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField4EndDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none">
                                    <asp:Label ID="lblEmpField4Status" runat="server" Text="Status"></asp:Label>
                                    <asp:DropDownList ID="cboEmpField4Status" runat="server" Height="25px" Width="200px"
                                        Enabled="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblEmpField4Percentage" runat="server" Text="% Completed"></asp:Label>
                                    <asp:TextBox ID="txtEmpField4Percent" runat="server" Style="text-align: right" Text="0.00"
                                        Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Panel ID="objEmpField4tabcRemarks" runat="server">
                                        <%--Tab-Headers--%>
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li id="objEmpField4tabpRemark1" runat="server" role="presentation" class="active"><a
                                                href="#Inf4Fld6" data-toggle="tab">
                                                <asp:Label ID="lblEmpField4Remark1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_txtEmpField4Remark1" runat="server" />
                                            </a></li>
                                            <li id="objEmpField4tabpRemark2" runat="server" role="presentation"><a href="#Inf4Fld7"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField4Remark2" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_txtEmpField4Remark2" runat="server" />
                                            </a></li>
                                            <li id="objEmpField4tabpRemark3" runat="server" role="presentation"><a href="#Inf4Fld8"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField4Remark3" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_txtEmpField4Remark3" runat="server" />
                                            </a></li>
                                        </ul>
                                        <%--Tab-Panes--%>
                                        <div class="tab-content p-b-0">
                                            <div role="tabpanel" class="tab-pane fade in active" id="Inf4Fld6">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField4Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf4Fld7">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField4Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf4Fld8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField4Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField4Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField4Weight" runat="server" Style="text-align: right" Text="0.00"
                                                onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField4GoalType" runat="server" Text="Goal Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField4GoalType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField4UoMType" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField4UomType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField4GoalValue" runat="server" Text="Goal Value" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField4GoalValue" runat="server" Style="text-align: right"
                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField4SearchEmp" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 100px">
                                        <asp:Panel ID="pnl_EmpField4dgvOwner" runat="server">
                                            <asp:DataGrid ID="dgvEmpField4Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkEmpField4allSelect" CssClass="filled-in" Text=" " runat="server"
                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField4AllSelect_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEmpField4select" CssClass="filled-in" Text=" " runat="server"
                                                                Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField4select_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName"
                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnEmpField4Save" runat="server" />
                        <asp:Button ID="btnEmpField4Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnEmpField4Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField5" runat="server" TargetControlID="hdf_btnEmpField5Save"
                    CancelControlID="hdf_btnEmpField5Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditEmpField5">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditEmpField5" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderEmpField5" runat="server" Text="AddEditEmpField5"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: auto; max-height: 500px;">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField5Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField5Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpField5Employee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmpField5Employee" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField5lblEmpField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objEmpField5txtEmpField1" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField5lblEmpField2" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objEmpField5txtEmpField2" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField5lblEmpField3" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objEmpField5txtEmpField3" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objEmpField5lblEmpField4" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpField5EmpFieldValue4" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:HiddenField ID="hdf_cboEmpField5EmpFieldValue4" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="objEmpField5lblEmpField5" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="objEmpField5txtEmpField5" runat="server" TextMode="MultiLine" Rows="4"
                                            Style="resize: none" class="form-control"></asp:TextBox>
                                        <asp:HiddenField ID="hdf_objEmpField5txtEmpField5" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnl_RightEmpField5" runat="server" Style="height: auto">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField5StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField5StartDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmpField5EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpEmpField5EndDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none">
                                    <asp:Label ID="lblEmpField5Status" runat="server" Text="Status"></asp:Label>
                                    <asp:DropDownList ID="cboEmpField5Status" runat="server" Height="25px" Width="200px"
                                        Enabled="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblEmpField5Percentage" runat="server" Text="% Completed"></asp:Label>
                                    <asp:TextBox ID="txtEmpField5Percent" runat="server" Style="text-align: right" Text="0.00"
                                        Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Panel ID="objEmpField5tabcRemarks" runat="server">
                                        <%--Tab-Headers--%>
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li id="objEmpField5tabpRemark1" runat="server" role="presentation" class="active"><a
                                                href="#Inf5Fld6" data-toggle="tab">
                                                <asp:Label ID="lblEmpField5Remark1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_txtEmpField5Remark1" runat="server" />
                                            </a></li>
                                            <li id="objEmpField5tabpRemark2" runat="server" role="presentation"><a href="#Inf5Fld7"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField5Remark2" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_txtEmpField5Remark2" runat="server" />
                                            </a></li>
                                            <li id="objEmpField5tabpRemark3" runat="server" role="presentation"><a href="#Inf5Fld8"
                                                data-toggle="tab">
                                                <asp:Label ID="lblEmpField5Remark3" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf_txtEmpField5Remark3" runat="server" />
                                            </a></li>
                                        </ul>
                                        <%--Tab-Panes--%>
                                        <div class="tab-content p-b-0">
                                            <div role="tabpanel" class="tab-pane fade in active" id="Inf5Fld6">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField5Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf5Fld7">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField5Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="Inf5Fld8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmpField5Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField5Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField5Weight" runat="server" Style="text-align: right" Text="0.00"
                                                onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField5GoalType" runat="server" Text="Goal Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField5GoalType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField5UoMType" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEmpField5UomType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEmpField5GoalValue" runat="server" Text="Goal Value" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField5GoalValue" runat="server" Style="text-align: right"
                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmpField5SearchEmp" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 100px">
                                        <asp:Panel ID="pnl_EmpField5dgvOwner" runat="server">
                                            <asp:DataGrid ID="dgvEmpField5Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkEmpField5allSelect" CssClass="filled-in" Text=" " runat="server"
                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField5AllSelect_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEmpField5select" CssClass="filled-in" Text=" " runat="server"
                                                                Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField5select_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName"
                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnEmpField5Save" runat="server" />
                        <asp:Button ID="btnEmpField5Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnEmpField5Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_YesNo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMessage" runat="server" Text="Message:" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="3" Style="resize: none;"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <%--'S.SANDEEP |03-SEP-2022| -- START--%>
                <%--'ISSUE/ENHANCEMENT : Sprint_2022-13--%>
                <cc1:ModalPopupExtender ID="popup_SubmitApproval" runat="server" TargetControlID="hdf_SubmitApproval"
                    CancelControlID="btnSubmitNo" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_SubmitApproval">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_SubmitApproval" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblSubmitTitle" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblSubmitMessage" runat="server" Text="Message:" CssClass="form-label" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSubmitYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnSubmitNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_SubmitApproval" runat="server" />
                    </div>
                </asp:Panel>
                <%--'S.SANDEEP |03-SEP-2022| -- END--%>
                <asp:HiddenField ID="hdf_topposition" runat="server" />
                <asp:HiddenField ID="hdf_leftposition" runat="server" />
                <cc1:ModalPopupExtender ID="popup_AppRejAssessment" runat="server" BackgroundCssClass="ModalPopupBG2"
                    CancelControlID="btnAppRejAss_Close" PopupControlID="pnl_AppRejAssessment" TargetControlID="hdfAppRejAss">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_AppRejAssessment" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblApprejAssessmentHeader" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 200px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAppRejAssessment_AssessorPart" runat="server" Text="Assessor Part"
                                    CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <asp:Label ID="lblAppRejAssessement_Assessor_Status" runat="server" Text="Status"
                                    CssClass="form-label" />
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <asp:RadioButton ID="radAssessment_Assessor_Approve" runat="server" Text="Agree"
                                    GroupName="Assessment_Assessor" />
                                <asp:RadioButton ID="radAssessment_Assessor_Reject" runat="server" Text="Disagree"
                                    GroupName="Assessment_Assessor" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAppRejAssessement_Assessor_Remark" runat="server" Text="Remark"
                                    CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtAppRejAssessment_Assessor_Remark" runat="server" TextMode="MultiLine"
                                            Style="resize: none" class="form-control" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Panel ID="pnlAppRejAssessment_Reviewer" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAppRejAssessment_ReviewerPart" runat="server" Text="Reviewer Part"
                                                CssClass="form-label" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                            <asp:Label ID="lblAppRejAssessement_Reviewer_Status" runat="server" Text="Status"
                                                CssClass="form-label" />
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <asp:RadioButton ID="radAssessment_Reviewer_Approve" runat="server" Text="Agree"
                                                GroupName="Assessment_Reviewer" />
                                            <asp:RadioButton ID="radAssessment_Reviewer_Reject" runat="server" Text="Disagree"
                                                GroupName="Assessment_Reviewer" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAppRejAssessement_Reviewer_Remark" runat="server" Text="Remark"
                                                CssClass="form-label" />
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppRejAssessment_Reviewer_Remark" runat="server" TextMode="MultiLine"
                                                        Style="resize: none" class="form-control" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnpostComment" runat="server" Text="Post Comment" CssClass="btn btn-primary" />
                        <asp:Button ID="btnAppRejAss_Close" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdfAppRejAss" runat="server" />
                    </div>
                </asp:Panel>
                <cf1:CnfDialog ID="cnfEdit" runat="server" />
                <ucCfnYesno:Confirmation ID="CnfSubmitFinalProgress" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
