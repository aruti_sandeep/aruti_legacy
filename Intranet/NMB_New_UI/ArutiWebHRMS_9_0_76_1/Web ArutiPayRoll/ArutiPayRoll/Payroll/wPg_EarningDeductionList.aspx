﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EarningDeductionList.aspx.vb"
    Inherits="Payroll_wPg_EarningDeductionList" Title="Employee Earning and Deduction List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var sctop;
            var prm;
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
                sctop = $("#scrollable-container").scrollTop();
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                 if (args.get_error() == undefined) {
                     //$("#scrollable-container").scrollTop($(scroll.Y).val());
                     $("#scrollable-container").scrollTop(sctop);
                }
            }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc9:ConfirmYesNo ID="popupApprove" runat="server" Title="Approve ED Heads" />
                <uc9:ConfirmYesNo ID="popupDisapprove" runat="server" Title="Disapprove ED Heads" />
                <uc10:DeleteReason ID="popupDelete" runat="server" Title="Are you sure you want to delete this Earning Deduction?"
                    ValidationGroup="popupDelete" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc10:DeleteReason ID="popApproveReason" runat="server" Title="Comments" ValidationGroup="popApproveReason" />
                <uc10:DeleteReason ID="popRejectReason" runat="server" Title="Comments" ValidationGroup="popRejectReason" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Earning and Deduction List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" ToolTip="Allocation">
                                                    <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCalcType" runat="server" Text="Calc. Type" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCalcType" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTranHead" runat="server" Text="Transaction Head" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTranHead" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblperiod" runat="server" Text="As On Period" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkIncludeInactiveEmployee" runat="server" Text="Include Inactive Employees"
                                            CssClass="filled-in" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprovalStatus" runat="server" Text="Approval Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEDApprovalStatus" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left;">
                                </div>
                                <asp:HiddenField ID="hfobjlblTotalAmt" runat="server" Value="0.00" />
                                <asp:Button ID="btnNew" runat="server" Text="New" Visible="false" CssClass="btn btn-default" />
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-default"
                                    UseSubmitBehavior="false" />
                                <asp:Button ID="btnDisApprove" runat="server" Text="Reject" CssClass="btn btn-default"
                                    UseSubmitBehavior="false" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTotalAmt" runat="server" Text="Total Flat Rate Amount" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblTotalAmt" runat="server" Text="0.00" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 500px;">
                                            <asp:GridView ID="dgED" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" Width="99%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                DataKeyNames="edunkid, typeof_id, IsGrp, employeeunkid, tranheadunkid, employeename, trnheadname, amount, periodunkid, membershiptranunkid, userunkid, IsApproved">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="align-center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" onclick="selectall(this);"
                                                                Text=" " CssClass="filled-in" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" onclick="selectallgroupchild(this);"
                                                                Text=" " CssClass="filled-in" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Change" ToolTip="Edit"
                                                                    CommandArgument="<%# Container.DataItemIndex %>">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Remove" ToolTip="Delete"
                                                                    CommandArgument="<%# Container.DataItemIndex %>">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="trnheadcode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhTrnHeadCode" />
                                                    <asp:BoundField DataField="trnheadname" HeaderText="Transaction Head" ReadOnly="True"
                                                        ItemStyle-Width="150px" FooterText="dgcolhTrnHead" />
                                                    <asp:BoundField DataField="trnheadtype_name" HeaderText="Head Type" ReadOnly="True"
                                                        ItemStyle-Width="150px" FooterText="dgcolhTranHeadType" />
                                                    <asp:BoundField DataField="typeof_name" HeaderText="Type Of" ReadOnly="True" ItemStyle-Width="150px"
                                                        FooterText="dgcolhTypeOf" />
                                                    <asp:BoundField DataField="calctype_name" HeaderText="Calculation Type" ReadOnly="true"
                                                        ItemStyle-Width="150px" FooterText="dgcolhCalcType" />
                                                    <asp:BoundField DataField="period_name" HeaderText="Period" ReadOnly="True" ItemStyle-Width="100px"
                                                        FooterText="colhPeriod" />
                                                    <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" ItemStyle-CssClass="colhAmount" />
                                                    <asp:BoundField DataField="isapproved" HeaderText="Status" ReadOnly="True" ItemStyle-HorizontalAlign="Center"
                                                        FooterText="colhApprovePending" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

        var totalamt = 0;        
            function selectall(chk) {
                $("[id*='chkSelect']").attr('checked', chk.checked);
                settotal();
            }

            function selectallgroupchild(chk) {
                if ($(chk).closest('tr').next('tr').children('td').length > $(chk).closest('tr').children('td').length) {
                    selectnextrow($(chk).closest('tr').next('tr'), chk);
                }
                settotal();
            }

            function selectnextrow(obj, chk) {
                $(obj).find("[id*='chkSelect']").attr('checked', chk.checked);
                if ($(obj).next('tr').children('td').length == $(obj).children('td').length) {
                    selectnextrow($(obj.next('tr')), chk);              
                }
            }
           
            function settotal() {
                totalamt = 0;
                $$('dgED').find("[id*='chkSelect']:checked").closest('tr').each(function() {
                    totalamt += parseFloat($(this).children('td.colhAmount').text().replace(',', '').replace('', '0'));
                })
                if (totalamt.toString().indexOf('.') >= 0)
                    $$('objlblTotalAmt').text(totalamt);
                else
                    $$('objlblTotalAmt').text(totalamt.toString().concat('.00'));

                $$('hfobjlblTotalAmt').val($$('objlblTotalAmt').text());
            }
           
    </script>

</asp:Content>
