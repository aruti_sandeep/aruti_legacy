﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_PayrollReport.aspx.vb"
    Inherits="Payroll_wPg_PayrollReport" Title="Payroll Report" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                <uc9:Export runat="server" ID="Export" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Report" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" ToolTip="Analysis By">
                                             <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBranch" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblToPeriod" runat="server" Text="To Period" Visible="False" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpPeriodTo" runat="server" Visible="false" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <asp:Label ID="lblExRate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkIgnorezeroHead" runat="server" Text="Ignore Zero Value Heads" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkIncludeEmployerContribution" runat="server" Text="Include Employer Contribution" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowPaymentDetails" runat="server" Text="Show Payment Details" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowLoansInSeparateColumns" runat="server" Text="Show Loans In Separate Columns" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowSavingsInSeparateColumns" runat="server" Text="Show Savings In Separate Columns" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowCompanyLogoOnReport" runat="server" Text="Show Company Logo" />
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnlCostCenterCode" runat="server" Visible="false">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCostCenterCode" runat="server" Text="Cost Center Code" Visible="false"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtCostCenterCode" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlMembership" runat="server" Visible="false">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMembership" runat="server" Text="Membership" Visible="false" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboMembership" runat="server" Visible="false">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                 </asp:Panel>
                                                <asp:Panel ID="pnlIdentity" runat="server" Visible="false">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblIdentity" runat="server" Text="Identity" Visible="false" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboIdentity" runat="server" Visible="false">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowEmpNameinSeperateColumn" runat="server" Text="Show Emp Name in Seperate Columns" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblIncludeHeads" runat="server" Text="Include Allowance, Other Earning and Other Deduction Heads"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="table-responsive" style="max-height: 680px;">
                                                            <asp:GridView ID="lvTranHead" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                                Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                                RowStyle-Wrap="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged"
                                                                                Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                OnCheckedChanged="chkSelect_OnCheckedChanged" Text=" " />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="tranheadunkid" HeaderText="tranheadunkid" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                        FooterText="colhCode" />
                                                                    <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                        FooterText="colhName" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="uppnlCustomReport" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblCustomSettings" runat="server" Text="Custom Settings" CssClass="form-label"></asp:Label>
                                                        </h2>
                                                        <ul class="header-dropdown m-r--5">
                                                            <li class="dropdown">
                                                                <asp:LinkButton ID="lnkCustomSettings" runat="server" Text="Save Settings" ToolTip="Save Settings">
                                                    <i class="far fa-save"></i>
                                                                </asp:LinkButton>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 400px;">
                                                                    <asp:GridView ID="lvAllocation_hierarchy" runat="server" AutoGenerateColumns="False"
                                                                        ShowFooter="False" Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                        DataKeyNames="Id" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkAllocationSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAllocationSelectAll_OnCheckedChanged"
                                                                                        Text=" " />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkAllcationSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                        AutoPostBack="true" OnCheckedChanged="chkAllocationSelect_OnCheckedChanged" Text=" " />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Name" HeaderText="Allocation/other Details" ReadOnly="true"
                                                                                FooterText="colhAllocation" />
                                                                            <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" />
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkUp" CommandArgument="up" runat="server" Text="&#x25B2;" OnClick="ChangeAllocationLocation" />
                                                                                    <asp:LinkButton ID="lnkDown" CommandArgument="down" runat="server" Text="&#x25BC;"
                                                                                        OnClick="ChangeAllocationLocation" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTrnHeadType" runat="server" Text="Transaction Head Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 775px;">
                                                                        <asp:GridView ID="lvCustomTranHead" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                                            Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="tranheadunkid"
                                                                             CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged"
                                                                                            Text=" " />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                            OnCheckedChanged="chkHeadSelect_OnCheckedChanged" Text=" " />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                    FooterText="colhAllocation" />
                                                                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                    FooterText="colhAllocation" />
                                                                                <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkUp" CommandArgument="up" runat="server" Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                                                        <asp:LinkButton ID="lnkDown" CommandArgument="down" runat="server" Text="&#x25BC;"
                                                                                            OnClick="ChangeTranHeadLocation" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btn btn-primary"
                                    ValidationGroup="Payslip" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default"
                                    ValidationGroup="Payslip" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ddlReportType" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="drpPeriod" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="drpPeriodTo" EventName="SelectedIndexChanged" />
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
