﻿<%@ Page Title="Payroll Employee List Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_PayrollEmployeeListReport.aspx.vb" Inherits="Payroll_Rpt_PayrollEmployeeListReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <uc6:advancefilter id="popupAdvanceFilter" runat="server" />
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <div style="float: left;">
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Employee List Report"></asp:Label>
                                </div>
                                <div style="text-align: right;">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </div>
                                <h2>
                                </h2>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpPeriod" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:UpdatePanel ID="uppnlCustomReport" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="card inner-card">
                                                <div class="header">
                                                    <h2>
                                                        <asp:Label ID="lblCustomSettings" runat="server" Text="Custom Settings" CssClass="form-label"></asp:Label>
                                                    </h2>
                                                    <ul class="header-dropdown m-r--5">
                                                        <li class="dropdown">
                                                            <asp:LinkButton ID="gbCustomSetting" runat="server" Text="Save Settings" ToolTip="Save Settings">
                                                    <i class="far fa-save"></i>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="table-responsive" style="max-height: 400px;">
                                                                <asp:GridView ID="lvAllocation_hierarchy" runat="server" AutoGenerateColumns="False"
                                                                    ShowFooter="False" Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                    DataKeyNames="Id" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                            HeaderStyle-CssClass="align-center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkAllocationSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAllocationSelectAll_OnCheckedChanged"
                                                                                    Text=" " />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkAllcationSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                    AutoPostBack="true" OnCheckedChanged="chkAllocationSelect_OnCheckedChanged" Text=" " />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Allocation/other Details" ReadOnly="true"
                                                                            FooterText="colhAllocation" />
                                                                        <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" />
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkUp" CommandArgument="up" runat="server" Text="&#x25B2;" OnClick="ChangeAllocationLocation" />
                                                                                <asp:LinkButton ID="lnkDown" CommandArgument="down" runat="server" Text="&#x25BC;"
                                                                                    OnClick="ChangeAllocationLocation" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc9:Export runat="server" ID="Export" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
