﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Payroll_Rpt_FlexcubeLoanFormReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmFlexcubeLoanFormReport"
    Private mdicLoanType As New Dictionary(Of Integer, String)
#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        IdentityType = 1
    End Enum
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            mdicLoanType.Add(1, "GENERAL")
            mdicLoanType.Add(2, "CAR")
            mdicLoanType.Add(3, "REFINANCE MORTGAGE")
            mdicLoanType.Add(4, "PURCHASE MORTGAGE")
            mdicLoanType.Add(5, "CONSTRUCTION MORTGAGE")

            If IsPostBack = False Then
                'SetLanguage()
                Call FillCombo()
                Call ResetValue()

                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    btnSaveSelection.Visible = False
                End If
            Else
                mdicLoanType = Me.ViewState("mdicLoanType")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdicLoanType") = mdicLoanType
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objCMaster As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              Session("UserAccessModeSetting").ToString(), True, _
                                              CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                              blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, " ISNULL(ispostingtoflexcube,0) = 1 ", True)

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")

            With cboIdentity
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objLoanScheme = Nothing
            objCMaster = Nothing
        End Try
    End Sub

    Private Function SetFilter(ByRef objFlexcubeLoanFormReport As clsFlexcubeLoanFormReport) As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Function
            End If

            If CInt(cboLoanScheme.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Loan Scheme."), Me)
                cboLoanScheme.Focus()
                Exit Function
            End If

            If CInt(cboApplicationNo.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please Select Application No."), Me)
                cboApplicationNo.Focus()
                Exit Function
            End If

            If CInt(cboIdentity.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Please Select Identity."), Me)
                cboIdentity.Focus()
                Exit Function
            End If

            Dim intLoanTypeId As Integer
            For Each key In mdicLoanType
                If cboLoanScheme.SelectedItem.Text.ToUpper.Contains(key.Value) Then
                    Select Case key.Key
                        Case 1
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.General_Loan
                            Exit For
                        Case 2
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Car_Loan
                            Exit For
                        Case 3
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Refinancing_Mortgage_Loan
                            Exit For
                        Case 4
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Purchase_Mortgage_Loan
                            Exit For
                        Case 5
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Construction_Mortgage_Loan
                            Exit For
                    End Select
                End If
            Next

            objFlexcubeLoanFormReport.SetDefaultValue()

            objFlexcubeLoanFormReport._EmployeeUnkId = CInt(cboEmployee.SelectedValue)
            objFlexcubeLoanFormReport._EmployeeName = cboEmployee.SelectedItem.Text

            objFlexcubeLoanFormReport._ProcessPendingLoanUnkid = CInt(cboApplicationNo.SelectedValue)
            objFlexcubeLoanFormReport._ApplicationNo = cboApplicationNo.SelectedItem.Text

            objFlexcubeLoanFormReport._LoanSchemeUnkId = CInt(cboLoanScheme.SelectedValue)
            objFlexcubeLoanFormReport._LoanSchemeName = cboLoanScheme.SelectedItem.Text

            objFlexcubeLoanFormReport._IdentityUnkId = CInt(cboIdentity.SelectedValue)
            objFlexcubeLoanFormReport._IdentityName = cboIdentity.SelectedItem.Text

            objFlexcubeLoanFormReport._LoanTypeId = intLoanTypeId


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboApplicationNo.SelectedValue = "0"
            cboIdentity.SelectedValue = "0"

            Call GetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Flexcube_Loan_Form_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.IdentityType
                            cboIdentity.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Dim objFlexcubeLoanFormReport As New clsFlexcubeLoanFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If SetFilter(objFlexcubeLoanFormReport) = False Then Exit Sub

            Dim xPeriodStart As Date
            Dim xPeriodEnd As Date
            Dim objProcessPendingLoan As New clsProcess_pending_loan
            Dim objPeriod As New clscommom_period_Tran
            objProcessPendingLoan._Processpendingloanunkid = CInt(cboApplicationNo.SelectedValue)
            objPeriod._Periodunkid(Session("Database_Name")) = CInt(objProcessPendingLoan._DeductionPeriodunkid)
           
            If CInt(objProcessPendingLoan._DeductionPeriodunkid) > 0 Then
                xPeriodStart = objPeriod._Start_Date
                xPeriodEnd = objPeriod._End_Date
            Else
                xPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                xPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            objProcessPendingLoan = Nothing

            Call SetDateFormat()

            objFlexcubeLoanFormReport.generateReportNew(Session("Database_Name"), _
                                         Session("UserId"), _
                                         Session("Fin_year"), _
                                         Session("CompanyUnkId"), _
                                         xPeriodStart, _
                                         xPeriodEnd, _
                                         Session("UserAccessModeSetting"), True, _
                                         Session("ExportReportPath"), _
                                         False, _
                                         0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objFlexcubeLoanFormReport._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            'If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 1
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Flexcube_Loan_Form_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.IdentityType
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIdentity.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Flexcube_Loan_Form_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Selection Saved Successfully"), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region "ComboBox Event"
    Protected Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Dim objProcesspendingloan As New clsProcess_pending_loan
        Dim dtTable As New DataTable
        Try
            Dim StrSearching As String = String.Empty
            Dim dsList As DataSet = Nothing

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                dsList = objProcesspendingloan.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                CStr(Session("UserAccessModeSetting")), _
                                                                True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), _
                                                                "Loan", , StrSearching, , _
                                                                CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                dtTable = dsList.Tables(0)

                Dim drRow As DataRow = dtTable.NewRow
                drRow("application_no") = "Select"
                drRow("processpendingloanunkid") = -1
                dtTable.Rows.InsertAt(drRow, 0)

            Else
                Dim dtTemp As New DataTable("List")
                dtTemp.Columns.Add("processpendingloanunkid", GetType(Integer))
                dtTemp.Columns.Add("application_no", GetType(String))
                dtTemp.Rows.Add(-1, "Select")
                dtTable = dtTemp
            End If


            cboApplicationNo.DataValueField = "processpendingloanunkid"
            cboApplicationNo.DataTextField = "application_no"
            cboApplicationNo.DataSource = dtTable
            cboApplicationNo.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objProcesspendingloan = Nothing
        End Try
    End Sub
#End Region

End Class
