﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_Payroll_Total_Variance
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private objPayrollReconciliation As clsPayrollTotalVariance
    Private mintFirstOpenPeriod As Integer = 0 'Sohail (20 Oct 2014)

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPayrollTotalVariance"
    'Anjan [04 June 2014 ] -- End
    'Sohail (23 Nov 2016) -- Start
    'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    Private mintFromYearUnkId As Integer
    Private mintToYearUnkId As Integer
    'Sohail (23 Nov 2016) -- End
    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private dtHeads As DataTable
    Private mstrInfoHeadIDs As String = ""
    'Sohail (13 Jan 2020) -- End

#End Region

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
#Region " Private Enum "
    Private Enum enHeadTypeId
        TranHeadType = 1
        IgnoreZeroVariance = 2
        ShowVariancePercColumn = 3
        InfoHeadIDs = 4
        IncludeInactiveEmployee = 5
    End Enum
#End Region
    'Sohail (13 Jan 2020) -- End


#Region " Page's Event "

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintFirstOpenPeriod", mintFirstOpenPeriod)
            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Me.ViewState.Add("mstrFromDatabaseName", mstrFromDatabaseName)
            Me.ViewState.Add("mstrToDatabaseName", mstrToDatabaseName)
            Me.ViewState.Add("mintFromYearUnkId", mintFromYearUnkId)
            Me.ViewState.Add("mintToYearUnkId", mintToYearUnkId)
            'Sohail (23 Nov 2016) -- End
            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            ViewState("dtHeads") = dtHeads
            ViewState("mstrInfoHeadIDs") = mstrInfoHeadIDs
            'Sohail (13 Jan 2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPayrollReconciliation = New clsPayrollTotalVariance(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

       

            Dim mintCurrPeriodID As Integer = 0
            If Not IsPostBack Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Call SetLanguage()
                'Anjan [04 June 2014 ] -- End
                FillCombo()
                ResetValue()
                'Sohail (13 Jan 2020) -- Start
                'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
                Call FillList()
                'Sohail (13 Jan 2020) -- End
            Else
                mintFirstOpenPeriod = CInt(ViewState("mintFirstOpenPeriod"))
                'Sohail (23 Nov 2016) -- Start
                'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                mstrFromDatabaseName = ViewState("mstrFromDatabaseName").ToString
                mstrToDatabaseName = ViewState("mstrToDatabaseName").ToString
                mintFromYearUnkId = CInt(ViewState("mintFromYearUnkId"))
                mintToYearUnkId = CInt(ViewState("mintToYearUnkId"))
                'Sohail (23 Nov 2016) -- End
                'Sohail (13 Jan 2020) -- Start
                'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
                dtHeads = CType(ViewState("dtHeads"), DataTable)
                mstrInfoHeadIDs = ViewState("mstrInfoHeadIDs").ToString
                'Sohail (13 Jan 2020) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Public Function"

    Public Sub FillCombo()

        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_Year")), 1) 'Sohail (27 Oct 2014)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True, , , , Session("Database_Name"))
            'Nilay (10-Feb-2016) -- Start
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            'Nilay (10-Feb-2016) -- End
            'Shani(20-Nov-2015) -- End

            cboPeriodFrom.DataValueField = "periodunkid"
            cboPeriodFrom.DataTextField = "name"
            cboPeriodFrom.DataSource = dsList.Tables(0)
            cboPeriodFrom.DataBind()
            'cboPeriodFrom.SelectedValue = 0
            cboPeriodFrom.SelectedValue = CStr(mintFirstOpenPeriod)

            'Shani (23-Jul-2016) -- Start
            Call cboPeriodFrom_SelectedIndexChanged(cboPeriodFrom, Nothing)
            'Shani (23-Jul-2016) -- End


            cboPeriodTo.DataValueField = "periodunkid"
            cboPeriodTo.DataTextField = "name"
            cboPeriodTo.DataSource = dsList.Tables(0).Copy
            cboPeriodTo.DataBind()
            'cboPeriodTo.SelectedValue = 0
            cboPeriodTo.SelectedValue = CStr(mintFirstOpenPeriod)
            'Shani (23-Jul-2016) -- Start
            Call cboPeriodTo_SelectedIndexChanged(cboPeriodTo, Nothing)
            'Shani (23-Jul-2016) -- End

            dsList = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("HeadType")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dicHeadType As New Dictionary(Of Integer, String)
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                dicHeadType.Add(CInt(dsList.Tables(0).Rows(i).Item("Id")), dsList.Tables(0).Rows(i).Item("Name").ToString)
            Next

            Me.ViewState.Add("HeadTypeList", dicHeadType)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            dsList = objTranHead.getComboList(Session("Database_Name").ToString, "List", False, enTranHeadType.Informational)

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            If mstrInfoHeadIDs.Trim <> "" Then
                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (mstrInfoHeadIDs.Split(CChar(",")).Contains(p.Item("tranheadunkid").ToString)) Select (p)).ToList
                For Each r As DataRow In lstRow
                    r.Item("IsChecked") = True
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            dtHeads = dsList.Tables(0)
            dtHeads.DefaultView.Sort = "IsChecked DESC, name "

            With dgHeads
                .DataSource = dtHeads.DefaultView
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (13 Jan 2020) -- End

    Public Sub ResetValue()
        Try

            'If cboPeriodFrom.Items.Count > 0 Then
            '    cboPeriodFrom.SelectedIndex = 0
            'End If
            'If cboPeriodTo.Items.Count >= 1 Then
            '    cboPeriodTo.SelectedIndex = 0
            'End If
            cboPeriodFrom.SelectedValue = CStr(mintFirstOpenPeriod)
            cboPeriodTo.SelectedValue = CStr(mintFirstOpenPeriod)
            If cboPeriodFrom.SelectedIndex > 0 Then cboPeriodFrom.SelectedIndex = cboPeriodTo.SelectedIndex - 1
            cboTrnHeadType.SelectedValue = "0"
            objPayrollReconciliation.setDefaultOrderBy(0)
            'Hemant (31 Dec 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting. in 76.1.
            chkIgnoreZeroVariance.Checked = True
            'Hemant (31 Dec 2018) -- End

            chkInactiveemp.Checked = False

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            Call GetValue()
            'Sohail (13 Jan 2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PayrollTotalVariance)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.TranHeadType
                            cboTrnHeadType.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            Call cboTrnHeadType_SelectedIndexChanged(cboTrnHeadType, New System.EventArgs)

                        Case enHeadTypeId.IgnoreZeroVariance
                            chkIgnoreZeroVariance.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowVariancePercColumn
                            chkShowVariancePercentageColumns.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.InfoHeadIDs
                            mstrInfoHeadIDs = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (13 Jan 2020) -- End

    Private Function SetFilter() As Boolean
        Try

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            mstrInfoHeadIDs = ""
            'Sohail (13 Jan 2020) -- End

            If CInt(cboPeriodFrom.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriodFrom.Focus()
                Exit Function
            End If
            If CInt(cboPeriodTo.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), Me)
                'Sohail (23 Mar 2019) -- End

                cboPeriodTo.Focus()
                Exit Function
            End If
            If cboPeriodTo.SelectedIndex <= cboPeriodFrom.SelectedIndex Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, " To Period cannot be less than or equal to Form Period"), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriodTo.Focus()
                Exit Function
            End If

            objPayrollReconciliation.SetDefaultValue()

            objPayrollReconciliation._PeriodId1 = CInt(cboPeriodFrom.SelectedValue)
            objPayrollReconciliation._PeriodName1 = cboPeriodFrom.SelectedItem.Text

            objPayrollReconciliation._PeriodId2 = CInt(cboPeriodTo.SelectedValue)
            objPayrollReconciliation._PeriodName2 = cboPeriodTo.SelectedItem.Text

            If CInt(cboTrnHeadType.SelectedValue) > 0 Then
                objPayrollReconciliation._TranHeadTypeId = CInt(cboTrnHeadType.SelectedValue)
                objPayrollReconciliation._TranHeadTypeName = cboTrnHeadType.SelectedItem.Text
            End If

            objPayrollReconciliation._DicTranHeadType = CType(Me.ViewState("HeadTypeList"), Global.System.Collections.Generic.Dictionary(Of Integer, String))

            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPayrollReconciliation._FromDatabaseName = CStr(Me.ViewState("FromDatabase"))
            'objPayrollReconciliation._ToDatabaseName = CStr(Me.ViewState("ToDatabase"))
            objPayrollReconciliation._FromDatabaseName = mstrFromDatabaseName
            objPayrollReconciliation._ToDatabaseName = mstrToDatabaseName

            objPayrollReconciliation._FromYearUnkId = mintFromYearUnkId
            objPayrollReconciliation._ToYearUnkId = mintToYearUnkId
            'Sohail (23 Nov 2016) -- End

            objPayrollReconciliation._BaseCurrencyId = CInt(Session("Base_CurrencyId"))
            objPayrollReconciliation._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objPayrollReconciliation._UserUnkId = CInt(Session("UserId"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            'Hemant (31 Dec 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting. in 76.1.
            objPayrollReconciliation._IgnoreZeroVariance = chkIgnoreZeroVariance.Checked
            'Hemant (31 Dec 2018) -- End

            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            objPayrollReconciliation._ShowVariancePercentageColumns = chkShowVariancePercentageColumns.Checked
            'Hemant (22 Jan 2019) -- End

            objPayrollReconciliation._IncludeActiveEmployee = chkInactiveemp.Checked

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            mstrInfoHeadIDs = String.Join(",", (From p In dtHeads Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("tranheadunkid").ToString)).ToArray)
            objPayrollReconciliation._InfoHeadIDs = mstrInfoHeadIDs
            'Sohail (13 Jan 2020) -- End

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
#End Region

#Region "Combobox Event"

    Protected Sub cboPeriodFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodFrom.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriodFrom.SelectedValue)
            'Nilay (10-Feb-2016) -- Start
            'dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            'Nilay (10-Feb-2016) -- End

            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If Me.ViewState("FromDatabase") Is Nothing Then
            '    Me.ViewState.Add("FromDatabase", mstrFromDatabaseName)
            'Else
            '    Me.ViewState("FromDatabase") = mstrFromDatabaseName
            'End If
            mintFromYearUnkId = objPeriod._Yearunkid
            'Sohail (23 Nov 2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPeriodTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodTo.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriodTo.SelectedValue)
            'Nilay (10-Feb-2016) -- Start
            'dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            'Nilay (10-Feb-2016) -- End

            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If

            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If Me.ViewState("ToDatabase") Is Nothing Then
            '    Me.ViewState.Add("ToDatabase", mstrToDatabaseName)
            'Else
            '    Me.ViewState("ToDatabase") = mstrToDatabaseName
            'End If
            mintToYearUnkId = objPeriod._Yearunkid
            'Sohail (23 Nov 2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Protected Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            If CInt(cboTrnHeadType.SelectedValue) <= 0 OrElse CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                pnlInfoheads.Enabled = True
            Else
                pnlInfoheads.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (13 Jan 2020) -- End

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objPayrollReconciliation.setDefaultOrderBy(0)
            'Nilay (10-Feb-2016) -- Start
            'objPayrollReconciliation.generateReport(0, Aruti.Data.enPrintAction.None, Aruti.Data.enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            Dim dsPeriodList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, 0, CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)

            Dim objYear As New clsCompany_Master

            Dim objFromPeriod As New clscommom_period_Tran
            Dim intFromYearunkid As Integer = (From p In dsPeriodList.Tables("Period") Where (CInt(p.Item("periodunkid")) = CInt(cboPeriodFrom.SelectedValue)) Select (CInt(p.Item("yearunkid")))).FirstOrDefault
            objYear._YearUnkid = intFromYearunkid
            objFromPeriod._Periodunkid(objYear._DatabaseName) = CInt(cboPeriodFrom.SelectedValue)

            Dim objToPeriod As New clscommom_period_Tran
            Dim intToYearunkid As Integer = (From p In dsPeriodList.Tables("Period") Where (CInt(p.Item("periodunkid")) = CInt(cboPeriodTo.SelectedValue)) Select (CInt(p.Item("yearunkid")))).FirstOrDefault
            objYear._YearUnkid = intToYearunkid
            objToPeriod._Periodunkid(objYear._DatabaseName) = CInt(cboPeriodTo.SelectedValue)

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            objPayrollReconciliation._FromPeriodStart = objFromPeriod._Start_Date
            objPayrollReconciliation._FromPeriodEnd = objFromPeriod._End_Date
            objPayrollReconciliation._ToPeriodStart = objToPeriod._Start_Date
            objPayrollReconciliation._ToPeriodEnd = objToPeriod._End_Date
            'Sohail (13 Jan 2020) -- End

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            objPayrollReconciliation.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                       objFromPeriod._Start_Date, objToPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, _
                                                       Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                                       0, Aruti.Data.enPrintAction.None, Aruti.Data.enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Nilay (10-Feb-2016) -- End

            Session("objRpt") = objPayrollReconciliation._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani (23-Jul-2016) -- Start
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            If objPayrollReconciliation._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani (23-Jul-2016) -- End


            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("FromDatabase") = Nothing
            Me.ViewState("ToDatabase") = Nothing
            'Hemant (17 Oct 2019) -- Start
            'Response.Redirect("~\UserHome.aspx", False)
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
            'Hemant (17 Oct 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
#Region " CheckBox's Events "

    Protected Sub chkHeadSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkHeadSelectAll As CheckBox = TryCast(sender, CheckBox)
            'Dim dtTranHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            If chkHeadSelectAll Is Nothing Then Exit Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In dgHeads.Rows
                cb = CType(dgHeads.Rows(gvr.RowIndex).FindControl("chkHeadSelect"), CheckBox)
                cb.Checked = chkHeadSelectAll.Checked
            Next
            For Each gvr As DataRow In dtHeads.Rows
                gvr("IsChecked") = chkHeadSelectAll.Checked
            Next
            dtHeads.AcceptChanges()

            'marrDetailedSalaryBreakdownReportHeadsIds.Clear()
            'marrDetailedSalaryBreakdownReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeadSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = TryCast(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = dtHeads.Select("tranheadunkid = '" & CInt(dgHeads.DataKeys(gvRow.RowIndex).Value) & "'")
                If drRow.Length > 0 Then
                    drRow(0)("IsChecked") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Protected Sub txtSearchTranHeads_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTranHeads.TextChanged
        Try
            If dtHeads.Rows.Count <= 0 Then Exit Sub
            Dim dtView As DataView = dtHeads.DefaultView
            dtView.RowFilter = "Name LIKE '%" & txtSearchTranHeads.Text & "%' OR Code LIKE '%" & txtSearchTranHeads.Text & "%' "
            dtView.Sort = "IsChecked DESC, name "

            With dgHeads
                .DataSource = dtView
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Sohail (13 Jan 2020) -- End

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("FromDatabase") = Nothing
    '        Me.ViewState("ToDatabase") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            ' Me.Closebutton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblPeriodTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriodTo.ID, Me.lblPeriodTo.Text)
            Me.lblPeriodFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriodFrom.ID, Me.lblPeriodFrom.Text)
            Me.lblTrnHeadType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrnHeadType.ID, Me.lblTrnHeadType.Text)
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            Me.chkShowVariancePercentageColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowVariancePercentageColumns.ID, Me.chkShowVariancePercentageColumns.Text)
            'Hemant (22 Jan 2019) -- End
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            Me.dgHeads.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgHeads.Columns(1).FooterText, Me.dgHeads.Columns(1).HeaderText)
            Me.dgHeads.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgHeads.Columns(2).FooterText, Me.dgHeads.Columns(2).HeaderText)
            'Sohail (13 Jan 2020) -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014 ] -- End



End Class
