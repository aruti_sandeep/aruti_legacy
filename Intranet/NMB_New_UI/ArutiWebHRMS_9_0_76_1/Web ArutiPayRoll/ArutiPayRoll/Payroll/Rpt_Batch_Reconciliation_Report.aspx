﻿<%@ Page Title="Batch Reconciliation Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_Batch_Reconciliation_Report.aspx.vb" Inherits="Payroll_Rpt_Batch_Reconciliation_Report" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Batch Reconciliation Report"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblPeriod" runat="server" Text="Pay Period"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblBatch" runat="server" Text="Batch"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboBatch" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc9:Export runat="server" ID="Export" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
