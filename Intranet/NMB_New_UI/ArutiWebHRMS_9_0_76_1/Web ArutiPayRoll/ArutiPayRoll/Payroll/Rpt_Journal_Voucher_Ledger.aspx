﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Journal_Voucher_Ledger.aspx.vb"
    Inherits="Payroll_Rpt_Journal_Voucher_Ledger" Title="Journal Voucher Ledger" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList"
    TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>--%>

    <script type="text/javascript">

    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

        if (charCode == 13)
            return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popup_PayrollJournal" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnPayrollJournalClose" PopupControlID="pnl_PayrollJournal"
                    TargetControlID="HiddenField1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_PayrollJournal" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Payroll Journal Columns Export" CssClass="form-label" />
                                </h2>
                            </div>
                            <div class="body" style="max-height:525px">
                                <div class="row clearfix">
                                    <div class="table-responsive" style="max-height: 255px;">
                                        <asp:GridView ID="lvPayrollJournalColumns" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="False" Width="99%" DataKeyNames="ID" CssClass="table table-hover table-bordered"
                                            RowStyle-Wrap="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="objcolhNCheck">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" ToolTip="All"
                                                            OnCheckedChanged="lvPayrollJournalColumns_ItemChecked" Text=" " />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                            AutoPostBack="true" ToolTip="Checked" OnCheckedChanged="lvPayrollJournalColumns_ItemChecked"
                                                            Text=" " />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Name" HeaderText="Payroll Journal Columns" ReadOnly="true"
                                                    FooterText="colhPayrollJournalColumns" />
                                                <asp:BoundField DataField="ID" ReadOnly="true" Visible="false" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkUp" CommandArgument="up" runat="server" Text="&#x25B2;" OnClick="ChangeLocation" />
                                                        <asp:LinkButton ID="lnkDown" CommandArgument="down" runat="server" Text="&#x25BC;"
                                                            OnClick="ChangeLocation" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowColumnHeaderPayrollJournal" runat="server" Text="Show Column Header on Report" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblExportMode" runat="server" Text="Export Mode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboExportMode" runat="server" AutoPostBack="true" Width="250px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDateFormat" runat="server" Text="Date Format" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtDateFormat" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSaveAsTXT" runat="server" Text="Save As TXT" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkTABDelimiter" runat="server" Text="TAB Delimiter" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnPayrollJournalSaveSelection" runat="server" Text="Save Selection"
                                    CssClass="btn btn-default" />
                                <asp:Button ID="btnPayrollJournalOK" runat="server" Text="OK" CssClass="btn btn-primary" />
                                <asp:Button ID="btnPayrollJournalClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc3:AnalysisBy ID="popupAnalysisBy" runat="server" />
                <uc9:Export runat="server" ID="Export" />
                <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                <uc9:ConfirmYesNo ID="popupSure" runat="server" Title="Are you sure?" />
                
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Journal Voucher Ledger" CssClass="form-label"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Employee Allocation Filter..."
                                            Text="Employee Allocation Filter...">
                                                    <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:Label ID="lblExRate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDebitAmountFrom" runat="server" Text="Debit Amount From" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtDebitAmountFrom" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDebitAmountTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtDebitAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCreditAMountFrom" runat="server" Text="Credit Amount From" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCreditAmountFrom" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCreditAMountTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCreditAMountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboBranch" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCCGroup" runat="server" Text="C.Center Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCCGroup" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkShowGroupByCCGroup" runat="server" Text="Show Group By Cost Center Group"
                                            AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlCustomCCenter" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCustomCCenter" runat="server" Text="Custom C.Center" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCustomCCenter" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInvoiceRef" runat="server" Text="Invoice Reference" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtInvoiceRef" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkIncludeEmployerContribution" runat="server" Text="Include Employer Contribution">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkIgnoreZero" runat="server" Text="Ignore Zero"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSummaryBottom" runat="server" Text="Show Summary Report at the Bottom"
                                            AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSummaryNewPage" runat="server" Text="Show Summary Report on New Page"
                                            AutoPostBack="true"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowColumnHeader" runat="server" Text="Show Column Header" AutoPostBack="true">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkIncludeEmpCodeNameOnDebit" runat="server" Text="Include Emp. code and Name on Debit"
                                            AutoPostBack="false"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTranHead" runat="server" Text="Transaction Head" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTranHead" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPostingDate" runat="server" Text="Posting Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpPostingDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCostCenter" runat="server" Text="Default C. Center" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCostCenter" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkPayrollJournalReport" runat="server" Text="Payroll Journal Report..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkPayrollJournalExport" runat="server" Text="Payroll Journal Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkiScalaJVExport" runat="server" Text="iScala JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSunJV5Export" runat="server" Text="Sun JV 5 Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSunJVExport" runat="server" Text="Sun JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkTBCJVExport" runat="server" Text="SAP JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkiScala2JVExport" runat="server" Text="iScala2 JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkXEROJVExport" runat="server" Text="XERO JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkNetSuiteERPJVExport" runat="server" Text="NetSuite ERP JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkFlexCubeRetailJVExport" runat="server" Text="Flex Cube Retail JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSunAccountProjectJVExport" runat="server" Text="Sun Account (Project JV) Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkDynamicsNavJVExport" runat="server" Text="Dynamics Nav JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkFlexCubeUPLDJVExport" runat="server" Text="Flex Cube UPLD JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAPJVBSOneExport" runat="server" Text="SAP JV BS One Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkFlexCubeJVExport" runat="server" Text="Flex Cube JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkFlexCubeJVPostOracle" runat="server" Text="Flex Cube JV Post to Oracle..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkFlexCubeLoanBatchExport" runat="server" Text="Flex Cube Loan Batch Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkBRJVExport" runat="server" Text="BR-JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkBRJVPostSQL" runat="server" Text="BR-JV Post to SQL..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAPECC6_0JVExport" runat="server" Text="SAP ECC6.0-JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAGE300JVExport" runat="server" Text="SAGE 300-JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkPASTELV2JVExport" runat="server" Text="PASTEL V2-JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAGE300ERPJVExport" runat="server" Text="SAGE 300 ERP JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAGE300ERPImport" runat="server" Text="SAGE 300 ERP Importation Temp..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkCoralSunJVExport" runat="server" Text="Coral Sun JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkCargoWiseJVExport" runat="server" Text="Cargo Wise JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAPStandardJVExport" runat="server" Text="SAP Standard JV Export......"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAGEEvolutionJVExport" runat="server" Text="SAGE Evolution JV Export......"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkHakikaBankJVExport" runat="server" Text="Hakika Bank JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkDynamicsNavisionJVExport" runat="server" Text="Dynamics Navision JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkDynamicNavisionJVPostDB" runat="server" Text="Dynamic Navision JV Post to DB..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSunSystemV6_3JVExport" runat="server" Text="SunSystem v6.3 JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkSAGE200JVExport" runat="server" Text="SAGE 200 JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkOrbitCBSJVExport" runat="server" Text="ORBIT CBS JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkOracleCBSJVXLSExport" runat="server" Text="ORACLE CBS JV XLS Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkOracleCBSJVXMLExport" runat="server" Text="ORACLE CBS JV XML Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkFlexcubeCBSJVExport" runat="server" Text="Flexcube CBS JV Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlLoanSchemeList" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblSearchScheme" runat="server" Text="Search Scheme" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtSearchScheme" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 400px;">
                                                <asp:GridView ID="gvScheme" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                    Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="objchkSelectAllLoanScheme" runat="server" AutoPostBack="true" OnCheckedChanged="objchkSelectAllLoanScheme_OnCheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="objdgcolhCheck" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                    OnCheckedChanged="objdgcolhCheck_OnCheckedChanged" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true" Visible="false" />
                                                        <asp:BoundField DataField="code" HeaderText="Scheme Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                            FooterText="colhCode" HeaderStyle-Width="30%" ItemStyle-Width="30%" />
                                                        <asp:BoundField DataField="name" HeaderText="Scheme Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                            FooterText="colhName" HeaderStyle-Width="60%" ItemStyle-Width="60%" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlTranHead" runat="server" ScrollBars="Auto">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 150px;">
                                                <asp:GridView ID="gbFilterTBCJV" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                    ShowFooter="False" Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_OnCheckedChanged"
                                                                    Text=" " />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="code" HeaderText="Code" FooterText="colhEContribHeadCode" />
                                                        <asp:BoundField DataField="name" HeaderText="Employer Contribution Head" FooterText="colhEContribHead" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
