﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Bank_Payment_List.aspx.vb"
    Inherits="Payroll_Rpt_Bank_Payment_List" Title="Bank Payment List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }

        $("body").on("click", "[id*=chkStagecheckall]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest(".card .inner-card");
            $("[id*=chkSelect]", grid).prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest(".body");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            $('#<%= dgvDataList.ClientID %> tbody tr:visible td [id*=ChkgvSelect]').prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest(".body");
            var chkHeader = $("[id*=chkAllSelect]", grid);

            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Bank Payment List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboReportType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeeName" runat="server" Text="Emp.Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployeeName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyBankName" runat="server" Text="Company Bank Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCompanyBankName" runat="server"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyBranchName" runat="server" Text="Company Bank Branch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCompanyBranchName" runat="server"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyAccountNo" runat="server" Text="Company Account No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCompanyAccountNo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportMode" runat="server" Text="Report Mode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboReportMode" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChequeNo" runat="server" Text="Cheque No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboChequeNo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBankName" runat="server" Text="Emp.Bank Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCondition" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboBankName" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranchName" runat="server" Text="Branch Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboBranchName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCountryName" runat="server" Text="Country Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCountry" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Paid Currency" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCurrency" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlOtherSetting" runat="server" Visible="false">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPresentDays" runat="server" Text="Present Days" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboPresentDays" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblBasicSalary" runat="server" Text="Basic Salary" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboBasicSalary" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblSocialSecurity" runat="server" Text="Social Security" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboSocialSecurity" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblExtraIncome" runat="server" Text="Extra Income" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboExtraIncome" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAbsentDays" runat="server" Text="Absent Deduction" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboAbsentDays" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblIdentityType" runat="server" Text="Identity Type" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtIdentityType" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPaymentType" runat="server" Text="Payment Type" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPaymentType" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCustomCurrFormat" runat="server" Text="Custom Curr. Format" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtCustomCurrFormat" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmountTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCutOffAmount" runat="server" Text="Cut Off Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCutOffAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPostingDate" runat="server" Text="Posting Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpPostingDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipRepo" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboMembershipRepo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSignatory1" runat="server" Text="Show Signatory 1" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkDefinedSignatory" runat="server" Text="Show Defined Signatory"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSignatory2" runat="server" Text="Show Signatory 2" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkDefinedSignatory2" runat="server" Text="Show Defined Signatory 2"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSignatory3" runat="server" Text="Show Signatory 3" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkDefinedSignatory3" runat="server" Text="Show Defined Signatory 3"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowGroupByBankBranch" runat="server" Text="Show Group By Bank/Branch"
                                            AutoPostBack="true" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowBankCode" runat="server" Text="Show Bank Code" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkEmployeeSign" runat="server" Text="Show Employee Sign" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowBranchCode" runat="server" Text="Show Branch Code" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowFNameSeparately" runat="server" Text="Show First Name,Other Name and Surname in Separate Columns"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPayrollPeriod" runat="server" Text="Show Payroll Period"
                                            Checked="true" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSaveAsTXT_WPS" runat="server" Text="Save As TXT" CssClass="filled-in"
                                            Checked="true"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSortCode" runat="server" Text="Show Sort Code" Checked="true"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowAccountType" runat="server" Text="Show Account Type" Checked="false"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEmployeeCode" runat="server" Text="Show Employee Code" Checked="true"
                                            AutoPostBack="true" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowReportHeader" runat="server" Text="Show Report Header" Checked="true"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkLetterhead" runat="server" Text="Letterhead" Checked="false"
                                            CssClass="filled-in" AutoPostBack="false"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkAddresstoEmployeeBank" runat="server" Text="Address to Employee Bank"
                                            Checked="false" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSelectedBankInfo" runat="server" Text="Show Selected Bank Info"
                                            Checked="false" AutoPostBack="false" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowCompanyGrpInfo" runat="server" Text="Show Company Group Info"
                                            Checked="false" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkEFTCityBankExport" runat="server" Text="EFT City Direct Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_CBA_Export" runat="server" Text="EFT CBA Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_EXIM_Export" runat="server" Text="EFT EXIM Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_Custom_CSV_Export" runat="server" Text="EFT Custom CSV Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_Custom_XLS_Export" runat="server" Text="EFT Custom XLS Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_FlexCubeRetailGEFU_Export" runat="server" Text="EFT Flex Cube Retail - GEFU Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_ECO_Bank" runat="server" Text="EFT ECO Bank..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkBankPaymentLetter" runat="server" Text="Bank Payment Letter..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTBarclaysBankExport" runat="server" Text="Barclays Bank Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTNationalBankMalawi" runat="server" Text="EFT National Bank Malawi..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_FNB_Bank_Export" runat="server" Text="EFT FNB Bank Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTStandardCharteredBank_S2B" runat="server" Text="EFT Standard Chartered Bank(S2B)..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_ABSA_Bank" runat="server" Text="EFT ABSA Bank..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTNationalBankMalawiXLSX" runat="server" Text="EFT National Bank Malawi XLSX..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTEquityBankKenya" runat="server" Text="EFT Equity Bank Kenya..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTNationalBankKenya" runat="server" Text="EFT National Bank Kenya..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTCitiBankKenya" runat="server" Text="EFT City Bank Kenya..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTGenericCSV" runat="server" Text="EFT Generic CSV..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTBankOfKigali" runat="server" Text="EFT Bank Of Kigali..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_NCBA" runat="server" Text="EFT NCBA..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_NBC" runat="server" Text="EFT NBC..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_DTB" runat="server" Text="EFT DTB..."></asp:LinkButton>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkMobileMoneyEFTMPesaExport" runat="server" Text="EFT MPesa Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkDynamicNavisionExport" runat="server" Text="Dynamic Nav Export..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkDynamicNavisionPaymentPostDB" runat="server" Text="Dynamic Nav Payment Post to DB..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkEFTGenericCSVPostWeb" runat="server" Text="EFT Generic CSV Post To Web..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkEFTPaymentBatchApproval" runat="server" Text="EFT Payment Batch Approval..."></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkEFTPaymentBatchReconciliation" runat="server" Text="EFT Payment Batch Reconciliation..."></asp:LinkButton>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlGenericPanel" runat="server" Visible="false">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 500px; overflow: auto;">
                                                <asp:GridView ID="dgBatch" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                    Width="99%" CssClass="table table-hover table-bordered" DataKeyNames="Batch">
                                                    <Columns>
                                                        <asp:BoundField DataField="Batch" HeaderText="Batch" ReadOnly="true" FooterText="dgColhBatch" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="dgColhStatus" />
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="dgColhPostedData"
                                                            HeaderText="Export">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnExport" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    ToolTip="Export" CommandName="Export">
                                                                        <i class="fas fa-solid fa-download"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="dgColhReconciliation" 
                                                            HeaderText="Reconciliation">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkReconciliation" runat="server" CommandName="Reconciliation"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ToolTip="Reconciliation Query">
                                                                        <i class="fas fa-solid fa-check-double"></i>                                                                        
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_EFTCustom" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnEFTClose" PopupControlID="pnl_EFTCustom" TargetControlID="HiddenField1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_EFTCustom" runat="server" Style="display: none; width: 450px"
                    CssClass="card modal-dialog">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <%-- <div class="card">--%>
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblTitle" runat="server" Text="EFT Custom Columns Export" />
                                    </h2>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 500px; overflow: auto;">
                                                <asp:GridView ID="lvEFTCustomColumns" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="False" Width="99%" CssClass="table table-hover table-bordered" DataKeyNames="ID">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="objcolhNCheck">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" ToolTip="All"
                                                                    OnCheckedChanged="lvEFTCustomColumns_ItemChecked" Text=" " CssClass="filled-in" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                    AutoPostBack="true" ToolTip="Checked" OnCheckedChanged="lvEFTCustomColumns_ItemChecked"
                                                                    CssClass="filled-in" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Name" HeaderText="EFT Custom Columns" ReadOnly="true"
                                                            FooterText="colhEFTCustomColumns" />
                                                        <asp:BoundField DataField="ID" ReadOnly="true" Visible="false" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                    Text="&#x25B2;" OnClick="ChangeLocation" />
                                                                <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                    Text="&#x25BC;" OnClick="ChangeLocation" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowColumnHeader" runat="server" Text="Show Column Header on Report"
                                                CssClass="filled-in" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblMembership" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboMembership" runat="server" AutoPostBack="false"
                                                    Width="250px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDateFormat" runat="server" Text="Date Format" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDateFormat" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkSaveAsTXT" runat="server" Text="Save As TXT" CssClass="filled-in" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkTABDelimiter" runat="server" Text="TAB Delimiter" CssClass="filled-in" />
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnEFTSaveSelection" runat="server" Text="Save Selection" CssClass="btn btn-default" />
                                    <asp:Button ID="btnEFTOK" runat="server" Text="OK" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnEFTClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </div>
                           <%-- </div>--%>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupReconciliation" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnRClose" PopupControlID="pnlReconciliation" TargetControlID="HfReconciliation">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlReconciliation" runat="server" Style="display: none;" CssClass="card modal-dialog modal-xlg">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblRTitle" runat="server" Text="Payment Batch Posting" CssClass="form-label" />
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblRFilterCriteria" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboRPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRBatch" runat="server" Text="Batch" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboRBatch" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboRStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnReconciliation" runat="server" Text="Reconciliation" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnRSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblBankInfo" runat="server" Text="Bank Information" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblBankGroup" runat="server" Text="Bank Group" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList data-live-search="true" ID="cboBankGroup" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblBankBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList data-live-search="true" ID="cboBankBranch" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div style="float: left">
                                                        <asp:RadioButton ID="radApplyToChecked" runat="server" Text="Apply To Checked" GroupName="APPLY" />
                                                        <asp:RadioButton ID="radApplyToAll" runat="server" Text="Apply To All" GroupName="APPLY" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnRApply" runat="server" Text="Apply" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px; overflow: auto;">
                                            <asp:DataGrid ID="dgvDataList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                Width="99%" CssClass="table table-hover table-bordered">
                                                <ItemStyle />
                                                <Columns>
                                                     <%--0--%>
                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        FooterText="objdgcolhSelect">
                                                        <headertemplate>
                                                            <asp:CheckBox ID="ChkAll" runat="server" Text=" " CssClass="chk-sm" />
                                                        </headertemplate>
                                                                    <itemtemplate>
                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                        </itemtemplate>
                                                        <headerstyle />
                                                        <itemstyle />
                                                    </asp:TemplateColumn>
                                                   <%--1--%>
                                                    <asp:BoundColumn DataField="batchname" HeaderText="Batch" ReadOnly="true" FooterText="dgcolhBatchId"  />
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="employeeCode" HeaderText="Employee Code" ReadOnly="true"
                                                        ItemStyle-Width="150px" FooterText="dgcolhEmployeeCode" />
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="BankName" HeaderText="Bank Name" ReadOnly="true" FooterText="dgcolhBankGroup"  />
                                                    <%--4--%>
                                                    <asp:TemplateColumn HeaderText="Branch" ItemStyle-HorizontalAlign="Center" FooterText="dgcolhBranch"
                                                        ItemStyle-Width="250px">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cbodgcolhBranch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cbodgcolhBranch_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="branchname" HeaderText="objdgcolhBranchName" Visible="false"
                                                        FooterText="objdgcolhBranchName"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:TemplateColumn HeaderText="Account No" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="180px"
                                                        FooterText="dgcolhAccountNo">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhAccountNo" runat="server" Text='<%# Eval("accountno") %>'
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle />
                                                    </asp:TemplateColumn>
                                                    <%-- <asp:BoundColumn DataField="accountno" HeaderText="Account No" ReadOnly="true" FooterText="dgcolhAccountNo" />--%>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="branchunkid" HeaderText="objdgcolhBranchUnkid" Visible="false"
                                                        FooterText="objdgcolhBranchUnkid"></asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="objdgcolhEmployeeUnkid" Visible="false"
                                                        FooterText="objdgcolhEmployeeUnkid"></asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="dpndtbeneficetranunkid" HeaderText="objdgcolhDpndtBeneficeTranUnkid"
                                                        Visible="false" FooterText="objdgcolhDpndtBeneficeTranUnkid"></asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="Amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount" />
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Button ID="btnRExport" runat="server" Text="Export" CssClass="btn btn-default" />
                                </div>
                                <asp:Button ID="btnSubmitForApproval" runat="server" Text="Submit For Approval" CssClass="btn btn-primary"
                                    Visible="false" />
                                <asp:Button ID="btnRClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="HfReconciliation" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupBatchApproval" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnAClose" PopupControlID="pnlBatchApproval" TargetControlID="HfBatchApproval">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlBatchApproval" runat="server" Style="display: none;" CssClass="card modal-dialog modal-xlg">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblATitle" runat="server" Text="Payment Batch Approval" CssClass="form-label" />
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAFilterCriteria" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboAPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboAStatus" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblABatch" runat="server" Text="Batch" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="cboABatch" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnASearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <%--<div class="card">--%>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 300px; overflow: auto;">
                                                            <asp:GridView ID="dgvBatchApprovalList" runat="server" AutoGenerateColumns="False"
                                                                AllowPaging="False" Width="99%" CssClass="table table-hover table-bordered" DataKeyNames="batchname,employeeunkid,branchunkid,accountno,dpndtbeneficetranunkid">
                                                                <Columns>
                                                                    <asp:BoundField DataField="batchname" HeaderText="BatchId" ReadOnly="true" FooterText="dgcolhBatchId" />
                                                                    <asp:BoundField DataField="employeeCode" HeaderText="Employee Code" ReadOnly="true"
                                                                        FooterText="dgcolhEmployeeCode" />
                                                                    <asp:BoundField DataField="bankname" HeaderText="Bank Name" ReadOnly="true" FooterText="dgcolhBankGroup" />
                                                                    <asp:BoundField DataField="branchname" HeaderText="Branch Name" ReadOnly="true" FooterText="dgcolhBranch" />
                                                                    <asp:BoundField DataField="accountno" HeaderText="Account No" ReadOnly="true" FooterText="dgcolhAccountNo" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnApproveReject" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnSendRequest" runat="server" Text="Send Request" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnAClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                                <asp:HiddenField ID="HfBatchApproval" runat="server" />
                                            </div>
                                        </div>
                                        <%-- </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc9:Export runat="server" ID="Export" />
                <uc8:ConfirmYesNo ID="popupReconciliationYesNo" runat="server" Title="Reconciliation has been already done for Selected Batch, Are you sure you want to Reconciliation again for Selected Batch?"
                    Message="Reconciliation has been already done for Selected Batch, Are you sure you want to Reconciliation again for Selected Batch?" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
