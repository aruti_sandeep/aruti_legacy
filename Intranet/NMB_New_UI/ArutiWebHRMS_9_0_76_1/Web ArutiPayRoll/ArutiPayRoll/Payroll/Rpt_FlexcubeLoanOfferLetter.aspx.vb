﻿
#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports ArutiReports

#End Region
Partial Class Payroll_Rpt_FlexcubeLoanOfferLetter
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmFlexcubeLoanOfferLetter"
    Private mdicLoanType As New Dictionary(Of Integer, String)

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            

            mdicLoanType.Add(1, "GENERAL")
            mdicLoanType.Add(2, "CAR")
            mdicLoanType.Add(3, "REFINANCE MORTGAGE")
            mdicLoanType.Add(4, "PURCHASE MORTGAGE")
            mdicLoanType.Add(5, "CONSTRUCTION MORTGAGE")

            If IsPostBack = False Then
                Call FillCombo()
                cboLoanScheme_SelectedIndexChanged(New Object, New EventArgs())
            Else
                mdicLoanType = Me.ViewState("mdicLoanType")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdicLoanType") = mdicLoanType
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim dsList As New DataSet
        Try
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              Session("UserAccessModeSetting").ToString(), True, _
                                              CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                              blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, " ISNULL(ispostingtoflexcube,0) = 1 ", True)

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objLoanScheme = Nothing
        End Try
    End Sub

    Private Function SetFilter(ByRef objFlexcubeLoanOfferLetter As clsFlexcubeLoanOfferLetter) As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Function
            End If

            If CInt(cboLoanScheme.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Loan Scheme."), Me)
                cboLoanScheme.Focus()
                Exit Function
            End If

            If CInt(cboApplicationNo.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please Select Application No."), Me)
                cboApplicationNo.Focus()
                Exit Function
            End If

            Dim intLoanTypeId As Integer
            For Each key In mdicLoanType
                If cboLoanScheme.SelectedItem.Text.ToUpper.Contains(key.Value) Then
                    Select Case key.Key
                        Case 1
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.General_Loan
                            Exit For
                        Case 2
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Car_Loan
                            Exit For
                        Case 3
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Refinancing_Mortgage_Loan
                            Exit For
                        Case 4
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Purchase_Mortgage_Loan
                            Exit For
                        Case 5
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Construction_Mortgage_Loan
                            Exit For
                    End Select
                End If
            Next

            If intLoanTypeId <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Offer Letter is not available for Selected Loan Scheme"), Me)
                Exit Function
            End If


            objFlexcubeLoanOfferLetter.SetDefaultValue()

            objFlexcubeLoanOfferLetter._EmployeeUnkId = CInt(cboEmployee.SelectedValue)
            objFlexcubeLoanOfferLetter._EmployeeName = cboEmployee.SelectedItem.Text

            objFlexcubeLoanOfferLetter._ProcessPendingLoanUnkid = CInt(cboApplicationNo.SelectedValue)
            objFlexcubeLoanOfferLetter._ApplicationNo = cboApplicationNo.SelectedItem.Text

            objFlexcubeLoanOfferLetter._LoanSchemeUnkId = CInt(cboLoanScheme.SelectedValue)
            objFlexcubeLoanOfferLetter._LoanSchemeName = cboLoanScheme.SelectedItem.Text

            objFlexcubeLoanOfferLetter._LoanTypeId = intLoanTypeId


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try

            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboApplicationNo.SelectedValue = "0"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Dim objFlexcubeLoanOfferLetter As New clsFlexcubeLoanOfferLetter(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(objFlexcubeLoanOfferLetter) = False Then Exit Sub

            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            Call SetDateFormat()

            objFlexcubeLoanOfferLetter.generateReportNew(Session("Database_Name"), _
                                                         Session("UserId"), _
                                                         Session("Fin_year"), _
                                                         Session("CompanyUnkId"), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         Session("UserAccessModeSetting"), True, _
                                                         Session("ExportReportPath"), _
                                                         False, _
                                                         0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objFlexcubeLoanOfferLetter._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"
    Protected Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Dim objProcesspendingloan As New clsProcess_pending_loan
        Dim dtTable As New DataTable
        Try
            Dim StrSearching As String = String.Empty
            Dim dsList As DataSet = Nothing

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                dsList = objProcesspendingloan.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                CStr(Session("UserAccessModeSetting")), _
                                                                True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), _
                                                                "Loan", , StrSearching, , _
                                                                CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                dtTable = dsList.Tables(0)

                Dim drRow As DataRow = dtTable.NewRow
                drRow("application_no") = "Select"
                drRow("processpendingloanunkid") = -1
                dtTable.Rows.InsertAt(drRow, 0)

            Else
                Dim dtTemp As New DataTable("List")
                dtTemp.Columns.Add("processpendingloanunkid", GetType(Integer))
                dtTemp.Columns.Add("application_no", GetType(String))
                dtTemp.Rows.Add(-1, "Select")
                dtTable = dtTemp
            End If


            cboApplicationNo.DataValueField = "processpendingloanunkid"
            cboApplicationNo.DataTextField = "application_no"
            cboApplicationNo.DataSource = dtTable
            cboApplicationNo.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objProcesspendingloan = Nothing
        End Try
    End Sub
#End Region
End Class
