﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Payroll_Rpt_PayrollEmployeeListReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objPayrollEmployeeListReport As clsPayrollEmployeeListReport
    Private ReadOnly mstrModuleName As String = "frmPayrollEmployeeListReport"
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrCustomAllocIds As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        CustomAllocIds = 1
    End Enum
#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPayrollEmployeeListReport = New clsPayrollEmployeeListReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()
                Call Fill_Allocation()
            Else
                mintFirstOpenPeriod = CInt(ViewState("mintFirstOpenPeriod"))
                mstrCustomAllocIds = CStr(ViewState("mstrCustomAllocIds"))
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintFirstOpenPeriod", mintFirstOpenPeriod)
            Me.ViewState.Add("mstrCustomAllocIds", mstrCustomAllocIds)
            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Try
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, Session("Database_Name").ToString, CDate(Session("fin_startdate")), "Period", True)

            If mintFirstOpenPeriod > 0 Then
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("periodunkid = " & mintFirstOpenPeriod & " ")
                If drRow.Length <= 0 Then
                    Dim dRow As DataRow = dsCombo.Tables(0).NewRow

                    objPeriod._Periodunkid(Session("Database_Name").ToString) = mintFirstOpenPeriod

                    dRow.Item("periodunkid") = mintFirstOpenPeriod
                    dRow.Item("name") = objPeriod._Period_Name
                    dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                    dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                    dRow.Item("yearunkid") = objPeriod._Yearunkid

                    dsCombo.Tables(0).Rows.Add(dRow)
                End If
            End If
            With drpPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintFirstOpenPeriod.ToString
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_Allocation()
        Try

            Dim dsList As New DataSet
            Dim dtTable As New DataTable

            Dim marrAlloc As New ArrayList
            
            If mstrCustomAllocIds.Trim <> "" Then
                marrAlloc.AddRange(CStr(mstrCustomAllocIds).Split(CChar(",")))
            End If

            If marrAlloc.Count > 0 Then
                dsList = objPayrollEmployeeListReport.GetAllocationList("Id NOT IN(" & String.Join(",", CType(marrAlloc.ToArray(Type.GetType("System.String")), String())) & ")")
                dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    dsRow.Item("IsChecked") = False
                Next
                dsList.Tables(0).AcceptChanges()

                dtTable.Merge(dsList.Tables(0), True)

                dsList = objPayrollEmployeeListReport.GetAllocationList("Id IN (" & String.Join(",", CType(marrAlloc.ToArray(Type.GetType("System.String")), String())) & ")")
                dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    dsRow.Item("IsChecked") = False
                Next

                Dim intPos As Integer = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dr As DataRow = dtTable.NewRow
                    dr.ItemArray = dRow.ItemArray
                    dtTable.Rows.InsertAt(dr, intPos)
                    intPos += 1
                Next
            Else
                dsList = objPayrollEmployeeListReport.GetAllocationList()
                dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    dsRow.Item("IsChecked") = False
                Next
                dtTable.Merge(dsList.Tables(0), True)
            End If

            For Each xrow As DataRow In dtTable.Rows
                If marrAlloc.Contains(xrow.Item("Id").ToString) = True Then
                    xrow("IsChecked") = True
                    xrow.AcceptChanges()
                End If
            Next

            With lvAllocation_hierarchy
                .DataSource = dtTable
                .DataBind()
            End With
            Me.ViewState.Add("dtAllocation", dtTable)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            drpPeriod.SelectedValue = CStr(mintFirstOpenPeriod)
            mstrAdvanceFilter = ""
            Call GetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If drpPeriod.SelectedValue = "" OrElse CInt(drpPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Period"), Me)
                drpPeriod.Focus()
                Exit Function
            End If

            objPayrollEmployeeListReport.SetDefaultValue()

            objPayrollEmployeeListReport._PeriodId = CInt(drpPeriod.SelectedValue)
            objPayrollEmployeeListReport._PeriodName = drpPeriod.SelectedItem.Text

            Dim result As Dictionary(Of Integer, String) = (From p In CType(Me.ViewState("dtAllocation"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select p).ToDictionary(Function(row) CInt(row("Id")), Function(row) row("Name").ToString())
            objPayrollEmployeeListReport._AllocationDetails = result

            objPayrollEmployeeListReport.setDefaultOrderBy(0)

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Payroll_Employee_List_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.CustomAllocIds
                            mstrCustomAllocIds = CStr(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objPayrollEmployeeListReport.setDefaultOrderBy(0)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)

            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objPayrollEmployeeListReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                objPeriod._Start_Date, _
                                                                objPeriod._End_Date, _
                                                                Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                OpenAfterExport, , , , , , , mstrAdvanceFilter)


            If objPayrollEmployeeListReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objPayrollEmployeeListReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gbCustomSetting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbCustomSetting.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 1
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Payroll_Employee_List_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.CustomAllocIds
                        Dim xStr As String = ""
                        objUserDefRMode._Headtypeid = intHeadType
                        Dim selectedTags As List(Of String) = (From p In CType(Me.ViewState("dtAllocation"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("Id").ToString)).ToList
                        xStr = String.Join(",", selectedTags.ToArray())
                        objUserDefRMode._EarningTranHeadIds = xStr

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Payroll_Employee_List_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Selection Saved Successfully"), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
#End Region

#Region " Other Controls Events "
    Protected Sub chkAllocationSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkAllocationSelectAll As CheckBox = TryCast(sender, CheckBox)
            Dim dtAllocation As DataTable = CType(Me.ViewState("dtAllocation"), DataTable)
            If chkAllocationSelectAll Is Nothing Then Exit Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In lvAllocation_hierarchy.Rows
                cb = CType(lvAllocation_hierarchy.Rows(gvr.RowIndex).FindControl("chkAllcationSelect"), CheckBox)
                cb.Checked = chkAllocationSelectAll.Checked
            Next
            For Each gvr As DataRow In dtAllocation.Rows
                gvr("IsChecked") = chkAllocationSelectAll.Checked
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAllocationSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = TryCast(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = CType(ViewState("dtAllocation"), DataTable).Select("Id = '" & lvAllocation_hierarchy.DataKeys(gvRow.RowIndex).Value.ToString & "'")
                If drRow.Length > 0 Then
                    drRow(0)("IsChecked") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChangeAllocationLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim dtAllocation As DataTable = CType(Me.ViewState("dtAllocation"), DataTable)
            Dim xRow As DataRow = dtAllocation.Rows(rowIndex)
            Dim xNewRow As DataRow = dtAllocation.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvAllocation_hierarchy.Rows.Count - 1 Then Exit Sub
                dtAllocation.Rows.RemoveAt(rowIndex)
                dtAllocation.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtAllocation.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtAllocation.Rows.Remove(xRow)
                dtAllocation.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtAllocation.Rows.IndexOf(xNewRow)
            End If
            With lvAllocation_hierarchy
                .DataSource = dtAllocation
                .DataBind()
            End With
            dtAllocation.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    
#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPeriod.ID, Me.lblPeriod.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gbCustomSetting.ID, Me.gbCustomSetting.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lvAllocation_hierarchy.Columns(1).FooterText(), lvAllocation_hierarchy.Columns(1).HeaderText())


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)

            Me.gbCustomSetting.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbCustomSetting.ID, Me.gbCustomSetting.ToolTip)
            lvAllocation_hierarchy.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lvAllocation_hierarchy.Columns(1).FooterText, lvAllocation_hierarchy.Columns(1).HeaderText)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Trim.Replace("&", ""))
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Traininig Calendar.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Selection Saved Successfully")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
