﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Loan_Advance_Saving.aspx.vb"
    Inherits="Reports_Rpt_Loan_Advance_Saving" Title="Loan Advance Saving Report" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        
                    </h2>
                </div>
                <div class="row clearfix  d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <div style="float: left;">
                                        <%--<asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>--%>
                                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Advance Saving" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server"  ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </div>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboCurrency" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboBranch" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBankName" runat="server" Text="Emp. Bank Name" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboBankName" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblBankBranch" runat="server" Text="Bank Branch" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboBankBranch" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlInterestInfo" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowInterestInfo" runat="server" Text="Show Interest Information">
                                            </asp:CheckBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlLoanReceipt" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowLoanReceipt" runat="server" Text="Show Receipt/Written Off">
                                            </asp:CheckBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlSaving" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkIgnoreZeroSavingDeduction" runat="server" Text="Ignore Zero Saving Deduction">
                                            </asp:CheckBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlShowOnHold" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowOnHold" runat="server" Text="Show On Hold"></asp:CheckBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlShowPrincipleBFCF" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowPrincipleBFCF" runat="server" Text="Show Principle BF/CF">
                                            </asp:CheckBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlScheme" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblSearchScheme" runat="server" Text="Search Scheme" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtSearchScheme" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 400px;">
                                                <asp:GridView ID="gvScheme" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                    Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                    OnCheckedChanged="chkSelect_OnCheckedChanged" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true" Visible="false" />
                                                        <asp:BoundField DataField="code" HeaderText="Scheme Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                            FooterText="colhCode" HeaderStyle-Width="30%" ItemStyle-Width="30%" />
                                                        <asp:BoundField DataField="name" HeaderText="Scheme Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                            FooterText="colhName" HeaderStyle-Width="60%" ItemStyle-Width="60%" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
