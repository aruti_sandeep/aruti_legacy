﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Payroll_Rpt_Credit_Card_Report
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmCredit_Card_Report"
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = String.Empty
    Private mstrAnalysis_Join As String = String.Empty
    Private mstrAnalysis_OrderBy As String = String.Empty
    Private mstrAnalysis_OrderBy_GName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme

            Dim dsList As New DataSet

            dsList = objLoanScheme.getComboList(True, "List", -1, " ISNULL(loanschemecategory_id,0) = " & enLoanSchemeCategories.CREDIT_CARD, False)
            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objLoanScheme = Nothing



            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = -1
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing

           
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboLoanScheme.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboLoanApplicationNo.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Public Function SetFilter(ByRef objCreditCard As clsCredit_Card_Report) As Boolean
        Try
            objCreditCard.SetDefaultValue()

            objCreditCard._EmployeeID = CInt(cboEmployee.SelectedValue)
            objCreditCard._EmployeeName = cboEmployee.SelectedItem.Text
            objCreditCard._LoanSchemeId = CInt(cboLoanScheme.SelectedValue)
            objCreditCard._LoanScheme = cboLoanScheme.SelectedItem.Text
            objCreditCard._LoanApplicationId = CInt(cboLoanApplicationNo.SelectedValue)
            objCreditCard._LoanApplicationNo = cboLoanApplicationNo.SelectedItem.Text
            objCreditCard._ViewByIds = mstrViewByIds
            objCreditCard._ViewIndex = mintViewIndex
            objCreditCard._ViewByName = mstrViewByName
            objCreditCard._Analysis_Fields = mstrAnalysis_Fields
            objCreditCard._Analysis_Join = mstrAnalysis_Join
            objCreditCard._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCreditCard._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objCreditCard._Report_GroupName = mstrReport_GroupName
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objCreditCard._IncludeAccessFilterQry = False

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            GC.Collect()

            If Not IsPostBack Then
                SetLanguage()
                Call FillCombo()
                cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
            Else
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysis_Fields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysis_Join = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysis_OrderBy_GName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReport_GroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysis_Fields
            Me.ViewState("AnalysisJoin") = mstrAnalysis_Join
            Me.ViewState("AnalysisOrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("ReportGroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objCreditCard As New clsCredit_Card_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Scheme is compulsory information.Please Select Loan Scheme."), Me)
                cboLoanScheme.Focus()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim objLoan As New clsProcess_pending_loan
            Dim dsList As DataSet = objLoan.GetEmployeeLoanApplicationNoList(CInt(cboEmployee.SelectedValue), CInt(cboLoanScheme.SelectedValue), True, "List", -1, "loan_statusunkid IN (" & enLoanApplicationStatus.PENDING & "," & enLoanApplicationStatus.APPROVED & ")")
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow = dsList.Tables(0).Select("processpendingloanunkid > 0")
                If dRow Is Nothing OrElse dRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "There is no Loan Application(s) to export Report."), Me)
                    cboLoanApplicationNo.Focus()
                    objLoan = Nothing
                    Exit Sub
                End If
            End If
            objLoan = Nothing


            If SetFilter(objCreditCard) = False Then Exit Sub

            objCreditCard._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objCreditCard._OpenAfterExport = False
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            objCreditCard._CompanyUnkId = CInt(Session("CompanyUnkId"))

            objCreditCard._UserUnkId = CInt(Session("UserId"))
            objCreditCard._UserAccessFilter = CStr(Session("AccessLevelFilterString"))

            SetDateFormat()

            If CInt(Session("Employeeunkid")) > 0 Then
                objCreditCard._UserName = Session("DisplayName").ToString()
            End If

            objCreditCard.Generate_DetailReport(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString, _
                                                  Session("UserAccessModeSetting").ToString, True)

            If objCreditCard._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objCreditCard._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCreditCard = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboLoanScheme.SelectedIndexChanged
        Try
            Dim objLoan As New clsProcess_pending_loan
            Dim dsList As DataSet = objLoan.GetEmployeeLoanApplicationNoList(CInt(cboEmployee.SelectedValue), CInt(cboLoanScheme.SelectedValue), True, "List", -1, "loan_statusunkid IN (" & enLoanApplicationStatus.PENDING & "," & enLoanApplicationStatus.APPROVED & ")")
            With cboLoanApplicationNo
                .DataValueField = "processpendingloanunkid"
                .DataTextField = "application_no"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objLoan = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.LblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

  
End Class
