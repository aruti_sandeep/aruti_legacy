﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NumberOnly.ascx.vb" Inherits="Controls_NumberOnly" %>
<div class="form-group">
    <div class="form-line">
        <asp:TextBox ID="txtNumeric" runat="server" CssClass="form-control" style="text-align:right;" Text="0" onKeypress="return onlyNumeric(this, event);" ></asp:TextBox>
    </div>
</div>