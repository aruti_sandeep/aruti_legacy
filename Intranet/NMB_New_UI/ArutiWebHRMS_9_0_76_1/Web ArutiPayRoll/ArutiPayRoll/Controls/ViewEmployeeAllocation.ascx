﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewEmployeeAllocation.ascx.vb"
    Inherits="Controls_ViewEmployeeAllocation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popup_ViewEmployeeAllocation" runat="server"
    BackgroundCssClass="modal-backdrop" CancelControlID="btnClose" PopupControlID="pnl_ViewEmployeeAllocation"
    TargetControlID="hdf_ViewEmployeeAllocation">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnl_ViewEmployeeAllocation" runat="server" CssClass="card modal-dialog modal-lg"
    Style="display: none;">
    <div class="header">
        <h2>
            <asp:Label ID="lblTitle" runat="server" Text="Employee Allocation Detail"></asp:Label>
        </h2>
    </div>
    <div class="body">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <asp:Label ID="objlblEmployee" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblBranch" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblJobGroup" runat="server" Text="Job Group"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblJobGroup" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblDepartmentGroup" runat="server" Text="Dept. Group"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblDepartmentGroup" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblDepartment" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblSectionGroup" runat="server" Text="Section Group"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblSectionGroup" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblSection" runat="server" Text="Section"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblSection" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblClassGroup" runat="server" Text="Class Group"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblClassGroup" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblClass" runat="server" Text="Class"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblClass" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblUnitGroup" runat="server" Text="Unit Group"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblUnitGroup" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblUnits" runat="server" Text="Unit"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblUnits" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblGradeGroup" runat="server" Text="Grade Group"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblGradeGroup" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblGrade" runat="server" Text="Grade"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblGrade" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblGradeLevel" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblTeam" runat="server" Text="Team"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblTeam" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblJob" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblCostCenter" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row clearfix" style="display: none">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="lblScale" runat="server" Text="Scale"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <asp:Label ID="objlblScale" runat="server" Text="0.00"></asp:Label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            </div>
        </div>
    </div>
    <div class="footer">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" />
        <asp:HiddenField ID="hdf_ViewEmployeeAllocation" runat="server" />
    </div>
</asp:Panel>
