﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FileUpload.ascx.vb" Inherits="Controls_FileUpload"
    EnableTheming="True" %>

<script type="text/javascript">

    function fileUpLoadChange(imgFileClientID) {
    //    $('#image_file').change(function() {
    //        var filename = $('#image_file').val();
    //        $('#select_file').html(filename.replace(/^.*[\\\/]/, ''));
    //     });

    //var imgFile = '#<%= image_file.ClientID %>';
//    var imgFile = document.getElementById(imgFileClientID);
    var imgFile = $(imgFileClientID);
    
//    if ($.browser.msie) {
//        //alert('This is a Microsoft Internet Explorer ' + $.browser.version.substr(0, 1));
//        $(imgFile).css({ 'width': '102px' });
//        $(imgFile).css({ 'right': '60px' });
//    }
//    else if ($.browser.mozilla) {
//        //alert('This is a Mozilla Firefox ' + $.browser.version.substr(0, 1));    
//        $(imgFile).css({ 'width': '102px' });
//        $(imgFile).css({ 'right': '50px' });
//        $(imgFile).css({ 'font-size': '14px' });
//    }
//    else if ($.browser.webkit || $.browser.safari) {
//        //alert('This is a Webkit Engine Browser (Apple Safari or Google Chrome) with version ' + $.browser.version.substr(0, 1));
//        $(imgFile).css({ 'width': '102px' });
//        $(imgFile).css({ 'right': '52px' });
//    }
//    else if ($.browser.opera) {
//        //alert('This is an Opera ' + $.browser.version.substr(0, 1));
//        $(imgFile).css({ 'width': 'auto' });
//        $(imgFile).css({ 'right': '0px' });
//    }

//    if ($(imgFile).is(':disabled') == true) {
//        $('#ctl0_config_btnUpload').css({ 'color': '#A6A6A6' });
//        $('#ctl0_config_btnUpload').css({ 'background-image': '#FFFFFF' });
//    }
//    else {
//        $('#ctl0_config_btnUpload').css({ 'color': '#222' });
//        $('#ctl0_config_btnUpload').css({ 'background-image': '#BEB7B9' });
//    }
    $(imgFile).on('change', function () {
        var blnInvalidFile = false;
        //var ext = this.value.match(/\.(.+)$/)[1];
            var ext = this.value.split('.').pop();
            var msgInvalid = 'Sorry, Allowable file type(s) are PDF';
            var blnImage = <%= AllowImageFile.ToString.ToLower %>;
            var blnDoc = <%= AllowDocumentFile.ToString.ToLower %>;                       

            if (blnImage == true) {
                msgInvalid = msgInvalid + ', Image';
            }
            if (blnDoc == true) {
                msgInvalid = msgInvalid + ', Document';
            }
            msgInvalid = msgInvalid + ' Only.';

        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'jpeg':
            case 'bmp':
            case 'png':
            case 'gif':
                    if (blnImage == true) {                        
                        break;
                    }
                    else {
                        this.value = '';
                        blnInvalidFile = true;           
                        break;
                    }
                case 'doc':
                case 'docx':
                    if (blnDoc == true) {
                        msgInvalid = msgInvalid + ', Document';
                    break;
                    }
                    else {
                        this.value = '';
                        blnInvalidFile = true;
                        break;
                    }
                case 'pdf':
                break;
            default:
                //alert('Please select proper Image file or PDF file.');
                this.value = '';
                blnInvalidFile = true;
        }

            if (blnInvalidFile == true) {
                    
                    showSuccessMessage(msgInvalid);

            }
                        
            if (window.FileReader && window.File && window.FileList && window.Blob && blnInvalidFile==false) {
                var maxsize = '<%= RemainingSizeKB %>';
                //if (maxsize > 0 && (this.files[0].size / 1024 / 1024) > maxsize) {
                if (maxsize > 0 && (this.files[0].size) > maxsize) {
                    //alert('Sorry,You cannot upload file greater than ' + maxsize + ' MB.');                    
                    showSuccessMessage('Sorry,You cannot upload file greater than ' + (maxsize / 1024 / 1024).toFixed(2) + ' MB.');

                    this.value = '';
                    blnInvalidFile = true;
                }
            }
        //var filename = $(imgFile).val();
        var filename = '';
        if (blnInvalidFile == false) {
            filename = $(imgFile).val();
        }
        //$('#select_file').html(filename.replace(/^.*[\\\/]/, ''));

        if (filename != '') {
                $('#' + $(imgFile).parents('div[id*=fileUpload]:first').find("input[id*='btnUpload']")[0].id).click();
            //$('#<%= btnUpload.ClientID %>').click();
            //$('#' + $(imgFile).parent().parent().parent()[0].children[1].id).click();
            //$(imgFile).click();
            //javascript: __doPostBack($(imgFile)[0].name, 'bttnUpload_Click');
        }

    });
}

</script>

<div id="fileUpload">
    <asp:UpdatePanel ID="UPUpload1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="fluploadcontainer">
            <label class="myLabel btn btn-primary btn-circle waves-effect waves-circle waves-float">
           
           <%--'Hemant (11 Apr 2022) -- Start --%>           
           <%--'ISSUE/ENHANCEMENT(ZRA) : Move CV, cover letter and qualification on search job page to allow to attach documents for each vacancy in self service--%>
           <%--<asp:FileUpload ID="image_file" runat="server" accept="image/*" CssClass="flupload waves-effect" />--%>
             <asp:FileUpload ID="image_file" runat="server" accept="MIME_type" CssClass="flupload waves-effect" />
           <%--'Hemant (11 Apr 2022) -- End--%>
           
                <i class="fas fa-camera "></i>
                 </label>
                <%--<a id="ctl0_config_btnUpload" class="btn btn-primary btn-circle waves-effect waves-circle waves-float"
                    title="Choose File" onclick="return false" href="javascript:;//ctl0_config_btnUpload">
                    <i class="fas fa-camera"></i></a>--%>
                <%--<label id = "select_file"  style="font-weight: bold;float:left; ">Please select file</label>--%>
                <%--<input type="button" value="Browse..." style="float:none;" onclick="document.getElementById('<%= image_file.ClientID %>').click();" />--%>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnUpload" runat="server" Text="Upload" Style="float: left; display: none;" />
</div>
