﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ExportReport.ascx.vb"
    Inherits="Controls_ExportReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popupExportReport" runat="server" BackgroundCssClass="modal-backdrop bd-l5"
    CancelControlID="btnCloseReportExport" PopupControlID="Panel1" TargetControlID="hdnfieldDelReason">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog modal-l5" Style="display: none;">
    <div class="header">
        <h2>
            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
        </h2>
    </div>
    <div class="body" style="max-height: 400px">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="m-t-0">Report Exported Successfully.</h3>
                        <h5>Click 'Save' button to Save Report</h5>
                    </div>
                </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                <asp:Button ID="btnCloseReportExport" runat="server" Text="Close" CssClass="btn btn-default" />
                <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
            </div>
</asp:Panel>

