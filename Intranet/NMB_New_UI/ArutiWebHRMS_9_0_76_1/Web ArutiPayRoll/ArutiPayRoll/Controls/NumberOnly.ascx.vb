﻿
Partial Class Controls_NumberOnly
    Inherits System.Web.UI.UserControl

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Default Properties "

    Public Property Height() As Unit
        Get
            Return txtNumeric.Height
        End Get
        Set(ByVal value As Unit)
            txtNumeric.Height = value
        End Set
    End Property

    Public Property Width() As Unit
        Get
            Return txtNumeric.Width
        End Get
        Set(ByVal value As Unit)
            txtNumeric.Width = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return txtNumeric.Text
        End Get
        Set(ByVal value As String)
            txtNumeric.Text = value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return txtNumeric.Enabled
        End Get
        Set(ByVal value As Boolean)
            txtNumeric.Enabled = value
        End Set
    End Property

    Public Property Read_Only() As Boolean
        Get
            Return txtNumeric.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtNumeric.ReadOnly = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return txtNumeric.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            txtNumeric.AutoPostBack = value
        End Set
    End Property

    Public WriteOnly Property Style() As String
        Set(ByVal value As String)
            txtNumeric.Attributes.Add("style", value)
        End Set
    End Property

    Public Property Decimal_() As Decimal
        Get
            Dim decAmt As Decimal = 0
            Decimal.TryParse(txtNumeric.Text, decAmt)
            Return decAmt
        End Get
        Set(ByVal value As Decimal)
            Dim decAmt As Decimal = 0
            Decimal.TryParse(value, decAmt)
            txtNumeric.Text = decAmt.ToString
        End Set
    End Property

#End Region

#Region " Default Events "

    Protected Sub txtNumeric_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumeric.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

#End Region

End Class
