﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewDateCtrl.ascx.vb"
    Inherits="Controls_NewDateCtrl" %>
<div class="form-group date-picker">
    <div class="form-line" style="position: relative" id="bs_datepicker_container">
        <asp:TextBox runat="server" ID="TxtDate" AutoPostBack="True" CssClass="form-control"
            placeholder="Please choose a date..." />
    </div>
    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
</div>
