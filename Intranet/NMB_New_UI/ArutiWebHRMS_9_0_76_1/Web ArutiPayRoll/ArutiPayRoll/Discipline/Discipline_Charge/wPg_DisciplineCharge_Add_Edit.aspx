﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_DisciplineCharge_Add_Edit.aspx.vb" Inherits="Discipline_Discipline_Charge_wPg_DisciplineCharge_Add_Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="RConfirmation" TagPrefix="ucyesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
            var dtpResponseDt = $('#<%= dtpReponseDate.ClientID %>');
            var cboResponsetype = $('#<%= cboReponseType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            //            if (parseInt(cboResponsetype.val()) <= 0) {
            //                alert('Response type is compulsory information. Please select Response type to continue.');
            //                cboResponsetype.focus();
            //                return false;
            //            }
            if (dtpResponseDt.val().trim() == "") {
                alert('Sorry, response date is mandatory information. Please give proper response date.');
                dtpResponseDt.focus();
                return false;
            }
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnSClose" PopupControlID="Panel2" TargetControlID="hdnfieldDelReason">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel2" runat="server" CssClass="card modal-dialog" Style="display: none;
                    z-index: 100002!important;" DefaultButton="btnSOk">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblHearingSchduling" runat="server" Text="Hearing Scheduling Details"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height: 500px;">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblHearingDate" runat="server" Text="Hearing Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtHearingDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblHearingTime" runat="server" Text="Hearing Time" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtHearingTime" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVenue" runat="server" Text="Venue" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtVenue" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblHeadingRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtHearingRemark" runat="server" ReadOnly="true" Rows="3" TextMode="MultiLine"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSOk" runat="server" Text="Ok" CssClass="btn btn-primary" Visible="false" />
                                <asp:Button ID="btnSClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="popup_DisciplineCharge" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_DiscplineCharge" PopupControlID="pnl_DisciplineCharge" TargetControlID="hdf_DiscplineCharge">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnl_DisciplineCharge" runat="server" CssClass="card modal-dialog"
                    Style="display: none; z-index: 100002!important;" DefaultButton="btnSOk">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDiscplinechargeHeader" runat="server" Text="Add/Edit Charge Count Response"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height: 500px;">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOffenceCategory" runat="server" Text="Offence Category" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOffenceCategory" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOffenceDescription" runat="server" Text="Offence Description" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOffenceDescription" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIncident" runat="server" Text="Incident" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIncident" runat="server" Rows="3" TextMode="MultiLine" ReadOnly="true"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponseDate" runat="server" Text="Response Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpReponseDate" runat="server" Enabled="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponseType" runat="server" Text="Response Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReponseType" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponse" runat="server" Text="Response" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtResponseDescription" runat="server" Rows="3" TextMode="MultiLine"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDocType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDocumentType" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                            <div id="fileuploader">
                                                <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                    value="Save Attachment" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Button ID="btnScanAttachDoc" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                            Text="Browse" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:Panel ID="pnl_dgvResponse" runat="server" ScrollBars="Auto">
                                                <asp:DataGrid ID="dgvResponse" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                    Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                    HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                                    <i class="fas fa-trash text-danger"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--0--%>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                        <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download">
                                                                                <i class="fas fa-download"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--1--%>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                        <%--2--%>
                                                        <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                            Visible="false" />
                                                        <%--3--%>
                                                        <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                            Visible="false" />
                                                        <%--4--%>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                                </div>
                                <asp:Button ID="btnFilesave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnFileClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="hdf_DiscplineCharge" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                <ucyesno:RConfirmation ID="popup_response" runat="server" Message="" Title="Aruti" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Discipline Charge" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Discipline Details" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPersonalInvolved" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblJobTitle" runat="server" Text="Job Title" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtJobTitle" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblRefNo" runat="server" Text="Reference No." CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboReferenceNo" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                                    <asp:Label ID="lblDate" runat="server" Text="Charge Date" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtChargeDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-r-0">
                                                    <asp:Label ID="lblInterdictionDate" runat="server" Text="Interdiction Date" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInterdictionDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblGeneralChargeDescr" runat="server" Text="Charge Description" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtChargeDescription" runat="server" TextMode="MultiLine" Rows="3"
                                                            CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblChargeCount" runat="server" Text="Charge Count(s)" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 350px;">
                                                            <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server">
                                                                <asp:DataGrid ID="dgvChargeCountList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" HeaderStyle-Font-Bold="false"
                                                                    Width="99%">
                                                                    <ItemStyle />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="charge_count" FooterText="dgcolhCount" HeaderText="Count"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--0--%>
                                                                        <asp:BoundColumn DataField="incident_description" FooterText="dgcolhIncident" HeaderText="Incident"
                                                                            ReadOnly="true" />
                                                                        <%--1--%>
                                                                        <asp:BoundColumn DataField="charge_category" FooterText="dgcolhOffCategory" HeaderText="Offence Category"
                                                                            ReadOnly="true" />
                                                                        <%--2--%>
                                                                        <asp:BoundColumn DataField="charge_descr" FooterText="dgcolhOffence" HeaderText="Offence Discription"
                                                                            ReadOnly="true" />
                                                                        <%--3--%>
                                                                        <asp:BoundColumn DataField="charge_severity" FooterText="dgcolhSeverity" HeaderText="Severity"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--4--%>
                                                                        <asp:BoundColumn DataField="disciplinefileunkid" FooterText="objcolhfileunkid" ReadOnly="true"
                                                                            Visible="false" />
                                                                        <%--5--%>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                            HeaderText="Add Response" FooterText="dglnkCol">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                                    Font-Underline="false"><i class="fas fa-comments"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--6--%>
                                                                        <asp:BoundColumn DataField="disciplinefiletranunkid" Visible="false" />
                                                                        <%--7--%>
                                                                        <asp:BoundColumn DataField="response_date" FooterText="dgcolhResponseDate" HeaderText="Response Date"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true" Visible="false">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--8--%>
                                                                        <asp:BoundColumn DataField="response_type" FooterText="dgcolhResponseType" HeaderText="Response Type"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true" Visible="false">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--9--%>
                                                                        <asp:BoundColumn DataField="response_remark" FooterText="dgcolhResponseText" HeaderText="Response Remark"
                                                                            ItemStyle-HorizontalAlign="Center" ReadOnly="true" Visible="false">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <%--10--%>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                            HeaderText="Remove Response" FooterText="dglnkResponsevoid">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkResponsevoid" runat="server" OnClick="delete_Click" CommandName="remove_response"
                                                                                    Font-Underline="false"><i class="fas fa-trash text-danger"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--11--%>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                            HeaderText="Download Attachment" FooterText="dglnkDownloadAttachment">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkDownloadAttachment" runat="server" OnClick="linkDownload_Click"
                                                                                    CommandName="download_attachment" Font-Underline="false"><i class="fas fa-download"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--12--%>
                                                                    </Columns>
                                                                    <HeaderStyle Font-Bold="False" />
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="text-align: left; position: absolute">
                                    <asp:Button ID="btnViewScheduling" runat="server" Text="View Scheduling" CssClass="btn btn-default" />
                                </div>
                                <asp:Button ID="btnFinalSave" runat="server" Text="Final Save" CssClass="btn btn-default" />
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgvResponse" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
                <asp:PostBackTrigger ControlID="dgvChargeCountList" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");

        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_DisciplineCharge_Add_Edit.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnScanAttachDoc.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        //$('input[type=file]').live("click", function() {
        $("body").on("click", 'input[type=file]', function(){
            return IsValidAttach();
        });
    </script>

</asp:Content>
