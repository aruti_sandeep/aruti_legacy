﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization
#End Region

Partial Class Staff_Transfer_Transfer_Approval_wPgTransferApprovalList
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmStaffTransferApprovalList"
    Dim DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                SetMessages()
                Call SetLanguage()
                Call FillCombo()
                SetVisibility()
                If dgStaffTransferApproval.Items.Count <= 0 Then
                    dgStaffTransferApproval.DataSource = New List(Of String)
                    dgStaffTransferApproval.DataBind()
                End If
            Else
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                       Session("UserAccessModeSetting").ToString(), True, _
                                       False, "List", True)
            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))


            Dim objStaffTransfer As New clstransfer_request_tran
            dsList = objStaffTransfer.GetTransferRequest_Status("Status", True, False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "1"
            End With

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(CInt(Session("UserId")))
                txtApprover.Text = objUser._Username.ToString()
                objUser = Nothing
            Else
                lblApprover.Visible = False
                txtApprover.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim objApproval As New clssttransfer_approval_Tran
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            If CBool(Session("AllowToViewStaffTransferApprovalList")) = False Then Exit Sub

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND st.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND sta.statusunkid = " & CInt(cboStatus.SelectedValue) & " AND sta.visibleunkid = " & CInt(cboStatus.SelectedValue)
            Else
                strSearch &= "AND sta.visibleunkid <> -1 " & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND st.requestdate >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND st.requestdate >= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If


            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objApproval.GetTrasferApprovalList(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                                                             "List", strSearch, True, Nothing)


            Dim mintTransferrequestunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

                For Each drRow As DataRow In dsList.Tables(0).Rows

                    If CInt(drRow("transferapprovalunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintTransferrequestunkid <> CInt(drRow("transferrequestunkid")) Then
                        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND transferrequestunkid = " & CInt(drRow("transferrequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintTransferrequestunkid = CInt(drRow("transferrequestunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-") & " " & info1.ToTitleCase(drRow("loginuser").ToString())
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 1 Then

                                    If CInt(dr(i)("statusunkid")) = 2 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-") & " " & info1.ToTitleCase(dr(i)("loginuser").ToString())
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Pushed Back By :-") & " " & info1.ToTitleCase(dr(i)("loginuser").ToString())
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Pushed Back By :-") & " " & info1.ToTitleCase(drRow("loginuser").ToString())
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next
                info1 = Nothing
            End If

            Dim dtTable As  DataTable = New DataView(dsList.Tables(0), "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable

            Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "transferrequestunkid", "employeeunkid")

            Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("transferrequestunkid") Equals drTemp.Field(Of Integer)("transferrequestunkid") And _
                         drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable


            If dRow1.Count > 0 Then
                dRow1.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            End If


            dgStaffTransferApproval.AutoGenerateColumns = False
            dgStaffTransferApproval.DataSource = dtTable
            dgStaffTransferApproval.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproval = Nothing
        End Try
    End Sub

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("transferrequestunkid = " & CInt(dr("transferrequestunkid").ToString()) & " AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

    Private Sub SetVisibility()
        Dim mstrStaffTransferRequestAllocations As String = ""
        Try

            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhChangeStatus", False, True)).Visible = CBool(Session("AllowToApproveStaffTransferApproval"))


            If Session("StaffTransferRequestAllocations") Is Nothing OrElse Session("StaffTransferRequestAllocations").ToString().Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Staff Transfer Allocation(s) are compulsory information.Please configure it from configuration settings screen."), Me)
                Exit Sub
            End If

            mstrStaffTransferRequestAllocations = Session("StaffTransferRequestAllocations").ToString()
            Dim ar() As String = mstrStaffTransferRequestAllocations.Split(CChar("|"))

            If ar IsNot Nothing AndAlso ar.Length > 0 Then

                For i As Integer = 0 To ar.Length - 1

                    Select Case CInt(ar(i))

                        Case enAllocation.BRANCH
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhBranch", False, True)).Visible = True

                        Case enAllocation.DEPARTMENT_GROUP
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhDeptGrp", False, True)).Visible = True

                        Case enAllocation.DEPARTMENT
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhDepartment", False, True)).Visible = True

                        Case enAllocation.SECTION_GROUP
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhSectionGroup", False, True)).Visible = True

                        Case enAllocation.SECTION
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhSection", False, True)).Visible = True

                        Case enAllocation.UNIT_GROUP
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUnitGroup", False, True)).Visible = True

                        Case enAllocation.UNIT
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUnit", False, True)).Visible = True

                        Case enAllocation.TEAM
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhTeam", False, True)).Visible = True

                        Case enAllocation.JOB_GROUP
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhJobGroup", False, True)).Visible = True

                        Case enAllocation.JOBS
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhJob", False, True)).Visible = True

                        Case enAllocation.CLASS_GROUP
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhClassGroup", False, True)).Visible = True

                        Case enAllocation.CLASSES
                            dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhClass", False, True)).Visible = True

                    End Select

                Next

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgStaffTransferApproval.CurrentPageIndex = 0
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dgStaffTransferApproval.DataSource = New List(Of String)
            dgStaffTransferApproval.DataBind()
            cboEmployee.SelectedValue = "0"
            cboStatus.SelectedValue = "1"
            mstrAdvanceFilter = ""
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
    '    Try
    '        mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
    '        Call FillList()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgStaffTransferApproval_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgStaffTransferApproval.ItemCommand
        Dim objTransferApproval As New clssttransfer_approval_Tran
        Try
            If e.CommandName.ToUpper = "STATUS" Then

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgStaffTransferApproval, "objcolhUserID", False, True)).Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You can't Edit this trasfer request detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If


                objTransferApproval._Transferapprovalunkid = CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhtransferapprovalunkid", False, True)).Text)

                Dim dsList As DataSet
                Dim dtList As DataTable

                Dim mstrSearch As String = "sta.transferrequestunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhtransferrequestunkid", False, True)).Text) & " AND sta.transferapprovalunkid <> " & CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhtransferapprovalunkid", False, True)).Text)

                dsList = objTransferApproval.GetTrasferApprovalList(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, False, Nothing)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                        If objTransferApproval._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                            dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Approved, "", DataViewRowState.CurrentRows).ToTable
                            If dtList.Rows.Count > 0 Then Continue For

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this trasfer request detail. Reason: This trasfer request is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this trasfer request detail. Reason: This trasfer request is already pushed back."), Me)
                                Exit Sub
                            End If

                        ElseIf objTransferApproval._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this trasfer request detail. Reason: This trasfer request is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this trasfer request detail. Reason: This trasfer request is already pushed back."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                If CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhStatusunkid", False, True)).Text) = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this trasfer request detail. Reason: This trasfer request is already approved."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhStatusunkid", False, True)).Text) = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this trasfer request detail. Reason: This trasfer request is already approved."), Me)
                    Exit Sub
                ElseIf CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhStatusunkid", False, True)).Text) = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this trasfer request detail. Reason: This trasfer request is already pushed back."), Me)
                    Exit Sub
                End If


                Session("transferrequestunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhtransferrequestunkid", False, True)).Text)
                Session("transferapprovalunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhtransferapprovalunkid", False, True)).Text)
                Session("StaffTransferEmployeeId") = CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhEmployeeID", False, True)).Text)
                Session("Mapuserunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhUserID", False, True)).Text)

                Response.Redirect(Session("rootpath").ToString & "Staff_Transfer/Transfer_Approval/wPgTransferApproval.aspx", False)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTransferApproval = Nothing
        End Try
    End Sub

    Protected Sub dgStaffTransferApproval_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgStaffTransferApproval.ItemDataBound
        Try

            If e.Item.ItemType = ListItemType.Header Then
                SetDateFormat()
                e.Item.Font.Bold = True
            End If

            If e.Item.ItemIndex < 0 Then Exit Sub

            If e.Item.ItemIndex >= 0 Then

                If CBool(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhIsGrp", False, True)).Text) = True Then
                    CType(e.Item.Cells(0).FindControl("lnkChangeStatus"), LinkButton).Visible = False

                    e.Item.Cells(0).Visible = False
                    e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).ColumnSpan = e.Item.Cells.Count - 8
                    e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Text = e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhEmployee", False, True)).Text & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & "Request Date  : " & eZeeDate.convertDate(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "objcolhrdate", False, True)).Text).ToShortDateString()
                    e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Font.Bold = True

                    For i = 3 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).CssClass = "group-header"
                    e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Style.Add("text-align", "left")
                Else

                    If IsDBNull(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Text) = False AndAlso e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Text <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).Text).ToShortDateString()
                    End If

                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).Text = info1.ToTitleCase(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).Text)
                    info1 = Nothing
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgStaffTransferApproval_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgStaffTransferApproval.PageIndexChanged
        Try
            dgStaffTransferApproval.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFromDate.ID, Me.lblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblToDate.ID, Me.lblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApprover.ID, Me.lblApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhEmployee", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhEmployee", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhReason", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhReason", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhStatus", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhStatus", False, True)).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhEmployee", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhEmployee", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhEmployee", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhApprovalDate", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUser", False, True)).HeaderText)

            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhBranch", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblBranch", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhBranch", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhDeptGrp", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblDeptGroup", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhDeptGrp", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhDepartment", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblDepartment", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhDepartment", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhSectionGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblSectionGroup", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhSectionGroup", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhSection", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblSection", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhSection", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUnitGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblUnitGroup", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUnitGroup", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUnit", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblUnit", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhUnit", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhTeam", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblTeam", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhTeam", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhJobGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblJobGroup", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhJobGroup", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhJob", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblJob", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhJob", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhClassGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblClassGroup", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhClassGroup", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhClass", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblClass", Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhClass", False, True)).HeaderText)

            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhReason", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhReason", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhReason", False, True)).HeaderText)
            Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhStatus", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhStatus", False, True)).FooterText, Me.dgStaffTransferApproval.Columns(getColumnId_Datagrid(dgStaffTransferApproval, "colhStatus", False, True)).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Approved By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Rejected By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You can't Edit this Loan detail. Reason: You are logged in into another user login.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings

End Class
