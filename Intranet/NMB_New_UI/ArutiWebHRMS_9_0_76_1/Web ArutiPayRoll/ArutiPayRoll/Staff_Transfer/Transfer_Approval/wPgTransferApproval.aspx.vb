﻿Option Strict On
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.IO

#End Region

Partial Class Staff_Transfer_Transfer_Approval_wPgTransferApproval
    Inherits Basepage

#Region " Private Variable(s) "
    Private Shared ReadOnly mstrModuleName As String = "frmStaffTransferApproval"
    Private mintTransferRequestId As Integer = 0
    Private mintTransferApprovalId As Integer = 0
    Private mintEmployeeId As Integer = 0
    Private mintMapUserId As Integer = 0
    Private mintPriority As Integer = -1
    Private DisplayMessage As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    Private mdtTransferRequestApplicationDocument As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    Private mintDeleteAttRowIndex As Integer = -1
    Dim xStatusId As Integer = clstransfer_request_tran.enTransferRequestStatus.Pending
    Private objCONN As SqlConnection

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillAllocationCombo(ByVal objEmployee As clsEmployee_Master)
        Dim dsCombos As New DataSet
        Dim objCommon As New clsCommon_Master
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSectionGrp As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objGradeGrp As New clsGradeGroup
        Dim objGrade As New clsGrade
        Try

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.STAFF_TRANSFER, True, "Reason")
            With cboNewReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Reason")
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objStation.getComboList("Station", True)
            If objEmployee._Stationunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("stationunkid") = objEmployee._Stationunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            If objEmployee._Deptgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("deptgroupunkid") = objEmployee._Deptgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewDeptGroup
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objDepartment.getComboList("Department", True)
            If objEmployee._Departmentunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("departmentunkid") = objEmployee._Departmentunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objSectionGrp.getComboList("SectionGrp", True)
            If objEmployee._Sectiongroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("sectiongroupunkid") = objEmployee._Sectiongroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewSectionGroup
                .DataValueField = "sectiongroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objSection.getComboList("Section", True)
            If objEmployee._Sectionunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("sectionunkid") = objEmployee._Sectionunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewSection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objUnitGroup.getComboList("UnitGrp", True)
            If objEmployee._Unitgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("unitgroupunkid") = objEmployee._Unitgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewUnitGroup
                .DataValueField = "unitgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objUnit.getComboList("Unit", True)
            If objEmployee._Unitunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("unitunkid") = objEmployee._Unitunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewUnit
                .DataValueField = "unitunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objTeam.getComboList("Team", True)
            If objEmployee._Teamunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("teamunkid") = objEmployee._Teamunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewTeam
                .DataValueField = "teamunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objJobGrp.getComboList("JobGrp", True)
            If objEmployee._Jobgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("jobgroupunkid") = objEmployee._Jobgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewJobGroup
                .DataValueField = "jobgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objJob.getComboList("Job", True)
            If objEmployee._Jobunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("jobunkid") = objEmployee._Jobunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            If objEmployee._Classgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("classgroupunkid") = objEmployee._Classgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewClassGroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objClass.getComboList("Class", True)
            If objEmployee._Classunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("classesunkid") = objEmployee._Classunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewClass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Dim mstrStaffTransferRequestAllocations As String = ""
        Try

            If Session("StaffTransferRequestAllocations") Is Nothing OrElse Session("StaffTransferRequestAllocations").ToString().Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Staff Transfer Allocation(s) are compulsory information.Please configure it from configuration settings screen."), Me)
                Exit Sub
            End If

            mstrStaffTransferRequestAllocations = Session("StaffTransferRequestAllocations").ToString()
            Dim ar() As String = mstrStaffTransferRequestAllocations.Split(CChar("|"))

            If ar IsNot Nothing AndAlso ar.Length > 0 Then

                divBranch.Visible = False
                divDeptGroup.Visible = False
                divDept.Visible = False
                divSectionGroup.Visible = False
                divSection.Visible = False
                divUnitGrp.Visible = False
                divUnit.Visible = False
                divTeam.Visible = False
                divJobGroup.Visible = False
                divJob.Visible = False
                divClassGrp.Visible = False
                divClass.Visible = False

                For i As Integer = 0 To ar.Length - 1

                    Select Case CInt(ar(i))

                        Case enAllocation.BRANCH
                            LblNewBranch.Visible = True
                            cboNewBranch.Visible = True
                            divBranch.Visible = True
                            RFVBranch.ValidationGroup = "RequireFields"

                        Case enAllocation.DEPARTMENT_GROUP
                            LblNewDeptGroup.Visible = True
                            cboNewDeptGroup.Visible = True
                            divDeptGroup.Visible = True
                            RFVDepartmentGrp.ValidationGroup = "RequireFields"

                        Case enAllocation.DEPARTMENT
                            LblNewDepartment.Visible = True
                            cboNewDepartment.Visible = True
                            divDept.Visible = True
                            RFVDepartment.ValidationGroup = "RequireFields"

                        Case enAllocation.SECTION_GROUP
                            LblNewSectionGroup.Visible = True
                            cboNewSectionGroup.Visible = True
                            divSectionGroup.Visible = True
                            RFVSectionGrp.ValidationGroup = "RequireFields"

                        Case enAllocation.SECTION
                            LblNewSection.Visible = True
                            cboNewSection.Visible = True
                            divSection.Visible = True
                            RFVSection.ValidationGroup = "RequireFields"

                        Case enAllocation.UNIT_GROUP
                            LblNewUnitGroup.Visible = True
                            cboNewUnitGroup.Visible = True
                            divUnitGrp.Visible = True
                            RFVUnitGrp.ValidationGroup = "RequireFields"

                        Case enAllocation.UNIT
                            LblNewUnit.Visible = True
                            cboNewUnit.Visible = True
                            divUnit.Visible = True
                            RFVUnit.ValidationGroup = "RequireFields"

                        Case enAllocation.TEAM
                            LblNewTeam.Visible = True
                            cboNewTeam.Visible = True
                            divTeam.Visible = True
                            RFVTeam.ValidationGroup = "RequireFields"

                        Case enAllocation.JOB_GROUP
                            LblNewJobGroup.Visible = True
                            cboNewJobGroup.Visible = True
                            divJobGroup.Visible = True
                            RFVJobGrp.ValidationGroup = "RequireFields"

                        Case enAllocation.JOBS
                            LblNewJob.Visible = True
                            cboNewJob.Visible = True
                            divJob.Visible = True
                            RFVJob.ValidationGroup = "RequireFields"

                        Case enAllocation.CLASS_GROUP
                            LblNewClassGroup.Visible = True
                            cboNewClassGroup.Visible = True
                            divClassGrp.Visible = True
                            RFVClassGrp.ValidationGroup = "RequireFields"

                        Case enAllocation.CLASSES
                            LblNewClass.Visible = True
                            cboNewClass.Visible = True
                            divClass.Visible = True
                            RFVClass.ValidationGroup = "RequireFields"

                    End Select

                Next

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Try
            Dim path As String = Server.MapPath("../../images/ChartUser.png")

            Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

                Using m As MemoryStream = New MemoryStream()
                    image.Save(m, image.RawFormat)
                    Dim imageBytes As Byte() = m.ToArray()
                    base64String = Convert.ToBase64String(imageBytes)
                End Using
            End Using
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return base64String
    End Function

    Private Sub SetEmployeeDetails(ByVal xEmployeeId As Integer)
        Dim objEmployee As New clsEmployee_Master
        Dim objBranch As New clsStation
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSectionGroup As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGroup As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGroup As New clsClassGroup
        Dim objClass As New clsClass
        Dim objGradeGroup As New clsGradeGroup
        Dim objGrade As New clsGrade
        Try
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = xEmployeeId

            objlblCode.Text = objEmployee._Employeecode.ToString()
            objlblEmpName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            If objEmployee._Stationunkid > 0 Then
                objBranch._Stationunkid = objEmployee._Stationunkid
                objlblBranch.Text = objBranch._Name.ToString()
            End If

            If objEmployee._Deptgroupunkid > 0 Then
                objDeptGroup._Deptgroupunkid = objEmployee._Deptgroupunkid
                objlblDeptGroup.Text = objDeptGroup._Name.ToString()
            End If

            If objEmployee._Departmentunkid > 0 Then
                objDepartment._Departmentunkid = objEmployee._Departmentunkid
                objlblDepartment.Text = objDepartment._Name.ToString()
            End If

            If objEmployee._Sectiongroupunkid > 0 Then
                objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                objlblSectionGroup.Text = objSectionGroup._Name.ToString()
            End If

            If objEmployee._Sectionunkid > 0 Then
                objSection._Sectionunkid = objEmployee._Sectionunkid
                objlblSection.Text = objSection._Name.ToString()
            End If

            If objEmployee._Unitgroupunkid > 0 Then
                objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                objlblUnitGroup.Text = objUnitGroup._Name.ToString()
            End If

            If objEmployee._Unitunkid > 0 Then
                objUnit._Unitunkid = objEmployee._Unitunkid
                objlblUnit.Text = objUnit._Name.ToString()
            End If

            If objEmployee._Teamunkid > 0 Then
                objTeam._Teamunkid = objEmployee._Teamunkid
                objlblTeam.Text = objTeam._Name.ToString()
            End If

            If objEmployee._Jobgroupunkid > 0 Then
                objJobGroup._Jobgroupunkid = objEmployee._Jobgroupunkid
                objlblJobGroup.Text = objJobGroup._Name.ToString()
            End If

            If objEmployee._Jobunkid > 0 Then
                objJob._Jobunkid = objEmployee._Jobunkid
                objlblJob.Text = objJob._Job_Name.ToString()
            End If

            If objEmployee._Classgroupunkid > 0 Then
                objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                objlblClassGroup.Text = objClassGroup._Name.ToString()
            End If

            If objEmployee._Classunkid > 0 Then
                objClass._Classesunkid = objEmployee._Classunkid
                objlblClass.Text = objClass._Name.ToString()
            End If

            If objEmployee._Gradegroupunkid > 0 Then
                objGradeGroup._Gradegroupunkid = objEmployee._Gradegroupunkid
                objlblGradeGroup.Text = objGradeGroup._Name.ToString()
            End If

            If objEmployee._Gradeunkid > 0 Then
                objGrade._Gradeunkid = objEmployee._Gradeunkid
                objlblGrade.Text = objGrade._Name.ToString()
            End If

            If objEmployee._blnImgInDb Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    If objEmployee._Photo IsNot Nothing Then
                        imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                    Else
                        imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
                    End If
                End If
            End If

            FillAllocationCombo(objEmployee)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objGrade = Nothing
            objGradeGroup = Nothing
            objClass = Nothing
            objClassGroup = Nothing
            objJob = Nothing
            objJobGroup = Nothing
            objTeam = Nothing
            objUnit = Nothing
            objUnitGroup = Nothing
            objSection = Nothing
            objSectionGroup = Nothing
            objDepartment = Nothing
            objDeptGroup = Nothing
            objBranch = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub FillTransferRequestAttachment()
        Dim dtView As DataView
        Try
            If mdtTransferRequestApplicationDocument Is Nothing Then
                dgStaffTransferAttachment.DataSource = Nothing
                dgStaffTransferAttachment.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtTransferRequestApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgStaffTransferAttachment.AutoGenerateColumns = False
            dgStaffTransferAttachment.DataSource = dtView
            dgStaffTransferAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtTransferRequestApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtTransferRequestApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(Session("Employeeunkid"))
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.STAFF_TRANSFER_REQUEST
                dRow("transactionunkid") = -1
                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Now
                dRow("orgfilepath") = strfullpath
                dRow("GUID") = Guid.NewGuid.ToString
                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                dRow("Documentype") = GetMimeType(strfullpath)
                Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try

                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtTransferRequestApplicationDocument.Rows.Add(dRow)
            Call FillTransferRequestAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If CInt(cboNewBranch.SelectedValue) <= 0 AndAlso cboNewBranch.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewBranch.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewBranch.Text, Me)
                cboNewBranch.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewDeptGroup.SelectedValue) <= 0 AndAlso cboNewDeptGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewDeptGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewDeptGroup.Text, Me)
                cboNewDeptGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewDepartment.SelectedValue) <= 0 AndAlso cboNewDepartment.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewDepartment.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewDepartment.Text, Me)
                cboNewDepartment.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewSectionGroup.SelectedValue) <= 0 AndAlso cboNewSectionGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewSectionGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewSectionGroup.Text, Me)
                cboNewSectionGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewSection.SelectedValue) <= 0 AndAlso cboNewSection.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewSection.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewSection.Text, Me)
                cboNewSection.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewUnitGroup.SelectedValue) <= 0 AndAlso cboNewUnitGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewUnitGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewUnitGroup.Text, Me)
                cboNewUnitGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewUnit.SelectedValue) <= 0 AndAlso cboNewUnit.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewUnit.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewUnit.Text, Me)
                cboNewUnit.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewTeam.SelectedValue) <= 0 AndAlso cboNewTeam.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewTeam.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewTeam.Text, Me)
                cboNewTeam.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewJobGroup.SelectedValue) <= 0 AndAlso cboNewJobGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewJobGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewJobGroup.Text, Me)
                cboNewJobGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewJob.SelectedValue) <= 0 AndAlso cboNewJob.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewJob.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewJob.Text, Me)
                cboNewJob.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewClassGroup.SelectedValue) <= 0 AndAlso cboNewClassGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewClassGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewClassGroup.Text, Me)
                cboNewClassGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewClass.SelectedValue) <= 0 AndAlso cboNewClass.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewClass.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewClass.Text, Me)
                cboNewClass.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewReason.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 3, "Reason is compulsory information.Please Select Reason."), Me)
                cboNewReason.Focus()
                mblnFlag = False

            ElseIf xStatusId = clstransfer_request_tran.enTransferRequestStatus.PushedBack AndAlso txtApprovalRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 4, "Approval Remark is compulsory information.Please enter approval remark."), Me)
                txtApprovalRemark.Focus()
                mblnFlag = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
            Return mblnFlag
        End Try
        Return mblnFlag
    End Function

    Private Sub GetValue()
        Dim objStaffTransferApproval As New clssttransfer_approval_Tran
        Try

            Dim objTransferRequest As New clstransfer_request_tran
            objTransferRequest._Transferrequestunkid(Nothing) = mintTransferRequestId
            dtpRequestDate.SetDate = objTransferRequest._Requestdate.Date
            txtRemark.Text = objTransferRequest._Remark
            SetEmployeeDetails(objTransferRequest._Employeeunkid)
            objTransferRequest = Nothing

            objStaffTransferApproval._Transferapprovalunkid = mintTransferApprovalId
            objStaffTransferApproval._Transferrequestunkid = mintTransferRequestId
            cboNewBranch.SelectedValue = objStaffTransferApproval._Stationunkid.ToString()
            cboNewDeptGroup.SelectedValue = objStaffTransferApproval._Deptgroupunkid.ToString()
            cboNewDepartment.SelectedValue = objStaffTransferApproval._Departmentunkid.ToString()
            cboNewSectionGroup.SelectedValue = objStaffTransferApproval._Sectiongroupunkid.ToString()
            cboNewSection.SelectedValue = objStaffTransferApproval._Sectionunkid.ToString()
            cboNewUnitGroup.SelectedValue = objStaffTransferApproval._Unitgroupunkid.ToString()
            cboNewUnit.SelectedValue = objStaffTransferApproval._Unitunkid.ToString()
            cboNewTeam.SelectedValue = objStaffTransferApproval._Teamunkid.ToString()
            cboNewJobGroup.SelectedValue = objStaffTransferApproval._Jobgroupunkid.ToString()
            cboNewJob.SelectedValue = objStaffTransferApproval._Jobunkid.ToString()
            cboNewClassGroup.SelectedValue = objStaffTransferApproval._Classgroupunkid.ToString()
            cboNewClass.SelectedValue = objStaffTransferApproval._Classunkid.ToString()
            cboNewReason.SelectedValue = objStaffTransferApproval._Reasonunkid.ToString()
            txtApprovalRemark.Text = objStaffTransferApproval._Approvalremark
            mintPriority = objStaffTransferApproval._Priority

            If mdtTransferRequestApplicationDocument Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                Call objAttchement.GetList(Session("Document_Path").ToString(), "List ", "", mintEmployeeId, , , , "hrdocuments_tran.transactionunkid = " & mintTransferRequestId, CBool(IIf(mintTransferRequestId > 0, False, True)), , enScanAttactRefId.STAFF_TRANSFER_REQUEST, 0, True, "")
                mdtTransferRequestApplicationDocument = objAttchement._Datatable.Copy()
                objAttchement = Nothing
            End If

            Call FillTransferRequestAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaffTransferApproval = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objStaffTransferApproval As clssttransfer_approval_Tran, ByVal xStatusId As Integer)
        Try
            objStaffTransferApproval._Transferapprovalunkid = mintTransferApprovalId
            objStaffTransferApproval._Transferrequestunkid = mintTransferRequestId
            objStaffTransferApproval._Approvaldate = Now
            objStaffTransferApproval._Mapuserunkid = mintMapUserId
            objStaffTransferApproval._Stationunkid = CInt(cboNewBranch.SelectedValue)
            objStaffTransferApproval._Deptgroupunkid = CInt(cboNewDeptGroup.SelectedValue)
            objStaffTransferApproval._Departmentunkid = CInt(cboNewDepartment.SelectedValue)
            objStaffTransferApproval._Sectiongroupunkid = CInt(cboNewSectionGroup.SelectedValue)
            objStaffTransferApproval._Sectionunkid = CInt(cboNewSection.SelectedValue)
            objStaffTransferApproval._Unitgroupunkid = CInt(cboNewUnitGroup.SelectedValue)
            objStaffTransferApproval._Unitunkid = CInt(cboNewUnit.SelectedValue)
            objStaffTransferApproval._Teamunkid = CInt(cboNewTeam.SelectedValue)
            objStaffTransferApproval._Jobgroupunkid = CInt(cboNewJobGroup.SelectedValue)
            objStaffTransferApproval._Jobunkid = CInt(cboNewJob.SelectedValue)
            objStaffTransferApproval._Classgroupunkid = CInt(cboNewClassGroup.SelectedValue)
            objStaffTransferApproval._Classunkid = CInt(cboNewClass.SelectedValue)
            objStaffTransferApproval._Reasonunkid = CInt(cboNewReason.SelectedValue)
            objStaffTransferApproval._Approvalremark = txtApprovalRemark.Text.Trim
            objStaffTransferApproval._Statusunkid = xStatusId
            objStaffTransferApproval._Visibleunkid = xStatusId
            objStaffTransferApproval._Userunkid = CInt(Session("UserId"))
            objStaffTransferApproval._Isvoid = False
            objStaffTransferApproval._FormName = mstrModuleName
            objStaffTransferApproval._ClientIP = CStr(Session("IP_ADD"))
            objStaffTransferApproval._HostName = CStr(Session("HOST_NAME"))
            objStaffTransferApproval._IsWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SaveAttachment(ByVal mdtAttachments As DataTable) As Boolean
        Try
            Dim strFileName As String = ""
            Dim strError As String = ""

            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.STAFF_TRANSFER_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

            For Each dRow As DataRow In mdtAttachments.Rows

                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                    If File.Exists(CStr(dRow("orgfilepath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath

                        dRow.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Configuration Path does not Exist."), Me)
                        Return False
                    End If

                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Configuration Path does not Exist."), Me)
                            Return False
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Configuration Path does not Exist."), Me)
                        Return False
                    End If

                End If

            Next

            If mdsDoc IsNot Nothing Then mdsDoc.Clear()
            mdsDoc = Nothing

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveAttachment", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub


                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                If Request.QueryString("uploadimage") Is Nothing Then

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 5 Then
                        mintTransferApprovalId = CInt(arr(0))
                        mintMapUserId = CInt(arr(1))
                        mintTransferRequestId = CInt(arr(2))
                        mintEmployeeId = CInt(arr(3))
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(4))
                        Session("UserId") = mintMapUserId


                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If

                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Session("UserId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub
                            Else

                                If mblnATLoginRequiredToApproveApplications = False Then

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")

                        gobjConfigOptions = New clsConfigOptions
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)


                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If


                        If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                        End If
                        Session("DateFormat") = ConfigParameter._Object._CompanyDateFormat
                        Session("DateSeparator") = ConfigParameter._Object._CompanyDateSeparator

                        Call SetDateFormat()

                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                        objUser = Nothing

                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                                End If  ' If mblnATLoginRequiredToApproveApplications = False Then

                            End If ' If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else

                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub

                        End If  '  If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                        'Pinkal (23-Feb-2024) -- End

                        Dim objTransfer As New clstransfer_request_tran
                        objTransfer._Transferrequestunkid = mintTransferRequestId
                        Dim mdtRequestDate As Date = objTransfer._Requestdate.Date
                        objTransfer = Nothing

                        Dim objStaffTransferApproval As New clssttransfer_approval_Tran
                        Dim dsList As DataSet = Nothing
                        Dim mstrSearch As String = "sta.transferapprovalunkid = " & mintTransferApprovalId & " AND sta.transferrequestunkid = " & mintTransferRequestId & " AND sta.employeeunkid = " & mintEmployeeId & " AND sta.mapuserunkid = " & mintMapUserId

                        dsList = objStaffTransferApproval.GetTrasferApprovalList(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), mdtRequestDate.Date, mdtRequestDate.Date, Session("UserAccessModeSetting").ToString _
                                                                                                                                 , True, False, "List", mstrSearch, False, Nothing)

                        objStaffTransferApproval = Nothing


                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            If CInt(dsList.Tables(0).Rows(0)("visibleunkid")) = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "You can not Edit this transfer request detail. Reason: This transfer request is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                                'Pinkal (23-Feb-2024) -- Start
                                '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                Session("ApprovalLink") = Nothing
                                Session("ApproverUserId") = Nothing

                                If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                        Session.Abandon()
                                        If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                            Response.Cookies("ASP.NET_SessionId").Value = ""
                                            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                        End If

                                        If Request.Cookies("AuthToken") IsNot Nothing Then
                                            Response.Cookies("AuthToken").Value = ""
                                            Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                        End If

                                    End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                                'Pinkal (23-Feb-2024) -- End

                                Exit Sub
                            End If  '  If CInt(dsList.Tables(0).Rows(0)("visibleunkid")) = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, there is no pending transfer request data."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                            'Pinkal (23-Feb-2024) -- End

                            Exit Sub
                        End If   ' If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then 

                        Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= CInt(dsList.Tables(0).Rows(0)("priority")) AndAlso x.Field(Of Integer)("statusunkid") <> clstransfer_request_tran.enTransferRequestStatus.Pending)
                        If dtRow.Count > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "You can not Edit this transfer request detail. Reason: This transfer request is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then
                            'Pinkal (23-Feb-2024) -- End

                            Exit Sub
                        End If ' If dtRow.Count > 0 Then

                        GoTo Link

                    End If

                End If

            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False AndAlso Request.QueryString("uploadimage") Is Nothing Then
Link:
                SetControlCaptions()
                SetLanguage()
                SetMessages()
                FillDocumentTypeCombo()
                SetVisibility()

                If Session("transferapprovalunkid") IsNot Nothing Then
                    mintTransferApprovalId = CInt(Session("transferapprovalunkid"))
                End If
                If Session("transferrequestunkid") IsNot Nothing Then
                    mintTransferRequestId = CInt(Session("transferrequestunkid"))
                End If
                If Session("StaffTransferEmployeeId") IsNot Nothing Then
                    mintEmployeeId = CInt(Session("StaffTransferEmployeeId"))
                End If
                If Session("Mapuserunkid") IsNot Nothing Then
                    mintMapUserId = CInt(Session("Mapuserunkid"))
                End If
                GetValue()
            Else
                mintTransferApprovalId = CInt(ViewState("transferapprovalunkid"))
                mintTransferRequestId = CInt(ViewState("transferrequestunkid"))
                mintEmployeeId = CInt(ViewState("StaffTransferEmployeeId"))
                mintMapUserId = CInt(ViewState("Mapuserunkid"))
                mdtTransferRequestApplicationDocument = CType(ViewState("TransferRequestApplicationDocument"), DataTable)
                mintDeleteAttRowIndex = CInt(ViewState("DeleteAttRowIndex"))
                mintPriority = CInt(ViewState("Priority"))
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("transferapprovalunkid") = mintTransferApprovalId
            ViewState("transferrequestunkid") = mintTransferRequestId
            ViewState("StaffTransferEmployeeId") = mintEmployeeId
            ViewState("Mapuserunkid") = mintMapUserId
            ViewState("TransferRequestApplicationDocument") = mdtTransferRequestApplicationDocument
            ViewState("DeleteAttRowIndex") = mintDeleteAttRowIndex
            ViewState("Priority") = mintPriority
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub dgStaffTransferAttachment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgStaffTransferAttachment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtTransferRequestApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtTransferRequestApplicationDocument.Select("GUID = '" & e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        mintDeleteAttRowIndex = mdtTransferRequestApplicationDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "colhName", False, True)).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnAddAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If
                AddDocumentAttachment(f, f.FullName)
                Call FillTransferRequestAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnPushedBack.Click
        Dim objStaffTransferApproval As New clssttransfer_approval_Tran
        Try

            If CType(sender, Button).ID.ToUpper() = btnApprove.ID.ToUpper() Then
                xStatusId = clstransfer_request_tran.enTransferRequestStatus.Approved
            ElseIf CType(sender, Button).ID.ToUpper() = btnPushedBack.ID.ToUpper() Then
                xStatusId = clstransfer_request_tran.enTransferRequestStatus.PushedBack
            End If

            If Validation() = False Then Exit Sub

            SetValue(objStaffTransferApproval, xStatusId)

            'If SaveAttachment(mdtTransferRequestApplicationDocument) = False Then Exit Sub

            Dim mblnFlag As Boolean = False
            If mintTransferApprovalId > 0 Then
                mblnFlag = objStaffTransferApproval.Update(CInt(Session("CompanyUnkId")), Session("Database_Name").ToString(), CInt(Session("Fin_year")), dtpRequestDate.GetDate.Date, Nothing)
            End If

            If mblnFlag = False And objStaffTransferApproval._Message <> "" Then
                DisplayMessage.DisplayMessage(objStaffTransferApproval._Message, Me)
                Exit Sub
            Else
                Dim mstrStatus As String = ""
                If xStatusId = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clstransfer_request_tran", 3, "Approved")
                ElseIf xStatusId = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clstransfer_request_tran", 4, "Pushed Back")
                End If

                If mdtTransferRequestApplicationDocument IsNot Nothing Then
                    mdtTransferRequestApplicationDocument.Clear()
                    mdtTransferRequestApplicationDocument = Nothing
                End If


                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If


                Dim objTransfer As New clstransfer_request_tran

                objTransfer._Transferrequestunkid = mintTransferRequestId

                objTransfer._Userunkid = objStaffTransferApproval._Userunkid
                objTransfer._WebFormName = objStaffTransferApproval._FormName
                objTransfer._WebClientIP = objStaffTransferApproval._ClientIP
                objTransfer._WebHostName = objStaffTransferApproval._HostName
                objTransfer._IsWeb = objStaffTransferApproval._IsWeb

                If objTransfer._Statusunkid = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                    objTransfer.Send_NotificationEmployee(mintEmployeeId, objTransfer._Requestdate.Date, CInt(Session("CompanyUnkId")), clstransfer_request_tran.enTransferRequestStatus.Approved, "", enLoginMode, intLoginByEmployeeId, intUserId)

                    Dim strPath As String = ""
                    Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp

                    If Session("StaffTransferFinalApprovalNotificationUserIds").ToString().Trim.Length > 0 Then
                        strPath = DisplayMessage.Export_EStaffTransferRequest(Path, objTransfer._Requestdate.Date, objTransfer._Transferrequestunkid, objTransfer._Employeeunkid, objTransfer._Requestdate.Date)
                        objStaffTransferApproval.SendStaffTransferFinalApprovalNotificationToUsers(CInt(Session("CompanyUnkId")), objTransfer._Requestdate.Date, Session("StaffTransferFinalApprovalNotificationUserIds").ToString() _
                                                                                                                                   , mintEmployeeId, Path, strPath)
                    End If
                Else
                    If xStatusId = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                        objTransfer.Send_NotificationApprover(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), objTransfer._Requestdate.Date, clstransfer_request_tran.enTransferRequestStatus.Pending _
                                                                                , Session("UserAccessModeSetting").ToString, objTransfer._Employeeunkid, mintTransferRequestId.ToString(), Session("ArutiSelfServiceURL").ToString(), mintPriority, txtApprovalRemark.Text.Trim, enLoginMode, intLoginByEmployeeId, intUserId)

                    ElseIf xStatusId = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then

                        Dim dsList As DataSet = objStaffTransferApproval.GetTrasferApprovalList(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), objTransfer._Requestdate.Date, objTransfer._Requestdate.Date, Session("UserAccessModeSetting").ToString _
                                                                                                                                 , True, False, "List", "sta.transferrequestunkid = " & mintTransferRequestId & " AND sta.priority < " & mintPriority, False, Nothing)

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            objTransfer.Send_NotificationApprover(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), objTransfer._Requestdate.Date, clstransfer_request_tran.enTransferRequestStatus.PushedBack _
                                                                               , Session("UserAccessModeSetting").ToString, objTransfer._Employeeunkid, mintTransferRequestId.ToString(), Session("ArutiSelfServiceURL").ToString(), mintPriority, txtApprovalRemark.Text.Trim, enLoginMode, intLoginByEmployeeId, intUserId)
                        Else
                            objTransfer.Send_NotificationEmployee(mintEmployeeId, objTransfer._Requestdate.Date, CInt(Session("CompanyUnkId")), clstransfer_request_tran.enTransferRequestStatus.PushedBack, txtApprovalRemark.Text.Trim, enLoginMode, intLoginByEmployeeId, intUserId)
                        End If

                    End If
                End If

                objTransfer = Nothing

                mintPriority = 0
                mintTransferApprovalId = 0
                mintTransferRequestId = 0
                mintEmployeeId = 0
                mintMapUserId = 0
                Session("transferapprovalunkid") = Nothing
                Session("transferrequestunkid") = Nothing
                Session("StaffTransferEmployeeId") = Nothing
                Session("Mapuserunkid") = Nothing


                If Request.QueryString.Count > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Transfer Request application") & " " & mstrStatus & " " & _
                                                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "successfully") & ".", Me, Session("rootpath").ToString & "Index.aspx")

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                    Session.Abandon()
                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If
                    'Pinkal (23-Feb-2024) -- End

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Transfer Request application") & " " & mstrStatus & " " & _
                                                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "successfully") & ".", Me, Session("rootpath").ToString() & "Staff_Transfer/Transfer_Approval/wPgTransferApprovalList.aspx")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (23-Feb-2024) -- Start
            '(A1X-2461) NMB : R&D - Force manual user login on approval links.
            If Request.QueryString.Count > 0 Then
                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()
                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If
                Response.Redirect("~/Index.aspx", False)
            Else
            Response.Redirect(Session("rootpath").ToString() & "Staff_Transfer/Transfer_Approval/wPgTransferApprovalList.aspx", False)
            End If
            'Pinkal (23-Feb-2024) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            mintDeleteAttRowIndex = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteAttRowIndex >= 0 Then
                mdtTransferRequestApplicationDocument.Rows(mintDeleteAttRowIndex)("AUD") = "D"
                mdtTransferRequestApplicationDocument.AcceptChanges()
                Call FillTransferRequestAttachment()
                mintDeleteAttRowIndex = -1
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblMainHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployeeDetails.ID, Me.LblEmployeeDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTransferDetails.ID, Me.LblTransferDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAttachmentInfo.ID, Me.lblAttachmentInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDocumentType.ID, Me.lblDocumentType.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblReason.ID, Me.LblReason.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRemark.ID, Me.LblRemark.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAddAttachment.ID, Me.btnAddAttachment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnApprove.ID, Me.btnApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnPushedBack.ID, Me.btnPushedBack.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblMainHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblMainHeader.Text)

            Me.LblEmployeeDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblEmployeeDetails.ID, LblEmployeeDetails.Text)
            Me.LblTransferDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblTransferDetails.ID, LblTransferDetails.Text)
            Me.lblAttachmentInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAttachmentInfo.ID, lblAttachmentInfo.Text)
            Me.lblDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDocumentType.ID, lblDocumentType.Text)

            Me.LblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblBranch.ID, Me.LblBranch.Text)
            Me.LblNewBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblBranch.ID, Me.LblBranch.Text)
            Me.LblDeptGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDeptGroup.ID, Me.LblDeptGroup.Text)
            Me.LblNewDeptGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDeptGroup.ID, Me.LblDeptGroup.Text)
            Me.LblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDepartment.ID, Me.LblDepartment.Text)
            Me.LblNewDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDepartment.ID, Me.LblDepartment.Text)
            Me.LblSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSectionGroup.ID, Me.LblSectionGroup.Text)
            Me.LblNewSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSectionGroup.ID, Me.LblSectionGroup.Text)
            Me.LblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSection.ID, Me.LblSection.Text)
            Me.LblNewSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSection.ID, Me.LblSection.Text)
            Me.LblUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnitGroup.ID, Me.LblUnitGroup.Text)
            Me.LblNewUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnitGroup.ID, Me.LblUnitGroup.Text)
            Me.LblUnit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnit.ID, Me.LblUnit.Text)
            Me.LblNewUnit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnit.ID, Me.LblUnit.Text)
            Me.LblTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblTeam.ID, Me.LblTeam.Text)
            Me.LblNewTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblTeam.ID, Me.LblTeam.Text)
            Me.LblJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJobGroup.ID, Me.LblJobGroup.Text)
            Me.LblNewJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJobGroup.ID, Me.LblJobGroup.Text)
            Me.LblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJob.ID, Me.LblJob.Text)
            Me.LblNewJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJob.ID, Me.LblJob.Text)
            Me.LblClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClassGroup.ID, Me.LblClassGroup.Text)
            Me.LblNewClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClassGroup.ID, Me.LblClassGroup.Text)
            Me.LblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClass.ID, Me.LblClass.Text)
            Me.LblNewClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClass.ID, Me.LblClass.Text)
            Me.LblGradeGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblGradeGroup.ID, Me.LblGradeGroup.Text)
            Me.LblGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblGrade.ID, Me.LblGrade.Text)

            Me.LblReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReason.ID, Me.LblReason.Text)
            Me.LblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRemark.ID, Me.LblRemark.Text)

            Me.btnAddAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddAttachment.ID, Me.btnAddAttachment.Text).Replace("&", "")
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnPushedBack.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnPushedBack.ID, Me.btnPushedBack.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Staff Transfer Allocation(s) are compulsory information.Please configure it from configuration settings screen.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "is compulsory information.Please select.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Reason is compulsory information.Please Select Reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Transfer Request application applied successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Please select PDF or Image file.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Configuration Path does not Exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Transfer Request application")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "You can not Edit this transfer request detail. Reason: This transfer request is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, there is no pending transfer request data.")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


End Class
