﻿<%@ Page Language="VB" Title="Staff Transfer Approval List" AutoEventWireup="false"
    MasterPageFile="~/Home1.master" CodeFile="wPgTransferApprovalList.aspx.vb" Inherits="Staff_Transfer_Transfer_Approval_wPgTransferApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Staff Transfer Approval List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="false" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprover" runat="server" CssClass="form-control" Enabled="false">  </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgStaffTransferApproval" runat="server" AutoGenerateColumns="False"
                                                  CssClass="table table-hover table-bordered " Width="150%" AllowPaging="true" PageSize = "500" PagerStyle-Position="Top" PagerStyle-Wrap="true" PagerStyle-Mode="NumericPages"   PagerStyle-HorizontalAlign="Left">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="110px" Visible="true" ItemStyle-Width="110px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="colhChangeStatus">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkChangeStatus" Font-Underline="false" CommandName="Status"
                                                                    Text="Change Status" ToolTip="Change Status" runat="server">
                                                                     <i class="fas fa-tasks"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Employee" HeaderText="Employee" Visible="false" FooterText="colhEmployee" />
                                                    <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" FooterText="colhApprovalDate" />
                                                    <asp:BoundColumn DataField="CUser" HeaderText="Approver" FooterText="colhUser" />
                                                    <asp:BoundColumn DataField="station" HeaderText="Branch" ReadOnly="true" FooterText="colhBranch"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="deptgrp" HeaderText="Department Group" ReadOnly="true"
                                                        FooterText="colhDeptGrp" Visible="false" />
                                                    <asp:BoundColumn DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDepartment"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="sectiongrp" HeaderText="Section Group" ReadOnly="true"
                                                        FooterText="colhSectionGroup" Visible="false" />
                                                    <asp:BoundColumn DataField="section" HeaderText="Section" ReadOnly="true" FooterText="colhSection"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="unitgrp" HeaderText="Unit Group" ReadOnly="true" FooterText="colhUnitGroup"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="unit" HeaderText="Unit" ReadOnly="true" FooterText="colhUnit"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="team" HeaderText="Team" ReadOnly="true" FooterText="colhTeam"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="jobgrp" HeaderText="Job Group" ReadOnly="true" FooterText="colhJobGroup"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="job" HeaderText="Job" ReadOnly="true" FooterText="colhJob"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="classgrp" HeaderText="Class Group" ReadOnly="true" FooterText="colhClassGroup"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="classes" HeaderText="Class" ReadOnly="true" FooterText="colhClass"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="Reason" HeaderText="Reason" ReadOnly="true" FooterText="colhReason" />
                                                    <asp:BoundColumn DataField="Status" HeaderText="Status" FooterText="colhStatus" />
                                                    <asp:BoundColumn DataField="rdate" HeaderText="rdate" FooterText="objcolhrdate" Visible="false" />
                                                    <asp:BoundColumn DataField="priority" HeaderText="objcolhPriority" Visible="false"
                                                        FooterText="objcolhPriority" />
                                                    <asp:BoundColumn DataField="isgrp" HeaderText="objcolhIsGrp" Visible="false" FooterText="objcolhIsGrp" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="objcolhEmployeeID" Visible="false"
                                                        FooterText="objcolhEmployeeID" />
                                                    <asp:BoundColumn DataField="mapuserunkid" HeaderText="objcolhUserID" Visible="false"
                                                        FooterText="objcolhUserID" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="objcolhStatusunkid" Visible="false"
                                                        FooterText="objcolhStatusunkid" />
                                                    <asp:BoundColumn DataField="appstatusunkid" HeaderText="objcolhAppStatusID" Visible="false"
                                                        FooterText="objcolhAppStatusID" />
                                                    <asp:BoundColumn DataField="transferrequestunkid" HeaderText="objcolhtransferrequestunkid"
                                                        Visible="false" FooterText="objcolhtransferrequestunkid" />
                                                    <asp:BoundColumn DataField="transferapprovalunkid" HeaderText="objcolhtransferapprovalunkid"
                                                        Visible="false" FooterText="objcolhtransferapprovalunkid" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
