﻿Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

Partial Class Staff_Transfer_Level_Role_Maping_wPgLevelRoleMapping
    Inherits Basepage


#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmStaffTransferLevelRoleMapping"
    Private mintMappingunkid As Integer
    Private currentId As String = ""
    Private Index As Integer
    Private mblnShowpopupLevelRoleMapping As Boolean = False
#End Region

#Region "Form Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                SetMessages()
                Call SetLanguage()
                ListFillCombo()
                FillList(True)
                SetVisibility()
            Else
                mintMappingunkid = CType(ViewState("mintMappingunkid"), Integer)
                mblnShowpopupLevelRoleMapping = Convert.ToBoolean(ViewState("ShowpopupLevelRoleMapping").ToString())

                If mblnShowpopupLevelRoleMapping Then
                    popupLevelRoleMapping.Show()
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintMappingunkid") = mintMappingunkid
            ViewState("ShowpopupLevelRoleMapping") = mblnShowpopupLevelRoleMapping
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "   For List"

#Region "   Private Function"

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsLevelRoleMapping As DataSet = Nothing
        Dim dtLevel As DataTable = Nothing
        Dim strSearching As String = ""
        Dim objLevelRoleMapping As New clsstapproverlevel_role_mapping

        Try

            If CBool(Session("AllowToViewStaffTransferLevelRoleMapping")) = False Then Exit Sub

            If isblank Then
                strSearching = "AND 1 = 2 "
            End If

            If CInt(cboRoleList.SelectedValue) > 0 Then
                strSearching &= "AND stapproverlevel_role_mapping.roleunkid = " & CInt(cboRoleList.SelectedValue) & " "
            End If

            If CInt(cboLevelList.SelectedValue) > 0 Then
                strSearching &= "AND stapproverlevel_role_mapping.levelunkid = " & CInt(cboLevelList.SelectedValue) & " "
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            dsLevelRoleMapping = objLevelRoleMapping.GetList("List", True, Nothing, strSearching)

            If dsLevelRoleMapping.Tables(0).Rows.Count <= 0 Then
                isblank = True
                Dim drRow As DataRow = dsLevelRoleMapping.Tables(0).NewRow()
                drRow("stlevelname") = ""
                drRow("levelunkid") = 0
                drRow("stmappingunkid") = 0
                drRow("isuseraccess") = False
                dsLevelRoleMapping.Tables(0).Rows.Add(drRow)
            End If

            If dsLevelRoleMapping IsNot Nothing Then
                dtLevel = New DataView(dsLevelRoleMapping.Tables(0), "", "priority", DataViewRowState.CurrentRows).ToTable()

                Dim strLevelName As String = ""
                Dim dtTable As DataTable = dtLevel.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.Boolean"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtLevel.Rows
                    If CStr(drow("stlevelname")).Trim <> strLevelName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("stmappingunkid") = drow("stmappingunkid")
                        dtRow("stlevelname") = drow("stlevelname")
                        strLevelName = drow("stlevelname").ToString()
                        dtRow("isuseraccess") = False
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtLevel.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

                dtTable.AcceptChanges()

                If isblank = True Then
                    dgLevelRoleMappingList.DataSource = New List(Of String)
                    dgLevelRoleMappingList.DataBind()
                Else
                    dgLevelRoleMappingList.DataSource = dtTable
                    dgLevelRoleMappingList.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevelRoleMapping = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Enabled = CBool(Session("AllowToAddStaffTransferLevelRoleMapping"))
            'dgLevelRoleMappingList.Columns(getColumnId_Datagrid(dgLevelRoleMappingList, "dgcolhedit", False, True)).Visible = CBool(Session("AllowToEditStaffTransferLevelRoleMapping"))
            dgLevelRoleMappingList.Columns(getColumnId_Datagrid(dgLevelRoleMappingList, "dgcolhdelete", False, True)).Visible = Session("AllowToDeleteStaffTransferLevelRoleMapping")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ListFillCombo()
        Dim dsList As DataSet = Nothing
        Dim objRole As clsUserRole_Master
        Dim objLevel As clsstapproverlevel_master
        Try
            objRole = New clsUserRole_Master
            dsList = objRole.getComboList("List", True)
            With cboRoleList
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            With cboRole
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            dsList.Clear()
            dsList = Nothing

            objLevel = New clsstapproverlevel_master
            dsList = objLevel.getListForCombo("List", True)
            With cboLevelList
                .DataTextField = "name"
                .DataValueField = "stlevelunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            With cboLevel
                .DataTextField = "name"
                .DataValueField = "stlevelunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            dsList.Clear()
            dsList = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRole = Nothing
            objLevel = Nothing
        End Try
    End Sub

    Private Sub SetAtValue(ByVal xFormName As String, ByVal objLevelRoleMapping As clsstapproverlevel_role_mapping)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLevelRoleMapping._AuditUserId = CInt(Session("UserId"))
            End If
            objLevelRoleMapping._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objLevelRoleMapping._ClientIP = CStr(Session("IP_ADD"))
            objLevelRoleMapping._HostName = CStr(Session("HOST_NAME"))
            objLevelRoleMapping._FormName = xFormName
            objLevelRoleMapping._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            Reset()
            popupLevelRoleMapping.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboRoleList.SelectedValue = "0"
            cboLevelList.SelectedValue = "0"
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objLevelRoleMapping As clsstapproverlevel_role_mapping

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            objLevelRoleMapping = New clsstapproverlevel_role_mapping
            objLevelRoleMapping._Stmappingunkid = lnkedit.CommandArgument.ToString()
            mintMappingunkid = lnkedit.CommandArgument.ToString()
            GetValue()
            mblnShowpopupLevelRoleMapping = True
            popupLevelRoleMapping.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevelRoleMapping = Nothing
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objLevelRoleMapping As clsstapproverlevel_role_mapping
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)

            objLevelRoleMapping = New clsstapproverlevel_role_mapping
            objLevelRoleMapping._Stmappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingunkid = CInt(lnkdelete.CommandArgument.ToString())

            If objLevelRoleMapping.isUsed(mintMappingunkid) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, You cannot delete this mapping of role and level . Reason: This Mapping is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Are you sure you want to delete ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevelRoleMapping = Nothing
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Dim objLevelRoleMapping As clsstapproverlevel_role_mapping
        Try

            objLevelRoleMapping = New clsstapproverlevel_role_mapping

            SetAtValue(mstrModuleName, objLevelRoleMapping)

            objLevelRoleMapping._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objLevelRoleMapping._FormName = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLevelRoleMapping._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objLevelRoleMapping.Delete(mintMappingunkid) = False Then
                DisplayMessage.DisplayMessage(objLevelRoleMapping._Message, Me)
                Exit Sub
            End If

            mintMappingunkid = 0

            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevelRoleMapping = Nothing
        End Try
    End Sub

#End Region

#Region " Datagrid Events "

    Protected Sub dgLevelRoleMappingList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgLevelRoleMappingList.ItemCommand
        Try

            'If CInt(dgvStaffTransfer.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvStaffTransfer, "objColhStatusunkid", False, True)).Text) <> clstransfer_request_tran.enTransferRequestStatus.Pending Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "You cannot Edit this transfer request application. Reason: This application is already") & dgvStaffTransfer.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvStaffTransfer, "colhstatus", False, True)).Text & " .", Me)
            '    Exit Sub
            'End If

            'If e.CommandName.ToUpper = "EDIT" Then

            '    Session("TransferRequestId") = CInt(dgvStaffTransfer.DataKeys(e.Item.ItemIndex))

            '    Response.Redirect(Session("rootpath").ToString() & "Staff_Transfer/wPgStaffTransferRequest.aspx", False)

            'ElseIf e.CommandName.ToUpper = "DELETE" Then

            '    popDeleteReason.Reason = ""

            '    If CInt(dgvStaffTransfer.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvStaffTransfer, "objColhStatusunkid", False, True)).Text) = clstransfer_request_tran.enTransferRequestStatus.Pending Then
            '        mintTransferRequestId = CInt(dgvStaffTransfer.DataKeys(e.Item.ItemIndex))
            '        popDeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Are you sure you want to delete this Transfer Request Application?")
            '        popDeleteReason.Show()
            '    End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvStaffTransfer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLevelRoleMappingList.ItemDataBound
        Try
            If e.Item.ItemIndex < 0 Then Exit Sub

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                If Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then

                    Dim lnkEdit As LinkButton = TryCast(e.Item.FindControl("lnkedit"), LinkButton)
                    lnkEdit.Visible = False

                    Dim lnkDelete As LinkButton = TryCast(e.Item.FindControl("lnkdelete"), LinkButton)
                    lnkDelete.Visible = False

                    e.Item.Cells(3).Text = DataBinder.Eval(e.Item.DataItem, "stlevelname").ToString
                    e.Item.Cells(3).ColumnSpan = e.Item.Cells.Count - 1
                    e.Item.BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Item.Font.Bold = True

                    For i As Integer = 4 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "   For Add/Edit"

#Region "   Private Function"

    Private Function Validation() As Boolean
        Try
            If CInt(cboRole.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Role is  compulsory information.Please select Role."), Me)
                cboRole.Focus()
                Return False
            ElseIf CInt(cboLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Level is  compulsory information.Please select Level."), Me)
                cboLevel.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub GetValue()
        Dim objLevelRoleMapping As clsstapproverlevel_role_mapping
        Try
            objLevelRoleMapping = New clsstapproverlevel_role_mapping
            If mintMappingunkid > 0 Then
                objLevelRoleMapping._Stmappingunkid = mintMappingunkid
            End If
            cboRole.SelectedValue = objLevelRoleMapping._Roleunkid.ToString()
            cboLevel.SelectedValue = objLevelRoleMapping._Levelunkid.ToString()
            chkUserAccess.Checked = objLevelRoleMapping._Isuseraccess
            chkUserAccess.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevelRoleMapping = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objLevelRoleMapping As clsstapproverlevel_role_mapping)
        Try
            If mintMappingunkid > 0 Then objLevelRoleMapping._Stmappingunkid = mintMappingunkid
            objLevelRoleMapping._Isuseraccess = chkUserAccess.Checked
            objLevelRoleMapping._Roleunkid = CInt(cboRole.SelectedValue)
            objLevelRoleMapping._Levelunkid = CInt(cboLevel.SelectedValue)
            objLevelRoleMapping._Userunkid = CInt(Session("UserId"))
            SetAtValue(mstrModuleName, objLevelRoleMapping)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset()
        Try
            cboRole.SelectedValue = "0"
            cboLevel.SelectedValue = "0"
            chkUserAccess.Checked = False
            chkUserAccess.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnSaveLevelRoleMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveLevelRoleMapping.Click
        Dim objLevelRoleMapping As New clsstapproverlevel_role_mapping
        Try

            If Validation() = False Then
                mblnShowpopupLevelRoleMapping = True
                popupLevelRoleMapping.Show()
                Exit Sub
            End If

            SetValue(objLevelRoleMapping)

            Dim blnFlag As Boolean = False
            If mintMappingunkid > 0 Then
                blnFlag = objLevelRoleMapping.Update()
            ElseIf mintMappingunkid <= 0 Then
                blnFlag = objLevelRoleMapping.Insert()
            End If

            If blnFlag = False And objLevelRoleMapping._Message <> "" Then
                DisplayMessage.DisplayMessage(objLevelRoleMapping._Message, Me)
                mblnShowpopupLevelRoleMapping = True
                popupLevelRoleMapping.Show()
                Exit Sub
            Else
                FillList(False)
                Reset()
                mintMappingunkid = 0
                ViewState("mintMappingunkid") = mintMappingunkid
                mblnShowpopupLevelRoleMapping = False
                popupLevelRoleMapping.Hide()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevelRoleMapping = Nothing
        End Try
    End Sub

    Protected Sub btnCloseLevelRoleMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseLevelRoleMapping.Click
        Try
            Reset()
            mblnShowpopupLevelRoleMapping = False
            popupLevelRoleMapping.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblAddEditPageHeader.ID, Me.LblAddEditPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCaption.ID, Me.lblCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRoleList.ID, Me.LblRoleList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLevelList.ID, Me.LblLevelList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRole.ID, Me.LblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLevel.ID, Me.LblLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkUserAccess.ID, Me.chkUserAccess.Text)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLevelRoleMappingList.Columns(2).FooterText, Me.dgLevelRoleMappingList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLevelRoleMappingList.Columns(3).FooterText, Me.dgLevelRoleMappingList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLevelRoleMappingList.Columns(4).FooterText, Me.dgLevelRoleMappingList.Columns(4).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveLevelRoleMapping.ID, Me.btnSaveLevelRoleMapping.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCloseLevelRoleMapping.ID, Me.btnCloseLevelRoleMapping.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.LblAddEditPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAddEditPageHeader.ID, Me.LblAddEditPageHeader.Text)
            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCaption.ID, Me.lblCaption.Text)
            Me.LblRoleList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRoleList.ID, Me.LblRoleList.Text)
            Me.LblLevelList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLevelList.ID, Me.LblLevelList.Text)
            Me.LblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRole.ID, Me.LblRole.Text)
            Me.LblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLevel.ID, Me.LblLevel.Text)
            Me.chkUserAccess.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkUserAccess.ID, Me.chkUserAccess.Text)

            Me.dgLevelRoleMappingList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLevelRoleMappingList.Columns(2).FooterText, Me.dgLevelRoleMappingList.Columns(2).HeaderText)
            Me.dgLevelRoleMappingList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLevelRoleMappingList.Columns(3).FooterText, Me.dgLevelRoleMappingList.Columns(3).HeaderText)
            Me.dgLevelRoleMappingList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLevelRoleMappingList.Columns(4).FooterText, Me.dgLevelRoleMappingList.Columns(4).HeaderText)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))
            Me.btnSaveLevelRoleMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveLevelRoleMapping.ID, Me.btnSaveLevelRoleMapping.Text.Replace("&", ""))
            Me.btnCloseLevelRoleMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseLevelRoleMapping.ID, Me.btnCloseLevelRoleMapping.Text.Replace("&", ""))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Loan Scheme is  compulsory information.Please select Loan Scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Role is  compulsory information.Please select Role.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Level is  compulsory information.Please select Level.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, You cannot delete this mapping of role and level . Reason: This Mapping is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Are you sure you want to delete ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings


End Class
