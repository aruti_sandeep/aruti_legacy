﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgStaffTransferRequest.aspx.vb"
    Inherits="Staff_Transfer_Staff_Transfer_Request_wPgStaffTransferRequest" Title="Staff Transfer Request" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }

        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }


    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblMainHeader" runat="server" Text="Staff Transfer Request"></asp:Label>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <asp:Panel ID="pnlData" runat="server" CssClass="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card profile-detail">
                                    <div class="body">
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                            <li role="presentation" class="active"><a href="#Employee_Detail" aria-controls="Employee_Detail"
                                                role="tab" data-toggle="tab">
                                                <asp:Label ID="LblEmployeeDetails" runat="server" Text="Employee Details"></asp:Label>
                                            </a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="Employee_Detail">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix d--f ai--c jc--c">
                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                                <div class="image-area m-r-10">
                                                                    <%--<uc2:ZoomImage ID="imgZoomEmp" runat="server" ZoomPercentage="250" />--%>
                                                                    <asp:Image ID="imgEmp" runat="server" Width="120" Height="120" Style="border-radius: 50%;" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblCode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblCode" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblEmpName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblEmpName" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblBranch" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblDeptGroup" runat="server" Text="Department Group" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblDeptGroup" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="divider">
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="display: table">
                                                                        <asp:Label ID="LblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblDepartment" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblSectionGroup" runat="server" Text="Section Group" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblSectionGroup" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblSection" runat="server" Text="Section" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblSection" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblUnitGroup" runat="server" Text="Unit Group" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblUnitGroup" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="divider">
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblUnit" runat="server" Text="Unit" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblUnit" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblTeam" runat="server" Text="Team" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblTeam" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblJobGroup" runat="server" Text="Job Group" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblJobGroup" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblJob" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="divider">
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblClassGroup" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblClass" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblGradeGroup" runat="server" Text="Grade Group" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblGradeGroup" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblGrade" runat="server" Text="Grade" CssClass="form-label"></asp:Label>
                                                                        <asp:Label ID="objlblGrade" runat="server" Text="-"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Ul1">
                                                            <li role="presentation" class="active"><a href="#Transfer_Detail" aria-controls="Transfer_Detail"
                                                                role="tab" data-toggle="tab">
                                                                <asp:Label ID="LblTransferDetails" runat="server" Text="Transfer Details"></asp:Label>
                                                            </a></li>
                                                        </ul>
                                                        <div class="tab-content" style="overflow: hidden">
                                                            <%-- <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divBranch">
                                                                            <asp:Label ID="LblNewBranch" runat="server" Text="Branch" CssClass="form-label" Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewBranch" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divDeptGroup">
                                                                            <asp:Label ID="LblNewDeptGroup" runat="server" Text="Department Group" CssClass="form-label"
                                                                                Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewDeptGroup" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divDept">
                                                                            <asp:Label ID="LblNewDepartment" runat="server" Text="Department" CssClass="form-label"
                                                                                Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewDepartment" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divSectionGroup">
                                                                            <asp:Label ID="LblNewSectionGroup" runat="server" Text="Section Group" CssClass="form-label"
                                                                                Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewSectionGroup" runat="server"
                                                                                    Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divSection">
                                                                            <asp:Label ID="LblNewSection" runat="server" Text="Section" CssClass="form-label"
                                                                                Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewSection" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divUnitGrp">
                                                                            <asp:Label ID="LblNewUnitGroup" runat="server" Text="Unit Group" CssClass="form-label"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewUnitGroup" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divUnit">
                                                                            <asp:Label ID="LblNewUnit" runat="server" Text="Unit" CssClass="form-label" Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewUnit" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divTeam">
                                                                            <asp:Label ID="LblNewTeam" runat="server" Text="Team" CssClass="form-label" Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewTeam" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divJobGroup">
                                                                            <asp:Label ID="LblNewJobGroup" runat="server" Text="Job Group" CssClass="form-label"
                                                                                Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewJobGroup" runat="server" Visible="false" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divJob">
                                                                            <asp:Label ID="LblNewJob" runat="server" Text="Job" CssClass="form-label" Visible="false"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewJob" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divClassGrp">
                                                                            <asp:Label ID="LblNewClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewClassGroup" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divClass">
                                                                            <asp:Label ID="LblNewClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewClass" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                            <asp:Label ID="LblReason" runat="server" Text="Reason" CssClass="form-label"></asp:Label>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList data-live-search="true" ID="cboNewReason" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        </div>
                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>--%>
                                                            <div class="row clearfix" style="display: flow-root">
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divBranch">
                                                                    <asp:Label ID="LblNewBranch" runat="server" Text="Branch" CssClass="form-label" Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVBranch" runat="server" ControlToValidate="cboNewBranch"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewBranch" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divDeptGroup">
                                                                    <asp:Label ID="LblNewDeptGroup" runat="server" Text="Department Group" CssClass="form-label"
                                                                        Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVDepartmentGrp" runat="server" ControlToValidate="cboNewDeptGroup"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewDeptGroup" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divDept">
                                                                    <asp:Label ID="LblNewDepartment" runat="server" Text="Department" CssClass="form-label"
                                                                        Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVDepartment" runat="server" ControlToValidate="cboNewDepartment"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewDepartment" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divSectionGroup">
                                                                    <asp:Label ID="LblNewSectionGroup" runat="server" Text="Section Group" CssClass="form-label"
                                                                        Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVSectionGrp" runat="server" ControlToValidate="cboNewSectionGroup"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewSectionGroup" runat="server"
                                                                            Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divSection">
                                                                    <asp:Label ID="LblNewSection" runat="server" Text="Section" CssClass="form-label"
                                                                        Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVSection" runat="server" ControlToValidate="cboNewSection"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewSection" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divUnitGrp">
                                                                    <asp:Label ID="LblNewUnitGroup" runat="server" Text="Unit Group" CssClass="form-label"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVUnitGrp" runat="server" ControlToValidate="cboNewUnitGroup"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewUnitGroup" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divUnit">
                                                                    <asp:Label ID="LblNewUnit" runat="server" Text="Unit" CssClass="form-label" Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVUnit" runat="server" ControlToValidate="cboNewUnit"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewUnit" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divTeam">
                                                                    <asp:Label ID="LblNewTeam" runat="server" Text="Team" CssClass="form-label" Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVTeam" runat="server" ControlToValidate="cboNewTeam"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewTeam" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divJobGroup">
                                                                    <asp:Label ID="LblNewJobGroup" runat="server" Text="Job Group" CssClass="form-label"
                                                                        Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVJobGrp" runat="server" ControlToValidate="cboNewJobGroup"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewJobGroup" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divJob">
                                                                    <asp:Label ID="LblNewJob" runat="server" Text="Job" CssClass="form-label" Visible="false"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVJob" runat="server" ControlToValidate="cboNewJob"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewJob" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divClassGrp">
                                                                    <asp:Label ID="LblNewClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVClassGrp" runat="server" ControlToValidate="cboNewClassGroup"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewClassGroup" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" runat="server" id="divClass">
                                                                    <asp:Label ID="LblNewClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RFVClass" runat="server" ControlToValidate="cboNewClass"
                                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                        Font-Bold="true" ValidationGroup=""></asp:RequiredFieldValidator>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList data-live-search="true" ID="cboNewClass" runat="server" Visible="false" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <asp:Panel ID="pnlAttachment" runat="server">
                                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card">
                                                                            <div class="header">
                                                                                <h2>
                                                                                    <asp:Label ID="lblAttachmentInfo" runat="server" Text="Attachment Info" CssClass="form-label"></asp:Label>
                                                                                </h2>
                                                                            </div>
                                                                            <div class="body">
                                                                                <asp:Panel ID="pnlScanAttachment" runat="server">
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                                            <asp:Label ID="lblDocumentType" runat="server" Text="Document Type(.PDF and Images only)"
                                                                                                CssClass="form-label"></asp:Label>
                                                                                            <div class="form-group">
                                                                                                <asp:DropDownList ID="cboDocumentType" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-25">
                                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-20  ">
                                                                                                <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                                                    <div id="fileuploader">
                                                                                                        <input type="button" id="btnAddFile" runat="server" onclick="return IsValidAttach()"
                                                                                                            value="Browse" class="btn btn-primary" />
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                                                                                <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row clearfix">
                                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 250px;">
                                                                                            <asp:DataGrid ID="dgStaffTransferAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                                                Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                                                                HeaderStyle-Font-Bold="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn FooterText="objcohDelete">
                                                                                                        <ItemTemplate>
                                                                                                            <span class="gridiconbc">
                                                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                                            </span>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                                    <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                                                    <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                                                        ItemStyle-Font-Size="22px">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                                                            </asp:LinkButton>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                                                        Visible="false" />
                                                                                                    <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                                                        Visible="false" />
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </div>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblReason" runat="server" Text="Reason" CssClass="form-label"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="RFVReason" runat="server" ControlToValidate="cboNewReason"
                                                                            InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                                            Font-Bold="true" ValidationGroup="RequireFields"></asp:RequiredFieldValidator>
                                                                        <div class="form-group">
                                                                            <asp:DropDownList data-live-search="true" ID="cboNewReason" runat="server" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="RFVRemark" runat="server" ControlToValidate="txtRemark"
                                                                            InitialValue="" ErrorMessage="* required" Display="Dynamic" ForeColor="Red" Font-Bold="true"
                                                                            ValidationGroup="RequireFields"></asp:RequiredFieldValidator>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                                    Rows="1"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                                            ValidationGroup="RequireFields" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <uc3:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgStaffTransferAttachment" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });

        function ImageLoad() {
            debugger;
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgStaffTransferRequest.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnAddAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });

            }
        }

        $("body").on("click", 'input[name*=myfile]', function() {
            return IsValidAttach();
        });
                
   
    </script>

</asp:Content>
