﻿Option Strict On
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.IO

#End Region

Partial Class Staff_Transfer_Staff_Transfer_Request_wPgStaffTransferRequest
    Inherits Basepage

#Region " Private Variable(s) "
    Private Shared ReadOnly mstrModuleName As String = "frmStaffTransferRequest"
    Private mintTransferRequestId As Integer = 0
    Private DisplayMessage As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    Private mdtTransferRequestApplicationDocument As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    Private mintDeleteAttRowIndex As Integer = -1
    Private mdctStaffAllocations As New Dictionary(Of Integer, Integer)
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillAllocationCombo(ByVal objEmployee As clsEmployee_Master)
        Dim dsCombos As New DataSet
        Dim objCommon As New clsCommon_Master
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSectionGrp As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objGradeGrp As New clsGradeGroup
        Dim objGrade As New clsGrade
        Try

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.STAFF_TRANSFER, True, "Reason")
            With cboNewReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Reason")
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objStation.getComboList("Station", True)
            If objEmployee._Stationunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("stationunkid") = objEmployee._Stationunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            If objEmployee._Deptgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("deptgroupunkid") = objEmployee._Deptgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewDeptGroup
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objDepartment.getComboList("Department", True)
            If objEmployee._Departmentunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("departmentunkid") = objEmployee._Departmentunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objSectionGrp.getComboList("SectionGrp", True)
            If objEmployee._Sectiongroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("sectiongroupunkid") = objEmployee._Sectiongroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewSectionGroup
                .DataValueField = "sectiongroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objSection.getComboList("Section", True)
            If objEmployee._Sectionunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("sectionunkid") = objEmployee._Sectionunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewSection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objUnitGroup.getComboList("UnitGrp", True)
            If objEmployee._Unitgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("unitgroupunkid") = objEmployee._Unitgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewUnitGroup
                .DataValueField = "unitgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objUnit.getComboList("Unit", True)
            If objEmployee._Unitunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("unitunkid") = objEmployee._Unitunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewUnit
                .DataValueField = "unitunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objTeam.getComboList("Team", True)
            If objEmployee._Teamunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("teamunkid") = objEmployee._Teamunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewTeam
                .DataValueField = "teamunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objJobGrp.getComboList("JobGrp", True)
            If objEmployee._Jobgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("jobgroupunkid") = objEmployee._Jobgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewJobGroup
                .DataValueField = "jobgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objJob.getComboList("Job", True)
            If objEmployee._Jobunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("jobunkid") = objEmployee._Jobunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            If objEmployee._Classgroupunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("classgroupunkid") = objEmployee._Classgroupunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewClassGroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

            dsCombos = objClass.getComboList("Class", True)
            If objEmployee._Classunkid > 0 Then
                Dim drRow = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("classesunkid") = objEmployee._Classunkid)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    dsCombos.Tables(0).Rows.Remove(drRow(0))
                    dsCombos.AcceptChanges()
                End If
                drRow = Nothing
            End If
            With cboNewClass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            dsCombos = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Dim mstrStaffTransferRequestAllocations As String = ""
        Try

            If Session("StaffTransferRequestAllocations") Is Nothing OrElse Session("StaffTransferRequestAllocations").ToString().Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Staff Transfer Allocation(s) are compulsory information.Please configure it from configuration settings screen."), Me)
                Exit Sub
            End If

            mstrStaffTransferRequestAllocations = Session("StaffTransferRequestAllocations").ToString()
            Dim ar() As String = mstrStaffTransferRequestAllocations.Split(CChar("|"))

            If ar IsNot Nothing AndAlso ar.Length > 0 Then

                divBranch.Visible = False
                divDeptGroup.Visible = False
                divDept.Visible = False
                divSectionGroup.Visible = False
                divSection.Visible = False
                divUnitGrp.Visible = False
                divUnit.Visible = False
                divTeam.Visible = False
                divJobGroup.Visible = False
                divJob.Visible = False
                divClassGrp.Visible = False
                divClass.Visible = False

                For i As Integer = 0 To ar.Length - 1

                    Select Case CInt(ar(i))

                        Case enAllocation.BRANCH
                            LblNewBranch.Visible = True
                            cboNewBranch.Visible = True
                            divBranch.Visible = True
                            RFVBranch.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.BRANCH, 0)

                        Case enAllocation.DEPARTMENT_GROUP
                            LblNewDeptGroup.Visible = True
                            cboNewDeptGroup.Visible = True
                            divDeptGroup.Visible = True
                            RFVDepartmentGrp.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.DEPARTMENT_GROUP, 0)

                        Case enAllocation.DEPARTMENT
                            LblNewDepartment.Visible = True
                            cboNewDepartment.Visible = True
                            divDept.Visible = True
                            RFVDepartment.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.DEPARTMENT, 0)

                        Case enAllocation.SECTION_GROUP
                            LblNewSectionGroup.Visible = True
                            cboNewSectionGroup.Visible = True
                            divSectionGroup.Visible = True
                            RFVSectionGrp.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.SECTION_GROUP, 0)

                        Case enAllocation.SECTION
                            LblNewSection.Visible = True
                            cboNewSection.Visible = True
                            divSection.Visible = True
                            RFVSection.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.SECTION, 0)

                        Case enAllocation.UNIT_GROUP
                            LblNewUnitGroup.Visible = True
                            cboNewUnitGroup.Visible = True
                            divUnitGrp.Visible = True
                            RFVUnitGrp.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.UNIT_GROUP, 0)

                        Case enAllocation.UNIT
                            LblNewUnit.Visible = True
                            cboNewUnit.Visible = True
                            divUnit.Visible = True
                            RFVUnit.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.UNIT, 0)

                        Case enAllocation.TEAM
                            LblNewTeam.Visible = True
                            cboNewTeam.Visible = True
                            divTeam.Visible = True
                            RFVTeam.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.TEAM, 0)

                        Case enAllocation.JOB_GROUP
                            LblNewJobGroup.Visible = True
                            cboNewJobGroup.Visible = True
                            divJobGroup.Visible = True
                            RFVJobGrp.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.JOB_GROUP, 0)

                        Case enAllocation.JOBS
                            LblNewJob.Visible = True
                            cboNewJob.Visible = True
                            divJob.Visible = True
                            RFVJob.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.JOBS, 0)

                        Case enAllocation.CLASS_GROUP
                            LblNewClassGroup.Visible = True
                            cboNewClassGroup.Visible = True
                            divClassGrp.Visible = True
                            RFVClassGrp.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.CLASS_GROUP, 0)

                        Case enAllocation.CLASSES
                            LblNewClass.Visible = True
                            cboNewClass.Visible = True
                            divClass.Visible = True
                            RFVClass.ValidationGroup = "RequireFields"
                            mdctStaffAllocations.Add(enAllocation.CLASSES, 0)

                    End Select

                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Try
            Dim path As String = Server.MapPath("../../images/ChartUser.png")

            Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

                Using m As MemoryStream = New MemoryStream()
                    image.Save(m, image.RawFormat)
                    Dim imageBytes As Byte() = m.ToArray()
                    base64String = Convert.ToBase64String(imageBytes)
                End Using
            End Using
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return base64String
    End Function

    Private Sub SetEmployeeDetails(ByVal xEmployeeId As Integer)
        Dim objEmployee As New clsEmployee_Master
        Dim objBranch As New clsStation
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSectionGroup As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGroup As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGroup As New clsClassGroup
        Dim objClass As New clsClass
        Dim objGradeGroup As New clsGradeGroup
        Dim objGrade As New clsGrade
        Try
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = xEmployeeId

            objlblCode.Text = objEmployee._Employeecode.ToString()
            objlblEmpName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            If objEmployee._Stationunkid > 0 Then
                objBranch._Stationunkid = objEmployee._Stationunkid
                objlblBranch.Text = objBranch._Name.ToString()
            End If

            If objEmployee._Deptgroupunkid > 0 Then
                objDeptGroup._Deptgroupunkid = objEmployee._Deptgroupunkid
                objlblDeptGroup.Text = objDeptGroup._Name.ToString()
            End If

            If objEmployee._Departmentunkid > 0 Then
                objDepartment._Departmentunkid = objEmployee._Departmentunkid
                objlblDepartment.Text = objDepartment._Name.ToString()
            End If

            If objEmployee._Sectiongroupunkid > 0 Then
                objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                objlblSectionGroup.Text = objSectionGroup._Name.ToString()
            End If

            If objEmployee._Sectionunkid > 0 Then
                objSection._Sectionunkid = objEmployee._Sectionunkid
                objlblSection.Text = objSection._Name.ToString()
            End If

            If objEmployee._Unitgroupunkid > 0 Then
                objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                objlblUnitGroup.Text = objUnitGroup._Name.ToString()
            End If

            If objEmployee._Unitunkid > 0 Then
                objUnit._Unitunkid = objEmployee._Unitunkid
                objlblUnit.Text = objUnit._Name.ToString()
            End If

            If objEmployee._Teamunkid > 0 Then
                objTeam._Teamunkid = objEmployee._Teamunkid
                objlblTeam.Text = objTeam._Name.ToString()
            End If

            If objEmployee._Jobgroupunkid > 0 Then
                objJobGroup._Jobgroupunkid = objEmployee._Jobgroupunkid
                objlblJobGroup.Text = objJobGroup._Name.ToString()
            End If

            If objEmployee._Jobunkid > 0 Then
                objJob._Jobunkid = objEmployee._Jobunkid
                objlblJob.Text = objJob._Job_Name.ToString()
            End If

            If objEmployee._Classgroupunkid > 0 Then
                objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                objlblClassGroup.Text = objClassGroup._Name.ToString()
            End If

            If objEmployee._Classunkid > 0 Then
                objClass._Classesunkid = objEmployee._Classunkid
                objlblClass.Text = objClass._Name.ToString()
            End If

            If objEmployee._Gradegroupunkid > 0 Then
                objGradeGroup._Gradegroupunkid = objEmployee._Gradegroupunkid
                objlblGradeGroup.Text = objGradeGroup._Name.ToString()
            End If

            If objEmployee._Gradeunkid > 0 Then
                objGrade._Gradeunkid = objEmployee._Gradeunkid
                objlblGrade.Text = objGrade._Name.ToString()
            End If

            If objEmployee._blnImgInDb Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    If objEmployee._Photo IsNot Nothing Then
                        imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                    Else
                        imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
                    End If
                End If
            End If

            FillAllocationCombo(objEmployee)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objGrade = Nothing
            objGradeGroup = Nothing
            objClass = Nothing
            objClassGroup = Nothing
            objJob = Nothing
            objJobGroup = Nothing
            objTeam = Nothing
            objUnit = Nothing
            objUnitGroup = Nothing
            objSection = Nothing
            objSectionGroup = Nothing
            objDepartment = Nothing
            objDeptGroup = Nothing
            objBranch = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub FillTransferRequestAttachment()
        Dim dtView As DataView
        Try
            If mdtTransferRequestApplicationDocument Is Nothing Then
                dgStaffTransferAttachment.DataSource = Nothing
                dgStaffTransferAttachment.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtTransferRequestApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgStaffTransferAttachment.AutoGenerateColumns = False
            dgStaffTransferAttachment.DataSource = dtView
            dgStaffTransferAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtTransferRequestApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtTransferRequestApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(Session("Employeeunkid"))
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.STAFF_TRANSFER_REQUEST
                dRow("transactionunkid") = -1
                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Now
                dRow("orgfilepath") = strfullpath
                dRow("GUID") = Guid.NewGuid.ToString
                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                dRow("Documentype") = GetMimeType(strfullpath)
                Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try

                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtTransferRequestApplicationDocument.Rows.Add(dRow)
            Call FillTransferRequestAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If CInt(cboNewBranch.SelectedValue) <= 0 AndAlso cboNewBranch.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewBranch.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewBranch.Text, Me)
                cboNewBranch.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewDeptGroup.SelectedValue) <= 0 AndAlso cboNewDeptGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewDeptGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewDeptGroup.Text, Me)
                cboNewDeptGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewDepartment.SelectedValue) <= 0 AndAlso cboNewDepartment.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewDepartment.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewDepartment.Text, Me)
                cboNewDepartment.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewSectionGroup.SelectedValue) <= 0 AndAlso cboNewSectionGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewSectionGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewSectionGroup.Text, Me)
                cboNewSectionGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewSection.SelectedValue) <= 0 AndAlso cboNewSection.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewSection.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewSection.Text, Me)
                cboNewSection.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewUnitGroup.SelectedValue) <= 0 AndAlso cboNewUnitGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewUnitGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewUnitGroup.Text, Me)
                cboNewUnitGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewUnit.SelectedValue) <= 0 AndAlso cboNewUnit.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewUnit.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewUnit.Text, Me)
                cboNewUnit.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewTeam.SelectedValue) <= 0 AndAlso cboNewTeam.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewTeam.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewTeam.Text, Me)
                cboNewTeam.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewJobGroup.SelectedValue) <= 0 AndAlso cboNewJobGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewJobGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewJobGroup.Text, Me)
                cboNewJobGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewJob.SelectedValue) <= 0 AndAlso cboNewJob.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewJob.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewJob.Text, Me)
                cboNewJob.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewClassGroup.SelectedValue) <= 0 AndAlso cboNewClassGroup.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewClassGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewClassGroup.Text, Me)
                cboNewClassGroup.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewClass.SelectedValue) <= 0 AndAlso cboNewClass.Visible = True Then
                DisplayMessage.DisplayMessage(LblNewClass.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "is compulsory information.Please select.") & " " & LblNewClass.Text, Me)
                cboNewClass.Focus()
                mblnFlag = False

            ElseIf CInt(cboNewReason.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 3, "Reason is compulsory information.Please Select Reason."), Me)
                cboNewReason.Focus()
                mblnFlag = False

            ElseIf txtRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 10, "Remark is compulsory information.Please Select Remark."), Me)
                txtRemark.Focus()
                mblnFlag = False

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
            Return mblnFlag
        End Try
        Return mblnFlag
    End Function

    Private Sub GetValue()
        Dim objStaffTransfer As New clstransfer_request_tran
        Try
            If Session("TransferRequestId") IsNot Nothing Then
                mintTransferRequestId = CInt(Session("TransferRequestId"))
            End If
            objStaffTransfer._Transferrequestunkid = mintTransferRequestId
            cboNewBranch.SelectedValue = objStaffTransfer._Stationunkid.ToString()
            cboNewDeptGroup.SelectedValue = objStaffTransfer._Deptgroupunkid.ToString()
            cboNewDepartment.SelectedValue = objStaffTransfer._Departmentunkid.ToString()
            cboNewSectionGroup.SelectedValue = objStaffTransfer._Sectiongroupunkid.ToString()
            cboNewSection.SelectedValue = objStaffTransfer._Sectionunkid.ToString()
            cboNewUnitGroup.SelectedValue = objStaffTransfer._Unitgroupunkid.ToString()
            cboNewUnit.SelectedValue = objStaffTransfer._Unitunkid.ToString()
            cboNewTeam.SelectedValue = objStaffTransfer._Teamunkid.ToString()
            cboNewJobGroup.SelectedValue = objStaffTransfer._Jobgroupunkid.ToString()
            cboNewJob.SelectedValue = objStaffTransfer._Jobunkid.ToString()
            cboNewClassGroup.SelectedValue = objStaffTransfer._Classgroupunkid.ToString()
            cboNewClass.SelectedValue = objStaffTransfer._Classunkid.ToString()
            cboNewReason.SelectedValue = objStaffTransfer._Reasonunkid.ToString()
            txtRemark.Text = objStaffTransfer._Remark


            If mdtTransferRequestApplicationDocument Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                Call objAttchement.GetList(Session("Document_Path").ToString(), "List ", "", CInt(Session("Employeeunkid")), , , , "hrdocuments_tran.transactionunkid = " & mintTransferRequestId, CBool(IIf(mintTransferRequestId > 0, False, True)), , enScanAttactRefId.STAFF_TRANSFER_REQUEST, 0, True, "")
                mdtTransferRequestApplicationDocument = objAttchement._Datatable.Copy()
                objAttchement = Nothing
            End If

            Call FillTransferRequestAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaffTransfer = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objStaffTransfer As clstransfer_request_tran)
        Try
            If mintTransferRequestId > 0 Then
                objStaffTransfer._Transferrequestunkid = mintTransferRequestId
            End If
            objStaffTransfer._Requestdate = Now
            objStaffTransfer._Employeeunkid = CInt(Session("Employeeunkid"))
            objStaffTransfer._Stationunkid = CInt(cboNewBranch.SelectedValue)
            objStaffTransfer._Deptgroupunkid = CInt(cboNewDeptGroup.SelectedValue)
            objStaffTransfer._Departmentunkid = CInt(cboNewDepartment.SelectedValue)
            objStaffTransfer._Sectiongroupunkid = CInt(cboNewSectionGroup.SelectedValue)
            objStaffTransfer._Sectionunkid = CInt(cboNewSection.SelectedValue)
            objStaffTransfer._Unitgroupunkid = CInt(cboNewUnitGroup.SelectedValue)
            objStaffTransfer._Unitunkid = CInt(cboNewUnit.SelectedValue)
            objStaffTransfer._Teamunkid = CInt(cboNewTeam.SelectedValue)
            objStaffTransfer._Jobgroupunkid = CInt(cboNewJobGroup.SelectedValue)
            objStaffTransfer._Jobunkid = CInt(cboNewJob.SelectedValue)
            objStaffTransfer._Classgroupunkid = CInt(cboNewClassGroup.SelectedValue)
            objStaffTransfer._Classunkid = CInt(cboNewClass.SelectedValue)
            objStaffTransfer._Reasonunkid = CInt(cboNewReason.SelectedValue)
            objStaffTransfer._Remark = txtRemark.Text.Trim
            objStaffTransfer._Statusunkid = CInt(clstransfer_request_tran.enTransferRequestStatus.Pending)
            objStaffTransfer._Userunkid = -1
            objStaffTransfer._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            objStaffTransfer._Isvoid = False
            objStaffTransfer._WebFormName = mstrModuleName
            objStaffTransfer._WebClientIP = CStr(Session("IP_ADD"))
            objStaffTransfer._WebHostName = CStr(Session("HOST_NAME"))
            objStaffTransfer._IsWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SaveAttachment(ByVal mdtAttachments As DataTable) As Boolean
        Try
            Dim strFileName As String = ""
            Dim strError As String = ""

            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.STAFF_TRANSFER_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

            For Each dRow As DataRow In mdtAttachments.Rows

                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                    If File.Exists(CStr(dRow("orgfilepath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath

                        dRow.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Configuration Path does not Exist."), Me)
                        Return False
                    End If

                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Configuration Path does not Exist."), Me)
                            Return False
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Configuration Path does not Exist."), Me)
                        Return False
                    End If

                End If

            Next

            If mdsDoc IsNot Nothing Then mdsDoc.Clear()
            mdsDoc = Nothing

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveAttachment", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function ValidApprovers() As Boolean
        Dim objLevelRoleMapping As New clsstapproverlevel_role_mapping
        Dim mblnFlag As Boolean = True
        Try
            Dim dtTable As DataTable = objLevelRoleMapping.GetNextApprovers(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), enUserPriviledge.AllowToApproveStaffTransferApproval, Now.Date, 1, CInt(Session("Employeeunkid")), True, Nothing, "", Nothing)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                If CInt(cboNewBranch.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.BRANCH) Then
                        mdctStaffAllocations(enAllocation.BRANCH) = CInt(cboNewBranch.SelectedValue)
                    End If
                End If

                If CInt(cboNewDeptGroup.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.DEPARTMENT_GROUP) Then
                        mdctStaffAllocations(enAllocation.DEPARTMENT_GROUP) = CInt(cboNewDeptGroup.SelectedValue)
                    End If
                End If

                If CInt(cboNewDepartment.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.DEPARTMENT) Then
                        mdctStaffAllocations(enAllocation.DEPARTMENT) = CInt(cboNewDepartment.SelectedValue)
                    End If
                End If

                If CInt(cboNewSectionGroup.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.SECTION_GROUP) Then
                        mdctStaffAllocations(enAllocation.SECTION_GROUP) = CInt(cboNewSectionGroup.SelectedValue)
                    End If
                End If

                If CInt(cboNewSection.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.SECTION) Then
                        mdctStaffAllocations(enAllocation.SECTION) = CInt(cboNewSection.SelectedValue)
                    End If
                End If

                If CInt(cboNewUnitGroup.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.UNIT_GROUP) Then
                        mdctStaffAllocations(enAllocation.UNIT_GROUP) = CInt(cboNewUnitGroup.SelectedValue)
                    End If
                End If

                If CInt(cboNewUnit.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.UNIT) Then
                        mdctStaffAllocations(enAllocation.UNIT) = CInt(cboNewUnit.SelectedValue)
                    End If
                End If

                If CInt(cboNewJobGroup.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.JOB_GROUP) Then
                        mdctStaffAllocations(enAllocation.JOB_GROUP) = CInt(cboNewJobGroup.SelectedValue)
                    End If
                End If

                If CInt(cboNewJob.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.JOBS) Then
                        mdctStaffAllocations(enAllocation.JOBS) = CInt(cboNewJob.SelectedValue)
                    End If
                End If

                If CInt(cboNewClassGroup.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.CLASS_GROUP) Then
                        mdctStaffAllocations(enAllocation.CLASS_GROUP) = CInt(cboNewClassGroup.SelectedValue)
                    End If
                End If

                If CInt(cboNewClass.SelectedValue) > 0 Then
                    If mdctStaffAllocations.ContainsKey(enAllocation.CLASSES) Then
                        mdctStaffAllocations(enAllocation.CLASSES) = CInt(cboNewClass.SelectedValue)
                    End If
                End If

                Dim dtNoUserAccess As DataTable = objLevelRoleMapping.GetNextApprovers(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), enUserPriviledge.AllowToApproveStaffTransferApproval, Now.Date, 1, CInt(Session("Employeeunkid")), False, Nothing, "", mdctStaffAllocations)

                If dtNoUserAccess IsNot Nothing AndAlso dtNoUserAccess.Rows.Count > 0 Then
                    dtTable.Merge(dtNoUserAccess)
                End If
            End If
            objLevelRoleMapping = Nothing

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "There is no approver(s) to approve this staff transfer request application.Please assign approver(s) accordingly."), Me)
                mblnFlag = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
        Finally
            objLevelRoleMapping = Nothing
        End Try
        Return mblnFlag
    End Function

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If IsPostBack = False AndAlso Request.QueryString("uploadimage") Is Nothing Then
                SetControlCaptions()
                SetLanguage()
                SetMessages()
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    SetEmployeeDetails(CInt(Session("Employeeunkid")))
                End If
                FillDocumentTypeCombo()
                SetVisibility()
                GetValue()
            Else
                mintTransferRequestId = CInt(ViewState("TransferRequestId"))
                mdtTransferRequestApplicationDocument = CType(ViewState("TransferRequestApplicationDocument"), DataTable)
                mintDeleteAttRowIndex = CInt(ViewState("DeleteAttRowIndex"))
                mdctStaffAllocations = CType(ViewState("StaffAllocations"), Dictionary(Of Integer, Integer))
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("TransferRequestId") = mintTransferRequestId
            ViewState("TransferRequestApplicationDocument") = mdtTransferRequestApplicationDocument
            ViewState("DeleteAttRowIndex") = mintDeleteAttRowIndex
            ViewState("StaffAllocations") = mdctStaffAllocations
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub dgStaffTransferAttachment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgStaffTransferAttachment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtTransferRequestApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtTransferRequestApplicationDocument.Select("GUID = '" & e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        mintDeleteAttRowIndex = mdtTransferRequestApplicationDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(getColumnId_Datagrid(dgStaffTransferAttachment, "colhName", False, True)).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnAddAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If
                AddDocumentAttachment(f, f.FullName)
                Call FillTransferRequestAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim objStaffTransfer As New clstransfer_request_tran
        Try
            If Validation() = False Then Exit Sub

            If ValidApprovers() = False Then Exit Sub

            SetValue(objStaffTransfer)

            If SaveAttachment(mdtTransferRequestApplicationDocument) = False Then Exit Sub

            If mdtTransferRequestApplicationDocument Is Nothing OrElse mdtTransferRequestApplicationDocument.Select("AUD <> 'D'").Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
                Exit Sub
            End If

            Dim mblnFlag As Boolean = False
            If mintTransferRequestId > 0 Then
                mblnFlag = objStaffTransfer.Update(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), Nothing, mdtTransferRequestApplicationDocument)
            Else
                mblnFlag = objStaffTransfer.Insert(CInt(Session("CompanyUnkId")), Session("Database_Name").ToString(), CInt(Session("Fin_year")), Nothing, mdtTransferRequestApplicationDocument)
            End If

            If mblnFlag = False And objStaffTransfer._Message <> "" Then
                DisplayMessage.DisplayMessage(objStaffTransfer._Message, Me)
                Exit Sub
            End If

            If mblnFlag Then
                mintTransferRequestId = 0
                Session("TransferRequestId") = Nothing

                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                objStaffTransfer.Send_NotificationEmployee(CInt(Session("Employeeunkid")), Now, CInt(Session("CompanyUnkId")), clstransfer_request_tran.enTransferRequestStatus.Pending, "", enLoginMode _
                                                                                , intLoginByEmployeeId, intUserId)

                objStaffTransfer.Send_NotificationApprover(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), objStaffTransfer._Requestdate.Date, clstransfer_request_tran.enTransferRequestStatus.Pending _
                                                                              , CStr(Session("UserAccessModeSetting")), CInt(Session("Employeeunkid")), objStaffTransfer._Transferrequestunkid.ToString(), Session("ArutiSelfServiceURL").ToString() _
                                                                              , -1, txtRemark.Text.Trim, enLoginMode, intLoginByEmployeeId, intUserId)

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Transfer Request application applied successfully."), Me, Session("rootpath").ToString() & "Staff_Transfer/Staff_Transfer_Request/wPgStaffTransferRequestList.aspx")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "Staff_Transfer/Staff_Transfer_Request/wPgStaffTransferRequestList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            mintDeleteAttRowIndex = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteAttRowIndex >= 0 Then
                mdtTransferRequestApplicationDocument.Rows(mintDeleteAttRowIndex)("AUD") = "D"
                mdtTransferRequestApplicationDocument.AcceptChanges()
                Call FillTransferRequestAttachment()
                mintDeleteAttRowIndex = -1
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblMainHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployeeDetails.ID, Me.LblEmployeeDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTransferDetails.ID, Me.LblTransferDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAttachmentInfo.ID, Me.lblAttachmentInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDocumentType.ID, Me.lblDocumentType.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblReason.ID, Me.LblReason.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRemark.ID, Me.LblRemark.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAddAttachment.ID, Me.btnAddAttachment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSubmit.ID, Me.btnSubmit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblMainHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblMainHeader.Text)

            Me.LblEmployeeDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblEmployeeDetails.ID, LblEmployeeDetails.Text)
            Me.LblTransferDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblTransferDetails.ID, LblTransferDetails.Text)
            Me.lblAttachmentInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAttachmentInfo.ID, lblAttachmentInfo.Text)
            Me.lblDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDocumentType.ID, lblDocumentType.Text)

            Me.LblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblBranch.ID, Me.LblBranch.Text)
            Me.LblNewBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblBranch.ID, Me.LblBranch.Text)
            Me.LblDeptGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDeptGroup.ID, Me.LblDeptGroup.Text)
            Me.LblNewDeptGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDeptGroup.ID, Me.LblDeptGroup.Text)
            Me.LblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDepartment.ID, Me.LblDepartment.Text)
            Me.LblNewDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblDepartment.ID, Me.LblDepartment.Text)
            Me.LblSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSectionGroup.ID, Me.LblSectionGroup.Text)
            Me.LblNewSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSectionGroup.ID, Me.LblSectionGroup.Text)
            Me.LblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSection.ID, Me.LblSection.Text)
            Me.LblNewSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblSection.ID, Me.LblSection.Text)
            Me.LblUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnitGroup.ID, Me.LblUnitGroup.Text)
            Me.LblNewUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnitGroup.ID, Me.LblUnitGroup.Text)
            Me.LblUnit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnit.ID, Me.LblUnit.Text)
            Me.LblNewUnit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblUnit.ID, Me.LblUnit.Text)
            Me.LblTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblTeam.ID, Me.LblTeam.Text)
            Me.LblNewTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblTeam.ID, Me.LblTeam.Text)
            Me.LblJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJobGroup.ID, Me.LblJobGroup.Text)
            Me.LblNewJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJobGroup.ID, Me.LblJobGroup.Text)
            Me.LblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJob.ID, Me.LblJob.Text)
            Me.LblNewJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblJob.ID, Me.LblJob.Text)
            Me.LblClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClassGroup.ID, Me.LblClassGroup.Text)
            Me.LblNewClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClassGroup.ID, Me.LblClassGroup.Text)
            Me.LblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClass.ID, Me.LblClass.Text)
            Me.LblNewClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblClass.ID, Me.LblClass.Text)
            Me.LblGradeGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblGradeGroup.ID, Me.LblGradeGroup.Text)
            Me.LblGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), Me.LblGrade.ID, Me.LblGrade.Text)

            Me.LblReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReason.ID, Me.LblReason.Text)
            Me.LblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRemark.ID, Me.LblRemark.Text)

            Me.btnAddAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddAttachment.ID, Me.btnAddAttachment.Text).Replace("&", "")
            Me.btnSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Staff Transfer Allocation(s) are compulsory information.Please configure it from configuration settings screen.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "is compulsory information.Please select.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Reason is compulsory information.Please Select Reason.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Transfer Request application applied successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Please select PDF or Image file.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Configuration Path does not Exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Remark is compulsory information.Please Select Remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. ")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "There is no approver(s) to approve this staff transfer request application.Please assign approver(s) accordingly.")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


End Class
