﻿Option Strict On
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.IO

#End Region

Partial Class Staff_Transfer_Staff_Transfer_Request_wPgStaffTransferRequestList
    Inherits Basepage

#Region " Private Variable(s) "
    Private Shared ReadOnly mstrModuleName As String = "frmStaffTransferRequestList"
    Private mintTransferRequestId As Integer = 0
    Private DisplayMessage As New CommonCodes
    Private clsuser As New User
    Private mstrDocumentTypeIDs As String = String.Empty
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objTransfer As New clstransfer_request_tran
        Dim dsCombo As DataSet = Nothing
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With


            dsCombo = objTransfer.GetTransferRequest_Status("List", True, False)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTransfer = Nothing
            objEMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearch As String = ""
        Dim objTransfer As New clstransfer_request_tran
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND sttransfer_request_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND CONVERT(CHAR(8),sttransfer_request_tran.requestdate,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND CONVERT(CHAR(8),sttransfer_request_tran.requestdate,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND sttransfer_request_tran.statusunkid = " & CInt(cboStatus.SelectedValue)
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If

            Dim dsList As DataSet = objTransfer.GetList("List", True, Nothing, strSearch)

            dgvStaffTransfer.AutoGenerateColumns = False
            dgvStaffTransfer.DataSource = dsList.Tables(0)
            dgvStaffTransfer.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTransfer = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Dim mstrStaffTransferRequestAllocations As String = ""
        Try

            If Session("StaffTransferRequestAllocations") Is Nothing OrElse Session("StaffTransferRequestAllocations").ToString().Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Staff Transfer Allocation(s) are compulsory information.Please configure it from configuration settings screen."), Me)
                Exit Sub
            End If

            mstrStaffTransferRequestAllocations = Session("StaffTransferRequestAllocations").ToString()
            Dim ar() As String = mstrStaffTransferRequestAllocations.Split(CChar("|"))

            If ar IsNot Nothing AndAlso ar.Length > 0 Then

                For i As Integer = 0 To ar.Length - 1

                    Select Case CInt(ar(i))

                        Case enAllocation.BRANCH
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhBranch", False, True)).Visible = True

                        Case enAllocation.DEPARTMENT_GROUP
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhDeptGrp", False, True)).Visible = True

                        Case enAllocation.DEPARTMENT
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhDepartment", False, True)).Visible = True

                        Case enAllocation.SECTION_GROUP
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhSectionGroup", False, True)).Visible = True

                        Case enAllocation.SECTION
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhSection", False, True)).Visible = True

                        Case enAllocation.UNIT_GROUP
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhUnitGroup", False, True)).Visible = True

                        Case enAllocation.UNIT
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhUnit", False, True)).Visible = True

                        Case enAllocation.TEAM
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhTeam", False, True)).Visible = True

                        Case enAllocation.JOB_GROUP
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhJobGroup", False, True)).Visible = True

                        Case enAllocation.JOBS
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhJob", False, True)).Visible = True

                        Case enAllocation.CLASS_GROUP
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhClassGroup", False, True)).Visible = True

                        Case enAllocation.CLASSES
                            dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhClass", False, True)).Visible = True

                    End Select

                Next

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If IsPostBack = False Then
                SetControlCaptions()
                SetLanguage()
                SetMessages()
                SetVisibility()
                FillCombo()
                dtpFromDate.SetDate = Now.Date
                dtpToDate.SetDate = Now.Date
                If dgvStaffTransfer.Items.Count <= 0 Then
                    dgvStaffTransfer.DataSource = New List(Of String)
                    dgvStaffTransfer.DataBind()
                End If
            Else
                mintTransferRequestId = CInt(ViewState("TransferRequestId"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("TransferRequestId") = mintTransferRequestId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Datagrid Events "

    Protected Sub dgvStaffTransfer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvStaffTransfer.ItemCommand
        Try

            If CInt(dgvStaffTransfer.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvStaffTransfer, "objColhStatusunkid", False, True)).Text) <> clstransfer_request_tran.enTransferRequestStatus.Pending Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "You cannot Edit this transfer request application. Reason: This application is already") & " " & dgvStaffTransfer.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvStaffTransfer, "colhstatus", False, True)).Text & " .", Me)
                Exit Sub
            End If

            Dim objApproval As New clssttransfer_approval_Tran
            If CBool(objApproval.IsPendingTraferRequestApplication(CInt(dgvStaffTransfer.DataKeys(e.Item.ItemIndex)), False)) = False Then
                objApproval = Nothing
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You cannot Edit this transfer request application. Reason: It is already in approval process."), Me)
                Exit Sub
            End If
            objApproval = Nothing

            If e.CommandName.ToUpper = "EDIT" Then

                Session("TransferRequestId") = CInt(dgvStaffTransfer.DataKeys(e.Item.ItemIndex))

                Response.Redirect(Session("rootpath").ToString() & "Staff_Transfer/Staff_Transfer_Request/wPgStaffTransferRequest.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then

                popDeleteReason.Reason = ""

                If CInt(dgvStaffTransfer.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvStaffTransfer, "objColhStatusunkid", False, True)).Text) = clstransfer_request_tran.enTransferRequestStatus.Pending Then
                    mintTransferRequestId = CInt(dgvStaffTransfer.DataKeys(e.Item.ItemIndex))
                    popDeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Are you sure you want to delete this Transfer Request Application?")
                    popDeleteReason.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvStaffTransfer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvStaffTransfer.ItemDataBound
        Try
            If e.Item.ItemIndex < 0 Then Exit Sub
            e.Item.Cells(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).Text).ToShortDateString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "Staff_Transfer/Staff_Transfer_Request/wPgStaffTransferRequest.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dtpFromDate.SetDate = Now.Date
            dtpToDate.SetDate = Now.Date
            cboStatus.SelectedValue = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonYes_Click
        Dim objTransfer As New clstransfer_request_tran
        Try
            objTransfer._Isvoid = True
            objTransfer._Voidreason = popDeleteReason.Reason
            objTransfer._Voiddatetime = Now
            objTransfer._Voiduserunkid = -1
            objTransfer._Userunkid = -1
            objTransfer._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            objTransfer._WebFormName = mstrModuleName
            objTransfer._WebClientIP = CStr(Session("IP_ADD"))
            objTransfer._WebHostName = CStr(Session("HOST_NAME"))
            objTransfer._IsWeb = True

            Dim objScanAttach As New clsScan_Attach_Documents

            'Pinkal (18-Aug-2023) -- Start
            '(A1X -1189) St. Judes - Automatic download of attendance data from Anviz device. 
            'Dim dsAttachment As DataSet = objScanAttach.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , "ISNULL(hrdocuments_tran.transactionunkid,-1) = '" & CInt(mintTransferRequestId) & "'")
            Dim dsAttachment As DataSet = objScanAttach.GetList(Session("Document_Path").ToString(), "List", "", CInt(cboEmployee.SelectedValue), , , , "ISNULL(hrdocuments_tran.transactionunkid,-1) = '" & CInt(mintTransferRequestId) & "'")
            'Pinkal (18-Aug-2023) -- End
            objScanAttach = Nothing

            If objTransfer.Delete(mintTransferRequestId, Nothing, dsAttachment.Tables(0)) = False Then
                DisplayMessage.DisplayMessage(objTransfer._Message, Me)
            End If
            mintTransferRequestId = 0
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTransfer = Nothing
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblDetialHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployee.ID, Me.LblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFromDate.ID, Me.LblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblToDate.ID, Me.LblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblStatus.ID, Me.LblStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnNew.ID, Me.btnNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhEmployee", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhEmployee", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhReason", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhReason", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRemark", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRemark", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhstatus", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhstatus", False, True)).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblDetialHeader.Text)

            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblEmployee.ID, LblEmployee.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblFromDate.ID, LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblToDate.ID, LblToDate.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblStatus.ID, LblStatus.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhEmployee", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhEmployee", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhEmployee", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRequestDate", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhReason", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhReason", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhReason", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRemark", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRemark", False, True)).FooterText, Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhRemark", False, True)).HeaderText)

            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhBranch", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblBranch", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhBranch", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhDeptGrp", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblDeptGroup", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhDeptGrp", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhDepartment", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblDepartment", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhDepartment", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhSectionGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblSectionGroup", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhSectionGroup", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhSection", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblSection", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhSection", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhUnitGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblUnitGroup", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhUnitGroup", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhUnit", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblUnit", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhUnit", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhTeam", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblTeam", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhTeam", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhJobGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblJobGroup", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhJobGroup", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhJob", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblJob", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhJob", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhClassGroup", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblClassGroup", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhClassGroup", False, True)).HeaderText)
            Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhClass", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmEmployeeMaster", CInt(HttpContext.Current.Session("LangId")), "LblClass", Me.dgvStaffTransfer.Columns(getColumnId_Datagrid(dgvStaffTransfer, "colhClass", False, True)).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "You cannot Edit this transfer request application. Reason: This application is already")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You cannot Edit this transfer request application. Reason: It is already in approval process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Are you sure you want to delete this Transfer Request Application?")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region



End Class
