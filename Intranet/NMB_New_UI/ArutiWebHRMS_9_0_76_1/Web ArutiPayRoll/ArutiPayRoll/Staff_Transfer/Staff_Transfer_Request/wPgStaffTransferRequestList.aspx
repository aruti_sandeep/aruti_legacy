<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgStaffTransferRequestList.aspx.vb"
    Inherits="Staff_Transfer_Staff_Transfer_Request_wPgStaffTransferRequestList" Title="Staff Transfer Request List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Staff Transfer Request List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="False" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="False" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="dgvStaffTransfer" runat="server" AutoGenerateColumns="False" DataKeyField = "transferrequestunkid"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="brnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit">
                                                                     <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                           <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Employee" HeaderText="Employee" ReadOnly="true"
                                                        FooterText="colhEmployee" />
                                                    <asp:BoundColumn DataField="requestdate" HeaderText="Request Date" ReadOnly="true"
                                                        FooterText="colhRequestDate" />
                                                    <asp:BoundColumn DataField="Branch" HeaderText="Branch" ReadOnly="true" FooterText="colhBranch" Visible="false" />
                                                    <asp:BoundColumn DataField="DeptGrp" HeaderText="Department Group" ReadOnly="true" FooterText="colhDeptGrp" Visible="false" />
                                                    <asp:BoundColumn DataField="Department" HeaderText="Department" ReadOnly="true" FooterText="colhDepartment" Visible="false" />
                                                    <asp:BoundColumn DataField="SectionGrp" HeaderText="Section Group" ReadOnly="true" FooterText="colhSectionGroup" Visible="false" />
                                                    <asp:BoundColumn DataField="Section" HeaderText="Section" ReadOnly="true" FooterText="colhSection" Visible="false" />
                                                    <asp:BoundColumn DataField="UnitGroup" HeaderText="Unit Group" ReadOnly="true" FooterText="colhUnitGroup" Visible="false" />
                                                    <asp:BoundColumn DataField="Unit" HeaderText="Unit" ReadOnly="true" FooterText="colhUnit" Visible="false" />
                                                    <asp:BoundColumn DataField="Team" HeaderText="Team" ReadOnly="true" FooterText="colhTeam" Visible="false" />
                                                    <asp:BoundColumn DataField="JobGrp" HeaderText="Job Group" ReadOnly="true" FooterText="colhJobGroup" Visible="false" />
                                                    <asp:BoundColumn DataField="Job" HeaderText="Job" ReadOnly="true" FooterText="colhJob" Visible="false" />
                                                    <asp:BoundColumn DataField="ClassGrp" HeaderText="Class Group" ReadOnly="true" FooterText="colhClassGroup" Visible="false" />
                                                    <asp:BoundColumn DataField="Class" HeaderText="Class" ReadOnly="true" FooterText="colhClass" Visible="false"/>
                                                    <asp:BoundColumn DataField="Reason" HeaderText="Reason" ReadOnly="true" FooterText="colhReason" />
                                                    <asp:BoundColumn DataField="Remark" HeaderText="Remark" ReadOnly="true" FooterText="colhRemark" />
                                                    <asp:BoundColumn DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="colhstatus" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true" FooterText="objColhStatusunkid" Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true" Visible="false" FooterText="objColhEmployeeId" />
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <uc2:DeleteReason ID="popDeleteReason" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
