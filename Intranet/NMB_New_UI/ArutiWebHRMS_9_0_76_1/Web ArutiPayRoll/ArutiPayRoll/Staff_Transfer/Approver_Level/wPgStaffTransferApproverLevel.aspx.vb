﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization

Partial Class Staff_Transfer_Approver_Level_wPgStaffTransferApproverLevel
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmStaffTransferApproverLevelList"
    Private ReadOnly mstrModuleName1 As String = "frmStaffTransferApproverLevelAddEdit"
    Private mintLevelId As Integer
    Private blnpopupStaffTransferApproverLevel As Boolean = False
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call SetLanguage()
                If CBool(Session("AllowToViewStaffTransferApproverLevel")) Then
                    FillList(False)
                Else
                    FillList(True)
                End If
                SetVisibility()
            Else
                mintLevelId = Convert.ToInt32(ViewState("stLevelID").ToString())
                blnpopupStaffTransferApproverLevel = Convert.ToBoolean(ViewState("blnpopupStaffTransferApproverLevel").ToString())

                If blnpopupStaffTransferApproverLevel Then
                    popupStaffTransferApproverLevel.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("stLevelID") = mintLevelId
            ViewState("blnpopupStaffTransferApproverLevel") = blnpopupStaffTransferApproverLevel
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim strfilter As String = ""
        Dim objApproverLevel As clsstapproverlevel_master
        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If

            objApproverLevel = New clsstapproverlevel_master

            dsApproverList = objApproverLevel.GetList("List", True, strfilter)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objApproverLevel.GetList("List", True, "1 = 2")
                isblank = True
            End If

            If isblank Then
                dgApprLevelList.DataSource = New List(Of String)
                dgApprLevelList.DataBind()
                isblank = False
            Else
                dgApprLevelList.DataSource = dsApproverList.Tables("List")
                dgApprLevelList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Visible = CBool(Session("AllowToAddStaffTransferApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue(ByVal objApproverLevel As clsstapproverlevel_master)
        Try
            objApproverLevel._stlevelunkid = mintLevelId
            objApproverLevel._stLevelcode = txtlevelcode.Text.Trim
            objApproverLevel._Stlevelname = txtlevelname.Text.Trim
            objApproverLevel._Priority = Convert.ToInt32(txtlevelpriority.Text)
            Call SetAtValue(objApproverLevel)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue(ByVal objApproverLevel As clsstapproverlevel_master)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objApproverLevel._Userunkid = CInt(Session("UserId"))
            End If
            objApproverLevel._WebClientIP = CStr(Session("IP_ADD"))
            objApproverLevel._WebHostName = CStr(Session("HOST_NAME"))
            objApproverLevel._WebFormName = mstrModuleName1
            objApproverLevel._IsWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objApproverLevel As clsstapproverlevel_master
        Try
            objApproverLevel = New clsstapproverlevel_master
            If mintLevelId > 0 Then
                objApproverLevel._stlevelunkid = mintLevelId
            End If
            txtlevelcode.Text = objApproverLevel._stLevelcode
            txtlevelname.Text = objApproverLevel._Stlevelname
            txtlevelpriority.Text = objApproverLevel._Priority.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub cleardata()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelId = 0
            popupStaffTransferApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveStaffTransferApproverLevel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveStaffTransferApproverLevel.Click
        Dim blnFlag As Boolean = False
        Dim objApproverLevel As clsstapproverlevel_master
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            End If

            objApproverLevel = New clsstapproverlevel_master
            Call SetValue(objApproverLevel)

            If mintLevelId > 0 Then
                blnFlag = objApproverLevel.Update()
            ElseIf mintLevelId = 0 Then
                blnFlag = objApproverLevel.Insert()
            End If

            If blnFlag = False And objApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objApproverLevel._Message, Me)
                blnpopupStaffTransferApproverLevel = True
                popupStaffTransferApproverLevel.Show()
                Exit Sub
            Else
                cleardata()
                FillList(False)
                mintLevelId = 0
                blnpopupStaffTransferApproverLevel = False
                popupStaffTransferApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverLevel = Nothing
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelId = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValue()
            blnpopupStaffTransferApproverLevel = True
            popupStaffTransferApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objApproverLevel As clsstapproverlevel_master
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelId = Convert.ToInt32(lnk.CommandArgument.ToString())
            objApproverLevel = New clsstapproverlevel_master
            If objApproverLevel.isUsed(mintLevelId) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Are You sure you want to delete this Approver Level ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverLevel = Nothing
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Dim objApproverLevel As clsstapproverlevel_master
        Try
            Dim blnFlag As Boolean = False
            objApproverLevel = New clsstapproverlevel_master
            SetAtValue(objApproverLevel)
            objApproverLevel._WebFormName = mstrModuleName

            blnFlag = objApproverLevel.Delete(mintLevelId)

            If blnFlag = False And objApproverLevel._Message <> "" Then
                DisplayMessage.DisplayMessage(objApproverLevel._Message, Me)
                Exit Sub
            Else
                FillList(False)
                mintLevelId = 0
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverLevel = Nothing
        End Try
    End Sub

    Protected Sub btnCloseOTRequiApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseStaffTransferApproverLevel.Click
        Try
            cleardata()
            blnpopupStaffTransferApproverLevel = False
            popupStaffTransferApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Gridview Events "

    Protected Sub dgApprLevelList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgApprLevelList.ItemDataBound
        Try
            If e.Item.ItemIndex < 0 Then Exit Sub

            Dim lnkedit As LinkButton = TryCast(e.Item.FindControl("lnkedit"), LinkButton)
            Dim lnkdelete As LinkButton = TryCast(e.Item.FindControl("lnkdelete"), LinkButton)

            lnkedit.Visible = CBool(Session("AllowToEditStaffTransferApproverLevel"))
            lnkdelete.Visible = CBool(Session("AllowToDeleteStaffTransferApproverLevel"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgApprLevelList.Columns(0).FooterText, dgApprLevelList.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgApprLevelList.Columns(1).FooterText, dgApprLevelList.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgApprLevelList.Columns(2).FooterText, dgApprLevelList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgApprLevelList.Columns(3).FooterText, dgApprLevelList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgApprLevelList.Columns(4).FooterText, dgApprLevelList.Columns(4).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lbllevelname.ID, Me.lbllevelname.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnSaveStaffTransferApproverLevel.ID, Me.btnSaveStaffTransferApproverLevel.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCloseStaffTransferApproverLevel.ID, Me.btnCloseStaffTransferApproverLevel.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPageHeader2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            dgApprLevelList.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgApprLevelList.Columns(0).FooterText, dgApprLevelList.Columns(0).HeaderText)
            dgApprLevelList.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgApprLevelList.Columns(1).FooterText, dgApprLevelList.Columns(1).HeaderText)
            dgApprLevelList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgApprLevelList.Columns(2).FooterText, dgApprLevelList.Columns(2).HeaderText)
            dgApprLevelList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgApprLevelList.Columns(3).FooterText, dgApprLevelList.Columns(3).HeaderText)
            dgApprLevelList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgApprLevelList.Columns(4).FooterText, dgApprLevelList.Columns(4).HeaderText)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))


            Me.lblCancelText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCancelText1.ID, Me.lblCancelText1.Text)
            Me.lbllevelcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lbllevelcode.ID, Me.lbllevelcode.Text)
            Me.lbllevelname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lbllevelname.ID, Me.lbllevelname.Text)
            Me.lbllevelpriority.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lbllevelpriority.ID, Me.lbllevelpriority.Text)
            Me.btnSaveStaffTransferApproverLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveStaffTransferApproverLevel.ID, Me.btnSaveStaffTransferApproverLevel.Text.Replace("&", ""))
            Me.btnCloseStaffTransferApproverLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseStaffTransferApproverLevel.ID, Me.btnCloseStaffTransferApproverLevel.Text.Replace("&", ""))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information.")

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Are You sure you want to delete this Approver Level ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
