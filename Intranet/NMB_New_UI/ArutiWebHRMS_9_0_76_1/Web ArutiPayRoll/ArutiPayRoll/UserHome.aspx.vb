﻿Imports Aruti.Data.clsEmployee_Master
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports Aruti.Data
Imports System.Linq
Imports System.IO
Imports System.Web.Script.Serialization

Partial Class UserHome
    Inherits Basepage

    Private Shared DisplayMessage As New CommonCodes
    Dim staffturnovercategory As String = ""
    Dim staffturnoverdata As String = ""
    Dim leaveanalysiscategory As String = ""
    Dim leaveanalysisdata As String = ""
    'Hemant (13 Oct 2023) -- Start
    'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
    Dim SalaryAnalysisCategory As String = ""
    Dim SalaryAnalysisData As String = ""
    'Hemant (13 Oct 2023) -- End


    'Pinkal (01-Jun-2021)-- Start
    'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    Dim mdctDashBoardSetting As Dictionary(Of Integer, Integer) = Nothing
    'Pinkal (01-Jun-2021) -- End


#Region " Page's Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        'Dim clsUserAddEdit As New Aruti.Data.clsUserAddEdit
        'Dim clsEmpMst As New Aruti.Data.clsEmployee_Master
        Dim objDashboard_Class As New Aruti.Data.clsDashboard_Class
        ' Dim MessageBox As New MessageBox

        Try

            'Dim clsUser As New User

            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    pnlTask.Visible = False
            'End If

            'If IsPostBack = False Then
            '    clsUser = CType(Session("clsuser"), User)
            '    lblRfname.Text = clsUser.Firstname
            '    lblrSurname.Text = clsUser.Surname
            '    lblrAppdate.Text = clsUser.appointeddate
            '    lblRgend.Text = clsUser.gender
            '    lblRbdate.Text = clsUser.birthdate
            '    lblRemail.Text = clsUser.Email
            '    lblRpadd.Text = clsUser.present_address1
            '    lblRpadd2.Text = clsUser.present_address2
            '    lblRMobno.Text = clsUser.present_mobile
            '    lblRcurShift.Text = clsUser.ShiftName
            '    lblRcudept.Text = clsUser.Department
            '    GridView1.DataSource = clsUser.LeaveBalances
            '    GridView1.DataBind()
            'Else
            'End If

            'S.SANDEEP |20-JAN-2022| -- START
            '**********************************************************************************
            'If (Page.IsPostBack = False) Then

            '    Dim dtBirthdayList As New DataTable()
            '    Dim dtNewEmployeeList As New DataTable()
            '    Dim dtWorkAnniversaryList As New DataTable()
            '    Dim dtHolidayList As New DataTable()
            '    Dim dtTeamMemberList As New DataTable()
            '    Dim dtEmployeeOnLeaveList As New DataTable()
            '    Dim dtPendingTaskList As New DataTable()

            '    Dim dtStaffList As New DataTable()

            '    Dim dsLeaveAnnalysisList As New DataSet()
            '    Dim blnApplyAccessFilter As Boolean = False
            '    Dim strAdvanceFilter As String = ""
            '    Dim strORQueryForUserAccess As String = ""
            '    Dim intEmpid As Integer = 0

            '    If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
            '        Dim objemp As New clsEmployee_Master
            '        objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))
            '        lblWelcomeEmployee.Text = objemp._Firstname.ToUpper()
            '        objemp = Nothing

            '        lnkQApplyOT.Visible = True
            '        lnkQApplyLeave.Visible = True
            '        lnkQMyPayslip.Visible = True

            '    ElseIf (CInt(Session("U_UserID")) > 0 AndAlso CInt(Session("E_Employeeunkid")) <= 0) Then
            '        lblWelcomeEmployee.Text = Session("UserName").ToUpper()

            '        lnkQApplyOT.Visible = False
            '        lnkQApplyLeave.Visible = False
            '        lnkQMyPayslip.Visible = False


            '    ElseIf (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0) Then
            '        Dim objemp As New clsEmployee_Master
            '        objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))
            '        lblWelcomeEmployee.Text = objemp._Firstname.ToUpper()
            '        objemp = Nothing

            '        lnkQApplyOT.Visible = True
            '        lnkQApplyLeave.Visible = True
            '        lnkQMyPayslip.Visible = True

            '    End If

            '    lblSummaryDate.Text = ConfigParameter._Object._CurrentDateAndTime.ToString("dd-MMM-yyyy")

            '    'Pinkal (01-Jun-2021)-- Start
            '    'New UI Self Service Enhancement : Working on New UI Dashboard Settings.

            '    Dim objDashBoardData As New clsUser_dashboard
            '    Dim dsList As DataSet = objDashBoardData.GetList("List", CInt(Session("CompanyUnkId")), CInt(IIf(CInt(Session("UserId")) > 0, CInt(Session("UserId")), 0)), CInt(IIf(CInt(Session("Employeeunkid")) > 0, CInt(Session("Employeeunkid")), 0)))
            '    mdctDashBoardSetting = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).ToDictionary(Function(x) x.Field(Of Integer)("itemtypeid"), Function(y) y.Field(Of Integer)("userdashboardunkid"))
            '    dsList.Clear()
            '    dsList = Nothing
            '    objDashBoardData = Nothing


            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_TodayBirthday) Then
            '        dtBirthdayList = GetBirthdayData()
            '        If dtBirthdayList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
            '            dlBirthday.DataSource = dtBirthdayList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
            '            lblBirthdayCount.Text = dtBirthdayList.AsEnumerable().Cast(Of DataRow).Count.ToString()
            '            dlBirthday.DataBind()
            '        Else
            '            lblBirthdayCount.Text = "0"
            '        End If
            '    Else
            '        dvBirthDayDetalls.Visible = False
            '    End If

            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_WorkAnniversary) Then
            '        dtWorkAnniversaryList = GetWorkAnniversaryData()
            '        If IsNothing(dtWorkAnniversaryList) = False AndAlso dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
            '            dlWorkAnniversary.DataSource = dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
            '            lblWorkAnniversaryCount.Text = dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Count.ToString()
            '            dlWorkAnniversary.DataBind()
            '        Else
            '            lblWorkAnniversaryCount.Text = "0"
            '        End If
            '    Else
            '        dvEmpWorkAnniversary.Visible = False
            '    End If

            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_NewlyHired) Then
            '        If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '            dtNewEmployeeList = GetNewEmployeeData()
            '            If dtNewEmployeeList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
            '                dlNewlyHired.DataSource = dtNewEmployeeList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
            '                lblNewlyHiredCount.Text = dtNewEmployeeList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("Id") = 6).Count.ToString()
            '                dlNewlyHired.DataBind()
            '            Else
            '                lblNewlyHiredCount.Text = "0"
            '            End If
            '            pnlNewlyHired.Visible = True
            '        Else
            '            pnlNewlyHired.Visible = False
            '        End If
            '    Else
            '        pnlNewlyHired.Visible = False
            '    End If


            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_UpcomingHolidays) Then
            '        dtHolidayList = GetHolidayData()
            '        If IsNothing(dtHolidayList) = False AndAlso dtHolidayList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
            '            lblUpcomingHolidaysCount.Text = dtHolidayList.Rows.Count
            '            dlUpcomingHolidays.DataSource = dtHolidayList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
            '            dlUpcomingHolidays.DataBind()
            '        Else
            '            lblUpcomingHolidaysCount.Text = "0"
            '        End If
            '    Else
            '        dvUpcomingHolidays.Visible = False
            '    End If

            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_TeamMembers) Then
            '        dtTeamMemberList = GetTeamMemberData()
            '        If IsNothing(dtTeamMemberList) = False AndAlso dtTeamMemberList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
            '            lblTeamMembersCount.Text = dtTeamMemberList.Rows.Count
            '            dlTeamMembers.DataSource = dtTeamMemberList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
            '            dlTeamMembers.DataBind()
            '        Else
            '            lblTeamMembersCount.Text = "0"
            '        End If
            '    Else
            '        dvTeamMembers.Visible = False
            '    End If

            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_EmpOnLeave) Then
            '        dtEmployeeOnLeaveList = GetEmployeeOnLeaveData()
            '        If IsNothing(dtEmployeeOnLeaveList) = False AndAlso dtEmployeeOnLeaveList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
            '            lblEmployeeOnLeaveCount.Text = dtEmployeeOnLeaveList.Rows.Count
            '            dlEmployeeOnLeaveCount.DataSource = dtEmployeeOnLeaveList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
            '            dlEmployeeOnLeaveCount.DataBind()
            '        Else
            '            lblEmployeeOnLeaveCount.Text = "0"
            '        End If
            '    Else
            '        dvEmpOnLeave.Visible = False
            '    End If

            '    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_PendingTask) Then

            '        dtPendingTaskList = GetPendingTaskData()

            '        If IsNothing(dtPendingTaskList) = False AndAlso dtPendingTaskList.Rows.Count > 0 Then
            '            pnlPendingTask.Visible = True
            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveClaimExpenseMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveClaimExpense.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveClaimExpenseMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveClaimExpenseCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLeaveMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveLeave.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLeaveMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()

            '                If IsNothing(drRow) = False Then
            '                    objlblApproveLeaveCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveOTApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveOTApplication.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveOTApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveOTApplicationCount.Text = drRow("totalcount")
            '                End If
            '            End If


            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveScoreCardMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveScoreCard.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveScoreCardMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveScoreCardCount.Text = drRow("totalcount")
            '                End If
            '            End If


            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkSetScoreCard.Visible = True

            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblSetScoreCardCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveUpdateProgressMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveUpdateProgress.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveUpdateProgressMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveUpdateProgressCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssessEmployeeCompetenceMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkAssessEmployeeCompetence.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveOTApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveOTApplicationCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssessEmployeeMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkAssessEmployee.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssessEmployeeMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblAssessEmployeeCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssetDeclarationT2ESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkAssetDeclaration.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssetDeclarationT2ESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblAssetDeclarationCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkMyAssessment.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblMyAssessmentCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyCompetenceAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkMyCompetenceAssessment.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyCompetenceAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblMyCompetenceAssessmentCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeAssessmentMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkReviewEmployeeAssessment.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeAssessmentMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblReviewEmployeeAssessmentCount.Text = drRow("totalcount")
            '                End If
            '            End If



            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeCompetenceMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkReviewEmployeeCompetence.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeCompetenceMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblReviewEmployeeCompetenceCount.Text = drRow("totalcount")
            '                End If
            '            End If


            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkSetScoreCard.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblSetScoreCardCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.NonDisclosureDeclarationESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkNonDisclosureDeclaration.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.NonDisclosureDeclarationESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblNonDisclosureDeclarationCount.Text = drRow("totalcount")
            '                End If
            '            End If


            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveSalaryChangeMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then

            '                lnkApproveSalaryChangeMSS.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveSalaryChangeMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveSalaryChangeMSSCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApprovePayslipPaymentMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApprovePayslipPaymentMSS.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApprovePayslipPaymentMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApprovePayslipPaymentMSSCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLoanApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveLoanApplicationMSS.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLoanApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveLoanApplicationMSSCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveCalibrationMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveCalibrationMSS.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveCalibrationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveCalibrationMSSCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveStaffRequisitionMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkApproveStaffRequisitionMSS.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveStaffRequisitionMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblApproveStaffRequisitionMSSCount.Text = drRow("totalcount")
            '                End If
            '            End If
            '            'Hemant (04 Sep 2021) -- Start
            '            'ENHANCEMENT : OLD-465 - Dashboard Pending Task Enhancement - Training Evaluation.
            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyTrainingFeedbackESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkMyTrainingFeedback.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyTrainingFeedbackESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblMyTrainingFeedbackCount.Text = drRow("totalcount")
            '                End If
            '            End If

            '            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.EvaluateEmployeeTrainingMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
            '                lnkEvaluateEmployeeTraining.Visible = True
            '                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.EvaluateEmployeeTrainingMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
            '                If IsNothing(drRow) = False Then
            '                    objlblEvaluateEmployeeTrainingCount.Text = drRow("totalcount")
            '                End If
            '            End If
            '            'Hemant (04 Sep 2021) -- End
            '        End If
            '    Else
            '        pnlPendingTask.Visible = False
            '    End If

            '    If CInt(Session("U_UserID")) > 0 Then

            '        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_LeaveAnalysis) OrElse mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_StaffTurnOver) Then

            '            'objDashboard_Class = New clsDashboard_Class(CStr(Session("Database_Name")), _
            '            '                                                         CStr(Session("UserAccessModeSetting")), _
            '            '                                                         CInt(Session("U_UserID")), _
            '            '                                                         CInt(Session("Fin_year")), _
            '            '                                                         CInt(Session("CompanyUnkId")), _
            '            '                                                         Session("fin_startdate"), _
            '            '                                                         Session("fin_enddate"), _
            '            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '            '                                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         False, _
            '            '                                                         0, _
            '            '                                                         0, _
            '            '                                                         0, _
            '            '                                                         0, _
            '            '                                                         0, _
            '            '                                                         0, _
            '            '                                             0, True, _
            '            '                                                         False)

            '            objDashboard_Class = New clsDashboard_Class(CStr(Session("Database_Name")), _
            '                                                         CStr(Session("UserAccessModeSetting")), _
            '                                                         CInt(Session("U_UserID")), _
            '                                                         CInt(Session("Fin_year")), _
            '                                                         CInt(Session("CompanyUnkId")), _
            '                                                         Session("fin_startdate"), _
            '                                                         Session("fin_enddate"), _
            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         False, _
            '                                                         0, _
            '                                                         0, _
            '                                                         0, _
            '                                                         0, _
            '                                                         0, _
            '                                                         0, _
            '                                                          0, False, _
            '                                                         False)

            '        End If

            '        Dim xCategory As String() = Nothing
            '        Dim jsSerializer As New JavaScriptSerializer

            '        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_StaffTurnOver) Then


            '            'Pinkal (09-Aug-2021)-- Start
            '            'NMB New UI Enhancements.
            '            'dtStaffList = objDashboard_Class.Staff_TO_Grid_Self_Service("grid")
            '            strAdvanceFilter = ""
            '            If CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

            '                blnApplyAccessFilter = True
            '                strAdvanceFilter = ""

            '            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

            '                Dim objEmp As New clsEmployee_Master
            '                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

            '                blnApplyAccessFilter = False

            '                Dim ar() As String = Session("EmpStaffTurnOverAllocation").ToString().Split(CChar(","))

            '                If ar.Length > 0 Then

            '                    For i As Integer = 0 To ar.Length - 1

            '                        Select Case CInt(ar(i))
            '                            Case enAllocation.BRANCH
            '                                strAdvanceFilter &= "AND ADF.stationunkid = " & objEmp._Stationunkid & " "

            '                            Case enAllocation.DEPARTMENT_GROUP
            '                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objEmp._Deptgroupunkid & " "

            '                            Case enAllocation.DEPARTMENT
            '                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objEmp._Departmentunkid & " "

            '                            Case enAllocation.SECTION_GROUP
            '                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objEmp._Sectiongroupunkid & " "

            '                            Case enAllocation.SECTION
            '                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objEmp._Sectionunkid & " "

            '                            Case enAllocation.UNIT_GROUP
            '                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objEmp._Unitgroupunkid & " "

            '                            Case enAllocation.UNIT
            '                                strAdvanceFilter &= "AND ADF.unitunkid = " & objEmp._Unitunkid & " "

            '                            Case enAllocation.TEAM
            '                                strAdvanceFilter &= "AND ADF.teamunkid =" & objEmp._Teamunkid & " "

            '                            Case enAllocation.JOB_GROUP
            '                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objEmp._Jobgroupunkid & " "

            '                            Case enAllocation.JOBS
            '                                strAdvanceFilter &= "AND ADF.jobunkid =" & objEmp._Jobunkid & " "

            '                            Case enAllocation.CLASS_GROUP
            '                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objEmp._Classgroupunkid & " "

            '                            Case enAllocation.CLASSES
            '                                strAdvanceFilter &= "AND ADF.classunkid =" & objEmp._Classunkid & " "

            '                            Case Else
            '                                strAdvanceFilter &= ""

            '                        End Select

            '                    Next

            '                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

            '                End If
            '                objEmp = Nothing

            '            End If

            '            dtStaffList = objDashboard_Class.Staff_TO_Grid_Self_Service(blnApplyAccessFilter, strAdvanceFilter, "grid")

            '            'Pinkal (09-Aug-2021) -- End

            '            Dim Staffsource = New List(Of ClsChartData)
            '            If dtStaffList.Columns.Contains("ViewId") Then
            '                dtStaffList.Columns.Remove("ViewId")
            '            End If

            '            xCategory = dtStaffList.Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.Ordinal > 0).Select(Function(x) x.ColumnName).ToArray()

            '            For Each dtrow As DataRow In dtStaffList.Rows
            '                'Pinkal (01-Jun-2021)-- Start
            '                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            '                'Dim strflds As String() = dtrow.ItemArray.Select(Function(x) x.ToString()).ToArray()
            '                'Dim lst As List(Of String)
            '                'If strflds IsNot Nothing AndAlso strflds.Length > 0 Then
            '                '    lst = strflds.ToList()
            '                '    lst.RemoveAt(0)
            '                'For index As Integer = 0 To lst.Count - 1
            '                '    If IsDBNull(lst(index)) OrElse lst(index).Length <= 0 Then
            '                '        lst(index) = 0
            '                '                End If
            '                'Next
            '                'Staffsource.Add(New ClsChartData With {.name = dtrow("Particulars"), .data = lst.Select(Function(x) CInt(x)).ToArray()})
            '                'End If
            '                Dim lst As List(Of String) = dtrow.ItemArray.Select(Function(x) x.ToString()).Skip(1).ToList()
            '                If lst IsNot Nothing AndAlso lst.Count > 0 Then
            '                    Staffsource.Add(New ClsChartData With {.name = dtrow("Particulars"), .data = lst.Select(Function(x) CInt(x)).ToArray()})
            '                End If
            '                lst.Clear()
            '                lst = Nothing
            '                'Pinkal (01-Jun-2021) -- End

            '            Next

            '            jsSerializer.MaxJsonLength = 2147483644
            '            staffturnoverdata = jsSerializer.Serialize(Staffsource)
            '            staffturnovercategory = jsSerializer.Serialize(xCategory)
            '            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "StaffChart", "getStaffTurnover('staff-turnover-container'," & staffturnovercategory & ",'Staff Turnover', " & staffturnoverdata & ");", True)
            '        Else
            '            pnlStaffTurnOverChart.Visible = False
            '        End If

            '        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_LeaveAnalysis) Then


            '            'Pinkal (09-Aug-2021)-- Start
            '            'NMB New UI Enhancements.

            '            'dsLeaveAnnalysisList = objDashboard_Class.Leave_Grid_SelfService("LeaveAnnalysis")

            '            strAdvanceFilter = ""
            '            If CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

            '                blnApplyAccessFilter = True
            '                strAdvanceFilter = ""


            '            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

            '                Dim objemp As New clsEmployee_Master
            '                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

            '                blnApplyAccessFilter = False
            '                strAdvanceFilter = ""

            '                If blnApplyAccessFilter = False Then

            '                    Dim ar() As String = Session("EmpOnLeaveAllocation").ToString().Split(CChar(","))

            '                    If ar.Length > 0 Then

            '                        For i As Integer = 0 To ar.Length - 1
            '                            Select Case CInt(ar(i))
            '                                Case enAllocation.BRANCH
            '                                    strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

            '                                Case enAllocation.DEPARTMENT_GROUP
            '                                    strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

            '                                Case enAllocation.DEPARTMENT
            '                                    strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

            '                                Case enAllocation.SECTION_GROUP
            '                                    strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

            '                                Case enAllocation.SECTION
            '                                    strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

            '                                Case enAllocation.UNIT_GROUP
            '                                    strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

            '                                Case enAllocation.UNIT
            '                                    strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

            '                                Case enAllocation.TEAM
            '                                    strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

            '                                Case enAllocation.JOB_GROUP
            '                                    strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

            '                                Case enAllocation.JOBS
            '                                    strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

            '                                Case enAllocation.CLASS_GROUP
            '                                    strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

            '                                Case enAllocation.CLASSES
            '                                    strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

            '                                Case Else
            '                                    strAdvanceFilter &= ""

            '                            End Select

            '                        Next

            '                        If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

            '                    End If

            '                End If

            '                objemp = Nothing
            '            End If

            '            dsLeaveAnnalysisList = objDashboard_Class.Leave_Grid_SelfService(blnApplyAccessFilter, strAdvanceFilter, "LeaveAnnalysis")

            '            'Pinkal (09-Aug-2021) -- End

            '            Dim LeaveAnnalysissource = New List(Of ClsChartData)
            '            dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Columns.Remove("LId")
            '            dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Columns.Remove("LCode")

            '            xCategory = dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.Ordinal > 0).Select(Function(x) x.ColumnName).ToArray()


            '            For Each dtrow As DataRow In dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Rows
            '                'Pinkal (01-Jun-2021)-- Start
            '                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            '                'Dim strflds As String() = dtrow.ItemArray.Select(Function(x) x.ToString()).ToArray()
            '                'Dim lst As List(Of String) = Nothing
            '                'If lst IsNot Nothing AndAlso lst.Count > 0 Then
            '                '    lst = strflds.ToList()
            '                '    lst.RemoveAt(0)
            '                '    For index As Integer = 0 To lst.Count - 1
            '                '        If IsDBNull(lst(index)) OrElse lst(index).Length <= 0 Then
            '                '            lst(index) = 0
            '                '                End If
            '                '            Next
            '                '    LeaveAnnalysissource.Add(New ClsChartData With {.name = dtrow("Lname"), .data = lst.Select(Function(x) CInt(x)).ToArray()})
            '                'End If
            '                Dim lst As List(Of String) = dtrow.ItemArray.Select(Function(x) x.ToString()).Skip(1).ToList()
            '                If lst IsNot Nothing AndAlso lst.Count > 0 Then
            '                    LeaveAnnalysissource.Add(New ClsChartData With {.name = dtrow("Lname"), .data = lst.Select(Function(x) CInt(x)).ToArray()})
            '                End If
            '                lst.Clear()
            '                lst = Nothing
            '                'Pinkal (01-Jun-2021) -- End

            '            Next

            '            jsSerializer = New JavaScriptSerializer
            '            jsSerializer.MaxJsonLength = 2147483644
            '            leaveanalysisdata = jsSerializer.Serialize(LeaveAnnalysissource)
            '            leaveanalysiscategory = jsSerializer.Serialize(xCategory)
            '            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "LeaveChart", "getLeaveData('leave-analysis-container'," & leaveanalysiscategory & ",'Total Leave Days', " & leaveanalysisdata & ");", True)
            '        Else
            '            pnlLeaveAnalysisChart.Visible = False
            '        End If

            '    Else
            '        pnlStaffTurnOverChart.Visible = False
            '        pnlLeaveAnalysisChart.Visible = False
            '    End If

            'Else

            '    If CInt(Session("U_UserID")) > 0 Then

            '        If IsNothing(ViewState("staffturnovercategory")) = False Then
            '            staffturnovercategory = ViewState("staffturnovercategory")
            '        End If

            '        If IsNothing(ViewState("staffturnoverdata")) = False Then
            '            staffturnoverdata = ViewState("staffturnoverdata")
            '        End If

            '        If staffturnovercategory.Length > 0 AndAlso staffturnoverdata.Length > 0 Then
            '            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "StaffChart", "getStaffTurnover('staff-turnover-container'," & staffturnovercategory & ",'Staff Turnover', " & staffturnoverdata & ");", True)
            '        End If


            '        If IsNothing(ViewState("leaveanalysiscategory")) = False Then
            '            leaveanalysiscategory = ViewState("leaveanalysiscategory")
            '        End If

            '        If IsNothing(ViewState("leaveanalysisdata")) = False Then
            '            leaveanalysisdata = ViewState("leaveanalysisdata")
            '        End If

            '        If leaveanalysiscategory.Length > 0 AndAlso leaveanalysisdata.Length > 0 Then
            '            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "LeaveChart", "getLeaveData('leave-analysis-container'," & leaveanalysiscategory & ",'Leave Annalysis', " & leaveanalysisdata & ");", True)
            '        End If

            '    End If

            '    mdctDashBoardSetting = CType(Me.ViewState("dctDashBoardSetting"), Dictionary(Of Integer, Integer))

            'End If

            'Dim mstrGroupName As String = GetCompanyGroup()

            'If GetCompanyGroup().ToUpper() <> "NMB PLC" AndAlso _
            '   GetCompanyGroup().ToUpper() <> "TANZANIA REVENUE AUTHORITY" AndAlso _
            '   GetCompanyGroup().ToUpper() <> "SIMBA GROUP" Then

            If Session("CompanyGroupName").ToString().ToUpper() <> "NMB PLC" AndAlso _
                Session("CompanyGroupName").ToString().ToUpper() <> "TANZANIA REVENUE AUTHORITY" AndAlso _
                Session("CompanyGroupName").ToString().ToUpper() <> "SIMBA GROUP" Then

                If (Page.IsPostBack = False) Then

                    Dim dtBirthdayList As New DataTable()
                    Dim dtNewEmployeeList As New DataTable()
                    Dim dtWorkAnniversaryList As New DataTable()
                    Dim dtHolidayList As New DataTable()
                    Dim dtTeamMemberList As New DataTable()
                    Dim dtEmployeeOnLeaveList As New DataTable()
                    Dim dtPendingTaskList As New DataTable()
                    Dim dtStaffList As New DataTable()
                    Dim dsLeaveAnnalysisList As New DataSet()
                    'Hemant (13 Oct 2023) -- Start
                    'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                    Dim dsSalaryAnalysisLIst As New DataSet
                    'Hemant (13 Oct 2023) -- End


                    'Pinkal (14-Feb-2022) -- Start
                    'Enhancement TRA : TnA Module Enhancement for TRA.
                    Dim dtTnADetails As New DataTable
                    'Pinkal (14-Feb-2022) -- End

                    'Pinkal (15-Sep-2023) -- Start
                    '(A1X-1284) Toyota - Configuration of Intranet links on the new UI.
                    Dim dtIntranetLink As New DataTable()
                    If Session("SessionGoalStatusFromDashBoard") IsNot Nothing Then Session("SessionGoalStatusFromDashBoard") = False
                    If Session("SessionProgressUpdateFromDashBoard") IsNot Nothing Then Session("SessionProgressUpdateFromDashBoard") = False
                    'Pinkal (15-Sep-2023) -- End

                    'Hemant (13 Oct 2023) -- Start
                    'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                    Call FillCombo()
                    'Hemant (13 Oct 2023) -- End


                    Dim blnApplyAccessFilter As Boolean = False
                    Dim strAdvanceFilter As String = ""
                    Dim strORQueryForUserAccess As String = ""
                    Dim intEmpid As Integer = 0

                    If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                        Dim objemp As New clsEmployee_Master
                        objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))
                        lblWelcomeEmployee.Text = objemp._Firstname.ToUpper()
                        objemp = Nothing

                        lnkQApplyOT.Visible = True
                        lnkQApplyLeave.Visible = True
                        lnkQMyPayslip.Visible = True
                        'S.SANDEEP |13-APR-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-143
                        If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                            lnkQRebate.Visible = True
                        Else
                            lnkQRebate.Visible = False
                        End If
                        'S.SANDEEP |13-APR-2022| -- END

                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1284) Toyota - Configuration of Intranet links on the new UI.
                        If GetCompanyGroup().ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                            lnkMyGoalStatus.Visible = True
                            lnkViewMyProgressUpdateReport.Visible = True
                        Else
                            lnkMyGoalStatus.Visible = False
                            lnkViewMyProgressUpdateReport.Visible = False
                        End If
                        lnkMSSEmployeeHeadCountReport.Visible = False
                        'Pinkal (15-Sep-2023) -- End

                        'Pinkal (30-Sep-2023) -- Start
                        '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
                        dvVacancy.Visible = False
                        'Pinkal (30-Sep-2023) -- End


                    ElseIf (CInt(Session("U_UserID")) > 0 AndAlso CInt(Session("E_Employeeunkid")) <= 0) Then
                        lblWelcomeEmployee.Text = Session("UserName").ToUpper()

                        lnkQApplyOT.Visible = False
                        lnkQApplyLeave.Visible = False
                        lnkQMyPayslip.Visible = False
                        'S.SANDEEP |13-APR-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-143
                        lnkQRebate.Visible = False
                        'S.SANDEEP |13-APR-2022| -- END

                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1284) Toyota - Configuration of Intranet links on the new UI.
                        If GetCompanyGroup().ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                            lnkMSSEmployeeHeadCountReport.Visible = True
                        Else
                            lnkMSSEmployeeHeadCountReport.Visible = False
                        End If
                        lnkMyGoalStatus.Visible = False
                        lnkViewMyProgressUpdateReport.Visible = False
                        'Pinkal (15-Sep-2023) -- End

                        'Pinkal (30-Sep-2023) -- Start
                        '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
                        If GetCompanyGroup().ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                            dvVacancy.Visible = True
                        Else
                            dvVacancy.Visible = False
                        End If
                        'Pinkal (30-Sep-2023) -- End



                    ElseIf (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0) Then
                        Dim objemp As New clsEmployee_Master
                        objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))
                        lblWelcomeEmployee.Text = objemp._Firstname.ToUpper()
                        objemp = Nothing

                        lnkQApplyOT.Visible = True
                        lnkQApplyLeave.Visible = True
                        lnkQMyPayslip.Visible = True
                        'S.SANDEEP |13-APR-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-143
                        If Session("CompanyGroupName").ToString().ToUpper() = "PW" Then
                            lnkQRebate.Visible = True
                        Else
                            lnkQRebate.Visible = False
                        End If
                        'S.SANDEEP |13-APR-2022| -- END

                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1284) Toyota - Configuration of Intranet links on the new UI. 
                        If GetCompanyGroup().ToUpper() = "KARIMJEE JIVANJEE GROUP" Then

                            Dim objReport As New clsArutiReportClass
                            Dim dsData As DataSet = objReport.getReportList(CInt(Session("UserId")), Session("CompanyUnkId"), True)
                            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                                Dim xCount As Integer = dsData.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ReportId") = enArutiReport.EmployeeHeadCount).DefaultIfEmpty().Count
                                If xCount > 0 Then
                                    lnkMSSEmployeeHeadCountReport.Visible = True
                                Else
                                    lnkMSSEmployeeHeadCountReport.Visible = False
                                End If
                            Else
                                lnkMSSEmployeeHeadCountReport.Visible = True
                            End If
                            objReport = Nothing
                            lnkMyGoalStatus.Visible = True
                            lnkViewMyProgressUpdateReport.Visible = True

                            'Pinkal (30-Sep-2023) -- Start
                            '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
                            dvVacancy.Visible = True
                            'Pinkal (30-Sep-2023) -- End
                        Else
                            lnkMSSEmployeeHeadCountReport.Visible = False
                            lnkMyGoalStatus.Visible = False
                            lnkViewMyProgressUpdateReport.Visible = False

                            'Pinkal (30-Sep-2023) -- Start
                            '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
                            dvVacancy.Visible = False
                            'Pinkal (30-Sep-2023) -- End
                        End If
                        'Pinkal (15-Sep-2023) -- End

                    End If

                    lblSummaryDate.Text = ConfigParameter._Object._CurrentDateAndTime.ToString("dd-MMM-yyyy")

                    'Pinkal (01-Jun-2021)-- Start
                    'New UI Self Service Enhancement : Working on New UI Dashboard Settings.

                    Dim objDashBoardData As New clsUser_dashboard
                    Dim dsList As DataSet = objDashBoardData.GetList("List", CInt(Session("CompanyUnkId")), CInt(IIf(CInt(Session("UserId")) > 0, CInt(Session("UserId")), 0)), CInt(IIf(CInt(Session("Employeeunkid")) > 0, CInt(Session("Employeeunkid")), 0)))
                    mdctDashBoardSetting = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).ToDictionary(Function(x) x.Field(Of Integer)("itemtypeid"), Function(y) y.Field(Of Integer)("userdashboardunkid"))
                    dsList.Clear()
                    dsList = Nothing
                    objDashBoardData = Nothing


                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_TodayBirthday) Then
                        dtBirthdayList = GetBirthdayData()
                        If dtBirthdayList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                            dlBirthday.DataSource = dtBirthdayList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
                            lblBirthdayCount.Text = dtBirthdayList.AsEnumerable().Cast(Of DataRow).Count.ToString()
                            dlBirthday.DataBind()
                        Else
                            lblBirthdayCount.Text = "0"
                        End If
                    Else
                        dvBirthDayDetalls.Visible = False
                    End If

                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_WorkAnniversary) Then
                        dtWorkAnniversaryList = GetWorkAnniversaryData()
                        If IsNothing(dtWorkAnniversaryList) = False AndAlso dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                            dlWorkAnniversary.DataSource = dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
                            lblWorkAnniversaryCount.Text = dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Count.ToString()
                            dlWorkAnniversary.DataBind()
                        Else
                            lblWorkAnniversaryCount.Text = "0"
                        End If
                    Else
                        dvEmpWorkAnniversary.Visible = False
                    End If

                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_NewlyHired) Then
                        If (Session("LoginBy") = Global.User.en_loginby.User) Then
                            dtNewEmployeeList = GetNewEmployeeData()
                            If dtNewEmployeeList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                                dlNewlyHired.DataSource = dtNewEmployeeList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
                                lblNewlyHiredCount.Text = dtNewEmployeeList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("Id") = 6).Count.ToString()
                                dlNewlyHired.DataBind()
                            Else
                                lblNewlyHiredCount.Text = "0"
                            End If
                            pnlNewlyHired.Visible = True
                        Else
                            pnlNewlyHired.Visible = False
                        End If
                    Else
                        pnlNewlyHired.Visible = False
                    End If


                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_UpcomingHolidays) Then
                        dtHolidayList = GetHolidayData()
                        If IsNothing(dtHolidayList) = False AndAlso dtHolidayList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                            lblUpcomingHolidaysCount.Text = dtHolidayList.Rows.Count
                            dlUpcomingHolidays.DataSource = dtHolidayList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
                            dlUpcomingHolidays.DataBind()
                        Else
                            lblUpcomingHolidaysCount.Text = "0"
                        End If
                    Else
                        dvUpcomingHolidays.Visible = False
                    End If

                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_TeamMembers) Then
                        dtTeamMemberList = GetTeamMemberData()
                        If IsNothing(dtTeamMemberList) = False AndAlso dtTeamMemberList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                            lblTeamMembersCount.Text = dtTeamMemberList.Rows.Count
                            dlTeamMembers.DataSource = dtTeamMemberList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
                            dlTeamMembers.DataBind()
                        Else
                            lblTeamMembersCount.Text = "0"
                        End If
                    Else
                        dvTeamMembers.Visible = False
                    End If

                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_EmpOnLeave) Then
                        dtEmployeeOnLeaveList = GetEmployeeOnLeaveData()
                        If IsNothing(dtEmployeeOnLeaveList) = False AndAlso dtEmployeeOnLeaveList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                            lblEmployeeOnLeaveCount.Text = dtEmployeeOnLeaveList.Rows.Count
                            dlEmployeeOnLeaveCount.DataSource = dtEmployeeOnLeaveList.AsEnumerable().Cast(Of DataRow).Take(3).CopyToDataTable()
                            dlEmployeeOnLeaveCount.DataBind()
                        Else
                            lblEmployeeOnLeaveCount.Text = "0"
                        End If
                    Else
                        dvEmpOnLeave.Visible = False
                    End If

                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_PendingTask) Then

                        dtPendingTaskList = GetPendingTaskData()

                        If IsNothing(dtPendingTaskList) = False AndAlso dtPendingTaskList.Rows.Count > 0 Then
                            pnlPendingTask.Visible = True
                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveClaimExpenseMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveClaimExpense.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveClaimExpenseMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveClaimExpenseCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLeaveMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveLeave.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLeaveMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()

                                If IsNothing(drRow) = False Then
                                    objlblApproveLeaveCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveOTApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveOTApplication.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveOTApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveOTApplicationCount.Text = drRow("totalcount")
                                End If
                            End If


                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveScoreCardMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveScoreCard.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveScoreCardMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveScoreCardCount.Text = drRow("totalcount")
                                End If
                            End If


                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkSetScoreCard.Visible = True

                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblSetScoreCardCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveUpdateProgressMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveUpdateProgress.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveUpdateProgressMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveUpdateProgressCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssessEmployeeCompetenceMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkAssessEmployeeCompetence.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveOTApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveOTApplicationCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssessEmployeeMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkAssessEmployee.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssessEmployeeMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblAssessEmployeeCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssetDeclarationT2ESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkAssetDeclaration.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.AssetDeclarationT2ESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblAssetDeclarationCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkMyAssessment.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblMyAssessmentCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyCompetenceAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkMyCompetenceAssessment.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyCompetenceAssessmentESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblMyCompetenceAssessmentCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeAssessmentMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkReviewEmployeeAssessment.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeAssessmentMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblReviewEmployeeAssessmentCount.Text = drRow("totalcount")
                                End If
                            End If



                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeCompetenceMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkReviewEmployeeCompetence.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ReviewEmployeeCompetenceMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblReviewEmployeeCompetenceCount.Text = drRow("totalcount")
                                End If
                            End If


                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkSetScoreCard.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.SetScoreCardESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblSetScoreCardCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.NonDisclosureDeclarationESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkNonDisclosureDeclaration.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.NonDisclosureDeclarationESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblNonDisclosureDeclarationCount.Text = drRow("totalcount")
                                End If
                            End If


                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveSalaryChangeMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then

                                lnkApproveSalaryChangeMSS.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveSalaryChangeMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveSalaryChangeMSSCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApprovePayslipPaymentMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApprovePayslipPaymentMSS.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApprovePayslipPaymentMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApprovePayslipPaymentMSSCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLoanApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveLoanApplicationMSS.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveLoanApplicationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveLoanApplicationMSSCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveCalibrationMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveCalibrationMSS.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveCalibrationMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveCalibrationMSSCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveStaffRequisitionMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkApproveStaffRequisitionMSS.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.ApproveStaffRequisitionMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblApproveStaffRequisitionMSSCount.Text = drRow("totalcount")
                                End If
                            End If
                            'Hemant (04 Sep 2021) -- Start
                            'ENHANCEMENT : OLD-465 - Dashboard Pending Task Enhancement - Training Evaluation.
                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyTrainingFeedbackESS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkMyTrainingFeedback.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.MyTrainingFeedbackESS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblMyTrainingFeedbackCount.Text = drRow("totalcount")
                                End If
                            End If

                            If dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.EvaluateEmployeeTrainingMSS) And x.Field(Of Integer)("totalcount") > 0).Count > 0 Then
                                lnkEvaluateEmployeeTraining.Visible = True
                                Dim drRow As DataRow = dtPendingTaskList.AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("ID") = CInt(clsDashboard_Class.enPendingTask.EvaluateEmployeeTrainingMSS) And x.Field(Of Integer)("totalcount") > 0).FirstOrDefault()
                                If IsNothing(drRow) = False Then
                                    objlblEvaluateEmployeeTrainingCount.Text = drRow("totalcount")
                                End If
                            End If
                            'Hemant (04 Sep 2021) -- End
                        End If
                    Else
                        pnlPendingTask.Visible = False
                    End If


                    'Pinkal (15-Sep-2023) -- Start
                    '(A1X-1284) Toyota - Configuration of Intranet links on the new UI.
                    If GetCompanyGroup().ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_IntranetLink) Then
                            Dim objIntranetLink As New clsdashboard_intranetlink
                            Dim dsIntranetLink As DataSet = objIntranetLink.GetList("List", CInt(Session("CompanyUnkId")), True, Nothing)
                            objIntranetLink = Nothing
                            dlIntranetLink.DataSource = dsIntranetLink.Tables(0)
                            dlIntranetLink.DataBind()
                        Else
                            dvIntranetLink.Visible = False
                        End If

                        'Pinkal (30-Sep-2023) -- Start
                        '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
                        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_VacancyStatus) AndAlso CInt(Session("U_UserID")) > 0 AndAlso dvVacancy.Visible Then
                            Dim objConfig As New clsConfigOptions
                            Dim mdtAsOnDate As Date = objConfig._CurrentDateAndTime.Date
                            objConfig = Nothing

                            Dim dsVacancy As DataSet = clsDashboard_Class.GetVacanciesFromCurrentFY(CDate(Session("fin_startdate")).Date, mdtAsOnDate.Date)
                            dlVacancy.DataSource = dsVacancy.Tables(0)
                            dlVacancy.DataBind()
                        Else
                            dvVacancy.Visible = False
                        End If
                        'Pinkal (30-Sep-2023) -- End

                    Else
                        dvIntranetLink.Visible = False
                        'Pinkal (30-Sep-2023) -- Start
                        '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
                        dvVacancy.Visible = False
                        'Pinkal (30-Sep-2023) -- End
                    End If
                    'Pinkal (15-Sep-2023) -- End


                    'Pinkal (14-Feb-2022) -- Start
                    'Enhancement TRA : TnA Module Enhancement for TRA.
                    If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_TnAAttendanceDetails) Then
                        GetTnADashBoardData()
                    Else
                        pnlTnADetails.Visible = False
                    End If
                    'Pinkal (14-Feb-2022) -- End


                    If CInt(Session("U_UserID")) > 0 Then

                        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_LeaveAnalysis) OrElse mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_StaffTurnOver) OrElse mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_SalaryAnalysis) Then
                            'Hemant (13 Oct 2023) -- [OrElse mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_SalaryAnalysis)]
                            'objDashboard_Class = New clsDashboard_Class(CStr(Session("Database_Name")), _
                            '                                                         CStr(Session("UserAccessModeSetting")), _
                            '                                                         CInt(Session("U_UserID")), _
                            '                                                         CInt(Session("Fin_year")), _
                            '                                                         CInt(Session("CompanyUnkId")), _
                            '                                                         Session("fin_startdate"), _
                            '                                                         Session("fin_enddate"), _
                            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                            '                                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         False, _
                            '                                                         0, _
                            '                                                         0, _
                            '                                                         0, _
                            '                                                         0, _
                            '                                                         0, _
                            '                                                         0, _
                            '                                             0, True, _
                            '                                                         False)

                            objDashboard_Class = New clsDashboard_Class(CStr(Session("Database_Name")), _
                                                                         CStr(Session("UserAccessModeSetting")), _
                                                                         CInt(Session("U_UserID")), _
                                                                         CInt(Session("Fin_year")), _
                                                                         CInt(Session("CompanyUnkId")), _
                                                                         Session("fin_startdate"), _
                                                                         Session("fin_enddate"), _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                          0, False, _
                                                                         False, _
                                                                         False)

                        End If

                        Dim xCategory As String() = Nothing
                        Dim jsSerializer As New JavaScriptSerializer

                        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_StaffTurnOver) Then


                            'Pinkal (09-Aug-2021)-- Start
                            'NMB New UI Enhancements.
                            'dtStaffList = objDashboard_Class.Staff_TO_Grid_Self_Service("grid")
                            strAdvanceFilter = ""
                            If CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                                blnApplyAccessFilter = True
                                strAdvanceFilter = ""

                            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                                Dim objEmp As New clsEmployee_Master
                                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                                blnApplyAccessFilter = False

                                Dim ar() As String = Session("EmpStaffTurnOverAllocation").ToString().Split(CChar(","))

                                If ar.Length > 0 Then

                                    For i As Integer = 0 To ar.Length - 1

                                        Select Case CInt(ar(i))
                                            Case enAllocation.BRANCH
                                                strAdvanceFilter &= "AND ADF.stationunkid = " & objEmp._Stationunkid & " "

                                            Case enAllocation.DEPARTMENT_GROUP
                                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objEmp._Deptgroupunkid & " "

                                            Case enAllocation.DEPARTMENT
                                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objEmp._Departmentunkid & " "

                                            Case enAllocation.SECTION_GROUP
                                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objEmp._Sectiongroupunkid & " "

                                            Case enAllocation.SECTION
                                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objEmp._Sectionunkid & " "

                                            Case enAllocation.UNIT_GROUP
                                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objEmp._Unitgroupunkid & " "

                                            Case enAllocation.UNIT
                                                strAdvanceFilter &= "AND ADF.unitunkid = " & objEmp._Unitunkid & " "

                                            Case enAllocation.TEAM
                                                strAdvanceFilter &= "AND ADF.teamunkid =" & objEmp._Teamunkid & " "

                                            Case enAllocation.JOB_GROUP
                                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objEmp._Jobgroupunkid & " "

                                            Case enAllocation.JOBS
                                                strAdvanceFilter &= "AND ADF.jobunkid =" & objEmp._Jobunkid & " "

                                            Case enAllocation.CLASS_GROUP
                                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objEmp._Classgroupunkid & " "

                                            Case enAllocation.CLASSES
                                                strAdvanceFilter &= "AND ADF.classunkid =" & objEmp._Classunkid & " "

                                            Case Else
                                                strAdvanceFilter &= ""

                                        End Select

                                    Next

                                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                                End If
                                objEmp = Nothing

                            End If

                            dtStaffList = objDashboard_Class.Staff_TO_Grid_Self_Service(blnApplyAccessFilter, strAdvanceFilter, "grid")

                            'Pinkal (09-Aug-2021) -- End

                            Dim Staffsource = New List(Of ClsChartData)
                            If dtStaffList.Columns.Contains("ViewId") Then
                                dtStaffList.Columns.Remove("ViewId")
                            End If

                            xCategory = dtStaffList.Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.Ordinal > 0).Select(Function(x) x.ColumnName).ToArray()

                            For Each dtrow As DataRow In dtStaffList.Rows
                                'Pinkal (01-Jun-2021)-- Start
                                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                                'Dim strflds As String() = dtrow.ItemArray.Select(Function(x) x.ToString()).ToArray()
                                'Dim lst As List(Of String)
                                'If strflds IsNot Nothing AndAlso strflds.Length > 0 Then
                                '    lst = strflds.ToList()
                                '    lst.RemoveAt(0)
                                'For index As Integer = 0 To lst.Count - 1
                                '    If IsDBNull(lst(index)) OrElse lst(index).Length <= 0 Then
                                '        lst(index) = 0
                                '                End If
                                'Next
                                'Staffsource.Add(New ClsChartData With {.name = dtrow("Particulars"), .data = lst.Select(Function(x) CInt(x)).ToArray()})
                                'End If
                                Dim lst As List(Of String) = dtrow.ItemArray.Select(Function(x) x.ToString()).Skip(1).ToList()
                                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                                    Staffsource.Add(New ClsChartData With {.name = dtrow("Particulars"), .data = lst.Select(Function(x) CDec(x)).ToArray()})
                                End If
                                lst.Clear()
                                lst = Nothing
                                'Pinkal (01-Jun-2021) -- End

                            Next

                            jsSerializer.MaxJsonLength = 2147483644
                            staffturnoverdata = jsSerializer.Serialize(Staffsource)
                            staffturnovercategory = jsSerializer.Serialize(xCategory)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "StaffChart", "getStaffTurnover('staff-turnover-container'," & staffturnovercategory & ",'Staff Turnover', " & staffturnoverdata & ");", True)
                        Else
                            pnlStaffTurnOverChart.Visible = False
                        End If

                        If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_LeaveAnalysis) Then


                            'Pinkal (09-Aug-2021)-- Start
                            'NMB New UI Enhancements.

                            'dsLeaveAnnalysisList = objDashboard_Class.Leave_Grid_SelfService("LeaveAnnalysis")

                            strAdvanceFilter = ""
                            If CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                                blnApplyAccessFilter = True
                                strAdvanceFilter = ""


                            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                                Dim objemp As New clsEmployee_Master
                                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                                blnApplyAccessFilter = False
                                strAdvanceFilter = ""

                                If blnApplyAccessFilter = False Then

                                    Dim ar() As String = Session("EmpOnLeaveAllocation").ToString().Split(CChar(","))

                                    If ar.Length > 0 Then

                                        For i As Integer = 0 To ar.Length - 1
                                            Select Case CInt(ar(i))
                                                Case enAllocation.BRANCH
                                                    strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                                                Case enAllocation.DEPARTMENT_GROUP
                                                    strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                                                Case enAllocation.DEPARTMENT
                                                    strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                                                Case enAllocation.SECTION_GROUP
                                                    strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                                                Case enAllocation.SECTION
                                                    strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                                                Case enAllocation.UNIT_GROUP
                                                    strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                                                Case enAllocation.UNIT
                                                    strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                                                Case enAllocation.TEAM
                                                    strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                                                Case enAllocation.JOB_GROUP
                                                    strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                                                Case enAllocation.JOBS
                                                    strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                                                Case enAllocation.CLASS_GROUP
                                                    strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                                                Case enAllocation.CLASSES
                                                    strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                                                Case Else
                                                    strAdvanceFilter &= ""

                                            End Select

                                        Next

                                        If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                                    End If

                                End If

                                objemp = Nothing
                            End If

                            dsLeaveAnnalysisList = objDashboard_Class.Leave_Grid_SelfService(blnApplyAccessFilter, strAdvanceFilter, "LeaveAnnalysis")

                            'Pinkal (09-Aug-2021) -- End

                            Dim LeaveAnnalysissource = New List(Of ClsChartData)
                            dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Columns.Remove("LId")
                            dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Columns.Remove("LCode")

                            xCategory = dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.Ordinal > 0).Select(Function(x) x.ColumnName).ToArray()


                            For Each dtrow As DataRow In dsLeaveAnnalysisList.Tables("LeaveAnnalysis").Rows
                                'Pinkal (01-Jun-2021)-- Start
                                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                                'Dim strflds As String() = dtrow.ItemArray.Select(Function(x) x.ToString()).ToArray()
                                'Dim lst As List(Of String) = Nothing
                                'If lst IsNot Nothing AndAlso lst.Count > 0 Then
                                '    lst = strflds.ToList()
                                '    lst.RemoveAt(0)
                                '    For index As Integer = 0 To lst.Count - 1
                                '        If IsDBNull(lst(index)) OrElse lst(index).Length <= 0 Then
                                '            lst(index) = 0
                                '                End If
                                '            Next
                                '    LeaveAnnalysissource.Add(New ClsChartData With {.name = dtrow("Lname"), .data = lst.Select(Function(x) CInt(x)).ToArray()})
                                'End If
                                Dim lst As List(Of String) = dtrow.ItemArray.Select(Function(x) x.ToString()).Skip(1).ToList()
                                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                                    LeaveAnnalysissource.Add(New ClsChartData With {.name = dtrow("Lname"), .data = lst.Select(Function(x) CDec(x)).ToArray()})
                                End If
                                lst.Clear()
                                lst = Nothing
                                'Pinkal (01-Jun-2021) -- End

                            Next

                            jsSerializer = New JavaScriptSerializer
                            jsSerializer.MaxJsonLength = 2147483644
                            leaveanalysisdata = jsSerializer.Serialize(LeaveAnnalysissource)
                            leaveanalysiscategory = jsSerializer.Serialize(xCategory)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "LeaveChart", "getLeaveData('leave-analysis-container'," & leaveanalysiscategory & ",'Total Leave Days', " & leaveanalysisdata & ");", True)
                        Else
                            pnlLeaveAnalysisChart.Visible = False
                        End If

                        'Hemant (13 Oct 2023) -- Start
                        'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                        If CBool(Session("AllowAccessSalaryAnalysis")) = True AndAlso mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_SalaryAnalysis) Then
                            dsSalaryAnalysisLIst = objDashboard_Class.Salary_Analysis("SalaryAnalysis", "")

                            Dim SalaryAnnalysissource = New List(Of ClsChartData)
                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("Pid") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("Pid")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("PCode") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("PCode")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("ST_Date") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("ST_Date")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("ED_Date") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("ED_Date")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("PStatusId") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("PStatusId")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("TCS") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns("TCS").ColumnName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmSlidingDashBoard", 406, "Total Bank Salary")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("TBS") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns("TBS").ColumnName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmSlidingDashBoard", 407, "Total Cash Salary")
                            End If

                            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("THS") Then
                                dsSalaryAnalysisLIst.Tables(0).Columns("THS").ColumnName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmSlidingDashBoard", 408, "Total Hold Salary")
                            End If

                            Dim outputTable As DataTable = New DataTable("SalaryAnalysis")
                            outputTable.Columns.Add(dsSalaryAnalysisLIst.Tables(0).Columns(0).ColumnName.ToString())

                            For Each inRow As DataRow In dsSalaryAnalysisLIst.Tables(0).Rows
                                Dim newColName As String = inRow(0).ToString()
                                outputTable.Columns.Add(newColName)
                            Next

                            For rCount As Integer = 1 To dsSalaryAnalysisLIst.Tables(0).Columns.Count - 1
                                Dim newRow As DataRow = outputTable.NewRow()
                                newRow(0) = dsSalaryAnalysisLIst.Tables(0).Columns(rCount).ColumnName.ToString()

                                For cCount As Integer = 0 To dsSalaryAnalysisLIst.Tables(0).Rows.Count - 1
                                    Dim colValue As String = dsSalaryAnalysisLIst.Tables(0).Rows(cCount)(rCount).ToString()
                                    newRow(cCount + 1) = colValue
                                Next

                                outputTable.Rows.Add(newRow)
                            Next

                            dsSalaryAnalysisLIst.Tables.Remove("SalaryAnalysis")
                            dsSalaryAnalysisLIst.Tables.Add(outputTable)

                            xCategory = dsSalaryAnalysisLIst.Tables(0).Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.Ordinal > 0).Select(Function(x) x.ColumnName).ToArray()

                            For Each dtrow As DataRow In dsSalaryAnalysisLIst.Tables("SalaryAnalysis").Rows

                                Dim lst As List(Of String) = dtrow.ItemArray.Select(Function(x) x.ToString()).Skip(1).ToList()
                                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                                    SalaryAnnalysissource.Add(New ClsChartData With {.name = dtrow("Pname"), .data = lst.Select(Function(x) CDec(x)).ToArray()})
                                End If
                                lst.Clear()
                                lst = Nothing

                            Next

                            jsSerializer = New JavaScriptSerializer
                            jsSerializer.MaxJsonLength = 2147483644
                            SalaryAnalysisData = jsSerializer.Serialize(SalaryAnnalysissource)
                            SalaryAnalysisCategory = jsSerializer.Serialize(xCategory)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SalaryChart", "getSalaryAnalysis('salary-analysis-container'," & SalaryAnalysisCategory & ",'Salary', " & SalaryAnalysisData & ");", True)
                        Else
                            pnlSalaryAnalysisChart.Visible = False
                        End If
                        'Hemant (13 Oct 2023) -- End


                    Else
                        pnlStaffTurnOverChart.Visible = False
                        pnlLeaveAnalysisChart.Visible = False
                        'Hemant (13 Oct 2023) -- Start
                        'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                        pnlSalaryAnalysisChart.Visible = False
                        'Hemant (13 Oct 2023) -- End

                    End If

                Else

                    If CInt(Session("U_UserID")) > 0 Then

                        If IsNothing(ViewState("staffturnovercategory")) = False Then
                            staffturnovercategory = ViewState("staffturnovercategory")
                        End If

                        If IsNothing(ViewState("staffturnoverdata")) = False Then
                            staffturnoverdata = ViewState("staffturnoverdata")
                        End If

                        If staffturnovercategory.Length > 0 AndAlso staffturnoverdata.Length > 0 Then
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "StaffChart", "getStaffTurnover('staff-turnover-container'," & staffturnovercategory & ",'Staff Turnover', " & staffturnoverdata & ");", True)
                        End If


                        If IsNothing(ViewState("leaveanalysiscategory")) = False Then
                            leaveanalysiscategory = ViewState("leaveanalysiscategory")
                        End If

                        If IsNothing(ViewState("leaveanalysisdata")) = False Then
                            leaveanalysisdata = ViewState("leaveanalysisdata")
                        End If

                        If leaveanalysiscategory.Length > 0 AndAlso leaveanalysisdata.Length > 0 Then
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "LeaveChart", "getLeaveData('leave-analysis-container'," & leaveanalysiscategory & ",'Leave Annalysis', " & leaveanalysisdata & ");", True)
                        End If

                        'Hemant (13 Oct 2023) -- Start
                        'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                        If IsNothing(ViewState("SalaryAnalysisCategory")) = False Then
                            SalaryAnalysisCategory = ViewState("SalaryAnalysisCategory")
                        End If

                        If IsNothing(ViewState("SalaryAnalysisData")) = False Then
                            SalaryAnalysisData = ViewState("SalaryAnalysisData")
                        End If
                        'Hemant (13 Oct 2023) -- End

                    End If

                    mdctDashBoardSetting = CType(Me.ViewState("dctDashBoardSetting"), Dictionary(Of Integer, Integer))

                End If

            Else

                If (Page.IsPostBack = False) Then
                    If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                        'Dim objemp As New clsEmployee_Master
                        'objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))
                        'lblWelcomeEmployee.Text = objemp._Firstname.ToUpper()
                        'objemp = Nothing
                        lblWelcomeEmployee.Text = Session("E_Firstname").ToString().ToUpper()

                    ElseIf (CInt(Session("U_UserID")) > 0 AndAlso CInt(Session("E_Employeeunkid")) <= 0) Then
                        lblWelcomeEmployee.Text = Session("UserName").ToUpper()

                    ElseIf (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0) Then
                        'Dim objemp As New clsEmployee_Master
                        'objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))
                        'lblWelcomeEmployee.Text = objemp._Firstname.ToUpper()
                        'objemp = Nothing
                        lblWelcomeEmployee.Text = Session("E_Firstname").ToString().ToUpper()
                    End If

                    'lblSummaryDate.Text = ConfigParameter._Object._CurrentDateAndTime.ToString("dd-MMM-yyyy")
                    lblSummaryDate.Text = GetCurrentDateTime().ToString("dd-MMM-yyyy")
                End If

                pnlStaffTurnOverChart.Visible = False
                pnlLeaveAnalysisChart.Visible = False
                'Hemant (13 Oct 2023) -- Start
                'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                pnlSalaryAnalysisChart.Visible = False
                'Hemant (13 Oct 2023) -- End
                dvEmpOnLeave.Visible = False
                pnlPendingTask.Visible = False
                dvTeamMembers.Visible = False
                dvUpcomingHolidays.Visible = False
                pnlNewlyHired.Visible = False
                dvEmpWorkAnniversary.Visible = False
                dvBirthDayDetalls.Visible = False
                lnkQApplyOT.Visible = False
                lnkQApplyLeave.Visible = False
                lnkQMyPayslip.Visible = False
                pnlTnADetails.Visible = False
                'S.SANDEEP |13-APR-2022| -- START
                'ISSUE/ENHANCEMENT : AC2-143
                lnkQRebate.Visible = False
                'S.SANDEEP |13-APR-2022| -- END

                dvVacancy.Visible = False

                'Pinkal (26-Jun-2022) -- Start
                'NMB Enhancement :(A1X-1028) NMB - Staff transfers application page.
                SetClockInClockOut()
                'Pinkal (26-Jun-2022) -- End

                'Pinkal (15-Sep-2023) -- Start
                '(A1X-1284) Toyota - Configuration of Intranet links on the new UI.
                If Session("SessionGoalStatusFromDashBoard") IsNot Nothing Then Session("SessionGoalStatusFromDashBoard") = False
                If Session("SessionProgressUpdateFromDashBoard") IsNot Nothing Then Session("SessionProgressUpdateFromDashBoard") = False
                dvIntranetLink.Visible = False
                lnkMyGoalStatus.Visible = False
                lnkMSSEmployeeHeadCountReport.Visible = False
                lnkViewMyProgressUpdateReport.Visible = False
                'Pinkal (15-Sep-2023) --End

            End If
            'S.SANDEEP |20-JAN-2022| -- END
            'Pinkal (01-Jun-2021) -- End


        Catch ex As Exception
            'S.SANDEEP |24-JAN-2022| -- START
            'ISSUE : Showing a modal dialog box
            'MsgBox("Error in page_load Event" & ex.Message)
            'Throw ex
            DisplayMessage.DisplayError(ex, Me)
            'S.SANDEEP |24-JAN-2022| -- END            
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'S.SANDEEP |20-JAN-2022| -- START
            ''Pinkal (01-Jun-2021)-- Start
            ''New UI Self Service Enhancement : Working on New UI Dashboard Settings.

            'Me.ViewState("dctDashBoardSetting") = mdctDashBoardSetting


            'If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_StaffTurnOver) Then
            '    Me.ViewState("staffturnovercategory") = staffturnovercategory
            '    Me.ViewState("staffturnoverdata") = staffturnoverdata
            '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "StaffChart", "getStaffTurnover('staff-turnover-container'," & staffturnovercategory & ",'Staff Turnover', " & staffturnoverdata & ");", True)
            'End If

            'If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_LeaveAnalysis) Then
            '    Me.ViewState("leaveanalysiscategory") = leaveanalysiscategory
            '    Me.ViewState("leaveanalysisdata") = leaveanalysisdata
            '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "LeaveChart", "getLeaveData('leave-analysis-container'," & leaveanalysiscategory & ",'Leave Annalysis', " & leaveanalysisdata & ");", True)
            'End If
            ''Pinkal (01-Jun-2021) -- End

            'If GetCompanyGroup().ToUpper() <> "NMB PLC" AndAlso GetCompanyGroup().ToUpper() <> "TANZANIA REVENUE AUTHORITY" AndAlso GetCompanyGroup().ToUpper() <> "SIMBA GROUP" Then

            If Session("CompanyGroupName").ToString().ToUpper() <> "NMB PLC" AndAlso Session("CompanyGroupName").ToString().ToUpper() <> "TANZANIA REVENUE AUTHORITY" _
               AndAlso Session("CompanyGroupName").ToString().ToUpper() <> "SIMBA GROUP" Then

                Me.ViewState("dctDashBoardSetting") = mdctDashBoardSetting

                If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_StaffTurnOver) Then
                    Me.ViewState("staffturnovercategory") = staffturnovercategory
                    Me.ViewState("staffturnoverdata") = staffturnoverdata
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "StaffChart", "getStaffTurnover('staff-turnover-container'," & staffturnovercategory & ",'Staff Turnover', " & staffturnoverdata & ");", True)
                End If

                If mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_LeaveAnalysis) Then
                    Me.ViewState("leaveanalysiscategory") = leaveanalysiscategory
                    Me.ViewState("leaveanalysisdata") = leaveanalysisdata
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "LeaveChart", "getLeaveData('leave-analysis-container'," & leaveanalysiscategory & ",'Leave Annalysis', " & leaveanalysisdata & ");", True)
                End If
                'Pinkal (01-Jun-2021) -- End

                'Hemant (13 Oct 2023) -- Start
                'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
                If CBool(Session("AllowAccessSalaryAnalysis")) = True AndAlso mdctDashBoardSetting.ContainsKey(enDashboardItems.Crd_SalaryAnalysis) Then
                    Me.ViewState("SalaryAnalysisCategory") = SalaryAnalysisCategory
                    Me.ViewState("SalaryAnalysisData") = SalaryAnalysisData
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SalaryChart", "getSalaryAnalysis('salary-analysis-container'," & SalaryAnalysisCategory & ",'Salary', " & SalaryAnalysisData & ");", True)
                End If
                'Hemant (13 Oct 2023) -- End

            End If
            'S.SANDEEP |20-JAN-2022| -- END



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Public Function GetPendingTaskData() As DataTable
        Dim dsPendingTaskList As New DataSet()
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim intEmpid As Integer = 0
        Dim intUser As Integer = 0
        Dim blnApproveSalaryChangeMSS As Boolean = False
        'Pinkal (09-Aug-2021)-- Start
        'NMB New UI Enhancements.
        Dim dtTable As DataTable = Nothing
        'Pinkal (09-Aug-2021) -- End
        Try

            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                intEmpid = CInt(Session("E_Employeeunkid"))
                intUser = 0
                blnApplyAccessFilter = False

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
                intEmpid = 0
                intUser = CInt(Session("U_UserID"))
                blnApplyAccessFilter = True
                blnApproveSalaryChangeMSS = CBool(Session("AllowToViewSalaryChangeList"))

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
                intEmpid = CInt(Session("E_Employeeunkid"))
                intUser = CInt(Session("U_UserID"))
                blnApplyAccessFilter = True
                blnApproveSalaryChangeMSS = CBool(Session("AllowToViewSalaryChangeList"))
            End If




            dsPendingTaskList = clsDashboard_Class.PendingTask(CStr(Session("Database_Name")), _
                                                               CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intUser, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, intEmpid, _
                                                               "", "", True, True, True, True, True, True, True, True, True, True, True, _
                                                               True, True, True, True, blnApproveSalaryChangeMSS, True, True, True, True, True, True, True, True, True, _
                                                               True, True)


            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.
            dtTable = dsPendingTaskList.Tables(0)
            'Pinkal (09-Aug-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    Public Function GetEmployeeOnLeaveData() As DataTable
        Dim dsEmployeeOnLeaveList As New DataSet()
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim intEmpid As Integer = 0
        Dim intUser As Integer = 0
        Dim strNoimage As String = ImageToBase64()
        Dim dtTable As DataTable = Nothing

        Try



            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            'If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
            '    Dim objemp As New clsEmployee_Master
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpOnLeaveAllocation"))
            '        Case enAllocation.BRANCH
            '            strAdvanceFilter = "ADF.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strAdvanceFilter = "ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strAdvanceFilter = "ADF.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strAdvanceFilter = "ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strAdvanceFilter = "ADF.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strAdvanceFilter = "ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strAdvanceFilter = "ADF.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strAdvanceFilter = "ADF.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strAdvanceFilter = ""
            '    End Select


            '    blnApplyAccessFilter = False
            '    strORQueryForUserAccess = ""
            '    intEmpid = CInt(Session("E_Employeeunkid"))
            '    objemp = Nothing

            'ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    blnApplyAccessFilter = True
            '    strAdvanceFilter = ""
            '    strORQueryForUserAccess = ""
            '    intUser = CInt(Session("U_UserID"))

            'ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    Dim objemp As New clsEmployee_Master
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpOnLeaveAllocation"))
            '        Case enAllocation.BRANCH
            '            strORQueryForUserAccess = "Trf_AS.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strORQueryForUserAccess = "Trf_AS.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strORQueryForUserAccess = "Trf_AS.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strORQueryForUserAccess = "Trf_AS.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strORQueryForUserAccess = "Trf_AS.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strORQueryForUserAccess = "Trf_AS.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strORQueryForUserAccess = ""
            '    End Select

            '    blnApplyAccessFilter = False
            '    strAdvanceFilter = ""
            '    intEmpid = CInt(Session("E_Employeeunkid"))
            '    objemp = Nothing
            '    intUser = CInt(Session("U_UserID"))

            'End If
            Dim objemp As New clsEmployee_Master

            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intEmpid = CInt(Session("E_Employeeunkid"))

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                blnApplyAccessFilter = True
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intUser = CInt(Session("U_UserID"))

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intEmpid = CInt(Session("E_Employeeunkid"))
                intUser = CInt(Session("U_UserID"))

            End If

            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpOnLeaveAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If

            objemp = Nothing

            'Pinkal (09-Aug-2021) -- End



            dsEmployeeOnLeaveList = clsDashboard_Class.EmployeeOnLeave(CStr(Session("Database_Name")), _
                                                               CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intUser, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, _
                                                               True, strNoimage, strAdvanceFilter, strORQueryForUserAccess)


            dtTable = dsEmployeeOnLeaveList.Tables(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    Public Function GetTeamMemberData() As DataTable
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim strNoimage As String = ImageToBase64()
        Dim dsTeamMemberList As New DataSet()
        Dim objemp As New clsEmployee_Master
        Dim dtTable As DataTable = Nothing
        Try

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            'If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpTeamMembersAllocation"))
            '        Case enAllocation.BRANCH
            '            strAdvanceFilter = "ADF.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strAdvanceFilter = "ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strAdvanceFilter = "ADF.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strAdvanceFilter = "ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strAdvanceFilter = "ADF.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strAdvanceFilter = "ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strAdvanceFilter = "ADF.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strAdvanceFilter = "ADF.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strAdvanceFilter = ""
            '    End Select

            'ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    blnApplyAccessFilter = True
            '    strAdvanceFilter = ""
            '    strORQueryForUserAccess = ""

            'ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpTeamMembersAllocation"))
            '        Case enAllocation.BRANCH
            '            strORQueryForUserAccess = "Trf_AS.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strORQueryForUserAccess = "Trf_AS.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strORQueryForUserAccess = "Trf_AS.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strORQueryForUserAccess = "Trf_AS.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strORQueryForUserAccess = "Trf_AS.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strORQueryForUserAccess = "Trf_AS.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strORQueryForUserAccess = ""
            '    End Select

            '    blnApplyAccessFilter = False
            '    strAdvanceFilter = ""
            'End If


            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                blnApplyAccessFilter = True
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            End If

            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpTeamMembersAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If



            'Pinkal (09-Aug-2021) -- End

            dsTeamMemberList = clsDashboard_Class.TeamMembers(CStr(Session("Database_Name")), _
                                                               CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("U_UserID")), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, True, True, strNoimage, strAdvanceFilter, strORQueryForUserAccess)

            dtTable = dsTeamMemberList.Tables(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objemp = Nothing
        End Try
        Return dtTable
    End Function

    Public Function GetHolidayData() As DataTable
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim dtTable As DataTable = Nothing
        Try


            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            Dim objemp As New clsEmployee_Master

            Dim dsHolidayList As New DataSet()
            Dim intEmpid As Integer = 0
            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then

                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                intEmpid = CInt(Session("E_Employeeunkid"))
                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
                intEmpid = -1
                blnApplyAccessFilter = True

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))
                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                'intEmpid = CInt(Session("E_Employeeunkid"))
            End If


            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpOnLeaveAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If

            objemp = Nothing

            'Pinkal (09-Aug-2021) -- End


            dsHolidayList = clsDashboard_Class.UpcomingHolidays(CStr(Session("Database_Name")), _
                                                   CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("U_UserID")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, strAdvanceFilter, strORQueryForUserAccess, intEmpid)

            dtTable = dsHolidayList.Tables(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    Public Function GetNewEmployeeData() As DataTable
        Dim blnApplyAccessFilter As Boolean = True
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim strNoimage As String = ImageToBase64()
        Dim dsNewEmployeeList As New DataSet()
        Dim dtTable As DataTable = Nothing
        Try

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.
            Dim objemp As New clsEmployee_Master

            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                blnApplyAccessFilter = True
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            End If

            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpTeamMembersAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If
            objemp = Nothing

            'Pinkal (09-Aug-2021) -- End

            dsNewEmployeeList = clsDashboard_Class.EmployeeDatesCountForSelfService(CStr(Session("Database_Name")), _
                                                               CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("U_UserID")), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, True, strNoimage, strAdvanceFilter, strORQueryForUserAccess, False, False, True)


            dtTable = dsNewEmployeeList.Tables(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return dtTable
    End Function

    Public Function GetWorkAnniversaryData() As DataTable
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim intEmpid As Integer = 0
        Dim strNoimage As String = ImageToBase64()
        Dim dsWorkAnniversaryList As New DataSet()
        Dim objemp As New clsEmployee_Master
        Dim dtTable As DataTable = Nothing
        Try

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            'If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpWorkAnniversaryAllocation"))
            '        Case enAllocation.BRANCH
            '            strAdvanceFilter = "ADF.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strAdvanceFilter = "ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strAdvanceFilter = "ADF.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strAdvanceFilter = "ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strAdvanceFilter = "ADF.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strAdvanceFilter = "ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strAdvanceFilter = "ADF.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strAdvanceFilter = "ADF.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strAdvanceFilter = ""
            '    End Select

            '    blnApplyAccessFilter = False
            '    strORQueryForUserAccess = ""
            '    objemp = Nothing
            'ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    blnApplyAccessFilter = True
            '    strAdvanceFilter = ""
            '    strORQueryForUserAccess = ""

            'ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    objemp = New clsEmployee_Master
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpWorkAnniversaryAllocation"))
            '        Case enAllocation.BRANCH
            '            strORQueryForUserAccess = "Trf_AS.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strORQueryForUserAccess = "Trf_AS.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strORQueryForUserAccess = "Trf_AS.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strORQueryForUserAccess = "Trf_AS.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strORQueryForUserAccess = "Trf_AS.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strORQueryForUserAccess = "Trf_AS.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strORQueryForUserAccess = ""
            '    End Select
            'End If


            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                blnApplyAccessFilter = True
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            End If

            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpWorkAnniversaryAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If

            'Pinkal (09-Aug-2021) -- End


            'Pinkal (01-Jun-2021)-- Start
            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            dsWorkAnniversaryList = clsDashboard_Class.EmployeeDatesCountForSelfService(CStr(Session("Database_Name")), _
                                                   CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("U_UserID")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, True, strNoimage, strAdvanceFilter, strORQueryForUserAccess, False, True)


            dtTable = dsWorkAnniversaryList.Tables(0)
            'Pinkal (01-Jun-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objemp = Nothing
        End Try
        Return dtTable
    End Function

    Public Function GetBirthdayData() As DataTable
        Dim dsBirthdayList As New DataSet()
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim intEmpid As Integer = 0
        Dim strNoimage As String = ImageToBase64()
        Dim dtTable As DataTable = Nothing

        Try


            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            'If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
            '    Dim objemp As New clsEmployee_Master
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpBirthdayAllocation"))
            '        Case enAllocation.BRANCH
            '            strAdvanceFilter = "ADF.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strAdvanceFilter = "ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strAdvanceFilter = "ADF.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strAdvanceFilter = "ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strAdvanceFilter = "ADF.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strAdvanceFilter = "ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strAdvanceFilter = "ADF.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strAdvanceFilter = "ADF.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strAdvanceFilter = ""
            '    End Select

            '    blnApplyAccessFilter = False
            '    strORQueryForUserAccess = ""
            '    intEmpid = CInt(Session("E_Employeeunkid"))
            '    objemp = Nothing

            'ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    blnApplyAccessFilter = True
            '    strAdvanceFilter = ""
            '    strORQueryForUserAccess = ""

            'ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
            '    Dim objemp As New clsEmployee_Master
            '    objemp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(Session("E_Employeeunkid"))

            '    Select Case CInt(Session("EmpBirthdayAllocation"))
            '        Case enAllocation.BRANCH
            '            strORQueryForUserAccess = "Trf_AS.stationunkid = " & objemp._Stationunkid & " "
            '        Case enAllocation.DEPARTMENT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.deptgroupunkid = " & objemp._Deptgroupunkid & " """
            '        Case enAllocation.DEPARTMENT
            '            strORQueryForUserAccess = "Trf_AS.departmentunkid = " & objemp._Departmentunkid & ""
            '        Case enAllocation.SECTION_GROUP
            '            strORQueryForUserAccess = "Trf_AS.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "
            '        Case enAllocation.SECTION
            '            strORQueryForUserAccess = "Trf_AS.sectionunkid = " & objemp._Sectionunkid & " "
            '        Case enAllocation.UNIT_GROUP
            '            strORQueryForUserAccess = "Trf_AS.unitgroupunkid = " & objemp._Unitgroupunkid & " "
            '        Case enAllocation.UNIT
            '            strORQueryForUserAccess = "Trf_AS.unitunkid = " & objemp._Unitunkid & " "
            '        Case enAllocation.TEAM
            '            strORQueryForUserAccess = "Trf_AS.teamunkid =" & objemp._Teamunkid & " "
            '        Case Else
            '            strORQueryForUserAccess = ""
            '    End Select

            '    blnApplyAccessFilter = False
            '    strAdvanceFilter = ""
            '    intEmpid = CInt(Session("E_Employeeunkid"))
            '    objemp = Nothing
            'End If

            Dim objemp As New clsEmployee_Master

            If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) <= 0) Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intEmpid = CInt(Session("E_Employeeunkid"))

            ElseIf CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                blnApplyAccessFilter = True
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then

                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intEmpid = CInt(Session("E_Employeeunkid"))

            End If

            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpBirthdayAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If

            objemp = Nothing

            'Pinkal (09-Aug-2021) -- End


            dsBirthdayList = clsDashboard_Class.EmployeeDatesCountForSelfService(CStr(Session("Database_Name")), _
                                                               CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("U_UserID")), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, True, strNoimage, strAdvanceFilter, strORQueryForUserAccess, True)


            dtTable = dsBirthdayList.Tables(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    'Sohail (12 Mar 2015) -- Start
    'ENHANCEMENT
    <WebMethod()> Public Shared Function LoadFavouritesIcon() As String
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim ds As System.Data.DataSet
        Dim strQ As String
        Try

            strQ = "SELECT  ISNULL(usermenuunkid, " & CInt(HttpContext.Current.Session("U_UserID")) & ") AS usermenuunkid  " & _
                          ", ISNULL(cfuser_menu.userunkid, 0) AS userunkid " & _
                          ", ISNULL(cfuser_menu.employeeunkid, " & CInt(HttpContext.Current.Session("E_Employeeunkid")) & ") AS employeeunkid " & _
                          ", A.menuunkid " & _
                          ", A.reportunkid " & _
                          ", ISNULL(cfuser_menu.isfavourite, 0) AS isfavourite " & _
                          ", ISNULL(cfuser_menu.isactive, 1) AS isactive " & _
                    "FROM    ( SELECT    ISNULL(cfmenu_master.menuunkid, 0) AS menuunkid  " & _
                                      ", 0 AS reportunkid " & _
                                      ", ISNULL(cfmenu_master.isactive, 1) AS isactive " & _
                              "FROM      hrmsConfiguration..cfmenu_master " & _
                              "WHERE     cfmenu_master.isactive = 1 " & _
                              "UNION ALL " & _
                              "SELECT    0 AS menuunkid  " & _
                                      ", cfreport_master.reportunkid " & _
                                      ", ISNULL(cfreport_master.isactive, 1) AS isactive " & _
                              "FROM      hrmsConfiguration..cfreport_master " & _
                              "WHERE     ISNULL(cfreport_master.isactive, 1) = 1 " & _
                            ") AS A " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_menu ON A.menuunkid = cfuser_menu.menuunkid " & _
                                                                        "AND cfuser_menu.reportunkid = A.reportunkid " & _
                                                                        "AND ISNULL(cfuser_menu.isactive, 1) = 1 "

            If HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then
                strQ &= "AND ISNULL(userunkid, " & CInt(HttpContext.Current.Session("U_UserID")) & ") = " & CInt(HttpContext.Current.Session("U_UserID")) & " "
            Else
                strQ &= "AND ISNULL(employeeunkid, " & CInt(HttpContext.Current.Session("E_Employeeunkid")) & ") = " & CInt(HttpContext.Current.Session("E_Employeeunkid")) & " "
            End If

            ds = clsDataOpr.WExecQuery(strQ, "List")

            If clsDataOpr.ErrorMessage <> "" Then
                Throw New Exception(clsDataOpr.ErrorMessage)
            End If

            Return ds.GetXml()

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Sohail (12 Mar 2015) -- End

    'Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

    '    If e.Row.RowIndex < 0 Then Exit Sub

    '    If e.Row.Cells(1).Text.Trim <> "" Then
    '        e.Row.Cells(1).Text = eZeeDate.convertDate(e.Row.Cells(1).Text).Date
    '    End If
    '    If e.Row.Cells(2).Text.Trim <> "" Then
    '        e.Row.Cells(2).Text = eZeeDate.convertDate(e.Row.Cells(2).Text).Date
    '    End If

    'End Sub

    <WebMethod()> _
    Public Shared Function SetPerfEvaluationOrder(ByVal ival As String) As String
        Dim strMsg As String = String.Empty
        Dim strValue As String = ""
        'S.SANDEEP |04-JAN-2021| -- START
        'ISSUE/ENHANCEMENT : COMPETENCIES
        Dim iScrOptionId As String = ""
        'S.SANDEEP |04-JAN-2021| -- END

        'S.SANDEEP |09-FEB-2021| -- START
        'ISSUE/ENHANCEMENT : Competencies Period Changes
        Dim iInstructions As String = ""
        'S.SANDEEP |09-FEB-2021| -- END
        Try
            strValue = HttpContext.Current.Session("OrgPerf_EvaluationOrder")
            'S.SANDEEP |04-JAN-2021| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            iScrOptionId = HttpContext.Current.Session("OrgScoringOptionId")
            'S.SANDEEP |04-JAN-2021| -- END            

            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            iInstructions = HttpContext.Current.Session("OrgAssessment_Instructions")
            'S.SANDEEP |09-FEB-2021| -- END
            If CInt(ival) = 1 Then
                If strValue IsNot Nothing AndAlso strValue.Trim.Length > 0 Then
                    If Array.IndexOf(strValue.Split(CChar("|")), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <> -1 Then
                        Dim arr As String() = strValue.Split(CChar("|"))
                        arr = arr.Select(Function(s) s.Replace(CInt(enEvaluationOrder.PE_BSC_SECTION).ToString(), "0")).ToArray()
                        strValue = String.Join("|", arr)
                    ElseIf Array.IndexOf(strValue.Split(CChar("|")), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <= -1 Then
                        Dim arr As String() = strValue.Split(CChar("|"))
                        arr = arr.Select(Function(s) s.Replace(CInt(enEvaluationOrder.PE_BSC_SECTION).ToString(), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString())).ToArray()
                        strValue = String.Join("|", arr)
                    End If
                End If
                HttpContext.Current.Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE
                'S.SANDEEP |04-JAN-2021| -- START
                'ISSUE/ENHANCEMENT : COMPETENCIES
                iScrOptionId = HttpContext.Current.Session("CmptScoringOptionId")
                'S.SANDEEP |04-JAN-2021| -- END

                'S.SANDEEP |09-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Period Changes
                iInstructions = HttpContext.Current.Session("CompetenciesAssess_Instructions")
                'S.SANDEEP |09-FEB-2021| -- END
            ElseIf CInt(ival) = 0 Then
                strValue = HttpContext.Current.Session("OrgPerf_EvaluationOrder")
                'S.SANDEEP |04-JAN-2021| -- START
                'ISSUE/ENHANCEMENT : COMPETENCIES
                iScrOptionId = HttpContext.Current.Session("OrgScoringOptionId")
                'S.SANDEEP |04-JAN-2021| -- END

                'S.SANDEEP |09-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Period Changes
                iInstructions = HttpContext.Current.Session("OrgAssessment_Instructions")
                'S.SANDEEP |09-FEB-2021| -- END

                If CBool(HttpContext.Current.Session("RunCompetenceAssessmentSeparately")) = True Then HttpContext.Current.Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD
            End If
            HttpContext.Current.Session("Perf_EvaluationOrder") = strValue
            'S.SANDEEP |04-JAN-2021| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            HttpContext.Current.Session("ScoringOptionId") = iScrOptionId
            'S.SANDEEP |04-JAN-2021| -- END            

            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            HttpContext.Current.Session("Assessment_Instructions") = iInstructions
            'S.SANDEEP |09-FEB-2021| -- END

            strMsg = strValue
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("U_UserID")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("E_Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
        Return strMsg
    End Function


    'Pinkal (25-Jan-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    Private Function GetCompanyGroup() As String
        Dim mstrGroupName As String = ""
        Try
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroupName = objGroup._Groupname
            objGroup = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mstrGroupName
    End Function
    'Pinkal (25-Jan-2022) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Public Function GetTnATodayEmployeeData() As DataTable
        Dim dsEmpTnADetailList As New DataSet()
        Dim blnApplyAccessFilter As Boolean = False
        Dim strAdvanceFilter As String = ""
        Dim strORQueryForUserAccess As String = ""
        Dim intUser As Integer = 0
        Dim intEmpid As Integer = 0
        Dim dtTable As DataTable = Nothing
        Try

            Dim objConfig As New clsConfigOptions
            Dim dtCurrentDate As Date = objConfig._CurrentDateAndTime
            objConfig = Nothing

            Dim objemp As New clsEmployee_Master
            If CInt(Session("E_Employeeunkid")) <= 0 AndAlso CInt(Session("U_UserID")) > 0 Then
                blnApplyAccessFilter = True
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intUser = CInt(Session("U_UserID"))

            ElseIf CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0 Then
                objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("E_Employeeunkid"))

                blnApplyAccessFilter = False
                strAdvanceFilter = ""
                strORQueryForUserAccess = ""
                intEmpid = CInt(Session("E_Employeeunkid"))
                intUser = CInt(Session("U_UserID"))
            End If

            If blnApplyAccessFilter = False Then

                Dim ar() As String = Session("EmpTnADetailsAllocation").ToString().Split(CChar(","))

                If ar.Length > 0 Then

                    For i As Integer = 0 To ar.Length - 1
                        Select Case CInt(ar(i))
                            Case enAllocation.BRANCH
                                strAdvanceFilter &= "AND ADF.stationunkid = " & objemp._Stationunkid & " "

                            Case enAllocation.DEPARTMENT_GROUP
                                strAdvanceFilter &= "AND ADF.deptgroupunkid = " & objemp._Deptgroupunkid & " "

                            Case enAllocation.DEPARTMENT
                                strAdvanceFilter &= "AND ADF.departmentunkid = " & objemp._Departmentunkid & " "

                            Case enAllocation.SECTION_GROUP
                                strAdvanceFilter &= "AND ADF.sectiongroupunkid = " & objemp._Sectiongroupunkid & " "

                            Case enAllocation.SECTION
                                strAdvanceFilter &= "AND ADF.sectionunkid = " & objemp._Sectionunkid & " "

                            Case enAllocation.UNIT_GROUP
                                strAdvanceFilter &= "AND ADF.unitgroupunkid = " & objemp._Unitgroupunkid & " "

                            Case enAllocation.UNIT
                                strAdvanceFilter &= "AND ADF.unitunkid = " & objemp._Unitunkid & " "

                            Case enAllocation.TEAM
                                strAdvanceFilter &= "AND ADF.teamunkid =" & objemp._Teamunkid & " "

                            Case enAllocation.JOB_GROUP
                                strAdvanceFilter &= "AND ADF.jobgroupunkid =" & objemp._Jobgroupunkid & " "

                            Case enAllocation.JOBS
                                strAdvanceFilter &= "AND ADF.jobunkid =" & objemp._Jobunkid & " "

                            Case enAllocation.CLASS_GROUP
                                strAdvanceFilter &= "AND ADF.classgroupunkid =" & objemp._Classgroupunkid & " "

                            Case enAllocation.CLASSES
                                strAdvanceFilter &= "AND ADF.classunkid =" & objemp._Classunkid & " "

                            Case Else
                                strAdvanceFilter &= ""

                        End Select

                    Next

                    If strAdvanceFilter.Trim.Length > 0 Then strAdvanceFilter = strAdvanceFilter.Substring(3)

                End If

            End If

            objemp = Nothing

            dsEmpTnADetailList = clsDashboard_Class.GetTodayTnADetails(CStr(Session("Database_Name")), _
                                                             CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intUser, _
                                                              dtCurrentDate, dtCurrentDate, _
                                                             CStr(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, _
                                                              strAdvanceFilter, strORQueryForUserAccess)


            dtTable = dsEmpTnADetailList.Tables(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    Private Sub GetTnADashBoardData()
        Try
            Dim dtTnADetails As DataTable = GetTnATodayEmployeeData()
            If IsNothing(dtTnADetails) = False AndAlso dtTnADetails.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                Dim dr As DataRow() = Nothing
                dr = dtTnADetails.Select("Status = '" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 74, "PR") & "'")
                If dr IsNot Nothing AndAlso dr.Length > 0 Then
                    LblTotalPresentCount.Text = dr.Length.ToString()
                Else
                    LblTotalPresentCount.Text = "0"
                End If
                dr = Nothing

                dr = dtTnADetails.Select("Status = '" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 72, "AB") & "'")
                If dr IsNot Nothing AndAlso dr.Length > 0 Then
                    LblAbsentCount.Text = dr.Length.ToString()
                Else
                    LblAbsentCount.Text = ""
                End If
                dr = Nothing

                dr = dtTnADetails.Select("leavetypeunkid > 0")
                If dr IsNot Nothing AndAlso dr.Length > 0 Then
                    LblTotalLeaveCount.Text = dr.Length.ToString()
                Else
                    LblTotalLeaveCount.Text = "0"
                End If
                dr = Nothing

                dr = dtTnADetails.Select("Status = '" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 73, "OFF") & "'")
                If dr IsNot Nothing AndAlso dr.Length > 0 Then
                    LblTotalDayOffCount.Text = dr.Length.ToString()
                Else
                    LblTotalDayOffCount.Text = "0"
                End If
                dr = Nothing

                dr = dtTnADetails.Select("Status = '" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 71, "HL") & "'")
                If dr IsNot Nothing AndAlso dr.Length > 0 Then
                    LblTotalHolidayCount.Text = dr.Length.ToString()
                Else
                    LblTotalHolidayCount.Text = "0"
                End If
                dr = Nothing

                gvTnADetails.DataSource = dtTnADetails
                gvTnADetails.DataBind()

            Else
                'lblAbsentCount.Text = "0"
                'lblPresentCount.Text = "0"
                'lblLeaveCount.Text = "0"
                'lblDayOffCount.Text = "0"
                'lblholidayCount.Text = "0"
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (14-Feb-2022) -- End

    'Pinkal (26-Jun-2022) -- Start
    'NMB Enhancement :(A1X-1028) NMB - Staff transfers application page.
    Private Sub SetClockInClockOut()
        Dim objLogin As New clslogin_Tran
        Try
            'If GetCompanyGroup().ToUpper() = "NMB PLC" Then

            If Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then

                If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0) OrElse CInt(Session("E_Employeeunkid")) > 0 Then

                    Dim mintEmployeeUnkId As Integer = CInt(Session("E_Employeeunkid"))
                    objLogin._Employeeunkid = mintEmployeeUnkId

                    If objLogin.GetLoginType(mintEmployeeUnkId) = 1 Then
                        If IsDBNull(objLogin._checkintime) = False AndAlso objLogin._checkintime <> Nothing Then
                            If DateDiff(DateInterval.Hour, objLogin._checkintime, Basepage.GetCurrentDateTime) >= 20 Then
                                objLogin._Logindate = Basepage.GetCurrentDateTime.Date
                                btnCheckIn.Visible = True
                                btnCheckOut.Visible = False
                            Else
                                btnCheckIn.Visible = False
                                btnCheckOut.Visible = True
                            End If   '  If DateDiff(DateInterval.Hour, objLogin._checkintime, Basepage.GetCurrentDateTime) >= 20 Then
                        Else
                            objLogin._Logindate = Basepage.GetCurrentDateTime.Date
                            btnCheckIn.Visible = True
                            btnCheckOut.Visible = False
                        End If     'If IsDBNull(objLogin._checkintime) = False AndAlso objLogin._checkintime <> Nothing Then
                    Else
                        btnCheckIn.Visible = True
                        btnCheckOut.Visible = False
                    End If  ' If objLogin.GetLoginType(mintEmployeeUnkId) = 1 Then
                Else
                    btnCheckIn.Visible = False
                    btnCheckOut.Visible = False
                End If  '  If (CInt(Session("E_Employeeunkid")) > 0 AndAlso CInt(Session("U_UserID")) > 0) OrElse CInt(Session("E_Employeeunkid")) > 0 Then
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLogin = Nothing
        End Try
    End Sub
    'Pinkal (26-Jun-2022) -- End

    'Hemant (13 Oct 2023) -- Start
    'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            dsList = (New clsDashboard_Class).Fill_Divison_Combo("List")
            With cboSDivision
                .DataTextField = "DValue"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (13 Oct 2023) -- End


#End Region

#Region " Button's Events "

    Protected Sub btnShowAllBirthday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAllBirthday.Click
        Try
            Dim dtBirthdayList As New DataTable()
            dtBirthdayList = GetBirthdayData()
            If IsNothing(dtBirthdayList) = False AndAlso dtBirthdayList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlPopupBirthday.DataSource = dtBirthdayList
                dlPopupBirthday.DataBind()
                dlPopupBirthday.Visible = True
            End If
            popupShowAll.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnShowallNewlyHired_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowallNewlyHired.Click
        Try
            Dim dtNewlyHiredList As New DataTable()
            dtNewlyHiredList = GetNewEmployeeData()
            If IsNothing(dtNewlyHiredList) = False AndAlso dtNewlyHiredList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlPopupNewlyHired.DataSource = dtNewlyHiredList
                dlPopupNewlyHired.DataBind()
                dlPopupNewlyHired.Visible = True
            End If
            popupShowAll.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnShowAllTeamMembers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAllTeamMembers.Click
        Try
            Dim dtTeamMembersList As New DataTable()
            dtTeamMembersList = GetTeamMemberData()
            If IsNothing(dtTeamMembersList) = False AndAlso dtTeamMembersList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlPopupTeamMembers.DataSource = dtTeamMembersList
                dlPopupTeamMembers.DataBind()
                dlPopupTeamMembers.Visible = True
            End If
            popupShowAll.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnShowAllUpcomingHolidays_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAllUpcomingHolidays.Click
        Try
            Dim dtUpcomingHolidaysList As New DataTable()
            dtUpcomingHolidaysList = GetHolidayData()
            If IsNothing(dtUpcomingHolidaysList) = False AndAlso dtUpcomingHolidaysList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlPopupUpcomingHolidays.DataSource = dtUpcomingHolidaysList
                dlPopupUpcomingHolidays.DataBind()
                dlPopupUpcomingHolidays.Visible = True
            End If
            popupShowAll.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnShowAllWorkAnniversary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAllWorkAnniversary.Click
        Try
            Dim dtWorkAnniversaryList As New DataTable()
            dtWorkAnniversaryList = GetWorkAnniversaryData()
            If IsNothing(dtWorkAnniversaryList) = False AndAlso dtWorkAnniversaryList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlPopupWorkAnniversary.DataSource = dtWorkAnniversaryList
                dlPopupWorkAnniversary.DataBind()
                dlPopupWorkAnniversary.Visible = True
            End If
            popupShowAll.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmployeeOnLeaveShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmployeeOnLeaveShowAll.Click
        Try
            Dim dtEmployeeOnLeaveList As New DataTable()
            dtEmployeeOnLeaveList = GetEmployeeOnLeaveData()
            If IsNothing(dtEmployeeOnLeaveList) = False AndAlso dtEmployeeOnLeaveList.AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlPopupEmployeeOnLeave.DataSource = dtEmployeeOnLeaveList
                dlPopupEmployeeOnLeave.DataBind()
                dlPopupEmployeeOnLeave.Visible = True
            End If
            popupShowAll.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseShowAll.Click
        Try
            dlPopupBirthday.Visible = False
            dlPopupNewlyHired.Visible = False
            dlPopupTeamMembers.Visible = False
            dlPopupUpcomingHolidays.Visible = False
            dlPopupWorkAnniversary.Visible = False
            dlPopupEmployeeOnLeave.Visible = False

            dlPopupBirthday.DataSource = Nothing
            dlPopupNewlyHired.DataSource = Nothing
            dlPopupTeamMembers.DataSource = Nothing
            dlPopupUpcomingHolidays.DataSource = Nothing
            dlPopupWorkAnniversary.DataSource = Nothing
            dlPopupEmployeeOnLeave.DataSource = Nothing

            popupShowAll.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveClaimExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveClaimExpense.Click
        Try
            'Pinkal (01-Jun-2021)-- Start
            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            'Response.Redirect(Session("rootpath") + "Leave/wPg_ProcessLeaveList.aspx", False)
            Response.Redirect(Session("rootpath") + "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
            'Pinkal (01-Jun-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveLeave.Click
        Try
            'Pinkal (03-May-2021)-- Start
            'NMB Enhancement  -  Working on AD Enhancement for NMB.
            'Response.Redirect(Session("rootpath") + "TnA/OT_Requisition/Ot_RequisitionApproval.aspx", False)

            'Pinkal (15-May-2023) -- Start
            '(A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
            'Response.Redirect(Session("rootpath") + "Leave/wPg_ProcessLeaveList.aspx", False)
            Response.Redirect(Session("rootpath") + "Leave/wPg_ProcessLeaveList.aspx?Id=" & HttpUtility.UrlEncode(clsCrypto.Encrypt("ApprovedLeaveFromDashboard")), False)
            'Pinkal (15-May-2023) -- End

            'Pinkal (03-May-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveOTApplication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveOTApplication.Click
        Try
            'Pinkal (01-Jun-2021)-- Start
            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            'Response.Redirect(Session("rootpath") + "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
            Response.Redirect(Session("rootpath") + "TnA/OT_Requisition/Ot_RequisitionApproval.aspx", False)
            'Pinkal (01-Jun-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSetScoreCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetScoreCard.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveScoreCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveScoreCard.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Common/wPgPerformance_Planning.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSubmitScoreCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSubmitScoreCard.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Common/wPgPerformance_Planning.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveUpdateProgress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveUpdateProgress.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Goals/wPgUpdateGoalProgress.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkMyAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMyAssessment.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAssessEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssessEmployee.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkMyCompetenceAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMyCompetenceAssessment.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAssessEmployeeCompetence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssessEmployeeCompetence.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkReviewEmployeeAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReviewEmployeeAssessment.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Evaluation/wPg_ReviewerEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkReviewEmployeeCompetence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReviewEmployeeCompetence.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Evaluation/wPg_ReviewerEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkNonDisclosureDeclaration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNonDisclosureDeclaration.Click
        Try
            Session("loginBy") = Global.User.en_loginby.Employee 'Sohail (12 Oct 2021
            Response.Redirect(Session("rootpath") + "Employee NonDisclosure Declaration/wPg_Empnondisclosure_declaration.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (12 Oct 2021) -- Start
    'NMB Enhancement : : Show ESS view on pending declaration task.
    Protected Sub lnkAssetDeclaration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssetDeclaration.Click
        Try
            Session("loginBy") = Global.User.en_loginby.Employee
            Response.Redirect(Session("rootpath") + "Assets Declaration/wPg_EmployeeAssetDeclarationListT2.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (06 Oct 2021) -- End

    Protected Sub lnkApproveSalaryChangeMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveSalaryChangeMSS.Click
        Try
            Response.Redirect(Session("rootpath") + "HR/wPg_SalaryIncrementList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApprovePayslipPaymentMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApprovePayslipPaymentMSS.Click
        Try
            Response.Redirect(Session("rootpath") + "Payroll/wPg_PaymentApproval.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveLoanApplicationMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveLoanApplicationMSS.Click
        Try
            'Pinkal (15-May-2023) -- Start
            ' (A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
            If CBool(Session("RoleBasedLoanApproval")) = True Then
                Response.Redirect(Session("rootpath") + "Loan_Savings/New_Loan/Role_Based_Loan_Approval_Process/wPg_RoleBasedLoanApprovalList.aspx?Id=" & HttpUtility.UrlEncode(clsCrypto.Encrypt("ApprovedLoanFromDashboard")), False)
            Else
                Response.Redirect(Session("rootpath") + "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApprovalList.aspx?Id=" & HttpUtility.UrlEncode(clsCrypto.Encrypt("ApprovedLoanFromDashboard")), False)
            End If
            'Pinkal (15-May-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveCalibrationMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveCalibrationMSS.Click
        Try
            Response.Redirect(Session("rootpath") + "Assessment New/Peformance_Calibration/wPg_CalibrationApprovalList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApproveStaffRequisitionMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApproveStaffRequisitionMSS.Click
        Try
            Response.Redirect(Session("rootpath") + "Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisitionList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Quick Links
    Protected Sub lnkQApplyLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQApplyLeave.Click
        Try
            Response.Redirect(Session("rootpath") + "Leave/wPg_LeaveFormAddEdit.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |13-APR-2022| -- START
    'ISSUE/ENHANCEMENT : AC2-143
    Protected Sub lnkQRebate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQRebate.Click
        Try
            Response.Redirect(Session("rootpath") + "Claims_And_Expenses/wPg_AddEditClaimAndRequestList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |13-APR-2022| -- END

    Protected Sub lnkQApplyOT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQApplyOT.Click
        Try
            Response.Redirect(Session("rootpath") + "TnA/OT_Requisition/OTRequisition.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkQMyPayslip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQMyPayslip.Click
        Try
            'S.SANDEEP |22-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : VIEW PAYSLIP ISSUE
            Session("IsFromDashBoard") = True
            'S.SANDEEP |22-JUN-2021| -- END
            Response.Redirect(Session("rootpath") + "Payroll/wPg_PayslipReport.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (04 Sep 2021) -- Start
    'ENHANCEMENT : OLD-465 - Dashboard Pending Task Enhancement - Training Evaluation.
    Protected Sub lnkMyTrainingFeedback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMyTrainingFeedback.Click
        Try
            ESS()
            Response.Redirect(Session("rootpath") + "Training/Training_Request/wPg_TrainingRequestFormList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEvaluateEmployeeTraining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEvaluateEmployeeTraining.Click
        Try
            Response.Redirect(Session("rootpath") + "Training/Training_Request/wPg_TrainingRequestFormList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (04 Sep 2021) -- End    

    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            GetTnADashBoardData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (14-Feb-2022) -- End


    'Pinkal (26-Jun-2022) -- Start
    'NMB Enhancement :(A1X-1028) NMB - Staff transfers application page.
    Protected Sub btnCheckIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckIn.Click
        Dim objLogin As New clslogin_Tran
        Dim mintEmployeeUnkId As Integer = CInt(Session("E_Employeeunkid"))
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId
            Dim dtDate As DateTime = Basepage.GetCurrentDateTime
            objLogin._Logindate = dtDate.Date
            objLogin._checkintime = CDate(dtDate.Date & " " & dtDate.ToString("HH:mm"))

            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(dtDate.Date, mintEmployeeUnkId)
            If mintShiftId > 0 Then
                objLogin._Shiftunkid = mintShiftId
            Else
                objLogin._Shiftunkid = 0
            End If
            objEmpShift = Nothing

            objLogin._Original_InTime = objLogin._checkintime
            objLogin._Checkouttime = Nothing
            objLogin._Original_OutTime = Nothing
            objLogin._SourceType = enInOutSource.Manual
            objLogin._InOutType = 0
            objLogin._Workhour = 0
            objLogin._Holdunkid = 0
            objLogin._Voiddatetime = Nothing
            objLogin._Isvoid = False
            objLogin._Voidreason = ""
            objLogin._LoginEmployeeUnkid = mintEmployeeUnkId

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLogin._Userunkid = CInt(Session("UserId"))
            End If
            objLogin._WebClientIP = CStr(Session("IP_ADD"))
            objLogin._WebHostName = CStr(Session("HOST_NAME"))
            objLogin._WebFormName = "frmDailyAttendanceOperation"



            Dim mblnFlag As Boolean = objLogin.Insert(Session("Database_Name") _
                                                  , CInt(Session("UserId")) _
                                                  , Session("Fin_year") _
                                                  , Session("CompanyUnkId") _
                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                  , Session("UserAccessModeSetting") _
                                                  , True, Session("IsIncludeInactiveEmp") _
                                                  , Session("PolicyManagementTNA") _
                                                  , Session("DonotAttendanceinSeconds") _
                                                  , Session("FirstCheckInLastCheckOut") _
                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                  , Session("IsDayOffConsiderOnWeekend") _
                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                  , False, -1, Nothing, "", "")

            If mblnFlag Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmDailyAttendanceOperation", 1, "You Have Successfully Logged in."), Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLogin = Nothing
            SetClockInClockOut()
        End Try
    End Sub

    Protected Sub btnCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckOut.Click
        Dim objLogin As New clslogin_Tran
        Dim mintEmployeeUnkId As Integer = CInt(Session("E_Employeeunkid"))
        Try
            Dim dsList As DataSet
            objLogin._Employeeunkid = mintEmployeeUnkId

            Dim dtDate As DateTime = Basepage.GetCurrentDateTime
            objLogin._Logindate = dtDate.Date

            dsList = objLogin.GetList("List", True)
            Dim drCheckIn() As DataRow = dsList.Tables(0).Select("checkouttime IS NULL AND inouttype = 0 ")
            For Each dr In drCheckIn
                objLogin._Loginunkid = CInt(dr("loginunkid").ToString)
                Exit For
            Next
            objLogin._Checkouttime = CDate(dtDate.Date & " " & dtDate.ToString("HH:mm"))
            objLogin._Original_OutTime = CDate(dtDate.Date & " " & dtDate.ToString("HH:mm"))

            objLogin._SourceType = enInOutSource.Manual

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            'Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
            Dim wkmins As Double = 0
            If objLogin._checkintime <> Nothing AndAlso IsDBNull(objLogin._checkintime) = False Then
                wkmins = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
            End If
            'Pinkal (27-Sep-2024) -- End

            objLogin._Workhour = CInt(wkmins)
            objLogin._InOutType = 1
            objLogin._Holdunkid = 0
            objLogin._Voiddatetime = Nothing
            objLogin._Isvoid = False
            objLogin._Voidreason = ""
            objLogin._LoginEmployeeUnkid = mintEmployeeUnkId

            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(dtDate.Date, mintEmployeeUnkId)
            If mintShiftId > 0 Then
                objLogin._Shiftunkid = mintShiftId
            Else
                objLogin._Shiftunkid = 0
            End If
            objEmpShift = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLogin._Userunkid = CInt(Session("UserId"))
            End If
            objLogin._WebClientIP = CStr(Session("IP_ADD"))
            objLogin._WebHostName = CStr(Session("HOST_NAME"))
            objLogin._WebFormName = "frmDailyAttendanceOperation"

            Dim mblnFlag As Boolean = objLogin.Update(Session("Database_Name") _
                                                  , CInt(Session("UserId")) _
                                                  , Session("Fin_year") _
                                                  , Session("CompanyUnkId") _
                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                  , Session("UserAccessModeSetting") _
                                                  , True, Session("IsIncludeInactiveEmp") _
                                                  , Session("PolicyManagementTNA") _
                                                  , Session("DonotAttendanceinSeconds") _
                                                  , Session("FirstCheckInLastCheckOut") _
                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                  , Session("IsDayOffConsiderOnWeekend") _
                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                  , False, -1, Nothing, "", "")

            If mblnFlag Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmDailyAttendanceOperation", 2, "You Have Successfully Logged out."), Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLogin = Nothing
            SetClockInClockOut()
        End Try
    End Sub

        'Pinkal (26-Jun-2022) -- End

    'Pinkal (15-Sep-2023) -- Start
    '(A1X-1284) Toyota - Configuration of Intranet links on the new UI.
    Protected Sub btnShowAllIntranetLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAllIntranetLink.Click
        Dim objIntranetLink As New clsdashboard_intranetlink
        Try
            Dim dsIntranetLink As New DataSet
            dsIntranetLink = objIntranetLink.GetList("List", CInt(Session("CompanyUnkId")), True, Nothing)
            If IsNothing(dsIntranetLink) = False AndAlso dsIntranetLink.Tables(0).AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlpopupIntranetLink.DataSource = dsIntranetLink.Tables(0)
                dlpopupIntranetLink.DataBind()
                dlpopupIntranetLink.Visible = True
            End If
            popupShowAll.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objIntranetLink = Nothing
        End Try
    End Sub

    Protected Sub lnkMSSEmployeeHeadCountReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMSSEmployeeHeadCountReport.Click
        Try
            Response.Redirect(Session("rootpath") + "Reports/Rpt_Employee_head_Count.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkMyGoalStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMyGoalStatus.Click
        Try
            Session.Add("SessionGoalStatusFromDashBoard", True)
            Response.Redirect(Session("rootpath") + "Assessment New/Common/wPgPerformance_Planning.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewMyProgressUpdateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewMyProgressUpdateReport.Click
        Try
            Session.Add("SessionProgressUpdateFromDashBoard", True)
            Response.Redirect(Session("rootpath") + "Assessment New/Performance Goals/wPgESSUpdateGoalProgress.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (15-Sep-2023) -- End


    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
    Protected Sub btnShowAllVacancy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAllVacancy.Click
        Try
            Dim dsVacancy As New DataSet

            Dim objConfig As New clsConfigOptions
            Dim mdtAsOnDate As Date = objConfig._CurrentDateAndTime.Date
            objConfig = Nothing

            dsVacancy = clsDashboard_Class.GetVacanciesFromCurrentFY(CDate(Session("fin_startdate")).Date, mdtAsOnDate.Date)
            If IsNothing(dsVacancy) = False AndAlso dsVacancy.Tables(0).AsEnumerable().Cast(Of DataRow).Count > 0 Then
                dlpopupVacancy.DataSource = dsVacancy.Tables(0)
                dlpopupVacancy.DataBind()
                dlpopupVacancy.Visible = True
            End If
            popupShowAll.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (30-Sep-2023) -- End


#End Region

    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.

#Region "Gridview Events"

    Protected Sub gvTnADetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTnADetails.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then
                If DataBinder.Eval(e.Row.DataItem, "Status") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 72, "AB") Then
                    e.Row.ForeColor = Drawing.Color.Red
                ElseIf DataBinder.Eval(e.Row.DataItem, "Status") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 74, "PR") Then
                    e.Row.ForeColor = Drawing.Color.Blue
                ElseIf DataBinder.Eval(e.Row.DataItem, "Status") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 71, "HL") Then
                    e.Row.ForeColor = Drawing.Color.Brown
                ElseIf DataBinder.Eval(e.Row.DataItem, "Status") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsDashboard_Class", 73, "OFF") Then
                    e.Row.ForeColor = Drawing.Color.Orange
                ElseIf CInt(gvTnADetails.DataKeys(e.Row.RowIndex)("leavetypeunkid")) > 0 Then
                    e.Row.ForeColor = Drawing.Color.FromArgb(gvTnADetails.DataKeys(e.Row.RowIndex)("color"))
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (14-Feb-2022) -- End

#Region " Combobox Events "
   
    'Hemant (13 Oct 2023) -- Start
    'ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
    Protected Sub cboSDivision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSDivision.SelectedIndexChanged
        Dim dsSalaryAnalysisLIst As New DataSet
        Dim jsSerializer As New JavaScriptSerializer
        Dim xCategory As String() = Nothing
        Dim objDashboard_Class As New Aruti.Data.clsDashboard_Class
        Try
            objDashboard_Class = New clsDashboard_Class(CStr(Session("Database_Name")), _
                                                                         CStr(Session("UserAccessModeSetting")), _
                                                                         CInt(Session("U_UserID")), _
                                                                         CInt(Session("Fin_year")), _
                                                                         CInt(Session("CompanyUnkId")), _
                                                                         Session("fin_startdate"), _
                                                                         Session("fin_enddate"), _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         False, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                         0, _
                                                                              0, False, _
                                                                         False, _
                                                                         False)
            If cboSDivision.SelectedValue <= 0 Then
                dsSalaryAnalysisLIst = objDashboard_Class.Salary_Analysis(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper, "")
            Else
                dsSalaryAnalysisLIst = objDashboard_Class.Salary_Analysis(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper, cboSDivision.SelectedItem.Text)
            End If
            

            Dim SalaryAnnalysissource = New List(Of ClsChartData)
            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("Pid") Then
                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("Pid")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("PCode") Then
                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("PCode")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("ST_Date") Then
                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("ST_Date")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("ED_Date") Then
                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("ED_Date")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("PStatusId") Then
                dsSalaryAnalysisLIst.Tables(0).Columns.Remove("PStatusId")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("TCS") Then
                dsSalaryAnalysisLIst.Tables(0).Columns("TCS").ColumnName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmSlidingDashBoard", 406, "Total Bank Salary")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("TBS") Then
                dsSalaryAnalysisLIst.Tables(0).Columns("TBS").ColumnName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmSlidingDashBoard", 407, "Total Cash Salary")
            End If

            If dsSalaryAnalysisLIst.Tables(0).Columns.Contains("THS") Then
                dsSalaryAnalysisLIst.Tables(0).Columns("THS").ColumnName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmSlidingDashBoard", 408, "Total Hold Salary")
            End If


            Dim outputTable As DataTable = New DataTable(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper)
            outputTable.Columns.Add(dsSalaryAnalysisLIst.Tables(0).Columns(0).ColumnName.ToString())

            For Each inRow As DataRow In dsSalaryAnalysisLIst.Tables(0).Rows
                Dim newColName As String = inRow(0).ToString()
                outputTable.Columns.Add(newColName)
            Next

            For rCount As Integer = 1 To dsSalaryAnalysisLIst.Tables(0).Columns.Count - 1
                Dim newRow As DataRow = outputTable.NewRow()
                newRow(0) = dsSalaryAnalysisLIst.Tables(0).Columns(rCount).ColumnName.ToString()

                For cCount As Integer = 0 To dsSalaryAnalysisLIst.Tables(0).Rows.Count - 1
                    Dim colValue As String = dsSalaryAnalysisLIst.Tables(0).Rows(cCount)(rCount).ToString()
                    newRow(cCount + 1) = colValue
                Next

                outputTable.Rows.Add(newRow)
            Next

            dsSalaryAnalysisLIst.Tables.Remove(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper)
            dsSalaryAnalysisLIst.Tables.Add(outputTable)

            xCategory = dsSalaryAnalysisLIst.Tables(0).Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.Ordinal > 0).Select(Function(x) x.ColumnName).ToArray()

            For Each dtrow As DataRow In dsSalaryAnalysisLIst.Tables(clsDashboard_Class.enDashBoardListIndex.SALARY_LIST.ToString.ToUpper).Rows

                Dim lst As List(Of String) = dtrow.ItemArray.Select(Function(x) x.ToString()).Skip(1).ToList()
                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                    SalaryAnnalysissource.Add(New ClsChartData With {.name = dtrow("Pname"), .data = lst.Select(Function(x) CDec(x)).ToArray()})
                End If
                lst.Clear()
                lst = Nothing

            Next

            jsSerializer = New JavaScriptSerializer
            jsSerializer.MaxJsonLength = 2147483644
            SalaryAnalysisData = jsSerializer.Serialize(SalaryAnnalysissource)
            SalaryAnalysisCategory = jsSerializer.Serialize(xCategory)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SalaryChart", "getSalaryAnalysis('salary-analysis-container'," & SalaryAnalysisCategory & ",'Salary', " & SalaryAnalysisData & ");", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (13 Oct 2023) -- End

#End Region
 
 

    
End Class


Public Class ClsChartData
    Private mstrName As String
    Public Property name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    Private mstrData As Decimal()
    Public Property data() As Decimal()
        Get
            Return mstrData
        End Get
        Set(ByVal value As Decimal())
            mstrData = value
        End Set
    End Property

End Class
