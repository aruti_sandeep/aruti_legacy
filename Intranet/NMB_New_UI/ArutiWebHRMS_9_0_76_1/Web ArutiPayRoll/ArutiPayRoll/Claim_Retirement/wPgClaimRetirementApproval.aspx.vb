﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading

#End Region

Partial Class Claim_Retirement_wPgClaimRetirementApproval
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmClaimRetirementApproval"
    Private ReadOnly mstrModuleName1 As String = "frmClaimRetirementInformation"
    Private ReadOnly mstrModuleName2 As String = "frmDependentsList"

    Private DisplayMessage As New CommonCodes

    Private mintClaimRequestMstId As Integer = 0
    Private mintClaimRetirementId As Integer = 0
    Private mintClaimretirementtranunkid As Integer = 0
    Private mintClaimretirementApprovalunkid As Integer = 0

    Private mintCREmpId As Integer = 0
    Private mintApproverunkid As Integer = 0
    Private mintApproverEmpunkid As Integer = 0
    Private mintPriority As Integer = -1
    Private mblnIsExternalApprover As Boolean = False

    Private mintExpeseTypeId As Integer = 0
    Private mintBaseCountryId As Integer = 0
    Private mdtImprestDetail As DataTable = Nothing
    Private mdtAttachment As DataTable = Nothing
    Private mdtFinalAttachment As DataTable = Nothing
    Private mblnIsClaimFormBudgetMandatory As Boolean = False
    Private mblnIsHRExpense As Boolean = False
    Private mdtApplyDate As DateTime = Nothing
    Private mintEditRowIndex As Integer = -1
    Private mintAttachmentIndex As Integer = -1
    Private mdecFinalCRapprovedAmount As Decimal = 0
    Private mdecExpenseBalance As Decimal = 0
    Private mdecEditOldExpenseAmt As Decimal = 0


    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Private mstrExpenseIDs As String = ""
    'Pinkal (10-Feb-2021) -- End


    Private mblnShowAttchmentPopup As Boolean
    Private blnClaimDetailPopup As Boolean = False
    Private mblnShowDependantPopup As Boolean

    Private objClaimRetirement As New clsclaim_retirement_master
    Private objClaimRetirementApprovalTran As New clsclaim_retirement_approval_Tran
    Private objCONN As SqlConnection

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            Session.Remove("ClaimRetirementId")
            Session.Remove("crmasterunkid")
            Session.Remove("ClaimRetireEmpId")
            Session.Remove("approverunkid")
            Session.Remove("approverEmpID")
            Session.Remove("Priority")
            Session.Remove("IsExternalApprover")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count > 0 Then
                    KillIdleSQLSessions()
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 6 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))
                        mintCREmpId = CInt(arr(2))
                        mintApproverunkid = CInt(arr(3))
                        mintClaimRetirementId = CInt(arr(4))
                        mintClaimretirementApprovalunkid = CInt(arr(5))


                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If


                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Session("UserId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub
                            Else

                                If mblnATLoginRequiredToApproveApplications = False Then

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If

                        Dim clsConfig As New clsConfigOptions
                        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        Session("fmtCurrency") = clsConfig._CurrencyFormat

                        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                        End If

                        Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try

                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname").ToString())
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        gobjLocalization._LangId = CInt(HttpContext.Current.Session("LangId"))


                                End If  ' If mblnATLoginRequiredToApproveApplications = False Then

                            End If '  If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub

                        End If  ' If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then


                        'Pinkal (23-Feb-2024) -- End


                        mdtImprestDetail = objClaimRetirementApprovalTran.GetClaimRetirementApproverExpesneList("List", False, Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                                         , Session("EmployeeAsOnDate").ToString(), True, mintApproverunkid, "", mintClaimRetirementId, Nothing).Tables(0)

                        If mdtImprestDetail IsNot Nothing AndAlso mdtImprestDetail.Rows.Count > 0 Then

                            If CInt(mdtImprestDetail.Rows(0)("statusunkid")) = 1 Then
                                DisplayMessage.DisplayMessage("You cannot Edit this claim retirement detail. Reason: This Claim retirement was already approved.", Me, "../Index.aspx")

                                'Pinkal (23-Feb-2024) -- Start
                                '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                Session("ApprovalLink") = Nothing
                                Session("ApproverUserId") = Nothing

                                If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                Session.Abandon()
                                        If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                            Response.Cookies("ASP.NET_SessionId").Value = ""
                                            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                        End If

                                        If Request.Cookies("AuthToken") IsNot Nothing Then
                                            Response.Cookies("AuthToken").Value = ""
                                            Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                        End If

                                    End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                Exit Sub

                                'Pinkal (23-Feb-2024) -- End

                            ElseIf CInt(mdtImprestDetail.Rows(0)("statusunkid")) = 3 Then
                                Session.Abandon()
                                DisplayMessage.DisplayMessage("You cannot Edit this claim retirement detail. Reason: This Claim retirement was already rejected.", Me, "../Index.aspx")

                                'Pinkal (23-Feb-2024) -- Start
                                '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                Session("ApprovalLink") = Nothing
                                Session("ApproverUserId") = Nothing

                                If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                        Session.Abandon()
                                        If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                            Response.Cookies("ASP.NET_SessionId").Value = ""
                                            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                        End If

                                        If Request.Cookies("AuthToken") IsNot Nothing Then
                                            Response.Cookies("AuthToken").Value = ""
                                            Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                        End If

                                    End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                Exit Sub

                                'Pinkal (23-Feb-2024) -- End

                            End If

                            mintClaimRequestMstId = CInt(mdtImprestDetail.Rows(0)("crmasterunkid"))
                            mintApproverEmpunkid = CInt(mdtImprestDetail.Rows(0)("approveremployeeunkid"))
                            mintPriority = CInt(mdtImprestDetail.Rows(0)("crpriority"))
                            mblnIsExternalApprover = CBool(mdtImprestDetail.Rows(0)("isexternalapprover"))

                        End If   '  If mdtImprestDetail IsNot Nothing AndAlso mdtImprestDetail.Rows.Count > 0 Then

                    End If   '    If arr.Length = 7 Then
                Else

                    Exit Sub

                End If  ' If Request.QueryString.Count > 0 Then

            End If  'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then


            If Not IsPostBack Then
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                If Session("ClaimRetirementId") IsNot Nothing AndAlso CInt(Session("ClaimRetirementId")) > 0 Then
                    mintClaimRetirementId = CInt(Session("ClaimRetirementId"))
                End If

                If Session("crmasterunkid") IsNot Nothing AndAlso CInt(Session("crmasterunkid")) > 0 Then
                    mintClaimRequestMstId = CInt(Session("crmasterunkid"))
                End If

                If Session("ClaimRetireEmpId") IsNot Nothing AndAlso CInt(Session("ClaimRetireEmpId")) > 0 Then
                    mintCREmpId = CInt(Session("ClaimRetireEmpId"))
                End If

                If Session("approverunkid") IsNot Nothing AndAlso CInt(Session("approverunkid")) > 0 Then
                    mintApproverunkid = CInt(Session("approverunkid"))
                End If

                If Session("approverEmpID") IsNot Nothing AndAlso CInt(Session("approverEmpID")) > 0 Then
                    mintApproverEmpunkid = CInt(Session("approverEmpID"))
                End If

                If Session("Priority") IsNot Nothing AndAlso CInt(Session("Priority")) >= 0 Then
                    mintPriority = CInt(Session("Priority"))
                End If

                If Session("IsExternalApprover") IsNot Nothing AndAlso CInt(Session("IsExternalApprover")) > 0 Then
                    mblnIsExternalApprover = CBool(Session("IsExternalApprover"))
                End If

                FillCombo()

                If mdtImprestDetail Is Nothing Then
                    mdtImprestDetail = objClaimRetirementApprovalTran.GetClaimRetirementApproverExpesneList("List", False, Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                                             , Session("EmployeeAsOnDate").ToString(), True, mintApproverunkid, "", mintClaimRetirementId, Nothing).Tables(0)
                End If

                If mdtFinalAttachment Is Nothing Then
                    Dim objAttchement As New clsScan_Attach_Documents
                    Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", CInt(cboEmployee.SelectedValue))
                    Dim strTranIds As String = String.Join(",", mdtImprestDetail.AsEnumerable().Select(Function(x) x.Field(Of Integer)("Claimretirementtranunkid").ToString).ToArray)
                    If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                    mdtFinalAttachment = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_RETIREMENT & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    objAttchement = Nothing
                Else
                    mdtAttachment = mdtFinalAttachment.Copy()
                End If

                GetClaimDetails()
                GetValue()
                FillImprestDetail()

            Else
                Call SetLanguage()
                mintClaimRetirementId = CInt(Me.ViewState("ClaimRetirementId"))
                mintClaimretirementtranunkid = CInt(Me.ViewState("Claimretirementtranunkid"))
                mintClaimRequestMstId = CInt(ViewState("crmasterunkid"))
                mintApproverunkid = CInt(ViewState("approverunkid"))
                mintApproverEmpunkid = CInt(ViewState("approverEmpID"))
                mintPriority = CInt(ViewState("Priority"))
                mblnIsExternalApprover = CBool(ViewState("IsExternalApprover"))
                mintCREmpId = CInt(ViewState("CREmpId"))

                mdtImprestDetail = CType(Me.ViewState("ImprestDetail"), DataTable)
                mdtAttachment = CType(Me.ViewState("Attachment"), DataTable)
                mdtFinalAttachment = CType(Me.ViewState("FinalAttachment"), DataTable)
                mintExpeseTypeId = CInt(ViewState("ExpeseTypeId"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mdtApplyDate = CType(Me.ViewState("ApplyDate"), DateTime)
                mintEditRowIndex = CInt(Me.ViewState("EditRowIndex"))
                mintAttachmentIndex = CInt(Me.ViewState("AttachmentIndex"))
                mdecFinalCRapprovedAmount = CDec(Me.ViewState("FinalCRapprovedAmount"))
                mdecExpenseBalance = CDec(Me.ViewState("ExpenseBalance"))
                mdecEditOldExpenseAmt = CDec(Me.ViewState("EditOldExpenseAmt"))
                mblnShowAttchmentPopup = CBool(Me.ViewState("ShowAttchmentPopup"))
                blnClaimDetailPopup = CBool(Me.ViewState("blnClaimDetailPopup"))
                mblnShowDependantPopup = CBool(Me.ViewState("ShowDependantPopup"))


                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                mstrExpenseIDs = Me.ViewState("ExpenseIDs").ToString()
                'Pinkal (10-Feb-2021) -- End

            End If



            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If


            If blnClaimDetailPopup Then
                popupClaimDetail.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            Me.ViewState("ClaimRetirementId") = mintClaimRetirementId
            Me.ViewState("Claimretirementtranunkid") = mintClaimretirementtranunkid
            Me.ViewState("crmasterunkid") = mintClaimRequestMstId
            Me.ViewState("approverunkid") = mintApproverunkid
            Me.ViewState("approverEmpID") = mintApproverEmpunkid
            Me.ViewState("Priority") = mintPriority
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover


            Me.ViewState("ImprestDetail") = mdtImprestDetail
            Me.ViewState("Attachment") = mdtAttachment
            Me.ViewState("FinalAttachment") = mdtFinalAttachment
            Me.ViewState("CREmpId") = mintCREmpId
            Me.ViewState("ExpeseTypeId") = mintExpeseTypeId
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("ApplyDate") = mdtApplyDate
            Me.ViewState("EditRowIndex") = mintEditRowIndex
            Me.ViewState("AttachmentIndex") = mintAttachmentIndex
            Me.ViewState("mdecFinalCRapprovedAmount") = mdecFinalCRapprovedAmount
            Me.ViewState("ExpenseBalance") = mdecExpenseBalance
            Me.ViewState("EditOldExpenseAmt") = mdecEditOldExpenseAmt
            Me.ViewState("blnClaimDetailPopup") = blnClaimDetailPopup
            Me.ViewState("ShowAttchmentPopup") = mblnShowAttchmentPopup
            Me.ViewState("ShowDependantPopup") = mblnShowDependantPopup


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Me.ViewState("ExpenseIDs") = mstrExpenseIDs
            'Pinkal (10-Feb-2021) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim blnSelect As Boolean = True
            Dim blnApplyFilter As Boolean = True
            Dim objEMaster As New clsEmployee_Master


            Dim dsCombo As DataSet = objEMaster.GetEmployeeList(Session("Database_Name").ToString() _
                                                                                             , CInt(Session("UserId")) _
                                                                                             , CInt(Session("Fin_year")) _
                                                                                             , CInt(Session("CompanyUnkId")) _
                                                                                             , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                             , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                             , Session("UserAccessModeSetting").ToString(), True _
                                                                                             , False, "List", False, mintCREmpId, , , , , , , , , , , , , , , , False)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintCREmpId.ToString()
            End With

            With cboCDClaimEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCREmpId.ToString()
            End With

            objEMaster = Nothing

            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
                .DataBind()
            End With

            With cboCDClaimCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dtTable.Copy()
                .DataBind()
                .Enabled = False
            End With

            objCostCenter = Nothing


            Dim objClaim As New clsclaim_request_master
            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            objClaim._Crmasterunkid = mintClaimRequestMstId
            'dsCombo = objClaim.GetFinalApproverApprovedDetails(CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboEmployee.SelectedValue), mintExpeseTypeId, mintClaimRequestMstId, 0, True)
            dsCombo = objClaim.GetFinalApproverApprovedDetails(CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(cboEmployee.SelectedValue), objClaim._Expensetypeid, mintClaimRequestMstId, 0, True)

            mstrExpenseIDs = String.Join(",", dsCombo.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("expenseunkid").ToString()).Distinct().ToArray())

            'Pinkal (10-Feb-2021) -- End

            objClaim = Nothing

            Dim dRow As DataRow = dsCombo.Tables(0).NewRow
            dRow("expenseunkid") = 0
            dRow("Expense") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Select")
            dsCombo.Tables(0).Rows.InsertAt(dRow, 0)


            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            'With cboImprest
            '    .DataValueField = "expenseunkid"
            '    .DataTextField = "Expense"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            'End With
            'cboImprest_SelectedIndexChanged(cboImprest, New EventArgs())
            cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
            'Pinkal (24-Oct-2019) -- End

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Dim dtExpense As DataTable = dsCombo.Tables(0).DefaultView.ToTable(True, "expenseunkid", "Expense")
            With cboCDClaimExpense
                .DataValueField = "expenseunkid"
                .DataTextField = "Expense"
                '.DataSource = dsCombo.Tables(0).Copy
                .DataSource = dtExpense
                .DataBind()
            End With
            objClaim = Nothing
            'Pinkal (10-Feb-2021) -- End


            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With

            With cboCDClaimCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0).Copy()
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            objExchange = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetClaimDetails()
        Try
            If mintClaimRequestMstId > 0 Then
                Dim objclaim As New clsclaim_request_master
                objclaim._Crmasterunkid = mintClaimRequestMstId
                mintExpeseTypeId = objclaim._Expensetypeid
                txtClaimNo.Text = objclaim._Claimrequestno
                txtImprestAmount.Text = Format(CDec(objclaim.GetFinalApproverApprovedAmount(objclaim._Employeeunkid, objclaim._Expensetypeid, mintClaimRequestMstId, True)), Session("fmtcurrency").ToString())

                '/ START  SET CLAIM DETAIL FOR POPUP 
                FillClaimDetailCombo()
                txtCDClaimNo.Text = txtClaimNo.Text
                dtpCDClaimDate.SetDate = objclaim._Transactiondate.Date

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboCDClaimEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtCDClaimDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtCDClaimDomicileAddress.Text = ""
                End If

                txtCDClaimRemark.Text = objclaim._Claim_Remark

                '/ END  SET CLAIM DETAIL FOR POPUP 
                objclaim = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objClaimRetirement._Claimretirementunkid = mintClaimRetirementId
            txtClaimRetirementNo.Text = objClaimRetirement._ClaimRetirementNo
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpCDClaimDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Function Validation() As Boolean
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboImprest.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Imprest is mandatory information. Please select Imprest to continue."), Me)
                cboImprest.Focus()
                Return False
            End If

            If txtQuantity.Text.Trim() = "" OrElse CDec(txtQuantity.Text) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQuantity.Focus()
                Return False
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboCurrency.Focus()
                Return False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try
            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddRetirement()
        Try
            Dim dtmp() As DataRow = Nothing
            If mdtImprestDetail.Rows.Count > 0 Then
                dtmp = mdtImprestDetail.Select("expenseunkid = '" & CInt(cboImprest.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQuantity.Text.Trim.Length > 0, CDec(txtQuantity.Text), 0)) & _
                                                              "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtRemark.Text.Trim() & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, you cannot add same Imprest again in the below list."), Me)
                    Exit Sub
                End If
            End If
            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboImprest.SelectedValue)

            mblnIsHRExpense = objExpMaster._IsHRExpense
            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""

            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            'If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '    If CDec(txtQuantity.Text) > CDec(txtBalance.Text) Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, you cannot set quantity greater than balance set."), Me)
            '        txtQuantity.Focus()
            '        Exit Sub
            '    End If

            'ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '    If CDec(CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) > CDec(txtBalance.Text) Then
            '        'Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set amount greater than balance set."), Me)
            '        txtQuantity.Focus()
            '        Exit Sub
            '    End If
            'End If

            'If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
            '    Dim mdecBudgetAmount As Decimal = 0
            '    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
            '    If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
            '        Exit Sub
            '    End If
            'End If

            'If dgvRetirement.Items.Count >= 1 Then
            '    Dim objExpMasterOld As New clsExpense_Master
            '    Dim iEncashment As DataRow() = Nothing
            '    If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
            '        iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '    ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
            '        iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '    End If
            '    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
            '    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '        'Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '        Exit Sub
            '    ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '        Exit Sub
            '    End If
            '    objExpMasterOld = Nothing
            'End If
            objExpMaster = Nothing


            Dim xCount = mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Claimretirementtranunkid") = -999).Count
            If xCount > 0 Then
                mdtImprestDetail.Rows.Clear()
            End If

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), ConfigParameter._Object._CurrentDateAndTime.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)

            Dim dRow As DataRow = mdtImprestDetail.NewRow

            dRow.Item("Claimretirementtranunkid") = -1
            dRow.Item("Claimretirementunkid") = mintClaimRetirementId
            dRow.Item("crmasterunkid") = mintClaimRequestMstId
            dRow.Item("expenseunkid") = CInt(cboImprest.SelectedValue)
            dRow.Item("expense") = cboImprest.SelectedItem.Text
            dRow.Item("quantity") = txtQuantity.Text
            dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQuantity.Text)
            dRow.Item("balance") = CDec(mdecExpenseBalance - CDec(dRow.Item("amount"))).ToString(Session("fmtcurrency").ToString())
            dRow.Item("expense_remark") = txtRemark.Text
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("iscancel") = False
            dRow.Item("canceluserunkid") = -1
            dRow.Item("cancel_datetime") = DBNull.Value
            dRow.Item("cancel_remark") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("loginemployeeunkid") = -1
            dRow.Item("voidloginemployeeunkid") = -1

            mdecExpenseBalance = mdecExpenseBalance - CDec(dRow.Item("amount"))

            mdtImprestDetail.Rows.Add(dRow)


            FillImprestDetail()
            Enable_Disable_Ctrls(False)
            Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EditRetirement()
        Try
            Dim iRow As DataRow() = Nothing
            If CInt(dgvRetirement.DataKeys(mintEditRowIndex).Values("Claimretirementtranunkid").ToString()) > 0 Then
                iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & CInt(dgvRetirement.DataKeys(mintEditRowIndex).Values("Claimretirementtranunkid").ToString()) & "' AND AUD <> 'D'")
            Else
                iRow = mdtImprestDetail.Select("GUID = '" & dgvRetirement.DataKeys(mintEditRowIndex).Values("GUID").ToString() & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("Claimretirementtranunkid")) > 0 Then
                    dtmp = mdtImprestDetail.Select("Claimretirementtranunkid <> '" & iRow(0).Item("Claimretirementtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboImprest.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQuantity.Text.Length > 0, CDec(txtQuantity.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtImprestDetail.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboImprest.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQuantity.Text.Length > 0, CDec(txtQuantity.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, you cannot add same Imprest again in the below list."), Me)
                    Exit Sub
                End If

                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboImprest.SelectedValue)
                mblnIsHRExpense = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""

                If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If

                'If objExpMaster._Isaccrue  Then
                'If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                '    If CDec(txtQuantity.Text) > CDec(txtBalance.Text) Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                '        txtQuantity.Focus()
                '        Exit Sub
                '    End If
                'ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                '    If CDec(CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) > CDec(txtBalance.Text) Then
                '        'Language.setLanguage(mstrModuleName)
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set amount greater than balance set."), Me)
                '        txtQuantity.Focus()
                '        Exit Sub
                '    End If
                'End If
                'End If
                'If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                '    Dim mdecBudgetAmount As Decimal = 0
                '    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                '    If mdecBudgetAmount < (CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)) Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                '        Exit Sub
                '    End If
                'End If

                'If dgvRetirement.Rows.Count >= 1 Then
                '    Dim objExpMasterOld As New clsExpense_Master
                '    Dim iEncashment As DataRow() = Nothing
                '    If CInt(dgvRetirement.DataKeys(mintEditRowIndex).Values("Claimretirementtranunkid").ToString()) > 0 Then
                '        iEncashment = mdtImprestDetail.Select("crtranunkid = '" & CInt(dgvRetirement.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                '    ElseIf dgvRetirement.Items(0).Cells(12).Text.ToString <> "" Then
                '        iEncashment = mdtImprestDetail.Select("GUID = '" & dgvRetirement.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                '    End If
                'End If
                objExpMaster = Nothing

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), ConfigParameter._Object._CurrentDateAndTime.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)


                iRow(0).Item("Claimretirementtranunkid") = iRow(0).Item("Claimretirementtranunkid")
                iRow(0).Item("Claimretirementunkid") = mintClaimRetirementId
                iRow(0).Item("crmasterunkid") = mintClaimRequestMstId
                iRow(0).Item("expenseunkid") = CInt(cboImprest.SelectedValue)
                iRow(0).Item("expense") = cboImprest.SelectedItem.Text
                iRow(0).Item("quantity") = CDec(txtQuantity.Text)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQuantity.Text)
                iRow(0).Item("balance") = CDec(CDec(mdecExpenseBalance + mdecEditOldExpenseAmt) - CDec(iRow(0).Item("amount"))).ToString(Session("fmtcurrency").ToString())
                iRow(0).Item("expense_remark") = txtRemark.Text
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                iRow(0).Item("iscancel") = False
                iRow(0).Item("canceluserunkid") = -1
                iRow(0).Item("cancel_datetime") = DBNull.Value
                iRow(0).Item("cancel_remark") = ""
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString

                iRow(0).Item("loginemployeeunkid") = -1
                iRow(0).Item("voidloginemployeeunkid") = -1

                mdecExpenseBalance = CDec((mdecExpenseBalance + mdecEditOldExpenseAmt) - CDec(iRow(0).Item("amount")))

                iRow(0).AcceptChanges()

                FillImprestDetail()
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboImprest.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQuantity.Text.Trim.Length > 0, txtQuantity.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillImprestDetail()
        Try

            Dim mdView As New DataView
            mdView = mdtImprestDetail.DefaultView
            mdView.RowFilter = "AUD <> 'D'"


            Dim mblnBlank As Boolean = False
            If mdView IsNot Nothing AndAlso mdView.ToTable().Rows.Count <= 0 Then
                Dim drRow As DataRow = mdView.Table.NewRow()
                drRow("Claimretirementtranunkid") = -999
                drRow("expenseunkid") = 0
                drRow("countryunkid") = mintBaseCountryId
                drRow("quantity") = 0.0
                drRow("unitprice") = 0.0
                drRow("amount") = 0.0
                drRow("balance") = 0.0
                drRow("AUD") = ""
                mdView.Table.Rows.Add(drRow)
                mblnBlank = True
            End If

            If mdView IsNot Nothing AndAlso mdView.ToTable().Rows.Count > 0 Then
                txtRetirementGrandTotal.Text = CDec(mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("amount"))).ToString(Session("fmtcurrency").ToString())
            Else
                txtRetirementGrandTotal.Text = "0.00"
            End If

            txtBalanceDue.Text = CDec(CDec(txtImprestAmount.Text) - CDec(txtRetirementGrandTotal.Text)).ToString(Session("fmtcurrency").ToString())

            dgvRetirement.DataSource = mdView
            dgvRetirement.DataBind()

            If mblnBlank Then dgvRetirement.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboEmployee.Enabled = iFlag

            If mdtImprestDetail IsNot Nothing Then
                If mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = CStr(mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                    cboCurrency.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboImprest.SelectedValue = "0"
            cboImprest_SelectedIndexChanged(cboImprest, New EventArgs())
            txtRemark.Text = ""
            txtQuantity.Text = "1.00"
            txtUnitPrice.Text = "1.00"
            txtBalance.Text = "0.00"
            txtAmount.Text = "0.00"
            cboCurrency.SelectedValue = mintBaseCountryId.ToString()
            cboCostCenter.SelectedValue = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub UpdateBalance(ByVal iRowindex As Integer, ByVal mdecAmount As Decimal)
        Try
            Dim dr As DataRow() = mdtImprestDetail.Select("AUD <> 'D' ")
            If dr.Length > 0 Then
                For i As Integer = iRowindex To dr.Length - 1
                    dr(i)("balance") = CDec(dr(i)("balance")) + mdecAmount
                    dr(i).AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If dgvRetirement.Rows.Count <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please add atleast one expense retirement in order to save."), Me)
                dgvRetirement.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objClaimRetirement._Claimretirementunkid = mintClaimRetirementId
            objClaimRetirement._Crmasterunkid = mintClaimRequestMstId
            objClaimRetirement._Expensetypeunkid = mintExpeseTypeId
            objClaimRetirement._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objClaimRetirement._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            objClaimRetirement._Approverunkid = -1
            objClaimRetirement._Approveremployeeunkid = -1
            objClaimRetirement._Retirement_Remark = ""
            objClaimRetirement._Imprest_Amount = 0
            objClaimRetirement._statusunkid = 2
            objClaimRetirement._P2prequisitionid = ""
            objClaimRetirement._P2pposteddata = ""
            objClaimRetirement._P2pstatuscode = ""
            objClaimRetirement._P2pmessage = ""

            objClaimRetirement._Isvoid = False
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                objClaimRetirement._Userunkid = CInt(Session("UserId"))
                objClaimRetirement._Loginemployeeunkid = -1
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objClaimRetirement._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objClaimRetirement._Userunkid = -1
            End If
            objClaimRetirement._Voiddatetime = Nothing
            objClaimRetirement._Voiduserunkid = -1
            objClaimRetirement._Voidloginemployeeunkid = -1

            objClaimRetirement._Iscancel = False
            objClaimRetirement._Cancel_Remark = ""
            objClaimRetirement._Cancel_Datetime = Nothing
            objClaimRetirement._Canceluserunkid = -1

            objClaimRetirement._FormName = mstrModuleName
            objClaimRetirement._WebClientIP = Session("IP_ADD").ToString()
            objClaimRetirement._WebHostName = Session("HOST_NAME").ToString()
            objClaimRetirement._IsWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ApproveExpense()
        Try
            If SaveData(btnApprove) Then
                If Request.QueryString.Count > 0 Then
                    If clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|")).ToArray().Length = 6 Then

                        SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True, Session("mdbname").ToString() _
                                                , CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())


                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                        Session.Clear()
                        Session.Abandon()
                        Session.RemoveAll()

                        If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                            Response.Cookies("ASP.NET_SessionId").Value = ""
                            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                        End If

                        If Request.Cookies("AuthToken") IsNot Nothing Then
                            Response.Cookies("AuthToken").Value = ""
                            Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                        End If
                        'Pinkal (23-Feb-2024) -- End


                        'Pinkal (11-Feb-2022) -- Start
                        'Enhancement NMB  - Language Change Issue for All Modules.	
                        'Response.Redirect("../Index.aspx", True)
                        Response.Redirect("../Index.aspx", False)
                        'Pinkal (11-Feb-2022) -- End
                    Else
                        Response.Redirect(Session("rootpath").ToString() & "Claim_Retirement/wPg_ClaimRetirement_ApprovalList.aspx", False)
                    End If
                Else
                    Response.Redirect(Session("rootpath").ToString() & "Claim_Retirement/wPg_ClaimRetirement_ApprovalList.aspx", False)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SaveData(ByVal sender As Object) As Boolean
        Dim blnFlag As Boolean = False
        Dim blnLastApprover As Boolean = False
        Dim mstrRejectRemark As String = ""
        Dim mintMaxPriority As Integer = -1
        Try
            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING
            Dim mdtApprovalDate As DateTime = Nothing


            Dim drList As List(Of DataRow) = mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") = "").ToList()
            If drList.Count > 0 Then
                For Each dr As DataRow In drList
                    dr("AUD") = "U"
                    dr.AcceptChanges()
                Next
            End If

            objClaimRetirementApprovalTran._ClaimRetirementTran = mdtImprestDetail.Copy()

            Dim mblnIssued As Boolean = False

            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            'Dim dsList As DataSet = objClaimRetirementApprovalTran.GetClaimRetirementApproverExpesneList("List", False, Session("Database_Name").ToString(), CInt(Session("UserId")) _
            '                                                                                                            , Session("EmployeeAsOnDate").ToString(), True, -1, "", mintClaimRetirementId, Nothing)

            Dim dsList As DataSet = objClaimRetirementApprovalTran.GetClaimRetirementApproverExpesneList("List", True, Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                                        , Session("EmployeeAsOnDate").ToString(), True, -1, "", mintClaimRetirementId, Nothing)
            'Pinkal (14-Feb-2022) -- End

            

            Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & mintPriority, "", DataViewRowState.CurrentRows).ToTable
            mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            'Dim objClaimMst As New clsclaim_request_master
            'objClaimMst._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))

            Dim mintApproverApplicationStatusId As Integer = 2
            Dim mblnIsRejected As Boolean = False

            For i As Integer = 0 To dtApproverTable.Rows.Count - 1
                blnLastApprover = False
                'Dim mintApproverEmpID As Integer = -1
                'Dim mintApproverID As Integer = -1
                'objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))
                If mintApproverunkid = CInt(dtApproverTable.Rows(i)("approverunkid")) Then
                    If CType(sender, Button).ID.ToUpper = btnApprove.ID.ToUpper Then
                        mintStatusID = 1
                        mintVisibleID = 1
                        mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                        If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then blnLastApprover = True
                        mintApproverApplicationStatusId = mintStatusID
                    ElseIf CType(sender, Button).ID.ToUpper = btnRemarkOk.ID.ToUpper Then
                        mintStatusID = 3
                        mintVisibleID = 3
                        mdtApprovalDate = Nothing
                        blnLastApprover = True
                        mblnIsRejected = True
                        mintApproverApplicationStatusId = mintStatusID
                    End If
                Else
                    Dim mintApproverPriority As Integer = -1

                    Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintPriority & " AND approverunkid = " & CInt(dtApproverTable.Rows(i)("approverunkid")))

                    If dRow.Length > 0 Then
                        mintStatusID = 2
                        mintVisibleID = 1
                        'Pinkal (27-Apr-2021)-- Start
                        'KBC Enhancement  -  Working on Claim Retirement Enhancement.
                        mdtApprovalDate = Nothing
                        'Pinkal (27-Apr-2021) -- End
                    Else
                        mintApproverPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintPriority)))

                        If mintApproverPriority <= -1 Then
                            GoTo AssignApprover1
                            mintStatusID = 2
                            mintVisibleID = 1
                            'GoTo AssignApprover
                        ElseIf mblnIsRejected Then
                            mintVisibleID = -1

                        ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                            mintVisibleID = 1

                        ElseIf mintApproverPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                            mintVisibleID = 2
                        Else
                            mintVisibleID = -1
                        End If
                        mintStatusID = 2
                        mdtApprovalDate = Nothing
                    End If
                End If
AssignApprover1:

                objClaimRetirementApprovalTran._Approveremployeeunkid = CInt(dtApproverTable.Rows(i)("approveremployeeunkid"))
                objClaimRetirementApprovalTran._Approverunkid = CInt(dtApproverTable.Rows(i)("approverunkid"))
                objClaimRetirementApprovalTran._Statusunkid = mintStatusID
                objClaimRetirementApprovalTran._Visibleid = mintVisibleID
                objClaimRetirementApprovalTran._Crmasterunkid = mintClaimRequestMstId
                objClaimRetirementApprovalTran._Claimretirementunkid = mintClaimRetirementId
                objClaimRetirementApprovalTran._FormName = mstrModuleName
                objClaimRetirementApprovalTran._WebClientIP = Session("IP_ADD").ToString()
                objClaimRetirementApprovalTran._WebHostName = Session("HOST_NAME").ToString()
                objClaimRetirementApprovalTran._IsWeb = True
                objClaimRetirementApprovalTran._Userunkid = CInt(Session("UserId"))

                'If blnLastApprover AndAlso mintStatusID = 1 Then
                '    If mdtTran IsNot Nothing Then
                '        For Each dRow As DataRow In mdtTran.Rows
                '            Dim sMsg As String = String.Empty

                '            sMsg = objClaimMaster.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                '                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                '                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")))
                '            If sMsg <> "" Then
                '                DisplayMessage.DisplayMessage(sMsg, Me)
                '                Return False
                '            End If
                '            sMsg = ""
                '        Next
                '    End If
                'End If


                If objClaimRetirementApprovalTran.InsertUpdate_ClaimRetirement_Approval(Nothing, mstrRejectRemark, blnLastApprover, mdtApprovalDate) = False Then
                    DisplayMessage.DisplayMessage(objClaimRetirementApprovalTran._Message, Me)
                    Return False
                End If
            Next


            If mintApproverApplicationStatusId = 1 Then
                objClaimRetirementApprovalTran._FormName = mstrModuleName
                objClaimRetirementApprovalTran._IsWeb = True
                objClaimRetirementApprovalTran._WebClientIP = Session("IP_ADD").ToString()
                objClaimRetirementApprovalTran._WebHostName = Session("HOST_NAME").ToString()
                objClaimRetirementApprovalTran.SendMailToApprover(Session("Database_Name").ToString(), Session("EmployeeAsOnDate").ToString(), mintClaimRetirementId _
                                                                                , txtClaimRetirementNo.Text, CInt(cboEmployee.SelectedValue), mintPriority, 1, "crpriority > " & mintPriority _
                                                                                , CInt(Session("CompanyUnkId")), Session("ArutiSelfServiceURL").ToString(), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), mstrModuleName)

            End If

            objClaimRetirement._Claimretirementunkid = mintClaimRetirementId
            objClaimRetirement._Userunkid = CInt(Session("UserId"))
            objClaimRetirement._FormName = mstrModuleName
            objClaimRetirement._WebClientIP = Session("IP_ADD").ToString()
            objClaimRetirement._WebHostName = Session("HOST_NAME").ToString()
            objClaimRetirement._IsWeb = True

            If objClaimRetirement._statusunkid = 1 Then
                objClaimRetirement.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtClaimRetirementNo.Text, Session("EmployeeAsOnDate").ToString() _
                                                                           , objClaimRetirement._statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE _
                                                                           , 0, CInt(Session("UserId")), "")

            ElseIf objClaimRetirement._statusunkid = 3 Then

                objClaimRetirement.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtClaimRetirementNo.Text, Session("EmployeeAsOnDate").ToString() _
                                                                           , objClaimRetirement._statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE _
                                                                           , 0, CInt(Session("UserId")), txtRejectRemark.Text)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function



#Region " Claim Details"

    Private Sub FillClaimDetailCombo()
        Dim dsCombo As DataSet = Nothing
        Try
            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            'Select Case CInt(Me.ViewState("mintExpenseCategoryID"))
            '    Case enExpenseType.EXP_LEAVE
            '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, False)
            '    Case Else
            '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'End Select
            Select Case CInt(Me.ViewState("mintExpenseCategoryID"))
                Case enExpenseType.EXP_LEAVE
                    dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, False, False, False)
                Case Else
                    dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True, False, False)
            End Select
            'S.SANDEEP |10-MAR-2022| -- END

            With cboCDExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintExpeseTypeId.ToString()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Button Events"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtRemark.Text.Trim.Length <= 0 Then
                    popup_RemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You have not set your retirement remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                    popup_RemarkYesNo.Show()
                Else
                    AddRetirement()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If

            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtRemark.Text.Trim.Length <= 0 Then
                    popup_RemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You have not set your retirement remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                    popup_RemarkYesNo.Show()
                Else
                    EditRetirement()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If Is_Valid() = False Then Exit Sub

            If mdtImprestDetail IsNot Nothing AndAlso mdtImprestDetail.Rows.Count > 0 Then
                If mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of Decimal)("unitprice") <= 0).Count > 0 Then
                    popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Some of the expense(s) had 0 unit price for selected employee.") & vbCrLf & _
                                                                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Do you wish to continue?")
                    popup_UnitPriceYesNo.Show()
                    Exit Sub
                Else
                    ApproveExpense()
                End If
            Else
                ApproveExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            popupRejectRemark.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRemarkOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemarkOk.Click
        Try
            If txtRejectRemark.Text.Trim.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Rejection Remark cannot be blank. Rejection Remark is required information."), Me)
                txtRejectRemark.Focus()
                popupRejectRemark.Show()
                Exit Sub
            End If
            If Is_Valid() = False Then Exit Sub

            If SaveData(sender) Then

                If Request.QueryString.Count > 0 Then

                    If clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|")).ToArray().Length = 6 Then

                        SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True, Session("mdbname").ToString() _
                                                , CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                        Session.Abandon()
                        Response.Redirect("../Index.aspx", False)
                    Else
                        Response.Redirect(Session("rootpath").ToString() & "Claim_Retirement/wPg_ClaimRetirement_ApprovalList.aspx", False)
                    End If
                Else
                    Response.Redirect(Session("rootpath").ToString() & "Claim_Retirement/wPg_ClaimRetirement_ApprovalList.aspx", False)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Try
            Dim iRow As DataRow() = Nothing

            If mintClaimRetirementId > 0 Then
                If popup_DeleteReason.Reason.Trim.Length > 0 Then
                    iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & mintClaimretirementtranunkid & "' AND AUD <> 'D'")
                    iRow(0).Item("isvoid") = True
                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = popup_DeleteReason.Reason.Trim()
                    iRow(0).Item("AUD") = "D"
                    iRow(0).Item("voiduserunkid") = CInt(Session("UserId"))
                    iRow(0).AcceptChanges()

                    FillImprestDetail()
                    Clear_Controls()
                    btnEdit.Visible = False : btnAdd.Visible = True
                    cboImprest.Enabled = True
                    iRow = Nothing
                Else
                    popup_DeleteReason.Show()
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Expense Delete process fail.", Me)
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboImprest.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (23-Feb-2024) -- Start
            '(A1X-2461) NMB : R&D - Force manual user login on approval links..

            'If Request.QueryString.Count > 0 AndAlso Request.QueryString.Keys.Count = 6 Then
            If Request.QueryString.Count > 0 Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True _
                                          , Session("mdbname").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")) _
                                          , Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())

                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If
                'Pinkal (23-Feb-2024) -- End

                Response.Redirect("../Index.aspx", False)
            Else
                Session.Remove("ClaimRetirementId")
                Session.Remove("crmasterunkid")
                Session.Remove("ClaimRetireEmpId")
                Session.Remove("approverunkid")
                Session.Remove("approverEmpID")
                Session.Remove("Priority")
                Session.Remove("IsExternalApprover")
                Response.Redirect("wPg_ClaimRetirement_ApprovalList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewClaimDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewClaimDetail.Click
        Try

            Dim objClaimRequestTran As New clsclaim_request_tran
            objClaimRequestTran._ClaimRequestMasterId = mintClaimRequestMstId
            Dim mdtClaimRequest As DataTable = objClaimRequestTran._DataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isimprest") = True).ToList().CopyToDataTable()
            dgvClaimData.DataSource = mdtClaimRequest
            dgvClaimData.DataBind()
            objClaimRequestTran = Nothing

            If mdtClaimRequest IsNot Nothing AndAlso mdtClaimRequest.Rows.Count > 0 Then
                If mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCDClaimCurrency.Enabled = False
                    cboCDClaimCurrency.SelectedValue = CStr(mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                End If
                txtClaimGrandTotal.Text = CDec(mdtClaimRequest.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("amount"))).ToString(Session("fmtcurrency").ToString())
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, dtAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
            objDependant = Nothing

            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()

            blnClaimDetailPopup = True
            popupClaimDetail.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Protected Sub popup_RemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_RemarkYesNo.buttonYes_Click
        Try
            If btnAdd.Visible Then
                AddRetirement()
            ElseIf btnEdit.Visible Then
                EditRetirement()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (10-Feb-2021) -- End



#Region "Claim Details Button"

    Protected Sub btnCDCloseClaimDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCDCloseClaimDetail.Click
        Try
            blnClaimDetailPopup = False
            popupClaimDetail.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Scan/Attachment Preview"

    Protected Sub btnViewAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAttchment.Click
        Dim mstrPreviewIds As String = ""
        Dim dtTable As New DataTable
        Try

            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmployee.SelectedValue), _
                                                                             enScanAttactRefId.CLAIM_RETIREMENT, _
                                                                             enImg_Email_RefId.Claim_Retirement, _
                                                                             mintClaimRetirementId)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            popup_ShowAttchment._Webpage = Me
            popup_ShowAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_ShowAttchment_btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ShowAttchment.btnPreviewClose_Click
        Try
            popup_ShowAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Combobox Event"

    Protected Sub cboImprest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboImprest.SelectedIndexChanged
        Try
            If CInt(cboImprest.SelectedValue) > 0 Then
                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboImprest.SelectedValue)
                cboCostCenter.SelectedValue = "0"

                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If

                Dim objclaim As New clsclaim_request_master

                'Pinkal (24-Oct-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                If CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.General_Retirement Then
                    mdecFinalCRapprovedAmount = objclaim.GetFinalApproverApprovedAmount(CInt(cboEmployee.SelectedValue), mintExpeseTypeId, mintClaimRequestMstId, True, CInt(cboImprest.SelectedValue))
                ElseIf CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.ExpenseWise_Retirement Then
                    mdecFinalCRapprovedAmount = objclaim.GetFinalApproverApprovedAmount(CInt(cboEmployee.SelectedValue), mintExpeseTypeId, mintClaimRequestMstId, True, objExpMaster._LinkedExpenseId)
                End If
                'Pinkal (24-Oct-2019) -- End

                objclaim = Nothing

                If mdtImprestDetail Is Nothing OrElse mdtImprestDetail.Rows.Count <= 0 Then
                    txtBalance.Text = mdecFinalCRapprovedAmount.ToString(Session("fmtcurrency").ToString())
                    mdecExpenseBalance = mdecFinalCRapprovedAmount
                Else
                    Dim mdecAmount As Decimal = mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("expenseunkid") = CInt(cboImprest.SelectedValue)).Sum(Function(x) x.Field(Of Decimal)("amount"))
                    If mdecAmount <= 0 Then mdecExpenseBalance = mdecFinalCRapprovedAmount

                    If mintClaimRetirementId > 0 Then
                        txtBalance.Text = CDec(mdecFinalCRapprovedAmount - mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("expenseunkid") = CInt(cboImprest.SelectedValue)).Sum(Function(x) x.Field(Of Decimal)("amount"))).ToString(Session("fmtcurrency").ToString())
                    Else
                        txtBalance.Text = mdecExpenseBalance.ToString(Session("fmtcurrency").ToString())
                    End If
                End If

                If mdtImprestDetail IsNot Nothing Then
                    If mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = CStr(mdtImprestDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            Dim mstrSearch As String = ""

            If CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.General_Retirement Then
                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) <= 0"
                mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) <= 0 AND ISNULL(cmexpense_master.expenseunkid,0) in (" & mstrExpenseIDs & ")"
                'Pinkal (10-Feb-2021) -- End
            ElseIf CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.ExpenseWise_Retirement Then
                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) > 0"
                mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) > 0 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) in (" & mstrExpenseIDs & ")"
                'Pinkal (10-Feb-2021) -- End
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                dsCombo = objExpMaster.getComboList(enExpenseType.EXP_IMPREST, True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, mstrSearch)
            Else
                dsCombo = objExpMaster.getComboList(enExpenseType.EXP_IMPREST, True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, mstrSearch)
            End If

            With cboImprest
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (24-Oct-2019) -- End

#End Region

#Region "Textbox Events"

    Protected Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged, txtUnitPrice.TextChanged
        Try

            If IsNumeric(txtQuantity.Text) = False Then
                txtQuantity.Text = "0.00"
                Exit Sub
            End If

            If IsNumeric(txtUnitPrice.Text) = False Then
                txtUnitPrice.Text = "0.00"
                Exit Sub
            End If

            txtAmount.Text = CDec(CDec(txtQuantity.Text) * CDec(txtUnitPrice.Text)).ToString(Session("fmtcurrency").ToString())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Gridview Events"

#Region "Retirement "

    Protected Sub dgvRetirement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvRetirement.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhQuantity", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhUnitPrice", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhAmount", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirement, "dgcolhBalance", False, True)).Text).ToString(Session("fmtcurrency").ToString())
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvRetirement_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgvRetirement.RowUpdating
        Try
            If e.RowIndex >= 0 Then

                Dim iRow As DataRow() = Nothing

                If CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid").ToString()) > 0 Then
                    iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid").ToString()) & "' AND AUD <> 'D'")
                ElseIf dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Trim().Length > 0 Then
                    iRow = mdtImprestDetail.Select("GUID = '" & dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Trim() & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboImprest.SelectedValue = iRow(0).Item("expenseunkid").ToString
                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    Call cboImprest_SelectedIndexChanged(cboImprest, New EventArgs())

                    txtQuantity.Text = CDec(iRow(0).Item("quantity")).ToString(Session("fmtcurrency").ToString())
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString())).ToString()
                    txtAmount.Text = CDec(CDec(iRow(0).Item("quantity")) * CDec(iRow(0).Item("unitprice"))).ToString(Session("fmtCurrency").ToString())
                    'txtBalance.Text = mdecExpenseBalance.ToString(Session("fmtCurrency").ToString())
                    txtRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboImprest.Enabled = False
                    mdecEditOldExpenseAmt = CDec(CDec(iRow(0).Item("quantity")) * CDec(iRow(0).Item("unitprice")))
                End If

                mintEditRowIndex = e.RowIndex
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvRetirement_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgvRetirement.RowDeleting
        Try
            Dim iRow As DataRow() = Nothing
            If CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid")) > 0 Then
                iRow = mdtImprestDetail.Select("Claimretirementtranunkid = '" & CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid")) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    mintClaimretirementtranunkid = CInt(dgvRetirement.DataKeys(e.RowIndex).Values("Claimretirementtranunkid"))
                    popup_DeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Are you sure you want to delete?")
                    popup_DeleteReason.Reason = ""
                    popup_DeleteReason.Show()
                End If

            ElseIf dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString().Length > 0 Then
                iRow = mdtImprestDetail.Select("GUID = '" & dgvRetirement.DataKeys(e.RowIndex).Values("GUID").ToString() & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                    mdecExpenseBalance = mdecExpenseBalance + CDec(iRow(0).Item("amount"))
                    UpdateBalance(e.RowIndex, CDec(iRow(0).Item("amount")))
                End If
            End If

            iRow(0).AcceptChanges()
            FillImprestDetail()
            Clear_Controls()
            btnEdit.Visible = False : btnAdd.Visible = True
            cboImprest.Enabled = True
            iRow = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvRetirement_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvRetirement.RowCommand
        Try
            If e.CommandName = "Preview" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

                Dim mstrPreviewIds As String = ""
                Dim dtTable As New DataTable
                dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmployee.SelectedValue), _
                                                                                     enScanAttactRefId.CLAIM_RETIREMENT, _
                                                                                     enImg_Email_RefId.Claim_Retirement, _
                                                                                     CInt(dgvRetirement.DataKeys(gridRow.RowIndex).Values("Claimretirementtranunkid").ToString()))

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
                End If

                popup_ShowAttchment.ScanTranIds = mstrPreviewIds
                popup_ShowAttchment._Webpage = Me
                popup_ShowAttchment._ZipFileName = "Claim_Retirement_" + cboEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                popup_ShowAttchment.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Claim Detail"

    Protected Sub dgvClaimData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvClaimData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(3).Text.Trim.Length > 0 Then  'Quantity
                    e.Item.Cells(3).Text = Format(CDec(e.Item.Cells(3).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(4).Text.Trim.Length > 0 Then  'Unit price
                    e.Item.Cells(4).Text = Format(CDec(e.Item.Cells(4).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(5).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency").ToString())
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region


    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblclaimRetirementNo.ID, Me.LblclaimRetirementNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblClaimNo.ID, Me.lblClaimNo.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprestAmount.ID, Me.lblImprestAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblImprest.ID, Me.lblImprest.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblBalance.ID, Me.lblBalance.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblQuantity.ID, Me.lblQuantity.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRemark.ID, Me.lblRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblUnitprice.ID, Me.LblUnitprice.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAmount.ID, Me.lblAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCurrency.ID, Me.lblCurrency.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRetirementGrandTotal.ID, Me.LblRetirementGrandTotal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblBalancedue.ID, Me.LblBalancedue.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnAdd.ID, Me.btnAdd.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnEdit.ID, Me.btnEdit.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnApprove.ID, Me.btnApprove.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReject.ID, Me.btnReject.Text.Trim.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(3).FooterText, Me.dgvRetirement.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(4).FooterText, Me.dgvRetirement.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(5).FooterText, Me.dgvRetirement.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(6).FooterText, Me.dgvRetirement.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(7).FooterText, Me.dgvRetirement.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvRetirement.Columns(8).FooterText, Me.dgvRetirement.Columns(8).HeaderText)


            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, mstrModuleName1, Me.LblClaimDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDExpCategory.ID, Me.lblCDExpCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkViewDependantList.ID, Me.lnkViewDependantList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lnkViewDependantList.ID, Me.lnkViewDependantList.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimDate.ID, Me.lblCDClaimDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimEmployee.ID, Me.lblCDClaimEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimDomicileAddress.ID, Me.lblCDClaimDomicileAddress.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimExpense.ID, Me.lblCDClaimExpense.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimCurrency.ID, Me.lblCDClaimCurrency.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.lblCDClaimCostCenter.ID, Me.lblCDClaimCostCenter.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.LblCDClaimRemark.ID, Me.LblCDClaimRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.LblClaimGrandTotal.ID, Me.LblClaimGrandTotal.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.btnCDCloseClaimDetail.ID, Me.btnCDCloseClaimDetail.Text.Trim.Replace("&", ""))


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(0).FooterText, Me.dgvClaimData.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(1).FooterText, Me.dgvClaimData.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(2).FooterText, Me.dgvClaimData.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(3).FooterText, Me.dgvClaimData.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(4).FooterText, Me.dgvClaimData.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(5).FooterText, Me.dgvClaimData.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, Me.dgvClaimData.Columns(6).FooterText, Me.dgvClaimData.Columns(6).HeaderText)


            'Language.setLanguage(mstrModuleName2)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, mstrModuleName2, Me.LblEmpDependentsList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.dgDepedent.Columns(0).FooterText, Me.dgDepedent.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.dgDepedent.Columns(1).FooterText, Me.dgDepedent.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.dgDepedent.Columns(2).FooterText, Me.dgDepedent.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.dgDepedent.Columns(3).FooterText, Me.dgDepedent.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, Me.dgDepedent.Columns(4).FooterText, Me.dgDepedent.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, "btnClose", Me.btnCloseDependantList.Text.Replace("&", ""))


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblclaimRetirementNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblclaimRetirementNo.ID, Me.LblclaimRetirementNo.Text)
            Me.lblClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClaimNo.ID, Me.lblClaimNo.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkViewClaimDetail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.Text)
            Me.lnkViewClaimDetail.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkViewClaimDetail.ID, Me.lnkViewClaimDetail.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Me.lblImprestAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprestAmount.ID, Me.lblImprestAmount.Text)
            Me.lblImprest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblImprest.ID, Me.lblImprest.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Me.lblQuantity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQuantity.ID, Me.lblQuantity.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemark.ID, Me.lblRemark.Text)
            Me.LblUnitprice.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblUnitprice.ID, Me.LblUnitprice.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.LblRetirementGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRetirementGrandTotal.ID, Me.LblRetirementGrandTotal.Text)
            Me.LblBalancedue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblBalancedue.ID, Me.LblBalancedue.Text)

            Me.btnAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdd.ID, Me.btnAdd.Text.Trim.Replace("&", ""))
            Me.btnEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnEdit.ID, Me.btnEdit.Text.Trim.Replace("&", ""))
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text.Trim.Replace("&", ""))
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReject.ID, Me.btnReject.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Me.dgvRetirement.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(3).FooterText, Me.dgvRetirement.Columns(3).HeaderText)
            Me.dgvRetirement.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(4).FooterText, Me.dgvRetirement.Columns(4).HeaderText)
            Me.dgvRetirement.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(5).FooterText, Me.dgvRetirement.Columns(5).HeaderText)
            Me.dgvRetirement.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(6).FooterText, Me.dgvRetirement.Columns(6).HeaderText)
            Me.dgvRetirement.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(7).FooterText, Me.dgvRetirement.Columns(7).HeaderText)
            Me.dgvRetirement.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvRetirement.Columns(8).FooterText, Me.dgvRetirement.Columns(8).HeaderText)


            'Language.setLanguage(mstrModuleName1)
            Me.LblClaimDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.LblClaimDetails.Text)
            Me.lblCDExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDExpCategory.ID, Me.lblCDExpCategory.Text)
            Me.lblCDClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkViewDependantList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkViewDependantList.ID, Me.lnkViewDependantList.Text)
            Me.lnkViewDependantList.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lnkViewDependantList.ID, Me.lnkViewDependantList.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Me.lblCDClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimNo.ID, Me.lblCDClaimNo.Text)
            Me.lblCDClaimDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimDate.ID, Me.lblCDClaimDate.Text)
            Me.lblCDClaimEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimEmployee.ID, Me.lblCDClaimEmployee.Text)
            Me.lblCDClaimDomicileAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimDomicileAddress.ID, Me.lblCDClaimDomicileAddress.Text)
            Me.lblCDClaimExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimExpense.ID, Me.lblCDClaimExpense.Text)
            Me.lblCDClaimCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimCurrency.ID, Me.lblCDClaimCurrency.Text)
            Me.lblCDClaimCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCDClaimCostCenter.ID, Me.lblCDClaimCostCenter.Text)
            Me.LblCDClaimRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblCDClaimRemark.ID, Me.LblCDClaimRemark.Text)
            Me.LblClaimGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblClaimGrandTotal.ID, Me.LblClaimGrandTotal.Text)
            Me.btnCDCloseClaimDetail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnCDCloseClaimDetail.ID, Me.btnCDCloseClaimDetail.Text.Trim.Replace("&", ""))


            Me.dgvClaimData.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(0).FooterText, Me.dgvClaimData.Columns(0).HeaderText)
            Me.dgvClaimData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(1).FooterText, Me.dgvClaimData.Columns(1).HeaderText)
            Me.dgvClaimData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(2).FooterText, Me.dgvClaimData.Columns(2).HeaderText)
            Me.dgvClaimData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(3).FooterText, Me.dgvClaimData.Columns(3).HeaderText)
            Me.dgvClaimData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(4).FooterText, Me.dgvClaimData.Columns(4).HeaderText)
            Me.dgvClaimData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(5).FooterText, Me.dgvClaimData.Columns(5).HeaderText)
            Me.dgvClaimData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.dgvClaimData.Columns(6).FooterText, Me.dgvClaimData.Columns(6).HeaderText)

            'Language.setLanguage(mstrModuleName2)
            Me.LblEmpDependentsList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, Me.LblEmpDependentsList.Text)
            Me.dgDepedent.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(0).FooterText, Me.dgDepedent.Columns(0).HeaderText)
            Me.dgDepedent.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(1).FooterText, Me.dgDepedent.Columns(1).HeaderText)
            Me.dgDepedent.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(2).FooterText, Me.dgDepedent.Columns(2).HeaderText)
            Me.dgDepedent.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(3).FooterText, Me.dgDepedent.Columns(3).HeaderText)
            Me.dgDepedent.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.dgDepedent.Columns(4).FooterText, Me.dgDepedent.Columns(4).HeaderText)
            Me.btnCloseDependantList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnCloseDependantList.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Employee is mandatory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Imprest is mandatory information. Please select Imprest to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Quantity is mandatory information. Please enter Quantity to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Currency is mandatory information. Please select Currency to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Please add atleast one expense retirement in order to save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Rejection Remark cannot be blank. Rejection Remark is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, you cannot add same Imprest again in the below list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Some of the expense(s) had 0 unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "You have not set your retirement remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Are you sure you want to delete?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
