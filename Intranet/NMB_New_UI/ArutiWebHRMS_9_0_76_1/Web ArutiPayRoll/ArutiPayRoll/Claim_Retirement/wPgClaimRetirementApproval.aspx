<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgClaimRetirementApproval.aspx.vb"
    Inherits="Claim_Retirement_wPgClaimRetirementApproval" Title="Claim Retirement Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <h2>
                    <asp:Label ID="lblPageHeader" runat="server" Text="Retirement Information"></asp:Label>
                </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div class="card inner    -card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboEmployee" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="LblclaimRetirementNo" runat="server" Text="Retirement No" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtClaimRetirementNo" runat="server" ReadOnly="true" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                    <asp:Label ID="lblClaimNo" runat="server" Text="Claim No." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtClaimNo" runat="server" ReadOnly="true" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1  m-t-35 p-l-0">
                                                    <asp:LinkButton ID="lnkViewClaimDetail" runat="server" ToolTip="View Claim Detail" >
                                                            <i class="fas fa-info-circle"></i>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblImprestAmount" runat="server" Text="Imprest Amount" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtImprestAmount" runat="server" ReadOnly="true" Style="text-align: right"
                                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <asp:Label ID="lblImprest" runat="server" Text="Imprest" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboImprest" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <asp:Label ID="lblBalance" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtBalance" Style="text-align: right" Text="0.00" runat="server"
                                                                ReadOnly="true" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboCostCenter" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <asp:Label ID="lblQuantity" runat="server" Text="Quantity" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtQuantity" runat="server" AutoPostBack="true" Style="text-align: right"
                                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboCurrency" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <asp:Label ID="LblUnitprice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtUnitPrice" runat="server" AutoPostBack="true" Style="text-align: right"
                                                                Text="0.00" onKeypress="return onlyNumbers(this,event);" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" Text="0.00"
                                                                ReadOnly="true" onKeypress="return onlyNumbers(this,event);" CssClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-primary" Visible="false" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 220px">
                                        <asp:GridView ID="dgvRetirement" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                            DataKeyNames="Claimretirementtranunkid,Claimretirementunkid,crmasterunkid,expenseunkid,GUID"
                                            CssClass="table table-hover table-bordered">
                                            <Columns>
                                                <asp:TemplateField FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Update" CssClass="gridedit"
                                                                ToolTip="Edit">
                                                                              <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                            <asp:LinkButton ID="lnkdelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                ToolTip="Delete">
                                                                              <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="25px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                            CommandName="Preview">
                                                                           <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="expense" HeaderText="Imprest" FooterText="dgcolhImprest" />
                                                <asp:BoundField DataField="quantity" HeaderText="Quantity" FooterText="dgcolhQuantity"
                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="unitprice" HeaderText="Unit Price" FooterText="dgcolhUnitPrice"
                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="balance" HeaderText="Balance" FooterText="dgcolhBalance"
                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Visible="false" />
                                                <asp:BoundField DataField="expense_remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="LblRetirementGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtRetirementGrandTotal" runat="server" ReadOnly="true" Style="text-align: right;"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="LblBalancedue" runat="server" Text="Balance Due" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtBalanceDue" runat="server" ReadOnly="true" Style="text-align: right;"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnViewAttchment" runat="server" Visible="false" Text="View Scan/Attchment"
                                CssClass="btn btn-default" />
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
       
            <cc1:ModalPopupExtender ID="popupClaimDetail" runat="server" BackgroundCssClass="modal-backdrop"
                CancelControlID="HiddenField2" PopupControlID="pnlClaimDetail" TargetControlID="HiddenField2">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlClaimDetail" runat="server" CssClass="card modal-dialog modal-lg"
                Style="display: none;" >
                <div class="header">
                    <h2>
                        <asp:Label ID="LblClaimDetails" runat="server" Text="Claim Details"></asp:Label>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <asp:LinkButton ID="lnkViewDependantList" runat="server" ToolTip="View Depedents List">
                                             <i class="fas fa-restroom"></i>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <div class="body" style = "height:475px" >
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="card inner-card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCDExpCategory" runat="server" Enabled="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimNo" runat="server" Text="Claim No." CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtCDClaimNo" runat="server" Enabled="false" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimDate" runat="server" Text="Date." CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtpCDClaimDate" runat="server" AutoPostBack="false" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCDClaimEmployee" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimDomicileAddress" runat="server" Text="Domicile Address"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtCDClaimDomicileAddress" runat="server" TextMode="MultiLine" ReadOnly="true"
                                                        CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="card inner-card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCDClaimExpense" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCDClaimCurrency" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCDClaimCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCDClaimCostCenter" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblCDClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtCDClaimRemark" runat="server" TextMode="MultiLine" ReadOnly="true"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                               <div class="card inner-card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 300px">
                                                <asp:DataGrid ID="dgvClaimData" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                    ShowFooter="False" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc" />
                                                        <asp:BoundColumn DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route" />
                                                        <asp:BoundColumn DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" />
                                                        <asp:BoundColumn DataField="quantity" FooterText="dgcolhQty" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="Quantity" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundColumn DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="Unit Price" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundColumn DataField="amount" FooterText="dgcolhAmount" HeaderStyle-HorizontalAlign="Right"
                                                            HeaderText="Amount" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundColumn DataField="expense_remark" FooterText="dgcolhExpenseRemark" HeaderText="Expense Remark"
                                                            Visible="true" />
                                                        <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" Visible="false" />
                                                        <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" Visible="false" />
                                                        <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" Visible="false" />
                                                    </Columns>
                                                    <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3">
                                            <asp:Label ID="LblClaimGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtClaimGrandTotal" runat="server" ReadOnly="true" Style="text-align: right;"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnCDCloseClaimDetail" runat="server" Text="Close" CssClass="btn btn-primary" />
                    <asp:HiddenField ID="HiddenField2" runat="server" />
                    <asp:HiddenField ID="HiddenField21" runat="server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupDependantList" runat="server" BackgroundCssClass="modal-backdrop"
                CancelControlID="btnCloseDependantList" PopupControlID="pnlDependantList" TargetControlID="lnkViewDependantList"
                Drag="true" PopupDragHandleControlID="pnlDependantList">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlDependantList" runat="server" CssClass="card modal-dialog" Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive" style="height: 250px;">
                                <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    CssClass="table table-hover table-bordered">
                                    <Columns>
                                        <asp:BoundColumn DataField="dependants" HeaderText="Name" FooterText="dgcolhName" />
                                        <asp:BoundColumn DataField="gender" HeaderText="Gender" FooterText="dgcolhGender" />
                                        <asp:BoundColumn DataField="age" HeaderText="Age" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAge" />
                                        <asp:BoundColumn DataField="Months" HeaderText="Month" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhMonth" />
                                        <asp:BoundColumn DataField="relation" HeaderText="Relation" FooterText="dgcolhRelation" />
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnCloseDependantList" runat="server" Text="Close" CssClass="btn btn-primary" />
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="popupRejectRemark" runat="server" BackgroundCssClass="modal-backdrop"
                CancelControlID="btnRemarkClose" PopupControlID="pnlReject"
                TargetControlID="txtRejectRemark">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlReject" runat="server" CssClass="card modal-dialog" Style="display: none;"
                DefaultButton="btnRemarkOk">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblTitle" runat="server" Text="Rejection Remark"></asp:Label>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtRejectRemark" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnRemarkOk" runat="server" Text="Ok" CssClass="btn btn-primary" />
                    <asp:Button ID="btnRemarkClose" runat="server" Text="Close" CssClass="btn btn-default" />
                </div>
            </asp:Panel>
            <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
            <uc3:Confirmation ID="popup_UnitPriceYesNo" runat="server" Message="" Title="Confirmation" />
            <uc3:Confirmation ID="popup_RemarkYesNo" runat="server" Message="" Title="Confirmation" />
            <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
