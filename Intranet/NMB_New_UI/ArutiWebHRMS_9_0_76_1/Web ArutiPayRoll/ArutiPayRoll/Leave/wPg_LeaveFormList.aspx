﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LeaveFormList.aspx.vb"
    Inherits="Leave_wPg_LeaveFormList" Title="Leave Form List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());

            }
        }
    </script>

    <script>
        function IsValidAttach() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= CancelFormConfirmation.ClientID %>_Panel1").css("z-index", "100002");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100005");
        }
    </script>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_LeaveFormList.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
        $("body").on("click", "input[type=file]", function() {
            return IsValidAttach();
        });
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Form List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlFilEmpList" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlfilLeave" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlApprover" runat="server">
                                        <asp:Label ID="lblApprover" Enabled="true" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprover" CssClass="textboxfil form-control" runat="server" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtfilStartDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlFormNo" runat="server">
                                        <asp:Label ID="lblFormNo" runat="server" Text="Application No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtdrpformNo" CssClass="textboxfil form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplyDate" runat="server" Text="Apply Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtfilApplyDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReturnDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtfilReturnDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="status1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" CssClass="btn btn-primary" runat="server" Text="New" />
                                <asp:Button ID="BtnSearch" CssClass="btn btn-default" runat="server" Text="Search" />
                                <asp:Button ID="Btnclose" runat="server" CssClass=" btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="dgView" runat="server" Width="150%" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit" HeaderStyle-Width="40px"
                                                        ItemStyle-Width="40px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Select" ToolTip="Edit">
                                                                         <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--0--%>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete" HeaderStyle-Width="40px"
                                                        ItemStyle-Width="40px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                             <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" FooterText="mnuCancelLeaveForm"
                                                        HeaderStyle-Width="40px" ItemStyle-Width="40px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImgCancel" runat="server" ImageUrl="~/images/CancelLeaveForm.png"
                                                                ToolTip="Cancel Leave" CommandName="Cancel" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--2--%>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" FooterText="mnuPrintForm"
                                                        HeaderStyle-Width="40px" ItemStyle-Width="40px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImgPrint" runat="server" ImageUrl="~/images/PrintSummary_16.png"
                                                                ToolTip="Print Form" CommandName="Print" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="formno" HeaderText="Application No" FooterText="colhFormNo" />
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" FooterText="colhEmpCode" />
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="colhEmployee" />
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="leavename" HeaderText="Leave" FooterText="colLeaveType" />
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="startdate" HeaderText="Start Date" FooterText="colhStartDate"
                                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="returndate" HeaderText="End Date" FooterText="colhEndDate"
                                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="days" HeaderText="Days" FooterText="colhLeaveDays" />
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="Approved_StartDate" HeaderText="Approved Start Date"
                                                        FooterText="colhApprovedStartDate" />
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="Approved_EndDate" HeaderText="Approved End Date" FooterText="colhApprovedEndDate" />
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="Approved_Days" HeaderText="Approved Days" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="colhApprovedDays" />
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="status" HeaderText="Status" FooterText="colhStatus" ItemStyle-Width="150px" />
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" Visible="false" />
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeunkId" Visible="false" FooterText="objcolhEmployeeId" />
                                                    <%--16--%>
                                                    <asp:BoundColumn DataField="leavetypeunkid" HeaderText="LeaveTypeunkId" Visible="false"  FooterText="objcolhLeaveTypeId" />
                                                    <%--17--%>
                                                    <%-- 'Pinkal (01-Oct-2018) -- Start    'Enhancement - Leave Enhancement for NMB.--%>
                                                    <asp:BoundColumn DataField="skipapproverflow" HeaderText="Skip Approver Flow" Visible="false" />
                                                    <%--18--%>
                                                    <%-- 'Pinkal (01-Oct-2018) -- End --%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupCancel" BackgroundCssClass="modal-backdrop" TargetControlID="txtCancelEmployee"
                    runat="server" PopupControlID="pnlCancelPopup" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCancelPopup" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Leave Cancel Form" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="height: 475px">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancelEmployeeCode" runat="server" Text="Employee Code" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCancelEmployeeCode" runat="server" ReadOnly="true" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancelEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCancelEmployee" runat="server" ReadOnly="true" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveFormNo" runat="server" Text="Form No" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLeaveFormNo" runat="server" ReadOnly="true" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancelLeaveType" runat="server" Text="Leave Type" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCancelLeaveType" runat="server" ReadOnly="true" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancelFormNo" runat="server" Text="Cancel Form No" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCancelFormNo" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancelReason" runat="server" Text="Reason" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCancelReason" runat="server" Rows="2" TextMode="MultiLine" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel ID="upCheck" runat="server">
                                            <ContentTemplate>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                    <li role="presentation" class="active"><a href="#TabContainer1" data-toggle="tab">
                                                        <asp:Label ID="lblLeaveFormDate" runat="server" Text="Leave Form Date" CssClass="form-label" />
                                                    </a></li>
                                                    <!-- Tab panes -->
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="TabContainer1">
                                                        <asp:CheckBoxList ID="chkListLeavedate" runat="server" AppendDataBoundItems="True"
                                                            AutoPostBack="True" RepeatLayout="Flow" RepeatColumns="1">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkSelectAll" EventName="CheckedChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <ul class="list-group">
                                    <li class="list-group-item active">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="badge"></asp:Label>
                                    </li>
                                    <li class="list-group-item">
                                        <asp:Label ID="LblLeaveBF" runat="server" Text="Leave BF"></asp:Label>
                                        <asp:Label ID="objlblLeaveBFvalue" runat="server" Text="0.00" CssClass="badge"></asp:Label>
                                    </li>
                                    <li class="list-group-item">
                                        <asp:Label ID="lblToDateAccrued" runat="server" Text="Total Accrued To Date"></asp:Label>
                                        <asp:Label ID="objlblAccruedToDateValue" runat="server" Text="0" CssClass="badge"></asp:Label>
                                    </li>
                                    <li class="list-group-item">
                                        <asp:Label ID="lblTotalIssuedToDate" runat="server" Text="Total Issued"></asp:Label>
                                        <asp:Label ID="objlblIssuedToDateValue" runat="server" Text="0.00" CssClass="badge"></asp:Label>
                                    </li>
                                    <li class="list-group-item">
                                        <asp:Label ID="LblTotalAdjustment" runat="server" Text="Total Adjustment"></asp:Label>
                                        <asp:Label ID="objlblTotalAdjustment" runat="server" Text="0.00" CssClass="badge"></asp:Label>
                                    </li>
                                    <li class="list-group-item">
                                        <asp:Label ID="LblBalanceAsonDate" runat="server" Text="Balance As on Date"></asp:Label>
                                        <asp:Label ID="objlblBalanceAsonDateValue" runat="server" align="left" Text="0.00"
                                            CssClass="badge"></asp:Label>
                                    </li>
                                    <li class="list-group-item">
                                        <asp:Label ID="lblBalance" runat="server" Text="Total Balance"></asp:Label>
                                        <asp:Label ID="objlblBalanceValue" runat="server" align="left" Text="0.00" CssClass="badge"></asp:Label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                       <%-- <asp:LinkButton ID="lnkCancelExpense" runat="server" Text="Cancel Expense"></asp:LinkButton>--%>
                        <asp:Button ID="lnkCancelExpense" runat="server" Text="Cancel Expense" CssClass="btn btn-primary">
                        </asp:Button>
                        <asp:Button ID="btnCancelScanAttachment" runat="server" Text="Browse" CssClass="btn btn-primary pull-left" />
                        <asp:Button ID="btnCancelSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                        <asp:Button ID="btnCancelClose" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                    </div>
                    <uc7:Confirmation ID="popup_YesNo" Title="Title" runat="server" />
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_CancelExp" runat="server" CancelControlID="btnCancelExpCancel"
                    BackgroundCssClass="modal-backdrop" PopupControlID="pnl_CancelExp" TargetControlID="btnHiddenExp">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CancelExp" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label9" runat="server" Text="Cancel Expense Form Information"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 475px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelExpCategory" runat="server" Text="Exp. Cat." CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboCancelExpCategory" runat="server" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelExpPeriod" runat="server" Text="Period" Visible="false" CssClass="form-label"></asp:Label>
                                <asp:Label ID="lblCancelExpClaimNO" runat="server" Text="Claim No." CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboCancelExpPeriod" runat="server" Width="0px" Visible="false"
                                        Enabled="false" />
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCancelExpClaimNo" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelExpDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                <uc2:DateCtrl ID="dtpCancelExpDate" runat="server" AutoPostBack="False" Enabled="false" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelExpEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCanelExpEmployee" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        <asp:HiddenField ID="txtCancelExpEmployeeid" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCancelExpRemark" runat="server" Text="Cancel Remark" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCancelExpRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px;">
                                    <asp:GridView ID="dgvCancelExpData" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        ShowFooter="False" PageSize="15" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Width="30px" HeaderStyle-Width="30px">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="ChkCancelExpAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkCancelExpAll_CheckedChanged"
                                                        CssClass="filled-in" Text=" " />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkCancelExpSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkCancelExpSelect_CheckedChanged"
                                                        CssClass="filled-in" Text=" " />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                FooterText="dgcolhExpense" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="UoM" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM"
                                                HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="true" FooterText="dgcolhCurrency" />
                                            <asp:BoundField DataField="expense_remark" HeaderText="dgcolhRemark" ReadOnly="true"
                                                Visible="false" />
                                            <asp:BoundField DataField="crapprovaltranunkid" HeaderText="objdgcolhApproverTranId"
                                                ReadOnly="true" Visible="false" />
                                            <asp:BoundField DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="lblCancelExpGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtCancelExpGrandTotal" runat="server" Enabled="false" Style="text-align: right;"
                                            CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnCancelExpSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCancelExpCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                        <asp:HiddenField ID="btnHiddenExp" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-25">
                                <div id="fileuploader">
                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Add" />
                                </div>
                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                    Text="Browse" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 150px;">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                </asp:Panel>
                <uc9:DeleteReason ID="popup_DeleteForm" runat="server" Title="Are you sure you want to delete selected transaction?" />
                <uc7:Confirmation ID="CancelFormConfirmation" runat="server" Message="" Title="Confirmation" />
                <uc7:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
