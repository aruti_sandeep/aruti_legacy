﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AddEditLeaveApprover.aspx.vb" Inherits="Leave_wPg_AddEditLeaveApprover"
    Title="Add/Edit Leave Approver" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
                    $("#scrollable-container1").scrollTop($(scroll1.Y).val());

                }
            }

//            $("[id*=ChkAll]").live("click", function() {
//                var chkHeader = $(this);
//                var grid = $(this).closest("table");
//                $("input[type=checkbox]", grid).each(function() {
//                    if (chkHeader.is(":checked")) {
//                        debugger;
//                        if ($(this).is(":visible")) {
//                            $(this).attr("checked", "checked");
//                        }

//                    } else {
//                        $(this).removeAttr("checked");
//                    }
//                });
//            });

//            $("[id*=ChkgvSelect]").live("click", function() {
//                var grid = $(this).closest("table");
//                var chkHeader = $("[id*=chkHeader]", grid);
//                var row = $(this).closest("tr")[0];

//                debugger;
//                if (!$(this).is(":checked")) {
//                    var row = $(this).closest("tr")[0];
//                    chkHeader.removeAttr("checked");
//                } else {

//                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                        chkHeader.attr("checked", "checked");
//                    }
//                }
//            });


//            $("[id*=ChkSelectedEmpAll]").live("click", function() {
//                var chkHeader = $(this);
//                var grid = $(this).closest("table");
//                $("input[type=checkbox]", grid).each(function() {
//                    if (chkHeader.is(":checked")) {
//                        debugger;
//                        if ($(this).is(":visible")) {
//                            $(this).attr("checked", "checked");
//                        }

//                    } else {
//                        $(this).removeAttr("checked");
//                    }
//                });
//            });

//            $("[id*=ChkgvSelectedEmp]").live("click", function() {
//                var grid = $(this).closest("table");
//                var chkHeader = $("[id*=chkHeader]", grid);
//                var row = $(this).closest("tr")[0];

//                debugger;
//                if (!$(this).is(":checked")) {
//                    var row = $(this).closest("tr")[0];
//                    chkHeader.removeAttr("checked");
//                } else {

//                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                        chkHeader.attr("checked", "checked");
//                    }
//                }
//            });


            $("body").on("click", "[id*=ChkAll]", function() {
                var chkHeader = $(this);
                debugger;
                var grid = $(this).closest("table");
                $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvSelect]", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=ChkAll]", grid);
                debugger;
                if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });


            $("body").on("click", "[id*=ChkSelectedEmpAll]", function() {
                var chkHeader = $(this);
                debugger;
                var grid = $(this).closest("table");
                $("[id*=ChkgvEmpSelected]").prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvEmpSelected]", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=ChkSelectedEmpAll]", grid);
                debugger;
                if ($("[id*=ChkgvEmpSelected]", grid).length == $("[id*=ChkgvEmpSelected]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });



            $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearch').val().length > 0) {
                    $('#<%= GvEmployee.ClientID %> tbody tr').hide();
                    $('#<%= GvEmployee.ClientID %> tbody tr:first').show();
                    $('#<%= GvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
                }
                else if ($('#txtSearch').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= GvEmployee.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearch').val('');
                $('#<%= GvEmployee.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearch').focus();
            }


//            $("[id*=chkSelectAll]").live("click", function() {
//                var chkHeader = $(this);
//                var grid = $(this).closest("table");
//                $("input[type=checkbox]", grid).each(function() {
//                    if (chkHeader.is(":checked")) {
//                        debugger;
//                        if ($(this).is(":visible")) {
//                            $(this).attr("checked", "checked");
//                        }

//                    } else {
//                        $(this).removeAttr("checked");
//                    }
//                });
//            });

//            $("[id*=ChkgvSelectAll]").live("click", function() {
//                var grid = $(this).closest("table");
//                var chkHeader = $("[id*=chkHeader]", grid);
//                var row = $(this).closest("tr")[0];

//                debugger;
//                if (!$(this).is(":checked")) {
//                    var row = $(this).closest("tr")[0];
//                    chkHeader.removeAttr("checked");
//                } else {

//                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                        chkHeader.attr("checked", "checked");
//                    }
//                }
//            });

            $("body").on("click", "[id*=chkSelectAll]", function() {
                var chkHeader = $(this);
                debugger;
                var grid = $(this).closest("table");
                $("[id*=ChkgvSelectAll]").prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvSelectAll]", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkSelectAll]", grid);
                debugger;
                if ($("[id*=ChkgvSelectAll]", grid).length == $("[id*=ChkgvSelectAll]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                }
                else {
                    chkHeader.prop("checked", false);
                }
            });

            
    </script>

        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                  <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                 <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Leave Approve"></asp:Label>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                             <div class="header">
                                                <h2>
                                                       <asp:Label ID="lblCaption" runat="server" Text="Add / Edit Leave Approver"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                 <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:CheckBox ID="chkExternalApprover" runat="server" Text="Make External Approver" AutoPostBack="true" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                          <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                 <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <asp:Label ID="lblApproverName" runat="server" Text="Approver Name" CssClass="form-label"></asp:Label>
                                                             <div class="form-group">
                                                                     <asp:DropDownList ID="drpApprover" runat="server" AutoPostBack="true" />
                                                             </div>  
                                                    </div>
                                                 </div>   
                                                 <div class="row clearfix"> 
                                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblApproveLevel" runat="server" Text="Approver Level" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                    <asp:DropDownList ID="drpLevel" runat="server" />
                                                            </div>
                                                       </div>
                                                 </div>
                                                 <div class="row clearfix"> 
                                                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                 <asp:Label ID="LblUser" runat="server" Text="User" CssClass="form-label" />
                                                                 <div class="form-group">
                                                                           <asp:DropDownList ID="drpUser" runat="server" />
                                                                 </div>
                                                         </div>
                                                  </div>
                                                 <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                 <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 p-l-0">
                                                                      <div class="form-group">
                                                                             <div class="form-line">
                                                                                    <input type="text" id="txtSearch" name="txtSearch" placeholder = "Type To Search Text"  maxlength="50"  onkeyup="FromSearching();" class="form-control" />
                                                                             </div>
                                                                      </div>       
                                                                 </div>
                                                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-t-5 p-l-0">
                                                                        <asp:Button ID="lnkMapLeaveType" runat="server" Text="Leave Type Mapping" CssClass = "btn btn-primary" />
                                                                 </div>
                                                        </div>
                                                   </div>  
                                                 <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">   
                                                                <div class="table-responsive" style="height: 350px">
                                                                        <asp:GridView ID="GvEmployee" runat="server" AutoGenerateColumns="False" ShowFooter="False" CssClass="table table-hover table-bordered"
                                                                            AllowPaging="false" DataKeyNames = "employeeunkid,Code,Employee Name,Department,Section,Job">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkAll" runat="server"  CssClass="filled-in" Text=" " />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelect" runat="server"  CssClass="filled-in" Text=" " />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                                                <asp:BoundField DataField="Employee Name" HeaderText="Employee" ReadOnly="true" FooterText="colhEmp" />
                                                                                <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                    Visible="false" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                </div>
                                                        </div>
                                                   </div>     
                                           </div>
                                           <div class="footer">
                                                 <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                           </div>     
                                        </div>
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                           <div class="card inner-card">
                                                <div class="body">
                                                       <div class="row clearfix">
                                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 715px">
                                                                          <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                AllowPaging="false" DataKeyNames="leaveapprovertranunkid,GUID,employeeunkid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkSelectedEmpAll" runat="server"  CssClass="filled-in" Text=" " />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                             <asp:CheckBox ID="ChkgvEmpSelected" runat="server"  CssClass="filled-in" Text=" " />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                                                    <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                                                    <asp:BoundField DataField="departmentname" HeaderText="Department" ReadOnly="true"
                                                                                        FooterText="colhDept" />
                                                                                    <asp:BoundField DataField="sectionname" HeaderText="Section" ReadOnly="true" FooterText="colhSection" />
                                                                                    <asp:BoundField DataField="jobname" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                        Visible="false" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                </div>
                                                          </div>
                                                       </div>
                                                </div>
                                                <div class="footer">      
                                                         <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-primary" />
                                                </div>
                                           </div>
                                     </div> 
                                </div>
                             </div>
                             <div class="footer">
                                      <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                      <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                             </div>
                         </div>
                      </div>
                 </div>               

                    <cc1:ModalPopupExtender ID="popupLeaveMapping" BackgroundCssClass="modal-backdrop"
                        TargetControlID="LblLeaveMapping" runat="server" PopupControlID="pnlLeaveMappingPopup"
                        CancelControlID="btnTempClose" />
                
                    <asp:Panel ID="pnlLeaveMappingPopup" runat="server" CssClass="card modal-dialog" Style="display: none;">
                      <div class="header">
                        <h2>
                            <asp:Label ID="LblLeaveMapping" runat="server" Text="Leave Type Mapping"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style = "height:450px">
                         <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <asp:Label ID="LblApproverLeaveMap" runat="server" Text="Approver" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                     <asp:Label ID="LblApproverLeaveMapVal" runat="server" Text="" CssClass="form-label" />
                            </div>
                        </div>
                         <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="LblLevelLeaveMap" runat="server" Text="Approver Level" CssClass="form-label"  />
                                </div>
                                  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                          <asp:Label ID="LblLevelLeaveMapVal" runat="server" Text="" CssClass="form-label"  />
                                  </div>
                         </div>
                         <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 350px;">
                                    
                                                <asp:GridView ID="GvLeaveTypeMapping" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                            AllowPaging="false" DataKeyNames = "leavetypeunkid,leavetypecode">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="filled-in" Text=" " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="ChkgvSelectAll" runat="server" CssClass="filled-in" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="leavetypecode" HeaderText="Leave Code" ReadOnly="True" />
                                                                <asp:BoundField DataField="leavename" HeaderText="Leave Type" ReadOnly="True" />
                                                                <asp:BoundField DataField="ispaid" HeaderText="IsPaid" ReadOnly="True" />
                                                            </Columns>
                                                            <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Left" />
                                               </asp:GridView>
                                    
                                    </div>
                               </div>
                         </div>      
                    </div>      
                    <div class="footer">
                           <asp:Button ID="btnLeaveMapSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            <asp:Button ID="btnLeaveMapClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            <asp:HiddenField ID="btnTempClose" runat="server" />
                    </div>
                                   
                    
                    
                     
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
