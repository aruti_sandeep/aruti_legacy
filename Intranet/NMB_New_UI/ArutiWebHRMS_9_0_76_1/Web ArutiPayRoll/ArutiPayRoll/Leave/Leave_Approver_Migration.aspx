<%@ Page Title="Transfer Leave Approver" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Leave_Approver_Migration.aspx.vb" Inherits="Leave_Approver_Migration" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());

            }
        }

//        $("[id*=chkHeder1]").live("click", function() {
//            var chkHeader = $(this);
//            var grid = $(this).closest("table");
//            $("input[type=checkbox]", grid).each(function() {
//                if (chkHeader.is(":checked")) {
//                    debugger;
//                    if ($(this).is(":visible")) {
//                        $(this).attr("checked", "checked");
//                    }

//                } else {
//                    $(this).removeAttr("checked");
//                }
//            });
//        });

//        $("[id*=chkbox1]").live("click", function() {
//            var grid = $(this).closest("table");
//            var chkHeader = $("[id*=chkHeader]", grid);
//            var row = $(this).closest("tr")[0];

//            debugger;
//            if (!$(this).is(":checked")) {
//                var row = $(this).closest("tr")[0];
//                chkHeader.removeAttr("checked");
//            } else {

//                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                    chkHeader.attr("checked", "checked");
//                }
//            }
        //        });

//        $("[id*=chkHeder2]").live("click", function() {
//            var chkHeader = $(this);
//            var grid = $(this).closest("table");
//            $("input[type=checkbox]", grid).each(function() {
//                if (chkHeader.is(":checked")) {
//                    debugger;
//                    if ($(this).is(":visible")) {
//                        $(this).attr("checked", "checked");
//                    }

//                } else {
//                    $(this).removeAttr("checked");
//                }
//            });
//        });

//        $("[id*=chkbox2]").live("click", function() {
//            var grid = $(this).closest("table");
//            var chkHeader = $("[id*=chkHeader]", grid);
//            var row = $(this).closest("tr")[0];

//            debugger;
//            if (!$(this).is(":checked")) {
//                var row = $(this).closest("tr")[0];
//                chkHeader.removeAttr("checked");
//            } else {

//                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                    chkHeader.attr("checked", "checked");
//                }
//            }
//        });

        $("body").on("click", "[id*=chkHeder1]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkbox1]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeder1]", grid);
            debugger;
            if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        $("body").on("click", "[id*=chkHeder2]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkbox2]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkbox2]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeder2]", grid);
            debugger;
            if ($("[id*=chkbox2]", grid).length == $("[id*=chkbox2]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtFrmSearch').val().length > 0) {
                $('#<%= dgvFrmEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvFrmEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvFrmEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtFrmSearch').val() + '\')').parent().show();
            }
            else if ($('#txtFrmSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvFrmEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtFrmSearch').val('');
            $('#<%= dgvFrmEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtFrmSearch').focus();
        }


        function ToSearching() {
            if ($('#txtToSearch').val().length > 0) {
                $('#<%= dgvToEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvToEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvToEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtToSearch').val() + '\')').parent().show();
            }
            else if ($('#txtToSearch').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= dgvToEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtToSearch').val('');
            $('#<%= dgvToEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtToSearch').focus();
        }
        
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                         <asp:Label ID="lblPageHeader" runat="server" Text="Transfer Leave Approver"></asp:Label>
                    </h2>
                </div>
                 <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                     <asp:Label ID="lblCaption" runat="server" Text="Approver Information"></asp:Label>
                                </h2>
                            </div>
                          <div class="body">
                                 <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                              <asp:CheckBox ID="chkShowInactiveApprovers" runat="server" Text="Show Inactive Approver(s)"  AutoPostBack="true" />
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <asp:CheckBox ID="chkShowInActiveEmployees" runat="server" Text="Show Inactive Employee(s)" AutoPostBack="true" />
                                    </div>
                               </div>
                                 <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblFromApprover" runat="server" Text="From Approver"  CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                        <asp:DropDownList ID="cboFrmApprover" runat="server" AutoPostBack="True" />
                                                </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblToApprover" runat="server" Text="To Approver" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                         <asp:DropDownList ID="cboToApprover" runat="server" AutoPostBack="True" />
                                                </div>
                                        </div>
                                </div>
                                 <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblFrmLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                        <asp:DropDownList ID="cboFrmLevel" runat="server" AutoPostBack="True" />
                                                </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblToLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                        <asp:DropDownList ID="cboToLevel" runat="server" AutoPostBack="True" />
                                                </div>
                                        </div>
                                </div>
                                 <div class="row clearfix">
                                     <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                         <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 p-l-0">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                            <input type="text" id="txtFrmSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                        maxlength="50"  onkeyup="FromSearching();" class = "form-control" />
                                                      </div>
                                                 </div>   
                                          </div>      
                                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15 p-l-0">
                                                <ul class="header-dropdown">
                                                        <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocation">
                                                         <i class="fas fa-sliders-h"></i>
                                                        </asp:LinkButton>
                                                </ul>
                                          </div>
                                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15 p-l-0">
                                              <ul class="header-dropdown">
                                                     <asp:LinkButton ID="lnkReset" runat="server"  ToolTip="Reset">
                                                        <i class="fas fa-sync-alt"></i>
                                                     </asp:LinkButton>
                                              </ul>
                                          </div>
                                     </div>
                                      <div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
                                      </div>
                                 </div>
                                 <div class="row clearfix">
                                           <div class="col-xs-5 col-md-5 col-sm-5 col-xs-5">
                                                <div class="table-responsive" style="height: 400px">
                                                        <asp:GridView ID="dgvFrmEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                            ShowFooter="False" AllowPaging="false" DataKeyNames="approverunkid,leaveapprovertranunkid,leaveapproverunkid,employeeunkid,employeecode,employeename">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkHeder1" runat="server"  CssClass="filled-in" Text=" " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkbox1" runat="server"  CssClass="filled-in" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgEmployeecode" />
                                                                <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgEmployee" />
                                                            </Columns>
                                                        </asp:GridView>
                                                </div>
                                           </div>
                                           <div class="col-xs-2 col-md-2 col-sm-2 col-xs-2 m-t-100">
                                                 <div>
                                                    <asp:Button ID="objbtnAssign" runat="server" Text=">>"  CssClass="btn btn-primary" />
                                                </div>
                                                <div style="margin-top:10px">
                                                    <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btn btn-primary" />
                                                </div>
                                          </div>
                                           <div class="col-xs-5 col-md-5 col-sm-5 col-xs-5">
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                        <li role="presentation" class="active"><a href="#tbpMigrationEmp" data-toggle="tab">
                                                                    <asp:Label ID="lblCaption1" runat="server" Text="Migrated Employee" CssClass="form-label"></asp:Label>
                                                        </a></li>
                                                        <li role="presentation"><a href="#tbpAssignedEmp" data-toggle="tab">
                                                                   <asp:Label ID="lblCaption2" runat="server" Text="Assigned Employee"  CssClass="form-label"></asp:Label>
                                                       </a></li>
                                                </ul>
                                                    <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="tbpMigrationEmp">
                                                         <div class="form-group">
                                                                <div class="form-line">
                                                                      <input type="text" id="txtToSearch" name="txtSearch" placeholder="Type To Search Text" maxlength="50" onkeyup="ToSearching();"  class = "form-control" />
                                                                </div>
                                                          </div>
                                                           <div class="table-responsive" style="height: 350px">
                                                                <asp:GridView ID="dgvToEmp" runat="server" AutoGenerateColumns="False" 
                                                                    ShowFooter="False" AllowPaging="false"  CssClass="table table-hover table-bordered"
                                                                    DataKeyNames="approverunkid,leaveapprovertranunkid,leaveapproverunkid,employeeunkid,employeecode,employeename">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="25">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkHeder2" runat="server" CssClass="filled-in" Text=" " />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkbox2" runat="server" CssClass="filled-in" Text=" " />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgMigratedEmpCode" />
                                                                        <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgMigratedEmp" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                           </div>
                                                    </div>
                                                    
                                                    <div role="tabpanel" class="tab-pane fade" id="tbpAssignedEmp">
                                                            <div class="table-responsive" style="height: 350px">
                                                                       <asp:GridView ID="dgvAssignedEmp" runat="server" AutoGenerateColumns="False"  CssClass="table table-hover table-bordered"
                                                                            ShowFooter="False" AllowPaging="false" >
                                                                            <Columns>
                                                                                <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgAssignEmpCode" />
                                                                                <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgAssignEmployee" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                            </div>
                                                      </div>
                                                </div>    
                                          </div>  
                                 </div>               
                         </div>
                         <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                         </div>
                      </div>
                    </div>
                 </div>               
            
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                       
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                   
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                        </td>
                                        <td style="width: 33%;">
                                          
                                        </td>
                                        <td style="width: 4%;">
                                        </td>
                                        <td style="width: 15;">
                                        </td>
                                        <td style="width: 33%">
                                           
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            
                                        </td>
                                        <td style="width: 33%;">
                                            
                                        </td>
                                        <td style="width: 4%;" rowspan="4">
                                            <table style="width: 100%;">
                                               
                                            </table>
                                        </td>
                                        <td style="width: 15;">
                                            
                                        </td>
                                        <td style="width: 33%">
                                           
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            
                                        </td>
                                        <td style="width: 35%;">
                                            
                                        </td>
                                        <td style="width: 15%;">
                                            
                                        </td>
                                        <td style="width: 35%">
                                            
                                        </td>
                                    </tr>
                                    <tr style="width: 100%; padding-right: 2%">
                                        <td style="width: 48%" colspan="2">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 75%">
                                                     
                                                    </td>
                                                    <td style="width: 15%; text-align: right; font-weight: bold">
                                                       
                                                    </td>
                                                    <td style="width: 10%; text-align: right; font-weight: bold">
                                                       
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 48%;" colspan="2">
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 48%" colspan="2">
                                            <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                height: 350px; overflow: auto; border: solid 1px #DDD">
                                                
                                            </div>
                                        </td>
                                        <td style="width: 48%" colspan="2">
                                            <table style="width: 100%; vertical-align: top">
                                                <tr style="width: 100%">
                                                    <td style="width: 100%">
                                                        <asp:MultiView ActiveViewIndex="0" ID="mltiview" runat="server">
                                                            <asp:View ID="vwStep1" runat="server">
                                                                <table width="100%">
                                                                    <tr style="width: 100%;">
                                                                        <td style="width: 100%; border-radius: 0px" class="grpheader">
                                                                            <h4>
                                                                                
                                                                            </h4>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%;">
                                                                        <td style="width: 100%;">
                                                                            <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                                                                height: 350px; overflow: auto; border: solid 1px #DDD">
                                                                                
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                            <asp:View ID="vwStep2" runat="server">
                                                                <table id="tblStp2" runat="server" width="100%">
                                                                    <tr style="width: 100%; background: steelblue;">
                                                                        <td style="width: 100%;" class="grpheader">
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%;">
                                                                        <td style="width: 100%;">
                                                                            <asp:Panel ID="pnl_dgvAssignedEmp" runat="server" ScrollBars="Auto" Height="350px">
                                                                             
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%; text-align: right">
                                                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnDefault" />
                                                        <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnDefault" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
