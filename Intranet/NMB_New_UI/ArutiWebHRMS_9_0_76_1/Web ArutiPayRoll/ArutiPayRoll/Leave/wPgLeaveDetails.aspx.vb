﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports ArutiReports

#End Region
Partial Class wPgLeaveDetails
    Inherits Basepage

#Region " Private Variable(s) "

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private ReadOnly mstrModuleName As String = "frmLeaveDetails"
    'Pinkal (26-Feb-2019) -- End
    Private msg As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private clsuser As New User
    'Private objEmployee As New clsEmployee_Master
    'Pinkal (11-Sep-2020) -- End

    'Pinkal (03-Feb-2023) -- Start
    '(A1X-593) New report - Claim Approvers Report.
    Private xEmployeeId As Integer = 0
    'Pinkal (03-Feb-2023) -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub SetInfo()
        Dim dsCombos As New DataSet
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objEmployee As New clsEmployee_Master
                dsCombos = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Employee", True)


                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = 0
                End With


                If xEmployeeId > 0 Then
                    cboEmployee.SelectedValue = xEmployeeId.ToString()
                    cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
                End If

                objEmployee = Nothing

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
                cboEmployee_SelectedIndexChanged(Nothing, Nothing)
                objglobalassess = Nothing
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

#End Region

#Region " Controls Event(s) "

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

    'Protected Sub GvLeaveDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvLeaveDetails.PageIndexChanging
    '    Try
    '        GvLeaveDetails.PageIndex = e.NewPageIndex
    '        GvLeaveDetails.DataSource = CType(Me.ViewState("EmpBalance"), DataTable)
    '        GvLeaveDetails.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Pinkal (11-Sep-2020) -- End

    Protected Sub GvLeaveDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvLeaveDetails.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub



            'Pinkal (03-Feb-2023) -- Start
            '(A1X-593) New report - Claim Approvers Report.

            'If e.Row.Cells(2).Text.Trim <> "" AndAlso e.Row.Cells(2).Text <> "&nbsp;" Then
            '    e.Row.Cells(2).Text = eZeeDate.convertDate(e.Row.Cells(2).Text).ToShortDateString
            'End If

            'If e.Row.Cells(3).Text.Trim <> "" AndAlso e.Row.Cells(3).Text <> "&nbsp;" Then
            '    e.Row.Cells(3).Text = eZeeDate.convertDate(e.Row.Cells(3).Text).Date
            'End If

            If e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvStartDate", False, True)).Text.Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvStartDate", False, True)).Text <> "&nbsp;" Then
                e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvStartDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvStartDate", False, True)).Text).ToShortDateString
            End If

            If e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvEndDate", False, True)).Text.Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvEndDate", False, True)).Text <> "&nbsp;" Then
                e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvEndDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvEndDate", False, True)).Text).ToShortDateString()
            End If

            If GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhApplyLeave", False, True)).Visible Then
                Dim lnkApplyLeave As LinkButton = e.Row.FindControl("lnkApplyLeave")
                If lnkApplyLeave IsNot Nothing Then
                    lnkApplyLeave.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhApplyLeave", False, True)).FooterText, Me.GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhApplyLeave", False, True)).HeaderText)
                End If
                lnkApplyLeave = Nothing
            End If

            If GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhApplicationStatus", False, True)).Visible Then
                Dim lnkApplicationStatus As LinkButton = e.Row.FindControl("lnkApplicationStatus")
                If lnkApplicationStatus IsNot Nothing Then
                    lnkApplicationStatus.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhApplicationStatus", False, True)).FooterText, Me.GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhApplicationStatus", False, True)).HeaderText)
            End If
                lnkApplicationStatus = Nothing
            End If

            'Pinkal (03-Feb-2023) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If
        If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            Exit Sub
        End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = lblPageHeader.Text
            'StrModuleName2 = "HR"
            'Pinkal (11-Sep-2020) -- End

            'Pinkal (03-Feb-2023) -- Start
            '(A1X-593) New report - Claim Approvers Report.
            If Request.QueryString.Count > 0 Then
                xEmployeeId = CInt(clsCrypto.Dicrypt(HttpUtility.HtmlDecode(Request.QueryString("Id"))))
            End If
            'Pinkal (03-Feb-2023) -- End

        If (Page.IsPostBack = False) Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'clsuser = CType(Session("clsuser"), User)
                'If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)
                'End If
                'Pinkal (11-Sep-2020) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Call SetControlCaptions()
                'Call Language._Object.SaveValue()
            SetLanguage()
            'Pinkal (26-Feb-2019) -- End

                SetDateFormat()
            Call SetInfo()

                
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
                End If


                'Pinkal (27-Sep-2024) -- Start
                'NMB Enhancement : (A1X-2787) NMB - Credit report development.
                If Session("CompanyGroupName").ToString().ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                    GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvLeaveBF", False, True)).Visible = False
                    GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvAsonAccrueDays", False, True)).Visible = False
                    GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvIssueDays", False, True)).Visible = False
                    GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvLeaveAdjustment", False, True)).Visible = False
                    GvLeaveDetails.Columns(getColumnID_Griview(GvLeaveDetails, "dgcolhLvDetailLvBalAsonDate", False, True)).Visible = False
                End If
                'Pinkal (27-Sep-2024) -- End

        End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Events "

    'Pinkal (03-Feb-2023) -- Start
    '(A1X-593) New report - Claim Approvers Report.

    Protected Sub lnkApplyLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            Dim mintLeaveTypeId As Integer = CInt(GvLeaveDetails.DataKeys(row.RowIndex).Value)

            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeaveFormAddEdit.aspx?Id=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(cboEmployee.SelectedValue.ToString() & "|" & mintLeaveTypeId.ToString())), False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkApplicationStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            Dim mintLeaveTypeId As Integer = CInt(GvLeaveDetails.DataKeys(row.RowIndex).Value)

            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeaveFormList.aspx?Id=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(cboEmployee.SelectedValue.ToString() & "|" & mintLeaveTypeId.ToString())), False)

            'Dim objLeaveType As New clsleavetype_master
            'Dim mstrLeaveTypIds As String = objLeaveType.GetDeductdToLeaveTypeIDs(mintLeaveTypeId)

            'If mstrLeaveTypIds.Trim.Length > 0 Then
            '    mstrLeaveTypIds &= "," & mintLeaveTypeId.ToString()
            'Else
            '    mstrLeaveTypIds = mintLeaveTypeId.ToString()
            'End If


            'Dim objLvform As New clsleaveform
            'Dim dtLeaveForm As DataTable = objLvform.GetAppliedLastLeaveForm(CInt(cboEmployee.SelectedValue), mstrLeaveTypIds)
            'objLvform = Nothing

            'If dtLeaveForm IsNot Nothing AndAlso dtLeaveForm.Rows.Count > 0 Then

            '    Dim mintLeaveFormId As Integer = CInt(dtLeaveForm.Rows(0)("formunkid"))

            '    objLeaveType._Leavetypeunkid = CInt(dtLeaveForm.Rows(0)("leavetypeunkid"))

            '    Dim objLeaveform As clsEmployeeLeaveForm
            '    objLeaveform = New clsEmployeeLeaveForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            '    objLeaveform.SetDefaultValue()
            '    objLeaveform._LeaveFormId = mintLeaveFormId
            '    objLeaveform._EmployeeId = CInt(cboEmployee.SelectedValue)
            '    objLeaveform._EmployeeName = cboEmployee.SelectedItem.Text
            '    objLeaveform._LeaveTypeId = objLeaveType._Leavetypeunkid
            '    objLeaveform._LeaveTypeName = objLeaveType._Leavename
            '    objLeaveform._YearId = CInt(Session("Fin_year"))
            '    objLeaveform._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            '    objLeaveform._Fin_StartDate = CDate(Session("fin_startdate")).Date
            '    objLeaveform._Fin_Enddate = CDate(Session("fin_enddate")).Date
            '    objLeaveform._IncludeDependents = False
            '    objLeaveform._DoNotShowExpesneIfNoExpense = True
            '    GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            '    objLeaveform._CompanyUnkId = CInt(Session("CompanyUnkId"))
            '    objLeaveform._UserUnkId = CInt(Session("UserId"))
            '    objLeaveform._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            '    objLeaveform._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
            '    objLeaveform._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            '    If CInt(Session("Employeeunkid")) > 0 Then
            '        objLeaveform._UserName = Session("DisplayName").ToString()
            '    End If

            '    objLeaveform.generateReportNew(CStr(Session("Database_Name")), _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    CStr(Session("UserAccessModeSetting")), True, _
            '                                    CStr(Session("ExportReportPath")), _
            '                                    CBool(Session("OpenAfterExport")), _
            '                                    0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))

            '    Session("objRpt") = objLeaveform._Rpt

            '    If Session("objRpt") IsNot Nothing Then
            '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            '    End If

            '    objLeaveform = Nothing
            'Else
            '    msg.DisplayMessage("No application(s) are not applied from this employee.Please check for other leave type.", Me)
            '    Exit Sub
            'End If

            'objLeaveType = Nothing

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (03-Feb-2023) -- End

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    'Pinkal (03-Feb-2023) -- Start
    '(A1X-593) New report - Claim Approvers Report.
    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try

                Dim clsuser As New User
                clsuser.GetLeaveBalance(cboEmployee.SelectedValue, Session("mdbname"), CInt(Session("Fin_year")))
            Dim dtTable As DataTable = clsuser.LeaveBalances.Copy

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                Dim drRow = dtTable.NewRow
                drRow("leavetypeunkid") = 0
                drRow("Leave") = ""
                drRow("StDate") = eZeeDate.convertDate(Now).ToString()
                drRow("EdDate") = eZeeDate.convertDate(Now).ToString()
                drRow("actualamount") = 0
                drRow("Acc_Amt") = 0
                drRow("Iss_Amt") = 0
                drRow("Balance") = 0
                drRow("LeaveAmountBF") = 0
                drRow("isshortleave") = 0
                drRow("AdjustmentLeave") = 0
                drRow("balanceasondate") = 0
                dtTable.Rows.Add(drRow)

                GvLeaveDetails.DataSource = dtTable
                GvLeaveDetails.DataBind()

                GvLeaveDetails.Rows(0).Visible = False

            Else
                GvLeaveDetails.DataSource = dtTable
                GvLeaveDetails.DataBind()
            End If

            clsuser = Nothing
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Pinkal (03-Feb-2023) -- End

#End Region


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

#Region " Language & UI Settings "

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLeaveInfo.ID, Me.lblLeaveInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(0).FooterText, Me.GvLeaveDetails.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(1).FooterText, Me.GvLeaveDetails.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(2).FooterText, Me.GvLeaveDetails.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(3).FooterText, Me.GvLeaveDetails.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(4).FooterText, Me.GvLeaveDetails.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(5).FooterText, Me.GvLeaveDetails.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(6).FooterText, Me.GvLeaveDetails.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(7).FooterText, Me.GvLeaveDetails.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(8).FooterText, Me.GvLeaveDetails.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(9).FooterText, Me.GvLeaveDetails.Columns(9).HeaderText)

            'Pinkal (03-Feb-2023) -- Start
            '(A1X-593) New report - Claim Approvers Report.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(10).FooterText, Me.GvLeaveDetails.Columns(10).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLeaveDetails.Columns(11).FooterText, Me.GvLeaveDetails.Columns(11).HeaderText)
            'Pinkal (03-Feb-2023) -- End


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (07-Mar-2019) -- End

            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblLeaveInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLeaveInfo.ID, Me.lblLeaveInfo.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.GvLeaveDetails.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(0).FooterText, Me.GvLeaveDetails.Columns(0).HeaderText)
            Me.GvLeaveDetails.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(1).FooterText, Me.GvLeaveDetails.Columns(1).HeaderText)
            Me.GvLeaveDetails.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(2).FooterText, Me.GvLeaveDetails.Columns(2).HeaderText)
            Me.GvLeaveDetails.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(3).FooterText, Me.GvLeaveDetails.Columns(3).HeaderText)
            Me.GvLeaveDetails.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(4).FooterText, Me.GvLeaveDetails.Columns(4).HeaderText)
            Me.GvLeaveDetails.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(5).FooterText, Me.GvLeaveDetails.Columns(5).HeaderText)
            Me.GvLeaveDetails.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(6).FooterText, Me.GvLeaveDetails.Columns(6).HeaderText)
            Me.GvLeaveDetails.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(7).FooterText, Me.GvLeaveDetails.Columns(7).HeaderText)
            Me.GvLeaveDetails.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(8).FooterText, Me.GvLeaveDetails.Columns(8).HeaderText)
            Me.GvLeaveDetails.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(9).FooterText, Me.GvLeaveDetails.Columns(9).HeaderText)

            'Pinkal (03-Feb-2023) -- Start
            '(A1X-593) New report - Claim Approvers Report.
            Me.GvLeaveDetails.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(10).FooterText, Me.GvLeaveDetails.Columns(10).HeaderText)
            Me.GvLeaveDetails.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveDetails.Columns(11).FooterText, Me.GvLeaveDetails.Columns(11).HeaderText)
            'Pinkal (03-Feb-2023) -- End

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (26-Feb-2019) -- End

End Class
