<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"  CodeFile="LeaveIssue.aspx.vb" Inherits="LeaveIssue" Title="Leave Issue" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="YesNobutton" TagPrefix="uc3" %>
<%@ Register src="~/Controls/ConfirmYesNo.ascx" tagname="Confirmation" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:Panel ID="MainPan" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                  <div class="block-header">
                        <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Issue"></asp:Label>
                        </h2>
                  </div>
                  <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                      <div class="header">
                                        <h2>
                                             <asp:Label ID="gbLeaveIssue" runat="server" Text="Leave Issue Information"></asp:Label>
                                        </h2>
                                    </div>
                                    <div class="body">
                                            <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                          <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                         <asp:Label ID="lblFromYear" runat="server" Text="From Year" Visible="false"  CssClass="form-label"></asp:Label>
                                                                         <div class="form-group">
                                                                                 <asp:DropDownList ID="cboYear" runat="server" Visible="false" />
                                                                         </div>
                                                                </div>
                                                        </div>
                                                          <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                          <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                          <div class="form-group">
                                                                                 <div class="form-line">
                                                                                         <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                                 </div>
                                                                          </div>       
                                                                </div>
                                                        </div>
                                                          <div class="row clearfix">
                                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                                         <asp:Label ID="lblFormNo" runat="server" Text="Form No"  CssClass="form-label"></asp:Label>
                                                                          <div class="form-group">
                                                                                <div class="form-line">
                                                                                        <asp:TextBox ID="txtFormNo" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                          </div>      
                                                                </div>
                                                                 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 m-t-35">
                                                                         <asp:Label ID="lblFDateVal" runat="server" Text=""  CssClass="form-label"></asp:Label>
                                                                </div>   
                                                         </div>
                                                          <div class="row clearfix">
                                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                                         <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"  CssClass="form-label"></asp:Label>
                                                                         <div class="form-group">
                                                                                <div class="form-line">
                                                                                         <asp:TextBox ID="txtLeaveType" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                         </div>       
                                                                </div>
                                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 m-t-20">
                                                                    <div class="form-group">
                                                                          <div class="form-line">
                                                                                <asp:TextBox ID="txtColor" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                          </div>
                                                                    </div>            
                                                                </div>
                                                          </div> 
                                                          <div class="row clearfix">
                                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">     
                                                                          <asp:Label ID="lblLeaveEndDate" runat="server" Text="As On Date"  CssClass="form-label"></asp:Label>
                                                                           <div class="form-group">
                                                                                  <div class="form-line">
                                                                                         <asp:TextBox ID="txtAsOnDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                                  </div>
                                                                           </div>       
                                                                </div>
                                                                 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 m-t-35">
                                                                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 p-l-0">
                                                                            <asp:Label ID="lblTotalLeaveDays" runat="server" Text="Total Days : " CssClass="form-label"></asp:Label>
                                                                         </div>
                                                                         <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 p-l-0">    
                                                                            <asp:Label  ID="lblDays" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                                        </div>    
                                                                 </div>
                                                           </div>     
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                           <ul class="list-group">
                                                                <li class="list-group-item active">
                                                                          <asp:Label ID="lblDescription" runat="server" Text="Description" Font-Bold="true" ></asp:Label>
                                                                          <asp:Label ID="lblAmount" runat="server" Text="Amount" Font-Bold="true" CssClass="badge"></asp:Label>
                                                                </li>
                                                                <li class="list-group-item">
                                                                        <asp:Label ID="lblLastYearAccrued" runat="server" Text="Total Accrued Upto Last Year" Font-Bold="true" ></asp:Label>
                                                                        <asp:Label ID="lblCaption1Value" runat="server" Text="" CssClass="badge"></asp:Label>
                                                                </li>
                                                                <li class="list-group-item">
                                                                         <asp:Label ID="LblLeaveBF" runat="server" Text="Leave BF" Font-Bold="True"></asp:Label>
                                                                          <asp:Label ID="objlblLeaveBFvalue" runat="server" Text=""  CssClass="badge"></asp:Label>
                                                                </li>
                                                                <li class="list-group-item">
                                                                        <asp:Label ID="lblToDateAccrued" runat="server" Text="Total Accrued To Date" Font-Bold="True"></asp:Label>
                                                                        <asp:Label ID="lblCaption2Value" runat="server" Text="" CssClass="badge"></asp:Label>
                                                                </li>
                                                                <li class="list-group-item">
                                                                       <asp:Label ID="lblLastYearIssued" runat="server" Text="Total Issued Upto Last Year" Font-Bold="True"></asp:Label>
                                                                       <asp:Label ID="lblCaption3Value" runat="server" Text="" CssClass="badge"></asp:Label>
                                                                </li>
                                                                <li class="list-group-item">
                                                                        <asp:Label ID="lblTotalIssuedToDate" runat="server" Text="Total Issued Upto Date" Font-Bold="True"></asp:Label>
                                                                         <asp:Label ID="lblCaption4Value" runat="server" Text="" CssClass="badge"></asp:Label>
                                                                </li>
                                                                 <li class="list-group-item">
                                                                         <asp:Label ID="LblTotalAdjustment" runat="server" Text="Total Adjustment" Font-Bold="True"></asp:Label>
                                                                         <asp:Label ID="objlblTotalAdjustment" runat="server" Text=""  CssClass="badge"></asp:Label>
                                                                 </li>
                                                                 <li class="list-group-item">
                                                                         <asp:Label ID="lblBalance" runat="server" Text="Balance" Font-Bold="True"></asp:Label>
                                                                         <asp:Label ID="lblCaption5Value" runat="server" Text="" CssClass="badge"></asp:Label>
                                                                 </li>
                                                           </ul> 
                                                    </div>
                                            </div>
                                            <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    
                                                    </div>
                                            </div>
                                            <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <div class="table-responsive" style="height: 200px">
                                                                     <asp:UpdatePanel ID="Upanel" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                                <asp:DataGrid ID="dgViewLeave" runat="server" Width="200%" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" CssClass="table table-hover table-bordered">
                                                                                    <ItemStyle Wrap="True" Width="50px" />
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="CalendarTableID" HeaderText="CalendarTableID"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="MonthYear" HeaderText="Month" FooterText=""></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="startday" HeaderText="startday"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="MonthDays" HeaderText="MonthDays"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="MonthId" HeaderText="MonthId"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="YearId" HeaderText="YearId"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sun_1" HeaderText="Sun"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Mon_1" HeaderText="Mon"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tue_1" HeaderText="Tue"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Wed_1" HeaderText="Wed"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Thu_1" HeaderText="Thu"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fri_1" HeaderText="Fri"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sat_1" HeaderText="Sat"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sun_2" HeaderText="Sun"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Mon_2" HeaderText="Mon"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tue_2" HeaderText="Tue"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Wed_2" HeaderText="Wed"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Thu_2" HeaderText="Thu"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fri_2" HeaderText="Fri"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sat_2" HeaderText="Sat"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sun_3" HeaderText="Sun"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Mon_3" HeaderText="Mon"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tue_3" HeaderText="Tue"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Wed_3" HeaderText="Wed"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Thu_3" HeaderText="Thu"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fri_3" HeaderText="Fri"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sat_3" HeaderText="Sat"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sun_4" HeaderText="Sun"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Mon_4" HeaderText="Mon"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tue_4" HeaderText="Tue"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Wed_4" HeaderText="Wed"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Thu_4" HeaderText="Thu"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fri_4" HeaderText="Fri"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sat_4" HeaderText="Sat"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sun_5" HeaderText="Sun"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Mon_5" HeaderText="Mon"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tue_5" HeaderText="Tue"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Wed_5" HeaderText="Wed"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Thu_5" HeaderText="Thu"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fri_5" HeaderText="Fri"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sat_5" HeaderText="Sat"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sun_6" HeaderText="Sun"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Mon_6" HeaderText="Mon"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tue_6" HeaderText="Tue"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Wed_6" HeaderText="Wed"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Thu_6" HeaderText="Thu"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fri_6" HeaderText="Fri"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Sat_6" HeaderText="Sat"></asp:BoundColumn>
                                                                                        <asp:ButtonColumn CommandName="ColumnClick" Visible="false" />
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="dgViewLeave" EventName="ItemCommand" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                             </div>
                                                    </div>
                                            </div>
                                    </div>   
                                    <div class="footer">
                                         <asp:Button ID="btnSave" runat="server" Text="Save" CssClass=" btn btn-primary"  />
                                         <asp:Button ID="btnClose" runat="server" Text="Close" CssClass=" btn btn-default" />
                                    </div>     
                            </div>
                        </div>
                  </div>          
                    <uc3:YesNobutton ID="radYesNo" runat="server" Title="Leave Issue" Message="Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?" />
                    <uc4:confirmation ID ="LeaveIssueConfirmation" runat="server" Title ="Confirmation"  />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
