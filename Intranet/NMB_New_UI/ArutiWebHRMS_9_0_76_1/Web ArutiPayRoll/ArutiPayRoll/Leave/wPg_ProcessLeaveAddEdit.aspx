﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ProcessLeaveAddEdit.aspx.vb"
    Inherits="Leave_wPg_ProcessLeaveAddEdit" Title="Leave Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());

            }
        }
    </script>

    <script type="text/javascript">
        function IsValidAttach() {
            var cboEmployee = $('#<%= cboApprovalExpEmployee.ClientID %>');

            if (parseInt($('select.cboScanDcoumentType')[0].value) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }

            if (parseInt(cboApprovalExpEmployee.val()) <= 0) {
                alert('Employee Name is compulsory information. Please select Employeee to continue.');
                cboApprovalExpEmployee.focus();
                return false;
            }
        }    
                      
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_YesNo.ClientID %>_Panel1").css("z-index", "100005");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approval"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body" style="height: 910px">
                                                <div class="row clearfix">
                                                    <asp:Panel ID="pnlLeaveBalance" runat="server">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <ul class="list-group">
                                                            <li class="list-group-item active">
                                                                <asp:Label ID="lblDescription" runat="server" Text="Description" Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblLastYearAccrued" runat="server" Text="Total Accrued Upto Last Year:"
                                                                    Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblLastYearAccruedValue" runat="server" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="LblLeaveBF" runat="server" Text="Leave BF" Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblLeaveBFvalue" runat="server" Text="0.00" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblToDateAccrued" runat="server" Text="Total Accrued To Date:" Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblAccruedToDateValue" runat="server" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblLastYearIssued" runat="server" Text="Total Issued Upto Last Year:"
                                                                    Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblLastYearIssuedValue" runat="server" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblTotalIssuedToDate" runat="server" Text="Total Issued Upto Date:"
                                                                    Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblIssuedToDateValue" runat="server" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="LblTotalAdjustment" runat="server" Text="Total Adjustment" Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblTotalAdjustment" runat="server" Text="0.00" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="LblBalanceAsonDate" runat="server" Text="Balance As on Date" Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblBalanceAsonDateValue" runat="server" align="left" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblBalance" runat="server" Text="Total Balance" Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objlblBalanceValue" runat="server" align="left" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="LblAsonDateAccrue" runat="server" Text="Leave Accrue As on End Date"
                                                                    Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objAsonDateAccrue" runat="server" align="left" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <asp:Label ID="lblAsonDateBalance" runat="server" Text="Leave Balance As on End Date"
                                                                    Font-Bold="True"></asp:Label>
                                                                <asp:Label ID="objAsonDateBalance" runat="server" align="left" Text="0" CssClass="badge"></asp:Label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </asp:Panel>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemark" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemark" runat="server" Rows="2" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:LinkButton ID="lnkShowLeaveForm" runat="server" Text="Leave Form" Font-Underline="false" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:LinkButton ID="lnkShowLeaveExpense" runat="server" Text="Leave Expense" Font-Underline="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:LinkButton ID="lnkLeaveDayCount" runat="server" Text="Leave Day Fraction" Font-Underline="false" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:LinkButton ID="lnkShowScanDocuementList" runat="server" Text="Show Scan Documents"
                                                            Font-Underline="false"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFormNo" runat="server" Text="Application No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFormNo" runat="server" ReadOnly="true" Enabled="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLeaveType" runat="server" ReadOnly="true" Enabled="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true" Enabled="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true" Enabled="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblReportTo" runat="server" Text="Report To" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtReportTo" runat="server" ReadOnly="true" Enabled="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <asp:Label ID="LblReliever" runat="server" CssClass="mandatory_field form-label"
                                                            Text="Reliever"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboReliever" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-35">
                                                        <asp:CheckBox ID="chkIncludeAllStaff" runat="server" Text="Include All Staff" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApprover" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStartdate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpStartdate" AutoPostBack="True" Enabled="True" runat="server"
                                                            ReadonlyDate="False" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                        <asp:Label ID="lblEnddate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpEnddate" runat="server" AutoPostBack="True" Enabled="True" ReadonlyDate="False" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-35">
                                                        <asp:Label ID="objNoofDays" runat="server" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprovalDate" runat="server" Text="Approval Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpApplyDate" AutoPostBack="false" Enabled="True" runat="server"
                                                            ReadonlyDate="True" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAllowance" runat="server" Text="Allowance" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboAllowance" runat="server" AutoPostBack="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtAllowance" runat="server" Enabled="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApprove" runat="server" CssClass="btn btn-primary" Text="Approve" />
                                <asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary" Text="Reject" />
                                <asp:Button ID="btnReschedule" runat="server" CssClass="btn btn-primary" Text="Re-schedule" />
                                <asp:Button ID="Btnclose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                    <uc7:Confirmation ID="LeaveApprovalConfirmation" runat="server" Title="Confirmation"
                        Message="" />
                    <uc7:Confirmation ID="Confirmation" runat="server" Title="Confirmation" Message="Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?" />
                    <uc7:Confirmation ID="SLExceedingConfirmation" Title="Confirmation" runat="server" />
                </div>
                <cc1:ModalPopupExtender ID="popupClaimRequest" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ClaimRequest" PopupControlID="pnl_ClaimRequest" CancelControlID="hdf_ClaimRequest" />
                <asp:Panel ID="pnl_ClaimRequest" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Claim & Request"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <asp:LinkButton ID="lnkClaimDepedents" runat="server" ToolTip="View Depedents List">
                                                  <i class="fas fa-restroom"></i>
                            </asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: 480px;">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" />
                                                                </div>
                                                                <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblEmpAddEdit" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboClaimEmployee" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLeaveTypeAddEdit" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" Enabled="False" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblValue" runat="server" Text="Leave Form" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="false" Enabled="False" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                            ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtUoMType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCalimQty" runat="server" CssClass="form-control" Text="0" AutoPostBack="true"
                                                                            Style="text-align: right"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtUnitPrice" runat="server" Text="1.00" Style="text-align: right"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                        <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                                            Enabled="false" Visible="false" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                <asp:Panel ID="pnlClaimBalAsonDate" runat="server">
                                                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 m-l--15">
                                                                        <asp:Label ID="lblClaimBalanceasondate" runat="server" Text="Balance As on Date"
                                                                            CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtClaimBalanceAsOnDate" runat="server" CssClass="form-control"
                                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 m-l--15">
                                                                        <asp:Label ID="lblBalanceAddEdit" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtClaimBalance" runat="server" CssClass="form-control" Enabled="false"
                                                                                    Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                <asp:Label ID="LblClaimCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboClaimCurrency" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                                    <li role="presentation" class="active"><a href="#tbpExpenseRemark" data-toggle="tab">
                                                                        <asp:Label ID="tbExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label" />
                                                                    </a></li>
                                                                    <li role="presentation"><a href="#tbpClaimRemark" data-toggle="tab">
                                                                        <asp:Label ID="tbClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label" />
                                                                    </a></li>
                                                                </ul>
                                                                <!-- Tab panes -->
                                                                <div class="tab-content">
                                                                    <div role="tabpanel" class="tab-pane fade in active" id="tbpExpenseRemark">
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div role="tabpanel" class="tab-pane fade" id="tbpClaimRemark">
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnClaimAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnClaimEdit" runat="server" Text="Edit" CssClass="btn btn-primary"
                                            Visible="false" />
                                        <asp:HiddenField ID="hdf_ClaimRequest" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 170px">
                                                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                        AllowPaging="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="brnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                                        CommandName="Preview">
                                                                                        <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                                FooterText="dgcolhExpense" />
                                                            <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" ReadOnly="true" FooterText="dgcolhSectorRoute" />
                                                            <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                                                             <asp:BoundColumn DataField="currency_sign" FooterText="dgcolhcurrency" HeaderText="Currency" />
                                                            <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                ItemStyle-CssClass="txttextalignright" />
                                                            <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                ItemStyle-CssClass="txttextalignright" />
                                                              <asp:BoundColumn DataField="base_amount" FooterText="dgcolhBasicAmount" HeaderStyle-HorizontalAlign="Right"
                                                                HeaderText="Base Amount" ItemStyle-HorizontalAlign="Right" />    
                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                ItemStyle-CssClass="txttextalignright" />
                                                            <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" ReadOnly="true"
                                                                Visible="true" FooterText="dgcolhExpenseRemark" />
                                                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhTranId" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false"
                                                                FooterText="objdgcolhGUID" />
                                                            <asp:BoundColumn DataField="crapprovaltranunkid" HeaderText="approvaltranunkid" ReadOnly="true"
                                                                Visible="false"  FooterText="objdgcolhapprovaltranunkid"  />
                                                        </Columns>
                                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                    </asp:DataGrid>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="form-control"
                                                            Style="text-align: right"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <asp:HiddenField ID="hdf_Claim" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnViewScanAttchment" runat="server" Visible="false" Text="View Scan/Attchment"
                            CssClass="btn btn-primary" />
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnClaimSave" runat="server" Text="Save" CssClass="btn btn-primary"
                                    ValidationGroup="Step" />
                                <asp:Button ID="btnClaimClose" runat="server" Text="Close" CssClass="btn btn-default"
                                    CausesValidation="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <uc7:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" />
                    <uc7:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Title="Confirmation" />
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hdf_claimDeletePopup" PopupControlID="pnlEmpDepedents" CancelControlID="btnEmppnlEmpDepedentsClose">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px;">
                                    <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                            <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdf_claimDeletePopup" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupClaimDelete" runat="server" BackgroundCssClass="modal-backdrop  bd-l2"
                    TargetControlID="hdf_ClaimDelete" PopupControlID="pnl_popupClaimDelete" CancelControlID="btnDelReasonNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_popupClaimDelete" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblClaimDelete" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblClaimTitle" runat="server" Text="Are you Sure You Want To delete?:"
                                    CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtClaimDelReason" runat="server" TextMode="MultiLine" CssClass="form-control" />
                                    </div>
                                </div>
                                <asp:RequiredFieldValidator ID="rqfv_txtClaimDelReason" runat="server" Display="None"
                                    ControlToValidate="txtClaimDelReason" ErrorMessage="Delete Reason can not be blank. "
                                    CssClass="ErrorControl" ForeColor="White" Style="" SetFocusOnError="True" ValidationGroup="ClaimDelete"></asp:RequiredFieldValidator>
                                <cc1:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="rqfv_txtClaimDelReason" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" CssClass="btn btn-primary"
                            ValidationGroup="ClaimDelete" />
                        <asp:Button ID="btnDelReasonNo" runat="server" Text="No" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdf_ClaimDelete" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupLeaveForm" BackgroundCssClass="modal-backdrop" TargetControlID="txtLeaveFormNo"
                    runat="server" PopupControlID="pnlLeaveFormpopup" CancelControlID="btnLeaveFormOk">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlLeaveFormpopup" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblLeaveFormText" Text="Leave Application Form" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="LblLeaveFormNo" Text="Leave Application No " runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormNo" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormEmpCode" Text="Employee Code " runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormEmpCode" ReadOnly="true" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormEmployee" Text="Employee" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormEmployee" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormLeaveType" Text="Leave Type" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormLeaveType" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormApplydate" Text="Apply Date" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormApplydate" ReadOnly="true" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormStartdate" Text="Start Date" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormStartdate" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormReturndate" Text="End Date" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormReturndate" ReadOnly="true" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormDayApply" Text="Days To Apply" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormDayApply" ReadOnly="true" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveAddress" Text="Address and Telephone Number While On leave"
                                    runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormAddress" runat="server" ReadOnly="true" TextMode="MultiLine"
                                            Rows="2" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblLeaveFormRemark" Text="Remark" runat="server" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtLeaveFormRemark" runat="server" ReadOnly="true" TextMode="MultiLine"
                                            Rows="2" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnLeaveFormOk" runat="server" CssClass="btn btn-primary" Text="Close" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupFraction" BackgroundCssClass="modal-backdrop" TargetControlID="LblFraction"
                    runat="server" PopupControlID="pnlFractionPopup" CancelControlID="btnFractionCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlFractionPopup" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblFraction" Text="Leave Day Count" runat="server"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 375px;">
                                    <asp:GridView ID="dgFraction" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:CommandField ShowEditButton="true" ItemStyle-HorizontalAlign="Center" ControlStyle-Font-Underline="false"
                                                ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="leavedate" HeaderText="Date" ReadOnly="True" FooterText="dgColhDate" />
                                            <asp:BoundField DataField="dayfraction" HeaderText="Fractions" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhFraction" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnFractionOK" runat="server" CssClass="btn btn-primary" Text="Ok" />
                        <asp:Button ID="btnFractionCancel" runat="server" CssClass="btn btn-default" Text="Cancel" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupExpense" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ApprovalClaim" PopupControlID="pnlExpensePopup" CancelControlID="hdf_ApprovalClaim"
                    BehaviorID="btnApprovalSaveAddEdit">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlExpensePopup" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblApprovalpopupHeader" runat="server" Text="Claim &amp; Request"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <asp:LinkButton ID="lnkApprovalViewDependants" runat="server" ToolTip="View Depedents List">
                                                  <i class="fas fa-restroom"></i>
                            </asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: 480px;">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalExpCategory" runat="server" AutoPostBack="true" />
                                                                </div>
                                                                <asp:Label ID="lblApprovalPeriod" runat="server" Text="Period" Visible="false" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalPeriod" runat="server" AutoPostBack="true" Visible="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtApprovalClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lbApprovalClaimlDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpApprovalClaimDate" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblAppvoalExpEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalExpEmployee" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalExpLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalExpLeaveType" runat="server" AutoPostBack="true"
                                                                        Enabled="False" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objApprovallblValue" runat="server" Text="Leave Form" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalReference" runat="server" AutoPostBack="false" Enabled="False" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblApprovalDomicileAdd" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtApprovalDomicileAddress" runat="server" ReadOnly="true" Rows="3"
                                                                            TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalExpense" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalUoM" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtApprovalUoMType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalSector" runat="server" Text="Sector/Route" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalSectorRoute" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtApprovalQty" runat="server" AutoPostBack="true" CssClass="form-control"
                                                                            Style="text-align: right" Text="0"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalCostCenter" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApprovalUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="lblApprovalCosting" runat="server" Text="Costing" Visible="false"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtApprovalUnitPrice" runat="server" CssClass="form-control" Style="text-align: right"
                                                                            Text="1.00"></asp:TextBox>
                                                                        <asp:TextBox ID="txtApprovalCosting" runat="server" CssClass="form-control" Enabled="false"
                                                                            Text="0.00" Visible="false"></asp:TextBox>
                                                                        <asp:HiddenField ID="txtApprovalCostingTag" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Panel ID="pnlApprovalClaimBalAsonDate" runat="server">
                                                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 m-l--15">
                                                                        <asp:Label ID="lblApprovalExpBalAsonDate" runat="server" Text="Balance As on Date"
                                                                            CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtApprovalExpBalAsonDate" runat="server" CssClass="form-control"
                                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-5 col-md-5 col-sm-5col-xs-5 m-l--15">
                                                                        <asp:Label ID="lblApprovalExpBalance" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtApprovalBalance" runat="server" CssClass="form-control" Enabled="false"
                                                                                    Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblApprovalCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboApprovalCurrency" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                                    <li role="presentation" class="active"><a href="#tbpApprovalExpenseRemark" data-toggle="tab">
                                                                        <asp:Label ID="tbApprovalExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label" />
                                                                    </a></li>
                                                                    <li role="presentation"><a href="#tbpApprovalClaimRemark" data-toggle="tab">
                                                                        <asp:Label ID="tbApprovalClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label" />
                                                                    </a></li>
                                                                </ul>
                                                                <!-- Tab panes -->
                                                                <div class="tab-content">
                                                                    <div role="tabpanel" class="tab-pane fade in active" id="tbpApprovalExpenseRemark">
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtApprovalExpRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div role="tabpanel" class="tab-pane fade" id="tbpApprovalClaimRemark">
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtApprovalClaimRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnApprovalAdd" runat="server" CssClass="btn btn-primary" Text="Add" />
                                        <asp:Button ID="btnApprovalEdit" runat="server" CssClass="btn btn-primary" Text="Edit"
                                            Visible="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 170px">
                                                    <asp:DataGrid ID="dgvApprovalData" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                        ShowFooter="False" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" ToolTip="Edit">
                                                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                                            <i class="fas fa-trash text-danger"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" CommandName="attachment" ToolTip="Attachment">
                                                                                           <i class="fa fa-paperclip" aria-hidden="true" style="font-size:20px;font-weight:bold"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc"
                                                                ReadOnly="true" />
                                                            <asp:BoundColumn DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route"
                                                                ReadOnly="true" />
                                                            <asp:BoundColumn DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" ReadOnly="true" />
                                                             <asp:BoundColumn DataField="currency_sign" FooterText="dgcolhcurrency" HeaderText="Currency" />
                                                            <asp:BoundColumn DataField="quantity" FooterText="dgcolhQty" HeaderText="Quantity"
                                                                ItemStyle-CssClass="txttextalignright" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderText="Unit Price"
                                                                ItemStyle-CssClass="txttextalignright" ReadOnly="true" />
                                                             <asp:BoundColumn DataField="base_amount" FooterText="dgcolhBasicAmount" HeaderStyle-HorizontalAlign="Right"
                                                                HeaderText="Base Amount" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="amount" FooterText="dgcolhAmount" HeaderText="Amount"
                                                                ItemStyle-CssClass="txttextalignright" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="expense_remark" FooterText="dgcolhExpenseRemark" HeaderText="Expense Remark"
                                                                ReadOnly="true" Visible="true" />
                                                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhTranId" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false"
                                                                FooterText="objdgcolhGUID" />
                                                        </Columns>
                                                        <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                    </asp:DataGrid>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblApprovalGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtApprovalGrandTotal" runat="server" CssClass="form-control" Enabled="False"
                                                            Style="text-align: right"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnApprovalSaveAddEdit" runat="server" CssClass="btn btn-primary"
                            Text="Save" />
                        <asp:Button ID="btnApprovalCloseAddEdit" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                        <asp:HiddenField ID="hdf_ApprovalClaim" runat="server" />
                    </div>
                    <uc7:Confirmation ID="popup_ApprovalUnitPriceYesNo" runat="server" Title="Confirmation" />
                    <uc7:Confirmation ID="popup_ApprovalExpRemarkYesNo" runat="server" Title="Confirmation" />
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupApprovalEmpDepedents" runat="server" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="btnApprovalEmppnlEmpDepedentsClose" PopupControlID="pnlApprovalEmpDepedents"
                    CancelControlID="btnApprovalEmppnlEmpDepedentsClose">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlApprovalEmpDepedents" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblApprovalEmpDependentsList" runat="server" Text="Dependents List" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px;">
                                    <asp:DataGrid ID="dgApprovalDepedent" runat="server" AutoGenerateColumns="false"
                                        AllowPaging="false" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                            <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnApprovalEmppnlEmpDepedentsClose" runat="server" Text="Cancel"
                            CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdnApprovalEmpDepedents" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  m-t-25">
                                <div id="fileuploader">
                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse" />
                                </div>
                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                    Text="Browse" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 150px;">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete">
                                                                             <i class="fas fa-trash text-danger"></i> 
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download">
                                                                            <i class="fa fa-download"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                    <uc7:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                </asp:Panel>
               <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_ProcessLeaveAddEdit.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $("body").on("click", "input[type=file]", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>
