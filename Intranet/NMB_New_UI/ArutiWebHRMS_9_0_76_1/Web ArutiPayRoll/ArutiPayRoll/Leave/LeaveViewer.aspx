﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="LeaveViewer.aspx.vb"
    Inherits="Leave_Viewer" Title="Leave Viewer Form" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollLeft($(scroll1.Y).val());

            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Leave Viewer"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFromDate" runat="server" Text="FromDate" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtfromdate" runat="server" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblToDate" runat="server" Text="ToDate" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dttodate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 350px">
                                                            <asp:DataGrid ID="dgViewEmployeelist" runat="server" AutoGenerateColumns="False"
                                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Select" ToolTip="Select">
                                                                                                         <i class="fas fa-pencil-alt text-primary"></i>
                                                                                </asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="True">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="lblEmployee">
                                                                    </asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 405px">
                                                            <asp:DataGrid ID="dgViewLeaveType" runat="server" AllowPaging="false"  AutoGenerateColumns="False" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="leavetypecode" HeaderText="Leave Code" ReadOnly="True">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="leavename" HeaderText="Leave Name" ReadOnly="True" FooterText="colhLeavetype">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="color" HeaderText="Leave Color" Visible="False"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="footer">
                                     <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                             </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="dgViewLeave" runat="server" SkinIDWidth="200%" AllowPaging="false"
                                                AutoGenerateColumns="False" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundColumn DataField="CalendarTableID" HeaderText="CalendarTableID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MonthYear" HeaderText="Month"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="startday" HeaderText="startday"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MonthDays" HeaderText="MonthDays"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MonthId" HeaderText="MonthId"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="YearId" HeaderText="YearId"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sun_1" HeaderText="Sun"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Mon_1" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Tue_1" HeaderText="Tue"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Wed_1" HeaderText="Wed"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Thu_1" HeaderText="Thu"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Fri_1" HeaderText="Fri"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sat_1" HeaderText="Sat"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sun_2" HeaderText="Sun"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Mon_2" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Tue_2" HeaderText="Tue"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Wed_2" HeaderText="Wed"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Thu_2" HeaderText="Thu"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Fri_2" HeaderText="Fri"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sat_2" HeaderText="Sat"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sun_3" HeaderText="Sun"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Mon_3" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Tue_3" HeaderText="Tue"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Wed_3" HeaderText="Wed"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Thu_3" HeaderText="Thu"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Fri_3" HeaderText="Fri"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sat_3" HeaderText="Sat"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sun_4" HeaderText="Sun"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Mon_4" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Tue_4" HeaderText="Tue"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Wed_4" HeaderText="Wed"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Thu_4" HeaderText="Thu"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Fri_4" HeaderText="Fri"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sat_4" HeaderText="Sat"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sun_5" HeaderText="Sun"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Mon_5" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Tue_5" HeaderText="Tue"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Wed_5" HeaderText="Wed"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Thu_5" HeaderText="Thu"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Fri_5" HeaderText="Fri"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sat_5" HeaderText="Sat"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sun_6" HeaderText="Sun"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Mon_6" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Tue_6" HeaderText="Tue"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Wed_6" HeaderText="Wed"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Thu_6" HeaderText="Thu"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Fri_6" HeaderText="Fri"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sat_6" HeaderText="Sat"></asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
