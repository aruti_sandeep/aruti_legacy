﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingNeedForm.aspx.vb"
    Inherits="Training_wPg_TrainingNeedForm" Title="Training Need form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js" type="text/javascript"></script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript" language="javascript">

        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
        }
        function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboScanDcoumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    

    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }


        function CalcAmt() {
            var a = $$('txtPopUpCostQty').val().replace(/[^0-9\.-]+/g, "");
            var b = $$('txtPopUpCostRate').val().replace(/[^0-9\.-]+/g, "");
            var c = a * b;

            PageMethods.ConvertToCurrency(c, onSuccess, onFailure);

            function onSuccess(str) {
                $$('txtPopUpCostUnitAmount').val(str);
                $$('hfPopUpCostUnitAmount').val(str);
            }

            function onFailure(err) {
                alert(err);
            }

        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 98%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupTrainingCost" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblPopUpCost" runat="server" PopupControlID="pnlTrainingCost"
                    DropShadow="false">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlTrainingCost" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="lblPopUpCost" runat="server" Text="Training Cost" CssClass="form-label"></asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPopUpCostHeading2" runat="server" Text="Training Approx. Cost"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height: 470px;">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPopUpCostItem" runat="server" Text="Item" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPopUpCostItem" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPopUpCostQty" runat="server" Text="Quantity" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox runat="server" ID="txtPopUpCostQty" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right;" onfocusout="CalcAmt()" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPopUpCostRate" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox runat="server" ID="txtPopUpCostRate" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right;" onfocusout="CalcAmt()" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPopUpCostUnitAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-0">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox runat="server" ID="txtPopUpCostUnitAmount" CssClass="form-control" ReadOnly="true"
                                                        onKeypress="return onlyNumbers(this, event);" Style="text-align: right;"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboPopUpCurrency" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfPopUpCostUnitAmount" runat="server" />
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPopUpCostDescription" runat="server" Text="Description" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox runat="server" ID="txtPopUpCostDescription" TextMode="MultiLine" Rows="3"
                                                    CssClass="form-control"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnAddCost" runat="server" Text="Add" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnUpdateCost" runat="server" Text="Update" CssClass="btn btn-default" />
                                    <asp:Button ID="btnCancelCost" runat="server" Text="Cancel" CssClass="btn btn-default" />
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 230px;">
                                            <asp:Panel ID="Panel2" runat="server" Width="100%" ScrollBars="Auto">
                                                <asp:GridView ID="dgvItemCost" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%"
                                                    DataKeyNames="trainingneedformcosttranunkid, trainingcostitemunkid, countryunkid, description">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhCostedit" HeaderText="Edit"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCostedit" runat="server" ToolTip="Edit" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Change">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhCostdelete" HeaderText="Delete"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCostdelete" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Remove">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="trainingcostitemname" FooterText="colhItem" HeaderText="Item">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="quantity" FooterText="colhQty" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right"
                                                            ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="unitprice" FooterText="colhUnitCost" HeaderText="Unit Price"
                                                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="amount" FooterText="colhAmount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Right"
                                                            ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox runat="server" ID="txtGrandTotal" ReadOnly="true" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right;" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnCostSave" runat="server" Text="Save" CssClass="btn btn-primary"
                                    Visible="false" />
                                <asp:Button ID="btnCostClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div id="ScanAttachment">
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="false"
                        CancelControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="block-header">
                            <h2>
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment" CssClass="form-label"></asp:Label>
                                <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"
                                    CssClass="form-label"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                        <div id="fileuploader">
                                            <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse"
                                                onclick="return IsValidAttach();" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                        Text="Browse" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 300px;">
                                        <asp:GridView ID="dgvAttchment" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                            RowStyle-Wrap="false" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%"
                                            DataKeyNames="scanattachtranunkid, filepath, GUID">
                                            <Columns>
                                                <asp:TemplateField FooterText="objcohDelete">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>"
                                                                ToolTip="Delete">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField FooterText="objcolhDownload">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>"
                                                                ToolTip="Download"><i class="fas fa-download"></i></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhFileName" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <div style="float: left">
                                <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                            </div>
                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                        </div>
                    </asp:Panel>
                    <uc2:Cnf_YesNo ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation" />
                </div>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" _ShowSkill="true" />
                <ucDel:DeleteReason ID="popupDeleteTrainNeedForm" runat="server" Title="Are you sure you want to delete selected Training Need Request?"
                    ValidationGroup="DeleteTrainNeedForm" />
                <ucDel:DeleteReason ID="popupDeleteTrainNeedCost" runat="server" Title="Are you sure you want to delete selected Cost Item?"
                    ValidationGroup="DeleteTrainNeedCost" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Need form" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Training Need Request" CssClass="form-label"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                    <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFormNo" runat="server" Text="Form No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtFormNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkIncludeTraining" runat="server" Text="Include Trainings from Performance"
                                            AutoPostBack="true" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDatefrom" runat="server" Text="Date From" CssClass="form-label"></asp:Label>
                                        <uc3:DateCtrl ID="dtpDatefrom" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc3:DateCtrl ID="dtpDateTo" runat="server" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDevRequire" runat="server" Text="Dev. Required" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDevRequire" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPriority" runat="server" Text="Priority" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPriority" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtGrievanceDesc" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 230px;">
                                            <asp:Panel ID="pnl_dgvEmployeeList" runat="server" Width="100%" ScrollBars="Auto">
                                                <asp:GridView ID="dgvEmployeeList" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                            FooterText="objcolhCheckAll">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                    OnCheckedChanged="chkSelect_OnCheckedChanged" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeeunkid" Visible="false" FooterText="objcolhEmpID">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="employeecode" FooterText="colhEmpCode" HeaderText="Code">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="employeename" FooterText="colhEmpName" HeaderText="Employee Name">
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-default" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTraningNeedRequest" runat="server" Text="Training Need Request"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px;">
                                            <asp:Panel ID="Panel1" runat="server" Width="100%" ScrollBars="Auto">
                                                <asp:GridView ID="dgvTrainingNeed" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false" Width="99%"
                                                    DataKeyNames="trainingneedformtranunkid, periodunkid, trainingcourseunkid, priority, isperformance_training, description">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="Edit" FooterText="colhedit"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkedit" runat="server" ToolTip="Edit" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Change">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="Delete" FooterText="colhdelete"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkdelete" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="Remove">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="2%" HeaderText="Cost"
                                                            FooterText="colhAddCost">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkAddCost" runat="server" ToolTip="Add Cost" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="AddCost">
                                                                        <i class="far fa-money-bill-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="2%" HeaderText="Attach."
                                                            FooterText="colhAddAttachment">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkAddAttachment" runat="server" ToolTip="Add Attachment" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    CommandName="AddAttachment">
                                                                        <i class="fas fa-paperclip"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="trainingcoursename" HeaderText="Dev. Required" FooterText="colhDevRequired">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TotalEmployee" HeaderText="Count" FooterText="colhCount">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="start_date" HeaderText="Start Date" FooterText="colhStartdate">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="end_date" HeaderText="End Date" FooterText="colhEnddate">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="priorityname" HeaderText="Priority" FooterText="colhPriority">
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSubmission_remark" runat="server" Text="Submission Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSubmission_remark" runat="server" TextMode="MultiLine" Rows="2"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                                    Visible="false" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="dgvTrainingNeed" EventName="RowCommand" />
                <asp:PostBackTrigger ControlID="dgvAttchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                url: "wPg_TrainingNeedForm.aspx?uploadimage=mSEfU19VPc4=",
                multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                    $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
        //$('input[type=file]').live("click", function() {
        $("body").on("click", 'input[type=file]', function(){
            return IsValidAttach();
        });
    </script>

</asp:Content>
