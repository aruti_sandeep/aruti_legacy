﻿Imports System.Data
Imports Aruti.Data
Imports System.Drawing

Partial Class Training_Training_Request_wPg_TrainingRequestFormList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTrainingRequestFormList"
    Private mintTrainingRequestunkid As Integer
    Private mstrAdvanceFilter As String = String.Empty
    Private currentId As String = ""
    'Hemant (28 Jul 2021) -- Start             
    'ENHANCEMENT : OLD-293 - Training Evaluation
    Private mstrConfirmationAction As String = ""
    'Hemant (28 Jul 2021) -- End
    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    Private mintTrainingNeedAllocationID As Integer = -1
    'Hemant (07 Mar 2022) -- End

#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                mintTrainingNeedAllocationID = CInt(Session("TrainingNeedAllocationID"))
                'Hemant (07 Mar 2022) -- End
                Call FillCombo()
                Call FillList()
            Else
                mintTrainingRequestunkid = CInt(Me.ViewState("mintTrainingRequestunkid"))
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
                'Hemant (28 Jul 2021) -- Start             
                'ENHANCEMENT : OLD-293 - Training Evaluation
                mstrConfirmationAction = Me.ViewState("mstrConfirmationAction")
                'Hemant (28 Jul 2021) -- End
                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                mintTrainingNeedAllocationID = CInt(Me.ViewState("mintTrainingNeedAllocationID"))
                'Hemant (07 Mar 2022) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            Me.ViewState("mstrConfirmationAction") = mstrConfirmationAction
            'Hemant (28 Jul 2021) -- End
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Me.ViewState("mintTrainingNeedAllocationID") = mintTrainingNeedAllocationID
            'Hemant (07 Mar 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objApprovalMaster As New clstraining_requisition_approval_master
        Dim dsList As New DataSet
        'Hemant (03 Dec 2021) -- Start
        'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
        Dim objTPeriod As New clsTraining_Calendar_Master
        'Hemant (03 Dec 2021) -- End
        Try

            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.            
            dsList = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            'Hemant (03 Dec 2021) -- End

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "EmpList")

                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("employeeunkid") = 0
                dRow.Item("EmpCodeName") = "Select"
                dsList.Tables(0).Rows.InsertAt(dRow, 0)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With drpTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With

            dsList = objMaster.GetCondition(False, True, True, False, False)

            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dtCondition
                .DataBind()
                .SelectedIndex = 0
            End With

            dsList = objApprovalMaster.getStatusComboList("List", True)
            With drpStatus
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objMaster.GetYesNo(True, "List")
            With drpScheduled
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objApprovalMaster.getCompletionStatusComboList("List", True)
            With cboCompletionStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            objTPeriod = Nothing
            'Hemant (03 Dec 2021) -- End
            objEmployee = Nothing
            objApprovalMaster = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList(Optional ByVal blnBindGridView As Boolean = True)
        'Hemant (07 Mar 2022) -- [blnBindGridView]
        Dim objRequestMaster As New clstraining_request_master
        Dim dtList As New DataTable
        Dim StrSearching As String = String.Empty
        Try
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND trtraining_request_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If
            'Hemant (03 Dec 2021) -- End

            If CInt(drpTrainingName.SelectedValue) > 0 Then
                StrSearching &= "AND trtraining_request_master.coursemasterunkid = " & CInt(drpTrainingName.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND trtraining_request_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(drpStatus.SelectedValue) > 0 Then
                StrSearching &= "AND trtraining_request_master.statusunkid = " & CInt(drpStatus.SelectedValue) & " "
            End If

            If CInt(drpScheduled.SelectedValue) > 0 Then
                If CInt(drpScheduled.SelectedValue) = CStr(1) Then
                    StrSearching &= "AND trtraining_request_master.isscheduled = 1 "
                ElseIf CInt(drpScheduled.SelectedValue) = CStr(2) Then
                    StrSearching &= "AND trtraining_request_master.isscheduled = 0 "
                End If
            End If

            If CInt(cboCompletionStatus.SelectedValue) > 0 Then
                StrSearching &= "AND trtraining_request_master.completed_statusunkid = " & CInt(cboCompletionStatus.SelectedValue) & " "
            End If

            If txtTotalTrainingCost.Text.Trim <> "" AndAlso CDec(txtTotalTrainingCost.Text) > 0 Then
                StrSearching &= "AND trtraining_request_master.totaltrainingcost " & cboCondition.SelectedItem.Text & " " & CDec(txtTotalTrainingCost.Text) & " "
            End If

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-951 - As a user, I want to have Request From and Request To filters on the training request list page
            If dtpTrainingRequestFrom.IsNull = False Then
                StrSearching &= "AND CONVERT(CHAR(8),trtraining_request_master.application_date,112) >= " & eZeeDate.convertDate(dtpTrainingRequestFrom.GetDate.Date) & " "
            End If

            If dtpTrainingRequestTo.IsNull = False Then
                StrSearching &= "AND CONVERT(CHAR(8),trtraining_request_master.application_date,112) <= " & eZeeDate.convertDate(dtpTrainingRequestTo.GetDate.Date) & " "
            End If
            'Hemant (12 Oct 2022) -- End


            'Hemant (05 Jul 2024) -- Start
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    StrSearching &= "AND " & mstrAdvanceFilter
            'End If
            'Hemant (05 Jul 2024) -- End


            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dtList = objRequestMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), _
                                              True, CBool(Session("IsIncludeInactiveEmp")), "Training", mintTrainingNeedAllocationID, , StrSearching, , _
                                              CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)), True, _
                                              mstrAdvanceFilter).Tables(0)
            'Hemant (07 Mar 2022) -- [mintTrainingNeedAllocationID, blnAddGrouping:=True]
            If dtList.Rows.Count > 0 Then
                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.
                'objRequestsGrandList.Text = Format(dtList.Compute("SUM(totaltrainingcost)", ""), Session("fmtCurrency"))
                'objApprovedGrandList.Text = Format(dtList.Compute("SUM(approvedamount)", ""), Session("fmtCurrency"))
                objRequestsGrandList.Text = Format(dtList.Compute("SUM(TotalTrainingCost)", "IsGrp = 1"), Session("fmtCurrency"))
                objApprovedGrandList.Text = Format(dtList.Compute("SUM(approvedamount)", "IsGrp = 1"), Session("fmtCurrency"))
                'Hemant (07 Mar 2022) -- End
            Else
                objRequestsGrandList.Text = "0.00"
                objApprovedGrandList.Text = "0.00"
            End If

            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            'Dim dtEmployeeView As DataTable = dtList.DefaultView.ToTable(True, "employeeunkid", "employeename")
            'If dtList.Columns.Contains("IsGrp") = False Then
            '    Dim dtCol As New DataColumn
            '    dtCol = New DataColumn
            '    dtCol.ColumnName = "IsGrp"
            '    dtCol.Caption = "IsGrp"
            '    dtCol.DataType = System.Type.GetType("System.Boolean")
            '    dtCol.DefaultValue = False
            '    dtList.Columns.Add(dtCol)

            'End If
            'Dim dRow As DataRow = Nothing
            'For Each drEmployee As DataRow In dtEmployeeView.Rows
            '    Dim drRow() As DataRow = dtList.Select("employeeunkid = " & CInt(drEmployee.Item("employeeunkid")) & " ")
            '    If drRow.Length > 0 Then
            '        dRow = dtList.NewRow()
            '        dRow.Item("trainingrequestunkid") = -1
            '        dRow.Item("employeeunkid") = CInt(drEmployee.Item("employeeunkid"))
            '        dRow.Item("employeename") = CStr(drEmployee.Item("employeename"))
            '        dRow.Item("IsGrp") = 1
            '        dtList.Rows.Add(dRow)
            '    End If

            'Next

            'Dim dtTable As DataTable
            'dtTable = New DataView(dtList, "", "employeeunkid  , trainingrequestunkid  ", DataViewRowState.CurrentRows).ToTable.Copy
            Dim dtTable As DataTable = dtList.Copy()
            Dim dtFinalTable As New DataTable
            If Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList") IsNot Nothing Then
                Cache.Remove(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList")
            End If
            If Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList") Is Nothing Then
                Cache.Insert(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList", dtTable, New CacheDependency(Server.MapPath(CStr(Session("Database_Name")) & "_TrainingRequestFormList.xml")), DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration)
            Else
                dtTable = CType(Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList"), DataTable)
            End If
            dtFinalTable = New DataView(dtTable, "IsGrp = 1 ", "", DataViewRowState.CurrentRows).ToTable
            'Hemant (07 Mar 2022) -- End


            'dgTrainingName.DataSource = dtTable
            'dgTrainingName.DataBind()
            If blnBindGridView = True Then
                gvSummary.AutoGenerateColumns = False
                gvSummary.DataSource = dtFinalTable
                gvSummary.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByRef objRequest As clstraining_request_master)
        Try
            objRequest._TrainingRequestunkid = mintTrainingRequestunkid
            objRequest._AuditUserId = CInt(Session("UserId"))
            objRequest._ClientIP = CStr(Session("IP_ADD"))
            objRequest._FormName = mstrModuleName
            objRequest._IsWeb = True
            objRequest._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview's Events"



    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    'Protected Sub dgTrainingName_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgTrainingName.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If CBool(dgTrainingName.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
    '                'Hemant (07 Mar 2022) -- Start            
    '                'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    '                'e.Row.Cells(7).Text = DataBinder.Eval(e.Row.DataItem, "employeename").ToString
    '                e.Row.Cells(7).Text = DataBinder.Eval(e.Row.DataItem, "refno").ToString & " - " & DataBinder.Eval(e.Row.DataItem, "CreateUserName").ToString & " - " & DataBinder.Eval(e.Row.DataItem, "Training").ToString & " - " & DataBinder.Eval(e.Row.DataItem, "allocationtranname").ToString & " - (" + CDate(DataBinder.Eval(e.Row.DataItem, "start_date")).Date.ToShortDateString + " - " + CDate(DataBinder.Eval(e.Row.DataItem, "end_date")).Date.ToShortDateString + ") "
    '                'Hemant (07 Mar 2022) -- End                   
    '                e.Row.Cells(7).ColumnSpan = e.Row.Cells.Count - 1
    '                e.Row.BackColor = Color.Silver
    '                e.Row.ForeColor = Color.Black
    '                e.Row.Font.Bold = True

    '                For i As Integer = 8 To e.Row.Cells.Count - 1
    '                    e.Row.Cells(i).Visible = False
    '                Next

    '                Dim ImgSelect As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
    '                ImgSelect.Visible = False

    '                Dim ImgDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
    '                ImgDelete.Visible = False

    '                Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
    '                ImgComplete.Visible = False

    '                Dim ImgEnroll As LinkButton = TryCast(e.Row.FindControl("ImgEnroll"), LinkButton)
    '                ImgEnroll.Visible = False

    '                'Hemant (28 Jul 2021) -- Start             
    '                'ENHANCEMENT : OLD-293 - Training Evaluation
    '                Dim ImgPreTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgPreTrainingFeedBack"), LinkButton)
    '                ImgPreTrainingFeedBack.Visible = False

    '                Dim ImgPostTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgPostTrainingFeedBack"), LinkButton)
    '                ImgPostTrainingFeedBack.Visible = False

    '                Dim ImgDaysAfterTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgDaysAfterTrainingFeedBack"), LinkButton)
    '                ImgDaysAfterTrainingFeedBack.Visible = False
    '                'Hemant (28 Jul 2021) -- End

    '            Else

    '                If e.Row.Cells(8).Text.ToString().Trim <> "" AndAlso e.Row.Cells(8).Text.Trim <> "&nbsp;" Then
    '                    e.Row.Cells(8).Text = CDate(e.Row.Cells(8).Text).Date.ToShortDateString
    '                End If

    '                If e.Row.Cells(9).Text.ToString().Trim <> "" AndAlso e.Row.Cells(9).Text.Trim <> "&nbsp;" Then
    '                    e.Row.Cells(9).Text = CDate(e.Row.Cells(9).Text).Date.ToShortDateString
    '                End If

    '                e.Row.Cells(11).Text = Format(CDec(e.Row.Cells(11).Text), Session("fmtCurrency"))
    '                e.Row.Cells(12).Text = Format(CDec(e.Row.Cells(12).Text), Session("fmtCurrency"))

    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
    '                Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
    '                If CBool(dgTrainingName.DataKeys(e.Row.RowIndex)("issubmit_approval").ToString) Then
    '                    lnkedit.Visible = False
    '                    lnkdelete.Visible = False
    '                Else
    '                    lnkedit.Visible = True
    '                    lnkdelete.Visible = True
    '                    lnkedit.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Edit")
    '                    lnkdelete.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Delete")
    '                End If
    '                'Hemant (28 Jul 2021) -- Start             
    '                'ENHANCEMENT : OLD-293 - Training Evaluation
    '                Dim objEvalQuestion As New clseval_question_master
    '                Dim dsQuestions As DataSet
    '                dsQuestions = objEvalQuestion.GetList("List")

    '                Dim ImgPreTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgPreTrainingFeedBack"), LinkButton)
    '                Dim ImgPostTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgPostTrainingFeedBack"), LinkButton)
    '                Dim ImgDaysAfterTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgDaysAfterTrainingFeedBack"), LinkButton)
    '                ImgPreTrainingFeedBack.Visible = False
    '                ImgPostTrainingFeedBack.Visible = False
    '                ImgDaysAfterTrainingFeedBack.Visible = False
    '                Dim drQuestions() As DataRow
    '                'Hemant (28 Jul 2021) -- End

    '                Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
    '                Dim ImgEnroll As LinkButton = TryCast(e.Row.FindControl("ImgEnroll"), LinkButton)
    '                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
    '                    ImgComplete.Visible = False
    '                    'Hemant (25 May 2021) -- Start
    '                    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
    '                    'Hemant (09 Feb 2022) -- Start            
    '                    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
    '                    'If CBool(Session("AllowToMarkTrainingAsComplete")) = True AndAlso _
    '                    '   CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
    '                    '   CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False AndAlso _
    '                    '   CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.PENDING Then
    '                    If CBool(Session("AllowToMarkTrainingAsComplete")) = True AndAlso _
    '                       (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = False AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) = True AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False AndAlso _
    '                        CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.PENDING) OrElse _
    '                       (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = True AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) = True AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False AndAlso _
    '                        CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.PENDING AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isposttrainingfeedback_submitted")) = True) Then
    '                        'Hemant (09 Feb 2022) -- End
    '                        ImgComplete.Visible = True
    '                    End If
    '                    'Hemant (25 May 2021) -- End
    '                    ImgEnroll.Visible = False
    '                    'Hemant (20 Aug 2021) -- Start
    '                    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    '                    If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
    '                       CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
    '                       CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED AndAlso _
    '                       CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isdaysafterfeedback_submitted")) = True _
    '                       Then
    '                        Dim intMinDays As Integer
    '                        Dim intEmployeeId As Integer
    '                        Dim intUser_EmpId As Integer = 0
    '                        Dim objReportTo As New clsReportingToEmployee
    '                        Dim objCategory As New clseval_group_master
    '                        Dim objUserAddEdit As New clsUserAddEdit

    '                        Dim dsCategory As DataSet

    '                        intEmployeeId = CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("employeeunkid"))

    '                        objUserAddEdit._Userunkid = Session("UserId")
    '                        If objUserAddEdit._EmployeeUnkid > 0 AndAlso objUserAddEdit._EmployeeCompanyUnkid = CInt(Session("CompanyUnkId")) Then
    '                            intUser_EmpId = objUserAddEdit._EmployeeUnkid
    '                        End If

    '                        objReportTo._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = intEmployeeId

    '                        Dim dt As DataTable = objReportTo._RDataTable
    '                        Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True And CInt(p.Item("reporttoemployeeunkid")) = CInt(intUser_EmpId)) Select (p)).ToList
    '                        If DefaultReportList.Count > 0 Then
    '                            dsCategory = objCategory.GetList("List")
    '                            Dim drDaysAfterTraining() As DataRow = dsCategory.Tables(0).Select("feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & "")
    '                            If drDaysAfterTraining.Length > 0 Then
    '                                intMinDays = CInt(dsCategory.Tables(0).Compute("MIN(feedbacknodaysaftertrainingattended)", "feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & ""))
    '                            End If
    '                            If CDate(ConfigParameter._Object._CurrentDateAndTime.Date) >= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_approval_date")).Date.AddDays(intMinDays) Then
    '                                drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING & " AND isforlinemanager = 1 ")
    '                                If drQuestions.Length > 0 Then
    '                                    ImgDaysAfterTrainingFeedBack.Visible = True
    '                                    ImgDaysAfterTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Days After Training FeedBack")
    '                                End If
    '                            End If
    '                        End If
    '                        objReportTo = Nothing
    '                        objCategory = Nothing
    '                        objUserAddEdit = Nothing
    '                    End If
    '                    dsQuestions = Nothing
    '                    'Hemant (20 Aug 2021) -- End
    '                Else
    '                    'Hemant (09 Feb 2022) -- Start            
    '                    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
    '                    'If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
    '                    '    CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False Then
    '                    If (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = False AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval") = False)) OrElse _
    '                        (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = True AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval") = False) AndAlso _
    '                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isposttrainingfeedback_submitted")) = True) Then
    '                        'Hemant (09 Feb 2022) -- End
    '                        'Hemant (09 Feb 2022) -- Start            
    '                        'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    '                        If CDate(ConfigParameter._Object._CurrentDateAndTime.Date) >= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("end_date")).Date AndAlso _
    '                           CDate(ConfigParameter._Object._CurrentDateAndTime.Date) <= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("end_date")).Date.AddDays(CInt(Session("DaysFromLastDateOfTrainingToAllowCompleteTraining"))) Then
    '                            'Hemant (09 Feb 2022) -- End
    '                            ImgComplete.Visible = True
    '                            'Hemant (09 Feb 2022) -- Start            
    '                            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    '                        Else
    '                            ImgComplete.Enabled = False
    '                            ImgComplete.Text = "<i class=""fas fa-check-circle"" style=""font-size:15px; color:Gray""></i >"
    '                        End If
    '                        'Hemant (09 Feb 2022) -- End
    '                    Else
    '                        ImgComplete.Visible = False
    '                    End If

    '                    If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) = False AndAlso _
    '                        CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("statusunkid")) = enTrainingRequestStatus.APPROVED Then
    '                        ImgEnroll.Visible = True
    '                    Else
    '                        ImgEnroll.Visible = False
    '                    End If

    '                    'Hemant (28 Jul 2021) -- Start             
    '                    'ENHANCEMENT : OLD-293 - Training Evaluation
    '                    If CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("statusunkid")) = enTrainingRequestStatus.APPROVED Then
    '                        drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.PRETRAINING)
    '                        If drQuestions.Length > 0 Then
    '                            ImgPreTrainingFeedBack.Visible = True
    '                            'Hemant (13 Aug 2021) -- Start
    '                            'ISSUE/ENHANCEMENT : OLD-441 - Provide the captions that display on mouse hover in language i.e. postTraining feedback and Days After Training Feedback.
    '                            ImgPreTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "PreTraining FeedBack")
    '                            'Hemant (13 Aug 2021) -- End
    '                        End If
    '                    End If

    '                    'Hemant (09 Feb 2022) -- Start            
    '                    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
    '                    'If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
    '                    '   CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
    '                    '   CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
    '                    If (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = False AndAlso _
    '                            CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
    '                            CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
    '                            CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED) OrElse _
    '                       (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = True AndAlso _
    '                            CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
    '                            (CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False OrElse _
    '                             CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isposttrainingfeedback_submitted")) = True)) Then
    '                        'Hemant (09 Feb 2022) -- End

    '                        drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.POSTTRAINING)
    '                        If drQuestions.Length > 0 Then
    '                            ImgPostTrainingFeedBack.Visible = True
    '                            'Hemant (13 Aug 2021) -- Start
    '                            'ISSUE/ENHANCEMENT : OLD-441 - Provide the captions that display on mouse hover in language i.e. postTraining feedback and Days After Training Feedback.
    '                            ImgPostTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Post Training FeedBack")
    '                            'Hemant (13 Aug 2021) -- End
    '                        End If

    '                        'Hemant (09 Feb 2022) -- Start            
    '                        'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
    '                    End If
    '                    If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
    '                       CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
    '                       CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
    '                        Dim intMinDays As Integer
    '                        Dim objCategory As New clseval_group_master
    '                        Dim dsCategory As DataSet
    '                        dsCategory = objCategory.GetList("List")
    '                        Dim drDaysAfterTraining() As DataRow = dsCategory.Tables(0).Select("feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & "")
    '                        If drDaysAfterTraining.Length > 0 Then
    '                            intMinDays = CInt(dsCategory.Tables(0).Compute("MIN(feedbacknodaysaftertrainingattended)", "feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & ""))
    '                        End If
    '                        If CDate(ConfigParameter._Object._CurrentDateAndTime.Date) >= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_approval_date")).Date.AddDays(intMinDays) Then
    '                            drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING)
    '                            If drQuestions.Length > 0 Then
    '                                ImgDaysAfterTrainingFeedBack.Visible = True
    '                                'Hemant (13 Aug 2021) -- Start
    '                                'ISSUE/ENHANCEMENT : OLD-441 - Provide the captions that display on mouse hover in language i.e. postTraining feedback and Days After Training Feedback.
    '                                ImgDaysAfterTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Days After Training FeedBack")
    '                                'Hemant (13 Aug 2021) -- End
    '                            End If
    '                        End If
    '                        objCategory = Nothing
    '                    End If
    '                    dsQuestions = Nothing
    '                    'Hemant (28 Jul 2021) -- End
    '                End If
    '                'Hemant (28 Jul 2021) -- Start             
    '                'ENHANCEMENT : OLD-293 - Training Evaluation
    '                objEvalQuestion = Nothing
    '                'Hemant (28 Jul 2021) -- End                    
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    Protected Sub dgTrainingName_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text).Date.ToShortDateString
                End If

                If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text).Date.ToShortDateString
                End If

                'e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhTotalTrainingCost", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhTotalTrainingCost", False, True)).Text), Session("fmtCurrency"))
                'e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhApprovedAmount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhApprovedAmount", False, True)).Text), Session("fmtCurrency"))

                Dim dgTrainingName As GridView = CType(sender, GridView)
                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                If CBool(dgTrainingName.DataKeys(e.Row.RowIndex)("issubmit_approval").ToString) Then
                    lnkedit.Visible = False
                    lnkdelete.Visible = False
                Else
                    lnkedit.Visible = True
                    lnkdelete.Visible = True
                    lnkedit.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Edit")
                    lnkdelete.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Delete")
                End If
                'Hemant (28 Jul 2021) -- Start             
                'ENHANCEMENT : OLD-293 - Training Evaluation
                Dim objEvalQuestion As New clseval_question_master

                Dim dsQuestions As DataSet
                dsQuestions = objEvalQuestion.GetList("List")
                'Hemant (08 Dec 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                Dim dsGridQuestions As DataSet
                dsGridQuestions = (New clseval_grid_columns_master).GetList("List", , , , , , False)
                Dim drGridQuestions() As DataRow
                'Hemant (08 Dec 2023) -- End

                Dim ImgPreTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgPreTrainingFeedBack"), LinkButton)
                Dim ImgPostTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgPostTrainingFeedBack"), LinkButton)
                Dim ImgDaysAfterTrainingFeedBack As LinkButton = TryCast(e.Row.FindControl("ImgDaysAfterTrainingFeedBack"), LinkButton)
                ImgPreTrainingFeedBack.Visible = False
                ImgPostTrainingFeedBack.Visible = False
                ImgDaysAfterTrainingFeedBack.Visible = False
                Dim drQuestions() As DataRow
                'Hemant (28 Jul 2021) -- End

                Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                Dim ImgEnroll As LinkButton = TryCast(e.Row.FindControl("ImgEnroll"), LinkButton)
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    ImgComplete.Visible = False
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
                    'If CBool(Session("AllowToMarkTrainingAsComplete")) = True AndAlso _
                    '   CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
                    '   CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False AndAlso _
                    '   CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.PENDING Then
                    If CBool(Session("AllowToMarkTrainingAsComplete")) = True AndAlso _
                       (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = False AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) = True AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False AndAlso _
                        CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.PENDING) OrElse _
                       (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = True AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) = True AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False AndAlso _
                        CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.PENDING AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isposttrainingfeedback_submitted")) = True) Then
                        'Hemant (09 Feb 2022) -- End
                        ImgComplete.Visible = True
                    End If
                    'Hemant (25 May 2021) -- End
                    ImgEnroll.Visible = False
                    'Hemant (20 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                    If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
                       CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
                       CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED AndAlso _
                       CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isdaysafterfeedback_submitted")) = True _
                       Then
                        Dim intMinDays As Integer
                        Dim intEmployeeId As Integer
                        Dim intUser_EmpId As Integer = 0
                        Dim objReportTo As New clsReportingToEmployee
                        Dim objCategory As New clseval_group_master
                        Dim objUserAddEdit As New clsUserAddEdit

                        Dim dsCategory As DataSet

                        intEmployeeId = CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("employeeunkid"))

                        objUserAddEdit._Userunkid = Session("UserId")
                        If objUserAddEdit._EmployeeUnkid > 0 AndAlso objUserAddEdit._EmployeeCompanyUnkid = CInt(Session("CompanyUnkId")) Then
                            intUser_EmpId = objUserAddEdit._EmployeeUnkid
                        End If

                        objReportTo._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = intEmployeeId

                        Dim dt As DataTable = objReportTo._RDataTable
                        Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True And CInt(p.Item("reporttoemployeeunkid")) = CInt(intUser_EmpId)) Select (p)).ToList
                        If DefaultReportList.Count > 0 Then
                            dsCategory = objCategory.GetList("List")
                            Dim drDaysAfterTraining() As DataRow = dsCategory.Tables(0).Select("feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & "")
                            If drDaysAfterTraining.Length > 0 Then
                                intMinDays = CInt(dsCategory.Tables(0).Compute("MIN(feedbacknodaysaftertrainingattended)", "feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & ""))
                            End If
                            If CDate(ConfigParameter._Object._CurrentDateAndTime.Date) >= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_approval_date")).Date.AddDays(intMinDays) Then
                                drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING & " AND isforlinemanager = 1 ")
                                'Hemant (08 Dec 2023) -- Start
                                'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                                drGridQuestions = dsGridQuestions.Tables(0).Select("isforlinemanager = 1 ")
                                'Hemant (08 Dec 2023) -- End
                                If drQuestions.Length > 0 OrElse drGridQuestions.Length > 0 Then
                                    'Hemant (08 Dec 2023) -- [OrElse drGridQuestions.Length > 0]
                                    ImgDaysAfterTrainingFeedBack.Visible = True
                                    ImgDaysAfterTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Days After Training FeedBack")
                                End If
                            End If
                        End If
                        objReportTo = Nothing
                        objCategory = Nothing
                        objUserAddEdit = Nothing
                    End If
                    dsQuestions = Nothing
                    'Hemant (20 Aug 2021) -- End
                Else
                    'Hemant (22 Dec 2023) -- Start
                    'ENHANCEMENT(TRA): A1X-1622 - Evaluation form settings enhancement to skip training completion process
                    If CBool(Session("SkipTrainingCompletionProcess")) = True Then
                        ImgComplete.Visible = False
                    Else
                        'Hemant (22 Dec 2023) -- End
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
                    'If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
                    '    CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False Then
                    If (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = False AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval") = False)) OrElse _
                        (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = True AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval") = False) AndAlso _
                        CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isposttrainingfeedback_submitted")) = True) Then
                        'Hemant (09 Feb 2022) -- End
                        'Hemant (09 Feb 2022) -- Start            
                        'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                        If CDate(ConfigParameter._Object._CurrentDateAndTime.Date) >= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("end_date")).Date AndAlso _
                           CDate(ConfigParameter._Object._CurrentDateAndTime.Date) <= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("end_date")).Date.AddDays(CInt(Session("DaysFromLastDateOfTrainingToAllowCompleteTraining"))) Then
                            'Hemant (09 Feb 2022) -- End
                            ImgComplete.Visible = True
                            'Hemant (09 Feb 2022) -- Start            
                            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                        Else
                            ImgComplete.Enabled = False
                            ImgComplete.Text = "<i class=""fas fa-check-circle"" style=""font-size:15px; color:Gray""></i >"
                        End If
                        'Hemant (09 Feb 2022) -- End
                    Else
                        ImgComplete.Visible = False
                    End If
                    End If  'Hemant (22 Dec 2023) -- End

                    'Hemant (22 Dec 2023) -- Start
                    'ENHANCEMENT(TRA): A1X-1623 - Evaluation form settings enhancement to skip training enrollment process
                    If CBool(Session("SkipTrainingEnrollmentProcess")) = True Then
                        ImgEnroll.Visible = False
                    Else
                        'Hemant (22 Dec 2023) -- End
                    If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) = False AndAlso _
                        CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("statusunkid")) = enTrainingRequestStatus.APPROVED Then
                        ImgEnroll.Visible = True
                    Else
                        ImgEnroll.Visible = False
                    End If
                    End If 'Hemant (22 Dec 2023)

                    'Hemant (28 Jul 2021) -- Start             
                    'ENHANCEMENT : OLD-293 - Training Evaluation
                    'Hemant (05 Jul 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2375 : Hide Training Evaluation screens
                    If CBool(Session("SkipPreTrainingEvaluationProcess")) = True Then
                        ImgPreTrainingFeedBack.Visible = False
                    Else
                        'Hemant (05 Jul 2024) -- End
                    If CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("statusunkid")) = enTrainingRequestStatus.APPROVED Then
                        drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.PRETRAINING)
                        If drQuestions.Length > 0 Then
                            ImgPreTrainingFeedBack.Visible = True
                            'Hemant (13 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-441 - Provide the captions that display on mouse hover in language i.e. postTraining feedback and Days After Training Feedback.
                            ImgPreTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "PreTraining FeedBack")
                            'Hemant (13 Aug 2021) -- End
                        End If
                    End If
                    End If 'Hemant (05 Jul 2024)


                    'Hemant (05 Jul 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2375 : Hide Training Evaluation screens
                    If CBool(Session("SkipPostTrainingEvaluationProcess")) = True Then
                        ImgPostTrainingFeedBack.Visible = False
                    Else
                        'Hemant (05 Jul 2024) -- End
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
                    'If CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
                    '   CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
                    '   CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
                    If (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = False AndAlso _
                            CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
                            CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
                            CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED) OrElse _
                       (CBool(Session("PostTrainingEvaluationBeforeCompleteTraining")) = True AndAlso _
                            CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm") = True) AndAlso _
                            (CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = False OrElse _
                             CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isposttrainingfeedback_submitted")) = True)) Then
                        'Hemant (09 Feb 2022) -- End

                        drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.POSTTRAINING)
                        If drQuestions.Length > 0 Then
                            ImgPostTrainingFeedBack.Visible = True
                            'Hemant (13 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-441 - Provide the captions that display on mouse hover in language i.e. postTraining feedback and Days After Training Feedback.
                            ImgPostTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Post Training FeedBack")
                            'Hemant (13 Aug 2021) -- End
                        End If

                        'Hemant (09 Feb 2022) -- Start            
                        'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
                    End If
                    End If 'Hemant (05 Jul 2024)
                    If CBool(Session("SkipDaysAfterTrainingEvaluationProcess")) = False AndAlso CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("isenroll_confirm")) AndAlso _
                       CBool(dgTrainingName.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = True AndAlso _
                       CInt(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
                        'Hemant (05 Jul 2024) -- [CBool(Session("SkipDaysAfterTrainingEvaluationProcess")) = False AndAlso]
                        Dim intMinDays As Integer
                        Dim objCategory As New clseval_group_master
                        Dim dsCategory As DataSet
                        dsCategory = objCategory.GetList("List")
                        Dim drDaysAfterTraining() As DataRow = dsCategory.Tables(0).Select("feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & "")
                        If drDaysAfterTraining.Length > 0 Then
                            intMinDays = CInt(dsCategory.Tables(0).Compute("MIN(feedbacknodaysaftertrainingattended)", "feedbackmodeid=" & CInt(clseval_group_master.enFeedBack.DAYSAFTERTRAINING) & ""))
                        End If
                        If CDate(ConfigParameter._Object._CurrentDateAndTime.Date) >= CDate(dgTrainingName.DataKeys(e.Row.RowIndex).Item("completed_approval_date")).Date.AddDays(intMinDays) Then
                            drQuestions = dsQuestions.Tables(0).Select("feedbackmodeid = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING)
                            'Hemant (08 Dec 2023) -- Start
                            'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                            drGridQuestions = dsGridQuestions.Tables(0).Select("isforposttraining = 1 ")
                            'Hemant (08 Dec 2023) -- End
                            If drQuestions.Length > 0 OrElse drGridQuestions.Length > 0 Then
                                'Hemant (08 Dec 2023) -- [OrElse drGridQuestions.Length > 0]
                                ImgDaysAfterTrainingFeedBack.Visible = True
                                'Hemant (13 Aug 2021) -- Start
                                'ISSUE/ENHANCEMENT : OLD-441 - Provide the captions that display on mouse hover in language i.e. postTraining feedback and Days After Training Feedback.
                                ImgDaysAfterTrainingFeedBack.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Days After Training FeedBack")
                                'Hemant (13 Aug 2021) -- End
                            End If
                        End If
                        objCategory = Nothing
                    End If
                    dsQuestions = Nothing
                    'Hemant (28 Jul 2021) -- End

                    'Hemant (28 Jul 2021) -- Start             
                    'ENHANCEMENT : OLD-293 - Training Evaluation
                    objEvalQuestion = Nothing
                    'Hemant (28 Jul 2021) -- End                    
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvSummary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSummary.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If CBool(gvSummary.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                        e.Row.BackColor = Color.Silver
                        e.Row.ForeColor = Color.Black
                        e.Row.Font.Bold = True

                        e.Row.Cells(getColumnID_Griview(gvSummary, "colhTotal_training_cost", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(gvSummary, "colhTotal_training_cost", False, True)).Text), Session("fmtcurrency").ToString())
                        e.Row.Cells(getColumnID_Griview(gvSummary, "colhApproved_amount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(gvSummary, "colhApproved_amount", False, True)).Text), Session("fmtcurrency").ToString())

                        If e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text).Date.ToShortDateString
                        End If
                        If e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text).Date.ToShortDateString
                        End If

                        Dim dgTrainingName As GridView = TryCast(e.Row.FindControl("dgTrainingName"), GridView)
                        Dim strRefno As String = gvSummary.DataKeys(e.Row.RowIndex).Item("refno").ToString
                        If Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList") Is Nothing Then
                            Call FillList(False)
                        End If
                        Dim dtTable As DataTable = CType(Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_TrainingRequestFormList"), DataTable)

                        If dtTable IsNot Nothing Then
                            dtTable = New DataView(dtTable, "refno = '" & strRefno & "' AND IsGrp = 0 ", "", DataViewRowState.CurrentRows).ToTable
                            dgTrainingName.DataSource = dtTable.Copy()
                            dgTrainingName.DataBind()
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 Mar 2022) -- End


#End Region

#Region "Button's Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training/Training_Request/wPg_TrainingRequestForm.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpTrainingName.SelectedIndex = 0
            'Hemant (25 Oct 2021) -- Start
            'cboEmployee.SelectedValue = "0"
            cboEmployee.SelectedIndex = 0
            'Hemant (25 Oct 2021) -- End
            drpScheduled.SelectedIndex = 0
            drpStatus.SelectedValue = "1"
            txtTotalTrainingCost.Text = "0"
            cboCondition.SelectedIndex = 0

            mstrAdvanceFilter = ""
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            'dgTrainingName.DataSource = New List(Of String)
            'dgTrainingName.DataBind()
            gvSummary.DataSource = New List(Of String)
            gvSummary.DataBind()
            'Hemant (07 Mar 2022) -- End            

        Catch ex As Exception

            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link Button's Events"

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            If CInt(dgTrainingName.DataKeys(row.RowIndex)("statusunkid")) <> enTrainingRequestStatus.PENDING Then
                DisplayMessage.DisplayMessage("You cannot Edit this Training Request. Reason: This Training Request is in " & dgTrainingName.DataKeys(row.RowIndex)("status") & " status.", Me)
                Exit Sub
            End If

            If CInt(dgTrainingName.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.PENDING Then
                Dim objApprovalProcessTran As New clstrainingapproval_process_tran
                If CBool(objApprovalProcessTran.IsPendingTrainingRequest(mintTrainingRequestunkid)) = False Then
                    DisplayMessage.DisplayMessage("You cannot Edit this Training Request. Reason: It is already in approval process.", Me)
                    Exit Sub
                End If
            End If

            Session("TrainingRequestunkid") = mintTrainingRequestunkid
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training/Training_Request/wPg_TrainingRequestForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            If CInt(dgTrainingName.DataKeys(row.RowIndex)("statusunkid")) <> enTrainingRequestStatus.PENDING Then
                DisplayMessage.DisplayMessage("You cannot Delete this Training Request. Reason: This Training Request is in " & dgTrainingName.DataKeys(row.RowIndex)("status") & " status.", Me)
                Exit Sub
            End If

            If CInt(dgTrainingName.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.PENDING Then
                Dim objApprovalProcessTran As New clstrainingapproval_process_tran
                If CBool(objApprovalProcessTran.IsPendingTrainingRequest(mintTrainingRequestunkid)) = False Then
                    DisplayMessage.DisplayMessage("You cannot Delete this Training Request. Reason: It is already in approval process.", Me)
                    Exit Sub
                End If
            End If

            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            mstrConfirmationAction = "delRequest"
            'Hemant (28 Jul 2021) -- End            
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "emp."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkComplete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkcomplete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkcomplete).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            Session("mblnFromCompleteESS") = True
            Session("TrainingRequestunkid") = mintTrainingRequestunkid
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training/Training_Request/wPg_TrainingRequestForm.aspx", False)
            Exit Sub

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEnroll_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Hemant (28 Jul 2021) -- Start             
        'ENHANCEMENT : OLD-293 - Training Evaluation
        Dim objRequestMaster As New clstraining_request_master
        Dim objEvalQuestion As New clseval_question_master
        Dim objEvaluation As New clstraining_evaluation_tran

        Dim intEmployeeunkid As Integer = -1
        Dim dsSumbitQuestions As DataSet
        Dim dsQuestions As DataSet = Nothing
        Dim blnAllowEnroll As Boolean = False
        'Hemant (28 Jul 2021) -- End        
        Try
            Dim lnkenroll As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkenroll).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation            
            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            intEmployeeunkid = objRequestMaster._Employeeunkid

            dsQuestions = objEvalQuestion.GetList("List", True, , clseval_group_master.enFeedBack.PRETRAINING)
            If CBool(Session("SkipPreTrainingEvaluationProcess")) = False AndAlso dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                'Hemant (05 Jul 2024) -- [CBool(Session("SkipPreTrainingEvaluationProcess")) = False]
                dsSumbitQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.PRETRAINING)
                If dsSumbitQuestions IsNot Nothing AndAlso dsSumbitQuestions.Tables(0).Rows.Count > 0 AndAlso objRequestMaster._IsPreTrainingFeedbackSubmitted = True Then
                    blnAllowEnroll = True
                End If
            Else
                blnAllowEnroll = True
            End If

            If blnAllowEnroll = True OrElse CBool(Session("PreTrainingEvaluationSubmitted")) = True Then
                'Hemant (28 Jul 2021) -- End
                Session("mblnFromEnroll") = True
                Session("TrainingRequestunkid") = mintTrainingRequestunkid
                Session("ReturnURL") = Request.Url.AbsoluteUri
                Response.Redirect(Session("rootpath").ToString & "Training/Training_Request/wPg_TrainingRequestForm.aspx", False)
                'Hemant (28 Jul 2021) -- Start             
                'ENHANCEMENT : OLD-293 - Training Evaluation            
            Else
                'Hemant (18 Aug 2021) -- Start             
                'ENHANCEMENT : OLD-442 - Redirect user to the pretraining feedback form list if he attempts to enroll to course before completing pretraining evaluation form
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You can't Enroll for this training before submitting Pre-Training Evaluation Form for this training."), Me)
                Session("TrainingRequestunkid") = mintTrainingRequestunkid
                Session("FeedBackModeId") = clseval_group_master.enFeedBack.PRETRAINING
                Session("ReturnURL") = Request.Url.AbsoluteUri
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, You can't Enroll for this training before submitting Pre-Training Evaluation Form for this training."), Me, Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx")
                Exit Sub
                'Hemant (18 Aug 2021) -- End
            End If
            'Hemant (28 Jul 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
        Finally
            objRequestMaster = Nothing
            objEvalQuestion = Nothing
            objEvaluation = Nothing
            'Hemant (28 Jul 2021) -- End

        End Try
    End Sub

    'Hemant (28 Jul 2021) -- Start             
    'ENHANCEMENT : OLD-293 - Training Evaluation
    Protected Sub lnkPreTrainingFeedBack_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objRequestMaster As New clstraining_request_master
        Dim objEvaluation As New clstraining_evaluation_tran
        Dim intEmployeeunkid As Integer = -1
        Dim dsQuestions As DataSet
        Try
            Dim lnkenroll As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkenroll).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            intEmployeeunkid = objRequestMaster._Employeeunkid

            dsQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.PRETRAINING)
            If dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                mstrConfirmationAction = "edit_pre-training"
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                'cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Pre-Training Evaluation Form is already Submitted for this training. Are you sure you want to edit this?")
                If objRequestMaster._IsPreTrainingFeedbackSubmitted = True Then
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Pre-Training Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
                Else
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Pre-Training Evaluation Form is already exists. Are you sure you want to edit this?")
                End If
                'Hemant (01 Sep 2021) -- End
                cnfConfirm.Show()
                Exit Sub
            Else
                Session("TrainingRequestunkid") = mintTrainingRequestunkid
                Session("FeedBackModeId") = clseval_group_master.enFeedBack.PRETRAINING
                Session("ReturnURL") = Request.Url.AbsoluteUri
                Response.Redirect(Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx", False)
                Exit Sub
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
            objEvaluation = Nothing
        End Try
    End Sub

    Protected Sub lnkPostTrainingFeedBack_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objRequestMaster As New clstraining_request_master
        Dim objEvaluation As New clstraining_evaluation_tran
        Dim intEmployeeunkid As Integer = -1
        Dim dsQuestions As DataSet
        Try
            Dim lnkenroll As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkenroll).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            intEmployeeunkid = objRequestMaster._Employeeunkid

            dsQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.POSTTRAINING)
            If dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                mstrConfirmationAction = "edit_post-training"
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                'cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Post-Training Evaluation Form is already Submitted for this training. Are you sure you want to edit this?")
                If objRequestMaster._IsPostTrainingFeedbackSubmitted = True Then
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Post-Training Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
                Else
                    cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Post-Training Evaluation Form is already exists. Are you sure you want to edit this?")
                End If
                'Hemant (01 Sep 2021) -- End
                cnfConfirm.Show()
                Exit Sub
            Else
                Session("TrainingRequestunkid") = mintTrainingRequestunkid
                Session("FeedBackModeId") = clseval_group_master.enFeedBack.POSTTRAINING
                Session("ReturnURL") = Request.Url.AbsoluteUri
                Response.Redirect(Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx", False)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
            objEvaluation = Nothing
        End Try
    End Sub

    Protected Sub lnkDayAfterTrainingFeedBack_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objRequestMaster As New clstraining_request_master
        Dim objEvaluation As New clstraining_evaluation_tran
        Dim intEmployeeunkid As Integer = -1
        Dim dsQuestions As DataSet
        Try
            Dim lnkenroll As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkenroll).NamingContainer, GridViewRow)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            'Hemant (07 Mar 2022) -- End
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            intEmployeeunkid = objRequestMaster._Employeeunkid
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            'dsQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.DAYSAFTERTRAINING)
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.DAYSAFTERTRAINING, True, False)
            Else
                dsQuestions = objEvaluation.GetList("List", intEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.DAYSAFTERTRAINING, False, True)
            End If
            'Hemant (20 Aug 2021) -- End

            If dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                mstrConfirmationAction = "edit_day_after_training"
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                'cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Days after training attended Evaluation Form is already Submitted for this training. Are you sure you want to edit this?")
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    If objRequestMaster._IsDaysAfterFeedbackSubmitted = True Then
                        cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Days after training attended Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
                    Else
                        cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Days after training attended Evaluation Form is already exists. Are you sure you want to edit this?")
                    End If
                Else
                    If objRequestMaster._IsDaysAfterLineManagerFeedbackSubmitted = True Then
                        cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Days after training attended Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
                    Else
                        cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Days after training attended Evaluation Form is already exists. Are you sure you want to edit this?")
                    End If
                End If
                'Hemant (01 Sep 2021) -- End
                cnfConfirm.Show()
                Exit Sub
            Else
                Session("TrainingRequestunkid") = mintTrainingRequestunkid
                Session("FeedBackModeId") = clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                Session("ReturnURL") = Request.Url.AbsoluteUri
                Response.Redirect(Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx", False)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
            objEvaluation = Nothing
        End Try
    End Sub
    'Hemant (28 Jul 2021) -- End

    'Hemant (04 Aug 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-719 - Training request form - new report
    Protected Sub lnkTrainingRequestForm_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objRequestMaster As New clstraining_request_master
        Try
            Dim lnkTrainingRequestForm As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkTrainingRequestForm).NamingContainer, GridViewRow)
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            mintTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("trainingrequestunkid"))

            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid

            SetDateFormat()

            Dim objTrainingRequestFormRpt As New ArutiReports.clsTrainingRequestFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objTrainingRequestFormRpt._RequestTypeId = objRequestMaster._InsertFormId
            objTrainingRequestFormRpt._RequesterId = 0
            objTrainingRequestFormRpt._EmployeeUnkid = objRequestMaster._Employeeunkid
            objTrainingRequestFormRpt._TrainingTypeId = objRequestMaster._TrainingTypeId
            objTrainingRequestFormRpt._TrainingUnkid = objRequestMaster._Coursemasterunkid
            objTrainingRequestFormRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objTrainingRequestFormRpt._UserUnkId = CInt(Session("UserId"))
            objTrainingRequestFormRpt._TrainingRequestunkid = mintTrainingRequestunkid
            GUI.fmtCurrency = Session("fmtCurrency").ToString

            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                Dim strUserName As String = String.Empty
                objUser._Userunkid = CInt(Session("UserId"))
                strUserName = objUser._Firstname & " " & objUser._Lastname
                If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
                objTrainingRequestFormRpt._User = strUserName
                objUser = Nothing
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objTrainingRequestFormRpt._User = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
                objEmp = Nothing
            End If

            objTrainingRequestFormRpt.generateReportNew(CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              Session("UserAccessModeSetting").ToString, True, _
                                              Session("ExportReportPath").ToString, _
                                              CBool(Session("OpenAfterExport")), _
                                              0, enPrintAction.None, enExportAction.None, _
                                              CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objTrainingRequestFormRpt._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            objTrainingRequestFormRpt = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub
    'Hemant (04 Aug 2022) -- End

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELREQUEST"
                    'Hemant (28 Jul 2021) -- End
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Enter the Reason for deleting this training request")
                    delReason.Show()
                    'Hemant (28 Jul 2021) -- Start             
                    'ENHANCEMENT : OLD-293 - Training Evaluation            
                Case "EDIT_PRE-TRAINING"
                    Session("TrainingRequestunkid") = mintTrainingRequestunkid
                    Session("FeedBackModeId") = clseval_group_master.enFeedBack.PRETRAINING
                    Session("ReturnURL") = Request.Url.AbsoluteUri
                    Response.Redirect(Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx", False)
                Case "EDIT_POST-TRAINING"
                    Session("TrainingRequestunkid") = mintTrainingRequestunkid
                    Session("FeedBackModeId") = clseval_group_master.enFeedBack.POSTTRAINING
                    Session("ReturnURL") = Request.Url.AbsoluteUri
                    Response.Redirect(Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx", False)
                Case "EDIT_DAY_AFTER_TRAINING"
                    Session("TrainingRequestunkid") = mintTrainingRequestunkid
                    Session("FeedBackModeId") = clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                    Session("ReturnURL") = Request.Url.AbsoluteUri
                    Response.Redirect(Session("rootpath").ToString & "Training/Training_Evalution/wPg_Training_Evalution_Form.aspx", False)
            End Select
            'Hemant (28 Jul 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Delete Reason"

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Dim objRequestMaster As New clstraining_request_master
        Try
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation            
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELREQUEST"
                    'Hemant (28 Jul 2021) -- End
                    SetValue(objRequestMaster)
                    objRequestMaster._Isvoid = True
                    objRequestMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objRequestMaster._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objRequestMaster._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                    End If
                    objRequestMaster._Voidreason = delReason.Reason
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        objRequestMaster._Userunkid = CInt(Session("UserId"))
                        objRequestMaster._LoginEmployeeunkid = -1
                    Else
                        objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                        objRequestMaster._Userunkid = -1
                    End If
                    blnFlag = objRequestMaster.Delete(mintTrainingRequestunkid)
                    If blnFlag = False AndAlso objRequestMaster._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objRequestMaster._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Training Request deleted successfully"), Me)
                        FillList()
                    End If
                    objRequestMaster = Nothing
                    'Hemant (28 Jul 2021) -- Start             
                    'ENHANCEMENT : OLD-293 - Training Evaluation
            End Select
            'Hemant (28 Jul 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetailHeader.ID, lblDetailHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingName.ID, lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEmployee.ID, lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStatus.ID, lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblScheduled.ID, lblScheduled.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCompletionStatus.ID, lblCompletionStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblHeader.ID, lblHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRequestsGrandList.ID, lblRequestsGrandList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprovedGrandList.ID, lblApprovedGrandList.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnNew.ID, btnNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, BtnSearch.ID, BtnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, BtnReset.ID, BtnReset.Text)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(4).FooterText, dgTrainingName.Columns(4).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(5).FooterText, dgTrainingName.Columns(5).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(6).FooterText, dgTrainingName.Columns(6).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(7).FooterText, dgTrainingName.Columns(7).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(8).FooterText, dgTrainingName.Columns(8).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(9).FooterText, dgTrainingName.Columns(9).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(10).FooterText, dgTrainingName.Columns(10).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(11).FooterText, dgTrainingName.Columns(11).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgTrainingName.Columns(12).FooterText, dgTrainingName.Columns(12).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetailHeader.ID, lblDetailHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingName.ID, lblTrainingName.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEmployee.ID, lblEmployee.Text)
            Me.lblTotalTrainingCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStatus.ID, lblStatus.Text)
            Me.lblScheduled.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblScheduled.ID, lblScheduled.Text)
            Me.lblCompletionStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCompletionStatus.ID, lblCompletionStatus.Text)
            Me.lblHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblHeader.ID, lblHeader.Text)
            Me.lblRequestsGrandList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRequestsGrandList.ID, lblRequestsGrandList.Text)
            Me.lblApprovedGrandList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprovedGrandList.ID, lblApprovedGrandList.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnNew.ID, btnNew.Text).Replace("&", "")
            Me.BtnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), BtnSearch.ID, BtnSearch.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), BtnReset.ID, BtnReset.Text).Replace("&", "")

            'dgTrainingName.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(4).FooterText, dgTrainingName.Columns(4).HeaderText)
            'dgTrainingName.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(5).FooterText, dgTrainingName.Columns(5).HeaderText)
            'dgTrainingName.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(6).FooterText, dgTrainingName.Columns(6).HeaderText)
            'dgTrainingName.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(7).FooterText, dgTrainingName.Columns(7).HeaderText)
            'dgTrainingName.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(8).FooterText, dgTrainingName.Columns(8).HeaderText)
            'dgTrainingName.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(9).FooterText, dgTrainingName.Columns(9).HeaderText)
            'dgTrainingName.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(10).FooterText, dgTrainingName.Columns(10).HeaderText)
            'dgTrainingName.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(11).FooterText, dgTrainingName.Columns(11).HeaderText)
            'dgTrainingName.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgTrainingName.Columns(12).FooterText, dgTrainingName.Columns(12).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Training Request deleted successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Enter the Reason for deleting this training request")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Edit")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Delete")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Are you sure you want to delete?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "PreTraining FeedBack")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Post Training FeedBack")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Days After Training FeedBack")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, You can't Enroll for this training before submitting Pre-Training Evaluation Form for this training.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Pre-Training Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Pre-Training Evaluation Form is already exists. Are you sure you want to edit this?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Post-Training Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Post-Training Evaluation Form is already exists. Are you sure you want to edit this?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Days after training attended Evaluation Form is already submitted for this training and changes cannot be done. Do you want to view the submitted form?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Days after training attended Evaluation Form is already exists. Are you sure you want to edit this?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
