﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class Training_Group_Training_Request_wPg_GroupTrainingRequest
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmGroupTrainingRequest"
    Private mintTrainingRequestunkid As Integer
    Private mintCourseMasterunkid As Integer
    Private mintDepartTrainingNeedId As Integer = -1
    Private mintTrainingNeedAllocationID As Integer = -1
    Private mblnShowTrainingNamePopup As Boolean = False
    Private mintDepartmentListId As Integer = -1
    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Private mintNoOfStaff As Integer
    Private mintNoOfStaffSelected As Integer
    'Hemant (09 Feb 2022) -- End
    Private mstrAdvanceFilter As String = "" 'Sohail (08 Mar 2022)
    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
    Private mintTrainingBudgetAllocationID As Integer = -1
    'Hemant (07 Mar 2022) -- End
    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Private mintGroupTrainingRequestunkid As Integer = -1
    Private mdtTrainingRequestDocument As DataTable
    Private objDocument As New clsScan_Attach_Documents
    'Hemant (12 Oct 2022) -- End
    'Hemant (02 Aug 2024) -- Start
    'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
    Private mstrFinancialSourceIds As String = ""
    Private mstrAllocationTranUnkIDs As String = ""
    'Hemant (02 Aug 2024) -- End

#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                dtpApplicationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                mintTrainingNeedAllocationID = CInt(Session("TrainingNeedAllocationID"))
                FillCombo()
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                'FillTrainingCostItem()
                'FillFinancingSource()
                'Hemant (02 Aug 2024) -- End
                Clear_Controls()
                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
                mintTrainingBudgetAllocationID = CInt(Session("TrainingBudgetAllocationID"))
                'Hemant (07 Mar 2022) -- End

                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                If Session("GroupTrainingRequestunkid") IsNot Nothing Then
                    mintGroupTrainingRequestunkid = CInt(Session("GroupTrainingRequestunkid"))
                    Session("GroupTrainingRequestunkid") = Nothing
                    Call GetValue(mintGroupTrainingRequestunkid)
                    cboTrainingName.Enabled = False
                End If
                FillTrainingCostItem()
                FillFinancingSource()
                'Hemant (02 Aug 2024) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                If IsNothing(ViewState("mdtTrainingRequestDocument")) = False Then
                    mdtTrainingRequestDocument = CType(ViewState("mdtTrainingRequestDocument"), DataTable).Copy()
                Else
                    mdtTrainingRequestDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.GROUP_TRAINING_REQUEST, mintGroupTrainingRequestunkid, CStr(Session("Document_Path")))
                    If mintGroupTrainingRequestunkid <= 0 Then
                        If mdtTrainingRequestDocument IsNot Nothing Then mdtTrainingRequestDocument.Rows.Clear()
                    End If
                End If
                Call FillTrainingRequestAttachment()
                'Hemant (12 Oct 2022) -- End

            Else
                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintCourseMasterunkid = CInt(ViewState("mintCourseMasterunkid"))
                mblnShowTrainingNamePopup = CBool(ViewState("mblnShowTrainingNamePopup"))
                mintDepartmentListId = CInt(ViewState("mintDepartmentListId"))
                mintTrainingNeedAllocationID = CInt(Me.ViewState("mintTrainingNeedAllocationID"))
                mintDepartTrainingNeedId = CInt(Me.ViewState("mintDepartTrainingNeedId"))
                'Hemant (09 Feb 2022) -- Start            
                'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
                mintNoOfStaff = CInt(ViewState("mintNoOfStaff"))
                mintNoOfStaffSelected = CInt(ViewState("mintNoOfStaffSelected"))
                'Hemant (09 Feb 2022) -- End
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter")) 'Sohail (08 Mar 2022)
                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
                mintTrainingBudgetAllocationID = CInt(Me.ViewState("mintTrainingBudgetAllocationID"))
                'Hemant (07 Mar 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                mintGroupTrainingRequestunkid = CInt(ViewState("mintGroupTrainingRequestunkid"))
                mdtTrainingRequestDocument = CType(ViewState("mdtTrainingRequestDocument"), DataTable)
                'Hemant (12 Oct 2022) -- End
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                mstrFinancialSourceIds = CStr(ViewState("mstrFinancialSourceIds"))
                mstrAllocationTranUnkIDs = CStr(ViewState("mstrAllocationTranUnkIDs"))
                'Hemant (02 Aug 2024) -- End
            End If

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            If Request.QueryString("uploadimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If
            'Hemant (12 Oct 2022) -- End

            If mblnShowTrainingNamePopup = True Then
                popupTrainingName.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            Me.ViewState("mintCourseMasterunkid") = mintCourseMasterunkid
            Me.ViewState("mblnShowTrainingCostItemPopup") = mblnShowTrainingNamePopup
            Me.ViewState("mintDepartmentListId") = mintDepartmentListId
            Me.ViewState("mintTrainingNeedAllocationID") = mintTrainingNeedAllocationID
            Me.ViewState("mintDepartTrainingNeedId") = mintDepartTrainingNeedId
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            Me.ViewState("mintNoOfStaff") = mintNoOfStaff
            Me.ViewState("mintNoOfStaffSelected") = mintNoOfStaffSelected
            'Hemant (09 Feb 2022) -- End
            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter) 'Sohail (08 Mar 2022)
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
            ViewState("mintTrainingBudgetAllocationID") = mintTrainingBudgetAllocationID
            'Hemant (07 Mar 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            ViewState("mintGroupTrainingRequestunkid") = mintGroupTrainingRequestunkid
            ViewState("mdtTrainingRequestDocument") = mdtTrainingRequestDocument
            'Hemant (12 Oct 2022) -- End
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            ViewState("mstrFinancialSourceIds") = mstrFinancialSourceIds
            ViewState("mstrAllocationTranUnkIDs") = mstrAllocationTranUnkIDs
            'Hemant (02 Aug 2024) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objInstitute As New clsinstitute_master
        Dim objMaster As New clsMasterData
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
        Dim objCommon As New clsCommon_Master
        'Hemant (12 Oct 2022) -- End
        Dim dsCombo As DataSet = Nothing
        Try
            Dim intFirstOpen As Integer = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                intFirstOpen = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If

            dsCombo = objTPeriod.getListForCombo("List", True, 1)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen.ToString
            End With
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            cboPeriod_SelectedIndexChanged(Nothing, Nothing)
            'Hemant (09 Feb 2022) -- End

            dsCombo = objInstitute.getListForCombo(False, "List", True)
            With cboTrainingProvider
                .DataTextField = "name"
                .DataValueField = "instituteunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Call FillTrainingVenueCombo(0)

            Dim strIDs As String = "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Trim <> "" Then
                Dim arr() As String = Session("Allocation_Hierarchy").ToString.Split(CChar("|"))
                Dim sIDs As String = String.Join(",", (From p In arr.AsEnumerable Where (CInt(p) >= mintTrainingNeedAllocationID) Select (p.ToString)).ToArray)
                If sIDs.Trim <> "" Then
                    strIDs = "0, 9, 10, " & sIDs
                End If
            End If
            dsCombo = objMaster.GetTrainingTargetedGroup("List", strIDs, False, False)

            With cboTargetedGroup
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                If dsCombo.Tables(0).Select("ID = " & mintTrainingNeedAllocationID & " ").Length > 0 Then
                    .SelectedValue = mintTrainingNeedAllocationID.ToString
                Else
                    .SelectedValue = "0"
                End If
            End With

            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            dsCombo = objMaster.getComboListForTrainingType(True, "List", " ID IN ( 0, " & enTrainingTravelType.Local_Travel & ", " & enTrainingTravelType.Foreign_Travel & " ) ")
            With drpTravelType
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            'Hemant (29 Apr 2022) -- End

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
            objInstitute = Nothing
            objMaster = Nothing
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            objCommon = Nothing
            'Hemant (12 Oct 2022) -- End
        End Try
    End Sub

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    Private Sub FillTrainingNameList()
        Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsList As DataSet = Nothing
        Try
            Dim strFilter As String = ""

            strFilter = " AND trdepartmentaltrainingneed_master.periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0)  = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0)  = 0 "
            dsList = objDeptTrainingNeed.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), CInt(Session("TrainingNeedAllocationID")), "Tranining" _
                                                        , False, strFilter, 0, 0)


            If dsList.Tables.Count > 0 Then
                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                'Dim xTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid")
                Dim xTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid", "Department")
                'Hemant (07 Mar 2022) -- End

                For Each r As DataRow In xTable.Rows
                    'Hemant (07 Mar 2022) -- Start            
                    'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                    'r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    r("trainingcoursename") = r("trainingcoursename").ToString() + " - " + r("Department").ToString + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    'Hemant (07 Mar 2022) -- End
                Next

                xTable.Columns("trainingcourseunkid").ColumnName = "masterunkid"
                xTable.Columns("trainingcoursename").ColumnName = "name"

                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                'xTable = New DataView(xTable, "", "trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy
                xTable = New DataView(xTable, "", "name, trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy
                'Hemant (07 Mar 2022) -- End

                Dim drRow As DataRow = xTable.NewRow
                drRow("name") = "Select"
                drRow("departmentaltrainingneedunkid") = -1
                drRow("masterunkid") = 0
                xTable.Rows.InsertAt(drRow, 0)

                With cboTrainingName
                    .DataTextField = "name"
                    .DataValueField = "departmentaltrainingneedunkid"
                    .DataSource = xTable
                    .DataBind()
                    .SelectedValue = "-1"
                End With

            Else
                Dim xTable As New DataTable
                dsList.Tables.Add(xTable)
                dsList.Tables(0).Columns.Add("name", GetType(System.String)).DefaultValue = ""
                dsList.Tables(0).Columns.Add("departmentaltrainingneedunkid", GetType(System.Int32)).DefaultValue = -1
                dsList.Tables(0).Columns.Add("masterunkid", GetType(System.Int32)).DefaultValue = 0

                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("name") = "Select"
                drRow("departmentaltrainingneedunkid") = -1
                drRow("masterunkid") = 0
                dsList.Tables(0).Rows.InsertAt(drRow, 0)

                With cboTrainingName
                    .DataTextField = "name"
                    .DataValueField = "departmentaltrainingneedunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "-1"
                End With

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDeptTrainingNeed = Nothing
            objTCategory = Nothing
        End Try
    End Sub
    'Hemant (09 Feb 2022) -- End


    Private Sub FillDepartmentTrainingInfo()
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim objDepartTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim objCommon As New clsCommon_Master
        Dim objRequestMaster As New clstraining_request_master
        Try
            If mintDepartTrainingNeedId > 0 Then

                objDepartTrainingNeed._Departmentaltrainingneedunkid = mintDepartTrainingNeedId
                'Hemant (03 Dec 2021) -- Start
                'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
                cboPeriod.SelectedValue = CStr(objDepartTrainingNeed._Periodunkid)
                'Hemant (03 Dec 2021) -- End
                FillTrainingVenueCombo(objDepartTrainingNeed._Trainingvenueunkid)
                mintCourseMasterunkid = CInt(objDepartTrainingNeed._Trainingcourseunkid)
                objCommon._Masterunkid = mintCourseMasterunkid
                'Hemant (09 Feb 2022) -- Start            
                'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
                'txtTrainingName.Text = objCommon._Name
                'Hemant (09 Feb 2022) -- End
                'Hemant (05 Jul 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): A1X - 2370 : Training request form: Mandatory start date and end date to be filled by the user. Remove dates that are auto-filled from training plans. 
                'dtpStartDate.SetDate = CDate(objDepartTrainingNeed._Startdate)
                'dtpEndDate.SetDate = CDate(objDepartTrainingNeed._Enddate)
                'Hemant (05 Jul 2024) -- End
                cboTrainingProvider.SelectedValue = CStr(objDepartTrainingNeed._Trainingproviderunkid)
                cboTrainingVenue.SelectedValue = CStr(objDepartTrainingNeed._Trainingvenueunkid)

                FillTrainingCostItem()
                txtTotalTrainingCost.Text = Format(CDec(objDepartTrainingNeed._Totalcost), CStr(Session("fmtCurrency")))
                'Hemant (09 Feb 2022) -- Start            
                'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
                mintNoOfStaff = CInt(objDepartTrainingNeed._Noofstaff)
                'Hemant (09 Feb 2022) -- End

                Call FillFinancingSource()
                mintDepartmentListId = CInt(objDepartTrainingNeed._Departmentunkid)
                cboTargetedGroup.SelectedValue = CStr(objDepartTrainingNeed._Targetedgroupunkid)
                txtNoOfStaff.Decimal_ = objDepartTrainingNeed._Noofstaff
                Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDFsource = Nothing
            objDepartTrainingNeed = Nothing
            objTCostItem = Nothing
            objRequestMaster = Nothing
        End Try
    End Sub

    Private Sub FillTrainingVenueCombo(ByVal intVenueId As Integer)
        Dim objTVenue As New clstrtrainingvenue_master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsCombo = objTVenue.getListForCombo("List", True)
            If intVenueId <= 0 Then
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 OR (islocked = 1 AND venueunkid = " & intVenueId & " ) ", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboTrainingVenue
                .DataTextField = "name"
                .DataValueField = "venueunkid"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTVenue = Nothing
        End Try
    End Sub

    Private Sub FillTrainingCostItem()
        Dim objTItem As New clstrainingitemsInfo_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim objTrainingCost As New clstraining_request_cost_tran
        'Hemant (02 Aug 2024) -- Start
        'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
        Dim objGCostItem As New clsgrouptraining_request_cost_tran
        'Hemant (02 Aug 2024) -- End
        Dim dsTCostItem As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim dsList As DataSet
        Dim mblnblank As Boolean = False
        Try

            dsList = objTItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            Dim dtCol As New DataColumn
            dtCol = New DataColumn
            dtCol.ColumnName = "trainingrequestcosttranunkid"
            dtCol.Caption = "trainingrequestcosttranunkid"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = -1
            dsList.Tables(0).Columns.Add(dtCol)

            dtCol = New DataColumn
            dtCol.ColumnName = "Amount"
            dtCol.Caption = "Amount"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingCostItem.DataSource = dsList
                dgvTrainingCostItem.DataBind()

                Dim intCellCount As Integer = dgvTrainingCostItem.Rows(0).Cells.Count

                dgvTrainingCostItem.Rows(0).Cells(0).Visible = False
                dgvTrainingCostItem.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingCostItem.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingCostItem.Rows(0).Cells(1).Text = "No Records Found"
            Else
                If mintDepartTrainingNeedId > 0 Then
                    dsTCostItem = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)
                    For Each drRow As DataRow In dsTCostItem.Tables(0).Rows
                        drRow("Amount") = 0
                    Next
                    dsTCostItem.AcceptChanges()
                    'Hemant (02 Aug 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                    If mintGroupTrainingRequestunkid > 0 Then
                        dsTCostItem = objGCostItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintGroupTrainingRequestunkid)
                    Else
                        'Hemant (02 Aug 2024) -- End
                    Dim dsDeptCostItem As DataSet = Nothing
                    dsDeptCostItem = objTCostItem.GetList("List", mintDepartTrainingNeedId)
                    dsDeptCostItem.Tables(0).Columns("costitemunkid").ColumnName = "infounkid"
                    Dim strDeptCostIDs As String = String.Join(",", (From p In dsDeptCostItem.Tables(0) Select (p.Item("infounkid").ToString)).ToArray)
                    If strDeptCostIDs.Trim <> "" Then
                        Dim row As List(Of DataRow) = (From p In dsTCostItem.Tables(0) Where (strDeptCostIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                        For Each dtRow As DataRow In row
                            Dim r() As DataRow = dsDeptCostItem.Tables(0).Select("infounkid = " & CInt(dtRow.Item("infounkid")) & " ")
                            If r.Length > 0 Then
                                dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), CStr(Session("fmtCurrency")))
                            Else
                                dtRow.Item("Amount") = 0
                            End If
                        Next
                    Else

                    End If
                    End If 'Hemant (02 Aug 2024)     
                Else
                    dsTCostItem = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)
                End If

                Dim strIDs As String = String.Join(",", (From p In dsTCostItem.Tables(0) Select (p.Item("infounkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        Dim r() As DataRow = dsTCostItem.Tables(0).Select("infounkid = " & CInt(dtRow.Item("infounkid")) & " ")
                        If r.Length > 0 Then
                            dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), CStr(Session("fmtCurrency")))
                            dtRow.Item("trainingrequestcosttranunkid") = CInt(r(0).Item("trainingrequestcosttranunkid"))
                        Else
                            dtRow.Item("Amount") = 0
                        End If
                    Next
                Else

                End If

                dtTable = New DataView(dsList.Tables(0), "", "Amount DESC, info_name", DataViewRowState.CurrentRows).ToTable

                dgvTrainingCostItem.DataSource = dtTable
                dgvTrainingCostItem.DataBind()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingCost = Nothing
            objTCostItem = Nothing
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            objGCostItem = Nothing
            'Hemant (02 Aug 2024) -- End
        End Try
    End Sub

    Private Sub FillFinancingSource()
        Dim objCommon As New clsCommon_Master
        Dim objTFsource As New clstraining_request_financing_sources_tran
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim dsList As DataSet = Nothing
        Dim dsFSource As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, "List", , True)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            If mstrFinancialSourceIds.Trim.Length > 0 Then
                Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (mstrFinancialSourceIds.Split(CChar(",")).Contains(p.Item("masterunkid").ToString) = True) Select (p)).ToList

                For Each dtRow As DataRow In row
                    dtRow.Item("IsChecked") = True
                Next

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvFinancingSource.DataSource = dtTable
                dgvFinancingSource.DataBind()

            Else
                'Hemant (02 Aug 2024) -- End
            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvFinancingSource.DataSource = dsList
                dgvFinancingSource.DataBind()

                Dim intCellCount As Integer = dgvFinancingSource.Rows(0).Cells.Count

                dgvFinancingSource.Rows(0).Cells(0).Visible = False
                dgvFinancingSource.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvFinancingSource.Rows(0).Cells(i).Visible = False
                Next
                dgvFinancingSource.Rows(0).Cells(1).Text = "No Records Found"
            Else
                If mintDepartTrainingNeedId > 0 Then
                    dsFSource = objDFsource.GetList("List", mintDepartTrainingNeedId)
                    If dsFSource.Tables(0).Columns.Contains("trainingrequestfinancingsourcestranunkid") = False Then
                        dsFSource.Tables(0).Columns.Add("trainingrequestfinancingsourcestranunkid", GetType(System.Int32)).DefaultValue = -1
                    End If
                Else
                    dsFSource = objTFsource.GetList("List", mintTrainingRequestunkid)
                End If

                Dim strIDs As String = String.Join(",", (From p In dsFSource.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvFinancingSource.DataSource = dtTable
                dgvFinancingSource.DataBind()
            End If
            End If  'Hemant (02 Aug 2024)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTFsource = Nothing
            objDFsource = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub FillTargetedGroup()
        Dim objDTAllocation As New clsDepttrainingneed_allocation_Tran
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim dsTargetGroup As DataSet = Nothing
        Dim dsRequestedEmp As DataSet = Nothing
        Dim intColType As Integer = 0
        Try

            'Hemant (30 Aug 2024) -- Start
            'If dtpStartDate.GetDate = Nothing OrElse dtpEndDate.GetDate = Nothing Then
            '    dgvEmployee.DataSource = Nothing
            '    dgvEmployee.DataBind()
            '    Exit Try
            'End If
            'Hemant (30 Aug 2024) -- End


            Select Case CInt(cboTargetedGroup.SelectedValue)

                Case 0 'Employee Names
                    Dim objEmp As New clsEmployee_Master
                    dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , mintDepartmentListId)

                    intColType = 5

                    dtTable = dsList.Tables(0)
                    objEmp = Nothing

                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " stationunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " deptgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " departmentunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION_GROUP

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " sectiongroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " sectionunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT_GROUP

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " unitgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " unitunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " teamunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOB_GROUP

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " jobgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOBS

                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " jobunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.CLASS_GROUP

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " classgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.CLASSES

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " classesunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable



                Case enAllocation.COST_CENTER

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2

                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " costcenterunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("code").ColumnName = "Code"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobunkid").ColumnName = "Id"
                dtTable.Columns("Code").ColumnName = "Code"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentercode").ColumnName = "Code"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(2).ColumnName = "Code"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_code").ColumnName = "Code"
                dtTable.Columns("country_name").ColumnName = "Name"
            ElseIf intColType = 5 Then
                dtTable.Columns("employeeunkid").ColumnName = "Id"
                dtTable.Columns("employeecode").ColumnName = "Code"
                dtTable.Columns("employeename").ColumnName = "Name"
            End If

            dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()

                Dim intCellCount As Integer = dgvEmployee.Rows(0).Cells.Count

                dgvEmployee.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvEmployee.Rows(0).Cells(i).Visible = False
                Next
                dgvEmployee.Rows(0).Cells(0).Text = "No Records Found"
            Else

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case 0 'Employee Names
                        'Hemant (02 Aug 2024) -- Start
                        'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                        Dim strSavedEmpIDs As String = String.Empty
                        If mintGroupTrainingRequestunkid > 0 Then
                            Dim objGTEmp As New clsgrouptraining_request_employee_tran
                            Dim dsSavedEmp As DataSet = objGTEmp.GetList(CStr(Session("Database_Name")), _
                                                                    CInt(Session("UserId")), _
                                                                    CInt(Session("Fin_year")), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                                    True, "Emp", True, "", mintGroupTrainingRequestunkid, "")
                            strSavedEmpIDs = String.Join(",", (From p In dsSavedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                            objGTEmp = Nothing

                        End If
                        'Hemant (02 Aug 2024) -- End
                        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
                        dsTargetGroup = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True, "", mintDepartTrainingNeedId)


                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id IN (" & strIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            End If
                            Dim row As List(Of DataRow)

                            'Hemant (02 Aug 2024) -- Start
                            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                            If strSavedEmpIDs.Trim.Length > 0 Then
                                row = (From p In dtTable Where (strSavedEmpIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList
                            Else
                                'Hemant (02 Aug 2024) -- End
                                row = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList
                            End If 'Hemant (02 Aug 2024)
                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                        End If

                        txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text))
                        dsRequestedEmp = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                  eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                                  True, CBool(Session("IsIncludeInactiveEmp")), "Training", mintTrainingNeedAllocationID, , _
                                                                  " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                                  CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
                        'Hemant (07 Mar 2022) -- [mintTrainingNeedAllocationID]
                        Dim strRequestedEmpIDs As String = String.Join(",", (From p In dsRequestedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strRequestedEmpIDs.Trim <> "" Then
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id NOT IN (" & strRequestedEmpIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            Else
                                dtTable = Nothing
                            End If
                            'Hemant (07 Mar 2022) -- Start            
                            'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                            If CInt(txtNoOfStaff.Text) > 0 Then
                                'Hemant (07 Mar 2022) -- End
                                txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text) - CInt(dsRequestedEmp.Tables(0).Rows.Count))
                                'Hemant (07 Mar 2022) -- Start            
                                'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                            Else
                                txtNoOfVacantStaff.Text = txtNoOfStaff.Text
                            End If
                            'Hemant (07 Mar 2022) -- End
                        End If

                    Case Else
                            dsTargetGroup = objDTAllocation.GetList("List", mintDepartTrainingNeedId)

                            Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("allocationtranunkid").ToString)).ToArray)
                            If strIDs.Trim <> "" Then
                                Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList
                            'Hemant (02 Aug 2024) -- Start
                            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                            If mstrAllocationTranUnkIDs.Trim.Length <= 0 Then
                                'Hemant (02 Aug 2024) -- End
                                For Each dtRow As DataRow In row
                                    dtRow.Item("IsChecked") = True
                                Next
                            End If  'Hemant (02 Aug 2024) 
                                Dim xTable As DataTable
                                Dim xRow() As DataRow = Nothing
                                xRow = dtTable.Select("id  IN (" & strIDs & ")")
                                If xRow.Length > 0 Then
                                    xTable = xRow.CopyToDataTable()
                                    dtTable = xTable
                                Else
                                    dtTable = Nothing
                                End If
                            End If
                        'Hemant (02 Aug 2024) -- Start
                        'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                        If mstrAllocationTranUnkIDs.Trim.Length > 0 Then
                            Dim row As List(Of DataRow) = (From p In dtTable Where (mstrAllocationTranUnkIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList
                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                        End If
                        'Hemant (02 Aug 2024) -- End

                End Select

                If dtTable IsNot Nothing Then
                    dtTable = New DataView(dtTable, "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable
                End If
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()
                If dtTable IsNot Nothing AndAlso dtTable.Select("IsChecked = 1 ").Length = dgvEmployee.Rows.Count Then
                    CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
                End If

                Call FillAllocEmployee()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDTAllocation = Nothing
            objTrainingRequest = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub FillAllocEmployee()
        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
        Dim objTrainingRequest As New clstraining_request_master
        'Hemant (02 Aug 2024) -- Start
        'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
        Dim objGTEmp As New clsgrouptraining_request_employee_tran
        'Hemant (02 Aug 2024) -- End
        Dim dsList As DataSet = Nothing
        Dim dsAllocEmp As DataSet = Nothing
        Dim dsRequestedEmp As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim intColType As Integer = 0
        Dim strFilter As String = ""
        Dim strIDs As String = ""
        Try
            'chkChooseEmployee.Checked = False
            'Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)

            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If


            'Hemant (30 Aug 2024) -- Start
            'If dtpStartDate.GetDate.Date = Nothing OrElse dtpEndDate.GetDate.Date = Nothing Then
            '    dgvAllocEmp.DataSource = Nothing
            '    dgvAllocEmp.DataBind()
            '    Exit Try
            'End If
            'Hemant (30 Aug 2024) -- End


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If


            strIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

            If strIDs.Trim <> "" Then

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case enAllocation.BRANCH
                        strFilter &= " AND T.stationunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT_GROUP
                        strFilter &= " AND T.deptgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT
                        strFilter &= " AND T.departmentunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION_GROUP
                        strFilter &= " AND T.sectiongroupunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION
                        strFilter &= " AND T.sectionunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT_GROUP
                        strFilter &= " AND T.unitgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT
                        strFilter &= " AND T.unitunkid IN (" & strIDs & ") "

                    Case enAllocation.TEAM
                        strFilter &= " AND T.teamunkid IN (" & strIDs & ") "

                    Case enAllocation.JOB_GROUP
                        strFilter &= " AND J.jobgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.JOBS
                        strFilter &= " AND J.jobunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASS_GROUP
                        strFilter &= " AND T.classgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASSES
                        strFilter &= " AND T.classunkid IN (" & strIDs & ") "

                    Case enAllocation.COST_CENTER
                        strFilter &= " AND C.costcenterunkid IN (" & strIDs & ") "

                End Select

                'Sohail (08 Mar 2022) -- Start
                'Enhancement :  : NMB - On group training request screen, provide an allocation link to help the user narrow down to smaller units when doing the training requests.
                If mstrAdvanceFilter.Trim <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                End If
                'Sohail (08 Mar 2022) -- End

            Else
                strFilter &= " AND 1 = 2 "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , 0, strAdvanceFilterQuery:=strFilter)


            dtTable = dsList.Tables(0)
            objEmp = Nothing

            'dtTable = New DataView(dtTable, "", "employeename", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()

                Dim intCellCount As Integer = dgvAllocEmp.Rows(0).Cells.Count

                dgvAllocEmp.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvAllocEmp.Rows(0).Cells(i).Visible = False
                Next
                dgvAllocEmp.Rows(0).Cells(0).Text = "No Records Found"
            Else
                If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                    strFilter &= " AND 1 = 2 "
                End If
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                Dim strSavedEmpIDs As String = String.Empty
                If mintGroupTrainingRequestunkid > 0 Then
                    Dim dsSavedEmp As DataSet = objGTEmp.GetList(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            True, "Emp", True, "", mintGroupTrainingRequestunkid, "")
                    strSavedEmpIDs = String.Join(",", (From p In dsSavedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)

                End If
                'Hemant (02 Aug 2024) -- End
                dsAllocEmp = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True, "", mintDepartTrainingNeedId, "")



                Dim strExistIDs As String = String.Join(",", (From p In dsAllocEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strExistIDs.Trim <> "" OrElse strSavedEmpIDs.Trim <> "" Then
                    'Hemant (02 Aug 2024) -- [strSavedEmpIDs]
                    Dim row As List(Of DataRow)
                    'Hemant (02 Aug 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                    If strSavedEmpIDs.Trim.Length > 0 Then
                        row = (From p In dtTable Where (strSavedEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = True) Select (p)).ToList
                    Else
                        'Hemant (02 Aug 2024) -- End
                        row = (From p In dtTable Where (strExistIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = True) Select (p)).ToList
                    End If  'Hemant (02 Aug 2024)

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                    If row.Count > 0 Then
                        'chkChooseEmployee.Checked = True
                        'Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)
                    End If
                End If

                txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text))
                dsRequestedEmp = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                          eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                          True, CBool(Session("IsIncludeInactiveEmp")), "Training", mintTrainingNeedAllocationID, , _
                                                          " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
                'Hemant (07 Mar 2022) -- [mintTrainingNeedAllocationID]
                Dim strRequestedEmpIDs As String = String.Join(",", (From p In dsRequestedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strRequestedEmpIDs.Trim <> "" Then
                    Dim xTable As DataTable
                    Dim xRow() As DataRow = Nothing
                    xRow = dtTable.Select("employeeunkid NOT IN (" & strRequestedEmpIDs & ")")
                    If xRow.Length > 0 Then
                        xTable = xRow.CopyToDataTable()
                        dtTable = xTable
                    Else
                        dtTable = Nothing
                    End If
                    'Hemant (07 Mar 2022) -- Start            
                    'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                    If CInt(txtNoOfStaff.Text) > 0 Then
                        'Hemant (07 Mar 2022) -- End
                        txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text) - CInt(dsRequestedEmp.Tables(0).Rows.Count))
                        'Hemant (07 Mar 2022) -- Start            
                        'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                    Else
                        txtNoOfVacantStaff.Text = txtNoOfStaff.Text
                    End If
                    'Hemant (07 Mar 2022) -- End
                End If

                If dtTable IsNot Nothing Then
                    dtTable = New DataView(dtTable, "", "IsChecked DESC, employeename", DataViewRowState.CurrentRows).ToTable
                End If

                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()
            End If

            'chkAllocEmpOnlyTicked.Checked = False

            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable = Nothing
            objDTEmp = Nothing
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            objGTEmp = Nothing
            'Hemant (02 Aug 2024) -- End
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            mintTrainingRequestunkid = 0
            mintDepartTrainingNeedId = 0
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            mstrFinancialSourceIds = ""
            mstrAllocationTranUnkIDs = ""
            'Hemant (02 Aug 2024) -- End
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'txtTrainingName.Text = ""
            cboTrainingName.SelectedIndex = 0
            'Hemant (09 Feb 2022) -- End
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            cboTrainingProvider.SelectedValue = "0"
            cboTrainingVenue.SelectedValue = "0"
            'FillTrainingCostItem()
            txtTotalTrainingCost.Text = "0.00"
            FillFinancingSource()
            rdbTravellingYes.Checked = False
            rdbTravellingNo.Checked = True
            txtExpectedReturn.Text = ""
            txtRemarks.Text = ""

            cboTargetedGroup.SelectedValue = "0"
            txtNoOfStaff.Decimal_ = 0
            dgvEmployee.DataSource = Nothing
            dgvEmployee.DataBind()

            dgvAllocEmp.DataSource = Nothing
            dgvAllocEmp.DataBind()
            txtNoOfVacantStaff.Decimal_ = 0

            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            drpTravelType.SelectedValue = CStr(0)
            pnlTravelType.Visible = False
            'Hemant (29 Apr 2022) -- End

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            mdtTrainingRequestDocument = Nothing
            dgvQualification.DataSource = mdtTrainingRequestDocument
            dgvQualification.DataBind()
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            txtVenue.Text = ""
            'Hemant (10 Nov 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try
            objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue"), Me)
                Return False
            End If

            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            'If CInt(txtTrainingName.Text.Trim.Length) <= 0 Then
            If CInt(cboTrainingName.SelectedValue) <= 0 Then
                'Hemant (09 Feb 2022) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Training Name is mandatory information. Please select Training Name to continue"), Me)
                Return False
            End If

            If dtpApplicationDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Application date is mandatory information. Please select Application date to continue"), Me)
                Return False
            End If

            If dtpStartDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, the Start date is mandatory information. Please select the start date to continue"), Me)
                Return False
            End If

            If dtpEndDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, the End date is mandatory information. Please select the end date to continue"), Me)
                Return False
            End If

            If dtpStartDate.GetDate.Date < objTPeriod._StartDate OrElse dtpStartDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, Start date should be in between selected period start date and end date"), Me)
                dtpStartDate.Focus()
                Return False
            End If

            If dtpEndDate.GetDate.Date < objTPeriod._StartDate OrElse dtpEndDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, End date should be in between selected period start date and end date"), Me)
                dtpEndDate.Focus()
                Return False
            End If

            If dtpStartDate.GetDate.Date > dtpEndDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Start date should not be greater than end date"), Me)
                dtpEndDate.Focus()
                Return False
            End If

            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            If txtVenue.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Venue is compulsory information. Please enter Venue to continue."), Me)
                txtVenue.Focus()
                Return False
            End If
            'Hemant (10 Nov 2022) -- End

            If CDec(txtTotalTrainingCost.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, Total Training Cost should be greater than Zero"), Me)
                Return False
            End If

            If (rdbTravellingYes.Checked Or rdbTravellingNo.Checked) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, ""Does this Training Require Travelling?"" is mandatory information. Please select Yes/No to continue"), Me)
                Return False
            End If

            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            If rdbTravellingYes.Checked = True AndAlso CInt(drpTravelType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry, Travel Type is mandatory information. Please select Travel Type to continue"), Me)
                Return False
            End If
            'Hemant (29 Apr 2022) -- End

            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
            'Hemant (24 Aug 2022) -- Start            
            'ISSUE(NMB) : ERROR - Attempted to divide by zero
            'If IsValidForMaxBudget(CDec(txtTotalTrainingCost.Text)) = False Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department."), Me)
            '    Return False
            'End If
            'Hemant (24 Aug 2022) -- End
            'Hemant (07 Mar 2022) -- End           

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
        End Try
    End Function

    Private Sub SetValue(ByRef objGroupTRequest As clsgrouptraining_request_master, ByRef objRequestMaster As clstraining_request_master)
        Try
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            objGroupTRequest._GroupTrainingRequestunkid = mintGroupTrainingRequestunkid
            objGroupTRequest._Periodunkid = CInt(cboPeriod.SelectedValue)
            objGroupTRequest._Application_Date = dtpApplicationDate.GetDate
            objGroupTRequest._Coursemasterunkid = mintCourseMasterunkid
            objGroupTRequest._Start_Date = dtpStartDate.GetDate
            objGroupTRequest._End_Date = dtpEndDate.GetDate
            objGroupTRequest._TotalTrainingCost = CDec(txtTotalTrainingCost.Text)
            objGroupTRequest._Trainingproviderunkid = CInt(cboTrainingProvider.SelectedValue)
            objGroupTRequest._Trainingvenueunkid = CInt(cboTrainingVenue.SelectedValue)
            objGroupTRequest._ExpectedReturn = CStr(txtExpectedReturn.Text)
            objGroupTRequest._Remarks = CStr(txtRemarks.Text)
            objGroupTRequest._Targetedgroupunkid = CInt(cboTargetedGroup.SelectedValue)
            objGroupTRequest._Allocationtranunkids = mstrAllocationTranUnkIDs

            If Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
                (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objGroupTRequest._IsSubmitApproval = True
            Else
                objGroupTRequest._IsSubmitApproval = False
            End If
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objGroupTRequest._Userunkid = CInt(Session("UserId"))
                objGroupTRequest._LoginEmployeeunkid = -1
            Else
                objGroupTRequest._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objGroupTRequest._Userunkid = -1
            End If
            objGroupTRequest._IsWeb = True
            objGroupTRequest._Isvoid = False
            objGroupTRequest._Voiddatetime = Nothing
            objGroupTRequest._Voidreason = ""
            objGroupTRequest._Voiduserunkid = -1
            objGroupTRequest._ClientIP = CStr(Session("IP_ADD"))
            objGroupTRequest._FormName = mstrModuleName
            objGroupTRequest._HostName = CStr(Session("HOST_NAME"))
            If mintDepartTrainingNeedId > 0 Then
                objGroupTRequest._DepartmentalTrainingNeedunkid = mintDepartTrainingNeedId
            End If
            objGroupTRequest._Venue = CStr(txtVenue.Text)
            'Hemant (02 Aug 2024) -- End

            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            objRequestMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            objRequestMaster._Application_Date = dtpApplicationDate.GetDate
            'objRequestMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objRequestMaster._Coursemasterunkid = mintCourseMasterunkid

            objRequestMaster._IsScheduled = True
            
            objRequestMaster._Start_Date = dtpStartDate.GetDate
            objRequestMaster._End_Date = dtpEndDate.GetDate
            objRequestMaster._Trainingproviderunkid = CInt(cboTrainingProvider.SelectedValue)
            objRequestMaster._Trainingvenueunkid = CInt(cboTrainingVenue.SelectedValue)
            objRequestMaster._TotalTrainingCost = CDec(txtTotalTrainingCost.Text)
            
            objRequestMaster._IsAlignedCurrentRole = False
            objRequestMaster._IsPartofPDP = False

            If rdbTravellingYes.Checked Then
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                objGroupTRequest._IsTravelling = True
                objGroupTRequest._TrainingTypeId = CInt(drpTravelType.SelectedValue)
                'Hemant (02 Aug 2024) -- End
                objRequestMaster._IsForeignTravelling = True
                'Hemant (29 Apr 2022) -- Start
                'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                objRequestMaster._TrainingTypeId = CInt(drpTravelType.SelectedValue)
                'Hemant (29 Apr 2022) -- End
            Else
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                objGroupTRequest._IsTravelling = False
                objGroupTRequest._TrainingTypeId = CInt(enTrainingTravelType.Local_Training)
                'Hemant (02 Aug 2024) -- End
                objRequestMaster._IsForeignTravelling = False
                'Hemant (29 Apr 2022) -- Start
                'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                objRequestMaster._TrainingTypeId = CInt(enTrainingTravelType.Local_Training)
                'Hemant (29 Apr 2022) -- End
            End If

            objRequestMaster._ExpectedReturn = CStr(txtExpectedReturn.Text)
            objRequestMaster._Remarks = CStr(txtRemarks.Text)
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            If CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
                (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                objRequestMaster._IsSubmitApproval = True
                objRequestMaster._IsSkipTrainingRequestAndApproval = True
            Else
                'Hemant (23 Sep 2021) -- End
                objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
                objRequestMaster._IsSubmitApproval = False
            End If 'Hemant (23 Sep 2021)
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Userunkid = CInt(Session("UserId"))
                objRequestMaster._LoginEmployeeunkid = -1
            Else
                objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objRequestMaster._Userunkid = -1
            End If
            objRequestMaster._IsWeb = True
            objRequestMaster._Isvoid = False
            objRequestMaster._Voiddatetime = Nothing
            objRequestMaster._Voidreason = ""
            objRequestMaster._Voiduserunkid = -1
            objRequestMaster._ClientIP = CStr(Session("IP_ADD"))
            objRequestMaster._FormName = mstrModuleName
            objRequestMaster._HostName = CStr(Session("HOST_NAME"))
            If mintDepartTrainingNeedId > 0 Then
                objRequestMaster._DepartmentalTrainingNeedunkid = mintDepartTrainingNeedId
            End If
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            If CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
               (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._IsEnrollConfirm = True
            Else
                'Hemant (23 Sep 2021) -- End
                objRequestMaster._IsEnrollConfirm = False
            End If 'Hemant (23 Sep 2021)
            objRequestMaster._IsEnrollReject = False
            objRequestMaster._EnrollAmount = 0
         
            objRequestMaster._CompletedStatusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objRequestMaster._TrainingCostEmp = CDec(txtTotalTrainingCost.Text) / mintNoOfStaffSelected
            If mintTrainingRequestunkid <= 0 Then objRequestMaster._InsertFormId = 2
            'Hemant (09 Feb 2022) -- End
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-586 - Validate requested amounts against cost center budget. System should not allow to request more than available funds.
            objRequestMaster._Refno = objRequestMaster.getNextRefNo().ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Createuserunkid = CInt(Session("UserId"))
                objRequestMaster._CreateloginEmployeeunkid = -1
            Else
                objRequestMaster._CreateloginEmployeeunkid = CInt(Session("Employeeunkid"))
                objRequestMaster._Createuserunkid = -1
            End If
            'Hemant (07 Mar 2022) -- End
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
            objRequestMaster._TrainingApprovalSettingID = CInt(Session("TrainingRequestApprovalSettingID"))
            'Hemant (25 Jul 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            'objRequestMaster._GroupTrainingRequestunkid = objRequestMaster.getNextGroupTraningUnkId
            objRequestMaster._GroupTrainingRequestunkid = mintGroupTrainingRequestunkid
            'Hemant (02 Aug 2024) -- End

            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            objRequestMaster._Venue = CStr(txtVenue.Text)
            'Hemant (10 Nov 2022) -- End
           
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Submit_Click()
        Dim objRequestMaster As New clstraining_request_master
        Dim objRequestCost As New clstraining_request_cost_tran
        Dim dtCostTran As DataTable
        'Hemant (02 Aug 2024) -- Start
        'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
        Dim dtOldCostTran As DataTable
        Dim dtFinalCostTran As DataTable
        Dim objGroupTRequestCost As New clsgrouptraining_request_cost_tran
        Dim objGroupTRequest As New clsgrouptraining_request_master

        Dim lstAllocEmp As New List(Of clsgrouptraining_request_employee_tran)
        Dim objAllocEmp As New clsgrouptraining_request_employee_tran
        Dim lstVoidAllocEmp As New List(Of clsgrouptraining_request_employee_tran)


        Dim gNewRow As List(Of GridViewRow) = Nothing
        Dim strEmpIDs As String = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)
        'Hemant (02 Aug 2024) -- End

        Dim strEmployeeList As String = String.Empty
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            Dim allEmp As List(Of String)

            If cboTargetedGroup.SelectedValue = "0" Then 'employee names
                gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee."), Me)
                    Exit Sub
                ElseIf CInt(txtNoOfVacantStaff.Text) > 0 AndAlso gRow.Count > CInt(txtNoOfVacantStaff.Text) Then
                    'Hemant (07 Mar 2022) -- [CInt(txtNoOfVacantStaff.Text) > 0]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s)."), Me)
                    Exit Sub
                End If

                allEmp = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("id").ToString()).ToList
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save

                If mintGroupTrainingRequestunkid > 0 Then

                    Dim dsTargetGroup As DataSet = objAllocEmp.GetList(CStr(Session("Database_Name")), _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               dtpStartDate.GetDate.Date, _
                                                               dtpEndDate.GetDate.Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, _
                                                               True, "Emp", True, "", mintGroupTrainingRequestunkid)

                    Dim drVoid As List(Of DataRow) = (From p In dsTargetGroup.Tables(0) Where (strEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = False) Select (p)).ToList
                    Dim strExistIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                    gNewRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString) = False).Select(Function(x) x).ToList

                    For Each r As DataRow In drVoid
                        objAllocEmp = New clsgrouptraining_request_employee_tran

                        With objAllocEmp
                            .pintGroupTrainingRequestEmployeeTranunkid = CInt(r.Item("grouptrainingrequestemployeetranunkid"))

                            .pblnIsvoid = True
                            .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 78, "Wrongly Posted.")

                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                .pintVoiduserunkid = CInt(Session("UserId"))
                                .pintVoidloginemployeeunkid = 0
                                .pintAuditUserId = CInt(Session("UserId"))
                            Else
                                .pintVoiduserunkid = 0
                                .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                .pintAuditUserId = CInt(Session("Employeeunkid"))
                            End If
                            .pblnIsweb = True
                            .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                            .pstrClientIp = Session("IP_ADD").ToString()
                            .pstrHostName = Session("HOST_NAME").ToString()
                            .pstrFormName = mstrModuleName
                        End With

                        lstVoidAllocEmp.Add(objAllocEmp)
                    Next

                    gRow = gNewRow

                End If


                For Each dgRow As GridViewRow In gRow
                    objAllocEmp = New clsgrouptraining_request_employee_tran

                    With objAllocEmp
                        .pintGroupTrainingRequestEmployeeTranunkid = -1
                        .pintGroupTrainingRequestunkid = mintGroupTrainingRequestunkid
                        .pintEmployeeunkid = CInt(dgvEmployee.DataKeys(dgRow.RowIndex)("id").ToString())

                        .pblnIsweb = True
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            .pintUserunkid = CInt(Session("UserId"))
                            .pintLoginemployeeunkid = 0
                            .pintAuditUserId = CInt(Session("UserId"))
                        Else
                            .pintUserunkid = 0
                            .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                            .pintAuditUserId = CInt(Session("Employeeunkid"))
                        End If
                        .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                        .pstrClientIp = Session("IP_ADD").ToString()
                        .pstrHostName = Session("HOST_NAME").ToString()
                        .pstrFormName = mstrModuleName
                    End With

                    lstAllocEmp.Add(objAllocEmp)

                Next
                'Hemant (02 Aug 2024) -- End

            Else
                gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee."), Me)
                    Exit Sub
                ElseIf CInt(txtNoOfVacantStaff.Text) > 0 AndAlso gRow.Count > CInt(txtNoOfVacantStaff.Text) Then
                    'Hemant (07 Mar 2022) -- [CInt(txtNoOfVacantStaff.Text) > 0]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s)."), Me)
                    Exit Sub
                End If

                allEmp = gRow.AsEnumerable().Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList

                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                mstrAllocationTranUnkIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

                '*** Training Employee ***
                gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True).ToList
                strEmpIDs = String.Join(",", dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True).Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString).ToArray)

                If (gRow IsNot Nothing AndAlso gRow.Count > 0) OrElse mintGroupTrainingRequestunkid > 0 Then

                    If mintGroupTrainingRequestunkid > 0 Then

                        Dim dsAllocEmp As DataSet = objAllocEmp.GetList(CStr(Session("Database_Name")), _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               dtpStartDate.GetDate.Date, _
                                                               dtpEndDate.GetDate.Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, _
                                                               True, "Emp", True, "", mintGroupTrainingRequestunkid)

                        Dim drVoid As List(Of DataRow) = (From p In dsAllocEmp.Tables(0) Where (strEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = False) Select (p)).ToList
                        Dim strExistIDs As String = String.Join(",", (From p In dsAllocEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        gNewRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True AndAlso strExistIDs.Split(CChar(",")).Contains(dgvAllocEmp.DataKeys(x.RowIndex).Item("employeeunkid").ToString) = False).Select(Function(x) x).ToList

                        For Each r As DataRow In drVoid
                            objAllocEmp = New clsgrouptraining_request_employee_tran

                            With objAllocEmp
                                .pintGroupTrainingRequestEmployeeTranunkid = CInt(r.Item("grouptrainingrequestemployeetranunkid"))

                                .pblnIsvoid = True
                                .pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                .pstrVoidreason = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 78, "Wrongly Posted.")

                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                    .pintVoiduserunkid = CInt(Session("UserId"))
                                    .pintVoidloginemployeeunkid = 0
                                    .pintAuditUserId = CInt(Session("UserId"))
                                Else
                                    .pintVoiduserunkid = 0
                                    .pintVoidloginemployeeunkid = CInt(Session("Employeeunkid"))
                                    .pintAuditUserId = CInt(Session("Employeeunkid"))
            End If
                                .pblnIsweb = True
                                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                .pstrClientIp = Session("IP_ADD").ToString()
                                .pstrHostName = Session("HOST_NAME").ToString()
                                .pstrFormName = mstrModuleName
                            End With

                            lstVoidAllocEmp.Add(objAllocEmp)
                        Next

                        gRow = gNewRow

                    End If

                    If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                        For Each dgRow As GridViewRow In gRow
                            objAllocEmp = New clsgrouptraining_request_employee_tran

                            With objAllocEmp
                                .pintGroupTrainingRequestEmployeeTranunkid = -1
                                .pintGroupTrainingRequestunkid = mintGroupTrainingRequestunkid
                                .pintEmployeeunkid = CInt(dgvAllocEmp.DataKeys(dgRow.RowIndex)("employeeunkid").ToString())

                                .pblnIsweb = True
                                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                                    .pintUserunkid = CInt(Session("UserId"))
                                    .pintLoginemployeeunkid = 0
                                    .pintAuditUserId = CInt(Session("UserId"))
                                Else
                                    .pintUserunkid = 0
                                    .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                                    .pintAuditUserId = CInt(Session("Employeeunkid"))
                                End If
                                .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                                .pstrClientIp = Session("IP_ADD").ToString()
                                .pstrHostName = Session("HOST_NAME").ToString()
                                .pstrFormName = mstrModuleName
                            End With

                            lstAllocEmp.Add(objAllocEmp)

                        Next
                    End If
                End If
                'Hemant (02 Aug 2024) -- End

            End If

            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            objGroupTRequest._lstGroupTEmpNew = lstAllocEmp
            objGroupTRequest._lstGroupTEmpVoid = lstVoidAllocEmp
            'Hemant (02 Aug 2024) -- End


            'Hemant (24 Aug 2022) -- Start            
            'ISSUE(NMB) : ERROR - Attempted to divide by zero
            If Me.ViewState("Sender").ToString().ToUpper() = btnSubmit.ID.ToUpper AndAlso IsValidForMaxBudget(CDec(txtTotalTrainingCost.Text)) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department."), Me)
                Exit Sub
            End If
            'Hemant (24 Aug 2022) -- End
            
            strEmployeeList = String.Join(",", allEmp.ToArray)

            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
            mintNoOfStaffSelected = allEmp.Count
            'Hemant (07 Mar 2022) -- End


            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRowAttachment As DataRow In mdtTrainingRequestDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRowAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRowAttachment("AUD").ToString = "A" AndAlso dRowAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRowAttachment("localpath")))
                    If File.Exists(CStr(dRowAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRowAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRowAttachment("fileuniquename") = strFileName
                        dRowAttachment("filepath") = strPath
                        dRowAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                ElseIf dRowAttachment("AUD").ToString = "D" AndAlso dRowAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRowAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                End If

            Next
            'Hemant (12 Oct 2022) -- End

            SetValue(objGroupTRequest, objRequestMaster)
            'Hemant (02 Aug 2024) -- [objGroupTRequest]

            If Me.ViewState("Sender").ToString().ToUpper() = "BTNSUBMIT" Then
                objRequestMaster._IsSubmitApproval = True
            Else
                objRequestMaster._IsSubmitApproval = False
            End If

            '*** Training Cost Item ***
            Dim lstTrainingCost As New List(Of clstraining_request_cost_tran)
            Dim objTrainingCost As New clstraining_request_cost_tran

            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            dtOldCostTran = objGroupTRequestCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintGroupTrainingRequestunkid).Tables(0)
            'Hemant (02 Aug 2024) -- End
            dtCostTran = objRequestCost._TranDataTable
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            dtFinalCostTran = objRequestCost._TranDataTable.Copy()
            'Hemant (02 Aug 2024) -- End            
            gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CDec(CType(x.FindControl("txtamount"), TextBox).Text) > 0).ToList
            For Each dr As GridViewRow In gRow
                Dim drRow As DataRow = dtFinalCostTran.NewRow
                drRow("trainingrequestcosttranunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString())
                drRow("trainingcostitemunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("infounkid").ToString())
                Dim txtamount As TextBox = CType(dr.FindControl("txtamount"), TextBox)
                drRow("amount") = CDec(txtamount.Text)

                drRow("Isvoid") = False
                drRow("Voiddatetime") = DBNull.Value
                drRow("Voidreason") = ""
                drRow("Voiduserunkid") = -1
                drRow("voidloginemployeeunkid") = -1
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objRequestMaster._Userunkid = CInt(Session("UserId"))
                Else
                    objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                End If
                drRow("AUD") = "A"
                dtFinalCostTran.Rows.Add(drRow)
            Next
            dtFinalCostTran.AcceptChanges()

            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow)()
            'Hemant (02 Aug 2024) -- End

            For Each dr As GridViewRow In gRow
                Dim drRow As DataRow = dtCostTran.NewRow
                drRow("trainingrequestcosttranunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString())
                drRow("trainingcostitemunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("infounkid").ToString())
                Dim txtamount As TextBox = CType(dr.FindControl("txtamount"), TextBox)
                drRow("amount") = CDec(txtamount.Text)

                drRow("Isvoid") = False
                drRow("Voiddatetime") = DBNull.Value
                drRow("Voidreason") = ""
                drRow("Voiduserunkid") = -1
                drRow("voidloginemployeeunkid") = -1
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objRequestMaster._Userunkid = CInt(Session("UserId"))
                Else
                    objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                End If
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                If CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) <= 0 AndAlso CDec(txtamount.Text) > 0 Then
                    'Hemant (02 Aug 2024) -- End
                drRow("AUD") = "A"
                dtCostTran.Rows.Add(drRow)
                    'Hemant (02 Aug 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                ElseIf CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) > 0 AndAlso CDec(txtamount.Text) > 0 Then
                    Dim drOld() As DataRow = dtOldCostTran.Select("trainingrequestcosttranunkid = " & CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) & "")
                    If drOld.Length > 0 Then
                        If CDec(drOld(0).Item("amount")) <> CDec(txtamount.Text) Then
                            drRow("AUD") = "U"
                            dtCostTran.Rows.Add(drRow)
                        End If
                    End If
                ElseIf CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) > 0 AndAlso CDec(txtamount.Text) <= 0 Then
                    Dim drOld() As DataRow = dtOldCostTran.Select("trainingrequestcosttranunkid = " & CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString()) & "")
                    If drOld.Length > 0 Then
                        drRow("Isvoid") = True
                        drRow("Voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drRow("Voidreason") = ""
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                            objRequestMaster._Voiduserunkid = CInt(Session("UserId"))
                        Else
                            objRequestMaster._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                        End If
                        drRow("AUD") = "D"
                        dtCostTran.Rows.Add(drRow)
                    End If
                End If
                'Hemant (02 Aug 2024) -- End
            Next
            dtCostTran.AcceptChanges()

            '*** Financial Sources ***
            Dim lstFinancingSources As New List(Of clstraining_request_financing_sources_tran)
            Dim objFinancingSources As New clstraining_request_financing_sources_tran

            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            'Dim gNewRow As List(Of GridViewRow) = Nothing
            'Hemant (02 Aug 2024) -- End

            Dim strIDs As String

            gRow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).ToList
            strIDs = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString).ToArray)

            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            objGroupTRequest._FinancingSourcesTranUnkids = strIDs
            'Hemant (02 Aug 2024) -- End

            For Each dgRow As GridViewRow In gRow
                objFinancingSources = New clstraining_request_financing_sources_tran

                With objFinancingSources
                    .pintTrainingRequestfinancingsourcestranunkid = -1
                    .pintTrainingRequestunkid = mintTrainingRequestunkid
                    .pintFinancingsourceunkid = CInt(dgvFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                    .pblnIsweb = True
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = 0
                    End If
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                lstFinancingSources.Add(objFinancingSources)
            Next

            objRequestMaster._lstFinancingSourceNew = lstFinancingSources

            objRequestMaster._Datasource_TrainingCostItem = dtFinalCostTran
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            dtCostTran.Columns("trainingrequestcosttranunkid").ColumnName = "grouptrainingrequestcosttranunkid"
            objGroupTRequest._Datasource_TrainingCostItem = dtCostTran
            'Hemant (02 Aug 2024) -- End


            If objGroupTRequest.InsertAllByEmployeeList(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       CStr(Session("UserAccessModeSetting")), _
                                                       Session("EmployeeAsOnDate").ToString, _
                                                       strEmployeeList, CInt(Session("TrainingApproverAllocationID")), Nothing, , _
                                                       mdtTrainingRequestDocument, objRequestMaster) = False Then

                'Hemant (02 Aug 2024) -- [objRequestMaster --> objGroupTRequest, objTrainingRequest:= objRequestMaster]
                'Hemant (12 Oct 2022) -- [mdtTrainingRequestDocument]
                'Hemant (09 Feb 2022) -- [CInt(Session("TrainingApproverAllocationID"))]
                DisplayMessage.DisplayMessage(objRequestMaster._Message, Me)
            Else
                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                If objRequestMaster._IsSubmitApproval = True AndAlso _
                 ((CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) OrElse _
                   (CBool(Session("SkipTrainingRequisitionAndApproval")) = False)) Then
                    'Hemant (23 Sep 2021) -- [AndAlso ((CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) OrElse (CBool(Session("SkipTrainingRequisitionAndApproval")) = False) ]
                    Dim dicTrainingRequest As New Dictionary(Of Integer, Integer)
                    Dim dsRequestedEmp As DataSet = objRequestMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                      eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                       True, CBool(Session("IsIncludeInactiveEmp")), "Training", mintTrainingNeedAllocationID, , _
                                                       " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " AND trtraining_request_master.employeeunkid in ( " & strEmployeeList & " ) ", , _
                                                       CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
                    'Hemant (07 Mar 2022) -- [mintTrainingNeedAllocationID]
                    dicTrainingRequest = (From p In dsRequestedEmp.Tables("Training") Select New With {Key .TrainingRequestunkid = CInt(p.Item("trainingrequestunkid")), Key .Employeeunkid = CInt(p.Item("employeeunkid"))}).ToDictionary(Function(x) x.TrainingRequestunkid, Function(y) y.Employeeunkid)

                    'Hemant (10 Nov 2022) -- Start
                    'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                    'objRequestMaster.SendNotificationApproverByEmployeeList(Session("Database_Name").ToString, dicTrainingRequest, _
                    '                                           CInt(IIf(objRequestMaster._MinApprovedPriority > 0, objRequestMaster._MinApprovedPriority, -1)), _
                    '                                           clstraining_request_master.enEmailType.Training_Approver, _
                    '                                           CInt(Session("CompanyUnkId")), _
                    '                                            mintCourseMasterunkid, dtpApplicationDate.GetDate.Date, CInt(Session("Fin_year")), _
                    '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("ArutiSelfServiceURL")), _
                    '                                           dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, CStr(Session("UserAccessModeSetting")), _
                    '                                           enLoginMode, intLoginByEmployeeId, intUserId, , , True, CStr(Session("TrainingFinalApprovedNotificationUserIds")))
                    objRequestMaster.Send_Notification_Approver(Session("Database_Name").ToString, -1, _
                                                                CInt(IIf(objRequestMaster._MinApprovedPriority > 0, objRequestMaster._MinApprovedPriority, -1)), _
                                                                clstraining_request_master.enEmailType.Training_Approver, _
                                                               CInt(Session("CompanyUnkId")), "", _
                                                                 mintCourseMasterunkid, dtpApplicationDate.GetDate.Date, CInt(Session("Fin_year")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("ArutiSelfServiceURL")), _
                                                               dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date, CStr(Session("UserAccessModeSetting")), 2, _
                                                               dicTrainingRequest, enLoginMode, intLoginByEmployeeId, intUserId, , , True, CStr(Session("TrainingFinalApprovedNotificationUserIds")))
                    'Hemant (10 Nov 2022) -- End
                   
                    'Hemant (04 Aug 2022) -- [CStr(Session("TrainingFinalApprovedNotificationUserIds"))]
                    'Hemant (09 Feb 2022) -- [dtpStartDate.GetDate.Date, dtpEndDate.GetDate.Date,blnSendEmailReportingTo := True,txtTrainingName.Text --> mintCourseMasterunkid]
                    'Hemant (16 Nov 2021) --  [Session("Database_Name").ToString]
                End If
                Call Clear_Controls()
                FillTrainingCostItem()
                'Hemant (03 Feb 2023) -- Start
                'ISSUE/EHANCEMENT :  To allow same one course to same employee in multiple periods While Tranining Request
                If IsNothing(ViewState("mdtTrainingRequestDocument")) = False Then
                    mdtTrainingRequestDocument = CType(ViewState("mdtTrainingRequestDocument"), DataTable).Copy()
                    If mdtTrainingRequestDocument IsNot Nothing Then mdtTrainingRequestDocument.Rows.Clear()
                Else
                    mdtTrainingRequestDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.GROUP_TRAINING_REQUEST, mintGroupTrainingRequestunkid, CStr(Session("Document_Path")))
                    If mintGroupTrainingRequestunkid <= 0 Then
                        If mdtTrainingRequestDocument IsNot Nothing Then mdtTrainingRequestDocument.Rows.Clear()
                    End If
                End If
                Call FillTrainingRequestAttachment()
                'Hemant (03 Feb 2023) -- End  
                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : Point#7. Upon submitting both the group and individual training requests, system does not give a prompt on successful submission. Screen just remains silent.
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Information saved successfully."), Me)
                If Request.QueryString.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Training/Group_Training_Request/wPg_GroupTrainingRequestList.aspx")
                End If
                'Hemant (02 Aug 2024) -- End                
                'Hemant (07 Mar 2022) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            objGroupTRequest = Nothing
            'Hemant (02 Aug 2024) -- End
            objRequestMaster = Nothing
            mstrAdvanceFilter = "" 'Sohail (08 Mar 2022)
        End Try
    End Sub

    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
    Public Function IsValidForMaxBudget(ByVal decAmount As Decimal) As Boolean
        Dim lstEmployeeList As List(Of String) = New List(Of String)
        Dim objTrainingRequest As New clstraining_request_master
        Dim objTNeedMaster As New clsDepartmentaltrainingneed_master
        Dim decAmountEmp As Decimal
        Try
            If mintTrainingNeedAllocationID = mintTrainingBudgetAllocationID Then Return True

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            'Hemant (03 Feb 2023) -- Start
            'ISSUE/EHANCEMENT :  To allow same one course to same employee in multiple periods While Tranining Request
            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then
                gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

                If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                    lstEmployeeList = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("id").ToString()).ToList
                End If
            Else
                'Hemant (03 Feb 2023) -- End  
            gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                lstEmployeeList = gRow.AsEnumerable().Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
            End If
            End If 'Hemant (03 Feb 2023)
            

            decAmountEmp = decAmount / lstEmployeeList.Count

            Dim dtEmployeeAllocationData, dtSummaryData As DataTable
            dtEmployeeAllocationData = objTrainingRequest.GetTrainingRequestedEmployeeAllocationData(CInt(cboPeriod.SelectedValue), mintTrainingBudgetAllocationID, CInt(Session("TrainingCostCenterAllocationID")), lstEmployeeList)
            Dim strAllocationName As String = ""
            dtSummaryData = objTNeedMaster.GetBudgetSummaryData(CInt(cboPeriod.SelectedValue), 0, mintTrainingNeedAllocationID, mintTrainingBudgetAllocationID, CInt(Session("TrainingCostCenterAllocationID")), CInt(Session("TrainingRemainingBalanceBasedOnID")), strAllocationName, , True)

            If dtEmployeeAllocationData IsNot Nothing AndAlso dtEmployeeAllocationData.Rows.Count > 0 AndAlso _
                dtSummaryData IsNot Nothing AndAlso dtSummaryData.Rows.Count > 0 Then
                For Each drRow As DataRow In dtEmployeeAllocationData.Rows
                    Dim drSummaryData() As DataRow = dtSummaryData.Select("allocationid =" & CInt(drRow.Item("allocationid")) & " AND departmentunkid = " & CInt(drRow.Item("allocationtranunkid")) & " ")
                    If drSummaryData.Count > 0 Then
                        Dim decMaxBudget, decTotalApprovedAmount, decTotalCurrentPendingRequestedAmt As Decimal
                        decMaxBudget = CDec(drSummaryData(0).Item("currmaxbudget_amt"))
                        decTotalApprovedAmount = CDec(drSummaryData(0).Item("CurrentApprovedAmt"))
                        decTotalCurrentPendingRequestedAmt = CDec(drSummaryData(0).Item("CurrentPendingRequestedAmt"))
                        If (decTotalApprovedAmount + decAmountEmp + decTotalCurrentPendingRequestedAmt) > decMaxBudget Then
                            Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department.")
                            'Hemant (21 Mar 2022) -- Start        
                            'ISSUE/ENHANCEMENT(NMB) : Message should end on the circled section only. Don’t go into details.
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1031, "Employee : #employeename#").Replace("#employeename#", drRow.Item("employee").ToString)
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1032, "Approved Total : #approvedtotal#").Replace("#approvedtotal#", Format(decTotalApprovedAmount, CStr(Session("fmtCurrency"))))
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1033, "Employee Requested Amount : #employeerequestedamount#").Replace("#employeerequestedamount#", Format(decAmountEmp, CStr(Session("fmtCurrency"))))
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1036, "Total Requested Amount : #totalrequestedamount#").Replace("#totalrequestedamount#", Format(decTotalCurrentPendingRequestedAmt, CStr(Session("fmtCurrency"))))
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1035, "Maximum Budget : #maxbudget#").Replace("#maxbudget#", Format(decMaxBudget, CStr(Session("fmtCurrency"))))
                            'Hemant (21 Mar 2022) -- End

                            DisplayMessage.DisplayMessage(strMsg, Me)
                            Return False
                        Else
                            drSummaryData(0).Item("CurrentPendingRequestedAmt") = CDec(drSummaryData(0).Item("CurrentPendingRequestedAmt")) + decAmountEmp
                            dtSummaryData.AcceptChanges()
                        End If
                    Else
                        Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department.")
                        'Hemant (21 Mar 2022) -- Start    
                        'ISSUE/ENHANCEMENT(NMB) : Message should end on the circled section only. Don’t go into details.
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1031, "Employee : #employeename#").Replace("#employeename#", drRow.Item("employee").ToString)
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1032, "Approved Total : #approvedtotal#").Replace("#approvedtotal#", Format(0, CStr(Session("fmtCurrency"))))
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1033, "Requested Amount : #requestedamount#").Replace("#requestedamount#", Format(decAmountEmp, CStr(Session("fmtCurrency"))))
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1036, "Total Requested Amount : #totalrequestedamount#").Replace("#totalrequestedamount#", Format(0, CStr(Session("fmtCurrency"))))
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1035, "Maximum Budget : #maxbudget#").Replace("#maxbudget#", Format(0, CStr(Session("fmtCurrency"))))
                        'Hemant (21 Mar 2022) -- End

                        DisplayMessage.DisplayMessage(strMsg, Me)
                        Return False
                    End If
                Next
                
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingRequest = Nothing
            objTNeedMaster = Nothing
        End Try
    End Function
    'Hemant (07 Mar 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtTrainingRequestDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtTrainingRequestDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = -1
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Training_Module
                dRow("scanattachrefid") = enScanAttactRefId.GROUP_TRAINING_REQUEST
                dRow("form_name") = mstrModuleName
                dRow("transactionunkid") = mintGroupTrainingRequestunkid

                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
            Else

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtTrainingRequestDocument.Rows.Add(dRow)
            Call FillTrainingRequestAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillTrainingRequestAttachment()
        Dim dtView As DataView
        Try
            If mdtTrainingRequestDocument Is Nothing Then
                dgvQualification.DataSource = Nothing
                dgvQualification.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtTrainingRequestDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvQualification.AutoGenerateColumns = False
            dgvQualification.DataSource = dtView
            dgvQualification.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (12 Oct 2022) -- End

    'Hemant (02 Aug 2024) -- Start
    'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
    Private Sub GetValue(ByVal intGroupTrainingRequestunkid As Integer)
        Try
            Dim objGroupTRequest As New clsgrouptraining_request_master

            objGroupTRequest._GroupTrainingRequestunkid = intGroupTrainingRequestunkid
            FillTrainingVenueCombo(objGroupTRequest._Trainingvenueunkid)
            Clear_Controls()
            dtpStartDate.SetDate = CDate(objGroupTRequest._Start_Date)
            dtpEndDate.SetDate = CDate(objGroupTRequest._End_Date)
            mstrAllocationTranUnkIDs = objGroupTRequest._Allocationtranunkids
            cboTrainingName.SelectedValue = objGroupTRequest._DepartmentalTrainingNeedunkid.ToString()
            Call cboTrainingName_SelectedIndexChanged(cboTrainingName, New System.EventArgs)
            
            cboTrainingProvider.SelectedValue = objGroupTRequest._Trainingproviderunkid.ToString
            cboTrainingVenue.SelectedValue = objGroupTRequest._Trainingvenueunkid.ToString
            txtTotalTrainingCost.Text = Format(CDec(objGroupTRequest._TotalTrainingCost), CStr(Session("fmtCurrency")))

            If CBool(objGroupTRequest._IsTravelling) = True Then
                rdbTravellingYes.Checked = True
                rdbTravellingNo.Checked = False
            Else
                rdbTravellingYes.Checked = False
                rdbTravellingNo.Checked = True
            End If
            rdbTravellingYes_CheckedChanged(Nothing, Nothing)
            drpTravelType.SelectedValue = CStr(objGroupTRequest._TrainingTypeId)

            txtExpectedReturn.Text = objGroupTRequest._ExpectedReturn
            txtRemarks.Text = objGroupTRequest._Remarks

            txtVenue.Text = objGroupTRequest._Venue
            mstrFinancialSourceIds = objGroupTRequest._FinancingSourcesTranUnkids

            'cboTargetedGroup.SelectedValue = objGroupTRequest._Targetedgroupunkid.ToString
            'Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (02 Aug 2024) -- End


#End Region

#Region " Button's Event "

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    'Protected Sub btnTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrainingName.Click

    '    Try
    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgvAddTrainingName.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

    '        If gRow Is Nothing OrElse gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage("Training is compulsory information.Please Check atleast One Training.", Me)
    '            Exit Sub
    '        End If
    '        If gRow Is Nothing OrElse gRow.Count > 1 Then
    '            DisplayMessage.DisplayMessage("Please Check Only One Training.", Me)
    '            Exit Sub
    '        End If
    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            For Each dgRow As GridViewRow In gRow
    '                mintCourseMasterunkid = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("masterunkid").ToString())
    '                txtTrainingName.Text = dgvAddTrainingName.DataKeys(dgRow.RowIndex)("name").ToString()
    '                mintDepartTrainingNeedId = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("departmentaltrainingneedunkid").ToString())
    '                FillDepartmentTrainingInfo()
    '            Next
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally


    '    End Try

    'End Sub
    'Hemant (09 Feb 2022) -- End


    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click, btnSave.Click
        Try
            'Hemant (02 Aug 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            If Me.ViewState("Sender").ToString().ToUpper() = btnSubmit.ID.ToUpper Then
                'Hemant (02 Aug 2024) -- End
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Are you sure you want to submit for approval for this Training Request?")
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            cnfConfirm.Show()
                'Hemant (02 Aug 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2682 :  Group training request: Option for temporary save
            Else
                Call Submit_Click()
            End If
            'Hemant (02 Aug 2024) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (08 Mar 2022) -- Start
    'Enhancement :  : NMB - On group training request screen, provide an allocation link to help the user narrow down to smaller units when doing the training requests.
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillAllocEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
            Call FillAllocEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (08 Mar 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If IsValidate() = False Then Exit Sub

            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                
                mdtTrainingRequestDocument = CType(ViewState("mdtTrainingRequestDocument"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillTrainingRequestAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvQualification.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2007, "No Files to download."), Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (12 Oct 2022) -- End
#End Region

#Region " Combobox Events "

    Protected Sub cboTrainingCalender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCalender.SelectedIndexChanged
        Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsList As DataSet = Nothing
        Try
            Dim strFilter As String = ""

            strFilter = " AND trdepartmentaltrainingneed_master.periodunkid = '" & CInt(cboTrainingCalender.SelectedValue) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0)  = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0)  = 0 "
            dsList = objDeptTrainingNeed.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), CInt(Session("TrainingNeedAllocationID")), "Tranining" _
                                                        , False, strFilter, 0, 0)

           
            If dsList.Tables.Count > 0 Then
                Dim xTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid")

                If xTable.Columns.Contains("IsGrp") = False Then
                    xTable.Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
                End If

                For Each r As DataRow In xTable.Rows
                    r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    r("IsGrp") = False
                Next

                xTable.Columns("trainingcourseunkid").ColumnName = "masterunkid"
                xTable.Columns("trainingcoursename").ColumnName = "name"

                Dim dsTCategoryList As DataSet = objTCategory.getListForCombo("List", False)
                Dim dRow As DataRow = Nothing
                For Each drTCategory As DataRow In dsTCategoryList.Tables(0).Rows
                    Dim drRow() As DataRow = xTable.Select("trainingcategoryunkid = " & CInt(drTCategory.Item("categoryunkid")) & " ")
                    If drRow.Length > 0 Then
                        dRow = xTable.NewRow()
                        dRow.Item("departmentaltrainingneedunkid") = -1
                        dRow.Item("trainingcategoryunkid") = CInt(drTCategory.Item("categoryunkid"))
                        dRow.Item("trainingcategoryname") = CStr(drTCategory.Item("categoryname"))
                        dRow.Item("masterunkid") = -1
                        dRow.Item("name") = ""
                        dRow.Item("startdate") = ""
                        dRow.Item("enddate") = ""
                        dRow.Item("allocationtranname") = ""
                        dRow.Item("allocationtranunkid") = -1
                        dRow.Item("IsGrp") = 1
                        xTable.Rows.Add(dRow)
                    End If
                Next

                xTable = New DataView(xTable, "", "trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy

                dgvAddTrainingName.DataSource = xTable
                dgvAddTrainingName.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDeptTrainingNeed = Nothing
            objTCategory = Nothing
        End Try
    End Sub

    Protected Sub cboTargetedGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTargetedGroup.SelectedIndexChanged
        Try
            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvEmployee.Enabled = True
                lblNoOfVacantStaff.Visible = False
                txtNoOfVacantStaff.Visible = False
                pnlAllocEmp.Visible = False
            Else
                dgvEmployee.Enabled = True
                lblNoOfVacantStaff.Visible = True
                txtNoOfVacantStaff.Visible = True
                pnlAllocEmp.Visible = True
            End If
            Call FillTargetedGroup()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            FillTrainingNameList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboTrainingName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingName.SelectedIndexChanged
        Try

            mintDepartTrainingNeedId = CInt(cboTrainingName.SelectedValue)
            FillDepartmentTrainingInfo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (09 Feb 2022) -- End

#End Region

#Region "Checkbox Events"

    Protected Sub ChkAllTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If IsValidData(False, False) = False Then Exit Try

            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            For Each gvRow As GridViewRow In dgvEmployee.Rows
                CType(gvRow.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = chkSelectAll.Checked
            Next

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkgvSelectTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If IsValidData(False, False) = False Then Exit Try

            Dim chkSelect As CheckBox = CType(sender, CheckBox)

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso dgvEmployee.Rows.Count = gRow.Count Then
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
            Else
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = False
            End If

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Links Event "

    'Hemant (09 Feb 2022) -- Start            
    'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
    'Protected Sub lnkAddTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingName.Click
    '    Try
    '        dgvAddTrainingName.DataSource = Nothing
    '        dgvAddTrainingName.DataBind()

    '        Dim objCalender As New clsTraining_Calendar_Master
    '        Dim dsList As DataSet = objCalender.getListForCombo("List", False, enStatusType.OPEN)
    '        With cboTrainingCalender
    '            .DataValueField = "calendarunkid"
    '            .DataTextField = "name"
    '            .DataSource = dsList.Tables(0).Copy()
    '            .DataBind()
    '        End With
    '        'Hemant (03 Dec 2021) -- Start
    '        'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    '        cboTrainingCalender.SelectedValue = CStr(cboPeriod.SelectedValue)
    '        lblTrainingCalender.Visible = True
    '        cboTrainingCalender.Visible = True
    '        cboTrainingCalender.Enabled = False
    '        'Hemant (03 Dec 2021) -- End
    '        dsList.Clear()
    '        dsList = Nothing
    '        cboTrainingCalender_SelectedIndexChanged(New Object, New EventArgs)


    '        mblnShowTrainingNamePopup = True
    '        popupTrainingName.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Hemant (09 Feb 2022) -- End


    'Sohail (08 Mar 2022) -- Start
    'Enhancement :  : NMB - On group training request screen, provide an allocation link to help the user narrow down to smaller units when doing the training requests.
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (08 Mar 2022) -- End

#End Region

#Region " GridView "

    Protected Sub dgvAddTrainingName_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAddTrainingName.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(dgvAddTrainingName.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "trainingcategoryname").ToString
                    e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = 2 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    Dim ChkgvSelect As CheckBox = TryCast(e.Row.FindControl("ChkgvSelect"), CheckBox)
                    ChkgvSelect.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Protected Sub dgvQualification_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvQualification.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(5).Text) > 0 Then
                    xrow = mdtTrainingRequestDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(5).Text) & "")
                Else
                    xrow = mdtTrainingRequestDocument.Select("GUID = '" & e.Item.Cells(4).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        Me.ViewState("DeleteAttRowIndex") = mdtTrainingRequestDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2008, "File does not Exist..."), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(1).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (12 Oct 2022) -- End


#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnSubmit.ID.ToUpper, btnSave.ID.ToUpper
                    Call Submit_Click()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Radion Button's Event "
    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Protected Sub rdbTravellingYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTravellingYes.CheckedChanged, rdbTravellingNo.CheckedChanged
        Try
            pnlTravelType.Visible = False
            drpTravelType.SelectedValue = CStr(0)
            If rdbTravellingYes.Checked = True Then
                pnlTravelType.Visible = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (29 Apr 2022) -- End
#End Region

#Region "Controls Event(S)"
    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtTrainingRequestDocument.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtTrainingRequestDocument.AcceptChanges()
                Call FillTrainingRequestAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (12 Oct 2022) -- End

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingRequestDetails.ID, lblTrainingRequestDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEmployeeDetails.ID, lblEmployeeDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingRequestInfoCaption.ID, lblTrainingRequestInfoCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApplicationDate.ID, lblApplicationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingName.ID, lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStartDate.ID, lblStartDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEndDate.ID, lblEndDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblProviderName.ID, lblProviderName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblProviderAddress.ID, lblProviderAddress.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTravelling.ID, lblTravelling.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblExpectedReturn.ID, lblExpectedReturn.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRemarks.ID, lblRemarks.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCostList.ID, lblTrainingCostList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblFinancingSourceList.ID, lblFinancingSourceList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEmployeeDetailsCaption.ID, lblEmployeeDetailsCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTargetedGroup.ID, lblTargetedGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblNoOfStaff.ID, lblNoOfStaff.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblNoOfVacantStaff.ID, lblNoOfVacantStaff.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTTrainingName.ID, lblTTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTrainingCalender.ID, lblTrainingCalender.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lnkAdvanceFilter.ID, lnkAdvanceFilter.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblVenue.ID, lblVenue.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSubmit.ID, Me.btnSubmit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnTrainingName.ID, Me.btnTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnCloseTCostItem.ID, Me.btnCloseTCostItem.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingCostItem.Columns(0).FooterText, dgvTrainingCostItem.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)
            

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblTrainingRequestDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingRequestDetails.ID, lblTrainingRequestDetails.Text)
            Me.lblEmployeeDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEmployeeDetails.ID, lblEmployeeDetails.Text)
            Me.lblTrainingRequestInfoCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingRequestInfoCaption.ID, lblTrainingRequestInfoCaption.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            Me.lblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApplicationDate.ID, lblApplicationDate.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingName.ID, lblTrainingName.Text)
            Me.lblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStartDate.ID, lblStartDate.Text)
            Me.lblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEndDate.ID, lblEndDate.Text)
            Me.lblProviderName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblProviderName.ID, lblProviderName.Text)
            Me.lblProviderAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblProviderAddress.ID, lblProviderAddress.Text)
            Me.lblTravelling.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTravelling.ID, lblTravelling.Text)
            Me.lblExpectedReturn.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblExpectedReturn.ID, lblExpectedReturn.Text)
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRemarks.ID, lblRemarks.Text)
            Me.lblTrainingCostList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCostList.ID, lblTrainingCostList.Text)
            Me.lblTotalTrainingCost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Me.lblFinancingSourceList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblFinancingSourceList.ID, lblFinancingSourceList.Text)
            Me.lblEmployeeDetailsCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEmployeeDetailsCaption.ID, lblEmployeeDetailsCaption.Text)
            Me.lblTargetedGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTargetedGroup.ID, lblTargetedGroup.Text)
            Me.lblNoOfStaff.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblNoOfStaff.ID, lblNoOfStaff.Text)
            Me.lblNoOfVacantStaff.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblNoOfVacantStaff.ID, lblNoOfVacantStaff.Text)
            Me.lblTTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTTrainingName.ID, lblTTrainingName.Text)
            Me.lblTrainingCalender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingCalender.ID, lblTrainingCalender.Text)
            Me.lnkAdvanceFilter.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lnkAdvanceFilter.ID, lnkAdvanceFilter.ToolTip)
            Me.lblVenue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblVenue.ID, lblVenue.Text)

            Me.btnSubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnTrainingName.ID, Me.btnTrainingName.Text).Replace("&", "")
            Me.btnCloseTCostItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnCloseTCostItem.ID, Me.btnCloseTCostItem.Text).Replace("&", "")

            dgvTrainingCostItem.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingCostItem.Columns(0).FooterText, dgvTrainingCostItem.Columns(0).HeaderText)
            dgvTrainingCostItem.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)

            dgvFinancingSource.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)

            dgvEmployee.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            dgvEmployee.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            dgvAllocEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            dgvAllocEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            dgvAddTrainingName.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, Training Name is mandatory information. Please select Training Name to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, Application date is mandatory information. Please select Application date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, the Start date is mandatory information. Please select the start date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, the End date is mandatory information. Please select the end date to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, Start date should be in between selected period start date and end date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, End date should be in between selected period start date and end date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, Start date should not be greater than end date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, Total Training Cost should be greater than Zero")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Are you sure you want to submit for approval for this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Information saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Sorry, ""Does this Training Require Travelling?"" is mandatory information. Please select Yes/No to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Sorry, Travel Type is mandatory information. Please select Travel Type to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Venue is compulsory information. Please enter Venue to continue.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
