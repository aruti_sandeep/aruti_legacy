﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_GroupTrainingRequest.aspx.vb"
    Inherits="Training_Group_Training_Request_wPg_GroupTrainingRequest" Title="Group Training Request" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        prm.add_endRequest(endTotalAmountRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
        
        function endTotalAmountRequestHandler(sender, event) {
          
            $('input[id*=txtAmount]').blur(function() {
				var id = $(this).get(0).id;
				$.ajax({
                    "url": window.location.href + "/ConvertToFmtCurrency",
                    "data": '{strNumber: ' + JSON.stringify($(this).val()) + '}',
                    "type": "POST",
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "success": function (result) {
                        var j = result.d;

                        $('#'+id).val(j);
						
                    },
                    error: function (d) {
                        alert("Whooaaa! Something went wrong..")
                    },                  
                });
                CalcTotal();
            });
        }
        
          function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
            $("body").on("click", "[id*=ChkAll]", function() {    
                var chkHeader = $(this);
               var grid = $(this).closest(".body");
                $("[id*=ChkgvSelect]",grid).prop("checked", $(chkHeader).prop("checked"));
            });

            $("body").on("click", "[id*=ChkgvSelect]", function() {   
                var grid = $(this).closest(".body");
               var chkHeader = $("[id*=chkAllSelect]", grid);
                debugger;
               if ($("[id*=ChkgvSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                    chkHeader.prop("checked", true);
                 }
               else {
                    chkHeader.prop("checked", false);
                }
          });

        $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearchAddTrainingName').val().length > 0) {
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr').hide();
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr:first').show();
                    $('#<%= dgvAddTrainingName.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchAddTrainingName').val() + '\')').parent().show();
                }
                else if ($('#txtSearchAddTrainingName').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= dgvAddTrainingName.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearchAddTrainingName').val('');
                $('#<%= dgvAddTrainingName.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearchAddTrainingName').focus();
            } 

	  function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();
                
            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }
                
            }
            else if ($('#' + txtID).val().length == 0) {
            resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
            $('.norecords').remove();
            }            
            $('#' + txtID).focus();
        }
        
          function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <cnf:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Group Training Requests" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#TrainingRequestDetails" data-toggle="tab">
                                        <asp:Label ID="lblTrainingRequestDetails" runat="server" Text="Training Request Details"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#EmployeeDetails" data-toggle="tab">
                                        <asp:Label ID="lblEmployeeDetails" runat="server" Text="Employee Details"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="TrainingRequestDetails">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblTrainingRequestInfoCaption" runat="server" Text="Training Request Info"
                                                                    CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblPeriod" Text="Training Period" runat="server" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblApplicationDate" runat="server" Text="Application Date" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <uc2:DateCtrl ID="dtpApplicationDate" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix" style="display: none">
                                                                <%--<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtTrainingName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 m-t-30">
                                                                    <asp:LinkButton runat="server" ID="lnkAddTrainingName" CssClass="m-r-10" ToolTip="Add Training Name">
                                                                        <i class="fas fa-plus-circle" ></i>
                                                                    </asp:LinkButton>
                                                                </div>--%>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblTrainingName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboTrainingName" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <uc2:DateCtrl ID="dtpStartDate" runat="server" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <uc2:DateCtrl ID="dtpEndDate" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblProviderName" runat="server" Text="Provider Name" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboTrainingProvider" runat="server" AutoPostBack="false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblProviderAddress" runat="server" Text="Provider Address" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboTrainingVenue" runat="server" AutoPostBack="false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblVenue" runat="server" Text="Venue" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtVenue" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                                Rows="2"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblTravelling" runat="server" Text="Does this Training Require Travelling?"
                                                                        CssClass="form-label"></asp:Label>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                                    <asp:RadioButton ID="rdbTravellingYes" runat="server" Text="Yes" GroupName="Travelling"
                                                                        AutoPostBack="true" />
                                                                    <asp:RadioButton ID="rdbTravellingNo" runat="server" Text="No" GroupName="Travelling"
                                                                        AutoPostBack="true" />
                                                                </div>
                                                                <asp:Panel ID="pnlTravelType" runat="server" Visible="false">
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblTravelType" runat="server" Text="Travel Type" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <asp:DropDownList ID="drpTravelType" runat="server" AutoPostBack="false">
                                                                            </asp:DropDownList>
                                                                </div>
                                                                </div>
                                                                </asp:Panel>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblExpectedReturn" runat="server" Text="Please describe the Expected Return on Investment"
                                                                        CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtExpectedReturn" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                                Rows="2"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblRemarks" runat="server" Text="Any other Remarks/Comments" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                                Rows="2"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblAttachmentInfo" runat="server" Text="Attachment Info" CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboDocumentType" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-20  ">
                                                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                            <div id="fileuploader">
                                                                                <input type="button" id="btnAddFile" runat="server" onclick="return IsValidAttach()"
                                                                                    value="Browse" class="btn btn-primary" />
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  p-l-0">
                                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                                                                    </div>
                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                                                        <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                                    <asp:DataGrid ID="dgvQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                        Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                                        HeaderStyle-Font-Bold="false">
                                                                        <Columns>
                                                                            <asp:TemplateColumn FooterText="objcohDelete">
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                            <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                            <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                                ItemStyle-Font-Size="22px">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                                Visible="false" />
                                                                            <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                                Visible="false" />
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblTrainingCostList" runat="server" Text="Training Cost List" CssClass="form-label"></asp:Label>
                                                            </h2>
                                                            <ul class="header-dropdown m-r--5">
                                                                <li class="dropdown"><a target="_blank" onclick="PageMethods.SetIsDeptTrainingFromBacklog(3);"
                                                                    href="<%= Session("rootpath") %>Training/Departmental_Training_Needs/wPg_DepartmentalTrainingNeeds.aspx">
                                                                    <i class="fa fa-th-list" style="margin-right: 5px;"></i>Open Budget Summary</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 460px">
                                                                        <asp:GridView ID="dgvTrainingCostItem" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="trainingrequestcosttranunkid,infounkid,amount"
                                                                            CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="info_name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEInfoName" />
                                                                                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="align-center"
                                                                                    ItemStyle-HorizontalAlign="Right" FooterText="colhamount">
                                                                                    <ItemTemplate>
                                                                                        <%--<nut:NumericText ID="txtAmount" runat="server" Type="Point" Style="text-align: right"
                                                                                            cssclass="form-control" Text='<%# Eval("amount") %>' />--%>
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtAmount" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                                                    CssClass="form-control text-right" Text='<%# Eval("amount") %>'></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="width: 100%; border-bottom: 1px solid #DDD;
                                                                    text-align: left;">
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblTotalTrainingCost" runat="server" Text="Total Training Cost" Class="TotalLabel"
                                                                        CssClass="form-label"></asp:Label>
                                                                    <div class="form-group">
                                                                        <div class="form-line" style="text-align: right;">
                                                                            <asp:TextBox ID="txtTotalTrainingCost" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                                Style="text-align: right" Text="0.00" CssClass="form-control" Enabled="false"
                                                                                EnableViewState="False" ViewStateMode="Disabled"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblFinancingSourceList" runat="server" Text="Financing Source List"
                                                                    CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="table-responsive" style="max-height: 200px">
                                                                        <asp:GridView ID="dgvFinancingSource" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="masterunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkAllFSource" runat="server" Text=" " CssClass="chk-sm" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvSelectFSource" runat="server" Text=" " CssClass="chk-sm" Checked='<%# Eval("IsChecked")%>' />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle />
                                                                                    <ItemStyle />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkDeleteAllFSource" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                CommandName="RemoveAll">
                                                                                            <i class="fas fa-times text-danger"></i>
                                                                                            </asp:LinkButton>
                                                                                        </span>
                                                                                    </HeaderTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="EmployeeDetails">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblEmployeeDetailsCaption" runat="server" Text="Employee Details"
                                                                    CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblTargetedGroup" Text="Targeted Group" runat="server" CssClass="form-label" />
                                                                    <div class="form-group">
                                                                        <asp:DropDownList ID="cboTargetedGroup" runat="server" AutoPostBack="true" Enabled="false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblNoOfStaff" Text="Number of Staff(s)" runat="server" CssClass="form-label" />
                                                                    <nut:NumericText ID="txtNoOfStaff" runat="server" CssClass="form-control" Enabled="false" />
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblNoOfVacantStaff" Text="Number of Vacant Staff(s)" runat="server"
                                                                        CssClass="form-label" />
                                                                    <nut:NumericText ID="txtNoOfVacantStaff" runat="server" CssClass="form-control" Enabled="false" />
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                </div>
                                                            </div>
                                                            <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <%--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--%>
                                                                    <div class="card inner-card">
                                                                        <div class="body">
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <div class="form-line">
                                                                                            <input type="text" id="txtSearchEmp" name="txtSearchEmp" placeholder="Search Targeted Group"
                                                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchEmp','<%= dgvEmployee.ClientID %>');" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row clearfix">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="table-responsive" style="max-height: 34vh;">
                                                                                        <asp:GridView ID="dgvEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                                            Enabled="false" DataKeyNames="id" CssClass="table table-hover table-bordered">
                                                                                            <Columns>
                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-CssClass="align-center">
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:CheckBox ID="ChkAllTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                                            AutoPostBack="true" OnCheckedChanged="ChkAllTargetedGroup_CheckedChanged" />
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="ChkgvSelectTargetedGroup" runat="server" Text=" " CssClass="chk-sm"
                                                                                                            Checked='<%# Eval("IsChecked")%>' AutoPostBack="true" OnCheckedChanged="ChkgvSelectTargetedGroup_CheckedChanged" />
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle />
                                                                                                    <ItemStyle />
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                                <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <HeaderTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="lnkDeleteAllEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                CommandName="RemoveAll"> <i class="fas fa-times text-danger"></i> </asp:LinkButton>
                                                                                                        </span>
                                                                                                    </HeaderTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <%--</div>--%>
                                                                </div>
                                                                <asp:Panel ID="pnlAllocEmp" runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card">
                                                                            <div class="body">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <input type="text" id="txtSearchAllocEmp" name="txtSearchAllocEmp" placeholder="Search Employee"
                                                                                                    maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchAllocEmp', '<%= dgvAllocEmp.ClientID %>');" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 header-dropdown m-r--5" style="padding-top: 15px;">
                                                                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter"> 
                                                                                            <i class="fas fa-sliders-h"></i>                                                                                                   
                                                                                        </asp:LinkButton>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 34vh;">
                                                                                            <asp:GridView ID="dgvAllocEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                                                DataKeyNames="employeeunkid" CssClass="table table-hover table-bordered">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                        HeaderStyle-CssClass="align-center">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="ChkAllAllocEmp" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'ChkgvSelectAllocEmp');" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="ChkgvSelectAllocEmp" runat="server" Text=" " CssClass="chk-sm"
                                                                                                                Checked='<%# Eval("IsChecked")%>' onclick="ChkSelect(this,'ChkgvSelectAllocEmp', 'ChkAllAllocEmp')" />
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle />
                                                                                                        <ItemStyle />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                                    <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                                    <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" Visible="false">
                                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                                        <HeaderTemplate>
                                                                                                            <span class="gridiconbc">
                                                                                                                <asp:LinkButton ID="lnkDeleteAllAllocEmp" runat="server" ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                                    CommandName="RemoveAll"> <i class="fas fa-times text-danger"></i> </asp:LinkButton>
                                                                                                            </span>
                                                                                                        </HeaderTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit for Approval" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="TabName" runat="server" />
                <cc1:ModalPopupExtender ID="popupTrainingName" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hfTrainingName" runat="server" CancelControlID="hfTrainingName"
                    PopupControlID="pnlTrainingName">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlTrainingName" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTTrainingName" Text="Add Training Name" runat="server" CssClass="form-label" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblTrainingCalender" runat="server" Text="Training Calendar" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboTrainingCalender" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="txtSearchAddTrainingName" name="txtSearch" placeholder="Type To Search Text"
                                            maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearchAddTrainingName', '<%= dgvAddTrainingName.ClientID %>');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="max-height: 415px;">
                                    <asp:GridView ID="dgvAddTrainingName" runat="server" AutoGenerateColumns="False"
                                        AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%" DataKeyNames="masterunkid,name,trainingcategoryname,allocationtranname,departmentaltrainingneedunkid,IsGrp">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="align-center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                </ItemTemplate>
                                                <HeaderStyle />
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" FooterText="colhEName" />
                                        </Columns>
                                        <HeaderStyle Font-Bold="False" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <div style="float: left">
                            <asp:Button ID="btnAddTrainingName" runat="server" CssClass="btn btn-primary" Text="Add Training"
                                Visible="false" />
                        </div>
                        <asp:Button ID="btnTrainingName" runat="server" CssClass="btn btn-primary" Text="Add" />
                        <asp:Button ID="btnCloseTCostItem" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="hfTrainingName" runat="Server" />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfobjlblTotalAmt" runat="server" Value="0.00" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
        $(document).ready(function() {
		    RetriveTab();
            
        });
         function RetriveTab() {
 
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "TrainingRequestDetails";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                debugger;
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }

    </script>

    <script type="text/javascript">

        $(document).ready(function() {           
			CalcTotal();
			
            $('input[id*=txtAmount]').blur(function() {
				var id = $(this).get(0).id;
				$.ajax({
                    "url": window.location.href + "/ConvertToFmtCurrency",
                    "data": '{strNumber: ' + JSON.stringify($(this).val()) + '}',
                    "type": "POST",
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "success": function (result) {
                        var j = result.d;

                        $('#'+id).val(j);
						
                    },
                    error: function (d) {
                        alert("Whooaaa! Something went wrong..")
                    },                  
                });
                CalcTotal();
            });

        });

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }
        
        function CalcTotal() {
            var totalamt = 0;
            $('input[id*=txtAmount]').each(function() {
                totalamt += Number($(this).val().replaceAll(',',''));
            });

            //if (totalamt.toString().indexOf('.') >= 0)
            //    $$('txtTotalTrainingCost').val(totalamt);
            //else
            //    $$('txtTotalTrainingCost').val(totalamt.toString().concat('.00'));
			
			$.ajax({
                    "url": window.location.href + "/ConvertToFmtCurrency",
                    "data": '{strNumber: ' + JSON.stringify(totalamt) + '}',
                    "type": "POST",
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "success": function (result) {
                        var j = result.d;

                        $$('txtTotalTrainingCost').val(j);
						$$('hfobjlblTotalAmt').val($$('txtTotalTrainingCost').text());
                    },
                    error: function (d) {
                        alert("Whooaaa! Something went wrong..")
                    },                  
                });

            $$('hfobjlblTotalAmt').val($$('txtTotalTrainingCost').text());
        }
        
      
        
    </script>

      <script type="text/javascript">

          $(document).ready(function() {
              ImageLoad();
              $(".ajax-upload-dragdrop").css("width", "auto");
          });
          function ImageLoad() {
              debugger;
              if ($(".ajax-upload-dragdrop").length <= 0) {
                  $("#fileuploader").uploadFile({
                  url: "wPg_GroupTrainingRequest.aspx?uploadimage=mSEfU19VPc4=",
                      method: "POST",
                      dragDropStr: "",
                      showStatusAfterSuccess: false,
                      showAbort: false,
                      showDone: false,
                      fileName: "myfile",
                      onSuccess: function(path, data, xhr) {
                          $("#<%= btnAddAttachment.ClientID %>").click();
                      },
                      onError: function(files, status, errMsg) {
                          alert(errMsg);
                      }
                  });
              }
          }
          //$('input[type=file]').live("click", function() {
          $("body").on("click", 'input[type=file]', function() {
              return IsValidAttach();
          });
    </script>

</asp:Content>
