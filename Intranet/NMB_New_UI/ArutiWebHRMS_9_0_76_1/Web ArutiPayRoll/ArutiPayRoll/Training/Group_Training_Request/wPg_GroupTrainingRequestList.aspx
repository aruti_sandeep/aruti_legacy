﻿<%@ Page Title="Group Training Request Form List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_GroupTrainingRequestList.aspx.vb" Inherits="Training_Group_Training_Request_wPg_GroupTrainingRequestList" %>

<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Group Training Request Form List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Training Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingName" runat="server" Text="Training Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpTrainingName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0 ">
                                        <asp:Label ID="lblTotalTrainingCost" runat="server" Text="Total Training Cost" CssClass="form-label"></asp:Label>
                                        <nut:NumericText ID="txtTotalTrainingCost" runat="server" Type="Point" />
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12  col-xs-12  m-t-20">
                                        <div class="form-group ">
                                            <asp:DropDownList ID="cboCondition" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:GridView ID="dgTraining" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                CssClass="table table-hover table-bordered" DataKeyNames="grouptrainingrequestunkid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                              <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Select" ToolTip="Edit"
                                                                    OnClick="lnkEdit_Click">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" OnClick="lnkDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Training" HeaderText="Training" ReadOnly="true" FooterText="colhTraining" />
                                                    <asp:BoundField DataField="application_date" HeaderText="Application Date" FooterText="dgcolhApplicationDate" />                                                    
                                                    <asp:BoundField DataField="start_date" HeaderText="Start Date" ReadOnly="true" FooterText="colhSDate" />
                                                    <asp:BoundField DataField="end_date" HeaderText="End Date" ReadOnly="true" FooterText="colhEDate" />
                                                    <asp:BoundField DataField="totaltrainingcost" HeaderText="Total Training Cost" ItemStyle-HorizontalAlign="Right"
                                                        ReadOnly="true" FooterText="colhTotal_training_cost" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
