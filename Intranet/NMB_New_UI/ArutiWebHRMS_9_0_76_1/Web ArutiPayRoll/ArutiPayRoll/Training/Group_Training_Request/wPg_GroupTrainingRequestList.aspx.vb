﻿Imports System.Data
Imports Aruti.Data
Imports System.Drawing

Partial Class Training_Group_Training_Request_wPg_GroupTrainingRequestList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmGroupTrainingRequestList"
    Private mintGroupTrainingRequestunkid As Integer
    Private mstrConfirmationAction As String = ""

#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()

                Call FillCombo()
                Call FillList()
            Else
                mintGroupTrainingRequestunkid = CInt(Me.ViewState("mintGroupTrainingRequestunkid"))
                mstrConfirmationAction = Me.ViewState("mstrConfirmationAction")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintGroupTrainingRequestunkid") = mintGroupTrainingRequestunkid
            Me.ViewState("mstrConfirmationAction") = mstrConfirmationAction
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objMaster As New clsMasterData
        Try
            dsList = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With drpTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMaster.GetCondition(False, True, True, False, False)

            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dtCondition
                .DataBind()
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objGroupTraining As New clsgrouptraining_request_master
        Dim dsList As DataSet
        Dim StrSearching As String = String.Empty
        Try
            StrSearching = "AND issubmit_approval = 0 "
            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND trgrouptraining_request_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            If CInt(drpTrainingName.SelectedValue) > 0 Then
                StrSearching &= "AND trgrouptraining_request_master.coursemasterunkid = " & CInt(drpTrainingName.SelectedValue) & " "
            End If

            If txtTotalTrainingCost.Text.Trim <> "" AndAlso CDec(txtTotalTrainingCost.Text) > 0 Then
                StrSearching &= "AND trgrouptraining_request_master.totaltrainingcost " & cboCondition.SelectedItem.Text & " " & CDec(txtTotalTrainingCost.Text) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If


            dsList = objGroupTraining.GetList("List", StrSearching)

            dgTraining.AutoGenerateColumns = False
            dgTraining.DataSource = dsList.Tables(0)
            dgTraining.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objGroupTraining = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByRef objRequest As clsgrouptraining_request_master)
        Try
            objRequest._GroupTrainingRequestunkid = mintGroupTrainingRequestunkid
            objRequest._AuditUserId = CInt(Session("UserId"))
            objRequest._ClientIP = CStr(Session("IP_ADD"))
            objRequest._FormName = mstrModuleName
            objRequest._IsWeb = True
            objRequest._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button's Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training/Group_Training_Request/wPg_GroupTrainingRequest.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            drpTrainingName.SelectedValue = 0
            txtTotalTrainingCost.Text = "0"
            cboCondition.SelectedIndex = 0

            dgTraining.DataSource = New List(Of String)
            dgTraining.DataBind()

        Catch ex As Exception

            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub


#End Region

#Region "Gridview's Events"
    Protected Sub dgTraining_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgTraining.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhSDate", False, True)).Text).Date.ToShortDateString
                End If

                If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhEDate", False, True)).Text).Date.ToShortDateString
                End If

                e.Row.Cells(getColumnID_Griview(dgTraining, "colhTotal_training_cost", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(dgTraining, "colhTotal_training_cost", False, True)).Text), Session("fmtcurrency").ToString())

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Link Button's Events"

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            mintGroupTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("grouptrainingrequestunkid"))

            Session("GroupTrainingRequestunkid") = mintGroupTrainingRequestunkid
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training/Group_Training_Request/wPg_GroupTrainingRequest.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)
            Dim dgTrainingName As GridView = TryCast(row.NamingContainer, GridView)
            mintGroupTrainingRequestunkid = CInt(dgTrainingName.DataKeys(row.RowIndex)("grouptrainingrequestunkid"))

            mstrConfirmationAction = "delRequest"
            cnfConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELREQUEST"
                    delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Enter the Reason for deleting this training request")
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Delete Reason"

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Dim objGroupTMaster As New clsgrouptraining_request_master
        Try
            Select Case mstrConfirmationAction.ToUpper()
                Case "DELREQUEST"
                    SetValue(objGroupTMaster)
                    objGroupTMaster._Isvoid = True
                    objGroupTMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        objGroupTMaster._Voiduserunkid = CInt(Session("UserId"))
                    Else
                        objGroupTMaster._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                    End If
                    objGroupTMaster._Voidreason = delReason.Reason
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        objGroupTMaster._Userunkid = CInt(Session("UserId"))
                        objGroupTMaster._LoginEmployeeunkid = -1
                    Else
                        objGroupTMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                        objGroupTMaster._Userunkid = -1
                    End If
                    blnFlag = objGroupTMaster.Delete(mintGroupTrainingRequestunkid)
                    If blnFlag = False AndAlso objGroupTMaster._Message.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(objGroupTMaster._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Group Training Request deleted successfully"), Me)
                        FillList()
                    End If
                    objGroupTMaster = Nothing
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objGroupTMaster = Nothing
        End Try
    End Sub


#End Region


    Private Sub SetControlCaptions()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetailHeader.ID, lblDetailHeader.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTrainingName.ID, lblTrainingName.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnNew.ID, btnNew.Text).Replace("&", "")
            Me.BtnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), BtnSearch.ID, BtnSearch.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), BtnReset.ID, BtnReset.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
