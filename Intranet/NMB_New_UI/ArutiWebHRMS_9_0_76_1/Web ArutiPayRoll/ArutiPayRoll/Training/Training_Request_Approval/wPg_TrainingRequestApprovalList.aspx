﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingRequestApprovalList.aspx.vb"
    Inherits="Training_Training_Request_Approval_wPg_TrainingRequestApprovalList"
    Title="Training Request Approval List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cnf:Confirmation ID="cnfApprovDisapprove" runat="server" Title="Aruti" />
                <cnf:Confirmation ID="cnfApproveConfirm" runat="server" Title="Aruti" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Request Approval List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Training Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlRole" runat="server" Visible="false">
                                            <asp:Label ID="lblRole" runat="server" Text="Role" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtRole" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlApprover" runat="server" Visible="false">
                                            <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label">
                                            </asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtApprover" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevel" runat="server" Text="Training" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpTraining" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Approval Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Checked="true"
                                            Font-Bold="true" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblemp" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpemp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="display: none;">
                                        <asp:Label ID="lblCompletionStatus" runat="server" Text="Completion Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCompletionStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px;">
                                            <asp:Panel ID="pnlSummary" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="gvSummary" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                    Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="IsGrp, refno, coursemasterunkid, trainingtypeid,insertformid,levelunkid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhselect" HeaderText=""
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhselect" HeaderText=""
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <img alt="" style="cursor: pointer" src="../../images/plus.png" />
                                                                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Style="display: none;">
                                                                    <asp:GridView ID="gvTrainingApprovalList" runat="server" AutoGenerateColumns="false"
                                                                        AllowPaging="false" Width="100%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                                        RowStyle-Wrap="false" DataKeyNames="pendingtrainingtranunkid,IsGrp,mapuserunkid,statusunkid,trainingrequestunkid,
                                                                        approverunkid,completed_statusunkid,iscompleted_submit_approval,completed_visibleid,employeeunkid,visibleid,
                                                                        grouptrainingrequestunkid,priority" OnRowDataBound="gvTrainingApprovalList_RowDataBound">
                                                                        <Columns>
                                                                            <%--<asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkChangeStatus" runat="server" ToolTip="Change Status" OnClick="lnkChangeStatus_Click"
                                                                        CommandArgument='<%#Eval("trainingrequestunkid")%>'>
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnComplete"
                                                                                Visible="false">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="ImgComplete" runat="server" ToolTip="Complete" OnClick="lnkComplete_Click"
                                                                                            CommandArgument='<%#Eval("trainingrequestunkid")%>'>
                                                                                               <i class="fas fa-check-circle"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnViewAttachment">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="lnkViewAttachment" runat="server" ToolTip="View Attachment" OnClick="lnkViewAttachment_Click"
                                                                                            CommandArgument='<%#Eval("trainingrequestunkid")%>'>
                                                                                            <i class="fas fa fa-eye" style="font-size:15px;"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="application_date" HeaderText="Application Date" ReadOnly="true"
                                                                                FooterText="colhapplication_date" />
                                                                            <%--<asp:BoundField DataField="Training" HeaderText="Training" ReadOnly="true" FooterText="colhTraining" />--%>
                                                                            <asp:BoundField DataField="Employee" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                                            <asp:BoundField DataField="ApproverName" HeaderText="Approver Name" ReadOnly="true"
                                                                                FooterText="colhApproverName" />
                                                                            <asp:BoundField DataField="trainingcostemp" HeaderText="Training Cost" ItemStyle-HorizontalAlign="Right"
                                                                                ReadOnly="true" FooterText="colhTrainingcostemp" Visible="false" />
                                                                            <asp:BoundField DataField="approvedamountemp" HeaderText="Approved Amount" ReadOnly="true"
                                                                                ItemStyle-HorizontalAlign="Right" FooterText="colhApprovedamountemp" Visible="false" />
                                                                            <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="colhStatus" />
                                                                            <asp:BoundField DataField="Completed_Status" HeaderText="Completion Status" ReadOnly="true"
                                                                                Visible="false" FooterText="colhCompletedStatus" />
                                                                            <asp:BoundField DataField="Isgrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" FooterText="btnTrainingRequestForm">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ImgTrainingRequestForm" runat="server" ToolTip="Training Request Form"
                                                                    OnClick="lnkTrainingRequestForm_Click"> 
                                                                                        <i class="fas fa-print style="font-size:15px; color:Black" "></i>
                                                                                        
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="refno" HeaderText="Ref. No" ReadOnly="true" FooterText="colhRefno" />
                                                        <asp:BoundField DataField="CreateUserName" HeaderText="Requestor" ReadOnly="true"
                                                            FooterText="colhRequestor" />
                                                        <asp:BoundField DataField="Training" HeaderText="Training" ReadOnly="true" FooterText="colhTraining" />
                                                        <asp:BoundField DataField="allocationtranname" HeaderText="Allocation" ReadOnly="true"
                                                            FooterText="colhAllocation" />
                                                        <asp:BoundField DataField="start_date" HeaderText="Start Date" ReadOnly="true" FooterText="colhStartDate" />
                                                        <asp:BoundField DataField="end_date" HeaderText="End Date" ReadOnly="true" FooterText="colhEndDate" />
                                                        <asp:BoundField DataField="total_training_cost" HeaderText="Total Training Cost"
                                                            ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="colhTotal_training_cost" />
                                                        <asp:BoundField DataField="approved_amount" HeaderText="Total Approved Amount" ReadOnly="true"
                                                            ItemStyle-HorizontalAlign="Right" FooterText="colhApproved_amount" />
                                                        <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="d-none" ItemStyle-CssClass="d-none">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hfPrevApprovedAmount" runat="server" Value='<%# Eval("PrevApprovedAmount") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlApproval" runat="server">
                                    <div class="row clearfix ">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblApprovedAmount" runat="server" Text="Approved Amount" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line" style="text-align: right;">
                                                    <asp:TextBox ID="txtApprovedAmount" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblApprRejectRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtApprRejectRemark" runat="server" TextMode="MultiLine" Rows="2"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30 d--f">
                                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary " />
                                            <asp:Button ID="btnDisapprove" runat="server" Text="Reject" class="btn btn-default" />
                                            <asp:Button ID="btnReturnApplicant" runat="server" Text="Return To Applicant" CssClass="btn btn-default" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close"
                                    Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_ScanAttachment" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ScanAttachment" PopupControlID="pnl_ScanAttachment" CancelControlID="hdf_ScanAttachment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttachment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attachment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="max-height: 300px;">
                                    <asp:GridView ID="dgvAttachment" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                        RowStyle-Wrap="false" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%"
                                        DataKeyNames="scanattachtranunkid, filepath, GUID,form_name">
                                        <Columns>
                                            <asp:TemplateField FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>"
                                                            ToolTip="Download"><i class="fas fa-download"></i></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhFileName" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttachment" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgvAttachment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function() {
            setevent();
        });

        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            setevent();
        }

        function setevent() {
            $("[src*=plus]").on("click", function() {
                if ($(this)[0].src.includes('plus') == true) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "../../images/minus.png");
                }
                else {
                    $(this).attr("src", "../../images/plus.png");
                    $(this).closest("tr").next().remove();
                }
            });

            $("input[id*=chkSelect]").on("change", function() {
                if ($(this).prop('checked') == true) {
                    $("input[id*=chkSelect]").prop('checked', false);
                    $(this).prop('checked', true);
                    $$('txtApprovedAmount').val($(this).parents("tr:first").find('[id*=hfPrevApprovedAmount]').val());
                }
            });
        }
    </script>

</asp:Content>
