﻿#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class Training_Training_Request_Approval_wPg_TrainingRequestApprovalList
    Inherits Basepage

#Region " Private Variable "
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTrainingRequestApprovalList"
    Private mstrAdvanceFilter As String = ""
    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    Private mintApproverLevelPriority As Integer
    Private objCONN As SqlConnection
    'Hemant (07 Mar 2022) -- End
    'Hemant (03 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
    Private mintTrainingRequestUnkid As Integer = -1
    Private mblnShowAttachmentPopup As Boolean = False
    'Hemant (03 Oct 2022) -- End
    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Private mintGroupTrainingRequestunkid As Integer
    'Hemant (12 Oct 2022) -- End
    'Hemant (10 Nov 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
    Private mdicApproverLevelPriority As New Dictionary(Of Integer, Integer)
    'Hemant (10 Nov 2022) -- End
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                If Request.QueryString.ToString.Contains("uploadimage") = False Then

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 3 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))
                        Session("mintMappingUnkid") = CInt(arr(2))



                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing)
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If


                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Session("UserId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub

                            Else

                                If mblnATLoginRequiredToApproveApplications = False Then

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions

                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))


                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        'If CBool(Session("IsArutiDemo")) = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False) Then
                        '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        '    Exit Sub
                        'End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management)) = False Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If


                        Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                        Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                        If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                        End If

                        Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim()


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try

                        'Dim base As New Basepage
                        'If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(Session("Employeeunkid"))) = False Then
                        '    DisplayMessage.DisplayMessage(strError, Me.Page)
                        '    Exit Try
                        'End If

                        Call GetDatabaseVersion()


                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid



                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                            Exit Sub
                        End If


                                End If  ' If mblnATLoginRequiredToApproveApplications = False Then


                            End If ' If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub
                        End If  'If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then


                    End If  ' If arr.Length = 3 Then

                End If  ' If Request.QueryString.ToString.Contains("uploadimage") = False Then

                CType(Me.Master.FindControl("pnlMenuWrapper"), Panel).Visible = False

            End If  '  If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count <= 0 Then
                'Hemant (07 Mar 2022) --  [Request.QueryString.Count <= 0]
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillCombo()
                SetApprovalData()
                FillList()

                'Hemant (07 Mar 2022) -- Start            
                'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Else
                mintApproverLevelPriority = CInt(Me.ViewState("mintApproverLevelPriority"))
                'Hemant (07 Mar 2022) -- End
                'Hemant (03 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
                mintTrainingRequestUnkid = CInt(Me.ViewState("mintTrainingRequestUnkid"))
                mblnShowAttachmentPopup = Convert.ToBoolean(Me.ViewState("mblnShowAttachmentPopup"))
                'Hemant (03 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                mintGroupTrainingRequestunkid = CInt(Me.ViewState("mintGroupTrainingRequestunkid"))
                'Hemant (12 Oct 2022) -- End
                'Hemant (10 Nov 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                mdicApproverLevelPriority = Me.ViewState("mdicApproverLevelPriority")
                'Hemant (10 Nov 2022) -- End
            End If
            If mblnShowAttachmentPopup Then
                popup_ScanAttachment.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Pinkal (23-Feb-2024) -- Start
        '(A1X-2461) NMB : R&D - Force manual user login on approval links.
        If Request.QueryString.Count <= 0 Then
        Me.IsLoginRequired = True
        End If
        'Pinkal (23-Feb-2024) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Me.ViewState("mintApproverLevelPriority") = mintApproverLevelPriority
            'Hemant (07 Mar 2022) -- End
            'Hemant (03 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
            Me.ViewState("mintTrainingRequestUnkid") = mintTrainingRequestUnkid
            Me.ViewState("mblnShowAttachmentPopup") = mblnShowAttachmentPopup
            'Hemant (03 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            Me.ViewState("mintGroupTrainingRequestunkid") = mintGroupTrainingRequestunkid
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
            Me.ViewState("mdicApproverLevelPriority") = mdicApproverLevelPriority
            'Hemant (10 Nov 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objApprovalMaster As New clstraining_requisition_approval_master
        Dim dsCombo As New DataSet
        'Hemant (03 Dec 2021) -- Start
        'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
        Dim objTPeriod As New clsTraining_Calendar_Master
        'Hemant (03 Dec 2021) -- End
        Try
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.  
            Dim intFirstOpen As Integer = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                intFirstOpen = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If

            dsCombo = objTPeriod.getListForCombo("List", True, 0)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen
            End With
            'Hemant (03 Dec 2021) -- End

            dsCombo = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

            With drpTraining
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", True)
            With drpemp
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objApprovalMaster.getStatusComboList("List", True)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

            dsCombo = objApprovalMaster.getCompletionStatusComboList("List", True)
            With cboCompletionStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            objTPeriod = Nothing
            'Hemant (03 Dec 2021) -- End
            objEmp = Nothing
            objApprovalMaster = Nothing
        End Try
    End Sub

    Private Sub SetApprovalData(Optional ByVal intTrainingTypeId As Integer = 0)
        'Hemant (29 Apr 2022) -- [intTrainingTypeId]

        'Hemant (07 Mar 2022) -- Start            
        'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
        'Hemant (25 Jul 2022) -- Start            
        'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
        'Dim objApprover As New clstraining_approver_master
        'Hemant (25 Jul 2022) -- End
        'Hemant (07 Mar 2022) -- End
        Dim objlevel As New clstraining_approverlevel_master
        Dim dsList As New DataSet
        Try
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
            If CInt(Session("TrainingRequestApprovalSettingID")) = enTrainingRequestApproval.ApproverEmpMapping Then
                Dim objApprover As New clstraining_approverlevel_master
                dsList = objApprover.GetLevelFromUserLogin(CInt(Session("UserId")), CInt(cboPeriod.SelectedValue))
                'Hemant (22 Dec 2022) -- [CInt(cboPeriod.SelectedValue)]
                If dsList.Tables("List").Rows.Count > 0 Then
                    txtApprover.Text = dsList.Tables(0).Rows(0)("ApproverName").ToString()
                    txtApprover.Attributes.Add("approverunkid", dsList.Tables("List").Rows(0).Item("approverunkid").ToString())
                    objlevel._Levelunkid = CInt(dsList.Tables(0).Rows(0)("levelunkid").ToString())
                    mintApproverLevelPriority = objlevel._Priority
                    'Hemant (10 Nov 2022) -- Start
                    'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                    'Hemant (30 Aug 2024) -- Start
                    'ISSUE(NMB): A1X - 2750 :  NMB - Training approval screen performance optimization
                    'mdicApproverLevelPriority = (From p In dsList.Tables("List") Select New With {Key .trainingtypeid = CInt(p.Item("trainingtypeid")), Key .priority = CInt(p.Item("priority"))}).ToDictionary(Function(x) x.trainingtypeid, Function(y) y.priority)
                    'Hemant (30 Aug 2024) -- End
                    'Hemant (10 Nov 2022) -- End
                End If
                'Hemant (25 Jul 2022) -- Start            
                'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                objApprover = Nothing
                pnlApprover.Visible = True
            Else
                Dim objApprover As New clstraining_approver_master
                'Hemant (25 Jul 2022) -- End
                dsList = objApprover.GetList("List", True, "", , Nothing, False, CInt(Session("RoleId")), intTrainingTypeId)
                'Hemant (29 Apr 2022) -- [intTrainingTypeId]

                If dsList.Tables("List").Rows.Count > 0 Then
                    txtRole.Text = dsList.Tables("List").Rows(0).Item("role").ToString()
                    txtRole.Attributes.Add("mappingunkid", dsList.Tables("List").Rows(0).Item("mappingunkid").ToString())
                    'Hemant (07 Mar 2022) -- Start            
                    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                    objApprover._Mappingunkid = CInt(IIf(CInt(dsList.Tables("List").Rows(0).Item("mappingunkid")) <= 0, 0, CInt(dsList.Tables("List").Rows(0).Item("mappingunkid"))))
                    objlevel._Levelunkid = objApprover._Levelunkid
                    mintApproverLevelPriority = objlevel._Priority
                    'Hemant (07 Mar 2022) -- End
                End If
                'Hemant (25 Jul 2022) -- Start            
                'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                objApprover = Nothing
                pnlRole.Visible = True
            End If
            'Hemant (25 Jul 2022) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
            'objApprover = Nothing
            'Hemant (25 Jul 2022) -- End
            dsList.Dispose()
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            objlevel = Nothing
            'Hemant (07 Mar 2022) -- End
        End Try
    End Sub

    Private Sub FillList(Optional ByVal blnBindGridView As Boolean = True)
        'Sohail (15 Mar 2022) - [blnBindGridView]

        Dim objApprovaltran As New clstrainingapproval_process_tran
        Dim dtApprovalList As New DataTable
        Dim mstrEmployeeIDs As String = ""
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
            If CInt(Session("TrainingRequestApprovalSettingID")) > 0 Then
                strSearch &= "AND trtraining_request_master.approvalsettingid = " & CInt(Session("TrainingRequestApprovalSettingID")) & " "
            End If
            'Hemant (25 Jul 2022) -- End
            If CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If
            'Hemant (03 Dec 2021) -- End

            If CInt(drpemp.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.employeeunkid = " & CInt(drpemp.SelectedValue) & " "
            End If

            If CInt(drpTraining.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.coursemasterunkid = " & CInt(drpTraining.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                'Hemant (03 Oct 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : AC2-911 - NMB - As a user, on the training request approval page, I want to see only training that are pending on that particular approver only
                'strSearch &= "AND trtraining_request_master.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                strSearch &= "AND A.ApprovalStatus = " & CInt(cboStatus.SelectedValue) & " "
                'Hemant (03 Oct 2022) -- End
            End If

            If CInt(cboCompletionStatus.SelectedValue) > 0 Then
                strSearch &= "AND trtraining_request_master.completed_statusunkid = " & CInt(cboCompletionStatus.SelectedValue) & " "
            End If

            strSearch &= "AND trtrainingapproval_process_tran.visibleid <> -1 " & " "

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
            objApprovaltran._TrainingApprovalSettingID = CInt(Session("TrainingRequestApprovalSettingID"))
            'Hemant (25 Jul 2022) -- End
            dsList = objApprovaltran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                             CInt(Session("TrainingNeedAllocationID")), "List", strSearch, mstrAdvanceFilter, chkMyApproval.Checked, True)
            'Sohail (15 Mar 2022) - [TrainingNeedAllocationID, blnAddGrouping]

            Dim mintTrainingRequestunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""
            Dim mstrCompletedStatus As String = ""

            Dim strApprovedByText As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ")
            Dim strRejectedByTexr As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ")
            'If dsList.Tables(0) IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    Dim dvView As DataView = dsList.Tables(0).DefaultView
            '    dvView.RowFilter = "IsGrp = False"

            '    For Each vRow As DataRowView In dvView

            '        mstrStaus = ""

            '        If mintTrainingRequestunkid <> CInt(vRow("trainingrequestunkid")) Then
            '            dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(vRow("employeeunkid")) & " AND trainingrequestunkid = " & CInt(vRow("trainingrequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
            '            mintTrainingRequestunkid = CInt(vRow("trainingrequestunkid"))
            '            mstrStaus = ""
            '            mstrCompletedStatus = ""
            '        End If

            '        If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
            '            Dim dr As DataRow() = dList.Select("priority >= " & CInt(vRow("priority")))

            '            If dr.Length > 0 Then

            '                For i As Integer = 0 To dr.Length - 1

            '                    If CInt(vRow("statusunkid")) = 2 Then
            '                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & vRow("ApproverName").ToString()
            '                        Exit For

            '                    ElseIf CInt(vRow("statusunkid")) = 1 Then

            '                        If CInt(dr(i)("statusunkid")) = 2 Then
            '                            mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        ElseIf CInt(dr(i)("statusunkid")) = 3 Then
            '                            mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        End If

            '                    ElseIf CInt(vRow("statusunkid")) = 3 Then
            '                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & vRow("ApproverName").ToString()
            '                        Exit For

            '                    End If

            '                Next
            '                For i As Integer = 0 To dr.Length - 1

            '                    If CInt(vRow("completed_statusunkid")) = 2 Then
            '                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & vRow("ApproverName").ToString()
            '                        Exit For

            '                    ElseIf CInt(vRow("completed_statusunkid")) = 1 Then

            '                        If CInt(dr(i)("completed_statusunkid")) = 2 Then
            '                            mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        ElseIf CInt(dr(i)("completed_statusunkid")) = 3 Then
            '                            mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
            '                            Exit For

            '                        End If

            '                    ElseIf CInt(vRow("completed_statusunkid")) = 3 Then
            '                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & vRow("ApproverName").ToString()
            '                        Exit For

            '                    End If

            '                Next

            '            End If

            '        End If

            '        If mstrStaus <> "" Then
            '            vRow("status") = mstrStaus.Trim
            '        End If
            '        If mstrCompletedStatus <> "" Then
            '            vRow("completed_status") = mstrCompletedStatus.Trim
            '        End If


            '    Next
            '    dsList.Tables(0).AcceptChanges()

                'For Each drRow As DataRow In dsList.Tables(0).Rows

                '    'Sohail (15 Mar 2022) -- Start
                '    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
                '    'If CInt(drRow("pendingtrainingtranunkid")) <= 0 Then Continue For
                '    If IsDBNull(drRow("pendingtrainingtranunkid")) = True OrElse CInt(drRow("pendingtrainingtranunkid")) <= 0 Then Continue For
                '    'Sohail (15 Mar 2022) -- End
                '    mstrStaus = ""

                '    If mintTrainingRequestunkid <> CInt(drRow("trainingrequestunkid")) Then
                '        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND trainingrequestunkid = " & CInt(drRow("trainingrequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                '        mintTrainingRequestunkid = CInt(drRow("trainingrequestunkid"))
                '        mstrStaus = ""
                '        mstrCompletedStatus = ""
                '    End If

                '    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                '        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                '        If dr.Length > 0 Then

                '            For i As Integer = 0 To dr.Length - 1

                '                If CInt(drRow("statusunkid")) = 2 Then
                '                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & drRow("ApproverName").ToString()
                '                    Exit For

                '                ElseIf CInt(drRow("statusunkid")) = 1 Then

                '                    If CInt(dr(i)("statusunkid")) = 2 Then
                '                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                '                        Exit For

                '                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                '                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                '                        Exit For

                '                    End If

                '                ElseIf CInt(drRow("statusunkid")) = 3 Then
                '                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & drRow("ApproverName").ToString()
                '                    Exit For

                '                End If

                '            Next
                '            For i As Integer = 0 To dr.Length - 1

                '                If CInt(drRow("completed_statusunkid")) = 2 Then
                '                    mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & drRow("ApproverName").ToString()
                '                    Exit For

                '                ElseIf CInt(drRow("completed_statusunkid")) = 1 Then

                '                    If CInt(dr(i)("completed_statusunkid")) = 2 Then
                '                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Approved By :-  ") & dr(i)("ApproverName").ToString()
                '                        Exit For

                '                    ElseIf CInt(dr(i)("completed_statusunkid")) = 3 Then
                '                        mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & dr(i)("ApproverName").ToString()
                '                        Exit For

                '                    End If

                '                ElseIf CInt(drRow("completed_statusunkid")) = 3 Then
                '                    mstrCompletedStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Rejected By :-  ") & drRow("ApproverName").ToString()
                '                    Exit For

                '                End If

                '            Next

                '        End If

                '    End If

                '    If mstrStaus <> "" Then
                '        drRow("status") = mstrStaus.Trim
                '    End If
                '    If mstrCompletedStatus <> "" Then
                '        drRow("completed_status") = mstrCompletedStatus.Trim
                '    End If

                'Next

            'End If

            dtApprovalList = dsList.Tables(0).Clone

            dtApprovalList.Columns("application_date").DataType = GetType(Object)

            Dim dtRow As DataRow

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dtRow = dtApprovalList.NewRow
                For Each dtCol As DataColumn In dtApprovalList.Columns
                    dtRow(dtCol.ColumnName) = dRow(dtCol.ColumnName)
                Next

                'If CBool(dRow("Isgrp")) = True Then
                '    dtRow("application_date") = dRow("application_no")
                'End If
                dtApprovalList.Rows.Add(dtRow)
            Next

            dtApprovalList.AcceptChanges()

            Dim dtTable As New DataTable
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            'If chkMyApproval.Checked Then
            '    dtTable = New DataView(dtApprovalList, "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dtApprovalList, "", "", DataViewRowState.CurrentRows).ToTable
            'End If
            'Hemant (21 Mar 2022) -- Start            
            'dtTable = New DataView(dtApprovalList, "", "", DataViewRowState.CurrentRows).ToTable
            If chkMyApproval.Checked Then
                dtTable = New DataView(dtApprovalList, "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0 OR mapuserunkid IS NULL", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dtApprovalList, "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Hemant (21 Mar 2022) -- End
            'Sohail (15 Mar 2022) -- End

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-451 - Training Completion Screen - Forces user to attach Doc even when Cert not required.
            'Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "trainingrequestunkid", "employeeunkid")

            'Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("trainingrequestunkid") Equals drTemp.Field(Of Integer)("trainingrequestunkid") And _
            '             drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable

            'If dRow1.Count > 0 Then
            '    dRow1.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            'End If

            'gvTrainingApprovalList.Columns(0).Visible = CBool(Session("AllowToChangeLoanStatus"))
            Dim xRow() As DataRow = Nothing
            Dim dt As New DataTable
            Dim xTable As New DataTable
            Dim dtFinalTable As New DataTable
            dt = New DataView(dtTable, "mapuserunkid = " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            'Dim dtTemp As DataTable = New DataView(dt, "", "", DataViewRowState.CurrentRows).ToTable(True, "trainingrequestunkid", "employeeunkid")
            Dim dtTemp As DataTable = New DataView(dt, "", "", DataViewRowState.CurrentRows).ToTable(True, "trainingrequestunkid", "employeeunkid", "refno")
            dtFinalTable = dtTable.Clone
            'Sohail (15 Mar 2022) -- End
            For Each drRow As DataRow In dtTemp.Rows
                xRow = dtTable.Select("trainingrequestunkid =" & drRow.Item("trainingrequestunkid"))
                If xRow.Length > 0 Then
                    xTable = xRow.CopyToDataTable()
                    dtFinalTable.Merge(xTable, True)
                    'Sohail (15 Mar 2022) -- Start
                    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
                    If dtFinalTable.Select("refno = '" & drRow.Item("refno") & "' AND IsGrp = 1 ").Length <= 0 Then
                        xRow = Nothing
                        xRow = dtTable.Select("refno = '" & drRow.Item("refno") & "' AND IsGrp = 1 ")
                        If xRow.Length > 0 Then
                            xTable = xRow.CopyToDataTable()
                            dtFinalTable.Merge(xTable, True)
                        End If
                    End If
                    'Sohail (15 Mar 2022) -- End
                End If

            Next
            dt = Nothing

            If dtFinalTable IsNot Nothing AndAlso dtFinalTable.Rows.Count > 0 Then
                Dim dvView As DataView = dtFinalTable.DefaultView
                dvView.RowFilter = "IsGrp = False"

                For Each vRow As DataRowView In dvView

                    mstrStaus = ""

                    If mintTrainingRequestunkid <> CInt(vRow("trainingrequestunkid")) Then
                        dList = New DataView(dsList.Tables(0), "employeeunkid = " & CInt(vRow("employeeunkid")) & " AND trainingrequestunkid = " & CInt(vRow("trainingrequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintTrainingRequestunkid = CInt(vRow("trainingrequestunkid"))
                        mstrStaus = ""
                        mstrCompletedStatus = ""
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(vRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(vRow("statusunkid")) = 2 Then
                                    mstrStaus = strApprovedByText & vRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(vRow("statusunkid")) = 1 Then

                                    If CInt(dr(i)("statusunkid")) = 2 Then
                                        mstrStaus = strApprovedByText & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = strRejectedByTexr & dr(i)("ApproverName").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(vRow("statusunkid")) = 3 Then
                                    mstrStaus = strRejectedByTexr & vRow("ApproverName").ToString()
                                    Exit For

                                End If

                            Next
                            For i As Integer = 0 To dr.Length - 1

                                If CInt(vRow("completed_statusunkid")) = 2 Then
                                    mstrCompletedStatus = strApprovedByText & vRow("ApproverName").ToString()
                                    Exit For

                                ElseIf CInt(vRow("completed_statusunkid")) = 1 Then

                                    If CInt(dr(i)("completed_statusunkid")) = 2 Then
                                        mstrCompletedStatus = strApprovedByText & dr(i)("ApproverName").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("completed_statusunkid")) = 3 Then
                                        mstrCompletedStatus = strRejectedByTexr & dr(i)("ApproverName").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(vRow("completed_statusunkid")) = 3 Then
                                    mstrCompletedStatus = strRejectedByTexr & vRow("ApproverName").ToString()
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        vRow("status") = mstrStaus.Trim
                    End If
                    If mstrCompletedStatus <> "" Then
                        vRow("completed_status") = mstrCompletedStatus.Trim
                    End If


                Next
            End If
            dtFinalTable.AcceptChanges()

            'Hemant (30 Aug 2024) -- Start
            'ISSUE(NMB): A1X - 2750 :  NMB - Training approval screen performance optimization
            'Dim dsApproverPriorityList As New DataSet
            'If CInt(Session("TrainingRequestApprovalSettingID")) = enTrainingRequestApproval.ApproverEmpMapping Then
            '    dsApproverPriorityList = (New clstraining_approverlevel_master).GetLevelFromUserLogin(CInt(Session("UserId")), CInt(cboPeriod.SelectedValue))
            'End If
            'Hemant (30 Aug 2024) -- End

            'Hemant (21 Mar 2022) -- Start    
            Dim lstTrainingRequestIDs As List(Of String) = Nothing
            lstTrainingRequestIDs = dtFinalTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
                                                  .Select(Function(x) x.Field(Of Integer)("trainingrequestunkid").ToString()).ToList().Distinct.ToList
            Dim dsTrainingApproval As DataSet
            dsTrainingApproval = objApprovaltran.GetApprovalDataByRefNo(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                        CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                                                        CInt(Session("TrainingNeedAllocationID")), "List", lstTrainingRequestIDs, strSearch, mstrAdvanceFilter)
            For Each drFinalTableRow As DataRow In dtFinalTable.Select("IsGrp = 1")
                'Hemant (10 Nov 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                'If mdicApproverLevelPriority.ContainsKey(drFinalTableRow.Item("trainingtypeid")) Then
                '    mintApproverLevelPriority = mdicApproverLevelPriority(drFinalTableRow.Item("trainingtypeid"))
                'End If
                'If dsApproverPriorityList IsNot Nothing AndAlso dsApproverPriorityList.Tables.Count > 0 AndAlso dsApproverPriorityList.Tables(0).Rows.Count > 0 Then
                '    Dim drApproverPriority() As DataRow = dsApproverPriorityList.Tables(0).Select("levelunkid = " & CInt(drFinalTableRow.Item("levelunkid")) & " AND trainingtypeid = " & CInt(drFinalTableRow.Item("trainingtypeid")) & " ")
                '    If drApproverPriority.Length > 0 Then
                '        mintApproverLevelPriority = CInt(drApproverPriority(0)("priority").ToString())
                '    End If
                'End If
                Dim drApproverPriority() As DataRow = dtFinalTable.Select("IsGrp = 0 AND refno = '" & drFinalTableRow.Item("refno").ToString() & "' ")
                    If drApproverPriority.Length > 0 Then
                        mintApproverLevelPriority = CInt(drApproverPriority(0)("priority").ToString())
                    End If
                'Hemant (10 Nov 2022) -- End
                Dim drTemp() As DataRow = dsTrainingApproval.Tables(0).Select("refno = '" & drFinalTableRow.Item("refno") & "' AND priority = " & mintApproverLevelPriority & "")
                Dim decTotalApprvedAmt As Decimal = 0
                If drTemp.Length > 0 Then
                    If IsDBNull(dsTrainingApproval.Tables(0).Compute("SUM(approvedamount)", "refno = '" & drFinalTableRow.Item("refno") & "' AND priority = " & mintApproverLevelPriority & "")) = False Then
                        decTotalApprvedAmt = dsTrainingApproval.Tables(0).Compute("SUM(approvedamount)", "refno = '" & drFinalTableRow.Item("refno") & "' AND priority = " & mintApproverLevelPriority & "")
                    End If
                End If
                drFinalTableRow.Item("approved_amount") = decTotalApprvedAmt

                If IsDBNull(dsTrainingApproval.Tables(0).Compute("Max(priority)", "refno = '" & drFinalTableRow.Item("refno") & "' AND priority < " & mintApproverLevelPriority & "")) = False Then
                    Dim intMinPriority As Integer = CInt(dsTrainingApproval.Tables(0).Compute("Max(priority)", "refno = '" & drFinalTableRow.Item("refno") & "' AND priority < " & mintApproverLevelPriority & ""))
                    If intMinPriority > 0 Then
                        Dim drPrevApprvedTemp() As DataRow = dsTrainingApproval.Tables(0).Select("refno = '" & drFinalTableRow.Item("refno") & "' AND priority = " & intMinPriority & "")
                        Dim decTotalPrevApprovedAmt As Decimal = 0
                        If drPrevApprvedTemp.Length > 0 Then
                            If IsDBNull(dsTrainingApproval.Tables(0).Compute("SUM(approvedamount)", "refno = '" & drFinalTableRow.Item("refno") & "' AND priority = " & intMinPriority & "")) = False Then
                                decTotalPrevApprovedAmt = dsTrainingApproval.Tables(0).Compute("SUM(approvedamount)", "refno = '" & drFinalTableRow.Item("refno") & "' AND priority = " & intMinPriority & "")
                            End If
                        End If
                        drFinalTableRow.Item("prevapprovedamount") = Format(decTotalPrevApprovedAmt, Session("fmtCurrency"))
                    Else
                        drFinalTableRow.Item("prevapprovedamount") = Format(CDec(drFinalTableRow("total_training_cost")), Session("fmtCurrency"))
                    End If
                Else
                    drFinalTableRow.Item("prevapprovedamount") = Format(CDec(drFinalTableRow("total_training_cost")), Session("fmtCurrency"))
                End If

            Next
            'Hemant (21 Mar 2022) -- End


            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            'xTable = Nothing
            ''Hemant (20 Aug 2021) -- End
            dtTable = dtFinalTable.Copy
            If Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval") IsNot Nothing Then
                Cache.Remove(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval")
            End If
            If Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval") Is Nothing Then
                Cache.Insert(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval", dtTable, New CacheDependency(Server.MapPath(CStr(Session("Database_Name")) & "_DTTrainingApproval.xml")), DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration)
            Else
                dtTable = CType(Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval"), DataTable)
            End If
            dtFinalTable = New DataView(dtTable, "IsGrp = 1 ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (15 Mar 2022) -- End

            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            'gvTrainingApprovalList.AutoGenerateColumns = False
            'gvTrainingApprovalList.DataSource = dtFinalTable
            'gvTrainingApprovalList.DataBind()
            If blnBindGridView = True Then
                gvSummary.AutoGenerateColumns = False
                gvSummary.DataSource = dtFinalTable
                gvSummary.DataBind()
            End If

            If dtFinalTable IsNot Nothing AndAlso dtFinalTable.Rows.Count > 0 And CInt(cboStatus.SelectedValue) = enTrainingRequestStatus.PENDING Then
                pnlApproval.Visible = True
            Else
                pnlApproval.Visible = False
            End If
            Clear_Controls()
            'Sohail (15 Mar 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
        End Try
    End Sub

    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    Private Function IsValidate() As Boolean
        Try
            'Hemant (11 Oct 2024) -- Start
            'ENHANCEMENT(Neotech): A1X - 2800 :  Setting to skip training budget approval process
            'If Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper AndAlso CDec(txtApprovedAmount.Text) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, Approved Amount is mandatory information. Please enter the Approved Amount to continue"), Me)
            '    Return False
            'End If
            'Hemant (11 Oct 2024) -- End
            If CInt(txtApprRejectRemark.Text.Length) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Approver Remark is mandatory information. Please select Approver Remark to continue"), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Clear_Controls()
        Try
            txtApprovedAmount.Text = Format(0, Session("fmtCurrency"))
            txtApprRejectRemark.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function IsValidForMaxBudget(ByVal decAmount As Decimal, ByVal gvGridView As GridView) As Boolean
        Dim lstEmployeeList As List(Of String) = New List(Of String)
        Dim objTrainingRequest As New clstraining_request_master
        Dim objTNeedMaster As New clsDepartmentaltrainingneed_master
        Dim decAmountEmp As Decimal
        Try
            If CInt(Session("TrainingNeedAllocationID")) = CInt(Session("TrainingBudgetAllocationID")) Then Return True

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvGridView.Rows.Cast(Of GridViewRow).Where(Function(x) gvGridView.DataKeys(x.RowIndex).Item("mapuserunkid").ToString = CInt(Session("UserId")) _
                                                                  AndAlso CInt(gvGridView.DataKeys(x.RowIndex).Item("statusunkid").ToString) = 1 _
                                                                  AndAlso CInt(gvGridView.DataKeys(x.RowIndex).Item("visibleid").ToString) = 1)

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                lstEmployeeList = gRow.AsEnumerable().Select(Function(x) gvGridView.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
            End If

            decAmountEmp = decAmount / lstEmployeeList.Count

            Dim dtEmployeeAllocationData, dtSummaryData As DataTable
            dtEmployeeAllocationData = objTrainingRequest.GetTrainingRequestedEmployeeAllocationData(CInt(cboPeriod.SelectedValue), CInt(Session("TrainingBudgetAllocationID")), CInt(Session("TrainingCostCenterAllocationID")), lstEmployeeList)
            Dim strAllocationName As String = ""
            dtSummaryData = objTNeedMaster.GetBudgetSummaryData(CInt(cboPeriod.SelectedValue), 0, CInt(Session("TrainingNeedAllocationID")), CInt(Session("TrainingBudgetAllocationID")), CInt(Session("TrainingCostCenterAllocationID")), CInt(Session("TrainingRemainingBalanceBasedOnID")), strAllocationName, , True)

            If dtEmployeeAllocationData IsNot Nothing AndAlso dtEmployeeAllocationData.Rows.Count > 0 AndAlso _
                dtSummaryData IsNot Nothing AndAlso dtSummaryData.Rows.Count > 0 Then
                For Each drRow As DataRow In dtEmployeeAllocationData.Rows
                    Dim drSummaryData() As DataRow = dtSummaryData.Select("allocationid =" & CInt(drRow.Item("allocationid")) & " AND departmentunkid = " & CInt(drRow.Item("allocationtranunkid")) & " ")
                    If drSummaryData.Count > 0 Then
                        Dim decMaxBudget, decTotalApprovedAmount, decTotalCurrentPendingRequestedAmt As Decimal
                        decMaxBudget = CDec(drSummaryData(0).Item("currmaxbudget_amt"))
                        decTotalApprovedAmount = CDec(drSummaryData(0).Item("CurrentApprovedAmt"))
                        decTotalCurrentPendingRequestedAmt = CDec(drSummaryData(0).Item("CurrentPendingRequestedAmt"))
                        If (decTotalApprovedAmount + decAmountEmp) > decMaxBudget Then
                            Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department.")
                            'Hemant (21 Mar 2022) -- Start            
                            'ISSUE/ENHANCEMENT(NMB) : Message should end on the circled section only. Don’t go into details.
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1031, "Employee : #employeename#").Replace("#employeename#", drRow.Item("employee").ToString)
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1032, "Approved Total : #approvedtotal#").Replace("#approvedtotal#", Format(decTotalApprovedAmount, CStr(Session("fmtCurrency"))))
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1033, "Employee Requested Amount : #employeerequestedamount#").Replace("#employeerequestedamount#", Format(decAmountEmp, CStr(Session("fmtCurrency"))))
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1036, "Total Requested Amount : #totalrequestedamount#").Replace("#totalrequestedamount#", Format(decTotalCurrentPendingRequestedAmt, CStr(Session("fmtCurrency"))))
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= "\n"
                            'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1035, "Maximum Budget : #maxbudget#").Replace("#maxbudget#", Format(decMaxBudget, CStr(Session("fmtCurrency"))))
                            'Hemant (21 Mar 2022) -- End

                            DisplayMessage.DisplayMessage(strMsg, Me)
                            Return False
                        Else
                            drSummaryData(0).Item("CurrentApprovedAmt") = CDec(drSummaryData(0).Item("CurrentApprovedAmt")) + decAmountEmp
                            dtSummaryData.AcceptChanges()
                        End If
                    Else
                        Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department.")
                        'Hemant (21 Mar 2022) -- Start            
                        'ISSUE/ENHANCEMENT(NMB) : Message should end on the circled section only. Don’t go into details.
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1031, "Employee : #employeename#").Replace("#employeename#", drRow.Item("employee").ToString)
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1032, "Approved Total : #approvedtotal#").Replace("#approvedtotal#", Format(0, CStr(Session("fmtCurrency"))))
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1033, "Requested Amount : #requestedamount#").Replace("#requestedamount#", Format(decAmountEmp, CStr(Session("fmtCurrency"))))
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1036, "Total Requested Amount : #totalrequestedamount#").Replace("#decTotalCurrentRequestedAmt#", Format(0, CStr(Session("fmtCurrency"))))
                        'strMsg &= "\n"
                        'strMsg &= "\n"
                        'strMsg &= Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1035, "Maximum Budget : #maxbudget#").Replace("#maxbudget#", Format(0, CStr(Session("fmtCurrency"))))
                        'Hemant (21 Mar 2022) -- End

                        DisplayMessage.DisplayMessage(strMsg, Me)
                        Return False
                    End If
                Next

            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingRequest = Nothing
            objTNeedMaster = Nothing
        End Try
    End Function
    'Hemant (07 Mar 2022) -- End

    'Hemant (03 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
    Private Sub FillAttachment()
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTable As DataTable
        Try
            If mintTrainingRequestunkid > 0 Then
                dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestUnkid, "")
                If dtTable.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "No file to download"), Me)
                    Exit Sub
                End If

            Else
                dtTable = Nothing
            End If
            dgvAttachment.AutoGenerateColumns = False
            dgvAttachment.DataSource = dtTable
            dgvAttachment.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (03 Oct 2022) -- End


#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
            'Hemant (03 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
            Me.ViewState("mblnShowAttachmentPopup") = False
            'Hemant (03 Oct 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, _
                                                                                                btnDisapprove.Click, _
                                                                                                btnReturnApplicant.Click

        'Hemant (19 Jul 2024) -- [btnReturnApplicant.Click]
        Try
            Select Case CType(sender, Button).ID.ToUpper
                Case btnDisapprove.ID.ToUpper
                    If txtApprRejectRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, Remark is mandatory information. Please add remark to continue."), Me)
                        Exit Sub
                    End If
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Are you sure you want to reject this Training Request?")
                Case btnApprove.ID.ToUpper
                    'Hemant (11 Oct 2024) -- Start
                    'ENHANCEMENT(Neotech): A1X - 2800 :  Setting to skip training budget approval process
                    If CDec(txtApprovedAmount.Text) <= 0 Then
                        cnfApproveConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "You have not indicated approved amount, are you sure you want to continue?")
                        cnfApproveConfirm.Show()
                    Else
                        'Hemant (11 Oct 2024)
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Are you sure you want to approve this Training Request?")
                    End If 'Hemant (11 Oct 2024) -- End
                    'Hemant (19 Jul 2024) -- Start
                    'ENHANCEMENT(NMB): A1X - 2683 : Training request form: Option for approver to return form to applicant in editable format
                Case btnReturnApplicant.ID.ToUpper
                    If txtApprRejectRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, Remark is mandatory information. Please add remark to continue."), Me)
                        Exit Sub
                    End If
                    cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Are you sure you want to return to Applicant this Training Request?")
                    'Hemant (19 Jul 2024) -- End
            End Select
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper

            'Hemant (11 Oct 2024) -- Start
            'ENHANCEMENT(Neotech): A1X - 2800 :  Setting to skip training budget approval process
            If CDec(txtApprovedAmount.Text) > 0 Then
                'Hemant (11 Oct 2024) -- End
            cnfApprovDisapprove.Show()
            End If 'Hemant (11 Oct 2024)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 Mar 2022) -- End

    'Hemant (03 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim objTrainingRequest As New clstraining_request_master
        Dim objDocument As New clsScan_Attach_Documents
        Dim objEmp As New clsEmployee_Master
        Dim strMsg As String = String.Empty
        Dim intEmployeeUnkid As Integer = -1
        Dim dtTable As DataTable
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
        Dim dtGroupTrainingAttachment As DataTable
        Dim dtFinalAttachment As DataTable
        'Hemant (12 Oct 2022) -- End
        Try
            
            If mintTrainingRequestUnkid > 0 Then
                dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestUnkid, "")
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                'If dtTable.Rows.Count <= 0 Then
                dtFinalAttachment = dtTable
                If mintGroupTrainingRequestunkid > 0 Then
                    dtGroupTrainingAttachment = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.GROUP_TRAINING_REQUEST, mintGroupTrainingRequestunkid, "")
                    dtFinalAttachment.Merge(dtGroupTrainingAttachment)
                End If
                'Hemant (12 Oct 2022) -- End

                If dtFinalAttachment.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "No file to download"), Me)
                    Exit Sub
                End If

                objTrainingRequest._TrainingRequestunkid = mintTrainingRequestUnkid
                intEmployeeUnkid = CInt(objTrainingRequest._Employeeunkid)
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = intEmployeeUnkid

                strMsg = DownloadAllDocument("Training_Request_" + objEmp._Firstname + "_" + objEmp._Othername + "_" + objEmp._Surname + "_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip", dtFinalAttachment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "No file to download"), Me)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingRequest = Nothing
            objEmp = Nothing
            objDocument = Nothing
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            mblnShowAttachmentPopup = False
            popup_ScanAttachment.Dispose()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (03 Oct 2022) -- End

#End Region

#Region " Link Event(s) "

    'Sohail (15 Mar 2022) -- Start
    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
    'Protected Sub lnkChangeStatus_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim objApprovaltran As New clstrainingapproval_process_tran
    '    Dim objTrainingApprover As New clstraining_approver_master
    '    Try
    '        Dim lnk As LinkButton = TryCast(sender, LinkButton)
    '        Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

    '        If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("uempid")) = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")) AndAlso CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("ecompid")) = CInt(Session("CompanyUnkId")) Then
    '            DisplayMessage.DisplayMessage("Sorry, you cannot approve/reject your own training requisition. Please contact aruti administrator to make new approver with same level.", Me)
    '        End If

    '        If CInt(Session("UserId")) <> CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")) Then
    '            'Language.setLanguage(mstrModuleName)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, this training request is a pending task for a different approver and cannot be edited from your account"), Me)
    '            Exit Sub
    '        End If

    '        objTrainingApprover._Mappingunkid = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("approverunkid"))

    '        Dim dsList As DataSet
    '        Dim dtList As DataTable

    '        Dim objapproverlevel As New clstraining_approverlevel_master
    '        objapproverlevel._Levelunkid = objTrainingApprover._Levelunkid

    '        dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
    '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                     CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
    '                                                     CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")), _
    '                                                     CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("trainingrequestunkid")), _
    '                                                     " trtrainingapproval_process_tran.mapuserunkid <> " & CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")))

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

    '            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

    '                If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
    '                    dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtList.Rows.Count > 0 Then Continue For

    '                    If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
    '                        'Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
    '                        Exit Sub
    '                    ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
    '                        'Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
    '                        Exit Sub
    '                    End If

    '                ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

    '                    If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 2 Then
    '                        'Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
    '                        Exit Sub
    '                    ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
    '                        'Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
    '                        Exit Sub
    '                    End If
    '                End If
    '            Next
    '        End If

    '        If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.APPROVED Then
    '            'Language.setLanguage(mstrModuleName)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
    '            Exit Sub
    '        End If

    '        If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("statusunkid")) = enTrainingRequestStatus.REJECTED Then
    '            'Language.setLanguage(mstrModuleName)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
    '            Exit Sub
    '        End If

    '        Session("mblnIsAddMode") = True
    '        Session("mblnFromApproval") = True
    '        'Hemant (03 Dec 2021) -- Start
    '        'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    '        'Session("mintMappingUnkid") = txtRole.Attributes("mappingunkid")
    '        Session("mintMappingUnkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("approverunkid")
    '        'Hemant (03 Dec 2021) -- End
    '        Session("PendingTrainingTranunkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("pendingtrainingtranunkid")
    '        Session("TrainingRequestunkid") = Convert.ToString(lnk.CommandArgument.ToString())
    '        Session("ReturnURL") = Request.Url.AbsoluteUri
    '        Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestForm.aspx", False)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objApprovaltran = Nothing
    '        objTrainingApprover = Nothing
    '    End Try
    'End Sub
    'Sohail (15 Mar 2022) -- End

    Protected Sub lnkComplete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objApprovaltran As New clstrainingapproval_process_tran
        Dim objTrainingApprover As New clstraining_approver_master
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            Dim gvTrainingApprovalList As GridView = TryCast(row.NamingContainer, GridView)
            'Sohail (15 Mar 2022) -- End

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("uempid")) = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")) AndAlso CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("ecompid")) = CInt(Session("CompanyUnkId")) Then
                DisplayMessage.DisplayMessage("Sorry, you cannot approve/reject your own training requisition. Please contact aruti administrator to make new approver with same level.", Me)
            End If

            If CInt(Session("UserId")) <> CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, this training request is a pending task for a different approver and cannot be edited from your account"), Me)
                Exit Sub
            End If

            objTrainingApprover._Mappingunkid = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("approverunkid"))

            Dim dsList As DataSet
            Dim dtList As DataTable

            Dim objapproverlevel As New clstraining_approverlevel_master
            objapproverlevel._Levelunkid = objTrainingApprover._Levelunkid

            dsList = objApprovaltran.GetApprovalTranList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                         CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")), _
                                                         CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("trainingrequestunkid")), _
                                                         " trtrainingapproval_process_tran.mapuserunkid <> " & CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("mapuserunkid")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If objapproverlevel._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                        dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND completed_statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                        If dtList.Rows.Count > 0 Then Continue For

                        If CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 2 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 3 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If

                    ElseIf objapproverlevel._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                        If CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 2 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("completed_statusunkid")) = 3 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("completed_statusunkid")) = enTrainingRequestStatus.APPROVED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited"), Me)
                Exit Sub
            End If

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("completed_statusunkid")) = enTrainingRequestStatus.REJECTED Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited"), Me)
                Exit Sub
            End If

            Session("mblnIsAddMode") = True
            Session("mblnFromCompleteMSS") = True
            Session("mintMappingUnkid") = txtRole.Attributes("mappingunkid")
            Session("PendingTrainingTranunkid") = gvTrainingApprovalList.DataKeys(row.RowIndex)("pendingtrainingtranunkid")
            Session("TrainingRequestunkid") = Convert.ToString(lnk.CommandArgument.ToString())
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "Training\Training_Request\wPg_TrainingRequestForm.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovaltran = Nothing
            objTrainingApprover = Nothing
        End Try
    End Sub

    'Hemant (04 Aug 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-719 - Training request form - new report
    Protected Sub lnkTrainingRequestForm_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkTrainingRequestForm As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkTrainingRequestForm).NamingContainer, GridViewRow)


            SetDateFormat()

            Dim objTrainingRequestFormRpt As New ArutiReports.clsTrainingRequestFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objTrainingRequestFormRpt._RefNo = gvSummary.DataKeys(row.RowIndex)("refno")
            objTrainingRequestFormRpt._RequestTypeId = 0
            objTrainingRequestFormRpt._RequesterId = 0
            objTrainingRequestFormRpt._EmployeeUnkid = 0
            objTrainingRequestFormRpt._TrainingTypeId = CInt(gvSummary.DataKeys(row.RowIndex)("trainingtypeid"))
            objTrainingRequestFormRpt._TrainingUnkid = CInt(gvSummary.DataKeys(row.RowIndex)("coursemasterunkid"))
            objTrainingRequestFormRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objTrainingRequestFormRpt._UserUnkId = CInt(Session("UserId"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString

            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                Dim strUserName As String = String.Empty
                objUser._Userunkid = CInt(Session("UserId"))
                strUserName = objUser._Firstname & " " & objUser._Lastname
                If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
                objTrainingRequestFormRpt._User = strUserName
                objUser = Nothing
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objTrainingRequestFormRpt._User = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
                objEmp = Nothing
            End If

            objTrainingRequestFormRpt.generateReportNew(CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              Session("UserAccessModeSetting").ToString, True, _
                                              Session("ExportReportPath").ToString, _
                                              CBool(Session("OpenAfterExport")), _
                                              0, enPrintAction.None, enExportAction.None, _
                                              CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objTrainingRequestFormRpt._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            objTrainingRequestFormRpt = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Hemant (04 Aug 2022) -- End

    'Hemant (03 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
    Protected Sub lnkViewAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTable As DataTable
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
        Dim dtGroupTrainingAttachment As DataTable
        Dim dtFinalAttachment As DataTable
        'Hemant (12 Oct 2022) -- End
        Try
            Dim lnkViewAttachment As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkViewAttachment).NamingContainer, GridViewRow)
            Dim gvTrainingApprovalList As GridView = TryCast(row.NamingContainer, GridView)
            mintTrainingRequestUnkid = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("trainingrequestunkid"))
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            mintGroupTrainingRequestunkid = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("grouptrainingrequestunkid"))
            'Hemant (12 Oct 2022) -- End
            If mintTrainingRequestUnkid > 0 Then
                dtTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.TRAINING_NEED_FORM, mintTrainingRequestUnkid, "")
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                'If dtTable.Rows.Count <= 0 Then
                dtFinalAttachment = dtTable
                If mintGroupTrainingRequestunkid > 0 Then
                    dtGroupTrainingAttachment = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.GROUP_TRAINING_REQUEST, mintGroupTrainingRequestunkid, "")
                    dtFinalAttachment.Merge(dtGroupTrainingAttachment)
                End If
                'Hemant (12 Oct 2022) -- End

                If dtFinalAttachment.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "No file to download"), Me)
                    Exit Sub
                End If
                dgvAttachment.AutoGenerateColumns = False
                dgvAttachment.DataSource = dtFinalAttachment
                dgvAttachment.DataBind()
            Else
                dtTable = Nothing
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "No file to download"), Me)
                Exit Sub
            End If
            mblnShowAttachmentPopup = True
            popup_ScanAttachment.Show()

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDocument = Nothing
        End Try

    End Sub
    'Hemant (03 Oct 2022) -- End
#End Region

#Region "GridView's Event"

    'Sohail (15 Mar 2022) -- Start
    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
    'Protected Sub gvTrainingApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingApprovalList.RowDataBound
    '    Try
    '        If e.Row.RowIndex >= 0 Then
    '            If e.Row.RowType = DataControlRowType.DataRow Then
    '                If CBool(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
    '                    e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "employee").ToString
    '                    e.Row.Cells(2).ColumnSpan = e.Row.Cells.Count - 2
    '                    e.Row.BackColor = Color.Silver
    '                    e.Row.ForeColor = Color.Black
    '                    e.Row.Font.Bold = True

    '                    For i As Integer = 3 To e.Row.Cells.Count - 2
    '                        e.Row.Cells(i).Visible = False
    '                    Next

    '                    Dim lnkChangeStatus As LinkButton = TryCast(e.Row.FindControl("lnkChangeStatus"), LinkButton)
    '                    lnkChangeStatus.Visible = False

    '                    Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
    '                    ImgComplete.Visible = False

    '                Else
    '                    If e.Row.Cells(2).Text.ToString().Trim <> "" AndAlso e.Row.Cells(2).Text.Trim <> "&nbsp;" Then
    '                        e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text).Date.ToShortDateString
    '                    End If

    '                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
    '                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))

    '                    Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
    '                    If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval AndAlso _
    '                       CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) > 0 AndAlso _
    '                       CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_visibleid")) >= 0 Then
    '                        ImgComplete.Visible = True
    '                    Else
    '                        ImgComplete.Visible = False
    '                    End If

    '                End If
    '                'If IsDBNull(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item(0)) = False AndAlso _
    '                '            CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item(0)) > 0 Then

    '                'End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Public Sub gvTrainingApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhTrainingcostemp", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhTrainingcostemp", False, True)).Text), Session("fmtcurrency").ToString())
                'e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhApprovedamountemp", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhApprovedamountemp", False, True)).Text), Session("fmtcurrency").ToString())
                If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhapplication_date", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhapplication_date", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhapplication_date", False, True)).Text = CDate(e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "colhapplication_date", False, True)).Text).Date.ToShortDateString
                End If

                Dim gvTrainingApprovalList As GridView = CType(sender, GridView)
                Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval AndAlso _
                   CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) > 0 AndAlso _
                   CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_visibleid")) >= 0 Then
                    ImgComplete.Visible = True
                Else
                    ImgComplete.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvSummary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSummary.RowDataBound
        Try
            If e.Row.RowIndex >= 0 Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If CBool(gvSummary.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                        'e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "refno").ToString & " - " & DataBinder.Eval(e.Row.DataItem, "CreateUserName").ToString & " - " & DataBinder.Eval(e.Row.DataItem, "Training").ToString & " - " & DataBinder.Eval(e.Row.DataItem, "allocationtranname").ToString & " - (" + eZeeDate.convertDate(DataBinder.Eval(e.Row.DataItem, "start_date").ToString).ToShortDateString + " - " + eZeeDate.convertDate(DataBinder.Eval(e.Row.DataItem, "end_date").ToString).ToShortDateString + ") "
                        'e.Row.Cells(2).ColumnSpan = e.Row.Cells.Count - 2
                        e.Row.BackColor = Color.Silver
                        e.Row.ForeColor = Color.Black
                        e.Row.Font.Bold = True

                        'For i As Integer = 3 To e.Row.Cells.Count - 2
                        '    e.Row.Cells(i).Visible = False
                        'Next

                        e.Row.Cells(getColumnID_Griview(gvSummary, "colhTotal_training_cost", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(gvSummary, "colhTotal_training_cost", False, True)).Text), Session("fmtcurrency").ToString())
                        e.Row.Cells(getColumnID_Griview(gvSummary, "colhApproved_amount", False, True)).Text = Format(CDec(e.Row.Cells(getColumnID_Griview(gvSummary, "colhApproved_amount", False, True)).Text), Session("fmtcurrency").ToString())

                        If e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text = CDate(eZeeDate.convertDate(e.Row.Cells(getColumnID_Griview(gvSummary, "colhStartDate", False, True)).Text)).Date.ToShortDateString
                        End If
                        If e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text = CDate(eZeeDate.convertDate(e.Row.Cells(getColumnID_Griview(gvSummary, "colhEndDate", False, True)).Text)).Date.ToShortDateString
                        End If

                        Dim gvTrainingApprovalList As GridView = TryCast(e.Row.FindControl("gvTrainingApprovalList"), GridView)
                        Dim strRefno As String = gvSummary.DataKeys(e.Row.RowIndex).Item("refno").ToString
                        If Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval") Is Nothing Then
                            Call FillList(False)
                        End If
                        Dim dtTable As DataTable = CType(Cache(CStr(Session("Database_Name")) & "__" & CStr(Session("U_UserId")) & "__" & CStr(Session("E_Employeeunkid")) & "_DTTrainingApproval"), DataTable)

                        If dtTable IsNot Nothing Then
                            dtTable = New DataView(dtTable, "refno = '" & strRefno & "' AND IsGrp = 0 ", "", DataViewRowState.CurrentRows).ToTable
                            gvTrainingApprovalList.DataSource = dtTable.Copy()
                            gvTrainingApprovalList.DataBind()
                        End If


                        'Sohail (15 Mar 2022) -- Start
                        'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
                        'Dim lnkChangeStatus As LinkButton = TryCast(e.Row.FindControl("lnkChangeStatus"), LinkButton)
                        'lnkChangeStatus.Visible = False
                        'Sohail (15 Mar 2022) -- End

                        'Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                        'ImgComplete.Visible = False



                    Else
                        'If e.Row.Cells(2).Text.ToString().Trim <> "" AndAlso e.Row.Cells(2).Text.Trim <> "&nbsp;" Then
                        '    e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text).Date.ToShortDateString
                        'End If

                        'e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                        'e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))


                        'Dim ImgComplete As LinkButton = TryCast(e.Row.FindControl("ImgComplete"), LinkButton)
                        'If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("iscompleted_submit_approval")) = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval AndAlso _
                        '   CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_statusunkid")) > 0 AndAlso _
                        '   CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex).Item("completed_visibleid")) >= 0 Then
                        '    ImgComplete.Visible = True
                        'Else
                        '    ImgComplete.Visible = False
                        'End If

                    End If

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (15 Mar 2022) -- End

    'Hemant (03 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-920 - NMB - As a user, I want approvers to be able to view attachments attached on the training request
    Protected Sub dgvAttachment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvAttachment.RowCommand
        Dim intScanattachtranunkid As Integer = 0
        Try
            Dim SrNo As Integer = CInt(e.CommandArgument)

            intScanattachtranunkid = CInt(dgvAttachment.DataKeys(SrNo).Item("scanattachtranunkid"))

            If e.CommandName = "Download" Then

                Dim xPath As String = ""

                If intScanattachtranunkid > 0 Then
                    xPath = dgvAttachment.DataKeys(SrNo).Item("filepath").ToString
                    If xPath.Contains(Session("ArutiSelfServiceURL").ToString) = True Then
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    End If
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "File does not Exist..."), Me)
                        Exit Sub
                    End If
                    fileInfo = Nothing
                    Dim strFile As String = xPath
                    Response.ContentType = "image/jpg/pdf"
                    Response.AddHeader("Content-Disposition", "attachment;filename=""" & dgvAttachment.Rows(SrNo).Cells(1).Text & """")
                    Response.Clear()
                    Response.TransmitFile(strFile)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                intScanattachtranunkid = 0

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (03 Oct 2022) -- End
#End Region

#Region " Combobox's Events "
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Call SetApprovalData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Confirmation"
    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
    Protected Sub cnfApprovDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfApprovDisapprove.buttonYes_Click
        Dim objTrainingApproval As New clstrainingapproval_process_tran
        Try
            Dim blnFlag As Boolean = False

            If IsValidate() = False Then Exit Sub

            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnApprove.ID.ToUpper, btnDisapprove.ID.ToUpper, btnReturnApplicant.ID.ToUpper
                    'Hemant (19 Jul 2024) -- [btnReturnApplicant.ID.ToUpper]
                    
                    Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                    Dim lstTrainingApproval As New List(Of clstrainingapproval_process_tran)
                    'Hemant (10 Nov 2022) -- Start
                    'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                    Dim dicTrainingRequest As New Dictionary(Of Integer, Integer)
                    'Hemant (10 Nov 2022) -- End

                    gRow = gvSummary.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

                    If gRow Is Nothing OrElse gRow.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Ref No. is compulsory information.Please check atleast one Ref No."), Me)
                        Exit Sub
                    End If

                    If gRow.Count > 1 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Please check only one Ref No."), Me)
                        Exit Sub
                    End If



                    For Each dgRow As GridViewRow In gRow

                        'Hemant (19 Jul 2024) -- Start
                        'ENHANCEMENT(NMB): A1X - 2683 : Training request form: Option for approver to return form to applicant in editable format
                        If Me.ViewState("Sender").ToString().ToUpper() = btnReturnApplicant.ID.ToUpper AndAlso CInt(gvSummary.DataKeys(dgRow.RowIndex).Item("insertformid")) <> 1 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, Return to Applicant is allowed to individual training application only."), Me)
                            Exit Sub
                        End If
                        'Hemant (19 Jul 2024) -- End

                        'Hemant (29 Apr 2022) -- Start
                        'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                        'Hemant (10 Nov 2022) -- Start
                        'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                        'SetApprovalData(CInt(gvSummary.DataKeys(dgRow.RowIndex).Item("trainingtypeid")))
                        'If mdicApproverLevelPriority.ContainsKey(CInt(gvSummary.DataKeys(dgRow.RowIndex).Item("trainingtypeid"))) Then
                        '    mintApproverLevelPriority = mdicApproverLevelPriority(CInt(gvSummary.DataKeys(dgRow.RowIndex).Item("trainingtypeid")))
                        'End If
                        'Hemant (10 Nov 2022) -- End
                        'Hemant (29 Apr 2022) -- End

                        Dim gvTrainingApprovalList As GridView = TryCast(dgRow.FindControl("gvTrainingApprovalList"), GridView)

                        Dim gTrainingApprovalListRow As IEnumerable(Of GridViewRow) = Nothing
                        gTrainingApprovalListRow = gvTrainingApprovalList.Rows.Cast(Of GridViewRow).Where(Function(x) gvTrainingApprovalList.DataKeys(x.RowIndex).Item("mapuserunkid").ToString = CInt(Session("UserId")) _
                                                                                                              AndAlso CInt(gvTrainingApprovalList.DataKeys(x.RowIndex).Item("statusunkid").ToString) = 1 _
                                                                                                              AndAlso CInt(gvTrainingApprovalList.DataKeys(x.RowIndex).Item("visibleid").ToString) = 1)

                        If gTrainingApprovalListRow Is Nothing OrElse gTrainingApprovalListRow.Count <= 0 Then
                            DisplayMessage.DisplayMessage("Employee is compulsory information.Please atleast One Employee should be assigned to Selected Training Batch.", Me)
                            Exit Sub
                        End If

                        'Hemant (07 Mar 2022) -- Start            
                        'ISSUE/ENHANCEMENT(NMB) : OLD-586 - Validate requested amounts against cost center budget. System should not allow to request more than available funds.
                        'Hemant (27 Oct 2022) -- Start
                        'ENHANCEMENT(NMB) : AC2-994 - As a user, I want system to accept rejection of training if training budget is not sufficient. Currently system is refusing.
                        'If IsValidForMaxBudget(CDec(txtApprovedAmount.Text), gvTrainingApprovalList) = False Then
                        If Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper AndAlso IsValidForMaxBudget(CDec(txtApprovedAmount.Text), gvTrainingApprovalList) = False Then
                            'Hemant (27 Oct 2022) -- End
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry, Approved amount should not exceed the maximum budgetary allocation for the selected department."), Me)
                            Exit Sub
                        End If
                        'Hemant (07 Mar 2022) -- End

                        Dim intNoOfEmployee As Integer = gTrainingApprovalListRow.Count
                        Dim intCourseMasterunkid As Integer = CInt(gvSummary.DataKeys(dgRow.RowIndex).Item("coursemasterunkid"))


                        For Each gvRowTrainingApprovalListRow As GridViewRow In gTrainingApprovalListRow
                            objTrainingApproval = New clstrainingapproval_process_tran
                            'If CInt(gvTrainingApprovalList.DataKeys(gvRowTrainingApprovalListRow.RowIndex)("mapuserunkid").ToString()) <> CInt(Session("UserId")) Then Continue For
                            'Hemant (30 Aug 2024) -- Start
                            mintApproverLevelPriority = CInt(gvTrainingApprovalList.DataKeys(gvRowTrainingApprovalListRow.RowIndex).Item("priority").ToString)
                            'Hemant (30 Aug 2024) -- End
                            With objTrainingApproval
                                .pintPendingTrainingTranunkid = CInt(gvTrainingApprovalList.DataKeys(gvRowTrainingApprovalListRow.RowIndex)("pendingtrainingtranunkid").ToString())
                                .pdecApprovedAmount = CDec(txtApprovedAmount.Text)
                                'Hemant (25 Jul 2022) -- Start            
                                'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                                If CInt(Session("TrainingRequestApprovalSettingID")) = enTrainingRequestApproval.ApproverEmpMapping Then
                                    .pintApproverTranunkid = CInt(txtApprover.Attributes("approverunkid"))
                                Else
                                    'Hemant (25 Jul 2022) -- End
                                    .pintApproverTranunkid = CInt(txtRole.Attributes("mappingunkid"))
                                End If 'Hemant (25 Jul 2022)
                                .pdtApprovaldate = ConfigParameter._Object._CurrentDateAndTime
                                .pintUserunkid = CInt(Session("UserId"))
                                .pblnIsWeb = True
                                .pblnIsvoid = False
                                .pdtVoiddatetime = Nothing
                                .pstrVoidreason = ""
                                .pintVoiduserunkid = -1
                                .pstrClientIP = CStr(Session("IP_ADD"))
                                .pstrFormName = mstrModuleName
                                .pstrHostName = CStr(Session("HOST_NAME"))
                                .pintMapuserunkid = CInt(Session("UserId"))
                                .pstrRemark = txtApprRejectRemark.Text
                                .pdecApprovedAmountEmp = CDec(txtApprovedAmount.Text) / intNoOfEmployee

                                If Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper Then
                                    .pintStatusunkid = enTrainingRequestStatus.APPROVED
                                ElseIf Me.ViewState("Sender").ToString().ToUpper() = btnDisapprove.ID.ToUpper Then
                                    .pintStatusunkid = enTrainingRequestStatus.REJECTED
                                    'Hemant (19 Jul 2024) -- Start
                                    'ENHANCEMENT(NMB): A1X - 2683 : Training request form: Option for approver to return form to applicant in editable format
                                ElseIf Me.ViewState("Sender").ToString().ToUpper() = btnReturnApplicant.ID.ToUpper Then
                                    .pintStatusunkid = enTrainingRequestStatus.RETURNTOAPPLICANT
                                    'Hemant (19 Jul 2024) -- End
                                End If

                                'Hemant (25 Jul 2022) -- Start            
                                'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
                                .pintTrainingApprovalSettingID = CInt(Session("TrainingRequestApprovalSettingID"))
                                'Hemant (25 Jul 2022) -- End

                                'Hemant (05 Jul 2024) -- Start
                                'ENHANCEMENT(NMB): A1X - 2375 : Hide Training Evaluation screens
                                ._SkipPreTrainingEvaluationProcess = CBool(Session("SkipPreTrainingEvaluationProcess"))
                                ._SkipTrainingEnrollmentProcess = CBool(Session("SkipTrainingEnrollmentProcess"))
                                ._SkipTrainingCompletionProcess = CBool(Session("SkipTrainingCompletionProcess"))
                                ._SkipPostTrainingEvaluationProcess = CBool(Session("SkipPostTrainingEvaluationProcess"))
                                ._SkipDaysAfterTrainingEvaluationProcess = CBool(Session("SkipDaysAfterTrainingEvaluationProcess"))
                                'Hemant (05 Jul 2024) -- End

                            End With

                            lstTrainingApproval.Add(objTrainingApproval)
                            'Hemant (10 Nov 2022) -- Start
                            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                            dicTrainingRequest.Add(CInt(gvTrainingApprovalList.DataKeys(gvRowTrainingApprovalListRow.RowIndex)("trainingrequestunkid")), CInt(gvTrainingApprovalList.DataKeys(gvRowTrainingApprovalListRow.RowIndex)("employeeunkid")))
                            'Hemant (10 Nov 2022) -- End
                        Next


                        If objTrainingApproval.UpdateAllByEmployeeList(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), _
                                                                       CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), _
                                                                       Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), CInt(Session("TrainingApproverAllocationID")), _
                                                                         eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                         intCourseMasterunkid, lstTrainingApproval, Nothing) = False Then

                            'Hemant (19 Jul 2024) -- [CInt(Session("CompanyUnkId")),CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")),Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), CInt(Session("TrainingApproverAllocationID"))]
                            DisplayMessage.DisplayMessage(objTrainingApproval._Message, Me)

                        Else
                            'Hemant (10 Nov 2022) -- Start
                            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                            If dicTrainingRequest.Count > 0 AndAlso Me.ViewState("Sender").ToString().ToUpper() = btnApprove.ID.ToUpper Then
                                Dim objRequestMaster As New clstraining_request_master
                                objRequestMaster._TrainingRequestunkid = CInt(dicTrainingRequest.Keys(0))
                                objRequestMaster.Send_Notification_Approver(Session("Database_Name").ToString, _
                                                                            -1, _
                                                                            CInt(IIf(mintApproverLevelPriority > 0, mintApproverLevelPriority, -1)), _
                                                                            clstraining_request_master.enEmailType.Training_Approver, _
                                                                            CInt(Session("CompanyUnkId")), "", objRequestMaster._Coursemasterunkid, _
                                                                            objRequestMaster._Application_Date.Date, CInt(Session("Fin_year")), _
                                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                            CStr(Session("ArutiSelfServiceURL")), objRequestMaster._Start_Date.Date, objRequestMaster._End_Date.Date, CStr(Session("UserAccessModeSetting")), _
                                                                            IIf((objRequestMaster._GroupTrainingRequestunkid > 0 OrElse dicTrainingRequest.Count > 1), 2, 1), dicTrainingRequest, enLogin_Mode.DESKTOP, _
                                                                            0, CInt(Session("UserId")), , , False, Session("TrainingFinalApprovedNotificationUserIds"))
                                objRequestMaster = Nothing

                            End If
                            'Hemant (10 Nov 2022) -- End
                            'Hemant (04 Aug 2022) -- Start            
                            'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
                            'Hemant (10 Nov 2022) -- Start
                            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                            'Dim blnSendEmailFinalApprovedUsers As Boolean = False
                            'Hemant (10 Nov 2022) -- End
                            'Hemant (04 Aug 2022) -- End
                            For Each clsTrainingApproval As clstrainingapproval_process_tran In lstTrainingApproval
                                Dim objRequestMaster As New clstraining_request_master
                                'Hemant (25 Jul 2022) -- Start            
                                'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                                objRequestMaster._TrainingApprovalSettingID = CInt(Session("TrainingRequestApprovalSettingID"))
                                'Hemant (25 Jul 2022) -- End
                                Dim objEmp As New clsEmployee_Master
                                Dim strEmployeeName As String = String.Empty

                                With clsTrainingApproval
                                    clsTrainingApproval._PendingTrainingTranunkid = .pintPendingTrainingTranunkid
                                    objRequestMaster._TrainingRequestunkid = clsTrainingApproval._TrainingRequestunkid

                                    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objRequestMaster._Employeeunkid
                                    strEmployeeName = objEmp._Employeecode & "-" & objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname

                                    If objTrainingApproval._Statusunkid = enTrainingRequestStatus.APPROVED Then

                                        'Hemant (10 Nov 2022) -- Start
                                        'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                                        'objRequestMaster.Send_Notification_Approver(Session("Database_Name").ToString, _
                                        '                                            CInt(objRequestMaster._Employeeunkid), _
                                        '                                            CInt(IIf(mintApproverLevelPriority > 0, mintApproverLevelPriority, -1)), _
                                        '                                            clstraining_request_master.enEmailType.Training_Approver, _
                                        '                                            CInt(Session("CompanyUnkId")), strEmployeeName, objRequestMaster._Coursemasterunkid, _
                                        '                                            objRequestMaster._Application_Date.Date, CInt(Session("Fin_year")), _
                                        '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        '                                            CStr(Session("ArutiSelfServiceURL")), objRequestMaster._Start_Date.Date, objRequestMaster._End_Date.Date, CStr(Session("UserAccessModeSetting")), _
                                        '                                            IIf(objRequestMaster._GroupTrainingRequestunkid > 0, 2, 1), dicTrainingRequest, enLogin_Mode.DESKTOP, _
                                        '                                            0, CInt(Session("UserId")), , , False, Session("TrainingFinalApprovedNotificationUserIds"), blnSendEmailFinalApprovedUsers)
                                        'Hemant (10 Nov 2022) -- End                                       
                                        'Hemant (10 Nov 2022) -- [intInsertFormId:=IIf(objRequestMaster._GroupTrainingRequestunkid > 0, 2, 1),dicTrainingRequest]
                                        'Hemant (03 Oct 2022) -- [CStr(Session("UserAccessModeSetting"))]
                                        'Hemant (04 Aug 2022) -- [Session("TrainingFinalApprovedNotificationUserIds")]
                                        'Hemant (04 Aug 2022) -- Start            
                                        'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
                                        'Hemant (10 Nov 2022) -- Start
                                        'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                                        'blnSendEmailFinalApprovedUsers = True
                                        'Hemant (10 Nov 2022) -- End
                                        'Hemant (04 Aug 2022) -- End
                                        If objRequestMaster._Statusunkid = enTrainingRequestStatus.APPROVED Then
                                            objRequestMaster.Send_Notification_Employee(CInt(objRequestMaster._Employeeunkid), _
                                                                                   enTrainingRequestStatus.APPROVED, _
                                                                                  clstraining_request_master.enEmailType.Training_Approver, _
                                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                  CInt(Session("CompanyUnkId")), objRequestMaster._Coursemasterunkid, _
                                                                                  objRequestMaster._Application_Date.Date, objRequestMaster._TrainingRequestunkid, _
                                                                                  CStr(Session("ArutiSelfServiceURL")), txtApprRejectRemark.Text.Trim, _
                                                                                  enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                                        End If

                                    ElseIf objTrainingApproval._Statusunkid = enTrainingRequestStatus.REJECTED Then
                                        objRequestMaster.Send_Notification_Employee(CInt(objRequestMaster._Employeeunkid), _
                                                                                    enTrainingRequestStatus.REJECTED, _
                                                                                   clstraining_request_master.enEmailType.Training_Approver, _
                                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                   CInt(Session("CompanyUnkId")), objRequestMaster._Coursemasterunkid, _
                                                                                   objRequestMaster._Application_Date.Date, objRequestMaster._TrainingRequestunkid, _
                                                                                   CStr(Session("ArutiSelfServiceURL")), txtApprRejectRemark.Text.Trim, _
                                                                                   enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                                    End If
                                End With
                                objRequestMaster = Nothing
                                objEmp = Nothing
                            Next


                            If Request.QueryString.Count <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Information saved successfully."), Me)
                                FillList()
                                Clear_Controls()
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Information saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                                'Pinkal (23-Feb-2024) -- Start
                                '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                                Session.Clear()
                                Session.Abandon()
                                Session.RemoveAll()

                                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                    Response.Cookies("ASP.NET_SessionId").Value = ""
                                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                End If

                                If Request.Cookies("AuthToken") IsNot Nothing Then
                                    Response.Cookies("AuthToken").Value = ""
                                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                End If
                                'Pinkal (23-Feb-2024) -- End
                            End If


                        End If

                    Next


            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproval = Nothing
        End Try
    End Sub
    'Hemant (07 Mar 2022) -- End

    'Hemant (11 Oct 2024) -- Start
    'ENHANCEMENT(Neotech): A1X - 2800 :  Setting to skip training budget approval process
    Protected Sub cnfApproveConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfApproveConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            cnfApprovDisapprove.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Are you sure you want to approve this Training Request?")
            cnfApprovDisapprove.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (11 Oct 2024) -- End


#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, lblDetialHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRole.ID, lblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lbllevel.ID, lbllevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblemp.ID, lblemp.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStatus.ID, lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCompletionStatus.ID, lblCompletionStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprover.ID, lblApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprovedAmount.ID, lblApprovedAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblApprRejectRemark.ID, lblApprRejectRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblScanHeader.ID, lblScanHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, chkMyApproval.ID, chkMyApproval.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSearch.ID, btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnApprove.ID, btnApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnDisapprove.ID, btnDisapprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnDownloadAll.ID, btnDownloadAll.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnScanClose.ID, btnScanClose.Text)


            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(2).FooterText, gvTrainingApprovalList.Columns(2).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(3).FooterText, gvTrainingApprovalList.Columns(3).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(4).FooterText, gvTrainingApprovalList.Columns(4).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(5).FooterText, gvTrainingApprovalList.Columns(5).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(6).FooterText, gvTrainingApprovalList.Columns(6).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(7).FooterText, gvTrainingApprovalList.Columns(7).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(8).FooterText, gvTrainingApprovalList.Columns(8).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, gvTrainingApprovalList.Columns(9).FooterText, gvTrainingApprovalList.Columns(9).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, lblDetialHeader.Text)
            'Hemant (03 Dec 2021) -- Start
            'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPeriod.ID, lblPeriod.Text)
            'Hemant (03 Dec 2021) -- End
            Me.lblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRole.ID, lblRole.Text)
            Me.lbllevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lbllevel.ID, lbllevel.Text)
            Me.lblemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblemp.ID, lblemp.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStatus.ID, lblStatus.Text)
            Me.lblCompletionStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCompletionStatus.ID, lblCompletionStatus.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprover.ID, lblApprover.Text)
            Me.lblApprovedAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprovedAmount.ID, lblApprovedAmount.Text)
            Me.lblApprRejectRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblApprRejectRemark.ID, lblApprRejectRemark.Text)
            Me.lblScanHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblScanHeader.ID, lblScanHeader.Text)

            Me.chkMyApproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), chkMyApproval.ID, chkMyApproval.Text)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSearch.ID, btnSearch.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnApprove.ID, btnApprove.Text)
            Me.btnDisapprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnDisapprove.ID, btnDisapprove.Text)
            Me.btnDownloadAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnDownloadAll.ID, btnDownloadAll.Text)
            Me.btnScanClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnScanClose.ID, btnScanClose.Text)

            'gvTrainingApprovalList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(2).FooterText, gvTrainingApprovalList.Columns(2).HeaderText)
            'gvTrainingApprovalList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(3).FooterText, gvTrainingApprovalList.Columns(3).HeaderText)
            'gvTrainingApprovalList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(4).FooterText, gvTrainingApprovalList.Columns(4).HeaderText)
            'gvTrainingApprovalList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(5).FooterText, gvTrainingApprovalList.Columns(5).HeaderText)
            'gvTrainingApprovalList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(6).FooterText, gvTrainingApprovalList.Columns(6).HeaderText)
            'gvTrainingApprovalList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(7).FooterText, gvTrainingApprovalList.Columns(7).HeaderText)
            'gvTrainingApprovalList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(8).FooterText, gvTrainingApprovalList.Columns(8).HeaderText)
            'gvTrainingApprovalList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvTrainingApprovalList.Columns(9).FooterText, gvTrainingApprovalList.Columns(9).HeaderText)
           
           

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, this training request is already approved and cannot be edited")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, this training request is already rejected and cannot be edited")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, this training request is a pending task for a different approver and cannot be edited from your account")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Approved By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Rejected By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, Approved Amount is mandatory information. Please enter the Approved Amount to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, Approver Remark is mandatory information. Please select Approver Remark to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, Requested amount should not exceed the maximum budgetary allocation for the selected department.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "No file to download")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, Remark is mandatory information. Please add remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Are you sure you want to reject this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Are you sure you want to approve this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "File does not Exist...")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Ref No. is compulsory information.Please check atleast one Ref No.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Please check only one Ref No.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Sorry, Approved amount should not exceed the maximum budgetary allocation for the selected department.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Information saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Are you sure you want to return to Applicant this Training Request?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Sorry, Return to Applicant is allowed to individual training application only.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "You have not indicated approved amount, are you sure you want to continue?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
