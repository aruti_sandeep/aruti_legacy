﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_CloseTrainingCalendar.aspx.vb"
    Inherits="Training_Close_Training_Calendar_wPg_CloseTrainingCalendar" Title="Close Training Calendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
        <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Close Training Calendar"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingCalendar" runat="server" Text="Calendar To be Closed"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingCalendar" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnCloseCalendar" runat="server" Text="Close Calendar" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            <asp:HiddenField ID="hfPendingTraining" runat="Server" />
                        </div>
                    </div>
                </div>
            </div>
            <cc1:ModalPopupExtender ID="popupPendingTraining" BackgroundCssClass="modal-backdrop bd-l2"
                TargetControlID="hfPendingTraining" runat="server" CancelControlID="hfPendingTraining"
                PopupControlID="pnlPendingTraining">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPendingTraining" runat="server" CssClass="card modal-dialog modal-lg"
                Style="display: none;">
                <div class="header">
                    <h2>
                        <asp:Label ID="lblTrainingCourseMaster" Text="Pending Training(s) List" runat="server" />
                    </h2>
                </div>
                <div class="body" style="max-height: 500px">
                    <div class="card inner-card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                                        <asp:Panel ID="pnl_ViewPendingToApproveTraining" runat="server" Visible="true" CssClass="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne_1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1"
                                                        aria-expanded="true" aria-controls="collapseOne_1">
                                                        <asp:Label ID="lbllnkPendingToApproveTraining" runat="server" Text="Pending To Approve Training List"
                                                            CssClass="form-label"></asp:Label>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_1">
                                                <div class="panel-body">
                                                    <asp:Panel ID="pnlGvPendingToApproveTraining" ScrollBars="Auto" Style="max-height: 400px"
                                                        runat="server">
                                                        <asp:GridView ID="GvPendingToApproveTraining" runat="server" runat="server" AutoGenerateColumns="False"
                                                            AllowPaging="false" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                            <Columns>
                                                                <asp:BoundField DataField="trainingcoursename" HeaderText="Training Name" ReadOnly="True" FooterText="colhPATraining" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnl_ViewLessStaffTraining" runat="server" Visible="true" CssClass="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo_1">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                        href="#collapseTwo_1" aria-expanded="false" aria-controls="collapseTwo_1">
                                                        <asp:Label ID="lblViewLessStaffTraining" runat="server" Text="Less No of Staff Attended Training List"
                                                            CssClass="form-label"></asp:Label>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1">
                                                <div class="panel-body">
                                                    <asp:Panel ID="pnlGvLessStaffTraining" ScrollBars="Auto" Style="max-height: 400px"
                                                        runat="server">
                                                        <asp:GridView ID="GvLessStaffTraining" runat="server" runat="server" AutoGenerateColumns="False"
                                                            AllowPaging="false" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="IsGrp,TrainingCategory">
                                                            <Columns>
                                                                <asp:BoundField DataField="Training" HeaderText="Training Name" ReadOnly="True" FooterText="colhLSTraining" />
                                                                <asp:BoundField DataField="NoOfStaff" HeaderText="No Of Staff" ReadOnly="True" FooterText="colhNoOfStaff" />
                                                                <asp:BoundField DataField="RequestedCount" HeaderText="Requested Count" ReadOnly="True"
                                                                    FooterText="colhRequestedCount" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnl_ViewPendingToCompleteTraining" runat="server" Visible="true" CssClass="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree_1">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                        href="#collapseThree_1" aria-expanded="false" aria-controls="collapseThree_1">
                                                        <asp:Label ID="lbllnkViewPendingToCompleteTraining" runat="server" Text="Pending To Complete Training Employee List"
                                                            CssClass="form-label"></asp:Label>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_1">
                                                <div class="panel-body">
                                                    <asp:Panel ID="pnlGvPendingToCompleteTraining" ScrollBars="Auto" Style="max-height: 400px"
                                                        runat="server">
                                                        <asp:GridView ID="GvPendingToCompleteTraining" runat="server" AutoGenerateColumns="False"
                                                            AllowPaging="false" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                            <Columns>
                                                                <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                                    FooterText="colhEmployee" />
                                                                <asp:BoundField DataField="Training" HeaderText="Training Name" ReadOnly="True" FooterText="colhPCTraining" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Button ID="btnPendingTrainingClose" runat="server" CssClass="btn btn-default"
                        Text="Close" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
