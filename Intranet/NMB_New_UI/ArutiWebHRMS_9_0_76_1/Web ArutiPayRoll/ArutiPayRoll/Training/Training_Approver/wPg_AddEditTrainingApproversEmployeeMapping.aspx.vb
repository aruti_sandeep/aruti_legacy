﻿Option Strict On

#Region "Import"

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Training_Training_Approver_wPg_AddEditTrainingApproversEmployeeMapping
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingApproversEmployeeMappingAddEdit"
    Dim DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = ""
    Private mdtEmployee As DataTable = Nothing
    Private mdtAssignedEmp As DataTable = Nothing
    Private dtAssignEmpView As DataView = Nothing
    Private mintapproverunkid As Integer = -1
    Private dtEmployee As DataTable = Nothing
    Private mstrEmployeeIDs As String = String.Empty
    Private mintApproverEmpunkid As Integer = -1
    'Hemant (16 Sep 2022) -- Start
    'ISSUE/ENHANCEMENT(NMB) : Pending Training approval should be go for New User instead of Old User  after changing User  in Training Approver Add/Edit Screen
    Private mintOldMappedUserId As Integer = -1
    'Hemant (16 Sep 2022) -- End
#End Region

#Region "Page's event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objApproverMaster As New clstrainingapprover_master_emp_map
        Dim objApproverTran As New clstrainingapprover_tran_emp_map
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                GC.Collect()
                Call SetLanguage()

                dgvAEmployee.DataSource = New List(Of String)
                dgvAEmployee.DataBind()
                dgvAssignedEmp.DataSource = New List(Of String)
                dgvAssignedEmp.DataBind()

                FillCombo()

                If Session("trapproverunkid") IsNot Nothing Then
                    cboApprover.Enabled = False
                    mintapproverunkid = CInt(Session("trapproverunkid"))
                    objApproverMaster._Approverunkid = mintapproverunkid
                    objApproverTran._Approverunkid = mintapproverunkid
                    objApproverTran.GetData()
                    cboTrainingCalendar.Enabled = False
                    chkExternalApprover.Enabled = False
                    cboTrainingType.Enabled = False
                Else
                    Session("trapproverunkid") = -1
                End If

                mdtAssignedEmp = objApproverTran._TranDataTable

                Call GetValue(objApproverMaster)
                'TrainingApprover.Visible = False
            Else
                mdtAssignedEmp = CType(Me.ViewState("mdtAssignedEmp"), DataTable)
                mdtEmployee = CType(Me.ViewState("mdtEmployee"), DataTable)
                mintapproverunkid = CInt(Me.ViewState("mintapproverunkid"))
                dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
                mintApproverEmpunkid = CInt(Me.ViewState("ApproverEmpunkid"))
                mintOldMappedUserId = CInt(Me.ViewState("OldMappedUserId"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMaster = Nothing
            objApproverTran = Nothing
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtAssignedEmp") = mdtAssignedEmp
            Me.ViewState("mdtEmployee") = mdtEmployee
            Me.ViewState("mintapproverunkid") = mintapproverunkid
            Me.ViewState("dtEmployee") = dtEmployee
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("ApproverEmpunkid") = mintApproverEmpunkid
            Me.ViewState("OldMappedUserId") = mintOldMappedUserId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("lnapproverunkid")
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objtrApproverLevelMaster As New clstraining_approverlevel_master
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Dim dsCombo As DataSet = Nothing
        Dim objEmp As New clsEmployee_Master
        Dim objUser As New clsUserAddEdit
        Dim objCalendar As New clsTraining_Calendar_Master
        Try
            dsCombo = objCalendar.getListForCombo("List", , StatusType.Open)
            With cboTrainingCalendar
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            If chkExternalApprover.Checked = False Then

                'dsCombo = objEmp.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                         CStr(Session("UserAccessModeSetting")), True, False, "List", _
                '                         , , , "isapproved = 1")

                'mdtEmployee = dsCombo.Tables("List").Copy
                'mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False

                'For i As Integer = 0 To mdtEmployee.Rows.Count - 1
                '    mdtEmployee.Rows(i).Item("ischeck") = False
                'Next

                'Dim drRow As DataRow = dsCombo.Tables("List").NewRow
                'drRow("name") = "Select"
                'drRow("employeeunkid") = 0
                'dsCombo.Tables("List").Rows.InsertAt(drRow, 0)
                'With cboApprover
                '    .DataTextField = "name"
                '    .DataValueField = "employeeunkid"
                '    .DataSource = dsCombo.Tables("List")
                '    .DataBind()
                'End With

                Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                           "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & _
                                           "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & _
                                           "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center



                dsCombo = objEmp.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                False, _
                                             "Employee", -1, False, "", CBool(Session("ShowFirstAppointmentDate")), False, False, True)

                dsCombo.Tables(0).Columns(1).ColumnName = "employeecode"
                dsCombo.Tables(0).Columns(2).ColumnName = "name"

                dsCombo.Tables(0).Columns(3).ColumnName = "DeptName"
                dsCombo.Tables(0).Columns(4).ColumnName = "job_name"

                mdtEmployee = dsCombo.Tables(0).Copy

                Dim dc As New DataColumn("ischeck", System.Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                mdtEmployee.Columns.Add(dc)

                Dim drRow As DataRow = mdtEmployee.NewRow
                drRow("name") = "Select"
                drRow("employeeunkid") = 0
                mdtEmployee.Rows.InsertAt(drRow, 0)
                With cboApprover
                    .DataTextField = "name"
                    .DataValueField = "employeeunkid"
                    .DataSource = mdtEmployee
                    .DataBind()
                End With


            Else
                dsList = objUser.GetExternalApproverList("List", CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         "345", True)

                Dim drRow As DataRow = dsList.Tables("List").NewRow
                drRow("name") = "Select"
                drRow("userunkid") = 0
                dsList.Tables("List").Rows.InsertAt(drRow, 0)
                With cboApprover
                    .DataTextField = "name"
                    .DataValueField = "userunkid"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If

            dsList = objtrApproverLevelMaster.getListForCombo(CInt(cboTrainingCalendar.SelectedValue), "List", True)
            With cboTrainingApproveLevel
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With


            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(enUserPriviledge.AllowToApproveTrainingRequestEmployeeMapping), CInt(Session("Fin_year")), True)

            With cboUser
                .DataValueField = "userunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("User")
                .DataBind()
            End With

            dsCombo = objMaster.getComboListForTrainingType(True, "List")
            With cboTrainingType
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
            objMaster = Nothing
            objtrApproverLevelMaster = Nothing
            objtrApproverLevelMaster = Nothing
            objEmp = Nothing
            objUser = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    Private Sub Fill_Employee()
        Dim objtrApproverTran As New clstrainingapprover_tran_emp_map
        Dim objEmployee As New clsEmployee_Master
        Dim dsEmployee As DataSet = Nothing
        Try
            Dim blnInActiveEmp As Boolean = False
            Dim strSearch As String = ""

            If mdtEmployee Is Nothing Then Exit Sub

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter
            End If

            If chkExternalApprover.Checked = False Then
                If CInt(cboApprover.SelectedValue) > 0 Then
                    strSearch &= "AND hremployee_master.employeeunkid <> " & CInt(cboApprover.SelectedValue) & " "
                End If
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If

            objEmployee = New clsEmployee_Master


            Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center



            dsEmployee = objEmployee.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            blnInActiveEmp, _
                                         "Employee", -1, False, strSearch, CBool(Session("ShowFirstAppointmentDate")), False, False, True)


            Dim iDataTab As New DataTable
            If mstrEmployeeIDs.Trim.Length > 0 Then
                iDataTab = New DataView(dsEmployee.Tables(0), "employeeunkid NOT IN(" & mstrEmployeeIDs & ") ", "", DataViewRowState.CurrentRows).ToTable().Copy()
            Else
                iDataTab = dsEmployee.Tables(0).Copy
            End If


            iDataTab.Columns(1).ColumnName = "employeecode"
            iDataTab.Columns(2).ColumnName = "name"

            iDataTab.Columns(3).ColumnName = "DeptName"
            iDataTab.Columns(4).ColumnName = "job_name"


            If iDataTab.Columns.Contains("IsCheck") = False Then
                Dim dccolumn As New DataColumn("IsCheck")
                dccolumn.DataType = Type.GetType("System.Boolean")
                dccolumn.DefaultValue = False
                iDataTab.Columns.Add(dccolumn)
            End If


            dgvAEmployee.DataSource = iDataTab
            dgvAEmployee.DataBind()

            If CInt(cboApprover.SelectedValue) > 0 Then
                objtrApproverTran._Approverunkid = mintapproverunkid
                Call Fill_Assigned_Employee()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtrApproverTran = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee(Optional ByVal blnIsFillter As Boolean = False)
        Try
            dtAssignEmpView = mdtAssignedEmp.DefaultView
            dtAssignEmpView.RowFilter = " AUD <> 'D' "
            dgvAssignedEmp.AutoGenerateColumns = False

            If blnIsFillter = True Then
                
            End If

            dgvAssignedEmp.DataSource = dtAssignEmpView
            dgvAssignedEmp.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Training Approver is mandatory information. Please provide Training Approver to continue."), Me)
                cboApprover.Focus()
                Return False
            End If

            If CInt(cboTrainingApproveLevel.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Training Approver Level is mandatory information. Please provide Training Approver Level to continue"), Me)
                cboTrainingApproveLevel.Focus()
                Return False
            End If

            If chkExternalApprover.Checked = False Then
                If CInt(cboUser.SelectedValue) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "User is mandatory information. Please provide User to continue"), Me)
                    cboUser.Focus()
                    Return False
                End If
            End If

            If CInt(cboTrainingType.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Training Type is mandatory information. Please provide Training Type to continue"), Me)
                cboTrainingType.Focus()
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Add_DataRow(ByVal dRow As DataGridItem)
        Dim objApproverMaster As New clstrainingapprover_master_emp_map
        Try
            If mdtAssignedEmp Is Nothing Then Exit Sub
            Dim mdtRow As DataRow = Nothing
            Dim dtAssignEmp() As DataRow = Nothing
            dtAssignEmp = mdtAssignedEmp.Select("employeeunkid = '" & CInt(dRow.Cells(3).Text) & "' AND AUD <> 'D' ")
            If dtAssignEmp.Length <= 0 Then
                mdtRow = mdtAssignedEmp.NewRow
                mdtRow.Item("ischeck") = False
                mdtRow.Item("approvertranunkid") = -1
                mdtRow.Item("approverunkid") = objApproverMaster._Approverunkid
                mdtRow.Item("employeeunkid") = CInt(dRow.Cells(3).Text)
                mdtRow.Item("userunkid") = CInt(Session("UserId"))
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtRow.Item("ecode") = dRow.Cells(1).Text
                mdtRow.Item("ename") = dRow.Cells(2).Text
                mdtRow.Item("edept") = dRow.Cells(4).Text
                mdtRow.Item("ejob") = dRow.Cells(5).Text

                mdtAssignedEmp.Rows.Add(mdtRow)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMaster = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objApproverMaster As clstrainingapprover_master_emp_map)
        Try
            objApproverMaster._ApproverEmpunkid = CInt(cboApprover.SelectedValue)
            objApproverMaster._Levelunkid = CInt(cboTrainingApproveLevel.SelectedValue)

            objApproverMaster._Isactive = True
            objApproverMaster._Userunkid = CInt(Session("UserId"))

            objApproverMaster._Calendarunkid = CInt(cboTrainingCalendar.SelectedValue)

            If mintapproverunkid > 0 Then
                objApproverMaster._Isvoid = objApproverMaster._Isvoid
                objApproverMaster._VoidDateTime = objApproverMaster._VoidDateTime
                objApproverMaster._VoidReason = objApproverMaster._VoidReason
                objApproverMaster._VoidUserunkid = objApproverMaster._VoidUserunkid
                objApproverMaster._Approverunkid = mintapproverunkid
            Else
                objApproverMaster._Isvoid = False
                objApproverMaster._VoidDateTime = Nothing
                objApproverMaster._VoidReason = ""
                objApproverMaster._VoidUserunkid = -1
            End If

            If chkExternalApprover.Checked = False Then
                objApproverMaster._MappedUserunkid = CInt(cboUser.SelectedValue)
            Else
                objApproverMaster._MappedUserunkid = CInt(cboApprover.SelectedValue)
            End If
            'Hemant (16 Sep 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Pending Training approval should be go for New User instead of Old User  after changing User  in Training Approver Add/Edit Screen
            objApproverMaster._OldMappedUserunkid = mintOldMappedUserId
            'Hemant (16 Sep 2022) -- End
            objApproverMaster._IsExternalApprover = chkExternalApprover.Checked

            objApproverMaster._TrainingTypeId = CInt(cboTrainingType.SelectedValue)

            Blank_ModuleName()
            objApproverMaster._ClientIP = Session("IP_ADD").ToString
            objApproverMaster._FormName = mstrModuleName
            objApproverMaster._HostName = Session("HOST_NAME").ToString
            objApproverMaster._IsWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue(ByVal objApproverMaster As clstrainingapprover_master_emp_map)
        Try

            chkExternalApprover.Checked = objApproverMaster._IsExternalApprover
            Call chkExternalApprover_CheckedChanged(Nothing, Nothing)

            Dim objEmployee As New clsEmployee_Master

            If objApproverMaster._IsExternalApprover = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = objApproverMaster._ApproverEmpunkid
                Dim strAppName As String = objUser._Firstname + " " + objUser._Lastname
                If strAppName.Trim.Length > 0 Then
                    cboApprover.SelectedItem.Text = strAppName
                Else
                    cboApprover.SelectedItem.Text = objUser._Username
                End If
                mintApproverEmpunkid = objUser._Userunkid
                cboApprover.SelectedValue = mintApproverEmpunkid.ToString
                objUser = Nothing
            Else
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objApproverMaster._ApproverEmpunkid
                mintApproverEmpunkid = objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                cboApprover.SelectedValue = mintApproverEmpunkid.ToString
            End If

            cboTrainingCalendar.SelectedValue = CStr(objApproverMaster._Calendarunkid)
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-22 - As a user, I want to have the ability to perform approver migrations incase of employee/approver movements
            cboTrainingCalendar_SelectedIndexChanged(Nothing, Nothing)
            'Hemant (22 Dec 2022) -- End	
            cboTrainingApproveLevel.SelectedValue = CStr(objApproverMaster._Levelunkid)
            cboUser.SelectedValue = CStr(objApproverMaster._MappedUserunkid)
            mintOldMappedUserId = CInt(objApproverMaster._MappedUserunkid)
            cboTrainingType.SelectedValue = CStr(objApproverMaster._TrainingTypeId)

            If mintapproverunkid > 0 Then
                mstrEmployeeIDs = objApproverMaster.GetApproverEmployeeId(CInt(objApproverMaster._Calendarunkid), mintApproverEmpunkid, CBool(objApproverMaster._IsExternalApprover), CInt(objApproverMaster._TrainingTypeId))
                'Hemant (22 Dec 2022) -- [CInt(objApproverMaster._Calendarunkid)]
            End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                Call Fill_Employee()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("trapproverunkid") = Nothing
            dtEmployee = Nothing
            dtAssignEmpView = Nothing
            mdtEmployee = Nothing
            mdtAssignedEmp = Nothing
            Response.Redirect(Session("rootpath").ToString & "Training/Training_Approver/wPg_TrainingApproversEmployeeMappingList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then Exit Sub


            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvAEmployee.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)
            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            If gRow.Count > 0 Then
                For Each gvItem As DataGridItem In gRow
                    Call Add_DataRow(gvItem)
                Next
            End If

            Call Fill_Assigned_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDeleteA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            If mdtAssignedEmp Is Nothing Then Exit Sub
            popYesNo.Title = "Aruti"
            Language.setLanguage(mstrModuleName)
            popYesNo.Message = Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete Selected Employee?")
            popYesNo.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objApproverMaster As New clstrainingapprover_master_emp_map
        Try
            If Validation() = False Then Exit Sub
            Call SetValue(objApproverMaster)

            Dim count As Integer = CInt(mdtAssignedEmp.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtAssignedEmp.Rows.Count = 0 OrElse count = 0 OrElse dgvAssignedEmp.Items.Count = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Employee is compulsory information.Please Check atleast One Employee."), Me)
                dgvAEmployee.Focus()
                Exit Sub
            End If

            If mintapproverunkid > 0 Then
                If objApproverMaster.Update(mdtAssignedEmp) Then
                    mintapproverunkid = -1
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Training Approver updated successfully."), Me, Convert.ToString(Session("rootpath")) & "Training/Training_Approver/wPg_TrainingApproversEmployeeMappingList.aspx")

                Else
                    DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
                    Exit Sub
                End If
            Else
                If objApproverMaster.Insert(mdtAssignedEmp) Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Training Approver saved successfully."), Me, Convert.ToString(Session("rootpath")) & "Training/Training_Approver/wPg_TrainingApproversEmployeeMappingList.aspx")

                Else
                    DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
                    Exit Sub
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMaster = Nothing
        End Try
    End Sub

    Protected Sub popYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popYesNo.buttonYes_Click
        Try
            dtAssignEmpView = mdtAssignedEmp.DefaultView

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing

            gRow = dgvAssignedEmp.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("ChkAsgSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please check atleast one of the employee to unassigned."), Me)
                Exit Sub
            End If

            Dim blnFlag As Boolean = False

            'Dim objLoan As New clsProcess_pending_loan
            Dim mblnFlag As Boolean = True


            Dim drTemp As DataRow() = Nothing
            For Each drRow As DataGridItem In gRow

                'If objLoan.GetApproverPendingLoanFormCount(mintapproverunkid, CInt(drRow.Cells(5).Text).ToString()) <= 0 Then
                drTemp = dtAssignEmpView.Table.Select("employeeunkid = '" & CInt(drRow.Cells(5).Text).ToString() & "'")
                For i As Integer = 0 To drTemp.Length - 1
                    drTemp(i).Item("AUD") = "D"
                    drTemp(i).Item("isvoid") = True
                    drTemp(i).Item("voiddatetime") = DateAndTime.Now
                    drTemp(i).Item("voiduserunkid") = Session("UserId")
                    drTemp(i).Item("voidreason") = ""
                    drTemp(i).AcceptChanges()
                Next

                'Else
                'mblnFlag = False
                'End If
            Next

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "This Employee(s) has pending training request(s).You cannot delete this employee(s)."), Me)
            End If
            'objLoan = Nothing


            dtAssignEmpView.Table.AcceptChanges()

            Call Fill_Assigned_Employee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call Fill_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
            Call Fill_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub cboApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.SelectedIndexChanged, _
                                                                                                                cboTrainingType.SelectedIndexChanged
        Dim objApproverMaster As New clstrainingapprover_master_emp_map
        Dim dsCombo As DataSet
        Dim objUser As New clsUserAddEdit
        Try


            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(enUserPriviledge.AllowToApproveTrainingRequestEmployeeMapping), CInt(Session("Fin_year")), True)

            Dim objoption As New clsPassowdOptions

            If objoption._IsEmployeeAsUser Then

                Dim mintuserid As Integer = objUser.Return_UserId(CInt(cboApprover.SelectedValue), CInt(Session("CompanyUnkId")))
                Dim drrow() As DataRow = dsCombo.Tables("User").Select("userunkid = " & mintuserid)
                If drrow.Length > 0 Then
                    cboUser.SelectedValue = mintuserid.ToString
                Else
                    cboUser.SelectedValue = "0"
                End If
            End If

            mstrEmployeeIDs = objApproverMaster.GetApproverEmployeeId(CInt(cboTrainingCalendar.SelectedValue), CInt(cboApprover.SelectedValue), chkExternalApprover.Checked, CInt(cboTrainingType.SelectedValue))
            'Hemant (22 Dec 2022) -- [CInt(cboTrainingCalendar.SelectedValue)]
            If CInt(cboApprover.SelectedValue) > 0 Then
                cboTrainingApproveLevel.SelectedIndex = 0
                Call Fill_Employee()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMaster = Nothing
            objUser = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
        End Try
    End Sub

    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-22 - As a user, I want to have the ability to perform approver migrations incase of employee/approver movements
    Protected Sub cboTrainingCalendar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCalendar.SelectedIndexChanged
        Dim objtrApproverLevelMaster As New clstraining_approverlevel_master
        Dim dsList As DataSet = Nothing
        Try
            dsList = objtrApproverLevelMaster.getListForCombo(CInt(cboTrainingCalendar.SelectedValue), "List", True)
            With cboTrainingApproveLevel
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objtrApproverLevelMaster = Nothing
            dsList = Nothing
        End Try
    End Sub
    'Hemant (22 Dec 2022) -- End	

#End Region

#Region "CheckBox Events"
    Protected Sub chkExternalApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            If chkExternalApprover.Checked = True Then
                lblUser.Visible = False
                cboUser.Visible = False
            Else
                lblUser.Visible = True
                cboUser.Visible = True
                cboUser.SelectedValue = "0"
            End If
            Call FillCombo()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Link Button Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, lblPageHeader.Text)

            Me.lblInfo.Text = Language._Object.getCaption("gbInfo", Me.lblInfo.Text)
            Me.lblAssignedEmployee.Text = Language._Object.getCaption("gbAssignedEmployee", Me.lblAssignedEmployee.Text)
            Me.lblTitle.Text = Language._Object.getCaption(lblTitle.ID, Me.lblTitle.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.ID, Me.lblApproveLevel.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblTrainingApproverName.Text = Language._Object.getCaption(Me.lblTrainingApproverName.ID, Me.lblTrainingApproverName.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.ID, Me.lblUser.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.ID, Me.lblApproverLevel.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.ID, Me.btnDeleteA.Text).Replace("&", "")
            Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.ID, Me.btnOK.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvAEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvAEmployee.Columns(1).FooterText, Me.dgvAEmployee.Columns(1).HeaderText)
            Me.dgvAEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvAEmployee.Columns(2).FooterText, Me.dgvAEmployee.Columns(2).HeaderText)

            Me.dgvAssignedEmp.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(1).FooterText, Me.dgvAssignedEmp.Columns(1).HeaderText)
            Me.dgvAssignedEmp.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(2).FooterText, Me.dgvAssignedEmp.Columns(2).HeaderText)
            Me.dgvAssignedEmp.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(3).FooterText, Me.dgvAssignedEmp.Columns(3).HeaderText)
            Me.dgvAssignedEmp.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(4).FooterText, Me.dgvAssignedEmp.Columns(4).HeaderText)

            Me.dgTrainingScheme.Columns(1).HeaderText = Language._Object.getCaption(Me.dgTrainingScheme.Columns(1).FooterText, Me.dgTrainingScheme.Columns(1).HeaderText)
            Me.dgTrainingScheme.Columns(2).HeaderText = Language._Object.getCaption(Me.dgTrainingScheme.Columns(2).FooterText, Me.dgTrainingScheme.Columns(2).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
