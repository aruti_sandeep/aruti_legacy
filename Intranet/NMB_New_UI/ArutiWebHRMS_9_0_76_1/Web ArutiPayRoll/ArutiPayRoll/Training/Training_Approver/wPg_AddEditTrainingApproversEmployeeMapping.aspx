﻿<%@ Page Title="Add/Edit Training Approver" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_AddEditTrainingApproversEmployeeMapping.aspx.vb"
    Inherits="Training_Training_Approver_wPg_AddEditTrainingApproversEmployeeMapping" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll1 = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll1.Y).val());
                $("#scrollable-container1").scrollTop($(scroll2.Y).val());
            }
        }

        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest(".body");
            $("[id*=ChkgvSelect]", grid).prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest(".body");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        function FromSearching() {
            if ($('#txtSearchEmp').val().length > 0) {
                $('#<%= dgvAEmployee.ClientID %> tbody tr').hide();
                $('#<%= dgvAEmployee.ClientID %> tbody tr:first').show();
                $('#<%= dgvAEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmp').val() + '\')').parent().show();
            }
            else if ($('#txtSearchEmp').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvAEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearchEmp').val('');
            $('#<%= dgvAEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchEmp').focus();
        }

        $("body").on("click", "[id*=ChkAsgAll]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest(".body");
            $("[id*=ChkAsgSelect]", grid).prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkAsgSelect]", function() {
            var grid = $(this).closest(".body");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=ChkAsgSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        function AssignedEmpSearching() {
            if ($('#txtAssignedEmpSearch').val().length > 0) {
                $('#<%= dgvAssignedEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvAssignedEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvAssignedEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtAssignedEmpSearch').val() + '\')').parent().show();
            }
            else if ($('#txtAssignedEmpSearch').val().length == 0) {
                resetAssignedEmpSearchValue();
            }
            if ($('#<%= dgvAssignedEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetAssignedEmpSearchValue();
            }
        }
        function resetAssignedEmpSearchValue() {
            $('#txtAssignedEmpSearch').val('');
            $('#<%= dgvAssignedEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtAssignedEmpSearch').focus();
        }

        $("body").on("click", "[id*=ChkAllTrainingScheme]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest(".body");
            $("[id*=ChkdgSelectTrainingScheme]", grid).prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkdgSelectTrainingScheme]", function() {
            var grid = $(this).closest(".body");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=ChkdgSelectTrainingScheme]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        function TrainingSchemeSearching() {
            if ($('#txtSearchScheme').val().length > 0) {
                $('#<%= dgTrainingScheme.ClientID %> tbody tr').hide();
                $('#<%= dgTrainingScheme.ClientID %> tbody tr:first').show();
                $('#<%= dgTrainingScheme.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchScheme').val() + '\')').parent().show();
            }
            else if ($('#txtSearchScheme').val().length == 0) {
                resetTrainingSchemeSearchValue();
            }
            if ($('#<%= dgTrainingScheme.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetTrainingSchemeSearchValue();
            }
        }
        function resetTrainingSchemeSearchValue() {
            $('#txtSearchScheme').val('');
            $('#<%= dgTrainingScheme.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchScheme').focus();
        }

    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupTrainingSchemeMapping" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblApprover" runat="server" PopupControlID="pnlTrainingSchemeMapping"
                    CancelControlID="btnCancel" />
                <asp:Panel ID="pnlTrainingSchemeMapping" runat="server" CssClass="card modal-dialog "
                    Style="display: none;" DefaultButton="btnOK">
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Training Scheme Mapping" CssClass="form-label"></asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Approver Detail" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height: 525px;">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblApprover" Style="margin-left: 10px" runat="server" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApproverLevel" runat="server" Text="Approver Level" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblApproverLevel" runat="server" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="divider divider-with-text-left">
                                    <span class="ant-divider-inner-text"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="txtSearchScheme" name="txtSearch" placeholder="Type To Search Text"
                                                    maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="TrainingSchemeSearching();" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive" style="max-height: 250px;">
                                    <asp:DataGrid ID="dgTrainingScheme" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <ItemStyle />
                                        <Columns>
                                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="ChkAllTrainingScheme" runat="server" Text=" " CssClass="chk-sm" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkdgSelectTrainingScheme" runat="server" Text=" " CssClass="chk-sm" />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="code" HeaderStyle-Width="40%" HeaderText="Training Scheme Code"
                                                ReadOnly="true" FooterText="dgcolhSchemeCode"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="name" HeaderStyle-Width="60%" HeaderText="Training Scheme Name"
                                                ReadOnly="true" FooterText="dgcolhTrainingScheme"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Trainingschememappingunkid" Visible="false" ReadOnly="true"
                                                FooterText="objdgcolhTrainingSchemeMappingunkid"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="trainingschemeunkid" Visible="false" ReadOnly="true">
                                            </asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="False" />
                                    </asp:DataGrid>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btn btn-primary" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc2:ConfirmYesNo ID="popYesNo" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Training Approver" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card ">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix ">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblInfo" runat="server" Text="Training Approver Information" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar" Width="100%"
                                                            CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingCalendar" runat="server"  AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  m-t-40">
                                                        <asp:CheckBox ID="chkExternalApprover" runat="server" Text="Make External Approver"
                                                            AutoPostBack="true" CssClass="filled-in" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingApproverName" runat="server" Text="Training Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingType" runat="server" Text="Training Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproveLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingApproveLevel" runat="server" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUser" runat="server" Text="User" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboUser" runat="server" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row clearfix m-r-10">
                                                    <h4>
                                                        <div style="float: left;">
                                                        </div>
                                                        <%--<div style="float: right;">
                                                            <ul class="header-dropdown m-r-15" style="list-style: none;">
                                                                <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                                                    role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v">
                                                                    </i></a>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li>
                                                                            <asp:LinkButton ID="lnkMapTrainingScheme" runat="server" Text="Map Training Scheme"></asp:LinkButton></li>
                                                                        <li>
                                                                            <asp:LinkButton ID="lnkReset" runat="server" Text="Reset"></asp:LinkButton></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>--%>
                                                        <div style="float: right;">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations">
                                                                 <i class="fas fa-sliders-h"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </h4>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" id="txtSearchEmp" name="txtSearch" placeholder="Type To Search Text"
                                                                    maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 300px;">
                                                            <asp:DataGrid ID="dgvAEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered" Width="99%">
                                                                <ItemStyle />
                                                                <Columns>
                                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAll" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle />
                                                                        <ItemStyle />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="colhEName" />
                                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        FooterText="colhEmpId" Visible="false" />
                                                                    <asp:BoundColumn DataField="DeptName" HeaderText="Department" FooterText="objdgcolhDepartment"
                                                                        Visible="false" />
                                                                    <asp:BoundColumn DataField="job_name" HeaderText='Job" ' FooterText="objdgcolhJob"
                                                                        Visible="false" />
                                                                </Columns>
                                                                <HeaderStyle Font-Bold="False" />
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAssignedEmployee" runat="server" Text="Assigned Employee" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" id="txtAssignedEmpSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                    maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="AssignedEmpSearching();" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 635px;">
                                                            <asp:DataGrid ID="dgvAssignedEmp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered" Width="150%">
                                                                <ItemStyle />
                                                                <Columns>
                                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAsgAll" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkAsgSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="ecode" HeaderText="Code" ReadOnly="true" FooterText="colhaCode" />
                                                                    <asp:BoundColumn DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhaEmp" />
                                                                    <asp:BoundColumn DataField="edept" HeaderText="Department" ReadOnly="true" FooterText="colhaDepartment" />
                                                                    <asp:BoundColumn DataField="ejob" HeaderText="Job" ReadOnly="true" FooterText="colhaJob" />
                                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        FooterText="colhaEmpId" Visible="false" />
                                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="AMasterId" ReadOnly="true"
                                                                        FooterText="colhAMasterId" Visible="false" />
                                                                    <asp:BoundColumn DataField="approvertranunkid" HeaderText="ATranId" ReadOnly="true"
                                                                        FooterText="colhATranId" Visible="false" />
                                                                </Columns>
                                                                <HeaderStyle Font-Bold="False" />
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnDeleteA" runat="server" Text="Delete" CssClass="btn btn-danger" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
