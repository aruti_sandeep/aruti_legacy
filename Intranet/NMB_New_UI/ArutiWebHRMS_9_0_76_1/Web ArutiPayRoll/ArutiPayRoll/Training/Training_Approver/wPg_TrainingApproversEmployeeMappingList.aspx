﻿<%@ Page Title="Training Approvers List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_TrainingApproversEmployeeMappingList.aspx.vb"
    Inherits="Training_Training_Approver_wPg_TrainingApproversEmployeeMappingList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Approvers List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar" Width="100%"
                                            CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTrainingCalendar" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <asp:Label ID="lblTrainingLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboTrainingLevel" runat="server"  data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <asp:Label ID="lblTrainingApprover" Style="margin-left: 10px" runat="server" CssClass="form-label"
                                            Text="Training Approver"></asp:Label>
                                        <asp:DropDownList ID="cboTrainingApprover" runat="server"  data-live-search="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <asp:Label ID="lblTrainingType" runat="server" Text="Training Type" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboTrainingType" runat="server"  data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboStatus" runat="server"  data-live-search="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvApproversList" runat="server" AllowPaging="false" CssClass="table table-hover table-bordered"
                                                Width="99%" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="brnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                <i class="fas fa-pencil-alt"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="100px" Visible="true" ItemStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkSetInActive" CommandName="InActive" ToolTip="Set InActive"
                                                                    runat="server">
                                                                    <i class="fas fa-user-times" style="font-size:18px;color:red"></i>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="lnkSetActive" CommandName="Active" Font-Underline="false" Style="font-weight: bold;
                                                                    font-size: 12px" Text="Set Active" runat="server">
                                                                     <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="levelname" HeaderText="Level" Visible="true" ReadOnly="true"
                                                        FooterText="colhLevel" />
                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="Approverunkid" Visible="false"
                                                        FooterText="colhlnapproverunkid"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="name" HeaderText="Training Approver" ReadOnly="true"
                                                        FooterText="colhTrainingApprover" />
                                                    <asp:BoundColumn DataField="departmentname" HeaderText="Department" ReadOnly="true"
                                                        FooterText="colhDepartment" />
                                                    <asp:BoundColumn DataField="jobname" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                    <asp:BoundColumn DataField="isactive" HeaderText="IsActive" Visible="false" ReadOnly="true"
                                                        FooterText="colhIsActive" />
                                                    <asp:BoundColumn DataField="extapprover" HeaderText="External Approver" ReadOnly="true"
                                                        FooterText="colhIsExternalApprover" />
                                                    <asp:BoundColumn DataField="TrainingType" HeaderText="Training Type" ReadOnly="true"
                                                        FooterText="colhTrainingType" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
