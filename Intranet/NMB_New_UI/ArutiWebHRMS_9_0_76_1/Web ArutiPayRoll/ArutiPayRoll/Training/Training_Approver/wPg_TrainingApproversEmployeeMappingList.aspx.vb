﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Training_Training_Approver_wPg_TrainingApproversEmployeeMappingList
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingApproverEmployeeMappingList"
    Private mintUserMappunkid As Integer = -1
    Dim DisplayMessage As New CommonCodes
    Private mstrapproverunkid As Integer = -1
#End Region

#Region "Page's Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Online_Job_Applications_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                GC.Collect()
                Call SetLanguage()
                Call FillCombo()
                Call SetVisibility()

                 Call FillList()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objLevel As New clstraining_approverlevel_master
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Try
            dsList = objCalendar.getListForCombo("List", , StatusType.Open)
            With cboTrainingCalendar
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objLevel.getListForCombo(CInt(cboTrainingCalendar.SelectedValue), "Level", True)

            With cboTrainingLevel
                .DataValueField = "levelunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            cboStatus.Items.Add(Language.getMessage(mstrModuleName, 5, "Active"))
            cboStatus.Items.Add(Language.getMessage(mstrModuleName, 6, "InActive"))
            cboStatus.SelectedIndex = 0

            dsList = objMaster.getComboListForTrainingType(True, "List")
            With cboTrainingType
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            Call cboTrainingLevel_SelectedIndexChanged(cboTrainingLevel, Nothing)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
            objLevel = Nothing
            objMaster = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
        End Try
    End Sub

    Private Sub FillList()
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map
        Dim dsApproverList As DataSet = Nothing
        Dim dtApprover As DataTable = Nothing
        Try

            Dim strSearch As String = ""

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewTrainingApproverEmployeeMappingList")) = False Then Exit Sub
            End If

            If CInt(cboTrainingCalendar.SelectedValue) > 0 Then
                strSearch &= "AND trtrainingapprover_master.calendarunkid = " & CInt(cboTrainingCalendar.SelectedValue) & " "
            End If

            If CInt(cboTrainingLevel.SelectedValue) > 0 Then
                strSearch &= "AND hrtraining_approverlevel_master.levelunkid = " & CInt(cboTrainingLevel.SelectedValue) & " "
            End If

            If CInt(cboTrainingApprover.SelectedValue) > 0 Then
                strSearch &= "AND trtrainingapprover_master.approverunkid = " & CInt(cboTrainingApprover.SelectedValue) & " "
            End If

            If CInt(cboTrainingType.SelectedValue) > 0 Then
                strSearch &= "AND trtrainingapprover_master.trainingtypeid = " & CInt(cboTrainingType.SelectedValue) & " "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsApproverList = objTrainingApprover.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                 CBool(IIf(cboStatus.SelectedIndex = 0, True, False)), False, strSearch)

            dtApprover = New DataView(dsApproverList.Tables("List"), "", "priority,name", DataViewRowState.CurrentRows).ToTable

            dgvApproversList.Columns(0).Visible = CBool(Session("AllowToEditTrainingApproverEmployeeMapping"))
            dgvApproversList.Columns(1).Visible = CBool(Session("AllowToDeleteTrainingApproverEmployeeMapping"))
            dgvApproversList.Columns(2).Visible = CBool(Session("AllowToSetActiveInactiveTrainingApproverEmployeeMapping"))


            dgvApproversList.DataSource = dtApprover
            dgvApproversList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
            If IsNothing(dsApproverList) = False Then
                dsApproverList.Clear()
                dsApproverList = Nothing
            End If
            If IsNothing(dtApprover) = False Then
                dtApprover.Clear()
                dtApprover = Nothing
            End If
        End Try
    End Sub

    Private Sub setATWebParameters(ByVal objTrainingApprover As clstrainingapprover_master_emp_map)
        Try
            Blank_ModuleName()
            objTrainingApprover._ClientIP = Session("IP_ADD").ToString
            objTrainingApprover._FormName = mstrModuleName
            objTrainingApprover._HostName = Session("HOST_NAME").ToString
            objTrainingApprover._IsWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                btnNew.Enabled = CBool(Session("AllowToAddTrainingApproverEmployeeMapping"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


#End Region

#Region "ComboBox Events"

    Protected Sub cboTrainingLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingLevel.SelectedIndexChanged, _
                                                                                                                    cboTrainingType.SelectedIndexChanged
        Dim objApprover As New clstrainingapprover_master_emp_map
        Dim dtTable As DataTable = Nothing
        Try

            'Hemant (21 Jun 2024) -- Start
            'ISSUE(NMB): A1X - 2650 : Slowness issue
            Dim strSearch As String = ""

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewTrainingApproverEmployeeMappingList")) = False Then Exit Sub
            End If

            If CInt(cboTrainingCalendar.SelectedValue) > 0 Then
                strSearch &= "AND trtrainingapprover_master.calendarunkid = " & CInt(cboTrainingCalendar.SelectedValue) & " "
            End If

            If CInt(cboTrainingLevel.SelectedValue) > 0 Then
                strSearch &= "AND hrtraining_approverlevel_master.levelunkid = " & CInt(cboTrainingLevel.SelectedValue) & " "
            End If

            If CInt(cboTrainingType.SelectedValue) > 0 Then
                strSearch &= "AND trtrainingapprover_master.trainingtypeid = " & CInt(cboTrainingType.SelectedValue) & " "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If
            'Hemant (21 Jun 2024) -- End


            dtTable = objApprover.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), _
                                            True, _
                                            CBool(Session("IsIncludeInactiveEmp")), _
                                            "List", True, False, _
                                            strSearch).Tables(0)
            'Hemant (21 Jun 2024) -- [CStr(IIf(CInt(cboTrainingLevel.SelectedValue) > 0, "hrtraining_approverlevel_master.levelunkid = " & CInt(cboTrainingLevel.SelectedValue), ""))  --> strSearch]


            Dim drRow As DataRow = dtTable.NewRow
            drRow("approverunkid") = 0
            drRow("ApproverNameWithLevel") = Language.getMessage(mstrModuleName, 4, "Select")
            dtTable.Rows.InsertAt(drRow, 0)

            With cboTrainingApprover
                .DataTextField = "ApproverNameWithLevel"
                .DataValueField = "approverunkid"
                .DataSource = dtTable
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprover = Nothing
            If IsNothing(dtTable) = False Then
                dtTable.Clear()
                dtTable = Nothing
            End If
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("trapproverunkid") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Training/Training_Approver/wPg_AddEditTrainingApproversEmployeeMapping.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboTrainingLevel.SelectedIndex = 0
            cboTrainingApprover.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            dgvApproversList.DataSource = New List(Of String)
            dgvApproversList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map
        Try
            If dgvApproversList.Items.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), Me)
            End If
            If popup_DeleteReason.Reason.Trim.Length <= 0 Then
                Exit Sub
            Else
                objTrainingApprover._VoidReason = popup_DeleteReason.Reason.ToString
            End If

            objTrainingApprover._Isvoid = True
            objTrainingApprover._VoidDateTime = DateAndTime.Now.Date
            objTrainingApprover._VoidUserunkid = CInt(Session("UserId"))
            Call setATWebParameters(objTrainingApprover)

            objTrainingApprover.Delete(CInt(Me.ViewState("TrainingApproverunkid")))

            Call FillList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_DeleteReason.Reason = ""
            Me.ViewState("TrainingApproverunkid") = Nothing
            objTrainingApprover = Nothing
        End Try
    End Sub
#End Region

#Region "DataGrid events"

    Protected Sub dgvApproversList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvApproversList.ItemCommand
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map
        Try
            If e.CommandName.ToUpper = "EDIT" Then
                Session.Add("trapproverunkid", e.Item.Cells(4).Text)
                Response.Redirect(Session("rootpath").ToString & "Training/Training_Approver/wPg_AddEditTrainingApproversEmployeeMapping.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                Me.ViewState.Add("TrainingApproverunkid", e.Item.Cells(4).Text)
                Language.setLanguage(mstrModuleName)
                popup_DeleteReason.Title = Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")
                popup_DeleteReason.Show()

            ElseIf e.CommandName.ToUpper = "INACTIVE" Then
                objTrainingApprover._Isactive = False
                Call setATWebParameters(objTrainingApprover)

                If objTrainingApprover.MakeActiveInActive(CInt(e.Item.Cells(4).Text)) = False Then
                    DisplayMessage.DisplayMessage(objTrainingApprover._Message, Me)
                    Exit Sub
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Approver InActive successfully."), Me)
                    Call FillList()
                End If

            ElseIf e.CommandName.ToUpper = "ACTIVE" Then
                objTrainingApprover._Isactive = True
                Call setATWebParameters(objTrainingApprover)

                If objTrainingApprover.MakeActiveInActive(CInt(e.Item.Cells(4).Text)) = False Then
                    DisplayMessage.DisplayMessage(objTrainingApprover._Message, Me)
                    Exit Sub
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Approver Active successfully."), Me)
                    Call FillList()
                End If


            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
        End Try
    End Sub

    Protected Sub dgvApproversList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvApproversList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                If cboStatus.SelectedItem.Text.ToUpper = "ACTIVE" Then
                    CType(e.Item.Cells(2).FindControl("lnkSetActive"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkSetInActive"), LinkButton).Visible = True
                Else
                    CType(e.Item.Cells(2).FindControl("lnkSetInActive"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkSetActive"), LinkButton).Visible = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblTrainingLevel.Text = Language._Object.getCaption(Me.lblTrainingLevel.ID, Me.lblTrainingLevel.Text)
            Me.lblTrainingApprover.Text = Language._Object.getCaption(Me.lblTrainingApprover.ID, Me.lblTrainingApprover.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
