﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingSettings.aspx.vb"
    Inherits="Training_Training_Settings_wPg_TrainingSettings" Title="Training Settings" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ColorPickerTextbox.ascx" TagName="ColorPickerTextbox"
    TagPrefix="colt" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();

            setEvents();
           
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Setups"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#TrainingCalendar" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingPriorities" data-toggle="tab">
                                        <asp:Label ID="lblTrainingPriorities" runat="server" Text="Training Priorities"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingCategories" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCategories" runat="server" Text="Training Categories"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingCosts" data-toggle="tab">
                                        <asp:Label ID="lblTrainingCosts" runat="server" Text="Training Cost Item"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#LearningMethods" data-toggle="tab">
                                        <asp:Label ID="lblLearningMethods" runat="server" Text="Learning Method Name"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#TrainingVenues" data-toggle="tab">
                                        <asp:Label ID="lblTrainingVenues" runat="server" Text="Training Venues"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#CostCenterMapping" data-toggle="tab">
                                        <asp:Label ID="lbltbCostCenterMapping" runat="server" Text="Cost Center Mpping"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#OtherSettings" data-toggle="tab">
                                        <asp:Label ID="lblOtherSettings" runat="server" Text="Other Settings"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="TrainingCalendar">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblCCalendar" runat="server" Text="Add/Edit Training Calendar"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCName" runat="server" Text="Name" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCName" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCStartDate" runat="server" Text="Start Date" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <uc2:DateCtrl ID="dtpCStartDate" runat="server" AutoPostBack="false" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCEndDate" runat="server" Text="End Date" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <uc2:DateCtrl ID="dtpCEndDate" runat="server" AutoPostBack="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCDescription" runat="server" Text="Description" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCDescription" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnCSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnCReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="gvTrainingCalendar" runat="server" AutoGenerateColumns="false"
                                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="calendarunkid,statusunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="CEdit" runat="server" ToolTip="Edit" CommandName="EditRow">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="CDelete" runat="server" ToolTip="Delete" CommandName="DeleteRow">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="calendar_name" HeaderText="Name" ReadOnly="True" FooterText="colhCName" />
                                                                            <asp:BoundField DataField="sdate" HeaderText="Start Date" ReadOnly="True" FooterText="colhStartDate" />
                                                                            <asp:BoundField DataField="edate" HeaderText="End Date" ReadOnly="True" FooterText="colhCEndDate" />
                                                                            <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True" FooterText="colhCStatus" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingPriorities">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblPPriorities" runat="server" Text="Add/Edit Training Priorities"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPPriorityName" runat="server" Text="Priority Name" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtPPriorityName" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPPriority" runat="server" Text="Priority" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtPPriority" runat="server" Type="Numeric" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnPSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnPReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="gvTrainingPriorities" runat="server" AutoGenerateColumns="false"
                                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="trpriorityunkid,trdefaultypeid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="CPEdit" runat="server" ToolTip="Edit" CommandName="EditRow">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="CPDelete" runat="server" ToolTip="Delete" CommandName="DeleteRow">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="trpriority_name" HeaderText="Priority Name" ReadOnly="True"
                                                                                FooterText="colhPPriorityName" />
                                                                            <asp:BoundField DataField="trpriority" HeaderText="Priority" ReadOnly="True" FooterText="colhPPriority" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingCategories">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblCtTrainingCategories" runat="server" Text="Add/Edit Training Categories"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCtCategoryCode" runat="server" Text="Category Code" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCtCategoryCode" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCtCategoryName" runat="server" Text="Category Name" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCtCategoryName" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCtPriority" runat="server" Text="Priority" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboPriority" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCtDescription" runat="server" Text="Description" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCtDescription" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnCtSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnCtReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="gvTrainingCategories" runat="server" AutoGenerateColumns="false"
                                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="categoryunkid,categorydefaultypeid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="CtEdit" runat="server" ToolTip="Edit" CommandName="EditRow">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="CtDelete" runat="server" ToolTip="Delete" CommandName="DeleteRow">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="categorycode" HeaderText="Category Code" ReadOnly="True"
                                                                                FooterText="colhCtCategoryCode" />
                                                                            <asp:BoundField DataField="categoryname" HeaderText="Category Name" ReadOnly="True"
                                                                                FooterText="colhCtCategoryName" />
                                                                            <asp:BoundField DataField="trpriority" HeaderText="Priority" ReadOnly="True" FooterText="colhCtPriority" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingCosts">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblCoTrainingCosts" runat="server" Text="Add/Edit Traning Costs"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoItemCode" Text="Item Code" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoItemCode" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoItemName" Text="Item Name" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoItemName" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCoDescription" Text="Description" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCoDescription" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnCoSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnCoReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="gvTrainingCosts" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="infounkid,infotypeid,defaultitemtypeid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="CoEdit" runat="server" ToolTip="Edit" CommandName="EditRow">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="CoDelete" runat="server" ToolTip="Delete" CommandName="DeleteRow">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="info_code" HeaderText="Training Cost Code" ReadOnly="True"
                                                                                FooterText="colhCoItemCode" />
                                                                            <asp:BoundField DataField="info_name" HeaderText="Training Cost Name" ReadOnly="True"
                                                                                FooterText="colhCoItemName" />
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                                                                FooterText="colhCoDescription" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="LearningMethods">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblLLearningMethods" runat="server" Text="Add/Edit Learning Methods"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLItemCode" Text="Item Code" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtLItemCode" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLItemName" Text="Item Name" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtLItemName" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLDescription" Text="Description" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtLDescription" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnLSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnLReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="gvLearningMethods" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="infounkid,infotypeid,defaultitemtypeid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="LEdit" runat="server" ToolTip="Edit" CommandName="EditRow">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="LDelete" runat="server" ToolTip="Delete" CommandName="DeleteRow">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="info_code" HeaderText="Learning Method Code" ReadOnly="True"
                                                                                FooterText="colLItemCode" />
                                                                            <asp:BoundField DataField="info_name" HeaderText="Learning Method Name" ReadOnly="True"
                                                                                FooterText="colhLItemName" />
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                                                                FooterText="colhLDescription" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="TrainingVenues">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblVTrainingVenues" runat="server" Text="Add/Edit Training Venues"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkCopyAddressAlreadyProvided" runat="server" Text="Copy Training Provider Address"
                                                                    AutoPostBack="true" />
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVTrainingProvider" Text="Training Provider" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrainingProvider" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVVenueName" Text="Venue Name" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtVVenueName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVAddress" Text="Address" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtVAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVCountry" Text="Country" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboCountry" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVState" Text="State" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboState" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVCity" Text="City" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboCity" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVContactPerson" Text="Contact Person" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtVContactPerson" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVFax" Text="Fax" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtVFax" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVTelNo" Text="Tel No" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtVTelNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVEmail" Text="Email" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtVEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblVCapacity" Text="Capacity" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtVCapacity" runat="server" Type="Numeric" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-30 ">
                                                                <asp:CheckBox ID="chkIsLocked" runat="server" Text="Locked" />
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnVSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnVReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvTrainingVenues" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="venueunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="lnkIEdit" runat="server" ToolTip="Edit" CommandName="EditRow">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkIDelete" runat="server" ToolTip="Delete" CommandName="DeleteRow">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="venuename" HeaderText="Venue Name" ReadOnly="True" FooterText="colhVVenueName" />
                                                                            <asp:BoundField DataField="address" HeaderText="Address" ReadOnly="True" FooterText="colhVAdress" />
                                                                            <asp:BoundField DataField="country" HeaderText="Country" ReadOnly="True" FooterText="colhVCountry" />
                                                                            <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" FooterText="colhVState" />
                                                                            <asp:BoundField DataField="city" HeaderText="City" ReadOnly="True" FooterText="colhVCity" />
                                                                            <asp:BoundField DataField="capacity" HeaderText="Capacity" ReadOnly="True" FooterText="colhVCapacity" />
                                                                            <asp:BoundField DataField="locked" HeaderText="Locked" ReadOnly="True" FooterText="colhVLocked" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="CostCenterMapping">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header" style="padding-bottom: 5px;padding-top: 5px;">
                                                        <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6">
                                                            <h2>
                                                                <asp:Label ID="lblCostCenterMapping" runat="server" Text="Cost Center Mapping"></asp:Label>
                                                            </h2>
                                                        </div>   
                                                        <div class="col-lg-4 col-md-4">
                                                                <asp:DropDownList ID="cboApplyCCenter" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblApplyCCenterMsg" runat="server" Text="Please select Cost Center."
                                                                    CssClass="d-none"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2">
                                                            <asp:Button ID="btnApplyCCenter" CssClass="btn btn-primary" runat="server" Text="Apply to ticked" />
                                                                <asp:Label ID="lblTickCCenterMsg" runat="server" Text="Please tick atleast one allocation."
                                                                    CssClass="d-none"></asp:Label>
                                                        </div>
                                                        </div>                                                     
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <input type="text" id="txtSearchCCenterMapping" name="txtSearchCCenterMapping" placeholder="Search Allocation"
                                                                            maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchCCenterMapping', '<%= gvCCenterMapping.ClientID %>', 'chkCCenterTicked', 'ChkgvSelectCCenterMapping');" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-15 padding-0">
                                                                <asp:CheckBox ID="chkCCenterTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                                    CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchCCenterMapping', '<% gvCCenterMapping.ClientID %>', 'chkCCenterTicked', 'ChkgvSelectCCenterMapping');" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvCCenterMapping" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="allocationid, allocationtranunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-CssClass="align-center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="ChkAllCCenterMapping" runat="server" Text=" " CssClass="chk-sm"
                                                                                        onclick="CheckAll(this, 'ChkgvSelectCCenterMapping');" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="ChkgvSelectCCenterMapping" runat="server" Text=" " CssClass="chk-sm"
                                                                                        onclick="ChkSelect(this, 'ChkgvSelectCCenterMapping', 'ChkAllCCenterMapping')"
                                                                                        Checked='<%# Eval("IsChecked")%>' />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle />
                                                                                <ItemStyle />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="AllocationName" HeaderText="Allocation" ReadOnly="True"
                                                                                FooterText="colhCCAllocationName" />
                                                                            <asp:BoundField DataField="allocationtranname" HeaderText="Department" ReadOnly="True"
                                                                                FooterText="colhCCAllocationTranName" />
                                                                            <asp:TemplateField HeaderText="Cost Center" FooterText="colhCCenter">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList ID="cboCCenter" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnCCSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnCCReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="OtherSettings">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblOOtherSettings" runat="server" Text="Other Settings"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <%--'S.SANDEEP |21-DEC-2021| -- START--%>
                                                            <%--<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">--%>
                                                            <div class="col-lg-6 col-md-6col-sm-12 col-xs-12">
                                                                <%--'S.SANDEEP |21-DEC-2021| -- END--%>
                                                                <asp:Label ID="lblTrainingNeedAllocation" Text="Department Training Needs is based on"
                                                                    runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrainingNeedAllocation" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkTrainingAddedDirectly" runat="server" Text="Training can be added directly into the Training Plan without the TNA process" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkRequestTraining" runat="server" Text="Allow to apply/request for a Training which is not in the Training Plan" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <%-- 'Pinkal (08-Apr-2021)-- Start
                                                             'Enhancement  -  Working on Employee Claim Form Report.--%>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsForeignTravelling" runat="server" Text="All training Requests with Foreign Travel Expenses must be approved by the Highest available level." />
                                                            </div>
                                                            <%-- 'Pinkal (08-Apr-2021)-- End --%>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsPreTrainingEvaluationSubmitted" runat="server" Text="Allow enrollment If Not Pre-Training Evaluation Submitted " />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsSkipTrainingRequisitionAndApproval" runat="server" Text="Skip Training Requisition & Approval" />
                                                            </div>
                                                        </div>
                                                         <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsPostTrainingEvaluationBeforeCompleteTraining" runat="server"
                                                                    Text="Allow To Submit Post Training Evaluation before Completing the Training" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsSkipTrainingEnrollmentProcess" runat="server" Text="Skip Training Enrollment Process" />
                                                            </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:CheckBox ID="chkIsSkipTrainingCompletionProcess" runat="server" Text="Skip Training Completion Process" />
                                                                    </div>
                                                        </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                         <div class="row clearfix">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:CheckBox ID="chkIsSkipPreTrainingEvaluationProcess" runat="server" Text="Skip Pre-Training Evaluation Process" />
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:CheckBox ID="chkIsSkipPostTrainingEvaluationProcess" runat="server" Text="Skip Post-Training Evaluation Process" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsSkipDaysAfterTrainingEvaluationProcess" runat="server" Text="Skip Days After Training Evaluation Process" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkIsSkipTrainingBudgetApprovalProcess" runat="server" Text="Skip Training Budget Approval Process" />
                                                            </div>
                                                        </div>
                                                        <%--'S.SANDEEP |21-DEC-2021| -- START--%>
                                                        <%--'ISSUE : GIVING PERIOD SELECTION ON TRAINING SETTING FOR FETCHING COMPETENCIES--%>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTrainingCompetenciesPeriod" Text="Fetch Competencies from Assessment Period"
                                                                    runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrainingCompetenciesPeriod" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--'S.SANDEEP |21-DEC-2021| -- END--%>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTrainingRemainingBalanceBasedOn" Text="Training Budget Remaining Balance Based On"
                                                                    runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrainingRemainingBalanceBasedOn" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%-- Hemant (09 Feb 2022) --Start--%>
                                                        <%-- OLD-561(NMB) : Option to complete training should have expiry days--%>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDaysFromLastDateOfTrainingToAllowCompleteTraining" runat="server"
                                                                    Text="Days From Last Date Of Training To Allow Complete Training" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtDaysFromLastDateOfTrainingToAllowCompleteTraining" runat="server"
                                                                        Type="Numeric" />
                                                                </div>
                                                            </div>
                                                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDaysForReminderEmailBeforeExpiryToCompleteTraining" runat="server"
                                                                    Text="Days For Reminder Email Before Expiry To Complete Training" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtDaysForReminderEmailBeforeExpiryToCompleteTraining" runat="server"
                                                                        Type="Numeric" />
                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                        <%--Hemant (09 Feb 2022) --End--%>
                                                        <%-- Hemant (09 Feb 2022) --Start--%>
                                                        <%-- OLD-549(NMB) : Give new screen for training approver allocation mapping--%>
                                                        <div class="row clearfix">                                                            
                                                            <div class="col-lg-6 col-md-6col-sm-12 col-xs-12">                                                                
                                                                <asp:Label ID="lblTrainingApproverAllocation" Text="Training Approver Allocation is based On"
                                                                    runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboTrainingApproverAllocation" runat="server" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--Hemant (09 Feb 2022) --End--%>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnOSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfCalendar" runat="server" Title="Aruti" />
                <cnf:Confirmation ID="cnfPriority" runat="server" Title="Aruti" />
                <cnf:Confirmation ID="cnfCategory" runat="server" Title="Aruti" />
                <cnf:Confirmation ID="cnfTrainingCost" runat="server" Title="Aruti" />
                <cnf:Confirmation ID="cnfLearningMethod" runat="server" Title="Aruti" />
                <cnf:Confirmation ID="cnfVenues" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>

        $(document).ready(function() {
		    RetriveTab();

            setEvents();
            
        });
         function RetriveTab() {

    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "TrainingCalendar";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
        }

        function setEvents() {
        
            $$('btnApplyCCenter').on('click', function(event) {
                if (IsValidApplyCCenter() == true) {
                    $('[id*=ChkgvSelectCCenterMapping]:checked').each(function() { $(this).parents('tr:first').find('select[id*=cboCCenter]').val($('select[id*=cboApplyCCenter]').val()).trigger('change.select2') });
                }
                event.preventDefault();
            });

            if ('<%= CStr(Session("TrainingBudgetAllocationID")) %>' != '15') {
                $$('lbltbCostCenterMapping').parents('li:first').remove();
                $('#CostCenterMapping').remove()
            }
            
        }
        
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:not(:has(>div)):containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:not(:has(>div)):containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
                $('.norecords').remove();
            }
            $('#' + txtID).focus();
        }

        function CheckAll(chkbox, chkgvSelect) {
            //var chkHeader = $(chkbox);
            var grid = $(chkbox).closest(".body");
            //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
            
        }

        function ChkSelect(chkbox, chkgvSelect, chkboxAll) {
            var grid = $(chkbox).closest(".body");
            var chkHeader = $("[id*=" + chkboxAll + "]", grid);
            if ($("[id*=" + chkgvSelect + "]", grid).length == $("[id*=" + chkgvSelect + "]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }

        }

        function IsValidApplyCCenter() {

            if (parseInt($('select[id*=cboApplyCCenter]').val()) <= 0) {
                showSuccessMessage($$('lblApplyCCenterMsg').text());
                return false;
            }
            else if ($('[id*=ChkgvSelectCCenterMapping]:checked').length <= 0) {
                showSuccessMessage($$('lblTickCCenterMsg').text());
                return false;
            }

            return true;
        }
        
    </script>

</asp:Content>
