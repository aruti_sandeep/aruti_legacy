﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_Training_Evalution.aspx.vb"
    Inherits="Training_Training_Evalution_wPg_Training_Evalution" Title="Training Evalution" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#EvalutionCategory" data-toggle="tab">
                                        <asp:Label ID="lblEvalutionCategory" runat="server" Text="Evalution Category"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#Questionnaire" data-toggle="tab">
                                        <asp:Label ID="lblQuestionnaireSetup" runat="server" Text="Questionnaire Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#SubQuestion" data-toggle="tab">
                                        <asp:Label ID="lblSubQuestionsSetup" runat="server" Text="Sub-Questions Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#GridColumnAnwser" data-toggle="tab">
                                        <asp:Label ID="lblGridColumnAnwser" runat="server" Text="Grid Column Anwser"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#OtherSettings" data-toggle="tab">
                                        <asp:Label ID="lblOtherSettings" runat="server" Text="Other Settings"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="EvalutionCategory">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblEvalutionCategoryHeader" runat="server" Text="Evalution Category"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblECName" runat="server" Text="Name" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtECName" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblECFeedbackMode" runat="server" Text="Feedback Mode" CssClass="form-label" />
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-l-0">
                                                                    <asp:RadioButton ID="rdbECFeedbackModePreTraining" runat="server" CssClass="d-block"
                                                                        Text="Pre-Training" GroupName="FeedbackMode" AutoPostBack="true" />
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-l-0">
                                                                    <asp:RadioButton ID="rdbECFeedbackModePostTraining" runat="server" CssClass="d-block"
                                                                        Text="Post-Training" GroupName="FeedbackMode" AutoPostBack="true" />
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-l-0">
                                                                    <asp:RadioButton ID="rdbECIsFeedbackModeDaysAfterTrainingAttend" runat="server" Text="Days after training attended."
                                                                        GroupName="FeedbackMode" AutoPostBack="true" />
                                                                    <asp:Panel ID="pnlECXXfeedbackdays" runat="server" CssClass="row clearfix" Enabled="false">
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <nut:NumericText ID="txtECXXfeedbackdays" runat="server" Type="Numeric" />
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkECFeedbackModeDaysAfterTrainingAttend" runat="server" Text="Notify Before Days For Feedback"
                                                                    AutoPostBack="true" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <asp:Panel ID="pnlECNotifyTrainingDays" runat="server" CssClass="col-lg-2 col-md-2 col-sm-6 col-xs-12"
                                                                Enabled="false">
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtECNotifyTrainingDays" runat="server" Type="Numeric" />
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnECSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnECReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvECList" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="categoryid,isnotifyforfeedback">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkECEdit" runat="server" ToolTip="Edit" OnClick="lnkECEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkECDelete" runat="server" ToolTip="Delete" OnClick="lnkECDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="categoryname" HeaderText="Category Name" ReadOnly="True"
                                                                                FooterText="colhcategoryname"></asp:BoundField>
                                                                            <asp:BoundField DataField="feedbacknodaysaftertrainingattended" HeaderText="Days after training attended"
                                                                                ReadOnly="True" FooterText="colhnoofdaysforfeedback"></asp:BoundField>
                                                                            <asp:BoundField DataField="feedbackmode" HeaderText="Feedback Mode" ReadOnly="True"
                                                                                FooterText="colhfeedbackmode"></asp:BoundField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Is Notify Before Feedback"
                                                                                FooterText="colhIsNotifyBeforeFeedback">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblIsNotifyFeedbackCheckTrue" Visible="false" runat="server" Text=""
                                                                                        CssClass="fas fa-check-circle text-success" />
                                                                                    <asp:Label ID="lblIsNotifyFeedbackCheckFalse" runat="server" Text="" Visible="false"
                                                                                        CssClass="fas fa-times-circle text-danger" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <%--4--%>
                                                                            <asp:BoundField DataField="notifybeforenodaysforfeedback" HeaderText="Notify Before Days For Feedback"
                                                                                ReadOnly="True" FooterText="colhnoofdaysforfeedback"></asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Questionnaire">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblQSHeader" runat="server" Text="Questionnaire Setup"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQSCategory" runat="server" Text="Category" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboQSCategory" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQSCode" runat="server" Text="Code" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtQSCode" runat="server" CssClass="form-control" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="TabQS">
                                                                    <li role="presentation" class="active"><a href="#QSQuestion" data-toggle="tab">
                                                                        <asp:Label ID="lblQSQuestion" runat="server" Text="Question"></asp:Label>
                                                                    </a></li>
                                                                    <li role="presentation"><a href="#QSDescription" data-toggle="tab">
                                                                        <asp:Label ID="lblQSDescription" runat="server" Text="Description"></asp:Label>
                                                                    </a></li>
                                                                </ul>
                                                                <div class="tab-content p-b-0">
                                                                    <div role="tabpanel" class="tab-pane fade in active" id="QSQuestion">
                                                                        <div class="row clearfix">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtQSQuestion" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div role="tabpanel" class="tab-pane fade in" id="QSDescription">
                                                                        <div class="row clearfix">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtQSDescription" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblQSAnswerType" runat="server" Text="Answer Type" CssClass="form-label" />
                                                                        <div class="form-group">
                                                                            <asp:DropDownList ID="cboQSAnswerType" runat="server" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <asp:CheckBox ID="chkQSAskForJustification" runat="server" Text="Ask For Justification"
                                                                            Visible="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <asp:CheckBox ID="chkQSForLineManagerFeedback" runat="server" Text="For Line Manager's Feedback"
                                                                            Visible="false" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Panel ID="pnlQuestionaireAnswer" runat="server" Visible="false">
                                                                    <div>
                                                                        <%--  <div style="float: left;">--%>
                                                                        <asp:Label ID="lblQuestionaireAnswer" runat="server" Text="List of the Questionaire Answer"></asp:Label>
                                                                        <%--</div>--%>
                                                                        <div style="float: right">
                                                                            <asp:LinkButton runat="server" ID="lnkQuestionaireAnswer" ToolTip="Add Questionaire Answer">
                                                                                <i class="fas fa-plus-circle" ></i>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table-responsive" style="max-height: 250px">
                                                                        <asp:GridView ID="dgvQuestionaireAnswer" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="trainingevaluationanswerunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkEditQuestionaireAnswer" runat="server" ToolTip="Remove" OnClick="lnkEditQuestionaireAnswer_Click">  
                                                                              <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkDeleteQuestionaireAnswer" runat="server" ToolTip="Remove"
                                                                                            OnClick="lnkDeleteQuestionaireAnswer_Click">  
                                                                              <i class="fas fa-trash text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="optionvalue" HeaderText="Option Value" ReadOnly="true"
                                                                                    FooterText="dgcolhEOptionValue" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Panel ID="pnlGridColumns" runat="server" Visible="false">
                                                                    <div>
                                                                        <asp:Label ID="lblGridColumns" runat="server" Text="List of the Grid's Columns"></asp:Label>
                                                                        <div style="float: right">
                                                                            <asp:LinkButton runat="server" ID="lnkGridColumns" ToolTip="Add Grid's Column">
                                                                                <i class="fas fa-plus-circle" ></i>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table-responsive" style="max-height: 250px">
                                                                        <asp:GridView ID="dgvGridColumns" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                            DataKeyNames="gridcolumnsid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkEditGridColumns" runat="server" ToolTip="Remove" OnClick="lnkEditGridColumns_Click">  
                                                                              <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkDeleteGridColumns" runat="server" ToolTip="Remove" OnClick="lnkDeleteGridColumns_Click">  
                                                                              <i class="fas fa-trash text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="gridcolumnsname" HeaderText="Columns Name" ReadOnly="true"
                                                                                    FooterText="dgcolhColumnsName" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnQSSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnQSReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvQuestionnaire" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="questionnaireId,isAskforJustification,isforlinemanager">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkQSEdit" runat="server" ToolTip="Edit" OnClick="lnkQSEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkQSDelete" runat="server" ToolTip="Delete" OnClick="lnkQSDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="categoryname" HeaderText="Category" ReadOnly="True" FooterText="colhcategoryname">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" FooterText="colhCode">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="question" HeaderText="Question" ReadOnly="True" FooterText="colhquestion">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="answertypeval" HeaderText="Answer Type" ReadOnly="True"
                                                                                FooterText="colhanswertypeval"></asp:BoundField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Ask For Justification"
                                                                                FooterText="colhAskForJustification">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblIsAskforJustificationCheckTrue" Visible="false" runat="server"
                                                                                        Text="" CssClass="fas fa-check-circle text-success" />
                                                                                    <asp:Label ID="lblIsAskforJustificationCheckFalse" runat="server" Text="" Visible="false"
                                                                                        CssClass="fas fa-times-circle text-danger " />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <%--4--%>
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                                                                FooterText="colhDescription"></asp:BoundField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="For Line Manager's Feedback"
                                                                                FooterText="colhForLineManagerFeedback">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblIsForLineManagerFeedbackTrue" Visible="false" runat="server" Text=""
                                                                                        CssClass="fas fa-check-circle text-success" />
                                                                                    <asp:Label ID="lblIsForLineManagerFeedbackFalse" runat="server" Text="" Visible="false"
                                                                                        CssClass="fas fa-times-circle text-danger " />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="SubQuestion">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblSubQuestionsHeader" runat="server" Text="Sub-Questions Setup"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblSQCategory" runat="server" Text="Category" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboSQCategory" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblSQQuestions" runat="server" Text="Question" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboSQQuestions" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="TabSubQS">
                                                                    <li role="presentation" class="active"><a href="#SubQSQuestion" data-toggle="tab">
                                                                        <asp:Label ID="lblSQSubQuestion" runat="server" Text="Sub-Question" CssClass="form-label" />
                                                                    </a></li>
                                                                    <li role="presentation"><a href="#SubQSDescription" data-toggle="tab">
                                                                        <asp:Label ID="lblSQDescription" runat="server" Text="Description" CssClass="form-label" />
                                                                    </a></li>
                                                                </ul>
                                                                <div class="tab-content p-b-0">
                                                                    <div role="tabpanel" class="tab-pane fade in active" id="SubQSQuestion">
                                                                        <div class="row clearfix">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtSQSubQuestion" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div role="tabpanel" class="tab-pane fade in" id="SubQSDescription">
                                                                        <div class="row clearfix">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <asp:TextBox ID="txtSQDescription" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Panel ID="pnlSubQuestionaireAnswer" runat="server" Visible="false">
                                                                    <div>
                                                                        <asp:Label ID="lblSubQuestionaireAnswer" runat="server" Text="List of the Sub Question Answer"></asp:Label>
                                                                        <div style="float: right">
                                                                            <asp:LinkButton runat="server" ID="lnkSubQuestionaireAnswer" ToolTip="Add Sub Question Answer">
                                                                                <i class="fas fa-plus-circle" ></i>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table-responsive" style="max-height: 250px">
                                                                        <asp:GridView ID="dgvSubQuestionaireAnswer" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="trainingevaluationanswerunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkEditSubQuestionaireAnswer" runat="server" ToolTip="Remove"
                                                                                            OnClick="lnkEditSubQuestionaireAnswer_Click">  
                                                                              <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkDeleteSubQuestionaireAnswer" runat="server" ToolTip="Remove"
                                                                                            OnClick="lnkDeleteSubQuestionaireAnswer_Click">  
                                                                              <i class="fas fa-trash text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="optionvalue" HeaderText="Option Value" ReadOnly="true"
                                                                                    FooterText="dgcolhEOptionValue" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnSQSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnSQReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvSubQuestion" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="subquestionid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkSQEdit" runat="server" ToolTip="Edit" OnClick="lnkSQEdit_Click">
                                                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkSQDelete" runat="server" ToolTip="Delete" OnClick="lnkSQDelete_Click">
                                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Category" HeaderText="Category" ReadOnly="True" FooterText="colhcategory">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Question" HeaderText="Question" ReadOnly="True" FooterText="colhQuestion">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="subquestion" HeaderText="Sub-Question" ReadOnly="True"
                                                                                FooterText="colhsubquestion"></asp:BoundField>
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                                                                FooterText="colhDescription"></asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="GridColumnAnwser">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblGridColumnAnwserHeader" runat="server" Text="Grid Column Anwser"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblGCCategory" runat="server" Text="Category" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboGCCategory" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblGCQuestions" runat="server" Text="Question" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboGCQuestions" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblGCColumnName" runat="server" Text="Column Name" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboGCColumnName" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Panel ID="pnlGridColumnAnswer" runat="server" Visible="false">
                                                                    <div>
                                                                        <asp:Label ID="lblGridColumnAnswer" runat="server" Text="List of the Grid Column Answer"></asp:Label>
                                                                        <div style="float: right">
                                                                            <asp:LinkButton runat="server" ID="lnkGridColumnAnswer" ToolTip="Add Sub Grid Column Answer">
                                                                                <i class="fas fa-plus-circle" ></i>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table-responsive" style="max-height: 250px">
                                                                        <asp:GridView ID="dgvGridColumnAnswer" runat="server" AutoGenerateColumns="False"
                                                                            AllowPaging="false" DataKeyNames="trainingevaluationanswerunkid" CssClass="table table-hover table-bordered">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkEditGridColumnAnswer" runat="server" ToolTip="Remove" OnClick="lnkEditGridColumnAnswer_Click">  
                                                                              <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkDeleteGridColumnAnswer" runat="server" ToolTip="Remove" OnClick="lnkDeleteGridColumnAnswer_Click">  
                                                                              <i class="fas fa-trash text-danger"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="optionvalue" HeaderText="Option Value" ReadOnly="true"
                                                                                    FooterText="dgcolhGCOptionValue" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnGCReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="OtherSettings">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblOOtherSettings" runat="server" Text="Other Settings"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPreTrainingFeedbackInstruction" runat="server" Text="Pre-Training Feedback Instruction"
                                                                    CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtPreTrainingFeedbackInstruction" runat="server" CssClass="form-control"
                                                                            TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPostTrainingFeedbackInstruction" runat="server" Text="Post-Training Feedback Instruction"
                                                                    CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtPostTrainingFeedbackInstruction" runat="server" CssClass="form-control"
                                                                            TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDaysAfterTrainingFeedbackInstruction" runat="server" Text="Days After Training Feedback Instruction"
                                                                    CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtDaysAfterTrainingFeedbackInstruction" runat="server" CssClass="form-control"
                                                                            TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDaysForEmployeeEvaluationReminderEmailAfterTrainingStartDate" runat="server"
                                                                    Text="Days For Employee Evaluation Reminder Email After Training Start Date" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtDaysForEmployeeEvaluationReminderEmailAfterTrainingStartDate" runat="server"
                                                                        Type="Numeric" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDaysForEmployeeEvaluationReminderEmailAfterTrainingEndDate" runat="server"
                                                                    Text="Days For Employee Evaluation Reminder Email After Training End Date" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtDaysForEmployeeEvaluationReminderEmailAfterTrainingEndDate" runat="server"
                                                                        Type="Numeric" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDaysForManagerEvaluationReminderEmailAfterSelfEvaluation" runat="server"
                                                                    Text="Days For Manager Evaluation Reminder Email After Self Evaluation" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <nut:NumericText ID="txtDaysForManagerEvaluationReminderEmailAfterSelfEvaluation" runat="server"
                                                                        Type="Numeric" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnOtherSave" CssClass="btn btn-primary" runat="server" Text="Save" />                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupQuestionAnswer" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hfQuestionAnswer" runat="server" CancelControlID="hfQuestionAnswer"
                    PopupControlID="pnlQuestionAnswer">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlQuestionAnswer" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblQuestionAnswer" Text="Add Question's Answers" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblOptionValue" runat="server" Text="Option Value" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtOptionValue" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnAddQuestionAnswer" runat="server" CssClass="btn btn-primary" Text="Add" />
                        <asp:Button ID="btnCloseQuestionAnswer" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                        <asp:HiddenField ID="hfQuestionAnswer" runat="Server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupSubQuestionAnswer" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hfSubQuestionAnswer" runat="server" CancelControlID="hfSubQuestionAnswer"
                    PopupControlID="pnlSubQuestionAnswer">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlSubQuestionAnswer" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblSubQuestionAnswer" Text="Add SubQuestion's Answers" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblSubOptionValue" runat="server" Text="Option Value" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtSubOptionValue" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnAddSubQuestionAnswer" runat="server" CssClass="btn btn-primary"
                            Text="Add" />
                        <asp:Button ID="btnCloseSubQuestionAnswer" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                        <asp:HiddenField ID="hfSubQuestionAnswer" runat="Server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupGridColumn" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hfGridColumn" runat="server" CancelControlID="hfGridColumn"
                    PopupControlID="pnlGridColumn">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGridColumn" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblGridColumn" Text="Add Grid's Column" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGridColumnsName" runat="server" Text="Columns Name" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtGridColumnsName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGridColumnAnswerType" runat="server" Text="Answer Type" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cboGridColumnAnswerType" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:CheckBox ID="chkForPreTrainingFeedback" runat="server" Text="For Pre-Training Feedback" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:CheckBox ID="chkForPostTrainingFeedback" runat="server" Text="For Post-Training Feedback" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:CheckBox ID="chkForManagerTrainingFeedback" runat="server" Text="For Manager Feedback" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnAddGridColumn" runat="server" CssClass="btn btn-primary" Text="Add" />
                        <asp:Button ID="btnCloseGridColumn" runat="server" CssClass="btn btn-default" Text="Close" />
                        <asp:HiddenField ID="hfGridColumn" runat="Server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupGridColumnsAnswer" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hfGridColumnsAnswer" runat="server" CancelControlID="hfGridColumnsAnswer"
                    PopupControlID="pnlGridColumnsAnswer">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGridColumnsAnswer" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblGridColumnsAnswer" Text="Add Grid's Column Answer" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGCOptionValue" runat="server" Text="Option Value" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtGCOptionValue" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnAddGridColumnAnswer" runat="server" CssClass="btn btn-primary"
                            Text="Add" />
                        <asp:Button ID="btnCloseGridColumnAnswer" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                        <asp:HiddenField ID="hfGridColumnsAnswer" runat="Server" />
                    </div>
                </asp:Panel>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
        $(document).ready(function() {
		    RetriveTab();
        });
         function RetriveTab() {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "EvalutionCategory";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {           
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }
    </script>

</asp:Content>
