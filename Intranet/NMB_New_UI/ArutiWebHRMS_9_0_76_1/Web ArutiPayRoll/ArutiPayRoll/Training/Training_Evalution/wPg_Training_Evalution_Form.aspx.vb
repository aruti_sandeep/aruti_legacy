﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

Partial Class Training_Training_Evalution_wPg_Training_Evalution_Form
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTrainingEvalutionForm"
    Private DisplayMessage As New CommonCodes
    Private mintTrainingRequestunkid As Integer = -1
    Private mintCourseMasterunkid As Integer = -1
    Private mintFeedBackModeId As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mdtQuestionnaire As DataTable
    Private objCONN As SqlConnection
    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Private mblnIsDaysAfterFeedbackSubmitted As Boolean = False
    'Hemant (20 Aug 2021) -- End
    'Hemant (01 Sep 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
    Private mblnIsPreTrainingFeedbackSubmitted As Boolean = False
    Private mblnIsPostTrainingFeedbackSubmitted As Boolean = False
    Private mblnIsDaysAfterLineManagerFeedbackSubmitted As Boolean = False
    'Hemant (01 Sep 2021) -- End
    'Hemant (10 Nov 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
    Private mdtCustomTabularGrid As New DataTable
    'Private mdicGridColumn As Dictionary(Of Integer, Integer)
    Private mdsGridColumn As New DataSet
    Private mdsGridColumnItems As New DataSet
    Private mdicBindGridColumn As New Dictionary(Of String, Integer)
    Private mdtBindGridColumn As New DataTable
    Private mintTrainingEvaluationTranUnkid As Integer = -1
    Private mstrDeleteAction As String = ""
    'Hemant (10 Nov 2023) -- End
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objRequestMaster As New clstraining_request_master
        Try

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If
            'Hemant (20 Aug 2021) -- End

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 6 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Me.ViewState.Add("IsDirect", True)
                    mintTrainingRequestunkid = CInt(arr(0))
                    ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(1))
                    mintEmployeeunkid = CInt(arr(2))
                    mintFeedBackModeId = CInt(arr(3))
                    ViewState("mintFeedBackModeId") = mintFeedBackModeId
                    If CBool(arr(5)) Then
                        HttpContext.Current.Session("UserId") = CInt(arr(4))
                    Else
                        HttpContext.Current.Session("Employeeunkid") = CInt(arr(4))
                    End If

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                    Dim objConfig As New clsConfigOptions
                    Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                    objConfig = Nothing

                    If mblnATLoginRequiredToApproveApplications = False Then
                        Dim objBasePage As New Basepage
                        objBasePage.GenerateAuthentication()
                        objBasePage = Nothing
                    End If


                    If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                        If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = IIf(CBool(arr(5)), CInt(Session("UserId")), CInt(Session("Employeeunkid")))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub
                        Else

                            If mblnATLoginRequiredToApproveApplications = False Then

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                    Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                    Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                    SetDateFormat()
                    If CBool(arr(5)) Then
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                    Else
                        Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""
                        Dim objEmp As New clsEmployee_Master
                        objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(Session("Employeeunkid"))
                        xUName = objEmp._Displayname : xPasswd = objEmp._Password
                        HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("UserId") = -1
                        HttpContext.Current.Session("Employeeunkid") = CInt(Session("Employeeunkid"))

                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        HttpContext.Current.Session("DisplayName") = xUName

                        HttpContext.Current.Session("UserName") = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
                        HttpContext.Current.Session("Password") = objEmp._Password
                        HttpContext.Current.Session("LeaveBalances") = 0
                        HttpContext.Current.Session("RoleID") = 0

                        HttpContext.Current.Session("Firstname") = objEmp._Firstname
                        HttpContext.Current.Session("Surname") = objEmp._Surname
                        HttpContext.Current.Session("MemberName") = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname
                        HttpContext.Current.Session("LangId") = 1
                        objEmp = Nothing
                    End If


                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                            End If  'If mblnATLoginRequiredToApproveApplications = False Then

                        End If 'If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                    Else
                        Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                        Session("ApproverUserId") = IIf(CBool(arr(5)), CInt(Session("UserId")), CInt(Session("Employeeunkid")))
                        DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                        Exit Sub

                    End If ' If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                    HttpContext.Current.Session("Login") = True

                End If 'If arr.Length = 6 Then

                CType(Me.Master.FindControl("pnlMenuWrapper"), Panel).Visible = False

            End If 'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count <= 0 Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Hemant (20 Aug 2021) -- End

            If Not IsPostBack Then
                GC.Collect()
                Call SetMessages()
                'Call Language._Object.SaveValue()

                If Not Session("TrainingRequestunkid") Is Nothing Or Not Session("TrainingRequestunkid") Is Nothing Then
                    ViewState("mintTrainingRequestunkid") = CInt(Session("TrainingRequestunkid"))
                    Session.Remove("TrainingRequestunkid")
                End If

                If Not Session("FeedBackModeId") Is Nothing Or Not Session("FeedBackModeId") Is Nothing Then
                    ViewState("mintFeedBackModeId") = CInt(Session("FeedBackModeId"))
                    Session.Remove("FeedBackModeId")
                End If

                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintFeedBackModeId = CInt(ViewState("mintFeedBackModeId"))

                objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
                mintEmployeeunkid = objRequestMaster._Employeeunkid
                mintCourseMasterunkid = objRequestMaster._Coursemasterunkid
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsDaysAfterFeedbackSubmitted = objRequestMaster._IsDaysAfterFeedbackSubmitted
                'Hemant (20 Aug 2021) -- End
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                mblnIsPreTrainingFeedbackSubmitted = objRequestMaster._IsPreTrainingFeedbackSubmitted
                mblnIsPostTrainingFeedbackSubmitted = objRequestMaster._IsPostTrainingFeedbackSubmitted
                mblnIsDaysAfterLineManagerFeedbackSubmitted = objRequestMaster._IsDaysAfterLineManagerFeedbackSubmitted
                'Hemant (01 Sep 2021) -- End

                If mintFeedBackModeId > 0 Then
                    Call FillEvalutionCategoryList()
                    FillQuestionsList()
                Else
                    btnSubmit.Visible = False
                    btnSave.Visible = False
                End If

                'Hemant (04 Sep 2021) -- Start
                'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
                Select Case mintFeedBackModeId
                    Case clseval_group_master.enFeedBack.PRETRAINING
                        txtInstruction.Text = Session("PreTrainingFeedbackInstruction").ToString
                    Case clseval_group_master.enFeedBack.POSTTRAINING
                        txtInstruction.Text = Session("PostTrainingFeedbackInstruction").ToString
                    Case clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                        txtInstruction.Text = Session("DaysAfterTrainingFeedbackInstruction").ToString
                End Select

                If txtInstruction.Text.Trim.Length > 0 Then pnlInstruction.Visible = True
                'Hemant (04 Sep 2021) -- End

            Else
                'mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintFeedBackModeId = CInt(ViewState("mintFeedBackModeId"))
                mintEmployeeunkid = CInt(ViewState("mintEmployeeunkid"))
                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintCourseMasterunkid = CInt(ViewState("mintCourseMasterunkid"))
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsDaysAfterFeedbackSubmitted = CBool(ViewState("isdaysafterfeedback_submitted"))
                'Hemant (20 Aug 2021) -- End
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                mblnIsPreTrainingFeedbackSubmitted = CBool(ViewState("mblnIsPreTrainingFeedbackSubmitted"))
                mblnIsPostTrainingFeedbackSubmitted = CBool(ViewState("mblnIsPostTrainingFeedbackSubmitted"))
                mblnIsDaysAfterLineManagerFeedbackSubmitted = CBool(ViewState("mblnIsDaysAfterLineManagerFeedbackSubmitted"))
                'Hemant (01 Sep 2021) -- End
                'Hemant (10 Nov 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                If ViewState("mdtCustomTabularGrid") IsNot Nothing Then
                    mdtCustomTabularGrid = CType(ViewState("mdtCustomTabularGrid"), DataTable)
                End If

                'If ViewState("mdicGridColumn") IsNot Nothing Then
                '    mdicGridColumn = CType(ViewState("mdicGridColumn"), Dictionary(Of Integer, Integer))
                'End If

                If ViewState("mdsGridColumn") IsNot Nothing Then
                    mdsGridColumn = CType(ViewState("mdsGridColumn"), DataSet)
                End If

                If ViewState("mdsGridColumnItems") IsNot Nothing Then
                    mdsGridColumnItems = CType(ViewState("mdsGridColumnItems"), DataSet)
                End If

                If ViewState("mdicBindGridColumn") IsNot Nothing Then
                    mdicBindGridColumn = CType(ViewState("mdicBindGridColumn"), Dictionary(Of String, Integer))
                End If

                If ViewState("mdtBindGridColumn") IsNot Nothing Then
                    mdtBindGridColumn = CType(ViewState("mdtBindGridColumn"), DataTable)
                End If

                mintTrainingEvaluationTranUnkid = CInt(ViewState("mintTrainingEvaluationTranUnkid"))
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                'Hemant (10 Nov 2023) -- End  
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            Me.ViewState("mintFeedBackModeId") = mintFeedBackModeId
            Me.ViewState("mintEmployeeunkid") = mintEmployeeunkid
            Me.ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            Me.ViewState("mintCourseMasterunkid") = mintCourseMasterunkid
            Me.ViewState("isdaysafterfeedback_submitted") = mblnIsDaysAfterFeedbackSubmitted
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            Me.ViewState("mblnIsPreTrainingFeedbackSubmitted") = mblnIsPreTrainingFeedbackSubmitted
            Me.ViewState("mblnIsPostTrainingFeedbackSubmitted") = mblnIsPostTrainingFeedbackSubmitted
            Me.ViewState("mblnIsDaysAfterLineManagerFeedbackSubmitted") = mblnIsDaysAfterLineManagerFeedbackSubmitted
            'Hemant (01 Sep 2021) -- End
            'Hemant (10 Nov 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
            ViewState("mdtCustomTabularGrid") = mdtCustomTabularGrid
            'ViewState("mdicGridColumn") = mdicGridColumn
            ViewState("mdsGridColumn") = mdsGridColumn
            ViewState("mdsGridColumnItems") = mdsGridColumnItems
            ViewState("mdicBindGridColumn") = mdicBindGridColumn
            ViewState("mdtBindGridColumn") = mdtBindGridColumn
            ViewState("mintTrainingEvaluationTranUnkid") = mintTrainingEvaluationTranUnkid
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
            'Hemant (10 Nov 2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region " Private Method(s)"

    Private Sub FillEvalutionCategoryList()
        Dim objeval_group_master As New clseval_group_master
        'Hemant (10 Nov 2023) -- Start
        'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
        Dim objQuestion As New clseval_question_master
        'Hemant (10 Nov 2023) -- End
        Dim dsList As New DataSet
        Try
            dsList = objeval_group_master.GetList("List", , mintFeedBackModeId)
            'Hemant (10 Nov 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
            If mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                Dim dtTemp As DataTable
                dtTemp = objeval_group_master.GetList("List", , clseval_group_master.enFeedBack.PRETRAINING).Tables(0)
                'Hemant (08 Dec 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                For Each drRow As DataRow In dtTemp.Rows
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        drRow.Item("categoryname") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Self Evaluation on") & " " & drRow.Item("categoryname").ToString
                    ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        drRow.Item("categoryname") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Manager Evaluation on") & " " & drRow.Item("categoryname").ToString
                    End If
                Next
                'Hemant (08 Dec 2023) -- End
                dsList.Tables(0).Merge(dtTemp)
            End If
            'Hemant (10 Nov 2023) -- End

            dgvCategory.DataSource = dsList.Tables(0)
            dgvCategory.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objeval_group_master = Nothing
            'Hemant (10 Nov 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
            objQuestion = Nothing
            'Hemant (10 Nov 2023) -- End
        End Try
    End Sub

    Private Function SetValue(ByVal objTrainingEvaluationTran As clstraining_evaluation_tran) As Boolean
        Try
            objTrainingEvaluationTran._Employeeunkid = mintEmployeeunkid
            objTrainingEvaluationTran._TrainingRequestunkid = mintTrainingRequestunkid
            objTrainingEvaluationTran._CourseMasterunkId = mintCourseMasterunkid
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingEvaluationTran._Userunkid = CInt(Session("UserId"))
                objTrainingEvaluationTran._LoginEmployeeunkid = -1
            Else
                objTrainingEvaluationTran._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objTrainingEvaluationTran._Userunkid = -1
            End If
            objTrainingEvaluationTran._Isvoid = False
            objTrainingEvaluationTran._Voiddatetime = Nothing
            objTrainingEvaluationTran._Voidreason = ""
            objTrainingEvaluationTran._Voiduserunkid = -1
            objTrainingEvaluationTran._AuditUserId = CInt(Session("UserId"))
            objTrainingEvaluationTran._ClientIP = CStr(Session("IP_ADD"))
            objTrainingEvaluationTran._FormName = mstrModuleName
            objTrainingEvaluationTran._IsWeb = True
            objTrainingEvaluationTran._HostName = CStr(Session("HOST_NAME"))

            mdtQuestionnaire = objTrainingEvaluationTran._DataTable.Copy

            For Each dgvCategoryItem As DataListItem In dgvCategory.Items
                Dim hfcategoryunkid As HiddenField = CType(dgvCategoryItem.FindControl("hfcategoryunkid"), HiddenField)
                Dim categoryid As Integer = CInt(hfcategoryunkid.Value)

                Dim dgvQuestion As DataList = CType(dgvCategoryItem.FindControl("dgvQuestion"), DataList)
                For Each dgvQuestionItem As DataListItem In dgvQuestion.Items
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    Dim dsList As New DataSet

                    Dim hfquestionnaireId As HiddenField = CType(dgvQuestionItem.FindControl("hfquestionnaireId"), HiddenField)
                    Dim questionnaireid As Integer = CInt(hfquestionnaireId.Value)

                    Dim hfAnswerType As HiddenField = CType(dgvQuestionItem.FindControl("hfAnswerType"), HiddenField)
                    Dim hfCategoryId As HiddenField = CType(dgvQuestionItem.FindControl("hfCategoryId"), HiddenField)
                    Dim hfAskForJustification As HiddenField = CType(dgvQuestionItem.FindControl("hfAskForJustification"), HiddenField)
                    'Hemant (20 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                    Dim hfForLineManagerFeedback As HiddenField = CType(dgvQuestionItem.FindControl("hfForLineManagerFeedback"), HiddenField)
                    'Hemant (20 Aug 2021) -- End

                    dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))
                    If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                        Dim dtquestion As DataRow
                        dtquestion = mdtQuestionnaire.NewRow

                        dtquestion.Item("categoryid") = hfCategoryId.Value
                        dtquestion.Item("questionnaireunkid") = questionnaireid
                        dtquestion.Item("feedbackmodeid") = mintFeedBackModeId
                        dtquestion.Item("answertypeid") = hfAnswerType.Value
                        dtquestion.Item("isaskforjustification") = hfAskForJustification.Value
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        dtquestion.Item("isforlinemanager") = hfForLineManagerFeedback.Value
                        'Hemant (20 Aug 2021) -- End

                        Dim strAnswer As String = String.Empty
                        Select Case CInt(hfAnswerType.Value)
                            Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                Dim txtItemNameFreeText As TextBox = CType(dgvQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                strAnswer = txtItemNameFreeText.Text
                            Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                strAnswer = txtItemNameNUM.Text.ToString
                            Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                Dim dtpItemName As Controls_DateCtrl = CType(dgvQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                strAnswer = dtpItemName.GetDate.Date.ToString("yyyyMMdd")
                            Case CInt(clseval_question_master.enAnswerType.RATING)
                                Dim cboItemNameRating As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                strAnswer = cboItemNameRating.SelectedValue
                            Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                Dim cboItemNameSelection As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                strAnswer = cboItemNameSelection.SelectedValue
                                'Hemant (10 Nov 2023) -- Start
                                'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                            Case CInt(clseval_question_master.enAnswerType.GRID)
                                Dim dgvItemNameGrid As GridView = CType(dgvQuestionItem.FindControl("dgvItemNameGrid"), GridView)
                                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                                gRow = dgvItemNameGrid.Rows.Cast(Of GridViewRow)()

                                If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                                    For Each dr As GridViewRow In gRow
                                        If CType(dr.FindControl("txtParticulars"), TextBox).Text.Trim.Length <= 0 Then Continue For
                                        For Each drKey As DataRow In mdtBindGridColumn.Select("questionnaireId = " & questionnaireid & "")
                                            dtquestion = mdtQuestionnaire.NewRow
                                            dtquestion.Item("categoryid") = hfCategoryId.Value
                                            dtquestion.Item("questionnaireunkid") = questionnaireid
                                            dtquestion.Item("feedbackmodeid") = mintFeedBackModeId
                                            dtquestion.Item("answertypeid") = hfAnswerType.Value
                                            dtquestion.Item("isaskforjustification") = hfAskForJustification.Value
                                            dtquestion.Item("isforlinemanager") = hfForLineManagerFeedback.Value
                                            If drKey.Item("controlname").ToString.StartsWith("cboColumn") = True Then
                                                dtquestion.Item("subquestionid") = -1
                                                dtquestion.Item("answer") = CType(dr.FindControl(drKey.Item("controlname").ToString), DropDownList).SelectedValue
                                                'Hemant (19 Jan 2024) -- Start
                                                'ENHANCEMENT(TRA): A1X-1638 - Evaluation form grid enhancement to include general employee and supervisor comments 
                                                If CBool(hfAskForJustification.Value) = True Then
                                                    Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                                                    dtquestion.Item("justification_answer") = txtJustify.Text
                                                Else
                                                    'Hemant (19 Jan 2024) -- End
                                                dtquestion.Item("justification_answer") = ""
                                                End If 'Hemant (19 Jan 2024)
                                                dtquestion.Item("gridcolumnsid") = drKey.Item("gridcolumnsid").ToString
                                                dtquestion.Item("particulars") = CType(dr.FindControl("txtParticulars"), TextBox).Text
                                                dtquestion.Item("AUD") = dgvItemNameGrid.DataKeys(dr.RowIndex)("AUD").ToString
                                                mdtQuestionnaire.Rows.Add(dtquestion)
                                            End If
                                        Next


                                    Next
                                End If
                                'Hemant (10 Nov 2023) -- End
                            Case Else
                        End Select


                        'Hemant (10 Nov 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                        If CInt(hfAnswerType.Value) <> CInt(clseval_question_master.enAnswerType.GRID) Then
                            dtquestion.Item("gridcolumnsid") = "0"
                            dtquestion.Item("particulars") = ""
                            'Hemant (10 Nov 2023) -- End
                            dtquestion.Item("subquestionid") = -1
                            dtquestion.Item("answer") = strAnswer
                            If CBool(hfAskForJustification.Value) = True Then
                                Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                                dtquestion.Item("justification_answer") = txtJustify.Text
                            Else
                                dtquestion.Item("justification_answer") = ""
                            End If

                            mdtQuestionnaire.Rows.Add(dtquestion)
                        End If 'Hemant (10 Nov 2023)

                    Else
                        Dim dgvSubQuestion As DataList = CType(dgvQuestionItem.FindControl("dgvSubQuestion"), DataList)
                        For Each dgvSubQuestionItem As DataListItem In dgvSubQuestion.Items
                            Dim dtquestion As DataRow
                            dtquestion = mdtQuestionnaire.NewRow

                            dtquestion.Item("categoryid") = hfCategoryId.Value
                            dtquestion.Item("questionnaireunkid") = questionnaireid
                            dtquestion.Item("feedbackmodeid") = mintFeedBackModeId
                            dtquestion.Item("answertypeid") = hfAnswerType.Value
                            dtquestion.Item("isaskforjustification") = hfAskForJustification.Value
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            dtquestion.Item("isforlinemanager") = hfForLineManagerFeedback.Value
                            'Hemant (20 Aug 2021) -- End
                            Dim hfSubQuestionId As HiddenField = CType(dgvSubQuestionItem.FindControl("hfSubQuestionId"), HiddenField)

                            Dim strAnswer As String = String.Empty
                            Select Case CInt(hfAnswerType.Value)
                                Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                    Dim txtItemNameFreeText As TextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                    strAnswer = txtItemNameFreeText.Text
                                Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                    Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                    strAnswer = txtItemNameNUM.Text.ToString
                                Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                    Dim dtpItemName As Controls_DateCtrl = CType(dgvSubQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                    strAnswer = dtpItemName.GetDate.Date.ToString("yyyyMMdd")
                                Case CInt(clseval_question_master.enAnswerType.RATING)
                                    Dim cboItemNameRating As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                    strAnswer = cboItemNameRating.SelectedValue
                                Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                    Dim cboItemNameSelection As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                    strAnswer = cboItemNameSelection.SelectedValue
                                Case Else
                            End Select

                            dtquestion.Item("subquestionid") = CInt(hfSubQuestionId.Value)
                            dtquestion.Item("answer") = strAnswer
                            If CBool(hfAskForJustification.Value) = True Then
                                Dim txtJustify As TextBox = CType(dgvSubQuestionItem.FindControl("txtJustify"), TextBox)
                                dtquestion.Item("justification_answer") = txtJustify.Text
                            Else
                                dtquestion.Item("justification_answer") = ""
                            End If
                            'Hemant (10 Nov 2023) -- Start
                            'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                            dtquestion.Item("gridcolumnsid") = "0"
                            dtquestion.Item("particulars") = ""
                            'Hemant (10 Nov 2023) -- End

                            mdtQuestionnaire.Rows.Add(dtquestion)
                        Next
                    End If
                    objEval_subquestion_master = Nothing
                Next

            Next

            Dim dt As New DataTable
            Dim xRow() As DataRow = Nothing
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                xRow = mdtQuestionnaire.Select("isforlinemanager=1")
            Else
                xRow = mdtQuestionnaire.Select("isforlinemanager=0")
            End If
            If xRow.Length > 0 Then
                dt = xRow.CopyToDataTable()
            End If
            objTrainingEvaluationTran._DataTable = dt

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillQuestionsList()
        Dim objEvaluation As New clstraining_evaluation_tran
        Dim objQuestions As New clseval_question_master
        Dim dsQuestions As DataSet
        Try
            dsQuestions = objEvaluation.GetList("List", mintEmployeeunkid, mintTrainingRequestunkid, mintFeedBackModeId)
            For Each dgvCategoryItem As DataListItem In dgvCategory.Items
                Dim hfcategoryunkid As HiddenField = CType(dgvCategoryItem.FindControl("hfcategoryunkid"), HiddenField)
                Dim categoryid As Integer = CInt(hfcategoryunkid.Value)

                Dim dgvQuestion As DataList = CType(dgvCategoryItem.FindControl("dgvQuestion"), DataList)

                For Each dgvQuestionItem As DataListItem In dgvQuestion.Items
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    Dim dsList As New DataSet

                    Dim hfquestionnaireId As HiddenField = CType(dgvQuestionItem.FindControl("hfquestionnaireId"), HiddenField)
                    Dim questionnaireid As Integer = CInt(hfquestionnaireId.Value)

                    'Hemant (10 Nov 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                    Dim hfForLineManagerFeedback As HiddenField = CType(dgvQuestionItem.FindControl("hfForLineManagerFeedback"), HiddenField)
                    'Hemant (10 Nov 2023) -- End
                    'Hemant ((08 Dec 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                    Dim hfTypeId As HiddenField = CType(dgvQuestionItem.FindControl("hfTypeId"), HiddenField)
                    'Hemant ((08 Dec 2023) -- End

                    Dim hfAnswerType As HiddenField = CType(dgvQuestionItem.FindControl("hfAnswerType"), HiddenField)
                    Dim hfCategoryId As HiddenField = CType(dgvQuestionItem.FindControl("hfCategoryId"), HiddenField)
                    Dim hfAskForJustification As HiddenField = CType(dgvQuestionItem.FindControl("hfAskForJustification"), HiddenField)

                    dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))
                    If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                        'Hemant (10 Nov 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                        Dim blnGridGenerate As Boolean = False
                        'Hemant (10 Nov 2023) -- End
                        'Hemant ((08 Dec 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                        If CInt(hfTypeId.Value) = 1 Then
                            Dim intCount As Integer = dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid <= 0 ").Length
                            If intCount <= 0 Then
                                Dim drTemp() As DataRow = (New clstraining_evaluation_tran).GetList("List", mintEmployeeunkid, mintTrainingRequestunkid, 1).Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " ")
                                If drTemp.Length > 0 Then
                                    drTemp(0).Item("answer") = ""
                                    drTemp(0).Item("particulars") = ""
                                    dsQuestions.Tables(0).ImportRow(drTemp(0))
                                End If
                            End If
                        End If
                        'Hemant ((08 Dec 2023) -- End
                        For Each drRow As DataRow In dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid <= 0 ")

                            Select Case CInt(hfAnswerType.Value)
                                Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                    Dim txtItemNameFreeText As TextBox = CType(dgvQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                    txtItemNameFreeText.Text = drRow.Item("answer").ToString
                                Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                    Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                    txtItemNameNUM.Text = drRow.Item("answer").ToString
                                Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                    Dim dtpItemName As Controls_DateCtrl = CType(dgvQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                    Dim dt As DateTime = DateTime.ParseExact(drRow.Item("answer").ToString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture)
                                    dtpItemName.SetDate = dt.Date
                                Case CInt(clseval_question_master.enAnswerType.RATING)
                                    Dim cboItemNameRating As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                    cboItemNameRating.SelectedValue = drRow.Item("answer").ToString()
                                Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                    Dim cboItemNameSelection As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                    cboItemNameSelection.SelectedValue = drRow.Item("answer").ToString()
                                    'Hemant (10 Nov 2023) -- Start
                                    'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                                Case CInt(clseval_question_master.enAnswerType.GRID)
                                    If blnGridGenerate = True Then Continue For
                                    Dim dtGrid As New DataTable
                                    Dim drGrid() As DataRow
                                    If CInt(hfTypeId.Value) = 1 Then
                                        drGrid = (New clstraining_evaluation_tran).GetList("List", mintEmployeeunkid, mintTrainingRequestunkid, 1).Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid <= 0  AND gridcolumnsid > 0  ")
                                    Else
                                        drGrid = dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid <= 0  AND gridcolumnsid > 0 AND isforlinemanager = " & hfForLineManagerFeedback.Value & " ")
                                    End If

                                    If drGrid.Length > 0 Then
                                        dtGrid = drGrid.CopyToDataTable()
                                    Else
                                        dtGrid = dsQuestions.Tables(0).Clone()
                                    End If
                                    
                                    If dtGrid IsNot Nothing AndAlso dtGrid.Rows.Count > 0 Then
                                        Dim dgvItemNameGrid As GridView = CType(dgvQuestionItem.FindControl("dgvItemNameGrid"), GridView)
                                        Dim dt As DataTable = mdtCustomTabularGrid.Clone()
                                        For Each drParticulars As DataRow In dtGrid.DefaultView.ToTable(True, "particulars").Rows
                                            Dim dr As DataRow = dt.NewRow
                                            dr("questionnaireid") = questionnaireid
                                            dr("trainingevaluationtranunkid") = -1
                                            dr("particulars") = drParticulars.Item("particulars").ToString
                                            For Each drcolumn As DataRow In dtGrid.Select("particulars = '" & drParticulars.Item("particulars").ToString & "'")
                                                dr("gridcolumnsid" & drcolumn.Item("gridcolumnsid").ToString) = drcolumn.Item("answer").ToString
                                                If dr("trainingevaluationtranunkids").ToString.Trim.Length > 0 Then
                                                    dr("trainingevaluationtranunkids") = dr("trainingevaluationtranunkids").ToString & "," & drcolumn.Item("trainingevaluationtranunkid").ToString
                                                Else
                                                    dr("trainingevaluationtranunkids") = drcolumn.Item("trainingevaluationtranunkid").ToString
                                                End If
                                            Next
                                            dt.Rows.Add(dr)
                                        Next

                                        ''For Each key As KeyValuePair(Of String, Integer) In mdicBindGridColumn
                                        ''    Dim dr As DataRow = dt.NewRow
                                        ''    dr("trainingevaluationtranunkid") = drGrid.Item("trainingevaluationtranunkid").ToString
                                        ''    dr("particulars") = drGrid.Item("particulars").ToString
                                        ''    dr("gridcolumnsid" & drGrid.Item("gridcolumnsid").ToString) = drGrid.Item("answer").ToString
                                        ''    dt.Rows.Add(dr)
                                        ''Next
                                        'For Each drGrid As DataRow In dtGrid.Rows
                                        '    Dim dr As DataRow = dt.NewRow
                                        '    dr("trainingevaluationtranunkid") = drGrid.Item("trainingevaluationtranunkid").ToString
                                        '    dr("particulars") = drGrid.Item("particulars").ToString
                                        '    dr("gridcolumnsid" & drGrid.Item("gridcolumnsid").ToString) = drGrid.Item("answer").ToString
                                        '    dt.Rows.Add(dr)
                                        'Next

                                        AddHandler dgvItemNameGrid.RowDataBound, AddressOf Me.dgvItemNameGrid_OnRowDataBound
                                        dgvItemNameGrid.DataSource = dt
                                        dgvItemNameGrid.DataBind()
                                    End If
                                    blnGridGenerate = True
                                    'Hemant (10 Nov 2023) -- End

                                    'Hemant (19 Jan 2024) -- Start
                                    'ENHANCEMENT(TRA): A1X-1638 - Evaluation form grid enhancement to include general employee and supervisor comments 
                                    If CBool(hfAskForJustification.Value) = True AndAlso dtGrid.Rows.Count > 0 Then
                                        Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                                        txtJustify.Text = dtGrid(0).Item("justification_answer").ToString()
                                    End If
                                    'Hemant (19 Jan 2024) -- End

                                    'Hemant ((08 Dec 2023) -- Start
                                    'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                                    If CInt(hfTypeId.Value) = 1 Then
                                        For Each xRow As DataRow In dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid <= 0 AND particulars = '' AND answer = '' ")
                                            dsQuestions.Tables(0).Rows.Remove(xRow)
                                        Next
                                    End If
                                    'Hemant ((08 Dec 2023) -- End

                                Case Else
                            End Select


                            If CBool(hfAskForJustification.Value) = True AndAlso CInt(hfAnswerType.Value) <> CInt(clseval_question_master.enAnswerType.GRID) Then
                                Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                                txtJustify.Text = drRow.Item("justification_answer").ToString()
                            End If
                            Exit For


                        Next
                       

                    Else
                        Dim dgvSubQuestion As DataList = CType(dgvQuestionItem.FindControl("dgvSubQuestion"), DataList)
                        For Each dgvSubQuestionItem As DataListItem In dgvSubQuestion.Items

                            Dim hfSubQuestionId As HiddenField = CType(dgvSubQuestionItem.FindControl("hfSubQuestionId"), HiddenField)

                            For Each drRow As DataRow In dsQuestions.Tables(0).Select("questionnaireid = " & CInt(hfquestionnaireId.Value) & " AND subquestionid = " & CInt(hfSubQuestionId.Value) & " ")
                                Dim strAnswer As String = String.Empty
                                Select Case CInt(hfAnswerType.Value)
                                    Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                        Dim txtItemNameFreeText As TextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                        txtItemNameFreeText.Text = drRow.Item("answer").ToString
                                    Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                        Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                        txtItemNameNUM.Text = drRow.Item("answer").ToString
                                    Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                        Dim dtpItemName As Controls_DateCtrl = CType(dgvSubQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                        Dim dt As DateTime = DateTime.ParseExact(drRow.Item("answer").ToString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture)
                                        dtpItemName.SetDate = dt.Date
                                    Case CInt(clseval_question_master.enAnswerType.RATING)
                                        Dim cboItemNameRating As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                        cboItemNameRating.SelectedValue = drRow.Item("answer").ToString()
                                    Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                        Dim cboItemNameSelection As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                        cboItemNameSelection.SelectedValue = drRow.Item("answer").ToString()
                                    Case Else
                                End Select

                                If CBool(hfAskForJustification.Value) = True Then
                                    Dim txtJustify As TextBox = CType(dgvSubQuestionItem.FindControl("txtJustify"), TextBox)
                                    txtJustify.Text = drRow.Item("justification_answer").ToString()
                                End If
                                Exit For
                            Next

                        Next
                    End If
                    objEval_subquestion_master = Nothing
                Next

            Next
            Dim drQuestions() As DataRow = objQuestions.GetList("List", True, , mintFeedBackModeId).Tables(0).Select("1=1")
            'Hemant (08 Dec 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
            Dim drGridQuestions() As DataRow
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                drGridQuestions = (New clseval_grid_columns_master).GetList("List", , , , True, , False).Tables(0).Select("1=1")
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                drGridQuestions = (New clseval_grid_columns_master).GetList("List", , , , , True, False).Tables(0).Select("1=1")
            Else
                drGridQuestions = (New clseval_grid_columns_master).GetList("List", , , , , , False).Tables(0).Select("1=2")
            End If
            'Hemant (08 Dec 2023) -- End

            If drQuestions.Length > 0 OrElse drGridQuestions.Length > 0 Then
                'Hemant (08 Dec 2023) -- [OrElse drGridQuestions.Length > 0]
                btnSubmit.Visible = True
                btnSave.Visible = True
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.PRETRAINING _
                   AndAlso mblnIsPreTrainingFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.POSTTRAINING _
                       AndAlso mblnIsPostTrainingFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING _
                    AndAlso mblnIsDaysAfterFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING _
                    AndAlso mblnIsDaysAfterLineManagerFeedbackSubmitted = True Then
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                End If
                'Hemant (20 Aug 2021) -- End
            Else
                btnSave.Visible = False
                btnSubmit.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objQuestions = Nothing
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            For Each dgvCategoryItem As DataListItem In dgvCategory.Items
                Dim dgvQuestion As DataList = CType(dgvCategoryItem.FindControl("dgvQuestion"), DataList)

                For Each dgvQuestionItem As DataListItem In dgvQuestion.Items
                    Dim objEval_subquestion_master As New clseval_subquestion_master
                    Dim dsList As New DataSet

                    Dim hfquestionnaireId As HiddenField = CType(dgvQuestionItem.FindControl("hfquestionnaireId"), HiddenField)
                    Dim hfAnswerType As HiddenField = CType(dgvQuestionItem.FindControl("hfAnswerType"), HiddenField)
                    Dim hfAskForJustification As HiddenField = CType(dgvQuestionItem.FindControl("hfAskForJustification"), HiddenField)
                    dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))
                    If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                        Select Case CInt(hfAnswerType.Value)
                            Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                Dim txtItemNameFreeText As TextBox = CType(dgvQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                If txtItemNameFreeText.Text.Trim.Length <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                If CInt(txtItemNameNUM.Text) < 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Negative Value is not allowed in Numeric Field."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                Dim dtpItemName As Controls_DateCtrl = CType(dgvQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                If dtpItemName.GetDate = Nothing Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.RATING)
                                Dim cboItemNameRating As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                If CInt(cboItemNameRating.SelectedValue) <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                            Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                Dim cboItemNameSelection As DropDownList = CType(dgvQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                If CInt(cboItemNameSelection.SelectedValue) <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                    Return False
                                End If
                                'Hemant (10 Nov 2023) -- Start
                                'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                            Case CInt(clseval_question_master.enAnswerType.GRID)
                                Dim dgvItemNameGrid As GridView = CType(dgvQuestionItem.FindControl("dgvItemNameGrid"), GridView)
                                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                                gRow = dgvItemNameGrid.Rows.Cast(Of GridViewRow)()

                                If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                                    For Each dr As GridViewRow In gRow
                                        If dr.RowIndex = 0 Then
                                            If CType(dr.FindControl("txtParticulars"), TextBox).Text.Trim.Length <= 0 Then
                                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                                Return False
                                            End If
                                        
                                        End If
                                        For Each drKey As DataRow In mdtBindGridColumn.Select("questionnaireId = " & CInt(hfquestionnaireId.Value) & "")
                                            If drKey.Item("controlname").ToString.StartsWith("cboColumn") = True Then
                                                If dgvItemNameGrid.Columns(getColumnID_Griview(dgvItemNameGrid, "dgcolh" & drKey.Item("controlname").ToString, False, True)).Visible = True Then
                                                    If CType(dr.FindControl("txtParticulars"), TextBox).Text.Trim.Length > 0 AndAlso CInt(CType(dr.FindControl(drKey.Item("controlname").ToString), DropDownList).SelectedValue) <= 0 Then
                                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                                        Return False
                                                    ElseIf CInt(CType(dr.FindControl(drKey.Item("controlname").ToString), DropDownList).SelectedValue) > 0 AndAlso CType(dr.FindControl("txtParticulars"), TextBox).Text.Trim.Length <= 0 Then
                                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                                        Return False

                                                    End If
                                                End If


                                            End If
                                        Next
                                        
                                    Next
                                End If
                                'Hemant (10 Nov 2023) -- End
                            Case Else
                        End Select


                        If CBool(hfAskForJustification.Value) = True Then
                            Dim txtJustify As TextBox = CType(dgvQuestionItem.FindControl("txtJustify"), TextBox)
                            If txtJustify.Text.Trim.Length <= 0 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, all questions justifications are mandatory. please attempt all questions justifications  to continue."), Me.Page)
                                Return False
                            End If
                        End If
                    Else
                        Dim dgvSubQuestion As DataList = CType(dgvQuestionItem.FindControl("dgvSubQuestion"), DataList)
                        For Each dgvSubQuestionItem As DataListItem In dgvSubQuestion.Items
                            Select Case CInt(hfAnswerType.Value)
                                Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                                    Dim txtItemNameFreeText As TextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameFreeText"), TextBox)
                                    If txtItemNameFreeText.Text.Trim.Length <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                                    Dim txtItemNameNUM As Controls_NumericTextBox = CType(dgvSubQuestionItem.FindControl("txtItemNameNUM"), Controls_NumericTextBox)
                                    If CInt(txtItemNameNUM.Text) < 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Negative Value is not allowed in Numeric Field."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                                    Dim dtpItemName As Controls_DateCtrl = CType(dgvSubQuestionItem.FindControl("dtpItemName"), Controls_DateCtrl)
                                    If dtpItemName.GetDate = Nothing Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.RATING)
                                    Dim cboItemNameRating As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameRating"), DropDownList)
                                    If CInt(cboItemNameRating.SelectedValue) <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case CInt(clseval_question_master.enAnswerType.SELECTION)
                                    Dim cboItemNameSelection As DropDownList = CType(dgvSubQuestionItem.FindControl("cboItemNameSelection"), DropDownList)
                                    If CInt(cboItemNameSelection.SelectedValue) <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue."), Me.Page)
                                        Return False
                                    End If
                                Case Else
                            End Select

                            If CBool(hfAskForJustification.Value) = True Then
                                Dim txtJustify As TextBox = CType(dgvSubQuestionItem.FindControl("txtJustify"), TextBox)
                                If txtJustify.Text.Trim.Length <= 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, all questions justifications are mandatory. please attempt all questions justifications  to continue."), Me.Page)
                                    Return False
                                End If
                            End If
                        Next
                    End If
                Next
            Next

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click, _
                                                                                               btnSave.Click
        Dim objTrainingEvaluationTran As New clstraining_evaluation_tran
        Dim objTrainingRequest As New clstraining_request_master
        'Hemant (20 Aug 2021) -- Start
        'ISSUE/ENHANCEMENT : OLD-447 - Evaluation Forms Enhancement - Line Manager Feedback.
        Dim blnIsSubmit As Boolean = False
        Dim btn As Button = CType(sender, Button)
        'Hemant (20 Aug 2021) -- End
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            If btn.ID = btnSubmit.ID Then
                objTrainingRequest._TrainingRequestunkid = mintTrainingRequestunkid
                Select Case mintFeedBackModeId
                    Case clseval_group_master.enFeedBack.PRETRAINING
                        objTrainingRequest._IsPreTrainingFeedbackSubmitted = True
                        objTrainingRequest._PreTrainingFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                        blnIsSubmit = True
                        'Hemant (22 Dec 2023) -- Start
                        'ENHANCEMENT(TRA): A1X-1623 - Evaluation form settings enhancement to skip training enrollment process
                        If CBool(Session("SkipTrainingEnrollmentProcess")) = True Then
                            objTrainingRequest._EnrollAmount = objTrainingRequest._ApprovedAmountEmp
                            objTrainingRequest._IsEnrollConfirm = True
                        If CBool(Session("SkipTrainingCompletionProcess")) = True Then
                            objTrainingRequest._IsCompletedSubmitApproval = True
                            objTrainingRequest._CompletedStatusunkid = enTrainingRequestStatus.APPROVED
                            objTrainingRequest._CompletedUserunkid = 1
                            objTrainingRequest._CompletedApprovaldate = objTrainingRequest._End_Date
                                'Hemant (05 Jul 2024) -- Start
                                'ENHANCEMENT(NMB): A1X - 2375 : Hide Training Evaluation screens
                                If CBool(Session("SkipPostTrainingEvaluationProcess")) = True Then
                                    objTrainingRequest._IsPostTrainingFeedbackSubmitted = True
                                    objTrainingRequest._PostTrainingFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                                End If
                                If CBool(Session("SkipDaysAfterTrainingEvaluationProcess")) = True Then
                                    objTrainingRequest._IsDaysAfterFeedbackSubmitted = True
                                    objTrainingRequest._DaysAfterFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                                    objTrainingRequest._IsDaysAfterLineManagerFeedbackSubmitted = True
                                    objTrainingRequest._DaysAfterLineManagerFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                                End If
                                'Hemant (05 Jul 2024) -- End
                            End If
                        End If
                        'Hemant (22 Dec 2023) -- End
                    Case clseval_group_master.enFeedBack.POSTTRAINING
                        objTrainingRequest._IsPostTrainingFeedbackSubmitted = True
                        objTrainingRequest._PostTrainingFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                        blnIsSubmit = True
                    Case clseval_group_master.enFeedBack.DAYSAFTERTRAINING
                        If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            Dim objReportTo As New clsReportingToEmployee
                            objReportTo._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = mintEmployeeunkid

                            Dim dt As DataTable = objReportTo._RDataTable
                            Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                            If DefaultReportList.Count > 0 Then
                                objTrainingRequest._IsDaysAfterFeedbackSubmitted = True
                                objTrainingRequest._DaysAfterFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                                objTrainingRequest._DaysAfterFeedbackSubmittedRemark = ""
                                blnIsSubmit = True
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, your line manager info is not present. Please contact support to proceed."), Me.Page)
                                btnSubmit.Enabled = False
                            End If
                            objReportTo = Nothing
                        Else
                            objTrainingRequest._IsDaysAfterLineManagerFeedbackSubmitted = True
                            objTrainingRequest._DaysAfterLineManagerFeedbackSubmittedDate = ConfigParameter._Object._CurrentDateAndTime
                            blnIsSubmit = True
                        End If

                End Select
            End If
            'Hemant (20 Aug 2021) -- End

            If SetValue(objTrainingEvaluationTran) = False Then
                Exit Sub
            End If

            If objTrainingEvaluationTran.SaveEvaluationTran(Nothing, objTrainingRequest) = False Then
                DisplayMessage.DisplayMessage(objTrainingEvaluationTran._Message, Me)
                Exit Sub
            Else
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                If Request.QueryString.Count <= 0 Then
                    'Hemant (13 Aug 2021) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Training Evaluation Saved Successfully."), Me, "../Training_Request/wPg_TrainingRequestFormList.aspx")
                    'Hemant (13 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Training Evaluation Saved Successfully."), Me, Convert.ToString(Session("rootpath")) & "Index.aspx")
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                If blnIsSubmit = True AndAlso mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING AndAlso (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    objTrainingEvaluationTran.Send_Notification_ReportingTo(mintEmployeeunkid, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                            CInt(Session("CompanyUnkId")), mintCourseMasterunkid, mintTrainingRequestunkid, CStr(Session("ArutiSelfServiceURL")), _
                                                                            enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                End If
                'Hemant (20 Aug 2021) -- End

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()
                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If
                    'Pinkal (23-Feb-2024) -- End
                End If
                'Hemant (13 Aug 2021) -- End


            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingEvaluationTran = Nothing
            objTrainingRequest = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Hemant (13 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            If Request.QueryString.Count <= 0 Then
                'Hemant (13 Aug 2021) -- End
                If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                    Response.Redirect(Session("ReturnURL").ToString, False)
                    Session("ReturnURL") = Nothing
                Else
                    Response.Redirect("~\UserHome.aspx", False)
                End If
                'Hemant (13 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-427 - Do not display any training cost amounts (from departmental plans) on the Training Request screen for employee. Show zeroes and allow user to save without forcing them to add amount..
            Else
                'Pinkal (23-Feb-2024) -- Start
                '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If
                'Pinkal (23-Feb-2024) -- End

                Response.Redirect("~/Index.aspx", False)
            End If
            'Hemant (13 Aug 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim btnAddNew As Button = CType(sender, Button)
            Dim dgvQuestion As DataListItem = CType(btnAddNew.NamingContainer, DataListItem)

            Dim dt As DataTable = mdtCustomTabularGrid.Clone()
            Dim dgvItemNameGrid As GridView = CType(dgvQuestion.FindControl("dgvItemNameGrid"), GridView)

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvItemNameGrid.Rows.Cast(Of GridViewRow)()

            Dim intQuestionId As Integer = -1
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For Each dr As GridViewRow In gRow
                    If intQuestionId = -1 Then intQuestionId = CInt(dgvItemNameGrid.DataKeys(dr.RowIndex)("questionnaireid").ToString)
                    Dim drRow As DataRow = dt.NewRow
                    drRow("trainingevaluationtranunkid") = -1
                    drRow("questionnaireid") = dgvItemNameGrid.DataKeys(dr.RowIndex)("questionnaireid").ToString
                    drRow("particulars") = CStr(CType(dr.FindControl("txtParticulars"), TextBox).Text)
                    For Each drKey As DataRow In mdtBindGridColumn.Select("questionnaireid = " & CInt(dgvItemNameGrid.DataKeys(dr.RowIndex)("questionnaireid").ToString) & "")
                        drRow("gridcolumnsid" & drKey.Item("gridcolumnsid").ToString) = CStr(CType(dr.FindControl(drKey.Item("controlname").ToString), DropDownList).SelectedValue)
                    Next
                    drRow("trainingevaluationtranunkids") = dgvItemNameGrid.DataKeys(dr.RowIndex)("trainingevaluationtranunkids").ToString
                    drRow("AUD") = dgvItemNameGrid.DataKeys(dr.RowIndex)("AUD").ToString
                    dt.Rows.Add(drRow)
                Next
            End If


            Dim drblank As DataRow = dt.NewRow
            drblank.Item("questionnaireid") = intQuestionId
            dt.Rows.Add(drblank)
            mdtCustomTabularGrid = dt


            AddHandler dgvItemNameGrid.RowDataBound, AddressOf Me.dgvItemNameGrid_OnRowDataBound
            dgvItemNameGrid.DataSource = mdtCustomTabularGrid
            dgvItemNameGrid.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkDeleteSelectedParticulars_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            Dim dgvItemNameGrid As GridView = CType(row.NamingContainer, GridView)
            Dim dt As DataTable = mdtCustomTabularGrid.Clone()
            Dim strParticulars As String = CType(row.FindControl("txtParticulars"), TextBox).Text
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvItemNameGrid.Rows.Cast(Of GridViewRow)()
            Dim intQuestionnaireId As Integer = -1
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For Each dr As GridViewRow In gRow
                    If intQuestionnaireId = -1 Then intQuestionnaireId = CInt(dgvItemNameGrid.DataKeys(dr.RowIndex)("questionnaireid"))
                    Dim drRow As DataRow = dt.NewRow
                    drRow("questionnaireid") = intQuestionnaireId
                    drRow("trainingevaluationtranunkid") = -1
                    drRow("particulars") = CStr(CType(dr.FindControl("txtParticulars"), TextBox).Text)
                    For Each drKey As DataRow In mdtBindGridColumn.Select("questionnaireid = " & CInt(dgvItemNameGrid.DataKeys(dr.RowIndex)("questionnaireid")) & "")
                        drRow("gridcolumnsid" & drKey.Item("gridcolumnsid").ToString) = CStr(CType(dr.FindControl(drKey.Item("controlname").ToString), DropDownList).SelectedValue)
                    Next
                    drRow("trainingevaluationtranunkids") = CStr(dgvItemNameGrid.DataKeys(dr.RowIndex)("trainingevaluationtranunkids"))
                    drRow("AUD") = CStr(dgvItemNameGrid.DataKeys(dr.RowIndex)("AUD"))
                    dt.Rows.Add(drRow)
                Next
            End If

            Dim strTrainingEvaluationTranUnkids As String = CStr(dgvItemNameGrid.DataKeys(row.RowIndex)("trainingevaluationtranunkids"))

            row.Cells(getColumnID_Griview(CType(dgvItemNameGrid, GridView), "objdgcolhAUD", False, True)).Text = "D"

            'row.Visible = False
            If strTrainingEvaluationTranUnkids.Trim.Length > 0 Then
                For Each xRow As DataRow In dt.Select("trainingevaluationtranunkids =  '" & strTrainingEvaluationTranUnkids & "' ")
                    xRow.Item("AUD") = "D"
                Next
            End If

            AddHandler dgvItemNameGrid.RowDataBound, AddressOf Me.dgvItemNameGrid_OnRowDataBound

            dgvItemNameGrid.DataSource = dt
            dgvItemNameGrid.DataBind()

            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Gridview Events"

    Protected Sub dgvCategory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgvCategory.ItemDataBound
        Dim dsList As New DataSet
        Dim objEval_question_master As New clseval_question_master
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfcategoryunkid As HiddenField = CType(e.Item.FindControl("hfcategoryunkid"), HiddenField)
                Dim dgvQuestion As DataList = CType(e.Item.FindControl("dgvQuestion"), DataList)
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    'Hemant (10 Nov 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                    If mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING AndAlso CInt(hfcategoryunkid.Value) = clseval_group_master.enFeedBack.PRETRAINING Then
                        Dim dtEmpPostEval As DataTable = objEval_question_master.GetList("CategoryWiseQuestion", True, CInt(hfcategoryunkid.Value), , True, , clseval_question_master.enAnswerType.GRID).Tables(0)
                        Dim dtEmpPreEval As DataTable = dtEmpPostEval.Copy()
                        'Hemant (08 Dec 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                        For Each drEmpPostEval As DataRow In dtEmpPostEval.Rows
                            drEmpPostEval.Item("question") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Self Post-Training Evaluation on") & " " & drEmpPostEval.Item("question").ToString
                        Next
                        For Each drEmpPreEval As DataRow In dtEmpPreEval.Rows
                            drEmpPreEval.Item("question") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Self Pre-Training Evaluation on") & " " & drEmpPreEval.Item("question").ToString
                            drEmpPreEval.Item("TypeId") = 1
                        Next
                        dtEmpPreEval.Merge(dtEmpPostEval)
                        dsList.Tables.Add(dtEmpPreEval.Copy)
                        'Hemant (08 Dec 2023) -- End
                    Else
                        'Hemant (10 Nov 2023) -- End
                        dsList = objEval_question_master.GetList("CategoryWiseQuestion", True, CInt(hfcategoryunkid.Value), , True)
                    End If 'Hemant (10 Nov 2023) -- End
                Else
                    'Hemant (20 Aug 2021) -- End

                    'Hemant (10 Nov 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                    If mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING AndAlso CInt(hfcategoryunkid.Value) = clseval_group_master.enFeedBack.PRETRAINING Then
                        Dim dtEmpPostEval As DataTable = objEval_question_master.GetList("CategoryWiseQuestion", True, CInt(hfcategoryunkid.Value), , , , clseval_question_master.enAnswerType.GRID).Tables(0)
                        Dim dtManagerEval As DataTable = dtEmpPostEval.Copy()
                        'Hemant (08 Dec 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                        Dim dtEmpPreEval As DataTable = dtEmpPostEval.Copy()
                        For Each drEmpPostEval As DataRow In dtEmpPostEval.Rows
                            drEmpPostEval.Item("question") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Employee Post-Training Evaluation on") & " " & drEmpPostEval.Item("question").ToString
                            drEmpPostEval.Item("TypeId") = 3
                        Next
                        For Each drEmpPreEval As DataRow In dtEmpPreEval.Rows
                            drEmpPreEval.Item("question") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Employee Pre-Training Evaluation on") & " " & drEmpPreEval.Item("question").ToString
                            drEmpPreEval.Item("TypeId") = 1
                        Next
                        'Hemant (08 Dec 2023) -- End
                        For Each drManagerEval As DataRow In dtManagerEval.Rows
                            drManagerEval.Item("isforlinemanager") = True
                            'Hemant (08 Dec 2023) -- Start
                            'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                            drManagerEval.Item("question") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Manager Evaluation on") & " " & drManagerEval.Item("question").ToString
                            drManagerEval.Item("TypeId") = 4
                            'Hemant (08 Dec 2023) -- End
                        Next
                        dtEmpPreEval.Merge(dtEmpPostEval)
                        'Hemant (08 Dec 2023) -- Start
                        'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                        dtEmpPreEval.Merge(dtManagerEval)
                        'Hemant (08 Dec 2023) -- End
                        dsList.Tables.Add(dtEmpPreEval.Copy)

                    Else
                        'Hemant (10 Nov 2023) -- End
                        dsList = objEval_question_master.GetList("CategoryWiseQuestion", True, CInt(hfcategoryunkid.Value))
                    End If  'Hemant (10 Nov 2023)

                End If 'Hemant (20 Aug 2021) 

                AddHandler dgvQuestion.ItemDataBound, AddressOf Me.dgvQuestion_ItemDataBound
                dgvQuestion.DataSource = dsList.Tables("CategoryWiseQuestion")
                dgvQuestion.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvQuestion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Dim dsList As New DataSet
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfquestionnaireId As HiddenField = CType(e.Item.FindControl("hfquestionnaireId"), HiddenField)
                Dim hfAnswerType As HiddenField = CType(e.Item.FindControl("hfAnswerType"), HiddenField)
                Dim hfAskForJustification As HiddenField = CType(e.Item.FindControl("hfAskForJustification"), HiddenField)
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                Dim hfForLineManagerFeedback As HiddenField = CType(e.Item.FindControl("hfForLineManagerFeedback"), HiddenField)
                'Hemant (20 Aug 2021) -- End

                Dim pnlAnswerFreeText As Panel = CType(e.Item.FindControl("pnlAnswerFreeText"), Panel)
                Dim pnlAnswerSelection As Panel = CType(e.Item.FindControl("pnlAnswerSelection"), Panel)
                Dim pnlAnswerDtp As Panel = CType(e.Item.FindControl("pnlAnswerDtp"), Panel)
                Dim pnlAnswerRating As Panel = CType(e.Item.FindControl("pnlAnswerRating"), Panel)
                Dim pnlAnswerNameNum As Panel = CType(e.Item.FindControl("pnlAnswerNameNum"), Panel)
                Dim pnlJustification As Panel = CType(e.Item.FindControl("pnlJustification"), Panel)
                'Hemant (10 Nov 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                Dim pnlAnswerGrid As Panel = CType(e.Item.FindControl("pnlAnswerGrid"), Panel)
                'Hemant (10 Nov 2023) -- End
                'Hemant ((08 Dec 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                Dim hfTypeId As HiddenField = CType(e.Item.FindControl("hfTypeId"), HiddenField)
                'Hemant ((08 Dec 2023) -- End


                Dim dgvSubQuestion As DataList = CType(e.Item.FindControl("dgvSubQuestion"), DataList)

                Dim pnlSubQuestion As Panel = CType(e.Item.FindControl("pnlSubQuestion"), Panel)
                Dim lblActionPlanGoalDescription As Label = CType(e.Item.FindControl("lblActionPlanGoalDescription"), Label)

                dsList = objEval_subquestion_master.GetList("SubQuestion", True, CInt(hfquestionnaireId.Value))

                If IsNothing(dsList) = False AndAlso dsList.Tables("SubQuestion").Rows.Count <= 0 Then
                    Select Case CInt(hfAnswerType.Value)
                        Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                            pnlAnswerFreeText.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerFreeText.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerFreeText.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                        Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                            pnlAnswerNameNum.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerNameNum.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerNameNum.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                        Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                            pnlAnswerDtp.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerDtp.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerDtp.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                        Case CInt(clseval_question_master.enAnswerType.RATING)
                            pnlAnswerRating.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerRating.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerRating.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                            Dim objEvalAnswer As New clseval_answer_master
                            Dim dsCombo As DataSet = Nothing
                            Dim cboItemNameRating As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameRating"), DropDownList)
                            dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfquestionnaireId.Value))
                            With cboItemNameRating
                                .DataValueField = "id"
                                .DataTextField = "name"
                                .DataSource = dsCombo.Tables("List")
                                .DataBind()
                                .SelectedValue = "0"
                            End With
                            objEvalAnswer = Nothing
                        Case CInt(clseval_question_master.enAnswerType.SELECTION)
                            pnlAnswerSelection.Visible = True
                            'Hemant (20 Aug 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                pnlAnswerSelection.Visible = Not CBool(hfForLineManagerFeedback.Value)
                            Else
                                pnlAnswerSelection.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If
                            'Hemant (20 Aug 2021) -- End
                            Dim objEvalAnswer As New clseval_answer_master
                            Dim dsCombo As DataSet = Nothing
                            Dim cboItemNameSelection As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameSelection"), DropDownList)
                            dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfquestionnaireId.Value))
                            With cboItemNameSelection
                                .DataValueField = "id"
                                .DataTextField = "name"
                                .DataSource = dsCombo.Tables("List")
                                .DataBind()
                                .SelectedValue = "0"
                            End With
                            objEvalAnswer = Nothing

                            'Hemant (10 Nov 2023) -- Start
                            'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
                        Case CInt(clseval_question_master.enAnswerType.GRID)
                            mdtCustomTabularGrid.Rows.Clear()
                            pnlAnswerGrid.Visible = True
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                'Hemant ((08 Dec 2023) -- Start
                                'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                                If CInt(hfTypeId.Value) = 1 Then
                                    pnlAnswerGrid.Enabled = False
                                    'Hemant (19 Jan 2024) -- Start
                                    'ENHANCEMENT(TRA): A1X-1638 - Evaluation form grid enhancement to include general employee and supervisor comments 
                                    pnlJustification.Enabled = False
                                    'Hemant (19 Jan 2024) -- End
                                Else
                                    'Hemant ((08 Dec 2023) -- End
                                pnlAnswerGrid.Visible = Not CBool(hfForLineManagerFeedback.Value)
                                End If 'Hemant ((08 Dec 2023)
                            Else
                                pnlAnswerGrid.Enabled = CBool(hfForLineManagerFeedback.Value)
                            End If

                            Dim dsItems As New DataSet
                            If mintFeedBackModeId = clseval_group_master.enFeedBack.PRETRAINING Then
                                dsItems = (New clseval_grid_columns_master).GetList("List", CInt(hfquestionnaireId.Value), True, True, , , False)
                            ElseIf mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                                Dim dtItems1 As DataTable = (New clseval_grid_columns_master).GetList("List", CInt(hfquestionnaireId.Value), True, False, True, , False).Tables(0)
                                If CBool(hfForLineManagerFeedback.Value) = True Then
                                    Dim dtItem2 As New DataTable
                                    dtItem2 = (New clseval_grid_columns_master).GetList("List", CInt(hfquestionnaireId.Value), True, False, , True, False).Tables(0)
                                    dtItems1.Merge(dtItem2)
                                    dtItems1 = dtItems1.DefaultView.ToTable(True)
                                End If
                                dsItems.Tables.Add(dtItems1.Copy)
                            End If

                            If mdsGridColumn.Tables.Count <= 0 Then
                                Dim xTable As DataTable = dsItems.Tables(0)
                                mdsGridColumn.Tables.Add(xTable.Copy)
                            End If

                            For Each drItems As DataRow In dsItems.Tables(0).Copy.Rows
                                Dim xRow() As DataRow = mdsGridColumn.Tables(0).Select("gridcolumnsid = " & CInt(drItems.Item("gridcolumnsid")) & " ")
                                If xRow.Length <= 0 Then
                                    mdsGridColumn.Tables(0).ImportRow(drItems)
                                End If
                            Next
                            mdsGridColumn.AcceptChanges()

                            'mdicGridColumn = (From p In dsItems.Tables("List") Select New With {Key .ColumnId = CInt(p.Item("gridcolumnsid").ToString), Key .AnswerType = CInt(p.Item("answertype").ToString)}).ToDictionary(Function(x) x.ColumnId, Function(y) y.AnswerType)
                            Dim objEvalAnswer As New clseval_answer_master
                            Dim dtGridColumnItems As DataTable = objEvalAnswer.GetList("List", CInt(hfquestionnaireId.Value), , , True).Tables(0)
                            If mdsGridColumnItems.Tables.Count <= 0 Then
                                mdsGridColumnItems.Tables.Add(dtGridColumnItems.Copy)
                            End If

                            Dim drGridColumnItems() As DataRow = mdsGridColumnItems.Tables(0).Select("questionnaireId = " & CInt(hfquestionnaireId.Value) & "")
                            If drGridColumnItems.Length <= 0 Then
                                For Each xGridColumnItems As DataRow In dtGridColumnItems.Select("questionnaireId = " & CInt(hfquestionnaireId.Value) & "")
                                    mdsGridColumnItems.Tables(0).ImportRow(xGridColumnItems)
                                Next
                            End If
                            mdsGridColumnItems.AcceptChanges()


                            objEvalAnswer = Nothing
                            Dim dgvItemNameGrid As GridView = TryCast(pnlAnswerGrid.FindControl("dgvItemNameGrid"), GridView)
                            Dim btnAdd As Button = TryCast(pnlAnswerGrid.FindControl("btnAdd"), Button)
                            Dim dCol1 As DataColumn

                            If mdtCustomTabularGrid.Columns.Contains("trainingevaluationtranunkid") = False Then
                                mdtCustomTabularGrid.Columns.Add("trainingevaluationtranunkid", Type.GetType("System.Int32")).DefaultValue = -1
                            End If

                            If mdtCustomTabularGrid.Columns.Contains("questionnaireid") = False Then
                                mdtCustomTabularGrid.Columns.Add("questionnaireid", Type.GetType("System.Int32")).DefaultValue = -1
                            End If

                            If mdtCustomTabularGrid.Columns.Contains("Particulars") = False Then
                                dCol1 = New DataColumn
                                With dCol1
                                    .ColumnName = "Particulars"
                                    .Caption = "Particulars"
                                    .DataType = System.Type.GetType("System.String")
                                    .DefaultValue = ""
                                End With
                                mdtCustomTabularGrid.Columns.Add(dCol1)
                            End If

                            Dim iColName As String = String.Empty
                            iColName = "" : iColName = "Particulars"
                            Dim dgvCol As New BoundField()

                            If mdtCustomTabularGrid.Columns.Contains("AUD") = False Then
                                mdtCustomTabularGrid.Columns.Add("AUD", Type.GetType("System.String")).DefaultValue = ""
                            End If

                            If mdtBindGridColumn.Columns.Contains("questionnaireId") = False Then
                                mdtBindGridColumn.Columns.Add("questionnaireId", Type.GetType("System.Int32")).DefaultValue = -1
                            End If
                            If mdtBindGridColumn.Columns.Contains("controlname") = False Then
                                mdtBindGridColumn.Columns.Add("controlname", Type.GetType("System.String")).DefaultValue = ""
                            End If
                            If mdtBindGridColumn.Columns.Contains("gridcolumnsid") = False Then
                                mdtBindGridColumn.Columns.Add("gridcolumnsid", Type.GetType("System.Int32")).DefaultValue = -1
                            End If


                            Dim strFilter As String = String.Empty
                            Dim dtControlType As New DataTable
                            If mintFeedBackModeId = clseval_group_master.enFeedBack.PRETRAINING Then
                                dtControlType = dsItems.Tables(0).DefaultView.ToTable(True, "answertype")
                                strFilter = " 1 = 1 "
                            ElseIf mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then

                                If CBool(hfForLineManagerFeedback.Value) = True Then
                                    strFilter = " isforlinemanager = 1 "
                                Else
                                    strFilter = " isforposttraining = 1 "

                                End If
                                Dim xTable As New DataTable
                                Dim xRow() As DataRow = dsItems.Tables(0).Select(strFilter)
                                If xRow.Length > 0 Then
                                    xTable = xRow.CopyToDataTable
                                Else
                                    xTable = dsItems.Tables(0).Clone()
                                End If
                                dtControlType = xTable.DefaultView.ToTable(True, "answertype")
                            End If


                            For Each drControlType As DataRow In dtControlType.Rows
                                Dim intColumnId As Integer = 0
                                For Each drow As DataRow In dsItems.Tables(0).Select("answertype = " & CInt(drControlType.Item("answertype")) & "")
                                    iColName = "" : iColName = drow.Item("gridcolumnsname").ToString

                                    intColumnId = intColumnId + 1

                                    'Hemant ((08 Dec 2023) -- Start
                                    'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                                    'If mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                                    '    If CBool(hfForLineManagerFeedback.Value) = True AndAlso CBool(drow.Item("isforlinemanager")) = False Then
                                    '        Continue For
                                    '    ElseIf CBool(hfForLineManagerFeedback.Value) = False AndAlso CBool(drow.Item("isforposttraining")) = False Then
                                    '        Continue For
                                    '    End If
                                    'End If
                                    'Hemant ((08 Dec 2023) -- End


                                    If mdtCustomTabularGrid.Columns.Contains("gridcolumnsid" & drow.Item("gridcolumnsid").ToString) = False Then
                                        dCol1 = New DataColumn
                                        With dCol1
                                            .ColumnName = "gridcolumnsid" & drow.Item("gridcolumnsid").ToString
                                            .Caption = "gridcolumnsid" & drow.Item("gridcolumnsid").ToString
                                            'Hemant ((08 Dec 2023) -- Start
                                            'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                                            '.DataType = System.Type.GetType("System.String")
                                            '.DefaultValue = ""
                                            .DataType = System.Type.GetType("System.Int32")
                                            .DefaultValue = 0
                                            'Hemant ((08 Dec 2023) -- End
                                        End With
                                        mdtCustomTabularGrid.Columns.Add(dCol1)
                                    End If

                                    If mdtCustomTabularGrid.Columns.Contains("trainingevaluationtranunkids") = False Then
                                        mdtCustomTabularGrid.Columns.Add("trainingevaluationtranunkids", Type.GetType("System.String")).DefaultValue = ""
                                    End If

                                    'Hemant ((08 Dec 2023) -- Start
                                    'ISSUE/ENHANCEMENT(TRA): A1X-1595 - Grid column setup modification to skip questionnaire setup for post training feedback
                                    If mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                                        If CBool(hfForLineManagerFeedback.Value) = True AndAlso CBool(drow.Item("isforlinemanager")) = False Then
                                            Continue For
                                        ElseIf CBool(hfForLineManagerFeedback.Value) = False AndAlso CBool(drow.Item("isforposttraining")) = False Then
                                            Continue For
                                        ElseIf CInt(hfTypeId.Value) = 1 AndAlso CBool(drow.Item("isforpretraining")) = False Then
                                            Continue For
                                        End If
                                    End If
                                    'Hemant ((08 Dec 2023) -- End

                                    Select Case CInt(drControlType.Item("answertype"))
                                        Case clseval_question_master.enAnswerType.SELECTION
                                            If mdtBindGridColumn.Select("questionnaireId = " & CInt(hfquestionnaireId.Value) & " AND controlname = 'cboColumn " & intColumnId & "' ").Length <= 0 Then
                                                mdtBindGridColumn.Rows.Add(CInt(hfquestionnaireId.Value), "cboColumn" & intColumnId, CInt(drow.Item("gridcolumnsid")))
                                            End If
                                            If mdicBindGridColumn.ContainsKey("cboColumn" & intColumnId) = False Then
                                                mdicBindGridColumn.Add("cboColumn" & intColumnId, CInt(drow.Item("gridcolumnsid")))
                                            End If
                                            dgvItemNameGrid.Columns(getColumnID_Griview(dgvItemNameGrid, "dgcolhcboColumn" & intColumnId, False, True)).HeaderText = CStr(drow.Item("gridcolumnsname").ToString)
                                            dgvItemNameGrid.Columns(getColumnID_Griview(dgvItemNameGrid, "dgcolhcboColumn" & intColumnId, False, True)).Visible = True

                                    End Select


                                Next
                            Next


                            If mdtCustomTabularGrid IsNot Nothing AndAlso mdtCustomTabularGrid.Rows.Count <= 0 Then
                                If mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                                    Dim dtEval As DataTable
                                    dtEval = (New clstraining_evaluation_tran).GetList("List", mintEmployeeunkid, mintTrainingRequestunkid, clseval_group_master.enFeedBack.PRETRAINING, , , CInt(hfquestionnaireId.Value)).Tables(0)
                                    For Each drEval As DataRow In dtEval.DefaultView.ToTable(True, "Particulars").Rows
                                        Dim drRow As DataRow = mdtCustomTabularGrid.NewRow
                                        drRow("questionnaireid") = CInt(hfquestionnaireId.Value)
                                        drRow("trainingevaluationtranunkid") = -1
                                        drRow("Particulars") = drEval.Item("Particulars")
                                        mdtCustomTabularGrid.Rows.Add(drRow)
                                    Next
                                    'Hemant (19 Jan 2024) -- Start
                                    'ENHANCEMENT(TRA): A1X-1639 - TRA - Training evaluation form grid enhancement to include overall rating section for every column grid
                                    Dim drDefault As DataRow = mdtCustomTabularGrid.NewRow
                                    drDefault("questionnaireid") = CInt(hfquestionnaireId.Value)
                                    drDefault("trainingevaluationtranunkid") = -1
                                    drDefault("Particulars") = "Over All Rating"
                                    mdtCustomTabularGrid.Rows.Add(drDefault)
                                    'Hemant (19 Jan 2024) -- End
                                    btnAdd.Visible = False
                                    dgvItemNameGrid.Columns(getColumnID_Griview(dgvItemNameGrid, "dgcolhDeleteParticulars", False, True)).Visible = False
                                Else
                                    Dim drRow As DataRow = mdtCustomTabularGrid.NewRow
                                    drRow("questionnaireid") = CInt(hfquestionnaireId.Value)
                                    drRow("trainingevaluationtranunkid") = -1
                                    drRow("Particulars") = ""
                                    mdtCustomTabularGrid.Rows.Add(drRow)
                                    If mblnIsPreTrainingFeedbackSubmitted = True Then
                                        btnAdd.Visible = False
                                        dgvItemNameGrid.Columns(getColumnID_Griview(dgvItemNameGrid, "dgcolhDeleteParticulars", False, True)).Visible = False
                                    End If
                                End If

                            End If

                            AddHandler dgvItemNameGrid.RowDataBound, AddressOf Me.dgvItemNameGrid_OnRowDataBound

                            dgvItemNameGrid.DataSource = mdtCustomTabularGrid
                            dgvItemNameGrid.DataBind()
                            'Hemant (10 Nov 2023) -- End

                        Case Else
                    End Select
                    If CBool(hfAskForJustification.Value) = True Then
                        pnlJustification.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlJustification.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlJustification.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                        'Hemant (19 Jan 2024) -- Start
                        'ENHANCEMENT(TRA): A1X-1638 - Evaluation form grid enhancement to include general employee and supervisor comments 
                        If CInt(hfAnswerType.Value) = CInt(clseval_question_master.enAnswerType.GRID) Then
                            Dim lblJustify As Label = TryCast(pnlJustification.FindControl("lblJustify"), Label)
                            If CBool(hfForLineManagerFeedback.Value) = True Then
                                lblJustify.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "General Supervisor Comments")
                            Else
                                lblJustify.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "General Employee Comments")
                            End If
                        End If
                        'Hemant (19 Jan 2024) -- End
                    End If
                    pnlSubQuestion.Visible = False
                Else
                    pnlSubQuestion.Visible = True
                    lblActionPlanGoalDescription.Visible = False
                    AddHandler dgvSubQuestion.ItemDataBound, AddressOf Me.dgvSubQuestion_ItemDataBound
                    dgvSubQuestion.DataSource = dsList.Tables("SubQuestion")
                    dgvSubQuestion.DataBind()
                End If



            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvSubQuestion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        Dim dsList As New DataSet
        Dim objEval_subquestion_master As New clseval_subquestion_master
        Try
            If e.Item.ItemIndex > -1 Then
                Dim hfSubQuestionId As HiddenField = CType(e.Item.FindControl("hfSubQuestionId"), HiddenField)
                Dim hfQuestionnaireId As HiddenField = CType(e.Item.FindControl("hfQuestionnaireId"), HiddenField)
                Dim hfAnswerType As HiddenField = CType(e.Item.FindControl("hfAnswerType"), HiddenField)
                Dim hfAskForJustification As HiddenField = CType(e.Item.FindControl("hfAskForJustification"), HiddenField)
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                Dim hfForLineManagerFeedback As HiddenField = CType(e.Item.FindControl("hfForLineManagerFeedback"), HiddenField)
                'Hemant (20 Aug 2021) -- End

                Dim pnlAnswerFreeText As Panel = CType(e.Item.FindControl("pnlAnswerFreeText"), Panel)
                Dim pnlAnswerSelection As Panel = CType(e.Item.FindControl("pnlAnswerSelection"), Panel)
                Dim pnlAnswerDtp As Panel = CType(e.Item.FindControl("pnlAnswerDtp"), Panel)
                Dim pnlAnswerRating As Panel = CType(e.Item.FindControl("pnlAnswerRating"), Panel)
                Dim pnlAnswerNameNum As Panel = CType(e.Item.FindControl("pnlAnswerNameNum"), Panel)
                Dim pnlJustification As Panel = CType(e.Item.FindControl("pnlJustification"), Panel)
                Dim dgvSubQuestion As DataList = CType(e.Item.FindControl("dgvSubQuestion"), DataList)


                Select Case CInt(hfAnswerType.Value)
                    Case CInt(clseval_question_master.enAnswerType.FREETEXT)
                        pnlAnswerFreeText.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerFreeText.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerFreeText.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    Case CInt(clseval_question_master.enAnswerType.NUMERIC)
                        pnlAnswerNameNum.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerNameNum.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerNameNum.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    Case CInt(clseval_question_master.enAnswerType.PICKDATE)
                        pnlAnswerDtp.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerDtp.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerDtp.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                    Case CInt(clseval_question_master.enAnswerType.RATING)
                        pnlAnswerRating.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerRating.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerRating.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                        Dim objEvalAnswer As New clseval_answer_master
                        Dim dsCombo As DataSet = Nothing
                        Dim cboItemNameRating As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameRating"), DropDownList)
                        dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfQuestionnaireId.Value), CInt(hfSubQuestionId.Value))
                        With cboItemNameRating
                            .DataValueField = "id"
                            .DataTextField = "name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With
                        objEvalAnswer = Nothing
                    Case CInt(clseval_question_master.enAnswerType.SELECTION)
                        pnlAnswerSelection.Visible = True
                        'Hemant (20 Aug 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            pnlAnswerSelection.Visible = Not CBool(hfForLineManagerFeedback.Value)
                        Else
                            pnlAnswerSelection.Enabled = CBool(hfForLineManagerFeedback.Value)
                        End If
                        'Hemant (20 Aug 2021) -- End
                        Dim objEvalAnswer As New clseval_answer_master
                        Dim dsCombo As DataSet = Nothing
                        Dim cboItemNameSelection As DropDownList = TryCast(pnlAnswerSelection.FindControl("cboItemNameSelection"), DropDownList)
                        dsCombo = objEvalAnswer.GetComboList("List", True, CInt(hfQuestionnaireId.Value), CInt(hfSubQuestionId.Value))
                        With cboItemNameSelection
                            .DataValueField = "id"
                            .DataTextField = "name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With
                        objEvalAnswer = Nothing
                    Case Else
                End Select

                If CBool(hfAskForJustification.Value) = True Then
                    pnlJustification.Visible = True
                    'Hemant (20 Aug 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        pnlJustification.Visible = Not CBool(hfForLineManagerFeedback.Value)
                    Else
                        pnlJustification.Enabled = CBool(hfForLineManagerFeedback.Value)
                    End If
                    'Hemant (20 Aug 2021) -- End
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (10 Nov 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA): A1X-1463 - Training evaluation form enhancement to support assessment of course objectives set on the pre-training form
    Protected Sub dgvItemNameGrid_OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If CType(sender, GridView).DataKeys(e.Row.RowIndex)("trainingevaluationtranunkids").ToString.Trim.Length > 0 OrElse mintFeedBackModeId = clseval_group_master.enFeedBack.DAYSAFTERTRAINING Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(CType(sender, GridView), "dgcolhParticulars", False, True)).Enabled = False
                    Dim txtParticulars As TextBox = CType(e.Row.Cells(GetColumnIndex.getColumnID_Griview(CType(sender, GridView), "dgcolhParticulars", False, True)).FindControl("txtParticulars"), TextBox)
                    txtParticulars.Enabled = False
                End If

                'If e.Row.Cells(getColumnID_Griview(CType(sender, GridView), "objdgcolhAUD", False, True)).Text = "D" Then
                '    e.Row.Visible = False
                'End If

                If CType(sender, GridView).DataKeys(e.Row.RowIndex)("AUD").ToString = "D" Then
                    e.Row.Visible = False
                End If

                Dim intColumnId As Integer = 1
                For Each drkey As DataRow In mdsGridColumn.Tables(0).Select("questionnaireid = " & CInt(CType(sender, GridView).DataKeys(e.Row.RowIndex)("questionnaireid").ToString) & " ")
                    If CInt(drkey.Item("answertype")) = clseval_question_master.enAnswerType.SELECTION Then
                        Dim objGridcolumns As New clseval_grid_columns_master
                        objGridcolumns._GridColumnsid = CInt(drkey.Item("gridcolumnsid"))

                        Dim drRow() As DataRow = mdsGridColumnItems.Tables(0).Select("trainingevaluationanswerunkid <= 0 OR gridcolumnsid = " & CInt(drkey.Item("gridcolumnsid")) & "")
                        Dim dtTable As New DataTable
                        If drRow.Length > 0 Then
                            dtTable = drRow.CopyToDataTable()
                        Else
                            dtTable = mdsGridColumnItems.Tables(0).Clone
                        End If

                        If GetColumnIndex.getColumnID_Griview(CType(sender, GridView), "dgcolhcboColumn" & intColumnId, False, True) > 0 Then

                            Dim cboColumn As DropDownList = CType(e.Row.Cells(GetColumnIndex.getColumnID_Griview(CType(sender, GridView), "dgcolhcboColumn" & intColumnId, False, True)).FindControl("cboColumn" & intColumnId), DropDownList)
                            With cboColumn
                                .DataValueField = "optionid"
                                .DataTextField = "optionvalue"
                                .DataSource = dtTable
                                .DataBind()
                                .SelectedValue = CStr(CType(CType(sender, GridView).DataSource, DataTable).Rows(CInt(e.Row.DataItemIndex)).Item("gridcolumnsid" & CInt(drkey.Item("gridcolumnsid"))))
                            End With
                        End If

                        CType(sender, GridView).Columns(getColumnID_Griview(CType(sender, GridView), "dgcolhcboColumn" & intColumnId, False, True)).HeaderText = CStr(objGridcolumns._GridColumnsName)

                        objGridcolumns = Nothing
                        If mdicBindGridColumn.ContainsKey("cboColumn" & intColumnId) = False Then
                            mdicBindGridColumn.Add("cboColumn" & intColumnId, CInt(drkey.Item("gridcolumnsid")))
                        End If


                    End If
                    intColumnId = intColumnId + 1
                Next


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (10 Nov 2023) -- End

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELPARTICULARS"
                    Dim lnkdelete As Button = TryCast(sender, Button)
                    Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Training Evaluation Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, all questions answers are mandatory. please attempt all questions to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Sorry, all questions justifications are mandatory. please attempt all questions justifications  to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, Negative Value is not allowed in Numeric Field.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, your line manager info is not present. Please contact support to proceed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Employee Pre-Training Evaluation on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Employee Post-Training Evaluation on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Manager Evaluation on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Self Evaluation on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Self Post-Training Evaluation on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Self Pre-Training Evaluation on")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "General Employee Comments")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "General Supervisor Comments")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
