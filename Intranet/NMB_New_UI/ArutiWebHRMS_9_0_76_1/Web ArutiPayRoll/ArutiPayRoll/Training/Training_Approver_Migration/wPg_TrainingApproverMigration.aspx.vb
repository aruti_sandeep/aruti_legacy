﻿Option Strict On

#Region "Import"

Imports System.Data
Imports Aruti.Data

#End Region
Partial Class Training_Training_Approver_Migration_wPg_TrainingApproverMigration
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmTrainingApproverMigration"
    Dim DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = String.Empty
    Private dtOldApproverEmp As DataTable = Nothing
    Private dtNewApproverAssignEmp As DataTable = Nothing
    Private dtNewApproverMigratedEmp As DataTable = Nothing
    Private dvOldApproverEmp As DataView
    Dim dtFromApprover As DataTable = Nothing
    Private mintOldApproverEmpunkid As Integer = -1
    Private mintNewApproverEmpunkid As Integer = -1
    Private mintNewApproverUserunkid As Integer = -1
    Private mblnIsExternalOldApprover As Boolean = False
    Private mblnIsExternalNewApprover As Boolean = False
    Private dtToApprover As DataTable = Nothing

#End Region

#Region "Page's Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                GC.Collect()
                Call SetLanguage()
                Call FillCombo()

            Else
                dtFromApprover = CType(Me.ViewState("dtFromApprover"), DataTable)
                dtOldApproverEmp = CType(Me.ViewState("dvOldApproverEmp"), DataTable)
                dtNewApproverAssignEmp = CType(Me.ViewState("dtNewApproverAssignEmp"), DataTable)
                dtNewApproverMigratedEmp = CType(Me.ViewState("dtNewApproverMigratedEmp"), DataTable)
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
                mintOldApproverEmpunkid = CInt(Me.ViewState("OldApproverEmpunkid"))
                mintNewApproverEmpunkid = CInt(Me.ViewState("NewApproverEmpunkid"))
                mintNewApproverUserunkid = CInt(Me.ViewState("mintNewApproverUserunkid"))
                mblnIsExternalOldApprover = CBool(Me.ViewState("IsExternalOldApprover"))
                mblnIsExternalNewApprover = CBool(Me.ViewState("IsExternalNewApprover"))
                dtToApprover = CType(Me.ViewState("dtToApprover"), DataTable)

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dtFromApprover") = dtFromApprover
            Me.ViewState("dvOldApproverEmp") = dtOldApproverEmp
            Me.ViewState("dtNewApproverAssignEmp") = dtNewApproverAssignEmp
            Me.ViewState("dtNewApproverMigratedEmp") = dtNewApproverMigratedEmp
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("OldApproverEmpunkid") = mintOldApproverEmpunkid
            Me.ViewState("NewApproverEmpunkid") = mintNewApproverEmpunkid
            Me.ViewState("mintNewApproverUserunkid") = mintNewApproverUserunkid
            Me.ViewState("IsExternalOldApprover") = mblnIsExternalOldApprover
            Me.ViewState("IsExternalNewApprover") = mblnIsExternalNewApprover
            Me.ViewState("dtToApprover") = dtToApprover

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objMaster As New clsMasterData
        Dim objTrainingApproverLevel As New clstraining_approverlevel_master

        Dim dsList As DataSet = Nothing
        Dim dsFill As DataSet = Nothing
        Try

            dsList = objCalendar.getListForCombo("List", , StatusType.Open)
            With cboTrainingCalendar
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objMaster.getComboListForTrainingType(True, "List")
            With cboTrainingType
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            'dsList = objTrainingApprover.getListForCombo(CInt(cboTrainingCalendar.SelectedValue), CInt(cboTrainingType.SelectedValue), "List", True, True)

            'dtFromApprover = dsList.Tables("List")

            'With cboOldApprover
            '    .DataTextField = "name"
            '    .DataValueField = "approverunkid"

            '    .DataSource = dtFromApprover
            '    .DataBind()
            'End With
            'Call cboOldApprover_SelectedIndexChanged(cboOldApprover, Nothing)

            Call cboTrainingType_SelectedIndexChanged(cboTrainingType, Nothing)

            dsFill = objTrainingApproverLevel.getListForCombo(CInt(cboTrainingCalendar.SelectedValue), "List", True)
            With cboOldLevel
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsFill.Tables(0)
                .DataBind()
            End With

            With cboNewLevel
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsFill.Tables(0).Copy
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
            objMaster = Nothing
            objTrainingApproverLevel = Nothing
            If IsNothing(dsFill) = False Then
                dsFill.Clear()
                dsFill = Nothing
            End If
        End Try
    End Sub

    Private Sub GetTrainingApproverLevel(ByVal sender As Object, ByVal intApproverEmpID As Integer, Optional ByVal blnExtarnalApprover As Boolean = False)
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map

        Dim dsList As DataSet = Nothing
        Try

            dsList = objTrainingApprover.GetLevelFromTrainingApprover(CInt(cboTrainingCalendar.SelectedValue), CInt(cboTrainingType.SelectedValue), intApproverEmpID, blnExtarnalApprover, True)

            If CType(sender, DropDownList).ID.ToUpper = "CBOOLDAPPROVER" Then
                With cboOldLevel
                    .DataValueField = "levelunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedIndex = 0
                End With

            ElseIf CType(sender, DropDownList).ID.ToUpper = "CBONEWAPPROVER" Then
                With cboNewLevel
                    .DataValueField = "levelunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedIndex = 0
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
        End Try
    End Sub

    Private Sub FillOldApproverEmployeeList()
        Try
            Dim strSearch As String = ""

            'If dtOldApproverEmp Is Nothing Then
            '    dtOldApproverEmp = CType(Me.ViewState("dtOldApproverEmp"), DataTable)
            'End If

            dvOldApproverEmp = dtOldApproverEmp.DefaultView
            If mstrAdvanceFilter.Trim.Length > 0 Then
                dvOldApproverEmp.RowFilter = mstrAdvanceFilter.Trim.Replace("ADF.", "")
                dvOldApproverEmp.Table.AcceptChanges()
                dtOldApproverEmp = dvOldApproverEmp.Table
                'Me.ViewState("dtOldApproverEmp") = dtOldApproverEmp
            End If

            dgOldApproverEmp.AutoGenerateColumns = False
            dgOldApproverEmp.DataSource = dtOldApproverEmp
            dgOldApproverEmp.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillNewApproverMigratedEmployeeList()
        Try
            If dtNewApproverMigratedEmp Is Nothing Then Exit Sub

            dgNewApproverMigratedEmp.AutoGenerateColumns = False
            dgNewApproverMigratedEmp.DataSource = dtNewApproverMigratedEmp
            dgNewApproverMigratedEmp.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillNewApproverAssignEmployeeList()
        Try
            If dtNewApproverAssignEmp Is Nothing Then Exit Sub

            dgNewApproverAssignEmp.AutoGenerateColumns = False
            dgNewApproverAssignEmp.DataSource = dtNewApproverAssignEmp
            dgNewApproverAssignEmp.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillOldApproverEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub objbtnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            If dtOldApproverEmp Is Nothing Then Exit Sub

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
                cboNewApprover.Focus()
                Exit Sub
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboNewLevel.Focus()
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgOldApproverEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvOldSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please check atleast one employee to migrate."), Me)
                Exit Sub
            End If


            Dim blnFlag As Boolean = False

            'If dgOldApproverEmp.Items.Count <= 0 Then Exit Sub

            If gRow.Count > 0 Then
                Dim xCount As Integer = -1
                For i As Integer = 0 To gRow.Count - 1
                    xCount = i
                    If dtNewApproverAssignEmp IsNot Nothing AndAlso dtNewApproverAssignEmp.Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtNewApproverAssignEmp.Select("employeeunkid=" & CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                        If dRow.Length > 0 Then
                            blnFlag = True
                            Continue For
                        End If
                    End If


                    If mblnIsExternalNewApprover = False AndAlso CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) = mintNewApproverEmpunkid Then Continue For

                    If dtNewApproverMigratedEmp Is Nothing Then
                        dtNewApproverMigratedEmp = dtOldApproverEmp.Clone
                        Dim drMigratedEmp As DataRow = dtNewApproverMigratedEmp.NewRow()
                        drMigratedEmp("select") = False
                        drMigratedEmp("approverunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                        drMigratedEmp("approvertranunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("approvertranunkid"))
                        drMigratedEmp("approverempunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("approverempunkid"))
                        drMigratedEmp("levelunkid") = 0
                        drMigratedEmp("employeeunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                        drMigratedEmp("employeecode") = dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                        drMigratedEmp("employeename") = dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                        dtNewApproverMigratedEmp.Rows.Add(drMigratedEmp)
                    Else
                        Dim dr As DataRow() = dtNewApproverMigratedEmp.Select("employeeunkid = " & CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                        If dr.Length <= 0 Then
                            Dim drMigratedEmp As DataRow = dtNewApproverMigratedEmp.NewRow()
                            drMigratedEmp("select") = False
                            drMigratedEmp("approverunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                            drMigratedEmp("approvertranunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("approvertranunkid"))
                            drMigratedEmp("approverempunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("approverempunkid"))
                            drMigratedEmp("levelunkid") = 0
                            drMigratedEmp("employeeunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                            drMigratedEmp("employeecode") = dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                            drMigratedEmp("employeename") = dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                            dtNewApproverMigratedEmp.Rows.Add(drMigratedEmp)
                        End If
                    End If

                    'Me.ViewState("dtNewApproverMigratedEmp") = dtNewApproverMigratedEmp

                    If dtOldApproverEmp IsNot Nothing Then
                        Dim drEmp = dtOldApproverEmp.AsEnumerable().Where(Function(x) x.Field(Of String)("employeecode") <> "None" And x.Field(Of Integer)("employeeunkid") = CInt(dgOldApproverEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                        If drEmp.Count > 0 Then
                            dtOldApproverEmp.Rows.Remove(drEmp(0))
                            dtOldApproverEmp.AcceptChanges()
                        End If
                        drEmp = Nothing
                    End If

                Next
                If blnFlag Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Some of the checked employee is already binded with the selected approver."), Me)
                End If

            End If

            Call FillNewApproverMigratedEmployeeList()
            Call FillOldApproverEmployeeList()

            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub objbtnUnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try
            If dtNewApproverMigratedEmp Is Nothing Then
                DisplayMessage.DisplayMessage("Please check atleast one employee to unassign.", Me)
                Exit Sub
            End If


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgNewApproverMigratedEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvNewSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please check atleast one employee to unassign."), Me)
                Exit Sub
            End If

            mstrAdvanceFilter = ""


            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                If dtOldApproverEmp Is Nothing Then
                    dtOldApproverEmp = dtNewApproverMigratedEmp.Clone
                    Dim drFromEmp As DataRow = dtOldApproverEmp.NewRow()
                    drFromEmp("select") = False
                    drFromEmp("approverunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                    drFromEmp("approvertranunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("approvertranunkid"))
                    drFromEmp("approverempunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("approverempunkid"))
                    drFromEmp("levelunkid") = 0
                    drFromEmp("employeeunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                    drFromEmp("employeecode") = dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                    drFromEmp("employeename") = dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                    dtOldApproverEmp.Rows.Add(drFromEmp)
                Else
                    Dim drFromEmp As DataRow = dtOldApproverEmp.NewRow()
                    drFromEmp("select") = False
                    drFromEmp("approverunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                    drFromEmp("approvertranunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("approvertranunkid"))
                    drFromEmp("approverempunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("approverempunkid"))
                    drFromEmp("levelunkid") = 0
                    drFromEmp("employeeunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                    drFromEmp("employeecode") = dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                    drFromEmp("employeename") = dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                    dtOldApproverEmp.Rows.Add(drFromEmp)
                End If



                If dtNewApproverMigratedEmp IsNot Nothing Then
                    Dim drEmp = dtNewApproverMigratedEmp.AsEnumerable().Where(Function(x) x.Field(Of String)("employeecode") <> "None" And x.Field(Of Integer)("employeeunkid") = CInt(dgNewApproverMigratedEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                    If drEmp.Count > 0 Then
                        dtNewApproverMigratedEmp.Rows.Remove(drEmp(0))
                        dtNewApproverMigratedEmp.AcceptChanges()
                    End If
                    drEmp = Nothing

                End If

            Next

            Call FillOldApproverEmployeeList()
            Call FillNewApproverMigratedEmployeeList()

            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objApproverTran As New clstrainingapprover_tran_emp_map
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Please Select From Approver to migrate employee(s)."), Me)
                cboOldApprover.Focus()
                Exit Sub

            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboOldLevel.Focus()
                Exit Sub

            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), Me)
                cboNewApprover.Focus()
                Exit Sub

            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Please Select Level to migrate employee(s)."), Me)
                cboNewLevel.Focus()
                Exit Sub

            ElseIf dgNewApproverMigratedEmp.Rows.Count = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "There is no employee(s) for migration."), Me)
                Exit Sub

            End If

            objApproverTran._UserId = CInt(Session("UserId"))

            If objApproverTran.Approver_Migration(dtNewApproverMigratedEmp, CInt(cboNewApprover.SelectedValue), mintNewApproverUserunkid) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Problem in Approver Migration."), Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Approver Migration done successfully."), Me)
                Me.ViewState("dvOldApproverEmp") = Nothing
                cboOldApprover.SelectedIndex = 0
                cboOldLevel.SelectedIndex = 0
                cboNewApprover.SelectedValue = "0"
                cboNewLevel.SelectedValue = "0"
                dgOldApproverEmp.DataSource = New List(Of String)
                dgOldApproverEmp.DataBind()
                dgNewApproverMigratedEmp.DataSource = New List(Of String)
                dgNewApproverMigratedEmp.DataBind()
                dtNewApproverMigratedEmp = Nothing
                dgNewApproverAssignEmp.DataSource = New List(Of String)
                dgNewApproverAssignEmp.DataBind()
                cboOldApprover.Focus()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverTran = Nothing
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub cboTrainingType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrainingType.SelectedIndexChanged, _
                                                                                                                           cboTrainingCalendar.SelectedIndexChanged
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboTrainingCalendar.SelectedValue) > 0 AndAlso CInt(cboTrainingType.SelectedValue) > 0 Then
                dsList = objTrainingApprover.getListForCombo(CInt(cboTrainingCalendar.SelectedValue), CInt(cboTrainingType.SelectedValue), "List", True, True)
                dtFromApprover = dsList.Tables("List").Copy
            Else
                Dim dtTemp As New DataTable("List")
                dtTemp.Columns.Add("approverunkid", GetType(Integer))
                dtTemp.Columns.Add("name", GetType(String))
                dtTemp.Rows.Add(-1, "Select")
                dtFromApprover = dtTemp
            End If

            With cboOldApprover
                .DataTextField = "name"
                .DataValueField = "approverunkid"

                .DataSource = dtFromApprover
                .DataBind()
            End With
            Call cboOldApprover_SelectedIndexChanged(cboOldApprover, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
        End Try
    End Sub

    Protected Sub cboOldApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                dgOldApproverEmp.DataSource = New List(Of String)
                dgOldApproverEmp.DataBind()
                cboOldLevel.SelectedIndex = 0
                cboNewLevel.SelectedIndex = 0
                dtToApprover = New DataView(dtFromApprover, "", "", DataViewRowState.CurrentRows).ToTable()
                mblnIsExternalOldApprover = False
            Else
                Dim dRow As DataRow() = dtFromApprover.Select("approverunkid = " & CInt(cboOldApprover.SelectedValue))
                If dRow.Length > 0 Then
                    mblnIsExternalOldApprover = CBool(dRow(0).Item("isexternalapprover"))
                    mintOldApproverEmpunkid = CInt(dRow(0).Item("empapproverunkid"))
                End If
                dtToApprover = New DataView(dtFromApprover, "approverunkid NOT IN(" & CInt(cboOldApprover.SelectedValue) & ") ", "", DataViewRowState.CurrentRows).ToTable()
            End If

            With cboNewApprover
                .DataTextField = "name"
                .DataValueField = "approverunkid"
                .DataSource = dtToApprover
                .DataBind()
            End With

            GetTrainingApproverLevel(sender, mintOldApproverEmpunkid, mblnIsExternalOldApprover)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            cboNewApprover.SelectedIndex = 0
            cboNewLevel.SelectedIndex = 0
            dgOldApproverEmp.DataSource = New List(Of String)
            dgOldApproverEmp.DataBind()
            dgNewApproverMigratedEmp.DataSource = New List(Of String)
            dgNewApproverMigratedEmp.DataBind()
            dgNewApproverAssignEmp.DataSource = New List(Of String)
            dgNewApproverAssignEmp.DataBind()
        End Try
    End Sub

    Protected Sub cboNewApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try
            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                cboNewLevel.SelectedIndex = 0
                dgNewApproverAssignEmp.DataSource = Nothing
                dgNewApproverAssignEmp.DataBind()
                dgNewApproverMigratedEmp.DataSource = Nothing
                dgNewApproverMigratedEmp.DataBind()
                mblnIsExternalNewApprover = False
            Else
                Dim dRow As DataRow() = dtToApprover.Select("approverunkid = " & CInt(cboNewApprover.SelectedValue))
                If dRow.Length > 0 Then
                    mblnIsExternalNewApprover = CBool(dRow(0).Item("isexternalapprover"))
                    mintNewApproverEmpunkid = CInt(dRow(0).Item("empapproverunkid"))
                    'If mblnIsExternalNewApprover = False Then
                    Dim objUserMapping As New clsapprover_Usermapping
                    objUserMapping.GetData(enUserType.Training_Approver, CInt(cboNewApprover.SelectedValue), -1, -1)
                    mintNewApproverUserunkid = objUserMapping._Userunkid
                    objUserMapping = Nothing
                    'Else
                    '    mintNewApproverUserunkid = CInt(dRow(0).Item("empapproverunkid"))
                    'End If
                End If
            End If
            GetTrainingApproverLevel(sender, mintNewApproverEmpunkid, mblnIsExternalNewApprover)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOldLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map
        Dim dsOldApproverEmp As DataSet = Nothing
        Try

            If CInt(cboOldLevel.SelectedValue) <= 0 Then
                cboOldLevel.SelectedIndex = 0
                dgOldApproverEmp.DataSource = New List(Of String)
                dgOldApproverEmp.DataBind()
            Else

                dsOldApproverEmp = objTrainingApprover.GetEmployeeFromTrainingApprover(CStr(Session("Database_Name")), _
                                                                                         CInt(Session("UserId")), _
                                                                                         CInt(Session("Fin_year")), _
                                                                                         CInt(Session("CompanyUnkId")), _
                                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                         CStr(Session("UserAccessModeSetting")), _
                                                                                         True, _
                                                                                         CBool(Session("IsIncludeInactiveEmp")), _
                                                                                         "List", _
                                                                                         CInt(cboTrainingCalendar.SelectedValue), CInt(cboTrainingType.SelectedValue), _
                                                                                         mintOldApproverEmpunkid, CInt(cboOldLevel.SelectedValue), _
                                                                                         mblnIsExternalOldApprover)

                dtOldApproverEmp = dsOldApproverEmp.Tables(0).Copy
                dvOldApproverEmp = dsOldApproverEmp.Tables(0).DefaultView
                Me.ViewState.Add("dvOldApproverEmp", dvOldApproverEmp)
                Me.ViewState.Add("dtOldApproverEmp", dtOldApproverEmp)

                Call FillOldApproverEmployeeList()
            End If

            If dsOldApproverEmp IsNot Nothing Then
                If dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
        End Try
    End Sub

    Protected Sub cboNewLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        Dim objTrainingApprover As New clstrainingapprover_master_emp_map
        Dim dsNewApproverAssignEmp As DataSet = Nothing
        Try

            If CInt(cboNewLevel.SelectedValue) <= 0 Then
                cboNewLevel.SelectedIndex = 0
            Else
                dsNewApproverAssignEmp = objTrainingApprover.GetEmployeeFromTrainingApprover(CStr(Session("Database_Name")), _
                                                                                               CInt(Session("UserId")), _
                                                                                               CInt(Session("Fin_year")), _
                                                                                               CInt(Session("CompanyUnkId")), _
                                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                               CStr(Session("UserAccessModeSetting")), _
                                                                                               True, _
                                                                                               CBool(Session("IsIncludeInactiveEmp")), _
                                                                                               "List", _
                                                                                               CInt(cboTrainingCalendar.SelectedValue), CInt(cboTrainingType.SelectedValue), _
                                                                                               mintNewApproverEmpunkid, _
                                                                                               CInt(cboNewLevel.SelectedValue), _
                                                                                               mblnIsExternalNewApprover)

                dtNewApproverAssignEmp = dsNewApproverAssignEmp.Tables(0)

                Call FillNewApproverAssignEmployeeList()
            End If

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                If dtNewApproverMigratedEmp IsNot Nothing Then
                    dtNewApproverMigratedEmp.Rows.Clear()
                End If
            End If

            If dsNewApproverAssignEmp IsNot Nothing Then
                If dsNewApproverAssignEmp.Tables(0).Rows.Count > 0 Then
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
            If IsNothing(dsNewApproverAssignEmp) = False Then
                dsNewApproverAssignEmp.Clear()
                dsNewApproverAssignEmp = Nothing
            End If
        End Try
    End Sub

#End Region

#Region "CheckBox Events"


#End Region

#Region "LinkButton Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "From Approver is compulsory information.Please select From Approver."), Me)
                cboOldApprover.Focus()
                Exit Sub
            End If
            If CInt(cboOldLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Level is compulsory information.Please select Level."), Me)
                cboOldLevel.Focus()
                Exit Sub
            End If

            If dtOldApproverEmp IsNot Nothing Then

                Dim dRow() As DataRow = dtOldApproverEmp.Select("select = True")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("select") = False
                        dRow(i).AcceptChanges()
                    Next
                End If

            End If

            popupAdvanceFilter.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            mstrAdvanceFilter = ""
            'txtOldSearchEmployee.Text = ""
            If dtOldApproverEmp IsNot Nothing Then
                Dim dRow() As DataRow = dtOldApproverEmp.Select("select = True")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("select") = False
                        dRow(i).AcceptChanges()
                    Next
                End If

            End If
            Call FillOldApproverEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbInfo", Me.lblDetialHeader.Text)

            Me.lblNewLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewLevel.ID, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNewApprover.ID, Me.lblNewApprover.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")

            Me.dgOldApproverEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgOldApproverEmp.Columns(1).FooterText, Me.dgOldApproverEmp.Columns(1).HeaderText)
            Me.dgOldApproverEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgOldApproverEmp.Columns(2).FooterText, Me.dgOldApproverEmp.Columns(2).HeaderText)

            Me.dgNewApproverMigratedEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverMigratedEmp.Columns(1).FooterText, Me.dgNewApproverMigratedEmp.Columns(1).HeaderText)
            Me.dgNewApproverMigratedEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverMigratedEmp.Columns(2).FooterText, Me.dgNewApproverMigratedEmp.Columns(2).HeaderText)

            Me.dgNewApproverAssignEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverAssignEmp.Columns(0).FooterText, Me.dgNewApproverAssignEmp.Columns(0).HeaderText)
            Me.dgNewApproverAssignEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgNewApproverAssignEmp.Columns(1).FooterText, Me.dgNewApproverAssignEmp.Columns(1).HeaderText)


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
