﻿<%@ Page Title="Training Approver Migration" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_TrainingApproverMigration.aspx.vb" Inherits="Training_Training_Approver_Migration_wPg_TrainingApproverMigration" %>

<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            RetriveTab();

        });
        function RetriveTab() {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "MigratedEmployee";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        }

        $("body").on("click", "[id*=ChkOldAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvOldSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkgvOldSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkOldAll]", grid);
            debugger;
            if ($("[id*=ChkgvOldSelect]", grid).length == $("[id*=ChkgvOldSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        $("body").on("click", "[id*=ChkNewAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvNewSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkgvNewSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkNewAll]", grid);
            debugger;
            if ($("[id*=ChkgvNewSelect]", grid).length == $("[id*=ChkgvNewSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        function FromSearching(txtID, gridClientID, chkID, chkGvID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                if (chkID != null && $$(chkID).is(':checked') == true) {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
                }
                else {
                    $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
                }

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID, chkID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {


            $('#' + txtID).val('');
            //$('#' + gridClientID + ' tr').show();
            //$('.norecords').remove();
            if (chkID != null && $$(chkID).is(':checked') == true) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
                $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
            }
            else {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
                $('.norecords').remove();
            }
            $('#' + txtID).focus();
        }
    </script>

    <asp:Panel ID="pnlmain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Approver Migration" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Training Approver Information"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar" Width="100%"
                                                            CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingCalendar" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrainingType" runat="server" Text="Training Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrainingType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix  d--f ai--c jc--c">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 as--fs">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOldApprover" runat="server" Text="From Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboOldApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOldLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboOldLevel" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" id="txtFrmSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                    maxlength="50" onkeyup="FromSearching('txtFrmSearch', '<%= dgOldApproverEmp.ClientID %>');"
                                                                    class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-15">
                                                        <div style="float: left;">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations">
                                                                <i class="fas fa-sliders-h"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                        <div style="float: right;">
                                                            <asp:LinkButton ID="lnkReset" runat="server" ToolTip="Reset">
                                                                 <i class="fas fa-redo-alt"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 400px;">
                                                            <asp:GridView ID="dgOldApproverEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                ShowFooter="False" AllowPaging="false" DataKeyNames="approverunkid,approvertranunkid,approverempunkid,employeeunkid,employeecode,employeename">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-CssClass="align-center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkOldAll" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvOldSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgEmployeecode" />
                                                                    <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgEmployee" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12  text-center">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Button ID="objbtnAssign" runat="server" Text=">>" CssClass="btn btn-primary" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12  as--fs">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblNewApprover" runat="server" Text="To Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboNewApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblNewLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboNewLevel" runat="server" AutoPostBack="True" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 450px;">
                                                            <ul class="nav nav-tabs" role="tablist" id="Tabs">
                                                                <li role="presentation" class="active"><a href="#MigratedEmployee" data-toggle="tab">
                                                                    Migrated Employee </a></li>
                                                                <li role="presentation"><a href="#AssignedEmployee" data-toggle="tab">Assigned Employee
                                                                </a></li>
                                                            </ul>
                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane fade in active" id="MigratedEmployee">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="text" id="txtToSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                                maxlength="50" onkeyup="FromSearching('txtToSearch', '<%= dgNewApproverMigratedEmp.ClientID %>');"
                                                                                class="form-control" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="table-responsive" style="max-height: 450px;">
                                                                        <asp:GridView ID="dgNewApproverMigratedEmp" runat="server" AutoGenerateColumns="False"
                                                                            ShowFooter="False" AllowPaging="false" CssClass="table table-hover table-bordered"
                                                                            DataKeyNames="approverunkid,approvertranunkid,approverempunkid,employeeunkid,employeecode,employeename">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-CssClass="align-center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="ChkNewAll" runat="server" Text=" " CssClass="chk-sm" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="ChkgvNewSelect" runat="server" Text=" " CssClass="chk-sm" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="colhdgMigratedEmpCode" />
                                                                                <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="colhdgMigratedEmp" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane fade" id="AssignedEmployee">
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="form-line">
                                                                                    <input type="text" id="txtAssignSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                                        maxlength="50" onkeyup="FromSearching('txtAssignSearch', '<%= dgNewApproverAssignEmp.ClientID %>');"
                                                                                        class="form-control" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row clearfix">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 450px;">
                                                                                <asp:DataGrid ID="dgNewApproverAssignEmp" runat="server" AutoGenerateColumns="False"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered">
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="True"
                                                                                            FooterText="colhdgAssignEmpCode">
                                                                                            <HeaderStyle Width="30%" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhdgAssignEmployee">
                                                                                            <HeaderStyle Width="70%" />
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="True"
                                                                                            Visible="False" />
                                                                                    </Columns>
                                                                                    <HeaderStyle Font-Bold="False" />
                                                                                    <ItemStyle />
                                                                                </asp:DataGrid>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
