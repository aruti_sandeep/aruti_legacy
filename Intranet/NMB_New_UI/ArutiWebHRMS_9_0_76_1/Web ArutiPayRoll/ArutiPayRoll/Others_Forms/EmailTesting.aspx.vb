﻿Imports Aruti.Data
Imports System.Data
Imports System.Linq

Partial Class Others_Forms_EmailTesting
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmResolutionStep"
    Private DisplayMessage As New CommonCodes
    Private objCompany As New clsCompany_Master
    Private objUserAddEdit As New clsUserAddEdit
    Public Shared mdtTran As DataTable

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                objCompany._Companyunkid = CInt(Session("CompanyUnkId").ToString())
                txtUsername.Text = objCompany._Username
                txtIp.Text = objCompany._Mailserverip
                txtPort.Text = CStr(IIf(CInt(objCompany._Mailserverport) = 0, 25, CStr(objCompany._Mailserverport)))
                chkssl.Checked = objCompany._Isloginssl
                chkUseCertificate.Checked = objCompany._IsCertificateAuthorization
                Dim StrSearching As String = ""
                FillList()
            Else
                If ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(ViewState("mdtTran"), DataTable)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub FillList()
        Try
            mdtTran = objUserAddEdit.GetList("User", "").Tables(0)
            If mdtTran.Columns.Contains("IsCheck") = False Then
                Dim dccolumn As New DataColumn("IsCheck")
                dccolumn.DataType = Type.GetType("System.Boolean")
                dccolumn.DefaultValue = False

                mdtTran.Columns.Add(dccolumn)
            End If

            Dim dv As DataView = mdtTran.DefaultView()
            mdtTran = dv.ToTable("dtUser", True, "username", "email", "userunkid", "IsCheck", "firstname", "lastname")
            gvUserList.DataSource = mdtTran
            gvUserList.DataBind()
            lblCount.Text = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Fill()

    End Sub

    Public Shared Function modifyRow(ByVal dr As DataRow) As Boolean

        If IsNothing(dr) = False Then
            dr("isCheck") = True
            dr.AcceptChanges()
        End If
        Return True

    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mdtTran") = mdtTran
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAddEditSelectEmpChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(mdtTran, DataTable).Select("userunkid = '" & gvUserList.DataKeys(gvRow.RowIndex)("userunkid").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
            CheckAll(gvUserList, mdtTran, "ChkAllSelectedEmp", "ischeck")
            lblCount.Text = mdtTran.Select("isCheck=True").Count.ToString()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub ChkAllSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvUserList.Rows.Count <= 0 Then Exit Sub
            Dim dtSelectedEmp As DataTable = CType(mdtTran, DataTable)
            If gvUserList.Rows.Count > 0 Then

                For i As Integer = 0 To gvUserList.Rows.Count - 1
                    If dtSelectedEmp.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = dtSelectedEmp.Select("userunkid = '" & gvUserList.DataKeys(i)("userunkid").ToString().Trim & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = gvUserList.Rows(i)
                        CType(gvRow.FindControl("ChkSelectedEmp"), CheckBox).Checked = cb.Checked
                    End If
                    dtSelectedEmp.AcceptChanges()
                Next
                mdtTran = dtSelectedEmp

                lblCount.Text = mdtTran.Select("isCheck=True").Count.ToString()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CheckAll(ByVal gv As GridView, ByVal dt As DataTable, ByVal chkid As String, ByVal filter As String)
        Try
            Dim head As GridViewRow = gv.HeaderRow
            Dim chkall As CheckBox = TryCast(head.FindControl(chkid), CheckBox)
            If gv.Rows.Count <> dt.Select(filter + "=True").Length Then
                chkall.Checked = False
            ElseIf gv.Rows.Count = dt.Select(filter + "=True").Length Then
                chkall.Checked = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim grow As IEnumerable(Of GridViewRow) = Nothing
            grow = gvUserList.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True)
            Dim objSendMail As New clsSendMail

            If IsNothing(grow) = False AndAlso grow.Count() > 0 Then

                Dim StartTime As DateTime
                StartTime = DateTime.Now

                For Each r As GridViewRow In grow
                    Dim StrMessage As New System.Text.StringBuilder

                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                    Dim mstrUsername As String = CType(r.FindControl("hfFirstName"), HiddenField).Value + " " + CType(r.FindControl("hfLastName"), HiddenField).Value
                    If mstrUsername.Trim().Length <= 0 Then
                        mstrUsername = gvUserList.Rows(r.RowIndex).Cells(getColumnID_Griview(gvUserList, "dgcolhUName", False)).Text
                    End If

                    If gvUserList.Rows(r.RowIndex).Cells(getColumnID_Griview(gvUserList, "dgcolhUEmail", False)).Text.Length <= 0 Then Continue For

                    StrMessage.Append("Dear" & " " & "<b>" & getTitleCase(mstrUsername) & "</b></span></p>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                    If txtMsg.Text.Trim.Length > 0 Then
                        StrMessage.Append(txtMsg.Text & " </span></p>")
                    Else
                        StrMessage.Append("This is test Email, Please do not reply." & " </span></p>")
                    End If

                    objSendMail._ToEmail = gvUserList.Rows(r.RowIndex).Cells(getColumnID_Griview(gvUserList, "dgcolhUEmail", False)).Text.Trim()
                    objSendMail._Subject = "Test"
                    objSendMail._Message = StrMessage.ToString()
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._SenderAddress = txtUsername.Text
                    objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                Next
                DisplayMessage.DisplayMessage("Send Mail SuccessFully" & "\n\n" & "Estimate Time : " & DateDiff(DateInterval.Second, StartTime, DateTime.Now).ToString() & " Seconds", Me)
                FillList()
                txtMsg.Text = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub





End Class
