﻿Imports Microsoft.VisualBasic
Imports System.Exception
Imports System.Web.Services 'Sohail (04 Jun 2012)
Imports Aruti.Data
Imports System.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.DirectoryServices
Imports System.Web.Script.Serialization
Imports System.IO.Packaging
Imports System.IO
Imports System.DirectoryServices.Protocols
Imports System.Net
Imports System.Linq.Expressions

Public Class Basepage
    Inherits System.Web.UI.Page

    Public Const m_NoLicenceMsg As String = "Sorry, you are not registered to use this module. Please contact Aruti support team to register this module." 'Sohail (27 Apr 2013)

    'Sohail (23 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    Public Const m_CustomErrorMsg As String = "Something went wrong, Please contact administrator!"
    'Sohail (23 Mar 2019) -- End

    Dim DisplayMessage As New CommonCodes 'Sohail (07 Jun 2013)

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP |19-JAN-2022| -- START
            'ISSUE : STOPPING IE
            Dim blnIsIE As Boolean = False
            blnIsIE = IsOldIE()
            If blnIsIE Then
                DisplayMessage.DisplayMessageIE("Sorry, It seems that you are using old browser. Please use Chrome/Edge/Firefox in order to operate.", Me)
                Exit Sub
            End If
            'S.SANDEEP |19-JAN-2022| -- END

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            If Me.IsLoginRequired Then
                If Session("clsuser") Is Nothing Then
                    'DisplayMessage.DisplayMessage("Sorry, Session is Expired. Please Re-Login again.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                    Exit Sub
                End If
            End If

            If Request.Url.GetLeftPart(UriPartial.Authority).ToLower() & Request.ApplicationPath.ToLower() & "/index.aspx" <> Request.Url.AbsoluteUri.ToLower() AndAlso Request.QueryString.ToString().Length <= 0 Then
                If Session("clsuser") IsNot Nothing AndAlso Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then
                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                        'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                        Exit Sub
                    End If
                Else
                    'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                    Exit Sub
                End If
            End If

            'Pinkal (24-Jun-2024) -- End


            GetUserPrivilegeDict()
            GetConfigPropsDict()

            If CType(Session("sessionexpired"), Boolean) = True Then
                Response.Write("<script>alert('Session Expired');</script>")
                Session("sessionexpired") = Nothing
            End If
            Session("mainfullurl") = Request.Url.OriginalString
            Session("issitelocal") = InStr(Session("mainfullurl"), "localhost") > 0
            If Session("LoginBy") IsNot Nothing Then
                Call SetDateFormat()
            End If

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try
    End Sub

    Protected Overloads Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        Try
            MyBase.OnPreInit(e)

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            If Me.IsLoginRequired Then
                If Session("clsuser") Is Nothing Then
                    Context.ApplicationInstance.CompleteRequest()
                    'DisplayMessage.DisplayMessage("Sorry, Session is Expired. Please Re-Login again.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                    Exit Sub
                End If
            End If
            'Pinkal (24-Jun-2024) -- End

            If Session("cur_theme") Is Nothing Then
                Session.Add("cur_theme", System.Configuration.ConfigurationManager.AppSettings("DefaultTheme"))
                'Page.Theme = DirectCast(Session("cur_theme"), String)
            Else
                'Page.Theme = DirectCast(Session("cur_theme"), String)
            End If
            If Request.ServerVariables("http_user_agent").IndexOf("Safari", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
                Page.ClientTarget = "uplevel"
            End If
            'Page.Theme = Session("Theme")
            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1 'Sohail (17 Dec 2018) - Commented to prevent duplicate header
            'Response.AppendHeader("X-Frame-Options", "DENY")
            Response.AppendHeader("X-XSS-Protection", "1; mode=block")
            'Sohail (13 Dec 2018) -- End

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            If Request.Url.GetLeftPart(UriPartial.Authority).ToLower() & Request.ApplicationPath.ToLower() & "/index.aspx" <> Request.Url.AbsoluteUri.ToLower() AndAlso Request.QueryString.ToString().Length <= 0 Then
                If Session("clsuser") IsNot Nothing AndAlso Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then
                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                        'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                        Exit Sub
                    End If
                Else
                    'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                    Exit Sub
                End If
            End If
            'Pinkal (24-Jun-2024) -- End


        Catch ex As Exception
            'Throw New Exception("Basepage->OnPreInit event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try


    End Sub

    Protected Overloads Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try
            'S.SANDEEP |19-JAN-2022| -- START
            'ISSUE : STOPPING IE
            Dim blnIsIE As Boolean = False
            blnIsIE = IsOldIE()
            If blnIsIE Then
                DisplayMessage.DisplayMessageIE("Sorry, It seems that you are using old browser. Please use Chrome/Edge/Firefox in order to operate.", Me)
                Exit Sub
            End If
            'S.SANDEEP |19-JAN-2022| -- END

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            If Me.IsLoginRequired Then
                If Session("clsuser") Is Nothing Then
                    'DisplayMessage.DisplayMessage("Sorry, Session is Expired. Please Re-Login again.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                    Exit Sub
                End If
            End If


            If Request.Url.GetLeftPart(UriPartial.Authority).ToLower() & Request.ApplicationPath.ToLower() & "/index.aspx" <> Request.Url.AbsoluteUri.ToLower() AndAlso Request.QueryString.ToString().Length <= 0 Then
                If Session("clsuser") IsNot Nothing AndAlso Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then
                    If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                        'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                        Exit Sub
                    End If
                Else
                    'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/sessionexpire.aspx", True)
                    Exit Sub
                End If
            End If
            'Pinkal (24-Jun-2024) -- End


            If Session("LoginBy") IsNot Nothing Then
                Call SetDateFormat()
            End If

            GetUserPrivilegeDict()
            GetConfigPropsDict()

            'Hemant (01 Sep 2023) -- Start
            Dim arrPages() As String = {"index.aspx", "userhome.aspx", "changepassword.aspx"}
            Dim arrAboutMePages() As String = {"wpgemployeedetail.aspx" _
                                                , "wpgemployeepdp.aspx" _
                                                , "wpgemployeepipformlist.aspx" _
                                                , "wpgemployeepipform.aspx" _
                                                , "wpg_payslipreport.aspx" _
                                                , "wpgleavedetails.aspx" _
                                                , "wpgdailyattendanceoperation.aspx" _
                                                , "otrequisition.aspx" _
                                                , "otsubmitforapproval.aspx" _
                                                , "ot_requisitionapproval.aspx" _
                                                , "wpg_loanapplicationlist.aspx" _
                                                , "wpg_addeditloanapplication.aspx" _
                                                , "wpg_addeditflexcubeloanapplication.aspx" _
                                                , "wpg_flexcubeloanbalance.aspx" _
                                                , "wpg_newloanadvancelist.aspx" _
                                                , "wpg_loanapprovallist.aspx" _
                                                , "wpgemployeelvlgoalslist.aspx" _
                                                , "wpgessupdategoalprogress.aspx" _
                                                , "wpg_customeitemview.aspx" _
                                                , "wpgperformance_planning.aspx" _
                                                , "wpg_selfevaluationlist.aspx" _
                                                , "wpg_viewperfevaluation.aspx" _
                                                , "wpg_viewfinalrating.aspx" _
                                                , "wpg_trainingrequestformlist.aspx" _
                                                , "wpg_trainingrequestform.aspx" _
                                                , "wpg_trainingscheduleview.aspx" _
                                                , "wpg_training_evalution_form.aspx" _
                                                , "wpg_claimandrequestlist.aspx" _
                                                , "wpg_expenseapprovallist.aspx" _
                                                , "wpg_claimretirementlist.aspx" _
                                                , "wpg_claimretirement_approvallist.aspx" _
                                                , "employeegrievance.aspx" _
                                                , "grievanceinitiatorresponselist.aspx" _
                                                , "grievanceresolutionstatus.aspx" _
                                                , "wpg_employeeassetdeclarationlistt2.aspx" _
                                                , "wpg_empnondisclosure_declaration.aspx" _
                                                , "searchjob.aspx" _
                                                , "applicantappliedjob.aspx" _
                                                , "wpg_scanorattachmentinfo.aspx" _
                                                , "wpgempqualificationlist.aspx" _
                                                , "wpgempqualification.aspx" _
                                                , "wpg_employeeskilllist.aspx" _
                                                , "wpg_employeeskill.aspx" _
                                                , "wpgempidentities_list.aspx" _
                                                , "wpgempidentities.aspx" _
                                                , "wpgempmembership_list.aspx" _
                                                , "wpgempmemberships.aspx" _
                                                , "wpg_employeeexperiencelist.aspx" _
                                                , "wpg_employeeexperience.aspx" _
                                                , "wpgemprefereelist.aspx" _
                                                , "wpgempreferee.aspx" _
                                                , "wpgbenefitlist.aspx" _
                                                , "wpg_assetsregisterlist.aspx" _
                                                , "wpg_employeeasset.aspx" _
                                                , "wpgdependantslist.aspx" _
                                                , "wpgempdepentants.aspx" _
                                                , "wpgdependantstatus.aspx" _
                                                , "wpgemployeeaddress.aspx" _
                                                , "wpg_talentprofile.aspx" _
                                                , "wpg_successionprofile.aspx" _
                                                , "wpg_employeetimesheet.aspx" _
                                                , "wpg_submitforapproval.aspx" _
                                                , "wpg_employeetimesheetapprovallist.aspx" _
                                                , "wpg_addeditclaimandrequestlist.aspx" _
                                                , "wpgclaimretirementinformation.aspx" _
                                                , "agreedisagreeresponse.aspx" _
                                                , "wpgemployeeupdate.aspx" _
                                                , "wpgemployeepersonal.aspx" _
                                                , "wpgemployeeevaluation.aspx" _
                                                , "wpg_leaveformlist.aspx" _
                                                , "wpg_leaveformaddedit.aspx" _
                                                , "wpg_processleavelist.aspx" _
                                                , "leaveviewer.aspx" _
                                                , "wpg_leaveplannerlist.aspx" _
                                                , "wpg_leaveplanner_addedit.aspx" _
                                                , "leaveplannerviewer.aspx" _
                                                , "wpgstafftransferrequestlist.aspx" _
                                                , "wpgstafftransferrequest.aspx" _
                                                , "wpg_loantranchelist.aspx" _
                                                , "wpg_addeditloantranche.aspx" _
                                                , "wpg_payrollreport.aspx" _
                                                , "wpg_eddetailreport.aspx" _
                                                , "rpt_p9a_kenya.aspx" _
                                                , "rpt_payedeductionsformp9report.aspx" _
                                                , "rpt_loan_advance_saving.aspx" _
                                                , "rpt_employee_cv.aspx" _
                                                , "rpt_employeeleavebalancereport.aspx" _
                                                , "rpt_leavestatementreport.aspx" _
                                                , "rpt_employeeleaveformreport.aspx" _
                                                , "rpt_leaveplannerreport.aspx" _
                                                , "rpt_lvform_approvalstatus_report.aspx" _
                                                , "rpt_issuedleave_status_report.aspx" _
                                                , "rpt_employee_assessment_form.aspx" _
                                                , "rpt_assessmentdonereport.aspx" _
                                                , "rpt_employeetimesheetreport.aspx" _
                                                , "rpt_traearlygoingreport.aspx" _
                                                , "rpt_traearlygoingreport.aspx" _
                                                , "rpt_traempmispunchreport.aspx" _
                                                , "rpt_traabsenteesexcusereport.aspx" _
                                                , "rpt_budget_employee_timesheet.aspx" _
                                                , "rpt_emptimesheetprojectsummary.aspx" _
                                                , "rpt_empprojectdescriptionreport.aspx" _
                                                , "rpt_ot_requisition_report.aspx" _
                                                , "rpt_ot_requisition_summaryreport.aspx" _
                                                , "rpt_employeeot_requisition_report.aspx" _
                                                , "rpt_employeeclaimform.aspx" _
                                                , "rpt_trainingrequestformreport.aspx" _
                                                , "rpt_trainingfeedbackformreport.aspx" _
                                                , "rpt_empbreaktimesheetreport.aspx" _
                                                , "rpt_daily_time_sheet.aspx" _
                                                , "rpt_flexcubeloanofferletter.aspx" _
                                                , "rpt_flexcubeloanformreport.aspx" _
                                                , "wpg_disciplinecharge_add_edit.aspx" _
                                                , "wpgemployeepipform.aspx" _
                                                , "wpgemployeepipformlist.aspx" _
                                                , "rpt_pipformreport.aspx" _
                                                , "wpgempemeraddress.aspx" _
                                                , "wpg_loanadvanceoperationlist.aspx" _
                                                , "wpg_employeeattestationdeclaration.aspx" _
                                                , "rpt_credit_card_report.aspx" _
                                                }
            'Pinkal (27-Sep-2024) -- [rpt_credit_card_report.aspx]
            'Hemant (21 Jun 2024) -- [wPg_EmployeeAttestationDeclaration.aspx]
            'Pinkal (17-May-2024) --[ "wpg_loanadvanceoperationlist.aspx" ]
            'Pinkal (23-Feb-2024) --(A1X-2461) NMB : R&D - Force manual user login on approval links.[ "wpgempemeraddress.aspx" ]

            If Array.IndexOf(arrPages, Path.GetFileName(Request.Url.AbsoluteUri).ToLower()) = -1 Then
                If Session("LoginBy") = Global.User.en_loginby.Employee AndAlso (Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0) Then
                    If Session("LoginBy") = Global.User.en_loginby.Employee AndAlso Array.IndexOf(arrAboutMePages, Path.GetFileName(Request.Url.GetLeftPart(UriPartial.Path)).ToLower()) = -1 Then
                        DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If
                End If
            End If
            'Hemant (01 Sep 2023) -- End

            If ArtLic._Object Is Nothing Then ArtLic._Object = New ArutiLic(False) 'Sohail (27 Apr 2013)
            MyBase.OnLoad(e)
        Catch ex As Exception
            'Throw New Exception("Basepage->OnLoad event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try


    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Try
            Dim strError As String = Server.GetLastError.Message & "; " & Server.GetLastError.StackTrace
            DisplayMessage.DisplayMessage(strError, Me.Page)
            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
        Catch vsEx As ViewStateException
            Server.ClearError()
            'Pinkal (27-Sep-2024) -- End
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try

    End Sub

    'Sohail (22 Feb 2021) -- Start
    'NMB Enhancement :  : Disable all button controls if AMC is pending.
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Try

            If Request.Url.OriginalString.ToLower().Contains("index.aspx") Then
                Exit Sub
            End If
            'If Me.IsLoginRequired = True Then
            'S.SANDEEP |03-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : AMS-AMC
            If Session("IsAMCPending") Is Nothing Then
                Session("IsAMCPending") = clsMasterData.IsAMCPending()
            End If
            If Session("menuView") Is Nothing Then
                Session("menuView") = 0
            End If
            'S.SANDEEP |03-MAR-2021| -- END

            If Session("menuView") = 0 Then
                'If ConfigParameter._Object._IsArutiDemo = False AndAlso Session("IsAMCPending") = True Then
                If Session("IsAMCPending") = True Then
                    Dim allButtons As List(Of Button) = (From p In Me.Controls.OfType(Of Button)() Select (p)).ToList
                    For Each ctrl As Button In allButtons
                        ctrl.Enabled = False
                    Next

                    Dim allExceptButtons As List(Of Control) = (From p In Me.Controls.OfType(Of Control)() Where (Not TypeOf (p) Is Button) Select (p)).ToList
                    For Each ctrl As Control In allExceptButtons
                        If ctrl.Controls.Count > 0 Then
                            For Each c As Control In ctrl.Controls
                                Call DisableAllButtonControls(c)
                            Next
                        End If
                    Next
                End If
            End If
            'End If
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally
            'S.SANDEEP |03-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : AMS-AMC
            Dim objConfig As New clsConfigOptions
            clsMasterData.UpdateLastUsed(objConfig._CurrentDateAndTime, "")
            objConfig = Nothing
            'S.SANDEEP |03-MAR-2021| -- END

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            HttpContext.Current.ApplicationInstance.CompleteRequest()
            'Pinkal (27-Sep-2024) -- End
        End Try
    End Sub

    Private Function DisableAllButtonControls(ByVal parentctrl As Control) As Boolean
        Try
            If TypeOf (parentctrl) Is Button Then
                CType(parentctrl, Button).Enabled = False

            ElseIf TypeOf (parentctrl) Is FileUpload Then
                CType(parentctrl, FileUpload).Enabled = False

            ElseIf TypeOf (parentctrl) Is HtmlInputButton Then
                CType(parentctrl, HtmlInputButton).Attributes.Add("disabled", "disabled")
                CType(parentctrl, HtmlInputButton).Visible = False

            ElseIf TypeOf (parentctrl) Is GridView Then
                CType(parentctrl, GridView).Enabled = False

            ElseIf TypeOf (parentctrl) Is DataGrid Then
                CType(parentctrl, DataGrid).Enabled = False

            Else
                Dim allgv As List(Of GridView) = (From p In parentctrl.Controls.OfType(Of GridView)() Select (p)).ToList
                For Each ctrl As GridView In allgv
                    ctrl.Enabled = False
                Next

                Dim alldg As List(Of DataGrid) = (From p In parentctrl.Controls.OfType(Of DataGrid)() Select (p)).ToList
                For Each ctrl As DataGrid In alldg
                    ctrl.Enabled = False
                Next

                Dim allLinkButton As List(Of LinkButton) = (From p In parentctrl.Controls.OfType(Of LinkButton)() Select (p)).ToList
                For Each ctrl As LinkButton In allLinkButton
                    ctrl.Enabled = False
                Next

                Dim allButtons As List(Of Button) = (From p In parentctrl.Controls.OfType(Of Button)() Select (p)).ToList
                For Each ctrl As Button In allButtons
                    If ctrl.ID.ToLower().Contains("search") Or ctrl.ID.ToLower().Contains("reset") Or ctrl.ID.ToLower().Contains("close") Then
                    Else
                        ctrl.Enabled = False
                    End If
                Next

                Dim allFileUpload As List(Of FileUpload) = (From p In parentctrl.Controls.OfType(Of FileUpload)() Select (p)).ToList
                For Each ctrl As FileUpload In allFileUpload
                    ctrl.Enabled = False
                Next

                Dim allHtmlInputButton As List(Of HtmlInputButton) = (From p In parentctrl.Controls.OfType(Of HtmlInputButton)() Select (p)).ToList
                For Each ctrl As HtmlInputButton In allHtmlInputButton
                    ctrl.Attributes.Add("disabled", "disabled")
                    ctrl.Visible = False
                Next

                Dim allExceptButtons As List(Of Control) = (From p In parentctrl.Controls.OfType(Of Control)() Where (Not TypeOf (p) Is Button) Select (p)).ToList
                For Each ctrl As Control In allExceptButtons
                    If ctrl.Controls.Count > 0 Then
                        If ctrl.ID <> Nothing AndAlso ctrl.ID.ToLower = "pnlmenuwrapper" Then Continue For
                        For Each c As Control In ctrl.Controls
                            Call DisableAllButtonControls(c)
                        Next
                    End If
                Next
            End If


        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally
            'S.SANDEEP |03-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : AMS-AMC
            Dim objConfig As New clsConfigOptions
            clsMasterData.UpdateLastUsed(objConfig._CurrentDateAndTime, "")
            objConfig = Nothing
            'S.SANDEEP |03-MAR-2021| -- END 
        End Try
    End Function
    'Sohail (22 Feb 2021) -- End


    Private pIsLoginRequired As Boolean
    Public Property IsLoginRequired() As Boolean
        Get
            Return pIsLoginRequired
        End Get
        Set(ByVal value As Boolean)
            pIsLoginRequired = value
        End Set
    End Property

    'Sohail (20 Feb 2015) -- Start
    Public Sub GetDatabaseVersion()
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim ds As System.Data.DataSet
        Dim strQ As String
        Try
            strQ = "EXEC hrmsConfiguration..version "

            ds = clsDataOpr.WExecQuery(strQ, "List")

            If ds.Tables(0).Rows.Count > 0 Then
                Session("DatabaseVersion") = ds.Tables(0).Rows(0).Item(0).ToString
            Else
                Session("DatabaseVersion") = ""
            End If

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (20 Feb 2015) -- End

    'Sohail (04 Jun 2012) -- Start
    'TRA - ENHANCEMENT - View Online Memberes
    <WebMethod()> Public Shared Function UpdateUsers() As String
        Dim res As String = ""
        Try


            'lstMembers.Items.Clear()

            'For Each s As String In clsActiveUserEngine.GetUserName()
            '    lstMembers.Items.Add(New ListItem(s, s))
            'Next

            Dim users As IEnumerable(Of String) = clsActiveUserEngine.GetUserName()
            SyncLock users
                For Each s In users
                    res += s.ToString.Replace(vbCrLf, " ") & ","
                Next
            End SyncLock

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("UpdateUsers", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
        Return res
    End Function

    <WebMethod()> Public Shared Function Joined(ByVal strUserId As String, ByVal strUserName As String, ByVal enUserType As clsActiveUsers.enUserType) As String
        Try
            clsActiveUserEngine.Joined(strUserId, strUserName, enUserType)

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("Joined", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
        Return ""
    End Function

    <WebMethod()> Public Shared Function Leaved(ByVal strUserId As String) As String
        Try
            clsActiveUserEngine.Leaved(strUserId)

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("Leaved", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
        Return ""
    End Function
    'Sohail (04 Jun 2012) -- End

    'Sohail (06 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    <WebMethod()> Public Shared Sub LogOut()

        'Pinkal (03-Sep-2012) -- Start
        'Enhancement : TRA Changes

        'If HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee Then
        '    Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")))
        'ElseIf HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then
        '    Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")))
        'End If
        Try
            If HttpContext.Current.Session("LoginBy") Is Nothing Then
            ElseIf HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee Then
                Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            ElseIf HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then
                Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            End If

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("LogOut", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End

        'HttpContext.Current.Session("LoginBy") = Nothing 'Sohail (08 May 2014)
        'Pinkal (03-Sep-2012) -- End

    End Sub
    'Sohail (06 Jun 2012) -- End

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    <WebMethod()> Public Shared Sub SectionClicked(ByVal intMenuView As Integer, ByVal strImgAcc As String)
        Try
            If HttpContext.Current.Session("Database_Name") Is Nothing Then Exit Sub 'Sohail (24 Jan 2022)

            HttpContext.Current.Session("menuView") = intMenuView
            HttpContext.Current.Session("imgAcc") = strImgAcc
            HttpContext.Current.Session("Lastview_id") = intMenuView 'Sohail (23 Dec 2020)
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            UpdateLastViewID(intMenuView)
            'Sohail (24 Nov 2016) -- End

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("SectionClicked", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub

    <WebMethod()> Public Shared Sub SetIsAuthorized(ByVal blnIsAuthorized As Boolean)
        HttpContext.Current.Session("IsAuthorized") = blnIsAuthorized
    End Sub
    'Sohail (18 May 2013) -- End
    'Hemant (29 Dec 2018) -- Start
    'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
    <WebMethod()> Public Shared Sub SetReturnURLFalse()
        HttpContext.Current.Session("ReturnURL") = Nothing
    End Sub
    'Hemant (29 Dec 2018) -- End
    'Sohail (15 Dec 2020) -- Start
    <WebMethod()> Public Shared Sub SetAttachmentSession(ByVal strScanAttchLableCaption As String, ByVal intScanAttchRefModuleID As Integer, ByVal intScanAttchenAction As Integer, ByVal strScanAttchToEditIDs As String)
        Try
            HttpContext.Current.Session("ScanAttchLableCaption") = strScanAttchLableCaption ' "Select Employee"
            HttpContext.Current.Session("ScanAttchRefModuleID") = intScanAttchRefModuleID ' enImg_Email_RefId.Applicant_Job_Vacancy
            HttpContext.Current.Session("ScanAttchenAction") = intScanAttchenAction 'enAction.ADD_ONE
            HttpContext.Current.Session("ScanAttchToEditIDs") = strScanAttchToEditIDs '""
        Catch ex As Exception

        End Try
    End Sub

    <WebMethod()> Public Shared Sub SetActiveMenuID(ByVal strActiveMenuID As String)
        Try
            HttpContext.Current.Session("IsFromDashBoard") = False 'Sandeep |09-DEC-2021| -- START {SESSION UNLOAD ISSUE FIX} -- END
            HttpContext.Current.Session("ActiveMenuID") = strActiveMenuID
        Catch ex As Exception

        End Try
    End Sub

    <WebMethod()> Public Shared Sub SetVisibiltyMenu(ByVal blnMenuVisible As Boolean)
        Try
            HttpContext.Current.Session("MenuVisible") = blnMenuVisible
        Catch ex As Exception

        End Try
    End Sub

    <WebMethod()> Public Shared Sub SetTheme(ByVal strTheme As String)
        Try
            HttpContext.Current.Session("Theme") = strTheme
            Select Case strTheme
                Case "brown"
                    Basepage.UpdateThemeID(enSelfServiceTheme.Brown)
                Case Else
                    Basepage.UpdateThemeID(enSelfServiceTheme.Blue)
            End Select

        Catch ex As Exception

        End Try
    End Sub

    'Sohail (15 Dec 2020) -- End

    'Sohail (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    <WebMethod()> Public Shared Sub SetIsDeptTrainingFromBacklog(ByVal intFormId As Integer)
        HttpContext.Current.Session("intFormId") = intFormId
    End Sub
    'Sohail (26 Mar 2021) -- End

    'Sohail (12 Mar 2015) -- Start
    'ENHANCEMENT
    <WebMethod()> Public Shared Sub SetFavourites(ByVal blnFavourite As Boolean, ByVal strMenuID As String)
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim ds As System.Data.DataSet
        Dim strQ As String
        Dim strMenuFilter As String = ""
        Dim intMenuID As Integer = 0
        Dim intReportID As Integer = 0

        Try

            Dim intFavourite As Integer = CInt(Int(blnFavourite))

            If strMenuID.Substring(0, 4).ToLower = "menu" Then
                strMenuFilter &= " AND menuunkid = " & CInt(strMenuID.Substring(5)) & " "
                intMenuID = CInt(strMenuID.Substring(5))
            Else
                strMenuFilter &= " AND reportunkid = " & CInt(strMenuID.Substring(5)) & " "
                intReportID = CInt(strMenuID.Substring(5))
            End If


            If HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then

                strQ = "IF NOT EXISTS ( SELECT  usermenuunkid " & _
                                    "FROM    hrmsConfiguration..cfuser_menu " & _
                                    "WHERE   isactive = 1 " & _
                                            "AND userunkid = " & CInt(HttpContext.Current.Session("UserId")) & " " & _
                                            " " & strMenuFilter & " ) " & _
                        "BEGIN " & _
                            "INSERT  INTO hrmsConfiguration..cfuser_menu " & _
                                    "( userunkid  " & _
                                    ", employeeunkid " & _
                                    ", menuunkid " & _
                                    ", reportunkid " & _
                                    ", isfavourite " & _
                                    ", isactive " & _
                                    ") " & _
                            "VALUES  ( " & CInt(HttpContext.Current.Session("UserId")) & "  " & _
                                    ", 0 " & _
                                    ", " & intMenuID & " " & _
                                    ", " & intReportID & " " & _
                                    ", " & intFavourite & " " & _
                                    ", 1 " & _
                                    ") " & _
                        "END " & _
                    "ELSE " & _
                        "BEGIN " & _
                            "UPDATE  hrmsConfiguration..cfuser_menu " & _
                            "SET     isfavourite = " & intFavourite & " " & _
                            "WHERE   isactive = 1 " & _
                                    "AND userunkid = " & CInt(HttpContext.Current.Session("UserId")) & " " & _
                                    " " & strMenuFilter & " " & _
                        "END "
            Else

                strQ = "IF NOT EXISTS ( SELECT  usermenuunkid " & _
                                    "FROM    hrmsConfiguration..cfuser_menu " & _
                                    "WHERE   isactive = 1 " & _
                                            "AND employeeunkid = " & CInt(HttpContext.Current.Session("Employeeunkid")) & " " & _
                                            " " & strMenuFilter & " ) " & _
                        "BEGIN " & _
                            "INSERT  INTO hrmsConfiguration..cfuser_menu " & _
                                    "( userunkid  " & _
                                    ", employeeunkid " & _
                                    ", menuunkid " & _
                                    ", reportunkid " & _
                                    ", isfavourite " & _
                                    ", isactive " & _
                                    ") " & _
                            "VALUES  ( 0  " & _
                                    ", " & CInt(HttpContext.Current.Session("Employeeunkid")) & " " & _
                                    ", " & intMenuID & " " & _
                                    ", " & intReportID & " " & _
                                    ", " & intFavourite & " " & _
                                    ", 1 " & _
                                    ") " & _
                        "END " & _
                    "ELSE " & _
                        "BEGIN " & _
                            "UPDATE  hrmsConfiguration..cfuser_menu " & _
                            "SET     isfavourite = " & intFavourite & " " & _
                            "WHERE   isactive = 1 " & _
                                    "AND employeeunkid = " & CInt(HttpContext.Current.Session("Employeeunkid")) & " " & _
                                    " " & strMenuFilter & " " & _
                        "END "

            End If

            ds = clsDataOpr.WExecQuery(strQ, "List")

            If clsDataOpr.ErrorMessage <> "" Then
                Throw New Exception(clsDataOpr.ErrorMessage)
            End If

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            InsertErrorLog("SetFavourites", ex)
            'Pinkal (13-Aug-2020) -- End
        End Try

    End Sub
    'Sohail (12 Mar 2015) -- End

    <WebMethod()> Public Shared Function MSS() As String
        Dim base As New Basepage
        Dim strError As String = ""
        Try
            If HttpContext.Current.Session("Database_Name") Is Nothing Then Return "" 'Sohail (24 Jan 2022)


            'Pinkal (01-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            If HttpContext.Current.Session("U_UserID") Is Nothing Then Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"
            'Pinkal (01-Feb-2022) -- End


            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            If HttpContext.Current.Session("LoginBy").ToString().ToUpper() = "USER" Then Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"
            If CInt(HttpContext.Current.Session("U_UserID")) <= 0 Then Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"
            'Pinkal (25-Jan-2022) -- End

            If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.User, CInt(HttpContext.Current.Session("U_UserID"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page)
                Throw New Exception(strError)
                Exit Try
            End If

            'Sohail (23 Dec 2020) -- Start
            'NMB Enhancement : - Allow ESS and MSS links in same login without switching to ESS / MSS.
            'SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            'clsActiveUserEngine.Leaved("EMP" & HttpContext.Current.Session("Employeeunkid").ToString)
            'Sohail (23 Dec 2020) -- End


            HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
            HttpContext.Current.Session("LangId") = HttpContext.Current.Session("U_LangId")
            HttpContext.Current.Session("UserId") = HttpContext.Current.Session("U_UserID")
            HttpContext.Current.Session("Employeeunkid") = HttpContext.Current.Session("U_Employeeunkid")
            HttpContext.Current.Session("UserName") = HttpContext.Current.Session("U_UserName")
            HttpContext.Current.Session("Password") = HttpContext.Current.Session("U_Password")
            HttpContext.Current.Session("LeaveBalances") = HttpContext.Current.Session("U_LeaveBalances")
            HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("U_MemberName")
            HttpContext.Current.Session("RoleID") = HttpContext.Current.Session("U_RoleID")
            HttpContext.Current.Session("Firstname") = HttpContext.Current.Session("U_Firstname")
            HttpContext.Current.Session("Surname") = HttpContext.Current.Session("U_Surname")
            HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("U_DisplayName")
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            HttpContext.Current.Session("Theme_id") = HttpContext.Current.Session("U_Theme_id")
            HttpContext.Current.Session("Lastview_id") = HttpContext.Current.Session("U_Lastview_id")
            'Sohail (24 Nov 2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            HttpContext.Current.Session("Email") = HttpContext.Current.Session("U_Email")
            'S.SANDEEP |08-JAN-2019| -- END

            strError = ""
            If base.SetUserSessions(strError) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If

            strError = ""
            If base.SetCompanySessions(strError, CInt(HttpContext.Current.Session("CompanyUnkId")), CInt(HttpContext.Current.Session("LangId"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If


            'HttpContext.Current.Response.Redirect(HttpContext.Current.Session("rootpath") & "UserHome.aspx")
            Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            InsertErrorLog("MSS", ex)
            Return ""
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function

    <WebMethod()> Public Shared Function ESS() As String
        Dim base As New Basepage
        Dim strError As String = ""
        Try
            If HttpContext.Current.Session("Database_Name") Is Nothing Then Return "" 'Sohail (24 Jan 2022)


            'Pinkal (01-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            If HttpContext.Current.Session("E_Employeeunkid") Is Nothing Then Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"
            'Pinkal (01-Feb-2022) -- End

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            If HttpContext.Current.Session("LoginBy").ToString().ToUpper() = "EMPLOYEE" Then Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"
            If CInt(HttpContext.Current.Session("E_Employeeunkid")) <= 0 Then Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"
            'Pinkal (25-Jan-2022) -- End

            If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(HttpContext.Current.Session("E_Employeeunkid"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page)
                Throw New Exception(strError)
                Exit Try
            End If

            'Sohail (23 Dec 2020) -- Start
            'NMB Enhancement : - Allow ESS and MSS links in same login without switching to ESS / MSS.
            'SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            'clsActiveUserEngine.Leaved("USER" & HttpContext.Current.Session("UserId").ToString)
            'Sohail (23 Dec 2020) -- End

            HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
            HttpContext.Current.Session("LangId") = 1 'Session("E_LangId")
            HttpContext.Current.Session("UserId") = HttpContext.Current.Session("E_UserID")
            HttpContext.Current.Session("Employeeunkid") = HttpContext.Current.Session("E_Employeeunkid")
            HttpContext.Current.Session("UserName") = HttpContext.Current.Session("E_UserName")
            HttpContext.Current.Session("Password") = HttpContext.Current.Session("E_Password")
            HttpContext.Current.Session("LeaveBalances") = HttpContext.Current.Session("E_LeaveBalances")
            HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("E_MemberName")
            HttpContext.Current.Session("RoleID") = HttpContext.Current.Session("E_RoleID")
            HttpContext.Current.Session("Firstname") = HttpContext.Current.Session("E_Firstname")
            HttpContext.Current.Session("Surname") = HttpContext.Current.Session("E_Surname")
            HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("E_DisplayName")
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            HttpContext.Current.Session("Theme_id") = HttpContext.Current.Session("E_Theme_id")
            HttpContext.Current.Session("Lastview_id") = HttpContext.Current.Session("E_Lastview_id")
            'Sohail (24 Nov 2016) -- End

            strError = ""
            If base.SetUserSessions(strError) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If

            strError = ""
            If base.SetCompanySessions(strError, CInt(HttpContext.Current.Session("CompanyUnkId")), CInt(HttpContext.Current.Session("LangId"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If


            'HttpContext.Current.Response.Redirect(HttpContext.Current.Session("rootpath") & "UserHome.aspx")
            Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            InsertErrorLog("ESS", ex)
            Return ""
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function

    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    Public Function SetUserSessions(ByRef strErrorMsg As String, Optional ByRef blnPasswordExpired As Boolean = False) As Boolean
        Dim clsDataOpr As New clsDataOperation(True)
        Dim objGlobalAccess As New GlobalAccess
        Dim ds As DataSet
        Dim strQ As String = ""
        strErrorMsg = ""
        Try

            Session("sessionexpired") = False
            HttpContext.Current.Session("Login") = True
            Session("rootpath") = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath & "/"


            'Pinkal (01-Jun-2021)-- Start
            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            'If Session("LoginBy") IsNot Nothing Then
            '    If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '        clsActiveUserEngine.Leaved("USER" & Session("UserId").ToString)
            '    ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '        clsActiveUserEngine.Leaved("EMP" & Session("Employeeunkid").ToString)
            '    End If
            'End If
            'Pinkal (01-Jun-2021) -- End


            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            Dim objConfigOption As New clsConfigOptions
            Dim mstrAllowEmpSystemAccessOnActualEOCRetirementDate As String = ""
            Dim mblnAllowEmpSystemAccessOnActualEOCRetirementDate As Boolean = False
            objConfigOption.IsValue_Changed("AllowEmpSystemAccessOnActualEOCRetirementDate", "-999", mstrAllowEmpSystemAccessOnActualEOCRetirementDate)
            mblnAllowEmpSystemAccessOnActualEOCRetirementDate = CBool(IIf(mstrAllowEmpSystemAccessOnActualEOCRetirementDate.Length <= 0, False, mstrAllowEmpSystemAccessOnActualEOCRetirementDate))
            objConfigOption = Nothing
            'Pinkal (27-Nov-2020) -- End

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'strQ = " select employeeunkid,(isnull(Firstname,'') + ' ' + isnull(othername,' ') + ' ' + isnull(surname ,' '))  as loginname from " & Session("mdbname") & "..hremployee_master "
            strQ = "SELECT " & _
                        "   employeeunkid " & _
                        ",  ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,' ') + ' ' + ISNULL(surname ,' ')  AS loginname " & _
                   "FROM " & Session("mdbname") & "..hremployee_master "
            'Nilay (09-Aug-2016) -- End

            If Session("LoginBy") = Global.User.en_loginby.Employee Then

                Dim sName As String = String.Empty
                sName = Session("DisplayName")
                If sName.Contains("'") Then
                    sName = sName.Replace("'", "''")
                End If
                strQ &= "   WHERE displayname = '" & sName & "' " & vbCrLf

            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.

                If CInt(Session("AuthenticationMode")) = enAuthenticationMode.BASIC_AUTHENTICATION Then

                    Dim objUser As New clsUserAddEdit
                    Dim mintUserUnkid As Integer

                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                    If Session("UserName") IsNot Nothing AndAlso Session("Password") IsNot Nothing Then
                        mintUserUnkid = objUser.IsValidLogin(Session("UserName"), Session("Password"), enLoginMode.USER, "hrmsConfiguration", mblnAllowEmpSystemAccessOnActualEOCRetirementDate)
                    End If
                    'Pinkal (01-Mar-2016) -- End

                    If mintUserUnkid <= 0 AndAlso objUser._Message <> "" Then
                        If objUser._ShowChangePasswdfrm = True AndAlso objUser._RetUserId > 0 Then
                            Session("ExUId") = objUser._RetUserId
                            blnPasswordExpired = True
                            Exit Function
                        Else
                            strErrorMsg = objUser._Message
                            Exit Function
                        End If
                    End If
                    objUser = Nothing
                End If
                'Pinkal (03-Apr-2017) -- End

            End If

            strQ &= " order by loginname " & vbCrLf
            ds = clsDataOpr.WExecQuery(strQ, "cffinancial_year_tran")

            objGlobalAccess.ListOfEmployee = ds.Tables(0)

            If Session("LoginBy") = Global.User.en_loginby.User Then
                objGlobalAccess.userid = Session("UserId")
            Else
                objGlobalAccess.employeeid = Session("Employeeunkid")
            End If

            HttpContext.Current.Session("objGlobalAccess") = objGlobalAccess

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            If CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Black Then
                HttpContext.Current.Session("Theme") = "black"
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Green Then
                HttpContext.Current.Session("Theme") = "green"
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Yellow Then
                HttpContext.Current.Session("Theme") = "yellow"
                'Sohail (28 Mar 2018) -- Start
                'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = CInt(enSelfServiceTheme.Brown) Then
                HttpContext.Current.Session("Theme") = "brown"
                'Sohail (28 Mar 2018) -- End
            Else
                HttpContext.Current.Session("Theme") = "blue"
            End If

            Session("menuView") = CInt(HttpContext.Current.Session("Lastview_id"))
            'Sohail (24 Nov 2016) -- End

            gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
            'If gobjLocalization Is Nothing Then
            '    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
            '    'Pinkal (09-Aug-2021)-- Start
            '    'NMB New UI Enhancements.
            'Else
            '    gobjLocalization.Refresh(False, enArutiApplicatinType.Aruti_Payroll)
            '    'Pinkal (09-Aug-2021) -- End
            'End If

            gobjLocalization._LangId = HttpContext.Current.Session("LangId")

            Dim objUserPrivilege As New clsUserPrivilege
            'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

           

            '*** Payroll
            'Session("AllowGlobalPayment") = objUserPrivilege._AllowGlobalPayment
            'Session("DeletePayment") = objUserPrivilege._DeletePayment
            'Session("AllowToViewGlobalVoidPaymentList") = objUserPrivilege._AllowToViewGlobalVoidPaymentList
            'Session("AllowToApprovePayment") = objUserPrivilege._AllowToApprovePayment
            'Session("AllowToVoidApprovedPayment") = objUserPrivilege._AllowToVoidApprovedPayment
            'Session("AllowToViewBatchPostingList") = objUserPrivilege._AllowToViewBatchPostingList
            'Session("AllowToAddBatchPosting") = objUserPrivilege._AllowToAddBatchPosting
            'Session("AllowToEditBatchPosting") = objUserPrivilege._AllowToEditBatchPosting
            'Session("AllowToDeleteBatchPosting") = objUserPrivilege._AllowToDeleteBatchPosting
            'Session("AllowToPostBatchPostingToED") = objUserPrivilege._AllowToPostBatchPostingToED
            'Session("AddPayment") = objUserPrivilege._AddPayment
            'Session("AllowToViewPaymentList") = objUserPrivilege._AllowToViewPaymentList
            'Session("AddCashDenomination") = objUserPrivilege._AddCashDenomination
            'Session("AllowToViewEmpEDList") = objUserPrivilege._AllowToViewEmpEDList
            'Session("AddEarningDeduction") = objUserPrivilege._AddEarningDeduction
            'Session("EditEarningDeduction") = objUserPrivilege._EditEarningDeduction
            'Session("AllowToVoidBatchPostingToED") = objUserPrivilege._AllowToVoidBatchPostingToED

            'Session("DeleteEarningDeduction") = objUserPrivilege._DeleteEarningDeduction
            'Session("AllowAuthorizePayslipPayment") = objUserPrivilege._AllowAuthorizePayslipPayment
            'Session("AllowVoidAuthorizedPayslipPayment") = objUserPrivilege._AllowVoidAuthorizedPayslipPayment

            'Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
            'Session("AllowToViewSalaryChangeList") = objUserPrivilege._AllowToViewSalaryChangeList
            'Session("AllowToApproveSalaryChange") = objUserPrivilege._AllowToApproveSalaryChange

            ''SHANI (17 APR 2015)-START
            ''Payroll Master
            'Session("AllowToViewPayrollGroupList") = objUserPrivilege._AllowToViewPayrollGroupList
            'Session("AddPayrollGroup") = objUserPrivilege._AddPayrollGroup
            'Session("EditPayrollGroup") = objUserPrivilege._EditPayrollGroup
            'Session("DeletePayrollGroup") = objUserPrivilege._DeletePayrollGroup

            'Session("AllowToViewPayPointList") = objUserPrivilege._AllowToViewPayPointList
            'Session("AddPayPoint") = objUserPrivilege._AddPayPoint
            'Session("EditPayPoint") = objUserPrivilege._EditPayPoint
            'Session("DeletePayPoint") = objUserPrivilege._DeletePayPoint

            'Session("AllowToViewBankBranchList") = objUserPrivilege._AllowToViewBankBranchList
            'Session("AddBankBranch") = objUserPrivilege._AddBankBranch
            'Session("EditBankBranch") = objUserPrivilege._EditBankBranch
            'Session("DeleteBankBranch") = objUserPrivilege._DeleteBankBranch

            ''SHANI (17 APR 2015)--END 
            ''Sohail (29 Aug 2019) -- Start
            ''NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
            'Session("AllowToPostJVOnHolidays") = objUserPrivilege._AllowToPostJVOnHolidays
            ''Sohail (29 Aug 2019) -- End

            ''*** Loan & Advance
            'Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
            'Session("AllowtoApproveAdvance") = objUserPrivilege._AllowtoApproveAdvance
            'Session("AddPendingLoan") = objUserPrivilege._AddPendingLoan
            'Session("EditPendingLoan") = objUserPrivilege._EditPendingLoan
            'Session("AllowToViewProcessLoanAdvanceList") = objUserPrivilege._AllowToViewProcessLoanAdvanceList
            'Session("AllowToViewProceedingApprovalList") = objUserPrivilege._AllowToViewProceedingApprovalList
            'Session("AllowToViewLoanAdvanceList") = objUserPrivilege._AllowToViewLoanAdvanceList
            'Session("DeleteLoanAdvance") = objUserPrivilege._DeleteLoanAdvance
            'Session("EditLoanAdvance") = objUserPrivilege._EditLoanAdvance
            'Session("AllowChangeLoanAvanceStatus") = objUserPrivilege._AllowChangeLoanAvanceStatus
            'Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
            'Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
            'Session("Is_Report_Assigned") = clsArutiReportClass.Is_Report_Assigned(enArutiReport.BBL_Loan_Report, Session("UserId"), Session("CompanyUnkId"))
            'Session("DeletePendingLoan") = objUserPrivilege._DeletePendingLoan
            'Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
            'Session("AllowChangePendingLoanAdvanceStatus") = objUserPrivilege._AllowChangePendingLoanAdvanceStatus
            'Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
            'Session("EditLoanAdvancePayment") = objUserPrivilege._EditLoanAdvancePayment
            'Session("DeleteLoanAdvancePayment") = objUserPrivilege._DeleteLoanAdvancePayment
            'Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
            'Session("EditLoanAdvanceReceived") = objUserPrivilege._EditLoanAdvanceReceived
            'Session("DeleteLoanAdvanceReceived") = objUserPrivilege._DeleteLoanAdvanceReceived
            'Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
            'Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
            ''Nilay (20-Sept-2016) -- Start
            ''Enhancement : Cancel feature for approved but not assigned loan application
            'Session("AllowToCancelApprovedLoanApp") = objUserPrivilege._AllowToCancelApprovedLoanApp
            ''Nilay (20-Sept-2016) -- End

            ''*** Savings
            'Session("AddSavingsPayment") = objUserPrivilege._AddSavingsPayment
            'Session("EditSavingsPayment") = objUserPrivilege._EditSavingsPayment
            'Session("DeleteSavingsPayment") = objUserPrivilege._DeleteSavingsPayment
            'Session("AllowToViewEmployeeSavingsList") = objUserPrivilege._AllowToViewEmployeeSavingsList
            'Session("AddEmployeeSavings") = objUserPrivilege._AddEmployeeSavings
            'Session("EditEmployeeSavings") = objUserPrivilege._EditEmployeeSavings
            'Session("DeleteEmployeeSavings") = objUserPrivilege._DeleteEmployeeSavings
            'Session("AllowChangeSavingStatus") = objUserPrivilege._AllowChangeSavingStatus

            ''Shani(18-Dec-2015) -- Start
            ''ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            'Session("AddSavingsDeposit") = objUserPrivilege._AddSavingsDeposit
            'Session("EditSavingsDeposit") = objUserPrivilege._EditSavingsDeposit
            'Session("DeleteSavingsDeposit") = objUserPrivilege._DeleteSavingsDeposit
            ''Shani(18-Dec-2015) -- End

            ''*** HR
            'Session("AddDependant") = objUserPrivilege._AddDependant
            'Session("EditDependant") = objUserPrivilege._EditDependant
            'Session("DeleteDependant") = objUserPrivilege._DeleteDependant
            'Session("ViewDependant") = objUserPrivilege._AllowToViewEmpDependantsList
            'Session("AddEmployeeAssets") = objUserPrivilege._AddEmployeeAssets
            'Session("EditEmployeeAssets") = objUserPrivilege._EditEmployeeAssets
            'Session("DeleteEmployeeAssets") = objUserPrivilege._DeleteEmployeeAssets
            'Session("ViewCompanyAssetList") = objUserPrivilege._AllowToViewCompanyAssetList
            'Session("AddEmployeeSkill") = objUserPrivilege._AddEmployeeSkill
            'Session("EditEmployeeSkill") = objUserPrivilege._EditEmployeeSkill
            'Session("DeleteEmployeeSkill") = objUserPrivilege._DeleteEmployeeSkill
            'Session("ViewEmpSkillList") = objUserPrivilege._AllowToViewEmpSkillList
            'Session("AddEmployeeQualification") = objUserPrivilege._AddEmployeeQualification
            'Session("EditEmployeeQualification") = objUserPrivilege._EditEmployeeQualification
            'Session("DeleteEmployeeQualification") = objUserPrivilege._DeleteEmployeeQualification
            'Session("ViewEmpQualificationList") = objUserPrivilege._AllowToViewEmpQualificationList
            'Session("AddEmployeeReferee") = objUserPrivilege._AddEmployeeReferee
            'Session("EditEmployeeReferee") = objUserPrivilege._EditEmployeeReferee
            'Session("DeleteEmployeeReferee") = objUserPrivilege._DeleteEmployeeReferee
            'Session("ViewEmpReferenceList") = objUserPrivilege._AllowToViewEmpReferenceList
            'Session("AddEmployeeExperience") = objUserPrivilege._AddEmployeeExperience
            'Session("EditEmployeeExperience") = objUserPrivilege._EditEmployeeExperience
            'Session("DeleteEmployeeExperience") = objUserPrivilege._DeleteEmployeeExperience
            'Session("ViewEmpExperienceList") = objUserPrivilege._AllowToViewEmpExperienceList
            'Session("AddEmployee") = objUserPrivilege._AddEmployee
            'Session("EditEmployee") = objUserPrivilege._EditEmployee
            'Session("DeleteEmployee") = objUserPrivilege._DeleteEmployee
            'Session("ViewEmployee") = objUserPrivilege._AllowToViewEmpList
            'Session("SetReinstatementdate") = objUserPrivilege._AllowtoSetEmpReinstatementDate
            'Session("ChangeConfirmationDate") = objUserPrivilege._AllowtoChangeConfirmationDate
            'Session("ChangeAppointmentDate") = objUserPrivilege._AllowtoChangeAppointmentDate
            'Session("SetEmployeeBirthDate") = objUserPrivilege._AllowtoSetEmployeeBirthDate
            'Session("SetEmpSuspensionDate") = objUserPrivilege._AllowtoSetEmpSuspensionDate
            'Session("SetEmpProbationDate") = objUserPrivilege._AllowtoSetEmpProbationDate
            'Session("SetEmploymentEndDate") = objUserPrivilege._AllowtoSetEmploymentEndDate
            'Session("SetLeavingDate") = objUserPrivilege._AllowtoSetLeavingDate
            'Session("ChangeRetirementDate") = objUserPrivilege._AllowtoChangeRetirementDate
            'Session("ViewScale") = objUserPrivilege._AllowTo_View_Scale
            'Session("ViewBenefitList") = objUserPrivilege._AllowToViewEmpBenefitList
            'Session("AllowtoChangeBranch") = objUserPrivilege._AllowtoChangeBranch
            'Session("AllowtoChangeDepartmentGroup") = objUserPrivilege._AllowtoChangeDepartmentGroup
            'Session("AllowtoChangeDepartment") = objUserPrivilege._AllowtoChangeDepartment
            'Session("AllowtoChangeSectionGroup") = objUserPrivilege._AllowtoChangeSectionGroup
            'Session("AllowtoChangeSection") = objUserPrivilege._AllowtoChangeSection
            'Session("AllowtoChangeUnitGroup") = objUserPrivilege._AllowtoChangeUnitGroup
            'Session("AllowtoChangeUnit") = objUserPrivilege._AllowtoChangeUnit
            'Session("AllowtoChangeTeam") = objUserPrivilege._AllowtoChangeTeam
            'Session("AllowtoChangeJobGroup") = objUserPrivilege._AllowtoChangeJobGroup
            'Session("AllowtoChangeJob") = objUserPrivilege._AllowtoChangeJob
            'Session("AllowtoChangeGradeGroup") = objUserPrivilege._AllowtoChangeGradeGroup
            'Session("AllowtoChangeGrade") = objUserPrivilege._AllowtoChangeGrade
            'Session("AllowtoChangeGradeLevel") = objUserPrivilege._AllowtoChangeGradeLevel
            'Session("AllowtoChangeClassGroup") = objUserPrivilege._AllowtoChangeClassGroup
            'Session("AllowtoChangeClass") = objUserPrivilege._AllowtoChangeClass
            'Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
            'Session("AllowtoChangeCompanyEmail") = objUserPrivilege._AllowtoChangeCompanyEmail
            'Session("AllowToAddEditPhoto") = objUserPrivilege._AllowToAddEditPhoto
            'Session("AllowToDeletePhoto") = objUserPrivilege._AllowToDeletePhoto
            'Session("AllowToViewScale") = objUserPrivilege._AllowTo_View_Scale
            ''Sohail (09 Oct 2019) -- Start
            ''NMB Enhancement # : show employee end date or eoc / leaving date which is less on Payslips.
            'Session("AllowToChangeEOCLeavingDateOnClosedPeriod") = objUserPrivilege._AllowToChangeEOCLeavingDateOnClosedPeriod
            ''Sohail (09 Oct 2019) -- End
            ''Sohail (16 Oct 2019) -- Start
            ''NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
            'Session("AllowToPostFlexcubeJVToOracle") = objUserPrivilege._AllowToPostFlexcubeJVToOracle
            ''Sohail (16 Oct 2019) -- End
            ''Sohail (16 Oct 2019) -- Start
            ''NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            'Session("AllowToViewPaidAmount") = objUserPrivilege._AllowToViewPaidAmount
            ''Sohail (16 Oct 2019) -- End
            ''Sohail (18 Oct 2019) -- Start
            ''NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
            'Session("AllowToChangeAppointmentDateOnClosedPeriod") = objUserPrivilege._AllowToChangeAppointmentDateOnClosedPeriod
            ''Sohail (18 Oct 2019) -- End

            ''Shani (08-Dec-2016) -- Start
            ''Enhancement -  Add Employee Allocaion/Date Privilage
            'Session("AllowToChangeEmpTransfers") = objUserPrivilege._AllowToChangeEmpTransfers
            'Session("AllowToChangeEmpRecategorize") = objUserPrivilege._AllowToChangeEmpRecategorize
            'Session("AllowToChangeEmpWorkPermit") = objUserPrivilege._AllowToChangeEmpWorkPermit
            ''Shani (08-Dec-2016) -- End


            ''Nilay (07-Feb-2016) -- Start
            ''Session("FirstNamethenSurname") = ConfigParameter._Object._FirstNamethenSurname
            ''Nilay (07-Feb-2016) -- End

            ''*** Leave
            'Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
            'Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
            'Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense
            'Session("AddLeaveForm") = objUserPrivilege._AddLeaveForm
            'Session("EditLeaveForm") = objUserPrivilege._EditLeaveForm
            'Session("DeleteLeaveForm") = objUserPrivilege._DeleteLeaveForm
            'Session("ViewLeaveFormList") = objUserPrivilege._AllowToViewLeaveFormList
            'Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
            'Session("ViewLeaveProcessList") = objUserPrivilege._AllowToViewLeaveProcessList
            'Session("ChangeLeaveFormStatus") = objUserPrivilege._AllowChangeLeaveFormStatus
            'Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance
            'Session("AddPlanLeave") = objUserPrivilege._AddPlanLeave
            'Session("EditPlanLeave") = objUserPrivilege._EditPlanLeave
            'Session("DeletePlanLeave") = objUserPrivilege._DeletePlanLeave
            'Session("ViewLeavePlannerList") = objUserPrivilege._AllowToViewLeavePlannerList
            'Session("ViewPlannedLeaveViewer") = objUserPrivilege._AllowtoViewPlannedLeaveViewer
            'Session("ViewLeaveViewer") = objUserPrivilege._AllowToViewLeaveViewer
            'Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
            'Session("AllowToCancelPreviousDateLeave") = objUserPrivilege._AllowToCancelPreviousDateLeave
            'Session("AllowtoMigrateLeaveApprovers") = objUserPrivilege._AllowtoMigrateLeaveApprover
            'Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave
            'Session("AllowToViewLeaveApproverList") = objUserPrivilege._AllowToViewLeaveApproverList
            'Session("AddLeaveApprover") = objUserPrivilege._AddLeaveApprover
            'Session("EditLeaveApprover") = objUserPrivilege._EditLeaveApprover
            'Session("DeleteLeaveApprover") = objUserPrivilege._DeleteLeaveApprover
            'Session("AllowToAssignIssueUsertoEmp") = objUserPrivilege._AllowToAssignIssueUsertoEmp
            'Session("AllowToMigrateIssueUser") = objUserPrivilege._AllowToMigrateIssueUser
            'Session("AllowtoEndELC") = objUserPrivilege._AllowToEndLeaveCycle
            'Session("AllowMapApproverWithUser") = objUserPrivilege._AllowMapApproverWithUser
            'Session("AllowtoMapLeaveType") = objUserPrivilege._AllowtoMapLeaveType
            'Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave
            'Session("AllowPrintLeaveForm") = objUserPrivilege._AllowPrintLeaveForm
            'Session("AllowPreviewLeaveForm") = objUserPrivilege._AllowPreviewLeaveForm

            ''Shani(17-Aug-2015) -- Start
            ''Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'Session("AllowSetleaveActiveApprover") = objUserPrivilege._AllowtoSetLeaveActiveApprover
            'Session("AllowSetleaveInactiveApprover") = objUserPrivilege._AllowtoSetLeaveInactiveApprover
            ''Shani(17-Aug-2015) -- End


            ''*** Time & Attendance
            'Session("AllowToAddEditDeleteGlobalTimesheet") = objUserPrivilege._AllowToAddEditDeleteGlobalTimesheet

            ''*** Medical
            'Session("AddMedicalClaim") = objUserPrivilege._AddMedicalClaim
            'Session("EditMedicalClaim") = objUserPrivilege._EditMedicalClaim
            'Session("DeleteMedicalClaim") = objUserPrivilege._DeleteMedicalClaim
            'Session("AllowToExportMedicalClaim") = objUserPrivilege._AllowToExportMedicalClaim
            'Session("AllowToCancelExportedMedicalClaim") = objUserPrivilege._AllowToCancelExportedMedicalClaim
            'Session("AllowToAddMedicalSickSheet") = objUserPrivilege._AllowToAddMedicalSickSheet
            'Session("AllowToEditMedicalSickSheet") = objUserPrivilege._AllowToEditMedicalSickSheet
            'Session("AllowToDeleteMedicalSickSheet") = objUserPrivilege._AllowToDeleteMedicalSickSheet
            'Session("AllowToPrintMedicalSickSheet") = objUserPrivilege._AllowToPrintMedicalSickSheet
            'Session("RevokeUserAccessOnSicksheet") = objUserPrivilege._RevokeUserAccessOnSicksheet
            'Session("AllowToViewEmpSickSheetList") = objUserPrivilege._AllowToViewEmpSickSheetList
            'Session("AllowToViewMedicalClaimList") = objUserPrivilege._AllowToViewMedicalClaimList
            'Session("AllowToSaveMedicalClaim") = objUserPrivilege._AllowToSaveMedicalClaim
            'Session("AllowToFinalSaveMedicalClaim") = objUserPrivilege._AllowToFinalSaveMedicalClaim

            ''*** Assessment

            ''S.SANDEEP [28 MAY 2015] -- START
            ''ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            ''Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._Allow_UnlockFinalSaveBSCPlanning
            ''Session("EditEmployeeAssessment") = objUserPrivilege._EditEmployeeAssessment
            ''Session("DeleteEmployeeAssessment") = objUserPrivilege._DeleteEmployeeAssessment
            ''Session("Allow_UnlockCommittedGeneralAssessment") = objUserPrivilege._Allow_UnlockCommittedGeneralAssessment
            ''Session("AllowToViewSelfAssessmentList") = objUserPrivilege._AllowToViewSelfAssessmentList
            ''Session("AddAssessmentAnalysis") = objUserPrivilege._AddAssessmentAnalysis
            ''Session("EditAssessmentAnalysis") = objUserPrivilege._EditAssessmentAnalysis
            ''Session("DeleteAssessmentAnalysis") = objUserPrivilege._DeleteAssessmentAnalysis
            ''Session("AllowToViewAssessorAssessmentList") = objUserPrivilege._AllowToViewAssessorAssessmentList
            ''Session("AllowToViewReviewerAssessmentList") = objUserPrivilege._AllowToViewReviewerAssessmentList
            ''Session("AllowToAddReviewerGeneralAssessment") = objUserPrivilege._AllowToAddReviewerGeneralAssessment
            ''Session("AllowToEditReviewerGeneralAssessment") = objUserPrivilege._AllowToEditReviewerGeneralAssessment
            ''Session("AllowToDeleteReviewerGeneralAssessment") = objUserPrivilege._AllowToDeleteReviewerGeneralAssessment
            ''Session("Allow_UnlockCommittedBSCAssessment") = objUserPrivilege._Allow_UnlockCommittedBSCAssessment
            ''Session("AllowToEditSelfBSCAssessment") = objUserPrivilege._AllowToEditSelfBSCAssessment
            ''Session("AllowToDeleteSelfBSCAssessment") = objUserPrivilege._AllowToDeleteSelfBSCAssessment
            ''Session("AllowToViewSelfAssessedBSCList") = objUserPrivilege._AllowToViewSelfAssessedBSCList
            ''Session("AllowToAddAssessorBSCAssessment") = objUserPrivilege._AllowToAddAssessorBSCAssessment
            ''Session("AllowToEditAssessorBSCAssessment") = objUserPrivilege._AllowToEditAssessorBSCAssessment
            ''Session("AllowToDeleteAssessorBSCAssessment") = objUserPrivilege._AllowToDeleteAssessorBSCAssessment
            ''Session("AllowToViewAssessorAssessedBSCList") = objUserPrivilege._AllowToViewAssessorAssessedBSCList
            ''Session("AllowToAddReviewerBSCAssessment") = objUserPrivilege._AllowToAddReviewerBSCAssessment
            ''Session("AllowToEditReviewerBSCAssessment") = objUserPrivilege._AllowToEditReviewerBSCAssessment
            ''Session("AllowToDeleteReviewerBSCAssessment") = objUserPrivilege._AllowToDeleteReviewerBSCAssessment
            ''Session("AllowToViewReviewerAssessedBSCList") = objUserPrivilege._AllowToViewReviewerAssessedBSCList        
            ''Session("Allow_FinalSaveBSCPlanning") = objUserPrivilege._Allow_FinalSaveBSCPlanning
            ''Session("AllowToMigrateAssessor_Reviewer") = objUserPrivilege._AllowToMigrateAssessor_Reviewer

            'Session("AllowtoSubmitGoalsforApproval") = objUserPrivilege._AllowtoSubmitGoalsforApproval
            'Session("AllowtoApproveRejectGoalsPlanning") = objUserPrivilege._AllowtoApproveRejectGoalsPlanning
            'Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._AllowtoUnlockFinalSavedGoals
            'Session("AllowtoPerformAssessorReviewerMigration") = objUserPrivilege._AllowtoPerformAssessorReviewerMigration
            'Session("AllowtoAddSelfEvaluation") = objUserPrivilege._AllowtoAddSelfEvaluation
            'Session("AllowtoEditSelfEvaluation") = objUserPrivilege._AllowtoEditSelfEvaluation
            'Session("AllowtoDeleteSelfEvaluation") = objUserPrivilege._AllowtoDeleteSelfEvaluation
            'Session("AllowtoUnlockcommittedSelfEvaluation") = objUserPrivilege._AllowtoUnlockcommittedSelfEvaluation
            'Session("AllowtoViewSelfEvaluationList") = objUserPrivilege._AllowtoViewSelfEvaluationList
            'Session("AllowtoAddAssessorEvaluation") = objUserPrivilege._AllowtoAddAssessorEvaluation
            'Session("AllowtoEditAssessorEvaluation") = objUserPrivilege._AllowtoEditAssessorEvaluation
            'Session("AllowtoDeleteAssessorEvaluation") = objUserPrivilege._AllowtoDeleteAssessorEvaluation
            'Session("AllowtoUnlockcommittedAssessorEvaluation") = objUserPrivilege._AllowtoUnlockcommittedAssessorEvaluation
            'Session("AllowtoViewAssessorEvaluationList") = objUserPrivilege._AllowtoViewAssessorEvaluationList
            'Session("AllowtoAddReviewerEvaluation") = objUserPrivilege._AllowtoAddReviewerEvaluation
            'Session("AllowtoEditReviewerEvaluation") = objUserPrivilege._AllowtoEditReviewerEvaluation
            'Session("AllowtoDeleteReviewerEvaluation") = objUserPrivilege._AllowtoDeleteReviewerEvaluation
            'Session("AllowtoUnlockcommittedReviewerEvaluation") = objUserPrivilege._AllowtoUnlockcommittedReviewerEvaluation
            'Session("AllowtoViewReviewerEvaluationList") = objUserPrivilege._AllowtoViewReviewerEvaluationList
            'Session("AllowtoAddAllocationGoals") = objUserPrivilege._AllowtoAddAllocationGoals
            'Session("AllowtoEditAllocationGoals") = objUserPrivilege._AllowtoEditAllocationGoals
            'Session("AllowtoDeleteAllocationGoals") = objUserPrivilege._AllowtoDeleteAllocationGoals
            'Session("AllowtoCommitAllocationGoals") = objUserPrivilege._AllowtoCommitAllocationGoals
            'Session("AllowtoUnlockcommittedAllocationGoals") = objUserPrivilege._AllowtoUnlockcommittedAllocationGoals
            'Session("AllowtoPerformGlobalAssignAllocationGoals") = objUserPrivilege._AllowtoPerformGlobalAssignAllocationGoals
            'Session("AllowtoUpdatePercentCompletedAllocationGoals") = objUserPrivilege._AllowtoUpdatePercentCompletedAllocationGoals
            'Session("AllowtoViewAllocationGoalsList") = objUserPrivilege._AllowtoViewAllocationGoalsList
            'Session("AllowtoAddEmployeeGoals") = objUserPrivilege._AllowtoAddEmployeeGoals
            'Session("AllowtoEditEmployeeGoals") = objUserPrivilege._AllowtoEditEmployeeGoals
            'Session("AllowtoDeleteEmployeeGoals") = objUserPrivilege._AllowtoDeleteEmployeeGoals
            'Session("AllowtoPerformGlobalAssignEmployeeGoals") = objUserPrivilege._AllowtoPerformGlobalAssignEmployeeGoals
            'Session("AllowtoUpdatePercentCompletedEmployeeGoals") = objUserPrivilege._AllowtoUpdatePercentCompletedEmployeeGoals
            'Session("AllowtoViewEmployeeGoalsList") = objUserPrivilege._AllowtoViewEmployeeGoalsList
            ''S.SANDEEP [28 MAY 2015] -- END

            ''Shani(06-Feb-2016) -- Start
            ''PA Changes Given By CCBRT
            'Session("AllowtoViewPerformanceEvaluation") = objUserPrivilege._AllowtoViewPerformanceEvaluation
            'Session("AllowtoViewCompanyGoalsList") = objUserPrivilege._AllowtoViewCompanyGoalsList
            'Session("AllowtoAddAssignedCompetencies") = objUserPrivilege._AllowtoAddAssignedCompetencies
            'Session("AllowtoEditAssignedCompetencies") = objUserPrivilege._AllowtoEditAssignedCompetencies
            'Session("AllowtoDeleteAssignedCompetencies") = objUserPrivilege._AllowtoDeleteAssignedCompetencies
            'Session("AllowtoAddCustomItems") = objUserPrivilege._AllowtoAddCustomItems
            'Session("AllowtoEditCustomItems") = objUserPrivilege._AllowtoEditCustomItems
            'Session("AllowtoDeleteCustomItems") = objUserPrivilege._AllowtoDeleteCustomItems
            ''Shani(06-Feb-2016) -- End

            ''Shani (26-Sep-2016) -- Start
            ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'Session("AllowToApproveGoalsAccomplishment") = objUserPrivilege._AllowToApproveGoalsAccomplishment
            ''Shani (26-Sep-2016) -- End




            ''*** Recruitment
            'Session("AllowToApproveEmployee") = objUserPrivilege._AllowToApproveEmployee
            'Session("AllowtoApproveApplicantEligibility") = objUserPrivilege._AllowtoApproveApplicantEligibility
            'Session("AllowtoDisapproveApplicantEligibility") = objUserPrivilege._AllowtoDisapproveApplicantEligibility
            'Session("AllowtoViewInterviewAnalysisList") = objUserPrivilege._AllowtoViewInterviewAnalysisList
            'Session("AllowToViewShortListApplicants") = objUserPrivilege._AllowToViewShortListApplicants
            'Session("AllowToApproveApplicantFilter") = objUserPrivilege._AllowToApproveApplicantFilter
            'Session("AllowToRejectApplicantFilter") = objUserPrivilege._AllowToRejectApplicantFilter
            'Session("AllowToApproveFinalShortListedApplicant") = objUserPrivilege._AllowToApproveFinalShortListedApplicant
            'Session("AllowToDisapproveFinalShortListedApplicant") = objUserPrivilege._AllowToDisapproveFinalShortListedApplicant
            'Session("AllowToViewStaffRequisitionList") = objUserPrivilege._AllowToViewStaffRequisitionList
            'Session("AllowToAddStaffRequisition") = objUserPrivilege._AllowToAddStaffRequisition
            'Session("AllowToEditStaffRequisition") = objUserPrivilege._AllowToEditStaffRequisition
            'Session("AllowToDeleteStaffRequisition") = objUserPrivilege._AllowToDeleteStaffRequisition
            'Session("AllowToViewStaffRequisitionApprovals") = objUserPrivilege._AllowToViewStaffRequisitionApprovals
            'Session("AllowToCancelStaffRequisition") = objUserPrivilege._AllowToCancelStaffRequisition
            'Session("AllowToApproveStaffRequisition") = objUserPrivilege._AllowToApproveStaffRequisition


            ''*** TRANING

            'Session("AllowToViewLevel1EvaluationList") = objUserPrivilege._AllowToViewLevel1EvaluationList
            'Session("AllowToAddLevelIEvaluation") = objUserPrivilege._AllowToAddLevelIEvaluation
            'Session("AllowToEditLevelIEvaluation") = objUserPrivilege._AllowToEditLevelIEvaluation
            'Session("AllowToDeleteLevelIEvaluation") = objUserPrivilege._AllowToDeleteLevelIEvaluation
            'Session("AllowToSave_CompleteLevelIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIEvaluation
            'Session("AllowToPrintLevelIEvaluation") = objUserPrivilege._AllowToPrintLevelIEvaluation
            'Session("AllowToPreviewLevelIEvaluation") = objUserPrivilege._AllowToPreviewLevelIEvaluation
            'Session("AllowToViewTrainingEnrollmentList") = objUserPrivilege._AllowToViewTrainingEnrollmentList
            'Session("AddTrainingEnrollment") = objUserPrivilege._AddTrainingEnrollment
            'Session("EditTrainingEnrollment") = objUserPrivilege._EditTrainingEnrollment
            'Session("DeleteTrainingEnrollment") = objUserPrivilege._DeleteTrainingEnrollment
            'Session("AllowToAddLevelIIIEvaluation") = objUserPrivilege._AllowToAddLevelIIIEvaluation
            'Session("AllowToEditLevelIIIEvaluation") = objUserPrivilege._AllowToEditLevelIIIEvaluation
            'Session("AllowToDeleteLevelIIIEvaluation") = objUserPrivilege._AllowToDeleteLevelIIIEvaluation
            'Session("AllowToSave_CompleteLevelIIIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIIIEvaluation
            'Session("AllowToViewLevel3EvaluationList") = objUserPrivilege._AllowToViewLevel3EvaluationList

            ''*** ASSET DECLARATION

            'Session("AddAssetDeclaration") = objUserPrivilege._Allow_AddAssetDeclaration
            'Session("EditAssetDeclaration") = objUserPrivilege._Allow_EditAssetDeclaration
            'Session("DeleteAssetDeclaration") = objUserPrivilege._Allow_DeleteAssetDeclaration
            'Session("FinalSaveAssetDeclaration") = objUserPrivilege._Allow_FinalSaveAssetDeclaration
            'Session("UnlockFinalSaveAssetDeclaration") = objUserPrivilege._Allow_UnlockFinalSaveAssetDeclaration
            'Session("ViewAssetsDeclarationList") = objUserPrivilege._AllowToViewAssetsDeclarationList

            ''Pinkal (03-Dec-2018) -- Start
            ''Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
            'Session("AllowtoUnlockEmployeeForAssetDeclaration") = objUserPrivilege._AllowtoUnlockEmployeeForAssetDeclaration
            ''Pinkal (03-Dec-2018) -- end
            ''Sohail (04 Feb 2020) -- Start
            ''NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
            'Session("AllowtoUnlockEmployeeForNonDisclosureDeclaration") = objUserPrivilege._AllowtoUnlockEmployeeForNonDisclosureDeclaration
            ''Sohail (04 Feb 2020) -- End

            ''*** CLAIM &  REQUEST
            'Session("AllowtoAddExpenseApprover") = objUserPrivilege._AllowtoAddExpenseApprover
            'Session("AllowtoEditExpenseApprover") = objUserPrivilege._AllowtoEditExpenseApprover
            'Session("AllowtoDeleteExpenseApprover") = objUserPrivilege._AllowtoDeleteExpenseApprover
            'Session("AllowtoViewExpenseApproverList") = objUserPrivilege._AllowtoViewExpenseApproverList
            'Session("AllowtoMigrateExpenseApprover") = objUserPrivilege._AllowtoMigrateExpenseApprover
            'Session("AllowtoSwapExpenseApprover") = objUserPrivilege._AllowtoSwapExpenseApprover
            'Session("AllowtoAddClaimExpenseForm") = objUserPrivilege._AllowtoAddClaimExpenseForm
            'Session("AllowtoEditClaimExpenseForm") = objUserPrivilege._AllowtoEditClaimExpenseForm
            'Session("AllowtoDeleteClaimExpenseForm") = objUserPrivilege._AllowtoDeleteClaimExpenseForm
            'Session("AllowtoViewClaimExpenseFormList ") = objUserPrivilege._AllowtoViewClaimExpenseFormList
            'Session("AllowtoCancelClaimExpenseForm") = objUserPrivilege._AllowtoCancelClaimExpenseForm
            'Session("AllowtoProcessClaimExpenseForm") = objUserPrivilege._AllowtoProcessClaimExpenseForm
            'Session("AllowtoViewProcessClaimExpenseFormList") = objUserPrivilege._AllowtoViewProcessClaimExpenseFormList
            'Session("AllowtoPostClaimExpenseToPayroll") = objUserPrivilege._AllowtoPostClaimExpenseToPayroll
            ''Shani(17-Aug-2015) -- Start
            ''Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'Session("AllowSetClaimActiveApprover") = objUserPrivilege._AllowtoSetClaimActiveApprover
            'Session("AllowSetClaimInactiveApprover") = objUserPrivilege._AllowtoSetClaimInactiveApprover
            ''Shani(17-Aug-2015) -- End

            ''S.SANDEEP [23 FEB 2016] -- START
            'Session("AllowtoChangePreassignedCompetencies") = objUserPrivilege._AllowtoChangePreassignedCompetencies
            ''S.SANDEEP [23 FEB 2016] -- END


            ''Pinkal (21-Dec-2016) -- Start
            ''Enhancement - Adding Swap Approver Privilage for Leave Module.
            'Session("AllowToSwapLeaveApprover") = objUserPrivilege._AllowToSwapLeaveApprover
            ''Pinkal (21-Dec-2016) -- End



            ''Pinkal (03-May-2017) -- Start
            ''Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            'Session("AllowToMigrateBudgetTimesheetApprover") = objUserPrivilege._AllowToMigrateBudgetTimesheetApprover
            'Session("AllowToMapBudgetTimesheetApprover") = objUserPrivilege._AllowToMapBudgetTimesheetApprover
            'Session("AllowToViewBudgetTimesheetApproverList") = objUserPrivilege._AllowToViewBudgetTimesheetApproverList
            'Session("AllowToAddBudgetTimesheetApprover") = objUserPrivilege._AllowToAddBudgetTimesheetApprover
            'Session("AllowToEditBudgetTimesheetApprover") = objUserPrivilege._AllowToEditBudgetTimesheetApprover
            'Session("AllowToDeleteBudgetTimesheetApprover") = objUserPrivilege._AllowToDeleteBudgetTimesheetApprover
            'Session("AllowToSetBudgetTimesheetApproverAsActive") = objUserPrivilege._AllowToSetBudgetTimesheetApproverAsActive
            'Session("AllowToSetBudgetTimesheetApproverAsInActive") = objUserPrivilege._AllowToSetBudgetTimesheetApproverAsInActive
            'Session("AllowToViewEmployeeBudgetTimesheetList") = objUserPrivilege._AllowToViewEmployeeBudgetTimesheetList
            'Session("AllowToAddEmployeeBudgetTimesheet") = objUserPrivilege._AllowToAddEmployeeBudgetTimesheet
            'Session("AllowToEditEmployeeBudgetTimesheet") = objUserPrivilege._AllowToEditEmployeeBudgetTimesheet
            'Session("AllowToDeleteEmployeeBudgetTimesheet") = objUserPrivilege._AllowToDeleteEmployeeBudgetTimesheet
            'Session("AllowToCancelEmployeeBudgetTimesheet") = objUserPrivilege._AllowToCancelEmployeeBudgetTimesheet
            'Session("AllowToViewPendingEmpBudgetTimesheetSubmitForApproval") = objUserPrivilege._AllowToViewPendingEmpBudgetTimesheetSubmitForApproval
            'Session("AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval") = objUserPrivilege._AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval
            'Session("AllowToSubmitForApprovalForPendingEmpBudgetTimesheet") = objUserPrivilege._AllowToSubmitForApprovalForPendingEmpBudgetTimesheet
            'Session("AllowToViewEmployeeBudgetTimesheetApprovalList") = objUserPrivilege._AllowToViewEmployeeBudgetTimesheetApprovalList
            'Session("AllowToChangeEmployeeBudgetTimesheetStatus") = objUserPrivilege._AllowToChangeEmployeeBudgetTimesheetStatus
            ''Pinkal (03-May-2017) -- End




            ''Nilay (16-Apr-2016) -- Start
            ''Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            ''Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(HttpContext.Current.Request.UserLanguages(0))
            ''System.Threading.Thread.CurrentThread.CurrentUICulture = culture
            ''System.Threading.Thread.CurrentThread.CurrentCulture = culture
            ''Session("DateFormat") = culture.DateTimeFormat.ShortDatePattern.ToString
            ''Nilay (16-Apr-2016) -- End

            ''Varsha Rana (17-Oct-2017) -- Start
            ''Enhancement - Give user privileges.
            'Session("AllowToAddLoanApprover") = objUserPrivilege._AllowToAddLoanApprover
            'Session("AllowToEditLoanApprover") = objUserPrivilege._AllowToEditLoanApprover
            'Session("AllowToDeleteLoanApprover") = objUserPrivilege._AllowToDeleteLoanApprover
            'Session("AllowToSetActiveInactiveLoanApprover") = objUserPrivilege._AllowToSetActiveInactiveLoanApprover
            'Session("AllowToViewLoanApproverLevelList") = objUserPrivilege._AllowToViewLoanApproverLevelList
            'Session("AllowToViewLoanApproverList") = objUserPrivilege._AllowToViewLoanApproverList
            'Session("AllowToProcessGlobalLoanApprove") = objUserPrivilege._AllowToProcessGlobalLoanApprove
            'Session("AllowToProcessGlobalLoanAssign") = objUserPrivilege._AllowToProcessGlobalLoanAssign
            'Session("AllowToViewLoanApprovalList") = objUserPrivilege._AllowToViewLoanApprovalList
            'Session("AllowToChangeLoanStatus") = objUserPrivilege._AllowToChangeLoanStatus
            'Session("AllowToTransferLoanApprover") = objUserPrivilege._AllowToTransferLoanApprover
            'Session("AllowToSwapLoanApprover") = objUserPrivilege._AllowToSwapLoanApprover



            'Session("AllowToEditTransferEmployeeDetails") = objUserPrivilege._AllowToEditTransferEmployeeDetails
            'Session("AllowToDeleteTransferEmployeeDetails") = objUserPrivilege._AllowToDeleteTransferEmployeeDetails

            'Session("AllowToEditRecategorizeEmployeeDetails") = objUserPrivilege._AllowToEditRecategorizeEmployeeDetails
            'Session("AllowToDeleteRecategorizeEmployeeDetails") = objUserPrivilege._AllowToDeleteRecategorizeEmployeeDetails

            'Session("AllowToEditProbationEmployeeDetails") = objUserPrivilege._AllowToEditProbationEmployeeDetails
            'Session("AllowToDeleteProbationEmployeeDetails") = objUserPrivilege._AllowToDeleteProbationEmployeeDetails

            'Session("AllowToEditConfirmationEmployeeDetails") = objUserPrivilege._AllowToEditConfirmationEmployeeDetails
            'Session("AllowToDeleteConfirmationEmployeeDetails") = objUserPrivilege._AllowToDeleteConfirmationEmployeeDetails

            'Session("AllowToEditSuspensionEmployeeDetails") = objUserPrivilege._AllowToEditSuspensionEmployeeDetails
            'Session("AllowToDeleteSuspensionEmployeeDetails") = objUserPrivilege._AllowToDeleteSuspensionEmployeeDetails

            'Session("AllowToEditTerminationEmployeeDetails") = objUserPrivilege._AllowToEditTerminationEmployeeDetails
            'Session("AllowToDeleteTerminationEmployeeDetails") = objUserPrivilege._AllowToDeleteTerminationEmployeeDetails

            'Session("AllowToEditRetiredEmployeeDetails") = objUserPrivilege._AllowToEditRetiredEmployeeDetails
            'Session("AllowToDeleteRetiredEmployeeDetails") = objUserPrivilege._AllowToDeleteRetiredEmployeeDetails

            'Session("AllowToRehireEmployee") = objUserPrivilege._AllowToRehireEmployee

            'Session("AllowToEditWorkPermitEmployeeDetails") = objUserPrivilege._AllowToEditWorkPermitEmployeeDetails
            'Session("AllowToDeleteWorkPermitEmployeeDetails") = objUserPrivilege._AllowToDeleteWorkPermitEmployeeDetails

            'Session("AllowToEditRehireEmployeeDetails") = objUserPrivilege._AllowToEditRehireEmployeeDetails
            'Session("AllowToDeleteRehireEmployeeDetails") = objUserPrivilege._AllowToDeleteRehireEmployeeDetails

            ''Sohail (21 Oct 2019) -- Start
            ''NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'Session("AllowToSetEmployeeExemptionDate") = objUserPrivilege._AllowToSetEmployeeExemptionDate
            'Session("AllowToEditEmployeeExemptionDate") = objUserPrivilege._AllowToEditEmployeeExemptionDate
            'Session("AllowToDeleteEmployeeExemptionDate") = objUserPrivilege._AllowToDeleteEmployeeExemptionDate
            ''Sohail (21 Oct 2019) -- End

            'Session("AddSalaryIncrement") = objUserPrivilege._AddSalaryIncrement
            'Session("AllowToAssignBenefitGroup") = objUserPrivilege._AllowToAssignBenefitGroup
            'Session("AllowToChangeEmpTransfers") = objUserPrivilege._AllowToChangeEmpTransfers
            'Session("AllowToChangeEmpRecategorize") = objUserPrivilege._AllowToChangeEmpRecategorize

            'Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
            'Session("EditEmployeeCostCenter") = objUserPrivilege._EditEmployeeCostCenter
            'Session("DeleteEmployeeCostCenter") = objUserPrivilege._DeleteEmployeeCostCenter

            'Session("AllowToSubmitForGoalAccomplished") = objUserPrivilege._AllowToSubmitForGoalAccomplished

            'Session("AllowToViewAssignedShiftList") = objUserPrivilege._AllowToViewAssignedShiftList
            'Session("AllowToViewAssignedPolicyList") = objUserPrivilege._AllowToViewAssignedPolicyList
            'Session("AllowToAddShiftPolicyAssignment") = objUserPrivilege._AllowToAddShiftPolicyAssignment
            'Session("AllowToDeleteAssignedPolicy") = objUserPrivilege._AllowToDeleteAssignedPolicy
            'Session("AllowToDeleteAssignedShift") = objUserPrivilege._AllowToDeleteAssignedShift

            'Session("AllowToPerformEligibleOperation") = objUserPrivilege._AllowToPerformEligibleOperation
            'Session("AllowToPerformNotEligibleOperation") = objUserPrivilege._AllowToPerformNotEligibleOperation

            'Session("AllowToViewFinalApplicantList") = objUserPrivilege._AllowToViewFinalApplicantList

            ''Varsha Rana (17-Oct-2017) -- End

            ''S.SANDEEP [16-Jan-2018] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 118
            'Session("AllowtoAddEmployeeSignature") = objUserPrivilege._AllowtoAddEmployeeSignature
            'Session("AllowtoDeleteEmployeeSignature") = objUserPrivilege._AllowtoDeleteEmployeeSignature
            'Session("AllowtoSeeEmployeeSignature") = objUserPrivilege._AllowtoSeeEmployeeSignature
            ''S.SANDEEP [16-Jan-2018] -- END

            ''S.SANDEEP [06-SEP-2018] -- START
            ''ISSUE/ENHANCEMENT : {Ref#274}
            'Session("AllowToViewResolutionStepList") = objUserPrivilege._AllowToViewGrievanceResolutionStepList
            'Session("AllowToAddGrievanceResolutionStep") = objUserPrivilege._AllowToAddGrievanceResolutionStep
            'Session("AllowToEditGrievanceResolutionStep") = objUserPrivilege._AllowToEditGrievanceResolutionStep
            'Session("AllowToDeleteGrievanceResolutionStep") = objUserPrivilege._AllowToDeleteGrievanceResolutionStep
            ''S.SANDEEP [06-SEP-2018] -- END




            ''Gajanan [13-AUG-2018] -- Start
            ''Enhancement - Implementing Grievance Module.
            'Session("AllowToAddGrievanceApproverLevel") = objUserPrivilege._AllowToAddGrievanceApproverLevel
            'Session("AllowToEditGrievanceApproverLevel") = objUserPrivilege._AllowToEditGrievanceApproverLevel
            'Session("AllowToDeleteGrievanceApproverLevel") = objUserPrivilege._AllowToDeleteGrievanceApproverLevel
            'Session("AllowToViewGrievanceApproverLevel") = objUserPrivilege._AllowToViewGrievanceApproverLevel

            'Session("AllowToAddGrievanceApprover") = objUserPrivilege._AllowToAddGrievanceApprover
            'Session("AllowToEditGrievanceApprover") = objUserPrivilege._AllowToEditGrievanceApprover
            'Session("AllowToDeleteGrievanceApprover") = objUserPrivilege._AllowToDeleteGrievanceApprover
            'Session("AllowToViewGrievanceApprover") = objUserPrivilege._AllowToViewGrievanceApprover
            'Session("AllowToActivateGrievanceApprover") = objUserPrivilege._AllowToActivateGrievanceApprover
            'Session("AllowToInActivateGrievanceApprover") = objUserPrivilege._AllowToInActivateGrievanceApprover

            'Session("AllowToViewGrievanceResolutionStepList") = objUserPrivilege._AllowToViewGrievanceResolutionStepList
            'Session("AllowToAddGrievanceResolutionStep") = objUserPrivilege._AllowToAddGrievanceResolutionStep
            'Session("AllowToEditGrievanceResolutionStep") = objUserPrivilege._AllowToEditGrievanceResolutionStep
            'Session("AllowToDeleteGrievanceResolutionStep") = objUserPrivilege._AllowToDeleteGrievanceResolutionStep
            ''Gajanan(13-AUG-2018) -- End    

            ''S.SANDEEP [09-OCT-2018] -- START
            'Session("AllowToAddTrainingApproverLevel") = objUserPrivilege._AllowToAddTrainingApproverLevel
            'Session("AllowToEditTrainingApproverLevel") = objUserPrivilege._AllowToEditTrainingApproverLevel
            'Session("AllowToDeleteTrainingApproverLevel") = objUserPrivilege._AllowToDeleteTrainingApproverLevel
            'Session("AllowToViewTrainingApproverLevel") = objUserPrivilege._AllowToViewTrainingApproverLevel
            'Session("AllowToAddTrainingApprover") = objUserPrivilege._AllowToAddTrainingApprover
            'Session("AllowToDeleteTrainingApprover") = objUserPrivilege._AllowToDeleteTrainingApprover
            'Session("AllowToViewTrainingApprover") = objUserPrivilege._AllowToViewTrainingApprover
            'Session("AllowToApproveTrainingRequisition") = objUserPrivilege._AllowToApproveTrainingRequisition
            'Session("AllowToActivateTrainingApprover") = objUserPrivilege._AllowToActivateTrainingApprover
            'Session("AllowToDeactivateTrainingApprover") = objUserPrivilege._AllowToDeactivateTrainingApprover
            ''S.SANDEEP [09-OCT-2018] -- END


            ''*** START  TNA OT REQUISITION

            ''Hemant (22 Oct 2018) -- Start
            ''Enhancement : Implementing New Module of OT Requisition Approver Level & Approver Master
            'Session("AllowToViewOTRequisitionApproverLevel") = objUserPrivilege._AllowToViewOTRequisitionApproverLevel
            'Session("AllowToAddOTRequisitionApproverLevel") = objUserPrivilege._AllowToAddOTRequisitionApproverLevel
            'Session("AllowToEditOTRequisitionApproverLevel") = objUserPrivilege._AllowToEditOTRequisitionApproverLevel
            'Session("AllowToDeleteOTRequisitionApproverLevel") = objUserPrivilege._AllowToDeleteOTRequisitionApproverLevel
            'Session("AllowToAddOTRequisitionApprover") = objUserPrivilege._AllowToAddOTRequisitionApprover
            'Session("AllowToEditOTRequisitionApprover") = objUserPrivilege._AllowToEditOTRequisitionApprover
            'Session("AllowToDeleteOTRequisitionApprover") = objUserPrivilege._AllowToDeleteOTRequisitionApprover
            'Session("AllowToViewOTRequisitionApprover") = objUserPrivilege._AllowToViewOTRequisitionApprover
            ''Hemant (22 Oct 2018) -- End


            ''Pinkal (27-Jun-2019) -- Start
            ''Enhancement - IMPLEMENTING OT MODULE.
            'Session("AllowtoApproveOTRequisition") = objUserPrivilege._AllowtoApproveOTRequisition
            'Session("AllowtoViewOTRequisitionApprovalList") = objUserPrivilege._AllowtoViewOTRequisitionApprovalList
            ''Pinkal (27-Jun-2019) -- End


            ''Pinkal (08-Jan-2020) -- Start
            ''Enhancement - NMB - Working on NMB OT Requisition Requirement.
            'Session("AllowToViewEmpOTAssignment") = objUserPrivilege._AllowToViewEmpOTAssignment
            'Session("AllowToAddEmpOTAssignment") = objUserPrivilege._AllowToAddEmpOTAssignment
            'Session("AllowToDeleteEmpOTAssignment") = objUserPrivilege._AllowToDeleteEmpOTAssignment
            'Session("AllowToCancelEmpOTRequisition") = objUserPrivilege._AllowToCancelEmpOTRequisition
            ''Pinkal (08-Jan-2020) -- End

            ''Pinkal (29-Jan-2020) -- Start
            ''Enhancement - Changes related To OT NMB Testing.
            'Session("AllowToPostOTRequisitionToPayroll") = objUserPrivilege._AllowToPostOTRequisitionToPayroll
            ''Pinkal (29-Jan-2020) -- End


            ''Pinkal (03-Mar-2020) -- Start
            ''ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
            'Session("AllowToMigrateOTRequisitionApprover") = objUserPrivilege._AllowToMigrateOTRequisitionApprover
            ''Pinkal (03-Mar-2020) -- End


            ''*** END  TNA OT REQUISITION


            ''S.SANDEEP |12-FEB-2019| -- START
            ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'Session("AllowToDeletePercentCompletedEmployeeGoals") = objUserPrivilege._AllowToDeletePercentCompletedEmployeeGoals
            'Session("AllowToViewPercentCompletedEmployeeGoals") = objUserPrivilege._AllowToViewPercentCompletedEmployeeGoals
            ''S.SANDEEP |12-FEB-2019| -- END

            ''Gajanan [22-Feb-2019] -- Start
            ''Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Session("AllowToApproveRejectEmployeeReferences") = objUserPrivilege._AllowToApproveRejectEmployeeReferences
            ''Gajanan [22-Feb-2019] -- End

            ''S.SANDEEP |15-APR-2019| -- START
            'Session("AllowToApproveRejectEmployeeMembership") = objUserPrivilege._AllowToApproveRejectEmployeeMembership
            ''S.SANDEEP |15-APR-2019| -- END

            ''Sohail (18 May 2019) -- Start
            ''NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Session("AllowToSetDependentActive") = objUserPrivilege._AllowToSetDependentActive
            'Session("AllowToSetDependentInactive") = objUserPrivilege._AllowToSetDependentInactive
            'Session("AllowToDeleteDependentStatus") = objUserPrivilege._AllowToDeleteDependentStatus
            'Session("AllowToViewDependentStatus") = objUserPrivilege._AllowToViewDependentStatus
            ''Sohail (18 May 2019) -- End


            ''S.SANDEEP |27-MAY-2019| -- START
            ''ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'Session("AllowToViewCalibrationApproverLevel") = objUserPrivilege._AllowToViewCalibrationApproverLevel
            'Session("AllowToAddCalibrationApproverLevel") = objUserPrivilege._AllowToAddCalibrationApproverLevel
            'Session("AllowToEditCalibrationApproverLevel") = objUserPrivilege._AllowToEditCalibrationApproverLevel
            'Session("AllowToDeleteCalibrationApproverLevel") = objUserPrivilege._AllowToDeleteCalibrationApproverLevel

            'Session("AllowToViewCalibrationApproverMaster") = objUserPrivilege._AllowToViewCalibrationApproverMaster
            'Session("AllowToAddCalibrationApproverMaster") = objUserPrivilege._AllowToAddCalibrationApproverMaster
            'Session("AllowToActivateCalibrationApproverMaster") = objUserPrivilege._AllowToActivateCalibrationApproverMaster
            'Session("AllowToDeleteCalibrationApproverMaster") = objUserPrivilege._AllowToDeleteCalibrationApproverMaster
            'Session("AllowToDeactivateCalibrationApproverMaster") = objUserPrivilege._AllowToDeactivateCalibrationApproverMaster
            'Session("AllowToApproveRejectCalibratedScore") = objUserPrivilege._AllowToApproveRejectCalibratedScore
            ''S.SANDEEP |27-MAY-2019| -- END



            ''Pinkal (27-Jun-2019) -- Start
            ''Enhancement - IMPLEMENTING OT MODULE.
            'Session("AllowToEditOTRequisitionApprover") = objUserPrivilege._AllowToEditOTRequisitionApprover
            'Session("AllowToDeleteOTRequisitionApprover") = objUserPrivilege._AllowToDeleteOTRequisitionApprover
            'Session("AllowToActivateOTRequisitionApprover") = objUserPrivilege._AllowToActivateOTRequisitionApprover
            'Session("AllowToInActivateOTRequisitionApprover") = objUserPrivilege._AllowToInActivateOTRequisitionApprover
            ''Pinkal (27-Jun-2019) -- End

            ''S.SANDEEP |27-JUL-2019| -- START
            ''ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'Session("AllowToEditCalibratedScore") = objUserPrivilege._AllowToEditCalibratedScore
            ''S.SANDEEP |27-JUL-2019| -- END


            ''Pinkal (13-Aug-2019) -- Start
            ''Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            'Session("AllowToViewBudgetTimesheetExemptEmployeeList") = objUserPrivilege._AllowToViewBudgetTimesheetExemptEmployeeList
            'Session("AllowToAddBudgetTimesheetExemptEmployee") = objUserPrivilege._AllowToAddBudgetTimesheetExemptEmployee
            'Session("AllowToDeleteBudgetTimesheetExemptEmployee") = objUserPrivilege._AllowToDeleteBudgetTimesheetExemptEmployee
            ''Pinkal (13-Aug-2019) -- End

            ''S.SANDEEP |16-AUG-2019| -- START
            ''ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            'Session("AllowtoCalibrateProvisionalScore") = objUserPrivilege._AllowtoCalibrateProvisionalScore
            ''S.SANDEEP |16-AUG-2019| -- END

            ''Sohail (14 Nov 2019) -- Start
            ''NMB UAT Enhancement # : New Screen Training Need Form.
            'Session("AllowToViewTrainingNeedForm") = objUserPrivilege._AllowToViewTrainingNeedForm
            'Session("AllowToAddTrainingNeedForm") = objUserPrivilege._AllowToAddTrainingNeedForm
            'Session("AllowToEditTrainingNeedForm") = objUserPrivilege._AllowToEditTrainingNeedForm
            'Session("AllowToDeleteTrainingNeedForm") = objUserPrivilege._AllowToDeleteTrainingNeedForm
            ''Sohail (14 Nov 2019) -- End

            ''S.SANDEEP |18-JAN-2020| -- START
            ''ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            'Session("AllowToUnlockEmployeeInPlanning") = objUserPrivilege._AllowToUnlockEmployeeInPlanning
            'Session("AllowToUnlockEmployeeInAssessment") = objUserPrivilege._AllowToUnlockEmployeeInAssessment
            ''S.SANDEEP |18-JAN-2020| -- END

            ''S.SANDEEP |01-MAY-2020| -- START
            ''ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
            'Session("AllowToViewCalibratorList") = objUserPrivilege._AllowToViewCalibratorList
            'Session("AllowToAddCalibrator") = objUserPrivilege._AllowToAddCalibrator
            'Session("AllowToMakeCalibratorActive") = objUserPrivilege._AllowToMakeCalibratorActive
            'Session("AllowToDeleteCalibrator") = objUserPrivilege._AllowToDeleteCalibrator
            'Session("AllowToMakeCalibratorInactive") = objUserPrivilege._AllowToMakeCalibratorInactive
            'Session("AllowToEditCalibrator") = objUserPrivilege._AllowToEditCalibrator
            'Session("AllowToEditCalibrationApproverMaster") = objUserPrivilege._AllowToEditCalibrationApproverMaster
            ''S.SANDEEP |01-MAY-2020| -- END

            ''Hemant (26 Nov 2020) -- Start
            ''Enhancement : Talent Pipeline new changes
            'Session("AllowToAddPotentialTalentEmployee") = objUserPrivilege._AllowToAddPotentialTalentEmployee
            'Session("AllowToApproveRejectTalentPipelineEmployee") = objUserPrivilege._AllowToApproveRejectTalentPipelineEmployee
            'Session("AllowToMoveApprovedEmployeeToQulified") = objUserPrivilege._AllowToMoveApprovedEmployeeToQulified
            'Session("AllowToRemoveTalentProcess") = objUserPrivilege._AllowToRemoveTalentProcess
            'Session("AllowToViewPotentialTalentEmployee") = objUserPrivilege._AllowToViewPotentialTalentEmployee
            ''Hemant (26 Nov 2020) -- End

            ''Gajanan [17-Dec-2020] -- Start
            'Session("AllowToAddPotentialSuccessionEmployee") = objUserPrivilege._AllowToAddPotentialSuccessionEmployee
            'Session("AllowToApproveRejectSuccessionPipelineEmployee") = objUserPrivilege._AllowToApproveRejectSuccessionPipelineEmployee
            'Session("AllowToMoveApprovedEmployeeToQulifiedSuccession") = objUserPrivilege._AllowToMoveApprovedEmployeeToQulifiedSuccession
            'Session("AllowToViewPotentialSuccessionEmployee") = objUserPrivilege._AllowToViewPotentialSuccessionEmployee
            'Session("AllowToRemoveSuccessionProcess") = objUserPrivilege._AllowToRemoveSuccessionProcess
            ''Gajanan [17-Dec-2020] -- End

            ''Gajanan [19-Feb-2021] -- Start
            'Session("AllowToSendNotificationToTalentScreener") = objUserPrivilege._AllowToSendNotificationToTalentScreener
            'Session("AllowToSendNotificationToTalentEmployee") = objUserPrivilege._AllowToSendNotificationToTalentEmployee
            'Session("AllowForTalentScreeningProcess") = objUserPrivilege._AllowForTalentScreeningProcess
            'Session("AllowToSendNotificationToSuccessionScreener") = objUserPrivilege._AllowToSendNotificationToSuccessionScreener
            'Session("AllowToSendNotificationToSuccessionEmployee") = objUserPrivilege._AllowToSendNotificationToSuccessionEmployee
            'Session("AllowForSuccessionScreeningProcess") = objUserPrivilege._AllowForSuccessionScreeningProcess
            'Session("AllowToSendNotificationToTalentApprover") = objUserPrivilege._AllowToSendNotificationToTalentApprover
            'Session("AllowToSendNotificationToSuccessionApprover") = objUserPrivilege._AllowToSendNotificationToSuccessionApprover
            ''Gajanan [19-Feb-2021] -- End

            ''Gajanan [30-JAN-2021] -- Start   
            ''Enhancement:Worked On PDP Module
            'Session("AllowToViewLearningAndDevelopmentPlan") = objUserPrivilege._AllowToViewLearningAndDevelopmentPlan
            'Session("AllowToAddPersonalAnalysisGoal") = objUserPrivilege._AllowToAddPersonalAnalysisGoal
            'Session("AllowToEditPersonalAnalysisGoal") = objUserPrivilege._AllowToEditPersonalAnalysisGoal
            'Session("AllowToDeletePersonalAnalysisGoal") = objUserPrivilege._AllowToDeletePersonalAnalysisGoal
            'Session("AllowToAddDevelopmentActionPlan") = objUserPrivilege._AllowToAddDevelopmentActionPlan
            'Session("AllowToEditDevelopmentActionPlan") = objUserPrivilege._AllowToEditDevelopmentActionPlan
            'Session("AllowToDeleteDevelopmentActionPlan") = objUserPrivilege._AllowToDeleteDevelopmentActionPlan
            'Session("AllowToAddUpdateProgressForDevelopmentActionPlan") = objUserPrivilege._AllowToAddUpdateProgressForDevelopmentActionPlan
            'Session("AllowToViewActionPlanAssignedTrainingCourses") = objUserPrivilege._AllowToViewActionPlanAssignedTrainingCourses
            'Session("AllowToAddCommentForDevelopmentActionPlan") = objUserPrivilege._AllowToAddCommentForDevelopmentActionPlan
            'Session("AllowToUnlockPDPForm") = objUserPrivilege._AllowToUnlockPDPForm
            'Session("AllowToLockPDPForm") = objUserPrivilege._AllowToLockPDPForm
            'Session("AllowToUndoUpdateProgressForDevelopmentActionPlan") = objUserPrivilege._AllowToUndoUpdateProgressForDevelopmentActionPlan
            ''Gajanan [30-JAN-2021] -- End

            ''Gajanan [26-Feb-2021] -- Start
            'Session("AllowToNominateEmployeeForSuccession") = objUserPrivilege._AllowToNominateEmployeeForSuccession

            'Session("AllowToModifyTalentSetting") = objUserPrivilege._AllowToModifyTalentSetting
            'Session("AllowToViewTalentSetting") = objUserPrivilege._AllowToViewTalentSetting
            'Session("AllowToModifySuccessionSetting") = objUserPrivilege._AllowToModifySuccessionSetting
            'Session("AllowToViewSuccessionSetting") = objUserPrivilege._AllowToViewSuccessionSetting
            ''Gajanan [26-Feb-2021] -- End

            'Session("AllowToViewTalentScreeningDetail") = objUserPrivilege._AllowToViewTalentScreeningDetail
            'Session("AllowToViewSuccessionScreeningDetail") = objUserPrivilege._AllowToViewSuccessionScreeningDetail

            ''Sohail (01 Mar 2021) -- Start
            ''NMB Enhancement : : New screen Departmental Training Need in New UI.
            'Session("AllowToViewDepartmentalTrainingNeed") = objUserPrivilege._AllowToViewDepartmentalTrainingNeed
            'Session("AllowToAddDepartmentalTrainingNeed") = objUserPrivilege._AllowToAddDepartmentalTrainingNeed
            'Session("AllowToEditDepartmentalTrainingNeed") = objUserPrivilege._AllowToEditDepartmentalTrainingNeed
            'Session("AllowToDeleteDepartmentalTrainingNeed") = objUserPrivilege._AllowToDeleteDepartmentalTrainingNeed
            ''Sohail (01 Mar 2021) -- End

            ''Hemant (26 Mar 2021) -- Start
            ''NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'Session("AllowToSubmitForApprovalFromDepartmentalTrainingNeed") = objUserPrivilege._AllowToSubmitForApprovalFromDepartmentalTrainingNeed
            'Session("AllowToTentativeApproveForDepartmentalTrainingNeed") = objUserPrivilege._AllowToTentativeApproveForDepartmentalTrainingNeed
            'Session("AllowToSubmitForApprovalFromTrainingBacklog") = objUserPrivilege._AllowToSubmitForApprovalFromTrainingBacklog
            'Session("AllowToFinalApproveDepartmentalTrainingNeed") = objUserPrivilege._AllowToFinalApproveDepartmentalTrainingNeed
            'Session("AllowToRejectDepartmentalTrainingNeed") = objUserPrivilege._AllowToRejectDepartmentalTrainingNeed
            'Session("AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed") = objUserPrivilege._AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed
            'Session("AllowToUnlockSubmittedForTrainingBudgetApproval") = objUserPrivilege._AllowToUnlockSubmittedForTrainingBudgetApproval
            'Session("AllowToAskForReviewTrainingAsPerAmountSet") = objUserPrivilege._AllowToAskForReviewTrainingAsPerAmountSet
            'Session("AllowToUndoApprovedTrainingBudgetApproval") = objUserPrivilege._AllowToUndoApprovedTrainingBudgetApproval
            ''Hemant (26 Mar 2021) -- End
            ''Sohail (12 Apr 2021) -- Start
            ''NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'Session("AllowToUndoRejectedTrainingBudgetApproval") = objUserPrivilege._AllowToUndoRejectedTrainingBudgetApproval
            ''Sohail (12 Apr 2021) -- End
            ''Sohail (26 Apr 2021) -- Start
            ''NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'Session("AllowToViewBudgetSummaryForDepartmentalTrainingNeed") = objUserPrivilege._AllowToViewBudgetSummaryForDepartmentalTrainingNeed
            'Session("AllowToSetMaxBudgetForDepartmentalTrainingNeed") = objUserPrivilege._AllowToSetMaxBudgetForDepartmentalTrainingNeed
            ''Sohail (26 Apr 2021) -- End



            ''Pinkal (27-Apr-2021)-- Start
            ''KBC Enhancement  -  Working on Claim Retirement Enhancement.
            'Session("AllowToViewRetireClaimApplication") = objUserPrivilege._AllowToViewRetireClaimApplication
            'Session("AllowToRetireClaimApplication") = objUserPrivilege._AllowToRetireClaimApplication
            'Session("AllowToEditRetiredClaimApplication") = objUserPrivilege._AllowToEditRetiredClaimApplication
            'Session("AllowToViewRetiredApplicationApproval") = objUserPrivilege._AllowToViewRetiredApplicationApproval
            'Session("AllowToApproveRetiredApplication") = objUserPrivilege._AllowToApproveRetiredApplication
            'Session("AllowToPostRetiredApplicationtoPayroll") = objUserPrivilege._AllowToPostRetiredApplicationtoPayroll
            ''Pinkal (27-Apr-2021) -- End


            ''Hemant (18 May 2021) -- Start
            ''ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
            'Session("AllowToApproveRejectTrainingCompletion") = objUserPrivilege._AllowToApproveRejectTrainingCompletion
            'Session("AllowToAddAttendedTraining") = objUserPrivilege._AllowToAddAttendedTraining
            ''Hemant (18 May 2021) -- End

            ''Hemant (25 May 2021) -- Start
            ''ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
            'Session("AllowToMarkTrainingAsComplete") = objUserPrivilege._AllowToMarkTrainingAsComplete
            ''Hemant (25 May 2021) -- End

            ''S.SANDEEP |10-MAR-2022| -- START
            ''ISSUE/ENHANCEMENT : OLD-580
            'Session("AllowToAddRebateForNonEmployee") = objUserPrivilege._AllowToAddRebateForNonEmployee
            ''S.SANDEEP |10-MAR-2022| -- END

            ''Hemant (11 Jul 2022) -- Start            
            ''ENHANCEMENT(NMB) : AC2-720 - Training approver add/edit with User Mapping
            'Session("AllowToViewTrainingApproverEmployeeMappingList") = objUserPrivilege._AllowToViewTrainingApproverEmployeeMappingList
            'Session("AllowToEditTrainingApproverEmployeeMapping") = objUserPrivilege._AllowToEditTrainingApproverEmployeeMapping
            'Session("AllowToDeleteTrainingApproverEmployeeMapping") = objUserPrivilege._AllowToDeleteTrainingApproverEmployeeMapping
            'Session("AllowToSetActiveInactiveTrainingApproverEmployeeMapping") = objUserPrivilege._AllowToSetActiveInactiveTrainingApproverEmployeeMapping
            'Session("AllowToAddTrainingApproverEmployeeMapping") = objUserPrivilege._AllowToAddTrainingApproverEmployeeMapping
            ''Hemant (11 Jul 2022) -- End


            ''Pinkal (27-Oct-2022) -- Start
            ''NMB Loan Module Enhancement.
            'Session("AllowToChangeDeductionPeriodOnLoanApproval") = objUserPrivilege._AllowToChangeDeductionPeriodOnLoanApproval
            ''Pinkal (27-Oct-2022) -- End


            ''Pinkal (23-Nov-2022) -- Start
            ''NMB Loan Module Enhancement.
            'Session("AllowToAccessLoanDisbursementDashboard") = objUserPrivilege._AllowToAccessLoanDisbursementDashboard
            'Session("AllowToGetCRBData") = objUserPrivilege._AllowToGetCRBData
            ''Pinkal (23-Nov-2022) -- End

            ''Pinkal (14-Dec-2022) -- Start
            ''NMB Loan Module Enhancement.
            'Session("AllowToSetDisbursementTranches") = objUserPrivilege._AllowToSetDisbursementTranches
            ''Pinkal (14-Dec-2022) -- End

            ''Pinkal (06-Jan-2023) -- Start
            ''(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            'Session("AllowToViewLoanSchemeRoleMapping") = objUserPrivilege._AllowToViewLoanSchemeRoleMapping
            'Session("AllowToaddLoanSchemeRoleMapping") = objUserPrivilege._AllowToaddLoanSchemeRoleMapping
            'Session("AllowToEditLoanSchemeRoleMapping") = objUserPrivilege._AllowToEditLoanSchemeRoleMapping
            'Session("AllowToDeleteLoanSchemeRoleMapping") = objUserPrivilege._AllowToDeleteLoanSchemeRoleMapping
            ''Pinkal (06-Jan-2023) -- End

            ''Pinkal (20-Feb-2023) -- Start
            ''NMB - Loan Approval Screen Enhancements.
            'Session("AllowToViewLoanExposuresOnLoanApproval") = objUserPrivilege._AllowToViewLoanExposuresOnLoanApproval
            ''Pinkal (20-Feb-2023) -- End
            ''Hemant (26 May 2023) -- Start
            ''ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            'Session("AllowToApproveRejectPaymentBatchPosting") = objUserPrivilege._AllowToApproveRejectPaymentBatchPosting
            ''Hemant (26 May 2023) -- End


            ''Pinkal (26-Jun-2023) -- Start
            ''NMB Enhancement :(A1X-1029) NMB - Staff transfers approval(role based).
            'Session("AllowToViewStaffTransferApproverLevel") = objUserPrivilege._AllowToViewStaffTransferApproverLevel
            'Session("AllowToAddStaffTransferApproverLevel") = objUserPrivilege._AllowToAddStaffTransferApproverLevel
            'Session("AllowToEditStaffTransferApproverLevel") = objUserPrivilege._AllowToEditStaffTransferApproverLevel
            'Session("AllowToDeleteStaffTransferApproverLevel") = objUserPrivilege._AllowToDeleteStaffTransferApproverLevel

            'Session("AllowToViewStaffTransferLevelRoleMapping") = objUserPrivilege._AllowToViewStaffTransferLevelRoleMapping
            'Session("AllowToAddStaffTransferLevelRoleMapping") = objUserPrivilege._AllowToAddStaffTransferLevelRoleMapping
            'Session("AllowToEditStaffTransferLevelRoleMapping") = objUserPrivilege._AllowToEditStaffTransferLevelRoleMapping
            'Session("AllowToDeleteStaffTransferLevelRoleMapping") = objUserPrivilege._AllowToDeleteStaffTransferLevelRoleMapping

            'Session("AllowToViewStaffTransferApprovalList") = objUserPrivilege._AllowToViewStaffTransferApprovalList
            'Session("AllowToApproveStaffTransferApproval") = objUserPrivilege._AllowToApproveStaffTransferApproval
            ''Pinkal (26-Jun-2023) -- End

            ''Hemant (15 Sep 2023) -- Start
            ''ENHANCEMENT(TRA): A1X-1257 - Coaches/coachees nomination request and approval
            'Session("AllowToViewCoachingApproverLevel") = objUserPrivilege._AllowToViewCoachingApproverLevel
            'Session("AllowToAddCoachingApproverLevel") = objUserPrivilege._AllowToAddCoachingApproverLevel
            'Session("AllowToEditCoachingApproverLevel") = objUserPrivilege._AllowToEditCoachingApproverLevel
            'Session("AllowToDeleteCoachingApproverLevel") = objUserPrivilege._AllowToDeleteCoachingApproverLevel
            'Session("AllowToFillCoachingForm") = objUserPrivilege._AllowToFillCoachingForm
            'Session("AllowToApproveCoachingForm") = objUserPrivilege._AllowToApproveCoachingForm
            'Session("AllowToEditCoachingForm") = objUserPrivilege._AllowToEditCoachingForm
            'Session("AllowToDeleteCoachingForm") = objUserPrivilege._AllowToDeleteCoachingForm
            'Session("AllowToViewCoachingForm") = objUserPrivilege._AllowToViewCoachingForm
            'Session("AllowToFillReplacementForm") = objUserPrivilege._AllowToFillReplacementForm
            ''Hemant (15 Sep 2023) -- End
            ''Hemant (13 Oct 2023) -- Start
            ''ENHANCEMENT(TOYOTA): A1X-1356 - Quick MSS dashboard link that redirects to salary analysis info
            'Session("AllowAccessSalaryAnalysis") = objUserPrivilege._AllowAccessSalaryAnalysis
            ''Hemant (13 Oct 2023) -- End


            ''Pinkal (04-Aug-2023) -- Start 
            ''(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'Session("AllowToAddLoanApproverLevel") = objUserPrivilege._AllowToAddLoanApproverLevel
            'Session("AllowToEditLoanApproverLevel") = objUserPrivilege._AllowToEditLoanApproverLevel
            'Session("AllowToDeleteLoanApproverLevel") = objUserPrivilege._AllowToDeleteLoanApproverLevel
            ''Pinkal (04-Aug-2023) -- End


            ''Pinkal (04-Aug-2023) -- Start
            ''(A1X-1165) NMB - Give new privilege to allow an approver to attach documents on the tranche approval page. 
            'Session("AllowToViewLoanTrancheApprovalList") = objUserPrivilege._AllowToViewLoanTrancheApprovalList
            'Session("AllowToApproveLoanTrancheApplication") = objUserPrivilege._AllowToApproveLoanTrancheApplication
            'Session("AllowToAttachDocumentOnLoanTrancheApproval") = objUserPrivilege._AllowToAttachDocumentOnLoanTrancheApproval
            'Session("AllowToChangeDeductionPeriodonLoanTrancheApproval") = objUserPrivilege._AllowToChangeDeductionPeriodonLoanTrancheApproval
            ''Pinkal (04-Aug-2023) -- End

            ''Hemant (10 May 2024) -- Start
            ''ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            'Session("AllowToAttachDocumentOnRolebaseLoanApproval") = objUserPrivilege._AllowToAttachDocumentOnRolebaseLoanApproval
            ''Hemant (10 May 2024) -- End

            ''Pinkal (17-May-2024) -- Start
            ''NMB Enhancement For Mortgage Loan.
            'Session("AllowToAddPrecedentSubsequentRemarks") = objUserPrivilege._AllowToAddPrecedentSubsequentRemarks
            'Session("AllowToPreviewOfferLetterOnLoanApproval") = objUserPrivilege._AllowToPreviewOfferLetterOnLoanApproval
            ''Pinkal (17-May-2024) -- End


            Session("AllowGlobalPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowGlobalPayment).Key))
            Session("DeletePayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeletePayment).Key))
            Session("AllowToViewGlobalVoidPaymentList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewGlobalVoidPaymentList).Key))
            Session("AllowToApprovePayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApprovePayment).Key))
            Session("AllowToVoidApprovedPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToVoidApprovedPayment).Key))
            Session("AllowToViewBatchPostingList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewBatchPostingList).Key))
            Session("AllowToAddBatchPosting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddBatchPosting).Key))
            Session("AllowToEditBatchPosting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditBatchPosting).Key))
            Session("AllowToDeleteBatchPosting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteBatchPosting).Key))
            Session("AllowToPostBatchPostingToED") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPostBatchPostingToED).Key))
            Session("AddPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddPayment).Key))
            Session("AllowToViewPaymentList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPaymentList).Key))
            Session("AddCashDenomination") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddCashDenomination).Key))
            Session("AllowToViewEmpEDList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpEDList).Key))
            Session("AddEarningDeduction") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEarningDeduction).Key))
            Session("EditEarningDeduction") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEarningDeduction).Key))
            Session("AllowToVoidBatchPostingToED") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToVoidBatchPostingToED).Key))
            Session("DeleteEarningDeduction") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEarningDeduction).Key))
            Session("AllowAuthorizePayslipPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowAuthorizePayslipPayment).Key))
            Session("AllowVoidAuthorizedPayslipPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowVoidAuthorizedPayslipPayment).Key))
            Session("AllowToApproveEarningDeduction") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveEarningDeduction).Key))
            Session("AllowToViewSalaryChangeList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewSalaryChangeList).Key))
            Session("AllowToApproveSalaryChange") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveSalaryChange).Key))
            Session("AllowToViewPayrollGroupList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPayrollGroupList).Key))
            Session("AddPayrollGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddPayrollGroup).Key))
            Session("EditPayrollGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditPayrollGroup).Key))
            Session("DeletePayrollGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeletePayrollGroup).Key))
            Session("AllowToViewPayPointList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPayPointList).Key))
            Session("AddPayPoint") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddPayPoint).Key))
            Session("EditPayPoint") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditPayPoint).Key))
            Session("DeletePayPoint") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeletePayPoint).Key))
            Session("AllowToViewBankBranchList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewBankBranchList).Key))
            Session("AddBankBranch") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddBankBranch).Key))
            Session("EditBankBranch") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditBankBranch).Key))
            Session("DeleteBankBranch") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteBankBranch).Key))
            Session("AllowToPostJVOnHolidays") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPostJVOnHolidays).Key))
            Session("AllowtoApproveLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveLoan).Key))
            Session("AllowtoApproveAdvance") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveAdvance).Key))
            Session("AddPendingLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddPendingLoan).Key))
            Session("EditPendingLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditPendingLoan).Key))
            Session("AllowToViewProcessLoanAdvanceList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewProcessLoanAdvanceList).Key))
            Session("AllowToViewProceedingApprovalList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewProceedingApprovalList).Key))
            Session("AllowToViewLoanAdvanceList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanAdvanceList).Key))
            Session("DeleteLoanAdvance") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteLoanAdvance).Key))
            Session("EditLoanAdvance") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditLoanAdvance).Key))
            Session("AllowChangeLoanAvanceStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowChangeLoanAvanceStatus).Key))
            Session("AddLoanAdvancePayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLoanAdvancePayment).Key))
            Session("AddLoanAdvanceReceived") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLoanAdvanceReceived).Key))

            Session("Is_Report_Assigned") = clsArutiReportClass.Is_Report_Assigned(enArutiReport.BBL_Loan_Report, Session("UserId"), Session("CompanyUnkId"))

            Session("DeletePendingLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeletePendingLoan).Key))
            Session("AllowAssignPendingLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowAssignPendingLoan).Key))
            Session("AllowChangePendingLoanAdvanceStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowChangePendingLoanAdvanceStatus).Key))
            Session("AddLoanAdvancePayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLoanAdvancePayment).Key))
            Session("EditLoanAdvancePayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditLoanAdvancePayment).Key))
            Session("DeleteLoanAdvancePayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteLoanAdvancePayment).Key))
            Session("AddLoanAdvanceReceived") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLoanAdvanceReceived).Key))
            Session("EditLoanAdvanceReceived") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditLoanAdvanceReceived).Key))
            Session("DeleteLoanAdvanceReceived") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteLoanAdvanceReceived).Key))
            Session("AllowtoApproveLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveLoan).Key))
            Session("AllowAssignPendingLoan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowAssignPendingLoan).Key))
            Session("AllowToCancelApprovedLoanApp") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelApprovedLoanApp).Key))
            Session("AddSavingsPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddSavingsPayment).Key))
            Session("EditSavingsPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditSavingsPayment).Key))
            Session("DeleteSavingsPayment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteSavingsPayment).Key))
            Session("AllowToViewEmployeeSavingsList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmployeeSavingsList).Key))
            Session("AddEmployeeSavings") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployeeSavings).Key))
            Session("EditEmployeeSavings") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeSavings).Key))
            Session("DeleteEmployeeSavings") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeSavings).Key))
            Session("AllowChangeSavingStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowChangeSavingStatus).Key))
            Session("AddSavingsDeposit") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddSavingsDeposit).Key))
            Session("EditSavingsDeposit") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditSavingsDeposit).Key))
            Session("DeleteSavingsDeposit") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteSavingsDeposit).Key))
            Session("AddDependant") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddDependant).Key))
            Session("EditDependant") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditDependant).Key))
            Session("DeleteDependant") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteDependant).Key))
            Session("ViewDependant") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpDependantsList).Key))
            Session("AddEmployeeAssets") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployeeAssets).Key))
            Session("EditEmployeeAssets") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeAssets).Key))
            Session("DeleteEmployeeAssets") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeAssets).Key))
            Session("ViewCompanyAssetList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCompanyAssetList).Key))
            Session("AddEmployeeSkill") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployeeSkill).Key))
            Session("EditEmployeeSkill") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeSkill).Key))
            Session("DeleteEmployeeSkill") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeSkill).Key))
            Session("ViewEmpSkillList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpSkillList).Key))
            Session("AddEmployeeQualification") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployeeQualification).Key))
            Session("EditEmployeeQualification") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeQualification).Key))
            Session("DeleteEmployeeQualification") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeQualification).Key))
            Session("ViewEmpQualificationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpQualificationList).Key))
            Session("AddEmployeeReferee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployeeReferee).Key))
            Session("EditEmployeeReferee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeReferee).Key))
            Session("DeleteEmployeeReferee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeReferee).Key))
            Session("ViewEmpReferenceList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpReferenceList).Key))
            Session("AddEmployeeExperience") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployeeExperience).Key))
            Session("EditEmployeeExperience") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeExperience).Key))
            Session("DeleteEmployeeExperience") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeExperience).Key))
            Session("ViewEmpExperienceList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpExperienceList).Key))
            Session("AddEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddEmployee).Key))
            Session("EditEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployee).Key))
            Session("DeleteEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployee).Key))
            Session("ViewEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpList).Key))
            Session("SetReinstatementdate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetEmpReinstatementDate).Key))
            Session("ChangeConfirmationDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeConfirmationDate).Key))
            Session("ChangeAppointmentDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeAppointmentDate).Key))
            Session("SetEmployeeBirthDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetEmployeeBirthDate).Key))
            Session("SetEmpSuspensionDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetEmpSuspensionDate).Key))
            Session("SetEmpProbationDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetEmpProbationDate).Key))
            Session("SetEmploymentEndDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetEmploymentEndDate).Key))
            Session("SetLeavingDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetLeavingDate).Key))
            Session("ChangeRetirementDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeRetirementDate).Key))
            Session("ViewScale") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowTo_View_Scale).Key))
            Session("ViewBenefitList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpBenefitList).Key))
            Session("AllowtoChangeBranch") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeBranch).Key))
            Session("AllowtoChangeDepartmentGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeDepartmentGroup).Key))
            Session("AllowtoChangeDepartment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeDepartment).Key))
            Session("AllowtoChangeSectionGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeSectionGroup).Key))
            Session("AllowtoChangeSection") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeSection).Key))
            Session("AllowtoChangeUnitGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeUnitGroup).Key))
            Session("AllowtoChangeUnit") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeUnit).Key))
            Session("AllowtoChangeTeam") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeTeam).Key))
            Session("AllowtoChangeJobGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeJobGroup).Key))
            Session("AllowtoChangeJob") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeJob).Key))
            Session("AllowtoChangeGradeGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeGradeGroup).Key))
            Session("AllowtoChangeGrade") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeGrade).Key))
            Session("AllowtoChangeGradeLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeGradeLevel).Key))
            Session("AllowtoChangeClassGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeClassGroup).Key))
            Session("AllowtoChangeClass") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeClass).Key))
            Session("AllowtoChangeCostCenter") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeCostCenter).Key))
            Session("AllowtoChangeCompanyEmail") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeCompanyEmail).Key))
            Session("AllowToAddEditPhoto") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddEditPhoto).Key))
            Session("AllowToDeletePhoto") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeletePhoto).Key))
            Session("AllowToViewScale") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowTo_View_Scale).Key))
            Session("AllowToChangeEOCLeavingDateOnClosedPeriod") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEOCLeavingDateOnClosedPeriod).Key))
            Session("AllowToPostFlexcubeJVToOracle") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPostFlexcubeJVToOracle).Key))
            Session("AllowToViewPaidAmount") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPaidAmount).Key))
            Session("AllowToChangeAppointmentDateOnClosedPeriod") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeAppointmentDateOnClosedPeriod).Key))
            Session("AllowToChangeEmpTransfers") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEmpTransfers).Key))
            Session("AllowToChangeEmpRecategorize") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEmpRecategorize).Key))
            Session("AllowToChangeEmpWorkPermit") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEmpWorkPermit).Key))
            Session("AddLeaveExpense") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLeaveExpense).Key))
            Session("EditLeaveExpense") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditLeaveExpense).Key))
            Session("DeleteLeaveExpense") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteLeaveExpense).Key))
            Session("AddLeaveForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLeaveForm).Key))
            Session("EditLeaveForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditLeaveForm).Key))
            Session("DeleteLeaveForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteLeaveForm).Key))
            Session("ViewLeaveFormList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLeaveFormList).Key))
            Session("AllowToCancelLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelLeave).Key))
            Session("ViewLeaveProcessList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLeaveProcessList).Key))
            Session("ChangeLeaveFormStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowChangeLeaveFormStatus).Key))
            Session("AddLeaveAllowance") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddLeaveAllowance).Key))
            Session("AddPlanLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddPlanLeave).Key))
            Session("EditPlanLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditPlanLeave).Key))
            Session("DeletePlanLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeletePlanLeave).Key))
            Session("ViewLeavePlannerList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLeavePlannerList).Key))
            Session("ViewPlannedLeaveViewer") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewPlannedLeaveViewer).Key))
            Session("ViewLeaveViewer") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLeaveViewer).Key))
            Session("AllowToCancelLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelLeave).Key))
            Session("AllowToCancelPreviousDateLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelPreviousDateLeave).Key))
            Session("AllowtoMigrateLeaveApprovers") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoMigrateLeaveApprover).Key))
            Session("AllowIssueLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowIssueLeave).Key))
            Session("AllowToViewLeaveApproverList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLeaveApproverList).Key))
            Session("AddLeaveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddLeaveApprover).Key))
            Session("EditLeaveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditLeaveApprover).Key))
            Session("DeleteLeaveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteLeaveApprover).Key))
            Session("AllowToAssignIssueUsertoEmp") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAssignIssueUsertoEmp).Key))
            Session("AllowToMigrateIssueUser") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMigrateIssueUser).Key))
            Session("AllowtoEndELC") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEndLeaveCycle).Key))
            Session("AllowMapApproverWithUser") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowMapApproverWithUser).Key))
            Session("AllowtoMapLeaveType") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoMapLeaveType).Key))
            Session("AllowtoApproveLeave") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveLeave).Key))
            Session("AllowPrintLeaveForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowPrintLeaveForm).Key))
            Session("AllowPreviewLeaveForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowPreviewLeaveForm).Key))
            Session("AllowSetleaveActiveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetLeaveActiveApprover).Key))
            Session("AllowSetleaveInactiveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetLeaveInactiveApprover).Key))
            Session("AllowToAddEditDeleteGlobalTimesheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddEditDeleteGlobalTimesheet).Key))
            Session("AddMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddMedicalClaim).Key))
            Session("EditMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditMedicalClaim).Key))
            Session("DeleteMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteMedicalClaim).Key))
            Session("AllowToExportMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToExportMedicalClaim).Key))
            Session("AllowToCancelExportedMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelExportedMedicalClaim).Key))
            Session("AllowToAddMedicalSickSheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddMedicalSickSheet).Key))
            Session("AllowToEditMedicalSickSheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditMedicalSickSheet).Key))
            Session("AllowToDeleteMedicalSickSheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteMedicalSickSheet).Key))
            Session("AllowToPrintMedicalSickSheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPrintMedicalSickSheet).Key))
            Session("RevokeUserAccessOnSicksheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._RevokeUserAccessOnSicksheet).Key))
            Session("AllowToViewEmpSickSheetList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpSickSheetList).Key))
            Session("AllowToViewMedicalClaimList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewMedicalClaimList).Key))
            Session("AllowToSaveMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSaveMedicalClaim).Key))
            Session("AllowToFinalSaveMedicalClaim") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToFinalSaveMedicalClaim).Key))
            Session("AllowtoSubmitGoalsforApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSubmitGoalsforApproval).Key))
            Session("AllowtoApproveRejectGoalsPlanning") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveRejectGoalsPlanning).Key))
            Session("Allow_UnlockFinalSaveBSCPlanning") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockFinalSavedGoals).Key))
            Session("AllowtoPerformAssessorReviewerMigration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoPerformAssessorReviewerMigration).Key))
            Session("AllowtoAddSelfEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddSelfEvaluation).Key))
            Session("AllowtoEditSelfEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditSelfEvaluation).Key))
            Session("AllowtoDeleteSelfEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteSelfEvaluation).Key))
            Session("AllowtoUnlockcommittedSelfEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockcommittedSelfEvaluation).Key))
            Session("AllowtoViewSelfEvaluationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewSelfEvaluationList).Key))
            Session("AllowtoAddAssessorEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddAssessorEvaluation).Key))
            Session("AllowtoEditAssessorEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditAssessorEvaluation).Key))
            Session("AllowtoDeleteAssessorEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteAssessorEvaluation).Key))
            Session("AllowtoUnlockcommittedAssessorEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockcommittedAssessorEvaluation).Key))
            Session("AllowtoViewAssessorEvaluationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewAssessorEvaluationList).Key))
            Session("AllowtoAddReviewerEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddReviewerEvaluation).Key))
            Session("AllowtoEditReviewerEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditReviewerEvaluation).Key))
            Session("AllowtoDeleteReviewerEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteReviewerEvaluation).Key))
            Session("AllowtoUnlockcommittedReviewerEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockcommittedReviewerEvaluation).Key))
            Session("AllowtoViewReviewerEvaluationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewReviewerEvaluationList).Key))
            Session("AllowtoAddAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddAllocationGoals).Key))
            Session("AllowtoEditAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditAllocationGoals).Key))
            Session("AllowtoDeleteAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteAllocationGoals).Key))
            Session("AllowtoCommitAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoCommitAllocationGoals).Key))
            Session("AllowtoUnlockcommittedAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockcommittedAllocationGoals).Key))
            Session("AllowtoPerformGlobalAssignAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoPerformGlobalAssignAllocationGoals).Key))
            Session("AllowtoUpdatePercentCompletedAllocationGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUpdatePercentCompletedAllocationGoals).Key))
            Session("AllowtoViewAllocationGoalsList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewAllocationGoalsList).Key))
            Session("AllowtoAddEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddEmployeeGoals).Key))
            Session("AllowtoEditEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditEmployeeGoals).Key))
            Session("AllowtoDeleteEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteEmployeeGoals).Key))
            Session("AllowtoPerformGlobalAssignEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoPerformGlobalAssignEmployeeGoals).Key))
            Session("AllowtoUpdatePercentCompletedEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUpdatePercentCompletedEmployeeGoals).Key))
            Session("AllowtoViewEmployeeGoalsList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewEmployeeGoalsList).Key))
            Session("AllowtoViewPerformanceEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewPerformanceEvaluation).Key))
            Session("AllowtoViewCompanyGoalsList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewCompanyGoalsList).Key))
            Session("AllowtoAddAssignedCompetencies") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddAssignedCompetencies).Key))
            Session("AllowtoEditAssignedCompetencies") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditAssignedCompetencies).Key))
            Session("AllowtoDeleteAssignedCompetencies") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteAssignedCompetencies).Key))
            Session("AllowtoAddCustomItems") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddCustomItems).Key))
            Session("AllowtoEditCustomItems") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditCustomItems).Key))
            Session("AllowtoDeleteCustomItems") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteCustomItems).Key))
            Session("AllowToApproveGoalsAccomplishment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveGoalsAccomplishment).Key))
            Session("AllowToApproveEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveEmployee).Key))
            Session("AllowtoApproveApplicantEligibility") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveApplicantEligibility).Key))
            Session("AllowtoDisapproveApplicantEligibility") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDisapproveApplicantEligibility).Key))
            Session("AllowtoViewInterviewAnalysisList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewInterviewAnalysisList).Key))
            Session("AllowToViewShortListApplicants") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewShortListApplicants).Key))
            Session("AllowToApproveApplicantFilter") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveApplicantFilter).Key))
            Session("AllowToRejectApplicantFilter") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToRejectApplicantFilter).Key))
            Session("AllowToApproveFinalShortListedApplicant") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveFinalShortListedApplicant).Key))
            Session("AllowToDisapproveFinalShortListedApplicant") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDisapproveFinalShortListedApplicant).Key))
            Session("AllowToViewStaffRequisitionList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewStaffRequisitionList).Key))
            Session("AllowToAddStaffRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddStaffRequisition).Key))
            Session("AllowToEditStaffRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditStaffRequisition).Key))
            Session("AllowToDeleteStaffRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteStaffRequisition).Key))
            Session("AllowToViewStaffRequisitionApprovals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewStaffRequisitionApprovals).Key))
            Session("AllowToCancelStaffRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelStaffRequisition).Key))
            Session("AllowToApproveStaffRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveStaffRequisition).Key))
            Session("AllowToViewLevel1EvaluationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLevel1EvaluationList).Key))
            Session("AllowToAddLevelIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddLevelIEvaluation).Key))
            Session("AllowToEditLevelIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditLevelIEvaluation).Key))
            Session("AllowToDeleteLevelIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteLevelIEvaluation).Key))
            Session("AllowToSave_CompleteLevelIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSave_CompleteLevelIEvaluation).Key))
            Session("AllowToPrintLevelIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPrintLevelIEvaluation).Key))
            Session("AllowToPreviewLevelIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPreviewLevelIEvaluation).Key))
            Session("AllowToViewTrainingEnrollmentList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTrainingEnrollmentList).Key))
            Session("AddTrainingEnrollment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddTrainingEnrollment).Key))
            Session("EditTrainingEnrollment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditTrainingEnrollment).Key))
            Session("DeleteTrainingEnrollment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteTrainingEnrollment).Key))
            Session("AllowToAddLevelIIIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddLevelIIIEvaluation).Key))
            Session("AllowToEditLevelIIIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditLevelIIIEvaluation).Key))
            Session("AllowToDeleteLevelIIIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteLevelIIIEvaluation).Key))
            Session("AllowToSave_CompleteLevelIIIEvaluation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSave_CompleteLevelIIIEvaluation).Key))
            Session("AllowToViewLevel3EvaluationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLevel3EvaluationList).Key))
            Session("AddAssetDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._Allow_AddAssetDeclaration).Key))
            Session("EditAssetDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._Allow_EditAssetDeclaration).Key))
            Session("DeleteAssetDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._Allow_DeleteAssetDeclaration).Key))
            Session("FinalSaveAssetDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._Allow_FinalSaveAssetDeclaration).Key))
            Session("UnlockFinalSaveAssetDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._Allow_UnlockFinalSaveAssetDeclaration).Key))
            Session("ViewAssetsDeclarationList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewAssetsDeclarationList).Key))
            Session("AllowtoUnlockEmployeeForAssetDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockEmployeeForAssetDeclaration).Key))
            Session("AllowtoUnlockEmployeeForNonDisclosureDeclaration") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoUnlockEmployeeForNonDisclosureDeclaration).Key))
            Session("AllowtoAddExpenseApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddExpenseApprover).Key))
            Session("AllowtoEditExpenseApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditExpenseApprover).Key))
            Session("AllowtoDeleteExpenseApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteExpenseApprover).Key))
            Session("AllowtoViewExpenseApproverList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewExpenseApproverList).Key))
            Session("AllowtoMigrateExpenseApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoMigrateExpenseApprover).Key))
            Session("AllowtoSwapExpenseApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSwapExpenseApprover).Key))
            Session("AllowtoAddClaimExpenseForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddClaimExpenseForm).Key))
            Session("AllowtoEditClaimExpenseForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoEditClaimExpenseForm).Key))
            Session("AllowtoDeleteClaimExpenseForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteClaimExpenseForm).Key))
            Session("AllowtoViewClaimExpenseFormList ") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewClaimExpenseFormList).Key))
            Session("AllowtoCancelClaimExpenseForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoCancelClaimExpenseForm).Key))
            Session("AllowtoProcessClaimExpenseForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoProcessClaimExpenseForm).Key))
            Session("AllowtoViewProcessClaimExpenseFormList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewProcessClaimExpenseFormList).Key))
            Session("AllowtoPostClaimExpenseToPayroll") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoPostClaimExpenseToPayroll).Key))
            Session("AllowSetClaimActiveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetClaimActiveApprover).Key))
            Session("AllowSetClaimInactiveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSetClaimInactiveApprover).Key))
            Session("AllowtoChangePreassignedCompetencies") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangePreassignedCompetencies).Key))
            Session("AllowToSwapLeaveApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSwapLeaveApprover).Key))
            Session("AllowToMigrateBudgetTimesheetApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMigrateBudgetTimesheetApprover).Key))
            Session("AllowToMapBudgetTimesheetApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMapBudgetTimesheetApprover).Key))
            Session("AllowToViewBudgetTimesheetApproverList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewBudgetTimesheetApproverList).Key))
            Session("AllowToAddBudgetTimesheetApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddBudgetTimesheetApprover).Key))
            Session("AllowToEditBudgetTimesheetApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditBudgetTimesheetApprover).Key))
            Session("AllowToDeleteBudgetTimesheetApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteBudgetTimesheetApprover).Key))
            Session("AllowToSetBudgetTimesheetApproverAsActive") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetBudgetTimesheetApproverAsActive).Key))
            Session("AllowToSetBudgetTimesheetApproverAsInActive") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetBudgetTimesheetApproverAsInActive).Key))
            Session("AllowToViewEmployeeBudgetTimesheetList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmployeeBudgetTimesheetList).Key))
            Session("AllowToAddEmployeeBudgetTimesheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddEmployeeBudgetTimesheet).Key))
            Session("AllowToEditEmployeeBudgetTimesheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditEmployeeBudgetTimesheet).Key))
            Session("AllowToDeleteEmployeeBudgetTimesheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteEmployeeBudgetTimesheet).Key))
            Session("AllowToCancelEmployeeBudgetTimesheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelEmployeeBudgetTimesheet).Key))
            Session("AllowToViewPendingEmpBudgetTimesheetSubmitForApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPendingEmpBudgetTimesheetSubmitForApproval).Key))
            Session("AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval).Key))
            Session("AllowToSubmitForApprovalForPendingEmpBudgetTimesheet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSubmitForApprovalForPendingEmpBudgetTimesheet).Key))
            Session("AllowToViewEmployeeBudgetTimesheetApprovalList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmployeeBudgetTimesheetApprovalList).Key))
            Session("AllowToChangeEmployeeBudgetTimesheetStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEmployeeBudgetTimesheetStatus).Key))
            Session("AllowToAddLoanApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddLoanApprover).Key))
            Session("AllowToEditLoanApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditLoanApprover).Key))
            Session("AllowToDeleteLoanApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteLoanApprover).Key))
            Session("AllowToSetActiveInactiveLoanApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetActiveInactiveLoanApprover).Key))
            Session("AllowToViewLoanApproverLevelList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanApproverLevelList).Key))
            Session("AllowToViewLoanApproverList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanApproverList).Key))
            Session("AllowToProcessGlobalLoanApprove") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToProcessGlobalLoanApprove).Key))
            Session("AllowToProcessGlobalLoanAssign") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToProcessGlobalLoanAssign).Key))
            Session("AllowToViewLoanApprovalList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanApprovalList).Key))
            Session("AllowToChangeLoanStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeLoanStatus).Key))
            Session("AllowToTransferLoanApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToTransferLoanApprover).Key))
            Session("AllowToSwapLoanApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSwapLoanApprover).Key))
            Session("AllowToEditTransferEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditTransferEmployeeDetails).Key))
            Session("AllowToDeleteTransferEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteTransferEmployeeDetails).Key))
            Session("AllowToEditRecategorizeEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditRecategorizeEmployeeDetails).Key))
            Session("AllowToDeleteRecategorizeEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteRecategorizeEmployeeDetails).Key))
            Session("AllowToEditProbationEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditProbationEmployeeDetails).Key))
            Session("AllowToDeleteProbationEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteProbationEmployeeDetails).Key))
            Session("AllowToEditConfirmationEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditConfirmationEmployeeDetails).Key))
            Session("AllowToDeleteConfirmationEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteConfirmationEmployeeDetails).Key))
            Session("AllowToEditSuspensionEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditSuspensionEmployeeDetails).Key))
            Session("AllowToDeleteSuspensionEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteSuspensionEmployeeDetails).Key))
            Session("AllowToEditTerminationEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditTerminationEmployeeDetails).Key))
            Session("AllowToDeleteTerminationEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteTerminationEmployeeDetails).Key))
            Session("AllowToEditRetiredEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditRetiredEmployeeDetails).Key))
            Session("AllowToDeleteRetiredEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteRetiredEmployeeDetails).Key))
            Session("AllowToRehireEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToRehireEmployee).Key))
            Session("AllowToEditWorkPermitEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditWorkPermitEmployeeDetails).Key))
            Session("AllowToDeleteWorkPermitEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteWorkPermitEmployeeDetails).Key))
            Session("AllowToEditRehireEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditRehireEmployeeDetails).Key))
            Session("AllowToDeleteRehireEmployeeDetails") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteRehireEmployeeDetails).Key))
            Session("AllowToSetEmployeeExemptionDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetEmployeeExemptionDate).Key))
            Session("AllowToEditEmployeeExemptionDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditEmployeeExemptionDate).Key))
            Session("AllowToDeleteEmployeeExemptionDate") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteEmployeeExemptionDate).Key))
            Session("AddSalaryIncrement") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AddSalaryIncrement).Key))
            Session("AllowToAssignBenefitGroup") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAssignBenefitGroup).Key))
            Session("AllowToChangeEmpTransfers") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEmpTransfers).Key))
            Session("AllowToChangeEmpRecategorize") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeEmpRecategorize).Key))
            Session("AllowtoChangeCostCenter") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoChangeCostCenter).Key))
            Session("EditEmployeeCostCenter") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._EditEmployeeCostCenter).Key))
            Session("DeleteEmployeeCostCenter") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._DeleteEmployeeCostCenter).Key))
            Session("AllowToSubmitForGoalAccomplished") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSubmitForGoalAccomplished).Key))
            Session("AllowToViewAssignedShiftList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewAssignedShiftList).Key))
            Session("AllowToViewAssignedPolicyList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewAssignedPolicyList).Key))
            Session("AllowToAddShiftPolicyAssignment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddShiftPolicyAssignment).Key))
            Session("AllowToDeleteAssignedPolicy") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteAssignedPolicy).Key))
            Session("AllowToDeleteAssignedShift") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteAssignedShift).Key))
            Session("AllowToPerformEligibleOperation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPerformEligibleOperation).Key))
            Session("AllowToPerformNotEligibleOperation") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPerformNotEligibleOperation).Key))
            Session("AllowToViewFinalApplicantList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewFinalApplicantList).Key))
            Session("AllowtoAddEmployeeSignature") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoAddEmployeeSignature).Key))
            Session("AllowtoDeleteEmployeeSignature") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoDeleteEmployeeSignature).Key))
            Session("AllowtoSeeEmployeeSignature") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoSeeEmployeeSignature).Key))
            Session("AllowToViewResolutionStepList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewGrievanceResolutionStepList).Key))
            Session("AllowToAddGrievanceResolutionStep") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddGrievanceResolutionStep).Key))
            Session("AllowToEditGrievanceResolutionStep") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditGrievanceResolutionStep).Key))
            Session("AllowToDeleteGrievanceResolutionStep") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteGrievanceResolutionStep).Key))
            Session("AllowToAddGrievanceApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddGrievanceApproverLevel).Key))
            Session("AllowToEditGrievanceApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditGrievanceApproverLevel).Key))
            Session("AllowToDeleteGrievanceApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteGrievanceApproverLevel).Key))
            Session("AllowToViewGrievanceApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewGrievanceApproverLevel).Key))
            Session("AllowToAddGrievanceApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddGrievanceApprover).Key))
            Session("AllowToEditGrievanceApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditGrievanceApprover).Key))
            Session("AllowToDeleteGrievanceApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteGrievanceApprover).Key))
            Session("AllowToViewGrievanceApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewGrievanceApprover).Key))
            Session("AllowToActivateGrievanceApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToActivateGrievanceApprover).Key))
            Session("AllowToInActivateGrievanceApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToInActivateGrievanceApprover).Key))
            Session("AllowToViewGrievanceResolutionStepList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewGrievanceResolutionStepList).Key))
            Session("AllowToAddGrievanceResolutionStep") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddGrievanceResolutionStep).Key))
            Session("AllowToEditGrievanceResolutionStep") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditGrievanceResolutionStep).Key))
            Session("AllowToDeleteGrievanceResolutionStep") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteGrievanceResolutionStep).Key))
            Session("AllowToAddTrainingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddTrainingApproverLevel).Key))
            Session("AllowToEditTrainingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditTrainingApproverLevel).Key))
            Session("AllowToDeleteTrainingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteTrainingApproverLevel).Key))
            Session("AllowToViewTrainingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTrainingApproverLevel).Key))
            Session("AllowToAddTrainingApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddTrainingApprover).Key))
            Session("AllowToDeleteTrainingApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteTrainingApprover).Key))
            Session("AllowToViewTrainingApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTrainingApprover).Key))
            Session("AllowToApproveTrainingRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveTrainingRequisition).Key))
            Session("AllowToActivateTrainingApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToActivateTrainingApprover).Key))
            Session("AllowToDeactivateTrainingApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeactivateTrainingApprover).Key))
            Session("AllowToViewOTRequisitionApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewOTRequisitionApproverLevel).Key))
            Session("AllowToAddOTRequisitionApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddOTRequisitionApproverLevel).Key))
            Session("AllowToEditOTRequisitionApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditOTRequisitionApproverLevel).Key))
            Session("AllowToDeleteOTRequisitionApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteOTRequisitionApproverLevel).Key))
            Session("AllowToAddOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddOTRequisitionApprover).Key))
            Session("AllowToEditOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditOTRequisitionApprover).Key))
            Session("AllowToDeleteOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteOTRequisitionApprover).Key))
            Session("AllowToViewOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewOTRequisitionApprover).Key))
            Session("AllowtoApproveOTRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoApproveOTRequisition).Key))
            Session("AllowtoViewOTRequisitionApprovalList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoViewOTRequisitionApprovalList).Key))
            Session("AllowToViewEmpOTAssignment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewEmpOTAssignment).Key))
            Session("AllowToAddEmpOTAssignment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddEmpOTAssignment).Key))
            Session("AllowToDeleteEmpOTAssignment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteEmpOTAssignment).Key))
            Session("AllowToCancelEmpOTRequisition") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToCancelEmpOTRequisition).Key))
            Session("AllowToPostOTRequisitionToPayroll") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPostOTRequisitionToPayroll).Key))
            Session("AllowToMigrateOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMigrateOTRequisitionApprover).Key))
            Session("AllowToDeletePercentCompletedEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeletePercentCompletedEmployeeGoals).Key))
            Session("AllowToViewPercentCompletedEmployeeGoals") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPercentCompletedEmployeeGoals).Key))
            Session("AllowToApproveRejectEmployeeReferences") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectEmployeeReferences).Key))
            Session("AllowToApproveRejectEmployeeMembership") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectEmployeeMembership).Key))
            Session("AllowToSetDependentActive") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetDependentActive).Key))
            Session("AllowToSetDependentInactive") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetDependentInactive).Key))
            Session("AllowToDeleteDependentStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteDependentStatus).Key))
            Session("AllowToViewDependentStatus") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewDependentStatus).Key))
            Session("AllowToViewCalibrationApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCalibrationApproverLevel).Key))
            Session("AllowToAddCalibrationApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddCalibrationApproverLevel).Key))
            Session("AllowToEditCalibrationApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditCalibrationApproverLevel).Key))
            Session("AllowToDeleteCalibrationApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteCalibrationApproverLevel).Key))
            Session("AllowToViewCalibrationApproverMaster") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCalibrationApproverMaster).Key))
            Session("AllowToAddCalibrationApproverMaster") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddCalibrationApproverMaster).Key))
            Session("AllowToActivateCalibrationApproverMaster") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToActivateCalibrationApproverMaster).Key))
            Session("AllowToDeleteCalibrationApproverMaster") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteCalibrationApproverMaster).Key))
            Session("AllowToDeactivateCalibrationApproverMaster") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeactivateCalibrationApproverMaster).Key))
            Session("AllowToApproveRejectCalibratedScore") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectCalibratedScore).Key))
            Session("AllowToEditOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditOTRequisitionApprover).Key))
            Session("AllowToDeleteOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteOTRequisitionApprover).Key))
            Session("AllowToActivateOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToActivateOTRequisitionApprover).Key))
            Session("AllowToInActivateOTRequisitionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToInActivateOTRequisitionApprover).Key))
            Session("AllowToEditCalibratedScore") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditCalibratedScore).Key))
            Session("AllowToViewBudgetTimesheetExemptEmployeeList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewBudgetTimesheetExemptEmployeeList).Key))
            Session("AllowToAddBudgetTimesheetExemptEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddBudgetTimesheetExemptEmployee).Key))
            Session("AllowToDeleteBudgetTimesheetExemptEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteBudgetTimesheetExemptEmployee).Key))
            Session("AllowtoCalibrateProvisionalScore") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowtoCalibrateProvisionalScore).Key))
            Session("AllowToViewTrainingNeedForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTrainingNeedForm).Key))
            Session("AllowToAddTrainingNeedForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddTrainingNeedForm).Key))
            Session("AllowToEditTrainingNeedForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditTrainingNeedForm).Key))
            Session("AllowToDeleteTrainingNeedForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteTrainingNeedForm).Key))
            Session("AllowToUnlockEmployeeInPlanning") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUnlockEmployeeInPlanning).Key))
            Session("AllowToUnlockEmployeeInAssessment") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUnlockEmployeeInAssessment).Key))
            Session("AllowToViewCalibratorList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCalibratorList).Key))
            Session("AllowToAddCalibrator") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddCalibrator).Key))
            Session("AllowToMakeCalibratorActive") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMakeCalibratorActive).Key))
            Session("AllowToDeleteCalibrator") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteCalibrator).Key))
            Session("AllowToMakeCalibratorInactive") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMakeCalibratorInactive).Key))
            Session("AllowToEditCalibrator") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditCalibrator).Key))
            Session("AllowToEditCalibrationApproverMaster") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditCalibrationApproverMaster).Key))
            Session("AllowToAddPotentialTalentEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddPotentialTalentEmployee).Key))
            Session("AllowToApproveRejectTalentPipelineEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectTalentPipelineEmployee).Key))
            Session("AllowToMoveApprovedEmployeeToQulified") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMoveApprovedEmployeeToQulified).Key))
            Session("AllowToRemoveTalentProcess") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToRemoveTalentProcess).Key))
            Session("AllowToViewPotentialTalentEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPotentialTalentEmployee).Key))
            Session("AllowToAddPotentialSuccessionEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddPotentialSuccessionEmployee).Key))
            Session("AllowToApproveRejectSuccessionPipelineEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectSuccessionPipelineEmployee).Key))
            Session("AllowToMoveApprovedEmployeeToQulifiedSuccession") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMoveApprovedEmployeeToQulifiedSuccession).Key))
            Session("AllowToViewPotentialSuccessionEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewPotentialSuccessionEmployee).Key))
            Session("AllowToRemoveSuccessionProcess") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToRemoveSuccessionProcess).Key))
            Session("AllowToSendNotificationToTalentScreener") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSendNotificationToTalentScreener).Key))
            Session("AllowToSendNotificationToTalentEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSendNotificationToTalentEmployee).Key))
            Session("AllowForTalentScreeningProcess") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowForTalentScreeningProcess).Key))
            Session("AllowToSendNotificationToSuccessionScreener") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSendNotificationToSuccessionScreener).Key))
            Session("AllowToSendNotificationToSuccessionEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSendNotificationToSuccessionEmployee).Key))
            Session("AllowForSuccessionScreeningProcess") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowForSuccessionScreeningProcess).Key))
            Session("AllowToSendNotificationToTalentApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSendNotificationToTalentApprover).Key))
            Session("AllowToSendNotificationToSuccessionApprover") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSendNotificationToSuccessionApprover).Key))
            Session("AllowToViewLearningAndDevelopmentPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLearningAndDevelopmentPlan).Key))
            Session("AllowToAddPersonalAnalysisGoal") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddPersonalAnalysisGoal).Key))
            Session("AllowToEditPersonalAnalysisGoal") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditPersonalAnalysisGoal).Key))
            Session("AllowToDeletePersonalAnalysisGoal") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeletePersonalAnalysisGoal).Key))
            Session("AllowToAddDevelopmentActionPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddDevelopmentActionPlan).Key))
            Session("AllowToEditDevelopmentActionPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditDevelopmentActionPlan).Key))
            Session("AllowToDeleteDevelopmentActionPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteDevelopmentActionPlan).Key))
            Session("AllowToAddUpdateProgressForDevelopmentActionPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddUpdateProgressForDevelopmentActionPlan).Key))
            Session("AllowToViewActionPlanAssignedTrainingCourses") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewActionPlanAssignedTrainingCourses).Key))
            Session("AllowToAddCommentForDevelopmentActionPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddCommentForDevelopmentActionPlan).Key))
            Session("AllowToUnlockPDPForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUnlockPDPForm).Key))
            Session("AllowToLockPDPForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToLockPDPForm).Key))
            Session("AllowToUndoUpdateProgressForDevelopmentActionPlan") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUndoUpdateProgressForDevelopmentActionPlan).Key))
            Session("AllowToNominateEmployeeForSuccession") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToNominateEmployeeForSuccession).Key))
            Session("AllowToModifyTalentSetting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToModifyTalentSetting).Key))
            Session("AllowToViewTalentSetting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTalentSetting).Key))
            Session("AllowToModifySuccessionSetting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToModifySuccessionSetting).Key))
            Session("AllowToViewSuccessionSetting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewSuccessionSetting).Key))
            Session("AllowToViewTalentScreeningDetail") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTalentScreeningDetail).Key))
            Session("AllowToViewSuccessionScreeningDetail") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewSuccessionScreeningDetail).Key))
            Session("AllowToViewDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewDepartmentalTrainingNeed).Key))
            Session("AllowToAddDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddDepartmentalTrainingNeed).Key))
            Session("AllowToEditDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditDepartmentalTrainingNeed).Key))
            Session("AllowToDeleteDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteDepartmentalTrainingNeed).Key))
            Session("AllowToSubmitForApprovalFromDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSubmitForApprovalFromDepartmentalTrainingNeed).Key))
            Session("AllowToTentativeApproveForDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToTentativeApproveForDepartmentalTrainingNeed).Key))
            Session("AllowToSubmitForApprovalFromTrainingBacklog") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSubmitForApprovalFromTrainingBacklog).Key))
            Session("AllowToFinalApproveDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToFinalApproveDepartmentalTrainingNeed).Key))
            Session("AllowToRejectDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToRejectDepartmentalTrainingNeed).Key))
            Session("AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed).Key))
            Session("AllowToUnlockSubmittedForTrainingBudgetApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUnlockSubmittedForTrainingBudgetApproval).Key))
            Session("AllowToAskForReviewTrainingAsPerAmountSet") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAskForReviewTrainingAsPerAmountSet).Key))
            Session("AllowToUndoApprovedTrainingBudgetApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUndoApprovedTrainingBudgetApproval).Key))
            Session("AllowToUndoRejectedTrainingBudgetApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToUndoRejectedTrainingBudgetApproval).Key))
            Session("AllowToViewBudgetSummaryForDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewBudgetSummaryForDepartmentalTrainingNeed).Key))
            Session("AllowToSetMaxBudgetForDepartmentalTrainingNeed") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetMaxBudgetForDepartmentalTrainingNeed).Key))
            Session("AllowToViewRetireClaimApplication") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewRetireClaimApplication).Key))
            Session("AllowToRetireClaimApplication") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToRetireClaimApplication).Key))
            Session("AllowToEditRetiredClaimApplication") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditRetiredClaimApplication).Key))
            Session("AllowToViewRetiredApplicationApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewRetiredApplicationApproval).Key))
            Session("AllowToApproveRetiredApplication") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRetiredApplication).Key))
            Session("AllowToPostRetiredApplicationtoPayroll") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPostRetiredApplicationtoPayroll).Key))
            Session("AllowToApproveRejectTrainingCompletion") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectTrainingCompletion).Key))
            Session("AllowToAddAttendedTraining") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddAttendedTraining).Key))
            Session("AllowToMarkTrainingAsComplete") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToMarkTrainingAsComplete).Key))
            Session("AllowToAddRebateForNonEmployee") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddRebateForNonEmployee).Key))
            Session("AllowToViewTrainingApproverEmployeeMappingList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewTrainingApproverEmployeeMappingList).Key))
            Session("AllowToEditTrainingApproverEmployeeMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditTrainingApproverEmployeeMapping).Key))
            Session("AllowToDeleteTrainingApproverEmployeeMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteTrainingApproverEmployeeMapping).Key))
            Session("AllowToSetActiveInactiveTrainingApproverEmployeeMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetActiveInactiveTrainingApproverEmployeeMapping).Key))
            Session("AllowToAddTrainingApproverEmployeeMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddTrainingApproverEmployeeMapping).Key))
            Session("AllowToChangeDeductionPeriodOnLoanApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeDeductionPeriodOnLoanApproval).Key))
            Session("AllowToAccessLoanDisbursementDashboard") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAccessLoanDisbursementDashboard).Key))
            Session("AllowToGetCRBData") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToGetCRBData).Key))
            Session("AllowToSetDisbursementTranches") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToSetDisbursementTranches).Key))
            Session("AllowToViewLoanSchemeRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanSchemeRoleMapping).Key))
            Session("AllowToaddLoanSchemeRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToaddLoanSchemeRoleMapping).Key))
            Session("AllowToEditLoanSchemeRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditLoanSchemeRoleMapping).Key))
            Session("AllowToDeleteLoanSchemeRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteLoanSchemeRoleMapping).Key))
            Session("AllowToViewLoanExposuresOnLoanApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanExposuresOnLoanApproval).Key))
            Session("AllowToApproveRejectPaymentBatchPosting") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveRejectPaymentBatchPosting).Key))
            Session("AllowToViewStaffTransferApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewStaffTransferApproverLevel).Key))
            Session("AllowToAddStaffTransferApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddStaffTransferApproverLevel).Key))
            Session("AllowToEditStaffTransferApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditStaffTransferApproverLevel).Key))
            Session("AllowToDeleteStaffTransferApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteStaffTransferApproverLevel).Key))
            Session("AllowToViewStaffTransferLevelRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewStaffTransferLevelRoleMapping).Key))
            Session("AllowToAddStaffTransferLevelRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddStaffTransferLevelRoleMapping).Key))
            Session("AllowToEditStaffTransferLevelRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditStaffTransferLevelRoleMapping).Key))
            Session("AllowToDeleteStaffTransferLevelRoleMapping") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteStaffTransferLevelRoleMapping).Key))
            Session("AllowToViewStaffTransferApprovalList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewStaffTransferApprovalList).Key))
            Session("AllowToApproveStaffTransferApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveStaffTransferApproval).Key))
            Session("AllowToViewCoachingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCoachingApproverLevel).Key))
            Session("AllowToAddCoachingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddCoachingApproverLevel).Key))
            Session("AllowToEditCoachingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditCoachingApproverLevel).Key))
            Session("AllowToDeleteCoachingApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteCoachingApproverLevel).Key))
            Session("AllowToFillCoachingForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToFillCoachingForm).Key))
            Session("AllowToApproveCoachingForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveCoachingForm).Key))
            Session("AllowToEditCoachingForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditCoachingForm).Key))
            Session("AllowToDeleteCoachingForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteCoachingForm).Key))
            Session("AllowToViewCoachingForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewCoachingForm).Key))
            Session("AllowToFillReplacementForm") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToFillReplacementForm).Key))
            Session("AllowAccessSalaryAnalysis") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowAccessSalaryAnalysis).Key))
            Session("AllowToAddLoanApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddLoanApproverLevel).Key))
            Session("AllowToEditLoanApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToEditLoanApproverLevel).Key))
            Session("AllowToDeleteLoanApproverLevel") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToDeleteLoanApproverLevel).Key))
            Session("AllowToViewLoanTrancheApprovalList") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToViewLoanTrancheApprovalList).Key))
            Session("AllowToApproveLoanTrancheApplication") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToApproveLoanTrancheApplication).Key))
            Session("AllowToAttachDocumentOnLoanTrancheApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAttachDocumentOnLoanTrancheApproval).Key))
            Session("AllowToChangeDeductionPeriodonLoanTrancheApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToChangeDeductionPeriodonLoanTrancheApproval).Key))
            Session("AllowToAttachDocumentOnRolebaseLoanApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAttachDocumentOnRolebaseLoanApproval).Key))
            Session("AllowToAddPrecedentSubsequentRemarks") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToAddPrecedentSubsequentRemarks).Key))
            Session("AllowToPreviewOfferLetterOnLoanApproval") = clsDataOpr.GetPrivilegeValue(CInt(Session("UserId")), Session("privilegeDict")(GetPropertyInfo(Function() objUserPrivilege._AllowToPreviewOfferLetterOnLoanApproval).Key))


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objUserPrivilege = Nothing
            'Pinkal (11-Sep-2020) -- End


            Return True

        Catch ex As Exception
            strErrorMsg = "Error in SetUserSessions : " & ex.Message
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objGlobalAccess = Nothing
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Function

    'Nilay (16-Apr-2016) -- Start
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Public Sub SetDateFormat()
        Try

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If Session("DateFormat") IsNot Nothing AndAlso Session("DateSeparator") IsNot Nothing Then
                Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
                NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
                NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture
            End If
            'Pinkal (20-Aug-2020) -- End
        Catch ex As Exception
            Throw New Exception("SetDateFormat:- " & ex.Message)
        End Try
    End Sub
    'Nilay (16-Apr-2016) -- End

    Public Function SetCompanySessions(ByRef strErrorMsg As String, ByVal intCompanyID As Integer, ByVal intLanguageID As Integer) As Boolean
        strErrorMsg = ""
        Dim objDataOperation As New clsDataOperation(True)
        Try
            If Session("mdbname") IsNot Nothing Then
                Dim objMasterData As New clsMasterData
                objMasterData.Update_IDM_Active_Status(intCompanyID, Session("mdbname"))
                objMasterData = Nothing
            End If


            'TODO Change all global objects


            'Pinkal (11-Dec-2018) -- Start
            'Issue - Login page is taking time to login in NMB.

            'gobjConfigOptions = New clsConfigOptions
            'gobjConfigOptions._Companyunkid = intCompanyID
            'ConfigParameter._Object._Companyunkid = intCompanyID
            'Company._Object._Companyunkid = intCompanyID

            'If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Company > ConfigParameter._Object._NoOfCompany Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If

            'Company._Object._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Employee_ForAllCompany > ConfigParameter._Object._NoOfEmployees Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If


            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
            'S.SANDEEP |24-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
            'Dim objCompany As New clsCompany_Master
            'objCompany._Companyunkid = intCompanyID

            'Dim clsConfig As New clsConfigOptions
            'clsConfig._Companyunkid = intCompanyID

            'If clsConfig._IsArutiDemo = False AndAlso objCompany._Total_Active_Company > clsConfig._NoOfCompany Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If

            ''Sohail (13 Sep 2019) -- Start
            ''Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
            ''objCompany._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'objCompany._Total_Active_Employee_AsOnDate = clsConfig._CurrentDateAndTime.Date
            ''Sohail (13 Sep 2019) -- End
            'If clsConfig._IsArutiDemo = False AndAlso objCompany._Total_Active_Employee_ForAllCompany > clsConfig._NoOfEmployees Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If

            ''Pinkal (11-Dec-2018) -- End


            ''Sohail (08 Dec 2018) -- Start
            ''NMB Enhancement - Hiding menu in 75.1.
            'Dim objGroupMaster As New clsGroup_Master
            'objGroupMaster._Groupunkid = 1
            'HttpContext.Current.Session("CompanyGroupName") = objGroupMaster._Groupname.ToUpper 'Company Group Name in UPPER CASE            
            'objGroupMaster = Nothing
            ''Sohail (08 Dec 2018) -- End



            'Sohail (08 Dec 2018) -- Start
            'NMB Enhancement - Hiding menu in 75.1.
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            HttpContext.Current.Session("CompanyGroupName") = objGroupMaster._Groupname.ToUpper 'Company Group Name in UPPER CASE            

            Dim clsConfig As New clsConfigOptions
            Dim objCompany As New clsCompany_Master

            objCompany._Companyunkid = intCompanyID
            'clsConfig._Companyunkid = intCompanyID

            Dim mintTotal_Active_Employee_ForAllCompany As Integer = objCompany._Total_Active_Employee_ForAllCompany

            Dim intNoOfCompany = clsConfig._NoOfCompany 'Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NoOfCompany).Key)), clsConfig._NoOfCompany.GetType())
            Dim blnIsArutiDemo = clsConfig._IsArutiDemo 'Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsArutiDemo).Key)), clsConfig._IsArutiDemo.GetType())
            Dim intNoOfEmployees = clsConfig._NoOfEmployees 'Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NoOfEmployees).Key)), clsConfig._NoOfEmployees.GetType())


            If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then
                If blnIsArutiDemo = False AndAlso objCompany._Total_Active_Company > intNoOfCompany Then
                    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."
                    Exit Try
                End If

                'Sohail (13 Sep 2019) -- Start
                'Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
                'objCompany._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
                objCompany._Total_Active_Employee_AsOnDate = clsConfig._CurrentDateAndTime.Date
                'Sohail (13 Sep 2019) -- End
                If blnIsArutiDemo = False AndAlso objCompany._Total_Active_Employee_ForAllCompany > intNoOfEmployees Then
                    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."
                    Exit Try
                End If

                'Pinkal (11-Dec-2018) -- End
            End If
            objGroupMaster = Nothing

            'S.SANDEEP |24-NOV-2020| -- END
            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END





            '================================ START TO SET CONFIGURATION RELATED SESSIONS=====================================

            'Pinkal (11-Dec-2018) -- Start
            'Issue - Login page is taking time to login in NMB.
            'Dim clsConfig As New clsConfigOptions
            'clsConfig._Companyunkid = intCompanyID
            'Pinkal (11-Dec-2018) -- End


            HttpContext.Current.Session("CompanyUnkId") = intCompanyID

            Session("IsArutiDemo") = blnIsArutiDemo
            Session("NoOfEmployees") = intNoOfEmployees
            Session("Total_Active_Employee_ForAllCompany") = mintTotal_Active_Employee_ForAllCompany

            Session("DateFormat") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyDateFormat).Key), clsConfig._CompanyDateFormat.GetType()), clsConfig._CompanyDateFormat.GetType())
            Session("DateSeparator") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyDateSeparator).Key), clsConfig._CompanyDateSeparator.GetType()), clsConfig._CompanyDateSeparator.GetType())
            Session("FirstNamethenSurname") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FirstNamethenSurname).Key), clsConfig._FirstNamethenSurname.GetType()), clsConfig._FirstNamethenSurname.GetType())
            Session("IsIncludeInactiveEmp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsIncludeInactiveEmp).Key), clsConfig._IsIncludeInactiveEmp.ToString.GetType()), clsConfig._IsIncludeInactiveEmp.ToString.GetType())
            Session("UserAccessModeSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._UserAccessModeSetting).Key), clsConfig._UserAccessModeSetting.GetType()), clsConfig._UserAccessModeSetting.GetType())
            Session("EmployeeAsOnDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeAsOnDate).Key), clsConfig._EmployeeAsOnDate.GetType()), clsConfig._EmployeeAsOnDate.GetType())
            Session("SickSheetNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetNotype).Key), clsConfig._SickSheetNotype.GetType()), clsConfig._SickSheetNotype.GetType())
            Session("fmtCurrency") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CurrencyFormat).Key), clsConfig._CurrencyFormat.GetType()), clsConfig._CurrencyFormat.GetType())
            Session("AllowEditAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditAddress).Key), clsConfig._AllowEditAddress.GetType()), clsConfig._AllowEditAddress.GetType())
            Session("AllowEditPersonalInfo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditPersonalInfo).Key), clsConfig._AllowEditPersonalInfo.GetType()), clsConfig._AllowEditPersonalInfo.GetType())
            Session("AllowEditEmergencyAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditEmergencyAddress).Key), clsConfig._AllowEditEmergencyAddress.GetType()), clsConfig._AllowEditEmergencyAddress.GetType())
            Session("LoanApplicationNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanApplicationNoType).Key), clsConfig._LoanApplicationNoType.GetType()), clsConfig._LoanApplicationNoType.GetType())
            Session("LoanApplicationPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanApplicationPrifix).Key), clsConfig._LoanApplicationPrifix.GetType()), clsConfig._LoanApplicationPrifix.GetType())
            Session("NextLoanApplicationNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NextLoanApplicationNo).Key), clsConfig._NextLoanApplicationNo.GetType()), clsConfig._NextLoanApplicationNo.GetType())
            Session("BatchPostingNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BatchPostingNotype).Key), clsConfig._BatchPostingNotype.GetType()), clsConfig._BatchPostingNotype.GetType())
            Session("BatchPostingPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BatchPostingPrifix).Key), clsConfig._BatchPostingPrifix.GetType()), clsConfig._BatchPostingPrifix.GetType())
            Session("DoNotAllowOverDeductionForPayment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowOverDeductionForPayment).Key), clsConfig._DoNotAllowOverDeductionForPayment.GetType()), clsConfig._DoNotAllowOverDeductionForPayment.GetType())
            Session("PaymentVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentVocNoType).Key), clsConfig._PaymentVocNoType.GetType()), clsConfig._PaymentVocNoType.GetType())
            Session("DontAllowPaymentIfNetPayGoesBelow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowPaymentIfNetPayGoesBelow).Key), clsConfig._DontAllowPaymentIfNetPayGoesBelow.GetType()), clsConfig._DontAllowPaymentIfNetPayGoesBelow.GetType())
            Session("PaymentVocPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentVocPrefix).Key), clsConfig._PaymentVocPrefix.GetType()), clsConfig._PaymentVocPrefix.GetType())
            Session("LeaveCancelFormNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveCancelFormNotype).Key), clsConfig._LeaveCancelFormNotype.GetType()), clsConfig._LeaveCancelFormNotype.GetType())
            Session("LeaveCancelFormNoPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveCancelFormNoPrifix).Key), clsConfig._LeaveCancelFormNoPrifix.GetType()), clsConfig._LeaveCancelFormNoPrifix.GetType())
            Session("IsDenominationCompulsory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDenominationCompulsory).Key), clsConfig._IsDenominationCompulsory.GetType()), clsConfig._IsDenominationCompulsory.GetType())
            Session("SetPayslipPaymentApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetPayslipPaymentApproval).Key), clsConfig._SetPayslipPaymentApproval.GetType()), clsConfig._SetPayslipPaymentApproval.GetType())
            Session("AssetDeclarationInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationInstruction).Key), clsConfig._AssetDeclarationInstruction.GetType()), clsConfig._AssetDeclarationInstruction.GetType())
            Session("NonDisclosureDeclaration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclaration).Key), clsConfig._NonDisclosureDeclaration.GetType()), clsConfig._NonDisclosureDeclaration.GetType())
            Session("NonDisclosureWitnesser1") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureWitnesser1).Key), clsConfig._NonDisclosureWitnesser1.GetType()), clsConfig._NonDisclosureWitnesser1.GetType())
            Session("NonDisclosureWitnesser2") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureWitnesser2).Key), clsConfig._NonDisclosureWitnesser2.GetType()), clsConfig._NonDisclosureWitnesser2.GetType())
            Session("NonDisclosureWitnesser2") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureWitnesser2).Key), clsConfig._NonDisclosureWitnesser2.GetType()), clsConfig._NonDisclosureWitnesser2.GetType())
            Session("NonDisclosureDeclarationFromDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclarationFromDate).Key), clsConfig._NonDisclosureDeclarationFromDate.GetType()), clsConfig._NonDisclosureDeclarationFromDate.GetType())
            Session("NonDisclosureDeclarationToDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclarationToDate).Key), clsConfig._NonDisclosureDeclarationToDate.GetType()), clsConfig._NonDisclosureDeclarationToDate.GetType())
            Session("LockDaysAfterNonDisclosureDeclarationToDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._LockDaysAfterNonDisclosureDeclarationToDate)).Key), CInt(clsConfig._LockDaysAfterNonDisclosureDeclarationToDate).GetType()), CInt(clsConfig._LockDaysAfterNonDisclosureDeclarationToDate).GetType())
            Session("NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate)).Key), CInt(clsConfig._NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate).GetType()), CInt(clsConfig._NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate).GetType())
            Session("Document_Path") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Document_Path).Key), clsConfig._Document_Path.GetType()), clsConfig._Document_Path.GetType())
            Session("IgnoreZeroValueHeadsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IgnoreZeroValueHeadsOnPayslip).Key), clsConfig._IgnoreZeroValueHeadsOnPayslip.GetType()), clsConfig._IgnoreZeroValueHeadsOnPayslip.GetType())
            Session("ShowLoanBalanceOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowLoanBalanceOnPayslip).Key), clsConfig._ShowLoanBalanceOnPayslip.GetType()), clsConfig._ShowLoanBalanceOnPayslip.GetType())
            Session("ShowSavingBalanceOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowSavingBalanceOnPayslip).Key), clsConfig._ShowSavingBalanceOnPayslip.GetType()), clsConfig._ShowSavingBalanceOnPayslip.GetType())
            Session("ShowEmployerContributionOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowEmployerContributionOnPayslip).Key), clsConfig._ShowEmployerContributionOnPayslip.GetType()), clsConfig._ShowEmployerContributionOnPayslip.GetType())
            Session("ShowAllHeadsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowAllHeadsOnPayslip).Key), clsConfig._ShowAllHeadsOnPayslip.GetType()), clsConfig._ShowAllHeadsOnPayslip.GetType())
            Session("LogoOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LogoOnPayslip).Key), clsConfig._LogoOnPayslip.GetType()), clsConfig._LogoOnPayslip.GetType())
            Session("PayslipTemplate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PayslipTemplate).Key), clsConfig._PayslipTemplate.GetType()), clsConfig._PayslipTemplate.GetType())
            Session("LeaveTypeOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveTypeOnPayslip).Key), clsConfig._LeaveTypeOnPayslip.GetType()), clsConfig._LeaveTypeOnPayslip.GetType())
            Session("ShowCumulativeAccrual") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowCumulativeAccrualOnPayslip).Key), clsConfig._ShowCumulativeAccrualOnPayslip.GetType()), clsConfig._ShowCumulativeAccrualOnPayslip.GetType())
            Session("Base_CurrencyId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Base_CurrencyId).Key), clsConfig._Base_CurrencyId.GetType()), clsConfig._Base_CurrencyId.GetType())
            Session("ShowCategoryOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowCategoryOnPayslip).Key), clsConfig._ShowCategoryOnPayslip.GetType()), clsConfig._ShowCategoryOnPayslip.GetType())
            Session("ShowInformationalHeadsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowInformationalHeadsOnPayslip).Key), clsConfig._ShowInformationalHeadsOnPayslip.GetType()), clsConfig._ShowInformationalHeadsOnPayslip.GetType())
            Session("PaymentRoundingType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentRoundingType).Key), clsConfig._PaymentRoundingType.GetType()), clsConfig._PaymentRoundingType.GetType())
            Session("PaymentRoundingMultiple") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentRoundingMultiple).Key), clsConfig._PaymentRoundingMultiple.GetType()), clsConfig._PaymentRoundingMultiple.GetType())
            Session("CFRoundingAbove") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CFRoundingAbove).Key), clsConfig._CFRoundingAbove.GetType()), clsConfig._CFRoundingAbove.GetType())
            Session("ShowBirthDateOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBirthDateOnPayslip).Key), clsConfig._ShowBirthDateOnPayslip.GetType()), clsConfig._ShowBirthDateOnPayslip.GetType())
            Session("ShowAgeOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowAgeOnPayslip).Key), clsConfig._ShowAgeOnPayslip.GetType()), clsConfig._ShowAgeOnPayslip.GetType())
            Session("ShowNoOfDependantsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowNoOfDependantsOnPayslip).Key), clsConfig._ShowNoOfDependantsOnPayslip.GetType()), clsConfig._ShowNoOfDependantsOnPayslip.GetType())
            Session("ShowMonthlySalaryOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowMonthlySalaryOnPayslip).Key), clsConfig._ShowMonthlySalaryOnPayslip.GetType()), clsConfig._ShowMonthlySalaryOnPayslip.GetType())
            Session("OT1HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1HourHeadId).Key), clsConfig._OT1HourHeadId.GetType()), clsConfig._OT1HourHeadId.GetType())
            Session("OT2HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2HourHeadId).Key), clsConfig._OT2HourHeadId.GetType()), clsConfig._OT2HourHeadId.GetType())
            Session("OT3HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3HourHeadId).Key), clsConfig._OT3HourHeadId.GetType()), clsConfig._OT3HourHeadId.GetType())
            Session("OT4HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4HourHeadId).Key), clsConfig._OT4HourHeadId.GetType()), clsConfig._OT4HourHeadId.GetType())
            Session("OT1HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1HourHeadName).Key), clsConfig._OT1HourHeadName.GetType()), clsConfig._OT1HourHeadName.GetType())
            Session("OT2HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2HourHeadName).Key), clsConfig._OT2HourHeadName.GetType()), clsConfig._OT2HourHeadName.GetType())
            Session("OT3HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3HourHeadName).Key), clsConfig._OT3HourHeadName.GetType()), clsConfig._OT3HourHeadName.GetType())
            Session("OT4HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4HourHeadName).Key), clsConfig._OT4HourHeadName.GetType()), clsConfig._OT4HourHeadName.GetType())
            Session("OT1AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1AmountHeadId).Key), clsConfig._OT1AmountHeadId.GetType()), clsConfig._OT1AmountHeadId.GetType())
            Session("OT2AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2AmountHeadId).Key), clsConfig._OT2AmountHeadId.GetType()), clsConfig._OT2AmountHeadId.GetType())
            Session("OT3AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3AmountHeadId).Key), clsConfig._OT3AmountHeadId.GetType()), clsConfig._OT3AmountHeadId.GetType())
            Session("OT4AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4AmountHeadId).Key), clsConfig._OT4AmountHeadId.GetType()), clsConfig._OT4AmountHeadId.GetType())
            Session("OT1AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1AmountHeadName).Key), clsConfig._OT1AmountHeadName.GetType()), clsConfig._OT1AmountHeadName.GetType())
            Session("OT2AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2AmountHeadName).Key), clsConfig._OT2AmountHeadName.GetType()), clsConfig._OT2AmountHeadName.GetType())
            Session("OT3AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3AmountHeadName).Key), clsConfig._OT3AmountHeadName.GetType()), clsConfig._OT3AmountHeadName.GetType())
            Session("OT4AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4AmountHeadName).Key), clsConfig._OT4AmountHeadName.GetType()), clsConfig._OT4AmountHeadName.GetType())
            Session("ShowSalaryOnHoldOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowSalaryOnHoldOnPayslip).Key), clsConfig._ShowSalaryOnHoldOnPayslip.GetType()), clsConfig._ShowSalaryOnHoldOnPayslip.GetType())
            Session("ArutiSelfServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ArutiSelfServiceURL).Key), clsConfig._ArutiSelfServiceURL.GetType()), clsConfig._ArutiSelfServiceURL.GetType())
            If Session("ArutiSelfServiceURL") = "http://" & HttpContext.Current.Request.ApplicationPath Then
                Session("ArutiSelfServiceURL") = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath
            End If
            Session("AllowEditExperience") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditExperience).Key), clsConfig._AllowEditExperience.GetType()), clsConfig._AllowEditExperience.GetType())
            Session("AllowDeleteExperience") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteExperience).Key), clsConfig._AllowDeleteExperience.GetType()), clsConfig._AllowDeleteExperience.GetType())
            Session("AllowAddSkills") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddSkills).Key), clsConfig._AllowAddSkills.GetType()), clsConfig._AllowAddSkills.GetType())
            Session("AllowEditSkills") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditSkills).Key), clsConfig._AllowEditSkills.GetType()), clsConfig._AllowEditSkills.GetType())
            Session("AllowDeleteSkills") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteSkills).Key), clsConfig._AllowDeleteSkills.GetType()), clsConfig._AllowDeleteSkills.GetType())
            Session("AllowAddDependants") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddDependants).Key), clsConfig._AllowAddDependants.GetType()), clsConfig._AllowAddDependants.GetType())
            Session("AllowDeleteDependants") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteDependants).Key), clsConfig._AllowDeleteDependants.GetType()), clsConfig._AllowDeleteDependants.GetType())
            Session("IsDependant_AgeLimit_Set") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDependant_AgeLimit_Set).Key), clsConfig._IsDependant_AgeLimit_Set.GetType()), clsConfig._IsDependant_AgeLimit_Set.GetType())
            Session("AllowEditDependants") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditDependants).Key), clsConfig._AllowEditDependants.GetType()), clsConfig._AllowEditDependants.GetType())
            Session("AllowAddIdentity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddIdentity).Key), clsConfig._AllowAddIdentity.GetType()), clsConfig._AllowAddIdentity.GetType())
            Session("AllowDeleteIdentity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteIdentity).Key), clsConfig._AllowDeleteIdentity.GetType()), clsConfig._AllowDeleteIdentity.GetType())
            Session("AllowEditIdentity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditIdentity).Key), clsConfig._AllowEditIdentity.GetType()), clsConfig._AllowEditIdentity.GetType())
            Session("AllowAddMembership") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddMembership).Key), clsConfig._AllowAddMembership.GetType()), clsConfig._AllowAddMembership.GetType())
            Session("AllowDeleteMembership") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteMembership).Key), clsConfig._AllowDeleteMembership.GetType()), clsConfig._AllowDeleteMembership.GetType())
            Session("AllowEditMembership") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditMembership).Key), clsConfig._AllowEditMembership.GetType()), clsConfig._AllowEditMembership.GetType())
            Session("AllowAddQualifications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddQualifications).Key), clsConfig._AllowAddQualifications.GetType()), clsConfig._AllowAddQualifications.GetType())
            Session("AllowDeleteQualifications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteQualifications).Key), clsConfig._AllowDeleteQualifications.GetType()), clsConfig._AllowDeleteQualifications.GetType())
            Session("AllowEditQualifications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditQualifications).Key), clsConfig._AllowEditQualifications.GetType()), clsConfig._AllowEditQualifications.GetType())
            Session("AllowAddReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddReference).Key), clsConfig._AllowAddReference.GetType()), clsConfig._AllowAddReference.GetType())
            Session("AllowDeleteReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteReference).Key), clsConfig._AllowDeleteReference.GetType()), clsConfig._AllowDeleteReference.GetType())
            Session("AllowEditReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditReference).Key), clsConfig._AllowEditReference.GetType()), clsConfig._AllowEditReference.GetType())
            Session("IsBSC_ByEmployee") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsBSC_ByEmployee).Key), clsConfig._IsBSC_ByEmployee.GetType()), clsConfig._IsBSC_ByEmployee.GetType())
            Session("IsBSCObjectiveSaved") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsBSCObjectiveSaved).Key), clsConfig._IsBSCObjectiveSaved.GetType()), clsConfig._IsBSCObjectiveSaved.GetType())
            Session("IsAllowFinalSave") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAllowFinalSave).Key), clsConfig._IsAllowFinalSave.GetType()), clsConfig._IsAllowFinalSave.GetType())
            Session("AllowAddExperience") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddExperience).Key), clsConfig._AllowAddExperience.GetType()), clsConfig._AllowAddExperience.GetType())
            Session("IsAllocation_Hierarchy_Set") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAllocation_Hierarchy_Set).Key), clsConfig._IsAllocation_Hierarchy_Set.GetType()), clsConfig._IsAllocation_Hierarchy_Set.GetType())
            Session("Allocation_Hierarchy") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Allocation_Hierarchy).Key), clsConfig._Allocation_Hierarchy.GetType()), clsConfig._Allocation_Hierarchy.GetType())
            Session("AllowChangeCompanyEmail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowChangeCompanyEmail).Key), clsConfig._AllowChangeCompanyEmail.GetType()), clsConfig._AllowChangeCompanyEmail.GetType())
            Session("AllowToViewPersonalSalaryCalculationReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToViewPersonalSalaryCalculationReport).Key), clsConfig._AllowToViewPersonalSalaryCalculationReport.GetType()), clsConfig._AllowToViewPersonalSalaryCalculationReport.GetType())
            Session("AllowToViewEDDetailReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToViewEDDetailReport).Key), clsConfig._AllowToViewEDDetailReport.GetType()), clsConfig._AllowToViewEDDetailReport.GetType())
            Session("SickSheetNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetNotype).Key), clsConfig._SickSheetNotype.GetType()), clsConfig._SickSheetNotype.GetType())
            Session("LeaveApproverForLeaveType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsLeaveApprover_ForLeaveType).Key), clsConfig._IsLeaveApprover_ForLeaveType.GetType()), clsConfig._IsLeaveApprover_ForLeaveType.GetType())
            Session("LeaveFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveFormNoType).Key), clsConfig._LeaveFormNoType.GetType()), clsConfig._LeaveFormNoType.GetType())
            Session("LoanApprover_ForLoanScheme") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsLoanApprover_ForLoanScheme).Key), clsConfig._IsLoanApprover_ForLoanScheme.GetType()), clsConfig._IsLoanApprover_ForLoanScheme.GetType())
            Session("SendLoanEmailFromDesktopMSS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsSendLoanEmailFromDesktopMSS).Key), clsConfig._IsSendLoanEmailFromDesktopMSS.GetType()), clsConfig._IsSendLoanEmailFromDesktopMSS.GetType())
            Session("SendLoanEmailFromESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsSendLoanEmailFromESS).Key), clsConfig._IsSendLoanEmailFromESS.GetType()), clsConfig._IsSendLoanEmailFromESS.GetType())
            Session("Advance_NetPayPercentage") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_NetPayPercentage).Key), clsConfig._Advance_NetPayPercentage.GetType()), clsConfig._Advance_NetPayPercentage.GetType())
            Session("Advance_NetPayTranheadUnkid") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_NetPayTranheadUnkid).Key), clsConfig._Advance_NetPayTranheadUnkid.GetType()), clsConfig._Advance_NetPayTranheadUnkid.GetType())
            Session("Advance_DontAllowAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_DontAllowAfterDays).Key), clsConfig._Advance_DontAllowAfterDays.GetType()), clsConfig._Advance_DontAllowAfterDays.GetType())
            Session("CompanyDomain") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyDomain).Key), clsConfig._CompanyDomain.GetType()), clsConfig._CompanyDomain.GetType())
            Session("Notify_Dates") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Dates).Key), clsConfig._Notify_Dates.GetType()), clsConfig._Notify_Dates.GetType())
            Session("AppointeDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AppointeDateUserNotification).Key), clsConfig._AppointeDateUserNotification.GetType()), clsConfig._AppointeDateUserNotification.GetType())
            Session("ConfirmDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ConfirmDateUserNotification).Key), clsConfig._ConfirmDateUserNotification.GetType()), clsConfig._ConfirmDateUserNotification.GetType())
            Session("BirthDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BirthDateUserNotification).Key), clsConfig._BirthDateUserNotification.GetType()), clsConfig._BirthDateUserNotification.GetType())
            Session("SuspensionDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SuspensionDateUserNotification).Key), clsConfig._SuspensionDateUserNotification.GetType()), clsConfig._SuspensionDateUserNotification.GetType())
            Session("ProbationDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ProbationDateUserNotification).Key), clsConfig._ProbationDateUserNotification.GetType()), clsConfig._ProbationDateUserNotification.GetType())
            Session("EocDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EocDateUserNotification).Key), clsConfig._EocDateUserNotification.GetType()), clsConfig._EocDateUserNotification.GetType())
            Session("LeavingDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeavingDateUserNotification).Key), clsConfig._LeavingDateUserNotification.GetType()), clsConfig._LeavingDateUserNotification.GetType())
            Session("RetirementDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RetirementDateUserNotification).Key), clsConfig._RetirementDateUserNotification.GetType()), clsConfig._RetirementDateUserNotification.GetType())
            Session("ReinstatementDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ReinstatementDateUserNotification).Key), clsConfig._ReinstatementDateUserNotification.GetType()), clsConfig._ReinstatementDateUserNotification.GetType())
            Session("MarriageDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MarriageDateUserNotification).Key), clsConfig._MarriageDateUserNotification.GetType()), clsConfig._MarriageDateUserNotification.GetType())
            Session("ExemptionDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ExemptionDateUserNotification).Key), clsConfig._ExemptionDateUserNotification.GetType()), clsConfig._ExemptionDateUserNotification.GetType())
            Session("Notify_EmplData") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_EmplData).Key), clsConfig._Notify_EmplData.GetType()), clsConfig._Notify_EmplData.GetType())
            Session("Notify_Allocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Allocation).Key), clsConfig._Notify_Allocation.GetType()), clsConfig._Notify_Allocation.GetType())
            Session("DatafileExportPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DatafileExportPath).Key), clsConfig._DatafileExportPath.GetType()), clsConfig._DatafileExportPath.GetType())
            Session("DatafileName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DatafileName).Key), clsConfig._DatafileName.GetType()), clsConfig._DatafileName.GetType())
            Session("Accounting_TransactionReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_TransactionReference).Key), clsConfig._Accounting_TransactionReference.GetType()), clsConfig._Accounting_TransactionReference.GetType())
            Session("Accounting_JournalType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_JournalType).Key), clsConfig._Accounting_JournalType.GetType()), clsConfig._Accounting_JournalType.GetType())
            Session("Accounting_JVGroupCode") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_JVGroupCode).Key), clsConfig._Accounting_JVGroupCode.GetType()), clsConfig._Accounting_JVGroupCode.GetType())
            Session("LeaveBalanceSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveBalanceSetting).Key), clsConfig._LeaveBalanceSetting.GetType()), clsConfig._LeaveBalanceSetting.GetType())
            If Session("LeaveBalanceSetting") <= 0 Then
                Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
            End If
            Session("AllowToviewPaysliponEss") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToviewPaysliponEss).Key), clsConfig._AllowToviewPaysliponEss.GetType()), clsConfig._AllowToviewPaysliponEss.GetType())
            Session("ViewPayslipDaysBefore") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ViewPayslipDaysBefore).Key), clsConfig._ViewPayslipDaysBefore.GetType()), clsConfig._ViewPayslipDaysBefore.GetType())
            Session("DontShowPayslipOnESSIfPaymentNotAuthorized") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontShowPayslipOnESSIfPaymentNotAuthorized).Key), clsConfig._DontShowPayslipOnESSIfPaymentNotAuthorized.GetType()), clsConfig._DontShowPayslipOnESSIfPaymentNotAuthorized.GetType())
            Session("DontShowPayslipOnESSIfPaymentNotDone") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontShowPayslipOnESSIfPaymentNotDone).Key), clsConfig._DontShowPayslipOnESSIfPaymentNotDone.GetType()), clsConfig._DontShowPayslipOnESSIfPaymentNotDone.GetType())
            Session("ShowPayslipOnESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowHidePayslipOnESS).Key), clsConfig._ShowHidePayslipOnESS.GetType()), clsConfig._ShowHidePayslipOnESS.GetType())
            Session("IsImgInDataBase") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsImgInDataBase).Key), clsConfig._IsImgInDataBase.GetType()), clsConfig._IsImgInDataBase.GetType())
            Session("AllowToAddEditImageForESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditImageForESS).Key), clsConfig._AllowToAddEditImageForESS.GetType()), clsConfig._AllowToAddEditImageForESS.GetType())
            Session("ShowFirstAppointmentDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowFirstAppointmentDate).Key), clsConfig._ShowFirstAppointmentDate.GetType()), clsConfig._ShowFirstAppointmentDate.GetType())
            Session("BatchPostingNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BatchPostingNotype).Key), clsConfig._BatchPostingNotype.GetType()), clsConfig._BatchPostingNotype.GetType())
            Session("NotifyPayroll_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Payroll_Users).Key), clsConfig._Notify_Payroll_Users.GetType()), clsConfig._Notify_Payroll_Users.GetType())
            Session("EFTIntegration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTIntegration).Key), clsConfig._EFTIntegration.GetType()), clsConfig._EFTIntegration.GetType())
            Session("DatafileName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DatafileName).Key), clsConfig._DatafileName.GetType()), clsConfig._DatafileName.GetType())
            Session("AccountingSoftWare") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AccountingSoftWare).Key), clsConfig._AccountingSoftWare.GetType()), clsConfig._AccountingSoftWare.GetType())
            Session("MobileMoneyEFTIntegration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MobileMoneyEFTIntegration).Key), clsConfig._MobileMoneyEFTIntegration.GetType()), clsConfig._MobileMoneyEFTIntegration.GetType())
            Session("_LoanVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanVocNoType).Key), clsConfig._LoanVocNoType.GetType()), clsConfig._LoanVocNoType.GetType())
            Session("_PaymentVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentVocNoType).Key), clsConfig._PaymentVocNoType.GetType()), clsConfig._PaymentVocNoType.GetType())
            Session("_DoNotAllowOverDeductionForPayment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowOverDeductionForPayment).Key), clsConfig._DoNotAllowOverDeductionForPayment.GetType()), clsConfig._DoNotAllowOverDeductionForPayment.GetType())
            Session("_IsDenominationCompulsory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDenominationCompulsory).Key), clsConfig._IsDenominationCompulsory.GetType()), clsConfig._IsDenominationCompulsory.GetType())
            Session("_SetPayslipPaymentApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetPayslipPaymentApproval).Key), clsConfig._SetPayslipPaymentApproval.GetType()), clsConfig._SetPayslipPaymentApproval.GetType())
            Session("SavingsVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SavingsVocNoType).Key), clsConfig._SavingsVocNoType.GetType()), clsConfig._SavingsVocNoType.GetType())
            Session("AllowAssessor_Before_Emp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAssessor_Before_Emp).Key), clsConfig._AllowAssessor_Before_Emp.GetType()), clsConfig._AllowAssessor_Before_Emp.GetType())
            Session("IsCompanyNeedReviewer") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsCompanyNeedReviewer).Key), clsConfig._IsCompanyNeedReviewer.GetType()), clsConfig._IsCompanyNeedReviewer.GetType())
            Session("ConsiderItemWeightAsNumber") = False
            Session("PolicyManagementTNA") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PolicyManagementTNA).Key), clsConfig._PolicyManagementTNA.GetType()), clsConfig._PolicyManagementTNA.GetType())
            Session("IsAutomaticIssueOnFinalApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAutomaticIssueOnFinalApproval).Key), clsConfig._IsAutomaticIssueOnFinalApproval.GetType()), clsConfig._IsAutomaticIssueOnFinalApproval.GetType())
            Session("StaffReqFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffReqFormNoType).Key), clsConfig._StaffReqFormNoType.GetType()), clsConfig._StaffReqFormNoType.GetType())
            Session("ApplyStaffRequisition") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplyStaffRequisition).Key), clsConfig._ApplyStaffRequisition.GetType()), clsConfig._ApplyStaffRequisition.GetType())
            Session("ClosePayrollPeriodIfLeaveIssue") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClosePayrollPeriodIfLeaveIssue).Key), clsConfig._ClosePayrollPeriodIfLeaveIssue.GetType()), clsConfig._ClosePayrollPeriodIfLeaveIssue.GetType())
            Session("PaymentApprovalwithLeaveApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentApprovalwithLeaveApproval).Key), clsConfig._PaymentApprovalwithLeaveApproval.GetType()), clsConfig._PaymentApprovalwithLeaveApproval.GetType())
            Session("ClaimRequestVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRequestVocNoType).Key), clsConfig._ClaimRequestVocNoType.GetType()), clsConfig._ClaimRequestVocNoType.GetType())
            Session("SickSheetTemplate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetTemplate).Key), clsConfig._SickSheetTemplate.GetType()), clsConfig._SickSheetTemplate.GetType())
            Session("SickSheetAllocationId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetAllocationId).Key), clsConfig._SickSheetAllocationId.GetType()), clsConfig._SickSheetAllocationId.GetType())
            Session("CustomPayrollReportAllocIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportAllocIds).Key), clsConfig._CustomPayrollReportAllocIds.GetType()), clsConfig._CustomPayrollReportAllocIds.GetType())
            Session("CustomPayrollReportHeadsIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportHeadsIds).Key), clsConfig._CustomPayrollReportHeadsIds.GetType()), clsConfig._CustomPayrollReportHeadsIds.GetType())
            Session("CustomPayrollReportTemplate2AllocIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportTemplate2AllocIds).Key), clsConfig._CustomPayrollReportTemplate2AllocIds.GetType()), clsConfig._CustomPayrollReportTemplate2AllocIds.GetType())
            Session("CustomPayrollReportTemplate2HeadsIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportTemplate2HeadsIds).Key), clsConfig._CustomPayrollReportTemplate2HeadsIds.GetType()), clsConfig._CustomPayrollReportTemplate2HeadsIds.GetType())
            Session("Notify_IssuedLeave_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_IssuedLeave_Users).Key), clsConfig._Notify_IssuedLeave_Users.GetType()), clsConfig._Notify_IssuedLeave_Users.GetType())
            Session("ViewTitles_InPlanning") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ViewTitles_InPlanning).Key), clsConfig._ViewTitles_InPlanning.GetType()), clsConfig._ViewTitles_InPlanning.GetType())
            Session("FollowEmployeeHierarchy") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FollowEmployeeHierarchy).Key), clsConfig._FollowEmployeeHierarchy.GetType()), clsConfig._FollowEmployeeHierarchy.GetType())
            Session("CascadingTypeId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CascadingTypeId).Key), clsConfig._CascadingTypeId.GetType()), clsConfig._CascadingTypeId.GetType())
            Session("Assessment_Instructions") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Assessment_Instructions).Key), clsConfig._Assessment_Instructions.GetType()), clsConfig._Assessment_Instructions.GetType())
            Session("OrgAssessment_Instructions") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Assessment_Instructions).Key), clsConfig._Assessment_Instructions.GetType()), clsConfig._Assessment_Instructions.GetType())
            Session("ScoringOptionId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoringOptionId).Key), clsConfig._ScoringOptionId.GetType()), clsConfig._ScoringOptionId.GetType())
            Session("Perf_EvaluationOrder") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Perf_EvaluationOrder).Key), clsConfig._Perf_EvaluationOrder.GetType()), clsConfig._Perf_EvaluationOrder.GetType())
            Session("OrgPerf_EvaluationOrder") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Perf_EvaluationOrder).Key), clsConfig._Perf_EvaluationOrder.GetType()), clsConfig._Perf_EvaluationOrder.GetType())
            Session("ViewTitles_InEvaluation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ViewTitles_InEvaluation).Key), clsConfig._ViewTitles_InEvaluation.GetType()), clsConfig._ViewTitles_InEvaluation.GetType())
            Session("Self_Assign_Competencies") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Self_Assign_Competencies).Key), clsConfig._Self_Assign_Competencies.GetType()), clsConfig._Self_Assign_Competencies.GetType())
            Session("DetailedSalaryBreakdownReportHeadsIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DetailedSalaryBreakdownReportHeadsIds).Key), clsConfig._DetailedSalaryBreakdownReportHeadsIds.GetType()), clsConfig._DetailedSalaryBreakdownReportHeadsIds.GetType())
            Session("Notify_Bank_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Bank_Users).Key), clsConfig._Notify_Bank_Users.GetType()), clsConfig._Notify_Bank_Users.GetType())
            Session("AllowToChangeAttendanceAfterPayment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToChangeAttendanceAfterPayment).Key), clsConfig._AllowToChangeAttendanceAfterPayment.GetType()), clsConfig._AllowToChangeAttendanceAfterPayment.GetType())
            Session("OpenAfterExport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OpenAfterExport).Key), clsConfig._OpenAfterExport.GetType()), clsConfig._OpenAfterExport.GetType())
            Session("ExportReportPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ExportReportPath).Key), clsConfig._ExportReportPath.GetType()), clsConfig._ExportReportPath.GetType())
            Session("SMimeRunPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMimeRunPath).Key), clsConfig._SMimeRunPath.GetType()), clsConfig._SMimeRunPath.GetType())
            Session("StaffReqFormNoPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffReqFormNoPrefix).Key), clsConfig._StaffReqFormNoPrefix.GetType()), clsConfig._StaffReqFormNoPrefix.GetType())
            Session("StaffReqFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffReqFormNoType).Key), clsConfig._StaffReqFormNoType.GetType()), clsConfig._StaffReqFormNoType.GetType())
            Session("DonotAttendanceinSeconds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DonotAttendanceinSeconds).Key), clsConfig._DonotAttendanceinSeconds.GetType()), clsConfig._DonotAttendanceinSeconds.GetType())
            Session("FirstCheckInLastCheckOut") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FirstCheckInLastCheckOut).Key), clsConfig._FirstCheckInLastCheckOut.GetType()), clsConfig._FirstCheckInLastCheckOut.GetType())
            Session("EFTCitiDirectCountryCode") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectCountryCode).Key), clsConfig._EFTCitiDirectCountryCode.GetType()), clsConfig._EFTCitiDirectCountryCode.GetType())
            Session("EFTCitiDirectSkipPriorityFlag") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectSkipPriorityFlag).Key), clsConfig._EFTCitiDirectSkipPriorityFlag.GetType()), clsConfig._EFTCitiDirectSkipPriorityFlag.GetType())
            Session("EFTCitiDirectChargesIndicator") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectChargesIndicator).Key), clsConfig._EFTCitiDirectChargesIndicator.GetType()), clsConfig._EFTCitiDirectChargesIndicator.GetType())
            Session("EFTCitiDirectAddPaymentDetail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectAddPaymentDetail).Key), clsConfig._EFTCitiDirectAddPaymentDetail.GetType()), clsConfig._EFTCitiDirectAddPaymentDetail.GetType())
            Session("Accounting_Country") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_Country).Key), clsConfig._Accounting_Country.GetType()), clsConfig._Accounting_Country.GetType())
            Session("RoundOff_Type") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RoundOff_Type).Key), clsConfig._RoundOff_Type.GetType()), clsConfig._RoundOff_Type.GetType())
            Session("SectorRouteAssignToEmp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SectorRouteAssignToEmp).Key), clsConfig._SectorRouteAssignToEmp.GetType()), clsConfig._SectorRouteAssignToEmp.GetType())
            Session("LeaveLiabilityReportSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveLiabilityReportSetting).Key), clsConfig._LeaveLiabilityReportSetting.GetType()), clsConfig._LeaveLiabilityReportSetting.GetType())
            Session("ShowBankAccountNoOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBankAccountNoOnPayslip).Key), clsConfig._ShowBankAccountNoOnPayslip.GetType()), clsConfig._ShowBankAccountNoOnPayslip.GetType())
            Session("SelectedAllocationForDailyTimeSheetReport_Voltamp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp).Key), clsConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp.GetType()), clsConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp.GetType())
            Session("ShowEmployeeStatusForDailyTimeSheetReport_Voltamp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp).Key), clsConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp.GetType()), clsConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp.GetType())
            Session("LeaveAccrueDaysAfterEachMonth") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveAccrueDaysAfterEachMonth).Key), clsConfig._LeaveAccrueDaysAfterEachMonth.GetType()), clsConfig._LeaveAccrueDaysAfterEachMonth.GetType())
            Session("LeaveAccrueTenureSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveAccrueTenureSetting).Key), clsConfig._LeaveAccrueTenureSetting.GetType()), clsConfig._LeaveAccrueTenureSetting.GetType())
            Session("AllowToExceedTimeAssignedToActivity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._AllowToExceedTimeAssignedToActivity)).Key), CBool(clsConfig._AllowToExceedTimeAssignedToActivity).GetType()), CBool(clsConfig._AllowToExceedTimeAssignedToActivity).GetType())
            Session("AllowOverTimeToEmpTimesheet") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._AllowOverTimeToEmpTimesheet)).Key), CBool(clsConfig._AllowOverTimeToEmpTimesheet).GetType()), CBool(clsConfig._AllowOverTimeToEmpTimesheet).GetType())
            Session("NotAllowIncompleteTimesheet") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._NotAllowIncompleteTimesheet)).Key), CBool(clsConfig._NotAllowIncompleteTimesheet).GetType()), CBool(clsConfig._NotAllowIncompleteTimesheet).GetType())
            Session("DescriptionMandatoryForActivity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._DescriptionMandatoryForActivity)).Key), CBool(clsConfig._DescriptionMandatoryForActivity).GetType()), CBool(clsConfig._DescriptionMandatoryForActivity).GetType())
            Session("AllowActivityHoursByPercentage") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._AllowActivityHoursByPercentage)).Key), CBool(clsConfig._AllowActivityHoursByPercentage).GetType()), CBool(clsConfig._AllowActivityHoursByPercentage).GetType())
            Session("StatutoryMessageOnPayslipOnESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CStr(clsConfig._StatutoryMessageOnPayslipOnESS)).Key), CStr(clsConfig._StatutoryMessageOnPayslipOnESS).GetType()), CStr(clsConfig._StatutoryMessageOnPayslipOnESS).GetType())
            Session("SkipAbsentFromTnAMonthlyPayrollReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipAbsentFromTnAMonthlyPayrollReport).Key), clsConfig._SkipAbsentFromTnAMonthlyPayrollReport.GetType()), clsConfig._SkipAbsentFromTnAMonthlyPayrollReport.GetType())
            Session("IncludePendingApproverFormsInMonthlyPayrollReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IncludePendingApproverFormsInMonthlyPayrollReport).Key), clsConfig._IncludePendingApproverFormsInMonthlyPayrollReport.GetType()), clsConfig._IncludePendingApproverFormsInMonthlyPayrollReport.GetType())
            Session("IncludeSystemRetirementMonthlyPayrollReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IncludeSystemRetirementMonthlyPayrollReport).Key), clsConfig._IncludeSystemRetirementMonthlyPayrollReport.GetType()), clsConfig._IncludeSystemRetirementMonthlyPayrollReport.GetType())
            Session("CheckedMonthlyPayrollReport1HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport1HeadIds).Key), clsConfig._CheckedMonthlyPayrollReport1HeadIds.GetType()), clsConfig._CheckedMonthlyPayrollReport1HeadIds.GetType())
            Session("CheckedMonthlyPayrollReport2HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport2HeadIds).Key), clsConfig._CheckedMonthlyPayrollReport2HeadIds.GetType()), clsConfig._CheckedMonthlyPayrollReport2HeadIds.GetType())
            Session("CheckedMonthlyPayrollReport3HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport3HeadIds).Key), clsConfig._CheckedMonthlyPayrollReport3HeadIds.GetType()), clsConfig._CheckedMonthlyPayrollReport3HeadIds.GetType())
            Session("CheckedMonthlyPayrollReport4HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport4HeadIds).Key), clsConfig._CheckedMonthlyPayrollReport4HeadIds.GetType()), clsConfig._CheckedMonthlyPayrollReport4HeadIds.GetType())
            Session("CheckedMonthlyPayrollReportLeaveIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReportLeaveIds).Key), clsConfig._CheckedMonthlyPayrollReportLeaveIds.GetType()), clsConfig._CheckedMonthlyPayrollReportLeaveIds.GetType())
            Session("Ntf_TrainingLevelI_EvalUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_TrainingLevelI_EvalUserIds).Key), clsConfig._Ntf_TrainingLevelI_EvalUserIds.GetType()), clsConfig._Ntf_TrainingLevelI_EvalUserIds.GetType())
            Session("Ntf_TrainingLevelIII_EvalUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_TrainingLevelIII_EvalUserIds).Key), clsConfig._Ntf_TrainingLevelIII_EvalUserIds.GetType()), clsConfig._Ntf_TrainingLevelIII_EvalUserIds.GetType())
            Session("NotAllowIncompleteTimesheet") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._NotAllowIncompleteTimesheet)).Key), CBool(clsConfig._NotAllowIncompleteTimesheet).GetType()), CBool(clsConfig._NotAllowIncompleteTimesheet).GetType())
            Session("IsHolidayConsiderOnWeekend") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsHolidayConsiderOnWeekend).Key), clsConfig._IsHolidayConsiderOnWeekend.GetType()), clsConfig._IsHolidayConsiderOnWeekend.GetType())
            Session("IsDayOffConsiderOnWeekend") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDayOffConsiderOnWeekend).Key), clsConfig._IsDayOffConsiderOnWeekend.GetType()), clsConfig._IsDayOffConsiderOnWeekend.GetType())
            Session("IsHolidayConsiderOnDayoff") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsHolidayConsiderOnDayoff).Key), clsConfig._IsHolidayConsiderOnDayoff.GetType()), clsConfig._IsHolidayConsiderOnDayoff.GetType())
            Session("AllowtoViewSignatureESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowtoViewSignatureESS).Key), clsConfig._AllowtoViewSignatureESS.GetType()), clsConfig._AllowtoViewSignatureESS.GetType())
            Session("DontAllowAdvanceOnESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowAdvanceOnESS).Key), clsConfig._DontAllowAdvanceOnESS.GetType()), clsConfig._DontAllowAdvanceOnESS.GetType())
            Session("WebURLInternal") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._WebURLInternal).Key), clsConfig._WebURLInternal.GetType()), clsConfig._WebURLInternal.GetType())
            Session("ShowBgTimesheetActivity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetActivity).Key), clsConfig._ShowBgTimesheetActivity.GetType()), clsConfig._ShowBgTimesheetActivity.GetType())
            Session("ShowBgTimesheetAssignedActivityPercentage") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetAssignedActivityPercentage).Key), clsConfig._ShowBgTimesheetAssignedActivityPercentage.GetType()), clsConfig._ShowBgTimesheetAssignedActivityPercentage.GetType())
            Session("ShowBgTimesheetActivityProject") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetActivityProject).Key), clsConfig._ShowBgTimesheetActivityProject.GetType()), clsConfig._ShowBgTimesheetActivityProject.GetType())
            Session("ShowBgTimesheetActivityHrsDetail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetActivityHrsDetail).Key), clsConfig._ShowBgTimesheetActivityHrsDetail.GetType()), clsConfig._ShowBgTimesheetActivityHrsDetail.GetType())
            Session("RemarkMandatoryForTimesheetSubmission") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RemarkMandatoryForTimesheetSubmission).Key), clsConfig._RemarkMandatoryForTimesheetSubmission.GetType()), clsConfig._RemarkMandatoryForTimesheetSubmission.GetType())
            Session("GrievanceApprovalSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceApprovalSetting).Key), clsConfig._GrievanceApprovalSetting.GetType()), clsConfig._GrievanceApprovalSetting.GetType())
            Session("GrievanceRefNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceRefNoType).Key), clsConfig._GrievanceRefNoType.GetType()), clsConfig._GrievanceRefNoType.GetType())
            Session("GrievanceRefPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceRefPrefix).Key), clsConfig._GrievanceRefPrefix.GetType()), clsConfig._GrievanceRefPrefix.GetType())
            Session("DependantDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DependantDocsAttachmentMandatory).Key), clsConfig._DependantDocsAttachmentMandatory.GetType()), clsConfig._DependantDocsAttachmentMandatory.GetType())
            Session("QualificationCertificateAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._QualificationCertificateAttachmentMandatory).Key), clsConfig._QualificationCertificateAttachmentMandatory.GetType()), clsConfig._QualificationCertificateAttachmentMandatory.GetType())
            Session("SkipApprovalFlowInPlanning") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalFlowInPlanning).Key), clsConfig._SkipApprovalFlowInPlanning.GetType()), clsConfig._SkipApprovalFlowInPlanning.GetType())
            Session("OnlyOneItemPerCompetencyCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OnlyOneItemPerCompetencyCategory).Key), clsConfig._OnlyOneItemPerCompetencyCategory.GetType()), clsConfig._OnlyOneItemPerCompetencyCategory.GetType())
            Session("EnableBSCAutomaticRating") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EnableBSCAutomaticRating).Key), clsConfig._EnableBSCAutomaticRating.GetType()), clsConfig._EnableBSCAutomaticRating.GetType())
            Session("DontAllowToEditScoreGenbySys") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowToEditScoreGenbySys).Key), clsConfig._DontAllowToEditScoreGenbySys.GetType()), clsConfig._DontAllowToEditScoreGenbySys.GetType())
            Session("GoalsAccomplishedRequiresApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GoalsAccomplishedRequiresApproval).Key), clsConfig._GoalsAccomplishedRequiresApproval.GetType()), clsConfig._GoalsAccomplishedRequiresApproval.GetType())
            Session("AssessmentReportTemplateId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssessmentReportTemplateId).Key), clsConfig._AssessmentReportTemplateId.GetType()), clsConfig._AssessmentReportTemplateId.GetType())
            Session("IsUseAgreedScore") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsUseAgreedScore).Key), clsConfig._IsUseAgreedScore.GetType()), clsConfig._IsUseAgreedScore.GetType())
            Session("ReviewerScoreSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ReviewerScoreSetting).Key), clsConfig._ReviewerScoreSetting.GetType()), clsConfig._ReviewerScoreSetting.GetType())
            Session("IsAllowCustomItemFinalSave") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAllowCustomItemFinalSave).Key), clsConfig._IsAllowCustomItemFinalSave.GetType()), clsConfig._IsAllowCustomItemFinalSave.GetType())
            Session("ShowMyGoals") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowMyGoals).Key), clsConfig._ShowMyGoals.GetType()), clsConfig._ShowMyGoals.GetType())
            Session("ShowCustomHeaders") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowCustomHeaders).Key), clsConfig._ShowCustomHeaders.GetType()), clsConfig._ShowCustomHeaders.GetType())
            Session("ShowGoalsApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowGoalsApproval).Key), clsConfig._ShowGoalsApproval.GetType()), clsConfig._ShowGoalsApproval.GetType())
            Session("ShowSelfAssessment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowSelfAssessment).Key), clsConfig._ShowSelfAssessment.GetType()), clsConfig._ShowSelfAssessment.GetType())
            Session("ShowMyComptencies") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowMyComptencies).Key), clsConfig._ShowMyComptencies.GetType()), clsConfig._ShowMyComptencies.GetType())
            Session("ShowViewPerformanceAssessment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowViewPerformanceAssessment).Key), clsConfig._ShowViewPerformanceAssessment.GetType()), clsConfig._ShowViewPerformanceAssessment.GetType())
            Session("Ntf_FinalAcknowledgementUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_FinalAcknowledgementUserIds).Key), clsConfig._Ntf_FinalAcknowledgementUserIds.GetType()), clsConfig._Ntf_FinalAcknowledgementUserIds.GetType())
            Session("Ntf_GoalsUnlockUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_GoalsUnlockUserIds).Key), clsConfig._Ntf_GoalsUnlockUserIds.GetType()), clsConfig._Ntf_GoalsUnlockUserIds.GetType())
            Session("CreateADUserFromEmpMst") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CreateADUserFromEmpMst).Key), clsConfig._CreateADUserFromEmpMst.GetType()), clsConfig._CreateADUserFromEmpMst.GetType())
            Session("UserMustchangePwdOnNextLogon") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._UserMustchangePwdOnNextLogon).Key), clsConfig._UserMustchangePwdOnNextLogon.GetType()), clsConfig._UserMustchangePwdOnNextLogon.GetType())
            Session("SetRelieverAsMandatoryForApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetRelieverAsMandatoryForApproval).Key), clsConfig._SetRelieverAsMandatoryForApproval.GetType()), clsConfig._SetRelieverAsMandatoryForApproval.GetType())
            Session("AllowToSetRelieverOnLvFormForESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToSetRelieverOnLvFormForESS).Key), clsConfig._AllowToSetRelieverOnLvFormForESS.GetType()), clsConfig._AllowToSetRelieverOnLvFormForESS.GetType())
            Session("RelieverAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RelieverAllocation).Key), clsConfig._RelieverAllocation.GetType()), clsConfig._RelieverAllocation.GetType())
            Session("AllowLvApplicationApplyForBackDates") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowLvApplicationApplyForBackDates).Key), clsConfig._AllowLvApplicationApplyForBackDates.GetType()), clsConfig._AllowLvApplicationApplyForBackDates.GetType())
            Session("AssetDeclarationTemplate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationTemplate).Key), clsConfig._AssetDeclarationTemplate.GetType()), clsConfig._AssetDeclarationTemplate.GetType())
            Session("StaffRequisitionFinalApprovedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffRequisitionFinalApprovedNotificationUserIds).Key), clsConfig._StaffRequisitionFinalApprovedNotificationUserIds.GetType()), clsConfig._StaffRequisitionFinalApprovedNotificationUserIds.GetType())
            Session("EnableTraningRequisition") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EnableTraningRequisition).Key), clsConfig._EnableTraningRequisition.GetType()), clsConfig._EnableTraningRequisition.GetType())
            Session("DonotAllowToRequestTrainingAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DonotAllowToRequestTrainingAfterDays).Key), clsConfig._DonotAllowToRequestTrainingAfterDays.GetType()), clsConfig._DonotAllowToRequestTrainingAfterDays.GetType())
            Session("SetOTRequisitionMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetOTRequisitionMandatory).Key), clsConfig._SetOTRequisitionMandatory.GetType()), clsConfig._SetOTRequisitionMandatory.GetType())
            Session("CapOTHrsForHODApprovers") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CapOTHrsForHODApprovers).Key), clsConfig._CapOTHrsForHODApprovers.GetType()), clsConfig._CapOTHrsForHODApprovers.GetType())
            Session("SkipEmployeeMovementApprovalFlow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEmployeeMovementApprovalFlow).Key), clsConfig._SkipEmployeeMovementApprovalFlow.GetType()), clsConfig._SkipEmployeeMovementApprovalFlow.GetType())
            Session("RejectTransferUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectTransferUserNotification).Key), clsConfig._RejectTransferUserNotification.GetType()), clsConfig._RejectTransferUserNotification.GetType())
            Session("RejectReCategorizationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectReCategorizationUserNotification).Key), clsConfig._RejectReCategorizationUserNotification.GetType()), clsConfig._RejectReCategorizationUserNotification.GetType())
            Session("RejectProbationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectProbationUserNotification).Key), clsConfig._RejectProbationUserNotification.GetType()), clsConfig._RejectProbationUserNotification.GetType())
            Session("RejectConfirmationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectConfirmationUserNotification).Key), clsConfig._RejectConfirmationUserNotification.GetType()), clsConfig._RejectConfirmationUserNotification.GetType())
            Session("RejectSuspensionUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectSuspensionUserNotification).Key), clsConfig._RejectSuspensionUserNotification.GetType()), clsConfig._RejectSuspensionUserNotification.GetType())
            Session("RejectTerminationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectTerminationUserNotification).Key), clsConfig._RejectTerminationUserNotification.GetType()), clsConfig._RejectTerminationUserNotification.GetType())
            Session("RejectRetirementUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectRetirementUserNotification).Key), clsConfig._RejectRetirementUserNotification.GetType()), clsConfig._RejectRetirementUserNotification.GetType())
            Session("RejectWorkPermitUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectWorkPermitUserNotification).Key), clsConfig._RejectWorkPermitUserNotification.GetType()), clsConfig._RejectWorkPermitUserNotification.GetType())
            Session("RejectResidentPermitUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectResidentPermitUserNotification).Key), clsConfig._RejectResidentPermitUserNotification.GetType()), clsConfig._RejectResidentPermitUserNotification.GetType())
            Session("RejectCostCenterPermitUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectCostCenterPermitUserNotification).Key), clsConfig._RejectCostCenterPermitUserNotification.GetType()), clsConfig._RejectCostCenterPermitUserNotification.GetType())
            Session("RejectExemptionUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectExemptionUserNotification).Key), clsConfig._RejectExemptionUserNotification.GetType()), clsConfig._RejectExemptionUserNotification.GetType())
            Session("AssetDeclarationFinalSavedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationFinalSavedNotificationUserIds).Key), clsConfig._AssetDeclarationFinalSavedNotificationUserIds.GetType()), clsConfig._AssetDeclarationFinalSavedNotificationUserIds.GetType())
            Session("SkipEmployeeApprovalFlow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEmployeeApprovalFlow).Key), clsConfig._SkipEmployeeApprovalFlow.GetType()), clsConfig._SkipEmployeeApprovalFlow.GetType())
            Session("AssetDeclarationUnlockFinalSavedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationUnlockFinalSavedNotificationUserIds).Key), clsConfig._AssetDeclarationUnlockFinalSavedNotificationUserIds.GetType()), clsConfig._AssetDeclarationUnlockFinalSavedNotificationUserIds.GetType())
            Session("NonDisclosureDeclarationAcknowledgedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclarationAcknowledgedNotificationUserIds).Key), clsConfig._NonDisclosureDeclarationAcknowledgedNotificationUserIds.GetType()), clsConfig._NonDisclosureDeclarationAcknowledgedNotificationUserIds.GetType())
            Session("FlexCubeJVPostedOracleNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexCubeJVPostedOracleNotificationUserIds).Key), clsConfig._FlexCubeJVPostedOracleNotificationUserIds.GetType()), clsConfig._FlexCubeJVPostedOracleNotificationUserIds.GetType())
            Session("OracleHostName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleHostName).Key), clsConfig._OracleHostName.GetType()), clsConfig._OracleHostName.GetType())
            Session("OraclePortNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OraclePortNo).Key), clsConfig._OraclePortNo.GetType()), clsConfig._OraclePortNo.GetType())
            Session("OracleServiceName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleServiceName).Key), clsConfig._OracleServiceName.GetType()), clsConfig._OracleServiceName.GetType())
            Session("OracleUserName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleUserName).Key), clsConfig._OracleUserName.GetType()), clsConfig._OracleUserName.GetType())
            Session("Advance_CostCenterunkid") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_CostCenterunkid).Key), clsConfig._Advance_CostCenterunkid.GetType()), clsConfig._Advance_CostCenterunkid.GetType())
            Session("OracleUserPassword") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleUserPassword).Key), clsConfig._OracleUserPassword.GetType()), clsConfig._OracleUserPassword.GetType())
            If CInt(Session("Advance_CostCenterunkid")) >= 0 Then
                Session("OracleUserPassword") = clsSecurity.Decrypt(CStr(Session("OracleUserPassword")), "ezee")
            End If


            Session("SQLDataSource") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLDataSource).Key), clsConfig._SQLDataSource.GetType()), clsConfig._SQLDataSource.GetType())
            Session("SQLDatabaseName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLDatabaseName).Key), clsConfig._SQLDatabaseName.GetType()), clsConfig._SQLDatabaseName.GetType())
            Session("SQLDatabaseOwnerName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLDatabaseOwnerName).Key), clsConfig._SQLDatabaseOwnerName.GetType()), clsConfig._SQLDatabaseOwnerName.GetType())
            Session("SQLUserName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLUserName).Key), clsConfig._SQLUserName.GetType()), clsConfig._SQLUserName.GetType())
            Session("SQLUserPassword") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLUserPassword).Key), clsConfig._SQLUserPassword.GetType()), clsConfig._SQLUserPassword.GetType())
            If CInt(Session("Advance_CostCenterunkid")) >= 0 Then
                Session("SQLUserPassword") = clsSecurity.Decrypt(CStr(Session("SQLUserPassword")), "ezee")
            End If
            Session("AssetDeclarationFromDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationFromDate).Key), clsConfig._AssetDeclarationFromDate.GetType()), clsConfig._AssetDeclarationFromDate.GetType())
            Session("AssetDeclarationToDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationToDate).Key), clsConfig._AssetDeclarationToDate.GetType()), clsConfig._AssetDeclarationToDate.GetType())
            Session("DontAllowAssetDeclarationAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._DontAllowAssetDeclarationAfterDays)).Key), CInt(clsConfig._DontAllowAssetDeclarationAfterDays).GetType()), CInt(clsConfig._DontAllowAssetDeclarationAfterDays).GetType())
            Session("NewEmpUnLockDaysforAssetDecWithinAppointmentDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._NewEmpUnLockDaysforAssetDecWithinAppointmentDate)).Key), CInt(clsConfig._NewEmpUnLockDaysforAssetDecWithinAppointmentDate).GetType()), CInt(clsConfig._NewEmpUnLockDaysforAssetDecWithinAppointmentDate).GetType())
            Session("AssetDeclarationFromDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationFromDate).Key), clsConfig._AssetDeclarationFromDate.GetType()), clsConfig._AssetDeclarationFromDate.GetType())
            Session("ImportCostCenterP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ImportCostCenterP2PServiceURL).Key), clsConfig._ImportCostCenterP2PServiceURL.GetType()), clsConfig._ImportCostCenterP2PServiceURL.GetType())
            Session("OpexRequestCCP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OpexRequestCCP2PServiceURL).Key), clsConfig._OpexRequestCCP2PServiceURL.GetType()), clsConfig._OpexRequestCCP2PServiceURL.GetType())
            Session("CapexRequestCCP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CapexRequestCCP2PServiceURL).Key), clsConfig._CapexRequestCCP2PServiceURL.GetType()), clsConfig._CapexRequestCCP2PServiceURL.GetType())
            Session("BgtRequestValidationP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BgtRequestValidationP2PServiceURL).Key), clsConfig._BgtRequestValidationP2PServiceURL.GetType()), clsConfig._BgtRequestValidationP2PServiceURL.GetType())
            Session("NewRequisitionRequestP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NewRequisitionRequestP2PServiceURL).Key), clsConfig._NewRequisitionRequestP2PServiceURL.GetType()), clsConfig._NewRequisitionRequestP2PServiceURL.GetType())
            Session("SkipEmployeeApprovalFlow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEmployeeApprovalFlow).Key), clsConfig._SkipEmployeeApprovalFlow.GetType()), clsConfig._SkipEmployeeApprovalFlow.GetType())
            Session("SendDetailToEmployee") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SendDetailToEmployee).Key), clsConfig._SendDetailToEmployee.GetType()), clsConfig._SendDetailToEmployee.GetType())
            Session("SMSCompanyDetailToNewEmp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMSCompanyDetailToNewEmp).Key), clsConfig._SMSCompanyDetailToNewEmp.GetType()), clsConfig._SMSCompanyDetailToNewEmp.GetType())
            Session("IsHRFlexcubeIntegrated") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsHRFlexcubeIntegrated).Key), clsConfig._IsHRFlexcubeIntegrated.GetType()), clsConfig._IsHRFlexcubeIntegrated.GetType())

            Dim value = objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeServiceCollection).Key), clsConfig._FlexcubeServiceCollection.GetType())
            If value <> Nothing Then
            Session("FlexcubeServiceCollection") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeServiceCollection).Key), clsConfig._FlexcubeServiceCollection.GetType()), clsConfig._FlexcubeServiceCollection.GetType())
            Else
                Dim dict As New Dictionary(Of Integer, String)
                Session("FlexcubeServiceCollection") = dict
            End If


            Session("FlexcubeAccountCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeAccountCategory).Key), clsConfig._FlexcubeAccountCategory.GetType()), clsConfig._FlexcubeAccountCategory.GetType())
            Session("FlexcubeAccountClass") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeAccountClass).Key), clsConfig._FlexcubeAccountClass.GetType()), clsConfig._FlexcubeAccountClass.GetType())
            Session("SMSGatewayEmail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMSGatewayEmail).Key), clsConfig._SMSGatewayEmail.GetType()), clsConfig._SMSGatewayEmail.GetType())
            Session("SMSGatewayEmailType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMSGatewayEmailType).Key), clsConfig._SMSGatewayEmailType.GetType()), clsConfig._SMSGatewayEmailType.GetType())
            Session("EmployeeRejectNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeRejectNotificationUserIds).Key), clsConfig._EmployeeRejectNotificationUserIds.GetType()), clsConfig._EmployeeRejectNotificationUserIds.GetType())
            Session("EmployeeRejectNotificationTemplateId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeRejectNotificationTemplateId).Key), clsConfig._EmployeeRejectNotificationTemplateId.GetType()), clsConfig._EmployeeRejectNotificationTemplateId.GetType())
            Session("EmpMandatoryFieldsIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpMandatoryFieldsIDs).Key), clsConfig._EmpMandatoryFieldsIDs.GetType()), clsConfig._EmpMandatoryFieldsIDs.GetType())
            Session("PendingEmployeeScreenIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PendingEmployeeScreenIDs).Key), clsConfig._PendingEmployeeScreenIDs.GetType()), clsConfig._PendingEmployeeScreenIDs.GetType())
            Session("GoalTypeInclusion") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GoalTypeInclusion).Key), clsConfig._GoalTypeInclusion.GetType()), clsConfig._GoalTypeInclusion.GetType())
            Session("SkipApprovalOnEmpData") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalOnEmpData).Key), clsConfig._SkipApprovalOnEmpData.GetType()), clsConfig._SkipApprovalOnEmpData.GetType())
            Session("PendingEmployeeScreenIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PendingEmployeeScreenIDs).Key), clsConfig._PendingEmployeeScreenIDs.GetType()), clsConfig._PendingEmployeeScreenIDs.GetType())
            Session("EmployeeDataRejectedUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDataRejectedUserIds).Key), clsConfig._EmployeeDataRejectedUserIds.GetType()), clsConfig._EmployeeDataRejectedUserIds.GetType())
            Session("SectorRouteAssignToExpense") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SectorRouteAssignToExpense).Key), clsConfig._SectorRouteAssignToExpense.GetType()), clsConfig._SectorRouteAssignToExpense.GetType())
            Session("MakeEmpAssessCommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeEmpAssessCommentsMandatory).Key), clsConfig._MakeEmpAssessCommentsMandatory.GetType()), clsConfig._MakeEmpAssessCommentsMandatory.GetType())
            Session("MakeAsrAssessCommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAsrAssessCommentsMandatory).Key), clsConfig._MakeAsrAssessCommentsMandatory.GetType()), clsConfig._MakeAsrAssessCommentsMandatory.GetType())
            Session("MakeRevAssessCommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeRevAssessCommentsMandatory).Key), clsConfig._MakeRevAssessCommentsMandatory.GetType()), clsConfig._MakeRevAssessCommentsMandatory.GetType())

            Dim result = GetPropertyInfo(Function() clsConfig._AdministratorEmailForError)

            Session("AdministratorEmailForError") = objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(result.Key), result.Value)

            Session("AllowViewEmployeeScale") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowViewEmployeeScale).Key), clsConfig._AllowViewEmployeeScale.GetType()), clsConfig._AllowViewEmployeeScale.GetType())
            Session("AllowToAddEditEmployeePresentAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditEmployeePresentAddress).Key), clsConfig._AllowToAddEditEmployeePresentAddress.GetType()), clsConfig._AllowToAddEditEmployeePresentAddress.GetType())
            Session("AllowToAddEditEmployeeDomicileAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditEmployeeDomicileAddress).Key), clsConfig._AllowToAddEditEmployeeDomicileAddress.GetType()), clsConfig._AllowToAddEditEmployeeDomicileAddress.GetType())
            Session("AllowToAddEditEmployeeRecruitmentAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditEmployeeRecruitmentAddress).Key), clsConfig._AllowToAddEditEmployeeRecruitmentAddress.GetType()), clsConfig._AllowToAddEditEmployeeRecruitmentAddress.GetType())
            Session("AllowToEditLeaveApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToEditLeaveApplication).Key), clsConfig._AllowToEditLeaveApplication.GetType()), clsConfig._AllowToEditLeaveApplication.GetType())
            Session("AllowToDeleteLeaveApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToDeleteLeaveApplication).Key), clsConfig._AllowToDeleteLeaveApplication.GetType()), clsConfig._AllowToDeleteLeaveApplication.GetType())
            Session("ClaimRemarkMandatoryForClaim") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRemarkMandatoryForClaim).Key), clsConfig._ClaimRemarkMandatoryForClaim.GetType()), clsConfig._ClaimRemarkMandatoryForClaim.GetType())
            Session("DomicileDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DomicileDocsAttachmentMandatory).Key), clsConfig._DomicileDocsAttachmentMandatory.GetType()), clsConfig._DomicileDocsAttachmentMandatory.GetType())
            Session("ScoreCalibrationFormNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoreCalibrationFormNotype).Key), clsConfig._ScoreCalibrationFormNotype.GetType()), clsConfig._ScoreCalibrationFormNotype.GetType())
            Session("ScoreCalibrationFormNoPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoreCalibrationFormNoPrifix).Key), clsConfig._ScoreCalibrationFormNoPrifix.GetType()), clsConfig._ScoreCalibrationFormNoPrifix.GetType())
            Session("NextScoreCalibrationFormNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NextScoreCalibrationFormNo).Key), clsConfig._NextScoreCalibrationFormNo.GetType()), clsConfig._NextScoreCalibrationFormNo.GetType())
            Session("IsCalibrationSettingActive") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsCalibrationSettingActive).Key), clsConfig._IsCalibrationSettingActive.GetType()), clsConfig._IsCalibrationSettingActive.GetType())
            Session("EmployeeDisagreeGrievanceNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDisagreeGrievanceNotificationUserIds).Key), clsConfig._EmployeeDisagreeGrievanceNotificationUserIds.GetType()), clsConfig._EmployeeDisagreeGrievanceNotificationUserIds.GetType())
            Session("EmployeeDisagreeGrievanceNotificationEmailTitle") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDisagreeGrievanceNotificationEmailTitle).Key), clsConfig._EmployeeDisagreeGrievanceNotificationEmailTitle.GetType()), clsConfig._EmployeeDisagreeGrievanceNotificationEmailTitle.GetType())
            Session("EmployeeDisagreeGrievanceNotificationTemplateId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDisagreeGrievanceNotificationTemplateId).Key), clsConfig._EmployeeDisagreeGrievanceNotificationTemplateId.GetType()), clsConfig._EmployeeDisagreeGrievanceNotificationTemplateId.GetType())
            Session("EmpTimesheetSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpTimesheetSetting).Key), clsConfig._EmpTimesheetSetting.GetType()), clsConfig._EmpTimesheetSetting.GetType())
            Session("AllowEmpAssignedProjectExceedTime") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEmpAssignedProjectExceedTime).Key), clsConfig._AllowEmpAssignedProjectExceedTime.GetType()), clsConfig._AllowEmpAssignedProjectExceedTime.GetType())
            Session("Notify_LoanAdvance_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_LoanAdvance_Users).Key), clsConfig._Notify_LoanAdvance_Users.GetType()), clsConfig._Notify_LoanAdvance_Users.GetType())
            Session("GetTokenP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GetTokenP2PServiceURL).Key), clsConfig._GetTokenP2PServiceURL.GetType()), clsConfig._GetTokenP2PServiceURL.GetType())
            Session("EnforceManpowerPlanning") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EnforceManpowerPlanning).Key), clsConfig._EnforceManpowerPlanning.GetType()), clsConfig._EnforceManpowerPlanning.GetType())
            Session("ClaimRetirementFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRetirementFormNoType).Key), clsConfig._ClaimRetirementFormNoType.GetType()), clsConfig._ClaimRetirementFormNoType.GetType())
            Session("ShowInterdictionDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowInterdictionDate).Key), clsConfig._ShowInterdictionDate.GetType()), clsConfig._ShowInterdictionDate.GetType())
            Session("NotificationOnDisciplineFiling") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NotificationOnDisciplineFiling).Key), clsConfig._NotificationOnDisciplineFiling.GetType()), clsConfig._NotificationOnDisciplineFiling.GetType())
            Session("ShowReponseTypeOnPosting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowReponseTypeOnPosting).Key), clsConfig._ShowReponseTypeOnPosting.GetType()), clsConfig._ShowReponseTypeOnPosting.GetType())
            Session("PresentAddressDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PresentAddressDocsAttachmentMandatory).Key), clsConfig._PresentAddressDocsAttachmentMandatory.GetType()), clsConfig._PresentAddressDocsAttachmentMandatory.GetType())
            Session("RecruitmentAddressDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RecruitmentAddressDocsAttachmentMandatory).Key), clsConfig._RecruitmentAddressDocsAttachmentMandatory.GetType()), clsConfig._RecruitmentAddressDocsAttachmentMandatory.GetType())
            Session("IdentityDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IdentityDocsAttachmentMandatory).Key), clsConfig._IdentityDocsAttachmentMandatory.GetType()), clsConfig._IdentityDocsAttachmentMandatory.GetType())
            Session("JobExperienceDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._JobExperienceDocsAttachmentMandatory).Key), clsConfig._JobExperienceDocsAttachmentMandatory.GetType()), clsConfig._JobExperienceDocsAttachmentMandatory.GetType())
            Session("RecruitMandatoryFieldsIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RecruitMandatoryFieldsIDs).Key), clsConfig._RecruitMandatoryFieldsIDs.GetType()), clsConfig._RecruitMandatoryFieldsIDs.GetType())
            Dim arrSettings() As String = CStr(Session("RecruitMandatoryFieldsIDs")).Split(CChar(","))
            If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CurriculumVitae).ToString) = True Then
                Session("OneCurriculamVitaeMandatory") = True
            Else
                Session("OneCurriculamVitaeMandatory") = False
            End If
            If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CoverLetter).ToString) = True Then
                Session("OneCoverLetterMandatory") = True
            Else
                Session("OneCoverLetterMandatory") = False
            End If
            If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentQualification).ToString) = True Then
                Session("AttachmentQualificationMandatory") = True
            Else
                Session("AttachmentQualificationMandatory") = False
            End If
            'Sohail (11 Apr 2022) -- End

            'Hemant (18 May 2022) -- Start
            'ISSUE/ENHANCEMENT(ZRA) : AC2-384,AC2-386  Apply experience required validation if year of experienced is required on Apply now button on search job screen in self service.
            If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentTypeImage).ToString) = True Then
                Session("AllowableAttachmentTypeImage") = True
            Else
                Session("AllowableAttachmentTypeImage") = False
            End If
            If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentTypeDocument).ToString) = True Then
                Session("AllowableAttachmentTypeDocument") = True
            Else
                Session("AllowableAttachmentTypeDocument") = False
            End If
            Session("RecruitMandatoryJobExperiences") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RecruitMandatoryJobExperiences).Key), clsConfig._RecruitMandatoryJobExperiences.GetType()), clsConfig._RecruitMandatoryJobExperiences.GetType())
            Session("ClaimRetirementTypeId") = objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRetirementTypeId).Key), clsConfig._ClaimRetirementTypeId.GetType())
            Session("OTTenureDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OTTenureDays).Key), clsConfig._OTTenureDays.GetType()), clsConfig._OTTenureDays.GetType())
            Session("AdditionalStaffRequisitionDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AdditionalStaffRequisitionDocsAttachmentMandatory).Key), clsConfig._AdditionalStaffRequisitionDocsAttachmentMandatory.GetType()), clsConfig._AdditionalStaffRequisitionDocsAttachmentMandatory.GetType())
            Session("AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory).Key), clsConfig._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory.GetType()), clsConfig._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory.GetType())
            Session("ReplacementStaffRequisitionDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ReplacementStaffRequisitionDocsAttachmentMandatory).Key), clsConfig._ReplacementStaffRequisitionDocsAttachmentMandatory.GetType()), clsConfig._ReplacementStaffRequisitionDocsAttachmentMandatory.GetType())
            Session("GrievanceReportingToMaxApprovalLevel") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceReportingToMaxApprovalLevel).Key), clsConfig._GrievanceReportingToMaxApprovalLevel.GetType()), clsConfig._GrievanceReportingToMaxApprovalLevel.GetType())
            Session("AllowToviewEmpTimesheetReportForESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToviewEmpTimesheetReportForESS).Key), clsConfig._AllowToviewEmpTimesheetReportForESS.GetType()), clsConfig._AllowToviewEmpTimesheetReportForESS.GetType())
            Session("TrainingNeedFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingNeedFormNoType).Key), clsConfig._TrainingNeedFormNoType.GetType()), clsConfig._TrainingNeedFormNoType.GetType())
            Session("TrainingNeedFormNoPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingNeedFormNoPrefix).Key), clsConfig._TrainingNeedFormNoPrefix.GetType()), clsConfig._TrainingNeedFormNoPrefix.GetType())
            Session("ApplicableLeaveStatus") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicableLeaveStatus).Key), clsConfig._ApplicableLeaveStatus.GetType()), clsConfig._ApplicableLeaveStatus.GetType())
            Session("AllowToCancelLeaveButRetainExpense") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToCancelLeaveButRetainExpense).Key), clsConfig._AllowToCancelLeaveButRetainExpense.GetType()), clsConfig._AllowToCancelLeaveButRetainExpense.GetType())
            Session("AllowToViewScoreWhileDoingAssesment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToViewScoreWhileDoingAssesment).Key), clsConfig._AllowToViewScoreWhileDoingAssesment.GetType()), clsConfig._AllowToViewScoreWhileDoingAssesment.GetType())
            Session("AllowToCancelLeaveForClosedPeriod") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToCancelLeaveForClosedPeriod).Key), clsConfig._AllowToCancelLeaveForClosedPeriod.GetType()), clsConfig._AllowToCancelLeaveForClosedPeriod.GetType())
            Session("AllowEmployeeToAddEditSignatureESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEmployeeToAddEditSignatureESS).Key), clsConfig._AllowEmployeeToAddEditSignatureESS.GetType()), clsConfig._AllowEmployeeToAddEditSignatureESS.GetType())
            Session("AllowOnlyOneExpenseInClaimApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowOnlyOneExpenseInClaimApplication).Key), clsConfig._AllowOnlyOneExpenseInClaimApplication.GetType()), clsConfig._AllowOnlyOneExpenseInClaimApplication.GetType())
            Session("AllowTerminationIfPaymentDone") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowTerminationIfPaymentDone).Key), clsConfig._AllowTerminationIfPaymentDone.GetType()), clsConfig._AllowTerminationIfPaymentDone.GetType())
            Session("applicant_declaration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicantDeclaration).Key), clsConfig._ApplicantDeclaration.GetType()), clsConfig._ApplicantDeclaration.GetType())
            Session("VacancyFoundOutFromMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._VacancyFoundOutFromMandatoryInRecruitment).Key), clsConfig._VacancyFoundOutFromMandatoryInRecruitment.GetType()), clsConfig._VacancyFoundOutFromMandatoryInRecruitment.GetType())
            Session("EarliestPossibleStartDateMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EarliestPossibleStartDateMandatoryInRecruitment).Key), clsConfig._EarliestPossibleStartDateMandatoryInRecruitment.GetType()), clsConfig._EarliestPossibleStartDateMandatoryInRecruitment.GetType())
            Session("CommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CommentsMandatoryInRecruitment).Key), clsConfig._CommentsMandatoryInRecruitment.GetType()), clsConfig._CommentsMandatoryInRecruitment.GetType())
            Session("isvacancyalert") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._isVacancyAlert).Key), clsConfig._isVacancyAlert.GetType()), clsConfig._isVacancyAlert.GetType())
            Session("issendjobconfirmationemail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SendJobConfirmationEmail).Key), clsConfig._SendJobConfirmationEmail.GetType()), clsConfig._SendJobConfirmationEmail.GetType())
            Session("isattachapplicantcv") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AttachApplicantCV).Key), clsConfig._AttachApplicantCV.GetType()), clsConfig._AttachApplicantCV.GetType())
            Session("applicantunkid") = 0
            Session("LocalizationCountryUnkid") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CountryUnkid).Key), clsConfig._CountryUnkid.GetType()), clsConfig._CountryUnkid.GetType())
            Session("ApplicantCodeNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicantCodeNotype).Key), clsConfig._ApplicantCodeNotype.GetType()), clsConfig._ApplicantCodeNotype.GetType())
            Session("ApplicantCodePrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicantCodePrifix).Key), clsConfig._ApplicantCodePrifix.GetType()), clsConfig._ApplicantCodePrifix.GetType())
            Session("IgnoreNegativeNetPayEmployeesOnJV") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IgnoreNegativeNetPayEmployeesOnJV).Key), clsConfig._IgnoreNegativeNetPayEmployeesOnJV.GetType()), clsConfig._IgnoreNegativeNetPayEmployeesOnJV.GetType())
            Session("ShowRemainingNoOfLoanInstallmentsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowRemainingNoOfLoanInstallmentsOnPayslip).Key), clsConfig._ShowRemainingNoOfLoanInstallmentsOnPayslip.GetType()), clsConfig._ShowRemainingNoOfLoanInstallmentsOnPayslip.GetType())
            Session("SkipApprovalFlowForApplicantShortListing") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalFlowForApplicantShortListing).Key), clsConfig._SkipApprovalFlowForApplicantShortListing.GetType()), clsConfig._SkipApprovalFlowForApplicantShortListing.GetType())
            Session("SkipApprovalFlowForFinalApplicant") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalFlowForFinalApplicant).Key), clsConfig._SkipApprovalFlowForFinalApplicant.GetType()), clsConfig._SkipApprovalFlowForFinalApplicant.GetType())
            Session("RunCompetenceAssessmentSeparately") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RunCompetenceAssessmentSeparately).Key), clsConfig._RunCompetenceAssessmentSeparately.GetType()), clsConfig._RunCompetenceAssessmentSeparately.GetType())
            Session("AllocationWorkStation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllocationWorkStation).Key), clsConfig._AllocationWorkStation.GetType()), clsConfig._AllocationWorkStation.GetType())
            Session("CmptScoringOptionId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CmptScoringOptionId).Key), clsConfig._CmptScoringOptionId.GetType()), clsConfig._CmptScoringOptionId.GetType())
            Session("OrgScoringOptionId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoringOptionId).Key), clsConfig._ScoringOptionId.GetType()), clsConfig._ScoringOptionId.GetType())
            Session("EmpBirthdayAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpBirthdayAllocation).Key), clsConfig._EmpBirthdayAllocation.GetType()), clsConfig._EmpBirthdayAllocation.GetType())
            Session("EmpStaffTurnOverAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpStaffTurnOverAllocation).Key), clsConfig._EmpStaffTurnOverAllocation.GetType()), clsConfig._EmpStaffTurnOverAllocation.GetType())
            Session("EmpOnLeaveAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpOnLeaveAllocation).Key), clsConfig._EmpOnLeaveAllocation.GetType()), clsConfig._EmpOnLeaveAllocation.GetType())
            Session("EmpTeamMembersAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpTeamMembersAllocation).Key), clsConfig._EmpTeamMembersAllocation.GetType()), clsConfig._EmpTeamMembersAllocation.GetType())
            Session("EmpWorkAnniversaryAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpWorkAnniversaryAllocation).Key), clsConfig._EmpWorkAnniversaryAllocation.GetType()), clsConfig._EmpWorkAnniversaryAllocation.GetType())
            Session("EmpTnADetailsAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpTnADetailsAllocation).Key), clsConfig._EmpTnADetailsAllocation.GetType()), clsConfig._EmpTnADetailsAllocation.GetType())
            Session("CompetenciesAssess_Instructions") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompetenciesAssess_Instructions).Key), clsConfig._CompetenciesAssess_Instructions.GetType()), clsConfig._CompetenciesAssess_Instructions.GetType())
            Session("DeptTrainingNeedListColumnsIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DeptTrainingNeedListColumnsIDs).Key), clsConfig._DeptTrainingNeedListColumnsIDs.GetType()), clsConfig._DeptTrainingNeedListColumnsIDs.GetType())
            Session("TrainingNeedAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingNeedAllocationID).Key), clsConfig._TrainingNeedAllocationID.GetType()), clsConfig._TrainingNeedAllocationID.GetType())
            Session("TrainingBudgetAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingBudgetAllocationID).Key), clsConfig._TrainingBudgetAllocationID.GetType()), clsConfig._TrainingBudgetAllocationID.GetType())
            Session("TrainingCostCenterAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingCostCenterAllocationID).Key), clsConfig._TrainingCostCenterAllocationID.GetType()), clsConfig._TrainingCostCenterAllocationID.GetType())
            Session("TrainingRemainingBalanceBasedOnID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingRemainingBalanceBasedOnID).Key), clsConfig._TrainingRemainingBalanceBasedOnID.GetType()), clsConfig._TrainingRemainingBalanceBasedOnID.GetType())
            Session("ApplyNonDisclosureDeclaration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplyNonDisclosureDeclaration).Key), clsConfig._ApplyNonDisclosureDeclaration.GetType()), clsConfig._ApplyNonDisclosureDeclaration.GetType())
            Session("IsPDP_Perf_Integrated") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsPDP_Perf_Integrated).Key), clsConfig._IsPDP_Perf_Integrated.GetType()), clsConfig._IsPDP_Perf_Integrated.GetType())
            Session("PreTrainingEvaluationSubmitted") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PreTrainingEvaluationSubmitted).Key), clsConfig._PreTrainingEvaluationSubmitted.GetType()), clsConfig._PreTrainingEvaluationSubmitted.GetType())
            Session("PreTrainingFeedbackInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PreTrainingFeedbackInstruction).Key), clsConfig._PreTrainingFeedbackInstruction.GetType()), clsConfig._PreTrainingFeedbackInstruction.GetType())
            Session("PostTrainingFeedbackInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PostTrainingFeedbackInstruction).Key), clsConfig._PostTrainingFeedbackInstruction.GetType()), clsConfig._PostTrainingFeedbackInstruction.GetType())
            Session("DaysAfterTrainingFeedbackInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterTrainingFeedbackInstruction).Key), clsConfig._DaysAfterTrainingFeedbackInstruction.GetType()), clsConfig._DaysAfterTrainingFeedbackInstruction.GetType())
            Session("MaxEmpPlannedLeave") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MaxEmpPlannedLeave).Key), clsConfig._MaxEmpPlannedLeave.GetType()), clsConfig._MaxEmpPlannedLeave.GetType())
            Session("SkipTrainingRequisitionAndApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingRequisitionAndApproval).Key), clsConfig._SkipTrainingRequisitionAndApproval.GetType()), clsConfig._SkipTrainingRequisitionAndApproval.GetType())
            Session("SkipEOCValidationOnLoanTenure") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEOCValidationOnLoanTenure).Key), clsConfig._SkipEOCValidationOnLoanTenure.GetType()), clsConfig._SkipEOCValidationOnLoanTenure.GetType())
            Session("TrainingCompetenciesPeriodId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingCompetenciesPeriodId).Key), clsConfig._TrainingCompetenciesPeriodId.GetType()), clsConfig._TrainingCompetenciesPeriodId.GetType())
            Session("DaysFromLastDateOfTrainingToAllowCompleteTraining") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysFromLastDateOfTrainingToAllowCompleteTraining).Key), clsConfig._DaysFromLastDateOfTrainingToAllowCompleteTraining.GetType()), clsConfig._DaysFromLastDateOfTrainingToAllowCompleteTraining.GetType())
            Session("PostTrainingEvaluationBeforeCompleteTraining") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PostTrainingEvaluationBeforeCompleteTraining).Key), clsConfig._PostTrainingEvaluationBeforeCompleteTraining.GetType()), clsConfig._PostTrainingEvaluationBeforeCompleteTraining.GetType())
            Session("TrainingApproverAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingApproverAllocationID).Key), clsConfig._TrainingApproverAllocationID.GetType()), clsConfig._TrainingApproverAllocationID.GetType())
            Session("UnRetiredImprestToPayrollAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._UnRetiredImprestToPayrollAfterDays).Key), clsConfig._UnRetiredImprestToPayrollAfterDays.GetType()), clsConfig._UnRetiredImprestToPayrollAfterDays.GetType())
            Session("DoNotAllowToApplyImprestIfUnRetiredForExpCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowToApplyImprestIfUnRetiredForExpCategory).Key), clsConfig._DoNotAllowToApplyImprestIfUnRetiredForExpCategory.GetType()), clsConfig._DoNotAllowToApplyImprestIfUnRetiredForExpCategory.GetType())
            Session("TrainingFinalApprovedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingFinalApprovedNotificationUserIds).Key), clsConfig._TrainingFinalApprovedNotificationUserIds.GetType()), clsConfig._TrainingFinalApprovedNotificationUserIds.GetType())
            Session("LoanIntegration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanIntegration).Key), clsConfig._LoanIntegration.GetType()), clsConfig._LoanIntegration.GetType())
            Session("AddProceedingAgainstEachCount") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AddProceedingAgainstEachCount).Key), clsConfig._AddProceedingAgainstEachCount.GetType()), clsConfig._AddProceedingAgainstEachCount.GetType())
            Session("RoleBasedLoanApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RoleBasedLoanApproval).Key), clsConfig._RoleBasedLoanApproval.GetType()), clsConfig._RoleBasedLoanApproval.GetType())
            Session("LoanCentralizedApproverId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanCentralizedApproverId).Key), clsConfig._LoanCentralizedApproverId.GetType()), clsConfig._LoanCentralizedApproverId.GetType())
            Session("ShowWeightOnAssessScreenRatingScale") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowWeightOnAssessScreenRatingScale).Key), clsConfig._ShowWeightOnAssessScreenRatingScale.GetType()), clsConfig._ShowWeightOnAssessScreenRatingScale.GetType())
            Session("EFTRequestURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTRequestURL).Key), clsConfig._EFTRequestURL.GetType()), clsConfig._EFTRequestURL.GetType())
            Session("EFTReconciliationURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTReconciliationURL).Key), clsConfig._EFTReconciliationURL.GetType()), clsConfig._EFTReconciliationURL.GetType())
            Session("EFTRequestClientID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTRequestClientID).Key), clsConfig._EFTRequestClientID.GetType()), clsConfig._EFTRequestClientID.GetType())
            Session("EFTRequestPassword") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTRequestPassword).Key), clsConfig._EFTRequestPassword.GetType()), clsConfig._EFTRequestPassword.GetType())
            Session("SkipTrainingEnrollmentProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingEnrollmentProcess).Key), clsConfig._SkipTrainingEnrollmentProcess.GetType()), clsConfig._SkipTrainingEnrollmentProcess.GetType())
            Session("SkipTrainingCompletionProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingCompletionProcess).Key), clsConfig._SkipTrainingCompletionProcess.GetType()), clsConfig._SkipTrainingCompletionProcess.GetType())
            Session("DaysForEmployeeEvaluationReminderEmailAfterTrainingStartDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterTrainingStartDateEmployeeEvaluation).Key), clsConfig._DaysAfterTrainingStartDateEmployeeEvaluation.GetType()), clsConfig._DaysAfterTrainingStartDateEmployeeEvaluation.GetType())
            Session("DaysForEmployeeEvaluationReminderEmailAfterTrainingEndDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterTrainingEndDateEmployeeEvaluation).Key), clsConfig._DaysAfterTrainingEndDateEmployeeEvaluation.GetType()), clsConfig._DaysAfterTrainingEndDateEmployeeEvaluation.GetType())
            Session("DaysForManagerEvaluationReminderEmailAfterSelfEvaluation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterSelfEvaluationManagerEvaluation).Key), clsConfig._DaysAfterSelfEvaluationManagerEvaluation.GetType()), clsConfig._DaysAfterSelfEvaluationManagerEvaluation.GetType())
            Session("SkipPreTrainingEvaluationProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipPreTrainingEvaluationProcess).Key), clsConfig._SkipPreTrainingEvaluationProcess.GetType()), clsConfig._SkipPreTrainingEvaluationProcess.GetType())
            Session("SkipPostTrainingEvaluationProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipPostTrainingEvaluationProcess).Key), clsConfig._SkipPostTrainingEvaluationProcess.GetType()), clsConfig._SkipPostTrainingEvaluationProcess.GetType())
            Session("SkipDaysAfterTrainingEvaluationProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipDaysAfterTrainingEvaluationProcess).Key), clsConfig._SkipDaysAfterTrainingEvaluationProcess.GetType()), clsConfig._SkipDaysAfterTrainingEvaluationProcess.GetType())
            Session("SkipTrainingBudgetApprovalProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingBudgetApprovalProcess).Key), clsConfig._SkipTrainingBudgetApprovalProcess.GetType()), clsConfig._SkipTrainingBudgetApprovalProcess.GetType())
            Session("TrainingRequestApprovalSettingID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingRequestApprovalSettingID).Key), clsConfig._TrainingRequestApprovalSettingID.GetType()), clsConfig._TrainingRequestApprovalSettingID.GetType())
            If Session("TrainingRequestApprovalSettingID") <= 0 Then
                Session("TrainingRequestApprovalSettingID") = enTrainingRequestApproval.RolebaseAccess
            End If

            Session("DontAllowRatingBeyond100") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowRatingBeyond100).Key), clsConfig._DontAllowRatingBeyond100.GetType()), clsConfig._DontAllowRatingBeyond100.GetType())

            value = objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BSC_StatusColors).Key), clsConfig._BSC_StatusColors.GetType())
            If value <> Nothing Then
                Session("BSC_StatusColors") = value
            Else
                Dim dict As New Dictionary(Of Integer, Integer)
                Session("BSC_StatusColors") = dict
            End If

            Session("ConsiderZeroAsNumUpdateProgress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ConsiderZeroAsNumUpdateProgress).Key), clsConfig._ConsiderZeroAsNumUpdateProgress.GetType()), clsConfig._ConsiderZeroAsNumUpdateProgress.GetType())
            Session("MisconductConsecutiveAbsentDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MisconductConsecutiveAbsentDays).Key), clsConfig._MisconductConsecutiveAbsentDays.GetType()), clsConfig._MisconductConsecutiveAbsentDays.GetType())
            Session("MaxRoutePerExpCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MaxRoutePerExpCategory).Key), clsConfig._MaxRoutePerExpCategory.GetType()), clsConfig._MaxRoutePerExpCategory.GetType())
            Session("DoNotAllowCurrentFutureDatesToApplyOT") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowCurrentFutureDatesToApplyOT).Key), clsConfig._DoNotAllowCurrentFutureDatesToApplyOT.GetType()), clsConfig._DoNotAllowCurrentFutureDatesToApplyOT.GetType())
            Session("DoNotAllowOTEndTimeGreaterThanClockOutTime") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowOTEndTimeGreaterThanClockOutTime).Key), clsConfig._DoNotAllowOTEndTimeGreaterThanClockOutTime.GetType()), clsConfig._DoNotAllowOTEndTimeGreaterThanClockOutTime.GetType())
            Session("ApplyTOTPForLoanApplicationApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplyTOTPForLoanApplicationApproval).Key), clsConfig._ApplyTOTPForLoanApplicationApproval.GetType()), clsConfig._ApplyTOTPForLoanApplicationApproval.GetType())
            Session("TOTPTimeLimitInSeconds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TOTPTimeLimitInSeconds).Key), clsConfig._TOTPTimeLimitInSeconds.GetType()), clsConfig._TOTPTimeLimitInSeconds.GetType())
            Session("MaxTOTPUnSuccessfulAttempts") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MaxTOTPUnSuccessfulAttempts).Key), clsConfig._MaxTOTPUnSuccessfulAttempts.GetType()), clsConfig._MaxTOTPUnSuccessfulAttempts.GetType())
            Session("TOTPRetryAfterMins") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TOTPRetryAfterMins).Key), clsConfig._TOTPRetryAfterMins.GetType()), clsConfig._TOTPRetryAfterMins.GetType())
            Session("MakeAttachmentMandatoryOnTransfer") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnTransfer).Key), clsConfig._MakeAttachmentMandatoryOnTransfer.GetType()), clsConfig._MakeAttachmentMandatoryOnTransfer.GetType())
            Session("MakeAttachmentMandatoryOnRecategorize") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnRecategorize).Key), clsConfig._MakeAttachmentMandatoryOnRecategorize.GetType()), clsConfig._MakeAttachmentMandatoryOnRecategorize.GetType())
            Session("MakeAttachmentMandatoryOnSalaryChange") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnSalaryChange).Key), clsConfig._MakeAttachmentMandatoryOnSalaryChange.GetType()), clsConfig._MakeAttachmentMandatoryOnSalaryChange.GetType())
            Session("MakeAttachmentMandatoryOnPromotion") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnPromotion).Key), clsConfig._MakeAttachmentMandatoryOnPromotion.GetType()), clsConfig._MakeAttachmentMandatoryOnPromotion.GetType())
            Session("MakeAttachmentMandatoryOnRehire") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnRehire).Key), clsConfig._MakeAttachmentMandatoryOnRehire.GetType()), clsConfig._MakeAttachmentMandatoryOnRehire.GetType())
            Session("PhotoPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PhotoPath).Key), clsConfig._PhotoPath.GetType()), clsConfig._PhotoPath.GetType())
            Session("StaffTransferRequestAllocations") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffTransferRequestAllocations).Key), clsConfig._StaffTransferRequestAllocations.GetType()), clsConfig._StaffTransferRequestAllocations.GetType())
            Session("StaffTransferFinalApprovalNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffTransferFinalApprovalNotificationUserIds).Key), clsConfig._StaffTransferFinalApprovalNotificationUserIds.GetType()), clsConfig._StaffTransferFinalApprovalNotificationUserIds.GetType())
            Session("SingleCommentForAllObjectivesForEmpAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllObjectivesForEmpAssess).Key), clsConfig._SingleCommentForAllObjectivesForEmpAssess.GetType()), clsConfig._SingleCommentForAllObjectivesForEmpAssess.GetType())
            Session("SingleCommentForAllObjectivesForAsrAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllObjectivesForAsrAssess).Key), clsConfig._SingleCommentForAllObjectivesForAsrAssess.GetType()), clsConfig._SingleCommentForAllObjectivesForAsrAssess.GetType())
            Session("SingleCommentForAllObjectivesForRevAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllObjectivesForRevAssess).Key), clsConfig._SingleCommentForAllObjectivesForRevAssess.GetType()), clsConfig._SingleCommentForAllObjectivesForRevAssess.GetType())
            Session("SingleCommentForAllCompetenciesForEmpAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllCompetenciesForEmpAssess).Key), clsConfig._SingleCommentForAllCompetenciesForEmpAssess.GetType()), clsConfig._SingleCommentForAllCompetenciesForEmpAssess.GetType())
            Session("SingleCommentForAllCompetenciesForAsrAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllCompetenciesForAsrAssess).Key), clsConfig._SingleCommentForAllCompetenciesForAsrAssess.GetType()), clsConfig._SingleCommentForAllCompetenciesForAsrAssess.GetType())
            Session("SingleCommentForAllCompetenciesForRevAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllCompetenciesForRevAssess).Key), clsConfig._SingleCommentForAllCompetenciesForRevAssess.GetType()), clsConfig._SingleCommentForAllCompetenciesForRevAssess.GetType())
            Session("AllowPartialApproveRejectObjectives") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowPartialApproveRejectObjectives).Key), clsConfig._AllowPartialApproveRejectObjectives.GetType()), clsConfig._AllowPartialApproveRejectObjectives.GetType())
            Session("EFTMobileMoneyBank") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyBank).Key), clsConfig._EFTMobileMoneyBank.GetType()), clsConfig._EFTMobileMoneyBank.GetType())
            Session("EFTMobileMoneyBranch") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyBranch).Key), clsConfig._EFTMobileMoneyBranch.GetType()), clsConfig._EFTMobileMoneyBranch.GetType())
            Session("EFTMobileMoneyAccountName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyAccountName).Key), clsConfig._EFTMobileMoneyAccountName.GetType()), clsConfig._EFTMobileMoneyAccountName.GetType())
            Session("EFTMobileMoneyAccountNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyAccountNo).Key), clsConfig._EFTMobileMoneyAccountNo.GetType()), clsConfig._EFTMobileMoneyAccountNo.GetType())
            Session("CompanyAssetP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyAssetP2PServiceURL).Key), clsConfig._CompanyAssetP2PServiceURL.GetType()), clsConfig._CompanyAssetP2PServiceURL.GetType())
            Session("MandatoryAttachmentsForLoanTrancheApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MandatoryAttachmentsForLoanTrancheApplication).Key), clsConfig._MandatoryAttachmentsForLoanTrancheApplication.GetType()), clsConfig._MandatoryAttachmentsForLoanTrancheApplication.GetType())
            Session("EmpNotificationTrancheDateBeforeDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpNotificationTrancheDateBeforeDays).Key), clsConfig._EmpNotificationTrancheDateBeforeDays.GetType()), clsConfig._EmpNotificationTrancheDateBeforeDays.GetType())
            Session("MakeRemarkMandatoryForProgressUpdate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeRemarkMandatoryForProgressUpdate).Key), clsConfig._MakeRemarkMandatoryForProgressUpdate.GetType()), clsConfig._MakeRemarkMandatoryForProgressUpdate.GetType())
            Session("CompanyAssetP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyAssetP2PServiceURL).Key), clsConfig._CompanyAssetP2PServiceURL.GetType()), clsConfig._CompanyAssetP2PServiceURL.GetType())
            Session("AllowRejectedProgressUpdateToEdit") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowRejectedProgressUpdateToEdit).Key), clsConfig._AllowRejectedProgressUpdateToEdit.GetType()), clsConfig._AllowRejectedProgressUpdateToEdit.GetType())
            Session("LoginRequiredToApproveApplications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoginRequiredToApproveApplications).Key), clsConfig._LoginRequiredToApproveApplications.GetType()), clsConfig._LoginRequiredToApproveApplications.GetType())
            Session("LoanFlexcubeSuccessfulNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanFlexcubeSuccessfulNotificationUserIds).Key), clsConfig._LoanFlexcubeSuccessfulNotificationUserIds.GetType()), clsConfig._LoanFlexcubeSuccessfulNotificationUserIds.GetType())
            Session("EvaluationTypeId") = CInt(clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH)



            'Session("DateFormat") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyDateFormat).Key)), clsConfig._CompanyDateFormat.GetType())
            'Session("DateSeparator") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyDateSeparator).Key)), clsConfig._CompanyDateSeparator.GetType())
            'Session("FirstNamethenSurname") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FirstNamethenSurname).Key)), clsConfig._FirstNamethenSurname.GetType())
            'Session("IsIncludeInactiveEmp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsIncludeInactiveEmp).Key)), clsConfig._IsIncludeInactiveEmp.GetType())
            'Session("UserAccessModeSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._UserAccessModeSetting).Key)), clsConfig._UserAccessModeSetting.GetType())
            'Session("EmployeeAsOnDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeAsOnDate).Key)), clsConfig._EmployeeAsOnDate.GetType())
            'Session("SickSheetNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetNotype).Key)), clsConfig._SickSheetNotype.GetType())
            'Session("fmtCurrency") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CurrencyFormat).Key)), clsConfig._CurrencyFormat.GetType())
            'Session("AllowEditAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditAddress).Key)), clsConfig._AllowEditAddress.GetType())
            'Session("AllowEditPersonalInfo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditPersonalInfo).Key)), clsConfig._AllowEditPersonalInfo.GetType())
            'Session("AllowEditEmergencyAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditEmergencyAddress).Key)), clsConfig._AllowEditEmergencyAddress.GetType())
            'Session("LoanApplicationNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanApplicationNoType).Key)), clsConfig._LoanApplicationNoType.GetType())
            'Session("LoanApplicationPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanApplicationPrifix).Key)), clsConfig._LoanApplicationPrifix.GetType())
            'Session("NextLoanApplicationNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NextLoanApplicationNo).Key)), clsConfig._NextLoanApplicationNo.GetType())
            'Session("BatchPostingNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BatchPostingNotype).Key)), clsConfig._BatchPostingNotype.GetType())
            'Session("BatchPostingPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BatchPostingPrifix).Key)), clsConfig._BatchPostingPrifix.GetType())
            'Session("DoNotAllowOverDeductionForPayment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowOverDeductionForPayment).Key)), clsConfig._DoNotAllowOverDeductionForPayment.GetType())
            'Session("PaymentVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentVocNoType).Key)), clsConfig._PaymentVocNoType.GetType())
            'Session("DontAllowPaymentIfNetPayGoesBelow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowPaymentIfNetPayGoesBelow).Key)), clsConfig._DontAllowPaymentIfNetPayGoesBelow.GetType())
            'Session("PaymentVocPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentVocPrefix).Key)), clsConfig._PaymentVocPrefix.GetType())
            'Session("LeaveCancelFormNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveCancelFormNotype).Key)), clsConfig._LeaveCancelFormNotype.GetType())
            'Session("LeaveCancelFormNoPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveCancelFormNoPrifix).Key)), clsConfig._LeaveCancelFormNoPrifix.GetType())
            'Session("IsDenominationCompulsory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDenominationCompulsory).Key)), clsConfig._IsDenominationCompulsory.GetType())
            'Session("SetPayslipPaymentApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetPayslipPaymentApproval).Key)), clsConfig._SetPayslipPaymentApproval.GetType())
            'Session("AssetDeclarationInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationInstruction).Key)), clsConfig._AssetDeclarationInstruction.GetType())
            'Session("NonDisclosureDeclaration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclaration).Key)), clsConfig._NonDisclosureDeclaration.GetType())
            'Session("NonDisclosureWitnesser1") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureWitnesser1).Key)), clsConfig._NonDisclosureWitnesser1.GetType())
            'Session("NonDisclosureWitnesser2") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureWitnesser2).Key)), clsConfig._NonDisclosureWitnesser2.GetType())
            'Session("NonDisclosureWitnesser2") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureWitnesser2).Key)), clsConfig._NonDisclosureWitnesser2.GetType())
            'Session("NonDisclosureDeclarationFromDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclarationFromDate).Key)), clsConfig._NonDisclosureDeclarationFromDate.GetType())
            'Session("NonDisclosureDeclarationToDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclarationToDate).Key)), clsConfig._NonDisclosureDeclarationToDate.GetType())
            'Session("LockDaysAfterNonDisclosureDeclarationToDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._LockDaysAfterNonDisclosureDeclarationToDate)).Key)), CInt(clsConfig._LockDaysAfterNonDisclosureDeclarationToDate).GetType())
            'Session("NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate)).Key)), CInt(clsConfig._NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate).GetType())
            'Session("Document_Path") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Document_Path).Key)), clsConfig._Document_Path.GetType())
            'Session("IgnoreZeroValueHeadsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IgnoreZeroValueHeadsOnPayslip).Key)), clsConfig._IgnoreZeroValueHeadsOnPayslip.GetType())
            'Session("ShowLoanBalanceOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowLoanBalanceOnPayslip).Key)), clsConfig._ShowLoanBalanceOnPayslip.GetType())
            'Session("ShowSavingBalanceOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowSavingBalanceOnPayslip).Key)), clsConfig._ShowSavingBalanceOnPayslip.GetType())
            'Session("ShowEmployerContributionOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowEmployerContributionOnPayslip).Key)), clsConfig._ShowEmployerContributionOnPayslip.GetType())
            'Session("ShowAllHeadsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowAllHeadsOnPayslip).Key)), clsConfig._ShowAllHeadsOnPayslip.GetType())
            'Session("LogoOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LogoOnPayslip).Key)), clsConfig._LogoOnPayslip.GetType())
            'Session("PayslipTemplate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PayslipTemplate).Key)), clsConfig._PayslipTemplate.GetType())
            'Session("LeaveTypeOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveTypeOnPayslip).Key)), clsConfig._LeaveTypeOnPayslip.GetType())
            'Session("ShowCumulativeAccrual") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowCumulativeAccrualOnPayslip).Key)), clsConfig._ShowCumulativeAccrualOnPayslip.GetType())
            'Session("Base_CurrencyId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Base_CurrencyId).Key)), clsConfig._Base_CurrencyId.GetType())
            'Session("ShowCategoryOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowCategoryOnPayslip).Key)), clsConfig._ShowCategoryOnPayslip.GetType())
            'Session("ShowInformationalHeadsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowInformationalHeadsOnPayslip).Key)), clsConfig._ShowInformationalHeadsOnPayslip.GetType())
            'Session("PaymentRoundingType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentRoundingType).Key)), clsConfig._PaymentRoundingType.GetType())
            'Session("PaymentRoundingMultiple") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentRoundingMultiple).Key)), clsConfig._PaymentRoundingMultiple.GetType())
            'Session("CFRoundingAbove") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CFRoundingAbove).Key)), clsConfig._CFRoundingAbove.GetType())
            'Session("ShowBirthDateOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBirthDateOnPayslip).Key)), clsConfig._ShowBirthDateOnPayslip.GetType())
            'Session("ShowAgeOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowAgeOnPayslip).Key)), clsConfig._ShowAgeOnPayslip.GetType())
            'Session("ShowNoOfDependantsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowNoOfDependantsOnPayslip).Key)), clsConfig._ShowNoOfDependantsOnPayslip.GetType())
            'Session("ShowMonthlySalaryOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowMonthlySalaryOnPayslip).Key)), clsConfig._ShowMonthlySalaryOnPayslip.GetType())
            'Session("OT1HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1HourHeadId).Key)), clsConfig._OT1HourHeadId.GetType())
            'Session("OT2HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2HourHeadId).Key)), clsConfig._OT2HourHeadId.GetType())
            'Session("OT3HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3HourHeadId).Key)), clsConfig._OT3HourHeadId.GetType())
            'Session("OT4HourHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4HourHeadId).Key)), clsConfig._OT4HourHeadId.GetType())
            'Session("OT1HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1HourHeadName).Key)), clsConfig._OT1HourHeadName.GetType())
            'Session("OT2HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2HourHeadName).Key)), clsConfig._OT2HourHeadName.GetType())
            'Session("OT3HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3HourHeadName).Key)), clsConfig._OT3HourHeadName.GetType())
            'Session("OT4HourHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4HourHeadName).Key)), clsConfig._OT4HourHeadName.GetType())
            'Session("OT1AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1AmountHeadId).Key)), clsConfig._OT1AmountHeadId.GetType())
            'Session("OT2AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2AmountHeadId).Key)), clsConfig._OT2AmountHeadId.GetType())
            'Session("OT3AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3AmountHeadId).Key)), clsConfig._OT3AmountHeadId.GetType())
            'Session("OT4AmountHeadID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4AmountHeadId).Key)), clsConfig._OT4AmountHeadId.GetType())
            'Session("OT1AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT1AmountHeadName).Key)), clsConfig._OT1AmountHeadName.GetType())
            'Session("OT2AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT2AmountHeadName).Key)), clsConfig._OT2AmountHeadName.GetType())
            'Session("OT3AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT3AmountHeadName).Key)), clsConfig._OT3AmountHeadName.GetType())
            'Session("OT4AmountHeadName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OT4AmountHeadName).Key)), clsConfig._OT4AmountHeadName.GetType())
            'Session("ShowSalaryOnHoldOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowSalaryOnHoldOnPayslip).Key)), clsConfig._ShowSalaryOnHoldOnPayslip.GetType())
            'Session("ArutiSelfServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ArutiSelfServiceURL).Key)), clsConfig._ArutiSelfServiceURL.GetType())

            'If Session("ArutiSelfServiceURL") = "http://" & HttpContext.Current.Request.ApplicationPath Then
            '    Session("ArutiSelfServiceURL") = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath
            'End If

            'Session("AllowEditExperience") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditExperience).Key)), clsConfig._AllowEditExperience.GetType())
            'Session("AllowDeleteExperience") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteExperience).Key)), clsConfig._AllowDeleteExperience.GetType())
            'Session("AllowAddSkills") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddSkills).Key)), clsConfig._AllowAddSkills.GetType())
            'Session("AllowEditSkills") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditSkills).Key)), clsConfig._AllowEditSkills.GetType())
            'Session("AllowDeleteSkills") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteSkills).Key)), clsConfig._AllowDeleteSkills.GetType())
            'Session("AllowAddDependants") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddDependants).Key)), clsConfig._AllowAddDependants.GetType())
            'Session("AllowDeleteDependants") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteDependants).Key)), clsConfig._AllowDeleteDependants.GetType())
            'Session("IsDependant_AgeLimit_Set") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDependant_AgeLimit_Set).Key)), clsConfig._IsDependant_AgeLimit_Set.GetType())
            'Session("AllowEditDependants") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditDependants).Key)), clsConfig._AllowEditDependants.GetType())
            'Session("AllowAddIdentity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddIdentity).Key)), clsConfig._AllowAddIdentity.GetType())
            'Session("AllowDeleteIdentity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteIdentity).Key)), clsConfig._AllowDeleteIdentity.GetType())
            'Session("AllowEditIdentity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditIdentity).Key)), clsConfig._AllowEditIdentity.GetType())
            'Session("AllowAddMembership") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddMembership).Key)), clsConfig._AllowAddMembership.GetType())
            'Session("AllowDeleteMembership") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteMembership).Key)), clsConfig._AllowDeleteMembership.GetType())
            'Session("AllowEditMembership") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditMembership).Key)), clsConfig._AllowEditMembership.GetType())
            'Session("AllowAddQualifications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddQualifications).Key)), clsConfig._AllowAddQualifications.GetType())
            'Session("AllowDeleteQualifications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteQualifications).Key)), clsConfig._AllowDeleteQualifications.GetType())
            'Session("AllowEditQualifications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditQualifications).Key)), clsConfig._AllowEditQualifications.GetType())
            'Session("AllowAddReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddReference).Key)), clsConfig._AllowAddReference.GetType())
            'Session("AllowDeleteReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowDeleteReference).Key)), clsConfig._AllowDeleteReference.GetType())
            'Session("AllowEditReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEditReference).Key)), clsConfig._AllowEditReference.GetType())
            'Session("IsBSC_ByEmployee") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsBSC_ByEmployee).Key)), clsConfig._IsBSC_ByEmployee.GetType())
            'Session("IsBSCObjectiveSaved") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsBSCObjectiveSaved).Key)), clsConfig._IsBSCObjectiveSaved.GetType())
            'Session("IsAllowFinalSave") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAllowFinalSave).Key)), clsConfig._IsAllowFinalSave.GetType())
            'Session("AllowAddExperience") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAddExperience).Key)), clsConfig._AllowAddExperience.GetType())
            'Session("IsAllocation_Hierarchy_Set") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAllocation_Hierarchy_Set).Key)), clsConfig._IsAllocation_Hierarchy_Set.GetType())
            'Session("Allocation_Hierarchy") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Allocation_Hierarchy).Key)), clsConfig._Allocation_Hierarchy.GetType())
            'Session("AllowChangeCompanyEmail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowChangeCompanyEmail).Key)), clsConfig._AllowChangeCompanyEmail.GetType())
            'Session("AllowToViewPersonalSalaryCalculationReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToViewPersonalSalaryCalculationReport).Key)), clsConfig._AllowToViewPersonalSalaryCalculationReport.GetType())
            'Session("AllowToViewEDDetailReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToViewEDDetailReport).Key)), clsConfig._AllowToViewEDDetailReport.GetType())
            'Session("SickSheetNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetNotype).Key)), clsConfig._SickSheetNotype.GetType())
            'Session("LeaveApproverForLeaveType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsLeaveApprover_ForLeaveType).Key)), clsConfig._IsLeaveApprover_ForLeaveType.GetType())
            'Session("LeaveFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveFormNoType).Key)), clsConfig._LeaveFormNoType.GetType())
            'Session("LoanApprover_ForLoanScheme") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsLoanApprover_ForLoanScheme).Key)), clsConfig._IsLoanApprover_ForLoanScheme.GetType())
            'Session("SendLoanEmailFromDesktopMSS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsSendLoanEmailFromDesktopMSS).Key)), clsConfig._IsSendLoanEmailFromDesktopMSS.GetType())
            'Session("SendLoanEmailFromESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsSendLoanEmailFromESS).Key)), clsConfig._IsSendLoanEmailFromESS.GetType())
            'Session("Advance_NetPayPercentage") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_NetPayPercentage).Key)), clsConfig._Advance_NetPayPercentage.GetType())
            'Session("Advance_NetPayTranheadUnkid") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_NetPayTranheadUnkid).Key)), clsConfig._Advance_NetPayTranheadUnkid.GetType())
            'Session("Advance_DontAllowAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Advance_DontAllowAfterDays).Key)), clsConfig._Advance_DontAllowAfterDays.GetType())
            'Session("CompanyDomain") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyDomain.ToString.Trim).Key)), clsConfig._CompanyDomain.ToString.Trim.GetType())
            'Session("Notify_Dates") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Dates).Key)), clsConfig._Notify_Dates.GetType())
            'Session("AppointeDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AppointeDateUserNotification).Key)), clsConfig._AppointeDateUserNotification.GetType())
            'Session("ConfirmDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ConfirmDateUserNotification).Key)), clsConfig._ConfirmDateUserNotification.GetType())
            'Session("BirthDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BirthDateUserNotification).Key)), clsConfig._BirthDateUserNotification.GetType())
            'Session("SuspensionDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SuspensionDateUserNotification).Key)), clsConfig._SuspensionDateUserNotification.GetType())
            'Session("ProbationDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ProbationDateUserNotification).Key)), clsConfig._ProbationDateUserNotification.GetType())
            'Session("EocDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EocDateUserNotification).Key)), clsConfig._EocDateUserNotification.GetType())
            'Session("LeavingDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeavingDateUserNotification).Key)), clsConfig._LeavingDateUserNotification.GetType())
            'Session("RetirementDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RetirementDateUserNotification).Key)), clsConfig._RetirementDateUserNotification.GetType())
            'Session("ReinstatementDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ReinstatementDateUserNotification).Key)), clsConfig._ReinstatementDateUserNotification.GetType())
            'Session("MarriageDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MarriageDateUserNotification).Key)), clsConfig._MarriageDateUserNotification.GetType())
            'Session("ExemptionDateUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ExemptionDateUserNotification).Key)), clsConfig._ExemptionDateUserNotification.GetType())
            'Session("Notify_EmplData") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_EmplData).Key)), clsConfig._Notify_EmplData.GetType())
            'Session("Notify_Allocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Allocation).Key)), clsConfig._Notify_Allocation.GetType())
            'Session("DatafileExportPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DatafileExportPath).Key)), clsConfig._DatafileExportPath.GetType())
            'Session("DatafileName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DatafileName).Key)), clsConfig._DatafileName.GetType())
            'Session("Accounting_TransactionReference") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_TransactionReference).Key)), clsConfig._Accounting_TransactionReference.GetType())
            'Session("Accounting_JournalType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_JournalType).Key)), clsConfig._Accounting_JournalType.GetType())
            'Session("Accounting_JVGroupCode") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_JVGroupCode).Key)), clsConfig._Accounting_JVGroupCode.GetType())
            'Session("LeaveBalanceSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveBalanceSetting).Key)), clsConfig._LeaveBalanceSetting.GetType())

            'If Session("LeaveBalanceSetting") <= 0 Then
            '    Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
            'End If

            'Session("AllowToviewPaysliponEss") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToviewPaysliponEss).Key)), clsConfig._AllowToviewPaysliponEss.GetType())
            'Session("ViewPayslipDaysBefore") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ViewPayslipDaysBefore).Key)), clsConfig._ViewPayslipDaysBefore.GetType())
            'Session("DontShowPayslipOnESSIfPaymentNotAuthorized") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontShowPayslipOnESSIfPaymentNotAuthorized).Key)), clsConfig._DontShowPayslipOnESSIfPaymentNotAuthorized.GetType())
            'Session("DontShowPayslipOnESSIfPaymentNotDone") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontShowPayslipOnESSIfPaymentNotDone).Key)), clsConfig._DontShowPayslipOnESSIfPaymentNotDone.GetType())
            'Session("ShowPayslipOnESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowHidePayslipOnESS).Key)), clsConfig._ShowHidePayslipOnESS.GetType())
            'Session("IsImgInDataBase") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsImgInDataBase).Key)), clsConfig._IsImgInDataBase.GetType())

            'Session("AllowToAddEditImageForESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditImageForESS).Key)), clsConfig._AllowToAddEditImageForESS.GetType())
            'Session("ShowFirstAppointmentDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowFirstAppointmentDate).Key)), clsConfig._ShowFirstAppointmentDate.GetType())
            'Session("BatchPostingNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BatchPostingNotype).Key)), clsConfig._BatchPostingNotype.GetType())
            'Session("NotifyPayroll_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Payroll_Users).Key)), clsConfig._Notify_Payroll_Users.GetType())
            'Session("EFTIntegration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTIntegration).Key)), clsConfig._EFTIntegration.GetType())
            'Session("DatafileName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DatafileName).Key)), clsConfig._DatafileName.GetType())
            'Session("AccountingSoftWare") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AccountingSoftWare).Key)), clsConfig._AccountingSoftWare.GetType())
            'Session("MobileMoneyEFTIntegration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MobileMoneyEFTIntegration).Key)), clsConfig._MobileMoneyEFTIntegration.GetType())
            'Session("_LoanVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanVocNoType).Key)), clsConfig._LoanVocNoType.GetType())
            'Session("_PaymentVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentVocNoType).Key)), clsConfig._PaymentVocNoType.GetType())
            'Session("_DoNotAllowOverDeductionForPayment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowOverDeductionForPayment).Key)), clsConfig._DoNotAllowOverDeductionForPayment.GetType())
            'Session("_IsDenominationCompulsory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDenominationCompulsory).Key)), clsConfig._IsDenominationCompulsory.GetType())
            'Session("_SetPayslipPaymentApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetPayslipPaymentApproval).Key)), clsConfig._SetPayslipPaymentApproval.GetType())
            'Session("SavingsVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SavingsVocNoType).Key)), clsConfig._SavingsVocNoType.GetType())
            'Session("AllowAssessor_Before_Emp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowAssessor_Before_Emp).Key)), clsConfig._AllowAssessor_Before_Emp.GetType())
            'Session("IsCompanyNeedReviewer") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsCompanyNeedReviewer).Key)), clsConfig._IsCompanyNeedReviewer.GetType())
            'Session("ConsiderItemWeightAsNumber") = False
            'Session("PolicyManagementTNA") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PolicyManagementTNA).Key)), clsConfig._PolicyManagementTNA.GetType())
            'Session("IsAutomaticIssueOnFinalApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAutomaticIssueOnFinalApproval).Key)), clsConfig._IsAutomaticIssueOnFinalApproval.GetType())
            'Session("StaffReqFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffReqFormNoType).Key)), clsConfig._StaffReqFormNoType.GetType())
            'Session("ApplyStaffRequisition") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplyStaffRequisition).Key)), clsConfig._ApplyStaffRequisition.GetType())
            'Session("ClosePayrollPeriodIfLeaveIssue") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClosePayrollPeriodIfLeaveIssue).Key)), clsConfig._ClosePayrollPeriodIfLeaveIssue.GetType())
            'Session("PaymentApprovalwithLeaveApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PaymentApprovalwithLeaveApproval).Key)), clsConfig._PaymentApprovalwithLeaveApproval.GetType())
            'Session("ClaimRequestVocNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRequestVocNoType).Key)), clsConfig._ClaimRequestVocNoType.GetType())
            'Session("SickSheetTemplate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetTemplate).Key)), clsConfig._SickSheetTemplate.GetType())
            'Session("SickSheetAllocationId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SickSheetAllocationId).Key)), clsConfig._SickSheetAllocationId.GetType())
            'Session("CustomPayrollReportAllocIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportAllocIds).Key)), clsConfig._CustomPayrollReportAllocIds.GetType())
            'Session("CustomPayrollReportHeadsIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportHeadsIds).Key)), clsConfig._CustomPayrollReportHeadsIds.GetType())
            'Session("CustomPayrollReportTemplate2AllocIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportTemplate2AllocIds).Key)), clsConfig._CustomPayrollReportTemplate2AllocIds.GetType())
            'Session("CustomPayrollReportTemplate2HeadsIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CustomPayrollReportTemplate2HeadsIds).Key)), clsConfig._CustomPayrollReportTemplate2HeadsIds.GetType())
            'Session("Notify_IssuedLeave_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_IssuedLeave_Users).Key)), clsConfig._Notify_IssuedLeave_Users.GetType())
            'Session("ViewTitles_InPlanning") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ViewTitles_InPlanning).Key)), clsConfig._ViewTitles_InPlanning.GetType())
            'Session("FollowEmployeeHierarchy") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FollowEmployeeHierarchy).Key)), clsConfig._FollowEmployeeHierarchy.GetType())
            'Session("CascadingTypeId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CascadingTypeId).Key)), clsConfig._CascadingTypeId.GetType())
            'Session("Assessment_Instructions") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Assessment_Instructions).Key)), clsConfig._Assessment_Instructions.GetType())
            'Session("OrgAssessment_Instructions") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Assessment_Instructions).Key)), clsConfig._Assessment_Instructions.GetType())
            'Session("ScoringOptionId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoringOptionId).Key)), clsConfig._ScoringOptionId.GetType())
            'Session("Perf_EvaluationOrder") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Perf_EvaluationOrder).Key)), clsConfig._Perf_EvaluationOrder.GetType())
            'Session("OrgPerf_EvaluationOrder") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Perf_EvaluationOrder).Key)), clsConfig._Perf_EvaluationOrder.GetType())
            'Session("ViewTitles_InEvaluation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ViewTitles_InEvaluation).Key)), clsConfig._ViewTitles_InEvaluation.GetType())
            'Session("Self_Assign_Competencies") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Self_Assign_Competencies).Key)), clsConfig._Self_Assign_Competencies.GetType())
            'Session("DetailedSalaryBreakdownReportHeadsIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DetailedSalaryBreakdownReportHeadsIds).Key)), clsConfig._DetailedSalaryBreakdownReportHeadsIds.GetType())
            'Session("Notify_Bank_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_Bank_Users).Key)), clsConfig._Notify_Bank_Users.GetType())
            'Session("AllowToChangeAttendanceAfterPayment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToChangeAttendanceAfterPayment).Key)), clsConfig._AllowToChangeAttendanceAfterPayment.GetType())
            'Session("OpenAfterExport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OpenAfterExport).Key)), clsConfig._OpenAfterExport.GetType())
            'Session("ExportReportPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ExportReportPath).Key)), clsConfig._ExportReportPath.GetType())
            'Session("SMimeRunPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMimeRunPath).Key)), clsConfig._SMimeRunPath.GetType())
            'Session("IsArutiDemo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsArutiDemo).Key)), clsConfig._IsArutiDemo.GetType())
            'Session("StaffReqFormNoPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffReqFormNoPrefix).Key)), clsConfig._StaffReqFormNoPrefix.GetType())
            'Session("StaffReqFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffReqFormNoType).Key)), clsConfig._StaffReqFormNoType.GetType())
            'Session("DonotAttendanceinSeconds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DonotAttendanceinSeconds).Key)), clsConfig._DonotAttendanceinSeconds.GetType())
            'Session("FirstCheckInLastCheckOut") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FirstCheckInLastCheckOut).Key)), clsConfig._FirstCheckInLastCheckOut.GetType())
            'Session("EFTCitiDirectCountryCode") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectCountryCode).Key)), clsConfig._EFTCitiDirectCountryCode.GetType())
            'Session("EFTCitiDirectSkipPriorityFlag") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectSkipPriorityFlag).Key)), clsConfig._EFTCitiDirectSkipPriorityFlag.GetType())
            'Session("EFTCitiDirectChargesIndicator") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectChargesIndicator).Key)), clsConfig._EFTCitiDirectChargesIndicator.GetType())
            'Session("EFTCitiDirectAddPaymentDetail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTCitiDirectAddPaymentDetail).Key)), clsConfig._EFTCitiDirectAddPaymentDetail.GetType())
            'Session("Accounting_Country") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Accounting_Country).Key)), clsConfig._Accounting_Country.GetType())
            'Session("RoundOff_Type") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RoundOff_Type).Key)), clsConfig._RoundOff_Type.GetType())
            'Session("SectorRouteAssignToEmp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SectorRouteAssignToEmp).Key)), clsConfig._SectorRouteAssignToEmp.GetType())
            'Session("LeaveLiabilityReportSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveLiabilityReportSetting).Key)), clsConfig._LeaveLiabilityReportSetting.GetType())
            'Session("ShowBankAccountNoOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBankAccountNoOnPayslip).Key)), clsConfig._ShowBankAccountNoOnPayslip.GetType())
            'Session("SelectedAllocationForDailyTimeSheetReport_Voltamp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp).Key)), clsConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp.GetType())
            'Session("ShowEmployeeStatusForDailyTimeSheetReport_Voltamp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp).Key)), clsConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp.GetType())
            'Session("LeaveAccrueDaysAfterEachMonth") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveAccrueDaysAfterEachMonth).Key)), clsConfig._LeaveAccrueDaysAfterEachMonth.GetType())
            'Session("LeaveAccrueTenureSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LeaveAccrueTenureSetting).Key)), clsConfig._LeaveAccrueTenureSetting.GetType())
            'Session("AllowToExceedTimeAssignedToActivity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._AllowToExceedTimeAssignedToActivity)).Key)), CBool(clsConfig._AllowToExceedTimeAssignedToActivity).GetType())
            'Session("AllowOverTimeToEmpTimesheet") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._AllowOverTimeToEmpTimesheet)).Key)), CBool(clsConfig._AllowOverTimeToEmpTimesheet).GetType())
            'Session("NotAllowIncompleteTimesheet") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._NotAllowIncompleteTimesheet)).Key)), CBool(clsConfig._NotAllowIncompleteTimesheet).GetType())
            'Session("DescriptionMandatoryForActivity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._DescriptionMandatoryForActivity)).Key)), CBool(clsConfig._DescriptionMandatoryForActivity).GetType())
            'Session("AllowActivityHoursByPercentage") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._AllowActivityHoursByPercentage)).Key)), CBool(clsConfig._AllowActivityHoursByPercentage).GetType())
            'Session("StatutoryMessageOnPayslipOnESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CStr(clsConfig._StatutoryMessageOnPayslipOnESS)).Key)), CStr(clsConfig._StatutoryMessageOnPayslipOnESS).GetType())
            'Session("SkipAbsentFromTnAMonthlyPayrollReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipAbsentFromTnAMonthlyPayrollReport).Key)), clsConfig._SkipAbsentFromTnAMonthlyPayrollReport.GetType())
            'Session("IncludePendingApproverFormsInMonthlyPayrollReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IncludePendingApproverFormsInMonthlyPayrollReport).Key)), clsConfig._IncludePendingApproverFormsInMonthlyPayrollReport.GetType())
            'Session("IncludeSystemRetirementMonthlyPayrollReport") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IncludeSystemRetirementMonthlyPayrollReport).Key)), clsConfig._IncludeSystemRetirementMonthlyPayrollReport.GetType())
            'Session("CheckedMonthlyPayrollReport1HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport1HeadIds).Key)), clsConfig._CheckedMonthlyPayrollReport1HeadIds.GetType())
            'Session("CheckedMonthlyPayrollReport2HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport2HeadIds).Key)), clsConfig._CheckedMonthlyPayrollReport2HeadIds.GetType())
            'Session("CheckedMonthlyPayrollReport3HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport3HeadIds).Key)), clsConfig._CheckedMonthlyPayrollReport3HeadIds.GetType())
            'Session("CheckedMonthlyPayrollReport4HeadIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReport4HeadIds).Key)), clsConfig._CheckedMonthlyPayrollReport4HeadIds.GetType())
            'Session("CheckedMonthlyPayrollReportLeaveIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CheckedMonthlyPayrollReportLeaveIds).Key)), clsConfig._CheckedMonthlyPayrollReportLeaveIds.GetType())
            'Session("Ntf_TrainingLevelI_EvalUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_TrainingLevelI_EvalUserIds).Key)), clsConfig._Ntf_TrainingLevelI_EvalUserIds.GetType())
            'Session("Ntf_TrainingLevelIII_EvalUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_TrainingLevelIII_EvalUserIds).Key)), clsConfig._Ntf_TrainingLevelIII_EvalUserIds.GetType())
            'Session("NotAllowIncompleteTimesheet") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CBool(clsConfig._NotAllowIncompleteTimesheet)).Key)), CBool(clsConfig._NotAllowIncompleteTimesheet).GetType())
            'Session("IsHolidayConsiderOnWeekend") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsHolidayConsiderOnWeekend).Key)), clsConfig._IsHolidayConsiderOnWeekend.GetType())
            'Session("IsDayOffConsiderOnWeekend") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsDayOffConsiderOnWeekend).Key)), clsConfig._IsDayOffConsiderOnWeekend.GetType())
            'Session("IsHolidayConsiderOnDayoff") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsHolidayConsiderOnDayoff).Key)), clsConfig._IsHolidayConsiderOnDayoff.GetType())
            'Session("AllowtoViewSignatureESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowtoViewSignatureESS).Key)), clsConfig._AllowtoViewSignatureESS.GetType())
            'Session("DontAllowAdvanceOnESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowAdvanceOnESS).Key)), clsConfig._DontAllowAdvanceOnESS.GetType())
            'Session("WebURLInternal") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._WebURLInternal).Key)), clsConfig._WebURLInternal.GetType())
            'Session("ShowBgTimesheetActivity") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetActivity).Key)), clsConfig._ShowBgTimesheetActivity.GetType())
            'Session("ShowBgTimesheetAssignedActivityPercentage") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetAssignedActivityPercentage).Key)), clsConfig._ShowBgTimesheetAssignedActivityPercentage.GetType())
            'Session("ShowBgTimesheetActivityProject") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetActivityProject).Key)), clsConfig._ShowBgTimesheetActivityProject.GetType())
            'Session("ShowBgTimesheetActivityHrsDetail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowBgTimesheetActivityHrsDetail).Key)), clsConfig._ShowBgTimesheetActivityHrsDetail.GetType())
            'Session("RemarkMandatoryForTimesheetSubmission") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RemarkMandatoryForTimesheetSubmission).Key)), clsConfig._RemarkMandatoryForTimesheetSubmission.GetType())
            'Session("GrievanceApprovalSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceApprovalSetting).Key)), clsConfig._GrievanceApprovalSetting.GetType())
            'Session("GrievanceRefNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceRefNoType).Key)), clsConfig._GrievanceRefNoType.GetType())
            'Session("GrievanceRefPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceRefPrefix).Key)), clsConfig._GrievanceRefPrefix.GetType())
            'Session("DependantDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DependantDocsAttachmentMandatory).Key)), clsConfig._DependantDocsAttachmentMandatory.GetType())
            'Session("QualificationCertificateAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._QualificationCertificateAttachmentMandatory).Key)), clsConfig._QualificationCertificateAttachmentMandatory.GetType())
            'Session("SkipApprovalFlowInPlanning") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalFlowInPlanning).Key)), clsConfig._SkipApprovalFlowInPlanning.GetType())
            'Session("OnlyOneItemPerCompetencyCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OnlyOneItemPerCompetencyCategory).Key)), clsConfig._OnlyOneItemPerCompetencyCategory.GetType())
            'Session("EnableBSCAutomaticRating") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EnableBSCAutomaticRating).Key)), clsConfig._EnableBSCAutomaticRating.GetType())
            'Session("DontAllowToEditScoreGenbySys") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowToEditScoreGenbySys).Key)), clsConfig._DontAllowToEditScoreGenbySys.GetType())
            'Session("GoalsAccomplishedRequiresApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GoalsAccomplishedRequiresApproval).Key)), clsConfig._GoalsAccomplishedRequiresApproval.GetType())
            'Session("AssessmentReportTemplateId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssessmentReportTemplateId).Key)), clsConfig._AssessmentReportTemplateId.GetType())
            'Session("IsUseAgreedScore") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsUseAgreedScore).Key)), clsConfig._IsUseAgreedScore.GetType())
            'Session("ReviewerScoreSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ReviewerScoreSetting).Key)), clsConfig._ReviewerScoreSetting.GetType())
            'Session("IsAllowCustomItemFinalSave") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsAllowCustomItemFinalSave).Key)), clsConfig._IsAllowCustomItemFinalSave.GetType())
            'Session("ShowMyGoals") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowMyGoals).Key)), clsConfig._ShowMyGoals.GetType())
            'Session("ShowCustomHeaders") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowCustomHeaders).Key)), clsConfig._ShowCustomHeaders.GetType())
            'Session("ShowGoalsApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowGoalsApproval).Key)), clsConfig._ShowGoalsApproval.GetType())
            'Session("ShowSelfAssessment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowSelfAssessment).Key)), clsConfig._ShowSelfAssessment.GetType())
            'Session("ShowMyComptencies") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowMyComptencies).Key)), clsConfig._ShowMyComptencies.GetType())
            'Session("ShowViewPerformanceAssessment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowViewPerformanceAssessment).Key)), clsConfig._ShowViewPerformanceAssessment.GetType())
            'Session("Ntf_FinalAcknowledgementUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_FinalAcknowledgementUserIds).Key)), clsConfig._Ntf_FinalAcknowledgementUserIds.GetType())
            'Session("Ntf_GoalsUnlockUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Ntf_GoalsUnlockUserIds).Key)), clsConfig._Ntf_GoalsUnlockUserIds.GetType())
            'Session("CreateADUserFromEmpMst") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CreateADUserFromEmpMst).Key)), clsConfig._CreateADUserFromEmpMst.GetType())
            'Session("UserMustchangePwdOnNextLogon") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._UserMustchangePwdOnNextLogon).Key)), clsConfig._UserMustchangePwdOnNextLogon.GetType())
            'Session("SetRelieverAsMandatoryForApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetRelieverAsMandatoryForApproval).Key)), clsConfig._SetRelieverAsMandatoryForApproval.GetType())
            'Session("AllowToSetRelieverOnLvFormForESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToSetRelieverOnLvFormForESS).Key)), clsConfig._AllowToSetRelieverOnLvFormForESS.GetType())
            'Session("RelieverAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RelieverAllocation).Key)), clsConfig._RelieverAllocation.GetType())
            'Session("AllowLvApplicationApplyForBackDates") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowLvApplicationApplyForBackDates).Key)), clsConfig._AllowLvApplicationApplyForBackDates.GetType())
            'Session("AssetDeclarationTemplate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationTemplate).Key)), clsConfig._AssetDeclarationTemplate.GetType())
            'Session("StaffRequisitionFinalApprovedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffRequisitionFinalApprovedNotificationUserIds).Key)), clsConfig._StaffRequisitionFinalApprovedNotificationUserIds.GetType())
            'Session("EnableTraningRequisition") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EnableTraningRequisition).Key)), clsConfig._EnableTraningRequisition.GetType())
            'Session("DonotAllowToRequestTrainingAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DonotAllowToRequestTrainingAfterDays).Key)), clsConfig._DonotAllowToRequestTrainingAfterDays.GetType())
            'Session("SetOTRequisitionMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SetOTRequisitionMandatory).Key)), clsConfig._SetOTRequisitionMandatory.GetType())
            'Session("CapOTHrsForHODApprovers") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CapOTHrsForHODApprovers).Key)), clsConfig._CapOTHrsForHODApprovers.GetType())
            'Session("SkipEmployeeMovementApprovalFlow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEmployeeMovementApprovalFlow).Key)), clsConfig._SkipEmployeeMovementApprovalFlow.GetType())
            'Session("RejectTransferUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectTransferUserNotification).Key)), clsConfig._RejectTransferUserNotification.GetType())
            'Session("RejectReCategorizationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectReCategorizationUserNotification).Key)), clsConfig._RejectReCategorizationUserNotification.GetType())
            'Session("RejectProbationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectProbationUserNotification).Key)), clsConfig._RejectProbationUserNotification.GetType())
            'Session("RejectConfirmationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectConfirmationUserNotification).Key)), clsConfig._RejectConfirmationUserNotification.GetType())
            'Session("RejectSuspensionUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectSuspensionUserNotification).Key)), clsConfig._RejectSuspensionUserNotification.GetType())
            'Session("RejectTerminationUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectTerminationUserNotification).Key)), clsConfig._RejectTerminationUserNotification.GetType())
            'Session("RejectRetirementUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectRetirementUserNotification).Key)), clsConfig._RejectRetirementUserNotification.GetType())
            'Session("RejectWorkPermitUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectWorkPermitUserNotification).Key)), clsConfig._RejectWorkPermitUserNotification.GetType())
            'Session("RejectResidentPermitUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectResidentPermitUserNotification).Key)), clsConfig._RejectResidentPermitUserNotification.GetType())
            'Session("RejectCostCenterPermitUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectCostCenterPermitUserNotification).Key)), clsConfig._RejectCostCenterPermitUserNotification.GetType())
            'Session("RejectExemptionUserNotification") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RejectExemptionUserNotification).Key)), clsConfig._RejectExemptionUserNotification.GetType())
            'Session("AssetDeclarationFinalSavedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationFinalSavedNotificationUserIds).Key)), clsConfig._AssetDeclarationFinalSavedNotificationUserIds.GetType())
            'Session("SkipEmployeeApprovalFlow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEmployeeApprovalFlow).Key)), clsConfig._SkipEmployeeApprovalFlow.GetType())
            'Session("AssetDeclarationUnlockFinalSavedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationUnlockFinalSavedNotificationUserIds).Key)), clsConfig._AssetDeclarationUnlockFinalSavedNotificationUserIds.GetType())
            'Session("NonDisclosureDeclarationAcknowledgedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NonDisclosureDeclarationAcknowledgedNotificationUserIds).Key)), clsConfig._NonDisclosureDeclarationAcknowledgedNotificationUserIds.GetType())
            'Session("FlexCubeJVPostedOracleNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexCubeJVPostedOracleNotificationUserIds).Key)), clsConfig._FlexCubeJVPostedOracleNotificationUserIds.GetType())
            'Session("OracleHostName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleHostName).Key)), clsConfig._OracleHostName.GetType())
            'Session("OraclePortNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OraclePortNo).Key)), clsConfig._OraclePortNo.GetType())
            'Session("OracleServiceName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleServiceName).Key)), clsConfig._OracleServiceName.GetType())
            'Session("OracleUserName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleUserName).Key)), clsConfig._OracleUserName.GetType())
            'Session("OracleUserPassword") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OracleUserPassword).Key)), clsConfig._OracleUserPassword.GetType())
            'Session("SQLDataSource") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLDataSource).Key)), clsConfig._SQLDataSource.GetType())
            'Session("SQLDatabaseName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLDatabaseName).Key)), clsConfig._SQLDatabaseName.GetType())
            'Session("SQLDatabaseOwnerName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLDatabaseOwnerName).Key)), clsConfig._SQLDatabaseOwnerName.GetType())
            'Session("SQLUserName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLUserName).Key)), clsConfig._SQLUserName.GetType())
            'Session("SQLUserPassword") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SQLUserPassword).Key)), clsConfig._SQLUserPassword.GetType())
            'Session("AssetDeclarationFromDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationFromDate.ToString()).Key)), clsConfig._AssetDeclarationFromDate.ToString().GetType())
            'Session("AssetDeclarationToDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationToDate.ToString()).Key)), clsConfig._AssetDeclarationToDate.ToString().GetType())
            'Session("DontAllowAssetDeclarationAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._DontAllowAssetDeclarationAfterDays)).Key)), CInt(clsConfig._DontAllowAssetDeclarationAfterDays).GetType())
            'Session("NewEmpUnLockDaysforAssetDecWithinAppointmentDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() CInt(clsConfig._NewEmpUnLockDaysforAssetDecWithinAppointmentDate)).Key)), CInt(clsConfig._NewEmpUnLockDaysforAssetDecWithinAppointmentDate).GetType())
            'Session("AssetDeclarationFromDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AssetDeclarationFromDate).Key)), clsConfig._AssetDeclarationFromDate.GetType())
            'Session("ImportCostCenterP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ImportCostCenterP2PServiceURL).Key)), clsConfig._ImportCostCenterP2PServiceURL.GetType())
            'Session("OpexRequestCCP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OpexRequestCCP2PServiceURL).Key)), clsConfig._OpexRequestCCP2PServiceURL.GetType())
            'Session("CapexRequestCCP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CapexRequestCCP2PServiceURL).Key)), clsConfig._CapexRequestCCP2PServiceURL.GetType())
            'Session("BgtRequestValidationP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BgtRequestValidationP2PServiceURL).Key)), clsConfig._BgtRequestValidationP2PServiceURL.GetType())
            'Session("NewRequisitionRequestP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NewRequisitionRequestP2PServiceURL).Key)), clsConfig._NewRequisitionRequestP2PServiceURL.GetType())
            'Session("SkipEmployeeApprovalFlow") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEmployeeApprovalFlow).Key)), clsConfig._SkipEmployeeApprovalFlow.GetType())
            'Session("SendDetailToEmployee") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SendDetailToEmployee).Key)), clsConfig._SendDetailToEmployee.GetType())
            'Session("SMSCompanyDetailToNewEmp") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMSCompanyDetailToNewEmp).Key)), clsConfig._SMSCompanyDetailToNewEmp.GetType())
            'Session("IsHRFlexcubeIntegrated") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsHRFlexcubeIntegrated).Key)), clsConfig._IsHRFlexcubeIntegrated.GetType())
            'Session("FlexcubeServiceCollection") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeServiceCollection).Key)), clsConfig._FlexcubeServiceCollection.GetType())
            'Session("FlexcubeAccountCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeAccountCategory).Key)), clsConfig._FlexcubeAccountCategory.GetType())
            'Session("FlexcubeAccountClass") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._FlexcubeAccountClass).Key)), clsConfig._FlexcubeAccountClass.GetType())
            'Session("SMSGatewayEmail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMSGatewayEmail).Key)), clsConfig._SMSGatewayEmail.GetType())
            'Session("SMSGatewayEmailType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SMSGatewayEmailType).Key)), clsConfig._SMSGatewayEmailType.GetType())
            'Session("EmployeeRejectNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeRejectNotificationUserIds).Key)), clsConfig._EmployeeRejectNotificationUserIds.GetType())
            'Session("EmployeeRejectNotificationTemplateId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeRejectNotificationTemplateId).Key)), clsConfig._EmployeeRejectNotificationTemplateId.GetType())
            'Session("EmpMandatoryFieldsIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpMandatoryFieldsIDs).Key)), clsConfig._EmpMandatoryFieldsIDs.GetType())
            'Session("PendingEmployeeScreenIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PendingEmployeeScreenIDs).Key)), clsConfig._PendingEmployeeScreenIDs.GetType())
            'Session("GoalTypeInclusion") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GoalTypeInclusion).Key)), clsConfig._GoalTypeInclusion.GetType())
            'Session("SkipApprovalOnEmpData") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalOnEmpData).Key)), clsConfig._SkipApprovalOnEmpData.GetType())
            'Session("PendingEmployeeScreenIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PendingEmployeeScreenIDs).Key)), clsConfig._PendingEmployeeScreenIDs.GetType())
            'Session("EmployeeDataRejectedUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDataRejectedUserIds).Key)), clsConfig._EmployeeDataRejectedUserIds.GetType())
            'Session("SectorRouteAssignToExpense") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SectorRouteAssignToExpense).Key)), clsConfig._SectorRouteAssignToExpense.GetType())
            'Session("MakeEmpAssessCommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeEmpAssessCommentsMandatory).Key)), clsConfig._MakeEmpAssessCommentsMandatory.GetType())
            'Session("MakeAsrAssessCommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAsrAssessCommentsMandatory).Key)), clsConfig._MakeAsrAssessCommentsMandatory.GetType())
            'Session("MakeRevAssessCommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeRevAssessCommentsMandatory).Key)), clsConfig._MakeRevAssessCommentsMandatory.GetType())
            'Session("AdministratorEmailForError") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AdministratorEmailForError).Key)), clsConfig._AdministratorEmailForError.GetType())
            'Session("AllowViewEmployeeScale") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowViewEmployeeScale).Key)), clsConfig._AllowViewEmployeeScale.GetType())
            'Session("AllowToAddEditEmployeePresentAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditEmployeePresentAddress).Key)), clsConfig._AllowToAddEditEmployeePresentAddress.GetType())
            'Session("AllowToAddEditEmployeeDomicileAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditEmployeeDomicileAddress).Key)), clsConfig._AllowToAddEditEmployeeDomicileAddress.GetType())
            'Session("AllowToAddEditEmployeeRecruitmentAddress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToAddEditEmployeeRecruitmentAddress).Key)), clsConfig._AllowToAddEditEmployeeRecruitmentAddress.GetType())
            'Session("AllowToEditLeaveApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToEditLeaveApplication).Key)), clsConfig._AllowToEditLeaveApplication.GetType())
            'Session("AllowToDeleteLeaveApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToDeleteLeaveApplication).Key)), clsConfig._AllowToDeleteLeaveApplication.GetType())
            'Session("ClaimRemarkMandatoryForClaim") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRemarkMandatoryForClaim).Key)), clsConfig._ClaimRemarkMandatoryForClaim.GetType())
            'Session("DomicileDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DomicileDocsAttachmentMandatory).Key)), clsConfig._DomicileDocsAttachmentMandatory.GetType())
            'Session("ScoreCalibrationFormNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoreCalibrationFormNotype).Key)), clsConfig._ScoreCalibrationFormNotype.GetType())
            'Session("ScoreCalibrationFormNoPrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoreCalibrationFormNoPrifix).Key)), clsConfig._ScoreCalibrationFormNoPrifix.GetType())
            'Session("NextScoreCalibrationFormNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NextScoreCalibrationFormNo).Key)), clsConfig._NextScoreCalibrationFormNo.GetType())
            'Session("IsCalibrationSettingActive") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsCalibrationSettingActive).Key)), clsConfig._IsCalibrationSettingActive.GetType())
            'Session("EmployeeDisagreeGrievanceNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDisagreeGrievanceNotificationUserIds).Key)), clsConfig._EmployeeDisagreeGrievanceNotificationUserIds.GetType())
            'Session("EmployeeDisagreeGrievanceNotificationEmailTitle") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDisagreeGrievanceNotificationEmailTitle).Key)), clsConfig._EmployeeDisagreeGrievanceNotificationEmailTitle.GetType())
            'Session("EmployeeDisagreeGrievanceNotificationTemplateId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmployeeDisagreeGrievanceNotificationTemplateId).Key)), clsConfig._EmployeeDisagreeGrievanceNotificationTemplateId.GetType())
            'Session("EmpTimesheetSetting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpTimesheetSetting).Key)), clsConfig._EmpTimesheetSetting.GetType())
            'Session("AllowEmpAssignedProjectExceedTime") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEmpAssignedProjectExceedTime).Key)), clsConfig._AllowEmpAssignedProjectExceedTime.GetType())
            'Session("Notify_LoanAdvance_Users") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._Notify_LoanAdvance_Users).Key)), clsConfig._Notify_LoanAdvance_Users.GetType())
            'Session("GetTokenP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GetTokenP2PServiceURL).Key)), clsConfig._GetTokenP2PServiceURL.GetType())
            'Session("EnforceManpowerPlanning") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EnforceManpowerPlanning).Key)), clsConfig._EnforceManpowerPlanning.GetType())
            'Session("ClaimRetirementFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRetirementFormNoType).Key)), clsConfig._ClaimRetirementFormNoType.GetType())
            'Session("ShowInterdictionDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowInterdictionDate).Key)), clsConfig._ShowInterdictionDate.GetType())
            'Session("NotificationOnDisciplineFiling") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._NotificationOnDisciplineFiling).Key)), clsConfig._NotificationOnDisciplineFiling.GetType())
            'Session("ShowReponseTypeOnPosting") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowReponseTypeOnPosting).Key)), clsConfig._ShowReponseTypeOnPosting.GetType())
            'Session("PresentAddressDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PresentAddressDocsAttachmentMandatory).Key)), clsConfig._PresentAddressDocsAttachmentMandatory.GetType())
            'Session("RecruitmentAddressDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RecruitmentAddressDocsAttachmentMandatory).Key)), clsConfig._RecruitmentAddressDocsAttachmentMandatory.GetType())
            'Session("IdentityDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IdentityDocsAttachmentMandatory).Key)), clsConfig._IdentityDocsAttachmentMandatory.GetType())
            'Session("JobExperienceDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._JobExperienceDocsAttachmentMandatory).Key)), clsConfig._JobExperienceDocsAttachmentMandatory.GetType())
            'Session("RecruitMandatoryFieldsIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RecruitMandatoryFieldsIDs).Key)), clsConfig._RecruitMandatoryFieldsIDs.GetType())
            'Dim arrSettings() As String = CStr(Session("RecruitMandatoryFieldsIDs")).Split(CChar(","))
            'If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CurriculumVitae).ToString) = True Then
            '    Session("OneCurriculamVitaeMandatory") = True
            'Else
            '    Session("OneCurriculamVitaeMandatory") = False
            'End If
            'If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CoverLetter).ToString) = True Then
            '    Session("OneCoverLetterMandatory") = True
            'Else
            '    Session("OneCoverLetterMandatory") = False
            'End If
            'If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentQualification).ToString) = True Then
            '    Session("AttachmentQualificationMandatory") = True
            'Else
            '    Session("AttachmentQualificationMandatory") = False
            'End If
            ''Sohail (11 Apr 2022) -- End

            ''Hemant (18 May 2022) -- Start
            ''ISSUE/ENHANCEMENT(ZRA) : AC2-384,AC2-386  Apply experience required validation if year of experienced is required on Apply now button on search job screen in self service.
            'If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentTypeImage).ToString) = True Then
            '    Session("AllowableAttachmentTypeImage") = True
            'Else
            '    Session("AllowableAttachmentTypeImage") = False
            'End If
            'If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentTypeDocument).ToString) = True Then
            '    Session("AllowableAttachmentTypeDocument") = True
            'Else
            '    Session("AllowableAttachmentTypeDocument") = False
            'End If

            'Session("RecruitMandatoryJobExperiences") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RecruitMandatoryJobExperiences).Key)), clsConfig._RecruitMandatoryJobExperiences.GetType())
            'Session("ClaimRetirementTypeId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ClaimRetirementTypeId).Key)), clsConfig._ClaimRetirementTypeId.GetType())
            'Session("OTTenureDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._OTTenureDays).Key)), clsConfig._OTTenureDays.GetType())
            'Session("AdditionalStaffRequisitionDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AdditionalStaffRequisitionDocsAttachmentMandatory).Key)), clsConfig._AdditionalStaffRequisitionDocsAttachmentMandatory.GetType())
            'Session("AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory).Key)), clsConfig._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory.GetType())
            'Session("ReplacementStaffRequisitionDocsAttachmentMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ReplacementStaffRequisitionDocsAttachmentMandatory).Key)), clsConfig._ReplacementStaffRequisitionDocsAttachmentMandatory.GetType())
            'Session("GrievanceReportingToMaxApprovalLevel") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._GrievanceReportingToMaxApprovalLevel).Key)), clsConfig._GrievanceReportingToMaxApprovalLevel.GetType())
            'Session("AllowToviewEmpTimesheetReportForESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToviewEmpTimesheetReportForESS).Key)), clsConfig._AllowToviewEmpTimesheetReportForESS.GetType())
            'Session("TrainingNeedFormNoType") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingNeedFormNoType).Key)), clsConfig._TrainingNeedFormNoType.GetType())
            'Session("TrainingNeedFormNoPrefix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingNeedFormNoPrefix).Key)), clsConfig._TrainingNeedFormNoPrefix.GetType())
            'Session("ApplicableLeaveStatus") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicableLeaveStatus).Key)), clsConfig._ApplicableLeaveStatus.GetType())
            'Session("AllowToCancelLeaveButRetainExpense") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToCancelLeaveButRetainExpense).Key)), clsConfig._AllowToCancelLeaveButRetainExpense.GetType())
            'Session("AllowToViewScoreWhileDoingAssesment") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToViewScoreWhileDoingAssesment).Key)), clsConfig._AllowToViewScoreWhileDoingAssesment.GetType())
            'Session("AllowToCancelLeaveForClosedPeriod") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowToCancelLeaveForClosedPeriod).Key)), clsConfig._AllowToCancelLeaveForClosedPeriod.GetType())
            'Session("AllowEmployeeToAddEditSignatureESS") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowEmployeeToAddEditSignatureESS).Key)), clsConfig._AllowEmployeeToAddEditSignatureESS.GetType())
            'Session("AllowOnlyOneExpenseInClaimApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowOnlyOneExpenseInClaimApplication).Key)), clsConfig._AllowOnlyOneExpenseInClaimApplication.GetType())
            'Session("AllowTerminationIfPaymentDone") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowTerminationIfPaymentDone).Key)), clsConfig._AllowTerminationIfPaymentDone.GetType())
            'Session("applicant_declaration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicantDeclaration).Key)), clsConfig._ApplicantDeclaration.GetType())
            'Session("VacancyFoundOutFromMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._VacancyFoundOutFromMandatoryInRecruitment).Key)), clsConfig._VacancyFoundOutFromMandatoryInRecruitment.GetType())
            'Session("EarliestPossibleStartDateMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EarliestPossibleStartDateMandatoryInRecruitment).Key)), clsConfig._EarliestPossibleStartDateMandatoryInRecruitment.GetType())
            'Session("CommentsMandatory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CommentsMandatoryInRecruitment).Key)), clsConfig._CommentsMandatoryInRecruitment.GetType())
            'Session("isvacancyalert") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._isVacancyAlert).Key)), clsConfig._isVacancyAlert.GetType())
            'Session("issendjobconfirmationemail") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SendJobConfirmationEmail).Key)), clsConfig._SendJobConfirmationEmail.GetType())
            'Session("isattachapplicantcv") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AttachApplicantCV).Key)), clsConfig._AttachApplicantCV.GetType())
            'Session("applicantunkid") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() 0).Key)), 0.GetType())
            'Session("LocalizationCountryUnkid") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CountryUnkid).Key)), clsConfig._CountryUnkid.GetType())
            'Session("ApplicantCodeNotype") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicantCodeNotype).Key)), clsConfig._ApplicantCodeNotype.GetType())
            'Session("ApplicantCodePrifix") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplicantCodePrifix).Key)), clsConfig._ApplicantCodePrifix.GetType())
            'Session("IgnoreNegativeNetPayEmployeesOnJV") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IgnoreNegativeNetPayEmployeesOnJV).Key)), clsConfig._IgnoreNegativeNetPayEmployeesOnJV.GetType())
            'Session("ShowRemainingNoOfLoanInstallmentsOnPayslip") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowRemainingNoOfLoanInstallmentsOnPayslip).Key)), clsConfig._ShowRemainingNoOfLoanInstallmentsOnPayslip.GetType())
            'Session("SkipApprovalFlowForApplicantShortListing") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalFlowForApplicantShortListing).Key)), clsConfig._SkipApprovalFlowForApplicantShortListing.GetType())
            'Session("SkipApprovalFlowForFinalApplicant") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipApprovalFlowForFinalApplicant).Key)), clsConfig._SkipApprovalFlowForFinalApplicant.GetType())
            'Session("RunCompetenceAssessmentSeparately") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RunCompetenceAssessmentSeparately).Key)), clsConfig._RunCompetenceAssessmentSeparately.GetType())
            'Session("AllocationWorkStation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllocationWorkStation).Key)), clsConfig._AllocationWorkStation.GetType())
            'Session("CmptScoringOptionId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CmptScoringOptionId).Key)), clsConfig._CmptScoringOptionId.GetType())
            'Session("OrgScoringOptionId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ScoringOptionId).Key)), clsConfig._ScoringOptionId.GetType())
            'Session("EmpBirthdayAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpBirthdayAllocation).Key)), clsConfig._EmpBirthdayAllocation.GetType())
            'Session("EmpStaffTurnOverAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpStaffTurnOverAllocation).Key)), clsConfig._EmpStaffTurnOverAllocation.GetType())
            'Session("EmpOnLeaveAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpOnLeaveAllocation).Key)), clsConfig._EmpOnLeaveAllocation.GetType())
            'Session("EmpTeamMembersAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpTeamMembersAllocation).Key)), clsConfig._EmpTeamMembersAllocation.GetType())
            'Session("EmpWorkAnniversaryAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpWorkAnniversaryAllocation).Key)), clsConfig._EmpWorkAnniversaryAllocation.GetType())
            'Session("EmpTnADetailsAllocation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpTnADetailsAllocation).Key)), clsConfig._EmpTnADetailsAllocation.GetType())
            'Session("CompetenciesAssess_Instructions") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompetenciesAssess_Instructions).Key)), clsConfig._CompetenciesAssess_Instructions.GetType())
            'Session("DeptTrainingNeedListColumnsIDs") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DeptTrainingNeedListColumnsIDs).Key)), clsConfig._DeptTrainingNeedListColumnsIDs.GetType())
            'Session("TrainingNeedAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingNeedAllocationID).Key)), clsConfig._TrainingNeedAllocationID.GetType())
            'Session("TrainingBudgetAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingBudgetAllocationID).Key)), clsConfig._TrainingBudgetAllocationID.GetType())
            'Session("TrainingCostCenterAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingCostCenterAllocationID).Key)), clsConfig._TrainingCostCenterAllocationID.GetType())
            'Session("TrainingRemainingBalanceBasedOnID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingRemainingBalanceBasedOnID).Key)), clsConfig._TrainingRemainingBalanceBasedOnID.GetType())
            'Session("ApplyNonDisclosureDeclaration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplyNonDisclosureDeclaration).Key)), clsConfig._ApplyNonDisclosureDeclaration.GetType())
            'Session("IsPDP_Perf_Integrated") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._IsPDP_Perf_Integrated).Key)), clsConfig._IsPDP_Perf_Integrated.GetType())
            'Session("PreTrainingEvaluationSubmitted") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PreTrainingEvaluationSubmitted).Key)), clsConfig._PreTrainingEvaluationSubmitted.GetType())
            'Session("PreTrainingFeedbackInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PreTrainingFeedbackInstruction).Key)), clsConfig._PreTrainingFeedbackInstruction.GetType())
            'Session("PostTrainingFeedbackInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PostTrainingFeedbackInstruction).Key)), clsConfig._PostTrainingFeedbackInstruction.GetType())
            'Session("DaysAfterTrainingFeedbackInstruction") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterTrainingFeedbackInstruction).Key)), clsConfig._DaysAfterTrainingFeedbackInstruction.GetType())
            'Session("MaxEmpPlannedLeave") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MaxEmpPlannedLeave).Key)), clsConfig._MaxEmpPlannedLeave.GetType())
            'Session("SkipTrainingRequisitionAndApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingRequisitionAndApproval).Key)), clsConfig._SkipTrainingRequisitionAndApproval.GetType())
            'Session("SkipEOCValidationOnLoanTenure") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipEOCValidationOnLoanTenure).Key)), clsConfig._SkipEOCValidationOnLoanTenure.GetType())
            'Session("TrainingCompetenciesPeriodId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingCompetenciesPeriodId).Key)), clsConfig._TrainingCompetenciesPeriodId.GetType())
            'Session("DaysFromLastDateOfTrainingToAllowCompleteTraining") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysFromLastDateOfTrainingToAllowCompleteTraining).Key)), clsConfig._DaysFromLastDateOfTrainingToAllowCompleteTraining.GetType())
            'Session("PostTrainingEvaluationBeforeCompleteTraining") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PostTrainingEvaluationBeforeCompleteTraining).Key)), clsConfig._PostTrainingEvaluationBeforeCompleteTraining.GetType())
            'Session("TrainingApproverAllocationID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingApproverAllocationID).Key)), clsConfig._TrainingApproverAllocationID.GetType())
            'Session("UnRetiredImprestToPayrollAfterDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._UnRetiredImprestToPayrollAfterDays).Key)), clsConfig._UnRetiredImprestToPayrollAfterDays.GetType())
            'Session("DoNotAllowToApplyImprestIfUnRetiredForExpCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowToApplyImprestIfUnRetiredForExpCategory).Key)), clsConfig._DoNotAllowToApplyImprestIfUnRetiredForExpCategory.GetType())
            'Session("TrainingRequestApprovalSettingID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingRequestApprovalSettingID).Key)), clsConfig._TrainingRequestApprovalSettingID.GetType())
            'If Session("TrainingRequestApprovalSettingID") <= 0 Then
            '    Session("TrainingRequestApprovalSettingID") = enTrainingRequestApproval.RolebaseAccess
            'End If

            'Session("TrainingFinalApprovedNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TrainingFinalApprovedNotificationUserIds).Key)), clsConfig._TrainingFinalApprovedNotificationUserIds.GetType())
            'Session("LoanIntegration") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanIntegration).Key)), clsConfig._LoanIntegration.GetType())
            'Session("AddProceedingAgainstEachCount") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AddProceedingAgainstEachCount).Key)), clsConfig._AddProceedingAgainstEachCount.GetType())
            'Session("RoleBasedLoanApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._RoleBasedLoanApproval).Key)), clsConfig._RoleBasedLoanApproval.GetType())
            'Session("LoanCentralizedApproverId") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanCentralizedApproverId).Key)), clsConfig._LoanCentralizedApproverId.GetType())
            'Session("ShowWeightOnAssessScreenRatingScale") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ShowWeightOnAssessScreenRatingScale).Key)), clsConfig._ShowWeightOnAssessScreenRatingScale.GetType())
            'Session("EFTRequestURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTRequestURL).Key)), clsConfig._EFTRequestURL.GetType())
            'Session("EFTReconciliationURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTReconciliationURL).Key)), clsConfig._EFTReconciliationURL.GetType())
            'Session("EFTRequestClientID") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTRequestClientID).Key)), clsConfig._EFTRequestClientID.GetType())
            'Session("EFTRequestPassword") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTRequestPassword).Key)), clsConfig._EFTRequestPassword.GetType())
            'Session("SkipTrainingEnrollmentProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingEnrollmentProcess).Key)), clsConfig._SkipTrainingEnrollmentProcess.GetType())
            'Session("SkipTrainingCompletionProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingCompletionProcess).Key)), clsConfig._SkipTrainingCompletionProcess.GetType())
            'Session("DaysForEmployeeEvaluationReminderEmailAfterTrainingStartDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterTrainingStartDateEmployeeEvaluation).Key)), clsConfig._DaysAfterTrainingStartDateEmployeeEvaluation.GetType())
            'Session("DaysForEmployeeEvaluationReminderEmailAfterTrainingEndDate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterTrainingEndDateEmployeeEvaluation).Key)), clsConfig._DaysAfterTrainingEndDateEmployeeEvaluation.GetType())
            'Session("DaysForManagerEvaluationReminderEmailAfterSelfEvaluation") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DaysAfterSelfEvaluationManagerEvaluation).Key)), clsConfig._DaysAfterSelfEvaluationManagerEvaluation.GetType())
            'Session("SkipPreTrainingEvaluationProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipPreTrainingEvaluationProcess).Key)), clsConfig._SkipPreTrainingEvaluationProcess.GetType())
            'Session("SkipPostTrainingEvaluationProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipPostTrainingEvaluationProcess).Key)), clsConfig._SkipPostTrainingEvaluationProcess.GetType())
            'Session("SkipDaysAfterTrainingEvaluationProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipDaysAfterTrainingEvaluationProcess).Key)), clsConfig._SkipDaysAfterTrainingEvaluationProcess.GetType())
            'Session("SkipTrainingBudgetApprovalProcess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SkipTrainingBudgetApprovalProcess).Key)), clsConfig._SkipTrainingBudgetApprovalProcess.GetType())

            '================================ END TO SET CONFIGURATION RELATED SESSIONS=====================================


            '================================ START TO SET COMPANY RELATED SESSIONS=====================================
            'Pinkal (11-Dec-2018) -- Start
            'Issue - Login page is taking time to login in NMB to have set on begining of this function.
            'Dim objCompany As New clsCompany_Master
            'objCompany._Companyunkid = intCompanyID


            ' Dim objCompany As New clsCompany_Master
            'objCompany._Companyunkid = intCompanyID

            'Pinkal (11-Dec-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            Session("CompanyCode") = objCompany._Code
            'Pinkal (20-Nov-2018) -- End

            Session("CompanyBankGroupId") = objCompany._Bankgroupunkid
            Session("CompanyBankBranchId") = objCompany._Branchunkid

            'Sohail (26 Feb 2018) -- Start
            'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
            Session("Senderaddress") = objCompany._Senderaddress
            Session("Email_Type") = CInt(objCompany._Email_Type)
            Session("Mailserverip") = objCompany._Mailserverip
            Session("Mailserverport") = CInt(objCompany._Mailserverport)
            Session("SenderUsername") = objCompany._Username
            Session("SenderUserPassword") = objCompany._Password
            Session("Isloginssl") = CBool(objCompany._Isloginssl)
            Session("IsCertificateAuthorization") = CBool(objCompany._IsCertificateAuthorization)
            Session("EWS_Domain") = objCompany._EWS_Domain
            Session("EWS_URL") = objCompany._EWS_URL
            'S.SANDEEP [21-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002552}
            Session("ByPassProxy") = objCompany._Isbypassproxy
            'S.SANDEEP [21-SEP-2018] -- END
            'Sohail (18 Jul 2019) -- Start
            'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
            Session("SenderName") = objCompany._Sendername
            'Sohail (18 Jul 2019) -- End

            'S.SANDEEP [29-SEP-2022] -- START
            'ISSUE/ENHANCEMENT : CODE NOT IMPLEMENTED HERE BY DEVELOPER
            Session("protocolunkid") = objCompany._Protocolunkid
            'S.SANDEEP [29-SEP-2022] -- END

            'Sohail (26 Feb 2018) -- End
            'Sohail (28 Mar 2018) -- Start
            'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            Dim a As Drawing.Bitmap
            If objCompany._Image IsNot Nothing Then
                a = objCompany._Image
            Else
                a = New Drawing.Bitmap(16, 16, Drawing.Imaging.PixelFormat.Format24bppRgb)
                a.MakeTransparent()
            End If
            Dim ms As New System.IO.MemoryStream
            a.Save(ms, Drawing.Imaging.ImageFormat.Png)
            Dim byteImage() As Byte = ms.ToArray
            Session("CompanyLogo") = Convert.ToBase64String(byteImage)
            'Sohail (28 Mar 2018) -- End


            'Session("DontAllowRatingBeyond100") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DontAllowRatingBeyond100).Key)), clsConfig._DontAllowRatingBeyond100.GetType())
            'Session("BSC_StatusColors") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._BSC_StatusColors).Key)), clsConfig._BSC_StatusColors.GetType())
            'Session("ConsiderZeroAsNumUpdateProgress") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ConsiderZeroAsNumUpdateProgress).Key)), clsConfig._ConsiderZeroAsNumUpdateProgress.GetType())
            'Session("MisconductConsecutiveAbsentDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MisconductConsecutiveAbsentDays).Key)), clsConfig._MisconductConsecutiveAbsentDays.GetType())
            'Session("MaxRoutePerExpCategory") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MaxRoutePerExpCategory).Key)), clsConfig._MaxRoutePerExpCategory.GetType())
            'Session("DoNotAllowCurrentFutureDatesToApplyOT") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowCurrentFutureDatesToApplyOT).Key)), clsConfig._DoNotAllowCurrentFutureDatesToApplyOT.GetType())
            'Session("DoNotAllowOTEndTimeGreaterThanClockOutTime") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._DoNotAllowOTEndTimeGreaterThanClockOutTime).Key)), clsConfig._DoNotAllowOTEndTimeGreaterThanClockOutTime.GetType())
            'Session("ApplyTOTPForLoanApplicationApproval") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._ApplyTOTPForLoanApplicationApproval).Key)), clsConfig._ApplyTOTPForLoanApplicationApproval.GetType())
            'Session("TOTPTimeLimitInSeconds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TOTPTimeLimitInSeconds).Key)), clsConfig._TOTPTimeLimitInSeconds.GetType())
            'Session("MaxTOTPUnSuccessfulAttempts") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MaxTOTPUnSuccessfulAttempts).Key)), clsConfig._MaxTOTPUnSuccessfulAttempts.GetType())
            'Session("TOTPRetryAfterMins") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._TOTPRetryAfterMins).Key)), clsConfig._TOTPRetryAfterMins.GetType())
            'Session("MakeAttachmentMandatoryOnTransfer") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnTransfer).Key)), clsConfig._MakeAttachmentMandatoryOnTransfer.GetType())
            'Session("MakeAttachmentMandatoryOnRecategorize") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnRecategorize).Key)), clsConfig._MakeAttachmentMandatoryOnRecategorize.GetType())
            'Session("MakeAttachmentMandatoryOnSalaryChange") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnSalaryChange).Key)), clsConfig._MakeAttachmentMandatoryOnSalaryChange.GetType())
            'Session("MakeAttachmentMandatoryOnPromotion") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnPromotion).Key)), clsConfig._MakeAttachmentMandatoryOnPromotion.GetType())
            'Session("MakeAttachmentMandatoryOnRehire") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeAttachmentMandatoryOnRehire).Key)), clsConfig._MakeAttachmentMandatoryOnRehire.GetType())
            'Session("PhotoPath") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._PhotoPath).Key)), clsConfig._PhotoPath.GetType())
            'Session("StaffTransferRequestAllocations") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffTransferRequestAllocations).Key)), clsConfig._StaffTransferRequestAllocations.GetType())
            'Session("StaffTransferFinalApprovalNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._StaffTransferFinalApprovalNotificationUserIds).Key)), clsConfig._StaffTransferFinalApprovalNotificationUserIds.GetType())
            'Session("SingleCommentForAllObjectivesForEmpAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllObjectivesForEmpAssess).Key)), clsConfig._SingleCommentForAllObjectivesForEmpAssess.GetType())
            'Session("SingleCommentForAllObjectivesForAsrAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllObjectivesForAsrAssess).Key)), clsConfig._SingleCommentForAllObjectivesForAsrAssess.GetType())
            'Session("SingleCommentForAllObjectivesForRevAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllObjectivesForRevAssess).Key)), clsConfig._SingleCommentForAllObjectivesForRevAssess.GetType())
            'Session("SingleCommentForAllCompetenciesForEmpAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllCompetenciesForEmpAssess).Key)), clsConfig._SingleCommentForAllCompetenciesForEmpAssess.GetType())
            'Session("SingleCommentForAllCompetenciesForAsrAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllCompetenciesForAsrAssess).Key)), clsConfig._SingleCommentForAllCompetenciesForAsrAssess.GetType())
            'Session("SingleCommentForAllCompetenciesForRevAssess") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._SingleCommentForAllCompetenciesForRevAssess).Key)), clsConfig._SingleCommentForAllCompetenciesForRevAssess.GetType())
            'Session("AllowPartialApproveRejectObjectives") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowPartialApproveRejectObjectives).Key)), clsConfig._AllowPartialApproveRejectObjectives.GetType())
            'Session("EFTMobileMoneyBank") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyBank).Key)), clsConfig._EFTMobileMoneyBank.GetType())
            'Session("EFTMobileMoneyBranch") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyBranch).Key)), clsConfig._EFTMobileMoneyBranch.GetType())
            'Session("EFTMobileMoneyAccountName") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyAccountName).Key)), clsConfig._EFTMobileMoneyAccountName.GetType())
            'Session("EFTMobileMoneyAccountNo") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EFTMobileMoneyAccountNo).Key)), clsConfig._EFTMobileMoneyAccountNo.GetType())
            'Session("CompanyAssetP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyAssetP2PServiceURL).Key)), clsConfig._CompanyAssetP2PServiceURL.GetType())
            'Session("MandatoryAttachmentsForLoanTrancheApplication") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MandatoryAttachmentsForLoanTrancheApplication).Key)), clsConfig._MandatoryAttachmentsForLoanTrancheApplication.GetType())
            'Session("EmpNotificationTrancheDateBeforeDays") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._EmpNotificationTrancheDateBeforeDays).Key)), clsConfig._EmpNotificationTrancheDateBeforeDays.GetType())
            'Session("MakeRemarkMandatoryForProgressUpdate") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._MakeRemarkMandatoryForProgressUpdate).Key)), clsConfig._MakeRemarkMandatoryForProgressUpdate.GetType())
            'Session("CompanyAssetP2PServiceURL") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._CompanyAssetP2PServiceURL).Key)), clsConfig._CompanyAssetP2PServiceURL.GetType())
            'Session("AllowRejectedProgressUpdateToEdit") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._AllowRejectedProgressUpdateToEdit).Key)), clsConfig._AllowRejectedProgressUpdateToEdit.GetType())
            'Session("LoginRequiredToApproveApplications") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoginRequiredToApproveApplications).Key)), clsConfig._LoginRequiredToApproveApplications.GetType())
            'Session("LoanFlexcubeSuccessfulNotificationUserIds") = Convert.ChangeType(objDataOperation.GetConfigValue(intCompanyID, Session("ConfigPropsDict")(GetPropertyInfo(Function() clsConfig._LoanFlexcubeSuccessfulNotificationUserIds).Key)), clsConfig._LoanFlexcubeSuccessfulNotificationUserIds.GetType())



            '================================ END TO SET COMPANY RELATED SESSIONS=====================================



            Dim objMaster As New clsMasterData



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Session("UserAccessLevel") = objMaster.GetUserAccessLevel(CInt(Session("UserId")), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("UserAccessModeSetting"))
            'Shani(20-Nov-2015) -- End

            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                HttpContext.Current.Session("UserAccessLevel") = ""
            End If

            If Session("UserAccessLevel") = "" Then
                HttpContext.Current.Session("AccessLevelFilterString") = " AND 1 = 1  "

                HttpContext.Current.Session("AccessLevelBranchFilterString") = " "
                HttpContext.Current.Session("AccessLevelDepartmentGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelDepartmentFilterString") = " "
                HttpContext.Current.Session("AccessLevelSectionGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelSectionFilterString") = " "
                HttpContext.Current.Session("AccessLevelUnitGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelUnitFilterString") = " "
                HttpContext.Current.Session("AccessLevelTeamFilterString") = " "
                HttpContext.Current.Session("AccessLevelJobGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelJobFilterString") = " "
                HttpContext.Current.Session("AccessLevelClassGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelClassFilterString") = " "
            Else
                HttpContext.Current.Session("AccessLevelFilterString") = UserAccessLevel._AccessLevelFilterString

                HttpContext.Current.Session("AccessLevelBranchFilterString") = UserAccessLevel._AccessLevelBranchFilterString
                HttpContext.Current.Session("AccessLevelDepartmentGroupFilterString") = UserAccessLevel._AccessLevelDepartmentGroupFilterString
                HttpContext.Current.Session("AccessLevelDepartmentFilterString") = UserAccessLevel._AccessLevelDepartmentFilterString
                HttpContext.Current.Session("AccessLevelSectionGroupFilterString") = UserAccessLevel._AccessLevelSectionGroupFilterString
                HttpContext.Current.Session("AccessLevelSectionFilterString") = UserAccessLevel._AccessLevelSectionFilterString
                HttpContext.Current.Session("AccessLevelUnitGroupFilterString") = UserAccessLevel._AccessLevelUnitGroupFilterString
                HttpContext.Current.Session("AccessLevelUnitFilterString") = UserAccessLevel._AccessLevelUnitFilterString
                HttpContext.Current.Session("AccessLevelTeamFilterString") = UserAccessLevel._AccessLevelTeamFilterString
                HttpContext.Current.Session("AccessLevelJobGroupFilterString") = UserAccessLevel._AccessLevelJobGroupFilterString
                HttpContext.Current.Session("AccessLevelJobFilterString") = UserAccessLevel._AccessLevelJobFilterString
                HttpContext.Current.Session("AccessLevelClassGroupFilterString") = UserAccessLevel._AccessLevelClassGroupFilterString
                HttpContext.Current.Session("AccessLevelClassFilterString") = UserAccessLevel._AccessLevelClassFilterString
            End If

            'S.SANDEEP |04-AUG-2021| -- START
            Call SetIP_HOST()
            'Try
            '    If HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
            '        'Session("IP_ADD") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR")).AddressList(0).ToString
            '        'Session("HOST_NAME") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR").ToString).HostName.ToString()
            '        HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            '    Else
            '        HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
            '        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            '    End If

            '    'Gajanan [29-July-2020] -- Start
            '    'Enhancement: Get Hostanme from Database 
            '    'If HttpContext.Current.Session("HOST_NAME") Is Nothing Then HttpContext.Current.Session("HOST_NAME") = ""
            '    'If HttpContext.Current.Session("IP_ADD") = IIf(HttpContext.Current.Session("HOST_NAME").ToString.Trim.Length <= 0, HttpContext.Current.Session("IP_ADD"), HttpContext.Current.Session("HOST_NAME")) Then
            '    '    Dim mstrhost As String = ""
            '    '    mstrhost = GetHostNameFromIp(HttpContext.Current.Session("IP_ADD"))
            '    '    If mstrhost.Trim().Length > 0 Then
            '    '        HttpContext.Current.Session("HOST_NAME") = mstrhost
            '    '    End If
            '    'End If
            '    'If HttpContext.Current.Session("HOST_NAME") Is Nothing Then HttpContext.Current.Session("HOST_NAME") = ""
            '    'If HttpContext.Current.Session("IP_ADD") = IIf(HttpContext.Current.Session("HOST_NAME").ToString.Trim.Length <= 0, HttpContext.Current.Session("IP_ADD"), HttpContext.Current.Session("HOST_NAME")) Then
            '    Dim mstrhost As String = ""
            '    mstrhost = GetHostNameFromIp(HttpContext.Current.Session("IP_ADD"))
            '    If mstrhost.Trim().Length > 0 Then
            '        HttpContext.Current.Session("HOST_NAME") = mstrhost
            '    End If
            '    'End If
            '    'Gajanan [29-July-2020] -- End

            '    'S.SANDEEP |15-AUG-2020| -- START
            '    'ISSUE/ENHANCEMENT : Log Error Only
            'Catch ex As System.Net.Sockets.SocketException
            '    CommonCodes.LogErrorOnly(ex)
            '    HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '    HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '    'S.SANDEEP |15-AUG-2020| -- END
            'Catch ex As Exception
            '    CommonCodes.LogErrorOnly(ex)
            '    HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '    HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            'End Try
            'S.SANDEEP |04-AUG-2021| -- END

            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objMaster = Nothing
            clsConfig = Nothing
            objCompany = Nothing
            'Pinkal (11-Sep-2020) -- End


            Return True

        Catch ex As Exception
            strErrorMsg = "Error in SetCompanySessions : " & ex.Message
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Function

    Public Function GetCompanyYearInfo(ByRef strErrorMsg As String, ByVal strCompUnkId As Integer) As Boolean
        Dim objDataOperation As New clsDataOperation(True)
        Dim dsList As DataSet
        Dim mSQL As String = ""
        strErrorMsg = ""
        Try
            mSQL = "SELECT  cfcompany_master.companyunkid " & _
                               ", cfcompany_master.code AS CompCode " & _
                               ", cfcompany_master.name AS CompName " & _
                               ", cffinancial_year_tran.yearunkid " & _
                               ", cffinancial_year_tran.financialyear_name " & _
                               ", cffinancial_year_tran.database_name " & _
                               ", cffinancial_year_tran.start_date " & _
                               ", cffinancial_year_tran.end_date " & _
                               ", hrmsConfiguration..cfcompany_master.countryunkid " & _
                               ", cffinancial_year_tran.isclosed " & _
                   "FROM    hrmsConfiguration..cfcompany_master " & _
                                        "JOIN hrmsConfiguration..cffinancial_year_tran ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
                                        "AND cfcompany_master.isactive = 1 " & _
                                        "AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE cfcompany_master.companyunkid = " & strCompUnkId & " "

            dsList = objDataOperation.WExecQuery(mSQL, "List")

            For Each dsRow As DataRow In dsList.Tables("List").Rows
                HttpContext.Current.Session("CompCode") = dsRow.Item("CompCode")
                HttpContext.Current.Session("CompName") = dsRow.Item("CompName")
                HttpContext.Current.Session("Fin_year") = dsRow.Item("yearunkid")
                HttpContext.Current.Session("FinancialYear_Name") = dsRow.Item("financialyear_name")
                HttpContext.Current.Session("Database_Name") = dsRow.Item("database_name")
                HttpContext.Current.Session("fin_startdate") = dsRow.Item("start_date")
                HttpContext.Current.Session("fin_enddate") = dsRow.Item("end_date")
                HttpContext.Current.Session("Compcountryid") = dsRow.Item("countryunkid")
                HttpContext.Current.Session("isclosed") = CBool(dsRow.Item("isclosed"))

            Next

            Return True

        Catch ex As Exception
            strErrorMsg = "Error in GetCompanyYearInfo : " & ex.Message
        End Try
    End Function

    Public Function IsAccessGivenUserEmp(ByRef strErrorMsg As String, ByVal intLoginBy As Integer, ByVal intUserEmpUnkID As Integer) As Boolean
        Dim objDataOperation As New clsDataOperation(True)
        Dim StrQ As String = String.Empty
        Dim dList As DataSet
        strErrorMsg = ""
        Try
            If (intLoginBy = Global.User.en_loginby.User) Then

                StrQ = "SELECT * FROM hrmsConfiguration..cfcompanyaccess_privilege " & _
                   " WHERE userunkid = " & intUserEmpUnkID & " AND yearunkid = " & CInt(Session("Fin_year")) & " "

            ElseIf (intLoginBy = Global.User.en_loginby.Employee) Then

                StrQ = "SELECT  employeeunkid, password FROM " & Session("Database_Name") & "..hremployee_master WHERE ISNULL(isapproved, 0) = 1 AND employeeunkid = " & intUserEmpUnkID & " "

            End If

            dList = objDataOperation.WExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                strErrorMsg = "basepage-->IsValidUserEmp Function" & objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                Return False
            End If

            If dList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                If (intLoginBy = Global.User.en_loginby.User) Then
                    strErrorMsg = "Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator."
                ElseIf (intLoginBy = Global.User.en_loginby.Employee) Then
                    strErrorMsg = "Sorry, You cannot login to system. Reason : Either you are not belong to selected company or you are not approved yet. Please contact Administrator."
                End If
                Return False
            End If

        Catch ex As Exception
            Throw New Exception("basepage-->IsAccessGivenUserEmp Function" & ex.Message)
        End Try
    End Function
    'Sohail (30 Mar 2015) -- End


    'Pinkal (01-Apr-2015) -- Start
    'Enhancement - IMPLEMENT EMPLOYEE RE-CATEGORIZATION FOR TANAPA

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw New Exception("basepage-->b64encode Function" & ex.Message)
        End Try
    End Function

    Public Shared Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            Throw New Exception("basepage-->b64decode Function" & ex.Message)
        End Try
        Return decodedString
    End Function


    'Pinkal (01-Apr-2015) -- End

    'Nilay (28-Aug-2015) -- Start
    <WebMethod()> Public Shared Sub SetCashDenimination(ByVal intRowId As Integer, ByVal strColName As String, ByVal strValue As String)
        Try
            If HttpContext.Current.Session("PayCashDenomination") Is Nothing Then Exit Sub
            Dim dtCashDenomination As DataTable = CType(HttpContext.Current.Session("PayCashDenomination"), DataTable)
            If dtCashDenomination.Rows.Count >= intRowId Then
                If dtCashDenomination.Columns.Contains(strColName) = False Then Exit Sub
                dtCashDenomination.Rows(intRowId)(strColName) = CStr(IIf(strValue.Trim.Length > 0, strValue.Trim, "0"))
                HttpContext.Current.Session("PayCashDenomination") = dtCashDenomination
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Nilay (28-Aug-2015) -- End

    'S.SANDEEP |08-FEB-2022| -- START
    'ISSUE : CODE OPTIMIZATION/ERROR SOLVING
    ''Sohail (27 Apr 2013) -- Start
    ''TRA - ENHANCEMENT
    '<WebMethod()> Public Overloads Shared Function CheckLicence(ByVal strModuleName As String) As Boolean
    '    Try
    '        If ConfigParameter._Object Is Nothing Then
    '            If HttpContext.Current.Session("clsuser") Is Nothing Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        End If
    '        If ConfigParameter._Object._IsArutiDemo = True Then 'IsDemo
    '            If ConfigParameter._Object._IsExpire = False Then
    '                'Throw New Exception("Not Licsened")
    '                Return True
    '                'Else
    '                'Return False
    '            End If
    '            'If ArtLic._Object.DaysLeft <= -1 Then
    '            '    Return False
    '            'End If
    '            Return True
    '        End If

    '        Select Case strModuleName.Trim.ToUpper

    '            Case "PAYROLL_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
    '                    Return False
    '                End If

    '            Case "LOAN_AND_SAVINGS_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
    '                    Return False
    '                End If

    '            Case "EMPLOYEE_BIODATA_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
    '                    Return False
    '                End If

    '            Case "LEAVE_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
    '                    Return False
    '                End If

    '            Case "TIME_AND_ATTENDANCE_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
    '                    Return False
    '                End If

    '            Case "EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
    '                    Return False
    '                End If

    '            Case "TRAININGS_NEEDS_ANALYSIS"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) = False Then
    '                    Return False
    '                End If

    '            Case "ON_JOB_TRAINING_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
    '                    Return False
    '                End If

    '            Case "RECRUITMENT_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
    '                    Return False
    '                End If

    '            Case "ONLINE_JOB_APPLICATIONS_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Online_Job_Applications_Management) = False Then
    '                    Return False
    '                End If

    '            Case "EMPLOYEE_DISCIPLINARY_CASES_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
    '                    Return False
    '                End If

    '            Case "MEDICAL_BILLS_AND_CLAIMS_MANAGEMENT"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
    '                    Return False
    '                End If

    '            Case "EMPLOYEE_SELF_SERVICE"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Self_Service) = False Then
    '                    Return False
    '                End If

    '            Case "MANAGER_SELF_SERVICE"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False Then
    '                    Return False
    '                End If

    '            Case "EMPLOYEE_ASSETS_DECLARATIONS"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
    '                    Return False
    '                End If

    '            Case "CUSTOM_REPORT_ENGINE"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Custom_Report_Engine) = False Then
    '                    Return False
    '                End If

    '            Case "INTEGRATION_ACCOUNTING_SYSTEM_ERPS"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Accounting_System_ERPs) = False Then
    '                    Return False
    '                End If

    '            Case "INTEGRATION_ELECTRONIC_BANKING_INTERFACE"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Electronic_Banking_Interface) = False Then
    '                    Return False
    '                End If

    '            Case "Integration_Time_Attendance_Devices"
    '                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices) = False Then
    '                    Return False
    '                End If

    '            Case Else
    '                Return False
    '        End Select

    '        'Pinkal (13-Aug-2020) -- Start
    '        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    '    Catch ex As Exception
    '        InsertErrorLog("CheckLicence", ex)
    '        Return False
    '    End Try
    '    'Pinkal (13-Aug-2020) -- End

    '    Return True
    'End Function
    ''Sohail (27 Apr 2013) -- End

    <WebMethod()> Public Overloads Shared Function CheckLicence(ByVal strModuleName As String) As Boolean
        Dim objConfig As New clsConfigOptions
        Try
                If HttpContext.Current.Session("clsuser") Is Nothing Then
                    Return True
                End If

            Dim blnflgIsDemo As Boolean = False : blnflgIsDemo = objConfig._IsArutiDemo
            Dim blnflgIsExpire As Boolean = False : blnflgIsExpire = objConfig._IsExpire

            If blnflgIsDemo = True Then 'IsDemo
                If blnflgIsExpire = False Then
                    'Throw New Exception("Not Licsened")
                    Return True
                    'Else
                    'Return False
                End If
                'If ArtLic._Object.DaysLeft <= -1 Then
                '    Return False
                'End If
                Return True
            End If

            Select Case strModuleName.Trim.ToUpper

                Case "PAYROLL_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                        Return False
                    End If

                Case "LOAN_AND_SAVINGS_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                        Return False
                    End If

                Case "EMPLOYEE_BIODATA_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                        Return False
                    End If

                Case "LEAVE_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                        Return False
                    End If

                Case "TIME_AND_ATTENDANCE_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                        Return False
                    End If

                Case "EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                        Return False
                    End If

                Case "TRAININGS_NEEDS_ANALYSIS"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) = False Then
                        Return False
                    End If

                Case "ON_JOB_TRAINING_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                        Return False
                    End If

                Case "RECRUITMENT_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
                        Return False
                    End If

                Case "ONLINE_JOB_APPLICATIONS_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Online_Job_Applications_Management) = False Then
                        Return False
                    End If

                Case "EMPLOYEE_DISCIPLINARY_CASES_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                        Return False
                    End If

                Case "MEDICAL_BILLS_AND_CLAIMS_MANAGEMENT"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
                        Return False
                    End If

                Case "EMPLOYEE_SELF_SERVICE"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Self_Service) = False Then
                        Return False
                    End If

                Case "MANAGER_SELF_SERVICE"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False Then
                        Return False
                    End If

                Case "EMPLOYEE_ASSETS_DECLARATIONS"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
                        Return False
                    End If

                Case "CUSTOM_REPORT_ENGINE"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Custom_Report_Engine) = False Then
                        Return False
                    End If

                Case "INTEGRATION_ACCOUNTING_SYSTEM_ERPS"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Accounting_System_ERPs) = False Then
                        Return False
                    End If

                Case "INTEGRATION_ELECTRONIC_BANKING_INTERFACE"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Electronic_Banking_Interface) = False Then
                        Return False
                    End If

                Case "Integration_Time_Attendance_Devices"
                    If blnflgIsDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices) = False Then
                        Return False
                    End If

                Case Else
                    Return False
            End Select

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("CheckLicence", ex)
            Return False
        Finally
            objConfig = Nothing
        End Try
        'Pinkal (13-Aug-2020) -- End

        Return True
    End Function
    'S.SANDEEP |08-FEB-2022| -- END





    'Sohail (24 Nov 2016) -- Start
    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.CheckLicence
    Public Shared Function UpdateThemeID(ByVal intThemeId As Integer) As Boolean
        Try
            If (HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(HttpContext.Current.Session("UserId"))
                objUser._Theme_Id = intThemeId
                objUser.Update(True)
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._LoginEmployeeUnkid = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._Theme_Id = intThemeId


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                '                                        CInt(HttpContext.Current.Session("Fin_year")), _
                '                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                '                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                '                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                '                                        CInt(HttpContext.Current.Session("UserId")), False, _
                '                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                '                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                '                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                '                                        ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
                '                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                '                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))


                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                Dim objConfig As New clsConfigOptions
                Dim mdtCurrentDate As Date = objConfig._CurrentDateAndTime
                objConfig = Nothing

                'objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                '                                        CInt(HttpContext.Current.Session("Fin_year")), _
                '                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                '                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                '                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                '                                        CInt(HttpContext.Current.Session("UserId")), False, _
                '                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                '                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                '                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                '                                      ConfigParameter._Object._CurrentDateAndTime.Date, CBool(HttpContext.Current.Session("CreateADUserFromEmpMst")), _
                '                                      CBool(HttpContext.Current.Session("UserMustchangePwdOnNextLogon")), , , _
                '                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                '                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))


                objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                                                        CInt(HttpContext.Current.Session("Fin_year")), _
                                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                                                        CInt(HttpContext.Current.Session("UserId")), False, _
                                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                                                        mdtCurrentDate.Date, CBool(HttpContext.Current.Session("CreateADUserFromEmpMst")), _
                                                      CBool(HttpContext.Current.Session("UserMustchangePwdOnNextLogon")), , , _
                                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))

                'Pinkal (18-Aug-2018) -- End

                'Pinkal (25-Jan-2022) -- End


            End If
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Throw New Exception("UpdateThemeID : " & ex.Message)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function

    Public Shared Function UpdateLastViewID(ByVal intLastViewId As Integer) As Boolean
        Try
            If (HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User) Then
                HttpContext.Current.Session("U_Lastview_id") = intLastViewId 'Sohail (23 Dec 2020)
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(HttpContext.Current.Session("UserId"))
                objUser._LastView_Id = intLastViewId
                objUser.Update(True)
            Else
                HttpContext.Current.Session("E_Lastview_id") = intLastViewId 'Sohail (23 Dec 2020)
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._LoginEmployeeUnkid = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._LastView_Id = intLastViewId


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                '                                        CInt(HttpContext.Current.Session("Fin_year")), _
                '                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                '                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                '                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                '                                        CInt(HttpContext.Current.Session("UserId")), False, _
                '                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                '                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                '                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                '                                        ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
                '                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                '                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))


                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                Dim objConfig As New clsConfigOptions
                Dim mdtCurrentDate As Date = objConfig._CurrentDateAndTime
                objConfig = Nothing

                'objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                '                                        CInt(HttpContext.Current.Session("Fin_year")), _
                '                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                '                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                '                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                '                                        CInt(HttpContext.Current.Session("UserId")), False, _
                '                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                '                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                '                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                '                                        ConfigParameter._Object._CurrentDateAndTime.Date, CBool(HttpContext.Current.Session("CreateADUserFromEmpMst")), _
                '                                        CBool(HttpContext.Current.Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                '                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                '                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))

                objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                                                        CInt(HttpContext.Current.Session("Fin_year")), _
                                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                                                        CInt(HttpContext.Current.Session("UserId")), False, _
                                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                                                        mdtCurrentDate.Date, CBool(HttpContext.Current.Session("CreateADUserFromEmpMst")), _
                                                      CBool(HttpContext.Current.Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))


                'Pinkal (25-Jan-2022) -- End


                'Pinkal (18-Aug-2018) -- End

            End If
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Throw New Exception("UpdateLastViewID :- " & ex.Message)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function
    'Sohail (24 Nov 2016) -- End


    'Shani(01-MAR-2017) -- Start
    <System.Web.Services.WebMethod()> Public Shared Function Delete_File(ByVal strPath As String) As Boolean
        Try

            If System.IO.File.Exists(strPath) Then
                System.IO.File.Delete(strPath)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    'Shani(01-MAR-2017) -- End

    'Pinkal (01-Mar-2023) -- Start
    '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.

    'Public Function IsAuthenticated(ByRef strMsg As String, ByVal mstrIPAddress As String, ByVal mstrDomain As String, ByVal username As String, ByVal pwd As String) As Boolean
    '    Try

    '        If mstrDomain.Trim.Length <= 0 Then Return False

    '        Dim ar() As String = Nothing
    '        If mstrDomain.Trim.Length > 0 AndAlso mstrDomain.Trim.Contains(".") Then
    '            ar = mstrDomain.Trim.Split(CChar("."))
    '            mstrDomain = ""
    '            If ar.Length > 0 Then
    '                For i As Integer = 0 To ar.Length - 1
    '                    mstrDomain &= ",DC=" & ar(i)
    '                Next
    '            End If
    '        End If

    '        If mstrDomain.Trim.Length > 0 Then
    '            mstrDomain = mstrDomain.Trim.Substring(1)
    '        End If

    '        Dim entry As DirectoryEntry = New DirectoryEntry("LDAP://" & mstrIPAddress.Trim & "/" & mstrDomain, username.Trim, pwd.Trim)

    '        'Bind to the native AdsObject to force authentication.
    '        Dim obj As Object = entry.NativeObject
    '        Dim search As DirectorySearcher = New DirectorySearcher(entry)
    '        search.Filter = "(SAMAccountName=" & username & ")"
    '        search.PropertiesToLoad.Add("cn")
    '        Dim result As SearchResult = search.FindOne()

    '        If (result Is Nothing) Then
    '            Return False
    '        End If


    '        '    'Pinkal (20-Aug-2020) -- Start
    '        '    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    '        'Catch exCOM As DirectoryServicesCOMException
    '        '    CommonCodes.LogErrorOnly(exCOM)
    '        '    Return False
    '        '    'Pinkal (20-Aug-2020) -- End
    '    Catch ex As Exception
    '        If ex.Message.Trim.Contains("Unknown error (0x80005000)") Then
    '            'Pinkal (23-Nov-2018) -- Start
    '            'Enhancement - Changed Message As Per NMB Requirement.
    '            'strMsg = "IsAuthenticated : Invalid Username or Password!! Please try again !!!."
    '            strMsg = "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."
    '            'Pinkal (23-Nov-2018) -- End
    '        Else
    '            strMsg = "IsAuthenticated : " & ex.Message
    '        End If
    '        Return False
    '    End Try
    '    Return True
    'End Function

    Public Function IsAuthenticated(ByRef strMsg As String, ByVal mstrIPAddress As String, ByVal mstrDomain As String, ByVal mstrPortNo As String, ByVal username As String, ByVal pwd As String) As Boolean
        Dim mblnFlag As Boolean = True
        Dim objLdap As LdapConnection = Nothing

        'Pinkal (20-Dec-2024) -- Start
        'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
        Dim mblnLocked As Boolean = False
        Dim mblnDisabled As Boolean = False
        'Pinkal (20-Dec-2024) -- End

        Try
            If mstrDomain.Trim.Length <= 0 Then Return False
            Dim ar() As String = Nothing
            If mstrDomain.Trim.Length > 0 AndAlso mstrDomain.Trim.Contains(".") Then
                ar = mstrDomain.Trim.Split(CChar("."))
                mstrDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrDomain.Trim.Length > 0 Then
                mstrDomain = mstrDomain.Trim.Substring(1)
            End If

            objLdap = clsADIntegration.SetADConnection(mstrIPAddress, mstrDomain, mstrPortNo, username, pwd)

            '/* START FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT
            Dim objresult As SearchResultEntry = Nothing

            'Pinkal (20-Dec-2024) -- Start
            'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
            Dim mstrAttributes() As String = New String() {"SAMAccountName", "userAccountControl", "lockoutTime"}
            'Pinkal (20-Dec-2024) -- End

            Dim objSSLSearch As New SearchRequest(mstrDomain, "(SAMAccountName=" & username & ")", DirectoryServices.Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLdap.SendRequest(objSSLSearch), SearchResponse)

            If objResponse.ResultCode = ResultCode.Success Then
                If objResponse.Entries.Count > 0 Then
                    objresult = objResponse.Entries(0)

                    'Pinkal (20-Dec-2024) -- Start
                    'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 

                    'WHETHER USER IS LOCKOUT OR NOT
                    Dim lockoutTime As Long = If(objresult.Attributes("lockoutTime").Count > 0, CLng(objresult.Attributes("lockoutTime")(0)), 0)
                    If lockoutTime > 0 Then
                        mblnLocked = True
                        mblnDisabled = False
                        mblnFlag = False
                        Throw New Exception("Your account is currently locked due to multiple failed login attempts. Please contact your system administrator or wait until the lockout period expires.")
                    End If

                    'WHETHER USER IS DISABLED OR NOT
                    Dim uac As Integer = If(objresult.Attributes("userAccountControl").Count > 0, CInt(objresult.Attributes("userAccountControl")(0)), 0)
                    If (uac And 2) <> 0 Then
                        mblnDisabled = True
                        mblnLocked = False
                        mblnFlag = False
                        Throw New Exception("Your account is currently disabled. Please contact your system administrator.")
                    End If

                    'Pinkal (20-Dec-2024) -- End

                    mblnFlag = True
                Else
                    mblnFlag = False
                End If
            Else
                mblnFlag = False
            End If
            '/* END FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT

        Catch ex As Exception
            If ex.Message.Trim.Contains("Unknown error (0x80005000)") Then
                strMsg = "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."
            Else
                'Pinkal (20-Dec-2024) -- Start
                'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
                If mblnLocked Then
                    strMsg = "Your account is currently locked due to multiple failed login attempts. Please contact your system administrator or wait until the lockout period expires."
                ElseIf mblnDisabled Then
                    strMsg = "Your account is currently disabled. Please contact your system administrator."
                Else
                    strMsg = "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."
                End If
                'Pinkal (20-Dec-2024) -- End
            End If
            mblnFlag = False
        Finally
            objLdap = Nothing
        End Try
        Return mblnFlag
    End Function

    'Pinkal (01-Mar-2023) -- End

    'Sohail (05 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client and Send error in email to aruti help desk.
    <WebMethod()> Public Shared Function SendErrorReport(ByVal strError As String) As String
        Dim objSendMail As New clsSendMail
        Dim strResult As String = String.Empty
        Try
            If HttpContext.Current.Session("AdministratorEmailForError") Is Nothing OrElse HttpContext.Current.Session("AdministratorEmailForError").ToString.Trim = "" Then
                System.Threading.Thread.Sleep(1000)
                Return "Administrator Email is not set. Please set Administrator Email on Aruti Configuration to Report Error."
                Exit Try
            End If
            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
            'Dim ser As New JavaScriptSerializer()
            'Dim objError As clsErrorlog_Tran = ser.Deserialize(Of clsErrorlog_Tran)(strError.Replace("""\/Date(", "").Replace(")\/""", ""))
            'S.SANDEEP |04-MAR-2020| -- END


            'Sohail (12 Apr 2019) -- Start
            'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
            'objSendMail._ToEmail = HttpContext.Current.Session("AdministratorEmailForError").ToString
            'objSendMail._ToEmail = "arutihelpdesk@aruti.com"
            'objSendMail._CCAddress = "anatory@npktechnologies.com"
            'objSendMail._BCCAddress = "anjan@ezeetechnosys.com"
            'Sohail (12 Apr 2019) -- Start
            'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
            'objSendMail._Subject = "Error in Aruti Self Service : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "Error : <B>" & strError & "</B>"
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Code : " & HttpContext.Current.Session("CompCode")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Name : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Database Version : " & HttpContext.Current.Session("DatabaseVersion")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Location : " & HttpContext.Current.Request.Url.AbsoluteUri.Replace("/SendErrorReport", "")
            'objSendMail._Message &= "<BR />"
            'If HttpContext.Current.Session("Firstname").ToString.Trim <> "" Then
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("Firstname").ToString & " " & HttpContext.Current.Session("Surname").ToString
            'Else
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("UserName").ToString
            'End If


            'strResult = objSendMail.SendMail(CInt(HttpContext.Current.Session("CompanyUnkId")))
            'objSendMail._Subject = "Error in Aruti Self Service : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "Error : <B>" & clsCrypto.Dicrypt(objError._Error_Message) & "</B>"
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Code : " & HttpContext.Current.Session("CompCode")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Name : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Database Version : " & objError._Database_Version
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Location : " & objError._Error_Location
            'objSendMail._Message &= "<BR />"
            'If HttpContext.Current.Session("Firstname").ToString.Trim <> "" Then
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("Firstname").ToString & " " & HttpContext.Current.Session("Surname").ToString
            'Else
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("UserName").ToString
            'End If

            'strResult = objSendMail.SendMail(CInt(HttpContext.Current.Session("CompanyUnkId")))

            'If strResult.Trim = "" Then
            '    objError._Isemailsent = True
            '    If objError.Update(Nothing, objError) = False Then

            '    End If
            'End If

            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
            'Dim lstError As List(Of clsErrorlog_Tran) = objError.GetListCollection(Nothing, objError._Errorlogunkid)
            Dim objError As New clsErrorlog_Tran
            Dim lstError As List(Of clsErrorlog_Tran) = objError.GetListCollection(Nothing, CInt(strError))
            'S.SANDEEP |04-MAR-2020| -- END

            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                objError._LoginTypeId = enLogin_Mode.MGR_SELF_SERVICE
            Else
                objError._LoginTypeId = enLogin_Mode.EMP_SELF_SERVICE
            End If
            objError._Form_Name = "frmErrorLog"
            objError._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT

            If objError.SendEmail(Nothing, lstError) = False Then
                If objError._Message <> "" Then
                    Return objError._Message
                End If
            End If
            'Sohail (12 Apr 2019) -- End
            'Sohail (12 Apr 2019) -- End

            Return strResult

        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    'Sohail (05 Apr 2019) -- End

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Public Shared Function DownloadAllDocument(ByVal zipFilename As String, _
                                               ByVal dtTable As DataTable, _
                                               ByVal strSelfServiceURL As String, _
                                               Optional ByVal compression As CompressionOption = CompressionOption.Normal) As String
        Dim strMessage As String = String.Empty
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                If zipFilename.Trim.Length <= 0 Then zipFilename = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                Dim dfile As DataTable = Nothing
                If dtTable.Columns.Contains("AUD") Then
                    dfile = dtTable.Select("AUD" <> "D").CopyToDataTable()
                Else
                    dfile = dtTable
                End If
                If dfile IsNot Nothing AndAlso dfile.Rows.Count > 0 Then
                    If dfile.Columns.Contains("temppath") = False Then dfile.Columns.Add("temppath", Type.GetType("System.String"))
                    If dfile.Columns.Contains("error") = False Then dfile.Columns.Add("error", Type.GetType("System.String"))
                    Dim strError As String = String.Empty : Dim strLocalPath As String = String.Empty
                    Dim fileToAdd As New List(Of String)
                    For Each xRow As DataRow In dfile.Rows
                        If IsDBNull(xRow("file_data")) = False Then
                            strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & IIf(xRow("fileuniquename").ToString.Trim.Length <= 0, xRow("filename").ToString, xRow("fileuniquename").ToString).ToString.Replace(" ", "")
                            Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                            Dim fs As New FileStream(strLocalPath, FileMode.Create)
                            ms.WriteTo(fs)
                            ms.Close()
                            fs.Close()
                            fs.Dispose()
                            If strLocalPath <> "" Then
                                fileToAdd.Add(strLocalPath)
                            End If
                        ElseIf IsDBNull(xRow("file_data")) = True Then
                            If xRow("temppath").ToString.Trim <> "" Then
                                If System.IO.File.Exists(xRow("temppath").ToString.Trim) Then
                                    strLocalPath = xRow("temppath").ToString.Trim
                                End If
                            Else
                                strLocalPath = xRow("filepath").ToString
                                strLocalPath = strLocalPath.Replace(strSelfServiceURL, "")
                                If Strings.Left(strLocalPath, 1) <> "/" Then
                                    strLocalPath = "~/" & strLocalPath
                                Else
                                    strLocalPath = "~" & strLocalPath
                                End If
                                strLocalPath = System.Web.HttpContext.Current.Server.MapPath(strLocalPath)
                                If strLocalPath <> "" Then
                                    If IO.File.Exists(strLocalPath) Then
                                        fileToAdd.Add(strLocalPath)
                                    End If
                                End If
                            End If
                        End If
                    Next
                    If fileToAdd.Count > 0 Then
                        strMessage = AddFileToZip(zipFilename, fileToAdd)
                    Else
                        strMessage = "No Files to download."
                    End If
                End If
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            strMessage = ex.Message
        End Try
        Return strMessage
    End Function

    Private Shared Function AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal) As String
        Dim strMessage As String = String.Empty
        Try
            If zipFilename.Trim.Length <= 0 Then zipFilename = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            Using ms As New MemoryStream()
                Using pkg As Package = Package.Open(ms, FileMode.Create)
                    For Each item As String In fileToAdd
                        'S.SANDEEP |11-FEB-2022| -- START
                        'ISSUE : CODE OPTIMIZATION/ERROR SOLVING/URI ERROR
                        'Dim destFilename As String = "/" & Path.GetFileName(item)
                        'Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                        'Dim byt As Byte() = File.ReadAllBytes(item)

                        'Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                        '    CopyStream(CType(fileStream, Stream), part.GetStream())
                        'End Using
                        Try
                        Dim destFilename As String = "/" & Path.GetFileName(item)
                        Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                        Dim byt As Byte() = File.ReadAllBytes(item)

                        Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                        Catch ex As System.ArgumentException
                            Continue For
                        Catch ex As System.UriFormatException
                            Continue For
                        End Try
                        'S.SANDEEP |11-FEB-2022| -- END
                    Next
                    pkg.Close()
                End Using
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/zip"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
                HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
                HttpContext.Current.Response.Flush()
                'Pinkal (11-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                HttpContext.Current.Response.End()
                'Pinkal (11-Feb-2022) -- End
            End Using
        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception
            strMessage = ex.Message
        Finally
        End Try
        Return strMessage
    End Function

    Private Shared Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try
            Const bufSize As Integer = (5859 * 1024)
            Dim buf(bufSize - 1) As Byte
            Dim bytesRead As Integer = 0

            bytesRead = source.Read(buf, 0, bufSize)
            Do While bytesRead > 0
                target.Write(buf, 0, bytesRead)
                bytesRead = source.Read(buf, 0, bufSize)
            Loop

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            Throw New Exception("CopyStream :- " & ex.Message)
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END



    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Public Function CalculateNightHrs(ByVal xShiftID As Integer, ByVal mdtStarttime As DateTime, ByVal mdtEndtime As DateTime) As Integer
        Dim mintTotalNighthrs As Integer = 0
        Try

            Dim objshift As New clsshift_tran
            objshift.GetShiftTran(xShiftID)

            Dim mdtNightFrmHrs As DateTime = objshift._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(mdtStarttime.Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("nightfromhrs")).Distinct().First()
            Dim mdtNightToHrs As DateTime = objshift._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(mdtStarttime.Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("nighttohrs")).Distinct().First()

            If mdtStarttime.Date <> Nothing AndAlso (mdtNightFrmHrs <> Nothing AndAlso mdtNightToHrs <> Nothing) Then

                If DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtNightToHrs.AddDays(1)) > 0 AndAlso mdtNightFrmHrs <> mdtNightToHrs Then

                    mdtNightFrmHrs = CDate(mdtStarttime.Date & " " & mdtNightFrmHrs.ToShortTimeString())
                    mdtNightToHrs = CDate(mdtEndtime.Date & " " & mdtNightToHrs.ToShortTimeString())

                    If mdtNightToHrs < mdtNightFrmHrs Then
                        mdtNightToHrs = CDate(mdtStarttime.Date.AddDays(1) & " " & mdtNightToHrs.ToShortTimeString())
                    End If

                    If mdtStarttime <> Nothing AndAlso mdtEndtime <> Nothing Then
                        If mdtStarttime <= mdtNightFrmHrs AndAlso mdtEndtime >= mdtNightToHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtNightToHrs))

                        ElseIf mdtStarttime <= mdtNightFrmHrs AndAlso mdtEndtime <= mdtNightToHrs AndAlso mdtEndtime >= mdtNightFrmHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtEndtime))

                        ElseIf mdtStarttime >= mdtNightFrmHrs AndAlso mdtEndtime <= mdtNightToHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtStarttime, mdtEndtime))

                        ElseIf mdtStarttime >= mdtNightFrmHrs AndAlso mdtEndtime >= mdtNightToHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtStarttime, mdtNightToHrs))
                        End If

                    End If   '  If mdtStarttime <> Nothing AndAlso mdtEndtime <> Nothing Then

                End If  ' If DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtNightToHrs.AddDays(1)) > 0 AndAlso mdtNightFrmHrs <> mdtNightToHrs Then

            End If  ' If mdtStarttime.Date <> Nothing AndAlso (mdtNightFrmHrs <> Nothing AndAlso mdtNightToHrs <> Nothing) Then


            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            If mintTotalNighthrs < 0 Then mintTotalNighthrs = 0
            'Pinkal (08-Jan-2020) -- End

        Catch ex As Exception
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (01 Feb 2020) -- End
        End Try
        Return mintTotalNighthrs
    End Function

    'Pinkal (24-Oct-2019) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    <WebMethod()> Public Shared Function ConvertToCurrency(ByVal strNumber As String) As String
        Try
            Dim dec As Decimal
            Decimal.TryParse(strNumber, dec)

            Return Format(dec, "##,##,##,##0.00")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    <WebMethod()> Public Shared Function ConvertToFmtCurrency(ByVal strNumber As String) As String
        Try
            Dim dec As Decimal
            Decimal.TryParse(strNumber, dec)

            Dim strFormat As String = "##,##,##,##0.00"
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Session("fmtCurrency") IsNot Nothing Then
                strFormat = CStr(HttpContext.Current.Session("fmtCurrency"))
            End If

            Return Format(dec, strFormat)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Sohail (14 Nov 2019) -- End

    'S.SANDEEP |17-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
    Public Shared Sub KillIdleSQLSessions()
        Try
            'Using objCONN As New SqlClient.SqlConnection
            '    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
            '    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
            '    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
            '    objCONN.ConnectionString = constr
            '    objCONN.Open()

            '    Dim oSQL As String = ""

            '    oSQL = "DECLARE @user_spid INT " & _
            '           "DECLARE CurSPID CURSOR FAST_FORWARD " & _
            '           "FOR " & _
            '           "    SELECT SPID FROM master.dbo.sysprocesses(NOLOCK) WHERE spid > 50 AND STATUS = 'sleeping' AND DATEDIFF(MINUTE, last_batch, GETDATE()) >= 30 AND spid <> @@spid " & _
            '           "OPEN CurSPID " & _
            '           "FETCH NEXT " & _
            '           "FROM CurSPID " & _
            '           "INTO @user_spid " & _
            '           "WHILE (@@FETCH_STATUS = 0) " & _
            '           "    BEGIN " & _
            '           "        EXEC ('KILL ' + @user_spid) " & _
            '           "        FETCH NEXT " & _
            '           "        FROM CurSPID " & _
            '           "        INTO @user_spid " & _
            '           "    END " & _
            '           "CLOSE CurSPID " & _
            '           "DEALLOCATE CurSPID "

            '    Using oCmd As New SqlClient.SqlCommand
            '        oCmd.CommandText = oSQL
            '        oCmd.CommandType = CommandType.Text
            '        oCmd.Connection = objCONN
            '        oCmd.ExecuteNonQuery()
            '    End Using

            '    objCONN.Close()
            '    SqlClient.SqlConnection.ClearPool(objCONN)

            'End Using
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Throw New Exception(ex.Message)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Sub
    'S.SANDEEP |17-MAR-2020| -- END


    'Gajanan [29-July-2020] -- Start
    'Enhancement: Get Hostanme from Database 
    Public Function GetHostNameFromIp(ByVal mstrIp As String) As String
        Try
            Dim exists As Boolean = False
            Dim mstrHostAddress As String = ""

            Using objCONN As New SqlClient.SqlConnection
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN.ConnectionString = constr
                objCONN.Open()

                Dim oSQL As String = ""

                oSQL = "SELECT CASE WHEN OBJECT_ID('hrmsConfiguration.dbo.cfdnslookup', 'U') IS NOT NULL THEN 1 ELSE 0 END "
                Using oCmd As New SqlClient.SqlCommand
                    oCmd.CommandText = oSQL
                    oCmd.CommandType = CommandType.Text
                    oCmd.Connection = objCONN
                    exists = CBool(oCmd.ExecuteScalar() = 1)
                End Using

                If exists Then
                    oSQL = "Select machine_name from hrmsConfiguration..cfdnslookup where ip_address = @ip_address "

                    Using oCmd As New SqlClient.SqlCommand
                        oCmd.CommandText = oSQL
                        oCmd.Parameters.AddWithValue("@ip_address", mstrIp)
                        oCmd.CommandType = CommandType.Text
                        oCmd.Connection = objCONN
                        If IsNothing(oCmd.ExecuteScalar()) = False Then
                            mstrHostAddress = oCmd.ExecuteScalar().ToString()
                        End If
                    End Using
                End If
                objCONN.Close()
                SqlClient.SqlConnection.ClearPool(objCONN)
            End Using
            Return mstrHostAddress
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Gajanan [29-July-2020] -- End


    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    <WebMethod()> Public Shared Function InsertErrorLog(ByVal mstrMethodName As String, ByVal ex As Exception) As Boolean
        Dim objErrorLog As New clsErrorlog_Tran
        With objErrorLog
            Dim S_dispmsg As String = "Method : " & mstrMethodName & ", Error :  " & ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                S_dispmsg &= "; " & ex.InnerException.Message
            End If
            S_dispmsg = S_dispmsg.Replace("'", "")
            S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
            ._Error_Message = S_dispmsg
            ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
            ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
            ._Companyunkid = HttpContext.Current.Session("Companyunkid")
            If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                ._Database_Version = ""
            Else
                ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
            End If
            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                ._Userunkid = HttpContext.Current.Session("UserId")
                ._Loginemployeeunkid = 0
            Else
                ._Userunkid = 0
                ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
            End If
            ._Isemailsent = False
            ._Isweb = True
            ._Isvoid = False
            ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
            ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
        End With
        Dim strDBName As String = ""
        If HttpContext.Current.Session("mdbname") IsNot Nothing Then
            If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                strDBName = "hrmsConfiguration"
            End If
        End If
        If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then
            Return False
        End If
        Return True
    End Function
    'Pinkal (13-Aug-2020) -- End

    'Sohail (06 Nov 2020) -- Start
    'NMB Enhancement # : - Make navigation path clickable to navigate on respective page on each mandatory filds message on search job page in ESS.
    <WebMethod()> Public Shared Sub SetRecruitmentAttachmentSession()
        Try
            HttpContext.Current.Session("ScanAttchLableCaption") = "Select Employee"
            HttpContext.Current.Session("ScanAttchRefModuleID") = enImg_Email_RefId.Applicant_Job_Vacancy
            HttpContext.Current.Session("ScanAttchenAction") = enAction.ADD_ONE
            HttpContext.Current.Session("ScanAttchToEditIDs") = ""
        Catch ex As Exception
            InsertErrorLog("SetRecruitmentAttachmentSession", ex)
        End Try
    End Sub
    'Sohail (06 Nov 2020) -- End


    'Pinkal (23-Jul-2021)-- Start
    'Dashboard Performance Issue.

    Public Shared Function SetWebLanguage(ByVal mstrDatabaseName As String, ByVal mstrWebFormName As String, ByVal mintLanguageId As Integer, ByVal mstrControlId As String, ByVal mstrControlText As String) As String
        Dim mstrLanguage As String = ""
        Try
            Using clsDataOpr As New clsDataOperation(True)
                mstrLanguage = clsDataOpr.GetLanguages(mstrDatabaseName, mstrWebFormName, mintLanguageId, mstrControlId, mstrControlText)
            End Using


            'Using clsDataOpr As New clsDataOperation(True)

            '    'Dim strQ As String = " SELECT language " & _
            '    '                               ", language1 " & _
            '    '                               ", language2 " & _
            '    '                               "  FROM " & mstrDatabaseName & "..cflanguage WHERE  formname = @formname AND controlname = @controlname "

            '    Dim strQ As String = " SELECT CASE WHEN @LanguageId = 1 THEN language1 " & _
            '                                   "   WHEN @LanguageId = 2 THEN language2 " & _
            '                                   " ELSE language END AS deflanguage " & _
            '                                   "  FROM " & mstrDatabaseName & "..cflanguage WITH (NOLOCK) WHERE  formname = @formname AND controlname = @controlname "

            '    clsDataOpr.ClearParameters()
            '    clsDataOpr.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            '    clsDataOpr.AddParameter("@controlname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlId)
            '    clsDataOpr.AddParameter("@LanguageId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageId)

            '    Dim dsList As DataSet = clsDataOpr.WExecQuery(strQ, "Language")
            '    'Hemant (23 Sep 2021) -- Start
            '    'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            '    If clsDataOpr.ErrorMessage <> "" Then
            '        Throw New Exception(clsDataOpr.ErrorMessage)
            '    End If
            '    'Hemant (23 Sep 2021) -- End
            '    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '        'If mintLanguageId = 1 Then
            '        '    mstrLanguage = dsList.Tables(0).Rows(0)("language1").ToString()
            '        'ElseIf mintLanguageId = 2 Then
            '        '    mstrLanguage = dsList.Tables(0).Rows(0)("language2").ToString()
            '        'Else
            '        '    mstrLanguage = dsList.Tables(0).Rows(0)("language").ToString()
            '        'End If
            '        mstrLanguage = dsList.Tables(0).Rows(0)("deflanguage").ToString()
                    'End If

            '    If mstrLanguage.Trim.Length <= 0 Then mstrLanguage = mstrControlText

            'End Using
        Catch ex As Exception
            InsertErrorLog("SetWebLanguage", ex)
        End Try
        Return mstrLanguage
    End Function

    Public Shared Sub SetWebCaption(ByVal mstrDatabaseName As String, ByVal mstrWebFormName As String, ByVal mstrControlId As String, ByVal mstrControlText As String)
        Try
            Using clsDataOpr As New clsDataOperation(True)
                Dim strQ As String = " INSERT INTO " & mstrDatabaseName & "..cflanguage " & _
                                               " ( " & _
                                               "  formname " & _
                                               ", controlname " & _
                                               ", language " & _
                                               ", language1 " & _
                                               ", language2 " & _
                                               " ) " & _
                                               " SELECT " & _
                                               "  @formname " & _
                                               ", @controlname " & _
                                               ", @language " & _
                                               ", @language1 " & _
                                               ", @language2 " & _
                                               " WHERE NOT EXISTS " & _
                                               " ( " & _
                                               "   SELECT * FROM " & mstrDatabaseName & "..cflanguage WHERE formname = @formname AND controlname = @controlname " & _
                                               " ); "

                clsDataOpr.ClearParameters()
                clsDataOpr.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
                clsDataOpr.AddParameter("@controlname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlId)
                clsDataOpr.AddParameter("@language", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlText)
                clsDataOpr.AddParameter("@language1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlText)
                clsDataOpr.AddParameter("@language2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlText)
                clsDataOpr.WExecQuery(strQ, "Language")
                'Hemant (23 Sep 2021) -- Start
                'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
                If clsDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(clsDataOpr.ErrorMessage)
                End If
                'Hemant (23 Sep 2021) -- End
            End Using
        Catch ex As Exception
            InsertErrorLog("SetWebCaption", ex)
        End Try
    End Sub

    Public Shared Function GetWebMessage(ByVal mstrDatabaseName As String, ByVal mintLanguageId As Integer, ByVal mstrWebFormName As String, ByVal mintMsgCode As Integer, ByVal mstrMessage As String) As String
        Dim mstrReturnMessage As String = ""
        Try
            Using clsDataOpr As New clsDataOperation(True)
                mstrReturnMessage = clsDataOpr.GetMessages(mstrDatabaseName, mintLanguageId, mstrWebFormName, mintMsgCode, mstrMessage)
            End Using

            'Using clsDataOpr As New clsDataOperation(True)

            '    'Dim strQ As String = " SELECT message " & _
            '    '                               ", message1 " & _
            '    '                               ", message2 " & _
            '    '                              "  FROM " & mstrDatabaseName & "..cfmessages WHERE  module_name = @module_name AND messagecode = @messagecode AND message = @message "

            '    Dim strQ As String = " SELECT CASE WHEN @LanguageId = 1 THEN message1 " & _
            '                               "       WHEN @LanguageId = 2 THEN message2 " & _
            '                               " ELSE message END AS defmessage " & _
            '                               "  FROM " & mstrDatabaseName & "..cfmessages WITH (NOLOCK) WHERE  module_name = @module_name AND messagecode = @messagecode AND message = @message "

            '    clsDataOpr.ClearParameters()
            '    clsDataOpr.AddParameter("@module_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrWebFormName)
            '    clsDataOpr.AddParameter("@messagecode", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mintMsgCode.ToString())
            '    clsDataOpr.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMessage)
            '    clsDataOpr.AddParameter("@LanguageId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageId)


            '    Dim dsList As DataSet = clsDataOpr.WExecQuery(strQ, "Message")
            '    'Hemant (23 Sep 2021) -- Start
            '    'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            '    If clsDataOpr.ErrorMessage <> "" Then
            '        Throw New Exception(clsDataOpr.ErrorMessage)
            '    End If
            '    'Hemant (23 Sep 2021) -- End
            '    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '        'If mintLanguageId = 1 Then
            '        '    mstrReturnMessage = dsList.Tables(0).Rows(0)("message1").ToString()
            '        'ElseIf mintLanguageId = 2 Then
            '        '    mstrReturnMessage = dsList.Tables(0).Rows(0)("message2").ToString()
            '        'Else
            '        '    mstrReturnMessage = dsList.Tables(0).Rows(0)("message").ToString()
            '        'End If
            '        mstrReturnMessage = dsList.Tables(0).Rows(0)("defmessage").ToString()
                    'End If

            '    If mstrReturnMessage.Trim.Length <= 0 Then mstrReturnMessage = mstrMessage
            'End Using
        Catch ex As Exception
            InsertErrorLog("GetWebMessage", ex)
        End Try
        Return mstrReturnMessage
    End Function

    Public Shared Sub SetWebMessage(ByVal mstrDatabaseName As String, ByVal mstrWebFormName As String, ByVal mintMsgCode As Integer, ByVal mstrMessage As String)
        Try
            Using clsDataOpr As New clsDataOperation(True)
                Dim strQ As String = " INSERT INTO " & mstrDatabaseName & "..cfmessages " & _
                                               " ( " & _
                                               "  messagecode " & _
                                               ", module_name " & _
                                               ", message " & _
                                               ", message1 " & _
                                               ", message2 " & _
                                               " ) " & _
                                               " SELECT " & _
                                               "  @messagecode " & _
                                               ", @module_name " & _
                                               ", @message " & _
                                               ", @message1 " & _
                                               ", @message2 " & _
                                               " WHERE NOT EXISTS " & _
                                               " ( " & _
                                               "   SELECT * FROM " & mstrDatabaseName & "..cfmessages WHERE module_name = @module_name AND messagecode = @messagecode AND message = @message " & _
                                               " ); "

                clsDataOpr.ClearParameters()
                clsDataOpr.AddParameter("@module_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrWebFormName)
                clsDataOpr.AddParameter("@messagecode", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mintMsgCode.ToString())
                clsDataOpr.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMessage)
                clsDataOpr.AddParameter("@message1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMessage)
                clsDataOpr.AddParameter("@message2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMessage)
                clsDataOpr.WExecQuery(strQ, "Message")
                'Hemant (23 Sep 2021) -- Start
                'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
                If clsDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(clsDataOpr.ErrorMessage)
                End If
                'Hemant (23 Sep 2021) -- End
            End Using
        Catch ex As Exception
            InsertErrorLog("SetWebMessage", ex)
        End Try
    End Sub
    'Pinkal (23-Jul-2021) -- End

    'S.SANDEEP |04-AUG-2021| -- START
    Public Sub SetIP_HOST()
        Try
            If HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            Else
                HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            End If

            Dim mstrhost As String = ""
            mstrhost = GetHostNameFromIp(HttpContext.Current.Session("IP_ADD"))
            If mstrhost.Trim().Length > 0 Then
                HttpContext.Current.Session("HOST_NAME") = mstrhost
            End If
        Catch ex As System.Net.Sockets.SocketException
            'CommonCodes.LogErrorOnly(ex)
            HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
        Catch ex As Exception
            'CommonCodes.LogErrorOnly(ex)
            HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
        End Try
    End Sub
    'S.SANDEEP |04-AUG-2021| -- END


    'S.SANDEEP |18-SEP-2021| -- START
    Public Shared Function RemoveRTFFormatting(ByVal rtfContent As String) As String
        rtfContent = rtfContent.Trim()
        Dim rtfRegEx As Regex = New Regex("({\\)(.+?)(})|(\\)(.+?)(\b)", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.ExplicitCapture Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Compiled)
        Dim output As String = rtfRegEx.Replace(rtfContent, String.Empty)
        output = Regex.Replace(output, "\}", String.Empty)
        'Return output.Remove(output.Length - 1)
        Return output.Trim
    End Function
    'S.SANDEEP |18-SEP-2021| -- END


    'S.SANDEEP |19-JAN-2022| -- START
    'ISSUE : STOPPING IE
    Public Shared Function IsOldIE() As Boolean
        Try
            Dim bAgent As String = HttpContext.Current.Request.UserAgent.ToString()
            Dim blnIsIE As Boolean = False
            If bAgent.ToUpper.Contains("CHROME") Then
                blnIsIE = False
            ElseIf bAgent.ToUpper.Contains("FIREFOX") Then
                blnIsIE = False
            ElseIf bAgent.ToUpper.Contains("SAFARI") Then
                blnIsIE = False
            ElseIf bAgent.ToUpper.Contains("EDG") Then
                blnIsIE = False
            Else
                blnIsIE = True
            End If
            Return blnIsIE
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Function
    'S.SANDEEP |19-JAN-2022| -- END


    'Pinkal (25-Jan-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    Public Shared Function GetCurrentDateTime() As DateTime
        Dim dtDate As DateTime = Now
        Try
            Dim objConfig As New clsConfigOptions
            dtDate = objConfig._CurrentDateAndTime
            objConfig = Nothing
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
        Return dtDate
    End Function

    'Pinkal (25-Jan-2022) -- End



    'Pinkal (01-Feb-2022) -- Start
    'Enhancement NMB  - Language Change Issue for All Modules.	
    Public Function GetLanguageData(ByVal mstrDatabaseName As String, ByVal lstFormName As List(Of String), ByVal mintLanguageId As Integer) As DataTable
        Dim dtLanguage As DataTable = Nothing
        Try

            If lstFormName Is Nothing OrElse lstFormName.Count <= 0 Then Return dtLanguage

            Dim mstrFormName As String = "'" & String.Join("','", lstFormName.ToArray()) & "'"

            Using clsDataOperation As New eZeeCommonLib.clsDataOperation(True)

                Dim strQ As String = " SELECT formname,controlname, " & _
                                               " CASE WHEN @LanguageId = 0 THEN language " & _
                                               "          WHEN @LanguageId = 1 THEN isnull(language1,language) " & _
                                               "          WHEN @LanguageId = 2 THEN isnull(language2,language) end as language " & _
                                               " FROM " & mstrDatabaseName & "..cflanguage " & _
                                               " WHERE formname in (" & mstrFormName & ")"

                clsDataOperation.ClearParameters()
                clsDataOperation.AddParameter("@LanguageId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageId)
                Dim dsList As DataSet = clsDataOperation.WExecQuery(strQ, "Language")

                If clsDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(clsDataOperation.ErrorMessage)
                End If

                If dsList IsNot Nothing Then dtLanguage = dsList.Tables(0)

            End Using

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
        Return dtLanguage
    End Function
    'Pinkal (01-Feb-2022) -- End

    'S.SANDEEP |03-SEP-2022| -- START
    'ISSUE/ENHANCEMENT : Sprint_2022-13
    Public Shared Function SubmitForApproval(ByVal blnIsFinal As Boolean, _
                                             ByVal intLastStatusId As Integer, _
                                             ByVal intEmplId As Integer, _
                                             ByVal intPerdId As Integer, _
                                             ByVal dtCmpts As DataTable, _
                                             ByVal mstrModuleName As String, _
                                             ByVal wPg As Page) As String
        Dim sMsg As String = ""
        Dim objAssessor As New clsAssessor_tran
        Dim objFMapping As New clsAssess_Field_Mapping
        Dim objperiod As New clscommom_period_Tran
        Dim objEmpField1 As New clsassess_empfield1_master
        Dim objEStatusTran As New clsassess_empstatus_tran
        Dim DisplayMessage As New CommonCodes
        Try
            If blnIsFinal Then
                sMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period.")
                Exit Try
            ElseIf intLastStatusId = enObjective_Status.SUBMIT_APPROVAL Then
                sMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period.")
                Exit Try
            End If

            sMsg = objAssessor.IsAssessorPresent(intEmplId)
            If sMsg <> "" Then
                Exit Try
            End If

            sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, intPerdId, 0, intEmplId)
            If sMsg <> "" Then
                Exit Try
            End If

            If CBool(HttpContext.Current.Session("Self_Assign_Competencies")) = True Then
                If dtCmpts.Rows.Count <= 0 Then
                    sMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Submission for Approval Failed, Reason: Competencies are not set for this employee for the selected period.")
                    Exit Try
                End If
            End If

            If CBool(HttpContext.Current.Session("Self_Assign_Competencies")) = True Then
                If dtCmpts IsNot Nothing AndAlso dtCmpts.Rows.Count > 0 Then
                    Dim xGrpWeight As Decimal = 0 : Dim xGrpId As Integer = -1
                    For Each xRow As DataRow In dtCmpts.Select("isGrp = False")
                        If xGrpId <> xRow.Item("assessgroupunkid") Then
                            Dim objAGroup As New clsassess_group_master
                            objAGroup._Assessgroupunkid = xRow.Item("assessgroupunkid")
                            xGrpWeight = xGrpWeight + objAGroup._Weight
                            objAGroup = Nothing
                            xGrpId = xRow.Item("assessgroupunkid")
                        End If
                    Next
                    Dim dblwgt As Double = 0
                    dblwgt = dtCmpts.Compute("SUM(assigned_weight)", "")
                    dblwgt = Format(dblwgt, "###################0.#0")
                    If CDbl(dblwgt) <> xGrpWeight Then
                        sMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, you cannot Submit this Performance Planning for Approval, Reason Competencies are not adding upto assessment group weight.")
                        Exit Try
                    End If
                End If
            End If

            Dim objCMaster As New clsassess_custom_items
            sMsg = objCMaster.CheckPlannedItem(intPerdId, intEmplId)
            If sMsg.Trim.Length > 0 Then
                Exit Try
            End If

            objEStatusTran._Assessoremployeeunkid = 0
            objEStatusTran._Assessormasterunkid = 0
            objEStatusTran._Commtents = ""
            objEStatusTran._Employeeunkid = intEmplId
            objEStatusTran._Isunlock = False
            objEStatusTran._Loginemployeeunkid = 0
            objEStatusTran._Periodunkid = intPerdId
            objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objEStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
            objEStatusTran._Userunkid = HttpContext.Current.Session("UserId")

            If objEStatusTran.Insert() = False Then
                sMsg = objEStatusTran._Message
                Exit Try
            End If
            If CStr(HttpContext.Current.Session("ArutiSelfServiceURL")).Trim.Length > 0 Then
                objperiod._Periodunkid(HttpContext.Current.Session("Database_Name")) = intPerdId
                objEmpField1.Send_Notification_Assessor(intEmplId, _
                                                        intPerdId, _
                                                        objperiod._Yearunkid, _
                                                        HttpContext.Current.Session("FinancialYear_Name"), _
                                                        CStr(HttpContext.Current.Session("Database_Name")), _
                                                        HttpContext.Current.Session("CompanyUnkId"), _
                                                        HttpContext.Current.Session("ArutiSelfServiceURL"), _
                                                        enLogin_Mode.EMP_SELF_SERVICE, 0, 0)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, wPg)
        Finally
            objAssessor = Nothing : objFMapping = Nothing : objperiod = Nothing : objEmpField1 = Nothing : objEStatusTran = Nothing
            DisplayMessage = Nothing
        End Try
        Return sMsg
    End Function
    'S.SANDEEP |03-SEP-2022| -- END


    'Hemant (27 Oct 2022) -- Start
    <WebMethod()> Public Shared Sub SetIsFlexcubeApplication(ByVal mblnIsFlexcubeApplication As Boolean)
        HttpContext.Current.Session("mblnIsFlexcubeApplication") = mblnIsFlexcubeApplication
    End Sub
    'Hemant (27 Oct 2022) -- End

    'Hemant (03 Mar 2023) -- Start
    'ISSUE/EHANCEMENT(NMB) : A1X-665 - As a user, on MSS, under scorecard, i want to have all operations related to goal approval, unlock scorecard, Approve performance progress, perfomance asssment and performance result under one screen as stand alone buttons
    Public Function AssessEmployeeValidation(ByRef strMessage As String, ByVal intEmployeeunkid As Integer, ByVal intPeriodId As Integer, ByVal eMode As enAssessmentMode) As Boolean
        Dim objEvaluation As New clsevaluation_analysis_master
        Dim objPeriod As New clscommom_period_Tran
        Dim intAnalysisUnkid As Integer = -1
        Dim intSL_AnalysisUnkid As Integer = -1
        Dim blnIsCommitted As Boolean = False
        Dim blnIsSLCommitted As Boolean = False
        Try
            Dim sMsg As String = String.Empty

            sMsg = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), intEmployeeunkid, intPeriodId, Session("Self_Assign_Competencies"), eMode, True)


            If sMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Return False
            End If

            objPeriod._Periodunkid(HttpContext.Current.Session("Database_Name")) = intPeriodId
            If objPeriod._Statusid = StatusType.Close Then
                strMessage = "Sorry, you cannot assess this employee. Reason : Period is closed."
                Return False
            End If

            intAnalysisUnkid = objEvaluation.GetAnalusisUnkid(intEmployeeunkid, CInt(eMode), intPeriodId, clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD)
            objEvaluation._Analysisunkid = intAnalysisUnkid
            blnIsCommitted = objEvaluation._Iscommitted
            If blnIsCommitted = True Then
                strMessage = "Sorry, you cannot do assess operation. Reason : Assessment is committed."
                Return False
            End If

            If Session("AllowAssessor_Before_Emp") = False Then
                intSL_AnalysisUnkid = objEvaluation.GetAnalusisUnkid(intEmployeeunkid, CInt(enAssessmentMode.SELF_ASSESSMENT), intPeriodId, clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD)
                objEvaluation._Analysisunkid = intSL_AnalysisUnkid
                blnIsSLCommitted = objEvaluation._Iscommitted
                If intSL_AnalysisUnkid <= 0 OrElse (intSL_AnalysisUnkid > 0 AndAlso blnIsSLCommitted = False) Then

                    strMessage = "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                 "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."
                    Return False
                End If

                If eMode = CInt(enAssessmentMode.REVIEWER_ASSESSMENT) Then
                    Dim intAL_AnalysisUnkid As Integer = -1
                    Dim blnIsALCommitted As Boolean = False
                    intAL_AnalysisUnkid = objEvaluation.GetAnalusisUnkid(intEmployeeunkid, CInt(enAssessmentMode.APPRAISER_ASSESSMENT), intPeriodId, clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD)
                    objEvaluation._Analysisunkid = intAL_AnalysisUnkid
                    blnIsALCommitted = objEvaluation._Iscommitted
                    If intAL_AnalysisUnkid <= 0 OrElse (intAL_AnalysisUnkid > 0 AndAlso blnIsALCommitted = False) Then

                        strMessage = "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                     "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."
                        Return False
                    End If
                End If
            End If

            If intAnalysisUnkid > 0 Then
                strMessage = "You have already done the evaluation for this employee for the selected period"
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            objEvaluation = Nothing
            objPeriod = Nothing
        End Try

    End Function
    'Hemant (03 Mar 2023) -- End  

    'Pinkal (23-Feb-2024) -- Start
    '(A1X-2461) NMB : R&D - Force manual user login on approval links.
    Public Sub GenerateAuthentication()
        Try

            Dim strguid As String = Guid.NewGuid().ToString()

            HttpContext.Current.Session("AuthToken") = Nothing
            HttpContext.Current.Session("AuthToken") = strguid

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            If HttpContext.Current.Response.Cookies IsNot Nothing Then
                HttpContext.Current.Response.Cookies("AuthToken").Value = ""
                HttpContext.Current.Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                HttpContext.Current.Response.Cookies.Remove("AuthToken")
            End If
            'Pinkal (27-Sep-2024) -- End

            'Pinkal (24-Oct-2024) -- Start
            '(A1X-2825) - TADB - Notify specific users when a Flexcube loan is final approved.
            If HttpContext.Current.Request.Cookies IsNot Nothing Then
                HttpContext.Current.Request.Cookies.Clear()
            End If
            'Pinkal (24-Oct-2024) -- End

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            Dim objCookie As New HttpCookie("AuthToken", strguid)

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            If HttpContext.Current.Session("CompanyGroupName") Is Nothing Then
                Dim objGroupMaster As New clsGroup_Master
                objGroupMaster._Groupunkid = 1
                HttpContext.Current.Session("CompanyGroupName") = objGroupMaster._Groupname.ToUpper
                objGroupMaster = Nothing
            End If
            'Pinkal (27-Sep-2024) -- End

            If HttpContext.Current.Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
               objCookie.Secure = True
               objCookie.HttpOnly = True
            End If

            objCookie.Domain = HttpContext.Current.Request.Url.Host
            objCookie.Expires = Now.AddHours(2)
            objCookie.Path = "/"
            HttpContext.Current.Response.Cookies.Add(objCookie)
            'Pinkal (24-Jun-2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (23-Feb-2024) -- End

    Public Function CookieValidation() As Boolean
        Dim mblnflag As Boolean = True
        Try
            Dim MyCookie As HttpCookie = Nothing
            Dim MyCookieCollection As HttpCookieCollection = Request.Cookies
            If MyCookieCollection Is Nothing OrElse MyCookieCollection.Count <= 0 Then
                'ViewState.Clear()
                'DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                Context.ApplicationInstance.CompleteRequest()
                mblnflag = False
                Return mblnflag
            ElseIf MyCookieCollection("AuthToken") Is Nothing OrElse MyCookieCollection("AuthToken").Value = "" Then
                Context.ApplicationInstance.CompleteRequest()
                mblnflag = False
                Return mblnflag
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnflag = False
        End Try
        Return mblnflag
    End Function



    Public Sub New()

    End Sub

    
    Public Sub GetUserPrivilegeDict()
        Dim privilegeDict As Dictionary(Of String, Integer) = Nothing
        Try
            If Session("privilegeDict") Is Nothing Then

                privilegeDict = New Dictionary(Of String, Integer)

                privilegeDict.Add("_AccessConfiguaration", 1)
                privilegeDict.Add("_ChangeLanguage", 2)
                privilegeDict.Add("_CompanyCreation", 3)
                privilegeDict.Add("_DatabaseBackup", 6)
                privilegeDict.Add("_DatabaseRestore", 7)
                privilegeDict.Add("_AddCommonMasters", 8)
                privilegeDict.Add("_EditCommonMaster", 9)
                privilegeDict.Add("_DeleteCommonMaster", 10)
                privilegeDict.Add("_AddTerminationReason", 11)
                privilegeDict.Add("_EditTerminationReason", 12)
                privilegeDict.Add("_DeleteTerminationReason", 13)
                privilegeDict.Add("_AddSkills", 14)
                privilegeDict.Add("_EditSkills", 15)
                privilegeDict.Add("_DeleteSkills", 16)
                privilegeDict.Add("_AddAdvertise", 17)
                privilegeDict.Add("_EditAdvertise", 18)
                privilegeDict.Add("_DeleteAdvertise", 19)
                privilegeDict.Add("_AddQualification_Course", 20)
                privilegeDict.Add("_EditQualification_Course", 21)
                privilegeDict.Add("_DeleteQualification_Course", 22)
                privilegeDict.Add("_AddResultCode", 23)
                privilegeDict.Add("_EditResultCode", 24)
                privilegeDict.Add("_DeleteResultCode", 25)
                privilegeDict.Add("_AddMembership", 26)
                privilegeDict.Add("_EditMembership", 27)
                privilegeDict.Add("_DeleteMembership", 28)
                privilegeDict.Add("_AddBenefitPlan", 29)
                privilegeDict.Add("_EditBenefitPlan", 30)
                privilegeDict.Add("_DeleteBenefitPlan", 31)
                privilegeDict.Add("_AllowToEditCurrencySign", enUserPriviledge.AllowToEditCurrencySign)
                privilegeDict.Add("_AddState", 32)
                privilegeDict.Add("_EditState", 33)
                privilegeDict.Add("_DeleteState", 34)
                privilegeDict.Add("_AddCity", 35)
                privilegeDict.Add("_EditCity", 36)
                privilegeDict.Add("_DeleteCity", 37)
                privilegeDict.Add("_AddZipCode", 38)
                privilegeDict.Add("_EditZipCode", 39)
                privilegeDict.Add("_DeleteZipCode", 40)
                privilegeDict.Add("_AddEmployee", 41)
                privilegeDict.Add("_EditEmployee", 42)
                privilegeDict.Add("_DeleteEmployee", 43)
                privilegeDict.Add("_AddDependant", 44)
                privilegeDict.Add("_EditDependant", 45)
                privilegeDict.Add("_DeleteDependant", 46)
                privilegeDict.Add("_AddBeneficiaries", 47)
                privilegeDict.Add("_EditBeneficiaries", 48)
                privilegeDict.Add("_DeleteBeneficiaries", 49)
                privilegeDict.Add("_AddBenefitAllocation", 50)
                privilegeDict.Add("_EditBenefitAllocation", 51)
                privilegeDict.Add("_DeleteBenefitAllocation", 52)
                privilegeDict.Add("_AllowToSetBenefitAllocationActive", enUserPriviledge.AllowToSetBenefitAllocationActive)
                privilegeDict.Add("_AllowToSetBenefitAllocationInactive", enUserPriviledge.AllowToSetBenefitAllocationInactive)
                privilegeDict.Add("_AllowToChangeEOCLeavingDateOnClosedPeriod", enUserPriviledge.AllowToChangeEOCLeavingDateOnClosedPeriod)
                privilegeDict.Add("_AllowToPostFlexcubeJVToOracle", enUserPriviledge.AllowToPostFlexcubeJVToOracle)
                privilegeDict.Add("_AllowToViewPaidAmount", enUserPriviledge.AllowToViewPaidAmount)
                privilegeDict.Add("_AllowToChangeAppointmentDateOnClosedPeriod", enUserPriviledge.AllowToChangeAppointmentDateOnClosedPeriod)
                privilegeDict.Add("_AllowToApproveRejectEmployeeExemption", enUserPriviledge.AllowToApproveRejectEmployeeExemption)
                privilegeDict.Add("_AllowToSetEmployeeExemptionDate", enUserPriviledge.AllowToSetEmployeeExemptionDate)
                privilegeDict.Add("_AllowToEditEmployeeExemptionDate", enUserPriviledge.AllowToEditEmployeeExemptionDate)
                privilegeDict.Add("_AllowToDeleteEmployeeExemptionDate", enUserPriviledge.AllowToDeleteEmployeeExemptionDate)
                privilegeDict.Add("_AllowToViewTrainingNeedForm", enUserPriviledge.AllowToViewTrainingNeedForm)
                privilegeDict.Add("_AllowToAddTrainingNeedForm", enUserPriviledge.AllowToAddTrainingNeedForm)
                privilegeDict.Add("_AllowToEditTrainingNeedForm", enUserPriviledge.AllowToEditTrainingNeedForm)
                privilegeDict.Add("_AllowToDeleteTrainingNeedForm", enUserPriviledge.AllowToDeleteTrainingNeedForm)
                privilegeDict.Add("_AllowToAssignBenefitGroup", enUserPriviledge.AllowToAssignBenefitGroup)
                privilegeDict.Add("_AddEmployeeBenefit", 54)
                privilegeDict.Add("_EditEmployeeBenefit", 55)
                privilegeDict.Add("_DeleteEmployeeBenefit", 56)
                privilegeDict.Add("_AddEmployeeReferee", 57)
                privilegeDict.Add("_EditEmployeeReferee", 58)
                privilegeDict.Add("_DeleteEmployeeReferee", 59)
                privilegeDict.Add("_AddEmployeeReferences", 60)
                privilegeDict.Add("_EditEmployeeReferences", 61)
                privilegeDict.Add("_DeleteEmployeeReferences", 62)
                privilegeDict.Add("_AddEmployeeAssets", 63)
                privilegeDict.Add("_EditEmployeeAssets", 64)
                privilegeDict.Add("_DeleteEmployeeAssets", 65)
                privilegeDict.Add("_AddEmployeeSkill", 66)
                privilegeDict.Add("_EditEmployeeSkill", 67)
                privilegeDict.Add("_DeleteEmployeeSkill", 68)
                privilegeDict.Add("_AddEmployeeQualification", 69)
                privilegeDict.Add("_EditEmployeeQualification", 70)
                privilegeDict.Add("_DeleteEmployeeQualification", 71)
                privilegeDict.Add("_AddEmployeeExperience", 72)
                privilegeDict.Add("_EditEmployeeExperience", 73)
                privilegeDict.Add("_DeleteEmployeeExperience", 74)
                privilegeDict.Add("_AddEmployeeDiscipline", 75)
                privilegeDict.Add("_EditEmployeeDiscipline", 76)
                privilegeDict.Add("_DeleteEmployeeDiscipline", 77)
                privilegeDict.Add("_AddDisciplinaryAction", 78)
                privilegeDict.Add("_EditDisciplinaryAction", 79)
                privilegeDict.Add("_DeleteDisciplinaryAction", 80)
                privilegeDict.Add("_AddDisciplineOffence", 81)
                privilegeDict.Add("_EditDisciplineOffence", 82)
                privilegeDict.Add("_DeleteDisciplineOffence", 83)
                privilegeDict.Add("_AddMedicalMasters", 93)
                privilegeDict.Add("_EditMedicalMasters", 94)
                privilegeDict.Add("_DeleteMedicalMasters", 95)
                privilegeDict.Add("_AddMedicalInjuries", 96)
                privilegeDict.Add("_EditMedicalInjuries", 97)
                privilegeDict.Add("_DeleteMedicalInjuries", 98)
                privilegeDict.Add("_AddMedicalInstitutes", 99)
                privilegeDict.Add("_EditMedicalInstitutes", 100)
                privilegeDict.Add("_DeleteMedicalInstitutes", 101)
                privilegeDict.Add("_AddAssignMedicalCategory", 102)
                privilegeDict.Add("_EditAssignMedicalCategory", 103)
                privilegeDict.Add("_DeleteAssignMedicalCategory", 104)
                privilegeDict.Add("_AddMedicalCover", 105)
                privilegeDict.Add("_EditMedicalCover", 106)
                privilegeDict.Add("_DeleteMedicalCover", 107)
                privilegeDict.Add("_AddMedicalClaim", 108)
                privilegeDict.Add("_EditMedicalClaim", 109)
                privilegeDict.Add("_DeleteMedicalClaim", 110)
                privilegeDict.Add("_AddPayrollGroup", 111)
                privilegeDict.Add("_EditPayrollGroup", 112)
                privilegeDict.Add("_DeletePayrollGroup", 113)
                privilegeDict.Add("_AddCostCenter", 114)
                privilegeDict.Add("_EditCostCenter", 115)
                privilegeDict.Add("_DeleteCostCenter", 116)
                privilegeDict.Add("_AddPayPoint", 117)
                privilegeDict.Add("_EditPayPoint", 118)
                privilegeDict.Add("_DeletePayPoint", 119)
                privilegeDict.Add("_AddPeriods", 120)
                privilegeDict.Add("_EditPeriods", 121)
                privilegeDict.Add("_DeletePeriods", 122)
                privilegeDict.Add("_AddVendor", 123)
                privilegeDict.Add("_EditVendor", 124)
                privilegeDict.Add("_DeleteVendor", 125)
                privilegeDict.Add("_AddBankBranch", 126)
                privilegeDict.Add("_EditBankBranch", 127)
                privilegeDict.Add("_DeleteBankBranch", 128)
                privilegeDict.Add("_AddBankAccType", 129)
                privilegeDict.Add("_EditBankAccType", 130)
                privilegeDict.Add("_DeleteBankAccType", 131)
                privilegeDict.Add("_AddBankEDI", 132)
                privilegeDict.Add("_EditBankEDI", 133)
                privilegeDict.Add("_DeleteBankEDI", 134)
                privilegeDict.Add("_AddCurrency", 135)
                privilegeDict.Add("_EditCurrency", 136)
                privilegeDict.Add("_DeleteCurrency", 137)
                privilegeDict.Add("_AddDenomination", 138)
                privilegeDict.Add("_EditDenomination", 139)
                privilegeDict.Add("_DeleteDenomination", 140)
                privilegeDict.Add("_AddEmployeeCostCenter", 141)
                privilegeDict.Add("_EditEmployeeCostCenter", 142)
                privilegeDict.Add("_DeleteEmployeeCostCenter", 143)
                privilegeDict.Add("_AddEmployeeBanks", 144)
                privilegeDict.Add("_EditEmployeeBanks", 145)
                privilegeDict.Add("_DeleteEmployeeBanks", 146)
                privilegeDict.Add("_AddBatchTransaction", 147)
                privilegeDict.Add("_EditBatchTransaction", 148)
                privilegeDict.Add("_DeleteBatchTransaction", 149)
                privilegeDict.Add("_AddTransactionHead", 150)
                privilegeDict.Add("_EditTransactionHead", 151)
                privilegeDict.Add("_DeleteTransactionHead", 152)
                privilegeDict.Add("_AddEarningDeduction", 153)
                privilegeDict.Add("_EditEarningDeduction", 154)
                privilegeDict.Add("_DeleteEarningDeduction", 155)
                privilegeDict.Add("_AllowBatchTransactionOnED", 156)
                privilegeDict.Add("_AllowPerformGlobalAssignOnED", 157)
                privilegeDict.Add("_AllowPerformEmployeeExemptionOnED", 158)
                privilegeDict.Add("_AddEmployeeExemption", 159)
                privilegeDict.Add("_EditEmployeeExemption", 160)
                privilegeDict.Add("_DeleteEmployeeExemption", 161)
                privilegeDict.Add("_AllowProcessPayroll", 162)
                privilegeDict.Add("_AllowViewPayslip", 163)
                privilegeDict.Add("_AllowPrintPayslip", 164)
                privilegeDict.Add("_AddPayment", 165)
                privilegeDict.Add("_EditPayment", 166)
                privilegeDict.Add("_DeletePayment", 167)
                privilegeDict.Add("_AllowClosePeriod", 168)
                privilegeDict.Add("_AllowCloseYear", 169)
                privilegeDict.Add("_AllowPrepareBudget", 170)
                privilegeDict.Add("_AddSalaryIncrement", 171)
                privilegeDict.Add("_EditSalaryIncrement", 172)
                privilegeDict.Add("_DeleteSalaryIncrement", 173)
                privilegeDict.Add("_AllowDefineWageTable", 174)
                privilegeDict.Add("_AddLeaveType", 175)
                privilegeDict.Add("_EditLeaveType", 176)
                privilegeDict.Add("_DeleteLeaveType", 177)
                privilegeDict.Add("_AddHolidays", 178)
                privilegeDict.Add("_EditHolidays", 179)
                privilegeDict.Add("_DeleteHolidays", 180)
                privilegeDict.Add("_AddLeaveApproverLevel", 181)
                privilegeDict.Add("_EditLeaveApproverLevel", 182)
                privilegeDict.Add("_DeleteLeaveApproverLevel", 183)
                privilegeDict.Add("_AddLeaveApprover", 184)
                privilegeDict.Add("_EditLeaveApprover", 185)
                privilegeDict.Add("_DeleteLeaveApprover", 186)
                privilegeDict.Add("_AddLeaveForm", 187)
                privilegeDict.Add("_EditLeaveForm", 188)
                privilegeDict.Add("_DeleteLeaveForm", 189)
                privilegeDict.Add("_AllowPrintLeaveForm", 190)
                privilegeDict.Add("_AllowPreviewLeaveForm", 191)
                privilegeDict.Add("_AllowEmailLeaveForm", 192)
                privilegeDict.Add("_AllowProcessLeaveForm", 193)
                privilegeDict.Add("_AllowIssueLeave", 194)
                privilegeDict.Add("_AllowLeaveAccrue", 195)
                privilegeDict.Add("_AddEmployeeHolidays", 196)
                privilegeDict.Add("_EditEmployeeHolidays", 197)
                privilegeDict.Add("_DeleteEmployeeHolidays", 198)
                privilegeDict.Add("_AllowLeaveViewing", 199)
                privilegeDict.Add("_AddShiftInformation", 200)
                privilegeDict.Add("_EditShiftInformation", 201)
                privilegeDict.Add("_DeleteShiftInformation", 202)
                privilegeDict.Add("_AddTimeSheetInformation", 203)
                privilegeDict.Add("_EditTimeSheetInformation", 204)
                privilegeDict.Add("_DeleteTimeSheetInformation", 205)
                privilegeDict.Add("_AllowLoginTimesheet", 206)
                privilegeDict.Add("_AllowHoldEmployee", 207)
                privilegeDict.Add("_AllowUnHoldEmployee", 208)
                privilegeDict.Add("_AllowProcessEmployeeAbsent", 209)
                privilegeDict.Add("_AddStation", 210)
                privilegeDict.Add("_EditStation", 211)
                privilegeDict.Add("_DeleteStation", 212)
                privilegeDict.Add("_AddDepartmentGroup", 213)
                privilegeDict.Add("_EditDepartmentGroup", 214)
                privilegeDict.Add("_DeleteDepartmentGroup", 215)
                privilegeDict.Add("_AddDepartment", 216)
                privilegeDict.Add("_EditDepartment", 217)
                privilegeDict.Add("_DeleteDepartment", 218)
                privilegeDict.Add("_AddSection", 219)
                privilegeDict.Add("_EditSection", 220)
                privilegeDict.Add("_DeleteSection", 221)
                privilegeDict.Add("_AddUnit", 222)
                privilegeDict.Add("_EditUnit", 223)
                privilegeDict.Add("_DeleteUnit", 224)
                privilegeDict.Add("_AddJobGroup", 225)
                privilegeDict.Add("_EditJobGroup", 226)
                privilegeDict.Add("_DeleteJobGroup", 227)
                privilegeDict.Add("_AddJob", 228)
                privilegeDict.Add("_EditJob", 229)
                privilegeDict.Add("_DeleteJob", 230)
                privilegeDict.Add("_AddClassGroup", 231)
                privilegeDict.Add("_EditClassGroup", 232)
                privilegeDict.Add("_DeleteClassGroup", 233)
                privilegeDict.Add("_AddClasses", 234)
                privilegeDict.Add("_EditClasses", 235)
                privilegeDict.Add("_DeleteClasses", 236)
                privilegeDict.Add("_AddGradeGroup", 237)
                privilegeDict.Add("_EditGradeGroup", 238)
                privilegeDict.Add("_DeleteGradeGroup", 239)
                privilegeDict.Add("_AddGrade", 240)
                privilegeDict.Add("_EditGrade", 241)
                privilegeDict.Add("_DeleteGrade", 242)
                privilegeDict.Add("_AddGradeLevel", 243)
                privilegeDict.Add("_EditGradeLevel", 244)
                privilegeDict.Add("_DeleteGradeLevel", 245)
                privilegeDict.Add("_AddLoanScheme", 246)
                privilegeDict.Add("_EditLoanScheme", 247)
                privilegeDict.Add("_DeleteLoanScheme", 248)
                privilegeDict.Add("_AllowLoanApproverCreation", 249)
                privilegeDict.Add("_AddPendingLoan", 250)
                privilegeDict.Add("_EditPendingLoan", 251)
                privilegeDict.Add("_DeletePendingLoan", 252)
                privilegeDict.Add("_AllowAssignPendingLoan", 253)
                privilegeDict.Add("_EditLoanAdvance", 254)
                privilegeDict.Add("_DeleteLoanAdvance", 255)
                privilegeDict.Add("_AllowChangeLoanAvanceStatus", 256)
                privilegeDict.Add("_AddSavingScheme", 257)
                privilegeDict.Add("_EditSavingScheme", 258)
                privilegeDict.Add("_DeleteSavingScheme", 259)
                privilegeDict.Add("_AddEmployeeSavings", 260)
                privilegeDict.Add("_EditEmployeeSavings", 261)
                privilegeDict.Add("_DeleteEmployeeSavings", 262)
                privilegeDict.Add("_AllowChangeSavingStatus", 263)
                privilegeDict.Add("_AllowtoApproveLeave", 264)
                privilegeDict.Add("_AddDisciplineStatus", 274)
                privilegeDict.Add("_EditDisciplineStatus", 275)
                privilegeDict.Add("_DeleteDisciplineStatus", 276)
                privilegeDict.Add("_AllowViewEmployeeMovement", 277)
                privilegeDict.Add("_AllowEnrollFingerPrint", 278)
                privilegeDict.Add("_AllowDeleteFingerPrint", 279)
                privilegeDict.Add("_AllowEnrollNewCard", 280)
                privilegeDict.Add("_AllowDeleteEnrolledCard", 281)
                privilegeDict.Add("_AllowImportEmployee", 282)
                privilegeDict.Add("_AddTrainingInstitute", 283)
                privilegeDict.Add("_EditTrainingInstitute", 284)
                privilegeDict.Add("_DeleteTrainingInstitute", 285)
                privilegeDict.Add("_AllowImportTransactionHead", 286)
                privilegeDict.Add("_AddPayslipMessage", 287)
                privilegeDict.Add("_EditPayslipMessage", 288)
                privilegeDict.Add("_DeletePayslipMessage", 289)
                privilegeDict.Add("_AllowAddGlobalPayslipMessage", 290)
                privilegeDict.Add("_AllowDeleteAccruedLeave", 291)
                privilegeDict.Add("_AddPlanLeave", 292)
                privilegeDict.Add("_EditPlanLeave", 293)
                privilegeDict.Add("_DeletePlanLeave", 294)
                privilegeDict.Add("_AllowtoViewPlannedLeaveViewer", 295)
                privilegeDict.Add("_AddLoanAdvancePayment", 296)
                privilegeDict.Add("_EditLoanAdvancePayment", 297)
                privilegeDict.Add("_DeleteLoanAdvancePayment", 298)
                privilegeDict.Add("_AddLoanAdvanceReceived", 299)
                privilegeDict.Add("_EditLoanAdvanceReceived", 300)
                privilegeDict.Add("_DeleteLoanAdvanceReceived", 301)
                privilegeDict.Add("_AllowAssignBatchtoEmployee", 302)
                privilegeDict.Add("_AllowImportLeaveTnA", 303)
                privilegeDict.Add("_AllowExportLeaveTnA", 304)
                privilegeDict.Add("_AllowFinalAnalysis", 305)
                privilegeDict.Add("_AddTrainingAnalysis", 306)
                privilegeDict.Add("_EditTrainingAnalysis", 307)
                privilegeDict.Add("_DeleteTrainingAnalysis", 308)
                privilegeDict.Add("_AddTrainingEnrollment", 309)
                privilegeDict.Add("_EditTrainingEnrollment", 310)
                privilegeDict.Add("_DeleteTrainingEnrollment", 311)
                privilegeDict.Add("_CancelTrainingEnrollment", 312)
                privilegeDict.Add("_AddCourseScheduling", 313)
                privilegeDict.Add("_EditCourseScheduling", 314)
                privilegeDict.Add("_DeleteCourseScheduling", 315)
                privilegeDict.Add("_AddCashDenomination", 319)
                privilegeDict.Add("_EditCashDenomination", 320)
                privilegeDict.Add("_AllowImportED", 321)
                privilegeDict.Add("_AllowExportED", 322)
                privilegeDict.Add("_AllowGlobalPayment", 323)
                privilegeDict.Add("_AllowGlobalSalaryIncrement", 324)
                privilegeDict.Add("_AllowChangePendingLoanAdvanceStatus", 325)
                privilegeDict.Add("_AddSavingsPayment", 326)
                privilegeDict.Add("_EditSavingsPayment", 327)
                privilegeDict.Add("_DeleteSavingsPayment", 328)
                privilegeDict.Add("_AllowMapApproverWithUser", 329)
                privilegeDict.Add("_AllowChangeLeaveFormStatus", 330)
                privilegeDict.Add("_AllowCommonExport", 331)
                privilegeDict.Add("_AllowCommonImport", 332)
                privilegeDict.Add("_AllowAddUser", 333)
                privilegeDict.Add("_AllowEditUser", 334)
                privilegeDict.Add("_AllowDeleteUser", 335)
                privilegeDict.Add("_AllowChangePassword", 336)
                privilegeDict.Add("_AssignReportPrivilege", 337)
                privilegeDict.Add("_AllowAddUserRole", 338)
                privilegeDict.Add("_AllowEditUserRole", 339)
                privilegeDict.Add("_AllowDeleteUserRole", 340)
                privilegeDict.Add("_AllowAddCompany", 341)
                privilegeDict.Add("_AllowEditCompany", 342)
                privilegeDict.Add("_AllowDeleteCompany", 343)
                privilegeDict.Add("_AllowToApproveEmployee", 344)
                privilegeDict.Add("_AllowtoApproveLoan", enUserPriviledge.AllowToApproveLoan)
                privilegeDict.Add("_AllowtoApproveAdvance", 346)
                privilegeDict.Add("_AllowToApproveEarningDeduction", enUserPriviledge.AllowToApproveEarningDeduction)
                privilegeDict.Add("_AllowAccessSalaryAnalysis", 348)
                privilegeDict.Add("_AllowAccessLeaveAnalysis", 349)
                privilegeDict.Add("_AllowAccessAttendanceSummary", 350)
                privilegeDict.Add("_AllowAccessStaffTurnOver", 351)
                privilegeDict.Add("_AllowAccessTrainingAnalysis", 352)
                privilegeDict.Add("_AllowToApproveEmpExemtion", 353)
                privilegeDict.Add("_AllowtoAddVacancy", 354)
                privilegeDict.Add("_AllowtoEditVacancy", 355)
                privilegeDict.Add("_AllowtoDeleteVacancy", 356)
                privilegeDict.Add("_AllowtoAddApplicant", 357)
                privilegeDict.Add("_AllowtoEditApplicant", 358)
                privilegeDict.Add("_AllowtoDeleteApplicant", 359)
                privilegeDict.Add("_AllowtoExportDataOnWeb", 360)
                privilegeDict.Add("_AllowtoImportDataFromWeb", 361)
                privilegeDict.Add("_AllowtoAddBatch", 362)
                privilegeDict.Add("_AllowtoEditBatch", 363)
                privilegeDict.Add("_AllowtoDeleteBatch", 364)
                privilegeDict.Add("_AllowtoCancelBatch", 365)
                privilegeDict.Add("_AllowtoAddInterviewSchedule", 366)
                privilegeDict.Add("_AllowtoDeleteInterviewSchedule", 367)
                privilegeDict.Add("_AllowtoCancelInterviewSchedule", 368)
                privilegeDict.Add("_AllowtoAddInterviewAnalysis", 369)
                privilegeDict.Add("_AllowtoChangeBatch", 370)
                privilegeDict.Add("_AllowtoAddInterviewFinalEvaluation", 371)
                privilegeDict.Add("_AllowtoEditInterviewAnalysis", 372)
                privilegeDict.Add("_AllowtoDeleteInterviewAnalysis", 373)
                privilegeDict.Add("_AllowtoMarkApplicantEligible", 374)
                privilegeDict.Add("_AllowtoSendMailApplicant", 375)
                privilegeDict.Add("_AllowtoGenerateApplicantLetter", 376)
                privilegeDict.Add("_AllowtoPrintFinalApplicantList", 377)
                privilegeDict.Add("_AllowtoImportEligibleApplicant", 378)
                privilegeDict.Add("_AllowtoMakeBatchActive", 379)
                privilegeDict.Add("_AllowtoAttachApplicantDocuments", 380)
                privilegeDict.Add("_AllowtoShortListFinalShortListApplicants", 381)
                privilegeDict.Add("_AllowTo_View_Scale", 382)
                privilegeDict.Add("_AllowtoChangeConfirmationDate", 383)
                privilegeDict.Add("_AllowtoChangeAppointmentDate", 384)
                privilegeDict.Add("_AllowtoSetEmployeeBirthDate", 385)
                privilegeDict.Add("_AllowtoSetEmpSuspensionDate", 386)
                privilegeDict.Add("_AllowtoSetEmpProbationDate", 387)
                privilegeDict.Add("_AllowtoSetEmploymentEndDate", 388)
                privilegeDict.Add("_AllowtoSetLeavingDate", 389)
                privilegeDict.Add("_AllowtoChangeRetirementDate", 390)
                privilegeDict.Add("_AllowtoMarkEmployeeClear", 391)
                privilegeDict.Add("_AllowChangeAssetStatus", 392)
                privilegeDict.Add("_AllowToApproveResolutionStep", 393)
                privilegeDict.Add("_AllowToAddResolutionStep", 394)
                privilegeDict.Add("_AllowToEditResolutionStep", 395)
                privilegeDict.Add("_AllowToDeleteResolutionStep", 396)
                privilegeDict.Add("_AllowToAddDiscipileExemption", 397)
                privilegeDict.Add("_AllowToAddDisciplinePosting", 398)
                privilegeDict.Add("_AllowToViewDisciplineExemption", 399)
                privilegeDict.Add("_AllowToViewDisciplinePosting", 400)
                privilegeDict.Add("_AllowToDeleteDisciplineExemption", 401)
                privilegeDict.Add("_AllowToDeleteDisciplinePosting", 402)
                privilegeDict.Add("_AllowChangeGeneralSettings", 403)
                privilegeDict.Add("_AllowAddLetterTemplate", 404)
                privilegeDict.Add("_AllowEditLetterTemplate", 405)
                privilegeDict.Add("_AllowDeleteLetterTemplate", 406)
                privilegeDict.Add("_AllowToSendMail", 407)
                privilegeDict.Add("_AllowToDeleteMail", 408)
                privilegeDict.Add("_AllowToViewUserLog", 409)
                privilegeDict.Add("_AllowToAddAccount", 410)
                privilegeDict.Add("_AllowToEditAccount", 411)
                privilegeDict.Add("_AllowToDeleteAccount", 412)
                privilegeDict.Add("_AllowToAddCompanyConfig", 413)
                privilegeDict.Add("_AllowToEditCompanyConfig", 414)
                privilegeDict.Add("_AllowToDeleteCompanyConfig", 415)
                privilegeDict.Add("_AllowToAddEmpConfig", 416)
                privilegeDict.Add("_AllowToEditEmpConfig", 417)
                privilegeDict.Add("_AllowToDeleteEmpConfig", 418)
                privilegeDict.Add("_AllowToAddCCConfig", 419)
                privilegeDict.Add("_AllowToEditCCConfig", 420)
                privilegeDict.Add("_AllowToDeleteCCConfig", 421)
                privilegeDict.Add("_AllowtoSetEmpReinstatementDate", 426)
                privilegeDict.Add("_Allow_AddAssetDeclaration", 427)
                privilegeDict.Add("_Allow_EditAssetDeclaration", 428)
                privilegeDict.Add("_Allow_DeleteAssetDeclaration", 429)
                privilegeDict.Add("_Allow_FinalSaveAssetDeclaration", 430)
                privilegeDict.Add("_Allow_UnlockFinalSaveAssetDeclaration", 431)
                privilegeDict.Add("_AllowtoAddLeaveAllowance", 432)
                privilegeDict.Add("_AllowtoAddDisciplineCommittee", 433)
                privilegeDict.Add("_AllowtoEditDisciplineCommittee", 434)
                privilegeDict.Add("_AllowtoDeleteDisciplineCommittee", 435)
                privilegeDict.Add("_AllowtoCloseCase", 436)
                privilegeDict.Add("_AllowtoReOpenCase", 437)
                privilegeDict.Add("_AllowToExportMedicalClaim", 438)
                privilegeDict.Add("_AllowToSaveMedicalClaim", 439)
                privilegeDict.Add("_AllowToFinalSaveMedicalClaim", 440)
                privilegeDict.Add("_AllowToCancelExportedMedicalClaim", 441)
                privilegeDict.Add("_AllowToMakeServiceProviderActive", 442)
                privilegeDict.Add("_AllowToMapUserWithServiceProvider", 443)
                privilegeDict.Add("_AllowToAddMedicalSickSheet", 444)
                privilegeDict.Add("_AllowToEditMedicalSickSheet", 445)
                privilegeDict.Add("_AllowToDeleteMedicalSickSheet", 446)
                privilegeDict.Add("_AllowToPrintMedicalSickSheet", 447)
                privilegeDict.Add("_AllowToAddAppraisalAnalysis", 472)
                privilegeDict.Add("_AllowToDeleteAppraisalAnalysis", 473)
                privilegeDict.Add("_AllowToPerformSalaryIncrement", 474)
                privilegeDict.Add("_AllowToVoidSalaryIncrement", 475)
                privilegeDict.Add("_AllowToAddReminder", 476)
                privilegeDict.Add("_AllowToAddEmployeeToFinalList", 477)
                privilegeDict.Add("_AllowToSaveAppraisals", 478)
                privilegeDict.Add("_AllowToAddEvaluationGroup", 479)
                privilegeDict.Add("_AllowToEditEvaluationGroup", 480)
                privilegeDict.Add("_AllowToDeleteEvaluationGroup", 481)
                privilegeDict.Add("_AllowToAddEvaluationItems", 482)
                privilegeDict.Add("_AllowToEditEvaluationItems", 483)
                privilegeDict.Add("_AllowToDeleteEvaluationItems", 484)
                privilegeDict.Add("_AllowToAddEvaluationSubItems", 485)
                privilegeDict.Add("_AllowToEditEvaluationSubItems", 486)
                privilegeDict.Add("_AllowToDeleteEvaluationSubItems", 487)
                privilegeDict.Add("_AllowToAddEvaluationIIIItems", 488)
                privilegeDict.Add("_AllowToEditEvaluationIIIItems", 489)
                privilegeDict.Add("_AllowToDeleteEvaluationIIIItems", 490)
                privilegeDict.Add("_AllowToAddLevelIEvaluation", 491)
                privilegeDict.Add("_AllowToEditLevelIEvaluation", 492)
                privilegeDict.Add("_AllowToDeleteLevelIEvaluation", 493)
                privilegeDict.Add("_AllowToPrintLevelIEvaluation", 494)
                privilegeDict.Add("_AllowToPreviewLevelIEvaluation", 495)
                privilegeDict.Add("_AllowToSave_CompleteLevelIEvaluation", 496)
                privilegeDict.Add("_AllowToAddLevelIIIEvaluation", 497)
                privilegeDict.Add("_AllowToEditLevelIIIEvaluation", 498)
                privilegeDict.Add("_AllowToDeleteLevelIIIEvaluation", 499)
                privilegeDict.Add("_AllowToPrintLevelIIIEvaluation", 500)
                privilegeDict.Add("_AllowToPreviewLevelIIIEvaluation", 501)
                privilegeDict.Add("_AllowToSave_CompleteLevelIIIEvaluation", 502)
                privilegeDict.Add("_AllowToAddTrainingPriority", 514)
                privilegeDict.Add("_AllowToEditTrainingPriority", 515)
                privilegeDict.Add("_AllowToDeleteTrainingPriority", 516)
                privilegeDict.Add("_AllowToExportWithEmployee", 517)
                privilegeDict.Add("_AllowToExportWithoutEmployee", 518)
                privilegeDict.Add("_AllowToPreviewScheduledTraining", 519)
                privilegeDict.Add("_AllowToPrintScheduledTraining", 520)
                privilegeDict.Add("_AllowToUpdateQualifictaion", 521)
                privilegeDict.Add("_AllowToRe_Categorize", 522)
                privilegeDict.Add("_AllowSalaryIncrementFromEnrollment", 523)
                privilegeDict.Add("_AllowToVoidSalaryIncrementFromEnrollment", 524)
                privilegeDict.Add("_Show_Probation_Dates", 525)
                privilegeDict.Add("_Show_Suspension_Dates", 526)
                privilegeDict.Add("_Show_Appointment_Dates", 527)
                privilegeDict.Add("_Show_Confirmation_Dates", 528)
                privilegeDict.Add("_Show_BirthDates", 529)
                privilegeDict.Add("_Show_Anniversary_Dates", 530)
                privilegeDict.Add("_Show_Contract_Ending_Dates", 531)
                privilegeDict.Add("_Show_TodayRetirement_Dates", 532)
                privilegeDict.Add("_Show_ForcastedRetirement_Dates", 533)
                privilegeDict.Add("_AllowVoidPayroll", 534)
                privilegeDict.Add("_Show_ForcastedEOC_Dates", 535)
                privilegeDict.Add("_AllowToCancelLeave", 536)
                privilegeDict.Add("_AllowToCancelPreviousDateLeave", 537)
                privilegeDict.Add("_AllowToEditQueries", 538)
                privilegeDict.Add("_AllowToAddTemplate", 539)
                privilegeDict.Add("_AllowToEditTemplate", 540)
                privilegeDict.Add("_AllowToDeleteTemplate", 541)
                privilegeDict.Add("_AllowToAccessCustomReport", 542)
                privilegeDict.Add("_AllowToViewLeaveApproverList", 543)
                privilegeDict.Add("_AllowToViewLeaveFormList", 544)
                privilegeDict.Add("_AllowToViewLeaveProcessList", 545)
                privilegeDict.Add("_AllowToViewLeavePlannerList", 546)
                privilegeDict.Add("_AllowToViewEmpHolidayList", 547)
                privilegeDict.Add("_AllowToViewMedicalCoverList", 548)
                privilegeDict.Add("_AllowToViewMedicalClaimList", 549)
                privilegeDict.Add("_AllowToViewEmpInjuryList", 550)
                privilegeDict.Add("_AllowToViewEmpSickSheetList", 551)
                privilegeDict.Add("_AllowToViewEmpList", 552)
                privilegeDict.Add("_AllowToViewBenefitAllocationList", 553)
                privilegeDict.Add("_AllowToViewEmpBenefitList", 554)
                privilegeDict.Add("_AllowToViewEmpReferenceList", 555)
                privilegeDict.Add("_AllowToViewCompanyAssetList", 556)
                privilegeDict.Add("_AllowToViewEmpSkillList", 557)
                privilegeDict.Add("_AllowToViewEmpQualificationList", 558)
                privilegeDict.Add("_AllowToViewEmpExperienceList", 559)
                privilegeDict.Add("_AllowToViewEmpDependantsList", 560)
                privilegeDict.Add("_AllowToViewChargesProceedingList", 561)
                privilegeDict.Add("_AllowToViewProceedingApprovalList", 562)
                privilegeDict.Add("_AllowToViewDisciplinaryCommitteeList", 563)
                privilegeDict.Add("_AllowToViewLeaveTypeList", 564)
                privilegeDict.Add("_AllowToViewHolidayList", 565)
                privilegeDict.Add("_AllowToViewApproverLevelList", 566)
                privilegeDict.Add("_AllowToViewLeaveViewer", 567)
                privilegeDict.Add("_AllowToViewMedicalMasterList", 568)
                privilegeDict.Add("_AllowToViewServiceProviderList", 569)
                privilegeDict.Add("_AllowToViewMedicalCategoryList", 570)
                privilegeDict.Add("_AllowToViewCommonMasterList", 571)
                privilegeDict.Add("_AllowToViewMembershipMasterList", 572)
                privilegeDict.Add("_AllowToViewStateList", 573)
                privilegeDict.Add("_AllowToViewCityList", 574)
                privilegeDict.Add("_AllowToViewZipcodeList", 575)
                privilegeDict.Add("_AllowToViewEmpMovementList", 576)
                privilegeDict.Add("_AllowToViewShiftList", 577)
                privilegeDict.Add("_AllowToViewTrainingInstituteList", 578)
                privilegeDict.Add("_AllowToViewApprisalAnalysisList", 585)
                privilegeDict.Add("_AllowToViewTrainingNeedsAnalysisList", 586)
                privilegeDict.Add("_AllowToViewTrainingSchedulingList", 587)
                privilegeDict.Add("_AllowToViewTrainingEnrollmentList", 588)
                privilegeDict.Add("_AllowToViewLevel1EvaluationList", 589)
                privilegeDict.Add("_AllowToViewLevel3EvaluationList", 590)
                privilegeDict.Add("_AllowToViewAssetsDeclarationList", 591)
                privilegeDict.Add("_AllowToViewProcessLoanAdvanceList", 592)
                privilegeDict.Add("_AllowToViewLoanAdvanceList", 593)
                privilegeDict.Add("_AllowToViewEmployeeSavingsList", 594)
                privilegeDict.Add("_AllowToViewVacancyMasterList", 595)
                privilegeDict.Add("_AllowToViewApplicantMasterList", 596)
                privilegeDict.Add("_AllowToViewShortListApplicants", 597)
                privilegeDict.Add("_AllowToViewBatchSchedulingList", 598)
                privilegeDict.Add("_AllowToViewInterviewSchedulingList", 599)
                privilegeDict.Add("_AllowToViewFinalApplicantList", enUserPriviledge.AllowToViewFinalApplicantList)
                privilegeDict.Add("_AllowToViewEmpDistributedCostCenterList", 601)
                privilegeDict.Add("_AllowToViewEmpBankList", 602)
                privilegeDict.Add("_AllowToViewTransactionHeadsList", 603)
                privilegeDict.Add("_AllowToViewEmpEDList", 604)
                privilegeDict.Add("_AllowToViewEmpExemptionList", 605)
                privilegeDict.Add("_AllowToViewPaymentList", 607)
                privilegeDict.Add("_AllowToViewGlobalVoidPaymentList", 608)
                privilegeDict.Add("_AllowAuthorizePayslipPayment", 609)
                privilegeDict.Add("_AllowVoidAuthorizedPayslipPayment", 610)
                privilegeDict.Add("_AllowToPerformForceLogout", 611)
                privilegeDict.Add("_AllowToViewStationList", 612)
                privilegeDict.Add("_AllowToViewDeptGroupList", 613)
                privilegeDict.Add("_AllowToViewDepartmentList", 614)
                privilegeDict.Add("_AllowToViewSectionGroupList", 615)
                privilegeDict.Add("_AllowToViewSectionList", 616)
                privilegeDict.Add("_AllowToViewUnitGroupList", 617)
                privilegeDict.Add("_AllowToViewUnitList", 618)
                privilegeDict.Add("_AllowToViewTeamList", 619)
                privilegeDict.Add("_AllowToViewJobGroupList", 620)
                privilegeDict.Add("_AllowToViewJobList", 621)
                privilegeDict.Add("_AllowToViewClassGroupList", 622)
                privilegeDict.Add("_AllowToViewClassList", 623)
                privilegeDict.Add("_AllowToViewGradeGroupList", 624)
                privilegeDict.Add("_AllowToViewGradeList", 625)
                privilegeDict.Add("_AllowToViewGradeLevelList", 626)
                privilegeDict.Add("_AllowToViewCostCenterList", 627)
                privilegeDict.Add("_AllowToViewPayrollPeriodList", 628)
                privilegeDict.Add("_AllowToViewSalaryChangeList", 629)
                privilegeDict.Add("_AllowToViewReasonMasterList", 630)
                privilegeDict.Add("_AllowToViewSkillMasterList", 631)
                privilegeDict.Add("_AllowToViewAgencyMasterList", 632)
                privilegeDict.Add("_AllowToViewQualificationMasterList", 633)
                privilegeDict.Add("_AllowToViewResultCodeList", 634)
                privilegeDict.Add("_AllowToViewBenefitPlanList", 635)
                privilegeDict.Add("_AllowToViewDisciplinaryOffencesList", 636)
                privilegeDict.Add("_AllowToViewDisciplinaryPenaltiesList", 637)
                privilegeDict.Add("_AllowToViewDisciplinaryStatusList", 638)
                privilegeDict.Add("_AllowToViewEvaluationGroupList", 650)
                privilegeDict.Add("_AllowToViewEvaluationItemList", 651)
                privilegeDict.Add("_AllowToViewEvaluationSubItemList", 652)
                privilegeDict.Add("_AllowToViewEvaluationIIIitemList", 653)
                privilegeDict.Add("_AllowToViewPayrollGroupList", 654)
                privilegeDict.Add("_AllowToViewPayPointList", 655)
                privilegeDict.Add("_AllowToViewBankBranchList", 656)
                privilegeDict.Add("_AllowToViewBankAccountTypeList", 657)
                privilegeDict.Add("_AllowToViewBankEDIList", 658)
                privilegeDict.Add("_AllowToViewCurrencyList", 659)
                privilegeDict.Add("_AllowToViewCurrencyDenominationList", 660)
                privilegeDict.Add("_AllowToViewPayslipMessageList", 661)
                privilegeDict.Add("_AllowToViewAccountList", 662)
                privilegeDict.Add("_AllowToViewCompanyAccountConfigurationList", 663)
                privilegeDict.Add("_AllowToViewEmployeeAccountConfigurationList", 664)
                privilegeDict.Add("_AllowToViewCostcenterAccountConfigurationList", 665)
                privilegeDict.Add("_AllowToViewBatchTransactionList", 666)
                privilegeDict.Add("_AllowToViewCashDenominationList", 667)
                privilegeDict.Add("_AllowToViewLoanSchemeList", 668)
                privilegeDict.Add("_AllowToViewSavingSchemeList", 669)
                privilegeDict.Add("_AllowToViewApplicantReferenceNo", 670)
                privilegeDict.Add("_AllowToApproveSalaryChange", 671)
                privilegeDict.Add("_AddTeam", 672)
                privilegeDict.Add("_EditTeam", 673)
                privilegeDict.Add("_DeleteTeam", 674)
                privilegeDict.Add("_AddSectionGroup", 675)
                privilegeDict.Add("_EditSectionGroup", 676)
                privilegeDict.Add("_DeleteSectionGroup", 677)
                privilegeDict.Add("_AddUnitGroup", 678)
                privilegeDict.Add("_EditUnitGroup", 679)
                privilegeDict.Add("_DeleteUnitGroup", 680)
                privilegeDict.Add("_EditImportedEmployee", 681)
                privilegeDict.Add("_AllowToScanAttachDocument", 682)
                privilegeDict.Add("_AllowToExportMembership", 683)
                privilegeDict.Add("_AllowToImporttMembership", 684)
                privilegeDict.Add("_AllowToSetReportingTo", 685)
                privilegeDict.Add("_AllowToViewDiary", 686)
                privilegeDict.Add("_AllowToSetAllocationMapping", 687)
                privilegeDict.Add("_AllowToDeleteAllocationMapping", 688)
                privilegeDict.Add("_AllowToImportDependants", 689)
                privilegeDict.Add("_AllowToExportDependants", 690)
                privilegeDict.Add("_AllowtoImportEmpReferee", 691)
                privilegeDict.Add("_AllowtoExportEmpReferee", 692)
                privilegeDict.Add("_AllowtoImportEmpSkills", 693)
                privilegeDict.Add("_AllowtoExportEmpSkills", 694)
                privilegeDict.Add("_AllowtoImportEmpQualification", 695)
                privilegeDict.Add("_AllowtoExportEmpQualification", 696)
                privilegeDict.Add("_AllowtoExportOtherQualification", 697)
                privilegeDict.Add("_AllowtoExportEmpExperience", 698)
                privilegeDict.Add("_AllowtoImportEmpExperience", 699)
                privilegeDict.Add("_AllowtoMapVacancy", 700)
                privilegeDict.Add("_AllowtoViewInterviewAnalysisList", 701)
                privilegeDict.Add("_AllowtoMapLoanApprover", 702)
                privilegeDict.Add("_AllowtoImportLoan_Advance", 703)
                privilegeDict.Add("_AllowtoImportSavings", 704)
                privilegeDict.Add("_AllowtoImportAccrueLeave", 705)
                privilegeDict.Add("_AllowtoMigrateLeaveApprover", 706)
                privilegeDict.Add("_AllowtoImportAttendanceData", 707)
                privilegeDict.Add("_AllowtoMakeGroupAttendance", 708)
                privilegeDict.Add("_AllowtoAddGeneralReminder", 709)
                privilegeDict.Add("_AllowtoEditGeneralReminder", 710)
                privilegeDict.Add("_AllowtoDeleteGeneralReminder", 711)
                privilegeDict.Add("_AllowtoChangeUser", 712)
                privilegeDict.Add("_AllowtoChangeCompany", 713)
                privilegeDict.Add("_AllowtoChangeDatabase", 714)
                privilegeDict.Add("_AllowtoChangePassword", 715)
                privilegeDict.Add("_AllowtoViewSentItems", 716)
                privilegeDict.Add("_AllowtoViewTrashItems", 717)
                privilegeDict.Add("_AllowtoPrintLetters", 718)
                privilegeDict.Add("_AllowtoExportLetters", 719)
                privilegeDict.Add("_AllowtoReadMails", 720)
                privilegeDict.Add("_AllowtoViewAuditTrails", 721)
                privilegeDict.Add("_AllowtoViewApplicationEvtLog", 722)
                privilegeDict.Add("_AllowtoViewUserAuthLog", 723)
                privilegeDict.Add("_AllowtoViewUserAttemptsLog", 724)
                privilegeDict.Add("_AllowtoViewAuditLogs", 725)
                privilegeDict.Add("_AllowtoUnlockUser", 726)
                privilegeDict.Add("_AllowtoExportAbilityLevel", 727)
                privilegeDict.Add("_AllowtoSaveAbilityLevel", 728)
                privilegeDict.Add("_AllowtoTakeDatabaseBackup", 729)
                privilegeDict.Add("_AllowtoRestoreDatabase", 730)
                privilegeDict.Add("_AllowtoAddVoidReason", 731)
                privilegeDict.Add("_AllowtoEditVoidReason", 732)
                privilegeDict.Add("_AllowtoDeleteVoidReason", 733)
                privilegeDict.Add("_AllowtoAddReminderType", 734)
                privilegeDict.Add("_AllowtoEditReminderType", 735)
                privilegeDict.Add("_AllowtoDeleteReminderType", 736)
                privilegeDict.Add("_AllowtoSavePasswordOption", 737)
                privilegeDict.Add("_AllowtoAddDeviceSettings", 738)
                privilegeDict.Add("_AllowtoEditDeviceSettings", 739)
                privilegeDict.Add("_AllowtoDeleteDeviceSettings", 740)
                privilegeDict.Add("_AllowtoChangeCompanyOptions", 741)
                privilegeDict.Add("_AllowtoSaveCompanyGroup", 742)
                privilegeDict.Add("_AllowtoExportAccounts", 743)
                privilegeDict.Add("_AllowtoImportAccounts", 744)
                privilegeDict.Add("_AllowtoExportEmpBanks", 745)
                privilegeDict.Add("_AllowtoImportEmpBanks", 746)
                privilegeDict.Add("_AllowtoSetTranHeadActive", 747)
                privilegeDict.Add("_AllowtoMapLeaveType", 748)
                privilegeDict.Add("_AllowtoViewLetterTemplate", 749)
                privilegeDict.Add("_AddLeaveExpense", 750)
                privilegeDict.Add("_EditLeaveExpense", 751)
                privilegeDict.Add("_DeleteLeaveExpense", 752)
                privilegeDict.Add("_ImportADUsers", 753)
                privilegeDict.Add("_AssignUserAccess", 754)
                privilegeDict.Add("_EditUserAccess", 755)
                privilegeDict.Add("_ExportPrivilegs", 756)
                privilegeDict.Add("_AllowtoChangeBranch", 757)
                privilegeDict.Add("_AllowtoChangeDepartmentGroup", 758)
                privilegeDict.Add("_AllowtoChangeDepartment", 759)
                privilegeDict.Add("_AllowtoChangeSectionGroup", 760)
                privilegeDict.Add("_AllowtoChangeSection", 761)
                privilegeDict.Add("_AllowtoChangeUnitGroup", 762)
                privilegeDict.Add("_AllowtoChangeUnit", 763)
                privilegeDict.Add("_AllowtoChangeTeam", 764)
                privilegeDict.Add("_AllowtoChangeJobGroup", 765)
                privilegeDict.Add("_AllowtoChangeJob", 766)
                privilegeDict.Add("_AllowtoChangeGradeGroup", 767)
                privilegeDict.Add("_AllowtoChangeGrade", 768)
                privilegeDict.Add("_AllowtoChangeGradeLevel", 769)
                privilegeDict.Add("_AllowtoChangeClassGroup", 770)
                privilegeDict.Add("_AllowtoChangeClass", 771)
                privilegeDict.Add("_AllowtoChangeCostCenter", 772)
                privilegeDict.Add("_AllowtoChangeCompanyEmail", 773)
                privilegeDict.Add("_AllowtoImportEmployee_User", 774)
                privilegeDict.Add("_AllowToAddBatchPosting", 775)
                privilegeDict.Add("_AllowToEditBatchPosting", 776)
                privilegeDict.Add("_AllowToDeleteBatchPosting", 777)
                privilegeDict.Add("_AllowToPostBatchPostingToED", 778)
                privilegeDict.Add("_AllowToViewBatchPostingList", 779)
                privilegeDict.Add("_AllowToViewMembershipOnDiary", 780)
                privilegeDict.Add("_AllowToViewBanksOnDiary", 781)
                privilegeDict.Add("_AllowToAssignIssueUsertoEmp", 782)
                privilegeDict.Add("_AllowToMigrateIssueUser", 783)
                privilegeDict.Add("_AllowToAddPaymentApproverLevel", 784)
                privilegeDict.Add("_AllowToEditPaymentApproverLevel", 785)
                privilegeDict.Add("_AllowToDeletePaymentApproverLevel", 786)
                privilegeDict.Add("_AllowToAddPaymentApproverMapping", 787)
                privilegeDict.Add("_AllowToEditPaymentApproverMapping", 788)
                privilegeDict.Add("_AllowToDeletePaymentApproverMapping", 789)
                privilegeDict.Add("_AllowToApprovePayment", 790)
                privilegeDict.Add("_AllowToVoidApprovedPayment", 791)
                privilegeDict.Add("_AllowToViewPaymentApproverLevelList", 792)
                privilegeDict.Add("_AllowToEndLeaveCycle", 793)
                privilegeDict.Add("_AllowToAddMedicalDependantException", 794)
                privilegeDict.Add("_AllowToAddEditPhoto", 795)
                privilegeDict.Add("_AllowToDeletePhoto", 796)
                privilegeDict.Add("_AllowToImportPhoto", 797)
                privilegeDict.Add("_AllowToSubmitApplicantFilter_Approval", 799)
                privilegeDict.Add("_AllowToApproveApplicantFilter", 800)
                privilegeDict.Add("_AllowToRejectApplicantFilter", 801)
                privilegeDict.Add("_AllowToAddApplicantFilter", 802)
                privilegeDict.Add("_AllowToDeleteApplicantFilter", 803)
                privilegeDict.Add("_AllowToSubmitForApprovalInFinalShortlistedApplicant", 804)
                privilegeDict.Add("_AllowToApproveFinalShortListedApplicant", 805)
                privilegeDict.Add("_AllowToDisapproveFinalShortListedApplicant", 806)
                privilegeDict.Add("_AllowtoSubmitApplicantEligibilityForApproval", 807)
                privilegeDict.Add("_AllowtoApproveApplicantEligibility", 808)
                privilegeDict.Add("_AllowtoDisapproveApplicantEligibility", 809)
                privilegeDict.Add("_AllowToMoveFinalShortListedApplicantToShortListed", 810)
                privilegeDict.Add("_AllowtoAddPayActivity", 811)
                privilegeDict.Add("_AllowtoEditPayActivity", 812)
                privilegeDict.Add("_AllowtoDeletePayActivity", 813)
                privilegeDict.Add("_AllowtoAddUnitOfMeasure", 814)
                privilegeDict.Add("_AllowtoEditUnitOfMeasure", 815)
                privilegeDict.Add("_AllowtoDeleteUnitOfMeasure", 816)
                privilegeDict.Add("_AllowtoAdd_EditActivityRate", 817)
                privilegeDict.Add("_AllowtoPostActivitytoPayroll", 818)
                privilegeDict.Add("_AllowtoVoidActivtyfromPayroll", 819)
                privilegeDict.Add("_AllowtoAdd_EditPayPerActivity", 820)
                privilegeDict.Add("_AllowtoDeletePayPerActivity", 821)
                privilegeDict.Add("_AllowtoExportPayPerActivity", 822)
                privilegeDict.Add("_AllowtoImportPayPerActivity", 823)
                privilegeDict.Add("_AllowtoAssignPayPerActivityGlobally", 824)
                privilegeDict.Add("_AllowtoviewActivityList", 825)
                privilegeDict.Add("_AllowtoViewMeasureList", 826)
                privilegeDict.Add("_AllowToImportEmpIdentity", 827)
                privilegeDict.Add("_AllowToMapDeviceUser", 828)
                privilegeDict.Add("_AllowToImportDeviceUser", 829)
                privilegeDict.Add("_AllowToEditTimesheetCard", enUserPriviledge.AllowToEditTimeSheetCard)
                privilegeDict.Add("_AllowToAddEditDeleteGlobalTimesheet", enUserPriviledge.AllowToAddEditDeleteGlobalTimeSheet)
                privilegeDict.Add("_AllowToReCalculateTimings", enUserPriviledge.AllowToReCalculateTimings)
                privilegeDict.Add("_AllowToRoundoffTimings", enUserPriviledge.AllowToRoundoffTimings)
                privilegeDict.Add("_AllowToDeleteEmailNotificationAuditTrails", 834)
                privilegeDict.Add("_AllowToChangeOtherUserPassword", 835)
                privilegeDict.Add("_AllowToAddStaffRequisitionApproverLevel", 836)
                privilegeDict.Add("_AllowToEditStaffRequisitionApproverLevel", 837)
                privilegeDict.Add("_AllowToDeleteStaffRequisitionApproverLevel", 838)
                privilegeDict.Add("_AllowToViewStaffRequisitionApproverLevelList", 839)
                privilegeDict.Add("_AllowToAddStaffRequisitionApproverMapping", 840)
                privilegeDict.Add("_AllowToEditStaffRequisitionApproverMapping", 841)
                privilegeDict.Add("_AllowToDeleteStaffRequisitionApproverMapping", 842)
                privilegeDict.Add("_AllowToViewStaffRequisitionApproverMappingList", 843)
                privilegeDict.Add("_AllowToAddStaffRequisition", 844)
                privilegeDict.Add("_AllowToEditStaffRequisition", 845)
                privilegeDict.Add("_AllowToDeleteStaffRequisition", 846)
                privilegeDict.Add("_AllowToViewStaffRequisitionList", 847)
                privilegeDict.Add("_AllowToApproveStaffRequisition", 848)
                privilegeDict.Add("_AllowToCancelStaffRequisition", 849)
                privilegeDict.Add("_AllowToViewStaffRequisitionApprovals", 850)
                privilegeDict.Add("_AllowToAddStatutoryPayment", 851)
                privilegeDict.Add("_AllowToEditStatutoryPayment", 852)
                privilegeDict.Add("_AllowToDeleteStatutoryPayment", 853)
                privilegeDict.Add("_AllowToViewStatutoryPaymentList", 854)
                privilegeDict.Add("_RevokeUserAccessOnSicksheet", 855)
                privilegeDict.Add("_AllowToSetPaymentApproverLevelActive", 856)
                privilegeDict.Add("_Show_ForcastedELC_Dates", 857)
                privilegeDict.Add("_AllowtoDeletePayPerActivityAuditTrails", 858)
                privilegeDict.Add("_AllowtoImportPeopleSoftData", 859)
                privilegeDict.Add("_AllowtoAddAssessmentPeriod", 860)
                privilegeDict.Add("_AllowtoEditAssessmentPeriod", 861)
                privilegeDict.Add("_AllowtoDeleteAssessmentPeriod", 862)
                privilegeDict.Add("_AllowtoAddAssessorAccess", 863)
                privilegeDict.Add("_AllowtoEditAssessorAccess", 864)
                privilegeDict.Add("_AllowtoDeleteAssessorAccess", 865)
                privilegeDict.Add("_AllowtoImportAssessor", 866)
                privilegeDict.Add("_AllowtoAddReviewerAccess", 867)
                privilegeDict.Add("_AllowtoEditReviewerAccess", 868)
                privilegeDict.Add("_AllowtoDeleteReviewerAccess", 869)
                privilegeDict.Add("_AllowtoImportReviewer", 870)
                privilegeDict.Add("_AllowtoPerformAssessorReviewerMigration", 871)
                privilegeDict.Add("_AllowtoAddAssessmentRatios", 872)
                privilegeDict.Add("_AllowtoEditAssessmentRatios", 873)
                privilegeDict.Add("_AllowtoDeleteAssessmentRatios", 874)
                privilegeDict.Add("_AllowtoCloseAssessmentPeriod", 875)
                privilegeDict.Add("_AllowtoAddAssessmentGroup", 876)
                privilegeDict.Add("_AllowtoEditAssessmentGroup", 877)
                privilegeDict.Add("_AllowtoDeleteAssessmentGroup", 878)
                privilegeDict.Add("_AllowtoAddAssessmentScales", 879)
                privilegeDict.Add("_AllowtoEditAssessmentScales", 880)
                privilegeDict.Add("_AllowtoDeleteAssessmentScales", 881)
                privilegeDict.Add("_AllowtoMapScaleGrouptoGoals", 882)
                privilegeDict.Add("_AllowtoAddCompetencies", 883)
                privilegeDict.Add("_AllowtoEditCompetencies", 884)
                privilegeDict.Add("_AllowtoDeleteCompetencies", 885)
                privilegeDict.Add("_AllowtoLoadfromCompetenciesLibrary", 886)
                privilegeDict.Add("_AllowtoAddAssignedCompetencies", 887)
                privilegeDict.Add("_AllowtoEditAssignedCompetencies", 888)
                privilegeDict.Add("_AllowtoDeleteAssignedCompetencies", 889)
                privilegeDict.Add("_AllowtoAddPerspective", 890)
                privilegeDict.Add("_AllowtoEditPerspective", 891)
                privilegeDict.Add("_AllowtoDeletePerspective", 892)
                privilegeDict.Add("_AllowtoSaveBSCTitles", 893)
                privilegeDict.Add("_AllowtoAddBSCTitlesMapping", 894)
                privilegeDict.Add("_AllowtoEditBSCTitlesMapping", 895)
                privilegeDict.Add("_AllowtoDeleteBSCTitlesMapping", 896)
                privilegeDict.Add("_AllowtoSaveBSCTitlesViewSetting", 897)
                privilegeDict.Add("_AllowtoAddCompanyGoals", 898)
                privilegeDict.Add("_AllowtoEditCompanyGoals", 899)
                privilegeDict.Add("_AllowtoDeleteCompanyGoals", 900)
                privilegeDict.Add("_AllowtoCommitCompanyGoals", 901)
                privilegeDict.Add("_AllowtoUnlockcommittedCompanyGoals", 902)
                privilegeDict.Add("_AllowtoAddAllocationGoals", 903)
                privilegeDict.Add("_AllowtoEditAllocationGoals", 904)
                privilegeDict.Add("_AllowtoDeleteAllocationGoals", 905)
                privilegeDict.Add("_AllowtoCommitAllocationGoals", 906)
                privilegeDict.Add("_AllowtoUnlockcommittedAllocationGoals", 907)
                privilegeDict.Add("_AllowtoPerformGlobalAssignAllocationGoals", 908)
                privilegeDict.Add("_AllowtoUpdatePercentCompletedAllocationGoals", 909)
                privilegeDict.Add("_AllowtoAddEmployeeGoals", 910)
                privilegeDict.Add("_AllowtoEditEmployeeGoals", 911)
                privilegeDict.Add("_AllowtoDeleteEmployeeGoals", 912)
                privilegeDict.Add("_AllowtoPerformGlobalAssignEmployeeGoals", 913)
                privilegeDict.Add("_AllowtoUpdatePercentCompletedEmployeeGoals", 914)
                privilegeDict.Add("_AllowtoSubmitGoalsforApproval", 915)
                privilegeDict.Add("_AllowtoApproveRejectGoalsPlanning", 916)
                privilegeDict.Add("_AllowtoUnlockFinalSavedGoals", 917)
                privilegeDict.Add("_AllowtoAddCustomHeaders", 918)
                privilegeDict.Add("_AllowtoEditCustomHeaders", 919)
                privilegeDict.Add("_AllowtoDeleteCustomHeaders", 920)
                privilegeDict.Add("_AllowtoAddCustomItems", 921)
                privilegeDict.Add("_AllowtoEditCustomItems", 922)
                privilegeDict.Add("_AllowtoDeleteCustomItems", 923)
                privilegeDict.Add("_AllowtoAddComputationFormula", 924)
                privilegeDict.Add("_AllowtoDeleteComputationFormula", 925)
                privilegeDict.Add("_AllowtoViewAssessmentPeriodList", 926)
                privilegeDict.Add("_AllowtoViewAssessorAccessList", 927)
                privilegeDict.Add("_AllowtoViewReviewerAccessList", 928)
                privilegeDict.Add("_AllowtoViewAssessmentGroupList", 929)
                privilegeDict.Add("_AllowtoViewCompanyGoalsList", 930)
                privilegeDict.Add("_AllowtoViewAllocationGoalsList", 931)
                privilegeDict.Add("_AllowtoViewEmployeeGoalsList", 932)
                privilegeDict.Add("_AllowtoAddSelfEvaluation", 933)
                privilegeDict.Add("_AllowtoEditSelfEvaluation", 934)
                privilegeDict.Add("_AllowtoDeleteSelfEvaluation", 935)
                privilegeDict.Add("_AllowtoUnlockcommittedSelfEvaluation", 936)
                privilegeDict.Add("_AllowtoAddAssessorEvaluation", 937)
                privilegeDict.Add("_AllowtoEditAssessorEvaluation", 938)
                privilegeDict.Add("_AllowtoDeleteAssessorEvaluation", 939)
                privilegeDict.Add("_AllowtoUnlockcommittedAssessorEvaluation", 940)
                privilegeDict.Add("_AllowtoAddReviewerEvaluation", 941)
                privilegeDict.Add("_AllowtoEditReviewerEvaluation", 942)
                privilegeDict.Add("_AllowtoDeleteReviewerEvaluation", 943)
                privilegeDict.Add("_AllowtoUnlockcommittedReviewerEvaluation", 944)
                privilegeDict.Add("_AllowtoViewSelfEvaluationList", 945)
                privilegeDict.Add("_AllowtoViewAssessorEvaluationList", 946)
                privilegeDict.Add("_AllowtoViewReviewerEvaluationList", 947)
                privilegeDict.Add("_AllowtoViewPerformanceEvaluation", 948)
                privilegeDict.Add("_AllowtoAddClaimExpenses", 949)
                privilegeDict.Add("_AllowtoEditClaimExpenses", 950)
                privilegeDict.Add("_AllowtoDeleteClaimExpenses", 951)
                privilegeDict.Add("_AllowtoAddSectorRouteCost", 953)
                privilegeDict.Add("_AllowtoEditSectorRouteCost", 954)
                privilegeDict.Add("_AllowtoDeleteSectorRouteCost", 955)
                privilegeDict.Add("_AllowtoViewSectorRouteCostList", 956)
                privilegeDict.Add("_AllowtoAddEmpExpesneAssignment", 957)
                privilegeDict.Add("_AllowtoDeleteEmpExpesneAssignment", 958)
                privilegeDict.Add("_AllowtoViewEmpExpesneAssignment", 959)
                privilegeDict.Add("_AllowtoAddExpenseApprovalLevel", 960)
                privilegeDict.Add("_AllowtoEditExpenseApprovalLevel", 961)
                privilegeDict.Add("_AllowtoDeleteExpenseApprovalLevel", 962)
                privilegeDict.Add("_AllowtoViewExpenseApprovalLevelList", 963)
                privilegeDict.Add("_AllowtoAddExpenseApprover", 964)
                privilegeDict.Add("_AllowtoEditExpenseApprover", 965)
                privilegeDict.Add("_AllowtoDeleteExpenseApprover", 966)
                privilegeDict.Add("_AllowtoViewExpenseApproverList", 967)
                privilegeDict.Add("_AllowtoMigrateExpenseApprover", 968)
                privilegeDict.Add("_AllowtoSwapExpenseApprover", 969)
                privilegeDict.Add("_AllowtoAddClaimExpenseForm", 970)
                privilegeDict.Add("_AllowtoEditClaimExpenseForm", 971)
                privilegeDict.Add("_AllowtoDeleteClaimExpenseForm", 972)
                privilegeDict.Add("_AllowtoViewClaimExpenseFormList", 973)
                privilegeDict.Add("_AllowtoCancelClaimExpenseForm", 974)
                privilegeDict.Add("_AllowtoProcessClaimExpenseForm", 975)
                privilegeDict.Add("_AllowtoViewProcessClaimExpenseFormList", 976)
                privilegeDict.Add("_AllowtoPostClaimExpenseToPayroll", 977)
                privilegeDict.Add("_AllowtoViewReportAbilityLevel", 978)
                privilegeDict.Add("_AllowtoExportReportAbilityLevel", 979)
                privilegeDict.Add("_AllowtoSaveReportAbilityLevel", 980)
                privilegeDict.Add("_AllowtoSetClaimInactiveApprover", 981)
                privilegeDict.Add("_AddSavingsDeposit", 982)
                privilegeDict.Add("_EditSavingsDeposit", 983)
                privilegeDict.Add("_DeleteSavingsDeposit", 984)
                privilegeDict.Add("_AllowtoChangePreassignedCompetencies", 991)
                privilegeDict.Add("_AllowToVoidBatchPostingToED", 992)
                privilegeDict.Add("_AllowEditGlobalPayslipMessage", 993)
                privilegeDict.Add("_AllowDeleteGlobalPayslipMessage", 994)
                privilegeDict.Add("_AllowViewGlobalPayslipMessage", 995)
                privilegeDict.Add("_AllowToAssignGradePriority", 996)
                privilegeDict.Add("_AllowToViewSalaryAnniversaryMonth", 997)
                privilegeDict.Add("_AllowToAddSalaryAnniversaryMonth", 998)
                privilegeDict.Add("_AllowToDeleteSalaryAnniversaryMonth", 999)
                privilegeDict.Add("_AllowtoComputeProcess", 1000)
                privilegeDict.Add("_AllowtoComputeVoidProcess", 1001)
                privilegeDict.Add("_AllowToImportWagesTable", 1002)
                privilegeDict.Add("_AllowToExportWagesTable", 1003)
                privilegeDict.Add("_AllowToViewFundSource", 1004)
                privilegeDict.Add("_AllowToAddFundSource", 1005)
                privilegeDict.Add("_AllowToEditFundSource", 1006)
                privilegeDict.Add("_AllowToDeleteFundSource", 1007)
                privilegeDict.Add("_AllowToViewFundAdjustment", 1008)
                privilegeDict.Add("_AllowToAddFundAdjustment", 1009)
                privilegeDict.Add("_AllowToEditFundAdjustment", 1010)
                privilegeDict.Add("_AllowToDeleteFundAdjustment", 1011)
                privilegeDict.Add("_AllowToViewBudgetFormula", 1012)
                privilegeDict.Add("_AllowToAddBudgetFormula", 1013)
                privilegeDict.Add("_AllowToEditBudgetFormula", 1014)
                privilegeDict.Add("_AllowToDeleteBudgetFormula", 1015)
                privilegeDict.Add("_AllowToViewBudget", 1016)
                privilegeDict.Add("_AllowToAddBudget", 1017)
                privilegeDict.Add("_AllowToEditBudget", 1018)
                privilegeDict.Add("_AllowToDeleteBudget", 1019)
                privilegeDict.Add("_AllowToApproveProceedingCounts", 1020)
                privilegeDict.Add("_AllowToViewFundActivity", 1021)
                privilegeDict.Add("_AllowToAddFundActivity", 1022)
                privilegeDict.Add("_AllowToEditFundActivity", 1023)
                privilegeDict.Add("_AllowToDeleteFundActivity", 1024)
                privilegeDict.Add("_AllowToViewFundActivityAdjustment", 1025)
                privilegeDict.Add("_AllowToAddFundActivityAdjustment", 1026)
                privilegeDict.Add("_AllowToEditFundActivityAdjustment", 1027)
                privilegeDict.Add("_AllowToDeleteFundActivityAdjustment", 1028)
                privilegeDict.Add("_AllowToViewFundProjectCode", 1030)
                privilegeDict.Add("_AllowToAddFundProjectCode", 1031)
                privilegeDict.Add("_AllowToEditFundProjectCode", 1032)
                privilegeDict.Add("_AllowToDeleteFundProjectCode", 1033)
                privilegeDict.Add("_AllowToMapLevelToApprover", 1034)
                privilegeDict.Add("_AllowToViewBudgetApproverLevelMapping", 1035)
                privilegeDict.Add("_AllowToDeleteBudgetApproverLevelMapping", 1036)
                privilegeDict.Add("_AllowToViewBudgetApproverLevel", 1037)
                privilegeDict.Add("_AllowToAddBudgetApproverLevel", 1038)
                privilegeDict.Add("_AllowToEditBudgetApproverLevel", 1039)
                privilegeDict.Add("_AllowToDeleteBudgetApproverLevel", 1040)
                privilegeDict.Add("_AllowToApproveBudget", 1041)
                privilegeDict.Add("_AllowToVoidApprovedBudget", 1042)
                privilegeDict.Add("_AllowToViewBudgetApprovals", 1043)
                privilegeDict.Add("_AllowToViewEmailSetup", enUserPriviledge.AllowToViewEmailSetup)
                privilegeDict.Add("_AllowToAddEmailSetup", enUserPriviledge.AllowToAddEmailSetup)
                privilegeDict.Add("_AllowToEditEmailSetup", enUserPriviledge.AllowToEditEmailSetup)
                privilegeDict.Add("_AllowToDeleteEmailSetup", enUserPriviledge.AllowToDeleteEmailSetup)
                privilegeDict.Add("_AllowtoSetLeaveActiveApprover", 1044)
                privilegeDict.Add("_AllowtoSetLeaveInactiveApprover", 1045)
                privilegeDict.Add("_AllowtoSetClaimActiveApprover", 1046)
                privilegeDict.Add("_AllowToEditBudgetCodes", enUserPriviledge.AllowToEditBudgetCodes)
                privilegeDict.Add("_AllowToDeleteBudgetCodes", enUserPriviledge.AllowToDeleteBudgetCodes)
                privilegeDict.Add("_AllowToViewBudgetCodes", enUserPriviledge.AllowToViewBudgetCodes)
                privilegeDict.Add("_AllowToCancelApprovedLoanApp", enUserPriviledge.AllowToCancelApprovedLoanApp)
                privilegeDict.Add("_AllowToApproveGoalsAccomplishment", enUserPriviledge.AllowToApproveGoalsAccomplishment)
                privilegeDict.Add("_AllowToMapBudgetTimesheetApprover", enUserPriviledge.AllowToMapBudgetTimesheetApprover)
                privilegeDict.Add("_AllowToChangeEmpRecategorize", enUserPriviledge.AllowToChangeEmpRecategorize)
                privilegeDict.Add("_AllowToChangeEmpTransfers", enUserPriviledge.AllowToChangeEmpTransfers)
                privilegeDict.Add("_AllowToChangeEmpWorkPermit", enUserPriviledge.AllowToChangeEmpWorkPermit)
                privilegeDict.Add("_AllowToSwapLeaveApprover", enUserPriviledge.AllowToSwapLeaveApprover)
                privilegeDict.Add("_AllowToViewSkillExpertise", enUserPriviledge.AllowToViewSkillExpertise)
                privilegeDict.Add("_AllowToAddSkillExpertise", enUserPriviledge.AllowToAddSkillExpertise)
                privilegeDict.Add("_AllowToEditSkillExpertise", enUserPriviledge.AllowToEditSkillExpertise)
                privilegeDict.Add("_AllowToDeleteSkillExpertise", enUserPriviledge.AllowToDeleteSkillExpertise)
                privilegeDict.Add("_AllowToViewBudgetTimesheetApproverLevelList", enUserPriviledge.AllowToViewBudgetTimesheetApproverLevelList)
                privilegeDict.Add("_AllowToAddBudgetTimesheetApproverLevel", enUserPriviledge.AllowToAddBudgetTimesheetApproverLevel)
                privilegeDict.Add("_AllowToEditBudgetTimesheetApproverLevel", enUserPriviledge.AllowToEditBudgetTimesheetApproverLevel)
                privilegeDict.Add("_AllowToDeleteBudgetTimesheetApproverLevel", enUserPriviledge.AllowToDeleteBudgetTimesheetApproverLevel)
                privilegeDict.Add("_AllowToViewBudgetTimesheetApproverList", enUserPriviledge.AllowToViewBudgetTimesheetApproverList)
                privilegeDict.Add("_AllowToAddBudgetTimesheetApprover", enUserPriviledge.AllowToAddBudgetTimesheetApprover)
                privilegeDict.Add("_AllowToEditBudgetTimesheetApprover", enUserPriviledge.AllowToEditBudgetTimesheetApprover)
                privilegeDict.Add("_AllowToDeleteBudgetTimesheetApprover", enUserPriviledge.AllowToDeleteBudgetTimesheetApprover)
                privilegeDict.Add("_AllowToSetBudgetTimesheetApproverAsActive", enUserPriviledge.AllowToSetBudgetTimesheetApproverAsActive)
                privilegeDict.Add("_AllowToSetBudgetTimesheetApproverAsInActive", enUserPriviledge.AllowToSetBudgetTimesheetApproverAsInactive)
                privilegeDict.Add("_AllowToViewEmployeeBudgetTimesheetList", enUserPriviledge.AllowToViewEmployeeBudgetTimesheetList)
                privilegeDict.Add("_AllowToAddEmployeeBudgetTimesheet", enUserPriviledge.AllowToAddEmployeeBudgetTimesheet)
                privilegeDict.Add("_AllowToEditEmployeeBudgetTimesheet", enUserPriviledge.AllowToEditEmployeeBudgetTimesheet)
                privilegeDict.Add("_AllowToDeleteEmployeeBudgetTimesheet", enUserPriviledge.AllowToDeleteEmployeeBudgetTimesheet)
                privilegeDict.Add("_AllowToCancelEmployeeBudgetTimesheet", enUserPriviledge.AllowToCancelEmployeeBudgetTimesheet)
                privilegeDict.Add("_AllowToViewPendingEmpBudgetTimesheetSubmitForApproval", enUserPriviledge.AllowToViewPendingEmpBudgetTimesheetSubmitForApproval)
                privilegeDict.Add("_AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval", enUserPriviledge.AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval)
                privilegeDict.Add("_AllowToSubmitForApprovalForPendingEmpBudgetTimesheet", enUserPriviledge.AllowToSubmitForApprovalForPendingEmpBudgetTimesheet)
                privilegeDict.Add("_AllowToViewEmployeeBudgetTimesheetApprovalList", enUserPriviledge.AllowToViewEmployeeBudgetTimesheetApprovalList)
                privilegeDict.Add("_AllowToChangeEmployeeBudgetTimesheetStatus", enUserPriviledge.AllowToChangeEmployeeBudgetTimesheetStatus)
                privilegeDict.Add("_AllowToMigrateBudgetTimesheetApprover", enUserPriviledge.AllowToMigrateBudgetTimesheetApprover)
                privilegeDict.Add("_AllowToImportEmployeeShiftAssignment", enUserPriviledge.AllowToImportEmployeeShiftAssignment)
                privilegeDict.Add("_AllowToEnableDisableADUser", enUserPriviledge.AllowToEnableDisableADUser)
                privilegeDict.Add("_AllowToImportEmployeeAccountConfiguration", enUserPriviledge.AllowToImportEmployeeAccountConfiguration)
                privilegeDict.Add("_AllowToExportEmployeeAccountConfiguration", enUserPriviledge.AllowToExportEmployeeAccountConfiguration)
                privilegeDict.Add("_AllowToGetFileFormatOfEmployeeAccountConfiguration", enUserPriviledge.AllowToGetFileFormatOfEmployeeAccountConfiguration)
                privilegeDict.Add("_AllowToDeleteRehireEmployeeDetails", enUserPriviledge.AllowToDeleteRehireEmployeeDetails)
                privilegeDict.Add("_AllowToPerformEligibleOperation", enUserPriviledge.AllowToPerformEligibleOperation)
                privilegeDict.Add("_AllowToPerformNotEligibleOperation", enUserPriviledge.AllowToPerformNotEligibleOperation)
                privilegeDict.Add("_AllowToAddLoanApproverLevel", enUserPriviledge.AllowToAddLoanApproverLevel)
                privilegeDict.Add("_AllowToEditLoanApproverLevel", enUserPriviledge.AllowToEditLoanApproverLevel)
                privilegeDict.Add("_AllowToDeleteLoanApproverLevel", enUserPriviledge.AllowToDeleteLoanApproverLevel)
                privilegeDict.Add("_AllowToAddLoanApprover", enUserPriviledge.AllowToAddLoanApprover)
                privilegeDict.Add("_AllowToEditLoanApprover", enUserPriviledge.AllowToEditLoanApprover)
                privilegeDict.Add("_AllowToDeleteLoanApprover", enUserPriviledge.AllowToDeleteLoanApprover)
                privilegeDict.Add("_AllowToSetActiveInactiveLoanApprover", enUserPriviledge.AllowToSetActiveInactiveLoanApprover)
                privilegeDict.Add("_AllowToViewLoanApproverLevelList", enUserPriviledge.AllowToViewLoanApproverLevelList)
                privilegeDict.Add("_AllowToViewLoanApproverList", enUserPriviledge.AllowToViewLoanApproverList)
                privilegeDict.Add("_AllowToProcessGlobalLoanApprove", enUserPriviledge.AllowToProcessGlobalLoanApprove)
                privilegeDict.Add("_AllowToProcessGlobalLoanAssign", enUserPriviledge.AllowToProcessGlobalLoanAssign)
                privilegeDict.Add("_AllowToViewLoanApprovalList", enUserPriviledge.AllowToViewLoanApprovalList)
                privilegeDict.Add("_AllowToChangeLoanStatus", enUserPriviledge.AllowToChangeLoanStatus)
                privilegeDict.Add("_AllowToTransferLoanApprover", enUserPriviledge.AllowToTransferLoanApprover)
                privilegeDict.Add("_AllowToSwapLoanApprover", enUserPriviledge.AllowToSwapLoanApprover)
                privilegeDict.Add("_AllowToImportLeaveApprovers", enUserPriviledge.AllowToImportLeaveApprovers)
                privilegeDict.Add("_AllowToImportLeavePlanner", enUserPriviledge.AllowToImportLeavePlanner)
                privilegeDict.Add("_AllowToViewDeviceUserMapping", enUserPriviledge.AllowToViewDeviceUserMapping)
                privilegeDict.Add("_AllowToImportEmployeeBirthInfo", enUserPriviledge.AllowToImportEmployeeBirthInfo)
                privilegeDict.Add("_AllowToImportEmployeeAddress", enUserPriviledge.AllowToImportEmployeeAddress)
                privilegeDict.Add("_AllowToViewMissingEmployee", enUserPriviledge.AllowToViewMissingEmployee)
                privilegeDict.Add("_AllowToUpdateEmployeeDetails", enUserPriviledge.AllowToUpdateEmployeeDetails)
                privilegeDict.Add("_AllowToUpdateEmployeeMovements", enUserPriviledge.AllowToUpdateEmployeeMovements)
                privilegeDict.Add("_AllowToViewAssignedShiftList", enUserPriviledge.AllowToViewAssignedShiftList)
                privilegeDict.Add("_AllowToDeleteAssignedShift", enUserPriviledge.AllowToDeleteAssignedShift)
                privilegeDict.Add("_AllowToImportCompanyAssets", enUserPriviledge.AllowToImportCompanyAssets)
                privilegeDict.Add("_AllowToViewEligibleApplicants", enUserPriviledge.AllowToViewEligibleApplicants)
                privilegeDict.Add("_AllowToPerformComputeScoreProcess", enUserPriviledge.AllowToPerformComputeScoreProcess)
                privilegeDict.Add("_AllowToPerformVoidComputedScore", enUserPriviledge.AllowToPerformVoidComputedScore)
                privilegeDict.Add("_AllowToEditAppraisalSetup", enUserPriviledge.AllowToEditAppraisalSetup)
                privilegeDict.Add("_AllowToDeleteAppraisalSetup", enUserPriviledge.AllowToDeleteAppraisalSetup)
                privilegeDict.Add("_AllowToImportTrainingEnrollment", enUserPriviledge.AllowToImportTrainingEnrollment)
                privilegeDict.Add("_AllowToExportEmployeeList", enUserPriviledge.AllowToExportEmployeeList)
                privilegeDict.Add("_AllowToAddRemoveFields", enUserPriviledge.AllowToAddRemoveFields)
                privilegeDict.Add("_AllowToHistoricalSalaryChange", enUserPriviledge.AllowToHistoricalSalaryChange)
                privilegeDict.Add("_AllowToPostNewProceedings", enUserPriviledge.AllowToPostNewProceedings)
                privilegeDict.Add("_AllowToEditViewProceedings", enUserPriviledge.AllowToEditViewProceedings)
                privilegeDict.Add("_AllowToScanAttachmentDocuments", enUserPriviledge.AllowToScanAttachmentDocuments)
                privilegeDict.Add("_AllowToProceedingSubmitForApproval", enUserPriviledge.AllowToProceedingSubmitForApproval)
                privilegeDict.Add("_AllowToApproveDisapproveProceedings", enUserPriviledge.AllowToApproveDisapproveProceedings)
                privilegeDict.Add("_AllowToExemptTransactionHead", enUserPriviledge.AllowToExemptTransactionHead)
                privilegeDict.Add("_AllowToPostTransactionHead", enUserPriviledge.AllowToPostTransactionHead)
                privilegeDict.Add("_AllowToViewDisciplineHead", enUserPriviledge.AllowToViewDisciplineHead)
                privilegeDict.Add("_AllowToPrintPreviewDisciplineCharge", enUserPriviledge.AllowToPrintPreviewDisciplineCharge)
                privilegeDict.Add("_AllowToAddDisciplinaryCommittee", enUserPriviledge.AllowToAddDisciplinaryCommittee)
                privilegeDict.Add("_AllowToEditDeleteDisciplinaryCommittee", enUserPriviledge.AllowToEditDeleteDisciplinaryCommittee)
                privilegeDict.Add("_AllowToViewDisciplineProceedingList", enUserPriviledge.AllowToViewDisciplineProceedingList)
                privilegeDict.Add("_AllowToViewDisciplineHearingList", enUserPriviledge.AllowToViewDisciplineHearingList)
                privilegeDict.Add("_AllowToAddDisciplineHearing", enUserPriviledge.AllowToAddDisciplineHearing)
                privilegeDict.Add("_AllowToEditDisciplineHearing", enUserPriviledge.AllowToEditDisciplineHearing)
                privilegeDict.Add("_AllowToDeleteDisciplineHearing", enUserPriviledge.AllowToDeleteDisciplineHearing)
                privilegeDict.Add("_AllowToMarkAsClosedDisciplineHearing", enUserPriviledge.AllowToMarkAsClosedDisciplineHearing)
                privilegeDict.Add("_AllowToEditTransferEmployeeDetails", enUserPriviledge.AllowToEditTransferEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteTransferEmployeeDetails", enUserPriviledge.AllowToDeleteTransferEmployeeDetails)
                privilegeDict.Add("_AllowToEditRecategorizeEmployeeDetails", enUserPriviledge.AllowToEditRecategorizeEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteRecategorizeEmployeeDetails", enUserPriviledge.AllowToDeleteRecategorizeEmployeeDetails)
                privilegeDict.Add("_AllowToEditProbationEmployeeDetails", enUserPriviledge.AllowToEditProbationEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteProbationEmployeeDetails", enUserPriviledge.AllowToDeleteProbationEmployeeDetails)
                privilegeDict.Add("_AllowToEditConfirmationEmployeeDetails", enUserPriviledge.AllowToEditConfirmationEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteConfirmationEmployeeDetails", enUserPriviledge.AllowToDeleteConfirmationEmployeeDetails)
                privilegeDict.Add("_AllowToEditSuspensionEmployeeDetails", enUserPriviledge.AllowToEditSuspensionEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteSuspensionEmployeeDetails", enUserPriviledge.AllowToDeleteSuspensionEmployeeDetails)
                privilegeDict.Add("_AllowToEditTerminationEmployeeDetails", enUserPriviledge.AllowToEditTerminationEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteTerminationEmployeeDetails", enUserPriviledge.AllowToDeleteTerminationEmployeeDetails)
                privilegeDict.Add("_AllowToRehireEmployee", enUserPriviledge.AllowToRehireEmployee)
                privilegeDict.Add("_AllowToEditWorkPermitEmployeeDetails", enUserPriviledge.AllowToEditWorkPermitEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteWorkPermitEmployeeDetails", enUserPriviledge.AllowToDeleteWorkPermitEmployeeDetails)
                privilegeDict.Add("_AllowToEditRetiredEmployeeDetails", enUserPriviledge.AllowToEditRetiredEmployeeDetails)
                privilegeDict.Add("_AllowToDeleteRetiredEmployeeDetails", enUserPriviledge.AllowToDeleteRetiredEmployeeDetails)
                privilegeDict.Add("_AllowToGlobalVoidEmployeeMembership", enUserPriviledge.AllowToGlobalVoidEmployeeMembership)
                privilegeDict.Add("_AllowToAddShiftPolicyAssignment", enUserPriviledge.AllowToAddShiftPolicyAssignment)
                privilegeDict.Add("_AllowToAssignEmployeeDayOff", enUserPriviledge.AllowToAssignEmployeeDayOff)
                privilegeDict.Add("_AllowToViewEmployeeDayOff", enUserPriviledge.AllowToViewEmployeeDayOff)
                privilegeDict.Add("_AllowToDeleteEmployeeDayOff", enUserPriviledge.AllowToDeleteEmployeeDayOff)
                privilegeDict.Add("_AllowToViewAssignedPolicyList", enUserPriviledge.AllowToViewAssignedPolicyList)
                privilegeDict.Add("_AllowToDeleteAssignedPolicy", enUserPriviledge.AllowToDeleteAssignedPolicy)
                privilegeDict.Add("_AllowToPerformGlobalGoalOperations", enUserPriviledge.AllowToPerformGlobalGoalOperations)
                privilegeDict.Add("_AllowToPerformGlobalVoidGoals", enUserPriviledge.AllowToPerformGlobalVoidGoals)
                privilegeDict.Add("_AllowToSubmitForGoalAccomplished", enUserPriviledge.AllowToSubmitForGoalAccomplished)
                privilegeDict.Add("_AllowToImportEmployeeGoals", enUserPriviledge.AllowToImportEmployeeGoals)
                privilegeDict.Add("_AllowToPrintEmployeeScoreCard", enUserPriviledge.AllowToPrintEmployeeScoreCard)
                privilegeDict.Add("_AllowToExportEmployeeScoreCard", enUserPriviledge.AllowToExportEmployeeScoreCard)
                privilegeDict.Add("_AllowToSetTranHeadInactive", enUserPriviledge.AllowToSetTranHeadInactive)
                privilegeDict.Add("_AllowToAddLeaveFrequency", enUserPriviledge.AllowToAddLeaveFrequency)
                privilegeDict.Add("_AllowToDeleteLeaveFrequency", enUserPriviledge.AllowToDeleteLeaveFrequency)
                privilegeDict.Add("_AllowToViewLeaveFrequency", enUserPriviledge.AllowToViewLeaveFrequency)
                privilegeDict.Add("_AllowToPerformLeaveAdjustment", enUserPriviledge.AllowToPerformLeaveAdjustment)
                privilegeDict.Add("_AllowToEditRehireEmployeeDetails", enUserPriviledge.AllowToEditRehireEmployeeDetails)
                privilegeDict.Add("_AllowtoSeeEmployeeSignature", enUserPriviledge.AllowToSeeEmployeeSignature)
                privilegeDict.Add("_AllowtoAddEmployeeSignature", enUserPriviledge.AllowtoAddEmployeeSignature)
                privilegeDict.Add("_AllowtoDeleteEmployeeSignature", enUserPriviledge.AllowtoDeleteEmployeeSignature)
                privilegeDict.Add("_AllowtoViewPendingAccrueData", enUserPriviledge.AllowtoViewPendingAccrueLeaves)
                privilegeDict.Add("_AllowToViewEmpApproverLevelList", enUserPriviledge.AllowToViewEmpApproverLevelList)
                privilegeDict.Add("_AllowToAddEmpApproverLevel", enUserPriviledge.AllowToAddEmpApproverLevel)
                privilegeDict.Add("_AllowToEditEmpApproverLevel", enUserPriviledge.AllowToEditEmpApproverLevel)
                privilegeDict.Add("_AllowToDeleteEmpApproverLevel", enUserPriviledge.AllowToDeleteEmpApproverLevel)
                privilegeDict.Add("_AllowToViewEmployeeApproverList", enUserPriviledge.AllowToViewEmployeeApproverList)
                privilegeDict.Add("_AllowToAddEmployeeApprover", enUserPriviledge.AllowToAddEmployeeApprover)
                privilegeDict.Add("_AllowToDeleteEmployeeApprover", enUserPriviledge.AllowToDeleteEmployeeApprover)
                privilegeDict.Add("_AllowtoSetInactiveEmployeeApprover", enUserPriviledge.AllowToSetInActiveEmployeeApprover)
                privilegeDict.Add("_AllowToSetActiveEmployeeApprover", enUserPriviledge.AllowToSetActiveEmployeeApprover)
                privilegeDict.Add("_AllowToPerformEmpSubmitForApproval", enUserPriviledge.AllowToPerformEmpSubmitForApproval)
                privilegeDict.Add("_AllowtoApproveGrievance", enUserPriviledge.AllowtoApproveGrievance)
                privilegeDict.Add("_AllowToAddGrievanceApproverLevel", enUserPriviledge.AllowToAddGrievanceApproverLevel)
                privilegeDict.Add("_AllowToEditGrievanceApproverLevel", enUserPriviledge.AllowToEditGrievanceApproverLevel)
                privilegeDict.Add("_AllowToDeleteGrievanceApproverLevel", enUserPriviledge.AllowToDeleteGrievanceApproverLevel)
                privilegeDict.Add("_AllowToViewGrievanceApproverLevel", enUserPriviledge.AllowToViewGrievanceApproverLevel)
                privilegeDict.Add("_AllowToAddGrievanceApprover", enUserPriviledge.AllowToAddGrievanceApprover)
                privilegeDict.Add("_AllowToEditGrievanceApprover", enUserPriviledge.AllowToEditGrievanceApprover)
                privilegeDict.Add("_AllowToDeleteGrievanceApprover", enUserPriviledge.AllowToDeleteGrievanceApprover)
                privilegeDict.Add("_AllowToViewGrievanceApprover", enUserPriviledge.AllowToViewGrievanceApprover)
                privilegeDict.Add("_AllowToActivateGrievanceApprover", enUserPriviledge.AllowToActivateGrievanceApprover)
                privilegeDict.Add("_AllowToInActivateGrievanceApprover", enUserPriviledge.AllowToInActivateGrievanceApprover)
                privilegeDict.Add("_AllowToViewGrievanceResolutionStepList", enUserPriviledge.AllowToViewGrievanceResolutionStepList)
                privilegeDict.Add("_AllowToAddGrievanceResolutionStep", enUserPriviledge.AllowToAddGrievanceResolutionStep)
                privilegeDict.Add("_AllowToEditGrievanceResolutionStep", enUserPriviledge.AllowToEditGrievanceResolutionStep)
                privilegeDict.Add("_AllowToDeleteGrievanceResolutionStep", enUserPriviledge.AllowToDeleteGrievanceResolutionStep)
                privilegeDict.Add("_AllowToAddTrainingApproverLevel", enUserPriviledge.AllowToAddTrainingApproverLevel)
                privilegeDict.Add("_AllowToEditTrainingApproverLevel", enUserPriviledge.AllowToEditTrainingApproverLevel)
                privilegeDict.Add("_AllowToDeleteTrainingApproverLevel", enUserPriviledge.AllowToDeleteTrainingApproverLevel)
                privilegeDict.Add("_AllowToViewTrainingApproverLevel", enUserPriviledge.AllowToViewTrainingApproverLevel)
                privilegeDict.Add("_AllowToAddTrainingApprover", enUserPriviledge.AllowToAddTrainingApprover)
                privilegeDict.Add("_AllowToDeleteTrainingApprover", enUserPriviledge.AllowToDeleteTrainingApprover)
                privilegeDict.Add("_AllowToViewTrainingApprover", enUserPriviledge.AllowToViewTrainingApprover)
                privilegeDict.Add("_AllowToApproveTrainingRequisition", enUserPriviledge.AllowToApproveTrainingRequisition)
                privilegeDict.Add("_AllowToActivateTrainingApprover", enUserPriviledge.AllowToActivateTrainingApprover)
                privilegeDict.Add("_AllowToDeactivateTrainingApprover", enUserPriviledge.AllowToDeactivateTrainingApprover)
                privilegeDict.Add("_AllowToAddOTRequisitionApproverLevel", enUserPriviledge.AllowToAddOTRequisitionApproverLevel)
                privilegeDict.Add("_AllowToEditOTRequisitionApproverLevel", enUserPriviledge.AllowToEditOTRequisitionApproverLevel)
                privilegeDict.Add("_AllowToDeleteOTRequisitionApproverLevel", enUserPriviledge.AllowToDeleteOTRequisitionApproverLevel)
                privilegeDict.Add("_AllowToViewOTRequisitionApproverLevel", enUserPriviledge.AllowToViewOTRequisitionApproverLevel)
                privilegeDict.Add("_AllowToAddOTRequisitionApprover", enUserPriviledge.AllowToAddOTRequisitionApprover)
                privilegeDict.Add("_AllowToEditOTRequisitionApprover", enUserPriviledge.AllowToEditOTRequisitionApprover)
                privilegeDict.Add("_AllowToDeleteOTRequisitionApprover", enUserPriviledge.AllowToDeleteOTRequisitionApprover)
                privilegeDict.Add("_AllowToViewOTRequisitionApprover", enUserPriviledge.AllowToViewOTRequisitionApprover)
                privilegeDict.Add("_AllowToActivateOTRequisitionApprover", enUserPriviledge.AllowToActivateOTRequisitionApprover)
                privilegeDict.Add("_AllowToInActivateOTRequisitionApprover", enUserPriviledge.AllowToInActivateOTRequisitionApprover)
                privilegeDict.Add("_AllowtoApproveOTRequisition", enUserPriviledge.AllowtoApproveOTRequisition)
                privilegeDict.Add("_AllowtoUnlockEmployeeForAssetDeclaration", enUserPriviledge.AllowtoUnlockEmployeeForAssetDeclaration)
                privilegeDict.Add("_AllowtoUnlockEmployeeForNonDisclosureDeclaration", enUserPriviledge.AllowtoUnlockEmployeeForNonDisclosureDeclaration)
                privilegeDict.Add("_AllowToApproveRejectEmployeeQualifications", enUserPriviledge.AllowToApproveRejectEmployeeQualifications)
                privilegeDict.Add("_AllowToApproveRejectEmployeeReferences", enUserPriviledge.AllowToApproveRejectEmployeeReferences)
                privilegeDict.Add("_AllowToApproveRejectEmployeeSkills", enUserPriviledge.AllowToApproveRejectEmployeeSkills)
                privilegeDict.Add("_AllowToApproveRejectEmployeeJobExperiences", enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences)
                privilegeDict.Add("_AllowToApproveRejectEmployeeDependant", enUserPriviledge.AllowToApproveRejectEmployeeDependants)
                privilegeDict.Add("_AllowToApproveRejectEmployeeIdentities", enUserPriviledge.AllowToApproveRejectEmployeeIdentities)
                privilegeDict.Add("_AllowToApproveRejectEmployeeMembership", enUserPriviledge.AllowToApproveRejectEmployeeMemberships)
                privilegeDict.Add("_AllowToApproveRejectEmployeeAddress", enUserPriviledge.AllowToApproveRejectAddress)
                privilegeDict.Add("_AllowToApproveRejectEmployeeEmergencyAddress", enUserPriviledge.AllowToApproveRejectEmergencyAddress)
                privilegeDict.Add("_AllowToApproveRejectEmployeePersonalinfo", enUserPriviledge.AllowToApproveRejectPersonalInfo)
                privilegeDict.Add("_AllowToDeletePercentCompletedEmployeeGoals", enUserPriviledge.AllowToDeletePercentCompletedEmployeeGoals)
                privilegeDict.Add("_AllowToViewPercentCompletedEmployeeGoals", enUserPriviledge.AllowToViewPercentCompletedEmployeeGoals)
                privilegeDict.Add("_AllowToArchiveSystemErrorLog", enUserPriviledge.AllowToArchiveSystemErrorLog)
                privilegeDict.Add("_AllowToSendSystemErrorLogEmail", enUserPriviledge.AllowToSendSystemErrorLogEmail)
                privilegeDict.Add("_AllowToExportSystemErrorLog", enUserPriviledge.AllowToExportSystemErrorLog)
                privilegeDict.Add("_AllowToViewSystemErrorLog", enUserPriviledge.AllowToViewSystemErrorLog)
                privilegeDict.Add("_AllowToSetDependentActive", enUserPriviledge.AllowToSetDependentActive)
                privilegeDict.Add("_AllowToSetDependentInactive", enUserPriviledge.AllowToSetDependentInactive)
                privilegeDict.Add("_AllowToDeleteDependentStatus", enUserPriviledge.AllowToDeleteDependentStatus)
                privilegeDict.Add("_AllowToViewDependentStatus", enUserPriviledge.AllowToViewDependentStatus)
                privilegeDict.Add("_AllowToViewJVPosting", enUserPriviledge.AllowToViewJVPosting)
                privilegeDict.Add("_AllowToViewCalibrationApproverLevel", enUserPriviledge.AllowToViewCalibrationApproverLevel)
                privilegeDict.Add("_AllowToAddCalibrationApproverLevel", enUserPriviledge.AllowToAddCalibrationApproverLevel)
                privilegeDict.Add("_AllowToEditCalibrationApproverLevel", enUserPriviledge.AllowToEditCalibrationApproverLevel)
                privilegeDict.Add("_AllowToDeleteCalibrationApproverLevel", enUserPriviledge.AllowToDeleteCalibrationApproverLevel)
                privilegeDict.Add("_AllowToViewCalibrationApproverMaster", enUserPriviledge.AllowToViewCalibrationApproverMaster)
                privilegeDict.Add("_AllowToAddCalibrationApproverMaster", enUserPriviledge.AllowToAddCalibrationApproverMaster)
                privilegeDict.Add("_AllowToActivateCalibrationApproverMaster", enUserPriviledge.AllowToActivateCalibrationApproverMaster)
                privilegeDict.Add("_AllowToDeleteCalibrationApproverMaster", enUserPriviledge.AllowToDeleteCalibrationApproverMaster)
                privilegeDict.Add("_AllowToDeactivateCalibrationApproverMaster", enUserPriviledge.AllowToDeActivateCalibrationApproverMaster)
                privilegeDict.Add("_AllowToApproveRejectCalibratedScore", enUserPriviledge.AllowToApproveRejectCalibratedScore)
                privilegeDict.Add("_AllowtoViewOTRequisitionApprovalList", enUserPriviledge.AllowtoViewOTRequisitionApprovalList)
                privilegeDict.Add("_AllowToEditCalibratedScore", enUserPriviledge.AllowToEditCalibratedScore)
                privilegeDict.Add("_AllowToViewBudgetTimesheetExemptEmployeeList", enUserPriviledge.AllowToViewBudgetTimesheetExemptEmployeeList)
                privilegeDict.Add("_AllowToAddBudgetTimesheetExemptEmployee", enUserPriviledge.AllowToAddBudgetTimesheetExemptEmployee)
                privilegeDict.Add("_AllowToDeleteBudgetTimesheetExemptEmployee", enUserPriviledge.AllowToDeleteBudgetTimesheetExemptEmployee)
                privilegeDict.Add("_AllowToPostJVOnHolidays", enUserPriviledge.AllowToPostJVOnHolidays)
                privilegeDict.Add("_AllowtoCalibrateProvisionalScore", enUserPriviledge.AllowtoCalibrateProvisionalScore)
                privilegeDict.Add("_AllowToAddEditOprationalPrivileges", enUserPriviledge.AllowToAddEditOprationalPrivileges)
                privilegeDict.Add("_AllowToViewEmpOTAssignment", enUserPriviledge.AllowToViewEmpOTAssignment)
                privilegeDict.Add("_AllowToAddEmpOTAssignment", enUserPriviledge.AllowToAddEmpOTAssignment)
                privilegeDict.Add("_AllowToDeleteEmpOTAssignment", enUserPriviledge.AllowToDeleteEmpOTAssignment)
                privilegeDict.Add("_AllowToCancelEmpOTRequisition", enUserPriviledge.AllowToCancelEmpOTRequisition)
                privilegeDict.Add("_AllowToUnlockEmployeeInPlanning", enUserPriviledge.AllowToUnlockEmployeeInPlanning)
                privilegeDict.Add("_AllowToUnlockEmployeeInAssessment", enUserPriviledge.AllowToUnlockEmployeeInAssessment)
                privilegeDict.Add("_AllowToPostOTRequisitionToPayroll", enUserPriviledge.AllowToPostOTRequisitionToPayroll)
                privilegeDict.Add("_AllowToMigrateOTRequisitionApprover", enUserPriviledge.AllowToMigrateOTRequisitionApprover)
                privilegeDict.Add("_AllowToViewCalibratorList", enUserPriviledge.AllowToViewCalibratorList)
                privilegeDict.Add("_AllowToAddCalibrator", enUserPriviledge.AllowToAddCalibrator)
                privilegeDict.Add("_AllowToMakeCalibratorActive", enUserPriviledge.AllowToMakeCalibratorActive)
                privilegeDict.Add("_AllowToDeleteCalibrator", enUserPriviledge.AllowToDeleteCalibrator)
                privilegeDict.Add("_AllowToMakeCalibratorInactive", enUserPriviledge.AllowToMakeCalibratorInactive)
                privilegeDict.Add("_AllowToEditCalibrator", enUserPriviledge.AllowToEditCalibrator)
                privilegeDict.Add("_AllowToEditCalibrationApproverMaster", enUserPriviledge.AllowToEditCalibrationApproverMaster)
                privilegeDict.Add("_AllowtoViewGlobalInterviewAnalysisList", enUserPriviledge.AllowtoViewGlobalInterviewAnalysisList)
                privilegeDict.Add("_AllowToInactiveEmployeeCostCenter", enUserPriviledge.AllowToInactiveEmployeeCostCenter)
                privilegeDict.Add("_AllowToInactiveCompanyAccountConfiguration", enUserPriviledge.AllowToInactiveCompanyAccountConfiguration)
                privilegeDict.Add("_AllowToInactiveEmployeeAccountConfiguration", enUserPriviledge.AllowToInactiveEmployeeAccountConfiguration)
                privilegeDict.Add("_AllowToInactiveCostCenterAccountConfiguration", enUserPriviledge.AllowToInactiveCostCenterAccountConfiguration)
                privilegeDict.Add("_AllowToAddPotentialTalentEmployee", enUserPriviledge.AllowToAddPotentialTalentEmployee)
                privilegeDict.Add("_AllowToApproveRejectTalentPipelineEmployee", enUserPriviledge.AllowToApproveRejectTalentPipelineEmployee)
                privilegeDict.Add("_AllowToMoveApprovedEmployeeToQulified", enUserPriviledge.AllowToMoveApprovedEmployeeToQulified)
                privilegeDict.Add("_AllowToViewPotentialTalentEmployee", enUserPriviledge.AllowToViewPotentialTalentEmployee)
                privilegeDict.Add("_AllowToRemoveTalentProcess", enUserPriviledge.AllowToRemoveTalentProcess)
                privilegeDict.Add("_AllowToAddPotentialSuccessionEmployee", enUserPriviledge.AllowToAddPotentialSuccessionEmployee)
                privilegeDict.Add("_AllowToApproveRejectSuccessionPipelineEmployee", enUserPriviledge.AllowToApproveRejectSuccessionPipelineEmployee)
                privilegeDict.Add("_AllowToMoveApprovedEmployeeToQulifiedSuccession", enUserPriviledge.AllowToMoveApprovedEmployeeToQulifiedSuccession)
                privilegeDict.Add("_AllowToViewPotentialSuccessionEmployee", enUserPriviledge.AllowToViewPotentialSuccessionEmployee)
                privilegeDict.Add("_AllowToRemoveSuccessionProcess", enUserPriviledge.AllowToRemoveSuccessionProcess)
                privilegeDict.Add("_AllowToSendNotificationToTalentScreener", enUserPriviledge.AllowToSendNotificationToTalentScreener)
                privilegeDict.Add("_AllowToSendNotificationToTalentEmployee", enUserPriviledge.AllowToSendNotificationToTalentEmployee)
                privilegeDict.Add("_AllowForTalentScreeningProcess", enUserPriviledge.AllowForTalentScreeningProcess)
                privilegeDict.Add("_AllowToSendNotificationToSuccessionScreener", enUserPriviledge.AllowToSendNotificationToSuccessionScreener)
                privilegeDict.Add("_AllowToSendNotificationToSuccessionEmployee", enUserPriviledge.AllowToSendNotificationToSuccessionEmployee)
                privilegeDict.Add("_AllowForSuccessionScreeningProcess", enUserPriviledge.AllowForSuccessionScreeningProcess)
                privilegeDict.Add("_AllowToSendNotificationToTalentApprover", enUserPriviledge.AllowToSendNotificationToTalentApprover)
                privilegeDict.Add("_AllowToSendNotificationToSuccessionApprover", enUserPriviledge.AllowToSendNotificationToSuccessionApprover)
                privilegeDict.Add("_AllowToViewLearningAndDevelopmentPlan", enUserPriviledge.AllowToViewLearningAndDevelopmentPlan)
                privilegeDict.Add("_AllowToAddPersonalAnalysisGoal", enUserPriviledge.AllowToAddPersonalAnalysisGoal)
                privilegeDict.Add("_AllowToEditPersonalAnalysisGoal", enUserPriviledge.AllowToEditPersonalAnalysisGoal)
                privilegeDict.Add("_AllowToDeletePersonalAnalysisGoal", enUserPriviledge.AllowToDeletePersonalAnalysisGoal)
                privilegeDict.Add("_AllowToAddDevelopmentActionPlan", enUserPriviledge.AllowToAddDevelopmentActionPlan)
                privilegeDict.Add("_AllowToEditDevelopmentActionPlan", enUserPriviledge.AllowToEditDevelopmentActionPlan)
                privilegeDict.Add("_AllowToDeleteDevelopmentActionPlan", enUserPriviledge.AllowToDeleteDevelopmentActionPlan)
                privilegeDict.Add("_AllowToAddUpdateProgressForDevelopmentActionPlan", enUserPriviledge.AllowToAddUpdateProgressForDevelopmentActionPlan)
                privilegeDict.Add("_AllowToNominateEmployeeForSuccession", enUserPriviledge.AllowToNominateEmployeeForSuccession)
                privilegeDict.Add("_AllowToModifyTalentSetting", enUserPriviledge.AllowToModifyTalentSetting)
                privilegeDict.Add("_AllowToViewTalentSetting", enUserPriviledge.AllowToViewTalentSetting)
                privilegeDict.Add("_AllowToModifySuccessionSetting", enUserPriviledge.AllowToModifySuccessionSetting)
                privilegeDict.Add("_AllowToViewSuccessionSetting", enUserPriviledge.AllowToViewSuccessionSetting)
                privilegeDict.Add("_AllowToAdjustExpenseBalance", enUserPriviledge.AllowToAdjustExpenseBalance)
                privilegeDict.Add("_AllowToViewRetireClaimApplication", enUserPriviledge.AllowToViewRetireClaimApplication)
                privilegeDict.Add("_AllowToRetireClaimApplication", enUserPriviledge.AllowToRetireClaimApplication)
                privilegeDict.Add("_AllowToEditRetiredClaimApplication", enUserPriviledge.AllowToEditRetiredClaimApplication)
                privilegeDict.Add("_AllowToViewRetiredApplicationApproval", enUserPriviledge.AllowToViewRetiredApplicationApproval)
                privilegeDict.Add("_AllowToApproveRetiredApplication", enUserPriviledge.AllowToApproveRetiredApplication)
                privilegeDict.Add("_AllowToPostRetiredApplicationtoPayroll", enUserPriviledge.AllowToPostRetiredApplicationtoPayroll)
                privilegeDict.Add("_AllowToViewTalentScreeningDetail", enUserPriviledge.AllowToViewTalentScreeningDetail)
                privilegeDict.Add("_AllowToViewSuccessionScreeningDetail", enUserPriviledge.AllowToViewSuccessionScreeningDetail)
                privilegeDict.Add("_AllowToViewDepartmentalTrainingNeed", enUserPriviledge.AllowToViewDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToAddDepartmentalTrainingNeed", enUserPriviledge.AllowToAddDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToEditDepartmentalTrainingNeed", enUserPriviledge.AllowToEditDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToDeleteDepartmentalTrainingNeed", enUserPriviledge.AllowToDeleteDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToSubmitForApprovalFromDepartmentalTrainingNeed", enUserPriviledge.AllowToSubmitForApprovalFromDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToTentativeApproveForDepartmentalTrainingNeed", enUserPriviledge.AllowToTentativeApproveForDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToSubmitForApprovalFromTrainingBacklog", enUserPriviledge.AllowToSubmitForApprovalFromTrainingBacklog)
                privilegeDict.Add("_AllowToFinalApproveDepartmentalTrainingNeed", enUserPriviledge.AllowToFinalApproveDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToRejectDepartmentalTrainingNeed", enUserPriviledge.AllowToRejectDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed", enUserPriviledge.AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToUnlockSubmittedForTrainingBudgetApproval", enUserPriviledge.AllowToUnlockSubmittedForTrainingBudgetApproval)
                privilegeDict.Add("_AllowToAskForReviewTrainingAsPerAmountSet", enUserPriviledge.AllowToAskForReviewTrainingAsPerAmountSet)
                privilegeDict.Add("_AllowToUndoApprovedTrainingBudgetApproval", enUserPriviledge.AllowToUndoApprovedTrainingBudgetApproval)
                privilegeDict.Add("_AllowToAddCommentForDevelopmentActionPlan", enUserPriviledge.AllowToAddCommentForDevelopmentActionPlan)
                privilegeDict.Add("_AllowToViewActionPlanAssignedTrainingCourses", enUserPriviledge.AllowToViewActionPlanAssignedTrainingCourses)
                privilegeDict.Add("_AllowToUndoRejectedTrainingBudgetApproval", enUserPriviledge.AllowToUndoRejectedTrainingBudgetApproval)
                privilegeDict.Add("_AllowToViewBudgetSummaryForDepartmentalTrainingNeed", enUserPriviledge.AllowToViewBudgetSummaryForDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToSetMaxBudgetForDepartmentalTrainingNeed", enUserPriviledge.AllowToSetMaxBudgetForDepartmentalTrainingNeed)
                privilegeDict.Add("_AllowToApproveTrainingRequestRelatedToCost", enUserPriviledge.AllowToApproveTrainingRequestRelatedToCost)
                privilegeDict.Add("_AllowToApproveTrainingRequestForeignTravelling", enUserPriviledge.AllowToApproveTrainingRequestForeignTravelling)
                privilegeDict.Add("_AllowToUnlockPDPForm", enUserPriviledge.AllowToUnlockPDPForm)
                privilegeDict.Add("_AllowToLockPDPForm", enUserPriviledge.AllowToLockPDPForm)
                privilegeDict.Add("_AllowToUndoUpdateProgressForDevelopmentActionPlan", enUserPriviledge.AllowToUndoUpdateProgressForDevelopmentActionPlan)
                privilegeDict.Add("_AllowToApproveRejectTrainingCompletion", enUserPriviledge.AllowToApproveRejectTrainingCompletion)
                privilegeDict.Add("_AllowToAddAttendedTraining", enUserPriviledge.AllowToAddAttendedTraining)
                privilegeDict.Add("_AllowToMarkTrainingAsComplete", enUserPriviledge.AllowToMarkTrainingAsComplete)
                privilegeDict.Add("_AllowToAddRebateForNonEmployee", enUserPriviledge.Allowtoaddrebatefornonemployee)
                privilegeDict.Add("_AllowToApproveTrainingRequestLocalTravelling", enUserPriviledge.AllowToApproveTrainingRequestLocalTravelling)
                privilegeDict.Add("_AllowToViewTrainingApproverEmployeeMappingList", enUserPriviledge.AllowToViewTrainingApproverEmployeeMappingList)
                privilegeDict.Add("_AllowToEditTrainingApproverEmployeeMapping", enUserPriviledge.AllowToEditTrainingApproverEmployeeMapping)
                privilegeDict.Add("_AllowToDeleteTrainingApproverEmployeeMapping", enUserPriviledge.AllowToDeleteTrainingApproverEmployeeMapping)
                privilegeDict.Add("_AllowToSetActiveInactiveTrainingApproverEmployeeMapping", enUserPriviledge.AllowToSetActiveInactiveTrainingApproverEmployeeMapping)
                privilegeDict.Add("_AllowToAddTrainingApproverEmployeeMapping", enUserPriviledge.AllowToAddTrainingApproverEmployeeMapping)
                privilegeDict.Add("_AllowToChangeDeductionPeriodOnLoanApproval", enUserPriviledge.AllowToChangeDeductionPeriodonLoanApproval)
                privilegeDict.Add("_AllowToAccessLoanDisbursementDashboard", enUserPriviledge.AllowToAccessLoanDisbursementDashboard)
                privilegeDict.Add("_AllowToGetCRBData", enUserPriviledge.AllowToGetCRBData)
                privilegeDict.Add("_AllowToSetDisbursementTranches", enUserPriviledge.AllowToSetDisbursementTranches)
                privilegeDict.Add("_AllowToViewLoanSchemeRoleMapping", enUserPriviledge.AllowToViewLoanSchemeRoleMapping)
                privilegeDict.Add("_AllowToaddLoanSchemeRoleMapping", enUserPriviledge.AllowToAddLoanSchemeRoleMapping)
                privilegeDict.Add("_AllowToEditLoanSchemeRoleMapping", enUserPriviledge.AllowToEditLoanSchemeRoleMapping)
                privilegeDict.Add("_AllowToDeleteLoanSchemeRoleMapping", enUserPriviledge.AllowToDeleteLoanSchemeRoleMapping)
                privilegeDict.Add("_AllowToViewLoanExposuresOnLoanApproval", enUserPriviledge.AllowToViewLoanExposuresOnLoanApproval)
                privilegeDict.Add("_AllowToSendCandidateFeedback", enUserPriviledge.AllowToSendCandidateFeedback)
                privilegeDict.Add("_AllowToDownloadAptitudeResult", enUserPriviledge.AllowToDownloadAptitudeResult)
                privilegeDict.Add("_AllowToViewStaffLoanBalance", enUserPriviledge.AllowToViewStaffLoanBalance)
                privilegeDict.Add("_AllowToApproveVacancyChanges", enUserPriviledge.AllowToApproveVacancy)
                privilegeDict.Add("_AllowToApproveRejectPaymentBatchPosting", enUserPriviledge.AllowToApproveRejectPaymentBatchPosting)
                privilegeDict.Add("_AllowToViewStaffTransferApproverLevel", enUserPriviledge.AllowToViewStaffTransferApproverLevel)
                privilegeDict.Add("_AllowToAddStaffTransferApproverLevel", enUserPriviledge.AllowToAddStaffTransferApproverLevel)
                privilegeDict.Add("_AllowToEditStaffTransferApproverLevel", enUserPriviledge.AllowToEditStaffTransferApproverLevel)
                privilegeDict.Add("_AllowToDeleteStaffTransferApproverLevel", enUserPriviledge.AllowToDeleteStaffTransferApproverLevel)
                privilegeDict.Add("_AllowToViewStaffTransferLevelRoleMapping", enUserPriviledge.AllowToViewStaffTransferLevelRoleMapping)
                privilegeDict.Add("_AllowToAddStaffTransferLevelRoleMapping", enUserPriviledge.AllowToAddStaffTransferLevelRoleMapping)
                privilegeDict.Add("_AllowToEditStaffTransferLevelRoleMapping", enUserPriviledge.AllowToEditStaffTransferLevelRoleMapping)
                privilegeDict.Add("_AllowToDeleteStaffTransferLevelRoleMapping", enUserPriviledge.AllowToDeleteStaffTransferLevelRoleMapping)
                privilegeDict.Add("_AllowToViewStaffTransferApprovalList", enUserPriviledge.AllowToViewStaffTransferApprovalList)
                privilegeDict.Add("_AllowToApproveStaffTransferApproval", enUserPriviledge.AllowToApproveStaffTransferApproval)
                privilegeDict.Add("_AllowToViewLoanTrancheApprovalList", enUserPriviledge.AllowToViewLoanTrancheApprovalList)
                privilegeDict.Add("_AllowToApproveLoanTrancheApplication", enUserPriviledge.AllowToApproveLoanTrancheApplication)
                privilegeDict.Add("_AllowToAttachDocumentOnLoanTrancheApproval", enUserPriviledge.AllowToAttachDocumentOnLoanTrancheApproval)
                privilegeDict.Add("_AllowToChangeDeductionPeriodonLoanTrancheApproval", enUserPriviledge.AllowToChangeDeductionPeriodonLoanTrancheApproval)
                privilegeDict.Add("_AllowToViewCoachingApproverLevel", enUserPriviledge.AllowToViewCoachingApproverLevel)
                privilegeDict.Add("_AllowToAddCoachingApproverLevel", enUserPriviledge.AllowToAddCoachingApproverLevel)
                privilegeDict.Add("_AllowToEditCoachingApproverLevel", enUserPriviledge.AllowToEditCoachingApproverLevel)
                privilegeDict.Add("_AllowToDeleteCoachingApproverLevel", enUserPriviledge.AllowToDeleteCoachingApproverLevel)
                privilegeDict.Add("_AllowToFillCoachingForm", enUserPriviledge.AllowToFillCoachingForm)
                privilegeDict.Add("_AllowToApproveCoachingForm", enUserPriviledge.AllowToApproveCoachingForm)
                privilegeDict.Add("_AllowToEditCoachingForm", enUserPriviledge.AllowToEditCoachingForm)
                privilegeDict.Add("_AllowToDeleteCoachingForm", enUserPriviledge.AllowToDeleteCoachingForm)
                privilegeDict.Add("_AllowToViewCoachingForm", enUserPriviledge.AllowToViewCoachingForm)
                privilegeDict.Add("_AllowToFillReplacementForm", enUserPriviledge.AllowToFillReplacementForm)
                privilegeDict.Add("_Show_ForcastedWorkAnniversary", enUserPriviledge.ShowForcastedWorkAnniversary)
                privilegeDict.Add("_Show_ForcastedWorkResidencePermitExpiry", enUserPriviledge.ShowForcastedWorkResidencePermitExpiry)
                privilegeDict.Add("_ShowTotalLeaversFromCurrentFY", enUserPriviledge.ShowTotalLeaversFromCurrentFY)
                privilegeDict.Add("_AllowToAttachDocumentOnRolebaseLoanApproval", enUserPriviledge.AllowToAttachDocumentOnRolebaseLoanApproval)
                privilegeDict.Add("_AllowToAddPrecedentSubsequentRemarks", enUserPriviledge.AllowToAddPrecedentSubsequentRemarks)
                privilegeDict.Add("_AllowToPreviewOfferLetterOnLoanApproval", enUserPriviledge.AllowToPreviewOfferLetterOnLoanApproval)
                privilegeDict.Add("_AllowToViewExpenseCategoryList", enUserPriviledge.AllowToViewExpenseCategoryList)
                privilegeDict.Add("_AllowToAddExpenseCategory", enUserPriviledge.AllowToAddExpenseCategory)
                privilegeDict.Add("_AllowToEditExpenseCategory", enUserPriviledge.AllowToEditExpenseCategory)
                privilegeDict.Add("_AllowToDeleteExpenseCategory", enUserPriviledge.AllowToDeleteExpenseCategory)
                privilegeDict.Add("_AllowToShowExemptionDates", enUserPriviledge.AllowToShowExemptionDates)
                privilegeDict.Add("_AllowToApproveEmployeeClearance", enUserPriviledge.AllowToApproveEmployeeClearance)
                Session("privilegeDict") = privilegeDict

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub GetConfigPropsDict()
        Dim propertyDict As Dictionary(Of String, String) = Nothing
        Try

            If Session("ConfigPropsDict") Is Nothing Then

                propertyDict = New Dictionary(Of String, String)

                propertyDict.Add("_13thPayHeadId", "13thPayHeadID")
                propertyDict.Add("_Accounting_Country", "Accounting_Country")
                propertyDict.Add("_Accounting_JournalType", "Accounting_JournalType")
                propertyDict.Add("_Accounting_JVGroupCode", "Accounting_JVGroupCode")
                propertyDict.Add("_Accounting_TransactionReference", "Accounting_TransactionReference")
                propertyDict.Add("_AccountingSoftWare", "AccountingSoftWare")
                propertyDict.Add("_AccruedLeavePayHeadId", "AccruedLeavePayHeadID")
                propertyDict.Add("_ActiveDirectoryNotificationUserIds", "ActiveDirectoryNotificationUserIds")
                propertyDict.Add("_ActualHeadId", "ActualHeadID")
                propertyDict.Add("_AddEditTransectionHeadNotificationUserIds", "AddEditTransectionHeadNotificationUserIds")
                propertyDict.Add("_AddEditUserNotificationUserIds", "AddEditUserNotificationUserIds")
                propertyDict.Add("_AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory", "AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory")
                propertyDict.Add("_AdditionalStaffRequisitionDocsAttachmentMandatory", "AdditionalStaffRequisitionDocsAttachmentMandatory")
                propertyDict.Add("_ADDomain", "ADDomain")
                propertyDict.Add("_ADDomainUser", "ADDomainUser")
                propertyDict.Add("_ADDomainUserPwd", "ADDomainUserPwd")
                propertyDict.Add("_AddProceedingAgainstEachCount", "ProceedingAgainstEachCount")
                propertyDict.Add("_Address1MandatoryInRecruitment", "RecAddress1Mandatory")
                propertyDict.Add("_Address2MandatoryInRecruitment", "RecAddress2Mandatory")
                propertyDict.Add("_ADIPAddress", "ADIPAddress")
                propertyDict.Add("_AdminEmail", "AdminEmail")
                propertyDict.Add("_AdministratorEmailForError", "AdministratorEmailForError")
                propertyDict.Add("_ADPortNo", "ADPortNo")
                propertyDict.Add("_Advance_CostCenterunkid", "Advance_CostCenterunkid")
                propertyDict.Add("_Advance_DontAllowAfterDays", "Advance_DontAllowAfterDays")
                propertyDict.Add("_Advance_NetPayPercentage", "Advance_NetPayPercentage")
                propertyDict.Add("_Advance_NetPayTranheadUnkid", "Advance_NetPayTranheadUnkid")
                propertyDict.Add("_Allocation_Hierarchy", "Allocation_Hierarchy")
                propertyDict.Add("_AllocationIdx", "AllocationIdx")
                propertyDict.Add("_AllocationWorkStation", "AllocationWorkStation")
                propertyDict.Add("_AllowActivityHoursByPercentage", "AllowActivityHoursByPercentage")
                propertyDict.Add("_AllowAddDependants", "ADDDEPENDANTS")
                propertyDict.Add("_AllowAddExperience", "ADDEXPERIENCE")
                propertyDict.Add("_AllowAddIdentity", "ADDIDENTITY")
                propertyDict.Add("_AllowAddMembership", "ADDMEMBERSHIP")
                propertyDict.Add("_AllowAddQualifications", "ADDQUALIFICATION")
                propertyDict.Add("_AllowAddReference", "ADDREFERENCE")
                propertyDict.Add("_AllowAddSkills", "ADDSKILLS")
                propertyDict.Add("_AllowApplyOTForEachMonthDay", "AllowApplyOTMonthDay")
                propertyDict.Add("_AllowAssessor_Before_Emp", "AllowAssessor_Before_Emp")
                propertyDict.Add("_AllowChangeCompanyEmail", "CHANGECOMPANYEMAIL")
                propertyDict.Add("_AllowDeleteDependants", "DELETEDEPENDANTS")
                propertyDict.Add("_AllowDeleteExperience", "DELETEEXPERIENCE")
                propertyDict.Add("_AllowDeleteIdentity", "DELETEIDENTITY")
                propertyDict.Add("_AllowDeleteMembership", "DELETEMEMBERSHIP")
                propertyDict.Add("_AllowDeleteQualifications", "DELETEQUALIFICATION")
                propertyDict.Add("_AllowDeleteReference", "DELETEREFERENCE")
                propertyDict.Add("_AllowDeleteSkills", "DELETESKILLS")
                propertyDict.Add("_AllowEditAddress", "EDITADDRESS")
                propertyDict.Add("_AllowEditDependants", "EDITDEPENDANTS")
                propertyDict.Add("_AllowEditEmergencyAddress", "EDITEMERGENCYADDRESS")
                propertyDict.Add("_AllowEditExperience", "EDITEXPERIENCE")
                propertyDict.Add("_AllowEditIdentity", "EDITIDENTITY")
                propertyDict.Add("_AllowEditMembership", "EDITMEMBERSHIP")
                propertyDict.Add("_AllowEditPersonalInfo", "EDITPERSONALINFO")
                propertyDict.Add("_AllowEditQualifications", "EDITQUALIFICATION")
                propertyDict.Add("_AllowEditReference", "EDITREFERENCE")
                propertyDict.Add("_AllowEditSkills", "EDITSKILLS")
                propertyDict.Add("_AllowEmpAssignedProjectExceedTime", "AllowEmpAssignedProjectExceedTime")
                propertyDict.Add("_AllowEmployeeToAddEditSignatureESS", "AllowEmployeeToAddEditSignatureESS")
                propertyDict.Add("_AllowEmpSystemAccessOnActualEOCRetirementDate", "AllowEmpSystemAccessOnActualEOCRetirementDate")
                propertyDict.Add("_AllowFullSalaryHiredWithinFirstDays", "AllowFullSalaryHiredWithinFirstDays")
                propertyDict.Add("_AllowFullSalaryterminatedAfterFirstDays", "AllowFullSalaryterminatedAfterFirstDays")
                propertyDict.Add("_AllowLvApplicationApplyForBackDates", "AllowLvApplicationApplyForBackDates")
                propertyDict.Add("_AllowOnlyOneExpenseInClaimApplication", "AllowOnlyOneExpenseInClaimApplication")
                propertyDict.Add("_AllowOverTimeToEmpTimesheet", "AllowOverTimeToEmpTimesheet")
                propertyDict.Add("_AllowPartialApproveRejectObjectives", "AllowPartialApproveRejectObjectives")
                propertyDict.Add("_AllowPeriodicReview", "AllowPeriodicReview")
                propertyDict.Add("_AllowRehireOnClosedPeriod", "AllowRehireOnClosedPeriod")
                propertyDict.Add("_AllowRejectedProgressUpdateToEdit", "AllowRejectedProgressUpdateToEdit")
                propertyDict.Add("_AllowTerminationIfPaymentDone", "AllowTerminationIfPaymentDone")
                propertyDict.Add("_AllowToAddEditEmployeeDomicileAddress", "ADDEDITEMPLOYEEDOMICILEADDRESS")
                propertyDict.Add("_AllowToAddEditEmployeePresentAddress", "ADDEDITEMPLOYEEPRESENTADDRESS")
                propertyDict.Add("_AllowToAddEditEmployeeRecruitmentAddress", "ADDEDITEMPLOYEERECRUITMENTADDRESS")
                propertyDict.Add("_AllowToAddEditImageForESS", "AddEditImageForESS")
                propertyDict.Add("_AllowToAddTrainingwithoutTNAProcess", "AllowToAddTrainingwithoutTNAProcess")
                propertyDict.Add("_AllowToApplyRequestTrainingNotinTrainingPlan", "AllowToApplyRequestTrainingNotinTrainingPlan")
                propertyDict.Add("_AllowToCancelLeaveButRetainExpense", "AllowToCancelLeaveButRetainExpense")
                propertyDict.Add("_AllowToCancelLeaveForClosedPeriod", "AllowToCancelLeaveForClosedPeriod")
                propertyDict.Add("_AllowToChangeAttendanceAfterPayment", "AllowTochangeAttendanceAfterPayment")
                propertyDict.Add("_AllowtocloseCaseWO_Proceedings", "AllowToCloseCaseWOProceedings")
                propertyDict.Add("_AllowToClosePeriodBefore", "AllowToClosePeriodBefore")
                propertyDict.Add("_AllowToClosePeriodIfBdgtTimesheetsNotApproved", "AllowToClosePeriodIfBdgtTimesheetsNotApproved")
                propertyDict.Add("_AllowToDeleteLeaveApplication", "AllowToDeleteLeaveApplication")
                propertyDict.Add("_AllowToEditLeaveApplication", "AllowToEditLeaveApplication")
                propertyDict.Add("_AllowToExceedTimeAssignedToActivity", "AllowToExceedTimeAssignedToActivity")
                propertyDict.Add("_AllowToIncludeCaseInfoHearingNotification", "AllowToIncludeCaseInfoHearingNotification")
                propertyDict.Add("_AllowToNotifyChargedEmployeeOfHearing", "AllowToNotifyChargedEmployeeOfHearing")
                propertyDict.Add("_AllowToSetRelieverOnLvFormForESS", "AllowToSetRelieverOnLvFormForESS")
                propertyDict.Add("_AllowToSyncJobWithVacancyMaster", "AllowToSyncJobWithVacancyMaster")
                propertyDict.Add("_AllowToViewEDDetailReport", "VIEWEDDETAILREPORT")
                propertyDict.Add("_AllowToviewEmpTimesheetReportForESS", "AllowToviewEmpTimesheetReportForESS")
                propertyDict.Add("_AllowToViewESSVisibleWhileFilligDisciplineCharge", "AllowToViewESSVisibleWhileFilligDisciplineCharge")
                propertyDict.Add("_AllowToviewPaysliponEss", "AllowToviewPaysliponESS")
                propertyDict.Add("_AllowToViewPersonalSalaryCalculationReport", "VIEWPERSONALSALARYCALCULATIONREPORT")
                propertyDict.Add("_AllowToViewScoreWhileDoingAssesment", "AllowToViewScoreWhileDoingAssesment")
                propertyDict.Add("_AllowtoViewSignatureESS", "AllowtoViewSignatureESS")
                propertyDict.Add("_AllowViewEmployeeScale", "VIEWEMPLOYEESCALE")
                propertyDict.Add("_AmtToBePaidHeadId", "AmtToBePaidHeadID")
                propertyDict.Add("_ApplicableLeaveStatus", "ApplicableLeaveStatus")
                propertyDict.Add("_ApplicantCodeNotype", "ApplicantCodeNoType")
                propertyDict.Add("_ApplicantCodePrifix", "ApplicantCodePrifix")
                propertyDict.Add("_ApplicantDeclaration", "APPLICANTDECLARATION")
                propertyDict.Add("_ApplicantQualificationSortByInRecruitment", "RecApplicantQualificationSortBy")
                propertyDict.Add("_ApplyMigrationEnforcement", "ApplyMigrationEnforcement")
                propertyDict.Add("_ApplyNonDisclosureDeclaration", "ApplyNonDisclosureDeclaration")
                propertyDict.Add("_ApplyPayPerActivity", "ApplyPayPerActivity")
                propertyDict.Add("_ApplyStaffRequisition", "ApplyStaffRequisition")
                propertyDict.Add("_ApplyTOTPForLoanApplicationApproval", "ApplyTOTPForLoanApplicationApproval")
                propertyDict.Add("_AppointeDateUserNotification", "AppointeDateUserNotification")
                propertyDict.Add("_ApprovedBy", "ApprovedBy")
                propertyDict.Add("_AptitudeDatabaseName", "AptitudeDatabaseName")
                propertyDict.Add("_AptitudeDatabaseServer", "AptitudeDatabaseServer")
                propertyDict.Add("_AptitudePortNo", "AptitudePortNo")
                propertyDict.Add("_AptitudeUserName", "AptitudeUserName")
                propertyDict.Add("_AptitudeUserPwd", "AptitudeUserPwd")
                propertyDict.Add("_ArutiSelfServiceURL", "ArutiSelfServiceURL")
                propertyDict.Add("_ASR_Leave1", "ASR_Leave1")
                propertyDict.Add("_ASR_Leave2", "ASR_Leave2")
                propertyDict.Add("_ASR_Leave3", "ASR_Leave3")
                propertyDict.Add("_ASR_Leave4", "ASR_Leave4")
                propertyDict.Add("_ASR_Leave5", "ASR_Leave5")
                propertyDict.Add("_ASR_LvType", "ASR_LvType")
                propertyDict.Add("_ASR_ShowTotalHrs", "ASR_ShowTotalHrs")
                propertyDict.Add("_AssessableIncomeHeadID", "AssessableIncomeHeadID")
                propertyDict.Add("_Assessment_Instructions", "Assessment_Instructions")
                propertyDict.Add("_AssessmentReportTemplateId", "AssessmentReportTemplateId")
                propertyDict.Add("_AssetDeclarationFinalSavedNotificationUserIds", "AssetDeclarationFinalSavedNotificationUserIds")
                propertyDict.Add("_AssetDeclarationFromDate", "AssetDeclarationFromDate")
                propertyDict.Add("_AssetDeclarationInstruction", "AssetDeclarationInstruction")
                propertyDict.Add("_AssetDeclarationTemplate", "AssetDeclarationTemplate")
                propertyDict.Add("_AssetDeclarationToDate", "AssetDeclarationToDate")
                propertyDict.Add("_AssetDeclarationUnlockFinalSavedNotificationUserIds", "AssetDeclarationUnlockFinalSavedNotificationUserIds")
                propertyDict.Add("_AssignByJobCompetencies", "AssignByJobCompetencies")
                propertyDict.Add("_AttachApplicantCV", "AttachApplicantCV")
                propertyDict.Add("_AuthenticationCode", "AuthenticationCode")
                propertyDict.Add("_AutoIncrementUsedDisplayName", "AutoIncrementUsedDisplayName")
                propertyDict.Add("_B_Area", "B_Area")
                propertyDict.Add("_BackupTime", "BackupTime")
                propertyDict.Add("_Base_CurrencyId", "BaseCurrencyId")
                propertyDict.Add("_BasicSalaryComputation", "BasicSalaryComputation")
                propertyDict.Add("_BatchPostingNotype", "BatchPostingNoType")
                propertyDict.Add("_BatchPostingPrifix", "BatchPostingPrefix")
                propertyDict.Add("_BatchSchedulingTemplateId", "BatchSchedulingTemplateId")
                propertyDict.Add("_BaudRate", "BaudRate")
                propertyDict.Add("_BgtRequestValidationP2PServiceURL", "BudgetRequestValidationP2PServiceURL")
                propertyDict.Add("_Biostar2DatabaseName", "Biostar2DatabaseName")
                propertyDict.Add("_Biostar2DatabasePassword", "Biostar2DatabasePassword")
                propertyDict.Add("_Biostar2DatabaseUserName", "Biostar2DatabaseUserName")
                propertyDict.Add("_Biostar2DataServerAddress", "Biostar2DataServerAddress")
                propertyDict.Add("_Biostar2PortNo", "Biostar2PortNo")
                propertyDict.Add("_BirthDateMandatoryInRecruitment", "RecBirthDayMandatory")
                propertyDict.Add("_BirthDateUserNotification", "BirthDateUserNotification")
                propertyDict.Add("_BRJVVocNoType", "BRJVVocNoType")
                propertyDict.Add("_BRJVVocPrefix", "BRJVVocPrefix")
                propertyDict.Add("_BSC_StatusColors", "_eStat_")
                propertyDict.Add("_CapexRequestCCP2PServiceURL", "CapexRequestCCP2PServiceURL")
                propertyDict.Add("_CapOTHrsForHODApprovers", "CapOTHrsForHODApprovers")
                propertyDict.Add("_CarLoanRequestFlexcubeURL", "CarLoanRequestFlexcubeURL")
                propertyDict.Add("_CascadingTypeId", "CascadingTypeId")
                propertyDict.Add("_CertificatePath", "CertificatePath")
                propertyDict.Add("_CFRoundingAbove", "CFRoundingAbove")
                propertyDict.Add("_Checked_Fields", "Checked_Columns")
                propertyDict.Add("_CheckedBy", "CheckedBy")
                propertyDict.Add("_CheckedMonthlyPayrollReport1HeadIds", "CheckedMonthlyPayrollReport1HeadIds")
                propertyDict.Add("_CheckedMonthlyPayrollReport2HeadIds", "CheckedMonthlyPayrollReport2HeadIds")
                propertyDict.Add("_CheckedMonthlyPayrollReport3HeadIds", "CheckedMonthlyPayrollReport3HeadIds")
                propertyDict.Add("_CheckedMonthlyPayrollReport4HeadIds", "CheckedMonthlyPayrollReport4HeadIds")
                propertyDict.Add("_CheckedMonthlyPayrollReportLeaveIds", "CheckedMonthlyPayrollReportLeaveIds")
                propertyDict.Add("_CityMandatoryInRecruitment", "RecCityMandatory")
                propertyDict.Add("_ClaimRemarkMandatoryForClaim", "ClaimRemarkMandatoryForClaim")
                propertyDict.Add("_ClaimRequestPrefix", "ClaimRequestPrefix")
                propertyDict.Add("_ClaimRequestVocNoType", "ClaimRequestVocNoType")
                propertyDict.Add("_ClaimRetirementFormNoPrifix", "ClaimRetirementFormNoPrifix")
                propertyDict.Add("_ClaimRetirementFormNoType", "ClaimRetirementFormNoType")
                propertyDict.Add("_ClaimRetirementTypeId", "ClaimRetirementTypeId")
                propertyDict.Add("_ClosePayrollPeriodIfLeaveIssue", "ClosePayrollPeriodIfLeaveIssue")
                propertyDict.Add("_ClosePeriodDaysBefore", "ClosePeriodDaysBefore")
                propertyDict.Add("_CmptScoringOptionId", "CmptScoringOptionId")
                propertyDict.Add("_ColWidth_InEvaluation", "ColWidth_InEvaluation")
                propertyDict.Add("_ColWidth_InPlanning", "ColWidth_InPlanning")
                propertyDict.Add("_CommentsMandatoryInRecruitment", "RecCommentsMandatory")
                propertyDict.Add("_CommunicationTypeId", "CommTypeId")
                propertyDict.Add("_CompanyAssetP2PServiceURL", "CompanyAssetP2PServiceURL")
                propertyDict.Add("_CompanyDateFormat", "CompanyDateFormat")
                propertyDict.Add("_CompanyDateSeparator", "CompanyDateSeparator")
                propertyDict.Add("_CompanyDomain", "CompanyDomain")
                propertyDict.Add("_CompanyNameJobHistoryMandatoryInRecruitment", "RecCompanyNameJobHistoryMandatory")
                propertyDict.Add("_CompetenciesAssess_Instructions", "CompetenciesAssessInstructions")
                propertyDict.Add("_ConfigDatabaseExportPath", "ConfigDatabaseExportPath")
                propertyDict.Add("_ConfirmationMonth", "ConfirmationMonth")
                propertyDict.Add("_ConfirmDateUserNotification", "ConfirmDateUserNotification")
                propertyDict.Add("_ConnectionTypeId", "ConnectionTypeId")
                propertyDict.Add("_ConsiderConstantAbsentForMisconduct", "ConsiderConstantAbsentForMisconduct")
                propertyDict.Add("_ConsiderItemWeightAsNumber", "ConsiderItemWeightAsNumber")
                propertyDict.Add("_ConsiderZeroAsNumUpdateProgress", "ConsiderZeroAsNumUpdateProgress")
                propertyDict.Add("_ConstantAbsentDaysForMisconduct", "ConstantAbsentDaysForMisconduct")
                propertyDict.Add("_CountAttendanceNotLVIfLVapplied", "CountAttendanceNotLVIfLVapplied")
                propertyDict.Add("_CountryMandatoryInRecruitment", "RecCountryMandatory")
                propertyDict.Add("_CountryUnkid", "CountryUnkid")
                propertyDict.Add("_CRBRequestFlexcubeURL", "CRBRequestFlexcubeURL")
                propertyDict.Add("_CreateADUserFromEmpMst", "CreateADUserFromEmpMst")
                propertyDict.Add("_CreditCardImportationPath", "CreditCardImportationPath")
                propertyDict.Add("_CumulativeTaxHeadId", "CumulativeTaxHeadID")
                propertyDict.Add("_Currency", "CurrencySign")
                propertyDict.Add("_CurrencyFormat", "CurrencyFormat")
                propertyDict.Add("_CurrentSalaryMandatoryInRecruitment", "RecCurrentSalaryMandatory")
                propertyDict.Add("_CustomPayrollReportAllocIds", "CustomPayrollReportAllocIds")
                propertyDict.Add("_CustomPayrollReportHeadsIds", "CustomPayrollReportHeadsIds")
                propertyDict.Add("_CustomPayrollReportTemplate2AllocIds", "CustomPayrollReportTemplate2AllocIds")
                propertyDict.Add("_CustomPayrollReportTemplate2HeadsIds", "CustomPayrollReportTemplate2HeadsIds")
                propertyDict.Add("_DailyBasicSalaryHeadId", "DailyBasicSalaryHeadID")
                propertyDict.Add("_DatabaseExportPath", "DatabaseExportPath")
                propertyDict.Add("_DatabaseName", "DatabaseName")
                propertyDict.Add("_DatabaseOwner", "DatabaseOwner")
                propertyDict.Add("_DatabaseServer", "DatabaseServer")
                propertyDict.Add("_DatabaseServerSetting", "DatabaseServerSetting")
                propertyDict.Add("_DatabaseUserName", "DatabaseUserName")
                propertyDict.Add("_DatabaseUserPassword", "DatabaseUserPassword")
                propertyDict.Add("_DatafileExportPath", "DatafileExportPath")
                propertyDict.Add("_DatafileName", "DatafileName")
                propertyDict.Add("_DaysAfterSelfEvaluationManagerEvaluation", "DaysAfterSelfEvaluationManagerEvaluation")
                propertyDict.Add("_DaysAfterTrainingEndDateEmployeeEvaluation", "DaysAfterTrainingEndDateEmployeeEvaluation")
                propertyDict.Add("_DaysAfterTrainingFeedbackInstruction", "DaysAfterTrainingFeedbackInstruction")
                propertyDict.Add("_DaysAfterTrainingStartDateEmployeeEvaluation", "DaysAfterTrainingStartDateEmployeeEvaluation")
                propertyDict.Add("_DaysForReminderEmailBeforeExpiryToCompleteTraining", "DaysForReminderEmailBeforeExpiryToCompleteTraining")
                propertyDict.Add("_DaysFromLastDateOfTrainingToAllowCompleteTraining", "DaysFromLastDateOfTrainingToAllowCompleteTraining")
                propertyDict.Add("_DefaultHearingNotificationType", "DefaultHearingNotificationType")
                propertyDict.Add("_DefaultSelfServiceTheme", "DefaultSelfServiceTheme")
                propertyDict.Add("_DependantDocsAttachmentMandatory", "DependantDocsAttachmentMandatory")
                propertyDict.Add("_DeptTrainingNeedListColumnsIDs", "DeptTrainingNeedListColumnsIDs")
                propertyDict.Add("_DescriptionMandatoryForActivity", "DescriptionMandatoryForActivity")
                propertyDict.Add("_DetailedSalaryBreakdownReportByCCenterGroupHeadsIds", "DetailedSalaryBreakdownReportByCCenterGroupHeadsIds")
                propertyDict.Add("_DetailedSalaryBreakdownReportHeadsIds", "DetailedSalaryBreakdownReportHeadsIds")
                propertyDict.Add("_DeviceId", "DeviceId")
                propertyDict.Add("_DisableADUserAgainAfterMins", "DisableADUserAgainAfterMins")
                propertyDict.Add("_DisciplineChargeNotification", "DisciplineChargeNotification")
                propertyDict.Add("_DisciplineNotifyChargeClosedUserIds", "DisciplineNotifyChargeClosedUserIds")
                propertyDict.Add("_DisciplineNotifyChargePostedUserIds", "DisciplineNotifyChargePostedUserIds")
                propertyDict.Add("_DisciplineNotifyProceedingUserIds", "DisciplineNotifyProceedingUserIds")
                propertyDict.Add("_DisciplineNotifyWarningUserIds", "DisciplineNotifyWarningUserIds")
                propertyDict.Add("_DisciplineProceedingNotification", "DisciplineProceedingNotification")
                propertyDict.Add("_DisciplineRefNoType", "DisciplineRefNoType")
                propertyDict.Add("_DisciplineRefPrefix", "DisciplineRefPrefix")
                propertyDict.Add("_DisplayCalibrationUserAllocationId", "DisplayCalibrationUserAllocationId")
                propertyDict.Add("_DisplayLogo", "DisplayLogo")
                propertyDict.Add("_DisplayNameSetting", "DisplayNameSetting")
                propertyDict.Add("_DisplayView", "DisplayView")
                propertyDict.Add("_Document_Path", "DocumentPath")
                propertyDict.Add("_DomicileDocsAttachmentMandatory", "DomicileAddressDocsAttachmentMandatory")
                propertyDict.Add("_DoNotAllowCurrentFutureDatesToApplyOT", "DoNotAllowCurrentFutureDatesToApplyOT")
                propertyDict.Add("_DoNotAllowOTEndTimeGreaterThanClockOutTime", "DoNotAllowOTEndTimeGreaterThanClockOutTime")
                propertyDict.Add("_DoNotAllowOverDeductionForPayment", "DoNotAllowOverDeductionPayment")
                propertyDict.Add("_DoNotAllowToApplyImprestIfUnRetiredForExpCategory", "DoNotAllowToApplyImprestIfUnRetiredForExpCategory")
                propertyDict.Add("_DonotAllowToRequestTrainingAfterDays", "DonotAllowToRequestTrainingAfterDays")
                propertyDict.Add("_DonotAttendanceinSeconds", "DonotAttendanceinSeconds")
                propertyDict.Add("_DontAllowAdvanceOnESS", "DontAllowAdvanceOnESS")
                propertyDict.Add("_DontAllowAssetDeclarationAfterDays", "DontAllowAssetDeclarationAfterDays")
                propertyDict.Add("_DontAllowPaymentIfNetPayGoesBelow", "DontAllowPaymentIfNetPayGoesBelow")
                propertyDict.Add("_DontAllowRatingBeyond100", "DontAllowRatingBeyond100")
                propertyDict.Add("_DontAllowToEditScoreGenbySys", "DontAllowToEditScoreGenbySys")
                propertyDict.Add("_DontShowPayslipOnESSIfPaymentNotAuthorized", "DontShowPayslipOnESSIfPaymentNotAuthorized")
                propertyDict.Add("_DontShowPayslipOnESSIfPaymentNotDone", "DontShowPayslipOnESSIfPaymentNotDone")
                propertyDict.Add("_EarliestPossibleStartDateMandatoryInRecruitment", "RecEarliestPossibleStartDateMandatory")
                propertyDict.Add("_EFT_Code", "EFTCode")
                propertyDict.Add("_EFTCitiDirectAddPaymentDetail", "EFTCitiDirectAddPaymentDetail")
                propertyDict.Add("_EFTCitiDirectChargesIndicator", "EFTCitiDirectChargesIndicator")
                propertyDict.Add("_EFTCitiDirectCountryCode", "EFTCitiDirectCountryCode")
                propertyDict.Add("_EFTCitiDirectSkipPriorityFlag", "EFTCitiDirectSkipPriorityFlag")
                propertyDict.Add("_EFTIntegration", "EFTIntegration")
                propertyDict.Add("_EFTMobileMoneyAccountName", "EFTMobileMoneyAccountName")
                propertyDict.Add("_EFTMobileMoneyAccountNo", "EFTMobileMoneyAccountNo")
                propertyDict.Add("_EFTMobileMoneyBank", "EFTMobileMoneyBank")
                propertyDict.Add("_EFTMobileMoneyBranch", "EFTMobileMoneyBranch")
                propertyDict.Add("_EFTReconciliationURL", "EFTReconciliationURL")
                propertyDict.Add("_EFTRequestClientID", "EFTRequestClientID")
                propertyDict.Add("_EFTRequestPassword", "EFTRequestPassword")
                propertyDict.Add("_EFTRequestURL", "EFTRequestURL")
                propertyDict.Add("_EFTVocNoType", "EFTVocNoType")
                propertyDict.Add("_EFTVocPrefix", "EFTVocPrefix")
                propertyDict.Add("_ELCmonths", "ELCMonths")
                propertyDict.Add("_EmpBankDetailsCompulsoryOnEmpMaster", "EmpBankDetailsCompulsoryOnEmpMaster")
                propertyDict.Add("_EmpBirthdayAllocation", "EmpBirthdayAllocation")
                propertyDict.Add("_EmplCostHeadId", "EmplCostHeadID")
                propertyDict.Add("_EmployeeApprovalNotificationUserIds", "EmployeeApprovalNotificationUserIds")
                propertyDict.Add("_EmployeeAsOnDate", "EmployeeAsOnDate")
                propertyDict.Add("_EmployeeCodeNotype", "EmployeeCodeNotype")
                propertyDict.Add("_EmployeeCodePrifix", "EmployeeCodePrifix")
                propertyDict.Add("_EmployeeDataRejectedUserIds", "_RejEmpData_")
                propertyDict.Add("_EmployeeDisagreeGrievanceNotificationEmailTitle", "EmployeeDisagreeGrievanceNotificationEmailTitle")
                propertyDict.Add("_EmployeeDisagreeGrievanceNotificationTemplateId", "EmployeeDisagreeGrievanceNotificationTemplateId")
                propertyDict.Add("_EmployeeDisagreeGrievanceNotificationUserIds", "EmployeeDisagreeGrievanceNotificationUserIds")
                propertyDict.Add("_EmployeeRejectNotificationTemplateId", "EmployeeRejectNotificationTemplateId")
                propertyDict.Add("_EmployeeRejectNotificationUserIds", "EmployeeRejectNotificationUserIds")
                propertyDict.Add("_EmployerMandatoryInRecruitment", "RecEmployerMandatory")
                propertyDict.Add("_EmpMandatoryFieldsIDs", "EmpMandatoryFieldsIDs")
                propertyDict.Add("_EmpNotificationForClaimApplicationByManager", "EmpNotificationForClaimApplicationbyManager")
                propertyDict.Add("_EmpNotificationTrancheDateBeforeDays", "EmpNotificationTrancheDateBeforeDays")
                propertyDict.Add("_EmpOnLeaveAllocation", "EmpOnLeaveAllocation")
                propertyDict.Add("_EmpStaffTurnOverAllocation", "EmpStaffTurnOverAllocation")
                propertyDict.Add("_EmpSubstantivePostBeforeExpiryDays", "EmpSubstantivePostBeforeExpiryDays")
                propertyDict.Add("_EmpSubstantivePostNotificationUserIds", "EmpSubstantivePostNotificationUserIds")
                propertyDict.Add("_EmpTeamMembersAllocation", "EmpTeamMembersAllocation")
                propertyDict.Add("_EmpTimesheetSetting", "EmpTimesheetSetting")
                propertyDict.Add("_EmpTnADetailsAllocation", "EmpTnADetailsAllocation")
                propertyDict.Add("_EmpWorkAnniversaryAllocation", "EmpWorkAnniversaryAllocation")
                propertyDict.Add("_EnableBSCAutomaticRating", "EnableBSCAutomaticRating")
                propertyDict.Add("_EnableExchangeMailBoxAfterMins", "EnableExchangeMailBoxAfterMins")
                propertyDict.Add("_EnableTraningRequisition", "EnableTraningRequisition")
                propertyDict.Add("_EnforceManpowerPlanning", "EnforceManpowerPlanning")
                propertyDict.Add("_EocDateUserNotification", "EocDateUserNotification")
                propertyDict.Add("_ERC_AssetDecFromDate_Setting", "ERC_ASSETDECFROMDATE_SETTING")
                propertyDict.Add("_ERC_AssetDecFromDate_TemplateId", "ERC_ASSETDECFROMDATE_TEMPLATEID")
                propertyDict.Add("_ERC_AssetDecToDate_Setting", "ERC_ASSETDECTODATE_SETTING")
                propertyDict.Add("_ERC_AssetDecToDate_TemplateId", "ERC_ASSETDECTODATE_TEMPLATEID")
                propertyDict.Add("_ERC_BRT_Setting", "ERC_BRT_Setting")
                propertyDict.Add("_ERC_BRT_TemplateId", "ERC_BRT_TemplateId")
                propertyDict.Add("_ERC_CNF_Setting", "ERC_CNF_Setting")
                propertyDict.Add("_ERC_CNF_TemplateId", "ERC_CNF_TemplateId")
                propertyDict.Add("_ERC_EOC_Setting", "ERC_EOC_Setting")
                propertyDict.Add("_ERC_EOC_TemplateId", "ERC_EOC_TemplateId")
                propertyDict.Add("_ERC_ID_Setting", "ERC_ID_Setting")
                propertyDict.Add("_ERC_ID_TemplateId", "ERC_ID_TemplateId")
                propertyDict.Add("_ERC_LeaveEnd_Setting", "ERC_LEAVEEND_SETTING")
                propertyDict.Add("_ERC_LeaveEnd_TemplateId", "ERC_LEAVEEND_TEMPLATEID")
                propertyDict.Add("_ERC_LeavePlanner_Setting", "ERC_LEAVEPLANNER_SETTING")
                propertyDict.Add("_ERC_LeavePlanner_TemplateId", "ERC_LEAVEPLANNER_TEMPLATEID")
                propertyDict.Add("_ERC_PAAsrNtfSetting", "PAAsrNtfSettingValue")
                propertyDict.Add("_ERC_PAEmpNtfSetting", "PAEmpNtfSettingValue")
                propertyDict.Add("_ERC_PARevNtfSetting", "PARevNtfSettingValue")
                propertyDict.Add("_ERC_PED_Setting", "ERC_PED_Setting")
                propertyDict.Add("_ERC_PED_TemplateId", "ERC_PED_TemplateId")
                propertyDict.Add("_ERC_PlanningNotSubmittedAssessorNtfDaysValue", "PlanningNotSubmittedAssessorNtfDaysValue")
                propertyDict.Add("_ERC_PlanningNotSubmittedReviewerNtfDaysValue", "PlanningNotSubmittedReviewerNtfDaysValue")
                propertyDict.Add("_ERC_ResendNft_Manually", "ERC_ResendNft_Manually")
                propertyDict.Add("_ERC_RET_Setting", "ERC_RET_Setting")
                propertyDict.Add("_ERC_RET_TemplateId", "ERC_RET_TemplateId")
                propertyDict.Add("_ERC_SendNtf_Manually", "ERC_SendNtf_Manually")
                propertyDict.Add("_ERC_UpdateProgNtfSettingValue", "UpdateProgNtfDaysValue")
                propertyDict.Add("_ExchangeServerDatabaseName", "ExchangeServerDatabaseName")
                propertyDict.Add("_ExchangeServerIP", "ExchangeServerIP")
                propertyDict.Add("_ExchangeServerUserName", "ExchangeServerUserName")
                propertyDict.Add("_ExchangeServerUserPassword", "ExchangeServerUserPassword")
                propertyDict.Add("_ExcludeEmpFromPayrollDuringSuspension", "ExcludeEmpFromPayrollDuringSuspension")
                propertyDict.Add("_ExcludeProbatedEmpOnPerformance", "ExcludeProbatedEmpOnPerformance")
                propertyDict.Add("_ExemptionDateUserNotification", "ExemptionDateUserNotification")
                propertyDict.Add("_ExpectedBenefitsMandatoryInRecruitment", "RecExpectedBenefitsMandatory")
                propertyDict.Add("_ExpectedSalaryMandatoryInRecruitment", "RecExpectedSalaryMandatory")
                propertyDict.Add("_ExportDataPath", "ExportDataPath")
                propertyDict.Add("_ExportReportPath", "ExportReportPath")
                propertyDict.Add("_FailedRequestNotificationEmails", "FailedRequestNotificationEmails")
                propertyDict.Add("_FileFormat", "FileFormat")
                propertyDict.Add("_FireDisciplineWarningNotification", "FireDisciplineWarningNotification")
                propertyDict.Add("_FirstCheckInLastCheckOut", "FirstCheckInLastCheckOut")
                propertyDict.Add("_FirstNamethenSurname", "FirstNamethenSurname")
                propertyDict.Add("_FlatRateHeadsComputation", "FlatRateHeadsComputation")
                propertyDict.Add("_FlexcubeAccountCategory", "FlexcubeAccountCategory")
                propertyDict.Add("_FlexcubeAccountClass", "FlexcubeAccountClass")
                propertyDict.Add("_FlexCubeJVPostedOracleNotificationUserIds", "FlexCubeJVPostedOracleNotificationUserIds")
                propertyDict.Add("_FlexcubeMsgIDNoType", "FlexcubeMsgIDNoType")
                propertyDict.Add("_FlexcubeMsgIDPrefix", "FlexcubeMsgIDPrefix")
                propertyDict.Add("_FlexcubeServiceCollection", "_FlxSrv_")
                propertyDict.Add("_FlexSrvRunningTime", "FlexSrvRunningTime")
                propertyDict.Add("_FollowEmployeeHierarchy", "FollowEmployeeHierarchy")
                propertyDict.Add("_Forcasted_ViewSetting", "ForcastedViewSetting")
                propertyDict.Add("_ForcastedBirthDateValue", "ForcastedBirthDateValue")
                propertyDict.Add("_ForcastedELCValue", "ForcastedELCValue")
                propertyDict.Add("_ForcastedELCViewSetting", "ForcastedELCViewSetting")
                propertyDict.Add("_ForcastedEOC_ViewSetting", "ForcastedEOCViewSetting")
                propertyDict.Add("_ForcastedEOCValue", "ForcastedEOCValue")
                propertyDict.Add("_ForcastedValue", "ForcastedValue")
                propertyDict.Add("_ForcastedWorkAnniversaryValue", "ForcastedWorkAnniversaryValue")
                propertyDict.Add("_ForcastedWorkResidencePermitExpiryValue", "ForcastedWorkResidencePermitExpiryValue")
                propertyDict.Add("_FRetirementBy", "FRetirementBy")
                propertyDict.Add("_FRetirementValue", "FRetirementValue")
                propertyDict.Add("_FullSalaryHiredWithinFirstDays", "FullSalaryHiredWithinFirstDays")
                propertyDict.Add("_FullSalaryTerminatedAfterDays", "FullSalaryTerminatedAfterDays")
                propertyDict.Add("_GenderMandatoryInRecruitment", "RecGenderMandatory")
                propertyDict.Add("_GetTokenP2PServiceURL", "GetTokenP2PServiceURL")
                propertyDict.Add("_GoalsAccomplishedRequiresApproval", "GoalsAccomplishedRequiresApproval")
                propertyDict.Add("_GoalTypeInclusion", "GoalTypeInclusion")
                propertyDict.Add("_GrievanceApprovalSetting", "GrievanceApprovalSetting")
                propertyDict.Add("_GrievanceRefNoType", "GrievanceRefNoType")
                propertyDict.Add("_GrievanceRefPrefix", "GrievanceRefPrefix")
                propertyDict.Add("_GrievanceReportingToMaxApprovalLevel", "GrievanceReportingToMaxApprovalLevel")
                propertyDict.Add("_HideRecruitementOtherInfoScreen", "HideRecruitementOtherInfoScreen")
                propertyDict.Add("_HideRecruitementPermanentAddress", "HideRecruitementPermanentAddress")
                propertyDict.Add("_HideRecruitementQualificationRemark", "HideRecruitementQualificationRemark")
                propertyDict.Add("_HideRecruitementSkillScreen", "HideRecruitementSkillScreen")
                propertyDict.Add("_HighestQualificationMandatoryInRecruitment", "RecHighestQualificationMandatory")
                propertyDict.Add("_HourAmtMapping", "HourAmtMapping")
                propertyDict.Add("_IdentityDocsAttachmentMandatory", "IdentityDocsAttachmentMandatory")
                propertyDict.Add("_IdentityPath", "IdentityPath")
                propertyDict.Add("_IgnoreNegativeNetPayEmployeesOnJV", "IgnoreNegativeNetPayEmployeesOnJV")
                propertyDict.Add("_IgnoreZeroValueHeadsOnPayslip", "IgnoreZeroValueHeadsOnPayslip")
                propertyDict.Add("_ImportCostCenterP2PServiceURL", "ImportCostCenterP2PServiceURL")
                propertyDict.Add("_IncludeCustomItemInPlanning", "IncludeCustomItemInPlanning")
                propertyDict.Add("_IncludeHolidayInOvertimeToEmpTimesheet", "IncludeHolidayInOvertimeToEmpTimesheet")
                propertyDict.Add("_IncludePendingApproverFormsInMonthlyPayrollReport", "MonthlyPayrollReportIncludePendingApproverForms")
                propertyDict.Add("_IncludeSystemRetirementMonthlyPayrollReport", "IncludeSystemRetirementMonthlyPayrollReport")
                propertyDict.Add("_IncludeWeekendHolidaysBudgetTimesheetApprovalNotification", "IncludeWeekendHolidaysBudgetTimesheetApprovalNotification")
                propertyDict.Add("_IncludeWeekendInOvertimeToEmpTimesheet", "IncludeWeekendInOvertimeToEmpTimesheet")
                propertyDict.Add("_InternalJobAlertTemplateId", "InternalJobAlertTemplateId")
                propertyDict.Add("_InterviewSchedulingTemplateId", "InterviewSchedulingTemplateId")
                propertyDict.Add("_IpAddress", "IpAddress")
                propertyDict.Add("_IPRCalculationHeadId", "IPRCalculationHeadID")
                propertyDict.Add("_IsAllocation_Hierarchy_Set", "IsAllocation_Hierarchy_Set")
                propertyDict.Add("_IsAllowCustomItemFinalSave", "ISALLOWCUSTOMITEMFINALSAVE")
                propertyDict.Add("_IsAllowFinalSave", "ISALLOWFINALSAVE")
                propertyDict.Add("_IsAllowToClosePeriod", "IsAllowToClosePeriod")
                propertyDict.Add("_IsAutomaticIssueOnFinalApproval", "IsAutomaticLeaveIssueOnFinalApproval")
                propertyDict.Add("_IsBSC_ByEmployee", "BSC_ByEmployee")
                propertyDict.Add("_IsBSCObjectiveSaved", "IsBSCObjectiveSaved")
                propertyDict.Add("_IsCalibrationSettingActive", "IsCalibrationSettingActive")
                propertyDict.Add("_IsCompanyNeedReviewer", "CompanyNeedReviewer")
                propertyDict.Add("_IsDailyBackupEnabled", "IsDailyBackupEnabled")
                propertyDict.Add("_IsDayOffConsiderOnWeekend", "IsDayOffConsiderOnWeekend")
                propertyDict.Add("_IsDenominationCompulsory", "IsDenominationCompulsory")
                propertyDict.Add("_IsDependant_AgeLimit_Set", "IsDependantAgeLimitMandatory")
                propertyDict.Add("_IsHolidayConsiderOnDayoff", "IsHolidayConsiderOnDayoff")
                propertyDict.Add("_IsHolidayConsiderOnWeekend", "IsHolidayConsiderOnWeekend")
                propertyDict.Add("_IsHRFlexcubeIntegrated", "IsHRFlexcubeIntegrated")
                propertyDict.Add("_IsIdSensitive", "IsIdSensitive")
                propertyDict.Add("_IsImgInDataBase", "IsImgInDataBase")
                propertyDict.Add("_IsImportPeoplesoftData", "ImportPeoplesoftData")
                propertyDict.Add("_IsIncludeInactiveEmp", "IncludeInactiveEmployee")
                propertyDict.Add("_IsLeaveApprover_ForLeaveType", "IsLvApproverMandatoryForLeaveType")
                propertyDict.Add("_IsLoanApprover_ForLoanScheme", "IsLoanApproverMandatoryForLoanScheme")
                propertyDict.Add("_IsNidaWebRecruitmentIntegrated", "IsNidaWRIntegrated")
                propertyDict.Add("_IsOrbitIntegrated", "IsOrbitIntegrated")
                propertyDict.Add("_IsPasswordAutoGenerated", "IsPasswordAutoGenerated")
                propertyDict.Add("_IsPDP_Perf_Integrated", "IsPDP_Perf_Integrated")
                propertyDict.Add("_IsSendLoanEmailFromDesktopMSS", "IsSendLoanEmailFromDesktopMSS")
                propertyDict.Add("_IsSendLoanEmailFromESS", "IsSendLoanEmailFromESS")
                propertyDict.Add("_IsSeparateTnAPeriod", "IsSeparateTnAPeriod")
                propertyDict.Add("_IsSymmetryIntegrated", "IsSymmetryIntegrated")
                propertyDict.Add("_IsUseAgreedScore", "UseAgreedScore")
                propertyDict.Add("_isVacancyAlert", "isVancanyalert")
                propertyDict.Add("_IsWorkAnniversaryEnabled", "IsWorkAnniversaryEnabled")
                propertyDict.Add("_JobAlertTemplateId", "JobAlertTemplateId")
                propertyDict.Add("_JobConfirmationTemplateId", "JobConfirmationTemplateId")
                propertyDict.Add("_JobExperienceDocsAttachmentMandatory", "JobExperienceDocsAttachmentMandatory")
                propertyDict.Add("_Language1MandatoryInRecruitment", "RecLanguage1Mandatory")
                propertyDict.Add("_LeaveAccrueDaysAfterEachMonth", "LeaveAccrueDaysAfterEachMonth")
                propertyDict.Add("_LeaveAccruedDaysHeadID", "LeaveAccruedDaysHeadID")
                propertyDict.Add("_LeaveAccrueTenureSetting", "LeaveAccrueTenureSetting")
                propertyDict.Add("_LeaveApplicationEscalationSettings", "LeaveApplicationEscalationSettings")
                propertyDict.Add("_LeaveBalanceSetting", "LeaveBalanceSetting")
                propertyDict.Add("_LeaveCancelFormNoPrifix", "LeaveCancelFormNoPrifix")
                propertyDict.Add("_LeaveCancelFormNotype", "LeaveCancelFormNoType")
                propertyDict.Add("_LeaveCF_OptionId", "LeaveCF_OptionId")
                propertyDict.Add("_LeaveEscalationRejectionReason", "LeaveEscalationRejectionReason")
                propertyDict.Add("_LeaveFormNoPrifix", "LeaveFormNoPrifix")
                propertyDict.Add("_LeaveFormNoType", "LeaveFormNoType")
                propertyDict.Add("_LeaveIssueAsonDateOnLeaveBalanceListReport", "LeaveIssueAsonDateOnLeaveBalanceListReport")
                propertyDict.Add("_LeaveLiabilityReportSetting", "LeaveLiabilityReportSetting")
                propertyDict.Add("_LeaveMaxValue", "LeaveMaxValue")
                propertyDict.Add("_LeaveProcessDate", "LeaveProcessDate")
                propertyDict.Add("_LeaveReminderOccurrenceAfterDays", "LeaveReminderOccurrenceAfterDays")
                propertyDict.Add("_LeaveTakenHeadID", "LeaveTakenHeadID")
                propertyDict.Add("_LeaveTypeOnPayslip", "LeaveTypeOnPayslip")
                propertyDict.Add("_LeavingDateUserNotification", "LeavingDateUserNotification")
                propertyDict.Add("_LeftMargin", "LeftMargin")
                propertyDict.Add("_LoanApplicationNoType", "LoanApplicationNoType")
                propertyDict.Add("_LoanApplicationPrifix", "LoanApplicationPrifix")
                propertyDict.Add("_LoanCentralizedApproverId", "LoanCentralizedApproverId")
                propertyDict.Add("_LoanFlexcubeFailureNotificationUserIds", "LoanFlexcubeFailureNotificationUserIds")
                propertyDict.Add("_LoanFlexcubeSuccessfulNotificationUserIds", "LoanFlexcubeSuccessfulNotificationUserIds")
                propertyDict.Add("_LoanIntegration", "LoanIntegration")
                propertyDict.Add("_LoanTrancheApplicationPrefix", "LoanTrancheApplicationPrefix")
                propertyDict.Add("_LoanVocNoType", "LoanVocNoType")
                propertyDict.Add("_LoanVocPrefix", "LoanVocPrefix")
                propertyDict.Add("_LockDaysAfterNonDisclosureDeclarationToDate", "EmpLockDaysAfterNonDisclosureDeclarationToDate")
                propertyDict.Add("_LockUserOnSuspension", "LockUserOnSuspension")
                propertyDict.Add("_LoginRequiredToApproveApplications", "LoginRequiredToApproveApplications")
                propertyDict.Add("_LogoOnPayslip", "LogoOnPayslip")
                propertyDict.Add("_MachineSr", "MachineSr")
                propertyDict.Add("_MakeAsrAssessCommentsMandatory", "MakeAsrAssessCommentsMandatory")
                propertyDict.Add("_MakeAttachmentMandatoryOnPromotion", "MakeAttachmentMandatoryOnPromotion")
                propertyDict.Add("_MakeAttachmentMandatoryOnRecategorize", "MakeAttachmentMandatoryOnRecategorize")
                propertyDict.Add("_MakeAttachmentMandatoryOnRehire", "MakeAttachmentMandatoryOnRehire")
                propertyDict.Add("_MakeAttachmentMandatoryOnSalaryChange", "MakeAttachmentMandatoryOnSalaryChange")
                propertyDict.Add("_MakeAttachmentMandatoryOnTransfer", "MakeAttachmentMandatoryOnTransfer")
                propertyDict.Add("_MakeEmpAssessCommentsMandatory", "MakeEmpAssessCommentsMandatory")
                propertyDict.Add("_MakeRemarkMandatoryForProgressUpdate", "MakeRemarkMandatoryForProgressUpdate")
                propertyDict.Add("_MakeRevAssessCommentsMandatory", "MakeRevAssessCommentsMandatory")
                propertyDict.Add("_MandatoryAttachmentsForLoanTrancheApplication", "MandatoryAttachmentsForLoanTrancheApplication")
                propertyDict.Add("_ManualMortgageDisbursementRequestFlexcubeURL", "ManualMortgageDisbursementRequestFlexcubeURL")
                propertyDict.Add("_ManualRoundOff_Limit", "ManualRoundOffLimit")
                propertyDict.Add("_MaritalStatusMandatoryInRecruitment", "RecMaritalStatusMandatory")
                propertyDict.Add("_MarriageDateUserNotification", "MarriageDateUserNotification")
                propertyDict.Add("_MaxEmpLeavePlannerAllocation", "MaxEmpLeavePlannerAllocation")
                propertyDict.Add("_MaxEmpPlannedLeave", "MaxEmpPlannedLeave")
                propertyDict.Add("_MaxRoutePerExpCategory", "_MaxRouteExpCat_")
                propertyDict.Add("_MaxTOTPUnSuccessfulAttempts", "MaxTOTPUnSuccessfulAttempts")
                propertyDict.Add("_MaxVacancyAlert", "MaxVacancyAlert")
                propertyDict.Add("_Membership1", "Membership1")
                propertyDict.Add("_Membership2", "Membership2")
                propertyDict.Add("_MiddleNameMandatoryInRecruitment", "RecMiddleNameMandatory")
                propertyDict.Add("_MisconductConsecutiveAbsentDays", "MisconductConsecutiveAbsentDays")
                propertyDict.Add("_MobileMoneyEFTIntegration", "MobileMoneyEFTIntegration")
                propertyDict.Add("_MortgageRequestFlexcubeURL", "MortgageRequestFlexcubeURL")
                propertyDict.Add("_MotherTongueMandatoryInRecruitment", "RecMotherTongueMandatory")
                propertyDict.Add("_NAPSATaxHeadID", "NAPSATaxHeadID")
                propertyDict.Add("_NationalityMandatoryInRecruitment", "RecNationalityMandatory")
                propertyDict.Add("_NewEmpUnLockDaysforAssetDecWithinAppointmentDate", "NewEmpUnLockDaysforAssetDecWithinAppointmentDate")
                propertyDict.Add("_NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate", "NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate")
                propertyDict.Add("_NewJobAddNotificationUserIds", "NewJobAddNotificationUserIds")
                propertyDict.Add("_NewLoanRequestFlexcubeURL", "NewLoanRequestFlexcubeURL")
                propertyDict.Add("_NewRequisitionRequestP2PServiceURL", "NewRequisitionRequestP2PServiceURL")
                propertyDict.Add("_NextApplicantCodeNo", "NextApplicantCodeNo")
                propertyDict.Add("_NextBatchPostingNo", "NextBatchPostingNo")
                propertyDict.Add("_NextBRJVVocNo", "NextBRJVVocNo")
                propertyDict.Add("_NextClaimRequestVocNo", "NextClaimRequestVocNo")
                propertyDict.Add("_NextClaimRetirementFormNo", "NextClaimRetirementFormNo")
                propertyDict.Add("_NextDisciplineRefNo", "NextDisciplineRefNo")
                propertyDict.Add("_NextEFTVocNo", "NextEFTVocNo")
                propertyDict.Add("_NextEmployeeCodeNo", "NextEmployeeCodeNo")
                propertyDict.Add("_NextFlexcubeMsgID", "NextFlexcubeMsgID")
                propertyDict.Add("_NextGrievanceRefNo", "NextGrievanceRefNo")
                propertyDict.Add("_NextLeaveCancelFormNo", "NextLeaveCancelFormNo")
                propertyDict.Add("_NextLeaveFormNo", "NextLeaveFormNo")
                propertyDict.Add("_NextLoanApplicationNo", "NextLoanApplicationNo")
                propertyDict.Add("_NextLoanTrancheApplicationNo", "NextLoanTrancheApplicationNo")
                propertyDict.Add("_NextLoanVocNo", "NextLoanVocNo")
                propertyDict.Add("_NextPaymentVocNo", "NextPaymentVocNo")
                propertyDict.Add("_NextPayslipVocNo", "NextPayslipVocNo")
                propertyDict.Add("_NextPIPFormNo", "NextPIPFormNo")
                propertyDict.Add("_NextSavingsVocNo", "NextSavingsVocNo")
                propertyDict.Add("_NextScoreCalibrationFormNo", "NextScoreCalibrationFormNo")
                propertyDict.Add("_NextSickSheetNo", "NextSickSheetNo")
                propertyDict.Add("_NextStaffReqFormNo", "NextStaffReqFormNo")
                propertyDict.Add("_NextTrainingNeedFormNo", "NextTrainingNeedFormNo")
                propertyDict.Add("_NidaServiceCollection", "_NidaSrv_")
                propertyDict.Add("_NonDisclosureDeclaration", "NonDisclosureDeclaration")
                propertyDict.Add("_NonDisclosureDeclarationAcknowledgedNotificationUserIds", "NonDisclosureDeclarationAcknowledgedNotificationUserIds")
                propertyDict.Add("_NonDisclosureDeclarationFromDate", "NonDisclosureDeclarationFromDate")
                propertyDict.Add("_NonDisclosureDeclarationToDate", "NonDisclosureDeclarationToDate")
                propertyDict.Add("_NonDisclosureWitnesser1", "NonDisclosureWitnesser1")
                propertyDict.Add("_NonDisclosureWitnesser2", "NonDisclosureWitnesser2")
                propertyDict.Add("_NoOfChildHeadId", "NoOfChildHeadID")
                propertyDict.Add("_NoOfDependantHeadId", "NoOfDependantHeadID")
                propertyDict.Add("_NoofPagestoPrint", "NoofPagestoPrint")
                propertyDict.Add("_NotAllowClosePeriodIncompleteSubmitApproval", "NotAllowClosePeriodIncompleteSubmitApproval")
                propertyDict.Add("_NotAllowIncompleteTimesheet", "NotAllowIncompleteTimesheet")
                propertyDict.Add("_NoticePeriodMandatoryInRecruitment", "RecNoticePeriodMandatory")
                propertyDict.Add("_NotificationOnDisciplineFiling", "NotificationOnDisciplineFiling")
                propertyDict.Add("_Notify_Allocation", "Notify_Allocation")
                propertyDict.Add("_Notify_Bank_Users", "Notify_Bank")
                propertyDict.Add("_Notify_Budget_FundActivityPercentage", "Notify_Budget_FundActivityPercentage")
                propertyDict.Add("_Notify_Budget_FundSourcePercentage", "Notify_Budget_FundSourcePercentage")
                propertyDict.Add("_Notify_Budget_Users", "Notify_Budget")
                propertyDict.Add("_Notify_Dates", "Notify_Dates")
                propertyDict.Add("_Notify_EmplData", "Notify_EmplData")
                propertyDict.Add("_Notify_IssuedLeave_Users", "Notify_IssuedLeave")
                propertyDict.Add("_Notify_LoanAdvance_Users", "Notify_LoanAdvance")
                propertyDict.Add("_Notify_Payroll_Users", "Notify_Payroll")
                propertyDict.Add("_Ntf_FinalAcknowledgementUserIds", "Ntf_FinalAcknowledgementUserIds")
                propertyDict.Add("_Ntf_GoalsUnlockUserIds", "Ntf_GoalsUnlockUserIds")
                propertyDict.Add("_Ntf_TrainingLevelI_EvalUserIds", "Ntf_TrainingLevelI_EvalUserIds")
                propertyDict.Add("_Ntf_TrainingLevelIII_EvalUserIds", "Ntf_TrainingLevelIII_EvalUserIds")
                propertyDict.Add("_ntfProgressUpdateAllEmpOnEachMonthDay", "ntfProgressUpdateAllEmpOnEachMonthDay")
                propertyDict.Add("_ntfProgressUpdateAllEmpTemplateId", "ntfProgressUpdateAllEmpTemplateId")
                propertyDict.Add("_oldSkipApprovalOnEmpData", "OldSkipApprovalOnEmpData")
                propertyDict.Add("_OneCoverLetterMandatoryInRecruitment", "RecOneCoverLetterMandatory")
                propertyDict.Add("_OneCurriculamVitaeMandatoryInRecruitment", "RecOneCurriculamVitaeMandatory")
                propertyDict.Add("_OneJobExperienceMandatoryInRecruitment", "RecOneJobExperienceMandatory")
                propertyDict.Add("_OneQualificationMandatoryInRecruitment", "RecOneQualificationMandatory")
                propertyDict.Add("_OneReferenceMandatoryInRecruitment", "RecOneReferenceMandatory")
                propertyDict.Add("_OneSkillMandatoryInRecruitment", "RecOneSkillMandatory")
                propertyDict.Add("_OnImportIncludeTransectionHeadWhileSendingNotification", "OnImportIncludeTransectionHeadWhileSendingNotification")
                propertyDict.Add("_OnlyOneItemPerCompetencyCategory", "OnlyOneItemPerCompetencyCategory")
                propertyDict.Add("_OpenAfterExport", "OpenAfterExport")
                propertyDict.Add("_OpexRequestCCP2PServiceURL", "OpexRequestCCP2PServiceURL")
                propertyDict.Add("_OracleHostName", "OracleHostName")
                propertyDict.Add("_OraclePortNo", "OraclePortNo")
                propertyDict.Add("_OracleServiceName", "OracleServiceName")
                propertyDict.Add("_OracleUserName", "OracleUserName")
                propertyDict.Add("_OracleUserPassword", "OracleUserPassword")
                propertyDict.Add("_OrbitAgentUsername", "OrbitAgentUsername")
                propertyDict.Add("_OrbitFailedRequestNotificationEmails", "OrbitFailedRequestNotificationEmails")
                propertyDict.Add("_OrbitPassword", "OrbitPassword")
                propertyDict.Add("_OrbitServiceCollection", "_OrbSrv_")
                propertyDict.Add("_OrbitUsername", "OrbitUsername")
                propertyDict.Add("_OrderByDescendingOnPayslip", "OrderByDescendingOnPayslip")
                propertyDict.Add("_OT1AmountHeadId", "OT1AmountHeadID")
                propertyDict.Add("_OT1AmountHeadName", "OT1AmountHeadName")
                propertyDict.Add("_OT1HourHeadId", "OT1HourHeadID")
                propertyDict.Add("_OT1HourHeadName", "OT1HourHeadName")
                propertyDict.Add("_OT1Order", "OT1_Order")
                propertyDict.Add("_OT2AmountHeadId", "OT2AmountHeadID")
                propertyDict.Add("_OT2AmountHeadName", "OT2AmountHeadName")
                propertyDict.Add("_OT2HourHeadId", "OT2HourHeadID")
                propertyDict.Add("_OT2HourHeadName", "OT2HourHeadName")
                propertyDict.Add("_OT2Order", "OT2_Order")
                propertyDict.Add("_OT3AmountHeadId", "OT3AmountHeadID")
                propertyDict.Add("_OT3AmountHeadName", "OT3AmountHeadName")
                propertyDict.Add("_OT3HourHeadId", "OT3HourHeadID")
                propertyDict.Add("_OT3HourHeadName", "OT3HourHeadName")
                propertyDict.Add("_OT3Order", "OT3_Order")
                propertyDict.Add("_OT4AmountHeadId", "OT4AmountHeadID")
                propertyDict.Add("_OT4AmountHeadName", "OT4AmountHeadName")
                propertyDict.Add("_OT4HourHeadId", "OT4HourHeadID")
                propertyDict.Add("_OT4HourHeadName", "OT4HourHeadName")
                propertyDict.Add("_OT4Order", "OT4_Order")
                propertyDict.Add("_OtherPath", "OtherPath")
                propertyDict.Add("_OTTenureDays", "OTTenureDays")
                propertyDict.Add("_P2PTokenClientId", "P2PTokenClientId")
                propertyDict.Add("_P2PTokenClientSecret", "P2PTokenClientSecret")
                propertyDict.Add("_P2PTokenResourceURL", "P2PTokenResourceURL")
                propertyDict.Add("_P2PTokenTenantId", "P2PTokenTenantId")
                propertyDict.Add("_PaidHoursHeadID", "PaidHoursHeadID")
                propertyDict.Add("_PAScoringRoudingFactor", "PAScoringRoudingFactor")
                propertyDict.Add("_PaymentApprovalwithLeaveApproval", "ClaimRequest_PaymentApprovalwithLeaveApproval")
                propertyDict.Add("_PaymentRoundingMultiple", "PaymentRoundingMultiple")
                propertyDict.Add("_PaymentRoundingType", "PaymentRoundingType")
                propertyDict.Add("_PaymentVocNoType", "PaymentVocNoType")
                propertyDict.Add("_PaymentVocPrefix", "PaymentVocPrefix")
                propertyDict.Add("_PayslipTemplate", "PayslipTemplate")
                propertyDict.Add("_PayslipVocPrefix", "PayslipVocPrefix")
                propertyDict.Add("_PendingEmployeeScreenIDs", "PendingEmployeeScreenIDs")
                propertyDict.Add("_PEReport1_AllocationTypeId", "PEReport1_AllocationTypeId")
                propertyDict.Add("_PEReport1_HealthInsuranceMappedHeadId", "PEReport1_HealthInsuranceMappedHeadId")
                propertyDict.Add("_PEReport1_IncrementReasonMappedId", "PEReport1_IncrementReasonMappedId")
                propertyDict.Add("_PEReport1_PromotionReasonMappedId", "PEReport1_PromotionReasonMappedId")
                propertyDict.Add("_PEReport1_SelectedAllocationIds", "PEReport1_SelectedAllocationIds")
                propertyDict.Add("_PEReport1_SelectedMembershipIds", "PEReport1_SelectedMembershipIds")
                propertyDict.Add("_PEReport2_AllocationTypeId", "PEReport2_AllocationTypeId")
                propertyDict.Add("_PEReport2_HealthInsuranceMappedHeadId", "PEReport2_HealthInsuranceMappedHeadId")
                propertyDict.Add("_PEReport2_IncrementReasonMappedId", "PEReport2_IncrementReasonMappedId")
                propertyDict.Add("_PEReport2_PromotionReasonMappedId", "PEReport2_PromotionReasonMappedId")
                propertyDict.Add("_PEReport2_SelectedAllocationIds", "PEReport2_SelectedAllocationIds")
                propertyDict.Add("_PEReport2_SelectedMembershipIds", "PEReport2_SelectedMembershipIds")
                propertyDict.Add("_PEReport3_AllocationTypeId", "PEReport3_AllocationTypeId")
                propertyDict.Add("_PEReport3_HealthInsuranceMappedHeadId", "PEReport3_HealthInsuranceMappedHeadId")
                propertyDict.Add("_PEReport3_IncrementReasonMappedId", "PEReport3_IncrementReasonMappedId")
                propertyDict.Add("_PEReport3_PromotionReasonMappedId", "PEReport3_PromotionReasonMappedId")
                propertyDict.Add("_PEReport3_SelectedAllocationIds", "PEReport3_SelectedAllocationIds")
                propertyDict.Add("_PEReport3_SelectedMembershipIds", "PEReport3_SelectedMembershipIds")
                propertyDict.Add("_PEReport4_AllocationTypeId", "PEReport4_AllocationTypeId")
                propertyDict.Add("_PEReport4_HealthInsuranceMappedHeadId", "PEReport4_HealthInsuranceMappedHeadId")
                propertyDict.Add("_PEReport4_IncrementReasonMappedId", "PEReport4_IncrementReasonMappedId")
                propertyDict.Add("_PEReport4_PromotionReasonMappedId", "PEReport4_PromotionReasonMappedId")
                propertyDict.Add("_PEReport4_SelectedAllocationIds", "PEReport4_SelectedAllocationIds")
                propertyDict.Add("_PEReport4_SelectedMembershipIds", "PEReport4_SelectedMembershipIds")
                propertyDict.Add("_PEReport5_AllocationTypeId", "PEReport5_AllocationTypeId")
                propertyDict.Add("_PEReport5_HealthInsuranceMappedHeadId", "PEReport5_HealthInsuranceMappedHeadId")
                propertyDict.Add("_PEReport5_IncrementReasonMappedId", "PEReport5_IncrementReasonMappedId")
                propertyDict.Add("_PEReport5_PromotionReasonMappedId", "PEReport5_PromotionReasonMappedId")
                propertyDict.Add("_PEReport5_SelectedAllocationIds", "PEReport5_SelectedAllocationIds")
                propertyDict.Add("_PEReport5_SelectedMembershipIds", "PEReport5_SelectedMembershipIds")
                propertyDict.Add("_Perf_EvaluationOrder", "Perf_EvaluationOrder")
                propertyDict.Add("_PerformanceComputation", "PerformanceComputation")
                propertyDict.Add("_PhotoPath", "PhotoPath")
                propertyDict.Add("_PIPFormNoPrefix", "PIPFormNoPrefix")
                propertyDict.Add("_PIPFormNoType", "PIPFormNoType")
                propertyDict.Add("_PolicyManagementTNA", "PolicyManagementTNA")
                propertyDict.Add("_PortNo", "PortNo")
                propertyDict.Add("_PositionHeldMandatoryInRecruitment", "RecPositionHeldMandatory")
                propertyDict.Add("_PostCodeMandatoryInRecruitment", "RecPostCodeMandatory")
                propertyDict.Add("_PostTrainingEvaluationBeforeCompleteTraining", "PostTrainingEvaluationBeforeCompleteTraining")
                propertyDict.Add("_PostTrainingFeedbackInstruction", "PostTrainingFeedbackInstruction")
                propertyDict.Add("_PrepaidRequestFlexcubeURL", "PrepaidRequestFlexcubeURL")
                propertyDict.Add("_PreparedBy", "PreparedBy")
                propertyDict.Add("_PresentAddressDocsAttachmentMandatory", "PresentAddressDocsAttachmentMandatory")
                propertyDict.Add("_PreTrainingEvaluationSubmitted", "PreTrainingEvaluationSubmitted")
                propertyDict.Add("_PreTrainingFeedbackInstruction", "PreTrainingFeedbackInstruction")
                propertyDict.Add("_PreventDisciplineChargeunlessEmployeeNotified", "PreventDisciplineChargeunlessEmployeeNotified")
                propertyDict.Add("_PreventDisciplineChargeunlessEmployeeReceipt", "PreventDisciplineChargeunlessEmployeeReceipt")
                propertyDict.Add("_ProbationDateUserNotification", "ProbationDateUserNotification")
                propertyDict.Add("_ProbationMonth", "ProbationMonth")
                propertyDict.Add("_PSPFIntegration", "PSPFIntegration")
                propertyDict.Add("_QualificationAwardDateMandatoryInRecruitment", "RecQualificationAwardDateMandatory")
                propertyDict.Add("_QualificationCertificateAttachmentMandatory", "QualificationCertificateAttachmentMandatory")
                propertyDict.Add("_QualificationStartDateMandatoryInRecruitment", "RecQualificationStartDateMandatory")
                propertyDict.Add("_ReceivedBy", "ReceivedBy")
                propertyDict.Add("_RecruitMandatoryFieldsIDs", "RecruitMandatoryFieldsIDs")
                propertyDict.Add("_RecruitMandatoryJobExperiences", "RecruitMandatoryJobExperiences")
                propertyDict.Add("_RecruitMandatoryQualifications", "RecruitMandatoryQualifications")
                propertyDict.Add("_RecruitMandatoryReferences", "RecruitMandatoryReferences")
                propertyDict.Add("_RecruitMandatorySkills", "RecruitMandatorySkills")
                propertyDict.Add("_RecruitmentAddressDocsAttachmentMandatory", "RecruitmentAddressDocsAttachmentMandatory")
                propertyDict.Add("_RecruitmentCareerLink", "RecruitmentCareerLink")
                propertyDict.Add("_RecruitmentEmailSetupUnkid", "RecruitmentEmailSetupUnkid")
                propertyDict.Add("_RecruitmentWebServiceLink", "RecruitmentWebServiceLink")
                propertyDict.Add("_Reg1Caption", "Reg1Caption")
                propertyDict.Add("_Reg2Caption", "Reg2Caption")
                propertyDict.Add("_Reg3Caption", "Reg3Caption")
                propertyDict.Add("_RegionMandatoryInRecruitment", "RecRegionMandatory")
                propertyDict.Add("_ReinstatementDateUserNotification", "ReinstatementDateUserNotification")
                propertyDict.Add("_RejectConfirmationUserNotification", "RejectConfirmationUserNotification")
                propertyDict.Add("_RejectCostCenterPermitUserNotification", "RejectCostCenterPermitUserNotification")
                propertyDict.Add("_RejectExemptionUserNotification", "RejectExemptionUserNotification")
                propertyDict.Add("_RejectProbationUserNotification", "RejectProbationUserNotification")
                propertyDict.Add("_RejectReCategorizationUserNotification", "RejectReCategorizationUserNotification")
                propertyDict.Add("_RejectResidentPermitUserNotification", "RejectResidentPermitUserNotification")
                propertyDict.Add("_RejectRetirementUserNotification", "RejectRetirementUserNotification")
                propertyDict.Add("_RejectSuspensionUserNotification", "RejectSuspensionUserNotification")
                propertyDict.Add("_RejectTerminationUserNotification", "RejectTerminationUserNotification")
                propertyDict.Add("_RejectTransferUserNotification", "RejectTransferUserNotification")
                propertyDict.Add("_RejectWorkPermitUserNotification", "RejectWorkPermitUserNotification")
                propertyDict.Add("_RelieverAllocation", "RelieverAllocation")
                propertyDict.Add("_RemarkMandatoryForTimesheetSubmission", "RemarkMandatoryForTimesheetSubmission")
                propertyDict.Add("_ReplacementStaffRequisitionDocsAttachmentMandatory", "ReplacementStaffRequisitionDocsAttachmentMandatory")
                propertyDict.Add("_RequisitionStatusP2PServiceURL", "RequisitionStatusP2PServiceURL")
                propertyDict.Add("_RestrictEmpForBgTimesheetOnAfterDate", "RestrictEmpForBgTimesheetOnAfterDate")
                propertyDict.Add("_RestrictImportEmpWithSameDisplayName", "RestrictImportEmpWithSameDisplayName")
                propertyDict.Add("_RestrictImportEmpWithSameEmail", "RestrictImportEmpWithSameEmail")
                propertyDict.Add("_RetirementBy", "RetirementBy")
                propertyDict.Add("_RetirementDateUserNotification", "RetirementDateUserNotification")
                propertyDict.Add("_RetirementValue", "RetirementValue")
                propertyDict.Add("_ReviewerInfo", "ReviewerInfo")
                propertyDict.Add("_ReviewerScoreSetting", "ReviewerScoreSetting")
                propertyDict.Add("_RightMargin", "RightMargin")
                propertyDict.Add("_RoleBasedLoanApproval", "RoleBasedLoanApproval")
                propertyDict.Add("_RoundOff_Type", "RoundOffType")
                propertyDict.Add("_RunCompetenceAssessmentSeparately", "RunCompetenceAssessmentSeparately")
                propertyDict.Add("_RunDailyAbsentProcessTime", "RunDailyAbsentProcessTime")
                propertyDict.Add("_RunDailyAttendanceServiceTime", "RunDailyAttendanceServiceTime")
                propertyDict.Add("_RunDailyCompanyFixedAssetServiceTime", "RunDailyCompanyFixedAssetServiceTime")
                propertyDict.Add("_RunDailyCreditCardImportationServiceTime", "RunDailyCreditCardImportationServiceTime")
                propertyDict.Add("_SagemDatabaseName", "SagemDatabaseName")
                propertyDict.Add("_SagemDatabasePassword", "SagemDatabasePassword")
                propertyDict.Add("_SagemDatabaseUserName", "SagemDatabaseUserName")
                propertyDict.Add("_SagemDataServerAddress", "SagemDataServerAddress")
                propertyDict.Add("_SalaryAnniversaryMonthBy", "SalaryAnniversaryMonthBy")
                propertyDict.Add("_SalaryAnniversarySetting", "SalaryAnniversarySetting")
                propertyDict.Add("_SavingsVocNoType", "SavingsVocNoType")
                propertyDict.Add("_SavingsVocPrefix", "SavingsVocPrefix")
                propertyDict.Add("_ScoreCalibrationFormNoPrifix", "ScoreCalibrationFormNoPrifix")
                propertyDict.Add("_ScoreCalibrationFormNotype", "ScoreCalibrationFormNotype")
                propertyDict.Add("_ScoringOptionId", "ScoringOptionId")
                propertyDict.Add("_SectorRouteAssignToEmp", "SectorRouteAssignToEmps")
                propertyDict.Add("_SectorRouteAssignToExpense", "SectorRouteAssignToExpense")
                propertyDict.Add("_SelectedAllocationForDailyTimeSheetReport_Voltamp", "SELECTEDALLOCATIONFORDAILYTIMESHEETREPORT_VOLTAMP")
                propertyDict.Add("_Self_Assign_Competencies", "SelfAssignCompetencies")
                propertyDict.Add("_SendDetailToEmployee", "SendDetailToEmployee")
                propertyDict.Add("_SendJobConfirmationEmail", "SendJobConfirmationEmail")
                propertyDict.Add("_SendPasswordProtectedEPayslip", "SendPasswordProtectedEPayslip")
                propertyDict.Add("_SendTnAEarlyLateReportsUserIds", "SendTnAEarlyLateReportsUserIds")
                propertyDict.Add("_Separator", "Separator")
                propertyDict.Add("_SetBasicSalaryAsOtherEarningOnStatutoryReport", "SetBasicSalaryAsOtherEarningOnStatutoryReport")
                propertyDict.Add("_SetDeviceInOutStatus", "SetDeviceInOutStatus")
                propertyDict.Add("_SetMaxNumberofInstallmentsMandatory", "SetMaxNumberofInstallmentsMandatory")
                propertyDict.Add("_SetOTRequisitionMandatory", "SetOTRequisitionMandatory")
                propertyDict.Add("_SetPayslipPaymentApproval", "SetPaySlipPayApproval")
                propertyDict.Add("_SetPaySlipPaymentMandatory", "SetPaySlipPaymentMandatory")
                propertyDict.Add("_SetRelieverAsMandatoryForApproval", "SetRelieverAsMandatoryForApproval")
                propertyDict.Add("_ShowAgeOnPayslip", "ShowAgeOnPayslip")
                propertyDict.Add("_ShowAllHeadsOnPayslip", "ShowAllHeadsOnPayslip")
                propertyDict.Add("_ShowAppointmentDateOnPayslip", "ShowAppointmentDateOnPayslip")
                propertyDict.Add("_ShowArutisignatureinemailnotification", "ShowArutisignature")
                propertyDict.Add("_ShowBankAccountNoOnPayslip", "ShowBankAccountNoOnPayslip")
                propertyDict.Add("_ShowBasicSalaryOnLeaveFormReport", "ShowBasicSalaryOnLeaveFormReport")
                propertyDict.Add("_ShowBasicSalaryOnStatutoryReport", "ShowBasicSalaryOnStatutoryReport")
                propertyDict.Add("_ShowBgTimesheetActivity", "ShowBgTimesheetActivity")
                propertyDict.Add("_ShowBgTimesheetActivityHrsDetail", "ShowBgTimesheetActivityHrsDetail")
                propertyDict.Add("_ShowBgTimesheetActivityProject", "ShowBgTimesheetActivityProject")
                propertyDict.Add("_ShowBgTimesheetAssignedActivityPercentage", "ShowBgTimesheetAssignedActivityPercentage")
                propertyDict.Add("_ShowBirthDateOnPayslip", "ShowBirthDateOnPayslip")
                propertyDict.Add("_ShowBranchOnPayslip", "ShowBranchOnPayslip")
                propertyDict.Add("_ShowCategoryOnPayslip", "ShowCategoryOnPayslip")
                propertyDict.Add("_ShowClassGroupOnPayslip", "ShowClassGroupOnPayslip")
                propertyDict.Add("_ShowClassOnPayslip", "ShowClassOnPayslip")
                propertyDict.Add("_ShowCompanyNameOnPayslip", "ShowCompanyNameOnPayslip")
                propertyDict.Add("_ShowCompanyStampOnPayslip", "ShowCompanyStampOnPayslip")
                propertyDict.Add("_ShowCumulativeAccrualOnPayslip", "ShowCumulativeAccrualOnPayslip")
                propertyDict.Add("_ShowCustomHeaders", "ShowCustomHeaders")
                propertyDict.Add("_ShowDepartmentGroupOnPayslip", "ShowDepartmentGroupOnPayslip")
                propertyDict.Add("_ShowDepartmentOnPayslip", "ShowDepartmentOnPayslip")
                propertyDict.Add("_ShowEmployeeCodeOnPayslip", "ShowEmployeeCodeOnPayslip")
                propertyDict.Add("_ShowEmployeeStatusForDailyTimeSheetReport_Voltamp", "SHOWEMPLOYEESTATUSFORDAILYTIMESHEETREPORT_VOLTAMP")
                propertyDict.Add("_ShowEmployerContributionOnPayslip", "ShowEmployerContributionOnPayslip")
                propertyDict.Add("_ShowEOCDateOnPayslip", "ShowEOCDateOnPayslip")
                propertyDict.Add("_ShowExchangeRateOnPayslip", "ShowExchangeRateOnPayslip")
                propertyDict.Add("_ShowFirstAppointmentDate", "ShowFirstAppointmentDate")
                propertyDict.Add("_ShowGoalsApproval", "ShowGoalsApproval")
                propertyDict.Add("_ShowGradeGroupOnPayslip", "ShowGradeGroupOnPayslip")
                propertyDict.Add("_ShowGradeLevelOnPayslip", "ShowGradeLevelOnPayslip")
                propertyDict.Add("_ShowGradeOnPayslip", "ShowGradeOnPayslip")
                propertyDict.Add("_ShowHidePayslipOnESS", "ShowHidePayslipOnESS")
                propertyDict.Add("_ShowInformationalHeadsOnPayslip", "ShowInformationalHeadsOnPayslip")
                propertyDict.Add("_ShowInterdictionDate", "ShowInterdictionDate")
                propertyDict.Add("_ShowJobOnPayslip", "ShowJobOnPayslip")
                propertyDict.Add("_ShowLeavingDateOnPayslip", "ShowLeavingDateOnPayslip")
                propertyDict.Add("_ShowLoanBalanceOnPayslip", "ShowLoanBalanceOnPayslip")
                propertyDict.Add("_ShowLogoRightSide", "ShowLogoRightSide")
                propertyDict.Add("_ShowMembershipOnPayslip", "ShowMembershipOnPayslip")
                propertyDict.Add("_ShowMonthlySalaryOnPayslip", "ShowMonthlySalaryOnPayslip")
                propertyDict.Add("_ShowMyComptencies", "ShowMyComptencies")
                propertyDict.Add("_ShowMyGoals", "ShowMyGoals")
                propertyDict.Add("_ShowNoOfDependantsOnPayslip", "ShowNoOfDependantsOnPayslip")
                propertyDict.Add("_ShowPaymentApprovalDetailsOnStatutoryReport", "ShowPaymentApprovalDetailsOnPayrollSummaryReport")
                propertyDict.Add("_ShowPaymentDetailsOnPayslip", "ShowPaymentDetailsOnPayslip")
                propertyDict.Add("_ShowPropertyInfo", "ShowPropertyInfo")
                propertyDict.Add("_ShowRemainingNoOfLoanInstallmentsOnPayslip", "ShowRemainingNoOfLoanInstallmentsOnPayslip")
                propertyDict.Add("_ShowReponseTypeOnPosting", "ShowReponseTypeOnPosting")
                propertyDict.Add("_ShowRetirementDateOnPayslip", "ShowRetirementDateOnPayslip")
                propertyDict.Add("_ShowSalaryOnHoldOnPayslip", "ShowSalaryOnHoldOnPayslip")
                propertyDict.Add("_ShowSavingBalanceOnPayslip", "ShowSavingBalanceOnPayslip")
                propertyDict.Add("_ShowSectionGroupOnPayslip", "ShowSectionGroupOnPayslip")
                propertyDict.Add("_ShowSectionOnPayslip", "ShowSectionOnPayslip")
                propertyDict.Add("_ShowSelfAssessment", "ShowSelfAssessment")
                propertyDict.Add("_ShowTeamOnPayslip", "ShowTeamOnPayslip")
                propertyDict.Add("_ShowUnitGroupOnPayslip", "ShowUnitGroupOnPayslip")
                propertyDict.Add("_ShowUnitOnPayslip", "ShowUnitOnPayslip")
                propertyDict.Add("_ShowViewPerformanceAssessment", "ShowViewPerformanceAssessment")
                propertyDict.Add("_ShowWeightOnAssessScreenRatingScale", "ShowWeightColOnAssessScreenRatingScale")
                propertyDict.Add("_SickSheetAllocationId", "SickSheetAllocationId")
                propertyDict.Add("_SickSheetNotype", "SickSheetNoType")
                propertyDict.Add("_SickSheetPrifix", "SickSheetPrefix")
                propertyDict.Add("_SickSheetTemplate", "SickSheetTemplate")
                propertyDict.Add("_SingleCommentForAllCompetenciesForAsrAssess", "SingleCommentForAllCompetenciesForAsrAssess")
                propertyDict.Add("_SingleCommentForAllCompetenciesForEmpAssess", "SingleCommentForAllCompetenciesForEmpAssess")
                propertyDict.Add("_SingleCommentForAllCompetenciesForRevAssess", "SingleCommentForAllCompetenciesForRevAssess")
                propertyDict.Add("_SingleCommentForAllObjectivesForAsrAssess", "SingleCommentForAllObjectivesForAsrAssess")
                propertyDict.Add("_SingleCommentForAllObjectivesForEmpAssess", "SingleCommentForAllObjectivesForEmpAssess")
                propertyDict.Add("_SingleCommentForAllObjectivesForRevAssess", "SingleCommentForAllObjectivesForRevAssess")
                propertyDict.Add("_SkipAbsentFromTnAMonthlyPayrollReport", "MonthlyPayrollReportSkipAbsentFromTnA")
                propertyDict.Add("_SkipApprovalFlowForApplicantShortListing", "SkipApprovalFlowForApplicantShortListing")
                propertyDict.Add("_SkipApprovalFlowForFinalApplicant", "SkipApprovalFlowForFinalApplicant")
                propertyDict.Add("_SkipApprovalFlowInPlanning", "SkipApprovalFlowInPlanning")
                propertyDict.Add("_SkipApprovalOnEmpData", "SkipApprovalOnEmpData")
                propertyDict.Add("_SkipDaysAfterTrainingEvaluationProcess", "SkipDaysAfterTrainingEvaluationProcess")
                propertyDict.Add("_SkipEmployeeApprovalFlow", "SkipEmployeeApprovalFlow")
                propertyDict.Add("_SkipEmployeeMovementApprovalFlow", "SkipEmployeeMovementApprovalFlow")
                propertyDict.Add("_SkipEOCValidationOnLoanTenure", "SkipEOCValidationOnLoanTenure")
                propertyDict.Add("_SkipExchangeEnableEmailAllocations", "SkipExchangeEnableEmailAllocations")
                propertyDict.Add("_SkipPostTrainingEvaluationProcess", "SkipPostTrainingEvaluationProcess")
                propertyDict.Add("_SkipPreTrainingEvaluationProcess", "SkipPreTrainingEvaluationProcess")
                propertyDict.Add("_SkipTrainingBudgetApprovalProcess", "SkipTrainingBudgetApprovalProcess")
                propertyDict.Add("_SkipTrainingCompletionProcess", "SkipTrainingCompletionProcess")
                propertyDict.Add("_SkipTrainingEnrollmentProcess", "SkipTrainingEnrollmentProcess")
                propertyDict.Add("_SkipTrainingRequisitionAndApproval", "SkipTrainingRequisitionAndApproval")
                propertyDict.Add("_SMimeRunPath", "SMimeRunPath")
                propertyDict.Add("_SMSCompanyDetailToNewEmp", "SMSCompanyDetailToNewEmp")
                propertyDict.Add("_SMSGatewayEmail", "SMSGatewayEmail")
                propertyDict.Add("_SMSGatewayEmailType", "SMSGatewayEmailType")
                propertyDict.Add("_SQLDatabaseName", "SQLDatabaseName")
                propertyDict.Add("_SQLDatabaseOwnerName", "SQLDatabaseOwnerName")
                propertyDict.Add("_SQLDataSource", "SQLDataSource")
                propertyDict.Add("_SQLUserName", "SQLUserName")
                propertyDict.Add("_SQLUserPassword", "SQLUserPassword")
                propertyDict.Add("_StaffReqFormNoPrefix", "StaffReqFormNoPrefix")
                propertyDict.Add("_StaffReqFormNoType", "StaffReqFormNoType")
                propertyDict.Add("_StaffRequisitionDocsAttachmentMandatory", "StaffRequisitionDocsAttachmentMandatory")
                propertyDict.Add("_StaffRequisitionFinalApprovedNotificationUserIds", "StaffRequisitionFinalApprovedNotificationUserIds")
                propertyDict.Add("_StaffTransferFinalApprovalNotificationUserIds", "StaffTransferFinalApprovalNotificationUserIds")
                propertyDict.Add("_StaffTransferRequestAllocations", "StaffTransferRequestAllocations")
                propertyDict.Add("_StateMandatoryInRecruitment", "RecStateMandatory")
                propertyDict.Add("_StatutoryMessageOnPayslipOnESS", "StatutoryMessageOnPayslipOnESS")
                propertyDict.Add("_SuspensionDateUserNotification", "SuspensionDateUserNotification")
                propertyDict.Add("_SymmetryAuthenticationModeId", "SymmetryAuthenticationModeId")
                propertyDict.Add("_SymmetryCardNumberTypeId", "SymmetryCardNumberTypeId")
                propertyDict.Add("_SymmetryDatabaseName", "SymmetryDatabaseName")
                propertyDict.Add("_SymmetryDatabasePassword", "SymmetryDatabasePassword")
                propertyDict.Add("_SymmetryDatabaseUserName", "SymmetryDatabaseUserName")
                propertyDict.Add("_SymmetryDataServerAddress", "SymmetryDataServerAddress")
                propertyDict.Add("_SymmetryMarkAsDefault", "SymmetryMarkAsDefault")
                propertyDict.Add("_TaxableAmountHeadId", "TaxableAmountHeadID")
                propertyDict.Add("_TaxFreeIncomeHeadID", "TaxFreeIncomeHeadID")
                propertyDict.Add("_TimeZoneMinuteDifference", "TimeZoneMinuteDifference")
                propertyDict.Add("_TnAFailureNotificationUserIds", "TnAFailureNotificationUserIds")
                propertyDict.Add("_TOPPageMarging", "PageMargin")
                propertyDict.Add("_TopupRequestFlexcubeURL", "TopupRequestFlexcubeURL")
                propertyDict.Add("_TOTPRetryAfterMins", "TOTPRetryAfterMins")
                propertyDict.Add("_TOTPTimeLimitInSeconds", "TOTPTimeLimitInSeconds")
                propertyDict.Add("_TrainingApproverAllocationID", "TrainingApproverAllocationID")
                propertyDict.Add("_TrainingBudgetAllocationID", "TrainingBudgetAllocationID")
                propertyDict.Add("_TrainingCompetenciesPeriodId", "TrainingCompetenciesPeriodId")
                propertyDict.Add("_TrainingCostCenterAllocationID", "TrainingCostCenterAllocationID")
                propertyDict.Add("_TrainingFinalApprovedNotificationUserIds", "TrainingFinalApprovedNotificationUserIds")
                propertyDict.Add("_TrainingNeedAllocationID", "TrainingNeedAllocationID")
                propertyDict.Add("_TrainingNeedFormNoPrefix", "TrainingNeedFormNoPrefix")
                propertyDict.Add("_TrainingNeedFormNoType", "TrainingNeedFormNoType")
                propertyDict.Add("_TrainingRemainingBalanceBasedOnID", "TrainingRemainingBalanceBasedOnID")
                propertyDict.Add("_TrainingRequestApprovalSettingID", "TrainingRequestApprovalSettingID")
                propertyDict.Add("_TrainingRequireForForeignTravelling", "TrainingRequireForForeignTravelling")
                propertyDict.Add("_UnLockDaysAfterAssetDeclarationToDate", "EmpUnLockDaysAfterAssetDeclarationToDate")
                propertyDict.Add("_UnRetiredImprestToPayrollAfterDays", "UnRetiredImprestToPayrollAfterDays")
                propertyDict.Add("_UserAccessModeSetting", "UserAccessModeSetting")
                propertyDict.Add("_UserMustchangePwdOnNextLogon", "UserMustChangePwdOnNextLogon")
                propertyDict.Add("_VacancyFoundOutFromMandatoryInRecruitment", "RecVacancyFoundOutFromMandatory")
                propertyDict.Add("_ViewPayslipDaysBefore", "ViewPayslipDaysBefore")
                propertyDict.Add("_ViewTitles_InEvaluation", "ViewTitles_InEvaluation")
                propertyDict.Add("_ViewTitles_InPlanning", "ViewTitles_InPlanning")
                propertyDict.Add("_WAvgAseComputType", "WAvgAseComputType")
                propertyDict.Add("_WAvgEmpComputationType", "WAvgEmpComputationType")
                propertyDict.Add("_WAvgRevComputType", "WAvgRevComputType")
                propertyDict.Add("_WebClientCode", "WebClientCode")
                propertyDict.Add("_WebURL", "WebURL")
                propertyDict.Add("_WebURLInternal", "WebURLInternal")
                propertyDict.Add("_WeekendDays", "WeekendDays")
                propertyDict.Add("_WorkAnnivLetterTemplateId", "WorkAnnivLetterTemplateId")
                propertyDict.Add("_WorkAnnivSMS", "WorkAnnivSMS")
                propertyDict.Add("_WorkAnnivSubject", "WorkAnnivSubject")
                propertyDict.Add("_WorkAnnivUserIds", "WorkAnnivUserIds")
                propertyDict.Add("_WorkAnnivUsr", "WorkAnnivUsr")
                propertyDict.Add("_WorkAnnivYear", "WorkAnnivYear")
                propertyDict.Add("_WorkingDaysHeadId", "WorkingDaysHeadID")
                propertyDict.Add("_WScrComputType", "WScrComputType")

                Session("ConfigPropsDict") = propertyDict

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function GetPropertyInfo(Of TResult)(ByVal propertyExpression As Expression(Of Func(Of TResult))) As KeyValuePair(Of String, System.Type)
        Try
            ' Ensure the expression body is a MemberExpression
            Dim memberExpr = TryCast(propertyExpression.Body, MemberExpression)
            If memberExpr Is Nothing Then
                Throw New ArgumentException("Provided delegate is not a valid property expression.")
            End If

            ' Extract property info
            Dim propertyInfo = TryCast(memberExpr.Member, Reflection.PropertyInfo)
            If propertyInfo Is Nothing Then
                Throw New ArgumentException("Provided expression does not point to a property.")
            End If

            ' Get the property name and type
            Dim propertyName = propertyInfo.Name
            Dim propertyType As Type = propertyInfo.PropertyType

            Return New KeyValuePair(Of String, System.Type)(propertyName, propertyType)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return New KeyValuePair(Of String, System.Type)(ex.Message, Nothing)
        End Try
    End Function

 'S.SANDEEP |21-JAN-2024| -- START
    'ISSUE/ENHANCEMENT : Sprint_2025-02
    Public Shared Function GetAssessmentView(ByVal intEmpId As Integer, ByVal wPg As Page) As String
        Dim htmlTable As New System.Text.StringBuilder
        Dim DisplayMessage As New CommonCodes
        Dim objEAnalysisMst As New clsevaluation_analysis_master
        Dim dtBSC_TabularGrid As DataTable
        Dim dicPeriodicScore As New Dictionary(Of Integer, DataTable)
        Dim dicPeriodName As New Dictionary(Of Integer, String)
        Try
            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data_AllPeriod(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                                                 intEmpId, _
                                                                                  HttpContext.Current.Session("CascadingTypeId"), _
                                                                                  0, _
                                                                                  -1, _
                                                                                  -1, _
                                                                                  CBool(HttpContext.Current.Session("EnableBSCAutomaticRating")), _
                                                                                  CInt(HttpContext.Current.Session("ScoringOptionId")), _
                                                                                  HttpContext.Current.Session("Database_Name").ToString(), _
                                                                                  HttpContext.Current.Session("DontAllowRatingBeyond100"), _
                                                                                  HttpContext.Current.Session("Fin_year"), _
                                                                                  HttpContext.Current.Session("fin_startdate"), _
                                                                                  dicPeriodicScore, _
                                                                                  dicPeriodName, _
                                                                                  True, _
                                                                                  HttpContext.Current.Session("fmtCurrency"))

            Dim lstColumns As List(Of String) = New List(Of String)

            If dtBSC_TabularGrid.Columns.Contains("Field1") Then
                lstColumns.Add("Field1")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field2") Then
                lstColumns.Add("Field2")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field3") Then
                lstColumns.Add("Field3")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field4") Then
                lstColumns.Add("Field4")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field5") Then
                lstColumns.Add("Field5")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field6") Then
                lstColumns.Add("Field6")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field7") Then
                lstColumns.Add("Field7")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field8") Then
                lstColumns.Add("Field8")
            End If

            If dtBSC_TabularGrid.Columns.Contains("Weight") Then
                lstColumns.Add("Weight")
            End If

            If dtBSC_TabularGrid.Columns.Contains("eself_P1") Then
                lstColumns.Add("eself_P1")
            End If

            If dtBSC_TabularGrid.Columns.Contains("eremark_P1") Then
                lstColumns.Add("eremark_P1")
            End If

            If dtBSC_TabularGrid.Columns.Contains("aself_P1") Then
                lstColumns.Add("aself_P1")
            End If

            If dtBSC_TabularGrid.Columns.Contains("aremark_P1") Then
                lstColumns.Add("aremark_P1")
            End If

            If CBool(HttpContext.Current.Session("IsCompanyNeedReviewer")) Then
                If dtBSC_TabularGrid.Columns.Contains("rself_P1") AndAlso dtBSC_TabularGrid.AsEnumerable().Where(Function(x) x.Field(Of String)("rself_P1") <> "").Count > 0 Then
                    lstColumns.Add("rself_P1")
                End If

                If dtBSC_TabularGrid.Columns.Contains("rremark_P1") AndAlso dtBSC_TabularGrid.AsEnumerable().Where(Function(x) x.Field(Of String)("rremark_P1") <> "").Count > 0 Then
                    lstColumns.Add("rremark_P1")
                End If
            End If


            If dtBSC_TabularGrid.Columns.Contains("eself_P2") Then
                lstColumns.Add("eself_P2")
            End If

            If dtBSC_TabularGrid.Columns.Contains("eremark_P2") Then
                lstColumns.Add("eremark_P2")
            End If

            If dtBSC_TabularGrid.Columns.Contains("aself_P2") Then
                lstColumns.Add("aself_P2")
            End If

            If dtBSC_TabularGrid.Columns.Contains("aremark_P2") Then
                lstColumns.Add("aremark_P2")
            End If

            If CBool(HttpContext.Current.Session("IsCompanyNeedReviewer")) Then
                If dtBSC_TabularGrid.Columns.Contains("rself_P2") AndAlso dtBSC_TabularGrid.AsEnumerable().Where(Function(x) x.Field(Of String)("rself_P2") <> "").Count > 0 Then
                    lstColumns.Add("rself_P2")
                End If

                If dtBSC_TabularGrid.Columns.Contains("rremark_P2") AndAlso dtBSC_TabularGrid.AsEnumerable().Where(Function(x) x.Field(Of String)("rremark_P2") <> "").Count > 0 Then
                    lstColumns.Add("rremark_P2")
                End If
            End If

            If dtBSC_TabularGrid.Columns.Contains("IsGrp") Then
                lstColumns.Add("IsGrp")
            End If

            Dim finaltable = dtBSC_TabularGrid.DefaultView().ToTable("list", False, lstColumns.ToArray())

            htmlTable.AppendLine("<!DOCTYPE html>")
            htmlTable.AppendLine("<html lang=""en"">")
            htmlTable.AppendLine("<head>")
            htmlTable.AppendLine("  <meta charset=""UTF-8"">")
            htmlTable.AppendLine("  <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">")
            htmlTable.AppendLine("  <title>Assessment Summary View</title>")
            'htmlTable.AppendLine("  <style>")
            ''htmlTable.AppendLine("      body { font-family: Arial, sans-serif;} ")
            ''htmlTable.AppendLine("      header { width: 98%; border-radius: 8px; padding: 4px 12px; background-color: steelblue; color: #fff; text-align: center; font-size: 1.5rem; font-weight: bold; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);} ")
            ''htmlTable.AppendLine("      table { border-collapse: collapse; width: 100%; margin: 20px 0; color: #fff;} ")
            ''htmlTable.AppendLine("      th {border: 1px solid #000; text-align: left; vertical-align: top; padding: 8px;} ")
            ''htmlTable.AppendLine("      th { background-color: steelblue; color: #fff; font-weight: bold; } ")
            ''htmlTable.AppendLine("      td { border-collapse: collapse; color: #000; border: 1px solid #000; text-align: left; vertical-align: top; padding : 5px; } ")
            ''htmlTable.AppendLine("      td.highlight { background-color: #ff0;} ")
            ''htmlTable.AppendLine("table { " & _
            ''                        "text-align: left; vertical-align: top; border-collapse: collapse; " & _
            ''                        "width: 95%; " & _
            ''                        "margin: 8px auto; " & _
            ''                        "background-color: #fff; " & _
            ''                        "box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); " & _
            ''                        "border-radius: 8px; " & _
            ''                        "overflow: hidden; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        "th, td { " & _
            ''                        "text-align: left; vertical-align: top; border: 1px solid #ddd; " & _
            ''                        "text-align: left; " & _
            ''                        "padding: 10px; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        "th { " & _
            ''                        "text-align: left; vertical-align: top; background-color: steelblue; " & _
            ''                        "color: #fff; " & _
            ''                        "font-weight: bold; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        "td { " & _
            ''                        "text-align: left; vertical-align: top; background-color: #fdfdfd; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        "td.highlight { " & _
            ''                        "background-color: #ffe066; " & _
            ''                        "font-weight: bold; " & _
            ''                        "color: #333; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        "tr:nth-child(even) td { " & _
            ''                        "background-color: #f9f9f9; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        "tr:hover td { " & _
            ''                        "background-color: #e3f2fd; " & _
            ''                        "} " & _
            ''                        " " & _
            ''                        ".table-title { " & _
            ''                        "font-weight: bold; " & _
            ''                        "text-align: center; " & _
            ''                        "background-color: #f4f6f8; " & _
            ''                        "color: #555; " & _
            ''                        "padding: 10px 0; " & _
            ''                        "border: none; " & _
            ''                        "} ")
            'htmlTable.AppendLine("#assessment-summary header { " & _
            '                          "font-family: Arial, sans-serif; " & _
            '                          "width: 100%; " & _
            '                          "border-radius: 8px; " & _
            '                          "background-color: steelblue; " & _
            '                          "color: #fff; " & _
            '                          "text-align: center; " & _
            '                          "font-size: 1.5rem; " & _
            '                          "font-weight: bold; " & _
            '                          "box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); " & _
            '                        "} " & _
            '                        "#assessment-summary .styled-table.container { " & _
            '                          "width: 98%; " & _
            '                          "margin: 0 auto; " & _
            '                        "} " & _
            '                        "#assessment-summary .styled-table { " & _
            '                          "vertical-align: top; text-align: left; " & _
            '                          "border-collapse: collapse; " & _
            '                          "width: 100%; " & _
            '                          "margin: 8px 0; " & _
            '                          "background-color: #fff; " & _
            '                          "box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); " & _
            '                          "border-radius: 8px; " & _
            '                          "overflow: hidden; " & _
            '                        "} " & _
            '                        "#assessment-summary .styled-table th, td { " & _
            '                          "vertical-align: top; border: 1px solid #ddd; " & _
            '                          "padding: 10px; " & _
            '                        "} " & _
            '                        "#assessment-summary .styled-table th { " & _
            '                          "vertical-align: top; background-color: steelblue; " & _
            '                          "color: #fff; " & _
            '                          "font-weight: bold; " & _
            '                        "} " & _
            '                        "#assessment-summary .styled-table td { " & _
            '                          "vertical-align: top; background-color: #fdfdfd; " & _
            '                        "} " & _
            '                        "#assessment-summary .styled-table td.highlight { " & _
            '                          "vertical-align: top; background-color: #ffe066; " & _
            '                          "font-weight: bold; " & _
            '                          "color: #333; " & _
            '                        "} " & _
            '                       "#assessment-summary  .table-container .styled-table { " & _
            '                          "max-height: 600px;  " & _
            '                          "overflow-y: auto; " & _
            '                          "border: 1px solid #ddd; " & _
            '                        "} " & _
            '                        "#assessment-summary .fixed-header .styled-table thead th { " & _
            '                          "position: sticky; " & _
            '                          "top: 0; " & _
            '                          "z-index: 2; " & _
            '                        "} " & _
            '                    " ")
            'htmlTable.AppendLine("  </style>")
            htmlTable.AppendLine("</head>")
            htmlTable.AppendLine("<body>")
            htmlTable.AppendLine("  <div id=""assessment-summary"">")
            htmlTable.AppendLine("<div>")
            htmlTable.AppendLine("<header style=""font-family: Arial, sans-serif; width: 100%; border-radius: 8px; background-color: steelblue; color: #fff; text-align: center; font-size: 1.5rem; font-weight: bold; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);""> Assessment Summary View </header>")
            htmlTable.AppendLine("</div>")
            htmlTable.AppendLine("<div style=""overflow: auto; height: 60vh;"">")
            htmlTable.AppendLine("  <table style=""vertical-align: top; text-align: left; border-collapse: collapse; width: 100%; margin: 8px 0; background-color: #fff; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); border-radius: 8px; overflow: hidden;"">")
            htmlTable.AppendLine("    <thead>")
            htmlTable.AppendLine("      <tr>")

            Dim iValue As Integer = 0
            For Each col As DataColumn In finaltable.Columns
                If col.ColumnName = "IsGrp" Then Continue For
                If col.ColumnName.Contains("_P") = True Then
                    If iValue <> CInt(col.ColumnName.ToString().Substring(col.ColumnName.ToString().Length - 1)) Then
                        iValue = CInt(col.ColumnName.ToString().Substring(col.ColumnName.ToString().Length - 1))
                        htmlTable.AppendLine("        <th colspan=""6"" style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: steelblue; color: #fff; font-weight: bold;"">" & dicPeriodName(col.ColumnName.ToString().Substring(col.ColumnName.ToString().Length - 1)).Trim() & "</th>")
                    End If
                Else
                    htmlTable.AppendLine("        <th rowspan=""2"" style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: steelblue; color: #fff; font-weight: bold;"">" & col.Caption.Trim() & "</th>")
                End If
            Next

            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("      <tr>")
            For Each col As DataColumn In finaltable.Columns
                If col.ColumnName = "IsGrp" Then Continue For
                If col.ColumnName.Contains("_P") = True Then
                    htmlTable.AppendLine("        <th style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: steelblue; color: #fff; font-weight: bold;"">" & col.Caption.Trim() & "</th>")
                End If
            Next

            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("    </thead>")
            htmlTable.AppendLine("    <tbody>")
            For Each row As DataRow In finaltable.Rows
                If row("IsGrp") = True Then
                    htmlTable.AppendLine("      <tr>")
                    htmlTable.AppendLine("        <td colspan=" & finaltable.Columns.Count - 1 & " style=""vertical-align: top; background-color: #ffe066; font-weight: bold; color: #333;""><B>" & row("Field1").ToString().Trim() & "</B></td>")
                    htmlTable.AppendLine("      </tr>")
                Else
                    htmlTable.AppendLine("      <tr>")
                    For Each col As DataColumn In finaltable.Columns
                        If col.ColumnName = "IsGrp" Then Continue For
                        htmlTable.AppendLine("        <td style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: #fdfdfd;"">" & row(col).ToString().Trim() & "</td>")
                    Next
                    htmlTable.AppendLine("      </tr>")
                End If
            Next
            htmlTable.AppendLine("    </tbody>")
            htmlTable.AppendLine("  </table>")
            htmlTable.AppendLine("</div>")
            htmlTable.AppendLine("<br>")
            htmlTable.AppendLine("<div>")
            htmlTable.AppendLine("  <table style=""vertical-align: top; text-align: left; border-collapse: collapse; width: 100%; margin: 8px 0; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); border-radius: 8px; overflow: hidden;"">")
            htmlTable.AppendLine("    <thead>")
            htmlTable.AppendLine("      <tr>")
            htmlTable.AppendLine("        <td colspan=""8"" style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: #ffe066;""><B>" & dicPeriodName(dicPeriodName.Keys(0)).Trim() & "</B></td>")
            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("      <tr>")
            Dim xScore = dicPeriodicScore(dicPeriodicScore.Keys(0)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 6, "Self Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If

            xScore = dicPeriodicScore(dicPeriodicScore.Keys(0)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 7, "Assessor Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If

            xScore = dicPeriodicScore(dicPeriodicScore.Keys(0)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 8, "Reviewer Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If

            xScore = dicPeriodicScore(dicPeriodicScore.Keys(0)).AsEnumerable().Select(Function(x) x.Field(Of Decimal)("finaloverallscore")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 10001, "Final Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If
            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("      <tr>")
            htmlTable.AppendLine("        <td colspan=""8"" style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: #ffe066;""><B>" & dicPeriodName(dicPeriodName.Keys(1)).Trim() & "</B></td>")
            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("      <tr>")
            xScore = dicPeriodicScore(dicPeriodicScore.Keys(1)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 6, "Self Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If

            xScore = dicPeriodicScore(dicPeriodicScore.Keys(1)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 7, "Assessor Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If

            xScore = dicPeriodicScore(dicPeriodicScore.Keys(1)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 8, "Reviewer Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If

            xScore = dicPeriodicScore(dicPeriodicScore.Keys(1)).AsEnumerable().Select(Function(x) x.Field(Of Decimal)("finaloverallscore")).ToList
            If xScore.Count > 0 Then
                htmlTable.AppendLine("        <td colspan=""2""><B>" & Language.getMessage("frmPerformanceEvaluation", 10001, "Final Score :") & " " & xScore(0) & "</B></td>")
            Else
                htmlTable.AppendLine("        <td colspan=""2""></td>")
            End If
            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("      <tr>")
            If dicPeriodicScore(dicPeriodicScore.Keys(1)).Rows.Count > 0 Then
                xScore = dicPeriodicScore(dicPeriodicScore.Keys(1)).AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.AVG_FINAL_RESULT_SCORE _
                                                                                   ) _
                                                                   .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                htmlTable.AppendLine("        <td colspan=""8"" style=""vertical-align: top; border: 1px solid #ddd; padding: 10px; background-color: #ffe066;""><B> Fianl Average Score : " & xScore(0) & "</B></td>")
            End If
            htmlTable.AppendLine("      </tr>")
            htmlTable.AppendLine("    </thead>")
            htmlTable.AppendLine("  </table>")
            htmlTable.AppendLine("</div>")
            htmlTable.AppendLine("  </div>")
            htmlTable.AppendLine("</body>")
            htmlTable.AppendLine("</html>")

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
            DisplayMessage.DisplayError(ex, wPg)
        Finally
            objEAnalysisMst = Nothing
        End Try
        Return htmlTable.ToString()
    End Function
    'S.SANDEEP |21-JAN-2024| -- END

End Class

