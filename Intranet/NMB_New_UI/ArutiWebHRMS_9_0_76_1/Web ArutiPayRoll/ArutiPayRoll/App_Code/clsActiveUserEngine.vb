﻿Imports Microsoft.VisualBasic

Public Class clsActiveUserEngine

#Region " Members "

    Private Shared dicActiveUsers As New Dictionary(Of String, clsActiveUsers)

#End Region

#Region " Constructors "
    Public Sub New()

    End Sub
#End Region


    Public Shared Function Joined(ByVal strUserId As String, ByVal strUserName As String, ByVal enUserType As clsActiveUsers.enUserType) As String
        '*** Commented due to performance issue at NMB
        'SyncLock dicActiveUsers
        '    Dim user As clsActiveUsers = New clsActiveUsers(strUserId, strUserName, enUserType)
        '    user.mblnIsActive = True
        '    user.mstrUserName = strUserName
        '    user.mdtLastSeen = DateTime.Now
        '    user.menUserType = enUserType


        '    If dicActiveUsers.ContainsKey(strUserId) = False Then
        '        dicActiveUsers.Add(strUserId, user)
        '    End If
        'End SyncLock

        Return ""
    End Function

    Public Shared Function Leaved(ByVal strUserId As String) As String
        '*** Commented due to performance issue at NMB
        'SyncLock dicActiveUsers
        '    If dicActiveUsers.ContainsKey(strUserId) = True Then
        '        dicActiveUsers.Remove(strUserId)
        '    End If
        'End SyncLock
        Return ""
    End Function

    Public Shared Function GetUserName() As IEnumerable(Of String)
        SyncLock dicActiveUsers
            GetUserName = New List(Of String)
            Dim s As String
            For Each key In dicActiveUsers
                s = dicActiveUsers.Item(key.Key).mstrUserName
                DirectCast(GetUserName, List(Of String)).Add(s)
            Next
        End SyncLock
    End Function

End Class
