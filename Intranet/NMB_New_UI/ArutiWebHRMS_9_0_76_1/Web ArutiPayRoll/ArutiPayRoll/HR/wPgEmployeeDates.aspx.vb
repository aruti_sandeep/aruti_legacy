﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region

Partial Class wPgEmployeeDates
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User

    'S.SANDEEP [ 04 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Dim objEmp As New clsEmployee_Master

    'S.SANDEEP [ 04 JULY 2012 ] -- END

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    'Pinkal (06-May-2014) -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAction As New clsAction_Reason
        Try
            dsCombo = objAction.getComboList("Reason", True, False)
            With cboReason
                .DataValueField = "actionreasonunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Reason")
                .DataBind()
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("Procedure FillCombo : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub SetInfo()
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = Session("Employeeunkid")
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End


            txtEmployee.Text = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
            cboReason.SelectedValue = CStr(objEmp._Actionreasonunkid)
            If objEmp._Appointeddate <> Nothing Then
                dtpAppointDate.SetDate = objEmp._Appointeddate.Date
            Else
                dtpAppointDate.SetDate = Nothing
            End If

            If objEmp._Confirmation_Date <> Nothing Then
                dtpConfDate.SetDate = objEmp._Confirmation_Date.Date
            Else
                dtpConfDate.SetDate = Nothing
            End If

            If objEmp._Birthdate <> Nothing Then
                dtpBirthdate.SetDate = objEmp._Birthdate.Date
            Else
                dtpBirthdate.SetDate = Nothing
            End If

            If objEmp._Suspende_From_Date <> Nothing Then
                dtpSuspFrom.SetDate = objEmp._Suspende_From_Date.Date
            Else
                dtpSuspTo.SetDate = Nothing
            End If

            If objEmp._Suspende_To_Date <> Nothing Then
                dtpSuspTo.SetDate = objEmp._Suspende_To_Date.Date
            Else
                dtpSuspTo.SetDate = Nothing
            End If

            If objEmp._Probation_From_Date <> Nothing Then
                dtpProbFrom.SetDate = objEmp._Probation_From_Date.Date
            Else
                dtpProbFrom.SetDate = Nothing
            End If

            If objEmp._Probation_To_Date <> Nothing Then
                dtpProbTo.SetDate = objEmp._Probation_To_Date.Date
            Else
                dtpProbTo.SetDate = Nothing
            End If

            If objEmp._Empl_Enddate <> Nothing Then
                dtpEmplEndDate.SetDate = objEmp._Empl_Enddate.Date
            Else
                dtpEmplEndDate.SetDate = Nothing
            End If

            If objEmp._Termination_From_Date <> Nothing Then
                dtpLeavingDate.SetDate = objEmp._Termination_From_Date.Date
            Else
                dtpLeavingDate.SetDate = Nothing
            End If

            If objEmp._Termination_To_Date <> Nothing Then
                dtpRetrDate.SetDate = objEmp._Termination_To_Date.Date
            Else
                dtpRetrDate.SetDate = Nothing
            End If

            If objEmp._Reinstatementdate <> Nothing Then
                dtpReinstatement.SetDate = objEmp._Reinstatementdate.Date
            Else
                dtpReinstatement.SetDate = Nothing
            End If


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            If objEmp._Isapproved Then
                dtpConfDate.Enabled = False
                dtpEmplEndDate.Enabled = False
                dtpLeavingDate.Enabled = False
                dtpProbFrom.Enabled = False
                dtpProbTo.Enabled = False
                dtpRetrDate.Enabled = False
                dtpSuspFrom.Enabled = False
                dtpSuspTo.Enabled = False
                dtpReinstatement.Enabled = False
                cboReason.Enabled = False
                If CInt(cboReason.SelectedValue) <= 0 Then cboReason.SelectedItem.Text = ""
            End If
            'Pinkal (28-Dec-2015) -- End


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Dim objEmp As New clsEmployee_Master
        Try


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = Session("Employeeunkid")
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End


            objEmp._Actionreasonunkid = CInt(cboReason.SelectedValue)

            If dtpAppointDate.IsNull = False Then
                objEmp._Appointeddate = dtpAppointDate.GetDate.Date
            Else
                objEmp._Appointeddate = Nothing
            End If

            If dtpConfDate.IsNull = False Then
                objEmp._Confirmation_Date = dtpConfDate.GetDate.Date
            Else
                objEmp._Confirmation_Date = Nothing
            End If

            If dtpBirthdate.IsNull = False Then
                objEmp._Birthdate = dtpBirthdate.GetDate.Date
            Else
                objEmp._Birthdate = Nothing
            End If

            If dtpSuspFrom.IsNull = False Then
                objEmp._Suspende_From_Date = dtpSuspFrom.GetDate.Date
            Else
                objEmp._Suspende_From_Date = Nothing
            End If

            If dtpSuspTo.IsNull = False Then
                objEmp._Suspende_To_Date = dtpSuspTo.GetDate.Date
            Else
                objEmp._Suspende_To_Date = Nothing
            End If

            If dtpProbFrom.IsNull = False Then
                objEmp._Probation_From_Date = dtpProbFrom.GetDate.Date
            Else
                objEmp._Probation_From_Date = Nothing
            End If

            If dtpProbTo.IsNull = False Then
                objEmp._Probation_To_Date = dtpProbTo.GetDate.Date
            Else
                objEmp._Probation_To_Date = Nothing
            End If

            If dtpEmplEndDate.IsNull = False Then
                objEmp._Empl_Enddate = dtpEmplEndDate.GetDate.Date
            Else
                objEmp._Empl_Enddate = Nothing
            End If

            If dtpLeavingDate.IsNull = False Then
                objEmp._Termination_From_Date = dtpLeavingDate.GetDate.Date
            Else
                objEmp._Termination_From_Date = Nothing
            End If

            If dtpRetrDate.IsNull = False Then
                objEmp._Termination_To_Date = dtpRetrDate.GetDate.Date
            Else
                objEmp._Termination_To_Date = Nothing
            End If

            If dtpReinstatement.IsNull = False Then
                objEmp._Reinstatementdate = dtpReinstatement.GetDate.Date
            Else
                objEmp._Reinstatementdate = Nothing
            End If

            objEmp._Companyunkid = CInt(Session("CompanyUnkId"))
            Blank_ModuleName()
            objEmp._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            objEmp._WebClientIP = CStr(Session("IP_ADD"))
            objEmp._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objEmp._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''Sohail (23 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'If objEmp.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then
            '    'If objEmp.Update = False Then
            '    'Sohail (23 Apr 2012) -- End
            '    msg.DisplayMessage(objEmp._Message, Me)
            'End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'Dim blnFlag As Boolean = objEmp.Update(CStr(Session("Database_Name")), _
            '                                        CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        CStr(Session("IsArutiDemo")), _
            '                                        CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                        CInt(Session("NoOfEmployees")), _
            '                                        CInt(Session("UserId")), False, _
            '                                        CStr(Session("UserAccessModeSetting")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                        CBool(Session("IsIncludeInactiveEmp")), _
            '                                        CBool(Session("AllowToApproveEarningDeduction")), _
            '                            ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
            '                                        CStr(Session("EmployeeAsOnDate")), , _
            '                                        CBool(Session("IsImgInDataBase")))

            Dim blnFlag As Boolean = objEmp.Update(CStr(Session("Database_Name")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    CStr(Session("IsArutiDemo")), _
                                                    CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                                    CInt(Session("NoOfEmployees")), _
                                                    CInt(Session("UserId")), False, _
                                                    CStr(Session("UserAccessModeSetting")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), _
                                                    CBool(Session("AllowToApproveEarningDeduction")), _
                                                ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")) _
                                                , CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                    CStr(Session("EmployeeAsOnDate")), , _
                                                    CBool(Session("IsImgInDataBase")))

            'Pinkal (18-Aug-2018) -- End

            If blnFlag = False Then
                msg.DisplayMessage(objEmp._Message, Me)
            End If
            'Shani(20-Nov-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidDates() As Boolean
        Try
            If dtpProbTo.IsNull = False AndAlso dtpProbFrom.IsNull = False Then
                If dtpProbTo.GetDate.Date <= dtpProbFrom.GetDate.Date Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Probation To Date cannot be less than Probation From Date."), Me)
                    Return False
                End If
            End If

            If dtpSuspFrom.IsNull = False Then
                If dtpSuspFrom.GetDate.Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Suspension date cannot be less then today date. Please enter correct suspension date."), Me)
                    Return False
                End If
            End If

            If dtpSuspFrom.IsNull = False AndAlso dtpSuspTo.IsNull = False Then
                If dtpSuspTo.GetDate.Date <= dtpSuspFrom.GetDate.Date Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Suspended To Date cannot be less than Suspended From Date."), Me)
                    Return False
                End If
            End If

            If dtpBirthdate.IsNull = False Then
                If dtpBirthdate.GetDate.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "Birth date cannot be greater than or equal to today date. Please enter proper birth date."), Me)
                    Return False
                End If
            End If

            If dtpRetrDate.GetDate.Date <= dtpAppointDate.GetDate.Date Then
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Retirement date cannot be less than or equal to Appointed date. Please enter proper Retirement date."), Me)
                Return False
            End If

            If dtpConfDate.GetDate.Date < dtpAppointDate.GetDate.Date Then
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Confirmation date cannot be lesser than Appointed date. Please enter proper Confirmation date."), Me)
                Return False
            End If

            If dtpLeavingDate.IsNull = False Then
                If dtpLeavingDate.GetDate.Date > dtpRetrDate.GetDate.Date Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 53, "Leaving date cannot be greater than Retirement date. Please enter proper Leaving date."), Me)
                    Return False
                End If
            End If

            If dtpEmplEndDate.IsNull = False AndAlso dtpLeavingDate.IsNull = False Then
                If dtpEmplEndDate.GetDate.Date > dtpLeavingDate.GetDate.Date Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 54, "Employment End date cannot be greater than  Leaving date. Please enter proper Employment End date."), Me)
                    Return False
                End If
            End If

            If dtpEmplEndDate.IsNull = False Then
                If CInt(cboReason.SelectedValue) <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 55, "Employment End Reason is mandatory information. Please select Employment End Reason."), Me)
                    Return False
                End If
            End If

            Dim objMaster As New clsMasterData
            Dim intPeriodId As Integer
            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpAppointDate.GetDate.Date, , Session("Fin_year"))
            intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpAppointDate.GetDate.Date, CInt(Session("Fin_year")))
            'Shani(20-Nov-2015) -- End

            'Sohail (18 Oct 2019) -- Start
            'NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
            Dim blnSkip As Boolean = False
            Dim intLastClosedPeriod As Integer = 0
            If CBool(Session("AllowToChangeAppointmentDateOnClosedPeriod")) = True Then
                intLastClosedPeriod = objMaster.getLastPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.CLOSE, True)
                If intLastClosedPeriod = intPeriodId Then
                    blnSkip = True
                End If
            End If
            'Sohail (18 Oct 2019) -- End

            If intPeriodId > 0 Then
                Dim objPeriod As New clscommom_period_Tran

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = intPeriodId
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intPeriodId
                'Shani(20-Nov-2015) -- End

                'Sohail (18 Oct 2019) -- Start
                'NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
                'If objPeriod._Statusid = Aruti.Data.modGlobal.StatusType.Close Then
                If objPeriod._Statusid = Aruti.Data.modGlobal.StatusType.Close AndAlso blnSkip = False Then
                    'Sohail (18 Oct 2019) -- End
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, you cannot set this appointment date. Reason the period is already closed. Please set new appointment date."), Me)
                    Return False
                End If
            End If

            If dtpAppointDate.IsNull = True Then
                msg.DisplayMessage("Sorry, Appointment Date is mandatory information. Please set the Appointment Date.", Me)
                Return False
            End If

            If dtpConfDate.IsNull = True Then
                msg.DisplayMessage("Sorry, Confirmation Date is mandatory information. Please set the Confirmation Date.", Me)
                Return False
            End If

            If dtpRetrDate.IsNull = True Then
                msg.DisplayMessage("Sorry, Appointment Date is mandatory information. Please set the Appointment Date.", Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsValidDates() = False Then Exit Sub
            Call SetValue()
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
            Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
            Response.Redirect(mstrURLReferer, False)
            'Sohail (09 Nov 2020) -- End
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 



#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If



            If (Page.IsPostBack = False) Then

                SetLanguage()

                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    btnSave.Visible = False
                Else
                    lblEmployee.Visible = False : txtEmployee.Visible = False : btnSave.Visible = False
                End If

                dtpAppointDate.Enabled = CBool(Session("ChangeAppointmentDate"))
                dtpBirthdate.Enabled = CBool(Session("SetEmployeeBirthDate"))
                dtpConfDate.Enabled = CBool(Session("ChangeConfirmationDate"))
                dtpEmplEndDate.Enabled = CBool(Session("SetEmploymentEndDate"))
                dtpLeavingDate.Enabled = CBool(Session("SetLeavingDate"))
                dtpProbFrom.Enabled = CBool(Session("SetEmpProbationDate"))
                dtpProbTo.Enabled = CBool(Session("SetEmpProbationDate"))
                dtpReinstatement.Enabled = CBool(Session("SetReinstatementdate"))
                dtpRetrDate.Enabled = CBool(Session("ChangeRetirementDate"))
                dtpSuspFrom.Enabled = CBool(Session("SetEmpSuspensionDate"))
                dtpSuspTo.Enabled = CBool(Session("SetEmpSuspensionDate"))

                Call FillCombo()
                Call SetInfo()

                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx"
                End If
            Else
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mstrURLReferer") = mstrURLReferer
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Nov 2020) -- End

#End Region

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbDatesDetails", Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbDatesDetails", Me.Closebotton1.PageHeading)
            'SHANI [01 FEB 2015]--END 
            Me.lblAppointDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAppointDate.ID, Me.lblAppointDate.Text)
            Me.lblBirtdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBirtdate.ID, Me.lblBirtdate.Text)
            Me.lblSuspendedFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSuspendedFrom.ID, Me.lblSuspendedFrom.Text)
            Me.lblSuspendedTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSuspendedTo.ID, Me.lblSuspendedTo.Text)
            Me.lblProbationFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblProbationFrom.ID, Me.lblProbationFrom.Text)
            Me.lblProbationTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblProbationTo.ID, Me.lblProbationTo.Text)
            Me.lblLeavingDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeavingDate.ID, Me.lblLeavingDate.Text)
            Me.lblEmplReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmplReason.ID, Me.lblEmplReason.Text)
            Me.lblConfirmationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblConfirmationDate.ID, Me.lblConfirmationDate.Text)
            Me.lblEmplDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmplDate.ID, Me.lblEmplDate.Text)
            Me.lblRetirementDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRetirementDate.ID, Me.lblRetirementDate.Text)
            Me.lblReinstatementDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReinstatementDate.ID, Me.lblReinstatementDate.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub


End Class
