﻿<%@ Page Title="Membership Info" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgEmpMemberships.aspx.vb" Inherits="wPgEmpMemberships" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="ucYesNo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-8 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Membership Info Add/Edit"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipName" runat="server" Text="Mem. Catagory" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboMemCategory" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMemIssueDate" runat="server" Text="Issue Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpIssueDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipType" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboMembership" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblActivationDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpStartDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipNo" runat="server" Text="Membership No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtMembershipNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMemExpiryDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEndDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control" Rows="3" Wrap="true"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_active" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnmemClose" PopupControlID="pnlActiveMembership" TargetControlID="HiddenField1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlActiveMembership" runat="server" CssClass="newpopup" Style="display: none;
                    width: 430px; z-index: 100099!important;" DefaultButton="btnOk">
                    <div class="panel-primary" style="margin-bottom: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="lblTitle" runat="server" Text="Assign Head" />
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblActMembership" runat="server" Text="Membership"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAMembership" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblEmpHead" runat="server" Text="Emp. Head"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmpHead" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCoHead" runat="server" Text="Co. Head"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCoHead" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblEffctivePeriod" runat="server" Text="Effective Period"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboEffectivePeriod" runat="server" ReadOnly="true" Width="252px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous Slab" />
                                                <asp:CheckBox ID="chkOverwriteHeads" runat="server" Text="Overwrite Heads" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="btnDefault" />
                                        <asp:Button ID="btnmemClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ucYesNo:ConfirmYesNo ID="popup_yesno" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
