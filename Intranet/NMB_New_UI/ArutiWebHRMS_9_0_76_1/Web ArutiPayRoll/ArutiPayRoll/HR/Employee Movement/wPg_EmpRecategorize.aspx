﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EmpRecategorize.aspx.vb"
    Inherits="HR_wPg_EmpRecategorize" Title="Re-Categorize Employee" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    
        <script type="text/javascript">
            function IsValidAttach() {
                if (parseInt($('select.cboScanDcoumentType')[0].value) <= 0) {
                    alert('Please Select Document Type.');
                    $('.cboScanDcoumentType').focus();
                    return false;
                }
            }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Re-Categorize Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Re-Categorize Information"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkJobfilter" runat="server" ToolTip="Job Filter">
                                        <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboJob" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChangeReason" runat="server" Text="Change Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboChangeReason" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlAppointmentdate" runat="server" Visible="false">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAppointmentdate" runat="server" Visible="false" Text="Appoint Date"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppointDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save Changes" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="height: 250px">
                                    <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                        AllowPaging="false">
                                        <ItemStyle CssClass="griviewitem" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                      <i class="fas fa-exclamation-circle text-info"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                HeaderText="" ItemStyle-Width="30px">
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" ToolTip="Edit">
                                                        <i class="fa fa-pencil-alt text-primary"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                            Visible="false">
                                                            <i class="fas fa-eye text-primary"></i> 
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                HeaderText="" ItemStyle-Width="30px">
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                    <i class="fas fa-trash text-danger"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="effectivedate" FooterText="dgcolhChangeDate" HeaderText="Effective Date"
                                                ReadOnly="true"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="JobGroup" FooterText="dgcolhJobGroup" HeaderText="Job Group"
                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Job" FooterText="dgcolhJob" HeaderText="Job" ReadOnly="true">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Reason" FooterText="dgcolhReason" HeaderText="Reason"
                                                ReadOnly="true"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="categorizationtranunkid" FooterText="objdgcolhrecategorizeunkid"
                                                HeaderText="objdgcolhrecategorizeunkid" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="isfromemployee" FooterText="objdgcolhFromEmp" HeaderText="objdgcolhFromEmp"
                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="adate" FooterText="objdgcolhAppointdate" HeaderText="objdgcolhAppointdate"
                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                            <%--13--%>
                                        </Columns>
                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                    </asp:DataGrid>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" CssClass="label label-warning"></asp:Label>
                                <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary"></asp:Label>
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <cc1:ModalPopupExtender ID="modalJobFilter" runat="server" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnlJobFilter" TargetControlID="lblCancelText1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlJobFilter" runat="server" CssClass="card modal-dialog">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Job Filter" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="height: 350px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblbranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbobranch" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblunitgroup" Style="margin-left: 10px" runat="server" Text="Unit Group"
                                    CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbounitgroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lbldeptgroup" runat="server" Text="Dept. Group" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbodeptgroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblunit" Style="margin-left: 10px" runat="server" Text="Units" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbounit" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lbldepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbodepartment" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblteam" Style="margin-left: 10px" runat="server" Text="Team" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboteam" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblsectiongroup" runat="server" Text="Sec. Group" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbosectiongroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblclassgroup" Style="margin-left: 10px" runat="server" Text="Class Group">
                                </asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboclassgroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblsection" runat="server" Text="Sections" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbosection" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblclass" Style="margin-left: 10px" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboclass" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblJobGroup" runat="server" Text="Job Group" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboJobGroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGrade" Style="margin-left: 10px" runat="server" Text="Grade" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboGrade" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGradeLevel" Style="margin-left: 10px" runat="server" Text="Grade Level">
                                </asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboGradeLevel" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnOkFilter" runat="server" CssClass="btn btn-primary" Text="Ok" />
                        <asp:Button ID="btnCloseFilter" runat="server" CssClass="btn btn-default" Text="Close" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" TargetControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-25">
                                <div id="fileuploader">
                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Add" />
                                </div>
                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                    CssClass="btn btn-primary" Text="Browse" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 150px;">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                          <i class="fas fa-trash text-danger"></i>     
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                </asp:Panel>
                
                <uc2:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
             <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    
        <script type="text/javascript">

            $(document).ready(function() {
                ImageLoad();
                $(".ajax-upload-dragdrop").css("width", "auto");
            });
            function ImageLoad() {
                if ($(".ajax-upload-dragdrop").length <= 0) {
                    $("#fileuploader").uploadFile({
                    url: "wPg_EmpRecategorize.aspx?uploadimage=mSEfU19VPc4=",
                        multiple: false,
                        method: "POST",
                        dragDropStr: "",
                        maxFileSize: 1024 * 1024,
                        showStatusAfterSuccess: false,
                        showAbort: false,
                        sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                        showDone: false,
                        fileName: "myfile",
                        onSuccess: function(path, data, xhr) {
                            $("#<%= btnSaveAttachment.ClientID %>").click();
                        },
                        onError: function(files, status, errMsg) {
                            alert(errMsg);
                        }
                    });
                }
            }

            $("body").on("click", "input[type=file]", function() {
                return IsValidAttach();
            });
    </script>
    
</asp:Content>
