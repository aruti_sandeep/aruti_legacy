﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class HR_wPg_EmpWorkPermit
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmWorkPermitTransaction"
    Dim DisplayMessage As New CommonCodes

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objEmployee As New clsEmployee_Master
    'Private objEWPermit As New clsemployee_workpermit_tran
    'Gajanan [4-Sep-2020] -- End

    Private mstrEmployeeCode As String = ""
    Private mdtAppointmentDate As Date = Nothing
    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    Private mblnIsResidentPermit As Boolean = False
    'S.SANDEEP [04-Jan-2018] -- END


    'Gajanan [12-NOV-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objAPermit As New clsPermit_Approval_Tran
    'Gajanan [4-Sep-2020] -- End

    'Gajanan [12-NOV-2018] -- END

    'Gajanan [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'Gajanan [15-NOV-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    'ENHANCEMENT:Movement Approval Flow Edit / Delete
    Private mdtPermitList As DataTable
    'S.SANDEEP |17-JAN-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Page's Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If




            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objlblCaption.Visible = False
            lblPendingData.Visible = False
            'Gajanan [12-NOV-2018] -- END

            If Not IsPostBack Then

                Call SetLanguage()

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                GC.Collect()
                'Gajanan [4-Sep-2020] -- End


                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                If Request.QueryString.Count <= 0 Then Exit Sub
                Dim mstrRequset As String = ""
                Try
                    mstrRequset = b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3)))
                Catch
                    Session("clsuser") = Nothing
                    DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                    Exit Sub
                End Try
                If mstrRequset.Trim.Length > 0 Then
                    If CBool(mstrRequset) Then
                        mblnIsResidentPermit = CBool(mstrRequset)
                    End If
                End If
                Call FillCombo()
                Fill_Grid()
            Else
                mblnIsResidentPermit = CBool(Me.ViewState("mblnIsResidentPermit"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = CInt(Me.ViewState("mintTransactionId"))
                'Gajanan [15-NOV-2018] -- End
                'S.SANDEEP [04-Jan-2018] -- END

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                mdtPermitList = CType(Me.ViewState("mdtPermitList"), DataTable)
                'S.SANDEEP |17-JAN-2019| -- END




            End If

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            If mblnIsResidentPermit Then
                Me.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Resident Permit")
                lblPageHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Resident Permit Information")
                lblDetialHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Resident Permit Information")
                lblWorkPermitNo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 102, "Permit No")
            End If
            'S.SANDEEP [04-Jan-2018] -- END


            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            btnSave.Enabled = CBool(Session("AllowToChangeEmpWorkPermit"))
            'Shani (08-Dec-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mblnIsResidentPermit") = mblnIsResidentPermit
            'Gajanan [15-NOV-2018] -- START
            Me.ViewState.Add("mintTransactionId", mintTransactionId)
            'Gajanan [15-NOV-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            'ENHANCEMENT:Movement Approval Flow Edit / Delete
            Me.ViewState.Add("mdtPermitList", mdtPermitList)
            'S.SANDEEP |17-JAN-2019| -- END



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEmployee As New clsEmployee_Master
        Dim dsComboList As New DataSet
        Dim objCountry As New clsMasterData
        Dim objCMaster As New clsCommon_Master
        'Gajanan [4-Sep-2020] -- End

        Try
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    dsComboList = objEmployee.GetEmployeeList("EmployeeList", True, True, , , , , , , , , , , , , , , , , , )
            'Else
            '    dsComboList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
            'End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB

            'dsComboList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                CStr(Session("UserAccessModeSetting")), True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''Shani(24-Aug-2015) -- End
            'With cboEmployee
            '    'Nilay (09-Aug-2016) -- Start
            '    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            '    '.DataTextField = "employeename"
            '    .DataTextField = "EmpCodeName"
            '    'Nilay (09-Aug-2016) -- End
            '    .DataValueField = "employeeunkid"
            '    .DataSource = dsComboList.Tables("EmployeeList")
            '    .SelectedValue = CStr(0)
            '    .DataBind()
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


            dsComboList = objCountry.getCountryList("Country", True)
            With cboIssueCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'dsComboList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.WORK_PERMIT, True, "List")
            If mblnIsResidentPermit Then
                dsComboList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RESIDENT_PERMIT, True, "List")
            Else
                dsComboList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.WORK_PERMIT, True, "List")
            End If
            'S.SANDEEP [04-Jan-2018] -- END
            With cboChangeReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEmployee = Nothing
            objCountry = Nothing
            objCMaster = Nothing
            If IsNothing(dsComboList) = False Then
                dsComboList = Nothing
            End If
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Private Sub Fill_Grid()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim dsList As New DataSet
        Dim objEWPermit As New clsemployee_workpermit_tran
        Dim objAPermit As New clsPermit_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            'Dim dsList As New DataSet
            'objEWPermit = New clsemployee_workpermit_tran
            'Gajanan [4-Sep-2020] -- End

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}       
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                'dsList = objEWPermit.GetList("List", CInt(cboEmployee.SelectedValue)).Clone
                dsList = objEWPermit.GetList("List", mblnIsResidentPermit, CInt(cboEmployee.SelectedValue)).Clone
                'S.SANDEEP [04-Jan-2018] -- END

                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(0).Visible = False
                dgvHistory.Columns(1).Visible = False
                dgvHistory.Columns(2).Visible = False
                'Varsha Rana (17-Oct-2017) -- End
                'Gajanan [12-NOV-2018] -- End

            Else
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                'dsList = objEWPermit.GetList("List", CInt(cboEmployee.SelectedValue))
                dsList = objEWPermit.GetList("List", mblnIsResidentPermit, CInt(cboEmployee.SelectedValue))
                'S.SANDEEP [04-Jan-2018] -- END

                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditWorkPermitEmployeeDetails"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteWorkPermitEmployeeDetails"))
                'Varsha Rana (17-Oct-2017) -- End
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))
                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)

                'S.SANDEEP |17-JAN-2019| -- START
                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "operationtypeid"
                    .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
                End With
                dsList.Tables(0).Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "OperationType"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)
                'S.SANDEEP |17-JAN-2019| -- End




                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim dsPending As New DataSet
                    dsPending = objAPermit.GetList("List", mblnIsResidentPermit, CInt(cboEmployee.SelectedValue))
                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsList.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END
            End If
            dgvHistory.AutoGenerateColumns = False
            dgvHistory.DataSource = dsList.Tables(0)
            dgvHistory.DataBind()
            mdtPermitList = dsList.Tables(0)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Grid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            objEWPermit = Nothing
            objAPermit = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtEffectiveDate.SetDate = Nothing
            dtIssueDate.SetDate = Nothing
            dtExpiryDate.SetDate = Nothing
            txtIssuePlace.Text = ""
            txtWorkPermitNo.Text = ""
            cboChangeReason.SelectedValue = CStr(0)
            cboIssueCountry.SelectedValue = CStr(0)
            txtAppointDate.Text = ""
            lblAppointDate.Visible = False
            txtAppointDate.Visible = False

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            pnlAppointDate.Visible = False
            'Gajanan [17-Sep-2020] -- End

            mdtAppointmentDate = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearControls:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub SetValue(ByVal objEWPermit As clsemployee_workpermit_tran, _
                         ByVal objAPermit As clsPermit_Approval_Tran)
        'Gajanan [4-Sep-2020] -- End

        Try
            'objEWPermit._Workpermittranunkid = CInt(Me.ViewState("Workpermittranunkid"))
            'objEWPermit._Effectivedate = dtEffectiveDate.GetDate.Date
            'objEWPermit._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'If dtExpiryDate.IsNull = True Then
            '    objEWPermit._Expiry_Date = Nothing
            'Else
            '    objEWPermit._Expiry_Date = dtExpiryDate.GetDate.Date
            'End If

            'If dtIssueDate.IsNull = True Then
            '    objEWPermit._Issue_Date = Nothing
            'Else
            '    objEWPermit._Issue_Date = dtIssueDate.GetDate.Date
            'End If
            'objEWPermit._Issue_Place = txtIssuePlace.Text
            'objEWPermit._Isvoid = False
            'objEWPermit._Userunkid = CInt(Session("UserId"))
            'objEWPermit._Voiddatetime = Nothing
            'objEWPermit._Voidreason = ""
            'objEWPermit._Voiduserunkid = -1
            'objEWPermit._Work_Permit_No = txtWorkPermitNo.Text
            'objEWPermit._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
            'objEWPermit._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            ''S.SANDEEP [04-Jan-2018] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 120
            'objEWPermit._IsResidentPermit = mblnIsResidentPermit
            ''S.SANDEEP [04-Jan-2018] -- END

            'Blank_ModuleName()
            'objEWPermit._WebFormName = mstrModuleName
            'objEWPermit._WebClientIP = CStr(Session("IP_ADD"))
            'objEWPermit._WebHostName = CStr(Session("HOST_NAME"))
            'StrModuleName2 = "mnuGeneralMaster"
            'StrModuleName3 = "mnuCoreSetups"

            'Gajanan [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                'Gajanan [15-NOV-2018] -- End
                'Hemant (23 Apr 2020) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                'objEWPermit._Workpermittranunkid = CInt(Me.ViewState("Workpermittranunkid"))
                If Me.ViewState("Workpermittranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                    objEWPermit._Workpermittranunkid = CInt(Me.ViewState("Workpermittranunkid"))
                End If
                'Hemant (23 Apr 2020) -- End
                objEWPermit._Effectivedate = dtEffectiveDate.GetDate.Date
                objEWPermit._Employeeunkid = CInt(cboEmployee.SelectedValue)
                If dtExpiryDate.IsNull = True Then
                    objEWPermit._Expiry_Date = Nothing
                Else
                    objEWPermit._Expiry_Date = dtExpiryDate.GetDate.Date
                End If

                If dtIssueDate.IsNull = True Then
                    objEWPermit._Issue_Date = Nothing
                Else
                    objEWPermit._Issue_Date = dtIssueDate.GetDate.Date
                End If
                objEWPermit._Issue_Place = txtIssuePlace.Text
                objEWPermit._Isvoid = False
                objEWPermit._Userunkid = CInt(Session("UserId"))
                objEWPermit._Voiddatetime = Nothing
                objEWPermit._Voidreason = ""
                objEWPermit._Voiduserunkid = -1
                objEWPermit._Work_Permit_No = txtWorkPermitNo.Text
                objEWPermit._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
                objEWPermit._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objEWPermit._IsResidentPermit = mblnIsResidentPermit
                Blank_ModuleName()
                objEWPermit._WebFormName = mstrModuleName
                objEWPermit._WebClientIP = CStr(Session("IP_ADD"))
                objEWPermit._WebHostName = CStr(Session("HOST_NAME"))
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            Else
                objAPermit._Audittype = enAuditType.ADD
                objAPermit._Audituserunkid = CInt(Session("UserId"))

                objAPermit._Effectivedate = dtEffectiveDate.GetDate()
                objAPermit._Employeeunkid = CInt(cboEmployee.SelectedValue)

                'Hemant (23 Apr 2020) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                'If dtExpiryDate.IsNull = False Then
                If dtExpiryDate.IsNull = True Then
                    'Hemant (23 Apr 2020) -- End
                    objAPermit._Expiry_Date = Nothing
                Else
                    objAPermit._Expiry_Date = dtExpiryDate.GetDate()
                End If

                'Hemant (23 Apr 2020) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                'If dtIssueDate.IsNull = False Then
                If dtIssueDate.IsNull = True Then
                    'Hemant (23 Apr 2020) -- End
                    objAPermit._Issue_Date = Nothing
                Else
                    objAPermit._Issue_Date = dtIssueDate.GetDate()
                End If

                objAPermit._Issue_Place = txtIssuePlace.Text
                objAPermit._Isvoid = False
                objAPermit._Voiddatetime = Nothing
                objAPermit._Voidreason = ""
                objAPermit._Voiduserunkid = -1
                objAPermit._Work_Permit_No = txtWorkPermitNo.Text
                objAPermit._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
                objAPermit._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                objAPermit._Isresidentpermit = mblnIsResidentPermit
                'S.SANDEEP [04-Jan-2018] -- END
                objAPermit._Isvoid = False
                objAPermit._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objAPermit._Voiddatetime = Nothing
                objAPermit._Voidreason = ""
                objAPermit._Voiduserunkid = -1
                objAPermit._Tranguid = Guid.NewGuid.ToString()
                objAPermit._Transactiondate = Now
                objAPermit._Remark = ""
                objAPermit._Rehiretranunkid = 0
                objAPermit._Mappingunkid = 0
                'Hemant (23 Apr 2020) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                'objAPermit._Isweb = False
                objAPermit._Isweb = True
                objAPermit._Isfinal = False
                objAPermit._Ip = Session("IP_ADD").ToString()
                objAPermit._Hostname = Session("HOST_NAME").ToString()
                objAPermit._Form_Name = mstrModuleName

                'S.SANDEEP |17-JAN-2019| -- START
                If Me.ViewState("Workpermittranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                    objAPermit._Workpermittranunkid = mintTransactionId
                    objAPermit._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objAPermit._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                Blank_ModuleName()
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private Sub Set_WorkPermit()
    Private Sub Set_WorkPermit(ByVal objEWPermit As clsemployee_workpermit_tran)
        'Gajanan [4-Sep-2020] -- End
        Try

            If Me.ViewState("Workpermittranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                objEWPermit._Workpermittranunkid = CInt(Me.ViewState("Workpermittranunkid"))
            End If

            Dim dsWorkPermit As New DataSet
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'dsWorkPermit = objEWPermit.Get_Current_WorkPermit(Now.Date, CInt(cboEmployee.SelectedValue))
            dsWorkPermit = objEWPermit.Get_Current_WorkPermit(Now.Date, mblnIsResidentPermit, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [04-Jan-2018] -- END
            If dsWorkPermit.Tables(0).Rows.Count > 0 Then
                txtWorkPermitNo.Text = dsWorkPermit.Tables(0).Rows(0).Item("work_permit_no").ToString
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("issue_date")) = False Then
                    dtIssueDate.SetDate = CDate(dsWorkPermit.Tables(0).Rows(0).Item("issue_date"))
                End If
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date")) = False Then
                    dtExpiryDate.SetDate = CDate(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date"))
                End If
                cboIssueCountry.SelectedValue = CStr(dsWorkPermit.Tables(0).Rows(0).Item("workcountryunkid"))
                txtIssuePlace.Text = CStr(dsWorkPermit.Tables(0).Rows(0).Item("issue_place"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Set_WorkPermit:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private Sub SetEditValue()
    Private Sub SetEditValue(ByVal objEWPermit As clsemployee_workpermit_tran)
        'Gajanan [4-Sep-2020] -- End

        Try

            If Me.ViewState("Workpermittranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                objEWPermit._Workpermittranunkid = CInt(Me.ViewState("Workpermittranunkid"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = objEWPermit._Workpermittranunkid
                'Gajanan [15-NOV-2018] -- END
            End If

            dtEffectiveDate.SetDate = objEWPermit._Effectivedate
            cboEmployee.SelectedValue = CStr(objEWPermit._Employeeunkid)
            If objEWPermit._Expiry_Date <> Nothing Then
                dtExpiryDate.SetDate = objEWPermit._Expiry_Date
            End If
            If objEWPermit._Issue_Date <> Nothing Then
                dtIssueDate.SetDate = objEWPermit._Issue_Date
            End If
            txtWorkPermitNo.Text = objEWPermit._Work_Permit_No
            txtIssuePlace.Text = objEWPermit._Issue_Place
            cboIssueCountry.SelectedValue = CStr(objEWPermit._Workcountryunkid)
            cboChangeReason.SelectedValue = CStr(objEWPermit._Changereasonunkid)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetEditValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private Function Validation
    Private Function Validation(ByVal objEWPermit As clsemployee_workpermit_tran) As Boolean
        'Gajanan [4-Sep-2020] -- End

        Try
            If dtEffectiveDate.IsNull = True Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), Me)
                dtEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If txtWorkPermitNo.Text.Trim.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Work Permit No. is mandatory information. Please give work permit no."), Me)
                txtWorkPermitNo.Focus()
                Return False
            End If

            If (CInt(cboIssueCountry.SelectedValue) <= 0) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Issue Country is mandatory information. Please select issue country."), Me)
                cboIssueCountry.Focus()
                Return False
            End If

            If (dtIssueDate.IsNull = False And dtExpiryDate.IsNull = True) Or (dtIssueDate.IsNull = True And dtExpiryDate.IsNull = False) Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, issue date and expiry date are madatory information."), Me)
                dtExpiryDate.Focus()
                Return False
            End If

            If dtIssueDate.IsNull = False AndAlso dtExpiryDate.IsNull = False Then
                If dtExpiryDate.GetDate <= dtIssueDate.GetDate Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, expiry date cannot be less or equal to issue date."), Me)
                    dtExpiryDate.Focus()
                    Return False
                End If
            End If


            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue."), Me)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtEffectiveDate.GetDate Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), Me)
                    dtEffectiveDate.Focus()
                End If
            End If

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objEWPermit.isExist(mblnIsResidentPermit, Nothing, txtWorkPermitNo.Text, CInt(cboEmployee.SelectedValue), 0) Then
                If mblnIsResidentPermit Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 2, "Sorry, resident permit no is already present for the selected employee."), Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 2, "Sorry, work permit no is already present for the selected employee."), Me)
                End If
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End


            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                Dim objPMovement As New clsEmployeeMovmentApproval
                Dim intPrivilegeId As Integer = 0
                Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                If mblnIsResidentPermit Then
                    intPrivilegeId = 1200
                    eMovement = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                Else
                    intPrivilegeId = 1199
                    eMovement = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                End If
                Dim strMsg As String = String.Empty
                strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString, Nothing, CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing


                'S.SANDEEP |16-JAN-2019| -- START
                If objEWPermit.isExist(mblnIsResidentPermit, dtEffectiveDate.GetDate(), , CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    If mblnIsResidentPermit Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 4, "Sorry, resident permit information is already present for the selected effective date."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 1, "Sorry, work permit information is already present for the selected effective date."), Me)
                    End If
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objEWPermit.Get_Current_WorkPermit(dtEffectiveDate.GetDate(), mblnIsResidentPermit, CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If dsList.Tables(0).Rows(0)("work_permit_no").ToString().Trim = txtWorkPermitNo.Text AndAlso CInt(dsList.Tables(0).Rows(0)("workcountryunkid")) = CInt(cboIssueCountry.SelectedValue) Then
                        If mblnIsResidentPermit Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 5, "Sorry, resident permit no is already present for the selected employee."), Me)
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 2, "Sorry, work permit no is already present for the selected employee."), Me)
                        End If
                        Return False
                    End If
                End If
                dsList = Nothing

                If objEWPermit.isExist(mblnIsResidentPermit, Nothing, txtWorkPermitNo.Text, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    If mblnIsResidentPermit Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 2, "Sorry, resident permit no is already present for the selected employee."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_workpermit_tran", 2, "Sorry, work permit no is already present for the selected employee."), Me)
                    End If
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'S.SANDEEP [20-JUN-2018] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With


        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            If IsNothing(dsCombos) = False Then
                dsCombos.Clear()
                dsCombos = Nothing
            End If
            objEmployee = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEWPermit As New clsemployee_workpermit_tran
        Dim objAPermit As New clsPermit_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            Dim blnFlag As Boolean = False

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            'If Validation() = False Then
            If Validation(objEWPermit) = False Then
                'Gajanan [4-Sep-2020] -- End
                Exit Sub
            End If

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            'Call SetValue()
            Call SetValue(objEWPermit, objAPermit)
            'Gajanan [4-Sep-2020] -- End

            If CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                'S.SANDEEP |17-JAN-2019| -- START
                'blnFlag = objEWPermit.Update()
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objEWPermit.Update(Nothing)
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If

                    'Gajanan [9-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                    blnFlag = objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing)
                    'Gajanan [9-July-2019] -- End
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                'blnFlag = objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing)
                'Gajanan [9-July-2019] -- End

                If blnFlag = False AndAlso objAPermit._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAPermit._Message, Me)
                    Exit Sub
                End If

            Else
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'blnFlag = objEWPermit.Insert()
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objEWPermit.Insert()
                Else
                    'S.SANDEEP |17-JAN-2019| -- START
                    'ENHANCEMENT:Movement Approval Flow Edit / Delete


                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.DELETED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, objAPermit._Effectivedate, "", objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, objAPermit._Work_Permit_No, objAPermit._Employeeunkid, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    blnFlag = objAPermit.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED, Nothing)
                    If blnFlag = False AndAlso objAPermit._Message <> "" Then
                        DisplayMessage.DisplayMessage(objAPermit._Message, Me)
                        Exit Sub
                    End If
                End If
                'Gajanan [12-NOV-2018] -- End

            End If

            If blnFlag = False AndAlso objEWPermit._Message <> "" Then
                DisplayMessage.DisplayMessage(objEWPermit._Message, Me)
            Else
                Call Fill_Grid()

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                'Call Set_WorkPermit()
                Call Set_WorkPermit(objEWPermit)
                'Gajanan [4-Sep-2020] -- End

                'Gajanan [15-NOV-2018] -- START
                'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    'Gajanan [15-NOV-2018] -- END
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    Dim intPrivilegeId As Integer = 0
                    Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                    If mblnIsResidentPermit Then
                        intPrivilegeId = 1200
                        eMovement = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                    Else
                        intPrivilegeId = 1199
                        eMovement = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                    End If

                    'S.SANDEEP |17-JAN-2019| -- START
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If Me.ViewState("Workpermittranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END 
                    objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                 CInt(Session("Fin_year")), intPrivilegeId, eMovement, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), _
                                                 mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), eOperType, mblnIsResidentPermit, 0, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    objPMovement = Nothing
                End If

            End If
            Me.ViewState("Workpermittranunkid") = Nothing
            ClearControls()
            'S.SANDEEP [07-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001988}
            cboEmployee.Enabled = True
            'S.SANDEEP [07-Feb-2018] -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [15-NOV-2018] -- START
            'S.SANDEEP |17-JAN-2019| -- START
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- END
            'Gajanan [15-NOV-2018] -- END


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEWPermit = Nothing
            objAPermit = Nothing
            'Gajanan [4-Sep-2020] -- End

        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEWPermit As New clsemployee_workpermit_tran
        Dim objAPermit As New clsPermit_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try

            Blank_ModuleName()
            objEWPermit._WebFormName = mstrModuleName
            objEWPermit._WebClientIP = CStr(Session("IP_ADD"))
            objEWPermit._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuGeneralMaster"
            StrModuleName3 = "mnuCoreSetups"

            objEWPermit._Isvoid = True
            objEWPermit._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEWPermit._Voidreason = popup_DeleteReason.Reason
            objEWPermit._Voiduserunkid = CInt(Session("UserId"))
            objEWPermit._Userunkid = CInt(Session("UserId"))
            'Hemant (23 Apr 2020) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                objAPermit._Isvoid = True
                objAPermit._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objAPermit._Voidreason = popup_DeleteReason.Reason
                objAPermit._Voiduserunkid = -1
                objAPermit._Audituserunkid = CInt(Session("UserId"))
                objAPermit._Isweb = True
                objAPermit._Ip = Session("IP_ADD").ToString()
                objAPermit._Hostname = Session("HOST_NAME").ToString()
                objAPermit._Form_Name = mstrModuleName

                If objAPermit.Delete(CInt(Me.ViewState("Workpermittranunkid")), clsEmployeeMovmentApproval.enOperationType.DELETED, CInt(Session("CompanyUnkId")), Nothing) = False Then
                    If objAPermit._Message <> "" Then
                        DisplayMessage.DisplayMessage(objAPermit._Message, Me)
                    End If
                    Exit Sub
                    popup_DeleteReason.Hide()
                End If
            Else
                'Hemant (23 Apr 2020) -- End
                With objEWPermit
                    ._WebFormName = mstrModuleName
                    ._WebClientIP = Session("IP_ADD").ToString()
                    ._WebHostName = Session("HOST_NAME").ToString()
                End With
                If objEWPermit.Delete(CInt(Me.ViewState("Workpermittranunkid"))) = False Then
                    If objEWPermit._Message <> "" Then
                        DisplayMessage.DisplayMessage(objEWPermit._Message, Me)
                    End If
                    Exit Sub
                End If 'Hemant (23 Apr 2020)

            End If
            Call ClearControls()
            Call Fill_Grid()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popup_DeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region "ComboBox'S Events"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEWPermit As New clsemployee_workpermit_tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call Fill_Grid()
                Call ClearControls()
                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Call Set_WorkPermit(objEWPermit)
                'Gajanan [4-Sep-2020] -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEWPermit = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

#End Region

#Region "DataGrid's Events"

    Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEWPermit As New clsemployee_workpermit_tran
        Dim objAPermit As New clsPermit_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If dgvHistory.Items.Count <= -1 Then Exit Sub

            mdtAppointmentDate = Nothing
            txtAppointDate.Text = ""
            lblAppointDate.Visible = False
            txtAppointDate.Visible = False
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            pnlAppointDate.Visible = False
            'Gajanan [17-Sep-2020] -- End

            Me.ViewState("Workpermittranunkid") = 0
            Me.ViewState.Add("Workpermittranunkid", CInt(e.Item.Cells(10).Text))
            If e.CommandName.ToUpper = "EDIT" Then
                objEWPermit._Workpermittranunkid = CInt(Me.ViewState("Workpermittranunkid"))

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, "", objAPermit._Employeeunkid, "", Nothing, True, CInt(ViewState("Workpermittranunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                'Call SetEditValue()
                Call SetEditValue(objEWPermit)
                'Gajanan [4-Sep-2020] -- End

                mdtAppointmentDate = Nothing
                If CBool(e.Item.Cells(11).Text) = True Then
                    mdtAppointmentDate = eZeeDate.convertDate(e.Item.Cells(12).Text)
                    txtAppointDate.Text = mdtAppointmentDate.Date.ToShortDateString
                    lblAppointDate.Visible = True
                    txtAppointDate.Visible = True
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    pnlAppointDate.Visible = True
                    'Gajanan [17-Sep-2020] -- End
                End If
                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                cboEmployee.Enabled = False
                'S.SANDEEP [07-Feb-2018] -- END


            ElseIf e.CommandName.ToUpper = "DELETE" Then
                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objAPermit.isExist(mblnIsResidentPermit, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, "", objAPermit._Employeeunkid, "", Nothing, True, CInt(ViewState("Workpermittranunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()


                'S.SANDEEP |17-JAN-2019| -- START
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    Dim dr As DataRow() = mdtPermitList.Select(CType(Me.dgvHistory.Columns(10), BoundColumn).DataField & " = " & CInt(e.Item.Cells(10).Text) & " AND " & CType(Me.dgvHistory.Columns(14), BoundColumn).DataField & "= ''")
                    If dr.Length > 0 Then
                        Dim index As Integer = mdtPermitList.Rows.IndexOf(dr(0))
                        dgvHistory.SelectedIndex = index
                        dgvHistory.SelectedItemStyle.BackColor = Drawing.Color.LightCoral
                        dgvHistory.SelectedItemStyle.ForeColor = Drawing.Color.White
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

            ElseIf e.CommandName.ToUpper = "VIEWREPORT" Then
                Dim intMovementId As Integer = 0
                Dim intPrivilegeId As Integer = 0
                If mblnIsResidentPermit Then
                    intMovementId = clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT
                    intPrivilegeId = 1200
                Else
                    intMovementId = clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                    intPrivilegeId = 1199
                End If

                Popup_Viewreport._UserId = CInt(Session("UserId"))
                Popup_Viewreport._Priority = 0
                Popup_Viewreport._PrivilegeId = intPrivilegeId
                Popup_Viewreport._FillType = CType(intMovementId, clsEmployeeMovmentApproval.enMovementType)
                Popup_Viewreport._DtType = Nothing
                Popup_Viewreport._IsResidentPermit = mblnIsResidentPermit
                Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)

                Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                If Me.ViewState("Workpermittranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Workpermittranunkid")) > 0 Then
                    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                Popup_Viewreport._OprationType = eOperType
                Popup_Viewreport.Show()
                'Gajanan [12-NOV-2018] -- End

                Popup_Viewreport.Show()
                'Gajanan [12-NOV-2018] -- START
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvHistory.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If e.Item.Cells(11).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(11).Text) = True Then
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                End If
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(2).Text.Trim <> "" Then
                '    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString(Session("DateFormat").ToString)
                'End If
                'If e.Item.Cells(6).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Trim <> "" Then
                '    e.Item.Cells(6).Text = CDate(e.Item.Cells(6).Text).ToString(Session("DateFormat").ToString)
                'End If
                'If e.Item.Cells(7).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(7).Text.Trim <> "" Then
                '    e.Item.Cells(7).Text = CDate(e.Item.Cells(7).Text).ToString(Session("DateFormat").ToString)
                'End If
                If e.Item.Cells(3).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(3).Text.Trim <> "" Then
                    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToShortDateString
                End If
                If e.Item.Cells(7).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(7).Text.Trim <> "" Then
                    e.Item.Cells(7).Text = CDate(e.Item.Cells(7).Text).ToShortDateString
                End If
                If e.Item.Cells(8).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(8).Text.Trim <> "" Then
                    e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).ToShortDateString
                End If
                'Pinkal (16-Apr-2016) -- End


                If e.Item.Cells(14).Text.Trim <> "&nbsp;" AndAlso CStr(e.Item.Cells(14).Text).Trim.Length > 0 Then
                    e.Item.BackColor = Drawing.Color.PowderBlue
                    e.Item.ForeColor = System.Drawing.Color.Black
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = True
                    lblPendingData.Visible = True

                    'S.SANDEEP |17-JAN-2019| -- START
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).ToolTip = e.Item.Cells(15).Text
                    If CInt(e.Item.Cells(16).Text) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        'Hemant (23 Apr 2020) -- Start
                        'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                        'CType(e.Item.Cells(0).FindControl("imgDetail"), LinkButton).Visible = True
                        CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = True
                        'Hemant (23 Apr 2020) -- End
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                Else
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False

                    If e.Item.Cells(11).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(11).Text) = True Then
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END


                'Gajanan (28-May-2018) -- Start
                'Issue - Rehire Employee Data Also Edit And Delete.
                If Convert.ToInt32(e.Item.Cells(13).Text) > 0 Then
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False
                    e.Item.BackColor = System.Drawing.Color.Orange
                    objlblCaption.Visible = True
                End If
                'Gajanan (28-May-2018) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Language"

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbWPInformation", Me.lblDetialHeader.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.lblEffectiveDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblPlaceOfIssue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPlaceOfIssue.ID, Me.lblPlaceOfIssue.Text)
            Me.lblExpiryDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpiryDate.ID, Me.lblExpiryDate.Text)
            Me.lblIssueDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIssueDate.ID, Me.lblIssueDate.Text)
            Me.lblIssueCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIssueCountry.ID, Me.lblIssueCountry.Text)
            Me.lblWorkPermitNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblWorkPermitNo.ID, Me.lblWorkPermitNo.Text)
            Me.lblReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReason.ID, Me.lblReason.Text)
            dgvHistory.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(3).FooterText, dgvHistory.Columns(3).HeaderText)
            dgvHistory.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(4).FooterText, dgvHistory.Columns(4).HeaderText)
            dgvHistory.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(5).FooterText, dgvHistory.Columns(5).HeaderText)
            dgvHistory.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(6).FooterText, dgvHistory.Columns(6).HeaderText)
            dgvHistory.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(7).FooterText, dgvHistory.Columns(7).HeaderText)
            dgvHistory.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(8).FooterText, dgvHistory.Columns(8).HeaderText)
            dgvHistory.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(9).FooterText, dgvHistory.Columns(9).HeaderText)
            dgvHistory.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(10).FooterText, dgvHistory.Columns(10).HeaderText)
            dgvHistory.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(11).FooterText, dgvHistory.Columns(11).HeaderText)
            dgvHistory.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvHistory.Columns(12).FooterText, dgvHistory.Columns(12).HeaderText)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
