<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="true" CodeFile="wPgEmployeeProfile.aspx.vb"
    Inherits="wPgEmployeeProfile" Title="My Profile" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

$(document).ready(function () {
    BindControls();
});

function BindControls() {
    debugger;
var img = document.getElementById('<%= imgEmp.ClientID %>' + '_imgZoom');
    if (img != null)
        document.onmousemove = getMouseXY;
                   var cnt = $('.flupload').length;
                for (i = 0; i < cnt; i++) {
                    var fupld = $('.flupload')[i].id;
                    if (fupld != null)
                //S.SANDEEP |27-MAY-2022| -- START
                //ISSUE/ENHANCEMENT : IMAGE EVENT BINDING ISSUE
                //fileUpLoadChange($('.flupload')[i].id);                  
                fileUpLoadChange('#' + $('.flupload')[i].id);
                //S.SANDEEP |27-MAY-2022| -- END
                }

}
var req = Sys.WebForms.PageRequestManager.getInstance();
    req.add_endRequest(function () {
        BindControls();
}); 

    </script>

    <%--<script type="text/javascript">
function onlyNumbers(evt) 
    {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        var cHeight = document.getElementById('<%=txtHeight.ClientID%>').value;
        if (cHeight.length > 0)
            if (charCode == 46)
                if (cHeight.indexOf(".") > 0)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }
    
   function onlyNum(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        var cWeight = document.getElementById('<%=txtWeight.ClientID%>').value;
        if (cWeight.length > 0)
            if (charCode == 46)
                if (cWeight.indexOf(".") > 0)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }    
</script>--%>
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblEPhoto" runat="server" Text="Photo"></asp:Label>
                                    </h2>
                                </div>
                                <div class="body text-center">
                                    <uc8:ZoomImage ID="imgEmp" runat="server" ZoomPercentage="250" />
                                    <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc9:FileUpload ID="flUpload" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:LinkButton ID="btnRemoveImage" runat="server" ToolTip="Remove Image" CssClass="btn btn-danger btn-circle waves-effect waves-circle waves-float">
                                        <i class="fas fa-trash"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblESign" runat="server" Text="Signature"></asp:Label>
                                    </h2>
                                </div>
                                <div class="body text-center">
                                    <uc8:ZoomImage ID="imgSignature" runat="server" ZoomPercentage="250" />
                                    <asp:UpdatePanel ID="UPUploadSig" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc9:FileUpload ID="flUploadsig" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:LinkButton ID="btnRemoveSig" runat="server" ToolTip="Remove signature" CssClass="btn btn-danger btn-circle waves-effect waves-circle waves-float">
                                        <i class="fas fa-trash"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblpnl_header" runat="server" Text="Employee Detail" />
                                </h2>
                                <div class="btn-group user-helper-dropdown header-dropdown m-r--5">
                                    <i class="fas fa-ellipsis-v" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    </i>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <asp:LinkButton ID="lnkViewAllocations" runat="server" Text="" CssClass="waves-effect waves-block">
                                                <i class="fas fa-users"></i>
                                                <asp:Label ID="lbllnkViewAllocations" runat="server" Text="Allocations"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewAssessor" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-user-tie"></i>
                                                <asp:Label ID="lbllnkViewAssessor" runat="server" Text="View Assessor/Reviewer"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewDates" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-at"></i>
                                                <asp:Label ID="lbllnkViewDates" runat="server" Text="Dates"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewAddress" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-home"></i>
                                                <asp:Label ID="lbllnkViewAddress" runat="server" Text="Addresses"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewEmergency" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-address-book"></i>
                                                <asp:Label ID="lbllnkViewEmergency" runat="server" Text="Emergency Contacts"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewPersonal" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-id-badge"></i>
                                                <asp:Label ID="lbllnkViewPersonal" runat="server" Text="Personal"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewLeave" runat="server" Visible="False" CssClass="waves-effect waves-block">
                                                <i class="fas fa-calendar-check"></i>
                                                <asp:Label ID="lbllnkViewLeave" runat="server" Text="Leave Records"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewLeaveApprover" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-calendar-check"></i>
                                                <asp:Label ID="Label1" runat="server" Text="View Leave Approver"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewReportingTo" runat="server" Text="View Reporting To" CssClass="waves-effect waves-block">
                                                <i class="fas fa-people-arrows"></i>
                                                <asp:Label ID="lbllnkViewReportingTo" runat="server" Text="View Reporting To"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewClaimApprover" runat="server" Text="View Claim Request Approver"
                                                CssClass="waves-effect waves-block">
                                                <i class="fas fa-user-tag"></i>
                                                <asp:Label ID="lbllnkViewClaimApprover" runat="server" Text="View Claim Request Approver"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkViewOTApprover" runat="server" Text="View OT Requisition Approver"
                                                CssClass="waves-effect waves-block">
                                                <i class="fas fa-user-clock"></i>
                                                <asp:Label ID="lbllnkViewOTApprover" runat="server" Text="View OT Requisition Approver"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <asp:LinkButton ID="lnkNominateEmp" runat="server" CssClass="waves-effect waves-block">
                                                <i class="fas fa-user-tag"></i>
                                                <asp:Label ID="lbllnkNominateEmp" runat="server" Text="Nominate as a Successor"></asp:Label>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlCombo" runat="server">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowPending" runat="server" AutoPostBack="true" Text="Display Pending Employee(s)" />
                                        </div>
                                        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeeCode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCodeValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboTitle" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFirstName" runat="server" Text="First Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtFirstValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSurname" runat="server" Text="Surname" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSurnameValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOthername" runat="server" Text="Other Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOtherNameValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtGenderValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmploymentType" runat="server" Text="Employment Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmploymentTypeValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPaypoint" runat="server" Text="Pay Point" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPayPointValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdNo" runat="server" Text="Identity No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIdValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblShift" runat="server" Text="Shift" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtShiftValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPayType" runat="server" Text="Pay Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPayValue" runat="server" ReadOnly="true" CssClass="form-control"
                                                    Text=""></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmail" runat="server" Text="Company Email" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtcoEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Update" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupEmpReporting" runat="server" CancelControlID="btnEmpReportingClose"
                    PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlEmpReporting"
                    BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="modal-dialog card" Style="display: none;"
                    DefaultButton="btnEmpReportingClose">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblReportingTo" runat="server" Text="" />
                        </h2>
                    </div>
                    <div class="body" style="height: 400px">
                        <div class="table-responsive">
                            <asp:GridView ID="GvReportingTo" runat="server" CssClass="table table-hover table-bordered"
                                AllowPaging="false">
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupNominateEmployee" runat="server" CancelControlID="hfNominateEmployee"
                    PopupControlID="pnlNominateEmployee" TargetControlID="lblNominateEmployeeHeader"
                    Drag="true" PopupDragHandleControlID="pnlNominateEmployee" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlNominateEmployee" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblNominateEmployeeHeader" runat="server" Text="Nominate Employee" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="inner-card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNominateEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtNominateEmployee" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNominateEmployeeJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpNominateEmployeeJob" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNominateEmployeeSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="inner-card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 250px">
                                            <asp:GridView ID="dgvNominateEmployee" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="nominationunkid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkNominateEmployeeDelete" runat="server" ToolTip="Delete" OnClick="lnkNominateEmployeeDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="job_name" HeaderText="Job Name" ReadOnly="True" FooterText="colhYear">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnNominateEmployeeClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfNominateEmployee" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupNominationCancleReason" runat="server" CancelControlID="btnNominationCancleReasonClose"
                    PopupControlID="pnlNominationCancleReason" TargetControlID="hfNominationCancleReason"
                    Drag="true" PopupDragHandleControlID="pnlNominationCancleReason" BackgroundCssClass="modal-backdrop bd-l2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlNominationCancleReason" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label2" runat="server" Text="Succession Qualifying Criteria Info." />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="list-group">
                                    <li class="list-group-item">Is Minimum Qualification Level Match?
                                        <asp:Label ID="lblQualificationNominationCheckTrue" Visible="false" runat="server"
                                            Text="" CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblQualificationNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" />
                                    </li>
                                    <li class="list-group-item">Is Minimum Number of Years with Organization Match?
                                        <asp:Label ID="lblExpNominationCheckTrue" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblExpNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                    <li class="list-group-item">Is Minimum Performance Score Match?
                                        <asp:Label ID="lblPerfNominationCheckTrue" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblPerfNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnNominationCancleReasonClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfNominationCancleReason" runat="server" />
                    </div>
                </asp:Panel>
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
