<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AssetsRegisterList.aspx.vb"
    Inherits="wPg_AssetsRegisterList" Title="Company Assets List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Company Assets List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPending" runat="server" CssClass="filled-in" Text="Show Pending Employee"
                                            AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblAsset" runat="server" Text="Asset" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpAsset" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpdate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCodition" runat="server" Text="Condition" CssClass="form-label"></asp:Label></label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpCondition" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="drpStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAssetNo" runat="server" Text="Asset No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAssetno" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnAssign" runat="server" Text="Assign" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                        AllowPaging="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Returned" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="mnuReturned">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkReturned" runat="server" CommandName="Returned" ToolTip="Returned">
                                                    <i class="fas fa-undo-alt text-primary"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Written Off" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="mnuWrittenOff">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkWritten" runat="server" CommandName="Writtenoff" ToolTip="Written Off"
                                                        Font-Underline="false">
                                                        <i class="fas fa-clipboard-list text-primary"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Lost" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="mnuLost">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkLost" runat="server" CommandName="Lost" ToolTip="Lost" Font-Underline="false">
                                                    <i class="fas fa-user-minus text-primary"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                        <i class="fas fa-trash text-danger"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployeeName" />
                                            <asp:BoundColumn DataField="Asset" HeaderText="Asset Name" ReadOnly="True" FooterText="colhAssetName" />
                                            <asp:BoundColumn DataField="AssetNo" HeaderText="Asset No." ReadOnly="True" FooterText="colhAssestNo" />
                                            <asp:BoundColumn DataField="assetserial_no" HeaderText="Serial No." ReadOnly="True"
                                                FooterText="" />
                                            <asp:BoundColumn DataField="Date" HeaderText="Date" ReadOnly="True" FooterText="colhDate" />
                                            <asp:BoundColumn DataField="Status" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                            <asp:BoundColumn DataField="Condtion" HeaderText="Condition" ReadOnly="True" FooterText="colhCondition" />
                                            <asp:BoundColumn DataField="Remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark" />
                                            <asp:BoundColumn DataField="asset_statusunkid" HeaderText="Statusunkid" ReadOnly="True"
                                                Visible="false" />
                                        </Columns>
                                        <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                    </asp:DataGrid>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete ?" />
                <cc1:ModalPopupExtender ID="popupStatus" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnStatusClose" PopupControlID="pnlStatus" TargetControlID="txtStatusAsset" />
                <asp:Panel ID="pnlStatus" runat="server" CssClass="card modal-dialog" Style="display: none">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Employee Asset Status"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblStatusAsset" runat="server" Text="Asset" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtStatusAsset" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblStatusEmp" runat="server" Text="Employee" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtStatusEmp" runat="server" ReadOnly="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAssetStatus" runat="server" Text="Status" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtAssetStatus" runat="server" ReadOnly="true" CssClass="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblStatusRemarks" runat="server" Text="Remark" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtStatusRemark" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnStatusSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnStatusClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
