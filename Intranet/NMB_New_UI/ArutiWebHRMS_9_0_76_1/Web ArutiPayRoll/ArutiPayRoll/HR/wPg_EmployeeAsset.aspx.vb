﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports Newtonsoft.Json

#End Region

Partial Class wPg_EmployeeAsset
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objAsset As clsEmployee_Assets_Tran
    Private mintEmpAssetTranUnkid As Integer = -1
    Private mintSelectedEmployee As Integer = -1


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeAssets_AddEdit"
    'Pinkal (06-May-2014) -- End

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Dim xPostedData As String = ""
    Dim xResponseData As String = ""
    Dim mdtCompanyAsset As DataTable = Nothing
    'Pinkal (23-Dec-2023) -- End


#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objAsset = New clsEmployee_Assets_Tran

            Blank_ModuleName()
            objAsset._WebFormName = "frmEmployeeAssets_AddEdit"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            objAsset._WebClientIP = CStr(Session("IP_ADD"))
            objAsset._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objAsset._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            SetLanguage()

            Dim clsuser As New User
            clsuser = CType(Session("clsuser"), User)
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                Me.ViewState("Empunkid") = Session("UserId")
                btnSaveInfo.Visible = CBool(Session("EditEmployeeAssets"))
            Else
                Me.ViewState("Empunkid") = Session("Employeeunkid")
            End If

            If (Page.IsPostBack = False) Then
                FillCombo()
                dtpdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If Session("Asset_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(Session("Asset_EmpUnkID"))
                End If
                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                SetVisibility()
                'Pinkal (23-Dec-2023) -- End
            Else
                'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                xPostedData = Me.ViewState("PostedData").ToString()
                xResponseData = Me.ViewState("ResponseData").ToString()
                mdtCompanyAsset = CType(Me.ViewState("CompanyAsset"), DataTable)
                'Pinkal (23-Dec-2023) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleDeleteButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleNewButton(False)
    '    ToolbarEntry1.VisibleModeFormButton(False)
    '    ToolbarEntry1.VisibleExitButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PostedData") = xPostedData
            Me.ViewState("ResponseData") = xResponseData
            Me.ViewState("CompanyAsset") = mdtCompanyAsset
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Dec-2023) -- End

#End Region

#Region " Private Methods "

    Private Sub GetValue()
        Try
            txtAssetno.Text = objAsset._Asset_No
            TxtRemark.Text = objAsset._Remark
            drpAsset.SelectedValue = CStr(objAsset._Assetunkid)
            drpCondition.SelectedValue = CStr(objAsset._Conditionunkid)
            drpEmployee.SelectedValue = CStr(objAsset._Employeeunkid)
            If objAsset._Assign_Return_Date <> Nothing Then
                dtpdate.SetDate = objAsset._Assign_Return_Date.Date
            End If
            TxtSerialno.Text = objAsset._Assetserial_No

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAsset._Asset_No = txtAssetno.Text
            objAsset._Remark = TxtRemark.Text
            objAsset._Assetunkid = CInt(drpAsset.SelectedValue)
            objAsset._Conditionunkid = CInt(drpCondition.SelectedValue)
            objAsset._Employeeunkid = CInt(drpEmployee.SelectedValue)

            ' If menAction = enAction.EDIT_ONE Then
            'objAsset._Statusunkid = objAsset._Statusunkid
            'Else
            objAsset._Statusunkid = enEmpAssetStatus.Assigned
            ' End If



            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            'objAsset._Assign_Return_Date = dtpdate.GetDate.Date

            If dtpdate.IsNull = True Then
                objAsset._Assign_Return_Date = Nothing
            Else
                objAsset._Assign_Return_Date = dtpdate.GetDate.Date
            End If

            'Pinkal (12-Nov-2012) -- End


            If mintEmpAssetTranUnkid = -1 Then
                objAsset._Isvoid = False
                objAsset._Voiddatetime = Nothing
                objAsset._Voidreason = ""
                objAsset._Voiduserunkid = -1
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objAsset._Userunkid = 1
                objAsset._Userunkid = CInt(Session("UserId"))
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            Else
                objAsset._Isvoid = objAsset._Isvoid
                objAsset._Voiddatetime = objAsset._Voiddatetime
                objAsset._Voidreason = objAsset._Voidreason
                objAsset._Voiduserunkid = objAsset._Voiduserunkid
                objAsset._Userunkid = objAsset._Userunkid
            End If
            objAsset._Assetserial_No = TxtSerialno.Text
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "Condition")
            With drpCondition
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Condition")
                .DataBind()
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "Assets")
            With drpAsset
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Assets")
                .DataBind()
            End With

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                Dim objEmployee As New clsEmployee_Master

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsList = objEmp.GetEmployeeList("Employee", True, )
                '    dsList = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END                    
                'End If

                'Shani(24-Aug-2015) -- End
                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With

            Else

                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCommon = Nothing
            objEmp = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.

            If Session("CompanyAssetP2PServiceURL").ToString().Trim.Length <= 0 Then

                If CInt(drpEmployee.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue."), Me)
                    drpEmployee.Focus()
                    Return False
                End If

                If CInt(drpAsset.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Asset is compulsory information. Please select Asset to continue."), Me)
                    drpAsset.Focus()
                    Return False
                End If

                If txtAssetno.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Asset No. cannot be blank. Asset No. is compulsory information."), Me)
                    txtAssetno.Focus()
                    Return False
                End If

                If TxtSerialno.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Serial No. cannot be blank. Serial No. is compulsory information."), Me)
                    TxtSerialno.Focus()
                    Return False
                End If


                If CInt(drpCondition.SelectedValue) <= 0 AndAlso Session("CompanyAssetP2PServiceURL").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Asset Condition is compulsory information. Please select Asset Condition to continue."), Me)
                    drpCondition.Focus()
                    Return False
                End If

            End If

            'Pinkal (23-Dec-2023) -- End

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Sub ClearObject()
        Try
            drpAsset.SelectedIndex = 0
            drpCondition.SelectedIndex = 0
            txtAssetno.Text = ""
            TxtRemark.Text = ""
            TxtSerialno.Text = ""
            dtpdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            xPostedData = ""
            xResponseData = ""
            mintEmpAssetTranUnkid = -1
            mdtCompanyAsset.Rows.Clear()
            'Pinkal (23-Dec-2023) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.

    Private Sub SetVisibility()
        Try
            If Session("CompanyAssetP2PServiceURL") IsNot Nothing AndAlso Session("CompanyAssetP2PServiceURL").ToString().Trim.Length > 0 Then
                drpAsset.Enabled = False
                dtpdate.Enabled = False
                txtAssetno.Enabled = False
                TxtSerialno.Enabled = False
                drpCondition.Enabled = False
                TxtRemark.Enabled = False
                btnP2PRequest.Visible = True
            Else
                btnP2PRequest.Visible = False
            End If
            'Pinkal (23-Dec-2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Protected Sub btnSaveInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Try

            If Validation() Then

                If Session("CompanyAssetP2PServiceURL") Is Nothing OrElse Session("CompanyAssetP2PServiceURL").ToString().Trim.Length <= 0 Then

                    SetValue()

                    If objAsset.Insert(CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & objAsset._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry Saved Sucessfully." & objAsset._Message, Me)
                        ClearObject()
                    End If

                ElseIf Session("CompanyAssetP2PServiceURL") IsNot Nothing AndAlso Session("CompanyAssetP2PServiceURL").ToString().Trim.Length > 0 Then

                    If mdtCompanyAsset IsNot Nothing AndAlso mdtCompanyAsset.Rows.Count > 0 Then

                        Dim mblnFlag As Boolean = False
                        Dim objCommon As New clsCommon_Master

                        For i As Integer = 0 To mdtCompanyAsset.Rows.Count - 1

                            Dim mintmasterunkid As Integer = 0

                            If objCommon.isExist(clsCommon_Master.enCommonMaster.ASSETS, "", mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim()) = False Then
                                objCommon._Code = mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim()
                                objCommon._Name = mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim()
                                objCommon._Name1 = mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim()
                                objCommon._Name2 = mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim()
                                objCommon._Mastertype = CInt(clsCommon_Master.enCommonMaster.ASSETS)
                                objCommon._Isactive = True
                                objCommon._Userunkid = CInt(Session("UserId"))

                                If objCommon.Insert() = False Then
                                    eZeeMsgBox.Show(objCommon._Message, enMsgBoxStyle.Information)
                                    Exit For
                                Else
                                    mintmasterunkid = objCommon._Masterunkid
                                End If
                            Else
                                Dim dsList As DataSet = objCommon.GetList(clsCommon_Master.enCommonMaster.ASSETS, "List", -1, True, "AND cfcommon_master.name ='" & mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim() & "'")

                                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                    mintmasterunkid = CInt(dsList.Tables(0).Rows(0)("masterunkid"))
                                End If

                            End If  ' If objCommon.isExist(clsCommon_Master.enCommonMaster.ASSETS, "", mdtCompanyAsset.Rows(i)("AssetName").ToString().Trim()) = False Then

                            objAsset._Assetunkid = mintmasterunkid   'Asset
                            objAsset._Employeeunkid = CInt(drpEmployee.SelectedValue)
                            objAsset._Assign_Return_Date = Now.Date
                            objAsset._Asset_No = mdtCompanyAsset.Rows(i)("AssetNo").ToString()
                            objAsset._Assetserial_No = mdtCompanyAsset.Rows(i)("AssetGroup").ToString()
                            'objAsset._Asset_No = mdtCompanyAsset.Rows(i)("AssetGroup").ToString()
                            'objAsset._Assetserial_No = mdtCompanyAsset.Rows(i)("AssetNo").ToString()
                            objAsset._Remark = mdtCompanyAsset.Rows(i)("AssetLocation").ToString()
                            objAsset._Conditionunkid = 0
                            objAsset._Statusunkid = enEmpAssetStatus.Assigned
                            objAsset._PostedData = xPostedData
                            objAsset._ResponseData = xResponseData

                            objAsset._Isvoid = False
                            objAsset._Voiddatetime = Nothing
                            objAsset._Voidreason = ""
                            objAsset._Voiduserunkid = -1
                            objAsset._Userunkid = CInt(Session("UserId"))

                            If objAsset.isExist(drpEmployee.SelectedValue.ToString(), mintmasterunkid, mdtCompanyAsset.Rows(i)("AssetNo").ToString(), Session("CompanyAssetP2PServiceURL").ToString()) Then
                                mblnFlag = objAsset.Update(CInt(Session("CompanyUnkId")))
                            Else
                                mblnFlag = objAsset.Insert(CInt(Session("CompanyUnkId")))
                            End If

                            If mblnFlag = False And objAsset._Message <> "" Then
                                DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & objAsset._Message, Me)
                                Exit For
                            Else
                                DisplayMessage.DisplayMessage("Entry Saved Sucessfully." & objAsset._Message, Me)
                            End If

                        Next
                        ClearObject()
                        objCommon = Nothing

                    End If  'If mdtCompanyAsset IsNot Nothing AndAlso mdtCompanyAsset.Rows.Count > 0 Then

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "There is no response from P2P.Please try to request again for P2P request."), Me)
                    Exit Sub
                End If  ' ElseIf Session("CompanyAssetP2PServiceURL") IsNot Nothing AndAlso Session("CompanyAssetP2PServiceURL").ToString().Trim.Length > 0 Then

            End If  '  If Validation() Then

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (23-Dec-2023) -- End

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect(Session("rootpath").ToString & "HR/wPg_AssetsRegisterList.aspx", False)
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Protected Sub btnP2PRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnP2PRequest.Click
        Dim exForce As Exception = Nothing
        Try
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue."), Me)
                drpEmployee.Focus()
                Exit Sub
            End If

            If Session("CompanyAssetP2PServiceURL") IsNot Nothing AndAlso Session("CompanyAssetP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim objMasterData As New clsMasterData
                Dim objclsCompanyAssetP2PIntegration As New clsCompanyAssetP2PIntegration
                Dim mstrError As String = String.Empty
                Dim xHttpStatusCode As Integer = 0
                Dim mstrEmployeeCode As String = ""

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Nothing) = CInt(drpEmployee.SelectedValue)
                mstrEmployeeCode = objEmployee._Employeecode.ToString()
                objEmployee = Nothing

                Dim mstrToken As String = ""
                mstrToken = objMasterData.GetP2PToken(CInt(Session("CompanyUnkId")), Nothing)

                If mstrToken.Trim.Length > 0 Then

                    objclsCompanyAssetP2PIntegration.queryFixedAssetRequest.CompanyCode = Session("CompCode").ToString().Trim
                    objclsCompanyAssetP2PIntegration.queryFixedAssetRequest.EmployeeCode = mstrEmployeeCode.Trim

                    If objMasterData.GetSetP2PWebRequest(Session("CompanyAssetP2PServiceURL").ToString().Trim, True, True, "CompanyAssest", xResponseData, mstrError, objclsCompanyAssetP2PIntegration, xPostedData, mstrToken.Trim, xHttpStatusCode) = False Then
                        exForce = New Exception("Company Assest P2P Error : " & mstrError)
                        Throw exForce
                    End If

                    objMasterData = Nothing
                    objclsCompanyAssetP2PIntegration = Nothing

                    'xResponseData = "{""$id"":""1"",""FixedAssetList"":[{""$id"":""2"",""CompanyCode"":""NMB"",""EmployeeCode"":""000001"",""EmployeeName"":""Loyiso Bambatha"",""AssetNo"":""1000"",""AssetName"":""CPU"",""AssetGroup"":""Computer H"",""AssetLocation"":""10001""},{""$id"":""3"",""CompanyCode"":""NMB"",""EmployeeCode"":""1016"",""EmployeeName"":""Amos Iroga Sange"",""AssetNo"":""1002"",""AssetName"":""CPU"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""4"",""CompanyCode"":""NMB"",""EmployeeCode"":""5486"",""EmployeeName"":""Twaha Ali Hussein"",""AssetNo"":""FA00000001"",""AssetName"":""CPU"",""AssetGroup"":""Computer H"",""AssetLocation"":""10001""},{""$id"":""5"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000002"",""AssetName"":""Computer Monitor"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""6"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000003"",""AssetName"":""Printer"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""7"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000004"",""AssetName"":""Printer"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""8"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000005"",""AssetName"":""CPU"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""9"",""CompanyCode"":""NMB"",""EmployeeCode"":""5486"",""EmployeeName"":""Twaha Ali Hussein"",""AssetNo"":""FA00000006"",""AssetName"":""Computer Monitor"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""10"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000007"",""AssetName"":""Printer"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""11"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000008"",""AssetName"":""POS"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""12"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000009"",""AssetName"":""Data cabinet"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""13"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000010"",""AssetName"":""CPU"",""AssetGroup"":""Computer H"",""AssetLocation"":""""},{""$id"":""14"",""CompanyCode"":""NMB"",""EmployeeCode"":""000052"",""EmployeeName"":""Twaha Ali Hussein"",""AssetNo"":""FA00000011"",""AssetName"":""Computer Monitor"",""AssetGroup"":""Computer H"",""AssetLocation"":""10001""},{""$id"":""15"",""CompanyCode"":""NMB"",""EmployeeCode"":"""",""EmployeeName"":"""",""AssetNo"":""FA00000012"",""AssetName"":""Computer Monitor"",""AssetGroup"":""Computer H"",""AssetLocation"":""""}],""CompanyCode"":""NMB"",""ResultErrors"":[]}"

                    mdtCompanyAsset = JsonStringToDataTable(xResponseData)

                    If mdtCompanyAsset IsNot Nothing AndAlso mdtCompanyAsset.Rows.Count > 0 Then
                        mdtCompanyAsset = New DataView(mdtCompanyAsset, "EmployeeCode = '" & mstrEmployeeCode & "'", "", DataViewRowState.CurrentRows).ToTable()
                    End If

                    xResponseData = JsonConvert.SerializeObject(mdtCompanyAsset, Formatting.Indented)

                Else
                    exForce = New Exception(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Failed to get P2P Token."))
                    Throw exForce
                    Exit Sub
                End If  'If mstrToken.Trim.Length > 0 Then

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (23-Dec-2023) -- End

#End Region

#Region "ToolBar Event"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\HR\wPg_AssetsRegisterList.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    Private Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblCondition.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCondition.ID, Me.lblCondition.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.lblAsset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAsset.ID, Me.lblAsset.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblAssetNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAssetNo.ID, Me.lblAssetNo.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblSerialNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSerialNo.ID, Me.lblSerialNo.Text)

            Me.btnSaveInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveInfo.ID, Me.btnSaveInfo.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

End Class
