﻿<%@ Page Title="Emergency Address" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmpEmerAddress.aspx.vb" Inherits="wPgEmpEmerAddress" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--S.SANDEEP |26-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%--S.SANDEEP |26-APR-2019| -- END--%>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <asp:Panel ID="pnlAddress" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Emergency Address"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <asp:Panel ID="pnl_Employee" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="GbEmergencyContact1" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnEmergencyContact1" runat="server" Text="Emergency Contact 1" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkScanAttachEmerContact1" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblEAddressapproval1" Visible="false" runat="server" Text="Pending Approval"
                                                    CssClass="pull-right label label-primary" ></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgFirstName" runat="server" Text="Firstname" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEFirstname" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgLastName" runat="server" Text="Lastname" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtELastname" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgAddress" runat="server" Text="Address" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEAddress" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostCountry" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboECountry" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgState" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEState" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostTown" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboECity" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostCode" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEZipcode" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgProvince" runat="server" Text="Prov/Region" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtERegion" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgRoad" runat="server" Text="Road/Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEStreet" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgEstate" runat="server" Text="Estate" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEEState" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPlotNo" runat="server" Text="Plot No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEPlotNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgMobile" runat="server" Text="Mobile" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEMobile" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgAltNo" runat="server" Text="Alt. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEAltNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgTelNo" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtETelNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgFax" runat="server" Text="Fax" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEFax" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgEmail" runat="server" Text="Email :" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEEMail" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="GbEmergencyContact2" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnEmergencyContact2" runat="server" Text="Emergency Contact 2" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkCopyEAddress2" runat="server" ToolTip="Copy Address">
                                                    <i class="fas fa-copy"></i>
                                                        </asp:LinkButton>
                                                    </li>

                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkScanAttachEmerContact2" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblEAddressapproval2" Visible="false" runat="server" Text="Pending Approval"
                                                    CssClass="pull-right label label-primary" ></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgFirstName2" runat="server" Text="Firstname" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEFirstname2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgLastName2" runat="server" Text="Lastname" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtELastname2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgAddress2" runat="server" Text="Address" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEAddress2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostCountry2" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboECountry2" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgState2" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEState2" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostTown2" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboECity2" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostCode2" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEZipcode2" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgProvince2" runat="server" Text="Prov/Region" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtERegion2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgRoad2" runat="server" Text="Road/Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEStreet2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgEstate2" runat="server" Text="Estate" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEEState2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPlotNo2" runat="server" Text="Plot No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEPlotNo2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgMobile2" runat="server" Text="Mobile" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEMobile2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgAltNo2" runat="server" Text="Alt. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEAltNo2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgTelNo2" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtETelNo2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgFax2" runat="server" Text="Fax" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEFax2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgEmail2" runat="server" Text="Email" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEEMail2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="GbEmergencyContact3" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnEmergencyContact3" runat="server" Text="Emergency Contact 3" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkCopyEAddress3" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-copy"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkScanAttachEmerContact3" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblEAddressapproval3" Visible="false" runat="server" Text="Pending Approval"
                                                    CssClass="pull-right label label-primary" ></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgFirstName3" runat="server" Text="Firstname" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEFirstname3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgLastName3" runat="server" Text="Lastname" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtELastname3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgAddress3" runat="server" Text="Address" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEAddress3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostCountry3" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboECountry3" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgState3" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEState3" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostTown3" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboECity3" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPostCode3" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEZipcode3" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgProvince3" runat="server" Text="Prov/Region" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtERegion3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgRoad3" runat="server" Text="Road/Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEStreet3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgEstate3" runat="server" Text="EState" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEEState3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgPlotNo3" runat="server" Text="Plot No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEPlotNo3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgMobile3" runat="server" Text="Mobile" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEMobile3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgAltNo3" runat="server" Text="Alt. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEAltNo3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgTelNo3" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtETelNo3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgFax3" runat="server" Text="Fax" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEFax3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmgEmail3" runat="server" Text="Email" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtEEMail3" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Update" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ScanAttachment">
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                        CancelControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="panel-primary" style="margin: 0px">
                        
                        
                            <div class="panel-heading">
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div36" class="panel-default">
                                    <div id="Div37" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                        Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                        <div id="fileuploader">
                                                            <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse"
                                                                onclick="return IsValidAttach();" />
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                        Text="Browse" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="3" style="width: 100%">
                                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--0--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--1--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <%--2--%>
                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                            <%--3--%>
                                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                            <%--4--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                            <div style="float: left">
                                                <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                            </div>
                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                            <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btnDefault" />
                                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                        IsFireButtonNoClick="false" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmpEmerAddress.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        //$('input[type=file]').live("click", function() {
        $("body").on("click", 'input[type=file]', function() {            
            return IsValidAttach();
        });
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
</asp:Content>
