﻿<%@ Page Title="Employee Address" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgEmployeeAddress.aspx.vb" Inherits="wPgEmployeeAddress" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%--S.SANDEEP |26-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%--S.SANDEEP |26-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Address"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <asp:Panel ID="pnl_Employee" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="GbPresentAddress" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnPresentAddress" runat="server" Text="Present Address" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkScanAttachPersonal" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblpresentaddressapproval" Visible="false" runat="server" Text="Pending Approval"
                                                    CssClass="pull-right label label-primary"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAddress" runat="server" Text="Address1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPAddress1" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAddress2" runat="server" Text="Address2" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPAddress2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPostCountry" runat="server" Text="Post Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPCountry" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPresentState" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPState" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPostTown" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPCity" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPostcode" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPZipcode" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblProvince" runat="server" Text="Prov/Region" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPRegion" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRoad" runat="server" Text="Road/Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPStreet" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEstate" runat="server" Text="EState" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPEState" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPersentProvince1" runat="server" Text="Prov/Region1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPresentProvRegion1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPresentRoadStreet1" runat="server" Text="Road/Street1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPresentRoadStreet1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPresentChiefdom" runat="server" Text="Chiefdom" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPresentChiefdom" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPresentVillage" runat="server" Text="Village" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPresentVillage" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPresentTown1" runat="server" Text="Town1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPresentTown1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMobile" runat="server" Text="Mobile" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPMobile" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTelNo" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPTelNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPloteNo" runat="server" Text="Plot" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPPlotNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAlternativeNo" runat="server" Text="Alt. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPAltNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmail" runat="server" Text="Email" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPEmail" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFax" runat="server" Text="Fax" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPFax" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="GbDomicileAddress" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnDomicileAddress" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAttachDomicile" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lbldomicileaddressapproval" runat="server" Visible="false" Text="Pending Approval"
                                                    CssClass="label label-primary"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileAddress1" runat="server" Text="Address1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDAddress1" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileAddress2" runat="server" Text="Address2" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDAddress2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicilePostCountry" runat="server" Text="Post Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDCountry" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileState" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDState" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicilePostTown" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDCity" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicilePostCode" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDZipcode" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileProvince" runat="server" Text="Prov/Region" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDRegion" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileRoad" runat="server" Text="Road/Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDStreet" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileEState" runat="server" Text="EState" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDEState" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileProvince1" runat="server" Text="Prov/Region1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDomicileProvince1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileRoadStreet1" runat="server" Text="Road/Street1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDomicileRoadStreet1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileChiefdom" runat="server" Text="Chiefdom" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDomicileChiefdom" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicilevillage" runat="server" Text="Village" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDomicilevillage" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileTown1" runat="server" Text="Town1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDomicileTown1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileMobileNo" runat="server" Text="Mobile" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDMobile" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileTelNo" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDTelNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                        <asp:Label ID="Label18" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicilePlotNo" runat="server" Text="Plot" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDPlotNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileAltNo" runat="server" Text="Alt. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDAltNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileEmail" runat="server" Text="Email" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDEmail" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDomicileFax" runat="server" Text="Fax" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtDFax" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="gbRecruitmentAddress" runat="server" class="panel-default">
                                    </asp:Panel>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnRecruitmentAddress" runat="server" Text="Recruitment Address" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAttachRecruitment" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblrecruitmentaddressapproval" Visible="false" runat="server" Text="Pending Approval"
                                                    CssClass="pull-right label label-primary"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRAddress1" runat="server" Text="Address1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRAddress1" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRAddress2" runat="server" Text="Address2" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRAddress2" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRCountry" runat="server" Text="Post Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRCountry" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRState" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRState" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRCity" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRCity" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRCode" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRZipcode" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRRegion" runat="server" Text="Prov/Region" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRRegion" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRStreet" runat="server" Text="Road/Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRStreet" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblREState" runat="server" Text="EState" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtREState" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRecruitmentProvince1" runat="server" Text="Prov/Region1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRecruitmentProvince1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRecruitmentRoadStreet1" runat="server" Text="Road/Street1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRecruitmentRoadStreet1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRecruitmentChiefdom" runat="server" Text="Chiefdom" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRecruitmentChiefdom" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRecruitmentVillage" runat="server" Text="Village" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRecruitmentVillage" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRecruitmentTown1" runat="server" Text="Town1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRecruitmentTown1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRPlotNo" runat="server" Text="Plot" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRPlotNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRTelNo" runat="server" Text="Tel. No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRTelNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Update" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ScanAttachment">
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"></asp:Label>
                            </h2>
                            </div>
                        <div class="body">
                            <div class="row clearfix d--f ai--c">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-b-0 d--f">
                                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                        <div id="fileuploader">
                                            <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse"
                                                                onclick="return IsValidAttach();" />
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                        Text="Browse" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 350px">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-hover"
                                        AllowPaging="false">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--0--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--1--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <%--2--%>
                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                            <%--3--%>
                                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                            <%--4--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary pull-left" />
                            <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btn btn-default" />
                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                        </div>
                    </asp:Panel>
                    <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                        IsFireButtonNoClick="false" />
                </div>
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmployeeAddress.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('body').on("click","input[type=file]", function() {
            debugger;
            return IsValidAttach();
        });
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
</asp:Content>
