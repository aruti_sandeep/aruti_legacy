﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeExperienceList.aspx.vb"
    Inherits="HR_wPg_EmployeeExperienceList" MasterPageFile="~/Home1.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Job History And Experience List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpJob" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblSupervisor" runat="server" Text="Supervisor" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtSupervisor" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblInstitution" runat="server" Text="Company" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtCompany" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpStartdate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEnddate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" CssClass="btn btn-primary" runat="server" Text="New" Visible="False" />
                                <asp:Button ID="BtnSearch" CssClass="btn btn-default" runat="server" Text="Search" />
                                <asp:Button ID="BtnReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                    <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btn btn-primary" Text="View Detail" />
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary bg-pw"></asp:Label>
                                    <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" CssClass="label label-danger bg-lc"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <asp:DataGrid ID="GvExpList" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                        AllowPaging="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Select"
                                                            ToolTip="Edit">
                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                    </span>
                                                    <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details"
                                                        CommandName="View" Visible="false"><i class="fas fa-eye text-primary"></i> </asp:LinkButton>
                                                    <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                    <%--'Gajanan [17-DEC-2018] -- End--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<0>--%>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                            <i class="fas fa-trash text-danger"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                    <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<1>--%>
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                            </asp:BoundColumn>
                                            <%--<2>--%>
                                            <asp:BoundColumn DataField="Company" HeaderText="Company" ReadOnly="True" FooterText="colhCompany">
                                            </asp:BoundColumn>
                                            <%--<3>--%>
                                            <asp:BoundColumn DataField="Jobs" HeaderText="Job" ReadOnly="True" FooterText="colhJob">
                                            </asp:BoundColumn>
                                            <%--<4>--%>
                                            <asp:BoundColumn DataField="StartDate" HeaderText="Start Date" ReadOnly="True" FooterText="colhStartDate">
                                            </asp:BoundColumn>
                                            <%--<5>--%>
                                            <asp:BoundColumn DataField="EndDate" HeaderText="End Date" ReadOnly="True" FooterText="colhEndDate">
                                            </asp:BoundColumn>
                                            <%--<6>--%>
                                            <asp:BoundColumn DataField="Supervisor" HeaderText="Supervisor" ReadOnly="True" FooterText="colhSupervisor">
                                            </asp:BoundColumn>
                                            <%--<7>--%>
                                            <asp:BoundColumn DataField="Remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark">
                                            </asp:BoundColumn>
                                            <%--<8>--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <%--<9>--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                            </asp:BoundColumn>
                                            <%--<10>--%>
                                            <%--'Gajanan [17-DEC-2018] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                            <asp:BoundColumn DataField="ExpId" HeaderText="ExpId" Visible="false" FooterText="objdgcolhRefreeTranId">
                                            </asp:BoundColumn>
                                            <%--<11>--%>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                            </asp:BoundColumn>
                                            <%--<12>--%>
                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                        </Columns>
                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
