﻿<%@ Page Title="Employee Dependants" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmpDepentants.aspx.vb" Inherits="wPgEmpDepentants" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

    function GetMousePosition() {
    var img = document.getElementById('<%= imgDependant.ClientID %>' + '_imgZoom');
    if (img != null)
        document.onmousemove = getMouseXY;
    }

//    function FileUploadChangeEvent() {
//        var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
//        if (fupld != null)
//            fileUpLoadChange();
    //    }

    function FileUploadChangeEvent() {
        var img = document.getElementById('<%= imgDependant.ClientID %>' + '_imgZoom');
        if (img != null)
            document.onmousemove = getMouseXY;
            
        var cnt = $('.flupload').length;
        for (i = 0; i < cnt; i++) {
            var fupld = $('.flupload')[i].id;
    if (fupld != null)
                //S.SANDEEP |27-MAY-2022| -- START
                //ISSUE/ENHANCEMENT : IMAGE EVENT BINDING ISSUE
                //fileUpLoadChange($('.flupload')[i].id);                
                    fileUpLoadChange('#' + $('.flupload')[i].id);
                //S.SANDEEP |27-MAY-2022| -- END

        }
    }

        $(document).ready(function() {
        FileUploadChangeEvent();
        });


        var req = Sys.WebForms.PageRequestManager.getInstance();
        req.add_endRequest(function() {
        FileUploadChangeEvent();
        }); 
    </script>

    <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
            var cboEmp = $('#<%= cboEmployee.ClientID %>');
            var cboRelation = $('#<%= DrpRelation.ClientID %>');
            var txtLastname = $('#<%= txtLastName.ClientID %>');
            var txtFirstname = $('#<%= txtFirstName.ClientID %>');
                       
            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            if (parseInt(cboEmp.val()) <= 0) {
                alert('Employee is compulsory information. Please select Employee to continue.');
                cboEmp.focus();
                return false;
            }
            if (parseInt(cboRelation.val()) <= 0) {
                alert('Relation is compulsory information. Please select relation to continue.');
                cboRelation.focus();
                return false;
            }
            if (txtLastname.val().trim() == "") {
                alert('Lastname can not be blank. Lastname is compulsory information.');
                txtLastname.focus();
                return false;
            }
            if (txtFirstname.val().trim() == "") {
                alert('Firstname can not be blank. Firstname is compulsory information.');
                txtFirstname.focus();
                return false;
            }
            
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dependants"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Dependants Add/Edit"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                        role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v">
                                        </i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <asp:LinkButton ID="menuBenefitInfor" runat="server" Text="Benefit Info"></asp:LinkButton></li>
                                            <li>
                                                <asp:LinkButton ID="mnuMembershipInfo" runat="server" Text="Membership Info"></asp:LinkButton></li>
                                            <li>
                                                <asp:LinkButton ID="lnkCopyAddress" runat="server" CssClass="waves-effect waves-block"
                                                    Text="Copy Address From Employee"></asp:LinkButton>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 d--f jc--c ai--c">
                                        <uc8:ZoomImage ID="imgDependant" runat="server" ZoomPercentage="250" />
                                        <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <uc9:FileUpload ID="flUpload" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:LinkButton ID="btnRemoveImage" runat="server" ToolTip="Remove Image" CssClass="m-l-5 btn btn-danger btn-circle waves-effect waves-circle waves-float">
                                        <i class="fas fa-trash"></i>
                                        </asp:LinkButton>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBAddress" runat="server" Text="Address" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtAddress" runat="server" Rows="2" TextMode="MultiLine" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBFirstName" runat="server" Text="First Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBMiddleName" runat="server" Text="Middle Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBLastName" runat="server" Text="Last Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBBirthDate" runat="server" Text="Birthdate" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="True" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAge" runat="server" Text="Age" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAge" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpGender" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBRelation" runat="server" Text="Relation" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="DrpRelation" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkBeneficiaries" runat="server" Text="Treat as a Beneficiaries" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBPostCountry" runat="server" Text="Post Country" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpcountry" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblState" runat="server" Text="Post State" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpState" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBPostTown" runat="server" Text="Post Town" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drppostTown" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBPostCode" runat="server" Text="Post Code" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drppostcode" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBNationality" runat="server" Text="Nationality" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpnationality" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBEmail" runat="server" Text="Email" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBPostBox" runat="server" Text="Post Box" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtpostbox" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBIdNo" runat="server" Text="ID No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtidno" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBResidentialNo" runat="server" Text="Res. Tel No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtResNo" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBMobileNo" runat="server" Text="Mobile No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtmobileno" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" AutoPostBack="False" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAttachmentHeader" runat="server" Text="Document Attachment" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix d--f ai--c">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAttachmentDate" runat="server" Text="Attachment Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpAttachment" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 d--f ai--c">
                                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                            <div id="fileuploader">
                                                                <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                                    value="Browse" />
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse"
                                                            OnClick="btnSaveAttachment_Click" />
                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-default" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive">
                                                            <asp:DataGrid ID="dgvDependant" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                CssClass="table table-hover table-bordered" HeaderStyle-CssClass="griviewheader"
                                                                ItemStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                                                <Columns>
                                                                    <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                    ToolTip="Delete"></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                    <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                        ItemStyle-HorizontalAlign="Right" />
                                                                    <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                        ItemStyle-Font-Size="22px">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete"> 
                                                                            <i class="fa fa-download"></i> </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                        Visible="false" />
                                                                    <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                    <cc1:ModalPopupExtender ID="popup_MembershipInfo" runat="server" TargetControlID="hdf_MembershipInfo"
                    DropShadow="false" CancelControlID="hdf_MembershipInfo" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnl_MembershipInfo">
                    </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_MembershipInfo" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                                <asp:Label ID="lblmem_Title" runat="server" Text="Membership Info."></asp:Label>
                        </h2>
                    </div>
                    <div class="body m-b--25">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblMembershipCategory" runat="server" Text="Mem. Category" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboMemCategory" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblMemberNo" runat="server" Text="Mem. No." CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtMembershipNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblDBMedicalMembershipNo" runat="server" Text="Medical Mem." CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboMedicalNo" runat="server" />
                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnAddMembership" runat="server" Text="Add" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnEditMembership" runat="server" Text="Edit" CssClass="btn btn-primary"
                                                Visible="false" />
                                        <asp:Button ID="btnMemClear" runat="server" Text="Clear" CssClass="btn btn-default" />
                                    </div>
                                        </div>
                                    </div>
                                </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12 m-t--25">
                                <div class="card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 130px;">
                                                    <asp:DataGrid ID="lvMembershipInfo" runat="server" CssClass="table table-hover table-bordered"
                                                        AutoGenerateColumns="false" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                        Width="99%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <%--        <asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                                                ToolTip="Edit" />--%>
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="objEdit">
                                                                                    <i class="fas fa-pencil-alt text-primary"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <%--  <asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                                            ToolTip="Delete" />--%>
                                                                        <asp:LinkButton ID="imgDelete" runat="server" ToolTip="Delete" CommandName="objDelete">
                                                                                <i class="fas fa-trash text-danger"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="Category" HeaderText="Membership Type" FooterText="colhMembershipType" />
                                                        <asp:BoundColumn DataField="membershipname" HeaderText="Membership" FooterText="colhMembership" />
                                                        <asp:BoundColumn DataField="membershipno" HeaderText="Membership No" FooterText="colhMembershipNo" />
                                                        <asp:BoundColumn DataField="membership_categoryunkid" FooterText="objcolhMembershipCatId"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="membershipunkid" FooterText="objcolhMembershipCatId"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="dpndtmembershiptranunkid" FooterText="objcolhMembershipUnkid"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="GUID" FooterText="objcolhMGUID" Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                            <asp:HiddenField ID="hdf_MembershipInfo" runat="server" />
                                        <asp:Button ID="btnMem_Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnMem_Close" runat="server" Text="Close" CssClass="btn btn-default" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popup_BenefitInfo" runat="server" TargetControlID="hdf_BenefitInfo"
                    CancelControlID="hdf_BenefitInfo" DropShadow="false" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnl_BenefitInfo">
                    </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_BenefitInfo" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                                <asp:Label ID="lblBenefit_Title" runat="server" Text="Benefit Info."></asp:Label>
                        </h2>
                            </div>
                    <div class="body m-b--30">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                                <asp:Label ID="lblBenefitGroup" runat="server" Text="Benefit Group" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboBenefitGroup" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblValueBasis" runat="server" Text="Benefit In" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboValueBasis" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblDBBenefitType" runat="server" Text="Benefit Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboBenefitType" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblBenefitsInPercent" runat="server" Text="Perc. (%)" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtBenefitPercent" runat="server" CssClass="decimal form-control"
                                                        Style="text-align: right"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblDBBenefitInAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtBenefitAmount" runat="server" CssClass="decimal form-control"
                                                        Style="text-align: right"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblTotalPercent" runat="server" Text="Allocated (%)" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                    <asp:TextBox ID="txtTotalPercentage" runat="server" ReadOnly="true" Style="text-align: right"
                                                            Text="0.00" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblTotalRemainingPercent" runat="server" Text="Remaining (%)" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                    <asp:TextBox ID="txtRemainingPercent" runat="server" ReadOnly="true" Style="text-align: right"
                                                            Text="0.00" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnAddBenefit" runat="server" Text="Add" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnEditBenefit" runat="server" Text="Edit" CssClass="btn btn-primary"
                                                Visible="false" />
                                        <asp:Button ID="btnBenefitClear" runat="server" Text="Clear" CssClass="btn btn-default" />
                                    </div>
                                        </div>
                                    </div>
                                </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12 m-t--25">
                                <div class="card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 125px;">
                                                    <asp:DataGrid ID="lvBenefit" runat="server" CssClass="table table-hover table-bordered"
                                                        AutoGenerateColumns="false" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                        Width="99%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                    <%--<asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                                            ToolTip="Edit" />--%>
                                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="objEdit">
                                                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                    </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                    <%--  <asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                                            ToolTip="Delete" />--%>
                                                                    <asp:LinkButton ID="imgDelete" runat="server" ToolTip="Delete" CommandName="objDelete">
                                                                                    <i class="fas fa-trash text-danger"></i>
                                                                    </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="benefit_group" HeaderText="Benefit Group" FooterText="colhBenefitGroup" />
                                                        <asp:BoundColumn DataField="benefit_type" HeaderText="Benefit Type" FooterText="colhBenefitType" />
                                                        <asp:BoundColumn DataField="benefit_value" HeaderText="Value Basis" FooterText="colhValueBasis" />
                                                        <asp:BoundColumn DataField="benefit_percent" HeaderText="Percent(%)" FooterText="colhPercent"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                        <asp:BoundColumn DataField="benefit_amount" HeaderText="Amount" FooterText="colhAmount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                        <asp:BoundColumn DataField="benefitgroupunkid" FooterText="objcolhBenefitGrpId" Visible="false" />
                                                        <asp:BoundColumn DataField="benefitplanunkid" FooterText="objcolhBenefitId" Visible="false" />
                                                        <asp:BoundColumn DataField="value_id" FooterText="objcolhValueBasisId" Visible="false" />
                                                        <asp:BoundColumn DataField="dpndtbenefittranunkid" Visible="false" />
                                                        <asp:BoundColumn DataField="GUID" FooterText="objcolhBGUID" Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                            <asp:HiddenField ID="hdf_BenefitInfo" runat="server" />
                                        <asp:Button ID="btnBenefit_Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnBenefit_Close" runat="server" Text="Close" CssClass="btn btn-default" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
		    ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
		    $("#fileuploader").uploadFile({
			    url: "wPgEmpDepentants.aspx?uploadimage=mSEfU19VPc4=",
                method: "POST",
				dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
				$("#<%= btnAddAttachment.ClientID %>").click();
                },
                    onError: function(files, status, errMsg) {
	                alert(errMsg);
                }
            });
        }
        }
        $('body').on("click", 'input[type=file]', function() {
            if ($(this).attr("class") != "flupload") {
		    return IsValidAttach();
		    }
        });
    </script>

</asp:Content>
